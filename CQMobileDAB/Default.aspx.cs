using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                ProfileCommon pc = this.Profile.GetProfile(Profile.UserName);
                string myTheme = this.Profile.GetPropertyValue("Theme").ToString();

                //if (pc == null || myTheme == "None")
                //{
                //    Response.Redirect("~/Screens/ChooseTheme.aspx");
                //    return;
                //}

                if (myTheme == "VehicleMount")
                    Response.Redirect("~/Screens/MainMenuVM.aspx");
                else if (myTheme == "Wearable")
                    Response.Redirect("~/Screens/MainMenuWT.aspx");
                else
                    Response.Redirect("~/Screens/MainMenu.aspx");
            }
        }
        catch { }
    }
}
