using System;
using System.Data;
using System.Text;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// J:107  send through a 1
/// </summary>
public partial class Screens_CheckPallet : System.Web.UI.Page
{
    #region PrivateVariables
    private int result = 0;
    private string strresult = "";
    private string theErrMethod = "";
    #endregion PrivateVariables

    #region Page_Load 
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = "CHECK PALLET";

            if (!Page.IsPostBack)
            {
                TextBoxPalletId.Text = Session["PalletId"].ToString();

                CheckConfig();
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion Page_Load

    #region CommandCancel_Click 
    protected void CommandCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Screens/MainMenu.aspx");
    }
    #endregion CommandCancel_Click

    #region CommandNext_Click 
    protected void CommandNext_Click(object sender, EventArgs e)
    {
        try
        {
            result = CheckPallet_DataBind(0, 1);
            //Session["InstructionId"] = result.ToString();
            if (result != 0)
            {
                MultiView1.ActiveViewIndex = 0;
            }
            else
            {
                Session["ActionDoneCheckStoreFull"] = true;
                Response.Redirect(Session["FromUrl"].ToString());
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion CommandNext_Click

    #region CommandTryAgain_Click 
    protected void CommandTryAgain_Click(object sender, EventArgs e)
    {
        // MultiView1.ActiveViewIndex = 0;
        MultiView1.ActiveViewIndex = 0;
    }
    #endregion CommandTryAgain_Click

    #region CheckPallet_DataBind 
    protected int CheckPallet_DataBind(int type, int operatorId)
    {
        int palletId = RemovePalletScanChar();

        try
        {
            string barcode = TextBoxBarcode.Text;
            string batch = TextBoxBatch.Text;
            string quantity = TextBoxQuantity.Text;
            Decimal quantityInt = -1;

            MultiView1.ActiveViewIndex = 1;

            //// Product
            //if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 32))
            //{
            //    TextBoxBarcode.Visible = false;
            //    LabelBarcode.Visible = false;

            //    TextBoxBarcode.TabIndex = 0;
            //    TextBoxBatch.TabIndex = 1;

            //    TextBoxBatch.Focus();

            //    Master.MsgText = "Enter Batch Id";
            //}
            //else
            //{
            //    if (barcode == "")
            //    {

            //        TextBoxBarcode.Visible = true;
            //        LabelBarcode.Visible = true;

            //        TextBoxBarcode.TabIndex = 1;
            //        TextBoxBatch.TabIndex = 2;
            //        TextBoxQuantity.TabIndex = 3;

            //        TextBoxBarcode.Focus();

            //        Master.MsgText = "Enter Product Barcode";

            //        return -1;

            //    }
            //    else
            //        barcode = TextBoxBarcode.Text;
            //}
            //// Batch
            //if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 34))
            //{

            //    TextBoxBatch.Visible = false;
            //    TextBoxBatch.Visible = false;

            //    TextBoxBatch.TabIndex = 0;
            //    TextBoxQuantity.TabIndex = 1;

            //    TextBoxQuantity.Focus();

            //    Master.MsgText = "Enter Quantity";

            //}
            //else
            //{
            //    if (batch == "")
            //    {

            //        TextBoxBatch.Visible = true;
            //        LabelBatch.Visible = true;

            //        TextBoxBatch.TabIndex = 1;
            //        TextBoxQuantity.TabIndex = 2;

            //        TextBoxBatch.Focus();

            //        Master.MsgText = "Enter Batch";

            //        return -1;

            //    }
            //    else
            //        batch = TextBoxBatch.Text;
            //}

            if (TextBoxPalletId.Text == "")
            {
                Master.MsgText = "Enter Pallet Id";

                TextBoxPalletId.TabIndex = 1;
                TextBoxPalletId.Focus();
                TextBoxPalletId.Text = "";

                return -1;
            }

            if (quantity != "")
            {
                /****************************** Quantity ****************************/

                if (Decimal.TryParse(TextBoxQuantity.Text, out quantityInt))
                {
                    Master.MsgText = "Valid Quantity";
                }
                else
                {
                    Master.MsgText = "Not Valid Quantity";

                    return -1;
                }
            }
            else
            {

                Master.MsgText = "Enter Quantity";

                TextBoxQuantity.TabIndex = 1;
                TextBoxQuantity.Focus();
                TextBoxQuantity.Text = "";

                return -1;
            }
            
            CheckPallet checkPallet = new CheckPallet();

            DataSet ds = checkPallet.Verify(type, operatorId, palletId, barcode, batch, quantityInt);

            result = int.Parse(ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Result"].Ordinal).ToString());

            string msg = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Message"].Ordinal).ToString();

            if (result == 2) // Incorrect status
            {
                Master.MsgText = msg;
                return result;
            }
            else if (result == 3) // Incorrect barcode
            {
                Master.MsgText = msg;

                TextBoxQuantity.TabIndex = 1;
                TextBoxQuantity.Text = "";
                TextBoxQuantity.Focus();

                return result;
            }
            else if (result == 4) // Incorrect batch
            {
                Master.MsgText = msg;

                TextBoxQuantity.TabIndex = 1;
                TextBoxQuantity.Text = "";
                TextBoxQuantity.Focus();

                return result;
            }
            else if (result == 5) // Incorrect quantity
            {
                Master.MsgText = msg;

                TextBoxQuantity.TabIndex = 1;
                TextBoxQuantity.Text = "";
                TextBoxQuantity.Focus();

                return result;
            }
            else if (result != 0)
            {
                Master.MsgText = msg;
                return result;
            }
            else if (result == 0)
            {
                Master.MsgText = msg;
            }
            else
                Master.MsgText = msg;

            return result;
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

        return palletId;
    }
    #endregion CheckPallet_DataBind
    
    #region RemovePalletScanChar
    protected int RemovePalletScanChar()
    {
        string strPalletId = "";
        int palletId = 0;
      
        if (TextBoxPalletId.Text.Length >= 1)
        {
            try
            {
                strPalletId = TextBoxPalletId.Text;

                if (TextBoxPalletId.Text.StartsWith("P:"))
                {
                    if (!int.TryParse(TextBoxPalletId.Text, out palletId))
                    {
                        // Remove first 2 char.
                        StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                        strPalletId = strReader.ReadLine();
                        palletId = int.Parse(strPalletId);
                        return palletId;
                    }

                }
                else if (TextBoxPalletId.Text.StartsWith("J:"))
                {
                    if (!int.TryParse(TextBoxPalletId.Text, out palletId))
                    {
                        // Remove first 2 char.
                        StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                        strPalletId = strReader.ReadLine();
                        palletId = int.Parse(strPalletId);
                        return palletId;
                    }
                }
                else
                {
                    //this is just a number then use as is
                    if (int.TryParse(TextBoxPalletId.Text, out palletId))
                    {
                        strPalletId = TextBoxPalletId.Text;
                        palletId = int.Parse(strPalletId);
                       
                    }
                    else
                    {
                        Master.MsgText = "Invalid PalletID";
                        return -1;
                    }
                }
            }
            catch (Exception ex)
            {
                string errMsgReturn = SendErrorNow(ex.Message);
                Master.MsgText = errMsgReturn;
            }
        }

        return palletId;
    }
    #endregion

    #region CheckConfig
    protected void CheckConfig()
    {
        try
        {
            Configuration config = new Configuration();

            // Product Check
            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehhouseId"], 32))
            {
                TextBoxBarcode.Text = "";
                LabelBarcode.Visible = true;
                TextBoxBarcode.Visible = true;
            }
            else
            {
                TextBoxBarcode.Text = "0";
                LabelBarcode.Visible = false;
                TextBoxBarcode.Visible = false;
            }

            // Batch Check
            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehhouseId"], 34))
            {
                TextBoxBatch.Text = "";
                LabelBatch.Visible = true;
                TextBoxBatch.Visible = true;
            }
            else
            {
                TextBoxBatch.Text = "0";
                LabelBatch.Visible = false;
                TextBoxBatch.Visible = false;
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        int loopPrevention = -1;

        try
        {
            try
            {
                loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());
            }
            catch
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                loopPrevention = 0;
            }

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                  Resources.Default.CheckPalletTitle.ToString(),
                                                                  theErrMethod,
                                                                  ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {

                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
            }

        }
        catch 
        {

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                 Resources.Default.CheckPalletTitle.ToString(),
                                                                 theErrMethod,
                                                                 ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling
}
