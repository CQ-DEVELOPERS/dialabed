using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Help : System.Web.UI.Page
{
    #region Private Variables 
    DataSet result = new DataSet();
    #endregion Private Variables

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["OperatorId"] = 1;

        if (Request.QueryString.Count > 0)
        {
            ArrayList arrList = new ArrayList();

            for (int i = 0; i >= Request.QueryString.Count - 1; i++)
            {
                arrList.Add(Request.QueryString.Keys[i].ToString());       
            }              
          
            //Send this to the DB 
            string Error = arrList[0].ToString();

            CQHelp getHelp = new CQHelp();
            result = getHelp.ReturnHelpFile(Error);
          
        }

    }
  
}
