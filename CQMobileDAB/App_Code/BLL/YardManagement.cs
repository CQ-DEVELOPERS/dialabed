using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   20 July 2007
/// Summary description for OutboundShipment
/// </summary>
public class YardManagement
{
    #region Constructor Logic

    public YardManagement()
    {
    }
    #endregion "Constructor Logic"

    #region ContainerIn
    public int ContainerIn(string connectionStringName, int warehouseId, int operatorId, string barcode, string location)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Scan";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "in", DbType.Boolean, true);
        db.AddInParameter(dbCommand, "version", DbType.Int32, 2);
        db.AddInParameter(dbCommand, "location", DbType.String, location);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ContainerIn

    #region ContainerOut
    public int ContainerOut(string connectionStringName, int warehouseId, int operatorId, string barcode)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Scan";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "out", DbType.Boolean, true);
        db.AddInParameter(dbCommand, "version", DbType.Int32, 2);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ContainerOut
}
