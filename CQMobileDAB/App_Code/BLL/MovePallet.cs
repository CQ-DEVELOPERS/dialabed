using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
/// <summary>
/// Summary description for MovePallet
/// </summary>
public class MovePallet
{
	public MovePallet()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    #region AutoMovePallet
    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="palletId"></param>
    /// <param name="storeLocation"></param>
    /// <param name="confirmedQuantity"></param>
    /// <returns></returns>
    public DataSet AutoMovePallet(int type, int operatorId, int palletId, int newPalletId, string pickLocation,
                                  Decimal confirmedQuantity, string storeLocation, int instructionId) 
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_Mobile_Movement";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
            db.AddInParameter(dbCommand, "newPalletId", DbType.Int32, newPalletId);
            db.AddInParameter(dbCommand, "pickLocation", DbType.String, pickLocation);
            db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
            db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);
            db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

            dataSet = db.ExecuteDataSet(dbCommand);
           
        }
        catch
        { }

        return dataSet;

    }
    #endregion AutoMovePallet

    #region UpdateMove
    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="palletId"></param>
    /// <param name="newPalletId"></param>
    /// <param name="pickLocation"></param>
    /// <param name="confirmedQuantity"></param>
    /// <param name="storeLocation"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public DataSet UpdateMove(int type, int operatorId, int palletId, int newPalletId, string pickLocation,
                                Decimal confirmedQuantity, string storeLocation, int instructionId)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;
        try
        {

            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_Mobile_Movement";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
            db.AddInParameter(dbCommand, "newPalletId", DbType.Int32, newPalletId);
            db.AddInParameter(dbCommand, "pickLocation", DbType.String, pickLocation);
            db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
            db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);
            db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

            dataSet = db.ExecuteDataSet(dbCommand);

            //Cleanup and dispose
            db = null;
            dbCommand.Dispose();

        }
        catch (DbException dbexc)
        {
            string dbEx = dbexc.Message.ToString();

        }
        return dataSet;
    }
    #endregion UpdateMove

    #region CheckPalletInfo
    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="palletId"></param>
    /// <param name="productCode"></param>
    /// <param name="confirmBatch"></param>
    /// <param name="confirmedQuantity"></param>
    /// <returns></returns>
    public DataSet CheckPalletInfo(int type, int operatorId, int palletId, string productCode, int confirmBatch, Decimal confirmedQuantity)
    {

        DataSet ds = new DataSet();

        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_Mobile_Check_Pallet";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
            db.AddInParameter(dbCommand, "newPalletId", DbType.String, productCode);
            db.AddInParameter(dbCommand, "pickLocation", DbType.String, confirmBatch);
            db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);


            ds = db.ExecuteDataSet(dbCommand);

            //Cleanup and dispose
            db = null;
            dbCommand.Dispose();            
        }
        catch (DbException dbex)
        {
            string sdbEx = dbex.Message.ToString();
        }

        return ds;
    }
    #endregion CheckPalletInfo

    #region GetMoveInstruction
    /// <summary>
    /// Here the operator just got the info and cliked on "Find" therefore I don't think we need to send all the info through again.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="palletId"></param>
    /// <param name="productCode"></param>
    /// <param name="confirmBatch"></param>
    /// <param name="confirmedQuantity"></param>
    /// <returns></returns>
    //public DataSet FinAutoMoveLocation(int type, int operatorId, int palletId, string productCode, int confirmBatch, int confirmedQuantity)
    public int GetMoveInstruction(string connectionStringName, int operatorId)  
    {
          int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "p_Mobile_Movement_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);      

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetMoveInstruction

    #region AutoMoveDetails

   
    /*The types are:
          	    @type = 0 -- Get Pallet Info
    --Optional 	@type = 1 -- Cancel
                @type = 2 -- Product Picked
                @type = 4 -- Product Binned
    --Optional 	@type = 5 -- Loation Error
           	    @type = 6 -- Confirm Quantity
    --Optional	@type = 7 -- Find location
    */
    /// <summary>
    /// <remarks>Get Pallet Info p_Mobile_Movement 0, 1, 2, null, null, null, null, null</remarks>
    /// <remarks>Cancel Movement p_Mobile_Movement 1, 1, 2, null, null, null, null, null</remarks>
    /// </summary>
    /// <param name="conStrName"></param>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="palletId"></param>
    /// <param name="newPalletId"></param>
    /// <param name="pickLocation"></param>
    /// <param name="confirmedQuantity"></param>
    /// <param name="storeLocation"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public DataSet AutoMoveDetails(string conStrName,  
                                    int type,                             
                                    int operatorId, 
                                    int palletId,  
                                    int newPalletId,  
                                    string  pickLocation,
                                    Decimal confirmedQuantity,
                                    string storeLocation,
                                    int instructionId)
    {
        
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(conStrName);
      
        
        #region CREATE NULLS 
        //:(  Grant said I should

        ArrayList moveVariable = new ArrayList();
        moveVariable.Add(palletId.ToString());
        moveVariable.Add(newPalletId.ToString());
        moveVariable.Add(pickLocation);
        moveVariable.Add(confirmedQuantity.ToString());
        moveVariable.Add(storeLocation);
        moveVariable.Add(instructionId.ToString());
       
        for (int i = 0; i <= moveVariable.Count - 1; i++)
        {
            if (moveVariable[i].ToString() == "")
                moveVariable[i] = null;
            else if (moveVariable[i].ToString() == "0")
                moveVariable[i] = null;
            else if (i == 0 || i == 1 || i == 3 || i == 5)
            { 
                moveVariable[i] = int.Parse(moveVariable[i].ToString());
            
            }
        }

        #endregion 

        string sqlCommand = "p_Mobile_Movement";
       
        ///string sqlCommand = "p_Mobile_Putaway_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "type", DbType.Int32, type);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, moveVariable[0]);
        db.AddInParameter(dbCommand, "newPalletId", DbType.Int32, moveVariable[1]);
        db.AddInParameter(dbCommand, "pickLocation", DbType.String, moveVariable[2]);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, moveVariable[3]);
        db.AddInParameter(dbCommand, "storeLocation", DbType.String, moveVariable[4]);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, moveVariable[5]);     

        // DataSet that will hold the returned results		
        DataSet dataSet = new DataSet();
        //int iResult = -1;
        dataSet = db.ExecuteDataSet(dbCommand);
        //iResult = db.ExecuteDataSet(dbCommand);

        return dataSet;
        //return iResult;
    }
    #endregion AutoMoveDetails


}
