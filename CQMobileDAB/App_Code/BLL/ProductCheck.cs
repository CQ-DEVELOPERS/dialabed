using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

/// <summary>
/// Summary description for Transact
/// </summary>
public class ProductCheck
{
    public string skuCode = "1234567890";
    public Decimal quantity = -1;

    #region Constructor Logic
    public ProductCheck()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region ConfirmPallet
    public DataSet ConfirmPallet(string connectionStringName, int warehouseId, string referenceNumber)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Mobile_Product_Check_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmPallet

    #region ConfirmProduct
    public DataSet ConfirmProduct(string connectionStringName, int warehouseId, int jobId, string barcode)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Mobile_Product_Check_Barcode";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmProduct

    #region ConfirmBatch
    public DataSet ConfirmBatch(string connectionStringName, int warehouseId, int jobId, int storageUnitId, string batch)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Mobile_Product_Check_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmBatch

    #region ConfirmQuantity
    public DataSet ConfirmQuantity(string connectionStringName, int warehouseId, int jobId, int storageUnitBatchId, decimal quantity, bool scanMode, string barcode)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Mobile_Product_Check_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "scanMode", DbType.Boolean, scanMode);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmQuantity

    #region ConfirmFinished
    public int ConfirmFinished(string connectionStringName, int jobId, int operatorId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Product_Check_Finished";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmFinished

    #region DefaultQuantity
    public int DefaultQuantity(string connectionStringName, int warehouseId, string barCode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Product_Check_Get_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "barCode", DbType.String, barCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DefaultQuantity

    #region NewBox
    public int NewBox(string connectionStringName, int jobId, int operatorId, string referenceNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Product_Check_New_Box";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion NewBox

    #region NewBoxInstruction
    public int NewBoxInstruction(string connectionStringName, int instructionId, int operatorId, string referenceNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Product_Check_New_Box";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion NewBoxInstruction

    #region GetContainerType
    /// <summary>
    /// Get the jobs container type
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns></returns>
    public int GetContainerType(string connectionStringName
                                    , int JobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        int ContainerTypeId = 0;

        string sqlCommand = "p_Mobile_Product_Check_ContainerType";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddOutParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);

        db.ExecuteNonQuery(dbCommand);

        ContainerTypeId = (int)db.GetParameterValue(dbCommand, "@ContainerTypeId");

        return ContainerTypeId;
    }
    #endregion

    #region SetContainerType
    /// <summary>
    /// Get the jobs container type
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns></returns>
    public int SetContainerType(string connectionStringName
                                    , int JobId
                                    , int ContainerTypeId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Product_Check_ContainerType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);

        db.ExecuteNonQuery(dbCommand);

        return ContainerTypeId;
    }
    #endregion

    #region SearchContainerTypes
    /// <summary>
    /// Searches for a Container Type
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns></returns>
    public DataSet SearchContainerTypes(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_ContainerType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchContainerTypes"

    #region SearchPackagingJob
    /// <summary>
    /// Searches for packing linked to Principals on a specific job
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns></returns>
    public DataSet SearchPackagingJob(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Packaging_Job_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPackagingJob"

    #region ConfirmWeight
    public bool ConfirmWeight(string connectionStringName, int jobId, decimal weight)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Product_Check_Weight";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmWeight
}