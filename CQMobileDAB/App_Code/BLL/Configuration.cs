using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class Configuration
{
    #region Constructor Logic
    public Configuration()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region Configuration
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public static bool GetSwitch(string connectionStringName, int warehouseId, int configurationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_GetSwitch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    
    public static bool GetSwitchReceiptLineId(string connectionStringName, int receiptLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_GetSwitch_RecipetLine";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Configuration

    #region GetIntValue
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public int GetIntValue(string connectionStringName, int warehouseId, int configurationId)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Get_Value";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (Int32)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetIntValue

    #region GetLotAttributeRule

    public static LotAttributeRule GetLotAttricuteRule(string connectionStringName, int receiptLineId, int storageUnitId)
    {
        LotAttributeRule result = LotAttributeRule.None;
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_GetLotAttributeRule";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (LotAttributeRule)Enum.Parse(typeof(LotAttributeRule), db.ExecuteScalar(dbCommand).ToString(), true);
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    #endregion

}
