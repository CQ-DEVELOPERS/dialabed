using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

/// <summary>
/// Summary description for Transact
/// </summary>
public class Transact
{
    #region Constructor Logic
    public Transact()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region ConfirmProduct
    public int ConfirmProduct(string connectionStringName, int instructionId, string barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Confirm_Product";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "instructionId", DbType.Int32, ParameterDirection.InputOutput, "instructionId", DataRowVersion.Proposed, instructionId);
        //db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                //result = (int)db.ExecuteScalar(dbCommand);

                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                result = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmProduct

    #region ConfirmBatch
    public int ConfirmBatch(string connectionStringName, int instructionId, string barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Confirm_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmBatch

    #region ConfirmStoreLocation
    public int ConfirmStoreLocation(string connectionStringName, int instructionId, string storeBarcode, int operatorId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Confirm_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "storeBarcode", DbType.String, storeBarcode);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmStoreLocation

    #region ConfirmPickLocation
    public int ConfirmPickLocation(string connectionStringName, int instructionId, string pickBarcode, int operatorId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Confirm_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "pickBarcode", DbType.String, pickBarcode);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmPickLocation

    #region ConfirmQuantity
    public int ConfirmQuantity(string connectionStringName, int instructionId, Decimal confirmedQuantity)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Confirm_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmQuantity

    #region TareWeight
    public int TareWeight(string connectionStringName, int instructionId, decimal tareWeight)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Tare_Weight";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "tareWeight", DbType.Decimal, tareWeight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion TareWeight

    #region CancelTransaction
    public int CancelTransaction(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Cancel_Transaction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CancelTransaction

    #region Pick
    public int Pick(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Pick_Transaction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Pick

    #region Store
    public int Store(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Store_Transaction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Store

    #region GetPickingFull
    public int GetPickingFull(string connectionStringName, int operatorId)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Picking_Full";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch (Exception ex)
            {
                string str = ex.Message.ToString();
            }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPickingFull

    //#region GetPickingMixed
    //public int GetPickingMixed(string connectionStringName, int operatorId)
    //{
    //    int instructionId = 0;

    //    // Create the Database object
    //    Database db = DatabaseFactory.CreateDatabase(connectionStringName);

    //    string sqlCommand = "p_Mobile_Picking_Mixed";
    //    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

    //    db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

    //    // Set InstructionId as an output parameter
    //    db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

    //    using (DbConnection connection = db.CreateConnection())
    //    {
    //        connection.Open();

    //        try
    //        {
    //            // Execute the query
    //            db.ExecuteNonQuery(dbCommand);

    //            // Get InstructionId output parameter
    //            instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
    //        }
    //        catch (Exception ex)
    //        {
    //            string str = ex.Message.ToString();
    //        }

    //        connection.Close();

    //        return instructionId;
    //    }
    //}
    //#endregion GetPickingMixed

    #region GetPickingMixed
    public int GetPickingMixed(string connectionStringName, int operatorId, string referenceNumber, int instructionId)
    {
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Picking_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "instructionId", DbType.Int32, ParameterDirection.InputOutput, "instructionId", DataRowVersion.Proposed, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPickingMixed

    #region GetPickingMixedPrevious
    public int GetPickingMixedPrevious(string connectionStringName, int operatorId, string referenceNumber, int instructionId, Boolean next)
    {
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Picking_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "next", DbType.String, next);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "instructionId", DbType.Int32, ParameterDirection.InputOutput, "instructionId", DataRowVersion.Proposed, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPickingMixedPrevious

    #region GetPutawayFull
    public int GetPutawayFull(string connectionStringName, int operatorId, int palletId)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Store_Full";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    public int GetPutawayFull(string connectionStringName, int operatorId, int palletId, int warehouseId)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Store_Full_By_Warehouse";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPutawayFull

    #region GetPutawayMixed
    public int GetPutawayMixed(string connectionStringName, int operatorId, string barcode)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Store_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    public int GetPutawayMixed(string connectionStringName, int operatorId, string barcode, int warehouseId)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Store_Mixed_By_Warehouse";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPutawayMixed

    #region GetPutawayMixedNextLine
    public int GetPutawayMixedNextLine(string connectionStringName, int operatorId, int instructionId, string barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Store_Mixed_Next";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "instructionId", DbType.Int32, ParameterDirection.InputOutput, "instructionId", DataRowVersion.Proposed, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                result = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetPutawayMixedNextLine

    #region PalletCreate
    public int PalletCreate(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Movement_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "instructionId", DbType.Int32, ParameterDirection.InputOutput, "instructionId", DataRowVersion.Proposed, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                result = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion PalletCreate

    #region PalletUpdate
    public bool PalletUpdate(string connectionStringName, int instructionId, int palletId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Movement_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion PalletUpdate

    #region GetInstructionDetails
    public DataSet GetInstructionDetails(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Instruction_Detail";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructionDetails

    #region GetJobDetails
    public DataSet GetJobDetails(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Job_Detail";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetJobDetails

    #region InstructionUpdate
    public bool InstructionUpdate(string connectionStringName, int operatorId, int instructionId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Instruction_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion InstructionUpdate

    #region GetStoreLocation
    public String GetStoreLocation(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Get_StoreLocation";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        String result = null;

        try
        {
            result = (string)db.ExecuteScalar(dbCommand);
        }
        catch { }

        return result;
    }
    #endregion GetStoreLocation

    #region GetPickLocation
    public String GetPickLocation(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Get_PickLocation";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        String result = null;

        try
        {
            result = (string)db.ExecuteScalar(dbCommand);
        }
        catch { }

        return result;
    }
    #endregion GetPickLocation

    #region GetMixedDetails
    public DataSet GetMixedDetails(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Mixed_Detail";
        ///string sqlCommand = "p_Mobile_Putaway_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetMixedDetails

    #region LocationErrorStore
    public int LocationErrorStore(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Location_Error";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "storeError", DbType.Boolean, 1);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public int ManualErrorStore(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Location_Error";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "storeError", DbType.Boolean, 1);
        db.AddInParameter(dbCommand, "createStockTake", DbType.Boolean, 0);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion LocationErrorStore

    #region LocationErrorPick
    public int LocationErrorPick(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Location_Error";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "pickError", DbType.Boolean, 1);
        db.AddInParameter(dbCommand, "createStockTake", DbType.Boolean, 1);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion LocationErrorPick

    #region SendToPickFace
    public int SendToPickFace(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Send_To_Pickface";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SendToPickFace

    #region SearchProducts
    /// <summary>
    /// Search Products by Product Description and Product Code.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="productCode"></param>
    /// <param name="barcode"></param>
    /// <returns></returns>
    public DataSet SearchProducts(string connectionStringName, string productCode, string product, string skuCode, string batch, string barcode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Product_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchProducts

    #region NewJob
    public int NewJob(string connectionStringName, int operatorId, string referenceNumber, int containerId, int jobId, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_New_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "containerId", DbType.Int32, containerId);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "jobId", DbType.Int32, ParameterDirection.InputOutput, "jobId", DataRowVersion.Proposed, jobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                result = (int)db.GetParameterValue(dbCommand, "jobId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion NewJob

    #region GetContainerTypes
    public DataSet GetContainerTypes(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_ContainerType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetContainerTypes

    #region ValidLocation
    public bool ValidLocation(string connectionStringName, string barcode)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ValidLocation

    #region ValidProduct
    public bool ValidProduct(string connectionStringName, string barcode)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public bool ValidProduct(string connectionStringName, string barcode, string type)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "type", DbType.String, type);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ValidProduct

    #region VariableWeight
    public bool VariableWeight(string connectionStringName, string barcode)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Variable_Weight";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion VariableWeight

    #region ValidBatch
    public bool ValidBatch(string connectionStringName, string productCode, string batch)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ValidBatch

    #region ValidPalletStatus
    public string ValidPalletStatus(string connectionStringName, string barcode)
    {
        string result = "";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Pallet_Get_Status";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            connection.Close();

            return result;
        }
    }
    #endregion

    #region ValidStockLink
    public DataSet ValidStockLink(string connectionStringName, string location, string product)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Stock_Link";
        ///string sqlCommand = "p_Mobile_Putaway_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "product", DbType.String, product);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ValidStockLink

    #region ValidProductBatchLink
    public DataSet ValidProductBatchLink(string connectionStringName, string location, string product, string batch, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Batch_Link";
        ///string sqlCommand = "p_Mobile_Putaway_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ValidProductBatchLink

    #region AutoMove
    public int AutoMove(string connectionStringName, int operatorId, int storageUnitBatchId, string pickLocation, string storeLocation, decimal confirmedQuantity, string instructionTypeCode)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Auto_Movement";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add parameters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "@storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "pickLocation", DbType.String, pickLocation);
        db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
        db.AddInParameter(dbCommand, "instructionTypeCode", DbType.String, instructionTypeCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion AutoMove

    #region PalletLink
    public bool PalletLink(string connectionStringName, int operatorId, int storageUnitBatchId, string PalletId, string storeLocation, Decimal confirmedQuantity, string instructionTypeCode)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Pallet_Link";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add parameters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "PalletId", DbType.String, PalletId);
        db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
        db.AddInParameter(dbCommand, "instructionTypeCode", DbType.String, instructionTypeCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion PalletLink

    #region ReasonUpdate
    public bool ReasonUpdate(string connectionStringName, int instructionId, int reasonId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Reason_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "reasonId", DbType.Int32, reasonId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ReasonUpdate

    #region AdhocStockTake
    public int AdhocStockTake(string connectionStringName, int instructionId)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Location_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add parameters
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    public int AdhocStockTake(string connectionStringName, int instructionId, Decimal confirmedQuantity)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Location_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add parameters
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);

        //Return the correct id for the new record
        db.AddOutParameter(dbCommand, "JobId", DbType.Int32, result);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteScalar(dbCommand);
                result = (int)db.GetParameterValue(dbCommand, "@jobId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion AdhocStockTake

    #region AdhocReplenishment
    public int AdhocReplenishment(string connectionStringName, int instructionId)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Request_Replenishment";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add parameters
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch (Exception ex) { }

            connection.Close();

            return result;
        }
    }
    #endregion AdhocReplenishment

    #region FinishJob
    public int FinishJob(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Finish_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion FinishJob

    #region InstructionType
    public string InstructionType(string connectionStringName, int instructionId)
    {
        string result = "false";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_InstructionType";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = db.ExecuteScalar(dbCommand).ToString();
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InstructionType

    #region DisplayBatch
    public string DisplayBatch(string connectionStringName, int instructionId)
    {
        string result = "false";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Display_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = db.ExecuteScalar(dbCommand).ToString();
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DisplayBatch

    #region DisplayQty
    public string DisplayQty(string connectionStringName, int instructionId)
    {
        string result = "false";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Display_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = db.ExecuteScalar(dbCommand).ToString();
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DisplayQty

    #region StockTakeCheck
    public bool StockTakeCheck(string connectionStringName, int instructionId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Stock_Take_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion StockTakeCheck

    #region CheckSOH
    public bool CheckSOH(string connectionStringName, string location, string barcode, string batch, int warehouseId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_SOH";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CheckSOH

    #region GeneralErrorPick
    public int GeneralErrorPick(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_General_Error";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "pickError", DbType.Boolean, 1);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GeneralErrorPick

    #region GeneralErrorStore
    public int GeneralErrorStore(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_General_Error";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "storeError", DbType.Boolean, 1);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GeneralErrorStore

    public int GetStorageUnit(string connectionStringName, string barcode, int principalId, int warehouseId)
    {
        int result = -1;

        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_GetStorageUnitId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, principalId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    #region ScanStoreLocationByArea
    public bool ScanStoreLocationByArea(string connectionStringName, int receiptLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Scan_StoreLocation_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ScanStoreLocationByArea
	
	#region CheckLocationStockTake
    public int CheckLocationStockTake(string connectionStringName, int instructionId, string pickBarcode, int operatorId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Confirm_Location_No_Stock_Take";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "pickBarcode", DbType.String, pickBarcode);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CheckLocationStockTake

    #region GetSOH
    public int GetSOH(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Get_SOH";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }    
    }
    #endregion GetSOH

    #region ValidProductCorrectLocation
    public bool ValidProductCorrectLocation(string connectionStringName, string barcode, int locationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Pallet_Correct_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ValidProductCorrectLocation
    #region ValidLocationLink
    public bool ValidLocationLink(string connectionStringName, string barcode)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ValidLocationLink

    public int GetStorageUnitBatch(string connectionStringName, int storageunitid)
    {
        int result = -1;
        int storageUnitBatchId = -1;
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StorageUnitBatch_Search_StorageUnitId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageunitid);
        //db.AddInParameter(dbCommand, "principalId", DbType.Int32, principalId);
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddOutParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);
                result = (int)db.GetParameterValue(dbCommand, "@StorageUnitBatchId");
            }
            catch (Exception ex)
            {
            }

            connection.Close();

            return result;
        }
    }
}