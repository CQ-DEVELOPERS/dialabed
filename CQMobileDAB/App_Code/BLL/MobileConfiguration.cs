﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Summary description for MobileConfiguration
/// </summary>
public class MobileConfiguration
{
    public MobileConfiguration()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region Configuration
    /// <summary>
    /// 
    /// </summary>
    /// <returns>bool</returns>
    public bool GetSwitch(int configurationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("Connection String");

        string sqlCommand = "p_Configuration_GetSwitch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Configuration
}