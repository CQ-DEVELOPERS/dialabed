using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

/// <summary>
/// Summary description for Face
/// </summary>
public class Face
{
	public Face()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataSet GetFace(string connectionStringName, string FromDateTime, int OperatorId, int OperatorGroupId)
    {
        DataSet dataSet = new DataSet();
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Mobile_OperatorPickingPerformance";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "Date", DbType.DateTime, FromDateTime);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, OperatorId.ToString());
            db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId.ToString());


            dataSet = db.ExecuteDataSet(dbCommand);

           
        }
        catch (Exception ex)
        {
            string str = ex.Message.ToString();
        }
         return dataSet;
    
    }
}
