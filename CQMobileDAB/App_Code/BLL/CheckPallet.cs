using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// PutawayFull allows an operator to scan a pallet and put it away
/// </summary>
public class CheckPallet
{
    public CheckPallet()
    {
    }

    #region CheckPallet
    /// <summary>
    /// Check if a Pallet is correct against user input
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="jobId"></param>
    /// <param name="barcode"></param>
    /// <param name="storeLocation"></param>
    /// <param name="confirmedQuantity"></param>
    /// <returns></returns>
    public DataSet Verify(int type, int operatorId, int palletId, string barcode, string batch, Decimal quantity)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("Connection String");

        string sqlCommand = "p_Mobile_Check_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "type", DbType.Int32, type);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion CheckPallet

    #region AutoMovePallet
    /// <summary>
    /// Check if a Pallet is correct against user input
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="jobId"></param>
    /// <param name="barcode"></param>
    /// <param name="storeLocation"></param>
    /// <param name="confirmedQuantity"></param>
    /// <returns></returns>
    ///public DataSet AutoMovePallet(int type, int operatorId, int palletId, string barcode, string batch, int quantity)
    public DataSet AutoMovePallet(int type)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = new DataSet();
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_Mobile_Movment";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "orderId", DbType.Int32, type);


            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch (DbException dbex)
        {
            string xx = dbex.Message;

        }
        return dataSet;
    }
    #endregion CheckPallet

    #region GetCheckList
    public DataSet GetCheckList(string connectionStringName, int warehouseId, int operatorId, string referenceNumber)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Check_List_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetCheckList

    #region UpdateCheckList
    public bool UpdateCheckList(string connectionStringName, int operatorId, int jobId, int storageUnitBatchId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_List_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        dbCommand.CommandTimeout = 0;

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateCheckList

    #region Update Check List Quantity
    public bool UpdateCheckListQuantity(string connectionStringName, int warehouseId, int operatorId, int jobId, int storageUnitBatchId, decimal quantity, int storageUnitId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Product_Check_List_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        dbCommand.CommandTimeout = 0;

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion

    #region StatusRollup
    public bool StatusRollup(string connectionStringName, int operatorId, int jobId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_List_Rollup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        dbCommand.CommandTimeout = 0;

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion StatusRollup

    #region Check List Job Finish
    public bool CheckListJobFinish(string connectionStringName, int operatorId, int jobId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_CheckList_Product_Finished";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        dbCommand.CommandTimeout = 0;

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion

    #region CheckIssueInQA
    public bool CheckIssueInQA(string connectionStringName, int operatorId, int jobId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_QA_Lines";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        dbCommand.CommandTimeout = 0;

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion


    #region CheckOrderInQA
    public bool CheckOrderInQA(string connectionStringName, int operatorId, int jobId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Check_Order_QA_Lines";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion

    /* Random Job Check */

    #region VerifyRandomJob
    public int VerifyRandomJob(string connectionStringName, int operatorId, int warehouseId, string barcode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        int IsDetailedCheck = 0;
        string sqlCommand = "p_Mobile_Validate_Random_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);
        db.AddOutParameter(dbCommand, "IsDetailedCheck", DbType.Int32, IsDetailedCheck);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteScalar(dbCommand);
                IsDetailedCheck = (int)db.GetParameterValue(dbCommand, "@IsDetailedCheck");
            }
            catch (Exception ex) { }

            connection.Close();

            return IsDetailedCheck;
        }
    }
    #endregion CheckPallet

    #region CheckSkuQuantity

    public bool CheckFullQuantity(string connectionStringName, int warehouseId, int OperatorId, string barcode, Decimal ConfirmedQuantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Check_List_Update_Full";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "ConfirmedQuantity", DbType.Decimal, ConfirmedQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (bool)db.ExecuteScalar(dbCommand);
                connection.Close();
                return true;

            }
            catch { return false; }
        }
    }
    #endregion CheckSkuQuantity
}
