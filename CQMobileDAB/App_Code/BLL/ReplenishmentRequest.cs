using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// PutawayFull allows an operator to scan a pallet and put it away
/// </summary>
public class ReplenishmentRequest
{
    public ReplenishmentRequest()
    {
    }

    #region GetReplenishmentRequest
    /// <summary>
    /// Completes a mixed pallet job with many instruction linked
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="location"></param>
    /// <param name="barcode"></param>
    /// <returns></returns>
    public DataSet GetReplenishmentRequest(string connectionStringName, int operatorId, string location, string barcode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Replenishment_Request";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetReplenishmentRequest
}
