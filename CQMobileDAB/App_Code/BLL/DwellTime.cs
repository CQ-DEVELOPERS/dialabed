using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Summary description for MobileCommon
/// This is for Dwell time and other common classes needed
/// </summary>
public class DwellTime
{
    private bool result = false;

    #region Constructor

    public DwellTime()
	{
		//
		// TODO: Add constructor logic here
        // Result      ReasonId    Reason

		//
    }

    #endregion Constructor

    #region DwellTimeCheck
    /// <summary>       
    /// Send back the Dwell time boolean
    /// </summary>
    /// <param name="type"></param>
    /// <param name="reasonId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool DwellTimeCheck(string connectionStringName, int operatorId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Dwell_Time";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "Type", DbType.Int32, 2);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DwellTimeCheck

    #region DwellTimeReasons
    /// <summary>       
    /// Get the Dwell Time reasons
    /// </summary>
    /// <param name="type"></param>
    /// <param name="reasonId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public DataSet DwellTimeReasons(string connectionStringName)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Mobile_Dwell_Time";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "Type", DbType.Int32, 0);


            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch (Exception dbException)
        {

        }
        return dataSet;
    }
    #endregion DwellTimeReasons

    #region DwellTimeInsert
    /// <summary>       
    /// Insert a record for the Dwell Time
    /// </summary>
    /// <param name="type"></param>
    /// <param name="reasonId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public int DwellTimeInsert(string connectionStringName, int reasonId, int operatorId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Dwell_Time";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "Type", DbType.Int32, 1);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, reasonId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DwellTimeInsert
}
