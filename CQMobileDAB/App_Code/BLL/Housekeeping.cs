using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

/// <summary>
/// Summary description for Housekeeping
/// </summary>
public class Housekeeping
{
    #region PickfaceCheckSelect
    public DataSet PickfaceCheckSelect(string connectionStringName, int warehouseId, string location, int storageUnitBatchId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Pickface_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion PickfaceCheckSelect

    #region PickfaceCheckAccept
    public int PickfaceCheckAccept(string connectionStringName, int warehouseId, int operatorId, int storageUnitId, int storageUnitBatchId, int locationId, int quantity)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Pickface_Check_Accept";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "quantity", DbType.Double, quantity);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion PickfaceCheckAccept

    #region PickfaceCheckReject
    public int PickfaceCheckReject(string connectionStringName, int operatorId, int storageUnitId, int locationId)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Pickface_Check_Reject";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion PickfaceCheckReject

    #region PickfaceCheckNew
    public int PickfaceCheckNew(string connectionStringName, int operatorId, int storageUnitBatchId, int locationId)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Pickface_Check_New";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion PickfaceCheckNew
}