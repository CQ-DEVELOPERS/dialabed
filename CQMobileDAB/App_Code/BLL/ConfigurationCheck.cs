﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ConfigurationCheck
/// </summary>
public class ConfigurationCheck
{
    #region Constructor
    public ConfigurationCheck()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor

    public bool ItemCheck(string itemName)
    {

        string item = itemName;
        bool checkReturn = false;


        try
        {
            MobileConfiguration mbconfig = new MobileConfiguration();

            switch (item)
            {
                case "Product":

                    checkReturn = mbconfig.GetSwitch(8);

                    break;
                case "Batch":

                    checkReturn = mbconfig.GetSwitch(34);

                    break;
                default:
                    break;
            }

            mbconfig = null;
            item = null;
            itemName = null;
        }
        catch (Exception ex)
        {
            string exCheck = "false" + ex.Message.ToString();
        }

        return checkReturn;

    }

    #region PalletCheck

    public bool CheckPallet()
    {

        bool checkReturn = false;

        try
        {
            MobileConfiguration mbconfig = new MobileConfiguration();

            checkReturn = mbconfig.GetSwitch(28);

            mbconfig = null;

        }
        catch (Exception ex)
        {
            string exCheck = "false" + ex.Message.ToString();
        }

        return checkReturn;

    }
    #endregion PalletCheck

    #region PalletCheck

    public bool StockTakePFInsertAllow()
    {

        bool checkReturn = false;

        try
        {
            MobileConfiguration mbconfig = new MobileConfiguration();

            checkReturn = mbconfig.GetSwitch(35);

            mbconfig = null;

        }
        catch (Exception ex)
        {
            string exCheck = "false" + ex.Message.ToString();
        }

        return checkReturn;

    }
    #endregion PalletCheck

}