using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for DBErrorMessage
/// </summary>
public class DBErrorMessage
{
    public DBErrorMessage()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    private string msgReturn = "";
    
    public string ReturnErrorString(int result)
    {
        
        switch (result)
        { 
           
            case 900001:
                msgReturn = Resources.ResMessages.db_900001.ToString();
                break;

            case 900002:
                msgReturn = Resources.ResMessages.db_900002.ToString();
                break;

            case 900003:
                msgReturn = Resources.ResMessages.db_900003.ToString();
                break;

            case 900004:
                msgReturn = Resources.ResMessages.db_900004.ToString();
                break;

            case 900005:
                msgReturn = Resources.ResMessages.db_900005.ToString();
                break;

            case 900006:
                msgReturn = Resources.ResMessages.db_900006.ToString();
                break;

            case 900007:
                msgReturn = Resources.ResMessages.db_900007.ToString();
                break;

            case 900008:
                msgReturn = Resources.ResMessages.db_900008.ToString();
                break;

            case 900009:
                msgReturn = Resources.ResMessages.db_900009.ToString();
                break;


            default:
                break;

        
        
        }

        return msgReturn;
    }
}
