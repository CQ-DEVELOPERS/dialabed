using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class Security_Login : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Login1.Focus();

            if (Session["LogonErrorMessage"] != null)
            {
                LabelUserNotSetup.Visible = true;
                LabelUserNotSetup.Text = Session["LogonErrorMessage"].ToString();
            }
        }
    }
    #endregion PageLoad

    #region Login1_OnLoggedIn
    protected void Login1_OnLoggedIn(object sender, EventArgs e)
    {
        try
        {
            Session["UserName"] = Login1.UserName.ToString();

            LogonCredentials lc = new LogonCredentials();

            lc.GetOperatorsDatabase(Session["UserName"].ToString());

            Session["ServerName"] = lc.serverName;
            Session["DatabaseName"] = lc.databaseName;
            Session["ConnectionStringName"] = lc.databaseName;

            if (Session["ConnectionStringName"] == null)
                return;

            if (lc.GetLogonCredentials(Session["ConnectionStringName"].ToString(), Session["UserName"].ToString()))
            {
                Session["WarehouseId"] = lc.warehouseId;

                if ((int)Session["WarehouseId"] == -1)
                {
                    LabelUserNotSetup.Visible = true;
                    return;
                }
                Session["LogonErrorMessage"] = "";

                Session["OperatorGroupId"] = lc.operatorGroupId;
                Session["OperatorId"] = lc.operatorId;
                Session["CultureName"] = lc.cultureName;
                Session["Printer"] = lc.printer;
                Session["Port"] = lc.port;
                Session["IPAddress"] = lc.ipAddress;
                Session["OperatorGroupCode"] = lc.operatorGroupCode;

                if (lc.CheckConcurrentUsers(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], "Mobile"))
                {
                    lc.UserLoggedIn(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], "Mobile");
                }
                else
                {
                    Session["LogonErrorMessage"] = Resources.Default.MaximumConcurrentUsers;

                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
			else
            {
                Session["LogonErrorMessage"] = Resources.Default.ActiveLogin;

                FormsAuthentication.SignOut();

                FormsAuthentication.RedirectToLoginPage();
            }
        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;

            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
    #endregion Login1_OnLoggedIn
}
