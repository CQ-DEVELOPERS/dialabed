<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PickfaceCheck.aspx.cs" Inherits="Screens_PickfaceCheck" Title="Untitled Page" %>

<%@ Register Src="ConfirmProduct.ascx" TagName="ConfirmProduct" TagPrefix="uc2" %>
<%@ Register Src="ConfirmQuantity.ascx" TagName="ConfirmQuantity" TagPrefix="uc3" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="LocationId,StorageUnitId,StorageUnitBatchId" DataSourceID="ObjectDataSourceStock" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>" />
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default, ActualQuantity %>" />
                    <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" />
                    <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default, ReservedQuantity %>" />
                </Fields>        
      </asp:DetailsView>
                    <asp:ObjectDataSource ID="ObjectDataSourceStock" runat="server" TypeName="Housekeeping"
                SelectMethod="PickfaceCheckSelect">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:ControlParameter Name="location" Type="String" ControlID="TextBoxConfirmLocation" />
                    <asp:SessionParameter Name="storageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                
    
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="LabelConfirmLocation" runat="server" Text="<%$ Resources:Default, Location %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxConfirmLocation" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
            <asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:Button ID="LinkButtonAccept2" runat="server" TabIndex="1" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
            <asp:Button ID="LinkButtonReject2" runat="server" TabIndex="2" Text="<%$ Resources:Default, Reject %>" OnClick="LinkButtonReject_Click"></asp:Button>
            <br />



        </asp:View>
        <asp:View ID="View3" runat="server">
            <asp:Label ID="LabelConfirmProduct" runat="server" Text="Product"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxConfirmProduct" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonAccept3" runat="server" OnClick="LinkButtonAccept_Click" TabIndex="2" Text="<%$ Resources:Default, Accept %>" />
            <asp:Button ID="btnQuit0" runat="server" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>" TabIndex="3" Text="<%$ Resources:Default, Quit %>" />
        </asp:View>
        <asp:View ID="View4" runat="server">
             <asp:Label ID="Label11" runat="server" Text="Quantity"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="Button2" runat="server" OnClick="LinkButtonAccept_Click" TabIndex="2" Text="<%$ Resources:Default, Accept %>" />
            <asp:Button ID="Button3" runat="server" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>" TabIndex="3" Text="<%$ Resources:Default, Quit %>" />
        
        </asp:View>
        <asp:View ID="View5" runat="server">
            <asp:Button ID="NextProduct" runat="server" OnClick="NextProduct_Click" Text="Next Product" />
            <asp:Button ID="LinkButtonNext2" runat="server" OnClick="LinkButtonNext_Click" TabIndex="4" Text="<%$ Resources:Default, NextLocation %>" />
            <asp:Button ID="Button4" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>

            </asp:View>
    </asp:MultiView>
</asp:Content>

