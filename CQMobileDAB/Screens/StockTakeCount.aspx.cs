using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_StockTakeCount : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                if (Session["StorageUnitBatchId"] != null)
                    if (Session["StorageUnitBatchId"].ToString() == "-1")
                        Session["StorageUnitBatchId"] = null;

                if (Session["StorageUnitBatchId"] != null)
                {
                    if (Session["StorageUnitBatchId"].ToString() != "-1")
                        MultiView1.ActiveViewIndex = 2;
                }
                else
                {
                    if (Session["FromURL"] != null)
                    {
                        DetailsView1.DataBind();
                        Session["StorageUnitBatchId"] = DetailsView1.DataKey["StorageUnitBatchId"];

                        if (Session["StorageUnitBatchId"].ToString() != "-1")
                            MultiView1.ActiveViewIndex = 2;
                    }
                }

                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 632))
                    dvProduct.Visible = true;

                if (DetailsView1.Rows.Count > 0)
                    if (DetailsView1.DataKey["LocationId"].ToString() == "-1")
                        Response.Redirect("~/Screens/StockTakeJob.aspx");
            }
        }
        catch { }
    }
    #endregion Page_Load

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Page.SetFocus(TextBoxConfirmLocation);
                break;
            case 1:
                Page.SetFocus(TextBoxPallet);
                break;
            case 2:
                Page.SetFocus(TextBoxQuantity);
                break;
            case 5:
                Page.SetFocus(TextBoxBatch);
                break;
            case 6:
                Page.SetFocus(TextBoxWeight);
                break;
        };
    }

    #region lbAccept_Click
    protected void lbAccept_Click(object sender, EventArgs e)
    {
        try
        {
            int locationId = -1;

            if (DetailsView1.DataKey["LocationId"] != null)
                locationId = int.Parse(DetailsView1.DataKey["LocationId"].ToString());

            Transact tran = new Transact();
            StockTake st = new StockTake();

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    if (st.ValidLocation(Session["ConnectionStringName"].ToString(), TextBoxConfirmLocation.Text, locationId))
                    {
                        TextBoxConfirmLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.Successful;
                        MultiView1.ActiveViewIndex++;
                        Session["StockTakeLocationId"] = locationId;
                    }
                    else
                    {
                        TextBoxConfirmLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidLocation;
                    }
                    break;
                case 1:
                    Session["VariableWeight"] = false;

                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 632) &&
                        !string.IsNullOrEmpty(TextBoxPallet.Text) && dvProduct.DataKey["ProductCode"] != null)
                    {
                        //if (TextBoxPallet.Text != dvProduct.DataKey["ProductCode"].ToString())
                        //{
                        //    TextBoxPallet.Text = "";
                        //    Master.MsgText = Resources.ResMessages.InvalidProduct;
                        //    return;
                        //}
                    }

                    if (tran.ValidProduct(Session["ConnectionStringName"].ToString(), TextBoxPallet.Text, "STK"))
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        Session["Principal"] = int.Parse(rblPrincipal.SelectedValue.ToString());
                        int storageUnitId = tran.GetStorageUnit(Session["ConnectionStringName"].ToString(), TextBoxPallet.Text, (int)Session["Principal"], (int)Session["WarehouseId"]);

                        if (tran.VariableWeight(Session["ConnectionStringName"].ToString(), TextBoxPallet.Text))
                            Session["VariableWeight"] = true;

                        if (TextBoxPallet.Text.StartsWith("P:") || !Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 296)
                            || Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), -1, storageUnitId) == LotAttributeRule.None)
                        {
                            MultiView1.ActiveViewIndex++;
                            Session["Batch"] = "-1";
                        }
                        else
                        {
                            MultiView1.ActiveViewIndex = 5;
                        }

                        LabelBarcodeValue.Text = TextBoxPallet.Text;
                        LabelBarcodeShow.Visible = true;
                        LabelBarcodeValue.Visible = true;
                    }
                    else
                    {
                        TextBoxPallet.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidPalletId;
                        return;
                    }

                    break;
                case 2:
                    Decimal quantity = 0;

                    //if (MultiView1.ActiveViewIndex == 1)
                    if (!Decimal.TryParse(TextBoxQuantity.Text, out quantity))
                    {
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                        return;
                    }
                    else
                    {
                        LabelQuantityValue.Text = TextBoxQuantity.Text;
                        LabelQuantityShow.Visible = true;
                        LabelQuantityValue.Visible = true;

                        Master.MsgText = Resources.ResMessages.Successful;
                    }

                    if (Session["VariableWeight"] != null)
                        if ((bool)Session["VariableWeight"])
                        {
                            MultiView1.ActiveViewIndex = 6;
                            return;
                        }

                    DoCount();
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 632))
                        dvProduct.DataBind();
                    break;
                case 5:
                    if (tran.ValidBatch(Session["ConnectionStringName"].ToString(), TextBoxPallet.Text, TextBoxBatch.Text))
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        Session["Batch"] = TextBoxBatch.Text;
                        MultiView1.ActiveViewIndex = 2;

                        LabelBatchValue.Text = TextBoxBatch.Text;
                        LabelBatchShow.Visible = true;
                        LabelBatchValue.Visible = true;
                    }
                    else
                    {
                        TextBoxPallet.Text = "";
                        TextBoxBatch.Text = "";
                        MultiView1.ActiveViewIndex = 1;
                        Master.MsgText = Resources.ResMessages.InvalidBatch;
                        return;
                    }

                    break;
                case 6:
                    decimal weight = 0;

                    if (!decimal.TryParse(TextBoxWeight.Text, out weight))
                    {
                        LabelBatchValue.Text = TextBoxBatch.Text;

                        Master.MsgText = Resources.ResMessages.WeightReEnter;
                        return;
                    }
                    else
                    {
                        LabelWeightValue.Text = TextBoxWeight.Text;

                        LabelWeightShow.Visible = true;
                        LabelWeightValue.Visible = true;

                        Master.MsgText = Resources.ResMessages.Successful;
                    }

                    DoCount();
                    break;
            }
        }
        catch { }
    }
    #endregion lbAccept_Click

    protected void DoCount()
    {
        StockTake st = new StockTake();

        int locationId = -1;

        if (DetailsView1.DataKey["LocationId"] != null)
            locationId = int.Parse(DetailsView1.DataKey["LocationId"].ToString());

        Decimal quantity = 0;

        if (!Decimal.TryParse(TextBoxQuantity.Text, out quantity))
        {
            Master.MsgText = Resources.ResMessages.QuantityReEnter;
            return;
        }

        decimal weight = 0;

        if (Session["VariableWeight"] != null)
            if ((bool)Session["VariableWeight"])
                if (!decimal.TryParse(TextBoxWeight.Text, out weight))
                {
                    Master.MsgText = Resources.ResMessages.WeightReEnter;
                    return;
                }

        if (Session["StorageUnitBatchId"] == null)
            Session["StorageUnitBatchId"] = DetailsView1.DataKey["StorageUnitBatchId"];

        LabelScanPallet.Text = Resources.ResLabels.ScanNextPallet;

        if (Session["Batch"] == null)
            Session["Batch"] = -1;

        int result = st.CheckCount(Session["ConnectionStringName"].ToString(),
                        (int)Session["OperatorId"],
                        (int)Session["JobId"],
                        TextBoxPallet.Text,
                        Session["Batch"].ToString(),
                        locationId,
                        (int)Session["StorageUnitBatchId"],
                        quantity,
                        (int)Session["Principal"]);

        if (result == 0)
        {
            result = st.ConfirmCount(Session["ConnectionStringName"].ToString(),
                        (int)Session["OperatorId"],
                        (int)Session["JobId"],
                        TextBoxPallet.Text,
                        Session["Batch"].ToString(),
                        locationId,
                        (int)Session["StorageUnitBatchId"],
                        quantity,
                        weight,
                        (int)Session["Principal"]);

            Reset();
            MultiView1.ActiveViewIndex = 1;
            Master.MsgText = Resources.ResMessages.Successful;
            return;
        }
        else if (result == 1)
        {
            //Panel1.Visible = false;
            MultiView1.ActiveViewIndex = 3;
            Master.MsgText = "Product Not expected";
            return;
        }
        else if (result == 2)
        {
            //Panel1.Visible = false;
            MultiView1.ActiveViewIndex = 4;
            Master.MsgText = Resources.ResMessages.PickFaceProductChange;
            return;
        }
        else if (result == -1)
        {
            Reset();
            MultiView1.ActiveViewIndex = 4;
            Master.MsgText = Resources.ResMessages.InvalidProduct;
            return;
        }
    }

    #region LinkButtonNextLocation_Click
    protected void LinkButtonNextLocation_Click(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = Resources.ResLabels.ConfirmLocationCounted;
            MultiView1.ActiveViewIndex = 7;
        }
        catch { }
    }
    #endregion LinkButtonNextLocation_Click

    #region LinkButtonAcceptProduct_Click
    protected void LinkButtonAcceptProduct_Click(object sender, EventArgs e)
    {
        try
        {
            StockTake st = new StockTake();

            int locationId = int.Parse(DetailsView1.DataKey["LocationId"].ToString());

            Decimal quantity = 0;

            if (!Decimal.TryParse(TextBoxQuantity.Text, out quantity))
            {
                Master.MsgText = Resources.ResMessages.QuantityReEnter;
                return;
            }

            int result = st.ConfirmCount(Session["ConnectionStringName"].ToString(),
                                    (int)Session["OperatorId"],
                                    (int)Session["JobId"],
                                    TextBoxPallet.Text,
                                    Session["Batch"].ToString(),
                                    locationId,
                                    (int)Session["StorageUnitBatchId"],
                                    quantity,
                                    (int)Session["Principal"]);

            if (result == 0)
            {
                Reset();
                MultiView1.ActiveViewIndex++;
                Master.MsgText = Resources.ResMessages.Successful;
                //Panel1.Visible = true;
                return;
            }
            else
            {
                Reset();
                MultiView1.ActiveViewIndex++;
                Master.MsgText = Resources.ResMessages.Failed;
                //Panel1.Visible = false;
                return;
            }
        }
        catch { }
    }
    #endregion LinkButtonAcceptProduct_Click

    #region LinkButtonRejectProduct_Click
    protected void LinkButtonRejectProduct_Click(object sender, EventArgs e)
    {
        try
        {
            Reset();
            MultiView1.ActiveViewIndex++;
            Master.MsgText = Resources.ResMessages.Failed;
            //Panel1.Visible = true;
            return;
        }
        catch { }
    }
    #endregion LinkButtonRejectProduct_Click

    #region LinkButtonAcceptNext_Click
    protected void LinkButtonAcceptNext_Click(object sender, EventArgs e)
    {
        try
        {
            int locationId = -1;

            if (DetailsView1.DataKey["LocationId"] != null)
                locationId = (int)DetailsView1.DataKey["LocationId"];

            if (locationId == -1)
                if (Session["FromURL"] != null)
                    Response.Redirect(Session["FromURL"].ToString());
                else
                    Response.Redirect("~/Screens/StockTakeJob.aspx");

            StockTake st = new StockTake();

            if (st.Finished(Session["ConnectionStringName"].ToString(),
                            (int)Session["OperatorId"],
                            (int)Session["JobId"],
                            locationId) == 0)
            {
                if (Session["FromURL"] != null)
                    Response.Redirect(Session["FromURL"].ToString());

                Master.MsgText = Resources.ResMessages.Successful;
                Reset();
                DetailsView1.DataBind();

                LabelScanPallet.Text = Resources.ResLabels.ScanPallet;

                if (DetailsView1.Rows.Count > 0)
                    if (DetailsView1.DataKey["LocationId"].ToString() == "-1")
                        Response.Redirect("~/Screens/StockTakeJob.aspx");
            }
            else
            {
                Master.MsgText = Resources.ResMessages.Failed;
            }
        }
        catch { }
    }
    #endregion LinkButtonAcceptNext_Click

    #region LinkButtonRejectNext_Click
    protected void LinkButtonRejectNext_Click(object sender, EventArgs e)
    {
        try
        {
            Reset();
            MultiView1.ActiveViewIndex++;
            Master.MsgText = Resources.ResMessages.Successful;
            //Panel1.Visible = true;
            return;
        }
        catch { }
    }
    #endregion LinkButtonRejectNext_Click

    #region Reset
    protected void Reset()
    {
        try
        {
            LabelBarcodeShow.Visible = false;
            LabelBarcodeValue.Visible = false;
            LabelBatchShow.Visible = false;
            LabelBatchValue.Visible = false;
            LabelQuantityShow.Visible = false;
            LabelQuantityValue.Visible = false;
            LabelWeightShow.Visible = false;
            LabelWeightValue.Visible = false;

            Session["StorageUnitBatchId"] = -1;
            TextBoxPallet.Text = "";
            TextBoxBatch.Text = "";
            TextBoxQuantity.Text = "";
            TextBoxWeight.Text = "";
            MultiView1.ActiveViewIndex = 0;
        }
        catch { }
    }
    #endregion Reset

    #region LinkButtonSearch_Click
    protected void LinkButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Session["URL"] = "~/Screens/StockTakeCount.aspx";
            Response.Redirect("~/Screens/ProductSearch.aspx");
        }
        catch { }
    }
    #endregion LinkButtonSearch_Click

    protected void rblPrincipal_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["Principal"] = int.Parse(rblPrincipal.SelectedValue.ToString());
    }
}
