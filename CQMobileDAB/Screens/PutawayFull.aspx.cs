using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PutawayFull : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    //protected bool changeactiveindex(int configID)
    //{
    //    bool checkme = false;
    //    if (configID == 0)
    //    {
    //        checkme = true;
    //    }

    //    if (configID != 0)
    //    {

    //        checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], configID);

    //        string[,] active = Session["ActiveTabIndexValue"] as string[,];

    //        for (int i = 0; i < active.GetLength(0); i++)
    //        {
    //            if (active[i, 0] == configID.ToString())
    //            {
    //                if (!checkme)
    //                {
    //                    if ((i + 1) == active.GetLength(0))
    //                    {
    //                        Session["Type"] = active[0, 1].ToString();
    //                        Session["ConfiguarationId"] = active[0, 0].ToString(); ;
    //                    }
    //                    else
    //                    {
    //                        Session["Type"] = active[i + 1, 1].ToString();
    //                        Session["ConfiguarationId"] = active[i + 1, 0].ToString(); ;
    //                    }
    //                    break;
    //                }
    //                break;
    //            }
    //        }
    //    }
    //    return checkme;
    //}
    protected bool changeactiveindex(int configurationId)
    {
        bool checkme = false;

        if (configurationId == 0)
            checkme = true;

        if (configurationId != 0)
        {
            checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], configurationId);

            string[,] active = Session["ActiveTabIndexValue"] as string[,];

            for (int i = 0; i < active.GetLength(0); i++)
            {
                if (active[i, 0] == configurationId.ToString())
                {
                    checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));

                    if (!checkme)
                    {
                        Session["ConfiguarationId"] = active[i + 1, 0].ToString();
                        Session["Type"] = active[i + 1, 1].ToString();
                        Session["CurrentView"] = active[i + 1, 1].ToString();
                        Session["ActiveViewIndex"] = active[i + 1, 2].ToString();
                        Master.MsgText = active[i + 1, 3].ToString();

                        checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));
                        configurationId = int.Parse(Session["ConfiguarationId"].ToString());
                        break;
                    }
                    else
                    {
                        Session["ConfiguarationId"] = active[i, 0].ToString();
                        Session["Type"] = active[i, 1].ToString();
                        Session["CurrentView"] = active[i, 1].ToString();
                        Session["ActiveViewIndex"] = active[i, 2].ToString();
                        Master.MsgText = active[i, 3].ToString();

                        checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));
                    }
                    break;
                }
            }
        }
        return checkme;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (Session["Error"] != null)
            {
                if ((bool)Session["Error"])
                {
                    if (Session["Message"] != null)
                        Master.MsgText = Session["Message"].ToString();
                }
                else
                {
                    if (Session["Type"] != null)
                    {
                        Session["Error"] = null;
                         Master.MsgText = Session["Message"].ToString();
                         if (Session["CurrentView"] == Session["Type"])
                         {
                             //bool retval = false;
                             //while (!retval)
                             //{
                             //    retval = changeactiveindex(int.Parse(Session["ConfiguarationId"].ToString()));
                             //}
                             
                             switch (Session["Type"].ToString())
                             {
                                 case "GetFullPutaway":
                                     //Master.MsgText = Resources.ResMessages.ConfirmBatch;
                                     //DetailsViewInstruction.Visible = true;
                                     //Session["Type"] = "Product";
                                     //Session["ActiveViewIndex"] = 1;
                                     //Session["ConfiguarationId"] = 105;
                                     //break;
                                     DetailsViewInstruction.Visible = true;
                                     Session["ConfiguarationId"] = 104;
                                     break;
                                 case "Product":
                                     DetailsViewInstruction.Visible = true;
                                     Session["ConfiguarationId"] = 105;
                                     break;
                                 case "ConfirmBatch":
                                     DetailsViewInstruction.Visible = true;
                                     Session["ConfiguarationId"] = 108;
                                     break;
                                 case "StoreLocation":
                                     DVInstruction_Databind();
                                     Session["ConfiguarationId"] = 109;
                                     break;
                                 case "Alternate":
                                     DVInstruction_Databind();
                                     Session["ConfiguarationId"] = 106;
                                     break;
                                 case "LocationError":
                                     DVInstruction_Databind();
                                     //Master.MsgText = Resources.ResMessages.ConfirmAfterLocationError;
                                     //Session["Type"] = "StoreLocation";
                                     //Session["ActiveViewIndex"] = 2;
                                     Session["ConfiguarationId"] = 108;
                                     break;
                                 case "Quantity":
                                     //Transact tran = new Transact();

                                     //if (tran.Pick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0 && tran.Store(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
                                     //{
                                     //    ResetSessionValues();
                                     //    MultiViewConfirm.ActiveViewIndex = 0;
                                     //    Session["ActiveViewIndex"] = 0;
                                     //    Session["Message"] = Resources.ResMessages.Successful;
                                     //}
                                     //else
                                     //{
                                     //    Session["Message"] = Resources.ResMessages.Failed;
                                     //    Session["Error"] = true;
                                     //}
                                     Session["ConfiguarationId"] = 193;
                                     break;
                                 default:
                                     Session["Message"] = Resources.ResMessages.Failed;
                                     Session["Error"] = true;
                                     break;
                             }

                             bool retval = false;

                             while (!retval)
                             {
                                 retval = changeactiveindex(int.Parse(Session["ConfiguarationId"].ToString()));
                             }

                             if (Session["ConfiguarationId"].ToString() == "193")
                             {
                                 Transact tran = new Transact();

                                 if (tran.Pick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0 && tran.Store(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
                                 {
                                     ResetSessionValues();
                                     MultiViewConfirm.ActiveViewIndex = 0;
                                     Session["ActiveViewIndex"] = 0;
                                     Session["Message"] = Resources.ResMessages.Successful;
                                     DetailsViewInstruction.Visible = false;
                                 }
                                 else
                                 {
                                     Session["Message"] = Resources.ResMessages.Failed;
                                     Session["Error"] = true;
                                 }
                             }
                         }
                    }
                }
            }

            if (Session["ActiveViewIndex"] != null)
                MultiViewConfirm.ActiveViewIndex = int.Parse(Session["ActiveViewIndex"].ToString());
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                string[,] activeTabIndexValue = new string[,] {   { "0", "GetFullPutaway", "0", Resources.ResMessages.Successful },
                                                                { "104", "Product", "1", Resources.ResMessages.ConfirmBatch },
                                                                { "105", "ConfirmBatch", "2", Resources.ResMessages.ConfirmBatch },
                                                                { "108", "StoreLocation", "3", Resources.ResMessages.ConfirmStoreLocation},
                                                                { "109", "Alternate", "4", Resources.ResMessages.ConfirmAlternateStoreLocation },
                                                                { "106", "Quantity", "5", Resources.ResMessages.ConfirmQuantity },
                                                                { "193", "Finished", "0", Resources.ResMessages.Successful}};

                Session["ActiveTabIndexValue"] = activeTabIndexValue;

                //string[,] activetabindexval = new string[,] { { "0", "GetFullPutaway" }, { "105", "ConfirmBatch" }, { "108", "StoreLocation" }, { "109", "Alternate" }, { "106", "Quantity" } };
                //Session["ActiveTabIndexValue"] = activetabindexval;

                ResetSessionValues();
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/PutawayFull.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                ResetSessionValues();
            }

            if (Session["FromURL"] == null)
                Session["FromURL"] = "~/Screens/PickFull.aspx";

            if (Session["Type"] == null)
            {
                Session["Type"] = "GetFullPutaway";
                DetailsViewInstruction.Visible = false;
            }
            else
            {
                DVInstruction_Databind();
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void DVInstruction_Databind()
    {
        DetailsViewInstruction.DataBind();

        if (DetailsViewInstruction.Rows.Count > 0)
        {
            Session["ProductCode"] = DetailsViewInstruction.DataKey["ProductCode"].ToString();
            Session["StoreLocation"] = DetailsViewInstruction.DataKey["StoreLocation"].ToString();
            Session["PickLocation"] = DetailsViewInstruction.DataKey["PickLocation"].ToString();
            Session["Quantity"] = Decimal.Parse(DetailsViewInstruction.DataKey["Quantity"].ToString());
        }
    }

    protected void ResetSessionValues()
    {
        // Reset all the Session Values
        Session["Type"] = null;
        Session["ActiveViewIndex"] = null;
        Session["InstructionId"] = null;
        Session["ProductCode"] = null;
        Session["StoreLocation"] = null;
        Session["PickLocation"] = null;
        Session["Quantity"] = null;
        Session["Error"] = null;
        Session["ConfiguarationId"] = 0;
        Session["Message"] = null;
    }
}
