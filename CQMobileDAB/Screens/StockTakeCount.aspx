<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="StockTakeCount.aspx.cs" Inherits="Screens_StockTakeCount" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsView1" runat="server" DataSourceID="ObjectDataSource1" DataKeyNames="LocationId,StorageUnitBatchId" AutoGenerateRows="false" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
            <asp:BoundField DataField="CurrentLine" HeaderText="<%$ Resources:Default, CurrentLine %>" />
            <asp:BoundField DataField="TotalLines" HeaderText="<%$ Resources:Default, TotalLines %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="StockTake" SelectMethod="GetNextLocation">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <table>
        <tr>
            <td align="right">
                <asp:Label ID="LabelBarcodeShow" runat="server" Text="<%$ Resources:ResLabels, ProductCode %>" Visible="false"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="LabelBarcodeValue" runat="server" Text="" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="LabelBatchShow" runat="server" Text="<%$ Resources:ResLabels, Batch %>" Visible="false"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="LabelBatchValue" runat="server" Text="" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="LabelQuantityShow" runat="server" Text="<%$ Resources:ResLabels, Quantity %>" Visible="false"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="LabelQuantityValue" runat="server" Text="" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="LabelWeightShow" runat="server" Text="<%$ Resources:ResLabels, Weight %>" Visible="false"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="LabelWeightValue" runat="server" Text="" Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="LabelConfirmLocation" runat="server" Text="<%$ Resources:ResLabels, ScanLocation %>"></asp:Label>
            <asp:TextBox ID="TextBoxConfirmLocation" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="LinkButtonAccept1" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
            <asp:Button ID="btnQuit" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:DetailsView ID="dvProduct" runat="server" DataSourceID="odsNextProduct" DataKeyNames="ProductId,ProductCode" 
                AutoGenerateRows="false" Visible="false">
                <Fields>
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="Principal" HeaderText="<%$ Resources:Default, Principal %>" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="odsNextProduct" runat="server" TypeName="StockTake" SelectMethod="GetNextProduct">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="locationId" SessionField="StockTakeLocationId" Type="Int32" DefaultValue="-1" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
             <asp:RadioButtonList ID="rblPrincipal" runat="server" RepeatColumns="2" OnSelectedIndexChanged="rblPrincipal_SelectedIndexChanged">
                <asp:ListItem Selected="True" Text="<%$ Resources:Default, DAB %>" Value="5"></asp:ListItem>
                <asp:ListItem  Text="<%$ Resources:Default, TBS %>" Value="6"></asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:Label ID="LabelScanPallet" runat="server" Text="<%$ Resources:ResLabels, ScanPallet %>"></asp:Label>
            <asp:TextBox ID="TextBoxPallet" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="LinkButtonAccept2" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
            <asp:Button ID="LinkButtonNext2" runat="server" TabIndex="3" Text="<%$ Resources:Default, NextLocation %>" OnClick="LinkButtonNextLocation_Click"></asp:Button>
            <asp:Button ID="LinkButtonSearch2" runat="server" TabIndex="4" Text="<%$ Resources:Default, Search %>" OnClick="LinkButtonSearch_Click"></asp:Button>
            <asp:Button ID="Button1" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:ResLabels, Quantity %>"></asp:Label>
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="LinkButtonAccept3" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
            <asp:Button ID="Button2" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <asp:Button ID="LinkButtonAcceptProduct" runat="server" TabIndex="1" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAcceptProduct_Click"></asp:Button>
            <asp:Button ID="LinkButtonRejectProduct" runat="server" TabIndex="2" Text="<%$ Resources:Default, Reset %>" OnClick="LinkButtonRejectProduct_Click"></asp:Button>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <asp:Button ID="LinkButtonRejectProduct2" runat="server" TabIndex="1" Text="<%$ Resources:Default, Reset %>" OnClick="LinkButtonRejectProduct_Click"></asp:Button>
        </asp:View>
        <asp:View ID="View6" runat="server">
            <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:ResLabels, ScanBatch %>"></asp:Label>
            <asp:TextBox ID="TextBoxBatch" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="LinkButtonAccept4" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
            <asp:Button ID="Button3" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View7" runat="server">
            <asp:Label ID="LabelWeight" runat="server" Text="<%$ Resources:ResLabels, Weight %>"></asp:Label>
            <asp:TextBox ID="TextBoxWeight" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="LinkButtonAccept5" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
            <asp:Button ID="Button4" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View8" runat="server">
            <asp:Button ID="LinkButtonAcceptNext" runat="server" TabIndex="1" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAcceptNext_Click"></asp:Button>
            <asp:Button ID="LinkButtonRejectNext" runat="server" TabIndex="2" Text="<%$ Resources:Default, Reset %>" OnClick="LinkButtonRejectNext_Click"></asp:Button>
        </asp:View>
    </asp:MultiView>
<%--    <asp:Panel ID="Panel1" runat="server">
        <asp:Button ID="lbAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
        <asp:Button ID="LinkButtonNextLocation" runat="server" TabIndex="3" Text="<%$ Resources:Default, NextLocation %>" OnClick="LinkButtonNextLocation_Click"></asp:Button>
        <asp:Button ID="LinkButtonSearch" runat="server" TabIndex="4" Text="<%$ Resources:Default, Search %>" OnClick="LinkButtonSearch_Click"></asp:Button>
        <asp:Button ID="btnQuit" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
    </asp:Panel>--%>
</asp:Content>

