<%@ Page Language="C#"
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="GenericError.aspx.cs" 
    Inherits="Screens_GenericError" 
    Title="<%$ Resources:Default, GenericErrorTitle %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="center" width="100%">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="#3D4782"
                    Text="<%$ Resources:Reslabels, ExceptionPage %>" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td align="center" width="100%">
               <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Reslabels, EmptyLabel %>"></asp:Literal></td>
        </tr>
        <tr>
            <td align="center" width="100%">
                <asp:HyperLink ID="hlMenu" runat="server" Font-Bold="True" ForeColor="#3D4782"
                    NavigateUrl="MainMenu.aspx" TabIndex="3" Text="<%$ Resources:Default, Ok %>" ></asp:HyperLink></td>
        </tr>
    </table>
</asp:Content>

