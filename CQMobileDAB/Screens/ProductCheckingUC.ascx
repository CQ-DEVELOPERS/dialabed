﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductCheckingUC.ascx.cs" Inherits="Screens_ProductCheckingUC" %>


<%@ Register Src="SerialNumbers.ascx" TagName="SerialNumbers" TagPrefix="uc1" %>

<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
    <%--view 3 start--%>
<asp:View ID="View1" runat="server">
    <asp:DetailsView ID="DetailsViewProduct" runat="server" DataKeyNames="StorageUnitId" DataSourceID="ObjectDataSourceProduct" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="ProductCheck"
        SelectMethod="ConfirmProduct">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="Barcode" SessionField="ProductBarcode" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity2 %>"></asp:Label>
    <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="1"></asp:TextBox>
</asp:View>
<asp:View ID="View2" runat="server">
    <asp:DetailsView ID="DetailsViewQuantity" runat="server" DataKeyNames="StorageUnitBatchId" DataSourceID="ObjectDataSourceQuantity" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField Visible="false" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceQuantity" runat="server" TypeName="ProductCheck"
        SelectMethod="ConfirmQuantity">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" DefaultValue="-1" />
            <asp:ControlParameter Name="quantity" ControlID="TextBoxQuantity" Type="String" />
            <asp:Parameter Name="scanMode" Type="Boolean" DefaultValue="false" />
            <asp:SessionParameter Name="Barcode" SessionField="ProductBarcode" Type="String" />
            <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:View>
<asp:View ID="View3" runat="server">
    <br />
    <asp:Label ID="LabelOldReferenceNumber1" runat="server" Text="<%$ Resources:Default, OldNumber %>"></asp:Label>
    <asp:Label ID="LabelOldReferenceNumber2" runat="server" Text=""></asp:Label>
    <br />
    <asp:Label ID="LabelNewReferenceNumber" runat="server" Text="<%$ Resources:Default, NewNumber %>"></asp:Label>
    <asp:TextBox ID="TextBoxNewReferenceNumber" runat="server" TabIndex="1"></asp:TextBox>
</asp:View>
<asp:View ID="View4" runat="server">
    <br />
    <asp:Label ID="LabelContainerType" runat="server" Text="<%$ Resources:Default, ContainerType %>"></asp:Label><br />
    <asp:RadioButtonList ID="rblContainerType" runat="server" DataSourceId="ObjectDataSourceContainerType"
        DataTextField="Description" DataValueField="ContainerTypeId" RepeatLayout="Flow">
    </asp:RadioButtonList>
    <asp:ObjectDataSource ID="ObjectDataSourceContainerType" runat="server" TypeName="ProductCheck"
        SelectMethod="SearchContainerTypes">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:View>
</asp:MultiView>
<br />
<asp:Button ID="LinkButtonAccept" runat="server" TabIndex="9" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
<asp:Button ID="LinkButtonNewBox" runat="server" TabIndex="12" Text="<%$ Resources:Default, NewBox %>" OnClick="LinkButtonNewBox_Click" Visible="false"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="14" Text="<%$ Resources:Default, Quit %>" OnClick="btnQuit_Click"></asp:Button>
