using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_MixedJobCreate : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PrivateVariables
    private int result = 0;
    private string strresult = "";
    private string theErrMethod = "";
    #endregion PrivateVariables

    #region Page_Load 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            ClearAllForNewProduct();

            ////Call Dwell Time
            //int dwelltime = 0;
            //string constring = Session["ConnectionStringName"].ToString();
            //int operatorID = int.Parse(Session["OperatorId"].ToString());

            //Transact tran = new Transact();
            //dwelltime = tran.DwellTime(constring, 0, operatorID);

            //if (Session["DwellTime"] == null)
            //{
            //    if (dwelltime == 0) // This should be 1
            //    {
            //        Session["DwellTime"] = 0;
            //        Session["Url"] = "MixedJobCreate.aspx";
            //        Server.Transfer("CopyDwellTime.aspx?MixedJobCreate");

            //    }
            //}
            //else
            //{
            //    if (dwelltime == 1) // This should be 1
            //    {
            //        Session["DwellTime"] = 0;

            //    }
            //}
        }
    }
    #endregion Page_Load
    
    #region CommandCancel_Click 
    protected void CommandCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainMenu.aspx");
    }
    #endregion CommandCancel_Click

    #region CommandNext_Click
    protected void CommandNext_Click(object sender, EventArgs e)
    {
        MixedJobCreate_DataBind(0, 1);
    }
    #endregion CommandNext_Click

    #region CommandTryAgain_Click
    protected void CommandTryAgain_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    #endregion CommandTryAgain_Click

    #region MixedJobCreate_DataBind
    protected void MixedJobCreate_DataBind(int type, int operatorId)
    {
        string palletId = TextBoxPalletId.Text;
        int palletIdInt;
        string barcode = TextBoxBarcode.Text;
        string batch = TextBoxBatch.Text;
        string quantity = TextBoxQuantity.Text;
        Decimal quantityInt;

        if (palletId == "" || barcode == "" || batch == "" || quantity == "")
        {
            Master.MsgText = "Not all information was entered.";
            return;
        }
        bool isnumber;

        isnumber = int.TryParse(palletId, out palletIdInt);

        if(!isnumber)
        {
            Master.MsgText = "Pallet is not a valid number.";
            return;
        }

        isnumber = Decimal.TryParse(quantity, out quantityInt);

        if(!isnumber)
        {
            Master.MsgText = "Quantity is not a valid number.";
            return;
        }

        MixedJobCreate mixedJobCreate = new MixedJobCreate();
        DataSet ds = mixedJobCreate.Verify(type, operatorId, palletIdInt, barcode, batch, quantityInt);

        int result = int.Parse(ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Result"].Ordinal).ToString());
        
        if (result == 0)
        {
            string jobId = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["JobId"].Ordinal).ToString();

            Master.MsgText = "";

            Session["PalletId"] = int.Parse(palletId);
            //Response.Redirect("PutawayFull.aspx");
            
            TextBoxPalletId.Text = jobId;

            TextBoxBarcode.Text = "";
            TextBoxBatch.Text = "";
            TextBoxQuantity.Text = "";
        }
        else
        {
            MultiView1.ActiveViewIndex = 1;
            Master.MsgText = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Message"].Ordinal).ToString();
        }
    }
    #endregion MixedJobCreate_DataBind

    #region ClearAllForNewProduct
    private void ClearAllForNewProduct()
    {
        theErrMethod = "ClearAllForNewProduct";
        try
        {
            //Clear Session Variables
            Session["StoreLocation"] = "";
            Session["PickSecurityCode"] = "";
            Session["ConfirmedQuantity"] = "";
            Session["InstructionId"] = "";
            Session["Quantity"] = "";
            Session["Confirmed"] = "";
            Session["JobId"] = "";
            Session["PickLocation"] = "";
        }
        catch (Exception ex)
        {
            strresult = SendErrorNow("ClearAllForNewProduct" + "_" + ex.Message.ToString());
            Master.MsgText = strresult;
        }
    }
    #endregion ClearAllForNewProduct

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

//                strresult = cqexception.GenericMobileOutErrorHandling(5, "Screens_MixedJobCreate", theErrMethod, ex);

//                Master.MsgText = strresult;
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

//                strresult = cqexception.GenericMobileOutErrorHandling(3, "Screens_MixedJobCreate", theErrMethod, exMsg.Message.ToString());

//                Master.MsgText = strresult;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return strresult;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling 

}
