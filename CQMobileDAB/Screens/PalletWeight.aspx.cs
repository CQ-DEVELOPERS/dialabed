using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PalletWeight : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ReceiptLineId"] = null;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/PalletWeight.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            PalletWeight pw = new PalletWeight();
            decimal tareWeight = -1;
            decimal grossWeight = -1;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    DetailsViewDocument.DataBind();

                    if (DetailsViewDocument.Rows.Count > 0)
                    {
                        Session["JobId"] = int.Parse(DetailsViewDocument.DataKey["JobId"].ToString());
                        Session["PalletId"] = int.Parse(DetailsViewDocument.DataKey["PalletId"].ToString());
                    }

                    if (Session["JobId"] == null)
                        Session["JobId"] = -1;

                    if (Session["PalletId"] == null)
                        Session["PalletId"] = -1;

                    if ((int)Session["JobId"] != -1 || (int)Session["PalletId"] != -1)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
                case 1:
                    tareWeight = 0;
                    grossWeight = -1;

                    if (decimal.TryParse(TextBoxTareWeight.Text, out tareWeight))
                    {
                        if (pw.UpdateWeights(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["PalletId"], tareWeight, grossWeight))
                        {
                            MultiView1.ActiveViewIndex++;
                            Master.MsgText = Resources.ResMessages.Successful;
                            DetailsViewDocument.DataBind();
                        }
                        else
                        {
                            TextBoxTareWeight.Text = "";
                            Master.MsgText = Resources.ResMessages.InvalidTareWeight;
                        }
                    }
                    else
                    {
                        TextBoxTareWeight.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidTareWeight;
                    }
                    break;
                case 2:
                    tareWeight = -1;
                    grossWeight = 0;

                    if (decimal.TryParse(TextBoxGrossWeight.Text, out grossWeight))
                    {
                        if (pw.UpdateWeights(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["PalletId"], tareWeight, grossWeight))
                        {
                            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 345))
                            {
                                MultiView1.ActiveViewIndex++;
                                Master.MsgText = Resources.ResMessages.Successful;
                                DetailsViewDocument.DataBind();
                            }
                            else
                                Reset();
                        }
                        else
                        {
                            TextBoxGrossWeight.Text = "";
                            Master.MsgText = Resources.ResMessages.InvalidGrossWeight;
                        }
                    }
                    else
                    {
                        TextBoxGrossWeight.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidGrossWeight;
                    }
                    break;
                case 3:
                    Session["StorageUnitBatchId"] = 0;

                    Session["StorageUnitBatchId"] = pw.ConfirmProduct(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxPackaging.Text);

                    if ((int)Session["StorageUnitBatchId"] > 0)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewProduct.DataBind();
                    }
                    else
                    {
                        TextBoxPackaging.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidProduct;
                    }
                    break;
                case 4:
                    Decimal quantity = 0;

                    if (Decimal.TryParse(TextBoxQuantity.Text, out quantity))
                        if (pw.InsertPackaging(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["StorageUnitBatchId"], quantity, (int)Session["OperatorId"]))
                        {
                            MultiView1.ActiveViewIndex++;
                            Master.MsgText = Resources.ResMessages.Successful;
                            DetailsViewProduct.DataBind();
                            LinkButtonFinish.Visible = true;
                        }
                        else
                        {
                            TextBoxQuantity.Text = "";
                            Master.MsgText = Resources.ResMessages.InvalidQuantity;
                        }
                    break;
                case 5:
                    TextBoxPackaging.Text = "";
                    TextBoxQuantity.Text = "";

                    MultiView1.ActiveViewIndex = 3;
                    break;
            }
        }
        catch { }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
        //LinkButtonSkip.Visible = false;
        //LinkButtonRedelivery.Visible = false;

        //switch (MultiView1.ActiveViewIndex)
        //{
        //    case 0:
        //        LinkButtonRedelivery.Visible = true;
        //        break;
        //    case 2:
        //        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124))
        //            MultiView1.ActiveViewIndex++;
        //        else
        //        {
        //            TextBoxYear.Text = DateTime.Now.Year.ToString();
        //            TextBoxMonth.Text = DateTime.Now.Month.ToString();
        //            TextBoxDay.Text = DateTime.Now.Day.ToString();
        //            TextBoxHours.Text = DateTime.Now.Hour.ToString();
        //            TextBoxMinutes.Text = DateTime.Now.Minute.ToString();
        //        }
        //        break;
        //    case 4:
        //        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 151))
        //            MultiView1.ActiveViewIndex++;

        //        LinkButtonSkip.Visible = true;

        //        break;
        //    case 5:
        //        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 152))
        //            MultiView1.ActiveViewIndex++;

        //        LinkButtonSkip.Visible = true;

        //        break;
        //    case 6:
        //        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 174))
        //        {
        //            Reset();
        //        }
        //        LinkButtonSkip.Visible = true;
        //        break;
        //}
    }

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Response.Redirect("~/Screens/PalletWeight.aspx");
                break;
            //case 3:
            //    if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124))
            //        MultiView1.ActiveViewIndex = 1;
            //    else
            //        MultiView1.ActiveViewIndex--;
            //    break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    #region LinkButtonSkip_Click
    protected void LinkButtonSkip_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 5:

                Reset();
                break;

            default:
                MultiView1.ActiveViewIndex++;
                break;
        }
    }
    #endregion LinkButtonSkip_Click

    #region LinkButtonFinish_Click
    protected void LinkButtonFinish_Click(object sender, EventArgs e)
    {
        try
        {
            Reset();
        }
        catch { }
    }
    #endregion LinkButtonFinish_Click

    protected void Reset()
    {
        try
        {
            MultiView1.ActiveViewIndex = 0;
            Session["ReceiptLineIdPass"] = Session["ReceiptLineId"];
            Session["ReceiptLineId"] = null;
            Master.MsgText = Resources.ResMessages.Successful;
            DetailsViewProduct.DataBind();
            TextBoxBarcode.Text = "";
            TextBoxTareWeight.Text = "";
            TextBoxGrossWeight.Text = "";
            TextBoxQuantity.Text = "";
            TextBoxPackaging.Text = "";
            TextBoxStoreLocation.Text = "";
        }
        catch { }
    }

    protected void LinkButtonRedelivery_Click(object sender, EventArgs e)
    {
        try
        {
            Receiving rcv = new Receiving();
            int receiptId = 0;

            receiptId = rcv.Redelivery(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"]);

            if (receiptId > 0)
            {
                Session["ReceiptId"] = receiptId;
                Response.Redirect("~/Screens/ReceiveDocument.aspx");
            }
            else
            {
                TextBoxBarcode.Text = "";
                Master.MsgText = Resources.ResMessages.InvalidRedelivery;
            }
        }
        catch { }
    }
}
