<%@ Page Language="C#" 
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="Packaging.aspx.cs" 
    Inherits="Screens_Packaging" 
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:DetailsView ID="DetailsViewDocument" runat="server" DataKeyNames="ReceiptId" DataSourceID="ObjectDataSourceDocument" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="Receiving"
        SelectMethod="ReceivingDocumentSelect">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
         <asp:View ID="View7" runat="server">
            <asp:Label ID="lblPackagingAdd" runat="server" Text="<%$ Resources:Default, SelectPackaging %>"></asp:Label>
            <br />
            <asp:RadioButtonList ID="RadioButtonPackaging" runat="server" DataSourceID="ObjectDataSourcePackaging" DataValueField="StorageUnitBatchId" DataTextField="Product"
                      RepeatColumns="1"  RepeatDirection="Vertical"   RepeatLayout="Table">
            
            </asp:RadioButtonList>
            <br />
            
            
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>">
            </asp:Label>
            <asp:TextBox ID="TextboxQuantity" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonAdd" runat="server" TabIndex="6" Text="<%$ Resources:Default, Add %>" OnClick="LinkButtonAdd_Click"></asp:Button>
            <asp:Button ID="LinkButtonQuit" runat="server" TabIndex="6" Text="<%$ Resources:Default, Quit2 %>" OnClick="LinkButtonQuit_Click"></asp:Button>
        </asp:View>
        
        
   </asp:MultiView>
   <asp:ObjectDataSource ID="ObjectDataSourcePackaging" runat="server" TypeName="Receiving" SelectMethod="SearchPackageProducts">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
   </asp:ObjectDataSource>
</asp:Content>
        