<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="StockTakeArea.aspx.cs" Inherits="Screens_StockTakeArea" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:GridView ID="GridView1" TabIndex="1" runat="server" DataSourceID="ObjectDataSource1" DataKeyNames="AreaId" AutoGenerateColumns="false" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="OK" ShowSelectButton="true" />
            <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area %>" />
            <asp:BoundField DataField="Count" HeaderText="<%$ Resources:Default, Count %>" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="LabelNoRows" runat="server" Text="<%$ Resources:Default, NoMoreCounts %>"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:Button ID="btnQuit" runat="server" TabIndex="2" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="StockTake" SelectMethod="GetAreas">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

