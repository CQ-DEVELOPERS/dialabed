﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_CheckListByProduct : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/CheckListByProduct.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                if (Session["RandomJobBarcode"] != null)
                {
                    Session["JobReferenceNumber"] = Session["RandomJobBarcode"];
                    TextBoxReferenceNumber.Text = Session["RandomJobBarcode"].ToString();
                    ButtonAcceptRef_Click(new object(), new EventArgs());
                    //MultiView1.ActiveViewIndex = 1;
                }

                Session["FromURL"] = null;

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        Page.SetFocus(TextBoxReferenceNumber);
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["JobId"] = GridView1.SelectedDataKey["JobId"];
            Session["StorageUnitBatchId"] = GridView1.SelectedDataKey["StorageUnitBatchId"];
            Session["ConfirmedQuantity"] = GridView1.SelectedDataKey["ConfirmedQuantity"];
            Session["PreviousQuantity"] = null;

            ButtonFinished.Visible = true;
            ButtonAcceptQty.Visible = true;
            LabelQuantity.Visible = true;
            TextBoxQuantity.Visible = true;
            ButtonCancel.Visible = true;
            TextBoxBarcode.Enabled = false;
        }
        catch { }
    }

    #region GridView1_RowDataBound
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex != -1)
        {
            if (e.Row.Cells[2].Text != "0")
            {
                e.Row.Cells[0].Text = "Checked";
                e.Row.Cells[0].Enabled = false;
            }
        }
    }
    #endregion GridView1_RowDataBound

    #region ButtonAcceptRef_Click
    protected void ButtonAcceptRef_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck PC = new ProductCheck();
            DataSet ds = new DataSet();
            ds = PC.ConfirmPallet(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxReferenceNumber.Text);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                Session["JobId"] = int.Parse(ds.Tables[0].Rows[0]["JobId"].ToString());
                GridView1.DataBind();
                if (GridView1.Rows.Count > 0)
                {
                    TextBoxReferenceNumber.Enabled = false;
                    ButtonAcceptRef.Visible = false;
                    LabelBarcode.Visible = true;
                    TextBoxBarcode.Visible = true;
                    TextBoxBarcode.Enabled = true;
                    TextBoxBarcode.Focus();
                    Master.MsgText = Resources.ResMessages.Successful;
                }
                else
                {
                    TextBoxReferenceNumber.Enabled = false;
                    LabelBarcode.Visible = false;
                    TextBoxBarcode.Visible = false;
                    ButtonFinished.Visible = true;
                    ButtonAcceptRef.Visible = false;
                }
            }
            else
            {
                Master.MsgText = "There are no items to check";
                TextBoxReferenceNumber.Text = "";
            }
            
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    #endregion ButtonAcceptRef_Click

    #region ButtonFinished_Click
    protected void ButtonFinished_Click(object sender, EventArgs e)
    {
        try
        {
            CheckPallet cp = new CheckPallet();

            if (cp.CheckListJobFinish(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"]))
            {
                if (cp.CheckOrderInQA(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"]))
                {
                    Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;
                }
                else
                {
                    PrintDespatchLabel(int.Parse(Session["JobId"].ToString()));

                    if (Session["RandomJobBarcode"] != null)
                    {
                        Session["RandomJobBarcode"] = null;
                        Response.Redirect("~/Screens/RandomCheck.aspx");
                    }
                    Master.MsgText = Resources.ResMessages.Successful;
                }
            }
            else
                Master.MsgText = Resources.ResMessages.Failed;

            Reset();
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    #endregion ButtonFinished_Click

    private void PrintDespatchLabel(int jobId)
	{
        try
        {
            ArrayList checkedList = new ArrayList();
            checkedList.Add(jobId);

            Session["checkedList"] = checkedList;

            Session["PrintLabel"] = false;

            Session["LabelName"] = "Despatch By Route Label.lbl";

            Session["FromURL"] = "~/Reports/JobLabel.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                        Session["LabelName"].ToString(),
                                                        sessionPrinter,
                                                        sessionPort,
                                                        sesionIndicatorList,
                                                        sessionCheckedList,
                                                        sessionLocationList,
                                                        sessionConnectionString,
                                                        sessionTitle,
                                                        sessionBarcode,
                                                        sessionWarehouseId,
                                                        sessionProductList,
                                                        sessionTakeOnLabel,
                                                        sessionLabelCopies);
        }
        catch { }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }

    #region ButtonAcceptQty
    protected void ButtonAcceptQty_Click(object sender, EventArgs e)
    {
        try
        {
            Decimal quantity = 0;
            Decimal confirmedQuantity = 0;
            Decimal previousQuantity = 0;

            CheckPallet cp = new CheckPallet();

            if (!Decimal.TryParse(TextBoxQuantity.Text, out quantity))
            {
                TextBoxQuantity.Text = "";
                Master.MsgText = Resources.ResMessages.Failed;
                return;
            }

            if (!Decimal.TryParse(Session["ConfirmedQuantity"].ToString(), out confirmedQuantity))
            {
                Master.MsgText = Resources.ResMessages.QuantityMismatch;
                return;
            }

            if (quantity != confirmedQuantity)
            {
                if (Session["PreviousQuantity"] == null)
                {
                    Session["PreviousQuantity"] = quantity;
                    TextBoxQuantity.Text = "";
                    Master.MsgText = Resources.ResMessages.QuantityReEnter;
                    return;
                }
                else
                {
                    if (!Decimal.TryParse(Session["PreviousQuantity"].ToString(), out previousQuantity))
                    {
                        Master.MsgText = Resources.ResMessages.QuantityMismatch;
                        return;
                    }

                    //if (cp.StatusRollup(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"]))
                    //{
                    //    Reset();

                    //Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;
                    //    return;
                    //}
                }
            }

            if (cp.UpdateCheckList(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"], (int)Session["StorageUnitBatchId"], quantity))
            {
                //ButtonFinished.Visible = false;
                ButtonAcceptQty.Visible = false;
                LabelQuantity.Visible = false;
                TextBoxQuantity.Visible = false;

                if (quantity != confirmedQuantity)
                    Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;
                else
                    Master.MsgText = Resources.ResMessages.Successful;

                GridView1.SelectedIndex = -1;
                GridView1.DataBind();
                if (GridView1.Rows.Count > 0)
                {
                    TextBoxBarcode.Text = "";
                    TextBoxBarcode.Enabled = true;
                }
                else
                {
                    ButtonAcceptRef.Visible = false;
                    ButtonQuit.Visible = true;
                    ButtonFinished.Visible = true;
                    ButtonCancel.Visible = false;
                    TextBoxReferenceNumber.Enabled = false;
                    TextBoxBarcode.Text = "";
                    TextBoxBarcode.Enabled = true;
                    TextBoxBarcode.Visible = false;
                    LabelBarcode.Visible = false;
                }
            }
            else
                //Master.MsgText = Resources.ResMessages.Failed;
                Master.MsgText = (cp.UpdateCheckList(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"], (int)Session["StorageUnitBatchId"], quantity).ToString());

            Session["PreviousQuantity"] = null;

            TextBoxQuantity.Text = "";
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    #endregion ButtonAcceptQty

    #region Reset
    protected void Reset()
    {
        TextBoxReferenceNumber.Text = "";
        TextBoxQuantity.Text = "";
        GridView1.DataBind();
        TextBoxReferenceNumber.Enabled = true;
        ButtonAcceptRef.Visible = true;
        ButtonFinished.Visible = false;
        ButtonAcceptQty.Visible = false;
        LabelQuantity.Visible = false;
        TextBoxQuantity.Visible = false;
    }
    #endregion Reset
    protected void TextBoxReferenceNumber_TextChanged(object sender, EventArgs e)
    {
        Session["JobReferenceNumber"] = TextBoxReferenceNumber.Text;
    }
    protected void TextBoxBarcode_TextChanged(object sender, EventArgs e)
    {
        try
        {
            decimal productQuantity = 0;
            GridView1.AllowPaging = false;
            GridView1.DataBind(); 
            foreach (GridViewRow row in GridView1.Rows)
            {
                string productCode = (string)this.GridView1.DataKeys[row.RowIndex]["ProductCode"];
                
                if (productCode.Trim() == TextBoxBarcode.Text.Trim())
                {
                    productQuantity = decimal.Parse(this.GridView1.DataKeys[row.RowIndex]["ConfirmedQuantity"].ToString());
                }
            }
            ucProductChecking.GetProductDetails(TextBoxBarcode.Text, productQuantity);
            GridView1.AllowPaging = true;
            GridView1.DataBind();
        }
        catch
        {

        }
        
    }

    protected void ucProductChecking_ScanFinish(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        try
        {
            ProductCheck PC = new ProductCheck();
            DataSet ds = new DataSet();
            ds = PC.ConfirmPallet(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxReferenceNumber.Text);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataBind();
                if (GridView1.Rows.Count > 0)
                {
                    TextBoxReferenceNumber.Enabled = false;
                    ButtonAcceptRef.Visible = false;
                    LabelBarcode.Visible = true;
                    TextBoxBarcode.Visible = true;
                    TextBoxBarcode.Enabled = true;
                    TextBoxBarcode.Text = "";
                    TextBoxBarcode.Focus();
                    Master.MsgText = Resources.ResMessages.Successful;
                }
                else
                {
                    TextBoxReferenceNumber.Enabled = false;
                    LabelBarcode.Visible = false;
                    TextBoxBarcode.Visible = false;
                    ButtonFinished.Visible = true;
                    ButtonAcceptRef.Visible = false;
                }
            }
            else
            {
                Master.MsgText = "There are no items to check";
                TextBoxReferenceNumber.Text = "";
            }

        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }

    protected void ucProductChecking_ScanMessage(object sender, EventArgs e)
    {
        Screens_ProductCheckingUC pcuc = sender as Screens_ProductCheckingUC;
        Master.MsgText = pcuc.scanMessage;
        if (pcuc.scanMessage == "No records were found" || pcuc.scanMessage == "Incorrect Product")
        {
            MultiView1.ActiveViewIndex = 0;
            GridView1.DataBind();
            TextBoxBarcode.Text = "";
        }
        else
        {
            MultiView1.ActiveViewIndex = 1;
            TextBoxBarcode.Enabled = false;
        }
            
    }
    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        ButtonFinished.Visible = false;
        ButtonAcceptQty.Visible = false;
        LabelQuantity.Visible = false;
        TextBoxQuantity.Visible = false;
        ButtonCancel.Visible = false;
        TextBoxBarcode.Enabled = true;
    }
}