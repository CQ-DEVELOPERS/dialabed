using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ConfirmBatch : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbConfirmBatch.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            //lbResetBatch.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            if (Session["Type"].ToString() == "ConfirmBatch")
                TextBoxConfirmBatch.Focus();
        }

        if (Session["Type"] != null)
            if (Session["Type"] == "Product")
                if (Session["InstructionId"] != null)
                {
                    Transact tran = new Transact();

                    string batch = tran.DisplayBatch(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);

                    if (batch.ToLower() != "false")
                        TextBoxConfirmBatch.Text = batch;
                }
    }
    protected void lbConfirmBatch_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            int result = 0;

            result = tran.ConfirmBatch(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], TextBoxConfirmBatch.Text);

            switch (result)
            {
                case 0:
                    Session["Message"] = Resources.ResMessages.Successful;
                    Session["Error"] = false;
                    Session["CurrentView"] = "ConfirmBatch";
                    break;
                case 3:
                    Session["Message"] = Resources.ResMessages.InvalidBatchQA;
                    Session["Error"] = true;
                    break;
                case 4:
                    Session["Message"] = Resources.ResMessages.InvalidBatch;
                    Session["Error"] = true;
                    break;
            }

            TextBoxConfirmBatch.Text = "";
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
            Session["Error"] = true;
        }

        //if (Session["FromURL"] != null)
        //    Response.Redirect(Session["FromURL"].ToString());
    }
    protected void lbResetBatch_Click(object sender, EventArgs e)
    {
        TextBoxConfirmBatch.Text = "";
        TextBoxConfirmBatch.Focus();
    }
}
