<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="SkuCheck.aspx.cs" Inherits="Screens_SkuCheck" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="LabelConfirmLocation" runat="server" Text="<%$ Resources:Default, ScanBarcode %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
            <asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, CountSkuCode %>"></asp:Label>
            <br />
            <asp:Label ID="LabelSkuCode" runat="server"></asp:Label>
            <br />
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Default, EnterQuantity%>"></asp:Label>
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonBarcode" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonBarcode_Click"></asp:Button>
            <asp:Button ID="Button1" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
    </asp:MultiView>
</asp:Content>

