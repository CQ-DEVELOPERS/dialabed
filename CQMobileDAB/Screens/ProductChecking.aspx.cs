using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ProductChecking : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Theme
    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }
    #endregion Theme

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ReceiptLineId"] = null;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/ProductChecking.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Page.SetFocus(TextBoxReferenceNumber);
                break;
            case 1:
                Page.SetFocus(TextBoxBarcode);
                LinkButtonFinished.Visible = true;
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 309))
                {
                    LinkButtonChangeBox.Visible = true;
                }
                break;
            case 2:
                Page.SetFocus(TextBoxBatch);
                LinkButtonFinished.Visible = true;
                break;
            case 3:
                DetailsViewBatch_DataBind();
                Page.SetFocus(TextBoxQuantity);

                if (!SerialNumber.IsTrackedStorageUnitId(Session["ConnectionStringName"].ToString(), (int)Session["StorageUnitId"]))
                {
                    SerialNumbers1.Visible = false;
                    Session["SerialNumber"] = false;
                }
                else
                {
                    SerialNumbers1.Visible = true;
                    Session["KeyId"] = (int)Session["JobId"];
                    Session["KeyType"] = "PickJobId";
                    Session["KeyStorageUnitId"] = (int)Session["StorageUnitId"];
                }
                break;
            case 4:
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 398))
                  Page.SetFocus(TextBoxWeight);
                else
                  Page.SetFocus(LinkButtonNext);
                break;
            case 5:
                Page.SetFocus(TextBoxNewReferenceNumber);
                break;
            case 6:
                LinkButtonChangeBox.Visible = false;
                LinkButtonFinished.Visible = false;
                LinkButtonNewBox.Visible = false;

                Page.SetFocus(rblContainerType);
                break;
        }
    }

    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();
            Master.MsgText = Resources.ResMessages.Successful;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    DetailsViewJob.DataBind();

                    if (DetailsViewJob.Rows.Count > 1)
                    {
                        Session["JobId"] = int.Parse(DetailsViewJob.DataKey["JobId"].ToString());
                        
                        if (DetailsViewJob.DataKey["Finished"].ToString() == "1")
                            LinkButtonFinished.Visible = true;

                        LinkButtonNewBox.Visible = true;
                        LinkButtonBack.Visible = true;
                        
                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 309) && chk.GetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"])) == 0)
                        {
                            MultiView1.ActiveViewIndex = 6;
                        }
                        else
                        {
                            MultiView1.ActiveViewIndex++;
                        }
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidField;
                        TextBoxBarcode.Text = "";
                    }
                    break;
                case 1:
                    DetailsViewProduct.DataBind();

                    if (DetailsViewProduct.Rows.Count > 1)
                    {
                        Session["StorageUnitId"] = int.Parse(DetailsViewProduct.DataKey["StorageUnitId"].ToString());
                        MultiView1.ActiveViewIndex++;
                        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 34)
                            || Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 169)
                            || !Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 338)
                            || Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), -1, (int)Session["StorageUnitId"]) == LotAttributeRule.None)
                        {
                            TextBoxBatch.Text = "Default";
                            DetailsViewBatch.DataBind();

                            if (DetailsViewBatch.Rows.Count > 1)
                            {
                                Session["StorageUnitBatchId"] = int.Parse(DetailsViewBatch.DataKey["StorageUnitBatchId"].ToString());

                                if (cbScanMode.Checked)
                                {
                                    //TextBoxQuantity.Text = "1";
                                    // ** Start
                                    //KS May 2012 - Get default quantity

                                    ProductCheck pr = new ProductCheck();

                                    Decimal defqty = 0;

                                    defqty = pr.DefaultQuantity(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"],TextBoxBarcode.Text);

                                    TextBoxQuantity.Text = string.Concat(defqty);

                                    //int.Parse(defqty);
                                    // ** End

                                    DetailsViewQuantity.DataBind();

                                    if (DetailsViewQuantity.DataKey["StorageUnitBatchId"].ToString() == "-1")
                                    {
                                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                                    }
                                    TextBoxBarcode.Text = "";
                                    
                                    Next();
                                }
                                else
				{
				    Decimal quantity = decimal.Parse(TextBoxQuantity.Text);

				    ProductCheck pr = new ProductCheck();

				    pr.ConfirmQuantity(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["JobId"], (int)Session["StorageUnitBatchId"], quantity, false,TextBoxBarcode.Text);
                                    MultiView1.ActiveViewIndex++;
				}
                            }
                            else
                            {
                                Master.MsgText = Resources.ResMessages.InvalidBatch;
                                TextBoxBatch.Text = "";
                            }
                        }
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidProduct;
                        TextBoxBarcode.Text = "";
                    }
                    break;
                case 2:
                    DetailsViewBatch.DataBind();

                    if (DetailsViewBatch.Rows.Count > 1)
                    {
                        Session["StorageUnitBatchId"] = int.Parse(DetailsViewBatch.DataKey["StorageUnitBatchId"].ToString());
                        MultiView1.ActiveViewIndex++;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidBatch;
                        TextBoxBatch.Text = "";
                    }
                    break;
                case 3:
                    DetailsViewQuantity.DataBind();
                    
                    if (DetailsViewQuantity.DataKey["StorageUnitBatchId"].ToString() == "-1")
                    {
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                        TextBoxQuantity.Text = "";

                        if (Session["Mismatch"] == null)
                            Session["Mismatch"] = true;
                        else
                        {
                            if (chk.ConfirmFinished(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["OperatorId"]) == 0)
                            {
                                TextBoxReferenceNumber.Text = "";
                                Reset();
                                MultiView1.ActiveViewIndex = 0;
                            }
                            else
                            {
                                TextBoxReferenceNumber.Text = "";
                                Reset();
                                Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;
                                MultiView1.ActiveViewIndex = 0;
                            }
                        }
                    }
                    else
                    {
                        NextOrFinish();
                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 398))
                        {
                            LinkButtonNewBox.Visible = false;
                            LinkButtonFinished.Visible = false;
                        }
                        else
                        {
                            LinkButtonNext.Visible = false;
                            LinkButtonAccept.Visible = true;
                            MultiView1.ActiveViewIndex = 1;
                        }
                    }
                    break;
                case 4:
                    break;
                case 5:
                    int jobId = 0;

                    jobId = chk.NewBox(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["OperatorId"], TextBoxNewReferenceNumber.Text);

                    if(jobId > 0)
                    {
                        TextBoxReferenceNumber.Text = TextBoxNewReferenceNumber.Text;
                        TextBoxNewReferenceNumber.Text = "";
                        LabelOldReferenceNumber2.Text = "";
                        
                        MultiView1.ActiveViewIndex = 0;

                        DetailsViewJob.DataBind();

                        if (DetailsViewJob.Rows.Count > 1)
                        {
                            Session["JobId"] = int.Parse(DetailsViewJob.DataKey["JobId"].ToString());
                            LinkButtonBack.Visible = true;
                            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 309) && chk.GetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"])) == 0)
                            {
                                MultiView1.ActiveViewIndex = 6;
                            }
                            else
                            {
                                MultiView1.ActiveViewIndex++;
                            }
                        }
                        else
                        {
                            Master.MsgText = Resources.ResMessages.InvalidField;
                            TextBoxBarcode.Text = "";
                        }
                        break;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                        TextBoxNewReferenceNumber.Text = "";
                    }
                    break;
                case 6:
                    chk.SetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]), int.Parse(rblContainerType.SelectedValue));

                    if (DetailsViewJob.DataKey["Finished"].ToString() == "1")
                    {
                        LinkButtonFinished.Visible = true;
                    }

                    LinkButtonNewBox.Visible = true;

                    MultiView1.ActiveViewIndex = 1;
                    break;
            }
        }
        catch { }
    }

    #region DetailsViewBatch_PageIndexChanged
    protected void DetailsViewBatch_PageIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DetailsViewBatch.DataBind();

            Session["StorageUnitBatchId"] = int.Parse(DetailsViewBatch.DataKey["StorageUnitBatchId"].ToString());
            //DetailsViewStock1.PageIndex = e.NewPageIndex;
            //DetailsViewStock2.PageIndex = e.NewPageIndex;
            //DetailsViewStock_DataBind();
        }
        catch { }
    }
    #endregion DetailsViewBatch_PageIndexChanged

    #region DetailsViewBatch_DataBind
    protected void DetailsViewBatch_DataBind()
    {
        DetailsViewBatch.DataBind();

        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 318))
            DetailsViewBatch.Rows[5].Visible = false;
    }
    #endregion DetailsViewBatch_DataBind

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
    }

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                break;
            case 1:
                LinkButtonBack.Visible = false;
                MultiView1.ActiveViewIndex--;
                break;
            case 6:
                MultiView1.ActiveViewIndex = 1;
                break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    protected void LinkButtonNext_Click(object sender, EventArgs e)
    {
        Next();
    }

    protected void LinkButtonFinished_Click(object sender, EventArgs e)
    {
        Finish();
    }

    protected void LinkButtonNewBox_Click(object sender, EventArgs e)
    {
        ProductCheck chk = new ProductCheck();

        LinkButtonNext.Visible = false;
        LinkButtonFinished.Visible = false;
        LinkButtonBack.Visible = false;
        LinkButtonAccept.Visible = true;

        LabelOldReferenceNumber2.Text = TextBoxReferenceNumber.Text;

        MultiView1.ActiveViewIndex = 5;
    }

    protected void NextOrFinish()
    {
        Reset();
        MultiView1.ActiveViewIndex++;
        LinkButtonAccept.Visible = false;
        LinkButtonBack.Visible = false;
        LinkButtonNext.Visible = true;
        LinkButtonFinished.Visible = true;
    }

    protected void Next()
    {
        try
        {
            if (Session["SerialNumber"] != null)
            {
                Session["SerialNumber"] = null;
                LinkButtonAccept.Visible = true;
                LinkButtonBack.Visible = true;
                LinkButtonNext.Visible = false;
                LinkButtonFinished.Visible = false;

                DetailsViewJob.DataBind();

                MultiView1.ActiveViewIndex = 1;
            }
        }
        catch { }
    }

    protected void Finish()
    {
        try
        {
            ProductCheck chk = new ProductCheck();

            LinkButtonNext.Visible = false;
            LinkButtonFinished.Visible = false;
            LinkButtonBack.Visible = false;
            LinkButtonAccept.Visible = true;

            if (chk.ConfirmFinished(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["OperatorId"]) == 0)
            {
                TextBoxReferenceNumber.Text = "";
                Reset();
                MultiView1.ActiveViewIndex = 0;
            }
            else
            {
                TextBoxReferenceNumber.Text = "";
                Reset();
                Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;
                MultiView1.ActiveViewIndex = 0;
            }
        }
        catch { }
    }

    protected void Reset()
    {
        Master.MsgText = Resources.ResMessages.Successful;
        TextBoxBarcode.Text = "";
        TextBoxBatch.Text = "";
        TextBoxQuantity.Text = "";
        Session["Mismatch"] = null;
    }
    protected void LinkButtonChangeBox_Click(object sender, EventArgs e)
    {
        ProductCheck chk = new ProductCheck();

        int containerTypeId = chk.GetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]));
        if (containerTypeId != 0)
        {
            rblContainerType.SelectedValue = containerTypeId.ToString();
        }

        MultiView1.ActiveViewIndex = 6;
    }
    
    protected void LinkButtonWeighBox_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();

            if (chk.ConfirmWeight(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]), Convert.ToDecimal(TextBoxWeight.Text)))
            {
                Master.MsgText = Resources.ResMessages.Successful;
                LinkButtonNewBox.Visible = true;
                LinkButtonFinished.Visible = true;
                TextBoxWeight.Text = "";
            }
            else
            {
                Master.MsgText = Resources.ResMessages.InvalidEmptyWeight;
            }
        }
        catch { TextBoxWeight.Text = ""; }
    }
}
