<%@ Page Language="C#"
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="Replenishment.aspx.cs" 
    Inherits="Screens_Replenishment" 
    Title="<%$ Resources:Default, ReplenishmentTitle %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="Form1" runat="server">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="LabelLocation" runat="server" Font-Bold="True" Text="<%$ Resources:ResLabels, Location %>"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxLocation" runat="server" TabIndex="1"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelBarcode" runat="server" Visible="False" Font-Bold="True">Barcode:</asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxBarcode" runat="server" Visible="False" TabIndex="2"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="CommandNext" runat="server" TabIndex="3" OnClick="CommandNext_Click" Text="<%$ Resources:Default, Next %>"></asp:Button>
                        <asp:Button ID="btnQuit" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>