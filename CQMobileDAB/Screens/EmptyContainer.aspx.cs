using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_EmptyContainer : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ReceiptLineId"] = null;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/EmptyContainer.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            EmptyContainer pw = new EmptyContainer();
            decimal tareWeight = -1;
            decimal grossWeight = -1;
            decimal emptyWeight = -1;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    DetailsViewDocument.DataBind();

                    if(DetailsViewDocument.Rows.Count > 0)
                        Session["PalletId"] = int.Parse(DetailsViewDocument.DataKey["PalletId"].ToString());

                    if (Session["PalletId"] != null)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
                case 1:
                    tareWeight = 0;
                    grossWeight = -1;

                    if (decimal.TryParse(TextBoxTareWeight.Text, out tareWeight))
                    {
                        if (pw.UpdateWeights(Session["ConnectionStringName"].ToString(), (int)Session["PalletId"], tareWeight, grossWeight))
                        {
                            MultiView1.ActiveViewIndex++;
                            Master.MsgText = Resources.ResMessages.Successful;
                            DetailsViewDocument.DataBind();
                        }
                        else
                        {
                            TextBoxTareWeight.Text = "";
                            Master.MsgText = Resources.ResMessages.InvalidTareWeight;
                        }
                    }
                    else
                    {
                        TextBoxTareWeight.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidTareWeight;
                    }
                    break;
                case 2:
                    emptyWeight = 0;

                    if (decimal.TryParse(TextBoxEmptyWeight.Text, out emptyWeight))
                    {
                        if (pw.UpdateEmptyWeight(Session["ConnectionStringName"].ToString(), (int)Session["PalletId"], emptyWeight))
                        {
                            MultiView1.ActiveViewIndex++;
                            Master.MsgText = Resources.ResMessages.Successful;
                            DetailsViewDocument.DataBind();
                        }
                        else
                        {
                            TextBoxEmptyWeight.Text = "";
                            Master.MsgText = Resources.ResMessages.InvalidEmptyWeight;
                        }
                    }
                    else
                    {
                        TextBoxEmptyWeight.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidEmptyWeight;
                    }
                    break;
                case 3:
                    int reasonId = -1;

                    if (RadioButtonListReason.SelectedValue.ToString() == "")
                    {
                        Master.MsgText = Resources.ResMessages.InvalidField;
                        break;
                    }

                    reasonId = int.Parse(RadioButtonListReason.SelectedValue.ToString());

                    if (pw.UpdateEmptyReason(Session["ConnectionStringName"].ToString(), (int)Session["PalletId"], reasonId))
                    {
                        Reset();
                    }
                    else
                    {
                        TextBoxEmptyWeight.Text = "";
                        Master.MsgText = Resources.ResMessages.ReasonSelect;
                    }
                    break;
            }
        }
        catch { }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
    }

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Response.Redirect("~/Screens/EmptyContainer.aspx");
                break;
            //case 3:
            //    if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124))
            //        MultiView1.ActiveViewIndex = 1;
            //    else
            //        MultiView1.ActiveViewIndex--;
            //    break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    #region LinkButtonSkip_Click
    protected void LinkButtonSkip_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 5:

                Reset();
                break;

            default:
                MultiView1.ActiveViewIndex++;
                break;
        }
    }
    #endregion LinkButtonSkip_Click

    #region LinkButtonFinish_Click
    protected void LinkButtonFinish_Click(object sender, EventArgs e)
    {
        try
        {
            Reset();
        }
        catch { }
    }
    #endregion LinkButtonFinish_Click

    protected void Reset()
    {
        try
        {
            MultiView1.ActiveViewIndex = 0;
            Session["ReceiptLineIdPass"] = Session["ReceiptLineId"];
            Session["ReceiptLineId"] = null;
            Master.MsgText = Resources.ResMessages.Successful;
            TextBoxBarcode.Text = "";
            TextBoxTareWeight.Text = "";
            TextBoxEmptyWeight.Text = "";
            RadioButtonListReason.SelectedIndex = 0;
        }
        catch { }
    }

    protected void LinkButtonRedelivery_Click(object sender, EventArgs e)
    {
        try
        {
            Receiving rcv = new Receiving();
            int receiptId = 0;

            receiptId = rcv.Redelivery(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"]);

            if (receiptId > 0)
            {
                Session["ReceiptId"] = receiptId;
                Response.Redirect("~/Screens/ReceiveDocument.aspx");
            }
            else
            {
                TextBoxBarcode.Text = "";
                Master.MsgText = Resources.ResMessages.InvalidRedelivery;
            }
        }
        catch { }
    }
}
