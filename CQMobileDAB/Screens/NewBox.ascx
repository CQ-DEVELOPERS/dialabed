<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewBox.ascx.cs" Inherits="Screens_AlternatePickLocationPickface" %>
<asp:Label ID="LabelNewReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>"></asp:Label>
<asp:TextBox ID="TextBoxNewReferenceNumber" runat="server" TabIndex="11"></asp:TextBox>
<asp:Button ID="ButtonNewBox" runat="server" TabIndex="12" Text="<%$ Resources:Default, NewBox %>" OnClick="ButtonNewBox_Click"></asp:Button>
