<%@ Page Language="C#"
    MasterPageFile="~/Master.master"
    AutoEventWireup="true"
    CodeFile="MixedJobCreate.aspx.cs"
    Inherits="Screens_MixedJobCreate"
    Title="Mixed Job Create"
    StylesheetTheme="Default"
    Theme="Default"
%>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="Form1" runat="server">
        <asp:Label ID="LabelPalletId" Runat="server">PalletId:</asp:Label>
        <asp:TextBox ID="TextBoxPalletId" Runat="server" MaxLength="10"></asp:TextBox>
        <asp:Label ID="LabelBarcode" Runat="server" >Barcode:</asp:Label>
        <asp:TextBox ID="TextBoxBarcode" Runat="server"></asp:TextBox>
        <asp:Label ID="LabelBatch" Runat="server">Confirm Batch:</asp:Label>
        <asp:TextBox ID="TextBoxBatch" Runat="server"></asp:TextBox>
        <asp:Label ID="LabelQuantity" Runat="server">Quantity:</asp:Label>
        <asp:TextBox ID="TextBoxQuantity" Runat="server" MaxLength="10"></asp:TextBox>
        <asp:Button ID="CommandNext" Runat="server" OnClick="CommandNext_Click"  Text="<%$ Resources:Default, Next2 %>"></asp:Button>
        <asp:Button ID="CommandCancel" Runat="server" OnClick="CommandCancel_Click" Text="<%$ Resources:Default, Cancel2 %>" ></asp:Button>
    </asp:View>    
    <asp:View id="Form2" Runat="server">
        <asp:Label ID="LabelErrorMsg" Runat="server" ForeColor="#ff0000"></asp:Label>
        <asp:Button ID="CommandTryAgain" Runat="server" OnClick="CommandTryAgain_Click" Text="<%$ Resources:Default, TryAgain2 %>"></asp:Button>
     </asp:View>
    </asp:MultiView>
</asp:Content>