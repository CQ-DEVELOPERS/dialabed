using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_AlternatePickLocation : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                //lbPickLocation.Attributes.Add("onclick", "this.disabled= 'disabled' ");
                //lbAlternate.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            }

            if (Session["Type"].ToString() == "Alternate")
                Page.SetFocus(TextBoxConfirmPickLocation);

            if (Session["PickLocation"] != null)
                TextBoxConfirmPickLocation.Text = Session["PickLocation"].ToString();

            if (Session["InstructionId"] != null)
            {
                Transact tran = new Transact();

                string instructionTypeCode = tran.InstructionType(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);

                if (instructionTypeCode == "M")
                    ButtonError.Visible = true;
                else
                    ButtonError.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
    protected void lbPickLocation_Click(object sender, EventArgs e)
    {
        Session["Error"] = false;
        Session["CurrentView"] = "Alternate";
        //if (Session["FromURL"] != null)
        //    Response.Redirect(Session["FromURL"].ToString());
    }
    protected void lbAlternate_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();
            
            string instructionTypeCode = tran.InstructionType(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);

            if (instructionTypeCode == "PM" || instructionTypeCode == "FM" || instructionTypeCode == "PS")
            {
                if (tran.LocationErrorPick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
                {
                    int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), -1);
                    //int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]);

                    if (instructionId != -1)
                    {
                        Session["InstructionId"] = instructionId;
                        Session["Message"] = Resources.ResMessages.Successful;
                        Session["Error"] = false;
                        Session["Type"] = "PickLocation";
                        Session["ActiveViewIndex"] = 2;
                    }
                    else
                    {
                        Session["Message"] = Resources.ResMessages.ConfirmStoreLocation;
                        Session["Type"] = "StoreLocation";
                        Session["ActiveViewIndex"] = 7;
                        Session["ConfiguarationId"] = 100;
                    }

                    //Session["Type"] = "LocationError";
                    //Session["Error"] = false;
                    //Session["CurrentView"] = "LocationError";
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.InvalidLocation;
                    Session["Error"] = true;
                }
            }
            else
            {
                if (tran.LocationErrorPick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
                {
                    Session["Type"] = "LocationError";
                    Session["Error"] = false;
                    Session["CurrentView"] = "LocationError";
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.InvalidLocation;
                    Session["Error"] = true;
                }
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }

        //if (Session["FromURL"] != null)
        //    Response.Redirect(Session["FromURL"].ToString());
    }

    protected void LinkButtonPrevious_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ReferenceNumber"] != null)
            {
                Transact tran = new Transact();

                int instructionId = tran.GetPickingMixedPrevious(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), (int)Session["InstructionId"], false);

                if (instructionId != -1)
                {
                    Session["InstructionId"] = instructionId;
                    Session["Message"] = Resources.ResMessages.Successful;
                    Session["Error"] = false;
                    Session["Type"] = "GetMixedPickDetails";
                    Session["CurrentView"] = "GetMixedPickDetails";
                    Session["ActiveViewIndex"] = 1;
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.NoOtherLines;
                    Session["Error"] = true;
                }

                TextBoxConfirmPickLocation.Text = "";
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void LinkButtonNext_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ReferenceNumber"] != null)
            {
                Transact tran = new Transact();

                int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), (int)Session["InstructionId"]);
                //int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]);

                if (instructionId != -1)
                {
                    Session["InstructionId"] = instructionId;
                    Session["Message"] = Resources.ResMessages.Successful;
                    Session["Error"] = false;

                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 94))
                    {
                        Session["Type"] = "GetMixedPickDetails";
                        Session["CurrentView"] = "GetMixedPickDetails";
                        Session["ActiveViewIndex"] = 1;
                    }
                    else
                    {
                        Session["PickLocation"] = tran.GetPickLocation(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);
                        Session["Type"] = "PickLocation";
                        Session["CurrentView"] = "PickLocation";
                        Session["ActiveViewIndex"] = 2;
                    }
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.NoOtherLines;
                    Session["Error"] = true;
                }

                TextBoxConfirmPickLocation.Text = "";
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void ButtonError_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            if (tran.GeneralErrorPick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
            {
                Response.Redirect("~/Screens/MainMenu.aspx");
            }
            else
            {
                Response.Redirect("~/Screens/MainMenu.aspx");
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
