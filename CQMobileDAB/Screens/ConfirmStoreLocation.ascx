<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfirmStoreLocation.ascx.cs" Inherits="Screens_ConfirmStoreLocation" %>

<asp:Label ID="LabelConfirmStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
<br />
<asp:TextBox ID="TextBoxConfirmStoreLocation" runat="server" TabIndex="1"></asp:TextBox>
<br />
<asp:Button ID="lbStoreLocation" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbStoreLocation_Click"></asp:Button>
<asp:Button ID="lbManual" runat="server" TabIndex="3" Text="<%$ Resources:Default, Override %>" OnClick="lbManual_Click"></asp:Button>
<asp:Button ID="lbResetLoc" runat="server" TabIndex="4" Text="<%$ Resources:Default, Reset %>" OnClick="lbResetLoc_Click"></asp:Button>
<asp:Button ID="Button1" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
