<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PalletWeight.aspx.cs" Inherits="Screens_PalletWeight" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsViewDocument" runat="server" DataKeyNames="JobId,PalletId" DataSourceID="ObjectDataSourceDocument" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" />
            <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>" />
            <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" />
            <asp:BoundField DataField="TareWeight" HeaderText="<%$ Resources:Default, PalletTareWeight %>" />
            <asp:BoundField DataField="GrossWeight" HeaderText="<%$ Resources:Default, PalletGrossWeight %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="PalletWeight"
        SelectMethod="GetPackagingJob">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:ControlParameter Name="barcode" ControlID="TextBoxBarcode" DbType="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:Label ID="LabelTareWeight" runat="server" Text="<%$ Resources:Default, TareWeight2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxTareWeight" runat="server" TabIndex="2"></asp:TextBox>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <br />
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, GrossWeight %>"></asp:Label>
            <asp:TextBox ID="TextBoxGrossWeight" runat="server" TabIndex="3"></asp:TextBox>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <br />
            <asp:Label ID="LabelPackaging" runat="server" Text="<%$ Resources:Default, Packaging2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxPackaging" runat="server" TabIndex="4"></asp:TextBox>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <br />
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="5"></asp:TextBox>
        </asp:View>
        <asp:View ID="View6" runat="server">
            <asp:GridView ID="DetailsViewProduct" runat="server" DataSourceID="ObjectDataSourceProduct" AutoGenerateColumns="false" PageIndex="0" AllowPaging="true">
                <columns>
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="AcceptedQuantity" HeaderText="<%$ Resources:Default, Quantity %>" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                </columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="PalletWeight"
                SelectMethod="SearchPackaging">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="View7" runat="server">
            <br />
            <asp:Label ID="LabelStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxStoreLocation" runat="server" TabIndex="4"></asp:TextBox>
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="9" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="10" Text="<%$ Resources:Default, Back %>" OnClick="LinkButtonBack_Click"></asp:Button>
    <asp:Button ID="LinkButtonSkip" runat="server" TabIndex="11" Text="<%$ Resources:Default, Skip %>" OnClick="LinkButtonSkip_Click" Visible="false"></asp:Button>
    <asp:Button ID="LinkButtonFinish" runat="server" TabIndex="12" Text="<%$ Resources:Default, Finish %>" OnClick="LinkButtonFinish_Click" Visible="false"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="13" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

