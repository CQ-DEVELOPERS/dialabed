<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AutoMove.aspx.cs" Inherits="Screens_AutoMove" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:DetailsView ID="DetailsViewStock1" runat="server" DataKeyNames="StorageUnitBatchId" AutoGenerateRows="False" PageIndex="0" AllowPaging="true" OnPageIndexChanging="DetailsViewStock_PageIndexChanging" Visible="false">
    <Fields>
        <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
        <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
        <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
        <asp:BoundField DataField="Pickface" HeaderText="<%$ Resources:Default, Pickface %>" />
        <asp:BoundField DataField="PickFaceQuantity" HeaderText="Pick face Qty" />
        <asp:BoundField DataField="Principal" HeaderText="Principal" />
        <asp:BoundField DataField="BatchMatch" HeaderText="<%$ Resources:Default, BatchMatch %>" />
        <asp:TemplateField HeaderText="<%$ Resources:Default, StoreOrder %>" AccessibleHeaderText="StoreOrder">
            <ItemTemplate>
                <asp:TextBox ID="txtStoreArea" runat="server" Text='<%# Eval("StoreArea").ToString().Replace("<br />",Environment.NewLine) %>' TextMode="MultiLine" Rows="3"></asp:TextBox>
                <%--<asp:BoundField DataField="StoreArea" HeaderText="<%$ Resources:Default, StoreOrder %>" />--%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default, ActualQuantity %>" />
        <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" />
        <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default, ReservedQuantity %>" />
    </Fields>
</asp:DetailsView>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View0" runat="server">
        <asp:Label ID="LabelPickLocation" runat="server" Text="<%$ Resources:Default, PickLocation %>"></asp:Label>
        <asp:TextBox ID="TextBoxPickLocation" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View1" runat="server">
        <asp:Label ID="LabelPalletId" runat="server" Text="<%$ Resources:Default, ScanPallet %>"></asp:Label>
        <asp:TextBox ID="TextBoxPalletId" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View2" runat="server">
        <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, ScanBatch %>"></asp:Label>
        <asp:TextBox ID="TextBoxBatch" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View3" runat="server">
        <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
        <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View4" runat="server">
        <asp:Label ID="LabelNewQuantity1" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
        <asp:Label ID="LabelNewQuantity2" runat="server"></asp:Label>
        <br />
        <asp:Label ID="LabelStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
        <asp:TextBox ID="TextBoxStoreLocation" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View5" runat="server">
        <asp:DetailsView ID="DetailsViewInstruction" runat="server" DataSourceID="ObjectDataSourceInstruction" AutoGenerateRows="False">
            <Fields>
                <asp:BoundField DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>" />
                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
            </Fields>
        </asp:DetailsView>
        <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="Transact"
            SelectMethod="GetInstructionDetails">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                <asp:SessionParameter Name="instructionId" SessionField="InstructionId" Type="int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:View>
</asp:MultiView>
<br />
<asp:Button ID="LinkButtonAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
<asp:Button ID="btnBack" runat="server" TabIndex="3" Text="<%$ Resources:Default, Back %>" OnClick="btnBack_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

