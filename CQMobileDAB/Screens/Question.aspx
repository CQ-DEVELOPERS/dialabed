<%@ Page Language="C#" 
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="Question.aspx.cs" 
    Inherits="Screens_Question" 
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
         <asp:View ID="View1" runat="server">
            <asp:Label ID="QuestionText" runat="server" Text="<%$ Resources:Default, QuestionYn %>"></asp:Label>
            <asp:RadioButtonList ID="QuestionValueRadio" runat="server">
            <asp:ListItem Text="<%$ Resources:Default, Yes %>"></asp:ListItem> 
            <asp:ListItem Text="<%$ Resources:Default, No %>"></asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:Button ID="LinkButton1" runat="server" TabIndex="6" Text="<%$ Resources:Default, Next2 %>" OnClick="LinkButtonView1_Click"></asp:Button>
            
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:Label ID="QuestionTextBox" runat="server" Text="<%$ Resources:Default, QuestionBox %>"></asp:Label>
            <asp:TextBox ID="QuestionValueTextBox" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButton3" runat="server" TabIndex="6" Text="<%$ Resources:Default, Next2 %>" OnClick="LinkButtonView2_Click"></asp:Button>
            
            
        </asp:View>        
   </asp:MultiView>
</asp:Content>
        