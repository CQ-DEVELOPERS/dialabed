using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PutawayMixed : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region StyleSheetTheme
    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }
    #endregion StyleSheetTheme

    #region Page_PreInit
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }
    #endregion Page_PreInit

    #region changeactiveindex
    protected bool changeactiveindex(int configID)
    {
        bool checkme = false;
        if (configID == 0)
            checkme = true;
        
        if (configID != 0)
        {
            checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], configID);

            string[,] active = Session["ActiveTabIndexValue"] as string[,];

            for (int i = 0; i < active.GetLength(0); i++)
            {
                if (active[i, 0] == configID.ToString())
                {
                    if (!checkme)
                    {
                        if ((i + 1) == active.GetLength(0))
                        {
                            Session["Type"] = active[0, 1].ToString();
                            Session["ConfigurationId"] = active[0, 0].ToString(); ;
                        }
                        else
                        {
                            Session["Type"] = active[i + 1, 1].ToString();
                            Session["ConfigurationId"] = active[i + 1, 0].ToString(); ;
                        }
                        break;
                    }
                    break;
                }
            }
        }
        return checkme;
    }
    #endregion changeactiveindex

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (Session["Error"] != null)
            {
                if ((bool)Session["Error"])
                {
                    if (Session["Message"] != null)
                        Master.MsgText = Session["Message"].ToString();
                }
                else
                {
                    if (Session["Type"] != null)
                    {
                        Session["Error"] = null;
                        Master.MsgText = Session["Message"].ToString();
                        if (Session["CurrentView"] == Session["Type"])
                        {
                            bool retval = false;
                            while (!retval)
                            {
                                retval = changeactiveindex(int.Parse(Session["ConfigurationId"].ToString()));
                            }
                            switch (Session["Type"].ToString())
                            {
                                case "GetMixedPutaway":
                                    //DetailsViewInstruction.Visible = true; 
                                    //Master.MsgText = Resources.ResMessages.ConfirmProduct;
                                    //Session["Type"] = "Product";
                                    //Session["ActiveViewIndex"] = 1;
                                    //break;

                                    DetailsViewInstruction.Visible = false;
                                    Master.MsgText = Resources.ResMessages.ConfirmProduct;
                                    Session["Type"] = "Product";
                                    Session["ActiveViewIndex"] = 1;
                                    Session["ConfiguarationId"] = 0;
                                    break;
                                case "Product":
                                    //Master.MsgText = Resources.ResMessages.ConfirmBatch;
                                    //Session["Type"] = "ConfirmBatch";
                                    //Session["ActiveViewIndex"] = 2;
                                    //Session["ConfigurationId"] = 113;
                                    //break;
                                    DetailsViewInstruction.Visible = true;
                                    Master.MsgText = Resources.ResMessages.ConfirmStoreLocation;
                                    Session["Type"] = "StoreLocation";
                                    Session["ActiveViewIndex"] = 3;
                                    //Session["ConfigurationId"] = 116;
                                    break;
                                case "ConfirmBatch":
                                    Master.MsgText = Resources.ResMessages.ConfirmStoreLocation;
                                    Session["Type"] = "StoreLocation";
                                    Session["ActiveViewIndex"] = 3;
                                    Session["ConfigurationId"] = 116;
                                    break;
                                case "StoreLocation":
                                    Master.MsgText = Resources.ResMessages.ConfirmAlternateStoreLocation;
                                    Session["Type"] = "Alternate";
                                    Session["ActiveViewIndex"] = 4;
                                    Session["ConfigurationId"] = 117;
                                    break;
                                case "Alternate":
                                    Master.MsgText = Resources.ResMessages.ConfirmQuantity;
                                    Session["Type"] = "Quantity";
                                    Session["ActiveViewIndex"] = 5;
                                    Session["ConfigurationId"] = 114;
                                    break;
                                case "LocationError":
                                    DVInstruction_Databind();
                                    Master.MsgText = Resources.ResMessages.ConfirmAfterLocationError;
                                    Session["Type"] = "StoreLocation";
                                    Session["ActiveViewIndex"] = 3;
                                    Session["ConfigurationId"] = 116;
                                    break;
                                case "Quantity":
                                    Transact tran = new Transact();

                                    if (tran.Pick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0 && tran.Store(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
                                    {
                                        int instructionId = tran.GetPutawayMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), (int)Session["WarehouseId"]);

                                        if (instructionId != -1)
                                        {
                                            DetailsViewInstruction.Visible = false;
                                            ResetSessionValues();
                                            Session["InstructionId"] = instructionId;
                                            Session["ActiveViewIndex"] = 1;
                                            Session["Message"] = Resources.ResMessages.Successful;
                                            Session["Error"] = false;
                                            Session["Type"] = "Product";
                                            DetailsViewInstruction.DataBind();
                                        }
                                        else
                                        {
                                            ResetSessionValues();
                                            Session["ActiveViewIndex"] = 0;
                                            Session["Message"] = Resources.ResMessages.Successful;
                                        }
                                    }
                                    else
                                    {
                                        Session["Message"] = Resources.ResMessages.Failed;
                                        Session["Error"] = true;
                                    }

                                    break;
                                default:
                                    Session["Message"] = Resources.ResMessages.Failed;
                                    Session["Error"] = true;
                                    break;
                            }
                        }
                    }
                }
            }

            if (Session["ActiveViewIndex"] != null)
                MultiViewConfirm.ActiveViewIndex = (int)Session["ActiveViewIndex"];
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
    #endregion Page_LoadComplete

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                string[,] activetabindexval = new string[,] { { "0", "GetMixedPutaway" }, { "112", "Product" }, {"113","ConfirmBatch"}, { "116", "StoreLocation" }, { "117", "Alternate" }, { "114", "Quantity" } };
                Session["ActiveTabIndexValue"] = activetabindexval;
                ResetSessionValues();
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/PutawayMixed.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                ResetSessionValues();
            }

            if (Session["FromURL"] == null)
                Session["FromURL"] = "~/Screens/PickFull.aspx";

            if (Session["Type"] == null)
            {
                Session["Type"] = "GetMixedPutaway";
                DetailsViewInstruction.Visible = false;
            }
            else
            {
                DVInstruction_Databind();
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
    #endregion Page_Load

    #region DVInstruction_Databind
    protected void DVInstruction_Databind()
    {
        DetailsViewInstruction.DataBind();

        if (DetailsViewInstruction.Rows.Count > 0)
        {
            Session["ProductCode"] = DetailsViewInstruction.DataKey["ProductCode"].ToString();
            Session["StoreLocation"] = DetailsViewInstruction.DataKey["StoreLocation"].ToString();
            Session["PickLocation"] = DetailsViewInstruction.DataKey["PickLocation"].ToString();
            Session["Quantity"] = Decimal.Parse(DetailsViewInstruction.DataKey["Quantity"].ToString());
        }
    }
    #endregion DVInstruction_Databind

    #region lbComplete_Click
    protected void lbComplete_Click(object sender, EventArgs e)
    {
        try
        {
            Session["ActiveViewIndex"] = 6;
            Master.MsgText = Resources.ResMessages.ConfirmPutawayFinished;
            Session["Message"] = Resources.ResMessages.ConfirmPutawayFinished;
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
            Session["Error"] = true;
        }
    }
    #endregion lbComplete_Click

    #region LinkButtonAcceptNext_Click
    protected void LinkButtonAcceptNext_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            int Result = 0;

            Result = tran.FinishJob(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);

            if (Result == 0)
            {
                ResetSessionValues();
                Session["ActiveViewIndex"] = 0;
                Session["Message"] = Resources.ResMessages.Successful;

                Response.Redirect("~/Screens/PutawayMixed.aspx");
            }
            else
            {
                Session["Message"] = Resources.ResMessages.Failed;
                Session["Error"] = true;
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
            Session["Error"] = true;
        }
    }
    #endregion LinkButtonAcceptNext_Click

    #region LinkButtonRejectNext_Click
    protected void LinkButtonRejectNext_Click(object sender, EventArgs e)
    {
        try
        {
            Session["ActiveViewIndex"] = 1;
            Master.MsgText = Resources.ResMessages.Successful;
            Session["Message"] = Resources.ResMessages.Successful;
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
            Session["Error"] = true;
        }
    }
    #endregion LinkButtonRejectNext_Click

    #region ResetSessionValues
    protected void ResetSessionValues()
    {
        // Reset all the Session Values
        Session["Type"] = null;
        Session["ActiveViewIndex"] = null;
        Session["InstructionId"] = null;
        Session["ProductCode"] = null;
        Session["StoreLocation"] = null;
        Session["PickLocation"] = null;
        Session["Quantity"] = null;
        Session["Error"] = null;
        Session["ConfigurationId"] = 0;
        Session["Message"] = null;
    }
    #endregion ResetSessionValues
}
