using System;
using System.Data;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

public partial class Screens_PickfaceCheck : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");



                if (Session["StorageUnitBatchId"] != null || Session["Location"] != null || Session["URL"] != null)
                {
                    if (Session["StorageUnitBatchId"].ToString() != "-1")
                        MultiView1.ActiveViewIndex = 1;

                    if (Session["Location"] != null)
                        TextBoxConfirmLocation.Text = Session["Location"].ToString();

                    Session["URL"] = null;
                    Session["Location"] = null;
                    Session["Type"] = "PickLocation";

                    DetailsView1.DataBind();
                    DetailsView1.Visible = true;
                }
                else
                {
                    Session["StorageUnitBatchId"] = "-1";
                    Session["Type"] = "Product";
                    DetailsView1.DataBind();
                    DetailsView1.Visible = true;
                    MultiView1.ActiveViewIndex = 0;
                }
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region LinkButtonNext_Click
    protected void LinkButtonNext_Click(object sender, EventArgs e)
    {
        try
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    Response.Redirect("..");
                    break;
                case 4:
                    Reset();
                    Master.MsgText = Resources.Default.LocationScan;
                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonNext_Click

    #region LinkButtonAccept_Click
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            int locationId = -1;
            int storageUnitId = -1;


            if (DetailsView1.DataKey["LocationId"] != null)
                locationId = int.Parse(DetailsView1.DataKey["LocationId"].ToString());

            /* if (DetailsView1.DataKey["StorageUnitId"] != null)
             {
                 if(!string.IsNullOrEmpty(DetailsView1.DataKey["StorageUnitId"].ToString()))
                     storageUnitId = int.Parse(DetailsView1.DataKey["StorageUnitId"].ToString());
             }*/

            Housekeeping hk = new Housekeeping();
            Transact tr = new Transact();

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    if (tr.ValidLocationLink(Session["ConnectionStringName"].ToString(), TextBoxConfirmLocation.Text))
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsView1.DataBind();
                        DetailsView1.Visible = true;
                        MultiView1.ActiveViewIndex = 2;
                        Session["InstructionId"] = -1;
                    }
                    else
                    {
                        TextBoxConfirmLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidLocation;
                    }
                    break;
                case 1:
                    if (hk.PickfaceCheckAccept(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], /*storageUnitId*/ (int)Session["StorageUnitId"], (int)Session["StorageUnitBatchId"], locationId, int.Parse(TextBoxQuantity.Text)) == 0)
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        Session["StorageUnitBatchId"] = -1;
                        DetailsView1.Visible = true;
                        DetailsView1.DataBind();
                        MultiView1.ActiveViewIndex = 4;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.LocationError;
                    }
                    break;
                case 2:
                    if (tr.ValidProduct(Session["ConnectionStringName"].ToString(), TextBoxConfirmProduct.Text))
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        Session["StorageUnitId"] = tr.GetStorageUnit(Session["ConnectionStringName"].ToString(), TextBoxConfirmProduct.Text, 0, (int)Session["WarehouseId"]);
                        Session["StorageUnitBatchId"] = tr.GetStorageUnitBatch(Session["ConnectionStringName"].ToString(), (int)Session["StorageUnitId"]);
                        DetailsView1.Visible = true;
                        DetailsView1.DataBind();
                        MultiView1.ActiveViewIndex = 3;
                    }
                    else
                    {
                        TextBoxConfirmProduct.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidProduct;
                    }
                    break;
                case 3:
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsView1.DataBind();
                    DetailsView1.Visible = true;
                    MultiView1.ActiveViewIndex = 1;
                    Session["InstructionId"] = -1;
                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonAccept_Click

    #region LinkButtonReject_Click
    protected void LinkButtonReject_Click(object sender, EventArgs e)
    {
        try
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    Response.Redirect("..");
                    break;
                case 1:
                    int locationId = -1;
                    int storageUnitId = -1;

                    if (DetailsView1.DataKey["LocationId"] != null)
                        locationId = int.Parse(DetailsView1.DataKey["LocationId"].ToString());

                    if (DetailsView1.DataKey["StorageUnitId"] != null)
                        storageUnitId = int.Parse(DetailsView1.DataKey["StorageUnitId"].ToString());

                    Housekeeping hk = new Housekeeping();
                    Transact tr = new Transact();

                    if (hk.PickfaceCheckReject(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], storageUnitId, locationId) == 0)
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        Session["StorageUnitBatchId"] = -1;
                        DetailsView1.Visible = true;
                        DetailsView1.DataBind();
                        MultiView1.ActiveViewIndex = 4;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.LocationError;
                    }

                    //Master.MsgText = Resources.Default.LocationScan;
                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonReject_Click

    #region LinkButtonSearch_Click
    protected void LinkButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Session["Location"] = TextBoxConfirmLocation.Text;
            Session["URL"] = "~/Screens/PickfaceCheck.aspx";
            Response.Redirect("~/Screens/ProductSearch.aspx");
        }
        catch { }
    }
    #endregion LinkButtonSearch_Click

    #region Reset
    protected void Reset()
    {
        try
        {
            TextBoxConfirmLocation.Text = "";
            TextBoxConfirmProduct.Text = "";
            TextBoxQuantity.Text = "";
            MultiView1.ActiveViewIndex = 0;
        }
        catch { }
    }
    #endregion Reset

    protected void NextProduct_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Response.Redirect("..");
                break;
            case 4:
                TextBoxConfirmProduct.Text = "";
                TextBoxQuantity.Text = "";
                MultiView1.ActiveViewIndex = 2;
                //Master.MsgText = Resources.Default.LocationScan;
                break;
        }
    }
    #region ValidLocationLink
    public bool ValidLocationLink(string connectionStringName, string barcode)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Check_Valid_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ValidLocationLink

    public int GetStorageUnitBatch(string connectionStringName, int storageunitid)
    {
        int result = -1;
        int storageUnitBatchId = -1;
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StorageUnitBatch_Search_StorageUnitId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageunitid);
        //db.AddInParameter(dbCommand, "principalId", DbType.Int32, principalId);
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddOutParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);
                result = (int)db.GetParameterValue(dbCommand, "@StorageUnitBatchId");
            }
            catch (Exception ex)
            {
            }

            connection.Close();

            return result;
        }
    }
}

