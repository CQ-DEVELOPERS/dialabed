<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ReceiveByProduct.aspx.cs"
    Inherits="Screens_ReceiveByProduct" Title="Untitled Page" %>
<%@ Register Src="SerialNumbers.ascx" TagName="SerialNumbers" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Master.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" language="javascript">
      $(function () {
          $("#<%=TextBoxYear.ClientID %>").keyup(function () {
                 if ($(this).val().length == 4) {
                     $("#<%=TextBoxMonth.ClientID %>").focus();
                 }
             });
             $("#<%=TextBoxMonth.ClientID %>").keyup(function () {
                 if ($(this).val().length == 2) {
                     $("#<%=TextBoxDay.ClientID %>").focus();
                 }
             });
         });
    </script>
    <asp:DetailsView ID="DetailsViewDocument" runat="server" DataKeyNames="ReceiptId"
        DataSourceID="ObjectDataSourceDocument" AutoGenerateRows="False" PageIndex="0"
        AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="Receiving"
        SelectMethod="ReceivingDocumentSelect">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:DetailsView ID="DetailsViewProduct" runat="server" DataKeyNames="ReceiptLineId,Batch"
        DataSourceID="ObjectDataSourceProduct" AutoGenerateRows="False" PageIndex="0"
        AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
            <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>" />
            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
            <asp:BoundField DataField="ExpiryDate" HeaderText="<%$ Resources:Default, ExpiryDate %>" />
            <asp:BoundField DataField="ReceivedQuantity" HeaderText="<%$ Resources:Default, Quantity %>" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Receiving"
        SelectMethod="ReceivingLineSelect">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="receiptLineId" SessionField="ReceiptLineId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View0" runat="server">
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Product2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
            <asp:Button ID="ButtonPackaging" runat="server" TabIndex="2" Text="<%$ Resources:Default, Packaging %>" OnClick="ButtonPackaging_Click"></asp:Button>
        </asp:View>
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelLength" runat="server" Text="<%$ Resources:Default, Length %>"></asp:Label>
            <asp:TextBox ID="TextBoxLength" runat="server" TabIndex="2" Width="40" ReadOnly="False"></asp:TextBox>
            <asp:Label ID="LabelWidth" runat="server" Text="<%$ Resources:Default, Width %>"></asp:Label>
            <asp:TextBox ID="TextBoxWidth" runat="server" TabIndex="3" Width="40" ReadOnly="False"></asp:TextBox>
            <asp:Label ID="LabelHeight" runat="server" Text="<%$ Resources:Default, Height %>"></asp:Label>
            <asp:TextBox ID="TextBoxHeight" runat="server" TabIndex="4" Width="40" ReadOnly="False"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="LabelWeight" runat="server" Text="<%$ Resources:Default, Weight %>"></asp:Label>
            <asp:TextBox ID="TextBoxWeight" runat="server" TabIndex="4" Width="40" ReadOnly="False"></asp:TextBox>
            <asp:Label ID="LabelPackQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
            <asp:TextBox ID="TextBoxPackQuantity" runat="server" TabIndex="4" Width="40" ReadOnly="False"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="LabelPalletQuantity" runat="server" Text="<%$ Resources:Default, PalletQuantity %>"></asp:Label>
            <asp:TextBox ID="TextBoxPalletQuantity" runat="server" TabIndex="4" Width="40"></asp:TextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch %>"></asp:Label>
            <asp:TextBox ID="TextBoxBatch" runat="server" TabIndex="2"></asp:TextBox>
            <asp:TextBox ID="TextBox1" runat="server" TabIndex="3" Visible="False"></asp:TextBox>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <br />
            <asp:Label ID="LabelExpiryDate" runat="server" Text="<%$ Resources:Default, ExpiryDate %>"></asp:Label>
            <asp:TextBox ID="TextBoxExpiryDate" runat="server" TabIndex="3" Visible="False"></asp:TextBox>
            <br />
            <br />
            <table>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxYear" runat="server" TabIndex="4" MaxLength="4" EnableTheming="false" Width="40"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxMonth" runat="server" TabIndex="5" MaxLength="2" EnableTheming="false" Width="20"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDay" runat="server" TabIndex="6" MaxLength="2" EnableTheming="false" Width="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelYear" runat="server" Enabled="false" Text="YYYY"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelMonth" runat="server" Enabled="false" Text="MM"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelDay" runat="server" Enabled="false" Text="DD"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="TextBoxHours" runat="server" TabIndex="7" MaxLength="2" EnableTheming="false"
                Width="15" Visible="false" Text="12"></asp:TextBox>
            <asp:TextBox ID="TextBoxMinutes" runat="server" TabIndex="8" MaxLength="2" EnableTheming="false"
                Width="15" Visible="false" Text="00"></asp:TextBox>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <br />
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="4"></asp:TextBox>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <asp:GridView ID="GridViewSamples" runat="server" DataSourceID="ObjectDataSourceSamples"
                AutoGenerateColumns="False" PageIndex="0" AllowPaging="true">
                <Columns>
                    <asp:BoundField DataField="RetentionSamples" HeaderText="Retention" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
					<asp:BoundField DataField="PharmacySamples" HeaderText="Pharmacy" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
					<asp:BoundField DataField="AssaySamples" HeaderText="AssaySamples" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceSamples" runat="server" TypeName="Receiving" SelectMethod="GetSampleTypes">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="receiptLineId" SessionField="ReceiptLineId" Type="Int32" DefaultValue="-1" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:Label ID="LabelSample" runat="server" Text="<%$ Resources:Default, Samples %>"></asp:Label>
            <asp:TextBox ID="TextBoxSample" runat="server" TabIndex="5"></asp:TextBox>
        </asp:View>
        <asp:View ID="View6" runat="server">
            <br />
            <asp:Label ID="LabelReject" runat="server" Text="<%$ Resources:Default, Rejects %>"></asp:Label>
            <asp:TextBox ID="TextBoxReject" runat="server" TabIndex="5" Text="0"></asp:TextBox>
            <br />
            <asp:RadioButtonList ID="RadioButtonListReason" runat="server" TabIndex="6" RepeatColumns="3"
                RepeatDirection="Vertical" RepeatLayout="Table" DataSourceID="ObjectDataSourceReason"
                DataValueField="ReasonId" DataTextField="Reason">
            </asp:RadioButtonList>
            <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
                SelectMethod="GetReasonsByType">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="RW" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="View7" runat="server">
            <br />
            <asp:Label ID="LabelStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
            <asp:TextBox ID="TextBoxStoreLocation" runat="server" TabIndex="4"></asp:TextBox>
        </asp:View>
        <asp:View ID="View8" runat="server">
            <br />
            <asp:Label ID="LabelNumberOfPallets" runat="server" Text="<%$ Resources:Default, NumberOfPallets %>"></asp:Label>
            <asp:TextBox ID="TextBoxNumberOfPallets" runat="server" TabIndex="8"></asp:TextBox>
        </asp:View>
        <asp:View ID="View9" runat="server">
            <br />
            <asp:Label ID="LabelPallet" runat="server" Text="<%$ Resources:Default, PalletId %>"></asp:Label>
            <asp:TextBox ID="TextBoxPallet" runat="server" TabIndex="9"></asp:TextBox>
        </asp:View>
        <asp:View ID="View10" runat="server">
            <uc1:SerialNumbers ID="SerialNumbers1" runat="server" />
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="10" Text="<%$ Resources:Default, Accept %>"
        OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="11" Text="<%$ Resources:Default, Back %>"
        OnClick="LinkButtonBack_Click"></asp:Button>
    <asp:Button ID="LinkButtonSkip" runat="server" TabIndex="12" Text="<%$ Resources:Default, Skip %>"
        OnClick="LinkButtonSkip_Click" Visible="false"></asp:Button>
    <asp:Button ID="LinkButtonPackaging" runat="server" TabIndex="13" Text="<%$ Resources:Default, Packaging %>"
        OnClick="LinkButtonPackaging_Click"></asp:Button>
    <asp:Button ID="LinkButtonRedelivery" runat="server" TabIndex="14" Text="<%$ Resources:Default, Redelivery %>"
        OnClick="LinkButtonRedelivery_Click"></asp:Button>
    <asp:Button ID="btnComplete" runat="server" TabIndex="14" Text="<%$ Resources:Default, Finish %>"
        OnClick="Complete_Click"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="15" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>
