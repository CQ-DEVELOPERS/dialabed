<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="LoadBuild.aspx.cs" Inherits="Screens_LoadBuild" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsViewDocument" runat="server" DataKeyNames="ReceiptId" DataSourceID="ObjectDataSourceDocument" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
            <asp:BoundField DataField="VehicleRegistration" HeaderText="<%$ Resources:Default, VehicleRegistration %>" />
            <asp:BoundField DataField="DeliveryNoteNumber" HeaderText="<%$ Resources:Default, DeliveryNoteNumber %>" />
            <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" />
            <asp:BoundField DataField="SealNumber" HeaderText="<%$ Resources:Default, SealNumber %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="Receiving"
        SelectMethod="ReceivingDocumentSelect">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View0" runat="server">
            <br />
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, Load2 %>"></asp:Label>
            <asp:RadioButtonList ID="rblType" runat="server">
                <asp:ListItem Selected="True" Text="<%$ Resources:Default, New %>" Value="New"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Default, Existing %>" Value="Old"></asp:ListItem>
            </asp:RadioButtonList>
        </asp:View>
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, LoadNumber %>"></asp:Label>
            <asp:TextBox ID="TextBoxOrderNumber" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:Label ID="LabelVehicleRegistration" runat="server" Text="<%$ Resources:Default, VehicleRegistration2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxVehicleRegistration" runat="server" TabIndex="2"></asp:TextBox>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <br />
            <asp:Label ID="LabelDeliveryNoteNumber" runat="server" Text="<%$ Resources:Default, DeliveryNoteNumber2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxDeliveryNoteNumber" runat="server" TabIndex="3"></asp:TextBox>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <br />
            <asp:Label ID="LabelDeliveryDate" runat="server" Text="<%$ Resources:Default, DeliveryDate %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxYear" runat="server" TabIndex="4" MaxLength="4" EnableTheming="false" Width="30"></asp:TextBox>
            <asp:TextBox ID="TextBoxMonth" runat="server" TabIndex="5" MaxLength="2" EnableTheming="false" Width="15"></asp:TextBox>
            <asp:TextBox ID="TextBoxDay" runat="server" TabIndex="6" MaxLength="2" EnableTheming="false" Width="15"></asp:TextBox>
            <asp:TextBox ID="TextBoxHours" runat="server" TabIndex="7" MaxLength="2" EnableTheming="false" Width="15"></asp:TextBox>
            <asp:TextBox ID="TextBoxMinutes" runat="server" TabIndex="8" MaxLength="2" EnableTheming="false" Width="15"></asp:TextBox>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <br />
            <asp:Label ID="LabelSealNumber" runat="server" Text="<%$ Resources:Default, SealNumber %>"></asp:Label>
            <asp:TextBox ID="TextBoxSealNumber" runat="server" TabIndex="9"></asp:TextBox>
        </asp:View>
        <asp:View ID="View6" runat="server">
            <br />
            <asp:Label ID="LabelRoute" runat="server" Text="<%$ Resources:Default, Route %>"></asp:Label>
            <asp:RadioButtonList ID="RadioButtonListRoute" runat="server" TabIndex="12" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table"
                DataSourceID="ObjectDataSourceRoute" DataValueField="Route" DataTextField="Route" BorderStyle="Double" CellPadding="0" CellSpacing="0" BorderWidth="1" TextAlign="Right">
            </asp:RadioButtonList>
            <asp:ObjectDataSource ID="ObjectDataSourceRoute" runat="server" TypeName="Route"
                SelectMethod="SelectRoutes">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <%--<br />
            <asp:Label ID="LabelRoute" runat="server" Text="<%$ Resources:Default, Route %>"></asp:Label>
            <asp:TextBox ID="TextBoxRoute" runat="server" TabIndex="10"></asp:TextBox>--%>
        </asp:View>
        <asp:View ID="View7" runat="server">
            <br />
            <asp:Label ID="LabelRemarks" runat="server" Text="<%$ Resources:Default, Remarks %>"></asp:Label>
            <asp:TextBox ID="TextBoxRemarks" runat="server" TabIndex="11"></asp:TextBox>
        </asp:View>
        <asp:View ID="View8" runat="server">
            <br />
            <asp:Label ID="LabelDriverId" runat="server" Text="<%$ Resources:Default, Driver %>"></asp:Label>
            <asp:RadioButtonList ID="RadioButtonListDriver" runat="server" TabIndex="12" RepeatColumns="2" RepeatDirection="Vertical" RepeatLayout="Table"
                DataSourceID="ObjectDataSourceDriver" DataValueField="DriverId" DataTextField="Driver">
            </asp:RadioButtonList>
            <asp:ObjectDataSource ID="ObjectDataSourceDriver" runat="server" TypeName="IntransitLoad"
                SelectMethod="GetDrivers">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <%--<asp:Parameter Name="ReasonCode" Type="String" DefaultValue="RW" />--%>
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="13" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="14" Text="<%$ Resources:Default, Back %>" OnClick="LinkButtonBack_Click"></asp:Button>
    <asp:Button ID="LinkButtonSkip" runat="server" TabIndex="15" Text="<%$ Resources:Default, Skip %>" OnClick="LinkButtonSkip_Click" Visible="false"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="16" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

