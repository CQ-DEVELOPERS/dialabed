using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_AutoMove : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region StyleSheetTheme
    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }
    #endregion StyleSheetTheme

    #region Page_PreInit
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }
    #endregion Page_PreInit

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    Page.SetFocus(TextBoxPickLocation);
                    break;
                case 1:
                    Page.SetFocus(TextBoxPalletId);
                    break;
                case 2:
                    Page.SetFocus(TextBoxBatch);
                    break;
                case 3:
                    Page.SetFocus(TextBoxQuantity);
                    break;
                case 4:
                    Page.SetFocus(TextBoxStoreLocation);
                    break;
                
            };

    }
    #endregion Page_LoadComplete

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/AutoMove.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
            

        }
        catch { }
    }
    #endregion

    #region LinkButtonAccept_Click
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();
            decimal quantity = -1;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    if (tran.ValidLocation(Session["ConnectionStringName"].ToString(), TextBoxPickLocation.Text))
                    {
                        DetailsViewStock_DataBind();

                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxPickLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidLocation;
                    }

                    break;
                case 1:
                    
                    Session["ValidBatch"] = null;
                    
                    if (tran.ValidProduct(Session["ConnectionStringName"].ToString(), TextBoxPalletId.Text) == false)
                    {
                        TextBoxPalletId.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidPalletId;
                        break;
                    }

                    int storageUnitId = tran.GetStorageUnit(Session["ConnectionStringName"].ToString(), TextBoxPalletId.Text, -1, (int)Session["WarehouseId"]);
                    
                    if (TextBoxPalletId.Text.StartsWith("P:"))
                    {
                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 231))
                        {
                            string validPalletStatus = tran.ValidPalletStatus(Session["ConnectionStringName"].ToString(), TextBoxPalletId.Text);

                            if (validPalletStatus != "")
                            {
                                TextBoxPalletId.Text = "";
                                Master.MsgText = Resources.ResMessages.InvalidBatchQA;
                                break;
                            }
                        }

                        DetailsViewStock_DataBind();

                        if (DetailsViewStock1.Rows.Count < 1)
                        {
                            TextBoxPalletId.Text = "";
                            Master.MsgText = Resources.ResMessages.InvalidProduct;
                        }
                        else
                        {
                            MultiView1.ActiveViewIndex++;
                            //MultiView1.ActiveViewIndex++;
                            DetailsViewStock_DataBind();

                            Master.MsgText = Resources.ResMessages.Successful;
                        }

                        // Skip batch check
                        MultiView1.ActiveViewIndex++;
                    }
                    else
                    {
                        Session["ValidBatch"] = TextBoxPalletId.Text;
                        MultiView1.ActiveViewIndex++;

                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 231)
                            || Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), -1, storageUnitId) == LotAttributeRule.None)
                        {
                            DetailsViewStock_DataBind();

                            if (DetailsViewStock1.Rows.Count < 1)
                            {
                                MultiView1.ActiveViewIndex--;
                                TextBoxPalletId.Text = "";
                                Master.MsgText = Resources.ResMessages.InvalidBatch;
                                break;
                            }

                            MultiView1.ActiveViewIndex++;
                        }
                        else
                        {
                            DetailsViewStock_DataBind();

                            if (DetailsViewStock1.Rows.Count < 1)
                            {
                                MultiView1.ActiveViewIndex--;
                                TextBoxPalletId.Text = "";
                                Master.MsgText = Resources.ResMessages.InvalidProduct;
                                break;
                            }
                        }
                        
                    }

                    break;
                case 2:
                    if (Session["ValidBatch"] != null)
                    {
                        if (tran.ValidBatch(Session["ConnectionStringName"].ToString(), Session["ValidBatch"].ToString(), TextBoxBatch.Text))
                        {
                            Master.MsgText = Resources.ResMessages.Successful;
                        }
                        else
                        {
                            //TextBoxPickLocation.Text = "";
                            Master.MsgText = Resources.ResMessages.InvalidBatch;
                        }
                        // ** Start - Karen - May 2012 - check for stock **
                        if (tran.CheckSOH(Session["ConnectionStringName"].ToString(), TextBoxPickLocation.Text, TextBoxPalletId.Text, TextBoxBatch.Text, (int)Session["WarehouseId"]) == false)
                        {
                            //TextBoxPalletId.Text = "";
                            Master.MsgText = Resources.ResMessages.NoSOH;
                            break;
                        }
                        // ** End - check for stock **

                        DetailsViewStock_DataBind();

                        if (DetailsViewStock1.Rows.Count < 1)
                        {
                            //TextBoxPalletId.Text = "";
                            Master.MsgText = Resources.ResMessages.InvalidBatch;
                        }
                        else
                        {
                            MultiView1.ActiveViewIndex++;
                            Master.MsgText = Resources.ResMessages.Successful;
                        }
                    }
                    else
                    {
                        MultiView1.ActiveViewIndex++;
                    }
                    break;
                case 3:
                    if (decimal.TryParse(TextBoxQuantity.Text, out quantity))
                    {
                        LabelNewQuantity2.Text = TextBoxQuantity.Text;
                        Master.MsgText = Resources.ResMessages.Successful;
                        MultiView1.ActiveViewIndex++;
                    }
                    else
                    {
                        TextBoxQuantity.Text = "";
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                    }

                    break;
                case 4:
                    if (tran.ValidLocation(Session["ConnectionStringName"].ToString(), TextBoxStoreLocation.Text))
                    {
                        if (decimal.TryParse(TextBoxQuantity.Text, out quantity))
                        {
                            int storageUnitBatchId = (int)DetailsViewStock1.DataKey["StorageUnitBatchId"];
                            int instructionId = tran.AutoMove(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], storageUnitBatchId, TextBoxPickLocation.Text, TextBoxStoreLocation.Text, quantity, "M");

                            if (instructionId < 0)
                            {
                                switch (instructionId)
                                {
                                    case -11:
                                        Master.MsgText = Resources.ResMessages.InvalidBatchQA;
                                        break;
                                    case -10:
                                        Master.MsgText = Resources.ResMessages.InvalidProductStoreLocation;
                                        break;
                                    case -9:
                                        Master.MsgText = Resources.ResMessages.ReplenishmentAlreadyCreated;
                                        break;
                                    default:
                                        Master.MsgText = Resources.ResMessages.Failed;
                                        break;
                                }

                                return;
                            }
                            else
                            {
                                Session["InstructionId"] = instructionId;

                                Master.MsgText = Resources.ResMessages.Successful;

                                DetailsViewStock1.Visible = false;

                                DetailsViewInstruction.DataBind();

                                TextBoxPalletId.Text = "";
                                TextBoxBatch.Text = "";
                                TextBoxPickLocation.Text = "";
                                TextBoxStoreLocation.Text = "";
                                TextBoxQuantity.Text = "";
                                LinkButtonAccept.Text = Resources.Default.Next;
                                MultiView1.ActiveViewIndex++;
                            }
                        }
                        else
                        {
                            TextBoxQuantity.Text = "";
                            Master.MsgText = Resources.ResMessages.QuantityReEnter;
                        }
                    }
                    else
                    {
                        TextBoxStoreLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidLocation;
                    }

                    break;
                case 5:
                    LinkButtonAccept.Text = Resources.Default.Accept;
                    MultiView1.ActiveViewIndex = 0;
                    break;

            }
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message.ToString();
        }
    }
    #endregion LinkButtonAccept_Click

    #region DetailsViewStock_PageIndexChanging
    protected void DetailsViewStock_PageIndexChanging(object sender,DetailsViewPageEventArgs e)
    {
        try
        {
            DetailsViewStock1.PageIndex = e.NewPageIndex;
            DetailsViewStock_DataBind();
        }
        catch { }
    }
    #endregion DetailsViewStock_PageIndexChanging

    #region DetailsViewStock_DataBind
    protected void DetailsViewStock_DataBind()
    {
        try
        {
            Transact tran = new Transact();

            //if (Session["ValidBatch"] == null)
            //{
            //    DetailsViewStock1.DataSource = tran.ValidStockLink(Session["ConnectionStringName"].ToString(), TextBoxPickLocation.Text, TextBoxPalletId.Text);
            //}
            //else
            //{
            //    DetailsViewStock1.DataSource = tran.ValidProductBatchLink(Session["ConnectionStringName"].ToString(), TextBoxPickLocation.Text, TextBoxPalletId.Text, TextBoxBatch.Text, (int)Session["WarehouseId"]);
            //}

            foreach (DetailsViewRow row2 in DetailsViewStock1.Rows)
            {
                DetailsViewStock1.Fields[row2.RowIndex].Visible = true;
            }

                DetailsViewStock1.DataSource = tran.ValidProductBatchLink(Session["ConnectionStringName"].ToString(), TextBoxPickLocation.Text, TextBoxPalletId.Text, TextBoxBatch.Text, (int)Session["WarehouseId"]);

            DetailsViewStock1.DataBind();

            try
            {
                if (DetailsViewStock1.Rows.Count > 0)
                {
                    DetailsViewStock1.Visible = true;

                    foreach (DetailsViewRow row in DetailsViewStock1.Rows)
                    {
                        foreach (DataControlFieldCell cell in row.Cells)
                        {
                            DetailsViewStock1.Fields[row.RowIndex].Visible = true;

                            if (DetailsViewStock1.Fields[row.RowIndex].AccessibleHeaderText == "StoreOrder")
                            {
                                TextBox txtBox = (TextBox)cell.FindControl("txtStoreArea");

                                if (txtBox.Text == "")
                                    DetailsViewStock1.Fields[row.RowIndex].Visible = false;
                            }
                            else if (cell.Text == null || cell.Text == "" || cell.Text == "0" || cell.Text == "&nbsp;")
                            {
                                DetailsViewStock1.Fields[row.RowIndex].Visible = false;
                            }
                        }
                    }
                }
            }
            catch { }
        }
        catch { }
    }
    #endregion DetailsViewStock_DataBind

    #region btnBack_Click
    protected void btnBack_Click(object sender, EventArgs e)
    {
        try
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    break;
                case 1:
                    MultiView1.ActiveViewIndex--;
                    TextBoxPalletId.Text = "";
                    break;
                case 2:
                    MultiView1.ActiveViewIndex--;
                    TextBoxBatch.Text = "";
                    break;
                case 3:
                    MultiView1.ActiveViewIndex--;
                    TextBoxQuantity.Text = "";
                    break;
                case 4:
                    MultiView1.ActiveViewIndex--;
                    TextBoxStoreLocation.Text = "";
                    break;
                default:
                    MultiView1.ActiveViewIndex--;
                    break;
            }
        }
        catch { }
    }
    #endregion btnBack_Click
}
