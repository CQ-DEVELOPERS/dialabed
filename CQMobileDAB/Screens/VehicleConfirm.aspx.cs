using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_VehicleConfirm : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["FromScreen"] = "VehicleConfirm"; 
            if (!Page.IsPostBack)
            {
                TextBoxConfirmLoad.Focus();
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/VehicleConfirm.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                ResetSessionValues();
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void ResetSessionValues()
    {
        // Reset all the Session Values
    }

    protected void LinkButtonConfirmLoad_Click(object sender, EventArgs e)
    {
        int result = 0, total = 0, pallets = 0;
        DBErrorMessage dberr = new DBErrorMessage();

        if (TextBoxConfirmLoad.Text == "")
        {
            Master.MsgText = Resources.ResMessages.InvalidLoad.ToString();
            TextBoxConfirmLoad.TabIndex = 1;
            TextBoxConfirmLoad.Focus();
            return;
        }
        if (TextBoxVehicleID.Text == "")
        {
            Master.MsgText = Resources.ResMessages.InvalidVehicleID.ToString();
            TextBoxVehicleID.TabIndex = 1;
            TextBoxVehicleID.Focus();
            return;
        }
        else
        {
            TextBoxBarcode.Focus();
        }

        ArrayList arrList = new ArrayList();
        Despatch despatch = new Despatch();

        arrList = despatch.ConfirmVehicle(Session["ConnectionStringName"].ToString(), TextBoxConfirmLoad.Text, TextBoxVehicleID.Text, "-1", (int)Session["OperatorId"]);

        if (arrList != null)
        {
            result = int.Parse(arrList[0].ToString());

            pallets = int.Parse(arrList[1].ToString());

            total = int.Parse(arrList[2].ToString());
        }
        else
            return;
        if (result != 0)
        {
            Master.MsgText = dberr.ReturnErrorString(result);
            ResetSessionValues();
        }
        else
        {
            LabelPallets.Text = pallets.ToString();
            LabelTotal.Text = total.ToString();
            Master.MsgText = Resources.ResMessages.Successful.ToString();
            LabelConfirmLoad2Display.Text = TextBoxConfirmLoad.Text;
            LabelVehicleID2Display.Text = TextBoxVehicleID.Text;

            MultiViewConfirm.ActiveViewIndex++;
        }
    }

    protected void LinkButtonBarcode_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList arrList = new ArrayList();

            Button btn = new Button();
            btn = (Button)sender;

            int result = 0, total = 0, pallets = 0;
            int operatorId = -1;

            if (!int.TryParse(Session["OperatorId"].ToString(), out operatorId))
            {
                Master.MsgText = Resources.ResMessages.InvalidOperator.ToString();
                return;
            }

            if (LabelConfirmLoad2Display.Text == "")
            {
                Master.MsgText = Resources.ResMessages.InvalidLocation.ToString();
                TextBoxConfirmLoad.TabIndex = 1;
                TextBoxConfirmLoad.Focus();
                return;
            }

            if (TextBoxBarcode.Text == "")
            {
                Master.MsgText = Resources.ResMessages.InvalidPalletId.ToString();
                TextBoxBarcode.TabIndex = 1;
                TextBoxBarcode.Focus();
                return;
            }

            Despatch despatch = new Despatch();

            arrList = despatch.ConfirmVehicle(Session["ConnectionStringName"].ToString(), LabelConfirmLoad2Display.Text, LabelVehicleID2Display.Text, TextBoxBarcode.Text, (int)Session["OperatorId"]);

            if (arrList != null)
            {
                result = int.Parse(arrList[0].ToString());

                pallets = int.Parse(arrList[1].ToString());

                total = int.Parse(arrList[2].ToString());
            }
            else
                return;

            TextBoxBarcode.Text = "";

            DBErrorMessage dberr = new DBErrorMessage();
            
            //if (pallets == 0)
            //{
            //    Master.MsgText = Resources.ResMessages.NoOtherLines.ToString();
            //    ResetSessionValues();
            //    TextBoxVehicleID.Text = "";
            //    TextBoxConfirmLoad.Text = "";
            //    MultiViewConfirm.ActiveViewIndex--;
            //}

            if (result != 0)
            {
                Master.MsgText = dberr.ReturnErrorString(result);
                ResetSessionValues();
            }
            else
            {
                LabelPallets.Text = pallets.ToString();
                LabelTotal.Text = total.ToString();
                Master.MsgText = Resources.ResMessages.Successful.ToString();
                ResetSessionValues();
            }
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
}
