using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PalletRebuild : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/PalletRebuild.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      switch (MultiView1.ActiveViewIndex)
      {
        case 0:
          Page.SetFocus(TextBoxBarcode);
          break;
        case 1:
          Page.SetFocus(TextBoxProduct);
          break;
        case 3:
          Page.SetFocus(TextBoxQuantity);
          break;
        case 6:
          Page.SetFocus(TextBoxBarcode2);
          break;

      };

    }

    #region LinkButtonAccept_Click
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            PalletRebuild pr = new PalletRebuild();
            int jobId = -1;
            int unlinkedJobId = -1;
            
            jobId = pr.GetJob(Session["ConnectionStringName"].ToString(), TextBoxBarcode.Text);

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    if (jobId > 0)
                    {
                        Session["JobId"] = jobId;
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
                case 1:
                    GridViewInstructions.DataBind();

                    if (GridViewInstructions.Rows.Count > 0)
                    {
                        TextBoxQuantity.Text = "";
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxProduct.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidProduct;
                    }
                    break;
                case 3:
                    Decimal quantity = 0;

                    if (Decimal.TryParse(TextBoxQuantity.Text, out quantity))
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxQuantity.Text = "";
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                        break;
                    }
                    break;
                case 4:
                    unlinkedJobId = pr.GetJob(Session["ConnectionStringName"].ToString(), TextBoxBarcode2.Text);

                    if (unlinkedJobId > 0)
                    {
                        //Session["UnlinkedJobId"] = unlinkedJobId;

                        if (TextBoxProduct.Text == "ALL")
                        {
                            if (unlinkedJobId > 0)
                            {
                                if (pr.Move(Session["ConnectionStringName"].ToString(),
                                            jobId,
                                            unlinkedJobId))
                                {
                                    Master.MsgText = "Entire job moved.";

                                    TextBoxBarcode.Text = "";
                                    TextBoxProduct.Text = "";
                                    GridViewInstructions.SelectedIndex = 0;
                                    TextBoxQuantity.Text = "";
                                    MultiView1.ActiveViewIndex = 0;
                                    Master.MsgText = Resources.ResMessages.Successful;
                                }
                                else
                                {
                                    Master.MsgText = "There was an error, please try again.";
                                }
                            }
                            else
                            {
                                TextBoxBarcode2.Text = "";
                                Master.MsgText = Resources.ResMessages.InvalidBarcode;
                            }
                        }
                        else
                        {
                            if (pr.Remove(Session["ConnectionStringName"].ToString(),
                                            unlinkedJobId,
                                            Convert.ToInt32(Session["IssueLineId"]),
                                            Convert.ToInt32(Session["InstructionId"]),
                                            Decimal.Parse(TextBoxQuantity.Text)) == true)
                            {
                                Session["IssueLineId"] = null;
                                Session["InstructionId"] = null;

                                Master.MsgText = "Removed from Job";

                                TextBoxProduct.Text = "";
                                GridViewInstructions.SelectedIndex = -1;
                                TextBoxQuantity.Text = "";
                            }
                            else
                            {
                                Master.MsgText = "There was an error, please try again.";
                            }

                            MultiView1.ActiveViewIndex = 1;
                            Master.MsgText = Resources.ResMessages.Successful;
                        }
                    }
                    else
                    {
                        TextBoxBarcode2.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonAccept_Click

    #region ButtonAll_Click
    protected void ButtonAll_Click(object sender, EventArgs e)
    {
        try
        {
            TextBoxProduct.Text = "ALL";
            MultiView1.ActiveViewIndex = 4;
            Master.MsgText = Resources.ResMessages.Successful;
        }
        catch { }
    }
    #endregion ButtonAll_Click

    #region ButtonOr_Click
    protected void ButtonOr_Click(object sender, EventArgs e)
    {
        try
        {
            PalletRebuild pr = new PalletRebuild();
            int jobId = -1;
            int unlinkedJobId = -1;

            if (Session["JobId"] == null)
                return;

            jobId = (int)Session["JobId"];

            //jobId = pr.GetJob(Session["ConnectionStringName"].ToString(), TextBoxBarcode.Text);

            //if (jobId > 0)
            //{
            //    Session["JobId"] = jobId;
            //}
            //else
            //{
            //    TextBoxBarcode.Text = "";
            //    Master.MsgText = Resources.ResMessages.InvalidBarcode;
            //}

            unlinkedJobId = pr.GetJob(Session["ConnectionStringName"].ToString(), TextBoxBarcode2.Text);

            if (unlinkedJobId > 0)
            {
                Session["UnlinkedJobId"] = unlinkedJobId;

                if (pr.Move(Session["ConnectionStringName"].ToString(),
                            jobId,
                            unlinkedJobId))
                {
                    Master.MsgText = "Entire job moved.";

                    TextBoxProduct.Text = "";
                    GridViewInstructions.SelectedIndex = 0;
                    TextBoxQuantity.Text = "";
                }
                else
                {
                    Master.MsgText = "There was an error, please try again.";
                }

                MultiView1.ActiveViewIndex = 1;
                Master.MsgText = Resources.ResMessages.Successful;
            }
            else
            {
                TextBoxBarcode2.Text = "";
                Master.MsgText = Resources.ResMessages.InvalidBarcode;
            }
        }
        catch { }
    }
    #endregion ButtonOr_Click

    //protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    //{
    //    LinkButtonSkip.Visible = false;
    //    LinkButtonRedelivery.Visible = false;

    //    switch (MultiView1.ActiveViewIndex)
    //    {
    //        case 0:
    //            LinkButtonRedelivery.Visible = true;
    //            break;
    //        case 2:
    //            if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124))
    //                MultiView1.ActiveViewIndex++;
    //            else
    //            {
    //                TextBoxYear.Text = DateTime.Now.Year.ToString();
    //                TextBoxMonth.Text = DateTime.Now.Month.ToString();
    //                TextBoxDay.Text = DateTime.Now.Day.ToString();
    //                TextBoxHours.Text = DateTime.Now.Hour.ToString();
    //                TextBoxMinutes.Text = DateTime.Now.Minute.ToString();
    //            }
    //            break;
    //        case 4:
    //            if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 151))
    //                MultiView1.ActiveViewIndex++;

    //            LinkButtonSkip.Visible = true;

    //            break;
    //        case 5:
    //            if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 152))
    //                MultiView1.ActiveViewIndex++;

    //            LinkButtonSkip.Visible = true;

    //            break;
    //        case 6:
    //            if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 174))
    //            {
    //                Reset();
    //            }
    //            LinkButtonSkip.Visible = true;
    //            break;
    //    }
    //}

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    #region GridViewUnlinked_SelectedIndexChanged
    protected void GridViewUnlinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //GridViewInstructions.SelectedIndex = -1;

            Session["IssueLineId"] = GridViewInstructions.SelectedDataKey["IssueLineId"];
            Session["InstructionId"] = GridViewInstructions.SelectedDataKey["InstructionId"];

            TextBoxQuantity.Text = GridViewInstructions.SelectedDataKey["ConfirmedQuantity"].ToString();

            MultiView1.ActiveViewIndex++;
        }
        catch { }
    }
    #endregion "GridViewUnlinked_SelectedIndexChanged"

    protected void Reset()
    {
        MultiView1.ActiveViewIndex = 0;
        Session["IssueLineId"] = null;
        Session["InstructionId"] = null;
        Session["JobId"] = null;
        Master.MsgText = Resources.ResMessages.Successful;
        TextBoxBarcode.Text = "";
        TextBoxBarcode2.Text = "";
        TextBoxProduct.Text = "";
        TextBoxQuantity.Text = "";
    }
}
