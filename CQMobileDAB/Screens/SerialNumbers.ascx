<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SerialNumbers.ascx.cs" Inherits="Screens_AlternatePickLocation" %>

<asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
        <asp:GridView ID="GridView2" TabIndex="3" runat="server" DataSourceID="ObjectDataSource2" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="Count" HeaderText="Scanned" />
                <asp:BoundField DataField="Total" HeaderText="Total" />
            </Columns>
        </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="SerialNumber" SelectMethod="GetTotal">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                <asp:SessionParameter Name="keyId" SessionField="KeyId" Type="Int32" DefaultValue="-1" />
                <asp:SessionParameter Name="keyType" SessionField="KeyType" Type="String" DefaultValue="-1" />
                <asp:SessionParameter Name="storageUnitId" SessionField="KeyStorageUnitId" Type="Int32" DefaultValue="-1" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
        <asp:Label ID="LabelSerialNumber" runat="server" Text="<%$ Resources:Default, SerialNumber %>"></asp:Label>
        <asp:TextBox ID="TextBoxSerialNumber" runat="server" TabIndex="1"></asp:TextBox>
        <asp:Button ID="ButtonAdd" runat="server" TabIndex="2" Text="<%$ Resources:Default, Add %>" OnClick="ButtonAdd_Click"></asp:Button>
        <br />
        <br />
        <asp:GridView ID="GridView1" TabIndex="3" runat="server" DataSourceID="ObjectDataSource1" DataKeyNames="SerialNumberId" AutoGenerateColumns="false" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="SerialNumber" HeaderText="<%$ Resources:Default, SerialNumber %>" />
                <asp:CommandField ButtonType="Link" SelectText="DEL" ShowSelectButton="true" />
            </Columns>
        </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="SerialNumber" SelectMethod="GetSerialNumbers">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                <asp:SessionParameter Name="keyId" SessionField="KeyId" Type="Int32" DefaultValue="-1" />
                <asp:SessionParameter Name="keyType" SessionField="KeyType" Type="String" DefaultValue="-1" />
                <asp:SessionParameter Name="storageUnitId" SessionField="KeyStorageUnitId" Type="Int32" DefaultValue="-1" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:View>
</asp:MultiView>