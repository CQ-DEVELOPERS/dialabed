<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="StockTakeEmpty.aspx.cs" Inherits="Screens_StockTakeEmpty" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="LabelConfirmLocation" runat="server" Text="<%$ Resources:Default, Location %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxConfirmLocation" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="1" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
            <asp:Button ID="LinkButtonReject1" runat="server" TabIndex="2" Text="<%$ Resources:Default, Reset %>" OnClick="LinkButtonReject_Click"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="LocationId" DataSourceID="ObjectDataSourceStock" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default, ActualQuantity %>" />
                    <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" />
                    <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default, ReservedQuantity %>" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceStock" runat="server" TypeName="StockTake"
                SelectMethod="EmptyLocationSelect">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:ControlParameter Name="location" Type="String" ControlID="TextBoxConfirmLocation" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Button ID="LinkButtonCreate" runat="server" TabIndex="1" Text="<%$ Resources:Default, CreateStockTake %>" OnClick="LinkButtonCreate_Click"></asp:Button>
            <asp:Button ID="LinkButtonEmpty" runat="server" TabIndex="1" Text="<%$ Resources:Default, EmptyLocation %>" OnClick="LinkButtonEmpty_Click"></asp:Button>
            <asp:Button ID="LinkButtonReject2" runat="server" TabIndex="2" Text="<%$ Resources:Default, Reset %>" OnClick="LinkButtonReject_Click"></asp:Button>
        </asp:View>
    </asp:MultiView>
</asp:Content>

