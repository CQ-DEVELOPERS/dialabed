using System;
using System.Data;
using System.Text;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// J:107  send through a 1
/// </summary>
public partial class Screens_CheckPallet : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PrivateVariables
        private int result = 0;
        private string strresult = "";
        private string theErrMethod = "";
    #endregion PrivateVariables

    #region Page_Load 
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = "CHECK PALLET";

            if (!Page.IsPostBack)
            {
                TextBoxPalletId.Text = Session["PalletId"].ToString();

                CheckConfig();
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion Page_Load

    #region CommandCancel_Click 
    protected void CommandCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainMenu.aspx");
    }
    #endregion CommandCancel_Click

    #region CommandNext_Click 
    protected void CommandNext_Click(object sender, EventArgs e)
    {
        try
        {
            result = CheckPallet_DataBind(0, 1);
            //Session["InstructionId"] = result.ToString();
            if (result == 5 || result == 3) //5 is for mix and 3 is full
            {
                MultiView1.ActiveViewIndex = 0;
                //Master.MsgText = "Quantity Error";        
                TextBoxQuantity.Focus();
            }
            else if (result == -1)
            {
                MultiView1.ActiveViewIndex = 0;
                TextBoxBatch.Text = "";
                TextBoxQuantity.Text = "";
                TextBoxBarcode.Text = "";
                //Master.MsgText = "Please re-enter all values";

            }
            else
            {
                Session["ActionDoneCheckStoreFull"] = true;
                Response.Redirect(Session["Url"].ToString() + "?CheckPallet");

            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion CommandNext_Click

    #region CommandTryAgain_Click 
    protected void CommandTryAgain_Click(object sender, EventArgs e)
    {
        // MultiView1.ActiveViewIndex = 0;
        MultiView1.ActiveViewIndex = 0;
    }
    #endregion CommandTryAgain_Click

    #region CheckPallet_DataBind 
    protected int CheckPallet_DataBind(int type, int operatorId)
    {

        int palletId = RemovePalletScanChar();

        try
        {
          

            // TextBoxPalletId.Text;//remove P: first 2 char
            //int palletIdInt = 0;
            string barcode = TextBoxBarcode.Text;
            string batch = TextBoxBatch.Text;
            string quantity = TextBoxQuantity.Text;
            Decimal quantityInt = -1;


            MultiView1.ActiveViewIndex = 1;

            //Check Config File to see which should be checked.
            bool configChk = false;

            //MobileConfiguration mbconfig = new MobileConfiguration();      
            //configChk = mbconfig.GetSwitch(32);

            ConfigurationCheck configCheck = new ConfigurationCheck();

            /**************************** Barcode ******************************/

            configChk = configCheck.ItemCheck("Product");

            if (!configChk)
            {
                TextBoxBarcode.Visible = false;
                LabelBarcode.Visible = false;

                TextBoxBarcode.TabIndex = 0;
                TextBoxBatch.TabIndex = 1;

                TextBoxBatch.Focus();

                Master.MsgText = "Enter Batch Id";


            }
            else
            {
                if (barcode == "")
                {

                    TextBoxBarcode.Visible = true;
                    LabelBarcode.Visible = true;

                    TextBoxBarcode.TabIndex = 1;
                    TextBoxBatch.TabIndex = 2;
                    TextBoxQuantity.TabIndex = 3;

                    TextBoxBarcode.Focus();

                    Master.MsgText = "Enter Product Barcode";

                    return -1;

                }
                else
                    barcode = TextBoxBarcode.Text;
            }
            //32 PRODUCT    NOT ANYMORE IT IS 8 NOW
            //If Product Need to be check then else set all defaults off

            /*****************************************/

            /***************************** Batch *****************************/

            configChk = configCheck.ItemCheck("Batch");

            if (!configChk)
            {

                TextBoxBatch.Visible = false;
                TextBoxBatch.Visible = false;

                TextBoxBatch.TabIndex = 0;
                TextBoxQuantity.TabIndex = 1;

                TextBoxQuantity.Focus();

                Master.MsgText = "Enter Quantity";

            }
            else
            {
                if (batch == "")
                {

                    TextBoxBatch.Visible = true;
                    LabelBatch.Visible = true;

                    TextBoxBatch.TabIndex = 1;
                    TextBoxQuantity.TabIndex = 2;

                    TextBoxBatch.Focus();

                    Master.MsgText = "Enter Batch";

                    return -1;

                }
                else
                    batch = TextBoxBatch.Text;
            }


            /*****************************************/
            // Do this in sections 
            // First Location
            // Then Batch
            // Then Quantity

            // Test the PalletID


            if (TextBoxPalletId.Text != "")
            {
                quantityInt = RemovePalletScanChar();
                if (quantityInt != -1)
                {
                    //All is well
                }
                else
                {
                    Master.MsgText = "Enter Pallet Id";

                    TextBoxPalletId.TabIndex = 1;
                    TextBoxPalletId.Focus();
                    TextBoxPalletId.Text = "";

                    return -1;

                }

            }
            else
            {
                Master.MsgText = "Enter Pallet Id";

                TextBoxPalletId.TabIndex = 1;
                TextBoxPalletId.Focus();
                TextBoxPalletId.Text = "";

                return -1;
            }

            if (quantity != "")
            {
                /****************************** Quantity ****************************/

                if (Decimal.TryParse(TextBoxQuantity.Text, out quantityInt))
                {
                    Master.MsgText = "Valid Quantity";
                }
                else
                {
                    Master.MsgText = "Not Valid Quantity";

                    return -1;
                }
            }
            else
            {

                Master.MsgText = "Enter Quantity";

                TextBoxQuantity.TabIndex = 1;
                TextBoxQuantity.Focus();
                TextBoxQuantity.Text = "";

                return -1;
            }

            
            /*****************************************/

            CheckPallet checkPallet = new CheckPallet();

            DataSet ds = checkPallet.Verify(type, operatorId, palletId, barcode, batch, quantityInt);

            result = int.Parse(ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Result"].Ordinal).ToString());

            string msg = ds.Tables[0].Rows[0].ItemArray[1].ToString();

            if (result == 5) // 5 // Mixed Problem Full Qty Problem
            {
                Master.MsgText = msg;

                TextBoxQuantity.TabIndex = 1;
                TextBoxQuantity.Text = "";
                TextBoxQuantity.Focus();

                return result;
            }
            else if (result == 3) // 3 // Full 
            {
                Master.MsgText = msg;

                TextBoxQuantity.TabIndex = 1;
                TextBoxQuantity.Text = "";
                TextBoxQuantity.Focus();

                return result;
            }
            else if (result != 0)
            {
                Master.MsgText = msg;
                return result;
            }
            else if (result == 0)
            {
                Master.MsgText = msg;
                //Session["PalletId"] = palletId;

            }
            else
                Master.MsgText = ds.Tables[0].Rows[0]["Message"].ToString();

            /*****************************************/

            return result;

        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

        return palletId;
    }
    #endregion CheckPallet_DataBind
    
    #region ClearAllForNewProduct
    private void ClearAllForNewProduct()
    {
        theErrMethod = "ClearAllForNewProduct";
        try
        {
            //Clear Session Variables
            Session["StoreLocation"] = "";
            Session["PickSecurityCode"] = "";
            Session["ConfirmedQuantity"] = "";
            //Session["InstructionId"] = "";
            Session["Quantity"] = "";
            Session["Confirmed"] = "";

            Session["JobId"] = "";
            Session["PickLocation"] = "";
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;

        }
    }
    #endregion ClearAllForNewProduct

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        int loopPrevention = -1;

        try
        {
            try
            {
                loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());
            }
            catch
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                loopPrevention = 0;
            }



            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                  Resources.Default.CheckPalletTitle.ToString(),
                                                                  theErrMethod,
                                                                  ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {

                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
            }

        }
        catch (Exception exMsg)
        {

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                 Resources.Default.CheckPalletTitle.ToString(),
                                                                 theErrMethod,
                                                                 ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling

    #region RemovePalletScanChar
    protected int RemovePalletScanChar()
    {


        string strPalletId = "";
        int palletId = 0;
      
        if (TextBoxPalletId.Text.Length >= 1)
        {
            try
            {
                if (TextBoxPalletId.Text.StartsWith("P:"))
                {
                    if (!int.TryParse(TextBoxPalletId.Text, out palletId))
                    {
                        strPalletId = TextBoxPalletId.Text;
                        // Remove first 2 char.
                        StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                        strPalletId = strReader.ReadLine();
                        palletId = int.Parse(strPalletId);
                        //Session["PalletId"] = TextBoxPalletId.Text;

                        return palletId;
                    }

                }
                else if (TextBoxPalletId.Text.StartsWith("J:"))
                {
                    if (!int.TryParse(TextBoxPalletId.Text, out palletId))
                    {
                        // Remove first 2 char.
                        StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                        strPalletId = strReader.ReadLine();
                        //Session["PalletId"] = TextBoxPalletId.Text;
                        palletId = int.Parse(strPalletId);
                        return palletId;

                    }
                }
                else
                {
                    //this is just a number then use as is
                    if (int.TryParse(TextBoxPalletId.Text, out palletId))
                    {
                        strPalletId = TextBoxPalletId.Text;
                        palletId = int.Parse(strPalletId);
                       
                    }
                    else
                    {
                        Master.MsgText = "Invalid PalletID";
                        return -1;
                    }

                }


            }
            catch (Exception ex)
            {
                string errMsgReturn = SendErrorNow(ex.Message);
                Master.MsgText = errMsgReturn;
            }
           

        }

        return palletId;
    }
    #endregion

    #region CheckConfig
    protected void CheckConfig()
    {

        bool config = false;
        //config = CheckConfig("Product");
        ConfigurationCheck configCheck = new ConfigurationCheck();    

        /****************** Product Check **********************/
        try
        {
            config = configCheck.ItemCheck("Product");

            if (!config)
            {
                TextBoxBarcode.Text = "0";
                LabelBarcode.Visible = false;
                TextBoxBarcode.Visible = false;
            }
            else
            {
                TextBoxBarcode.Text = "";
                LabelBarcode.Visible = true;
                TextBoxBarcode.Visible = true;
            }


            /****************** Batch Check **********************/

            config = configCheck.ItemCheck("Batch");

            if (!config)
            {
                TextBoxBatch.Text = "0";
                LabelBatch.Visible = false;
                TextBoxBatch.Visible = false;
            }
            else
            {
                TextBoxBatch.Text = "";
                LabelBatch.Visible = true;
                TextBoxBatch.Visible = true;
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }


        /*********************************************************/
    }
    #endregion 
}
