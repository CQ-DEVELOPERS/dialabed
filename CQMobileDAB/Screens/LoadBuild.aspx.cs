using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_LoadBuild : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["IntransitLoadId"] = null;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/LoadBuild.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        IntransitLoad trans = new IntransitLoad();
        int intransitLoadId = 0;

        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                if (rblType.SelectedValue == "New")
                    intransitLoadId = trans.CreateOrder(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"]);
                else
                {
                    MultiView1.ActiveViewIndex++;
                    TextBoxOrderNumber.Text = "";
                }

                if (intransitLoadId > 0)
                {
                    Session["IntransitLoadId"] = intransitLoadId;
                    TextBoxOrderNumber.Text = intransitLoadId.ToString();
                    MultiView1.ActiveViewIndex++;
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                }
                else
                {
                    TextBoxOrderNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidOrderNumber;
                }

                break;
            case 1:
                intransitLoadId = trans.GetOrder(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxOrderNumber.Text);

                if (intransitLoadId > 0)
                {
                    Session["IntransitLoadId"] = intransitLoadId;
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                }
                else
                {
                    TextBoxOrderNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidOrderNumber;
                }

                break;
            case 2:
                intransitLoadId = trans.UpdateVehicleRegistration(Session["ConnectionStringName"].ToString(), (int)Session["IntransitLoadId"], TextBoxVehicleRegistration.Text);

                if (intransitLoadId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    TextBoxVehicleRegistration.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidVehicleID;
                }
                break;
            case 3:
                intransitLoadId = trans.UpdateDeliveryNoteNumber(Session["ConnectionStringName"].ToString(), (int)Session["IntransitLoadId"], TextBoxDeliveryNoteNumber.Text);

                if (intransitLoadId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    TextBoxDeliveryNoteNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
            case 4:
                String deliveryDate = TextBoxYear.Text + "/" + TextBoxMonth.Text + "/" + TextBoxDay.Text + " " + TextBoxHours.Text + ":" + TextBoxMinutes.Text;
                DateTime result;

                DateTime.TryParse(deliveryDate, out result);

                intransitLoadId = trans.UpdateDeliveryDate(Session["ConnectionStringName"].ToString(), (int)Session["IntransitLoadId"], deliveryDate);

                if (intransitLoadId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    //TextBoxDeliveryDate.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
            case 5:
                intransitLoadId = trans.UpdateSealNumber(Session["ConnectionStringName"].ToString(), (int)Session["IntransitLoadId"], TextBoxSealNumber.Text);

                if (intransitLoadId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    TextBoxSealNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
            case 6:
                intransitLoadId = trans.UpdateRoute(Session["ConnectionStringName"].ToString(), (int)Session["IntransitLoadId"], RadioButtonListRoute.SelectedValue);

                if (intransitLoadId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    RadioButtonListRoute.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
            case 7:
                intransitLoadId = trans.UpdateRemarks(Session["ConnectionStringName"].ToString(), (int)Session["IntransitLoadId"], TextBoxRemarks.Text);

                if (intransitLoadId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    TextBoxRemarks.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
            case 8:
                intransitLoadId = trans.UpdateDriverId(Session["ConnectionStringName"].ToString(), (int)Session["IntransitLoadId"], int.Parse(RadioButtonListDriver.SelectedValue));

                if (intransitLoadId > 0)
                {
                    Response.Redirect("~/Screens/LoadBuildJob.aspx");
                }
                else
                {
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
        }
    }

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    protected void LinkButtonSkip_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 5:
                Response.Redirect("~/Screens/LoadBuildJob.aspx");
                break;
            default:
                MultiView1.ActiveViewIndex++;
                break;
        }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
        LinkButtonSkip.Visible = true;

        switch (MultiView1.ActiveViewIndex)
        {
            case 1:
                LinkButtonSkip.Visible = false;
                break;
            case 2:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 276))
                    MultiView1.ActiveViewIndex++;
                break;
            case 3:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 277))
                    MultiView1.ActiveViewIndex++;
                break;
            case 4:
                TextBoxYear.Text = DateTime.Now.Year.ToString();
                TextBoxMonth.Text = DateTime.Now.Month.ToString();
                TextBoxDay.Text = DateTime.Now.Day.ToString();
                TextBoxHours.Text = DateTime.Now.Hour.ToString();
                TextBoxMinutes.Text = DateTime.Now.Minute.ToString();
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 278))
                    MultiView1.ActiveViewIndex++;
                break;
            case 5:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 279))
                    MultiView1.ActiveViewIndex++;
                break;
            case 6:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 280))
                    MultiView1.ActiveViewIndex++;
                break;
            case 7:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 281))
                    MultiView1.ActiveViewIndex++;
                break;
            case 8:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 282))
                    Response.Redirect("~/Screens/LoadBuildJob.aspx");
                break;
        }
    }
}
