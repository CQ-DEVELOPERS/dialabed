<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="LoadBuildJob.aspx.cs" Inherits="Screens_LoadBuildJob" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsViewDocument" runat="server" DataKeyNames="IntransitLoadId" DataSourceID="ObjectDataSourceDocument" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="IntransitLoadId" HeaderText="<%$ Resources:Default, IntransitLoadId %>" />
            <asp:BoundField DataField="NumberOfJobs" HeaderText="<%$ Resources:Default, NumberOfJobs %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="IntransitLoad"
        SelectMethod="IntransitLoadSelect">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="IntransitLoadId" SessionField="IntransitLoadId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:DetailsView ID="DetailsViewProduct" runat="server" DataKeyNames="ReceiptLineId,Batch" DataSourceID="ObjectDataSourceProduct" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
            <asp:BoundField DataField="ExpiryDate" HeaderText="<%$ Resources:Default, ExpiryDate %>" />
            <asp:BoundField DataField="ReceivedQuantity" HeaderText="<%$ Resources:Default, Quantity %>" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Receiving"
        SelectMethod="ReceivingLineSelect">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="receiptLineId" SessionField="ReceiptLineId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAdd" runat="server" TabIndex="9" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonRemove" runat="server" TabIndex="10" Text="<%$ Resources:Default, Remove %>" OnClick="LinkButtonRemove_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="11" Text="<%$ Resources:Default, Back %>" OnClick="LinkButtonBack_Click"></asp:Button>
    <asp:Button ID="ButtonFinished" runat="server" TabIndex="12" Text="Finished" OnClick="ButtonFinished_Click"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="13" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

