using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PickMixed : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected bool changeactiveindex(int configurationId)
    {
        bool checkme = true;

        if (configurationId != 0)
        {
            checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], configurationId);

            string[,] active = Session["ActiveTabIndexValue"] as string[,];

            for (int i = 0; i < active.GetLength(0); i++)
            {
                if (active[i, 0] == configurationId.ToString())
                {
                    checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));

                    if (!checkme)
                    {
                        Session["ConfiguarationId"] = active[i + 1, 0].ToString();
                        Session["Type"] = active[i + 1, 1].ToString();
                        Session["CurrentView"] = active[i + 1, 1].ToString();
                        Session["ActiveViewIndex"] = active[i + 1, 2].ToString();
                        Master.MsgText = active[i + 1, 3].ToString();

                        checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));
                        configurationId = int.Parse(Session["ConfiguarationId"].ToString());
                        break;
                    }
                    else
                    {
                        Session["ConfiguarationId"] = active[i, 0].ToString();
                        Session["Type"] = active[i, 1].ToString();
                        Session["CurrentView"] = active[i, 1].ToString();
                        Session["ActiveViewIndex"] = active[i, 2].ToString();
                        Master.MsgText = active[i, 3].ToString();

                        checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));
                    }
                    break;
                }
            }
        }

        return checkme;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            DetailsViewInstruction_DataBind();
            if (Session["Error"] != null)
            {
                if ((bool)Session["Error"])
                {
                    if (Session["Message"] != null)
                        Master.MsgText = Session["Message"].ToString();
                }
                else
                {
                    if (Session["Type"] != null)
                    {
                        Session["Error"] = null;
                        Master.MsgText = Session["Message"].ToString();

                        if (Session["CurrentView"] == Session["Type"])
                        {
                            switch (Session["Type"].ToString())
                            {
                                case "GetMixedPick":
                                    Master.MsgText = Resources.ResMessages.ConfirmMixedPick;
                                    Session["Message"] = Resources.ResMessages.ConfirmMixedPick;
                                    DetailsViewInstruction.Visible = false;
                                    Session["Type"] = "GetMixedPickDetails";
                                    Session["ActiveViewIndex"] = 1;
                                    Session["ConfiguarationId"] = 0;
                                    break;
                                case "GetMixedPickDetails":
                                    Master.MsgText = Resources.ResMessages.ConfirmPickLocation;
                                    Session["Message"] = Resources.ResMessages.ConfirmPickLocation;
                                    DetailsViewInstruction.Visible = true;
                                    DetailsViewInstruction_DataBind();
                                    Session["Type"] = "PickLocation";
                                    Session["ActiveViewIndex"] = 2;
                                    Session["ConfiguarationId"] = 94;
                                    break;
                                case "PickLocation":
                                    Master.MsgText = Resources.ResMessages.ConfirmAlternatePickLocation;
                                    Session["Message"] = Resources.ResMessages.ConfirmAlternatePickLocation;
                                    Session["Type"] = "Alternate";
                                    Session["ActiveViewIndex"] = 3;
                                    Session["ConfiguarationId"] = 95;
                                    break;
                                case "Alternate":
                                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 75))
                                    {
                                        Master.MsgText = Resources.ResMessages.ConfirmProduct;
                                        Session["Message"] = Resources.ResMessages.ConfirmProduct;
                                        Session["Type"] = "Product";
                                        Session["ActiveViewIndex"] = 4;
                                        Session["ConfiguarationId"] = 96;
                                    }
                                    else
                                    {
                                        Master.MsgText = Resources.ResMessages.ConfirmQuantity;
                                        Session["Message"] = Resources.ResMessages.ConfirmQuantity;
                                        Session["Type"] = "Quantity";
                                        Session["ActiveViewIndex"] = 6;
                                        Session["ConfiguarationId"] = 98;
                                    }
                                    break;
                                case "LocationError":
                                    DetailsViewInstruction_DataBind();
                                    Master.MsgText = Resources.ResMessages.ConfirmAfterLocationError;
                                    Session["Message"] = Resources.ResMessages.ConfirmAfterLocationError;
                                    Session["Type"] = "PickLocation";
                                    Session["ActiveViewIndex"] = 2;
                                    Session["ConfiguarationId"] = 94;
                                    break;
                                case "Product":
                                    Master.MsgText = Resources.ResMessages.ConfirmBatch;
                                    Session["Message"] = Resources.ResMessages.ConfirmBatch;
                                    Session["Type"] = "ConfirmBatch";
                                    Session["ActiveViewIndex"] = 5;
                                    Session["ConfiguarationId"] = 97;
                                    if (Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), -1, (int)Session["StorageUnitId"]) == LotAttributeRule.None)
                                        goto case "ConfirmBatch";
                                    break;
                                case "ConfirmBatch":
                                    Master.MsgText = Resources.ResMessages.ConfirmQuantity;
                                    Session["Message"] = Resources.ResMessages.ConfirmQuantity;
                                    Session["Type"] = "Quantity";
                                    Session["ActiveViewIndex"] = 6;
                                    Session["ConfiguarationId"] = 98;
                                    break;
                                case "Quantity":
                                    Transact tran = new Transact();

                                    if (tran.Pick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0 && tran.Store(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
                                    {

                                        GetNextPick();

                                        break;
                                    }
                                    else
                                    {
                                        Session["Message"] = Resources.ResMessages.Failed;
                                        Session["Error"] = true;
                                    }
                                    break;
                                case "StockTake":
                                    GetNextPick();
                                    break;
                                case "StoreLocation":
                                    ResetSessionValues();
                                    Session["Message"] = Resources.ResMessages.Successful;
                                    Session["Error"] = false;
                                    Session["Type"] = "GetMixedPick";
                                    Session["ActiveViewIndex"] = 0;
                                    DetailsViewInstruction.Visible = false;
                                    Session["ConfiguarationId"] = 0;

                                    break;
                                default:
                                    Session["Message"] = Resources.ResMessages.Failed;
                                    Session["Error"] = true;
                                    //Response.Redirect("~/Screens/PickMixed.aspx");
                                    break;
                            }

                            bool retval = false;

                            while (!retval)
                            {
                                retval = changeactiveindex(int.Parse(Session["ConfiguarationId"].ToString()));
                            }
                        }
                    }
                }
            }

            if (Session["ActiveViewIndex"] != null)
                MultiViewConfirm.ActiveViewIndex = int.Parse(Session["ActiveViewIndex"].ToString());

            if (DetailsViewInstruction.Visible == true)
            {
                DataSet GetmyFace = new DataSet();

                GetmyFace = Face.GetFace(Session["ConnectionStringName"].ToString(), DateTime.Now.ToString(), (int)Session["OperatorId"], (int)Session["OperatorGroupId"]);

                if (GetmyFace.Tables.Count > 0)
                {
                    if (GetmyFace.Tables[0].Rows.Count > 0)
                    {
                        if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "G")
                            ((Image)DetailsViewInstruction.FindControl("Image1")).ImageUrl = "~/Images/Green Face.jpg";

                        if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "Y")
                            ((Image)DetailsViewInstruction.FindControl("Image1")).ImageUrl = "~/Images/Yellow Face.jpg";

                        if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "R")
                            ((Image)DetailsViewInstruction.FindControl("Image1")).ImageUrl = "~/Images/Red Face.jpg";
                    }

                    if (GetmyFace.Tables[0].Rows.Count == 0)
                        ((Image)DetailsViewInstruction.FindControl("Image1")).ImageUrl = "~/Images/Red Face.jpg";
                }
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                string[,] activeTabIndexValue = new string[,] {   { "0", "GetMixedPick", "0", Resources.ResMessages.Successful } ,
                                                                { "0", "GetMixedPickDetails", "0", Resources.ResMessages.ConfirmMixedPick },
                                                                { "94", "PickLocation", "2", Resources.ResMessages.ConfirmPickLocation },
                                                                { "95", "Alternate", "3", Resources.ResMessages.ConfirmAlternatePickLocation },
                                                                { "96", "Product", "4", Resources.ResMessages.ConfirmProduct },
                                                                { "97", "ConfirmBatch", "5", Resources.ResMessages.ConfirmBatch },
                                                                { "98", "Quantity", "6", Resources.ResMessages.ConfirmQuantity },
                                                                { "301", "StockTake", "7", Resources.ResMessages.ConfirmQuantity },
                                                                { "100", "StoreLocation", "8", Resources.ResMessages.ConfirmStoreLocation }
                                                            };

                Session["ActiveTabIndexValue"] = activeTabIndexValue;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/PickMixed.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                if (Session["FromURL"] == null)
                    ResetSessionValues();
                else if (Session["FromURL"].ToString() != "~/Screens/PickMixed.aspx") // Means stock take was not counted
                    ResetSessionValues();
            }

            if (Session["Type"] == null)
            {
                Session["Type"] = "GetMixedPick";
                DetailsViewInstruction.Visible = false;
            }
            else
            {
                if (DetailsViewInstruction.Rows.Count > 0)
                {
                    Session["ProductCode"] = DetailsViewInstruction.DataKey["ProductCode"].ToString();
                    Session["StoreLocation"] = DetailsViewInstruction.DataKey["StoreLocation"].ToString();
                    Session["PickLocation"] = DetailsViewInstruction.DataKey["PickLocation"].ToString();
                    Session["Quantity"] = Decimal.Parse(DetailsViewInstruction.DataKey["Quantity"].ToString());
                }

                DetailsViewInstruction_DataBind();
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    #region DetailsViewInstruction_DataBind
    protected void DetailsViewInstruction_DataBind()
    {
        DetailsViewInstruction.DataBind();

        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 297))
            DetailsViewInstruction.Rows[3].Visible = false;
        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 298))
            DetailsViewInstruction.Rows[5].Visible = false;
        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 299))
            DetailsViewInstruction.Rows[9].Visible = false;
        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 300))
            DetailsViewInstruction.Rows[10].Visible = false;
    }
    #endregion DetailsViewInstruction_DataBind

    #region GetNextPick
    protected void GetNextPick()
    {
        Transact tran = new Transact();

        int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), -1);

        if (instructionId != -1)
        {
            ResetSessionValues();
            Session["InstructionId"] = instructionId;
            Session["Message"] = Resources.ResMessages.Successful;
            Session["Error"] = false;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 94))
            {
                Session["Type"] = "PickLocation";
                Session["ActiveViewIndex"] = 2;
            }
            else if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 95))
            {
                Session["Type"] = "Alternate";
                Session["ActiveViewIndex"] = 3;
            }
            else
            {
                Session["Type"] = "Product";
                Session["ActiveViewIndex"] = 4;
            }
            DetailsViewInstruction_DataBind();
        }
        else
        {
            Master.MsgText = Resources.ResMessages.ConfirmStoreLocation;
            Session["Message"] = Resources.ResMessages.ConfirmStoreLocation;
            Session["Type"] = "StoreLocation";
            Session["ActiveViewIndex"] = 8;
            Session["ConfiguarationId"] = 100;
        }
    }
    #endregion GetNextPick

    #region ResetSessionValues
    protected void ResetSessionValues()
    {
        // Reset all the Session Values
        Session["Type"] = null;
        Session["ActiveViewIndex"] = null;
        Session["InstructionId"] = null;
        Session["ProductCode"] = null;
        Session["StoreLocation"] = null;
        Session["PickLocation"] = null;
        Session["Quantity"] = null;
        Session["Error"] = null;
        Session["ConfiguarationId"] = 0;
        //Session["Message"] = null;
    }
    #endregion ResetSessionValues
}
