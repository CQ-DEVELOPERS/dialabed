<%@ Page Language="C#"
    MasterPageFile="~/Master.master"
    AutoEventWireup="true" 
    CodeFile="CapturePickWeight.aspx.cs"
    Inherits="Screens_CapturePickWeight" 
    Title="<%$ Resources:Default, CapturePickWeightTitlte %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <table align="left" width="100%">
                <tr>
                    <td align="left">
                        <asp:Label ID="LabelPalletId" runat="server" Font-Bold="True" Text="<%$ Resources:Default, ScanPallet %>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="TextBoxPalletId" runat="server" TabIndex="1"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" style="height: 14px">
                        <asp:Button ID="CommandAccept" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            OnClick="CommandAccept_Click" TabIndex="2" Text="<%$ Resources:Default, Accept %>"></asp:Button>
                        <asp:Button ID="lbResetPalletId" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            OnClick="lbReset_Click" TabIndex="2" Text="<%$ Resources:Default, Reset %>"></asp:Button>
                        <asp:Button ID="Button1" runat="server" TabIndex="3" Font-Bold="True" ForeColor="#3D4782" Text="<%$ Resources:Default, Quit %>"
                            PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
                        <asp:Button ID="lbNewPallet" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            TabIndex="5" Text="<%$ Resources:Default, NewJob %>" Visible="False"></asp:Button></td>
                </tr>
            </table>
        </asp:View>       
        <asp:View ID="View2" runat="server">
            <table align="left">
                <tr>
                    <td align="left">
                        <asp:Label ID="lbTareContainer" runat="server" Font-Bold="True">Tare Weight: </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="TextBoxTareWeight" runat="server" TabIndex="1"></asp:TextBox>
                        <asp:Label ID="lbUnitDefault" runat="server" Text="<%$ Resources:ResLabels, MeasureUnitDefault %>" Visible="False"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label1" runat="server" Font-Bold="True">Total Weight:</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="TextBoxTotalWeight" runat="server" TabIndex="2"></asp:TextBox>
                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:ResLabels, MeasureUnitDefault %>"
                            Visible="False"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" style="height: 14px">
                        <asp:Button ID="CommandTareContainer" runat="server" Font-Bold="True" ForeColor="#3D4782" TabIndex="3" Text="<%$ Resources:Default, Accept %>" OnClick="CommandTareContainer_Click"></asp:Button>
                        <asp:Button ID="lbResetWeight" runat="server" OnClick="lbReset_Click" TabIndex="4" Text="<%$ Resources:Default, Reset %>"></asp:Button>
                        <asp:Button ID="Button3" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <table align="center" width="100%">
                <tr>
                    <td align="center" style="height: 14px">
                        <asp:Literal ID="lblFinish" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="lbNextPallet" runat="server" Font-Bold="True" ForeColor="#3D4782" TabIndex="1" Text="<%$ Resources:Default, NextPallet %>" OnClick="lbNextPallet_Click"></asp:Button></td>
                </tr>
                <tr>
                    <td align="center" style="height: 14px">
                        <asp:Button ID="Button2" runat="server" Font-Bold="True" ForeColor="#3D4782" TabIndex="2" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

