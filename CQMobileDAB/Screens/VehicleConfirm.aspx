<%@ Page Language="C#" 
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="VehicleConfirm.aspx.cs" 
    Inherits="Screens_VehicleConfirm" 
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelConfirmLoad1" runat="server" Text="<%$ Resources:Default, VehicleConfirmLoad %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxConfirmLoad" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Label ID="LabelVehicleId1" runat="server" Text="<%$ Resources:Default, VehicleConfirmID %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxVehicleID" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonConfirmLoad" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonConfirmLoad_Click"></asp:Button>
            <asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:Label ID="LabelConfirmLoad2" runat="server" Text="<%$ Resources:Default, VehicleConfirmLoad %>"></asp:Label>
            <asp:Label ID="LabelConfirmLoad2Display" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="LabelVehicleID2" runat="server" Text="<%$ Resources:Default, VehicleConfirmID %>"></asp:Label>
            <asp:Label ID="LabelVehicleID2Display" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, PalletstoScan %>"></asp:Label>
            <asp:Label ID="LabelPallets" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Default, TotalPallets2 %>"></asp:Label>
            <asp:Label ID="LabelTotal" runat="server"></asp:Label>
            <br />
            <br />
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonBarcode" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonBarcode_Click"></asp:Button>
            <asp:Button ID="btnQuit1" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
    </asp:MultiView>
</asp:Content>

