<%@ Page Language="C#"
    MasterPageFile="~/Master.master"
    AutoEventWireup="true" 
    CodeFile="ConfirmOntoVehicle.aspx.cs"
    Inherits="Screens_ConfirmOntoVehicle" 
    Title="<%$ Resources:Default, CapturePickWeightTitlte %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <table align="left" width="100%">
                <tr>
                    <td align="left">
                        <asp:Label ID="LabelPalletId" runat="server" Font-Bold="True" Text="<%$ Resources:ResLabels, LoadOrder %>" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="TextBoxLoadOrder" runat="server" TabIndex="1"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="<%$ Resources:ResLabels, VehicleId %>"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" style="height: 26px">
                        <asp:TextBox ID="TextBoxVehicleId" runat="server" TabIndex="1"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Panel ID="Panel1" runat="server">
                            <table align="left" width="100%">
                                <tr>
                                    <td align="left" style="height: 18px">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="<%$ Resources:ResLabels, ScanPallet %>"></asp:Label>
                                        <asp:Label ID="lbLineCount" runat="server" Text="<%$ Resources:ResLabels, LineCount %>"
                                            Visible="False"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:TextBox ID="TextBoxPalletId" runat="server" TabIndex="1"></asp:TextBox></td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    
                </tr>
                <tr>
                    <td align="left" style="height: 14px">
                        <asp:Button ID="CommandAccept" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            OnClick="CommandAccept_Click" TabIndex="2" Text="<%$ Resources:Default, Accept %>"></asp:Button>
                        <asp:Button ID="lbResetloadOrder" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            OnClick="lbReset_Click" TabIndex="3" Text="<%$ Resources:Default, Reset %>"></asp:Button>
                        <asp:Button ID="Button3" runat="server" Font-Bold="True" ForeColor="#3D4782" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
                        <asp:Button ID="lbNext" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            OnClick="lbNext_Click" TabIndex="5" Text="<%$ Resources:Default, Next %>" Visible="False"></asp:Button>
                        <asp:Button ID="lbCompleteAll" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            TabIndex="6" Text="<%$ Resources:Default, CompleteAll %>" Visible="False" OnClick="lbCompleteAll_Click"></asp:Button></td>
                </tr>
            </table>
        </asp:View>       
        <asp:View ID="View2" runat="server">
            <table align="center" width="100%">
                <tr>
                    <td align="center" style="height: 14px">
                        <asp:Literal ID="lblFinish" runat="server" Text="<%$ Resources:ResLabels, EmptyLabel %>"></asp:Literal></td>
                </tr>
                <tr>
                    <td align="center" style="height: 14px">
                        <asp:Button ID="lbNextPallet" runat="server" Font-Bold="True" ForeColor="#3D4782" TabIndex="1" Text="<%$ Resources:Default, NextLoad %>" OnClick="lbNextLoad_Click"></asp:Button></td>
                </tr>
                <tr>
                    <td align="center" style="height: 14px">
                        <asp:Button ID="Button1" runat="server" TabIndex="2" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

