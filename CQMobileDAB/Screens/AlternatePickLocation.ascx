<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlternatePickLocation.ascx.cs" Inherits="Screens_AlternatePickLocation" %>

<asp:Label ID="LabelConfirmStoreLocation" runat="server" Text="<%$ Resources:Default, PickLocation %>"></asp:Label>
<br />
<asp:TextBox ID="TextBoxConfirmPickLocation" runat="server" Enabled="false"></asp:TextBox>
<br />
<asp:Button ID="lbPickLocation" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbPickLocation_Click"></asp:Button>
<asp:Button ID="lbAlternate" runat="server" TabIndex="3" Text="<%$ Resources:Default, AlterLocation %>" OnClick="lbAlternate_Click"></asp:Button>
<asp:Button ID="ButtonError" runat="server" TabIndex="4" Text="<%$ Resources:Default, Error %>" OnClick="ButtonError_Click" Visible="false"></asp:Button>
<asp:Button ID="LinkButtonPrevious" runat="server" TabIndex="5" Text="<%$ Resources:Default, PreviousTransaction %>" OnClick="LinkButtonPrevious_Click"></asp:Button>
<asp:Button ID="LinkButtonNext" runat="server" TabIndex="6" Text="<%$ Resources:Default, NextTransaction %>" OnClick="LinkButtonNext_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>