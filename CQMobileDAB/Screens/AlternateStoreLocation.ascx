<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlternateStoreLocation.ascx.cs" Inherits="Screens_AlternateStoreLocation" %>

<asp:Label ID="LabelConfirmStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
<br />
<asp:TextBox ID="TextBoxConfirmStoreLocation" runat="server" Enabled="false"></asp:TextBox>
<br />
<asp:Button ID="lbStoreLocation" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbStoreLocation_Click"></asp:Button>
<asp:Button ID="lbAlternate" runat="server" TabIndex="3" Text="<%$ Resources:Default, AlterLocation %>" OnClick="lbAlternate_Click"></asp:Button>
<asp:Button ID="lbManual" runat="server" TabIndex="3" Text="<%$ Resources:Default, Override %>" OnClick="lbManual_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>