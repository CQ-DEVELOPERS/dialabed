<%@ Page Language="C#" 
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="BlindReceiving.aspx.cs" 
    Inherits="Screens_BlindReceiving" 
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            
            <asp:Label ID="LabelView1" runat="server" Text="<%$ Resources:Default, OrderDetails %>"></asp:Label>
            <br />
            <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, PurchaseOrder %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxOrderNumber" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Label ID="LabelVehicleRegistration" runat="server" Text="<%$ Resources:Default, VehicleRegistration %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxVehicleRegistration" runat="server" TabIndex="2"></asp:TextBox>
            <br />
            <asp:Label ID="LabelDeliveryNoteNumber" runat="server" Text="<%$ Resources:Default, DeliveryNoteNumber %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxDeliveryNoteNumber" runat="server" TabIndex="3"></asp:TextBox>
            <br />
            <asp:Label ID="LabelDeliveryDate" runat="server" Text="<%$ Resources:Default, DeliveryDate %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxDeliveryDate" runat="server" TabIndex="4"></asp:TextBox>
            <br />
            <asp:Label ID="LabelSealNumber" runat="server" Text="<%$ Resources:Default, SealNumber %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxSealNumber" runat="server" TabIndex="5"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonView1" runat="server" TabIndex="6" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonView1_Click"></asp:Button>
            <asp:Button ID="btnQuit" runat="server" TabIndex="7" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        
        
        <asp:View ID="View2" runat="server">
            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Default, TotalPallets %>"></asp:Label>
            <asp:Label ID="LabelTotalPallets1" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, OrderNumber %>"></asp:Label>
            <asp:Label ID="LabelOrderNumber2" runat="server" Text=""></asp:Label>
            <br />
            <br />
            <asp:Label ID="LabelView2" runat="server" Text="<%$ Resources:Default, SelectPalletType %>"></asp:Label>
            <br />
            <asp:RadioButtonList ID="RadioButtonListPalletType" runat="server" TabIndex="1">
                <asp:ListItem Selected="True" Text="<%$ Resources:Default, FullPallet %>" Value="S"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Default, MixedPallet%>" Value="SM"></asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <br />
            <asp:Button ID="LinkButtonView2" runat="server" TabIndex="2" Text="<%$ Resources:Default, Confirm2 %>" OnClick="LinkButtonView2_Click"></asp:Button>
            <asp:Button ID="Button1" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <asp:Label ID="LabelView3" runat="server" Text="<%$ Resources:Default, MixedPallet %>"></asp:Label>
            <br />
            <br />
            <asp:Label ID="LabelNumberOfProducts" runat="server" Text="<%$ Resources:Default, NumberOfProducts %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxNumberOfProducts" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonView3" runat="server" TabIndex="2" Text="<%$ Resources:Default, Confirm2 %>" OnClick="LinkButtonView3_Click"></asp:Button>
            <asp:Button ID="Button2" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <asp:Label ID="LabelView4" runat="server" Text="<%$ Resources:Default, ProductDetails %>"></asp:Label>
            <asp:Label ID="LabelProductNumber" runat="server" Text="<%$ Resources:Default, OneofOne%>"></asp:Label>
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, ProductBarcode %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxBatch" runat="server" TabIndex="2"></asp:TextBox>
            <br />
            <asp:Label ID="LabelSupplierBatch" runat="server" Text="<%$ Resources:Default, SupplierBatch %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxReferenceNumber" runat="server" TabIndex="3"></asp:TextBox>
            <br />
            <asp:Label ID="LabelExpiryDate" runat="server" Text="<%$ Resources:Default, ExpiryDate %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxExpiryDate" runat="server" TabIndex="4"></asp:TextBox>
            <br />
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, PalletQuantity %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="5"></asp:TextBox>
            <br />
            <asp:Label ID="LabelTotalPallets" runat="server" Text="<%$ Resources:Default, TotalPallets %> "></asp:Label>
            <asp:Label ID="LabelTotalPallets2" runat="server"></asp:Label>
            <br />
            <asp:Button ID="LinkButtonView4" runat="server" TabIndex="6" Text="<%$ Resources:Default, Confirm2 %>" OnClick="LinkButtonView4_Click"></asp:Button>
            <asp:Button ID="LinkButtonCompleteView4" runat="server" TabIndex="7" Text="<%$ Resources:Default, Complete %>" OnClick="LinkButtonComplete_Click"></asp:Button>
            <asp:Button ID="Button3" runat="server" TabIndex="8" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        
    </asp:MultiView>
</asp:Content>

