using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

public partial class Screens_Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet GetmyFace = new DataSet();

        GetmyFace = Face.GetFace(Session["ConnectionStringName"].ToString(), "11/06/2008 10:00:00.000", 30, 6);

        //Label1.Text = 
        if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "G")
        {
            Image1.ImageUrl = "~/Images/Green Face.jpg";
        }

        if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "Y")
        {
            Image1.ImageUrl = "~/Images/Yellow Face.jpg";
        }

        if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "R")
        {
            Image1.ImageUrl = "~/Images/Red Face.jpg";
        }

        Label2.Text = "Date : " + GetmyFace.Tables[0].Rows[0]["Date"].ToString();
        Label3.Text = "Picked Units : " + GetmyFace.Tables[0].Rows[0]["PickedUnits"].ToString();
        Label4.Text = "Site Target :" + GetmyFace.Tables[0].Rows[0]["SiteUnits"].ToString();
    }
}
