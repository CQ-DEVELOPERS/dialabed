<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PalletLink.aspx.cs" Inherits="Screens_PalletLink" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    &nbsp;

<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
        <asp:Label ID="LabelStoreLocation" runat="server" Text="<%$ Resources:Default, Location %>"></asp:Label>
        <asp:TextBox ID="TextBoxStoreLocation" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View2" runat="server">
        <asp:Label ID="LabelPalletId" runat="server" Text="<%$ Resources:Default, ScanPallet %>"></asp:Label>
        <asp:TextBox ID="TextBoxPalletId" runat="server" TabIndex="2"></asp:TextBox>
    </asp:View>
    <asp:View ID="View3" runat="server">
    </asp:View>
    <asp:View ID="View4" runat="server">
        <br />
        <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
        <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="3"></asp:TextBox></asp:View>
</asp:MultiView>
<br />
<asp:Button ID="LinkButtonAccept" runat="server" TabIndex="4" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

