<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfirmQuantity.ascx.cs" Inherits="Screens_ConfirmQuantity" %>

<asp:Label ID="LabelConfirmQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
<br />
<asp:TextBox ID="TextBoxConfirmQuantity" runat="server" TabIndex="1"></asp:TextBox>
<br />
<asp:Button ID="lbConfirmQuantity" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbConfirmQuantity_Click"></asp:Button>
<asp:Button ID="lbResetQuantity" runat="server" TabIndex="3" Text="<%$ Resources:Default, Reset %>" OnClick="lbResetQuantity_Click"></asp:Button>
<asp:Button ID="Button1" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
