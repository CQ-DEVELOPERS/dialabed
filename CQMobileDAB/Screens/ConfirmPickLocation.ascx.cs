using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ConfirmPickLocation : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbPickLocation.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            //lbResetLoc.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            //LinkButtonPrevious.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            //LinkButtonNext.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            if (Session["Type"].ToString() == "PickLocation")
                TextBoxConfirmPickLocation.Focus();
        }

        if (TextBoxConfirmPickLocation.Text == "")
            return;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      Page.SetFocus(TextBoxConfirmPickLocation);

    }
    protected void lbPickLocation_Click(object sender, EventArgs e)
    {
        try
        {
            if (TextBoxConfirmPickLocation.Text == "")
                return;

            Transact tran = new Transact();

            if (tran.ConfirmPickLocation(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], TextBoxConfirmPickLocation.Text, (int)Session["OperatorId"]) == 0)
            {
                Session["Message"] = Resources.ResMessages.Successful;
                Session["Error"] = false;
                Session["CurrentView"] = "PickLocation";
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidLocation;
                Session["Error"] = true;
            }

            TextBoxConfirmPickLocation.Text = "";
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
    protected void lbResetLoc_Click(object sender, EventArgs e)
    {
        TextBoxConfirmPickLocation.Text = "";
        TextBoxConfirmPickLocation.Focus();
    }

    protected void LinkButtonSkip_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ReferenceNumber"] != null)
            {
                Transact tran = new Transact();

                int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), (int)Session["InstructionId"]);
                //int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]);

                if (instructionId != -1)
                {
                    Session["InstructionId"] = instructionId;
                    Session["Message"] = Resources.ResMessages.Successful;
                    Session["Error"] = false;
                    Session["CurrentView"] = "PickLocation";
                    Session["ActiveViewIndex"] = 3;
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.NoOtherLines;
                    Session["Error"] = true;
                }

                TextBoxConfirmPickLocation.Text = "";
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void LinkButtonPrevious_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ReferenceNumber"] != null)
            {
                Transact tran = new Transact();

                int instructionId = tran.GetPickingMixedPrevious(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), (int)Session["InstructionId"], false);

                if (instructionId != -1)
                {
                    Session["InstructionId"] = instructionId;
                    Session["Message"] = Resources.ResMessages.Successful;
                    Session["Error"] = false;
                    Session["Type"] = "GetMixedPickDetails";
                    Session["CurrentView"] = "GetMixedPickDetails";
                    Session["ActiveViewIndex"] = 1;
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.NoOtherLines;
                    Session["Error"] = true;
                }

                TextBoxConfirmPickLocation.Text = "";
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void LinkButtonNext_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ReferenceNumber"] != null)
            {
                Transact tran = new Transact();

                int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), (int)Session["InstructionId"]);
                //int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]);

                if (instructionId != -1)
                {
                    Session["InstructionId"] = instructionId;
                    Session["Message"] = Resources.ResMessages.Successful;
                    Session["Error"] = false;
                    Session["Type"] = "GetMixedPickDetails";
                    Session["CurrentView"] = "GetMixedPickDetails";
                    Session["ActiveViewIndex"] = 1;
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.NoOtherLines;
                    Session["Error"] = true;
                }

                TextBoxConfirmPickLocation.Text = "";
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
