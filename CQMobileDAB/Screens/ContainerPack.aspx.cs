using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ContainerMove : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/ContainerPack.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            Container cont = new Container();
            
            switch (MultiView1.ActiveViewIndex)
            {
                case 0: //ReferenceNumber
                    DetailsViewDocument.DataBind();

                    if (DetailsViewDocument.PageCount > 0)
                        Session["ContainerHeaderId"] = int.Parse(DetailsViewDocument.DataKey["ContainerHeaderId"].ToString());

                    if (Session["ContainerHeaderId"] != null)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
                case 1: //Location
                    DetailsViewDocument.DataBind();
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    break;
                case 2: //Product
                    DetailsViewDocument.DataBind();

                    if (DetailsViewDocument.PageCount == 1)
                    {
                        MultiView1.ActiveViewIndex++;
                    }
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    break;
                case 3: //Batch
                    DetailsViewDocument.DataBind();
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    break;
                case 4: //Quantity
                    Decimal quantity = -1;

                    Decimal.TryParse(TextBoxQuantity.Text, out quantity);

                    if (cont.Pack(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], (int)Session["ContainerHeaderId"], TextBoxPickLocation.Text, TextBoxProduct.Text, TextBoxBatch.Text, quantity))
                    {
                        MultiView1.ActiveViewIndex = 2;
                        TextBoxProduct.Text = "";
                        TextBoxBatch.Text = "";
                        TextBoxQuantity.Text = "";
                        DetailsViewDocument.DataBind();
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidQuantity;
                    }
                    break;
            }
        }
        catch { }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
    }

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Response.Redirect("~/Screens/ContainerPack.aspx");
                break;
            case 1: //Location
                TextBoxPickLocation.Text = "";
                MultiView1.ActiveViewIndex--;
                break;
            case 2: //Product
                TextBoxProduct.Text = "";
                MultiView1.ActiveViewIndex--;
                break;
            case 3: //Batch
                TextBoxBatch.Text = "";
                MultiView1.ActiveViewIndex--;
                break;
            case 4: //Quantity
                TextBoxQuantity.Text = "";
                MultiView1.ActiveViewIndex--;
                break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    #region LinkButtonSkip_Click
    protected void LinkButtonSkip_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 5:

                Reset();
                break;

            default:
                MultiView1.ActiveViewIndex++;
                break;
        }
    }
    #endregion LinkButtonSkip_Click

    #region LinkButtonFinish_Click
    protected void LinkButtonFinish_Click(object sender, EventArgs e)
    {
        try
        {
            Reset();
        }
        catch { }
    }
    #endregion LinkButtonFinish_Click

    protected void Reset()
    {
        try
        {
            MultiView1.ActiveViewIndex = 0;
            Session["ContainerHeaderId"] = null;
            Master.MsgText = Resources.ResMessages.Successful;
            TextBoxBarcode.Text = "";
            TextBoxPickLocation.Text = "";
        }
        catch { }
    }
}
