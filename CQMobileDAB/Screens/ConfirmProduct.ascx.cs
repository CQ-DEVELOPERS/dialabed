using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ConfirmProduct : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbConfirmProduct.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            //lbResetProduct.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            if (Session["Type"].ToString() == "Product")
                TextBoxConfirmProduct.Focus();


        }
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        Page.SetFocus(TextBoxConfirmProduct);
    }
    protected void lbConfirmProduct_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            int instructionId = 0;

            instructionId = tran.ConfirmProduct(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], TextBoxConfirmProduct.Text);

            if (instructionId >= 0)
            {
                Session["Message"] = Resources.ResMessages.Successful;
                Session["Error"] = false;
                Session["CurrentView"] = "Product";
                Session["StorageUnitId"] = tran.GetStorageUnit(Session["ConnectionStringName"].ToString(), TextBoxConfirmProduct.Text, 0, (int)Session["WarehouseId"]);

                if (instructionId != 0)
                    Session["InstructionId"] = instructionId;
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidProduct;
                Session["Error"] = true;
            }

            TextBoxConfirmProduct.Text = "";
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
            Session["Error"] = true;
        }

        //if (Session["FromURL"] != null)
        //    Response.Redirect(Session["FromURL"].ToString());
    }
    protected void lbResetProduct_Click(object sender, EventArgs e)
    {
        TextBoxConfirmProduct.Text = "";
        TextBoxConfirmProduct.Focus();
    }
}
