<%@ Page Language="C#" 
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="YardExit.aspx.cs" 
    Inherits="Screens_YardExit" 
    Title="Container Off Dock" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td align="center">
                <asp:Label ID="LabelContainer" runat="server" Text="Container No:"></asp:Label>
                <asp:TextBox ID="TextBoxBarcode" runat="server" Text="" TabIndex="1"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="ButtonOut" runat="server" Text="CONTAINER OUT" OnClick="ButtonOut_Click" TabIndex="2" />
                <asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
</asp:Content>
