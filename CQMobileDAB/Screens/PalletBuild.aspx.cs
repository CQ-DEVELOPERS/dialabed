using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PalletBuild : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/PalletBuild.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      switch (MultiView1.ActiveViewIndex)
      {
        case 0:
          Page.SetFocus(TextBoxBarcode);
          break;
        case 1:
          Page.SetFocus(TextBoxReferenceNumber);
          break;
      };

    }

    protected void ButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            PalletBuild build = new PalletBuild();

            if(build.ConfirmPalletId(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], TextBoxBarcode.Text) == 0)
            {
                MultiView1.ActiveViewIndex++;
                Master.MsgText = Resources.ResMessages.Successful;
            }
            else
            {
                TextBoxBarcode.Text = "";
                Master.MsgText = Resources.ResMessages.InvalidBarcode;
            }
        }
        catch { }
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        try
        {
            PalletBuild build = new PalletBuild();

            if(build.Add(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], TextBoxBarcode.Text, TextBoxReferenceNumber.Text))
                {
                    TextBoxReferenceNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.Successful;
                }
                else
                {
                    TextBoxReferenceNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidBarcode;
                }
        }
        catch { }
    }

    protected void ButtonRemove_Click(object sender, EventArgs e)
    {
        try
        {
            PalletBuild build = new PalletBuild();

            if (build.Remove(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], TextBoxBarcode.Text, TextBoxReferenceNumber.Text))
            {
                TextBoxReferenceNumber.Text = "";
                Master.MsgText = Resources.ResMessages.Successful;
            }
            else
            {
                TextBoxReferenceNumber.Text = "";
                Master.MsgText = Resources.ResMessages.InvalidBarcode;
            }
        }
        catch { }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
    }

    #region ButtonFinish_Click
    protected void ButtonFinish_Click(object sender, EventArgs e)
    {
        try
        {
            if (TextBoxReferenceNumber.Text != "")
                ButtonAdd_Click(ButtonAdd2, EventArgs.Empty);
            Reset();
        }
        catch { }
    }
    #endregion ButtonFinish_Click

    protected void Reset()
    {
        try
        {
            MultiView1.ActiveViewIndex = 0;
            Master.MsgText = Resources.ResMessages.Successful;
            TextBoxBarcode.Text = "";
            TextBoxReferenceNumber.Text = "";
        }
        catch { }
    }
}
