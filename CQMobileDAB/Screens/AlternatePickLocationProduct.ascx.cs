using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_AlternatePickLocationProduct : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbAlternate.Attributes.Add("onclick", "this.disabled= 'disabled' ");
        }
    }
    protected void lbPickLocation_Click(object sender, EventArgs e)
    {
        Session["Error"] = false;
        Session["CurrentView"] = "Alternate";
    }
    protected void lbAlternate_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            if (tran.LocationErrorPick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
            {
                Session["Type"] = "LocationError";
                Session["Error"] = false;
                Session["CurrentView"] = "LocationError";
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidLocation;
                Session["Error"] = true;
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
