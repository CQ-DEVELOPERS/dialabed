using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_GetFullPick : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lbAccept.Focus();
        }
    }
    protected void lbAccept_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();
            int instructionId = tran.GetPickingFull(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]);

            if (instructionId == -99)
            {
                Session["Message"] = Resources.ResMessages.NoStockPick;
                Session["Error"] = true;
            }
            else if (instructionId == -88)
            {
                Session["Message"] = Resources.ResMessages.NotAllowedArea;
                Session["Error"] = true;
            }
            else if (instructionId != -1)
            {
                Session["InstructionId"] = instructionId;
                Session["Message"] = Resources.ResMessages.Successful;
                Session["Error"] = false;
                Session["CurrentView"] = "GetFullPick";
            }
            else
            {
                Session["Message"] = Resources.ResMessages.NoJobs;
                Session["Error"] = true;
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
