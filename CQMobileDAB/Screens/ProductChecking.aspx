<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ProductChecking.aspx.cs" Inherits="Screens_ProductChecking" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<%@ Register Src="SerialNumbers.ascx" TagName="SerialNumbers" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>"></asp:Label>
            <asp:TextBox ID="TextBoxReferenceNumber" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:DetailsView ID="DetailsViewJob" runat="server" DataKeyNames="JobId,Finished" DataSourceID="ObjectDataSourceJob" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
                    <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" />
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, OrderedQuantity %>" />
                    <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" />
                    <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceJob" runat="server" TypeName="ProductCheck"
                SelectMethod="ConfirmPallet">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
                    <asp:ControlParameter Name="ReferenceNumber" ControlID="TextBoxReferenceNumber" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:CheckBox ID="cbScanMode" runat="server" Text="<%$ Resources:Default, ScanMode%>" Checked="false" TabIndex="20"/>
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <asp:DetailsView ID="DetailsViewProduct" runat="server" DataKeyNames="StorageUnitId" DataSourceID="ObjectDataSourceProduct" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="ErrorMsg" HeaderText="" ItemStyle-ForeColor="Red" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="ProductCheck"
                SelectMethod="ConfirmProduct">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
                    <asp:ControlParameter Name="Barcode" ControlID="TextBoxBarcode" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch %>"></asp:Label>
            <asp:TextBox ID="TextBoxBatch" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <asp:DetailsView ID="DetailsViewBatch" runat="server" DataKeyNames="StorageUnitBatchId" DataSourceID="ObjectDataSourceBatch" AutoGenerateRows="False" PageIndex="0" AllowPaging="true" OnPageIndexChanged="DetailsViewBatch_PageIndexChanged">
                <Fields>
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ExpiryDate" HeaderText="<%$ Resources:Default, ExpiryDate %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="ErrorMsg" HeaderText="" ItemStyle-ForeColor="Red" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="ProductCheck"
                SelectMethod="ConfirmBatch">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" DefaultValue="-1" />
                    <asp:ControlParameter Name="Batch" ControlID="TextBoxBatch" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <asp:DetailsView ID="DetailsViewQuantity" runat="server" DataKeyNames="StorageUnitBatchId" DataSourceID="ObjectDataSourceQuantity" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ExpiryDate" HeaderText="<%$ Resources:Default, ExpiryDate %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceQuantity" runat="server" TypeName="ProductCheck"
                SelectMethod="ConfirmQuantity">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" DefaultValue="-1" />
                    <asp:ControlParameter Name="quantity" ControlID="TextBoxQuantity" Type="String" />
                    <asp:ControlParameter Name="scanMode" ControlID="cbScanMode" Type="Boolean" />
                    <asp:ControlParameter Name="Barcode" ControlID="TextBoxBarcode" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <uc1:SerialNumbers ID="SerialNumbers1" runat="server" />
            <asp:Label ID="LabelWeight" runat="server" Text="<%$ Resources:Default, Weight %>"></asp:Label>
            <asp:TextBox ID="TextBoxWeight" runat="server" TabIndex="1" ></asp:TextBox>
            <asp:Button ID="LinkButtonWeighBox" runat="server" TabIndex="2" Text="Save Weight" OnClick="LinkButtonWeighBox_Click"></asp:Button>
            <%--<asp:Label ID="Label1" runat="server" Text="Quantity:"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server" TabIndex="1"></asp:TextBox>--%>
        </asp:View>
        <asp:View ID="View6" runat="server">
            <br />
            <asp:Label ID="LabelOldReferenceNumber1" runat="server" Text="<%$ Resources:Default, OldNumber %>"></asp:Label>
            <asp:Label ID="LabelOldReferenceNumber2" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="LabelNewReferenceNumber" runat="server" Text="<%$ Resources:Default, NewNumber %>"></asp:Label>
            <asp:TextBox ID="TextBoxNewReferenceNumber" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View7" runat="server">
            <br />
            <asp:Label ID="LabelContainerType" runat="server" Text="<%$ Resources:Default, ContainerType %>"></asp:Label><br />
            <asp:RadioButtonList ID="rblContainerType" runat="server" DataSourceId="ObjectDataSourceContainerType"
             DataTextField="Description" DataValueField="ContainerTypeId" RepeatLayout="Flow">
            </asp:RadioButtonList>
            <asp:ObjectDataSource ID="ObjectDataSourceContainerType" runat="server" TypeName="ProductCheck"
                SelectMethod="SearchContainerTypes">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="9" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="10" Text="<%$ Resources:Default, Back %>" OnClick="LinkButtonBack_Click" Visible="false"></asp:Button>
    <asp:Button ID="LinkButtonNext" runat="server" TabIndex="11" Text="<%$ Resources:Default, Next %>" OnClick="LinkButtonNext_Click" Visible="false"></asp:Button>
    <asp:Button ID="LinkButtonNewBox" runat="server" TabIndex="12" Text="<%$ Resources:Default, NewBox %>" OnClick="LinkButtonNewBox_Click" Visible="false"></asp:Button>
    <asp:Button ID="LinkButtonChangeBox" runat="server" Text="<%$ Resources:Default, ChangeBox %>" OnClick="LinkButtonChangeBox_Click" Visible="false"></asp:Button>
    <asp:Button ID="LinkButtonFinished" runat="server" TabIndex="13" Text="<%$ Resources:Default, Finish %>" OnClick="LinkButtonFinished_Click" Visible="false"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="14" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

