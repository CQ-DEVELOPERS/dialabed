<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ContainerUnpack.aspx.cs" Inherits="Screens_ContainerMove" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsViewDocument" runat="server" 
        DataKeyNames="ContainerHeaderId,ProductCode,Batch,Quantity" DataSourceID="ObjectDataSourceDocument"
        AutoGenerateRows="False" PageIndex="0" AllowPaging="true" 
        OnPageIndexChanged="DetailsViewDocument_PageIndexChanged" OnDataBound="DetailsViewDocument_OnDataBound">
        <Fields>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
            <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="Container"
        SelectMethod="GetItems">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
            <asp:ControlParameter Name="barcode" ControlID="TextBoxBarcode" DbType="String" />
            <asp:ControlParameter Name="ProductBarcode" ControlID="TextBoxProduct" DbType="String" />
            <asp:ControlParameter Name="Batch" ControlID="TextBoxBatch" DbType="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, ProductCode %>"></asp:Label>
            <asp:TextBox ID="TextBoxProduct" runat="server" TabIndex="2"></asp:TextBox>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <br />
            <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch %>"></asp:Label>
            <asp:TextBox ID="TextBoxBatch" runat="server" TabIndex="3"></asp:TextBox>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <br />
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="4"></asp:TextBox>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <br />
            <asp:Label ID="LabelStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
            <asp:TextBox ID="TextBoxStoreLocation" runat="server" TabIndex="5"></asp:TextBox>
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="9" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="10" Text="<%$ Resources:Default, Back %>" OnClick="LinkButtonBack_Click"></asp:Button>
    <asp:Button ID="LinkButtonSkip" runat="server" TabIndex="11" Text="<%$ Resources:Default, Skip %>" OnClick="LinkButtonSkip_Click" Visible="false"></asp:Button>
    <asp:Button ID="LinkButtonFinish" runat="server" TabIndex="12" Text="<%$ Resources:Default, Finish %>" OnClick="LinkButtonFinish_Click" Visible="false"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="13" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

