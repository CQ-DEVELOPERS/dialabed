<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="StockWriteOff.aspx.cs" Inherits="Screens_StockWriteOff" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
        <asp:Label ID="LabelPickLocation" runat="server" Text="<%$ Resources:Default, PickLocation %>"></asp:Label>
        <asp:TextBox ID="TextBoxPickLocation" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View2" runat="server">
        <asp:Label ID="LabelPalletId" runat="server" Text="<%$ Resources:Default, ScanPallet %>"></asp:Label>
        <asp:TextBox ID="TextBoxPalletId" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View3" runat="server">
        <asp:DetailsView ID="DetailsViewStock" runat="server" DataKeyNames="StorageUnitBatchId" DataSourceID="ObjectDataSourceStock" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
            <Fields>
                <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default, ActualQuantity %>" />
                <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" />
                <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default, ReservedQuantity %>" />
            </Fields>
        </asp:DetailsView>
        <asp:ObjectDataSource ID="ObjectDataSourceStock" runat="server" TypeName="Transact"
            SelectMethod="ValidStockLink">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                <asp:ControlParameter Name="location" Type="String" ControlID="TextBoxPickLocation" />
                <asp:ControlParameter Name="product" Type="String" ControlID="TextBoxPalletId" />
            </SelectParameters>
        </asp:ObjectDataSource>
        
        <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity2 %>"></asp:Label>
        <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="1"></asp:TextBox>
    </asp:View>
    <asp:View ID="View4" runat="server">
        <asp:GridView ID="GridViewReason"
            runat="server"
            AutoGenerateColumns="False"
            AutoGenerateSelectButton="True"
            DataSourceID="ObjectDataSourceReason"
            DataKeyNames="ReasonId"
            OnSelectedIndexChanged="GridViewReason_OnSelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="Reason" HeaderText="<%$ Resources:Default, Reason %>" />
            </Columns>
            <EmptyDataTemplate>
                <h5>No rows</h5>
            </EmptyDataTemplate>
        </asp:GridView>
        
        <asp:ObjectDataSource id="ObjectDataSourceReason" runat="server" TypeName="Reason" SelectMethod="GetReasonsByType">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="MOW" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:View>
    <asp:View ID="View5" runat="server">
        <asp:DetailsView ID="DetailsViewInstruction" runat="server" DataSourceID="ObjectDataSourceInstruction" AutoGenerateRows="False">
            <Fields>
                <asp:BoundField DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>" />
                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
            </Fields>
        </asp:DetailsView>
        <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="Transact"
            SelectMethod="GetInstructionDetails">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                <asp:SessionParameter Name="instructionId" SessionField="InstructionId" Type="int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:View>
</asp:MultiView>
<br />
<asp:Button ID="LinkButtonAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

