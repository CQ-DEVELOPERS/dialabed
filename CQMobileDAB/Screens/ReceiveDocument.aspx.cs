using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ReceiveDocument : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ReceiptId"] = null;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/ReceiveDocument.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      switch (MultiView1.ActiveViewIndex)
      {
        case 0:
          Page.SetFocus(TextBoxOrderNumber);
          break;
        case 1:
          Page.SetFocus(TextBoxVehicleRegistration);
          break;
        case 2:
          Page.SetFocus(TextBoxDeliveryNoteNumber);
          break;
        case 3:
          Page.SetFocus(TextBoxYear);
          break;
        case 4:
          Page.SetFocus(TextBoxSealNumber);
          break;
      };

    }
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        Receiving rcv = new Receiving();
        int receiptId = 0;

        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                receiptId = rcv.ConfirmOrder(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxOrderNumber.Text);

                if (receiptId > 0)
                {
                    Session["ReceiptId"] = receiptId;
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                }
                else
                {
                    TextBoxOrderNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidOrderNumber;
                }

                break;
            case 1:
                receiptId = rcv.ReceivingDocumentUpdateVehicleRegistration(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], TextBoxVehicleRegistration.Text);

                if (receiptId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    TextBoxVehicleRegistration.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidVehicleID;
                }
                break;
            case 2:
                receiptId = rcv.ReceivingDocumentUpdateDeliveryNoteNumber(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], TextBoxDeliveryNoteNumber.Text);

                if (receiptId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    TextBoxDeliveryNoteNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
            case 3:
                String deliveryDate = TextBoxYear.Text + "/" + TextBoxMonth.Text + "/" + TextBoxDay.Text + " " + TextBoxHours.Text + ":" + TextBoxMinutes.Text;
                DateTime result;

                DateTime.TryParse(deliveryDate, out result);

                receiptId = rcv.ReceivingDocumentUpdateDeliveryDate(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], deliveryDate);

                if (receiptId > 0)
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                    DetailsViewDocument.DataBind();
                }
                else
                {
                    //TextBoxDeliveryDate.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
            case 4:
                receiptId = rcv.ReceivingDocumentUpdateSealNumber(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], TextBoxSealNumber.Text);

                if (receiptId > 0)
                {
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 435))
                    {
                        Session["QuestionaireId"] = 1;
                        Session["QuestionaireType"] = "Delivery";
                        Session["Sequence"] = 0;
                        Session["ReturnURL"] = "~/Screens/ReceiveByProduct.aspx";
                        Response.Redirect("~/Screens/Question.aspx");
                    }
                    else
                        Response.Redirect("~/Screens/ReceiveByProduct.aspx");
                }
                else
                {
                    TextBoxSealNumber.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }
                break;
        }
    }

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    protected void LinkButtonSkip_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 4:
                Response.Redirect("~/Screens/ReceiveByProduct.aspx");
                break;
            default:
                MultiView1.ActiveViewIndex++;
                break;
        }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
        LinkButtonSkip.Visible = true;

        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                LinkButtonSkip.Visible = false;
                break;
            case 1:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 147))
                    MultiView1.ActiveViewIndex++;
                break;
            case 2:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 148))
                    MultiView1.ActiveViewIndex++;
                break;
            case 3:
                TextBoxYear.Text = DateTime.Now.Year.ToString();
                TextBoxMonth.Text = DateTime.Now.Month.ToString();
                TextBoxDay.Text = DateTime.Now.Day.ToString();
                TextBoxHours.Text = DateTime.Now.Hour.ToString();
                TextBoxMinutes.Text = DateTime.Now.Minute.ToString();
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 149))
                    MultiView1.ActiveViewIndex++;
                break;
            case 4:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 150))
                    Response.Redirect("~/Screens/ReceiveByProduct.aspx");
                break;
        }
    }
}
