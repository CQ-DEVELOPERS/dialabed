<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfirmProduct.ascx.cs" Inherits="Screens_ConfirmProduct" %>

<asp:Label ID="LabelConfirmProduct" runat="server" Text="<%$ Resources:Default, Product2%> "></asp:Label>
<br />
<asp:TextBox ID="TextBoxConfirmProduct" runat="server" TabIndex="1"></asp:TextBox>
<br />
<asp:Button ID="lbConfirmProduct" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbConfirmProduct_Click"></asp:Button>
<asp:Button ID="lbResetProduct" runat="server" TabIndex="3" Text="<%$ Resources:Default, Reset %>" OnClick="lbResetProduct_Click"></asp:Button>
<asp:Button ID="Button1" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>