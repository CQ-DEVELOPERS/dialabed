<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CheckList.aspx.cs" Inherits="Screens_CheckList" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="lblNewPallet" runat="server" Text="<%$ Resources:ResLabels, Barcode %>"></asp:Label>
    <br />
    <asp:TextBox ID="TextBoxReferenceNumber" runat="server" TabIndex="1"></asp:TextBox>
    <br />
    <asp:GridView ID="GridView1" TabIndex="3" runat="server" DataSourceID="ObjectDataSource1" DataKeyNames="JobId,StorageUnitBatchId,ConfirmedQuantity"
        AutoGenerateColumns="false" AutoGenerateSelectButton="true" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="GridView1_RowDataBound"
        AllowPaging="true" PageSize="4">
        <Columns>
            <asp:TemplateField HeaderText="<%$ Resources:Default, Product %>">
                <ItemTemplate>
                    <asp:Label ID="LabelProductCode" runat="server" Text='<%# Bind("ProductCode") %>' SkinId="Normal"></asp:Label>
                    <asp:Label ID="LabelProduct" runat="server" Text='<%# Bind("Product") %>' SkinId="Normal"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" />--%>
            <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>" />
        </Columns>
    </asp:GridView>
    
    <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>" Visible="false"></asp:Label>
    <br />
    <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="4" Visible="false"></asp:TextBox>
    <br />
    <asp:Button ID="ButtonAcceptRef" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="ButtonAcceptRef_Click"></asp:Button>
    <asp:Button ID="ButtonAcceptQty" runat="server" TabIndex="5" Text="<%$ Resources:Default, Accept %>" OnClick="ButtonAcceptQty_Click" Visible="false"></asp:Button>
    <asp:Button ID="ButtonFinished" runat="server" TabIndex="6" Text="<%$ Resources:Default, Finish %>" OnClick="ButtonFinished_Click" Visible="false"></asp:Button>
    <asp:Button ID="ButtonQuit" runat="server" TabIndex="7" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
    
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="CheckPallet" SelectMethod="GetCheckList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="Int32" DefaultValue="-1" />
            <asp:ControlParameter Name="referenceNumber" ControlID="TextBoxReferenceNumber" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

