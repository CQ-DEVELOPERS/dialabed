<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PickFull.aspx.cs" Inherits="Screens_PickFull" Title="Untitled Page" %>

<%@ Register Src="AlternatePickLocation.ascx" TagName="AlternatePickLocation" TagPrefix="uc7" %>
<%@ Register Src="AlternatePickLocationProduct.ascx" TagName="AlternatePickLocationProduct" TagPrefix="uc8" %>
<%@ Register Src="SendToPickFace.ascx" TagName="SendToPickFace" TagPrefix="uc9" %>
<%@ Register Src="GetFullPick.ascx" TagName="GetFullPick" TagPrefix="uc5" %>
<%@ Register Src="ConfirmStoreLocation.ascx" TagName="ConfirmStoreLocation" TagPrefix="uc4" %>
<%@ Register Src="ConfirmPickLocation.ascx" TagName="ConfirmPickLocation" TagPrefix="uc1" %>
<%@ Register Src="ConfirmProduct.ascx" TagName="ConfirmProduct" TagPrefix="uc2" %>
<%@ Register Src="ConfirmQuantity.ascx" TagName="ConfirmQuantity" TagPrefix="uc3" %>
<%@ Register Src="ConfirmBatch.ascx" TagName="ConfirmBatch" TagPrefix="uc8" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsViewInstruction" runat="server" DataKeyNames="ProductCode,Quantity,PickLocation,StoreLocation" OnDataBound="DetailsViewInstruction_DataBound" DataSourceID="ObjectDataSourceInstruction" AutoGenerateRows="False">
        <Fields>
            <%--<asp:TemplateField HeaderText="<%$ Resources:Default, NumberOfLines %>">
                <ItemTemplate>
                    <asp:Label ID="labelCurrentLine" runat="server" Text='<%# Bind("CurrentLine") %>'></asp:Label>
                    <asp:Label ID="labelOf" runat="server" Text="<%$ Resources:Default, Of %>"></asp:Label>
                    <asp:Label ID="labelTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>" />
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
            <asp:BoundField DataField="SOH" HeaderText="<%$ Resources:Default, SOH %>" />
            <asp:BoundField DataField="DisplayQuantity" HeaderText="<%$ Resources:Default, Quantity %>" />
            <asp:BoundField DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>" />
            <asp:BoundField DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>" />
            <asp:TemplateField HeaderText="<%$ Resources:Default, TargetFace %>" Visible="false">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="Transact"
        SelectMethod="GetInstructionDetails">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="instructionId" SessionField="InstructionId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <uc5:GetFullPick ID="GetFullPick1" runat="server" />
        </asp:View>
        <asp:View ID="View2" runat="server">
            <uc1:ConfirmPickLocation ID="ConfirmPickLocation1" runat="server" />
        </asp:View>
        <asp:View ID="View3" runat="server">
            <uc7:AlternatePickLocation ID="AlternatePickLocation1" runat="server" />
        </asp:View>
        <asp:View ID="View4" runat="server">
            <uc2:ConfirmProduct ID="ConfirmProduct1" runat="server" />
            <uc8:AlternatePickLocationProduct ID="AlternatePickLocationProduct" runat="server" />
        </asp:View>
         <asp:View ID="View7" runat="server">
            <uc8:ConfirmBatch ID="ConfirmBatch1" runat="server" />
        </asp:View>
        <asp:View ID="View5" runat="server">
            <uc3:ConfirmQuantity ID="ConfirmQuantity1" runat="server" />
            <uc9:SendToPickFace ID="SendToPickFace" runat="server" />
        </asp:View>
        <asp:View ID="View6" runat="server">
            <uc4:ConfirmStoreLocation ID="ConfirmStoreLocation1" runat="server" />
        </asp:View>
    </asp:MultiView>
</asp:Content>

