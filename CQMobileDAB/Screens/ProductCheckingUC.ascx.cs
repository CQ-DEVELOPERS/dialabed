﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ProductCheckingUC : System.Web.UI.UserControl
{
    public string scanMessage { get; set; }
    public event EventHandler ScanMessage;
    public event EventHandler ScanFinish;
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Theme
    //public override String StyleSheetTheme
    //{
    //    get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    //}

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }
    #endregion Theme

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ReceiptLineId"] = null;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/ProductChecking.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Page.SetFocus(TextBoxQuantity);
                break;
            case 1:
                break;
            case 2:
                Page.SetFocus(TextBoxNewReferenceNumber);
                break;
            case 3:
                LinkButtonNewBox.Visible = false;

                Page.SetFocus(rblContainerType);
                break;
        }
    }

    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();
            scanMessage = Resources.ResMessages.Successful;
            if (ScanMessage != null)
                ScanMessage(this, EventArgs.Empty);
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    confirmQuantity();
                    break;
                case 1:
                    break;
                case 2:
                    int jobId = 0;

                    jobId = chk.NewBox(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["OperatorId"], TextBoxNewReferenceNumber.Text);

                    if (jobId > 0)
                    {
                        Session["ReferenceNumber"] = TextBoxNewReferenceNumber.Text;
                        TextBoxNewReferenceNumber.Text = "";
                        LabelOldReferenceNumber2.Text = "";

                        MultiView1.ActiveViewIndex = 0;

                        break;
                    }
                    else
                    {
                        scanMessage = Resources.ResMessages.InvalidBarcode;
                        if (ScanMessage != null)
                            ScanMessage(this, EventArgs.Empty);
                        TextBoxNewReferenceNumber.Text = "";
                    }
                    break;
                case 3:
                    chk.SetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]), int.Parse(rblContainerType.SelectedValue));

                    LinkButtonNewBox.Visible = true;

                    if (ScanFinish != null)
                        ScanFinish(this, EventArgs.Empty);
                    break;
            }
        }
        catch { }
    }

    private void confirmQuantity()
    {
        try
        {
            Decimal quantity = 0;
            Decimal confirmedQuantity = 0;
            Decimal previousQuantity = 0;

            CheckPallet cp = new CheckPallet();

            if (!Decimal.TryParse(TextBoxQuantity.Text, out quantity))
            {
                TextBoxQuantity.Text = "";
                scanMessage = Resources.ResMessages.Failed;
                if (ScanMessage != null)
                    ScanMessage(this, EventArgs.Empty);
                return;
            }

            if (!Decimal.TryParse(Session["productConfirmedQuantity"].ToString(), out confirmedQuantity))
            {
                scanMessage = Resources.ResMessages.QuantityMismatch;
                if (ScanMessage != null)
                    ScanMessage(this, EventArgs.Empty);
                return;
            }

            if (quantity != confirmedQuantity)
            {
                if (Session["PreviousQuantity"] == null)
                {
                    Session["PreviousQuantity"] = quantity;
                    TextBoxQuantity.Text = "";
                    scanMessage = Resources.ResMessages.QuantityReEnter;
                    if (ScanMessage != null)
                        ScanMessage(this, EventArgs.Empty);
                    return;
                }
                else
                {
                    if (!Decimal.TryParse(Session["PreviousQuantity"].ToString(), out previousQuantity))
                    {
                        scanMessage = Resources.ResMessages.QuantityMismatch;
                        if (ScanMessage != null)
                            ScanMessage(this, EventArgs.Empty);
                        return;
                    }
                }
            }

            if (cp.UpdateCheckListQuantity(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], (int)Session["JobId"], -1, quantity, (int)Session["StorageUnitId"]))
            {
                if (quantity != confirmedQuantity)
                {
                    //scanMessage = Resources.ResMessages.SkuCheckQuantityQA;
                    //    if (ScanMessage != null)
                    //        ScanMessage(this, EventArgs.Empty);
                    if (ScanFinish != null)
                        ScanFinish(this, EventArgs.Empty);
                }
                    
                else
                {
                    if (ScanFinish != null)
                        ScanFinish(this, EventArgs.Empty);
                }
            }
            else
            {
                scanMessage = Resources.ResMessages.Failed;
                if (ScanMessage != null)
                    ScanMessage(this, EventArgs.Empty);
            }
                
                //Master.MsgText = (cp.UpdateCheckList(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"], (int)Session["StorageUnitBatchId"], quantity).ToString());

            Session["PreviousQuantity"] = null;

            TextBoxQuantity.Text = "";
        }
        catch (Exception ex)
        {
            scanMessage = ex.Message;
            if (ScanMessage != null)
                ScanMessage(this, EventArgs.Empty);
        }
    }

    #region GetProductDetails
    public void GetProductDetails(string barCode, decimal confirmedQuantity)
    {
        try
        {
            Session["productConfirmedQuantity"] = confirmedQuantity;
            ProductCheck PC = new ProductCheck();
            DataSet ds = new DataSet();
            ds = PC.ConfirmPallet(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], Session["JobReferenceNumber"].ToString());
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                Session["JobId"] = int.Parse(ds.Tables[0].Rows[0]["JobId"].ToString());

                Session["ProductBarcode"] = barCode;
                DetailsViewProduct.DataBind();
                if (DetailsViewProduct.Rows.Count > 0)
                {
                    Session["StorageUnitId"] = int.Parse(DetailsViewProduct.DataKey["StorageUnitId"].ToString());
                    MultiView1.ActiveViewIndex = 0;
                    scanMessage = Resources.ResMessages.Successful;
                    if (ScanMessage != null)
                        ScanMessage(this, EventArgs.Empty);
                }
                else
                {
                    scanMessage = Resources.ResMessages.InvalidProduct;
                    if (ScanMessage != null)
                        ScanMessage(this, EventArgs.Empty);
                }

            }
            else
            {
                scanMessage = "No records were found";
                if (ScanMessage != null)
                    ScanMessage(this, EventArgs.Empty);
            }
        }
        catch (Exception ex)
        {
            if (ex.InnerException == null)
                scanMessage = ex.Message;
            else
                scanMessage = ex.InnerException.Message;
            if (ScanMessage != null)
                ScanMessage(this, EventArgs.Empty);
        }
    }
    #endregion


    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
    }

    protected void LinkButtonNewBox_Click(object sender, EventArgs e)
    {
        ProductCheck chk = new ProductCheck();

        LinkButtonAccept.Visible = true;

        if (Session["ReferenceNumber"] != null)
            LabelOldReferenceNumber2.Text = Session["ReferenceNumber"].ToString();

        MultiView1.ActiveViewIndex = 3;
    }

    protected void NextOrFinish()
    {
        Reset();
        MultiView1.ActiveViewIndex++;
        LinkButtonAccept.Visible = false;
    }

    protected void Reset()
    {
        scanMessage = Resources.ResMessages.Successful;
        if (ScanMessage != null)
            ScanMessage(this, EventArgs.Empty);
        TextBoxQuantity.Text = "";
        Session["Mismatch"] = null;
    }
    protected void LinkButtonChangeBox_Click(object sender, EventArgs e)
    {
        ProductCheck chk = new ProductCheck();

        int containerTypeId = chk.GetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]));
        if (containerTypeId != 0)
        {
            rblContainerType.SelectedValue = containerTypeId.ToString();
        }

        MultiView1.ActiveViewIndex = 4;
    }
    protected void btnQuit_Click(object sender, EventArgs e)
    {
        if (ScanFinish != null)
            ScanFinish(this, EventArgs.Empty);
    }
}