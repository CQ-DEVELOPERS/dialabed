using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_Packaging : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            
        }
    }

    protected void LinkButtonAdd_Click(object sender, EventArgs e)
    {

        Receiving r = new Receiving();
        string StorageUnitBatchId = "";
        Decimal ActualQuantity = 0;
        int PackageLineID = -1;

        if (TextboxQuantity.Text.ToString() == "")
        {
            Master.MsgText = "Please Enter a Quantity";
        }
        else
        {

            StorageUnitBatchId = RadioButtonPackaging.SelectedItem.Value.ToString();
            ActualQuantity = Decimal.Parse(TextboxQuantity.Text.ToString());


            PackageLineID = r.InsertPackageLines(Session["ConnectionStringName"].ToString(),
                                int.Parse(Session["WarehouseId"].ToString()),
                                int.Parse(Session["ReceiptId"].ToString()),
                                int.Parse(StorageUnitBatchId),
                                int.Parse(Session["OperatorID"].ToString()),
                                 ActualQuantity);
            if (PackageLineID == -1)
            {
                Master.MsgText = "Already Added";
            }
            else
            {
                r.SetPackageLines(Session["ConnectionStringName"].ToString(),
                                    PackageLineID,
                                    int.Parse(StorageUnitBatchId),
                                    int.Parse(Session["OperatorID"].ToString()),
                                    ActualQuantity, ActualQuantity, 0);

                r.CompletePackageLines(Session["ConnectionStringName"].ToString(),
                                        (int)Session["WarehouseId"],
                                        int.Parse(Session["ReceiptId"].ToString()));

                
            }
        };
        RadioButtonPackaging.SelectedIndex = 0;
        TextboxQuantity.Text = "";
        
        

    }

    protected void LinkButtonQuit_Click(object sender, EventArgs e)
    {
        Session["PassReceiptId"] = "";
        Response.Redirect(Session["ReturnURL"].ToString());
    }
}
