<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StockTake.ascx.cs" Inherits="Screens_StockTake" %>

<asp:Label ID="LabelConfirmQuantity" runat="server" Text="<%$ Resources:Default, StockTake %>"></asp:Label>
<br />
<asp:TextBox ID="TextBoxConfirmQuantity" runat="server" TabIndex="1"></asp:TextBox>
<br />
<asp:Button ID="lbConfirmQuantity" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbConfirmQuantity_Click"></asp:Button>
<asp:Button ID="lbResetQuantity" runat="server" TabIndex="3" Text="<%$ Resources:Default, Reset %>" OnClick="lbResetQuantity_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
