<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PalletBuild.aspx.cs" Inherits="Screens_PalletBuild" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%--    <asp:DetailsView ID="DetailsViewDocument" runat="server" DataKeyNames="ContainerHeaderId" DataSourceID="ObjectDataSourceDocument"
        AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
            <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="Container"
        SelectMethod="GetItems">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
            <asp:ControlParameter Name="barcode" ControlID="TextBoxBarcode" DbType="String" />
            <asp:ControlParameter Name="ProductBarcode" ControlID="TextBoxProduct" DbType="String" />
            <asp:ControlParameter Name="Batch" ControlID="TextBoxBatch" DbType="String" />
            <asp:ControlParameter Name="LocationBarcode" ControlID="TextBoxPickLocation" DbType="String" />
        </SelectParameters>
    </asp:ObjectDataSource>--%>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
            <asp:Button ID="ButtonAccept1" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="ButtonAccept_Click"></asp:Button>
            <asp:Button ID="ButtonQuit1" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>"></asp:Label>
            <asp:TextBox ID="TextBoxReferenceNumber" runat="server" TabIndex="2"></asp:TextBox>
            <asp:Button ID="ButtonAdd2" runat="server" TabIndex="4" Text="<%$ Resources:Default, Add %>" OnClick="ButtonAdd_Click"></asp:Button>
            <asp:Button ID="ButtonRemove1" runat="server" TabIndex="4" Text="<%$ Resources:Default, Remove %>" OnClick="ButtonRemove_Click"></asp:Button>
            <asp:Button ID="ButtonFinish2" runat="server" TabIndex="5" Text="<%$ Resources:Default, Finish %>" OnClick="ButtonFinish_Click"></asp:Button>
            <asp:Button ID="ButtonQuit2" runat="server" TabIndex="6" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
    </asp:MultiView>
    <br />
</asp:Content>

