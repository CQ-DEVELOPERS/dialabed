using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_DespatchLocation : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }

           

        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PrivateVariables
    private int result = 0;
    private string strresult = "";
    private string theErrMethod = "";
    private int? ses = null;
    #endregion PrivateVariables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/DespatchLocation.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                } 
                
                Master.MsgText = Resources.Default.DespatchLocationTitle.ToString();
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion Page Load

    #region CommandAccept_Click
    protected void CommandAccept_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList arrList = new ArrayList();

            Button btn = new Button();
            btn = (Button)sender;

            int totalLines = 0, palletCheckID = 0;

            string palletCount = TextBoxPalletId.Text;
           
            /*************************************************/

            string Constr = Session["ConnectionStringName"].ToString(); // = "Connection String"; 


            // string connectionStringName = Session["ConnectionStringName"].ToString();
            int operatorId = -1;

            if (!int.TryParse(Session["OperatorId"].ToString(), out operatorId))
            {
                Master.MsgText = Resources.ResMessages.InvalidOperator.ToString();
                return;
            }

            if (TextBoxOrderNum.Text == "")
            {
                Master.MsgText = Resources.ResMessages.InvalidOrderNumber.ToString();
                TextBoxOrderNum.TabIndex = 1;
                TextBoxOrderNum.Focus();
                return;
            }

            if (TextBoxLocation.Text == "")
            {
                Master.MsgText = Resources.ResMessages.InvalidLocation.ToString();
                TextBoxLocation.TabIndex = 1;
                TextBoxLocation.Focus();
                return;
            }

            
            if (TextBoxPalletId.Text == "")
            {
                Master.MsgText = Resources.ResMessages.InvalidPalletId.ToString();
                TextBoxPalletId.TabIndex = 1;
                TextBoxPalletId.Focus();
                return;
            }
            

            //palletID = CleanPalletID();

            /**************************************************/
            //if (palletID == 0)
            //{
            //    Master.MsgText = Resources.ResMessages.InvalidPalletId.ToString();
            //    return;
            //}
            //else
            //{
            //    if (instructId != 0)
            //    {
                   
            //    }
            //    else
            //    {
            //        MultiView1.ActiveViewIndex = MultiView1.ActiveViewIndex + 1;
            //    }
            //}

            Despatch dplocation = new Despatch();
            arrList = dplocation.ConfirmLocation(Constr, TextBoxOrderNum.Text, TextBoxLocation.Text, TextBoxPalletId.Text);

            if (arrList != null)
            {
                result = int.Parse(arrList[0].ToString());

                palletCheckID = int.Parse(arrList[1].ToString());

                totalLines = int.Parse(arrList[2].ToString());

            }
            else
                return;

            DBErrorMessage dberr = new DBErrorMessage();

            if (result != 0 && palletCount == "-1")
            {
                Master.MsgText = dberr.ReturnErrorString(result);
                btn.ID = "NextPallet";
                lbReset_Click(sender, e);
           
                return;
            
            }                    
            else
            {


                if (palletCount == "-1")
                {
                    //This is the last line and therefore make the 
                    CommandAccept.Visible = false;
                    btn.ID = "NextPallet";
                    lbReset_Click(sender, e);

                }
                else
                {
                    Master.MsgText = Resources.ResMessages.Successful.ToString();
                    btn.ID = "NextPallet";
                    lbReset_Click(sender, e);
                }
            }
        
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion CommandAccept_Click

    #region Reset
    protected void lbReset_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lbButton = (LinkButton)sender;

            switch (lbButton.ID)
            {

                case "lbResetPallet":

                    TextBoxPalletId.TabIndex = 1;
                    CommandAccept.TabIndex = 2;
                    lbResetLocation.TabIndex = 3;
                    //hlQuit.TabIndex = 4;
                    lbCompleteAll.TabIndex = 5;
                    TextBoxLocation.TabIndex = 6;
                    TextBoxOrderNum.TabIndex = 7;

                    TextBoxPalletId.Text = "";
                    TextBoxPalletId.Focus();
                    break;
                
                case "NextPallet":

                    TextBoxPalletId.TabIndex = 1;
                    CommandAccept.TabIndex = 2;
                    lbResetLocation.TabIndex = 3;
                    //hlQuit.TabIndex = 4;
                    lbCompleteAll.TabIndex = 5;
                    TextBoxLocation.TabIndex = 6;
                    TextBoxOrderNum.TabIndex = 7;


                    TextBoxPalletId.Text = "";
                    TextBoxPalletId.Focus();

                    break;
                default:
                    break;

            }

            //  string str = lb.ID.ToString();
            //  the following needs reset
            //  PalletId 
            //  PickLocation 
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion Reset

    #region lbNextLocation
    protected void lbNextLocation_Click(object sender, EventArgs e)
    {
        try
        {
            CleanPage();
            MultiView1.ActiveViewIndex = 0;
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion lbNextLocation

    #region CommandLocationPallet
    protected void CommandLocationPallet_Click(object sender, EventArgs e)
    {
        try
        {
            bool allValid = true;
            int iCount = 10; //This is the lines or pallets that should be counted from the db //

            //This was for testing purposes
            //if(!int.TryParse(TextBoxCount.Text, out iCount))
            //{
            //    Master.MsgText = Resources.ResMessages.NotNumeric.ToString();
            //    return;
            //}

            // Save the palletID and Get the count from the Database on 
            // if iCount = 0

            for (int i = 0; i <= iCount; i++)
            {
                if (iCount - i == 0)
                {
                    lbCompleteAll.Visible = true;
                }
                else
                {
                    lbCompleteAll.Visible = false;
                }
            }

            //if (iCount == -1)
            //{
            //    lbCompleteAll.Visible = true;
            //}
            //else
            //{
            //    lbCompleteAll.Visible = false;
            //}


            if (allValid)
            { }
            else
            { }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion CommandLocationPallet

    #region Clean Page
    protected void CleanPage()
    {
        try
        {
            TextBoxOrderNum.Text = "";
            TextBoxLocation.Text = "";
            TextBoxPalletId.Text = "";
            
            TextBoxOrderNum.TabIndex = 1;
            TextBoxLocation.TabIndex = 2;
            TextBoxPalletId.TabIndex = 3;
            CommandAccept.TabIndex = 4;
            lbResetLocation.TabIndex = 5;
            //hlQuit.TabIndex = 6;
            lbCompleteAll.TabIndex = 7;

            TextBoxOrderNum.Focus();
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion Clean Page

    #region Clean Pallet ID
    protected int CleanPalletID()
    {
        int iPalletID = 0;

        try
        {
           
            string strPalletId = TextBoxPalletId.Text;

            if (TextBoxPalletId.Text != "")
            {

                if (TextBoxPalletId.Text.StartsWith("P:"))
                {
                    // Remove first 2 char.
                    StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                    strPalletId = strReader.ReadLine();
                    Session["PalletId"] = TextBoxPalletId.Text;
                    iPalletID = int.Parse(strPalletId);


                }
                else if (TextBoxPalletId.Text.StartsWith("J:"))
                {

                    // Remove first 2 char.
                    StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                    strPalletId = strReader.ReadLine();
                    Session["PalletId"] = TextBoxPalletId.Text;
                    iPalletID = int.Parse(strPalletId);


                }
                else if (TextBoxPalletId.Text.StartsWith("R:"))
                {

                    // Remove first 2 char.
                    StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                    strPalletId = strReader.ReadLine();
                    Session["PalletId"] = TextBoxPalletId.Text;
                    iPalletID = int.Parse(strPalletId);


                }
                else if (int.TryParse(TextBoxPalletId.Text, out iPalletID))
                {

                    Master.MsgText = Resources.ResMessages.InvalidPalletId.ToString();

                    Session["PalletId"] = iPalletID.ToString();

                    TextBoxPalletId.Text = "";
                    TextBoxPalletId.TabIndex = 1;
                    TextBoxPalletId.Focus();

                }
                else
                {
                    //Master.MsgText = Resources.ResMessages.EmptyField.ToString();

                    //TextBoxPalletId.Text = "";
                    //TextBoxPalletId.TabIndex = 1;
                    //TextBoxPalletId.Focus();
                }



            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
       
        return iPalletID;
    }     
    #endregion Clean Pallet ID

    #region Complete All
    protected void lbCompleteAll_Click(object sender, EventArgs e)
    {
        ArrayList arrlist = new ArrayList();

        //Save PICK & BIN
        string Constr = Session["ConnectionStringName"].ToString();// = "Connection String"; 


        // string connectionStringName = Session["ConnectionStringName"].ToString();
        int operatorId = -1;

        if (!int.TryParse(Session["OperatorId"].ToString(), out operatorId))
        {
            Master.MsgText = Resources.ResMessages.InvalidOperator.ToString();
            return;
        }

        if (TextBoxOrderNum.Text == "")
        {
            Master.MsgText = Resources.ResMessages.InvalidOrderNumber.ToString();
            TextBoxOrderNum.TabIndex = 1;
            TextBoxOrderNum.Focus();
            return;
        }

        if (TextBoxLocation.Text == "")
        {
            Master.MsgText = Resources.ResMessages.InvalidLocation.ToString();
            TextBoxLocation.TabIndex = 1;
            TextBoxLocation.Focus();
            return;
        }


        Despatch dplocation = new Despatch();
        arrlist  = dplocation.ConfirmLocation(Constr, TextBoxOrderNum.Text, TextBoxLocation.Text, "-1");


        bool allValid = false;
        try
        {
            if (!allValid)
            {
                MultiView1.ActiveViewIndex = MultiView1.ActiveViewIndex + 1;
                CleanPage();
            }
            else
            { }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion Complete All

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        int loopPrevention = -1;

        try
        {
            try
            {
                loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());
            }
            catch
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                loopPrevention = 0;
            }



            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                  Resources.Default.DespatchLocationTitle.ToString(),
                                                                  theErrMethod,
                                                                  ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {

                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
            }

        }
        catch (Exception exMsg)
        {

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                 Resources.Default.DespatchLocationTitle.ToString(),
                                                                 theErrMethod,
                                                                 ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling
}
