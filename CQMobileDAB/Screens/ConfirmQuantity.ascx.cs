using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ConfirmQuantity : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbConfirmQuantity.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            //lbResetQuantity.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            if (Session["Type"].ToString() == "Quantity")
            {
                TextBoxConfirmQuantity.Focus();
            }
        }

        //TextBoxConfirmQuantity.Text = "";

        //if (TextBoxConfirmQuantity.Text == "")
            //defaultQty();
    }

    protected void lbConfirmQuantity_Click(object sender, EventArgs e)
    {
        try
        {
            if (ConfirmQuantity())
            {
                Session["Message"] = Resources.ResMessages.Successful;
                Session["CurrentView"] = "Quantity";
                
                //Added this to clear the textbox quantity when doing putaways.
                //Textbox was populated with the previous putaway quantity
                TextBoxConfirmQuantity.Text = "";
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }

        //if (Session["FromURL"] != null)
        //    Response.Redirect(Session["FromURL"].ToString());
    }

    protected bool ConfirmQuantity()
    {
        try
        {
            Transact tran = new Transact();

            Decimal confirmQuantity = -1;
            Decimal quantity = 1;
            Decimal previousQuantity = -1;

            if (!Decimal.TryParse(Session["Quantity"].ToString(), out quantity))
            {
                Session["Error"] = true;
                Session["Message"] = Resources.ResMessages.InvalidField;
                return false;
            }

            if (!Decimal.TryParse(TextBoxConfirmQuantity.Text, out confirmQuantity))
            {
                Session["Error"] = true;
                Session["Message"] = Resources.ResMessages.QuantityReEnter;
                return false;
            }

            Session["Qty"] = confirmQuantity;

            string instructionTypeCode = tran.InstructionType(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);

            bool skip = false;

            if (instructionTypeCode == "R" && Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 245) //Replenishment - allow over picking
            || Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 401)) //Allow over picking
            {
                skip = true;
            }

            if (!skip)
            {
                if (confirmQuantity > quantity)
                {
                    Session["Error"] = true;
                    Session["Message"] = Resources.ResMessages.QuantityGreater;
                    return false;
                }
            }

            if (quantity == confirmQuantity)
                Session["QuantityCheck"] = true;
            else
            {
                if (Session["QuantityCheck"] == null)
                {
                    Session["QuantityCheck"] = false;
                    Session["PreviousQuantity"] = confirmQuantity;
                    //Session["Qty"] = confirmQuantity;
                }
                else
                {
                    Session["QuantityCheck"] = false;
                    if (Session["PreviousQuantity"] == null)
                    {
                        Session["PreviousQuantity"] = confirmQuantity;
                        //Session["Qty"] = confirmQuantity;
                    }
                    else
                    {
                        if (!Decimal.TryParse(Session["PreviousQuantity"].ToString(), out previousQuantity))
                        {
                            Session["Error"] = true;
                            Session["Message"] = Resources.ResMessages.InvalidField;
                            return false;
                        }
                        //Session["Qty"] = previousQuantity;
                    }
                }
            }

            if (previousQuantity == confirmQuantity)
            {
                Session["QuantityCheck"] = true;
                Session["PreviousQuantity"] = null;
            }

            if ((bool)Session["QuantityCheck"])
            {
                int result = tran.ConfirmQuantity(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], confirmQuantity);
                if (result == 0)
                {
                    Session["Error"] = false;
                    return true;
                }
                else if (result == 5)
                {
                    int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), -1);
                    //int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]);

                    if (instructionId != -1)
                    {
                        Session["InstructionId"] = instructionId;
                        Session["Message"] = Resources.ResMessages.Successful;
                        Session["Error"] = false;
                        Session["Type"] = "GetMixedPickDetails";
                        Session["ActiveViewIndex"] = 1;
                    }
                    return true;
                }
                else if (result == 8)
                {

                    Session["Error"] = false;
                    return true;

                    // Old Way of doing stock take
                    //int jobId = tran.AdhocStockTake(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], confirmQuantity);

                    //if (jobId > 0)
                    //{
                    //    Session["JobId"] = jobId;
                    //    Session["FromURL"] = "~/Screens/PickMixed.aspx";
                    //    Response.Redirect("~/Screens/StockTakeCount.aspx", false);
                    //    return false;
                    //}
                    //else
                    //{
                    //    Session["Message"] = Resources.ResMessages.Successful;
                    //    Session["Error"] = false;
                    //    Session["Type"] = "Quantity";
                    //    Session["CurrentView"] = "Quantity";
                    //    Session["ActiveViewIndex"] = 6;

                    //    if (tran.ConfirmQuantity(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], confirmQuantity) == 0)
                    //    {
                    //        Session["Error"] = false;
                    //        return true;
                    //    }
                    //    else
                    //    {
                    //        Session["Error"] = true;
                    //        return false;
                    //    }
                    //}
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.QuantityReEnter;
                    Session["Error"] = true;
                    return false;
                }
            }
            else
            {
                defaultQty();
                Session["PreviousQuantity"] = confirmQuantity;
                Session["Message"] = Resources.ResMessages.QuantityReEnter;
                Session["Error"] = true;
                return false;
            }
        }

        catch (Exception ex)
        {
            Session["Error"] = true;
            Session["Message"] = ex.Message;
            return false;
        }
    }

    protected void lbResetQuantity_Click(object sender, EventArgs e)
    {
        defaultQty();
        Session["QuantityCheck"] = null;
        Session["PreviousQuantity"] = null;
        TextBoxConfirmQuantity.Focus();
    }

    protected void defaultQty()
    {
        try
        {
            if (Session["InstructionId"] != null)
            {
                Transact tran = new Transact();

                string qty = "";

                if (Session["Qty"] == null)
                    qty = tran.DisplayQty(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);
                else
                {
                    qty = Session["Qty"].ToString();
                    Session["Qty"] = null;
                }

               // if (qty.ToLower() != "false")
                  //  TextBoxConfirmQuantity.Text = qty;
            }
        }
        catch { }
    }
}
