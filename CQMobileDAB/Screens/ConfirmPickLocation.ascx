<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfirmPickLocation.ascx.cs" Inherits="Screens_ConfirmPickLocation" %>

<asp:Label ID="LabelConfirmStoreLocation" runat="server" Text="<%$ Resources:Default, PickLocation %>"></asp:Label>
<br />
<asp:TextBox ID="TextBoxConfirmPickLocation" runat="server" TabIndex="1" TextMode="Password"></asp:TextBox>
<br />
<asp:Button ID="lbPickLocation" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbPickLocation_Click"></asp:Button>
<asp:Button ID="lbResetLoc" runat="server" TabIndex="3" Text="<%$ Resources:Default, Reset %>" OnClick="lbResetLoc_Click"></asp:Button>
<%--<asp:Button ID="LinkButtonSkip" runat="server" TabIndex="3" Text="<%$ Resources:Default, Skip %>" OnClick="LinkButtonSkip_Click"></asp:Button>--%>
<asp:Button ID="LinkButtonPrevious" runat="server" TabIndex="3" Text="<%$ Resources:Default, PreviousTransaction %>" OnClick="LinkButtonPrevious_Click"></asp:Button>
<asp:Button ID="LinkButtonNext" runat="server" TabIndex="4" Text="<%$ Resources:Default, NextTransaction %>" OnClick="LinkButtonNext_Click"></asp:Button>
<asp:Button ID="Button1" runat="server" TabIndex="2" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
