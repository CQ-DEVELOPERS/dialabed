using System;
using System.Data;
using System.Text;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_CreatePallet : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion 

    #region PrivateVariables
    private int result = 0;
    private string strresult = "";
    private string theErrMethod = "";   
    #endregion PrivateVariables

    #region Page_Load 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Master.MsgText = "CREATE PALLET";
           FillTextBoxes();
        }
    }
    #endregion Page_Load

    #region FillTextBoxes
    protected void FillTextBoxes()
    {
        
        Session["ActionCreatePalletDone"] = true;  

        TextBoxBarcode.TabIndex = 1;
        TextBoxQuantity.TabIndex = 2;
        CommandNext.TabIndex = 3;
        CommandCancel.TabIndex = 4;
        
        TextBoxBarcode.Text = Session["ProductCode"].ToString();
        TextBoxBatch.Text = Session["Batch"].ToString();

        try
        {
            // the reason for it being here is that on movements the session quantity will come through
            // but on Mix where a full pallet needs to be created the user will enter the qty depending on
            // the product

            if (Session["Url"].ToString() == "Movement.aspx")
                TextBoxQuantity.Text = Session["Quantity"].ToString();
            else
                TextBoxQuantity.Text = "";
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

     
        TextBoxPalletId.Focus();
        Master.MsgText = "Scan New PalletID";
    }
    #endregion

    #region CommandCancel_Click
    protected void CommandCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(Session["Url"].ToString());
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;

        }
    }
    #endregion CommandCancel_Click

    #region CommandNext_Click
    protected void CommandNext_Click(object sender, EventArgs e)
    {
        try
        {
            //CheckPallet_DataBind(0, 1);

            //result = 
            CheckPallet_DataBind(0, 1);
            //Session["InstructionId"] = result.ToString();
            if (result == 5)
            {
                Master.MsgText = "Quantity Error";
                TextBoxPalletId.Focus();
            }
            else
            {

                Response.Redirect(Session["Url"].ToString() + "?CheckPallet");

            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion CommandNext_Click

    #region CommandTryAgain_Click
    protected void CommandTryAgain_Click(object sender, EventArgs e)
    {
        try
        {
            MultiView1.ActiveViewIndex = 0;
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion CommandTryAgain_Click

    #region CheckPallet_DataBind
    protected void CheckPallet_DataBind(int type, int operatorId)
    {

        try
        {
            string palletId = TextBoxPalletId.Text;
            int palletIdInt = -1;
            string barcode = TextBoxBarcode.Text;
            string batch = TextBoxBatch.Text;
            string quantity = TextBoxQuantity.Text;
            Decimal quantityInt = -1;
            string constring = Session["ConnectionStringName"].ToString();
            int operatorID = int.Parse(Session["OperatorId"].ToString());
            int instructId = int.Parse(Session["InstructionId"].ToString());

            MultiView1.ActiveViewIndex = 1;

            if (palletId == "" || barcode == "" || batch == "" || quantity == "")
            {
                Master.MsgText = "Not all information was entered.";
                return;
            }
            bool isnumber;

            isnumber = int.TryParse(palletId, out palletIdInt);

            if (!isnumber)
            {
                palletIdInt = CleanPalletJob();

                if (palletIdInt == -1)
                {
                    Master.MsgText = "Pallet is not a valid number.";
                    return;
                }

            }

            isnumber = Decimal.TryParse(quantity, out quantityInt);

            if (!isnumber)
            {
                Master.MsgText = "Quantity is not a valid number.";
                return;
            }

            Transact tran = new Transact();

            result = tran.PalletCreate(constring, instructId);
            //confirm that -1

            bool bresult = tran.PalletUpdate(constring, result, palletIdInt, quantityInt);

            if (bresult)
            {
                // Master.MsgText = "Pallet correct, you can continue.";
                Session["InstructionId"] = result;

                Response.Redirect(Session["Url"].ToString() + "?ActionCreatePalletDone");
            }
            else
            {
                TextBoxBarcode.Text = "";
                TextBoxBarcode.Focus();
                Master.MsgText = "Invalid transaction"; //ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Message"].Ordinal).ToString();
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion CheckPallet_DataBind

    #region ClearAllForNewProduct
    private void ClearAllForNewProduct()
    {
       
        theErrMethod = "ClearAllForNewProduct";
        try
        {
            //Clear Session Variables
            Session["StoreLocation"] = "";
            Session["PickSecurityCode"] = "";
            Session["ConfirmedQuantity"] = "";
            Session["InstructionId"] = "";
            Session["Quantity"] = "";
            Session["Confirmed"] = "";
            Session["JobId"] = "";
            Session["PickLocation"] = "";
         }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion ClearAllForNewProduct  

    #region CleanPalletJob
    protected int CleanPalletJob()
    {
        int iPalletID = -1;
        string strPalletId = "";
        try
        {
            if (TextBoxPalletId.Text != "")
            {

                if (TextBoxPalletId.Text.StartsWith("P:"))
                {
                    if (!int.TryParse(TextBoxPalletId.Text, out iPalletID))
                    {
                        strPalletId = TextBoxPalletId.Text;
                        // Remove first 2 char.
                        StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                        strPalletId = strReader.ReadLine();
                        iPalletID = int.Parse(strPalletId);
                        //Session["PalletId"] = TextBoxPalletId.Text;                 
                    }

                }
                else if (TextBoxPalletId.Text.StartsWith("J:"))
                {
                    //if (!int.TryParse(TextBoxPalletId.Text, out iPalletID))
                    //{
                    //    strPalletId = TextBoxPalletId.Text;
                    //    // Remove first 2 char.
                    //    StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                    //    strPalletId = strReader.ReadLine();
                    //    iPalletID = int.Parse(strPalletId);
                    //    Session["PalletId"] = TextBoxPalletId.Text;

                    //}
                    Master.MsgText = "Invalid Job, Scan PalletId";
                    return -1;

                }
                else if (int.TryParse(TextBoxPalletId.Text, out iPalletID))
                {

                    return iPalletID;
                }
            }

            else
            {
                Master.MsgText = "Invalid Transaction";
                iPalletID = -1;

                return iPalletID;
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
        return iPalletID;
    }
    #endregion

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        int loopPrevention = -1;

        try
        {
            try
            {
                loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());
            }
            catch
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                loopPrevention = 0;
            }



            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                  Resources.Default.CreatePalletTitle.ToString(),
                                                                  theErrMethod,
                                                                  ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {

                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
            }

        }
        catch (Exception exMsg)
        {

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                 Resources.Default.CreatePalletTitle.ToString(),
                                                                 theErrMethod,
                                                                 ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling
}
