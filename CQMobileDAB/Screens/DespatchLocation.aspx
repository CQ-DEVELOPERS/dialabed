<%@ Page Language="C#"
    MasterPageFile="~/Master.master"
    AutoEventWireup="true" 
    CodeFile="DespatchLocation.aspx.cs"
    Inherits="Screens_DespatchLocation" 
    Title="<%$ Resources:Default, DespatchLocationTitle %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <table align="left" width="100%">
             <tr>
                    <td align="left">
                        <asp:Label ID="lbOrderNum" runat="server" Font-Bold="True" Text="<%$ Resources:Reslabels, OrderNumber %>"></asp:Label>
                        </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="TextBoxOrderNum" runat="server" TabIndex="1"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lbLocation" runat="server" Font-Bold="True" Text="<%$ Resources:Reslabels, Location %>"></asp:Label>
                        </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="TextBoxLocation" runat="server" TabIndex="2"></asp:TextBox>
                        <asp:Label ID="lbPalletDefault" runat="server" Text="<%$ Resources:ResLabels, Pallets_Ten %>"
                            Visible="False"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label1" runat="server" Font-Bold="True">ScanPalletID:</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="TextBoxPalletId" runat="server" TabIndex="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="height: 14px">
                        &nbsp;<asp:Button ID="CommandAccept" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            OnClick="CommandAccept_Click" TabIndex="4" Text="<%$ Resources:Default, Next %>"></asp:Button>
                        <asp:Button ID="lbResetLocation" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            OnClick="lbReset_Click" TabIndex="5" Text="<%$ Resources:Default, Reset %>"></asp:Button>
                        <asp:Button ID="btnQuit" runat="server" TabIndex="6" Text="<%$ Resources:Default, Quit %>"
                            PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
                        <asp:Button ID="lbCompleteAll" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            TabIndex="6" Text="<%$ Resources:Default, CompleteAll %>" OnClick="lbCompleteAll_Click"></asp:Button></td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <table align="center" width="100%">
                <tr>
                    <td align="center" style="height: 14px">
                        <asp:Literal ID="lblFinish" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td align="center" style="height: 14px">
                        <asp:Button ID="lbNextLocation" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            TabIndex="1" Text="<%$ Resources:Default, NextLocation %>" OnClick="lbNextLocation_Click"></asp:Button></td>
                </tr>
                <tr>
                    <td align="center" style="height: 14px">
                        <asp:Button ID="Button1" runat="server" TabIndex="2" Text="<%$ Resources:Default, Quit %>"
                            PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
                        </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

