﻿
CREATE VIEW [dbo].[viewOperator]
AS
  select o.WarehouseId,
		 w.Warehouse,
         o.OperatorId,
         o.Operator,
         o.Password,
         og.OperatorGroupId,
         og.OperatorGroupCode,
         og.OperatorGroup,
         o.Printer,
         o.Port,
         o.IPAddress,
         o.LastInstruction,
         o.SiteType,
         p.Principal,
         o.PickAisle,
         o.StoreAisle,
         o.ActiveIndicator
    from Operator       o (nolock)
    join OperatorGroup og (nolock) on o.OperatorGroupId = og.OperatorGroupId
    join Warehouse w (nolock) on o.WarehouseId = w.WarehouseId
    left
    join Principal      p (nolock) on o.PrincipalId = p.PrincipalId

