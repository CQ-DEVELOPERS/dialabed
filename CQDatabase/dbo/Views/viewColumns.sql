﻿
create view viewColumns
as

select tab.name as 'TableName'
      ,col.name as 'ColumnName'
      ,col.column_id as 'ColumnNumber'
      ,case when col.is_nullable = 1
            then 'null'
            else 'not null'
            end
      as 'Nullable'
      
      ,col.is_nullable
      ,col.is_identity
      ,case when pk.ColumnName is null
            then 0
            else 1
            end
      as 'is_PrimaryKey'
      
      ,typ.name
      + case when type_name(col.user_type_id) in ('decimal','real','money','float','numeric','smallmoney','tinyint','smallint','int','bigint','datetime','bit','ntext','sql_variant')
             then case when typ.name in ('char','decimal','nchar','numeric')
                  then isnull('(' + convert(varchar(5), columnproperty(col.object_id, col.name, 'precision')) + ',' + convert(varchar(10), OdbcScale(col.system_type_id, col.scale)) + ')','')
                  else ''
                  end
             else '(' + convert(varchar(10), col.max_length) + ')'
             end
      as 'TypeName'
      
      ,',@'
      + col.name
      + REPLICATE(' ', 26 - len(col.name))
      + typ.name
      + case when type_name(col.user_type_id) in ('decimal','real','money','float','numeric','smallmoney','tinyint','smallint','int','bigint','datetime','bit','ntext','sql_variant')
             then case when typ.name in ('char','decimal','nchar','numeric')
                  then isnull('(' + convert(varchar(5), columnproperty(col.object_id, col.name, 'precision')) + ',' + convert(varchar(10), OdbcScale(col.system_type_id, col.scale)) + ')','')
                  else ''
                  end
             else '(' + convert(varchar(10), col.max_length) + ')'
             end
      as 'Declare'
      
      ,',@'
      + col.name
      + REPLICATE(' ', 26 - len(col.name))
      + '= @'
      + col.name
      as 'Update'
      
      ,'if not exists(select top 1 1 from syscolumns where object_name(id) = ''' + tab.name + ''' and name = ''' + col.name + ''')' + CHAR(13)
       + '  alter table ' + tab.name + ' add ' + col.name + ' '
       + typ.name
       + case when type_name(col.user_type_id) in ('decimal','real','money','float','numeric','smallmoney','tinyint','smallint','int','bigint','datetime','bit','ntext','sql_variant')
              then case when typ.name in ('char','decimal','nchar','numeric','nvarchar','varbinary','varchar')
                   then isnull('(' + convert(varchar(5), columnproperty(col.object_id, col.name, 'precision')) + ',' + convert(varchar(10), OdbcScale(col.system_type_id, col.scale)) + ')','')
                   else ''
                   end
              else '(' + convert(varchar(10), col.max_length) + ')'
              end
       + case when col.is_nullable = 0 then ' ' else ' null' end as 'Add'
      ,'if exists(select top 1 1 from syscolumns where object_name(id) = ''' + tab.name + ''' and name = ''' + col.name + ''')' + CHAR(13)
       + '  alter table ' + tab.name + ' alter column ' + col.name + ' '
       + typ.name
       + case when type_name(col.user_type_id) in ('decimal','real','money','float','numeric','smallmoney','tinyint','smallint','int','bigint','datetime','bit','ntext','sql_variant')
              then case when typ.name in ('char','decimal','nchar','numeric','nvarchar','varbinary','varchar')
                   then isnull('(' + convert(varchar(5), columnproperty(col.object_id, col.name, 'precision')) + ',' + convert(varchar(10), OdbcScale(col.system_type_id, col.scale)) + ')','')
                   else ''
                   end
              else '(' + convert(varchar(10), col.max_length) + ')'
              end
       + case when col.is_nullable = 0 then ' ' else ' null' end as 'Alter'
      ,'if exists(select top 1 1 from syscolumns where object_name(id) = ''' + tab.name + ''' and name = ''' + col.name + ''')' + CHAR(13)
       + '  alter table ' + tab.name + ' drop column ' + col.name as 'Drop'

  from sys.tables  tab
  join sys.columns col on tab.object_id = col.object_id
  join sys.types   typ on col.user_type_id = typ.user_type_id
  left
  join viewPK       pk on tab.name = pk.TableName
                      and col.name = pk.ColumnName
