﻿ 
/*
  /// <summary>
  ///   View Name : viewVisualDataChecking
  ///   Filename       : viewVisualDataChecking.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE view viewVisualDataChecking
 
as
  select v.*, l.Latitude, l.Longitude, datediff(dd, DespatchDate, Getdate()) as 'ColourCode'
    from VisualDataOutbound v
    join Location           l on v.Location = l.Location
   where v.StatusCode in ('CK','CD')
