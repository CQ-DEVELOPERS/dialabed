﻿CREATE VIEW dbo.viewStock
AS
  select sub.StorageUnitBatchId,
         su.StorageUnitId,
         p.ProductId,
         p.HostId,
         b.BatchId,
         p.ProductCode,
         p.Product,
         p.Barcode as 'ProductBarcode',
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         b.BillOfEntry,
         b.BOELineNumber,
         s.Status as 'BatchStatus',
         su.ProductCategory,
         p.ShelfLifeDays,
         b.CreateDate as 'BatchCreateDate',
         b.ExpiryDate,
         datediff(dd, getdate(), ExpiryDate) as 'DaysToExpiry',
         case when p.ShelfLifeDays > 0
              then (convert(float, datediff(dd, getdate(), ExpiryDate)) / convert(float, p.ShelfLifeDays)) * 100
              else 0
              end as 'ShelfLifePercentage',
         pr.PrincipalId,
         pr.PrincipalCode
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
    join Status             s (nolock) on b.StatusId            = s.StatusId
    left
    join Principal         pr (nolock) on p.PrincipalId         = pr.PrincipalId
