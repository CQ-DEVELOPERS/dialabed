﻿
create view [dbo].[vw_MenuItem_OperatorGroup] as 
Select mi.MenuItemId,
       mi.MenuId,
       m.Menu  + ' \ ' + mi.MenuItem as 'MenuItem',
       mi.ToolTip,
       mi.NavigateTo,
       mi.ParentMenuItemId,
       mi.OrderBy,
       o.operatorgroupid,
       g.operatorGroup,
       o.access
from Menuitem	mi
Left join Menu m on mi.MenuId = m.MenuId
Left join OperatorGroupMenuItem o	(nolock)	on o.menuItemId = mi.MenuItemid
Left Join operatorgroup g on g.operatorgroupid = o.operatorgroupid	
