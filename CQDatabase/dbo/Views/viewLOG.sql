﻿
create view viewLOG
as
select a.WarehouseId, a.AreaId, l.LocationId, og.OperatorGroupId, a.Area, l.Location, og.OperatorGroup, og.OperatorGroupCode
  from LocationOperatorGroup lcog
  join Location                 l on lcog.LocationId = l.LocationId
  join AreaLocation            al on l.LocationId    = al.LocationId
  join Area                     a on al.AreaId       = a.AreaId
  join OperatorGroup           og on lcog.OperatorGroupId = og.OperatorGroupId
