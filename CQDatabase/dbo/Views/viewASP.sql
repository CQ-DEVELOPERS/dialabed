﻿
create view viewASP
as

Select distinct case when u.UserId is null
            then '...Undefined...'
            else uo.DatabaseName
            end 'DatabaseName',
       o.Operator,
       o.Password,
       o.OperatorGroup,
       o.OperatorId,
       u.UserId,
       'exec sp_grants_user ''' + o.Operator + ''', ''' + o.Password + ''', ''' + convert(varchar(10), WarehouseId) + ''', ''' + o.OperatorGroup + '''' as 'Script'
  From viewOperator                 o
  Join CQcommon.dbo.UserOperator   uo on uo.OperatorId = o.OperatorId
  left
  Join CQsecurity.dbo.aspnet_users  u on uo.UserId     = u.UserId
                                   and o.Operator    = u.UserName collate Latin1_General_CI_AS
