﻿CREATE VIEW dbo.viewContainer
AS
select d.ContainerHeaderId,
       d.ContainerDetailId,
       h.ReferenceNumber,
       h.JobId,
       h.PalletId,
       sub.StorageUnitBatchId,
       p.ProductCode,
       p.Product,
       sku.SKUCode,
       b.Batch,
       d.Quantity,
       pick.LocationId  as 'PickLocationId',
       pick.Location    as 'PickLocation',
       store.LocationId as 'StoreLocationId',
       store.Location   as 'StoreLocation',
       o.Operator,
       h.CreateDate
  from ContainerHeader    h (nolock)
  left
  join Operator           o (nolock) on h.CreatedBy = o.OperatorId
  join ContainerDetail    d (nolock) on h.ContainerHeaderId = d.ContainerHeaderId
  join StorageUnitBatch sub (nolock) on d.StorageUnitBatchId = sub.StorageUnitBatchId
  join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
  join Product            p (nolock) on su.ProductId = p.ProductId
  join SKU              sku (nolock) on su.SKUId = sku.SKUId
  join Batch              b (nolock) on sub.BatchId = b.BatchId
  left
  join Location        pick (nolock) on h.PickLocationId = pick.LocationId
  left
  join Location       store (nolock) on h.StoreLocationId = store.LocationId
