﻿
create view viewMessage
as

select im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceImportPOHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceImportPOHeaderId as 'InterfaceId', h.OrderNumber, h.RecordType, h.RecordStatus	, h.ProcessedDate, h.InsertDate, 'update InterfaceImportPOHeader set RecordStatus = ''N'', ProcessedDate = null where InterfaceImportPOHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceImportPOHeaderId) as 'Update', 'select * from InterfaceImportPOHeader where InterfaceImportPOHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceImportPOHeaderId) as 'Select', 'exec p_Interface_Reprocess ' + (select CONVERT(nvarchar(10), InterfaceTableId) from InterfaceTables where HeaderTable = 'InterfaceImportPOHeader') + ', ' + CONVERT(nvarchar(10), h.InterfaceImportPOHeaderId) as 'Reprocess'
  from InterfaceImportPOHeader h (nolock)
  left
  join InterfaceMessage       im (nolock) on h.InterfaceImportPOHeaderId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceImportPOHeader'
union
select im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceExportPOHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceExportPOHeaderId, h.OrderNumber, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, 'update InterfaceExportPOHeader set RecordStatus = ''N'', ProcessedDate = null where InterfaceExportPOHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceExportPOHeaderId), 'select * from InterfaceExportPOHeader where InterfaceExportPOHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceExportPOHeaderId), 'exec p_Interface_Reprocess ' + (select CONVERT(nvarchar(10), InterfaceTableId) from InterfaceTables where HeaderTable = 'InterfaceExportPOHeader') + ', ' + CONVERT(nvarchar(10), h.InterfaceExportPOHeaderId) as 'Reprocess'
  from InterfaceExportPOHeader h (nolock)
  left
  join InterfaceMessage       im (nolock) on h.InterfaceExportPOHeaderId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceExportPOHeader'
union
select im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceImportSOHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceImportSOHeaderId, h.OrderNumber, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, 'update InterfaceImportSOHeader set RecordStatus = ''N'', ProcessedDate = null where InterfaceImportSOHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceImportSOHeaderId), 'select * from InterfaceImportSOHeader where InterfaceImportSOHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceImportSOHeaderId), 'exec p_Interface_Reprocess ' + (select CONVERT(nvarchar(10), InterfaceTableId) from InterfaceTables where HeaderTable = 'InterfaceImportSOHeader') + ', ' + CONVERT(nvarchar(10), h.InterfaceImportSOHeaderId) as 'Reprocess'
  from InterfaceImportSOHeader h (nolock)
  left
  join InterfaceMessage       im (nolock) on h.InterfaceImportSOHeaderId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceImportSOHeader'
union
select im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceExportSOHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceExportSOHeaderId, h.OrderNumber, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, 'update InterfaceExportSOHeader set RecordStatus = ''N'', ProcessedDate = null where InterfaceExportSOHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceExportSOHeaderId), 'select * from InterfaceExportSOHeader where InterfaceExportSOHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceExportSOHeaderId), 'exec p_Interface_Reprocess ' + (select CONVERT(nvarchar(10), InterfaceTableId) from InterfaceTables where HeaderTable = 'InterfaceExportSOHeader') + ', ' + CONVERT(nvarchar(10), h.InterfaceExportSOHeaderId) as 'Reprocess'
  from InterfaceExportSOHeader h (nolock)
  left
  join InterfaceMessage       im (nolock) on h.InterfaceExportSOHeaderId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceExportSOHeader'
union
select im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceExportStockAdjustment') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceExportStockAdjustmentId, h.ProductCode, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, 'update InterfaceExportStockAdjustment set RecordStatus = ''N'', ProcessedDate = null where InterfaceExportStockAdjustmentId = ' + CONVERT(nvarchar(10), h.InterfaceExportStockAdjustmentId), 'select * from InterfaceExportStockAdjustment where InterfaceExportStockAdjustmentId = ' + CONVERT(nvarchar(10), h.InterfaceExportStockAdjustmentId), 'exec p_Interface_Reprocess ' + (select CONVERT(nvarchar(10), InterfaceTableId) from InterfaceTables where HeaderTable = 'InterfaceExportStockAdjustment') + ', ' + CONVERT(nvarchar(10), h.InterfaceExportStockAdjustmentId) as 'Reprocess'
  from InterfaceExportStockAdjustment h (nolock)
  left
  join InterfaceMessage       im (nolock) on h.InterfaceExportStockAdjustmentId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceExportStockAdjustment'
union
select im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceImportHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceImportHeaderId, h.OrderNumber, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, 'update InterfaceImportHeader set RecordStatus = ''N'', ProcessedDate = null where InterfaceImportHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceImportHeaderId), 'select * from InterfaceImportHeader where InterfaceImportHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceImportHeaderId), 'exec p_Interface_Reprocess ' + (select CONVERT(nvarchar(10), InterfaceTableId) from InterfaceTables where HeaderTable = 'InterfaceImportHeader') + ', ' + CONVERT(nvarchar(10), h.InterfaceImportHeaderId) as 'Reprocess'
  from InterfaceImportHeader h (nolock)
  left
  join InterfaceMessage       im (nolock) on h.InterfaceImportHeaderId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceImportHeader'
union
select im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceExportHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceExportHeaderId, h.OrderNumber, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, 'update InterfaceExportHeader set RecordStatus = ''N'', ProcessedDate = null where InterfaceExportHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceExportHeaderId), 'select * from InterfaceExportHeader where InterfaceExportHeaderId = ' + CONVERT(nvarchar(10), h.InterfaceExportHeaderId), 'exec p_Interface_Reprocess ' + (select CONVERT(nvarchar(10), InterfaceTableId) from InterfaceTables where HeaderTable = 'InterfaceExportHeader') + ', ' + CONVERT(nvarchar(10), h.InterfaceExportHeaderId) as 'Reprocess'
  from InterfaceExportHeader h (nolock)
  left
  join InterfaceMessage     im (nolock) on h.InterfaceExportHeaderId = im.InterfaceId
                                       and im.InterfaceTable = 'InterfaceExportHeader'

