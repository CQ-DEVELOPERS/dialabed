﻿
create view viewBOM
as
  select bh.BOMHeaderId,
         --bh.BOMType,
         bl.BOMLineId,
         bp.LineNumber,
         --bh.Description,
         hp.ProductCode,
         hp.Product,
         hsku.SKUCode,
         bh.Quantity,
         lp.ProductCode as 'CompProductCode',
         lp.Product as 'CompProduct',
         lsku.SKUCode as 'CompSKUCode',
         bp.Quantity as 'CompQuantity'
    from BOMHeader    bh (nolock)
    join StorageUnit hsu (nolock) on bh.StorageUnitId  = hsu.StorageUnitId
    join Product      hp (nolock) on hsu.ProductId     = hp.ProductId
    join SKU        hsku (nolock) on hsu.SKUId         = hsku.SKUId
    join BOMLine      bl (nolock) on bh.BOMHeaderId    = bl.BOMHeaderId
    join BOMProduct   bp (nolock) on bl.BOMLineId      = bp.BOMLineId
    join StorageUnit lsu (nolock) on bp.StorageUnitId  = lsu.StorageUnitId
    join Product      lp (nolock) on lsu.ProductId     = lp.ProductId
    join SKU        lsku (nolock) on lsu.SKUId         = lsku.SKUId
