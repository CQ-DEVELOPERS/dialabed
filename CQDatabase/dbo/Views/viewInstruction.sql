﻿
CREATE VIEW [dbo].[viewInstruction]
AS
select distinct i.WarehouseId,
       it.InstructionTypeId,
       it.InstructionType,
       it.InstructionTypeCode,
       i.OutboundShipmentId,
       i.JobId,
       --i.AutoComplete,
       s.StatusCode  as 'JobCode',
       s.Status      as 'JobStatus',
       si.StatusCode as 'InstructionCode',
       si.Status     as 'InstructionStatus',
       i.InstructionId,
       i.InstructionRefId,
       i.PalletId,
       i.ReceiptLineId,
       j.ReferenceNumber,
       o.OperatorId,
       o.Operator,
       vs.StorageUnitBatchId,
       vs.StorageUnitId,
       vs.ProductCode,
       vs.Product,
       vs.SKUCode,
       vs.SKU,
       vs.Batch,
       pick.WarehouseCode as 'PickWarehouseCode',
       pick.AreaId      as 'PickAreaId',
       pick.Area        as 'PickArea',
       pick.AreaCode    as 'PickAreaCode',
       pick.Location    as 'PickLocation',
       pick.LocationId  as 'PickLocationId',
       i.Picked,
       store.WarehouseCode as 'StoreWarehouseCode',
       store.AreaId     as 'StoreAreaId',
       store.Area       as 'StoreArea',
       store.AreaCode   as 'StoreAreaCode',
       store.Location   as 'StoreLocation',
       store.LocationId as 'StoreLocationId',
       i.Stored,
       pk.Quantity    as 'PalletQuantity',
       i.Quantity,
       i.ConfirmedQuantity,
       i.CheckQuantity,
       i.PreviousQuantity,
       i.Weight,
       i.ConfirmedWeight,
       i.CheckWeight,
       i.DropSequence,
       i.CreateDate,
       i.StartDate,
       i.EndDate,
       isnull(i.EndDate, isnull(i.StartDate, i.CreateDate)) 'MostRecent',
       'exec p_Instruction_Started ' + CONVERT(nvarchar(10), i.InstructionId) + ', 1' as 'Start',
       'exec p_Instruction_Finished ' + CONVERT(nvarchar(10), i.InstructionId) + ', 1'  as 'Finish',
       'exec p_Instruction_Reset ' + CONVERT(nvarchar(10), i.InstructionId)  as 'Reset',
       'exec p_Housekeeping_Instruction_Delete ' + CONVERT(nvarchar(10), i.InstructionId) as 'Delete'
  from Instruction i
  join Status          si (nolock) on i.StatusId           = si.StatusId
  join InstructionType it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
  join Job              j (nolock) on i.JobId              = j.JobId
  join Status           s (nolock) on j.StatusId           = s.StatusId
  left
  join viewStock       vs          on i.StorageUnitBatchId = vs.StorageUnitBatchId
  left
  join viewLocation  pick (nolock) on i.PickLocationId = pick.LocationId
  left
  join viewLocation store (nolock) on i.StoreLocationId = store.LocationId
  left
  join Operator         o (nolock) on j.OperatorId = o.OperatorId
  left
  join Pack              pk (nolock) on vs.StorageUnitId     = pk.StorageUnitId
                                    and i.WarehouseId        = pk.WarehouseId
                                    and pk.PackTypeId        = 1

