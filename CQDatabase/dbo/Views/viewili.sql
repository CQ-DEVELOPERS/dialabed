﻿CREATE VIEW dbo.viewili
AS
select distinct i.DropSequence,
       i.WarehouseId,
       i.OutboundShipmentId,
       ili.IssueId,
       i.IssueLineId,
       od.OrderNumber,
       ol.LineNumber,
       iss.WaveId,
       it.InstructionType,
       it.InstructionTypeCode,
       i.JobId,
       j.ReferenceNumber,
       convert(varchar(10), isnull(j.DropSequence,'')) + ' of ' + convert(varchar(10), isnull(j.Pallets,'')) as 'PalletNumber',
       s.StatusCode  as 'JobCode',
       s.Status      as 'JobStatus',
       si.StatusCode as 'InstructionCode',
       si.Status     as 'InstructionStatus',
       i.InstructionId,
       i.InstructionRefId,
       i.PalletId,
       i.ReceiptLineId,
       o.OperatorId,
       o.Operator,
       vs.StorageUnitBatchId,
       vs.StorageUnitId,
       vs.ProductCode,
       vs.Product,
       vs.SKUCode,
       vs.SKU,
       vs.Batch,
       pick.Location   as 'PickLocation',
       pick.LocationId as 'PickLocationId',
       i.Picked,
       store.Location   as 'StoreLocation',
       store.LocationId as 'StoreLocationId',
       i.Stored,
       i.Quantity,
       i.ConfirmedQuantity,
       i.CheckQuantity,
       i.Weight,
       i.ConfirmedWeight,
       i.CheckWeight,
       i.CreateDate,
       i.StartDate,
       i.EndDate,
       isnull(i.EndDate, isnull(i.StartDate, i.CreateDate)) 'MostRecent',
       'exec p_Instruction_Started ' + CONVERT(nvarchar(10), i.InstructionId) + ', 1' as 'Start',
       'exec p_Instruction_Finished ' + CONVERT(nvarchar(10), i.InstructionId) + ', 1'  as 'Finish',
       'exec p_Instruction_Reset ' + CONVERT(nvarchar(10), i.InstructionId)  as 'Reset',
       'exec p_Housekeeping_Instruction_Delete ' + CONVERT(nvarchar(10), i.InstructionId) as 'Delete'
  from Instruction i (nolock)
  join Status          si (nolock) on i.StatusId           = si.StatusId
  join InstructionType it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
  join Job              j (nolock) on i.JobId              = j.JobId
  join Status           s (nolock) on j.StatusId           = s.StatusId
  left
  join viewStock       vs          on i.StorageUnitBatchId = vs.StorageUnitBatchId
  left
  join Location      pick (nolock) on i.PickLocationId = pick.LocationId
  left
  join Location     store (nolock) on i.StoreLocationId = store.LocationId
  left
  join Operator         o (nolock) on j.OperatorId = o.OperatorId
  join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
  join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
  join Issue                iss (nolock) on il.IssueId = iss.IssueId
  join OutboundLine          ol (nolock) on il.OutboundLineId = ol.OutboundLineId
  join OutboundDocument od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
  --join OutboundDocument od (nolock) on od.OutboundDocumentId in (select ili.OutboundDocumentId from IssueLineInstruction ili (nolock) where isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId)
