﻿
create view viewUser
as
Select o.OperatorId,
       o.Operator,
       o.OperatorGroup,
       u.UserName,
       uo.DatabaseName,
       o.Password,
       'exec sp_grants_user ''' + o.Operator + ''', ''' + o.Password + ''', ''' + convert(varchar(10), WarehouseId) + ''', ''' + o.OperatorGroup + '''' as 'Create'
  From CQcommon..UserOperator   uo
  Join viewOperator o on uo.OperatorId = o.OperatorId
  Join CQsecurity..aspnet_users  u on uo.UserId     = u.UserId
                                  and o.Operator    = u.UserName collate Latin1_General_CI_AS
