﻿
create view viewSA
as
select im.InterfaceMessageId,
       im.InterfaceMessageCode,
       im.InterfaceMessage,
       im.Status,
       im.CreateDate as 'MessageDate',
       h.InterfaceExportStockAdjustmentId,
       h.RecordType,
       h.RecordStatus,
       h.ProcessedDate,
       h.InsertDate,
       h.ProductCode,
       h.Product,
       h.Batch,
       h.Quantity, h.Additional1, h.Additional2, h.Additional3, h.Additional4, h.Additional5,
       'update InterfaceExportStockAdjustment set RecordStatus = ''N'', ProcessedDate = null where InterfaceExportStockAdjustmentId = ' + CONVERT(nvarchar(10), h.InterfaceExportStockAdjustmentId) as 'Update',
       'select * from InterfaceExportStockAdjustment where InterfaceExportStockAdjustmentId = ' + CONVERT(nvarchar(10), h.InterfaceExportStockAdjustmentId) as 'Select',
       'exec p_Interface_Reprocess ' + (select CONVERT(nvarchar(10), InterfaceTableId) from InterfaceTables where HeaderTable = 'InterfaceExportStockAdjustment') + ', ' + CONVERT(nvarchar(10), h.InterfaceExportStockAdjustmentId) as 'Reprocess'
  from InterfaceExportStockAdjustment h (nolock)
  left
  join InterfaceMessage       im (nolock) on h.InterfaceExportStockAdjustmentId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceExportStockAdjustment'
