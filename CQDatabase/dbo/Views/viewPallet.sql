﻿CREATE VIEW dbo.viewPallet
AS
  select vl.WarehouseId,
         p.PalletId,
         vl.Area,
         vl.Location,
         vl.LocationId,
         vs.StorageUnitBatchId,
         vs.StorageUnitId,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vs.Batch,
         p.Quantity
    from Pallet     p
    join viewStock vs on p.StorageUnitBatchId = vs.StorageUnitBatchId
    left outer
    join viewLocation vl on p.LocationId = vl.LocationId
