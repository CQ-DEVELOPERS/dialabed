﻿CREATE VIEW dbo.viewAvailablePickfaces
AS
  select a.WarehouseId,
         a.AreaType,
         sub.StorageUnitId,
         sum(subl.ActualQuantity) as 'ActualQuantity',
         sum(subl.AllocatedQuantity) as 'AllocatedQuantity',
         sum(subl.ReservedQuantity) as 'ReservedQuantity'
    from StorageUnitBatch          sub (nolock)
    join Batch                       b (nolock) on sub.BatchId = b.BatchId
    join Status                      s (nolock) on b.StatusId = s.StatusId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join AreaLocation               al (nolock) on subl.LocationId        = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
    join Location                    l (nolock) on al.LocationId          = l.LocationId
   where a.StockOnHand = 1
     and a.AreaCode in ('PK','SP')
     and s.StatusCode = 'A'
     and l.ActivePicking = 1
group by a.WarehouseId,
         a.AreaType,
         sub.StorageUnitId
