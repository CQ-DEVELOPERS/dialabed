﻿CREATE VIEW dbo.viewOutboundStatus
AS
select os.OutboundShipmentId,
       oss.Status as 'OutboundShipment',
       oss.StatusCode as 'OutboundShipmentCode',
       i.IssueId,
       iss.Status as 'Issue',
       iss.StatusCode as 'IssueCode',
       il.IssueLineId,
       ils.Status as 'IssueLine',
       ils.StatusCode as 'IssueLineCode',
       j.JobId,
       js.Status as 'Job',
       js.StatusCode as 'JobCode',
       ins.InstructionId,
       inss.Status as 'Instruction',
       inss.StatusCode as 'InstructionCode',
       
       iss.StatusId as 'IssueStatusId',
       ils.StatusId as 'IssueLineStatusId',
       js.StatusId as 'JobStatusId',
       inss.StatusId as 'InstructionStatusId',
       
       iss.OrderBy as 'IssueOrderBy',
       ils.OrderBy as 'IssueLineOrderBy',
       js.OrderBy as 'JobOrderBy',
       inss.OrderBy as 'InstructionOrderBy'
  from IssueLineInstruction ili (nolock)
  join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
  join Status              inss (nolock) on ins.StatusId      = inss.StatusId
  join Issue                  i (nolock) on ili.IssueId       = i.IssueId
  join Status               iss (nolock) on i.StatusId        = iss.StatusId
  join IssueLine             il (nolock) on ili.IssueLineId   = il.IssueLineId
  join Status               ils (nolock) on il.StatusId       = ils.StatusId
  join Job                    j (nolock) on ins.JobId         = j.JobId
  join Status                js (nolock) on j.StatusId        = js.StatusId
  left 
  join OutboundShipment      os (nolock) on ili.OutboundShipmentId = os.OutboundShipmentId
  left 
  join Status               oss (nolock) on os.StatusId            = oss.StatusId
