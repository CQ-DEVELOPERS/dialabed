﻿
create view viewDefault
as
select OBJECT_NAME(con.object_id) 'ConstraintName', OBJECT_NAME(con.parent_object_id) as 'TableName', col.name as 'ColumnName', con.definition as 'Default'
  from sys.default_constraints con
  join sys.columns             col on con.parent_column_id = col.column_id and con.parent_object_id = col.object_id

