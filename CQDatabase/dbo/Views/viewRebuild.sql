﻿CREATE VIEW dbo.viewRebuild
AS
select WarehouseId,
       InstructionType,
       JobId,
       ReferenceNumber,
       JobCode,
       InstructionCode,
       InstructionId,
       PalletId,
       Operator,
       ProductCode,
       Product,
       SKUCode,
       Batch,
       PickLocation,
       StoreLocation,
       Quantity,
       case when InstructionTypeCode in ('P','PM','PS','FM')
            then ConfirmedQuantity * -1
            else ConfirmedQuantity
            end as 'ConfirmedQuantity',
       replace(case when InstructionTypeCode in ('STE','STL','STA','STP','R')
            then convert(varchar(10), ConfirmedQuantity)
            else '=SUM(R' + convert(varchar(10), row_number() over(order by isnull(EndDate, isnull(StartDate, CreateDate))) -1) + '+Q' + convert(varchar(10), row_number() over(order by isnull(EndDate, isnull(StartDate, CreateDate)))) + ')'
            end,'=SUM(R0+Q1)', convert(varchar(10), ConfirmedQuantity)) as 'SOH',
       '''' + convert(varchar(20), CreateDate, 120) as 'CreateDate',
       '''' + convert(varchar(20), StartDate, 120) as 'StartDate',
       '''' + convert(varchar(20), EndDate, 120) as 'EndDate'
  from viewINS
 where (PickLocation     = 'AD24A'
    or  StoreLocation    = 'AD24A')
    and CreateDate between '2009-10-16' and '2009-10-31'
