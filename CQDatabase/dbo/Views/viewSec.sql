﻿
create view viewSec
as
select vo.*,
       uo.DatabaseName,
       asp.UserId
  from viewOperator              vo
  join CQCommon..UserOperator    uo (nolock) on vo.OperatorId = uo.OperatorId
  join CQSecurity..aspnet_Users asp (nolock) on uo.UserId = asp.UserId

