﻿CREATE VIEW dbo.viewLocation
AS
  select a.WarehouseId,
         a.WarehouseCode,
         a.AreaId,
         a.Area,
         a.AreaCode,
         a.AreaType,
         a.AreaSequence,
         a.StockOnHand,
         a.Replenish,
         a.NarrowAisle,
         lt.LocationType,
         l.*
    from Location                    l (nolock)
    join LocationType               lt (nolock) on l.LocationTypeId        = lt.LocationTypeId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
