﻿CREATE VIEW dbo.viewSUA
AS
  select a.WarehouseId,
         su.StorageUnitId,
         a.AreaId,
         a.Area,
         a.AreaCode,
         a.AreaType,
         a.AreaSequence,
         a.StockOnHand,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         sua.StoreOrder,
         sua.PickOrder
    from StorageUnit          su (nolock)
    join Product               p (nolock) on su.ProductId     = p.ProductId
    join SKU                 sku (nolock) on su.SKUId         = sku.SKUId
    join StorageUnitArea     sua (nolock) on su.StorageUnitId = sua.StorageUnitId
    join Area                  a (nolock) on sua.AreaId        = a.AreaId
