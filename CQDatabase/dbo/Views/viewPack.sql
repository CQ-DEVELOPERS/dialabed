﻿CREATE VIEW dbo.viewPack
AS
  select pk.WarehouseId,
         pk.PackId,
         pt.PackTypeId,
         pt.PackType,
         vs.*,
         pk.BarCode as 'PackBarcode',
         pk.Quantity,
         pk.Length,
         pk.Width,
         pk.Height,
         pk.Volume,
         pk.Weight,
         pk.TareWeight,
         pk.NettWeight,
         pt.InboundSequence,
         pt.OutboundSequence
    from viewProduct vs
    join Pack        pk on vs.StorageUnitId = pk.StorageUnitId
    join PackType    pt on pk.PackTypeId = pt.PackTypeId

