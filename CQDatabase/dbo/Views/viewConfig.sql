﻿create view viewConfig
as
select c.ConfigurationId as 'ConfigId',
       c.WarehouseId,
       m.Module,
       Configuration as 'Config',
       Indicator,
       convert(nvarchar(max), Value) as 'Value',
       'update Configuration set Indicator = ' + case when Indicator = 0 then '1' else '0' end + ' where ConfigurationId = ' + convert(varchar(10), ConfigurationId) + ' and WarehouseId = ' + convert(varchar(10), WarehouseId) + '-- Original Value = ' + convert(varchar(10), Indicator) + ' for ' + Configuration as 'Toggle'
  from Configuration c
  join Module        m on c.ModuleId = m.ModuleId
