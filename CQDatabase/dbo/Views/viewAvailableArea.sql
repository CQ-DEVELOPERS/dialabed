﻿CREATE VIEW dbo.viewAvailableArea
AS
  select a.WarehouseId,
         a.AreaType,
         a.Area,
         a.AreaCode,
         sub.StorageUnitId,
         sum(subl.ActualQuantity) as 'ActualQuantity',
         sum(subl.AllocatedQuantity) as 'AllocatedQuantity',
         sum(subl.ReservedQuantity) as 'ReservedQuantity'
    from StorageUnitBatch          sub (nolock)
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join AreaLocation               al (nolock) on subl.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
   where a.StockOnHand = 1
group by a.WarehouseId,
         a.AreaType,
         a.Area,
         a.AreaCode,
         sub.StorageUnitId
