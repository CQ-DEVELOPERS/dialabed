﻿CREATE VIEW dbo.viewDrop
AS
select distinct isnull(osi.DropSequence, i.DropSequence) as 'DropSequence',
       j.DropSequence 'PalletSequence',
       i.WarehouseId,
       i.OutboundShipmentId,
       ili.IssueId,
       i.IssueLineId,
       od.OrderNumber,
       it.InstructionTypeCode,
       i.JobId,
       s.StatusCode  as 'JobCode',
       si.StatusCode as 'InstructionCode',
       i.InstructionId,
       i.InstructionRefId,
       i.PalletId,
       j.ReferenceNumber,
       o.Operator,
       p.ProductType,
       vs.ProductCode,
       vs.Product,
       vs.SKUCode,
       vs.SKU,
       vs.Batch,
       i.Quantity,
       i.ConfirmedQuantity,
       i.CheckQuantity,
       qty.Quantity as 'PalletQuantity',
       (convert(float, i.Quantity) / convert(float, qty.Quantity)) *100.000 as '%'
  from Instruction i
  join Status          si (nolock) on i.StatusId           = si.StatusId
  join InstructionType it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
  join Job              j (nolock) on i.JobId              = j.JobId
  join Status           s (nolock) on j.StatusId           = s.StatusId
  left
  join viewStock       vs          on i.StorageUnitBatchId = vs.StorageUnitBatchId
  join Product          p (nolock) on vs.ProductId = p.ProductId
  left
  join Location      pick (nolock) on i.PickLocationId = pick.LocationId
  left
  join Location     store (nolock) on i.StoreLocationId = store.LocationId
  left
  join Operator         o (nolock) on j.OperatorId = o.OperatorId
  join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
  join OutboundDocument od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
  join viewPalletQty   qty (nolock) on vs.StorageUnitId = qty.StorageUnitId
  left
  join OutboundShipmentIssue osi (nolock) on ili.IssueId = osi.IssueId
