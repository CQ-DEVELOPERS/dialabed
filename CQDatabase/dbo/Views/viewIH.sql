﻿
create view viewIH
as
select vi.InstructionId, vi.PalletId, vi.InstructionType, vs.ProductCode, vs.Batch, s.Status, vi.JobId, o.Operator, sl.Location as 'PickLoc', pl.Location as 'StoreLoc', ih.Quantity, ih.ConfirmedQuantity, ih.InsertDate
  from InstructionHistory ih (nolock)
  join viewInstruction    vi (nolock) on ih.InstructionId = vi.InstructionId
  left
  join Status              s (nolock) on ih.StatusId = s.StatusId
  left
  join Operator            o (nolock) on ih.OperatorId = o.OperatorId
  left
  join Location           sl (nolock) on ih.PickLocationId = sl.LocationId
  left
  join Location           pl (nolock) on ih.StoreLocationId = pl.LocationId
  left
  join viewStock          vs (nolock) on ih.StorageUnitBatchId = vs.StorageUnitBatchId
 
