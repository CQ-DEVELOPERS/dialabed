﻿CREATE VIEW dbo.viewCustomer
AS
select ec.ExternalCompanyId,
       ect.ExternalCompanyTypeId,
       ect.ExternalCompanyTypeCode,
       ect.ExternalCompanyType,
       ec.ExternalCompanyCode,
       ec.ExternalCompany
  from ExternalCompany      ec (nolock)
  join ExternalCompanyType ect (nolock) on ec.ExternalCompanyTypeId = ect.ExternalCompanyTypeId
 where ect.ExternalCompanyTypeCode = 'CUST'
