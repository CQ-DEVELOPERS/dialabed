﻿CREATE FUNCTION [dbo].[ufn_Culture] (@CultureId int = null)
RETURNS TABLE
AS
RETURN 
(
    select CultureId,
           CultureName as 'Culture'
      from Culture
     where CultureId = isnull(@CultureId, CultureId)
);
