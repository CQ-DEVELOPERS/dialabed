﻿CREATE FUNCTION [dbo].[ufn_IssueLine] (@IssueLineId int = null)
RETURNS TABLE
AS
RETURN 
(
    select IssueLineId,
           IssueLineId as 'IssueLine'
      from IssueLine
     where IssueLineId = isnull(@IssueLineId, IssueLineId)
);
