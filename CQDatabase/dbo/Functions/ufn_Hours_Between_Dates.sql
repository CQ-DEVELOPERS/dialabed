﻿CREATE FUNCTION dbo.ufn_Hours_Between_Dates(@StartDate datetime, @EndDate datetime, @InclWeekend bit, @WeekendType int) RETURNS int AS BEGIN

declare @Hours decimal(13,2),
		@Minutes  decimal(13,3)
		
set @Hours = 0
set @Minutes = DATEDIFF(MINUTE,@StartDate,@EndDate)

while @EndDate>=@StartDate
    begin
		if @InclWeekend = 1
		begin
		if  @WeekendType = 1 --Friday
		begin
		if  datename(weekday,@StartDate) <>'Saturday' and  datename(weekday,@StartDate)<>'Sunday'
            and (datepart(hour,@StartDate) >=0 and datepart(minute,@StartDate)>=0 )
            and (datepart(hour,@StartDate) <=23 and datepart(minute,@StartDate)>=59)
            and @Minutes > 59
            set @Hours = @Hours + 1
		end
		if  @WeekendType = 2 --Friday & Saturday
		begin
		if  datename(weekday,@StartDate)<>'Sunday'
            and (datepart(hour,@StartDate) >=0 and datepart(minute,@StartDate)>=0 )
            and (datepart(hour,@StartDate) <=23 and datepart(minute,@StartDate)>=59)
            and @Minutes > 59
            set @Hours = @Hours + 1
		end
		if  @WeekendType = 3 --Saturday Only
		begin
		if  datename(weekday,@StartDate)<>'Friday' and  datename(weekday,@StartDate)<>'Sunday'
            and (datepart(hour,@StartDate) >=0 and datepart(minute,@StartDate)>=0 )
            and (datepart(hour,@StartDate) <=23 and datepart(minute,@StartDate)>=59)
            and @Minutes > 59
            set @Hours = @Hours + 1
		end
		if  @WeekendType = 4 --Saturday & Sunday
		begin
		if  datename(weekday,@StartDate)<>'Friday' 
            and (datepart(hour,@StartDate) >=0 and datepart(minute,@StartDate)>=0 )
            and (datepart(hour,@StartDate) <=23 and datepart(minute,@StartDate)>=59)
            and @Minutes > 59
            set @Hours = @Hours + 1
		end
		if  @WeekendType = 5 --Sunday Only
		begin
		if  datename(weekday,@StartDate)<>'Friday' and  datename(weekday,@StartDate)<>'Saturday'
            and (datepart(hour,@StartDate) >=0 and datepart(minute,@StartDate)>=0 )
            and (datepart(hour,@StartDate) <=23 and datepart(minute,@StartDate)>=59)
            and @Minutes > 59
            set @Hours = @Hours + 1
		end
		if  @WeekendType = 0 --All days
		begin
		if  ((datepart(hour,@StartDate) >=0 and datepart(minute,@StartDate)>=0 )
        and (datepart(hour,@StartDate) <=23 and datepart(minute,@StartDate)>=59))
        and @Minutes > 59
            set @Hours = @Hours + 1
		end
        set @StartDate = dateadd(minute,1,@StartDate)
		end
		else
		begin
		if  datename(weekday,@StartDate) <>'Saturday' and  datename(weekday,@StartDate)<>'Sunday'
            and (datepart(hour,@StartDate) >=0 and datepart(minute,@StartDate)>=0 )
            and (datepart(hour,@StartDate) <=23 and datepart(minute,@StartDate)>=59)
			and @Minutes > 59
            begin
                   set @Hours = @Hours + 1
            end     

        set @StartDate = dateadd(minute,1,@StartDate)
		end
	end 

  return @Hours
END
