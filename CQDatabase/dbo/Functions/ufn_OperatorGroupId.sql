﻿CREATE FUNCTION dbo.ufn_OperatorGroupId(@OperatorId int)
RETURNS int
AS
BEGIN
  declare @OperatorGroupId int
  
  select @OperatorGroupId = og.OperatorGroupId
    from OperatorGroup og (nolock)
    join Operator       o (nolock) on og.OperatorGroupId = o.OperatorGroupId
   where OperatorId = @OperatorId
  
  return @OperatorGroupId
END
