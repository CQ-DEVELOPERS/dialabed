﻿CREATE FUNCTION [dbo].[ufn_InboundDocument] (@InboundDocumentId int = null)
RETURNS TABLE
AS
RETURN 
(
    select InboundDocumentId,
           OrderNumber as 'InboundDocument'
      from InboundDocument
     where InboundDocumentId = isnull(@InboundDocumentId, InboundDocumentId)
);
