﻿create function [dbo].[ufn_GetCurrentMinutes] ()
returns int 
as 
Begin
	return ((Datepart(hh,Getdate()) * 60) + DatePart(mi,Getdate()))
End
