﻿CREATE FUNCTION [dbo].[ufn_Menu] (@MenuId int = null)
RETURNS TABLE
AS
RETURN 
(
    select MenuId,
           MenuText as 'Menu'
      from Menu
     where MenuId = isnull(@MenuId, MenuId)
);
