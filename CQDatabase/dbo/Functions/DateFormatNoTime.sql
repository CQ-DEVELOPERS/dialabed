﻿Create Function [dbo].[DateFormatNoTime] (@Passdate Datetime)
Returns Datetime
AS 
Begin
	Return Convert(Datetime,Convert(Char(4),datepart(yyyy,@Passdate)) + '-' 
				+ Convert(Char(2),datepart(mm,@Passdate)) + '-' + 
				Convert(char(2),Datepart(dd,@Passdate) )) 

End
