﻿CREATE FUNCTION [dbo].[ufn_InboundLine] (@InboundLineId int = null)
RETURNS TABLE
AS
RETURN 
(
    select InboundLine.InboundLineId,
           InboundDocument.OrderNumber + ', ' + convert(varchar(10),InboundLine.LineNumber) as 'InboundLine'
      from InboundLine
      join InboundDocument on InboundLine.InboundDocumentId = InboundDocument.InboundDocumentId
     where InboundLine.InboundLineId = isnull(@InboundLineId, InboundLine.InboundLineId)
);
