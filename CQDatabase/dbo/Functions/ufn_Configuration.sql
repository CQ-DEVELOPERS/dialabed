﻿CREATE FUNCTION [dbo].[ufn_Configuration](@ConfigurationId int, @WarehouseId int)
RETURNS bit
AS
BEGIN
  declare @Indicator bit
  
  select @Indicator = Indicator
    from Configuration (nolock)
   where ConfigurationId = @ConfigurationId
     and WarehouseId = @WarehouseId
  
  if @Indicator is null
    set @Indicator = 0
  
  return @Indicator
END
