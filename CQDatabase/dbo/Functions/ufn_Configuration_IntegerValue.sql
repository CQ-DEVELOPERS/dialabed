﻿CREATE FUNCTION [dbo].[ufn_Configuration_IntegerValue](@ConfigurationId int, @WarehouseId int)
RETURNS int
AS
BEGIN
  declare @IntegerValue int
  
  select @IntegerValue = IntegerValue
    from Configuration (nolock)
   where ConfigurationId = @ConfigurationId
     and WarehouseId = @WarehouseId
  
  return @IntegerValue
END
