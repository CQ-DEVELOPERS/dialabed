﻿create function dbo.ufn_GetCurrentShiftCapacity(@Hours Int)
returns numeric(13,3)
as
Begin
	 declare @Weight numeric(13,3)
	 
	 select @Weight = sum(Weight) / count(EndDate) / 60 * (dbo.ufn_GetShiftEndtime() - dbo.ufn_GetCurrentMinutes())
	   from OperatorPerformance
	  where OperatorId in (select OperatorId from dbo.ufn_ActiveOperators(1))
     and EndDate > '2008-08-28 00:00'
  
	 return @Weight
end
