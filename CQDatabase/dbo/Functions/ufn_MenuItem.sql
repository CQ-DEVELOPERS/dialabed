﻿CREATE FUNCTION [dbo].[ufn_MenuItem] (@MenuItemId int = null)
RETURNS TABLE
AS
RETURN 
(
    select MenuItemId,
           MenuItemText as 'MenuItem'
      from MenuItem
     where MenuItemId = isnull(@MenuItemId, MenuItemId)
);
