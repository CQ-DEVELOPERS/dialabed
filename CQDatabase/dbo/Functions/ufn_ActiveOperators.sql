﻿create function dbo.ufn_ActiveOperators(@Hours int)
Returns Table 
AS

Return (Select OperatorId from Operator 
		Where datediff(hh,LastInstruction,Getdate()) < @Hours)

