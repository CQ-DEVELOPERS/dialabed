﻿--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Search') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Housekeeping_Stock_Take_Search
--    IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Search') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Housekeeping_Stock_Take_Search >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Housekeeping_Stock_Take_Search >>>'
--END
--go
 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Search
  ///   Filename       : p_Housekeeping_Stock_Take_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   @referenceNumber nvarchar(30)
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter
  ///   Modified Date  : 2018-06-21
  ///   Details        : Allowed for the level to display in the area column on stock take for the new format location Master.
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Search
(
 @WarehouseId int,
 @jobId       int,
 @location    nvarchar(15) = null
)
--with encryption
as
begin
	 set nocount on;
	 
	 create table #TableResult --Daniel Schotter: Changed syntax for improved performance
	 --declare @TableResult as table
	 (
   ReferenceNumber nvarchar(30),
   [Status]        nvarchar(50),
   JobId           int,
   PriorityId      int,
   Priority        nvarchar(50),
   OperatorId      int,
   Operator        nvarchar(50),
   Area            nvarchar(50),
   LocationId	     int,
   Location		      nvarchar(50),
   CreateDate      datetime,
   [Count]         int
	 )
	 
	 if @jobId is null or @jobId = 0
	   set @jobId = null

  if @location is null
    set @location = '%';
	 
	 if @jobId is null
    insert #TableResult
          (ReferenceNumber,
           JobId,
           Status,
           PriorityId,
           OperatorId,
           CreateDate,
           [Count],
           Area,
           LocationId)
    select j.ReferenceNumber,
           j.JobId,
           s.Status,
           j.PriorityId,
           j.OperatorId,
           max(i.CreateDate),
           count(1),
           Area,
           --min(isnull(l.[Ailse], l.Location)) as Aisle,
           min(i.PickLocationId) as LocationId
      from Job              j (nolock)
      join Status           s (nolock) on j.StatusId          = s.StatusId
      join Instruction      i (nolock) on j.JobId             = i.JobId
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Location         l (nolock) on i.PickLocationId    = l.LocationId
      join AreaLocation	   al (nolock) on l.LocationId = al.LocationId
      join Area			    a (nolock) on al.AreaId = a.AreaId
     where j.WarehouseId = @WarehouseId
       and s.Type        = 'J'
       and s.StatusCode in ('W','S','RL','RC')
       and it.InstructionTypeCode in ('STE','STL','STA','STP')
       and l.Location like @location
  group by j.ReferenceNumber,
           j.JobId,
           s.Status,
           j.PriorityId,
           j.OperatorId,
           l.Level,
           a.Area
  else
    insert #TableResult
          (ReferenceNumber,
           JobId,
           Status,
           PriorityId,
           OperatorId,
           CreateDate,
           [Count],
           Area,
           LocationId)
    select j.ReferenceNumber,
           j.JobId,
           s.Status,
           j.PriorityId,
           j.OperatorId,
           max(i.CreateDate),
           count(1),
           Area,
           min(i.PickLocationId)
      from Job              j (nolock)
      join Status           s (nolock) on j.StatusId          = s.StatusId
      join Instruction      i (nolock) on j.JobId             = i.JobId
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Location         l (nolock) on i.PickLocationId    = l.LocationId
      join AreaLocation	   al (nolock) on l.LocationId = al.LocationId
      join Area			    a (nolock) on al.AreaId = a.AreaId
     where j.WarehouseId = @WarehouseId
       and j.JobId       = @jobId
       and s.Type        = 'J'
       and s.StatusCode in ('W','S','RL','RC')
       and it.InstructionTypeCode in ('STE','STL','STA','STP')
       and l.Location like @location
  group by j.ReferenceNumber,
           j.JobId,
           s.Status,
           j.PriorityId,
           j.OperatorId,
           l.level,
           a.Area
  
  update tr
     set Priority = p.Priority
from #TableResult tr
    join Priority      p (nolock) on tr.PriorityId = p.PriorityId
  
  update tr
     set Operator = o.Operator
    from #TableResult tr
    join Operator o (nolock) on tr.OperatorId = o.OperatorId
  
   update tr
     set Location = l.Location
    from #TableResult tr
    join Location l (nolock) on tr.LocationId = l.LocationId
  
  select ReferenceNumber,
         Area,
         CreateDate,
         Status,
         [Count],
         JobId,
         PriorityId,
         Priority,
         Operator,
         Location
    from #TableResult
  order by JobId
end
 
--go
--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Search') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Housekeeping_Stock_Take_Search >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Housekeeping_Stock_Take_Search >>>'
--go
