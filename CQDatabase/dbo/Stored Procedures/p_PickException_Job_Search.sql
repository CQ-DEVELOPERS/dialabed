﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PickException_Job_Search
  ///   Filename       : p_PickException_Job_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PickException_Job_Search
(
 @OutboundShipmentId  int,
 @IssueId             int,
 @InstructionTypeCode nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   DocumentNumber nvarchar(30),
   OrderNumber    nvarchar(30),
   IssueLineId    int
  )
  
  declare @TableResult as table
  (
   DocumentNumber      nvarchar(30),
   OrderNumber         nvarchar(30),
   JobId               int,
   InstructionType     nvarchar(30),
   PriorityId          int,
   Priority            nvarchar(50),
   StatusId            int,
   Status              nvarchar(50),
   OperatorId          int,
   Operator            nvarchar(50)
  );
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is not null
    set @IssueId = null
  
  if @IssueId is not null
  begin
    insert @TableHeader
          (IssueLineId,
           OrderNumber)
    select il.IssueLineId,
           od.OrderNumber
      from IssueLine              il (nolock)
      join Issue                   i (nolock) on il.IssueId  = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where il.IssueId = @IssueId
    
    insert @TableResult
          (DocumentNumber,
           OrderNumber,
           InstructionType,
           JobId,
           PriorityId,
           StatusId,
           Status,
           OperatorId)
    select distinct th.DocumentNumber,
           th.OrderNumber,
           it.InstructionType,
           i.JobId,
           j.PriorityId,
           j.StatusId,
           s.Status,
           i.OperatorId
      from @TableHeader th
      join Instruction      i (nolock) on th.IssueLineId      = i.IssueLineId
      join Job              j (nolock) on i.JobId             = j.JobId
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Status           s (nolock) on j.StatusId = s.StatusId
     where it.InstructionTypeCode in ('PM','PS','FM','PR')
       and s.StatusCode in ('A','QA')
  end
  else if @OutboundShipmentId is not null
  begin
    insert @TableHeader
          (IssueLineId,
           DocumentNumber,
           OrderNumber)
    select il.IssueLineId,
           convert(nvarchar(30), osi.OutboundShipmentId),
           od.OrderNumber
      from OutboundShipmentIssue osi (nolock)
      join IssueLine              il (nolock) on osi.IssueId = il.IssueId
      join Issue                   i (nolock) on il.IssueId  = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where osi.OutboundShipmentId = @OutboundShipmentId
    
    insert @TableResult
          (DocumentNumber,
           OrderNumber,
           InstructionType,
           JobId,
           PriorityId,
           StatusId,
           Status,
           OperatorId)
    select distinct th.DocumentNumber,
           th.OrderNumber,
           it.InstructionType,
           i.JobId,
           j.PriorityId,
           j.StatusId,
           s.Status,
           i.OperatorId
      from @TableHeader th
      join IssueLineInstruction ili (nolock) on th.IssueLineId      = ili.IssueLineId
      join Instruction            i (nolock) on ili.InstructionId   = i.InstructionId
      join Job              j (nolock) on i.JobId             = j.JobId
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Status           s (nolock) on j.StatusId = s.StatusId
     where it.InstructionTypeCode in ('PM','PS','FM','PR')
       and s.StatusCode in ('A','QA')
  end
  
--  update tr
--     set Status = s.Status
--    from @TableResult tr
--    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority      p (nolock) on tr.PriorityId = p.PriorityId
  
  select distinct
         JobId,
         InstructionType,
         DocumentNumber,
         OrderNumber,
         Priority,
         Status,
         Operator
    from @TableResult
end
