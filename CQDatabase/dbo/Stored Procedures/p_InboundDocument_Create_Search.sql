﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Create_Search
  ///   Filename       : p_InboundDocument_Create_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   RowNumber,
  ///   InboundDocumentType,
  ///   InboundDocumentId,
  ///   OrderNumber,
  ///   DeliveryDate,
  ///   ExternalCompany,
  ///   CreateDate,
  ///   ModifiedDate,
  ///   Status
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Create_Search
(
 @InboundDocumentId int = null output,
 @PageSize          int = 10,
 @PageNumber        int = 1
)
 
as
begin
	 set nocount on;
  
	 declare @Error    int,
          @RowStart int,
          @RowEnd   int,
          @StatusId int

  if @PageNumber > 0 
  begin
    set @PageNumber = @PageNumber -1;
    set @RowStart   = @PageSize * @PageNumber + 1; 
    set @RowEnd     = @RowStart + @PageSize - 1; 
    
    if @InboundDocumentId = '-1'
      set @InboundDocumentId = null;
    
   select @StatusId = StatusId
     from Status
    where StatusCode = 'N'
      and Type       = 'ID';
    
    -- Use row_number() function
    with Result as
    (
        select 'RowNumber' = row_number() over(order by id.OrderNumber), 
               idt.InboundDocumentType,
               id.InboundDocumentId,
               id.OrderNumber,
               id.DeliveryDate,
               ec.ExternalCompany,
               id.CreateDate,
               id.ModifiedDate,
               s.Status
          from InboundDocument     id  (nolock)
          join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
          join ExternalCompany     ec  (nolock) on ec.ExternalCompanyId = id.ExternalCompanyId
          join Status              s   (nolock) on s.StatusId = id.StatusId
        where id.InboundDocumentId = isnull(@InboundDocumentId, id.InboundDocumentId)
          and s.StatusId           = @StatusId
    )
    
    select InboundDocumentType,
           InboundDocumentId,
           OrderNumber,
           DeliveryDate,
           ExternalCompany,
           CreateDate,
           ModifiedDate,
           Status
      from Result
     where RowNumber between @RowStart and @RowEnd
  end
end
