﻿/*  
  /// <summary>  
  ///   Procedure Name : p_Principal_Select_by_Operator  
  ///   Filename       : p_Principal_Select_by_Operator.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 23 Jan 2013 13:21:18  
  /// </summary>  
  /// <remarks>  
  ///   Selects rows from the Principal table.  
  /// </remarks>  
  /// <param>  
  /// </param>  
  /// <returns>  
  ///   Principal.PrincipalId,  
  ///   Principal.Principal   
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Principal_Select_by_Operator  
(
@UserName			nvarchar(50)
)

   
as  
begin  
  set nocount on;  
    
  declare @Error int  
  
 
    declare @ExternalCompanyId	int,
			@PrincipalId		int
          
    set @PrincipalId = null
    
  select @ExternalCompanyId = o.ExternalCompanyId,
         @PrincipalId       = PrincipalId
    from Operator o (nolock)
   where o.Operator = @UserName

	 
  select  
        -1 as PrincipalId  
        ,'{All}' as Principal  
  union  
  select  
         Principal.PrincipalId  
        ,Principal.Principal  
    from Principal  
    where PrincipalId = isnull(@PrincipalId, PrincipalId)
  order by Principal  
    
end  
