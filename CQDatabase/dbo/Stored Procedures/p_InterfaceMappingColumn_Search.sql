﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingColumn_Search
  ///   Filename       : p_InterfaceMappingColumn_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 10:53:58
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceMappingColumn table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingColumnId int = null output,
  ///   @InterfaceMappingColumnCode nvarchar(60) = null,
  ///   @InterfaceMappingColumn nvarchar(100) = null,
  ///   @InterfaceDocumentTypeId int = null,
  ///   @InterfaceFieldId int = null,
  ///   @InterfaceMappingFileId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceMappingColumn.InterfaceMappingColumnId,
  ///   InterfaceMappingColumn.InterfaceMappingColumnCode,
  ///   InterfaceMappingColumn.InterfaceMappingColumn,
  ///   InterfaceMappingColumn.InterfaceDocumentTypeId,
  ///   InterfaceDocumentType.InterfaceDocumentType,
  ///   InterfaceMappingColumn.InterfaceFieldId,
  ///   InterfaceField.InterfaceField,
  ///   InterfaceMappingColumn.ColumnNumber,
  ///   InterfaceMappingColumn.StartPosition,
  ///   InterfaceMappingColumn.EndPostion,
  ///   InterfaceMappingColumn.Format,
  ///   InterfaceMappingColumn.InterfaceMappingFileId 
  ///   InterfaceMappingFile.InterfaceMappingFileId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingColumn_Search
(
 @InterfaceMappingColumnId int = null output,
 @InterfaceMappingColumnCode nvarchar(60) = null,
 @InterfaceMappingColumn nvarchar(100) = null,
 @InterfaceDocumentTypeId int = null,
 @InterfaceFieldId int = null,
 @InterfaceMappingFileId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceMappingColumnId = '-1'
    set @InterfaceMappingColumnId = null;
  
  if @InterfaceMappingColumnCode = '-1'
    set @InterfaceMappingColumnCode = null;
  
  if @InterfaceMappingColumn = '-1'
    set @InterfaceMappingColumn = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
  if @InterfaceFieldId = '-1'
    set @InterfaceFieldId = null;
  
  if @InterfaceMappingFileId = '-1'
    set @InterfaceMappingFileId = null;
  
 
  select
         InterfaceMappingColumn.InterfaceMappingColumnId
        ,InterfaceMappingColumn.InterfaceMappingColumnCode
        ,InterfaceMappingColumn.InterfaceMappingColumn
        ,InterfaceMappingColumn.InterfaceDocumentTypeId
         ,InterfaceDocumentTypeInterfaceDocumentTypeId.InterfaceDocumentType as 'InterfaceDocumentType'
        ,InterfaceMappingColumn.InterfaceFieldId
         ,InterfaceFieldInterfaceFieldId.InterfaceField as 'InterfaceField'
        ,InterfaceMappingColumn.ColumnNumber
        ,InterfaceMappingColumn.StartPosition
        ,InterfaceMappingColumn.EndPostion
        ,InterfaceMappingColumn.Format
        ,InterfaceMappingColumn.InterfaceMappingFileId
    from InterfaceMappingColumn
    left
    join InterfaceDocumentType InterfaceDocumentTypeInterfaceDocumentTypeId on InterfaceDocumentTypeInterfaceDocumentTypeId.InterfaceDocumentTypeId = InterfaceMappingColumn.InterfaceDocumentTypeId
    left
    join InterfaceField InterfaceFieldInterfaceFieldId on InterfaceFieldInterfaceFieldId.InterfaceFieldId = InterfaceMappingColumn.InterfaceFieldId
    left
    join InterfaceMappingFile InterfaceMappingFileInterfaceMappingFileId on InterfaceMappingFileInterfaceMappingFileId.InterfaceMappingFileId = InterfaceMappingColumn.InterfaceMappingFileId
   where isnull(InterfaceMappingColumn.InterfaceMappingColumnId,'0')  = isnull(@InterfaceMappingColumnId, isnull(InterfaceMappingColumn.InterfaceMappingColumnId,'0'))
     and isnull(InterfaceMappingColumn.InterfaceMappingColumnCode,'%')  like '%' + isnull(@InterfaceMappingColumnCode, isnull(InterfaceMappingColumn.InterfaceMappingColumnCode,'%')) + '%'
     and isnull(InterfaceMappingColumn.InterfaceMappingColumn,'%')  like '%' + isnull(@InterfaceMappingColumn, isnull(InterfaceMappingColumn.InterfaceMappingColumn,'%')) + '%'
     and isnull(InterfaceMappingColumn.InterfaceDocumentTypeId,'0')  = isnull(@InterfaceDocumentTypeId, isnull(InterfaceMappingColumn.InterfaceDocumentTypeId,'0'))
     and isnull(InterfaceMappingColumn.InterfaceFieldId,'0')  = isnull(@InterfaceFieldId, isnull(InterfaceMappingColumn.InterfaceFieldId,'0'))
     and isnull(InterfaceMappingColumn.InterfaceMappingFileId,'0')  = isnull(@InterfaceMappingFileId, isnull(InterfaceMappingColumn.InterfaceMappingFileId,'0'))
  order by InterfaceMappingColumnCode
  
end
