﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StatusRollupLog_Search
  ///   Filename       : p_StatusRollupLog_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:40
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StatusRollupLog table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StatusRollupLog.JobId,
  ///   StatusRollupLog.JobCode,
  ///   StatusRollupLog.CreateDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StatusRollupLog_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         StatusRollupLog.JobId
        ,StatusRollupLog.JobCode
        ,StatusRollupLog.CreateDate
    from StatusRollupLog
  
end
