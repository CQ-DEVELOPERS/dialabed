﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Menu_Parameter
  ///   Filename       : p_Menu_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:57
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Menu.MenuId,
  ///   Menu.Menu 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Menu_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as MenuId
        ,'{All}' as Menu
  union
  select
         Menu.MenuId
        ,Menu.Menu
    from Menu
  
end
