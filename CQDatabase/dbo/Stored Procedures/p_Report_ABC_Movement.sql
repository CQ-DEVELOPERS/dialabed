﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_ABC_Movement
  ///   Filename       : p_Report_ABC_Movement.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_ABC_Movement
(
 @WarehouseId int,
 @FromDate    datetime,
 @ToDate      datetime,
 @UOM         nvarchar(10) = 'Units',
 @A           numeric(10) = 80,
 @B           numeric(10) = 95,
 @PrincipalId int
)
 
as
begin
	 --set nocount on;
  
  declare @Ranking            int,
          @StorageUnitId      int,
          @AreaId             int
  
  declare @TableResult as table
  (
   Warehouse       nvarchar(50),
   Ranking         int,
   StorageUnitId   int,
   ProductCode     nvarchar(30),
   Product         nvarchar(255),
   SKUCode         nvarchar(50),
   UOM             numeric(13,3),
   TotalMonthly    numeric(13,3),
   Cumulative      numeric(13,3),
   CumulativeItems numeric(13,3),
   Category        char(1),
   AreaId          int,
   Area            nvarchar(50),
   Lines           numeric(13,3),
   UnitWeight      numeric(13,3),
   FullPalletQty   int,
   PrincipalId	   int,
   PrincipalCode   nvarchar(50)
  )
  
  declare @TableTemp as table
  (
   StorageUnitId   int,
   ProductCode     nvarchar(30),
   Product         nvarchar(255),
   SKUCode         nvarchar(50),
   UOM             numeric(13,3),
   TotalMonthly    numeric(13,3),
   Cumulative      numeric(13,3),
   CumulativeItems numeric(13,3),
   Category        char(1),
   AreaId          int,
   Area            nvarchar(50),
   Lines           numeric(13,3),
   PrincipalId	   int,
   PrincipalCode   nvarchar(50)
  )
  
  if @PrincipalId = -1
	set @PrincipalId = null
	
--  insert @TableResult
--        (StorageUnitId,
--         ProductCode,
--         Product,
--         SKUCode,
--         AreaId,
--         Area)
--  select distinct
--         vs.StorageUnitId,
--         vs.ProductCode,
--         vs.Product,
--         vs.SKUCode,
--         vl.AreaId,
--         vl.Area
--    from viewStock                  vs
--    join StorageUnitBatchLocation subl on vs.StorageUnitBatchId = subl.StorageUnitBatchId
--    join viewLocation               vl on subl.LocationId       = vl.LocationId
--   where vl.WarehouseId = @WarehouseId
--     and vl.StockOnHand = 1
--  order by vs.ProductCode, vl.Area
  
  insert @TableResult
        (StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         PrincipalId)
  select distinct
         StorageUnitId,
         vs.ProductCode,
         vs.Product,
         SKUCode,
         p.PrincipalId
    from viewStock vs
    join Product p on p.ProductId = vs.ProductId
    where isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
  order by ProductCode  
  
  update tr
     set AreaId = sua.AreaId,
         Area   = sua.Area
    from @TableResult               tr
    join viewSUA                   sua on tr.StorageUnitId = sua.StorageUnitId
   where sua.WarehouseId = @WarehouseId
     and sua.StockOnHand = 1
     and sua.AreaCode in ('RK','BK')
     
  
  
    update tr
     set FullPalletQty = (select max(pk.Quantity)
			from StorageUnit           su (nolock) 
			join Pack                  pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
			join PackType              pt (nolock) on pk.PackTypeId = pt.PackTypeId
			where su.StorageUnitId = tr.StorageUnitId
			and PackType = 'Pallet')
    from @TableResult            tr
    
 
  update tr
     set AreaId = vl.AreaId,
         Area   = vl.Area
    from @TableResult               tr
    join StorageUnitBatch          sub on tr.StorageUnitId      = sub.StorageUnitId
    join StorageUnitBatchLocation subl on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join viewLocation               vl on subl.LocationId       = vl.LocationId
   where vl.WarehouseId = @WarehouseId
     and vl.StockOnHand = 1
     and vl.AreaCode in ('RK','BK')
     and tr.AreaId is null
  
  update tr
     set AreaId = vl.AreaId,
         Area   = vl.Area
    from @TableResult               tr
    join StorageUnitBatch          sub on tr.StorageUnitId      = sub.StorageUnitId
    join StorageUnitBatchLocation subl on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join viewLocation               vl on subl.LocationId       = vl.LocationId
   where vl.WarehouseId = @WarehouseId
     and vl.StockOnHand = 1
     and vl.AreaCode = 'SP'
     and tr.AreaId is null
  
  delete @TableResult where AreaId is null
  
  select	sub.StorageUnitId, 
			sum(i.ConfirmedQuantity) as 'ConfirmedQuantity', 
			sum(i.ConfirmedWeight) as 'ConfirmedWeight', 
			count(1) as 'NumberOfLines' 
    into #temp
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status             s (nolock) on i.StatusId          = s.StatusId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join @TableResult      tr on tr.StorageUnitId = sub.StorageUnitId
   --where it.InstructionTypeCode in ('P','PM','PS','FM','M','R')
   where it.InstructionTypeCode in ('P','PM','PS','FM')
     and s.StatusCode            = 'F'
     and i.ConfirmedQuantity     > 0
     and i.EndDate         between @FromDate and @ToDate
   group by sub.StorageUnitId, FullPalletQty, i.ConfirmedQuantity 
  
 
  update #temp
     set NumberOfLines = (select COUNT(1)
		from Instruction        i (nolock)
		join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId
		join Status             s (nolock) on i.StatusId          = s.StatusId
		join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
		join @TableResult      tr on tr.StorageUnitId = tt.StorageUnitId
	    where tr.FullPalletQty = i.Quantity
	    and tr.StorageUnitId = sub.StorageUnitId
	    --and it.InstructionTypeCode in ('P','PM','PS','FM','M','R')
	    and it.InstructionTypeCode in ('P','PM','PS','FM')
		and s.StatusCode            = 'F'
		and i.ConfirmedQuantity     > 0
		and i.EndDate         between @FromDate and @ToDate)
    from #temp           tt
    
  
  if @UOM = 'Units'
  begin
    update tr
       set UOM = ConfirmedQuantity
      from @TableResult tr
      join #temp         t on tr.StorageUnitId = t.StorageUnitId
  end
  else if @UOM = 'Weight'
  begin
    update tr
       set UnitWeight = pk.Weight
      from @TableResult tr
      join StorageUnit             su (nolock) on su.StorageUnitId = tr.StorageUnitId
	  join Pack                    pk (nolock) on su.StorageUnitId = pk.StorageUnitId
	  join PackType                pt (nolock) on pk.PackTypeId = pt.PackTypeId
	  where pt.PackType = 'Unit'
    update tr
       set UOM = ConfirmedQuantity * UnitWeight
      from @TableResult tr
      join #temp         t on tr.StorageUnitId = t.StorageUnitId
  end
  else if @UOM = 'Orders'
  begin
    update tr
       set UOM = ConfirmedQuantity
      from @TableResult tr
      join #temp         t on tr.StorageUnitId = t.StorageUnitId
  end
  else if @UOM = 'Full'
  begin
    update tr
       set UOM = NumberOfLines
      from @TableResult tr
      join #temp         t on tr.StorageUnitId = t.StorageUnitId
  end
  
  update @TableResult
     set UOM = 0
   where UOM is null
  
  
SET ARITHABORT OFF
SET ANSI_WARNINGS OFF  

  update @TableResult
     set TotalMonthly = isnull(((UOM / (select sum(UOM) from @TableResult)) * 100.000),0)
     
 
  set @Ranking = 0
  set rowcount 1
  
  while exists (select top 1 1 from @TableResult where Ranking is null)
  begin
    select @Ranking = @Ranking + 1
    
    select top 1
           @StorageUnitId = StorageUnitId,
           @AreaId        = AreaId
      from @TableResult
     where Ranking is null
    order by UOM desc
    
    update @TableResult
       set Ranking = @Ranking,
           Cumulative = TotalMonthly + isnull((select max(Cumulative) from @TableResult),0)
     where StorageUnitId = @StorageUnitId
       and AreaId        = @AreaId
  end
  
  set rowcount 0
  
--  set rowcount 1
--  
--  while exists (select top 1 1 from @TableResult where Cumulative is null)
--  begin
--    update @TableResult
--       set Cumulative = isnull(TotalMonthly,0) + isnull((select max(Cumulative) from @TableResult),0)
--     where Cumulative is null
--  end
--  
--  set rowcount 0
  
  update @TableResult
     set CumulativeItems = (convert(numeric(13,3), Ranking) / (select convert(numeric(13,3), max(Ranking)) from @TableResult)) * 100.000
  
  update @TableResult
     set Category = 'A'
   where Cumulative < @A
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'A')
   where Lines is null
     and Category = 'A'
  set rowcount 0
  
  update @TableResult
     set Category = 'B'
   where Cumulative between @A and @B
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'B') - (select max(CumulativeItems) from @TableResult where Category = 'A')
   where Lines is null
     and Category = 'B'
  set rowcount 0
  
  update @TableResult
     set Category = 'C'
   where Cumulative > @B
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'C') - (select max(CumulativeItems) from @TableResult where Category = 'B')
   where Lines is null
     and Category = 'C'
  set rowcount 0
  
  update @TableResult
     set TotalMonthly = 100
   where TotalMonthly > 100
  
  update @TableResult
     set Cumulative = 100
   where Cumulative > 100
  
  update @TableResult
     set CumulativeItems = 100
   where CumulativeItems > 100
  
  update @TableResult
     set Warehouse = w.Warehouse
    from Warehouse w
   where w.WarehouseId = @WarehouseId
   
  update @TableResult
     set PrincipalCode = p.PrincipalCode
     from @TableResult tr
    join Principal p on tr.PrincipalId = p.PrincipalId
      
  select Warehouse,
         Ranking,
         ProductCode,
         Product,
         SKUCode,
         UOM,
         TotalMonthly,
         convert(numeric(13,2), Cumulative) as 'Cumulative',
         convert(numeric(13,2), CumulativeItems) as 'CumulativeItems',
         Category,
         Area,
         Lines,
         PrincipalCode
    from @TableResult
  order by Ranking
end
