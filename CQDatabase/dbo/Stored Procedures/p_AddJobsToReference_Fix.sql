﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AddJobsToReference_Fix
  ///   Filename       : p_AddJobsToReference_Fix.sql
  ///   Create By      : Karen
  ///   Date Created   : April 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AddJobsToReference_Fix
 
as
begin
	 set nocount on;
  
  Declare @ReferenceNumber int

 set @ReferenceNumber = 9

	
Declare @JobsResult as  Table 
		(JobId int)


Insert into @JobsResult 
			(JobId)
select distinct j.JobId from [Plascon].[dbo].[Job] j
join [Plascon].[dbo].[Instruction] i on i.JobId = j.JobId
join [Plascon].[dbo].[InstructionType] it on it.InstructionTypeId = i.InstructionTypeId
where it.InstructionTypeCode in ('STL', 'STE', 'STA', 'STP')
and CreateDate between '2012-04-21 00:00' and '2012-04-23 06:00'

INSERT INTO StockTakeReferenceJob
           (StockTakeReferenceId
           ,JobId
           ,StatusCode)
select @ReferenceNumber
        ,jr.JobId
        ,'Closed'
   from @JobsResult  jr
   where (not exists(select 1 from StockTakeReferenceJob  strj where strj.JobId = jr.JobId))
   
end
