﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Weight_Check
  ///   Filename       : p_Pallet_Weight_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Weight_Check
(
 @jobId       int
)
 
as
begin
	 set nocount on;
declare @Result				int,
		@NettWeight			decimal,
		@ProductCategory	char(1)
  
 select	@NettWeight = j.NettWeight,
		@ProductCategory = su.ProductCategory
 from   job j
 join	Instruction i        (nolock) on i.JobId = j.JobId
 join	StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
 join	StorageUnit su       (nolock) on sub.StorageUnitId = su.StorageUnitId
 where  @jobId	= j.JobId
 
 set @Result = 1
 
 if  @ProductCategory = 'V'
 and @NettWeight <= 0
	set @Result = 0
	

  select @Result
  return @Result
 
end

