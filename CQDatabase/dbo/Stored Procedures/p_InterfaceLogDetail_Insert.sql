﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLogDetail_Insert
  ///   Filename       : p_InterfaceLogDetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:27
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceLogDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogDetailId int = null output,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @ReceivedByHost datetime = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceLogDetail.InterfaceLogDetailId,
  ///   InterfaceLogDetail.OrderNumber,
  ///   InterfaceLogDetail.ReceivedByHost,
  ///   InterfaceLogDetail.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLogDetail_Insert
(
 @InterfaceLogDetailId int = null output,
 @OrderNumber nvarchar(60) = null,
 @ReceivedByHost datetime = null,
 @ProcessedDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceLogDetailId = '-1'
    set @InterfaceLogDetailId = null;
  
  if @OrderNumber = '-1'
    set @OrderNumber = null;
  
	 declare @Error int
 
  insert InterfaceLogDetail
        (OrderNumber,
         ReceivedByHost,
         ProcessedDate)
  select @OrderNumber,
         @ReceivedByHost,
         @ProcessedDate 
  
  select @Error = @@Error, @InterfaceLogDetailId = scope_identity()
  
  
  return @Error
  
end
