﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroup_Select
  ///   Filename       : p_OperatorGroup_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2012 07:59:19
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null 
  /// </param>
  /// <returns>
  ///   OperatorGroup.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup,
  ///   OperatorGroup.RestrictedAreaIndicator,
  ///   OperatorGroup.OperatorGroupCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroup_Select
(
 @OperatorGroupId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  select
         OperatorGroup.OperatorGroupId,
         OperatorGroup.OperatorGroup,
         OperatorGroup.RestrictedAreaIndicator,
         OperatorGroup.OperatorGroupCode 
    from OperatorGroup
   where isnull(OperatorGroup.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(OperatorGroup.OperatorGroupId,'0'))
  
end
