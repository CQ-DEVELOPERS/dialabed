﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ApplicationExceptionHistory_Insert
  ///   Filename       : p_ApplicationExceptionHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:35
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ApplicationExceptionHistory table.
  /// </remarks>
  /// <param>
  ///   @ApplicationExceptionId int = null,
  ///   @PriorityLevel int = null,
  ///   @Module nvarchar(100) = null,
  ///   @Page nvarchar(100) = null,
  ///   @Method nvarchar(200) = null,
  ///   @ErrorMsg ntext = null,
  ///   @CreateDate datetime = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   ApplicationExceptionHistory.ApplicationExceptionId,
  ///   ApplicationExceptionHistory.PriorityLevel,
  ///   ApplicationExceptionHistory.Module,
  ///   ApplicationExceptionHistory.Page,
  ///   ApplicationExceptionHistory.Method,
  ///   ApplicationExceptionHistory.ErrorMsg,
  ///   ApplicationExceptionHistory.CreateDate,
  ///   ApplicationExceptionHistory.CommandType,
  ///   ApplicationExceptionHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ApplicationExceptionHistory_Insert
(
 @ApplicationExceptionId int = null,
 @PriorityLevel int = null,
 @Module nvarchar(100) = null,
 @Page nvarchar(100) = null,
 @Method nvarchar(200) = null,
 @ErrorMsg ntext = null,
 @CreateDate datetime = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ApplicationExceptionHistory
        (ApplicationExceptionId,
         PriorityLevel,
         Module,
         Page,
         Method,
         ErrorMsg,
         CreateDate,
         CommandType,
         InsertDate)
  select @ApplicationExceptionId,
         @PriorityLevel,
         @Module,
         @Page,
         @Method,
         @ErrorMsg,
         @CreateDate,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
