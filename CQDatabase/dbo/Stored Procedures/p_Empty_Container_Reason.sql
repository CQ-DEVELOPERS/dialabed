﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Empty_Container_Reason
  ///   Filename       : p_Empty_Container_Reason.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Empty_Container_Reason
(
 @palletId    int,
 @reasonId    int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @reasonId < 0
    set @reasonId = null
  
  begin transaction
  
  exec @Error = p_Pallet_Update
   @PalletId   = @palletId,
   @ReasonId   = @reasonId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Empty_Container_Reason'
    rollback transaction
    return @Error
end
