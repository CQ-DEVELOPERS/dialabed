﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QuestionAnswers_Delete
  ///   Filename       : p_QuestionAnswers_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:28
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the QuestionAnswers table.
  /// </remarks>
  /// <param>
  ///   @QuestionAnswersId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QuestionAnswers_Delete
(
 @QuestionAnswersId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete QuestionAnswers
   where QuestionAnswersId = @QuestionAnswersId
  
  select @Error = @@Error
  
  
  return @Error
  
end
