﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Company_Search
  ///   Filename       : p_Company_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:51
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Company table.
  /// </remarks>
  /// <param>
  ///   @CompanyId int = null output,
  ///   @Company nvarchar(100) = null,
  ///   @CompanyCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Company.CompanyId,
  ///   Company.Company,
  ///   Company.CompanyCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Company_Search
(
 @CompanyId int = null output,
 @Company nvarchar(100) = null,
 @CompanyCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @CompanyId = '-1'
    set @CompanyId = null;
  
  if @Company = '-1'
    set @Company = null;
  
  if @CompanyCode = '-1'
    set @CompanyCode = null;
  
 
  select
         Company.CompanyId
        ,Company.Company
        ,Company.CompanyCode
    from Company
   where isnull(Company.CompanyId,'0')  = isnull(@CompanyId, isnull(Company.CompanyId,'0'))
     and isnull(Company.Company,'%')  like '%' + isnull(@Company, isnull(Company.Company,'%')) + '%'
     and isnull(Company.CompanyCode,'%')  like '%' + isnull(@CompanyCode, isnull(Company.CompanyCode,'%')) + '%'
  
end
