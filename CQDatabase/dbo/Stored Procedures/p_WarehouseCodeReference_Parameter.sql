﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WarehouseCodeReference_Parameter
  ///   Filename       : p_WarehouseCodeReference_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 09:52:57
  /// </summary>
  /// <remarks>
  ///   Selects rows from the WarehouseCodeReference table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   WarehouseCodeReference.WarehouseCodeReferenceId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WarehouseCodeReference_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as WarehouseCodeReferenceId
        ,null as 'WarehouseCodeReference'
  union
  select
         WarehouseCodeReference.WarehouseCodeReferenceId
        ,WarehouseCodeReference.WarehouseCodeReferenceId as 'WarehouseCodeReference'
    from WarehouseCodeReference
  
end
