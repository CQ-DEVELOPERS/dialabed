﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_ReceivingNotification_Subscription
  ///   Filename       : p_Report_ReceivingNotification_Subscription.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_ReceivingNotification_Subscription
 
as
begin
	 set nocount on;
	 
	 declare @ReceiptId		int,
			 @ReportTypeId	int
	
	select @ReportTypeId = rt.reporttypeid
	from   ReportType    rt (nolock)
	where  rt.reporttypecode = 'REC'
	
	
	insert into ReportPrinted 
			   (ReportTypeId, 
				ReceiptId, 
				PrintedCopies,
				CreateDate)
	select      @ReportTypeId, 
				ReceiptId, 
				0,
				GETDATE() 
	  from Receipt               r (nolock)
	  join InboundDocument      id (nolock) on r.InboundDocumentId = id.InboundDocumentId
	  join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
	 where idt.InboundDocumentTypeCode in ('RET')
	   and id.DeliveryDate > '2014-12-18'
	   and r.statusid in (dbo.ufn_StatusId('R','RC'))
	   and not exists (select top 1 ReceiptId, ReportTypeId 
						 from ReportPrinted rp 
						where rp.ReceiptId = r.ReceiptId
						and rp.ReportTypeId = @ReportTypeId)
	 
 
 select (select Value 
			  from Configuration 
			 where ConfigurationId = 200 
			   and WarehouseId = WarehouseId)    as 'ServerName',
			db_name()							 as 'DatabaseName',
			'Auto Email'						 as 'UserName',
			-1									 as InboundShipmentId,
			-1									 as ConnectionString,
			rp.ReceiptId,
			cl.EMail							 as 'Email',
			cl.EMail							 as 'TO',
			'Receiving Notification'			 as 'Subject'
	  from ReportPrinted         rp (nolock)
	  join ReportType            rt (nolock) on rp.ReportTypeId = rt.ReportTypeId
	  join ContactList           cl (nolock) on cl.ReportTypeId = @ReportTypeId
	 where @ReportTypeId = rt.reporttypeid
	 and PrintedCopies = 0
	 
	update  ReportPrinted
	   set  PrintedCopies = 1,
	        EmailDate = getdate()
	 where  PrintedCopies = 0
	 and    ReportTypeId = @ReportTypeId	
   
end 
