﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_List
  ///   Filename       : p_StorageUnitBatch_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitBatch table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnitBatch.StorageUnitBatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as StorageUnitBatchId
        ,null as 'StorageUnitBatch'
  union
  select
         StorageUnitBatch.StorageUnitBatchId
        ,StorageUnitBatch.StorageUnitBatchId as 'StorageUnitBatch'
    from StorageUnitBatch
  
end
