﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingFile_List
  ///   Filename       : p_InterfaceMappingFile_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 09:03:35
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceMappingFile table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceMappingFile.InterfaceMappingFileId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingFile_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceMappingFileId
        ,null as 'InterfaceMappingFile'
  union
  select
         InterfaceMappingFile.InterfaceMappingFileId
        ,InterfaceMappingFile.InterfaceMappingFileId as 'InterfaceMappingFile'
    from InterfaceMappingFile
  
end
