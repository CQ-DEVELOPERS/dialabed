﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Update
  ///   Filename       : p_Configuration_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 12:12:00
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Configuration table.
  /// </remarks>
  /// <param>
  ///   @ConfigurationId int = null,
  ///   @WarehouseId int = null,
  ///   @ModuleId int = null,
  ///   @Configuration nvarchar(510) = null,
  ///   @Indicator bit = null,
  ///   @IntegerValue int = null,
  ///   @Value sql_variant = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Update
(
 @ConfigurationId int = null,
 @WarehouseId int = null,
 @ModuleId int = null,
 @Configuration nvarchar(510) = null,
 @Indicator bit = null,
 @IntegerValue int = null,
 @Value sql_variant = null 
)
 
as
begin
	 set nocount on;
  
  if @ConfigurationId = '-1'
    set @ConfigurationId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ModuleId = '-1'
    set @ModuleId = null;
  
  if @Configuration = '-1'
    set @Configuration = null;
  
	 declare @Error int
 
  update Configuration
     set ModuleId = isnull(@ModuleId, ModuleId),
         Configuration = isnull(@Configuration, Configuration),
         Indicator = isnull(@Indicator, Indicator),
         IntegerValue = isnull(@IntegerValue, IntegerValue),
         Value = isnull(@Value, Value) 
   where ConfigurationId = @ConfigurationId
     and WarehouseId = @WarehouseId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
