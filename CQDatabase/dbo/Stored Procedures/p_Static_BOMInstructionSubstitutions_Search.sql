﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_BOMInstructionSubstitutions_Search
  ///   Filename       : p_Static_BOMInstructionSubstitutions_Search.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>	 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Static_BOMInstructionSubstitutions_Search
(
     @BOMInstructionId int
    ,@BOMLineId int
)
--with encryption
as
begin
	 set nocount on;

	SELECT	 bp.LineNumber
            ,ISNULL(bil.Quantity, 0)    As Quantity
            ,ISNULL(bil.Quantity, 0)    As CurrentSets
            ,bi.Quantity                As OrderSets
            ,bi.BOMInstructionId
            ,bh.BOMHeaderId
            ,bl.BOMLineId
	FROM	BOMInstruction bi
	        Inner join BOMHeader bh on bi.BOMHeaderId = bh.BOMHeaderId
	        Inner join BOMLine bl on bh.BOMHeaderId = bl.BOMHeaderId
			Inner join BOMProduct bp on bl.BOMLineId = bp.BOMLineId			
	        Left join BOMInstructionLine bil on bi.BOMInstructionId = bil.BOMInstructionId
	                                        and bl.BOMLineId = bil.BOMLineId
	                                        and bp.LineNumber = bil.LineNumber
    WHERE   bi.BOMInstructionId = @BOMInstructionId
       AND  bl.BOMLineId = @BOMLineId
    GROUP BY
             bp.LineNumber
            ,bil.Quantity            
            ,bi.Quantity
            ,bi.BOMInstructionId
            ,bh.BOMHeaderId
            ,bl.BOMLineId
    ORDER BY bp.LineNumber
            
 
end
