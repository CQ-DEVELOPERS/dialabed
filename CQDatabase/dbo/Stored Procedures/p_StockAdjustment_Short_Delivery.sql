﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockAdjustment_Short_Delivery
  ///   Filename       : p_StockAdjustment_Short_Delivery.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jan 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockAdjustment_Short_Delivery
(
 @ReceiptId int
,@InboundDocumentTypeCode nvarchar(30)
)
 
as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   Id int identity
  ,StorageUnitBatchId int
  ,Quantity numeric(13,6)
  ,Remarks nvarchar(255)
  )
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
         ,@Counter int
         ,@LocationId int
         ,@FromWarehouseCode nvarchar(30)
         ,@OperatorId int
         ,@StorageUnitBatchId int
         ,@Quantity numeric(13,6)
         ,@Remarks nvarchar(255)
         ,@Id int
  
  set @trancount = @@trancount;
  
  select @LocationId = LocationId
        ,@OperatorId = OperatorId
    from Receipt (nolock)
   where ReceiptId = @ReceiptId
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_StockAdjustment_Short_Delivery;
    
		  -- Do the actual work here
     select @FromWarehouseCode = a.WarehouseCode
       from AreaLocation al (nolock)
       join Area a (nolock) on al.AreaId = a.AreaId
      where al.LocationId = @LocationId
     
     insert @TableResult
           (StorageUnitBatchId
           ,Quantity
           ,Remarks)
     select rl.StorageUnitBatchId
           ,case when @InboundDocumentTypeCode = 'IBT'
                 then sum(rl.RequiredQuantity - rl.AcceptedQuantity)
                 else sum(rl.AcceptedQuantity)
                 end
           ,case when sum(rl.RequiredQuantity - rl.AcceptedQuantity) > 0
                 then 'Order: ' + id.OrderNumber + ' Line Number:' + convert(nvarchar(10), il.LineNumber) + ' was over'
                 else 'Order: ' + id.OrderNumber + ' Line Number:' + convert(nvarchar(10), il.LineNumber) + ' was short'
                 end
       from ReceiptLine rl (nolock) 
       join InboundLine il (nolock) on rl.InboundLineId = il.InboundLineId
       join InboundDocument id (nolock) on il.InboundDocumentId = id.InboundDocumentId
      where rl.RequiredQuantity != case when @InboundDocumentTypeCode = 'RET'
                                        then rl.RequiredQuantity * 2 -- never equal unless 0
                                        when @InboundDocumentTypeCode = 'IBT'
                                        then rl.AcceptedQuantity
                                        end
        and rl.ReceiptId = @ReceiptId
      group by rl.StorageUnitBatchId
              ,id.OrderNumber
              ,il.LineNumber
     
     select @Counter = @@ROWCOUNT
     
     while @Counter > 0
     begin
       select @Counter = @Counter - 1
       
       select top 1 @Id = Id
             ,@StorageUnitBatchId = StorageUnitBatchId
             ,@Quantity = Quantity
             ,@Remarks = Remarks
         from @TableResult
       
       delete @TableResult where Id = @Id
       
       exec p_StockAdjustment_Insert
        @operatorId         = @operatorId
       ,@recordType         = 'PRD'
       ,@storageUnitBatchId = @StorageUnitBatchId
       ,@quantity           = @Quantity
       ,@fromWarehouseCode  = @FromWarehouseCode
       ,@Msg                = @Remarks
	   end
	   
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Wizard_Receipt_Insert;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
 
 
