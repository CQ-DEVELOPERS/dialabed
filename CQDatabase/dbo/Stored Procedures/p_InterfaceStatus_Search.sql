﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceStatus_Search
  ///   Filename       : p_InterfaceStatus_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:43
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceStatus table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceStatus.StatusId,
  ///   InterfaceStatus.StatusCode,
  ///   InterfaceStatus.StatusDescription 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceStatus_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceStatus.StatusId
        ,InterfaceStatus.StatusCode
        ,InterfaceStatus.StatusDescription
    from InterfaceStatus
  
end
