﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Picking_Workload_Summary
  ///   Filename       : p_Report_Picking_Workload_Summary.sql
  ///   Create By      : Karen
  ///   Date Created   : February 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Picking_Workload_Summary
(
 @WarehouseId            int,
 @FromDate               datetime,
 @ToDate                 datetime,
 @PrincipalId            int = null
)
 
as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   Wave                      nvarchar(50),
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   InstructionId			 int,
   InstructionTypeId		 int,
   InstructionType			 nvarchar(30),
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   ExternalCompanyId         int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   RouteId                   int,
   Route                     nvarchar(50),
   NumberOfLines             int,
   ShortPicks                int,
   DeliveryDate              datetime,
   CreateDate                datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   PriorityId                int,
   Priority                  nvarchar(50),
   LocationId                int,
   Location                  nvarchar(15),
   Rating                    int,
   AvailabilityIndicator     nvarchar(20),
   Remarks                   nvarchar(255),
   Total                     numeric(13,3),
   Complete                  numeric(13,3),
   PercentageComplete        numeric(13,3),
   Units                     int,
   Releases                  int,
   Weight                    float,
   Orderby                   int,
   PrincipalId               int,
   PrincipalCode             nvarchar(30),
   AreaId					 int,
   Area						 nvarchar(50)
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  
  if @PrincipalId = -1
    set @PrincipalId = null
  
    insert @TableResult
          (Wave,
           OutboundShipmentId,
           OutboundDocumentId,   
		   IssueId,
           LocationId,
           OrderNumber,
           ExternalCompanyId,
           CustomerCode,
           Customer,
           RouteId,
           Route,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           OutboundDocumentType,
           Rating,
           Remarks,
           NumberOfLines,
           ShortPicks,
           Total,
           Complete,
           Units,
           Releases,
           Weight,   
           PrincipalId)
    select w.Wave,
           osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           od.OrderNumber,
           od.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           isnull(os.RouteId, i.RouteId),
           os.Route,
           isnull(os.StatusId, i.StatusId),
           s.Status,
           i.PriorityId,
           isnull(os.ShipmentDate, i.DeliveryDate),
           od.CreateDate,
           odt.OutboundDocumentType,
           null,
           i.Remarks,
           i.NumberOfLines,
           i.ShortPicks,
           i.Total,
           i.Complete,
           i.Units,
           i.Releases,
           i.Weight,
           od.PrincipalId
      from OutboundDocument      od (nolock)
      join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      left 
      join OutboundShipmentIssue osi (nolock) on i.IssueId                = osi.IssueId
      left
      join OutboundShipment       os (nolock) on osi.OutboundShipmentId   = os.OutboundShipmentId     
	  join Status                 s (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      left
      join Wave                    w (nolock) on i.WaveId                 = w.WaveId
     where (isnull(os.ShipmentDate, isnull(i.DeliveryDate, od.CreateDate)) between @FromDate and @ToDate   
		or   od.CreateDate                                                  between @FromDate and @ToDate)
       and s.Type                     = 'IS'
       and s.StatusCode              in ('M','RL','PC','PS','CK','A','WC','QA','S')
       and i.WarehouseId              = @WarehouseId
       and isnull(od.PrincipalId, -1) = isnull(@PrincipalId, isnull(od.PrincipalId, -1))
       and odt.OutboundDocumentTypeCode not in ('REP','KIT')
  
  update tr
     set InstructionId = i.InstructionId,
		 InstructionTypeId = it.InstructionTypeId,
		 InstructionType = it.InstructionType,
		 LocationId = i.PickLocationId
    from @TableResult    tr
    join IssueLine		 il (nolock) on tr.IssueId = il.IssueId
    join Instruction      i (nolock) on il.IssueLineId = i.IssueLineId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    
   
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set Location = l.Location,
		 AreaId   = al.AreaId,
		 Area     = a.Area
    from @TableResult  tr
    join Location       l (nolock) on tr.LocationId = l.LocationId
    join AreaLocation  al (nolock) on tr.LocationId = al.LocationId
    join Area		    a (nolock) on al.AreaId = a.AreaId
  
  update tr
     set Priority = p.Priority,
         OrderBy  = p.OrderBy
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal     p (nolock) on tr.PrincipalId = p.PrincipalId
  

  update @TableResult
     set PercentageComplete = (Complete / Total) * 100
   where Complete > 0
     and Total    > 0
  
  update @TableResult

     set PercentageComplete = 0
   where PercentageComplete is null
  
  if dbo.ufn_Configuration(286, @warehouseId) = 1 -- WIP - show only route
    select Wave,
           min(IssueId) as 'IssueId',
           isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
           case when OutboundShipmentId is null then OrderNumber end as 'OrderNumber',
           case when OutboundShipmentId is null then CustomerCode end as 'CustomerCode',
           case when OutboundShipmentId is null then Customer end as 'Customer',
           isnull(RouteId,-1) as 'RouteId',
           Route,
           sum(NumberOfLines) as 'NumberOfLines',
           sum(ShortPicks) as 'ShortPicks',
           sum(ShortPicks) as UnitShort,
           min(DeliveryDate) as 'DeliveryDate',
           min(CreateDate) as 'CreateDate',
           Status,
           PriorityId,
           Priority,
           OutboundDocumentType,
           isnull(LocationId,-1) as 'LocationId',
           Location,
           Rating,
           AvailabilityIndicator,
           Remarks,
           sum(Complete) as 'Complete',
           sum(Total) as 'Total',
           sum(convert(int, round(PercentageComplete,0))) / count(1) as 'PercentageComplete',
           sum(Units) as 'Units',
           sum(Releases) as 'Releases',
           sum(Weight) as 'Weight',
           PrincipalCode,
           Area,
           InstructionType
      from @TableResult
     group by Wave,
              isnull(OutboundShipmentId, -1),
              case when OutboundShipmentId is null then OrderNumber end,
         
     case when OutboundShipmentId is null then CustomerCode end,
           case when OutboundShipmentId is null then Customer end,
           isnull(RouteId,-1),
              Route,
              Status,
              PriorityId,
              Priority,
              OutboundDocumentType,
              isnull(LocationId,-1),
              Location,
              Rating,
              AvailabilityIndicator,
              Remarks,
              PrincipalCode,
              Area,
              InstructionType
    order by  Area,
			  Location,
              OutboundShipmentId,
              OrderNumber
  else
    select Area,
		   isnull(LocationId,-1) as 'LocationId',
		   Location,
		   OutboundDocumentType,
		   Wave,
		   CustomerCode,
		   Customer, 
		   DeliveryDate,
		   isnull(RouteId,-1) as 'RouteId',
           Route,		   
           IssueId,
           isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
           OrderNumber,
           CustomerCode,
           Customer,           
           NumberOfLines,
           ShortPicks,
           ShortPicks as UnitShort,
           Complete,
           Total,
           convert(int, round(PercentageComplete,0)) as 'PercentageComplete',
           Units,
           Releases,
           Weight, 
           CreateDate,
           Status,
           PrincipalCode,
           InstructionType
      from @TableResult
  order by Area,
		   Location,
           OrderBy,
           OutboundShipmentId,
           OrderNumber
end 
