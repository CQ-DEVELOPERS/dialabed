﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Search
  ///   Filename       : p_Exception_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:05
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Exception table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null output,
  ///   @Exception nvarchar(510) = null,
  ///   @ExceptionCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Exception.ExceptionId,
  ///   Exception.ReceiptLineId,
  ///   Exception.InstructionId,
  ///   Exception.IssueLineId,
  ///   Exception.ReasonId,
  ///   Exception.Exception,
  ///   Exception.ExceptionCode,
  ///   Exception.CreateDate,
  ///   Exception.OperatorId,
  ///   Exception.ExceptionDate,
  ///   Exception.JobId,
  ///   Exception.Detail,
  ///   Exception.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Search
(
 @ExceptionId int = null output,
 @Exception nvarchar(510) = null,
 @ExceptionCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ExceptionId = '-1'
    set @ExceptionId = null;
  
  if @Exception = '-1'
    set @Exception = null;
  
  if @ExceptionCode = '-1'
    set @ExceptionCode = null;
  
 
  select
         Exception.ExceptionId
        ,Exception.ReceiptLineId
        ,Exception.InstructionId
        ,Exception.IssueLineId
        ,Exception.ReasonId
        ,Exception.Exception
        ,Exception.ExceptionCode
        ,Exception.CreateDate
        ,Exception.OperatorId
        ,Exception.ExceptionDate
        ,Exception.JobId
        ,Exception.Detail
        ,Exception.Quantity
    from Exception
   where isnull(Exception.ExceptionId,'0')  = isnull(@ExceptionId, isnull(Exception.ExceptionId,'0'))
     and isnull(Exception.Exception,'%')  like '%' + isnull(@Exception, isnull(Exception.Exception,'%')) + '%'
     and isnull(Exception.ExceptionCode,'%')  like '%' + isnull(@ExceptionCode, isnull(Exception.ExceptionCode,'%')) + '%'
  
end
