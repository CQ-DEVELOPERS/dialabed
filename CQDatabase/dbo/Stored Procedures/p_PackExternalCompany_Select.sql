﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackExternalCompany_Select
  ///   Filename       : p_PackExternalCompany_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 21:17:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the PackExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StorageUnitId int = null 
  /// </param>
  /// <returns>
  ///   PackExternalCompany.WarehouseId,
  ///   PackExternalCompany.ExternalCompanyId,
  ///   PackExternalCompany.StorageUnitId,
  ///   PackExternalCompany.FromPackId,
  ///   PackExternalCompany.FromQuantity,
  ///   PackExternalCompany.ToPackId,
  ///   PackExternalCompany.ToQuantity,
  ///   PackExternalCompany.Comments 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackExternalCompany_Select
(
 @WarehouseId int = null,
 @ExternalCompanyId int = null,
 @StorageUnitId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         PackExternalCompany.WarehouseId
        ,PackExternalCompany.ExternalCompanyId
        ,PackExternalCompany.StorageUnitId
        ,PackExternalCompany.FromPackId
        ,PackExternalCompany.FromQuantity
        ,PackExternalCompany.ToPackId
        ,PackExternalCompany.ToQuantity
        ,PackExternalCompany.Comments
    from PackExternalCompany
   where isnull(PackExternalCompany.WarehouseId,'0')  = isnull(@WarehouseId, isnull(PackExternalCompany.WarehouseId,'0'))
     and isnull(PackExternalCompany.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(PackExternalCompany.ExternalCompanyId,'0'))
     and isnull(PackExternalCompany.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(PackExternalCompany.StorageUnitId,'0'))
  --order by Comments
  
end
