﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Get_PickLocation
  ///   Filename       : p_Mobile_Get_PickLocation.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Mar 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Get_PickLocation
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
  
  select l.Location
    from Instruction i (nolock)
    join Location    l (nolock) on i.PickLocationId = l.LocationId
   where i.InstructionId = @InstructionId
end
