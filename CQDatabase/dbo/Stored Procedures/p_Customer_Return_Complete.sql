﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Customer_Return_Complete
  ///   Filename       : p_Customer_Return_Complete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Customer_Return_Complete
(
 @InboundDocumentId int,
 @OperatorId        int
)
 
as
begin
  set nocount on;
  return;
--  declare @Error             int,
--          @Errormsg          nvarchar(500),
--          @GetDate           datetime,
--          @OrderNumber       nvarchar(30)
--  
--  select @GetDate = dbo.ufn_Getdate()
--  
--  begin transaction
--  
--  select @OrderNumber = OrderNumber
--    from InboundDocument
--   where InboundDocumentId = @InboundDocumentId
--  
--  if not exists(select top 1 1 from InterfaceExportSOHeader where PrimaryKey = @OrderNumber)
--  begin
--    insert InterfaceExportSOHeader
--          (PrimaryKey,
--           Additional1,
--           RecordType,
--           RecordStatus,
--           CustomerCode)
--    select id.OrderNumber,
--           id.ReferenceNumber,
--           'CR',
--           'N',
--           ec.ExternalCompanyCode
--      from InboundDocument id
--      join ExternalCompany ec on id.ExternalCompanyId = ec.ExternalCompanyId
--     where id.InboundDocumentId = @InboundDocumentId
--    
--    if @@rowcount > 0
--      insert InterfaceExportSODetail
--            (ForeignKey,
--             LineNumber,
--             ProductCode,
--             Batch,
--             Quantity,
--             Additional1)
--      select id.OrderNumber,
--             il.LineNumber,
--             vs.ProductCode,
--             vs.Batch,
--             rl.RequiredQuantity,
--             w.WarehouseCode
--        from InboundDocument id
--        join InboundLine     il on id.InboundDocumentId  = il.InboundDocumentId
--        join ReceiptLine     rl on il.InboundLineId      = rl.InboundLineId
--        join viewStock       vs on rl.StorageUnitBatchId = vs.StorageUnitBatchId
--        join Warehouse        w on id.WarehouseId        = w.WarehouseId
--       where id.InboundDocumentId = @InboundDocumentId
--    
--    if @Error <> 0
--      goto error
--  end
--  
--  commit transaction
--  return
--  
--  error:
--    raiserror 900000 'Error executing p_Customer_Return_Complete'
--    rollback transaction
--    return @Error
end
