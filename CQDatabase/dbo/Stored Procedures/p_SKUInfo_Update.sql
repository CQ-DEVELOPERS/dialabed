﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKUInfo_Update
  ///   Filename       : p_SKUInfo_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:37
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the SKUInfo table.
  /// </remarks>
  /// <param>
  ///   @SKUId int = null,
  ///   @WarehouseId int = null,
  ///   @Quantity float = null,
  ///   @AlternatePallet int = null,
  ///   @SubstitutePallet int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKUInfo_Update
(
 @SKUId int = null,
 @WarehouseId int = null,
 @Quantity float = null,
 @AlternatePallet int = null,
 @SubstitutePallet int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update SKUInfo
     set SKUId = isnull(@SKUId, SKUId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         Quantity = isnull(@Quantity, Quantity),
         AlternatePallet = isnull(@AlternatePallet, AlternatePallet),
         SubstitutePallet = isnull(@SubstitutePallet, SubstitutePallet) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
