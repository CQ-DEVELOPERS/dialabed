﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MobileLog_List
  ///   Filename       : p_MobileLog_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:11
  /// </summary>
  /// <remarks>
  ///   Selects rows from the MobileLog table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   MobileLog.MobileLogId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MobileLog_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as MobileLogId
        ,null as 'MobileLog'
  union
  select
         MobileLog.MobileLogId
        ,MobileLog.MobileLogId as 'MobileLog'
    from MobileLog
  
end
