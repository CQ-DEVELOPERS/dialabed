﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Product_Import
  ///   Filename       : p_Wizard_Product_Import.sql
  ///   Create By      : Junaid Desai	
  ///   Date Created   : 26 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Product_Import

 
as
begin
	 set nocount on;
 
Declare		@ProductCode						nvarchar(30),	
			@SKUCode							nvarchar(50),	
			@Product							nvarchar(50),	
			@ProductBarcode						nvarchar(50),	
			@MinimumQuantity					float,			
			@ReorderQuantity					float,			
			@MaximumQuantity					float,			
			@CuringPeriodDays					int,			
			@ShelfLifeDays						int,			
			@QualityAssuranceIndicator			bit,			
			@ProductType						nvarchar(20),	
			@PackType							nvarchar(30),	
			@PackQuantity						float,			
			@PackBarcode						nvarchar(50),	
			@PackVolume							numeric(13,3),	
			@PackWeight							numeric(13,3)	


  declare	@Error							int,
			@Errormsg						nvarchar(500),
			@GetDate						datetime,
			@SKUId							int,
			@PackTypeId						int,
			@ProductId                      int,
			@StorageUnitId					int,
			@PackId							int

  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
DECLARE CursorProductImport CURSOR FOR

SELECT [ProductCode]
      ,[SKUCode]
      ,[Product]
      ,[ProductBarcode]
      ,[MinimumQuantity]
      ,[ReorderQuantity]
      ,[MaximumQuantity]
      ,[CuringPeriodDays]
      ,[ShelfLifeDays]
      ,[QualityAssuranceIndicator]
      ,[ProductType]
      ,[PackType]
      ,[PackQuantity]
      ,[PackBarcode]
      ,[PackVolume]
      ,[PackWeight]
  FROM [ProductImport]
 
OPEN CursorProductImport

FETCH CursorProductImport INTO  @ProductCode,				
								@SKUCode	,				
								@Product	,				
								@ProductBarcode	,			
								@MinimumQuantity,			
								@ReorderQuantity,			
								@MaximumQuantity,			
								@CuringPeriodDays,			
								@ShelfLifeDays	,			
								@QualityAssuranceIndicator,	
								@ProductType,				
								@PackType	,				
								@PackQuantity,				
								@PackBarcode,				
								@PackVolume	,				
								@PackWeight					


    
								
WHILE @@Fetch_Status = 0
   BEGIN

select @PackTypeId = PackTypeId
    from PackType
   where PackType = @PackType

select @SKUId = SKUId
    from SKU
   where SKUCode = @SKUCode

declare @StatusId int
select @StatusId = dbo.ufn_StatusId('P','A')
EXEC	[p_Product_Insert]
		@ProductId = @ProductId OUTPUT,
		@StatusId = @StatusId,
		@ProductCode = @ProductCode,
		@Product = @Product,
		@Barcode = @ProductBarcode,
		@MinimumQuantity = @MinimumQuantity,
		@ReorderQuantity = @ReorderQuantity,
		@MaximumQuantity = @MaximumQuantity,
		@CuringPeriodDays = @CuringPeriodDays,
		@ShelfLifeDays = @ShelfLifeDays,
		@QualityAssuranceIndicator = @QualityAssuranceIndicator
 
EXEC	[p_StorageUnit_Insert]
		@StorageUnitId = @StorageUnitId OUTPUT,
		@SKUId = @SKUId,
		@ProductId = @ProductId

EXEC	[p_Pack_Insert]
		@PackId = @PackId OUTPUT,
		@StorageUnitId = @StorageUnitId,
		@PackTypeId = @PackTypeId,
		@Quantity = @PackQuantity,
		@Barcode = @PackBarcode,
		@Length = NULL,
		@Width = NULL,
		@Height = NULL,
		@Volume = @PackVolume,
		@Weight = @PackWeight
  

FETCH CursorProductImport INTO  @ProductCode,				
								@SKUCode	,				
								@Product	,				
								@ProductBarcode	,			
								@MinimumQuantity,			
								@ReorderQuantity,			
								@MaximumQuantity,			
								@CuringPeriodDays,			
								@ShelfLifeDays	,			
								@QualityAssuranceIndicator,	
								@ProductType,				
								@PackType	,				
								@PackQuantity,				
								@PackBarcode,				
								@PackVolume	,				
								@PackWeight			

end

CLOSE CursorProductImport

DEALLOCATE CursorProductImport

  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Wizard_Product_Import'
    rollback transaction
	CLOSE CursorLocatonImport
	DEALLOCATE CursorLocatonImport
    return @Error
end
