﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Delivery_Summary
  ///   Filename       : p_Report_Delivery_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Delivery_Summary
(
 @ConnectionString   nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @OutboundShipmentId int,
 @IssueId            int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   IdentCol           int identity,
   OutboundShipmentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   DespatchDate       datetime,
   JobId              int,
   InstructionId      int,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   EndDate            datetime,
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   OperatorId         int,
   Operator           nvarchar(50),
   DropSequence       int,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   PickArea           nvarchar(50),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   StoreArea          nvarchar(50),
   RouteId            int,
   Route              nvarchar(50),
   Weight             numeric(13,3)
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is null
    insert @TableResult
          (DespatchDate,
           JobId,
           InstructionId,
           StorageUnitBatchId,
           EndDate,
           Quantity,
           ConfirmedQuantity,
           ShortQuantity,
           OperatorId,
           OrderNumber,
           ExternalCompanyId,
           PickLocationId,
           StoreLocationId,
           RouteId)
    select i.DeliveryDate,
           ins.JobId,
           ins.InstructionId,
           ins.StorageUnitBatchId,
           ins.EndDate,
           ins.Quantity,
           ins.ConfirmedQuantity,
           ins.Quantity - ins.ConfirmedQuantity,
           ins.OperatorId,
           od.OrderNumber,
           od.ExternalCompanyId,
           ins.PickLocationId,
           ins.StoreLocationId,
           i.RouteId
      from Instruction     ins (nolock)
      join IssueLine        il (nolock) on ins.IssueLineId      = il.IssueLineId
      join Issue             i (nolock) on il.IssueId           = i.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where i.IssueId              = isnull(i.IssueId, @IssueId)
  else
    insert @TableResult
          (OutboundShipmentId,
           JobId,
           InstructionId,
           StorageUnitBatchId,
           EndDate,
           Quantity,
           ConfirmedQuantity,
           ShortQuantity,
           OperatorId,
           OrderNumber,
           ExternalCompanyId,
           DropSequence,
           PickLocationId,
           StoreLocationId)
    select osi.OutboundShipmentId,
           ins.JobId,
           ins.InstructionId,
           ins.StorageUnitBatchId,
           ins.EndDate,
           ins.Quantity,
           ins.ConfirmedQuantity,
           ins.Quantity - ins.ConfirmedQuantity,
           ins.OperatorId,
           od.OrderNumber,
           od.ExternalCompanyId,
           osi.DropSequence,
           ins.PickLocationId,
           ins.StoreLocationId
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId    = ins.InstructionId
      join IssueLine              il (nolock) on ili.IssueLineId      = il.IssueLineId
      join Issue                   i (nolock) on il.IssueId           = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join OutboundShipmentIssue osi (nolock) on i.IssueId            = osi.IssueId
     where osi.OutboundShipmentId = isnull(@OutboundShipmentId, osi.OutboundShipmentId)
    union
    select osi.OutboundShipmentId,
           ins.JobId,
           ins.InstructionId,
           ins.StorageUnitBatchId,
           ins.EndDate,
           ins.Quantity,
           ins.ConfirmedQuantity,
           ins.Quantity - ins.ConfirmedQuantity,
           ins.OperatorId,
           od.OrderNumber,
           od.ExternalCompanyId,
           osi.DropSequence,
           ins.PickLocationId,
           ins.StoreLocationId
      from Instruction           ins (nolock)
      join IssueLine              il (nolock) on ins.IssueLineId      = il.IssueLineId
      join Issue                   i (nolock) on il.IssueId           = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join OutboundShipmentIssue osi (nolock) on i.IssueId            = osi.IssueId
     where osi.OutboundShipmentId = isnull(@OutboundShipmentId, osi.OutboundShipmentId)
  
  update tr
     set DespatchDate = os.ShipmentDate,
         RouteId      = os.RouteId
    from @TableResult    tr
    join OutboundShipment os on tr.OutboundShipmentId = os.OutboundShipmentId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  select OutboundShipmentId,
         DropSequence,
         Route,
         OrderNumber,
         DespatchDate,
         ExternalCompany,
         Weight
    from @TableResult
  group by OutboundShipmentId,
         DropSequence,
         Route,
         OrderNumber,
         DespatchDate,
         ExternalCompany,
         Weight
  order by OutboundShipmentId,
         DropSequence,
         OrderNumber
end
