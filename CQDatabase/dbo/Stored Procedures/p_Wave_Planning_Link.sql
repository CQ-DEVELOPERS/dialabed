﻿
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Link
  ///   Filename       : p_Wave_Planning_Link.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Link
(
 @waveId int,
 @outboundShipmentId int = null,
 @issueId            int,
 @dropSequence       int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  --select @OldOutboundShipmentId = OutboundShipmentId
  --  from OutboundShipmentIssue (nolock)
  -- where IssueId = @issueId
  
  begin transaction
  
  ---- delink from shipment
  --if @issueId != -1
  --begin
  --  exec @Error = p_OutboundShipmentIssue_Unlink
  --   @OutboundShipmentId = @OutboundShipmentId,
  --   @issueId            = @issueId
    
  --  set @OutboundShipmentId = -1
  --end
  
  if exists(select top 1 1
              from Wave w
              join Status s on s.StatusId = w.StatusId
                           and s.StatusCode = 'W'
             where w.WaveId = @waveId)
  begin
    if @outboundShipmentId is not null and @outboundShipmentId != -1
    begin
      exec p_OutboundShipment_Update
       @OutboundShipmentId = @outboundShipmentId,
       @waveId  = @waveId
      
      if @Error <> 0
        goto error
      
      update i
         set WaveId = @waveId
        from OutboundShipmentIssue osi (nolock)
        join Issue                   i (nolock) on osi.IssueId = i.IssueId
       where osi.OutboundShipmentId = @outboundShipmentId
      
      select @error = @@error
      
      if @Error <> 0
        goto error
    end
    else
    begin
      exec p_Issue_Update
       @IssueId = @IssueId,
       @waveId  = @waveId
      
      if @Error <> 0
        goto error
    end
  end
  
  if @@trancount > 0
    commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Wave_Planning_Link'
    if @@trancount = 1
      rollback transaction
    else
      commit transaction
    return @Error
end


