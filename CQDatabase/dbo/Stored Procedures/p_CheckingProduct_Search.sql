﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingProduct_Search
  ///   Filename       : p_CheckingProduct_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:18
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the CheckingProduct table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   CheckingProduct.InstructionId,
  ///   CheckingProduct.WarehouseId,
  ///   CheckingProduct.OrderNumber,
  ///   CheckingProduct.JobId,
  ///   CheckingProduct.ReferenceNumber,
  ///   CheckingProduct.StorageUnitBatchId,
  ///   CheckingProduct.StorageUnitId,
  ///   CheckingProduct.ProductCode,
  ///   CheckingProduct.ProductBarcode,
  ///   CheckingProduct.PackBarcode,
  ///   CheckingProduct.Product,
  ///   CheckingProduct.SKUCode,
  ///   CheckingProduct.Batch,
  ///   CheckingProduct.ExpiryDate,
  ///   CheckingProduct.Quantity,
  ///   CheckingProduct.ConfirmedQuantity,
  ///   CheckingProduct.CheckQuantity,
  ///   CheckingProduct.Checked,
  ///   CheckingProduct.Total,
  ///   CheckingProduct.Lines,
  ///   CheckingProduct.InstructionRefId,
  ///   CheckingProduct.SkipUpdate,
  ///   CheckingProduct.InsertDate,
  ///   CheckingProduct.ErrorMsg 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingProduct_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         CheckingProduct.InstructionId
        ,CheckingProduct.WarehouseId
        ,CheckingProduct.OrderNumber
        ,CheckingProduct.JobId
        ,CheckingProduct.ReferenceNumber
        ,CheckingProduct.StorageUnitBatchId
        ,CheckingProduct.StorageUnitId
        ,CheckingProduct.ProductCode
        ,CheckingProduct.ProductBarcode
        ,CheckingProduct.PackBarcode
        ,CheckingProduct.Product
        ,CheckingProduct.SKUCode
        ,CheckingProduct.Batch
        ,CheckingProduct.ExpiryDate
        ,CheckingProduct.Quantity
        ,CheckingProduct.ConfirmedQuantity
        ,CheckingProduct.CheckQuantity
        ,CheckingProduct.Checked
        ,CheckingProduct.Total
        ,CheckingProduct.Lines
        ,CheckingProduct.InstructionRefId
        ,CheckingProduct.SkipUpdate
        ,CheckingProduct.InsertDate
        ,CheckingProduct.ErrorMsg
    from CheckingProduct
  
end
