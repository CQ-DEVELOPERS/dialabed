﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Delete
  ///   Filename       : p_Container_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 11:46:27
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Container table.
  /// </remarks>
  /// <param>
  ///   @ContainerId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Delete
(
 @ContainerId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Container
     where ContainerId = @ContainerId
  
  select @Error = @@Error
  
  
  return @Error
  
end
