﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Query
  ///   Filename       : p_Report_Batch_Query.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Mar 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Batch_Query
	(@BatchNumber nvarchar(50) = null,
	@BatchRefNumber nvarchar(50) = null,
	@ProductCode	nvarchar(30) = null,
	@Product		nvarchar(50),
	@BatchStatus	nvarchar(50) = null,
	@OrderNumber	nvarchar(50) = null)

 
as
begin
	 set nocount on;

if ((@BatchNumber = '-1') or  (@BatchNumber = '-1')) set @BatchNumber = null
if ((@BatchRefNumber = '-1') or (@BatchRefNumber = '-1')) set @BatchRefNumber = null
if ((@ProductCode = '-1') or (@ProductCode = '-1')) set @ProductCode = null
if ((@Product = '-1') or (@Product = '-1')) set @Product = null
if ((@BatchStatus = '-1') or(@BatchStatus = '-1')) set @BatchStatus = null
if ((@OrderNumber = '-1') or (@OrderNumber = '-1')) set @OrderNumber = null


  
Select distinct  b.Batch,
			BatchReferenceNumber,
			  r.deliveryDate,
			  b.createDate as SampleDate,
			  bi.ExceptionDate as IntoLabDate,
			  bo.ExceptionDate as OutLabdate,
			  Datediff(hh,bo.ExceptionDate,bi.ExceptionDate) as DwellTime,
			  b.ModifiedDate as StatusUpdate,
			  o.Operator as UpdatedBy,
			  s.status ,
			  d.OrderNumber as PurchaseOrder,
			  dt.InboundDocumentType,
			p.ProductCode,
			p.Product

from ReceiptLine rl
	Join StorageunitBatch sub on sub.StorageUnitBatchid = rl.StorageUnitBatchId
	Join StorageUnit su on su.StorageUnitId = sub.StorageUnitId
	Join Product p on p.ProductId = su.ProductId
	Join Batch b on b.BatchId = sub.BatchId
	Join Receipt r on r.ReceiptId = rl.ReceiptId
	join InboundDocument d on d.InboundDocumentId = r.InboundDocumentId
	Join InboundDocumentType dt on dt.InboundDocumentTypeId = d.InboundDocumentTypeId
	Left Join Exception bi on bi.Detail = b.BatchId and bi.ExceptionCode = 'BATSAMRECV'
	Left Join Exception bo on bo.Detail = b.BatchId and bo.ExceptionCode = 'BATSAMCOMP'
	Left Join Operator o on o.OperatorId = b.ModifiedBy
	Join Status s on s.StatusId = b.StatusId
where 

	Batch like Isnull ('%' + convert(nvarchar(50),@BatchNumber) + '%',Batch)
	and  product like Isnull('%' + convert(nvarchar(50),@Product) + '%',product)
	and isnull(BatchReferenceNumber,'none') like isnull ('%' + convert(nvarchar(50),@BatchRefNumber) + '%',isnull(BatchReferenceNumber,'none'))
	and ProductCode like isnull('%' + convert(nvarchar(30),@ProductCode) + '%',ProductCode)
	and s.Status like isnull('%' + convert(nvarchar(50),@BatchStatus) + '%',s.Status)
	and d.OrderNumber like isnull('%' + convert(nvarchar(50),@OrderNumber) + '%',d.OrderNumber)
  
end
