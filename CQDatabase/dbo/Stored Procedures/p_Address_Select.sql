﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Address_Select
  ///   Filename       : p_Address_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:20
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Address table.
  /// </remarks>
  /// <param>
  ///   @AddressId int = null 
  /// </param>
  /// <returns>
  ///   Address.AddressId,
  ///   Address.ExternalCompanyId,
  ///   Address.Street,
  ///   Address.Suburb,
  ///   Address.Town,
  ///   Address.Country,
  ///   Address.Code 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Address_Select
(
 @AddressId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Address.AddressId
        ,Address.ExternalCompanyId
        ,Address.Street
        ,Address.Suburb
        ,Address.Town
        ,Address.Country
        ,Address.Code
    from Address
   where isnull(Address.AddressId,'0')  = isnull(@AddressId, isnull(Address.AddressId,'0'))
  
end
