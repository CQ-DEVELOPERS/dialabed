﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Manufacturing_Order_Search
  ///   Filename       : p_Manufacturing_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Manufacturing_Order_Search
(
 @OutboundShipmentId		int,
 @WarehouseId				int,
 @OutboundDocumentTypeId	int, 
 @OrderNumber				nvarchar(30),
 @ExternalCompanyCode		nvarchar(30),
 @ExternalCompany			nvarchar(255), 
 @FromDate					datetime,
 @ToDate					datetime
)

 
as
begin
	 set nocount on;
  
declare @TableResult as table
  (
   OutboundDocumentId        int,
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   NumberOfLines             int,
   DeliveryDate              datetime,
   CreateDate                datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   PriorityId                int,
   Priority                  nvarchar(50),
   LocationId                int,
   Location                  nvarchar(15),
   DespatchBay               int,
   DespatchBayDesc           nvarchar(15),
   AvailabilityIndicator     nvarchar(20),
   Remarks                   nvarchar(255),
   Total                     numeric(13,3),
   Complete                  numeric(13,3),
   PercentageComplete        numeric(13,3),
   StockAvailable            numeric(13,3),
   DropSequence              int
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  insert @TableResult
        (OutboundDocumentId,
         IssueId,
         LocationId,
         DespatchBay,
         OrderNumber,
         StatusId,
         Status,
         PriorityId,
         DeliveryDate,
         CreateDate,
         Remarks,
         NumberOfLines,
         Total,
         Complete,
         DropSequence)
  select od.OutboundDocumentId,
         i.IssueId,
         i.LocationId,
         i.DespatchBay,
         od.OrderNumber,
         i.StatusId,
         s.Status,
         i.PriorityId,
         i.DeliveryDate,
         od.CreateDate,
         i.Remarks,
         i.NumberOfLines,
         i.Total,
         i.Complete,
         i.DropSequence
    from OutboundDocument      od (nolock)
    join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join Status                 s (nolock) on i.StatusId                = s.StatusId
   where s.Type                    = 'IS'
     and s.StatusCode             in ('W','P','SA','M','RL','PC','PS','CK','A','WC','QA')
     and od.WarehouseId            = @WarehouseId
   --where id.OutboundDocumentTypeId    = isnull(@OutboundDocumentTypeId, id.OutboundDocumentTypeId)
     and od.OrderNumber like isnull('%' + @OrderNumber + '%', od.OrderNumber)
     and isnull(i.DeliveryDate, od.DeliveryDate) between @FromDate and @ToDate

  
  update r
     set OutboundShipmentId = isr.OutboundShipmentId
    from @TableResult             r
    join OutboundShipmentIssue isr (nolock) on r.IssueId = isr.IssueId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from IssueLine rl
                           where t.IssueId = rl.IssueId)
    from @TableResult t
  
  update r
     set Location = l.Location
    from @TableResult r
    join Location     l (nolock) on r.LocationId = l.LocationId
	
  update r
     set Priority = P.Priority
    from @TableResult r
    join Priority    P (nolock) on r.PriorityId = P.PriorityID  



  select ROW_NUMBER() 
         OVER (ORDER BY DropSequence) AS 'Rank',
         OutboundShipmentId,
         IssueId,
         OrderNumber,
         NumberOfLines,
         DeliveryDate,
         Status
    from @TableResult
    order by rank
end
