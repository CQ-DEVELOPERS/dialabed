﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DangerousGoods_Parameter
  ///   Filename       : p_DangerousGoods_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:31
  /// </summary>
  /// <remarks>
  ///   Selects rows from the DangerousGoods table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   DangerousGoods.DangerousGoodsId,
  ///   DangerousGoods.DangerousGoods 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DangerousGoods_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as DangerousGoodsId
        ,'{All}' as DangerousGoods
  union
  select
         DangerousGoods.DangerousGoodsId
        ,DangerousGoods.DangerousGoods
    from DangerousGoods
  
end
