﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Interface_Imported_Orders
  ///   Filename       : p_Report_Interface_Imported_Orders.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Jun 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Interface_Imported_Orders
(
 @WarehouseId int,
 @OrderNumber nvarchar(30),
 @FromDate    datetime,
 @ToDate      datetime
)
 
as
begin
	 set nocount on;
	 
	 declare @ProcessedDate datetime
	 
  declare @TableResult as table
  (
   OrderNumber                     nvarchar(30),
   RecordType                      nvarchar(30),
   RecordStatus                    char(1),
   CustomerCode                    nvarchar(30),
   Customer                        nvarchar(255),
   Address                         nvarchar(255),
   FromWarehouseCode               nvarchar(50),
   ToWarehouseCode                 nvarchar(50),
   DeliveryDate                    nvarchar(20),
   Remarks                         nvarchar(255),
   NumberOfLines                   int,
   ProcessedDate                   datetime,
   LineNumber                      int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   SKUCode                         nvarchar(50),
   Batch                           nvarchar(50),
   Quantity                        decimal,
   Weight                          decimal
  )
  
  if @OrderNumber is null
    set @OrderNumber = ''
  
  insert @TableResult
        (OrderNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Customer,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         ProcessedDate,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight)
  select isnull(h.OrderNumber, h.PrimaryKey) as 'OrderNumber',
         h.RecordType,
         h.RecordStatus,
         h.CustomerCode,
         h.Customer,
         h.Address,
         h.FromWarehouseCode,
         h.ToWarehouseCode,
         h.DeliveryDate,
         h.Remarks,
         h.NumberOfLines,
         h.ProcessedDate,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight
    from InterfaceImportSOHeader h (nolock)
    join InterfaceImportSODetail d (nolock) on h.InterfaceImportSOHeaderId = d.InterfaceImportSOHeaderId
   where isnull(h.OrderNumber, h.PrimaryKey) like '%' + @OrderNumber + '%'
     and h.ProcessedDate between @FromDate and @ToDate
  union
  select isnull(h.OrderNumber, h.PrimaryKey) as 'OrderNumber',
         h.RecordType,
         h.RecordStatus,
         h.SupplierCode,
         h.Supplier,
         h.Address,
         h.FromWarehouseCode,
         h.ToWarehouseCode,
         convert(nvarchar(20), h.DeliveryDate, 103),
         h.Remarks,
         h.NumberOfLines,
         h.ProcessedDate,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight
    from InterfaceImportPOHeader h (nolock)
    join InterfaceImportPODetail d (nolock) on h.InterfaceImportPOHeaderId = d.InterfaceImportPOHeaderId
   where isnull(h.OrderNumber, h.PrimaryKey) like '%' + @OrderNumber + '%'
     and h.ProcessedDate between @FromDate and @ToDate
  union
  select isnull(h.OrderNumber, h.PrimaryKey) as 'OrderNumber',
         h.RecordType,
         h.RecordStatus,
         h.SupplierCode,
         h.Supplier,
         h.Address,
         h.FromWarehouseCode,
         h.ToWarehouseCode,
         convert(nvarchar(20), h.DeliveryDate, 103),
         h.Remarks,
         h.NumberOfLines,
         h.ProcessedDate,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight
    from InterfaceImportPOHeader h (nolock)
    join InterfaceImportPODetail d (nolock) on h.InterfaceImportPOHeaderId = d.InterfaceImportPOHeaderId
   where isnull(h.OrderNumber, h.PrimaryKey) like '%' + @OrderNumber + '%'
     and h.ProcessedDate between @FromDate and @ToDate
  union
  select isnull(h.OrderNumber, h.PrimaryKey) as 'OrderNumber',
         h.RecordType,
         h.RecordStatus,
         h.CustomerCode,
         h.Customer,
         h.Address,
         h.FromWarehouseCode,
         h.ToWarehouseCode,
         convert(nvarchar(20), h.DeliveryDate, 103),
         h.Remarks,
         h.NumberOfLines,
         h.ProcessedDate,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight
    from InterfaceImportIVHeader h (nolock)
    join InterfaceImportIVDetail d (nolock) on h.InterfaceImportIVHeaderId = d.InterfaceImportIVHeaderId
   where isnull(h.OrderNumber, h.PrimaryKey) like '%' + @OrderNumber + '%'
     and h.ProcessedDate between @FromDate and @ToDate
  union
  select isnull(h.OrderNumber, h.PrimaryKey) as 'OrderNumber',
         h.RecordType,
         h.RecordStatus,
         h.CompanyCode,
         h.Company,
         h.Address,
         h.FromWarehouseCode,
         h.ToWarehouseCode,
         null,--h.DeliveryDate,
         h.Remarks,
         h.NumberOfLines,
         h.ProcessedDate,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight
    from InterfaceImportHeader h (nolock)
    join InterfaceImportDetail d (nolock) on h.InterfaceImportHeaderId = d.InterfaceImportHeaderId
   where isnull(h.OrderNumber, h.PrimaryKey) like '%' + @OrderNumber + '%'
     and h.InsertDate between @FromDate and @ToDate
  
  select OrderNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Customer,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         ProcessedDate,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight
    from @TableResult
end
