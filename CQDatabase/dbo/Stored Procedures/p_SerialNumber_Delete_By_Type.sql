﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Delete_By_Type
  ///   Filename       : p_SerialNumber_Delete_By_Type.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Delete_By_Type
(
 @operatorId     int,
 @SerialNumberId int
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_SerialNumber_Delete_By_Type',
          @GetDate           datetime,
          @Transaction       bit = 1
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  update SerialNumber
     set IssueId = null,
         IssueLineId = null,
         PickJobId = null,
         PickInstructionId = null
   where SerialNumberId = @SerialNumberId
     and (IssueId is not null
       or IssueLineId is not null
       or PickJobId is not null
       or PickInstructionId is not null)
  
  if @@ROWCOUNT = 0
  update SerialNumber
     set ReceiptId = null,
         ReceiptLineId = null,
         StoreJobId = null,
         StoreInstructionId = null
   where SerialNumberId = @SerialNumberId
     and (ReceiptId is not null
       or ReceiptLineId is not null
       or StoreJobId is not null
       or StoreInstructionId is not null)
  
  --exec @Error = p_SerialNumber_Delete
  -- @SerialNumberId      = @SerialNumberId
  
  --if @Error <> 0
  --  goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
