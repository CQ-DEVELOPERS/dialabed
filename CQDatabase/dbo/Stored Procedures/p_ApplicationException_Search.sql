﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ApplicationException_Search
  ///   Filename       : p_ApplicationException_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:37
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ApplicationException table.
  /// </remarks>
  /// <param>
  ///   @ApplicationExceptionId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ApplicationException.ApplicationExceptionId,
  ///   ApplicationException.PriorityLevel,
  ///   ApplicationException.Module,
  ///   ApplicationException.Page,
  ///   ApplicationException.Method,
  ///   ApplicationException.ErrorMsg,
  ///   ApplicationException.CreateDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ApplicationException_Search
(
 @ApplicationExceptionId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ApplicationExceptionId = '-1'
    set @ApplicationExceptionId = null;
  
 
  select
         ApplicationException.ApplicationExceptionId
        ,ApplicationException.PriorityLevel
        ,ApplicationException.Module
        ,ApplicationException.Page
        ,ApplicationException.Method
        ,ApplicationException.ErrorMsg
        ,ApplicationException.CreateDate
    from ApplicationException
   where isnull(ApplicationException.ApplicationExceptionId,'0')  = isnull(@ApplicationExceptionId, isnull(ApplicationException.ApplicationExceptionId,'0'))
  
end
