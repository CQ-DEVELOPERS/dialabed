﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COADetail_Update
  ///   Filename       : p_COADetail_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:21
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the COADetail table.
  /// </remarks>
  /// <param>
  ///   @COADetailId int = null,
  ///   @COAId int = null,
  ///   @NoteId int = null,
  ///   @NoteCode nvarchar(510) = null,
  ///   @Note nvarchar(max) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COADetail_Update
(
 @COADetailId int = null,
 @COAId int = null,
 @NoteId int = null,
 @NoteCode nvarchar(510) = null,
 @Note nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
  if @COADetailId = '-1'
    set @COADetailId = null;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @NoteId = '-1'
    set @NoteId = null;
  
	 declare @Error int
 
  update COADetail
     set COAId = isnull(@COAId, COAId),
         NoteId = isnull(@NoteId, NoteId),
         NoteCode = isnull(@NoteCode, NoteCode),
         Note = isnull(@Note, Note) 
   where COADetailId = @COADetailId
  
  select @Error = @@Error
  
  
  return @Error
  
end
