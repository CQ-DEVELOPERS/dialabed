﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentType_List
  ///   Filename       : p_InterfaceDocumentType_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 16:26:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceDocumentType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceDocumentType.InterfaceDocumentTypeId,
  ///   InterfaceDocumentType.InterfaceDocumentType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentType_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceDocumentTypeId
        ,'{All}' as InterfaceDocumentType
  union
  select
         InterfaceDocumentType.InterfaceDocumentTypeId
        ,InterfaceDocumentType.InterfaceDocumentType
    from InterfaceDocumentType
  order by InterfaceDocumentType
  
end
