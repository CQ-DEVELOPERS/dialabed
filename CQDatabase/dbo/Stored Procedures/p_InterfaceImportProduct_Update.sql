﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportProduct_Update
  ///   Filename       : p_InterfaceImportProduct_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:03
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportProduct table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportProductId int = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(510) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @SKU nvarchar(20) = null,
  ///   @PalletQuantity float = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @MinimumQuantity float = null,
  ///   @ReorderQuantity float = null,
  ///   @MaximumQuantity float = null,
  ///   @CuringPeriodDays int = null,
  ///   @ShelfLifeDays int = null,
  ///   @QualityAssuranceIndicator bit = null,
  ///   @ProductType nvarchar(40) = null,
  ///   @ProductCategory nvarchar(40) = null,
  ///   @OverReceipt numeric(13,3) = null,
  ///   @RetentionSamples int = null,
  ///   @HostId nvarchar(60) = null,
  ///   @InsertDate datetime = null,
  ///   @Additional1 varchar(255) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @DangerousGoodsCode nvarchar(510) = null,
  ///   @ABCStatus nchar(2) = null,
  ///   @Samples nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportProduct_Update
(
 @InterfaceImportProductId int = null,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(510) = null,
 @SKUCode nvarchar(20) = null,
 @SKU nvarchar(20) = null,
 @PalletQuantity float = null,
 @Barcode nvarchar(100) = null,
 @MinimumQuantity float = null,
 @ReorderQuantity float = null,
 @MaximumQuantity float = null,
 @CuringPeriodDays int = null,
 @ShelfLifeDays int = null,
 @QualityAssuranceIndicator bit = null,
 @ProductType nvarchar(40) = null,
 @ProductCategory nvarchar(40) = null,
 @OverReceipt numeric(13,3) = null,
 @RetentionSamples int = null,
 @HostId nvarchar(60) = null,
 @InsertDate datetime = null,
 @Additional1 varchar(255) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @DangerousGoodsCode nvarchar(510) = null,
 @ABCStatus nchar(2) = null,
 @Samples nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportProductId = '-1'
    set @InterfaceImportProductId = null;
  
	 declare @Error int
 
  update InterfaceImportProduct
     set RecordStatus = isnull(@RecordStatus, RecordStatus),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         ProductCode = isnull(@ProductCode, ProductCode),
         Product = isnull(@Product, Product),
         SKUCode = isnull(@SKUCode, SKUCode),
         SKU = isnull(@SKU, SKU),
         PalletQuantity = isnull(@PalletQuantity, PalletQuantity),
         Barcode = isnull(@Barcode, Barcode),
         MinimumQuantity = isnull(@MinimumQuantity, MinimumQuantity),
         ReorderQuantity = isnull(@ReorderQuantity, ReorderQuantity),
         MaximumQuantity = isnull(@MaximumQuantity, MaximumQuantity),
         CuringPeriodDays = isnull(@CuringPeriodDays, CuringPeriodDays),
         ShelfLifeDays = isnull(@ShelfLifeDays, ShelfLifeDays),
         QualityAssuranceIndicator = isnull(@QualityAssuranceIndicator, QualityAssuranceIndicator),
         ProductType = isnull(@ProductType, ProductType),
         ProductCategory = isnull(@ProductCategory, ProductCategory),
         OverReceipt = isnull(@OverReceipt, OverReceipt),
         RetentionSamples = isnull(@RetentionSamples, RetentionSamples),
         HostId = isnull(@HostId, HostId),
         InsertDate = isnull(@InsertDate, InsertDate),
         Additional1 = isnull(@Additional1, Additional1),
         Additional2 = isnull(@Additional2, Additional2),
         Additional3 = isnull(@Additional3, Additional3),
         Additional4 = isnull(@Additional4, Additional4),
         Additional5 = isnull(@Additional5, Additional5),
         Additional6 = isnull(@Additional6, Additional6),
         Additional7 = isnull(@Additional7, Additional7),
         Additional8 = isnull(@Additional8, Additional8),
         Additional9 = isnull(@Additional9, Additional9),
         Additional10 = isnull(@Additional10, Additional10),
         DangerousGoodsCode = isnull(@DangerousGoodsCode, DangerousGoodsCode),
         ABCStatus = isnull(@ABCStatus, ABCStatus),
         Samples = isnull(@Samples, Samples) 
   where InterfaceImportProductId = @InterfaceImportProductId
  
  select @Error = @@Error
  
  
  return @Error
  
end
