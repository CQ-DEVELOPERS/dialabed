﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Generic_Export_Shipment
  ///   Filename       : p_Generic_Export_Shipment.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Generic_Export_Shipment
 
as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int
  )
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Generic_Export_Shipment',
          @GetDate           datetime,
          @Transaction       bit = 1,
          @OutboundShipmentId int,
          @IssueId            int
  
  select @GetDate = dbo.ufn_Getdate()
  
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId)
  select Distinct osi.OutboundShipmentId,
         i.IssueId
    from Issue                   i (nolock)
    join Status                  s (nolock) on i.StatusId = s.StatusId
                                           and s.StatusCode in ('CD','DC','D','C')
    left 
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
   where isnull(i.Interfaced, 0) = 0
  
  while exists(select top 1 1 
                 from @TableResult)
  begin
    set @OutboundShipmentId = null
    set @IssueId = null
    
    select top 1
           @OutboundShipmentId = OutboundShipmentId,
           @IssueId            = IssueId
      from @TableResult
    
    delete @TableResult
     where (IssueId = @IssueId)
    
    exec @Error = p_Interface_Export_Trip_Insert
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId
    
    if @Error <> 0
      goto error
  end
  
  if @Error <> 0
    goto error
  
  result:
      
      return 0
    
    error:
      
      RAISERROR (@Errormsg,11,1)
      return @Error
end
 
 
