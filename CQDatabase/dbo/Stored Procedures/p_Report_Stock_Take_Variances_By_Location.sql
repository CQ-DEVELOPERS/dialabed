﻿/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_Variances_By_Location
  ///   Filename       : p_Report_Stock_Take_Variances_By_Location.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Report_Stock_Take_Variances_By_Location
(
 @StockTakeReferenceId		int,
 @WarehouseId				int,
 @FromDate					datetime,
 @ToDate					datetime,
 @AreaId					Int = null,
 @PrincipalId				int = null
)
as
begin

set nocount on; 


if (@StockTakeReferenceId = -1)
 set @StockTakeReferenceId = null

if (@AreaId = 0) or (@AreaId = -1)
 set @AreaId = null
 
 
 if @PrincipalId = -1
	set @PrincipalId = null
	
Declare @TableResult as  Table 
		(StockTakeReferenceId		int,
		 JobId						int,
		 InstructionId				int,
		 InstructionRefId			int,
		 InstructionTypeId			int,
		 InstructionType			Varchar(50),
		 InstructionTypeCode		Varchar(25),
		 Area						Varchar(50),
		 AreaId						int,
		 StorageUnitBatchId			int,
		 PickLocationId				int,
		 PickLocation				varchar(15),
         StoreLocationId			Int,
         StoreLocation				varchar(15),
		 ProductCode				Int,
		 Product					varchar(50),		
		 BatchId					int,
		 SKUId						int,
		 SKU						varchar(50),
		 Quantity					int,
		 PreviousCount1				int,
		 PreviousCount2				int,
		 LatestCount				int,
		 Variance					int,
		 CountedById				int,
		 CountedBy					varchar(50),
		 AuthorisedById				int,
		 AuthorisedBy				varchar(50),
		 CreateDate					datetime,
		 PrincipalId				int,
		 PrincipalCode				nvarchar(50))
		 
  declare @Instructions as table
  (
   LocationId         int,
   StorageUnitBatchId int,
   InstructionId      int,
   AreaId			  int
  )
   insert @Instructions
        (LocationId,
         StorageUnitBatchId,
         InstructionId,
         AreaId)
  select i.PickLocationId,
         i.StorageUnitBatchId,
         max(i.InstructionId),
         al.AreaId
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
    left join StockTakeReferenceJob strj (nolock) on strj.StockTakeReferenceId = @StockTakeReferenceId
    join AreaLocation al (nolock) on i.PickLocationId = al.LocationId
   where i.CreateDate           >= '2009-07-01'
     and it.InstructionTypeCode in ('STE','STL','STA','STP')
     and i.JobId = isnull(strj.JobId, i.JobId)
  group by i.PickLocationId, i.StorageUnitBatchId, al.AreaId
  
  --select * from @Instructions
  
insert @TableResult
        (StockTakeReferenceId,
         JobId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
         Quantity,
         CountedById,
		 AuthorisedById,
		 CreateDate,
		 AreaId,
		 Area)
  SELECT    @StockTakeReferenceId,
		    i.JobId, 
            it.InstructionType, 
            i.StorageUnitBatchId, 
            i.PickLocationId, 
            i.StoreLocationId, 
            i.Quantity,
            isnull(j.OperatorId,i.OperatorId),
            sta.operatorid,
            i.CreateDate,
            al.AreaId,
            a.Area
FROM        Instruction  i				(nolock) 
INNER JOIN  InstructionType it			(nolock) ON i.InstructionTypeId = it.InstructionTypeId
join		Job                 j		(nolock) on j.JobId = i.JobId
left join	StockTakeReferenceJob strj	(nolock) on strj.StockTakeReferenceId = @StockTakeReferenceId
left join	StockTakeAuthorise sta		(nolock) on sta.InstructionId = i.InstructionId
join		AreaLocation al				(nolock) on al.LocationId = i.PickLocationId
join		Area a						(nolock) on al.AreaId = a.AreaId
WHERE      (it.InstructionTypeCode in ('STL','STA','STP','STE'))
     and i.CreateDate   between @FromDate and @ToDate
	 and i.WarehouseId = Isnull(@WarehouseId,i.WarehouseId)
	 and i.JobId = isnull(strj.JobId, i.JobId)
	 and al.AreaId = ISNULL(@AreaId, al.AreaId)


 
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
   
    update tr
     set InstructionId  = i.InstructionId
    from @TableResult  tr
    join @Instructions  i on tr.StorageUnitBatchId = i.StorageUnitBatchId
                         and tr.PickLocationId       = i.LocationId
                         and tr.AreaId = i.AreaId
                           
  update tr
     set CountedBy = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.CountedById = o.OperatorId
    
  update tr
     set AuthorisedBy = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.AuthorisedById = o.OperatorId
    
    update tr
     set PreviousCount1   = i.Quantity,
         LatestCount      = i.ConfirmedQuantity,
         InstructionRefId = i.InstructionRefId,
         PreviousCount2	  = i.PreviousQuantity
    from @TableResult tr
    join Instruction   i (nolock) on tr.InstructionId = i.InstructionId
  
  --update tr
  --   set PreviousCount2 = i.Quantity
  --  from @TableResult tr
  --  join Instruction   i (nolock) on tr.InstructionRefId = i.InstructionId
    
  update tr
     set Variance =  isnull(tr.LatestCount,0) - isnull(tr.Quantity,0)
    from @TableResult tr
    where tr.AuthorisedById > 0


  select tr.InstructionId,
		 tr.InstructionRefId,
		 tr.StorageUnitBatchId,
		 tr.StockTakeReferenceId,
		 tr.Jobid,
         tr.InstructionType,
         tr.Area,
         tr.PickLocation,
         tr.StoreLocation,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         tr.Quantity,
         tr.PreviousCount1,
         tr.PreviousCount2,
         tr.LatestCount,
         tr.Variance,
         tr.CountedBy,
         tr.AuthorisedBy,
         pri.PrincipalCode
    from @TableResult    tr
    join viewStock       vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
    join Product  p (nolock) on vs.ProductId = p.ProductId
    left join Principal pri (nolock) on p.PrincipalId = pri.PrincipalId
    where isnull(p.PrincipalId,-1) = isnull(@PrincipalId,isnull(p.PrincipalId,-1))
  order by tr.CreateDate
  
end
