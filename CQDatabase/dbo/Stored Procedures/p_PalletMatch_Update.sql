﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletMatch_Update
  ///   Filename       : p_PalletMatch_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:46
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the PalletMatch table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null,
  ///   @InstructionId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(60) = null,
  ///   @Batch nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletMatch_Update
(
 @PalletId int = null,
 @InstructionId int = null,
 @StorageUnitBatchId int = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(60) = null,
 @Batch nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update PalletMatch
     set PalletId = isnull(@PalletId, PalletId),
         InstructionId = isnull(@InstructionId, InstructionId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         ProductCode = isnull(@ProductCode, ProductCode),
         SKUCode = isnull(@SKUCode, SKUCode),
         Batch = isnull(@Batch, Batch) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
