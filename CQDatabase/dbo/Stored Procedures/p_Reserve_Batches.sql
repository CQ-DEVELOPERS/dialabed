﻿
/*
 /// <summary>
 ///   Procedure Name : p_Reserve_Batches
 ///   Filename       : p_Reserve_Batches.sql
 ///   Create By      : Grant Schultz
 ///   Date Created   : 15 Mar 2010
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
 /// <newpara>
 ///   Modified by    : 
 ///   Modified Date  : 
 ///   Details        : 
 /// </newpara>
*/
CREATE procedure p_Reserve_Batches
(
 @IssueId int
)
 
as
begin
	 set nocount on;
  
  declare @TableProducts as table
  (
   LocationId         int,
   StorageUnitBatchId int,
   Quantity           numeric(13,6)
  )
  
  declare @TableLines as table
  (
   StorageUnitBatchId int,
   Quantity           numeric(13,6)
  )
  
  declare @WarehouseId int
  
  select @WarehouseId = WarehouseId
    from Issue (nolock)
   where IssueId = @IssueId
  
  insert @TableLines
        (StorageUnitBatchId,
         Quantity)
  select StorageUnitBatchId,
         Quantity
    from IssueLine (nolock)
   where IssueId = @IssueId
  
  insert @TableProducts
        (LocationId,
         StorageUnitBatchId,
         Quantity)
  select subl.LocationId,
         subl.StorageUnitBatchId,
         subl.ActualQuantity - subl.ReservedQuantity
    from @TableLines                il
    join StorageUnitBatch          sub (nolock) on il.StorageUnitBatchId  = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join Batch                       b (nolock) on sub.BatchId            = b.BatchId
    join Status                      s (nolock) on b.StatusId             = s.StatusId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId        = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
   where a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     and subl.ActualQuantity - subl.ReservedQuantity > 0
     and s.StatusCode = 'A'
     and a.AreaType = ''
     and l.ActivePicking = 1
     and a.AreaCode in ('RK','PK','BK','SP')
  
  select *
    from viewSOH soh
    join @TableProducts tp on soh.StorageUnitBatchId = tp.StorageUnitBatchId
                          and soh.LocationId = tp.LocationId
  
  select * from @TableProducts
  select * from @TableLines
  
  --return
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StorageUnitBatchId int,
          @Quantity          numeric(13,6),
          @JobId             int,
          @InstructionId     int,
          @InstructionTypeId int,
          @StatusId          int,
          @PriorityId        int,
          @OperatorId        int,
          @PickLocationId    int,
          @StoreLocationId   int

  select @GetDate = dbo.ufn_Getdate()
  
  select @InstructionTypeId = InstructionTypeId,
         @PriorityId        = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'RS'
  
  begin transaction
  
  while exists(select top 1 1
                 from @TableProducts)
  begin
    select @PickLocationId     = LocationId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = Quantity
      from @TableProducts
    
    delete @TableProducts
     where LocationId         = @PickLocationId
       and StorageUnitBatchId = @StorageUnitBatchId
       and Quantity           = @Quantity
    
    if @JobId is null
    begin
      set @StatusId = dbo.ufn_StatusId('J','RL')
      
      exec @Error = p_Job_Insert
       @JobId       = @JobId output,
       @PriorityId  = @PriorityId,
       @OperatorId  = @OperatorId,
       @StatusId    = @StatusId,
       @WarehouseId = @WarehouseId
      
      if @Error <> 0 or @JobId is null
      begin
        set @InstructionId = -1
        goto error
      end
    end
    
    set @StatusId = dbo.ufn_StatusId('I','W')
    
    exec @Error = p_Instruction_Insert
     @InstructionId       = @InstructionId output,
     @InstructionTypeId   = @InstructionTypeId,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @WarehouseId         = @WarehouseId,
     @StatusId            = @StatusId,
     @JobId               = @JobId,
     @OperatorId          = @OperatorId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = @StoreLocationId,
     @InstructionRefId    = null,
     @Quantity            = @Quantity,
     @ConfirmedQuantity   = @Quantity,
     @CreateDate          = @GetDate
    
    if @Error <> 0 or @InstructionId is null
      goto error
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
    
    --exec @Error = p_Instruction_Started
    -- @InstructionId = @InstructionId,
    -- @operatorId    = @operatorId
    
    --if @Error <> 0
    --  goto error
    
    --exec @Error = p_StorageUnitBatchLocation_Allocate
    -- @InstructionId       = @instructionId,
    -- @Confirmed           = 1
    
    --if @Error <> 0
    --  goto error
    
    --exec @Error = p_Instruction_Finished
    -- @InstructionId = @InstructionId,
    -- @operatorId    = @operatorId
    
    --if @Error <> 0
    --  goto error
    
    exec @Error = p_Issue_Update
     @IssueId = @IssueId,
     @ReservedId = @JobId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return

  error:
    raiserror 900000 'Error executing p_Reserve_Batches'
    rollback transaction
    return @Error
end
 
