﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Drop_Details
  ///   Filename       : p_Outbound_Drop_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Drop_Details
(
 @OutboundShipmentId int
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   OutboundShipmentId  int,
   IssueId             int,
   OutboundDocumentId  int,
   OrderNumber         nvarchar(30),
   ExternalCompanyId   int,
   ExternalCompanyCode nvarchar(30),
   ExternalCompany     nvarchar(255),
   RouteId             int,
   Route               nvarchar(50),
   StatusId            int,
   Status              nvarchar(50),
   DropSequence        int,
   OrderVolume         float,
   OrderWeight         float
  )
  
  declare @TableDetail as table
  (
   OutboundDocumentId int,
   OrderVolume        float,
   OrderWeight        float
  )
  
  insert @TableHeader
        (OutboundShipmentId,
         IssueId,
         OutboundDocumentId,
         OrderNumber,
         ExternalCompanyId,
         RouteId,
         DropSequence,
         StatusId)
  select isi.OutboundShipmentId,
         i.IssueId,
         od.OutboundDocumentId,
         od.OrderNumber,
         od.ExternalCompanyId,
         i.RouteId,
         i.DropSequence,
         i.StatusId
    from OutboundShipmentIssue isi (nolock)
    join Issue                   i (nolock) on isi.IssueId = i.IssueId
    join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
   where isi.OutboundShipmentId = @OutboundShipmentId
  
  update th
     set ExternalCompanyCode = ec.ExternalCompanyCode,
         ExternalCompany     = ec.ExternalCompany
    from @TableHeader    th
    join ExternalCompany ec (nolock) on th.ExternalCompanyId = ec.ExternalCompanyId
  
  update th
     set Status = s.Status
    from @TableHeader    th
    join Status           s (nolock) on th.StatusId = s.StatusId
  
  update th
     set Route     = r.Route
    from @TableHeader th
    join Route         r (nolock) on th.RouteId = r.RouteId
  
  insert @TableDetail
        (OutboundDocumentId,
         OrderVolume,
         OrderWeight)
  select th.OutboundDocumentId,
         sum(il.Quantity) * sum(p.Quantity),
         sum(il.Quantity) * sum(p.Weight)
    from @TableHeader th
    join OutboundLine ol (nolock) on th.OutboundDocumentId = ol.OutboundDocumentId
    join IssueLine    il (nolock) on ol.OutboundLineId     = il.OutboundLineId
    join Pack          p (nolock) on ol.StorageUnitId      = p.StorageUnitId
   where p.Quantity = 1
  group by th.OutboundDocumentId
  
  select th.OutboundShipmentId
        ,th.IssueId
        ,th.OrderNumber
        ,th.ExternalCompanyCode
        ,th.ExternalCompany
        ,th.RouteId
        ,th.Route
        ,1 as 'DefaultDropSequence'
        ,th.DropSequence
    from @TableHeader th
    left outer
    join @TableDetail td on th.OutboundDocumentId = td.OutboundDocumentId
  order by th.DropSequence, th.OrderNumber
end
