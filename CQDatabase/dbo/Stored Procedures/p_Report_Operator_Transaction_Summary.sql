﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_Transaction_Summary
  ///   Filename       : p_Report_Operator_Transaction_Summary.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2018-05-21
  ///   Details        : Pl001 - Plumblink - add warehouseid as parameter for report
  /// </newpara>
*/
CREATE procedure p_Report_Operator_Transaction_Summary
(
	@WarehouseId		int = null,
	@FromDate			Datetime,
	@ToDate				DateTime
)


 
as
begin

	 set nocount on;
	 
Declare @DwellTime2	varchar(20)	 
	
Declare @TempTable As Table
(
	OperatorGroupId		int,
	OperatorId			int,
	EndDate				dateTime,
	Units				int,
	Jobs				int,
	Weight				numeric(13,3),
	Lines				int,
	DwellTime			int,
	InstructionTypeId	int,
	InstructionType		varchar(30),
	OperatorAverage		int	
)
Declare @TempTable2 As Table
(
	Units				int,
	Jobs				int,
	InstructionTypeId	int,
	Average				int
)

Declare @TempTable3 As Table
(
	OperatorId			int,
	InstructionTypeId	int
)

Declare @TempTable4 As Table
(
	OperatorCount		int,
	InstructionTypeId	int
)

	if @WarehouseId is null
	begin
		set @WarehouseId = (select top 1 WarehouseId from Warehouse w where w.WarehouseId = w.ParentWarehouseId)
	end


	Insert Into @TempTable3 (OperatorId,
							 InstructionTypeId)
	
	SELECT  distinct(ots.OperatorId), 
			ots.InstructionTypeId
	FROM    OperatorTransactionSummary ots 
	INNER JOIN Operator O ON ots.OperatorId = O.OperatorId
	  and ots.WarehouseId = isnull(@WarehouseId, ots.WarehouseId)
	where CONVERT(datetime, CONVERT(varchar,ots.EndDate,101)) between  CONVERT(datetime, CONVERT(varchar,@FromDate,101)) and CONVERT(datetime, CONVERT(varchar,@ToDate,101))
	--Group BY	ots.InstructionTypeId

	

	Insert Into @TempTable4 (OperatorCount,
							InstructionTypeId)
	select count(OperatorId) Operators
		, InstructionTypeId 
	from @TempTable3 
	group by InstructionTypeId
	order by InstructionTypeId
	

	Insert Into @TempTable2 (Units,
							Jobs,
							InstructionTypeId)
	(
	SELECT  Sum(ots.Units) as Units, 
			Sum(ots.Jobs)  as Jobs,
			ots.InstructionTypeId
	FROM    OperatorTransactionSummary ots 
	INNER JOIN Operator O ON ots.OperatorId = O.OperatorId
	where CONVERT(datetime, CONVERT(varchar,ots.EndDate,101)) between  CONVERT(datetime, CONVERT(varchar,@FromDate,101)) and CONVERT(datetime, CONVERT(varchar,@ToDate,101))
	Group BY	ots.InstructionTypeId
	) 
	
	
	update t2
		set Average = t2.Jobs/t4.OperatorCount
    from @TempTable2 t2
    join @TempTable4 t4 on t2.InstructionTypeId = t4.InstructionTypeId
    
    
	Insert Into @TempTable (Units,
							Weight,
							Jobs,
							Lines,
							Dwelltime,
							Enddate,
							OperatorGroupId,
							OperatorId,
							InstructionTypeId)
	(
	SELECT  distinct Sum(ots.Units) as Units, 
			Sum(ots.Weight) as Weight,
			Sum(ots.Jobs) as Jobs,
			Sum(ots.Instructions) as Lines,
			Sum(ots.DwellTime) as DwellTime,
			ots.EndDate ,
			O.OperatorGroupId, 
			O.OperatorId,
			ots.InstructionTypeId
	FROM    OperatorTransactionSummary ots 
	INNER JOIN Operator O ON ots.OperatorId = O.OperatorId
	  and ots.WarehouseId = isnull(@WarehouseId,ots.WarehouseId)
	where CONVERT(datetime, CONVERT(varchar,ots.EndDate,101)) between  CONVERT(datetime, CONVERT(varchar,@FromDate,101)) and CONVERT(datetime, CONVERT(varchar,@ToDate,101))
	--remove next line to add checked
	--and ots.InstructionTypeId <> '9999'
	Group BY	O.OperatorGroupId,
				O.OperatorId,
				EndDate,
				ots.InstructionTypeId
	) 
    update tt
		set InstructionType = 'Checked'
    from @TempTable tt
    where tt.InstructionTypeId = '9999'
    
    update tt
		set tt.InstructionType = it.InstructionType
    from @TempTable tt
    join	InstructionType it (nolock) on it.InstructionTypeId = tt.InstructionTypeId 
    					
select	og.OperatorGroup,
		tt.InstructionType,
		o.Operator,
		sum(isnull(tt.Units,0)) as Units,
		sum(isnull(tt.Jobs,0))as Jobs,
		sum(isnull(tt.Weight,0)) as Weight,
		sum(isnull(tt.Lines,0)) as Lines,
		sum(isnull(tt.DwellTime,0)) as DwellTime,
		t2.Average as 'Average',
		(sum(isnull(tt.Units,0))/sum(isnull(tt.Jobs,0))) as 'OperatorAverage'
from	@TempTable	tt
join	OperatorGroup  og (nolock) on og.OperatorGroupId = tt.OperatorGroupId
join	Operator        o (nolock) on o.OperatorId = tt.OperatorId
--join	InstructionType i (nolock) on i.InstructionTypeId = tt.InstructionTypeId
join	@TempTable2     t2		   on tt.InstructionTypeId = t2.InstructionTypeId
group by og.OperatorGroup,
		  tt.InstructionType,
		   o.Operator,
		  t2.Average,
		    OperatorAverage
order by og.OperatorGroup,
		  tt.InstructionType,
		  o.Operator,
		 t2.Average,
		    OperatorAverage

end
   		
 
