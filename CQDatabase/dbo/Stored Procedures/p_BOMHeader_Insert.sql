﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMHeader_Insert
  ///   Filename       : p_BOMHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:03
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the BOMHeader table.
  /// </remarks>
  /// <param>
  ///   @BOMHeaderId int = null output,
  ///   @StorageUnitId int = null,
  ///   @Description nvarchar(100) = null,
  ///   @Remarks nvarchar(100) = null,
  ///   @Instruction varchar(max) = null,
  ///   @BOMType nvarchar(100) = null,
  ///   @HostId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   BOMHeader.BOMHeaderId,
  ///   BOMHeader.StorageUnitId,
  ///   BOMHeader.Description,
  ///   BOMHeader.Remarks,
  ///   BOMHeader.Instruction,
  ///   BOMHeader.BOMType,
  ///   BOMHeader.HostId,
  ///   BOMHeader.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMHeader_Insert
(
 @BOMHeaderId int = null output,
 @StorageUnitId int = null,
 @Description nvarchar(100) = null,
 @Remarks nvarchar(100) = null,
 @Instruction varchar(max) = null,
 @BOMType nvarchar(100) = null,
 @HostId int = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
  if @BOMHeaderId = '-1'
    set @BOMHeaderId = null;
  
	 declare @Error int
 
  insert BOMHeader
        (StorageUnitId,
         Description,
         Remarks,
         Instruction,
         BOMType,
         HostId,
         Quantity)
  select @StorageUnitId,
         @Description,
         @Remarks,
         @Instruction,
         @BOMType,
         @HostId,
         @Quantity 
  
  select @Error = @@Error, @BOMHeaderId = scope_identity()
  
  
  return @Error
  
end
