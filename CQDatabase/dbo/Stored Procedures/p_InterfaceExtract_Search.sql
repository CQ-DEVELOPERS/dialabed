﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExtract_Search
  ///   Filename       : p_InterfaceExtract_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:18
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceExtract table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceExtract.InterfaceExtractId,
  ///   InterfaceExtract.RecordType,
  ///   InterfaceExtract.SequenceNumber,
  ///   InterfaceExtract.OrderNumber,
  ///   InterfaceExtract.LineNumber,
  ///   InterfaceExtract.RecordStatus,
  ///   InterfaceExtract.ExtractCounter,
  ///   InterfaceExtract.ExtractedDate,
  ///   InterfaceExtract.ProcessedDate,
  ///   InterfaceExtract.Data 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExtract_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceExtract.InterfaceExtractId
        ,InterfaceExtract.RecordType
        ,InterfaceExtract.SequenceNumber
        ,InterfaceExtract.OrderNumber
        ,InterfaceExtract.LineNumber
        ,InterfaceExtract.RecordStatus
        ,InterfaceExtract.ExtractCounter
        ,InterfaceExtract.ExtractedDate
        ,InterfaceExtract.ProcessedDate
        ,InterfaceExtract.Data
    from InterfaceExtract
  
end
