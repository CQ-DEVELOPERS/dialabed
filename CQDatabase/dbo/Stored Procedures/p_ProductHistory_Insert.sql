﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductHistory_Insert
  ///   Filename       : p_ProductHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2014 14:16:27
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ProductHistory table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(510) = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @MinimumQuantity float = null,
  ///   @ReorderQuantity float = null,
  ///   @MaximumQuantity float = null,
  ///   @CuringPeriodDays int = null,
  ///   @ShelfLifeDays int = null,
  ///   @QualityAssuranceIndicator bit = null,
  ///   @RetentionSamples int = null,
  ///   @Samples int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @ProductType nvarchar(40) = null,
  ///   @OverReceipt numeric(13,3) = null,
  ///   @HostId nvarchar(60) = null,
  ///   @ParentProductCode nvarchar(60) = null,
  ///   @DangerousGoodsId int = null,
  ///   @Description2 nvarchar(510) = null,
  ///   @Description3 nvarchar(510) = null,
  ///   @PrincipalId int = null,
  ///   @AssaySamples float = null,
  ///   @Category nvarchar(100) = null,
  ///   @ProductCategoryId int = null,
  ///   @ProductAlias nvarchar(100) = null,
  ///   @ProductGroup nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   ProductHistory.ProductId,
  ///   ProductHistory.StatusId,
  ///   ProductHistory.ProductCode,
  ///   ProductHistory.Product,
  ///   ProductHistory.Barcode,
  ///   ProductHistory.MinimumQuantity,
  ///   ProductHistory.ReorderQuantity,
  ///   ProductHistory.MaximumQuantity,
  ///   ProductHistory.CuringPeriodDays,
  ///   ProductHistory.ShelfLifeDays,
  ///   ProductHistory.QualityAssuranceIndicator,
  ///   ProductHistory.RetentionSamples,
  ///   ProductHistory.Samples,
  ///   ProductHistory.CommandType,
  ///   ProductHistory.InsertDate,
  ///   ProductHistory.ProductType,
  ///   ProductHistory.OverReceipt,
  ///   ProductHistory.HostId,
  ///   ProductHistory.ParentProductCode,
  ///   ProductHistory.DangerousGoodsId,
  ///   ProductHistory.Description2,
  ///   ProductHistory.Description3,
  ///   ProductHistory.PrincipalId,
  ///   ProductHistory.AssaySamples,
  ///   ProductHistory.Category,
  ///   ProductHistory.ProductCategoryId,
  ///   ProductHistory.ProductAlias,
  ///   ProductHistory.ProductGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductHistory_Insert
(
 @ProductId int = null,
 @StatusId int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(510) = null,
 @Barcode nvarchar(100) = null,
 @MinimumQuantity float = null,
 @ReorderQuantity float = null,
 @MaximumQuantity float = null,
 @CuringPeriodDays int = null,
 @ShelfLifeDays int = null,
 @QualityAssuranceIndicator bit = null,
 @RetentionSamples int = null,
 @Samples int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @ProductType nvarchar(40) = null,
 @OverReceipt numeric(13,3) = null,
 @HostId nvarchar(60) = null,
 @ParentProductCode nvarchar(60) = null,
 @DangerousGoodsId int = null,
 @Description2 nvarchar(510) = null,
 @Description3 nvarchar(510) = null,
 @PrincipalId int = null,
 @AssaySamples float = null,
 @Category nvarchar(100) = null,
 @ProductCategoryId int = null,
 @ProductAlias nvarchar(100) = null,
 @ProductGroup nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ProductHistory
        (ProductId,
         StatusId,
         ProductCode,
         Product,
         Barcode,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity,
         CuringPeriodDays,
         ShelfLifeDays,
         QualityAssuranceIndicator,
         RetentionSamples,
         Samples,
         CommandType,
         InsertDate,
         ProductType,
         OverReceipt,
         HostId,
         ParentProductCode,
         DangerousGoodsId,
         Description2,
         Description3,
         PrincipalId,
         AssaySamples,
         Category,
         ProductCategoryId,
         ProductAlias,
         ProductGroup)
  select @ProductId,
         @StatusId,
         @ProductCode,
         @Product,
         @Barcode,
         @MinimumQuantity,
         @ReorderQuantity,
         @MaximumQuantity,
         @CuringPeriodDays,
         @ShelfLifeDays,
         @QualityAssuranceIndicator,
         @RetentionSamples,
         @Samples,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @ProductType,
         @OverReceipt,
         @HostId,
         @ParentProductCode,
         @DangerousGoodsId,
         @Description2,
         @Description3,
         @PrincipalId,
         @AssaySamples,
         @Category,
         @ProductCategoryId,
         @ProductAlias,
         @ProductGroup 
  
  select @Error = @@Error
  
  
  return @Error
  
end
