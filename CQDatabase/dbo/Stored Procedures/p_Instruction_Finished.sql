﻿--IF OBJECT_ID('dbo.p_Instruction_Finished') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Instruction_Finished
--    IF OBJECT_ID('dbo.p_Instruction_Finished') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Instruction_Finished >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Instruction_Finished >>>'
--END
--go

/*  
  /// <summary>  
  ///   Procedure Name : p_Instruction_Finished  
  ///   Filename       : p_Instruction_Finished.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 06 Sep 2007  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure [dbo].[p_Instruction_Finished]  
(  
 @instructionId int,  
 @operatorId    int = null  
)    
as  
begin  
  set nocount on;  
    
  declare @MobileLogId int  
    
  insert MobileLog  
        (ProcName,  
         InstructionId,  
         OperatorId,  
         StartDate)  
  select OBJECT_NAME(@@PROCID),  
         @InstructionId,  
         @operatorId,  
         getdate()  
    
  select @MobileLogId = scope_identity()  
    
  declare @Error               int,  
          @Errormsg            nvarchar(500),  
          @GetDate             datetime,  
          @Rowcount            int,  
          @StatusId            int,  
          @StatusCode          nvarchar(10),  
          @InstructionTypeCode nvarchar(10),  
          @JobId               int,  
          @PreviousStatusId    int,  
          @CreateRemainder     bit,  
          @InstructionRefId    int,  
          @insConfirmed        numeric(13,6),
          @PickLocationId      int,  
          @StoreLocationId     int,  
          @StorageUnitBatchId  int,  
          @PalletId            int,  
          @PalletSUBId         int,  
          @StartDate           datetime,  
          @PickAreaCode        nvarchar(10),  
          @PickWarehouseCode   nvarchar(10), 
		  @PickLocation		   nvarchar(10),
          @StoreAreaCode       nvarchar(10),  
          @StoreWarehouseCode  nvarchar(10),
		  @StoreLocation	   nvarchar(10),
          @PickAreaId          int,  
          @StoreAreaId         int,  
          @WarehouseId         int,  
          @StorageUnitId       int,  
          @SOHQuantity         numeric(13,6),  
          @AdjustmentType      nvarchar(10),
          @ASInstructionType   nvarchar(10),
          @ReasonCode          nvarchar(255),
          @MinimumQuantity     numeric(13,6),
          @MoveId              int,
          @StoreAdjustmentType nvarchar(10)
    
  select @GetDate = dbo.ufn_Getdate()  
    
  select @InstructionTypeCode = it.InstructionTypeCode,  
         @JobId               = i.JobId,  
         @InstructionRefId    = i.InstructionRefId,  
         @StatusCode          = s.StatusCode,  
         @PreviousStatusId    = s.StatusId,  
         @insConfirmed        = ConfirmedQuantity,  
         @CreateRemainder     = case when Quantity > ConfirmedQuantity  
                                     then 1  
                                     else 0  
                                     end,  
         @PickLocationId      = i.PickLocationId,  
         @StoreLocationId     = i.StoreLocationId,  
         @StorageUnitBatchId  = i.StorageUnitBatchId,  
         @PalletId            = i.PalletId,  
         @StartDate           = i.StartDate,  
         @warehouseId         = i.WarehouseId  
    from Instruction      i (nolock)  
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId  
    join Status           s (nolock) on i.StatusId = s.StatusId  
   where InstructionId = @instructionId  
  
  if @InstructionTypeCode not in('STE','STL','STA','STP')  
    if @StatusCode != 'S'
      return 0;
  
  select @StorageUnitId = StorageUnitId  
    from StorageUnitBatch (nolock)  
   where StorageUnitBatchId = @StorageUnitBatchId  
    
  select @PickAreaCode      = a.AreaCode,  
         @PickAreaId        = a.AreaId,  
         @PickWarehouseCode = a.WarehouseCode,
		 @PickLocation		= a.WarehouseCode2
    from Area          a (nolock)  
    join AreaLocation al (nolock) on a.AreaId = al.AreaId  
   where al.LocationId = @PickLocationId  
    
  select @StoreAreaCode      = a.AreaCode,  
         @StoreAreaId        = a.AreaId,  
         @StoreWarehouseCode = a.WarehouseCode,
		 @StoreLocation		 = a.WarehouseCode2
    from Area          a (nolock)  
    join AreaLocation al (nolock) on a.AreaId = al.AreaId  
   where al.LocationId = @StoreLocationId  
     and a.Area != 'SMD'  
    
  begin transaction  
    
  select @AdjustmentType    = AdjustmentType,
         @ASInstructionType = InstructionTypeCode,
         @ReasonCode        = ReasonCode,
         @StoreAdjustmentType = StoreAdjustmentType
    from AreaSequence (nolock)  
   where PickAreaId = @PickAreaId  
     and StoreAreaId = @StoreAreaId  
    
  if @AdjustmentType is not null and @InstructionTypeCode in ('PM','PS','FM','PR','P','S','SM','M','R','O')  
  begin  

	if (select AreaCode from Area where AreaId = @PickAreaId) != 'JJ'
		set @insConfirmed = @insConfirmed * -1

    if @AdjustmentType = 'ADJ'  
    begin  
      if @StoreWarehouseCode is not null  
      begin  
        insert InterfaceExportStockAdjustment  
              (RecordType,  
               RecordStatus,  
               ProductCode,  
               Product,
               SKUCode,  
               Batch,  
               Quantity,  
               Additional1,
               Additional3,
               PrincipalCode)  
        select @AdjustmentType,  
               'N',  
               p.ProductCode,  
               p.Product,
               sku.SKUCode,  
               b.Batch,  
               @insConfirmed,  
               @StoreWarehouseCode,
               @ReasonCode,
               pn.PrincipalCode
          from StorageUnitBatch sub (nolock)
          join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId          = p.ProductId
          join SKU              sku (nolock) on su.SKUId              = sku.SKUId
          join Batch              b (nolock) on sub.BatchId            = b.BatchId
          left
          join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
         where sub.StorageUnitBatchId = @StorageUnitBatchId
          
        select @Error = @@Error  
          
        if @Error <> 0  
          goto error  
      end  
    end  
    else if @AdjustmentType in ('SOH','MOW')  
    begin  
      if @PickWarehouseCode is not null  
      begin  
        insert InterfaceExportStockAdjustment  
              (RecordType,  
               RecordStatus,  
               ProductCode,  
               SKUCode,  
               Batch,  
               Quantity,  
               Additional1,
               Additional3)  
        select @AdjustmentType,  
               'N',  
               vs.ProductCode,  
               vs.SKUCode,  
               vs.Batch,  
               @insConfirmed * -1,  
               @PickWarehouseCode,
               @ReasonCode
          from viewStock vs  
         where StorageUnitBatchId = @StorageUnitBatchId  
          
        select @Error = @@Error  
          
        if @Error <> 0  
          goto error  
      end  
        
      if isnull(@StoreWarehouseCode,'') != ''
      begin  
        insert InterfaceExportStockAdjustment  
              (RecordType,  
               RecordStatus,  
               ProductCode,   
               Product,
               SKUCode,  
               Batch,  
               Quantity,  
               Additional1,
               PrincipalCode)
        select @AdjustmentType,  
               'N',  
               p.ProductCode,
               p.Product,
               sku.SKUCode,  
               b.Batch,  
               @insConfirmed,  
          @StoreWarehouseCode,
               pn.PrincipalCode
          from StorageUnitBatch sub (nolock)
       join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId  = p.ProductId
          join SKU              sku (nolock) on su.SKUId              = sku.SKUId
          join Batch              b (nolock) on sub.BatchId            = b.BatchId
          left
          join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
         where sub.StorageUnitBatchId = @StorageUnitBatchId
          
        select @Error = @@Error  
          
        if @Error <> 0  
          goto error  
      end  
    end  
    else if @AdjustmentType in ('XFR','WHT')
    begin  
      if @PickWarehouseCode is not null and @StoreWarehouseCode is not null  
      begin  
        insert InterfaceExportStockAdjustment  
              (RecordType,  
               RecordStatus,  
               ProductCode,  
               Product, 
               SKUCode,  
               Batch,  
               Quantity,  
               Additional1,  
               Additional2,
               Additional3,
			   Additional4,
			   Additional5,
               PrincipalCode)  
        select @AdjustmentType,  
               'N',  
               p.ProductCode,  
               p.Product,
               sku.SKUCode,  
               b.Batch,  
               @insConfirmed,  
               @PickWarehouseCode,  
               @StoreWarehouseCode,
			   @ReasonCode,
			   @PickLocation,
			   @StoreLocation,
               pn.PrincipalCode
          from StorageUnitBatch sub (nolock)
          join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId          = p.ProductId
          join SKU              sku (nolock) on su.SKUId              = sku.SKUId
          join Batch              b (nolock) on sub.BatchId            = b.BatchId
          left
          join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
         where sub.StorageUnitBatchId = @StorageUnitBatchId
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
       
       -- This was done for Sensory to move the stock from Raw to Samples and then also remove from Samples
       if @StoreAdjustmentType is not null
       begin
         insert InterfaceExportStockAdjustment  
               (RecordType,  
                RecordStatus,  
                ProductCode,  
                Product, 
                SKUCode,  
                Batch,  
                Quantity,  
                Additional1,  
                Additional2,
                Additional3,
                PrincipalCode)  
         select @StoreAdjustmentType,  
                'N',  
                p.ProductCode,  
                p.Product,
                sku.SKUCode,  
                b.Batch,  
                @insConfirmed * -1,  
                @StoreWarehouseCode,  
                null,
                @ReasonCode,
                pn.PrincipalCode
           from StorageUnitBatch sub (nolock)
           join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
           join Product            p (nolock) on su.ProductId          = p.ProductId
           join SKU              sku (nolock) on su.SKUId              = sku.SKUId
           join Batch              b (nolock) on sub.BatchId            = b.BatchId
           left
           join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
          where sub.StorageUnitBatchId = @StorageUnitBatchId
         
         select @Error = @@Error
         
         if @Error <> 0
           goto error
       end
   End  
 End  
    else if @PickWarehouseCode is not null and @StoreWarehouseCode is not null  
        and @PickWarehouseCode <> @StoreWarehouseCode and @InstructionTypeCode = isnull(@ASInstructionType, @InstructionTypeCode)  
    begin  
        declare @InterfaceExportHeaderId int  
          
        insert InterfaceExportHeader  
              (PrimaryKey,
               OrderNumber,
          RecordType,  
               RecordStatus,  
               FromWarehouseCode,  
               ToWarehouseCode,  
               Additional1,  
               Remarks,
               PrincipalCode)
        select distinct 
			            i.InstructionId,
			            i.InstructionId,
			            '67',  
               'W',  
                @PickWarehouseCode,  
                @StoreWarehouseCode,  
                @AdjustmentType,  
                'CQ Transfer:' + isnull(o.Operator,'') + ' '  + it.InstructionTypeCode + ' From ' + isnull(picka.Area,'None') + ' ' +   
                isnull(pickl.Location,'None') + ' to ' + isnull(storel.Location,'None') + ' RefNo:' + Isnull(j.ReferenceNumber, i.InstructionId) collate Latin1_General_CI_AS,
                null
           from Instruction        i
           join Status            si (nolock) on i.StatusId           = si.StatusId  
           join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId  
           join Job                j (nolock) on i.JobId              = j.JobId  
           join Status             s (nolock) on j.StatusId           = s.StatusId  
           left  
           join Location       pickl (nolock) on i.PickLocationId = pickl.LocationId  
           left  
           join AreaLocation  pickal (nolock) on pickl.LocationId = pickal.LocationId  
           left  
           join Area           picka (nolock) on pickal.AreaId     = picka.AreaId  
           left  
           join Location      storel (nolock) on i.StoreLocationId = storel.LocationId  
           left  
           join AreaLocation storeal (nolock) on storel.LocationId = storeal.LocationId  
           left  
           join Area          storea (nolock) on storel.LocationId = storeal.LocationId  
           left  
           join Operator           o (nolock) on j.OperatorId = o.OperatorId  
          where InstructionId = @InstructionId  
        
        select @Error = @@Error, @InterfaceExportHeaderId = @@IDENTITY
        
        if @Error <> 0
          goto error
          
        Insert InterfaceExportDetail  
              (InterfaceExportHeaderId,  
               LineNumber,  
               ProductCode,  
               Product,  
               SKUCode,  
               Batch,  
               Quantity,  
               Additional3)
        Select @InterfaceExportHeaderId,  
               1,  
               p.ProductCode,  
               p.Product,  
               sku.SKUCode,  
               b.Batch,  
               @insConfirmed,  
               @StoreWarehouseCode
          from StorageUnitBatch sub (nolock)
          join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId          = p.ProductId
          join SKU              sku (nolock) on su.SKUId              = sku.SKUId
          join Batch              b (nolock) on sub.BatchId            = b.BatchId
         where sub.StorageUnitBatchId = @StorageUnitBatchId
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
          
        update InterfaceExportHeader set Recordstatus = 'N' Where InterfaceExportHeaderId = @InterfaceExportHeaderId  
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
      end  
  end  
  
  --update j
  --   set StatusId = dbo.ufn_StatusId('J','RL')
  --  from Instruction      i (nolock)
  --  join Job              j (nolock) on i.JobId = j.JobId
  --  join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
  --                                  and it.InstructionTypeCode = 'M'
  --  join Status           s (nolock) on j.StatusId = s.StatusId
  --                                  and s.StatusCode = 'PS'
  -- where i.InstructionRefId = @InstructionId
  --   and i.PickLocationId = @StoreLocationId
  
  --select @Error = @@Error
  
  --if @Error <> 0
  --  goto error
  
  select @MoveId = InstructionId
    from Instruction      i (nolock)
    join Job              j (nolock) on i.JobId = j.JobId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                                    and it.InstructionTypeCode = 'M'
    join Status           s (nolock) on j.StatusId = s.StatusId
                                    and s.StatusCode = 'PS'
   where i.InstructionRefId = @InstructionId
  
  update j
     set StatusId = dbo.ufn_StatusId('J','RL')
    from Instruction      i (nolock)
    join Job              j (nolock) on i.JobId = j.JobId
   where i.InstructionId = @MoveId
     and i.PickLocationId = @StoreLocationId
  
  select @Error = @@Error, @rowcount = @@rowcount
  
  if @Error <> 0
    goto error
  
  if @rowcount = 0
  begin
    if exists(select top 1 1
                from AreaLocation al (nolock)
                join Area          a (nolock) on al.AreaId = a.AreaId
               where a.AreaCode = 'S' -- Was transaction stored in a staging location
                 and al.LocationId = @StoreLocationId) -- Is there a staging trasaction for this pallet??
    begin
    
      exec @Error = p_StorageUnitBatchLocation_Deallocate
       @instructionId = @MoveId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Instruction_Update
        @instructionId     = @MoveId,
        @PickLocationId    = @StoreLocationId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @instructionId = @MoveId
      
      if @Error <> 0
        goto error
      
      update j
         set StatusId = dbo.ufn_StatusId('J','RL')
        from Instruction      i (nolock)
        join Job              j (nolock) on i.JobId = j.JobId
       where i.InstructionId = @MoveId
         and i.PickLocationId = @StoreLocationId
      
      select @Error = @@Error, @rowcount = @@rowcount
      
      if @Error <> 0
        goto error
    end
    else
    begin -- Delete as transaction was overridden
      exec p_Housekeeping_Instruction_Delete
       @InstructionId = @MoveId
      
      if @Error <> 0
        goto error
    end
  end
  
  --if exists(select top 1 1 
  --            from MovementSequence ms (nolock)
  --           where PickAreaId = @PickAreaId
  --             and StoreAreaId = @StoreAreaId
  --             and StoreAreaId = StageAreaId) -- if staging area is same as store area then insert another movement from store / stage again
  begin
    exec @Error = p_Instruction_MovementSequence
     @InstructionId = @InstructionId,
     @operatorId    = @operatorId
    
    if @Error <> 0
      goto error
  end
  --if @InstructionTypeCode in ('PM','PS','FM','PR','P')  
  --  if datediff(ss, @StartDate, @GetDate) < 1 -- Cannot finish in less than 1 sec  
  --    goto error  
    
  --if @InstructionTypeCode in ('STE','STL','STA','STP')  
  --  if datediff(ss, @StartDate, @GetDate) = 0 -- Cannot finish in less than 1 sec  
  --    goto error  
  
  if @InstructionTypeCode in ('PM','PS','FM','PR','P')  
  begin  
    if (select dbo.ufn_Configuration(267, @WarehouseId)) = 1  
    begin  
      exec @Error = p_Production_Adjustment  
       @InstructionId = @InstructionId,  
       @Sign          = '+-'  
        
      if @Error <> 0  
        goto error  
    end
    
    if @CreateRemainder = 1   
    begin  
      exec @Error = p_Instruction_Reference  
       @InstructionId = @instructionId  
        
      if @Error <> 0  
        goto error  
    end  
    else if (select dbo.ufn_Configuration(336, @warehouseId)) = 1 -- Request Replenishments on 0
    begin  
      select @SOHQuantity = sum(subl.ActualQuantity) + sum(subl.AllocatedQuantity)  
        from StorageUnitBatchLocation subl (nolock)  
        join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId  
        join Location                    l (nolock) on subl.LocationId         = l.LocationId  
        join AreaLocation               al (nolock) on l.LocationId            = al.LocationId  
        join Area                        a (nolock) on al.AreaId               = a.AreaId  
       where sub.StorageUnitId = @StorageUnitId  
         and l.LocationId    = @PickLocationId  
         and l.ActivePicking = 1  
         and a.AreaCode     in ('PK','SP')
        
      if @SOHQuantity is null  
        set @SOHQuantity = 0  
        
      if @SOHQuantity = 0 and exists(select 1  
                                       from StorageUnitLocation sul (nolock)  
                                       join AreaLocation         al (nolock) on sul.LocationId            = al.LocationId  
                                       join Area                  a (nolock) on al.AreaId               = a.AreaId  
                                      where a.AreaCode in ('PK','SP')
                                        and sul.StorageUnitId = @StorageUnitId  
                                        and sul.LocationId = @PickLocationId
										and a.Replenish = 1)
      begin  
        exec @Error = p_Mobile_Request_Replenishment @InstructionId = @InstructionId, @ShowMsg = 0 -- Ignore errors  
      end  
    end
    
    if (select dbo.ufn_Configuration(391, @warehouseId)) = 1 -- Request Replenishments on Minimum Qty Reached
    begin  
      select @SOHQuantity = sum(subl.ActualQuantity) + sum(subl.AllocatedQuantity)  
        from StorageUnitBatchLocation subl (nolock)  
        join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId  
        join Location                    l (nolock) on subl.LocationId         = l.LocationId  
        join AreaLocation               al (nolock) on l.LocationId            = al.LocationId  
        join Area                        a (nolock) on al.AreaId               = a.AreaId  
       where sub.StorageUnitId = @StorageUnitId  
         and l.LocationId    = @PickLocationId  
         and l.ActivePicking = 1  
         and a.AreaCode     in ('PK','SP')
         and a.Replenish = 1
      
      if @SOHQuantity is null  
        set @SOHQuantity = 0
      
      select @MinimumQuantity = sul.MinimumQuantity
        from StorageUnitLocation sul (nolock)  
        join AreaLocation         al (nolock) on sul.LocationId            = al.LocationId  
        join Area                  a (nolock) on al.AreaId               = a.AreaId  
       where a.AreaCode in ('PK','SP')  
         and sul.StorageUnitId = @StorageUnitId  
         and sul.LocationId    = @PickLocationId
      
      if @MinimumQuantity is null
        set @MinimumQuantity = 0
      
      if @MinimumQuantity > @SOHQuantity
      begin  
        exec @Error = p_Mobile_Request_Replenishment @InstructionId = @InstructionId, @ShowMsg = 0 -- Ignore errors  
      end  
    end
  end
  
  if @InstructionTypeCode in ('PM','PS','FM')
  begin
    if dbo.ufn_Configuration(134, @WarehouseId) = 1  
    begin  
      exec @Error = p_Temporary_Pickface_Remove  
       @InstructionId = @instructionId  
        
      if @Error <> 0  
        goto error  
    end  
    else -- GS 2014-05-05 - Don't create on Temporary Pick?
    if @CreateRemainder = 1
    begin
      if dbo.ufn_Configuration(448, @WarehouseId) = 1 and @PickAreaCode in ('PK','SP')
      begin
        exec @Error = p_Mobile_Location_Error
         @instructionId   = @instructionId,
         @pickError       = 1,
         @storeError     = 0,
         @createStockTake = 1
      end
    end
  end
  
  --if @operatorId is null  
  begin  
    exec @Error = p_StorageUnitBatchLocation_Allocate  
     @InstructionId       = @instructionId,  
  @Confirmed           = 1  
      
    if @Error <> 0  
      goto error
  end
  
  if @InstructionTypeCode not in ('M', 'STL')
  begin
	  update subl
		 set ActualQuantity = case when isnull(i.ConfirmedQuantity,0) > subl.ActualQuantity
								   then 0
								   else subl.ActualQuantity - isnull(i.ConfirmedQuantity,0)
								   end
		from Instruction                 i (nolock)
		join StorageUnitBatchLocation subl (nolock) on i.StorageUnitBatchId = subl.StorageUnitBatchId
												   and i.StoreLocationId = subl.LocationId
		join AreaLocation               al (nolock) on i.PickLocationId = al.LocationId -- Pick Area is auto store, subtract from the store area
		join Area                        a (nolock) on al.AreaId = a.AreaId
	   where a.AutoStore = 1
		 --and i.JobId = @JobId
		 and i.InstructionId = @instructionId

		--Logging Transactions Delete when no in use 2016/08/02
		INSERT INTO StorageUnitBatchLocationTriggerLog(
			LocationId, 
			StorageUnitBatchId, 
			Query, 
			CurrentTime, 
			AllocatedQuantity, 
			ActualQuantity)
		SELECT 
			subl.LocationId, 
			subl.StorageUnitBatchId,
			'InstructionId = ' + CAST(i.InstructionId AS NVARCHAR)  + ', JobId = ' + CAST(i.JobId AS NVARCHAR) + ', InstructionTypeCode = ' + it.InstructionTypeCode AS Query,
			GETDATE(),
			subl.ActualQuantity,
			i.ConfirmedQuantity			
		from Instruction                 i (nolock)
		join InstructionType it on it.InstructionTypeId = i.InstructionTypeId
		join StorageUnitBatchLocation subl (nolock) on i.StorageUnitBatchId = subl.StorageUnitBatchId
												   and i.StoreLocationId = subl.LocationId
		join AreaLocation               al (nolock) on i.PickLocationId = al.LocationId -- Pick Area is auto store, subtract from the store area
		join Area                        a (nolock) on al.AreaId = a.AreaId
	   where a.AutoStore = 1
		 and i.JobId = @JobId
		 and i.InstructionId = @instructionId

	  
	  select @Error = @@ERROR
	  
	  if @Error <> 0  
		goto error
  end
  select @StatusId = dbo.ufn_StatusId('I','F')  
    
  exec @Error = p_Instruction_Update  
   @InstructionId = @instructionId,  
   @StatusId      = @StatusId,  
   @EndDate       = @GetDate  
    
  if @Error <> 0  
    goto error  
    
  if @InstructionTypeCode not in('STE','STL','STA','STP')  
  begin  
    if @InstructionTypeCode in('PM','PS','FM','P')  
    begin  
      exec @Error = p_Instruction_Update_ili  
       @InstructionId    = @InstructionId,  
       @InstructionRefId = @InstructionRefId,  
       @insConfirmed     = @insConfirmed  
        
      if @Error <> 0  
        goto error  
        
      if @PickAreaCode = 'PK' -- Added Mezzanine 2009-09-16  
      begin  
        select @PalletSUBId = p.StorageUnitBatchId    
          from Pallet             p (nolock)    
          join StorageUnitBatch sub (nolock) on p.StorageUnitBatchId = sub.StorageUnitBatchId  
         where p.PalletId        = @PalletId    
           and sub.StorageUnitId = @StorageUnitId  
          
        exec @Error = p_Instruction_Update  
         @InstructionId      = @InstructionId,  
         @StorageUnitBatchId = @PalletSUBId  
          
        if @Error <> 0  
          goto error  
      end  
    end  
      
    exec @Error = p_Status_Rollup  
     @jobId               = @JobId,  
     @instructionId       = @instructionId  
      
    if @Error <> 0  
      goto error  
      
    if @InstructionTypeCode = 'P'
      if dbo.ufn_Configuration(138, @warehouseId) = 1 -- Rollup up pick twice RL to CK to CD
      begin
        exec @Error = p_Status_Rollup
         @jobId               = @JobId,
         @instructionId       = @instructionId
        
  if @Error <> 0
          goto error
      end
    
    if @InstructionTypeCode in('PM','PS','FM')
    begin
      if dbo.ufn_Configuration(239, @warehouseId) = 1 -- Rollup up pick twice RL to CK to CD
      begin
        exec @Error = p_Status_Rollup
         @jobId               = @JobId,
         @instructionId       = @instructionId
        
        if @Error <> 0
          goto error
      end
    end
    
    if (select sum(i.ConfirmedQuantity)  
          from Instruction i (nolock)  
          join Status     si (nolock) on i.StatusId = si.StatusId  
          join Job         j (nolock) on i.JobId    = j.JobId  
          join Status     sj (nolock) on j.StatusId = sj.StatusId  
         where j.JobId      = @JobId  
           and sj.StatusCode = 'CK'  
           and si.StatusCode = 'F') = 0  
    begin  
      exec @Error = p_Status_Rollup  
       @jobId               = @JobId  
        
      if @Error <> 0  
        goto error  
    end  
      
    if @InstructionTypeCode in ('PR','SM','S')
    begin
      if @StoreLocationId is null
      begin
        set @Error = -1
        goto error
      end
    end
    
    if  dbo.ufn_Configuration(62,  @warehouseId) = 1 -- Track Batch into Pickface  
    and dbo.ufn_Configuration(157, @warehouseId) = 1 -- Merge Batches  
    and @StoreAreaCode in('PK','SP')
      if @InstructionTypeCode in ('R','M','PR','SM','S')  
      begin  
        exec @Error = p_Location_Fix_Pickface  
         @InstructionId = @InstructionId  
          
        if @Error <> 0  
          goto error  
      end  
      
    if @InstructionTypeCode in ('R','M','P','PR','SM','S')  
    begin  
      if @StoreAreaCode in ('BK','SP') -- Added specials for Mezzanine 2009-09-16  
      and dbo.ufn_Configuration(157, @warehouseId) = 1 -- Merge Batches  
      begin  
        exec @Error = p_Location_Fix_Bulk  
         @InstructionId      = @InstructionId,  
         @StorageUnitBatchId = @StorageUnitBatchId  
          
        if @Error <> 0  
          goto error  
      end  
        
      if (@PickAreaCode) = 'BK'  
      begin  
        select @PalletSUBId = p.StorageUnitBatchId    
          from Pallet             p (nolock)    
          join StorageUnitBatch sub (nolock) on p.StorageUnitBatchId = sub.StorageUnitBatchId  
         where p.PalletId        = @PalletId    
           and sub.StorageUnitId = @StorageUnitId  
          
        exec @Error = p_Instruction_Update  
         @InstructionId      = @InstructionId,  
         @StorageUnitBatchId = @PalletSUBId  
          
        if @Error <> 0  
          goto error  
      end  
    end  
    
    exec @Error = p_Pallet_Update
     @PalletId           = @PalletId,
     @Locationid         = @StoreLocationId
      
    if @Error <> 0  
      goto error
      
    --if @InstructionTypeCode in ('PR','SM','S')  
    --begin  
    --  exec @Error = p_Instruction_AreaSequence  
    --   @InstructionId = @InstructionId,  
    --   @OperatorId    = @OperatorId  
    --end  
      
--    if @InstructionTypeCode in ('PR','SM','S') and @StoreAreaCode = 'PK'  
--    begin  
--      exec @Error = p_Location_Merge  
--       @StorageUnitBatchId = @StorageUnitBatchId,  
--       @LocationId         = @StoreLocationId,  
--       @ConfirmedQuantity  = @insConfirmed  
--        
--      if @Error <> 0  
--        goto error  
--    end  
  end  
    
  exec @Error = p_Operator_Update  
   @OperatorId      = @operatorId,  
   @LastInstruction = @GetDate  
    
  if @Error <> 0  
    goto error  
  
  if (select dbo.ufn_Configuration(387, @WarehouseId)) = 1  
  begin
    exec @Error = p_Instruction_Auto_Complete @InstructionId = @instructionId
    
    if @Error <> 0  
      goto error
  end
    
  commit transaction  

  if @InstructionTypeCode = 'PR'  
  begin  
    if dbo.ufn_Configuration(119, @warehouseId) = 1  
    if @InstructionRefId is not null  
    if (select InstructionTypeCode  
          from Instruction      i  
          join InstructionType it on i.InstructionTypeId = it.InstructionTypeId  
         where InstructionId = @InstructionRefId) = 'P'  
    begin  
      exec @Error = p_StorageUnitBatchLocation_Reserve  
       @InstructionId       = @InstructionRefId  
        
      if @Error <> 0  
        goto error  
        
      exec @Error = p_Instruction_Finished  
       @InstructionId = @InstructionRefId  
        
      if @Error <> 0  
        goto error  
    end  
  end  
    
  --exec p_SerialNumber_Finished
  -- @InstructionId = @instructionId
  --,@OperatorId    = @operatorId
    
  select @StatusCode = s.StatusCode  
    from Instruction i (nolock)  
    join Status      s (nolock) on i.StatusId = s.StatusId  
   where InstructionId = @instructionId  
    
  update MobileLog  
     set EndDate    = getdate(),  
         ErrorMsg   = @Errormsg,  
         StatusCode = @StatusCode  
   where MobileLogId = @MobileLogId  
    
  return 0  
    
  error:  
    set @Errormsg = 'Error executing p_Instruction_Finished'  
      
    select @StatusCode = s.StatusCode  
      from Instruction i (nolock)  
      join Status      s (nolock) on i.StatusId = s.StatusId  
     where InstructionId = @instructionId  
      
    update MobileLog  
       set EndDate    = getdate(),  
           ErrorMsg   = @Errormsg,  
           StatusCode = @StatusCode  
     where MobileLogId = @MobileLogId  
      
    raiserror(@Errormsg,16,1)
    rollback transaction  
    if @Error is null  
      set @Error = 0  
    return @Error  
end  
 
--go
--IF OBJECT_ID('dbo.p_Instruction_Finished') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Instruction_Finished >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Instruction_Finished >>>'
--go


