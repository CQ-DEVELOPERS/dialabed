﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKU_Select
  ///   Filename       : p_SKU_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the SKU table.
  /// </remarks>
  /// <param>
  ///   @SKUId int = null 
  /// </param>
  /// <returns>
  ///   SKU.SKUId,
  ///   SKU.UOMId,
  ///   SKU.SKU,
  ///   SKU.SKUCode,
  ///   SKU.Quantity,
  ///   SKU.AlternatePallet,
  ///   SKU.SubstitutePallet,
  ///   SKU.Litres,
  ///   SKU.ParentSKUCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKU_Select
(
 @SKUId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         SKU.SKUId
        ,SKU.UOMId
        ,SKU.SKU
        ,SKU.SKUCode
        ,SKU.Quantity
        ,SKU.AlternatePallet
        ,SKU.SubstitutePallet
        ,SKU.Litres
        ,SKU.ParentSKUCode
    from SKU
   where isnull(SKU.SKUId,'0')  = isnull(@SKUId, isnull(SKU.SKUId,'0'))
  
end
