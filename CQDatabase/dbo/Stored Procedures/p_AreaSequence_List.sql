﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaSequence_List
  ///   Filename       : p_AreaSequence_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaSequence table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   AreaSequence.AreaSequenceId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaSequence_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as AreaSequenceId
        ,null as 'AreaSequence'
  union
  select
         AreaSequence.AreaSequenceId
        ,AreaSequence.AreaSequenceId as 'AreaSequence'
    from AreaSequence
  
end
