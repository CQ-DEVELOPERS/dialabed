﻿
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_SendToProblemStock
  ///   Filename       : p_Mobile_SendToProblemStock.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_SendToProblemStock
(
 @WarehouseId			int,
 @OrderNumber			nvarchar(30),
 @Location				nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
	 (
	  Id					int identity,
	  operatorId            int,
	  storageUnitBatchId    int,
	  pickLocation			nvarchar(30),
	  pickLocationId	    int,
	  storeLocation			nvarchar(30),
	  storeLocationId		int,
	  confirmedQuantity     int,
	  instructionTypeCode   nvarchar(30),
	  JobId					int
	 )
	 
	declare @TableResult2 as table
	 (
	  Id					int identity,
	  IssueId				int,
	  IssueLineId		    int,
	  JobId					int
	 )
	 
	   declare	@Error					int,
				@Id						int,
				@operatorId				int,
				@storageUnitBatchId		int,
				@pickLocation			nvarchar(30),
				@pickLocationId			int,
				@storeLocation			nvarchar(30),
				@storeLocationId		int,
				@confirmedQuantity		int,
				@instructionTypeCode	nvarchar(30),
				@JobId					int,
				@LocationId				int,
				@IssueId			    int,
				@IssueLineId		    int,
				@Branded				bit,
				@CheckingCount			int,
				@StatusId				int,
				@IssueStatusId			int,
				@AreaCode				nvarchar(50),
				@Downsizing				bit,
				@OutboundDocumentId		int,
				@Error2					bit
	
				       			
	select @LocationId = l.LocationId,
	       @Location = l.Location
						 from Location		l (nolock) 
						 join AreaLocation al (nolock) on l.locationid = al.locationid
						 join Area			a (nolock) on a.areaid = al.areaid 
						where (l.Location = @Location or CONVERT(nvarchar(10), l.SecurityCode) = @Location)
						  and a.Warehouseid = @WarehouseId
						  
	set @AreaCode = (select AreaCode 
					   from Area		  a (nolock)
				       join AreaLocation al (nolock) on a.AreaId = al.AreaId
				      where al.Locationid = @LocationId)
	
	if @AreaCode != 'WSP'
	begin
		set @error = -1
		goto error
	end
		
					      
	set @IssueId = (select IssueId 
					  from OutboundDocument od (nolock) 
					  join Issue             i (nolock)  on od.OutboundDocumentId = i.OutboundDocumentId
				     where od.OrderNumber = @OrderNumber)
								  
	set @IssueStatusId = (select i.StatusId 
							from OutboundDocument od (nolock) 
							join Issue             i (nolock)  on od.OutboundDocumentId = i.OutboundDocumentId
						   where od.OrderNumber = @OrderNumber)
		set  @CheckingCount = (select CheckingCount 
							 from Issue i (nolock)
							where i.IssueId = @IssueId)
	
	set @OutboundDocumentId = (select OutboundDocumentId 
								 from OutboundDocument od (nolock)
								where od.OrderNumber = @OrderNumber)

	
	  insert @TableResult2
             (IssueId,
              IssueLineId,
              JobId)
      select ili.IssueId,
             ili.IssueLineId,
             ili.JobId
        from viewili ili 
       where ili.ordernumber = @OrderNumber
       
	 
		exec @Error = p_Issue_Update
			 @IssueId		= @IssueId,
			 @DespatchBay	= @LocationId

			 
	    exec @Error = p_OutboundDocument_Update
			 @OutboundDocumentId	= @OutboundDocumentId,
			 @ToLocation			= @Location   
		
	insert @TableResult
            (operatorId,
             storageUnitBatchId,
             pickLocation,
             pickLocationId,
             storeLocation,
             storeLocationId,
             confirmedQuantity,
             instructionTypeCode,
             ili.JobId)
      select ili.operatorid,
             ili.StorageUnitBatchId,
             ili.PickLocation,
             ili.PickLocationId,
             ili.StoreLocation,
             ili.StoreLocationId,
             ili.confirmedQuantity,
             'M',
             JobId
        from viewili ili 
       where ili.ordernumber = @OrderNumber
         --and JobCode = 'QA'
  
  set @Error = 0  
  --select * from @TableResult
  if @Error = 0
  while exists(select top 1 1 from @TableResult)
  begin
	select	@Id							= Id,
			@operatorId					= operatorId,
			@storageUnitBatchId			= storageUnitBatchId,
			@pickLocation				= StoreLocation,
			@pickLocationId				= StoreLocationId,
			@storeLocation				= @Location,
			@confirmedQuantity			= confirmedQuantity,
			@instructionTypeCode		= instructionTypeCode,
			@JobId						= JobId
	from @TableResult tr	

    delete @TableResult where Id = @Id

    if @confirmedQuantity > 0
    begin
    exec @Error = p_Mobile_Auto_Movement
		 @operatorId			=@operatorId,
		 @storageUnitBatchId	=@storageUnitBatchId,
		 @pickLocation			=@pickLocation,
		 @storeLocation			=@storeLocation,
		 @confirmedQuantity		=@confirmedQuantity,
		 @instructionTypeCode	=@instructionTypeCode
	
	SET @Error = 1
	end	 
  update instruction 
	 set storelocationId = @LocationId
   where jobid = @jobid		
  
	end	
  
  --exec p_SOH_Allocated_Reserved_Quantity_Fix	
  goto result
  
  error:
    RAISERROR 900000 'Error'
	return
  result:
   set @Error = 1
    select @Error as '@Error'
    return @Error
    	
 end
 
 
 
