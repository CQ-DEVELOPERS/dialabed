﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_On_Hand_Serial_Number
  ///   Filename       : p_Report_Stock_On_Hand_Serial_Number.sql
  ///   Create By      : Karen
  ///   Date Created   : Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_On_Hand_Serial_Number
(
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @BatchId           int = null,
 @PrincipalId       int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	   ProductCode        nvarchar(30),
	   Product            nvarchar(255),
	   StorageUnitId	  int,
	   Batch              nvarchar(30),
	   BatchId			  int,
	   ActualQuantity     numeric(13,6),
	   PrincipalId		  int,
	   PrincipalCode	  nvarchar(50),
	   SerialNumber		  nvarchar(50)	      
	 )
	 
	 declare @Matrix as Table
	  (
	   ProductCode			nvarchar(30),
	   Product				nvarchar(255),
	   Batch				nvarchar(30),
	   PrincipalCode		nvarchar(50),   
	   SerialNumber			nvarchar(50),
	   ActualQuantity       numeric(13,6),
	   Sequence				int,
	   SetNumber			int
	  );
  
	declare @InboundSequence	int,
			@ShowWeight			bit = null,
			@StoreLocation		varchar(15),
			@PickLocation		varchar(15)			

 
  if @StorageUnitId = -1 set @StorageUnitId = null
  if @BatchId = -1 set @BatchId = null
  if @PrincipalId = -1 set @PrincipalId = null
  
  --insert @TableResult
  --      (ProductCode,
		-- Product,
		-- StorageUnitId,
		-- Batch,
		-- BatchId,
		-- ActualQuantity,
		-- PrincipalId,
		-- SerialNumber)
  --select distinct  p.ProductCode,
  --       p.Product,  
  --       su.StorageUnitId,       
  --       case when s.StatusCode != 'A'
  --            then b.Batch + '('+ s.StatusCode + ')'
  --            else b.Batch
  --            end as 'Batch',
  --       b.BatchId,
  --       0,
  --       p.PrincipalId,
  --       sn.SerialNumber
  --  from StorageUnitBatchLocation subl (nolock)
  --  join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  --  join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
  --  join Product                     p (nolock) on su.ProductId            = p.ProductId
  --  join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
  --  join Batch                       b (nolock) on sub.BatchId             = b.BatchId
  --  join Status                      s (nolock) on b.StatusId              = s.StatusId
  --  join Location                    l (nolock) on subl.Locationid         = l.LocationId
  --  join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
  --  join Area                        a (nolock) on al.AreaId               = a.AreaId
  --  left join SerialNumber			sn (nolock) on b.BatchId		       = sn.BatchId
  -- where a.WarehouseId      = @WarehouseId
  --   and su.StorageUnitId   = isnull(@StorageUnitId, su.StorageUnitId)
  --   and b.BatchId          = isnull(@BatchId, b.BatchId)
  --   and a.StockOnHand      = 1
  --   and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
  
  
  insert @TableResult
        (ProductCode,
		 Product,
		 StorageUnitId,
		 Batch,
		 BatchId,
		 ActualQuantity,
		 PrincipalId,
		 SerialNumber)
  select distinct  p.ProductCode,
         p.Product,  
         su.StorageUnitId,       
         b.Batch,
         b.BatchId,
         0,
         p.PrincipalId,
         sn.SerialNumber
   from SerialNumber			sn 
   join batch b on b.BatchId		       = sn.BatchId
   join StorageUnit su on su.StorageUnitId = sn.StorageUnitId
   join Product p on su.ProductId = p.ProductId
   where sn.StorageUnitId   = isnull(@StorageUnitId, sn.StorageUnitId)
     and b.BatchId          = isnull(@BatchId, b.BatchId)
     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
   
  
  select @InboundSequence = max(InboundSequence)
    from PackType (nolock)
    
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal        p (nolock) on tr.PrincipalId = p.PrincipalId
  
   --select * from @TableResult
   
  update tr
     set ActualQuantity = (select COUNT(sn.SerialNumber) from SerialNumber  sn
							where tr.BatchId = sn.BatchId
							  and tr.StorageUnitId = sn.StorageUnitId
						      and sn.ReceiptId is not null
							  and sn.IssueId is null )
    from @TableResult tr
    
    
  insert @Matrix
        (ProductCode,
         Product,
         PrincipalCode,
         Batch,
         ActualQuantity,
         SerialNumber,
         sequence,
         SetNumber)
  select distinct 
         td.ProductCode,
         td.Product,
         td.PrincipalCode,
         td.Batch,
         td.ActualQuantity,
         td.SerialNumber,
         ROW_NUMBER() OVER (partition by ProductCode ORDER BY ProductCode) Rank,
         1
    from @TableResult td 
   where ProductCode is not null
     and SerialNumber is not null 
     
    
     
    while exists (select sequence from @Matrix
		where Sequence > 6)
	begin
		update mx
		   set Sequence = (Sequence - 6),
			   SetNumber = SetNumber + 1
	      from @Matrix mx
	     where Sequence > 6
	end
    
	select * from @Matrix
	
end
