﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Change_Location
  ///   Filename       : p_Job_Change_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Change_Location
(
 @jobId      int,
 @locationId int
)
 
as
begin
	 set nocount on;
  
  declare @TableInstructions as table
  (
   InstructionId       int,
   WarehouseId         int,
   PickLocationId      int,
   StoreLocationId     int,
   Quantity            float,
   StatusCode          nvarchar(10)
  )
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @Quantity            float,
          @InstructionId       int,
          @StatusCode          nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  set @ErrorMsg = 'Error executing p_Job_Change_Location'
  
  insert @TableInstructions
        (InstructionId,
         WarehouseId,
         PickLocationId,
         StoreLocationId,
         Quantity,
         StatusCode)
  select i.InstructionId,
         i.WarehouseId,
         i.PickLocationId,
         i.StoreLocationId,
         i.Quantity,
         s.StatusCode
    from Instruction i (nolock)
    join Status      s (nolock) on i.StatusId = s.StatusId
   where i.JobId = @jobId
  
  begin transaction
  
  while exists(select top 1 1 from @TableInstructions)
  begin
    select top 1
           @InstructionId       = InstructionId,
           @Quantity            = Quantity,
           @StatusCode          = StatusCode
      from @TableInstructions
    
    delete @TableInstructions
     where InstructionId = @InstructionId
    
    --select Location,
    --       ProductCode,
    --       Product,
    --       SKUCode,
    --       Batch,
    --       ActualQuantity,
    --       AllocatedQuantity,
    --       ReservedQuantity
    --  from viewQty
    -- where LocationId in (select StoreLocationId from viewili where InstructionId = @InstructionId)
    
    if @StatusCode = 'F'
    begin
      exec @Error = p_StorageUnitBatchLocation_Reverse
       @instructionId = @instructionId,
       @Pick          = 0,
       @Store         = 1,
       @Confirmed     = 1
      
      if @Error <> 0
      begin
        set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Deallocate'
        goto error
      end
    end
      exec @Error = p_StorageUnitBatchLocation_Deallocate
       @instructionId = @instructionId,
       @Pick          = 0,
       @Store         = 1,
       @Confirmed     = 1
      
      if @Error <> 0
      begin
        set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Deallocate'
        goto error
      end
    
    --select Location,
    --       ProductCode,
    --       Product,
    --       SKUCode,
    --       Batch,
    --       ActualQuantity,
    --       AllocatedQuantity,
    --       ReservedQuantity
    --  from viewQty
    -- where LocationId in (select StoreLocationId from viewili where InstructionId = @InstructionI
    
    update Instruction
       set Stored = null
     where InstructionId = @InstructionId
    
    select @Error = @@error
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'Error updating Instruction'
      goto error
    end
    
    exec @Error = p_Instruction_Update
      @instructionId      = @instructionId,
      @storeLocationId    = @locationId
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'Error executing p_Instruction_Update'
      goto error
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @instructionId = @instructionId,
     @Pick          = 0,
     @Store         = 1,
     @Confirmed     = 1
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
      goto error
    end
    
    if @StatusCode = 'F'
    begin
      exec @Error = p_StorageUnitBatchLocation_Allocate
       @instructionId = @instructionId,
       @Pick          = 0,
       @Store         = 1,
       @Confirmed     = 1
      
      if @Error <> 0
      begin
        set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
        goto error
      end
    end
  end
  
  commit transaction
  return 0
  
  error:
    raiserror 900000 @ErrorMsg
    rollback transaction
    return @Error
end
