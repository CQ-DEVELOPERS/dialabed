﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackExternalCompany_Search
  ///   Filename       : p_PackExternalCompany_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 21:17:05
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the PackExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null output,
  ///   @ExternalCompanyId int = null output,
  ///   @StorageUnitId int = null output,
  ///   @FromPackId int = null,
  ///   @ToPackId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   PackExternalCompany.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   PackExternalCompany.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompany,
  ///   PackExternalCompany.StorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   PackExternalCompany.FromPackId,
  ///   Pack.PackId,
  ///   PackExternalCompany.FromQuantity,
  ///   PackExternalCompany.ToPackId,
  ///   Pack.PackId,
  ///   PackExternalCompany.ToQuantity,
  ///   PackExternalCompany.Comments 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackExternalCompany_Search
(
 @WarehouseId int = null output,
 @ExternalCompanyId int = null output,
 @StorageUnitId int = null output,
 @FromPackId int = null,
 @ToPackId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @FromPackId = '-1'
    set @FromPackId = null;
  
  if @ToPackId = '-1'
    set @ToPackId = null;
  
 
  select
         PackExternalCompany.WarehouseId
        ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,PackExternalCompany.ExternalCompanyId
        ,ExternalCompanyExternalCompanyId.ExternalCompany as 'ExternalCompany'
        ,PackExternalCompany.StorageUnitId
        ,PackExternalCompany.FromPackId
        ,PackExternalCompany.FromQuantity
        ,PackExternalCompany.ToPackId
        ,PackExternalCompany.ToQuantity
        ,PackExternalCompany.Comments
    from PackExternalCompany
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = PackExternalCompany.WarehouseId
    left
    join ExternalCompany ExternalCompanyExternalCompanyId on ExternalCompanyExternalCompanyId.ExternalCompanyId = PackExternalCompany.ExternalCompanyId
    left
    join StorageUnit StorageUnitStorageUnitId on StorageUnitStorageUnitId.StorageUnitId = PackExternalCompany.StorageUnitId
    left
    join Pack PackFromPackId on PackFromPackId.PackId = PackExternalCompany.FromPackId
    left
    join Pack PackToPackId on PackToPackId.PackId = PackExternalCompany.ToPackId
   where isnull(PackExternalCompany.WarehouseId,'0')  = isnull(@WarehouseId, isnull(PackExternalCompany.WarehouseId,'0'))
     and isnull(PackExternalCompany.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(PackExternalCompany.ExternalCompanyId,'0'))
     and isnull(PackExternalCompany.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(PackExternalCompany.StorageUnitId,'0'))
     and isnull(PackExternalCompany.FromPackId,'0')  = isnull(@FromPackId, isnull(PackExternalCompany.FromPackId,'0'))
     and isnull(PackExternalCompany.ToPackId,'0')  = isnull(@ToPackId, isnull(PackExternalCompany.ToPackId,'0'))
  --order by Comments
  
end
