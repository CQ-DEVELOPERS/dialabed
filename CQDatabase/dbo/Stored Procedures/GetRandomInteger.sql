﻿
CREATE PROCEDURE GetRandomInteger
(
@Min int,
@Max int,
@rv int OUTPUT
)
AS
BEGIN
DECLARE @Rand float

SELECT @Rand = ((@Max - @Min + 1) * RAND(CONVERT(int, CONVERT(varbinary, NEWID())))) + .5 + (@Min - 1)

SELECT @rv = ROUND(@Rand, 0)

END
