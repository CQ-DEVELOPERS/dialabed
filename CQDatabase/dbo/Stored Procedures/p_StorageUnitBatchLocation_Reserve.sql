﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocation_Reserve
  ///   Filename       : p_StorageUnitBatchLocation_Reserve.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Jul 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Reserve
(
 @InstructionId int
,@Pick          bit = 1 -- (0 = false, 1 = true)
,@Store         bit = 1 -- (0 = false, 1 = true)
,@Confirmed     bit = 1 -- (0 = false, 1 = true)
)
 
as
begin
	 set nocount on
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @StorageUnitBatchId int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @Quantity           numeric(13,6),
          @ReservedQuantity   numeric(13,6),
          @AllocatedQuantity  numeric(13,6)
--          @PalletId           int
   
   if @Confirmed = 0
     select @PickLocationId      = PickLocationId,
            @StoreLocationId     = StoreLocationId,
            @StorageUnitBatchId  = StorageUnitBatchId,
            @Quantity            = Quantity
       from Instruction
      where InstructionId = @InstructionId
   else
     select @PickLocationId      = PickLocationId,
            @StoreLocationId     = StoreLocationId,
            @StorageUnitBatchId  = StorageUnitBatchId,
            @Quantity            = isnull(ConfirmedQuantity,0)
       from Instruction
      where InstructionId = @InstructionId
  
  --begin transaction
  
--  -- Pick Location cannot = store location 
--  if @PickLocationId = @StoreLocationId and @Pick = 1
--  begin
--    set @error = -1
--    goto error
--  end
  
  if @PickLocationId is not null and @Pick = 1
  begin
    if (select Picked
          from Instruction
         where InstructionId = @InstructionId) is null
    begin
      select @ReservedQuantity = ReservedQuantity
        from StorageUnitBatchLocation
       where StorageUnitBatchId = @StorageUnitBatchId
         and LocationId         = @PickLocationId
      
      if @ReservedQuantity is null -- No row found
      begin
        -- Insert a row into StorageUnitBatchLocation.AllocatedQuantity
        exec @error = p_StorageUnitBatchLocation_Insert
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @PickLocationId,
         @ActualQuantity     = 0,
         @AllocatedQuantity  = 0,
         @ReservedQuantity   = @Quantity
        
        if @error <> 0
          goto error
      end
      else
      begin
        set @ReservedQuantity = @ReservedQuantity + @Quantity
        
        if @ReservedQuantity < 0
          set @ReservedQuantity = 0
        
--        select @PalletId = PalletId
--          from Pallet (nolock)
--         where StorageUnitBatchId = @StorageUnitBatchId
--           and LocationId         = @PickLocationId
        
        -- Update the StorageUnitBatchLocation.ReservedQuantity Column
        exec @error = p_StorageUnitBatchLocation_Update
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @PickLocationId,
         @ActualQuantity     = null, -- Don't update
         @AllocatedQuantity  = null, -- Don't update
         @ReservedQuantity   = @ReservedQuantity
        
        if @error <> 0
          goto error
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId = @InstructionId,
       @Picked        = 0
--       @PalletId      = @PalletId
      
      if @error <> 0
        goto error
     end
  end
  
  if @StoreLocationId is not null and @Store = 1
  begin
    if (select Stored
          from Instruction
         where InstructionId = @InstructionId) is null
    begin
      select @AllocatedQuantity = AllocatedQuantity
        from StorageUnitBatchLocation
       where StorageUnitBatchId = @StorageUnitBatchId
         and LocationId = @StoreLocationId
      
      if @AllocatedQuantity is null -- No row found
      begin
        -- Insert a row into StorageUnitBatchLocation.AllocatedQuantity
        exec @error = p_StorageUnitBatchLocation_Insert
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @StoreLocationId,
         @ActualQuantity     = 0,
         @AllocatedQuantity  = @Quantity,
         @ReservedQuantity   = 0
        
        if @error <> 0
          goto error
        
        -- Increment the Location.Used column
        exec @error = p_Location_Update
         @LocationId = @StoreLocationId,
         @Used       = 1
        
        if @error <> 0
          goto error
      end
      else
      begin
        set @AllocatedQuantity = @AllocatedQuantity + @Quantity
        
        if @AllocatedQuantity < 0
          set @AllocatedQuantity = 0
        
        -- Update the StorageUnitBatchLocation.AllocatedQuantity Column
        exec @error = p_StorageUnitBatchLocation_Update
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @StoreLocationId,
         @ActualQuantity     = null, -- Don't update
         @AllocatedQuantity  = @AllocatedQuantity,
         @ReservedQuantity   = null  -- Don't update
        
        if @error <> 0
          goto error
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId = @InstructionId,
       @Stored        = 0
      
      if @error <> 0
        goto error
     end
  end
  
  --commit transaction
  return
  
  error:
    --rollback transaction
    return @error
end
