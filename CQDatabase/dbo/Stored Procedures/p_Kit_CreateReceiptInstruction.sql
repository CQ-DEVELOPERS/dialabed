﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Kit_CreateReceiptInstruction
  ///   Filename       : p_Kit_CreateReceiptInstruction.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@OrderNumber>
  ///       <@StorageUnitId>
  ///       <@WarehouseId>
  ///       <@Quantity>
  ///       <@BatchId>	 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Kit_CreateReceiptInstruction
(
     @BOMInstructionId  int
    ,@OrderNumber       varchar(50)
    ,@StorageUnitId     int
    ,@WarehouseId       int
    ,@Quantity          float
    ,@BatchId           int = null)
as

begin

/*
--**proc variables and test data
    declare
         @OrderNumber    varchar(50)
        ,@StorageUnitId  int
        ,@WarehouseId    int
        ,@Quantity       float
        ,@BatchId         int
        
     select 
         @OrderNumber    = 'test1'
        ,@StorageUnitId  = 124
        ,@WarehouseId    = 1
        ,@Quantity       = 5
    
--**End test data
*/
    declare
         @ExternalCompanyId         int
        ,@IssueId                   int
        ,@InboundDocumentId         int
        ,@InboundDocumentTypeId     int
        ,@InboundLineId             int
        ,@InboundShipmentId         int
        ,@StorageUnitBatchId        int   
        ,@StatusId                  int
        ,@LineNumber                int
        ,@DeliveryDate              datetime
        ,@GetDate                   datetime
        ,@Error                     int
        ,@ErrorMsg                  varchar(100)
        ,@ProductCode               varchar(30)
        ,@Product                   varchar(255)
        ,@ReceiptId                 int
        
    select
         @InboundDocumentTypeId = 6
        ,@StatusId = dbo.ufn_StatusId('ID','W')
        ,@DeliveryDate = dbo.ufn_Getdate()
        ,@GetDate = dbo.ufn_Getdate()
        
    begin transaction KitReceipt

    select @ExternalCompanyId   = ExternalCompanyId
    from ExternalCompany (nolock)  
    where ExternalCompanyCode = 'KIT'
     
    if @ExternalCompanyId is null  
    begin  
      exec @Error = p_ExternalCompany_Insert  
       @ExternalCompanyId         = @ExternalCompanyId output,  
       @ExternalCompanyTypeId     = 1,  
       @RouteId                   = null,  
       --@drop  
       @ExternalCompany           = 'Internal kitting area',  
       @ExternalCompanyCode       = 'KIT'  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'  
        goto error_header  
      end  
    end

    exec @Error = p_InboundDocument_Insert
       @InboundDocumentId     = @InboundDocumentId output,
       @InboundDocumentTypeId = @InboundDocumentTypeId,
       @ExternalCompanyId     = @ExternalCompanyId,
       @StatusId              = @StatusId,
       @WarehouseId           = @WarehouseId,
       @OrderNumber           = @OrderNumber,
       @DeliveryDate          = @GetDate,
       @CreateDate            = @GetDate,
       @ModifiedDate          = null
      
    if @Error != 0
    begin
        select @ErrorMsg = 'Error executing p_InboundDocument_Insert'
        goto error_header
    end
      
    select @ProductCode = p.ProductCode
          ,@Product = p.Product
          ,@LineNumber = 1 
    from BOMHeader bh
    inner join StorageUnit su on su.StorageUnitId = bh.StorageUnitId
    inner join Product p on su.ProductId = p.ProductId
    where bh.StorageUnitId = @StorageUnitId

    exec @Error = p_interface_xml_Product_Insert
         @ProductCode        = @ProductCode
        ,@Product            = @Product
        ,@WarehouseId        = @WarehouseId
        ,@StorageUnitId      = @StorageUnitId output
        ,@StorageUnitBatchId = @StorageUnitBatchId output
       
    if @Error != 0 or @StorageUnitBatchId is null
    begin
        select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
        goto error_detail
    end
    
    exec @Error = p_InboundLine_Insert
     @InboundLineId     = @InboundLineId output,
     @InboundDocumentId = @InboundDocumentId,
     @StorageUnitId     = @StorageUnitId,
     @StatusId          = @StatusId,
     @LineNumber        = @LineNumber,
     @Quantity          = @Quantity,
     @BatchId           = @BatchId
       
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_InboundLine_Insert'
      goto error_detail
    end    
    
    exec @Error = p_Receiving_Create_Receipt
         @InboundDocumentId = @InboundDocumentId,
         @InboundLineId     = @InboundLineId,
         @OperatorId        = null
         
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Receiving_Create_Receipt'
      goto error_detail
    end
       
    update BOMInstruction
       Set InboundDocumentId = @InboundDocumentId
     where BOMInstructionId = @BOMInstructionId
         
    if @@Error != 0
    begin
      select @ErrorMsg = 'Error updating BOMInstruction'
      goto error_detail
    end
    
    update id
       set StorageUnitId = od.StorageUnitId,
           BatchId       = od.BatchId
      from BOMInstruction   bi
      join OutboundDocument od on bi.OutboundDocumentId = od.OutboundDocumentId
      join InboundDocument  id on bi.InboundDocumentId  = id.InboundDocumentId
     where bi.BOMInstructionId = @BOMInstructionId
       and id.StorageUnitId is null
    
    if @@Error != 0
    begin
      select @ErrorMsg = 'Error updating InboundDocument'
      goto error_detail
    end
    
    select @ReceiptId = ReceiptId
          ,@StatusId = dbo.ufn_StatusId('R','OH')
    From Receipt
    Where InboundDocumentId = @InboundDocumentId
    
    exec @Error = p_Receipt_Update_Status @ReceiptId, @StatusId
    
    error_detail:
    error_header:
    
    if @Error = 0
    begin     
      if @@trancount > 0
        commit transaction KitReceipt
      
    end
    else
    begin      
      if @@trancount > 0
        rollback transaction KitReceipt
      
    end
        
end
