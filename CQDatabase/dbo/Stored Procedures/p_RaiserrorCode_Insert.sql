﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RaiserrorCode_Insert
  ///   Filename       : p_RaiserrorCode_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:35
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the RaiserrorCode table.
  /// </remarks>
  /// <param>
  ///   @RaiserrorCodeId int = null output,
  ///   @RaiserrorCode nvarchar(20) = null,
  ///   @RaiserrorMessage nvarchar(510) = null,
  ///   @StoredProcedure nvarchar(256) = null 
  /// </param>
  /// <returns>
  ///   RaiserrorCode.RaiserrorCodeId,
  ///   RaiserrorCode.RaiserrorCode,
  ///   RaiserrorCode.RaiserrorMessage,
  ///   RaiserrorCode.StoredProcedure 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RaiserrorCode_Insert
(
 @RaiserrorCodeId int = null output,
 @RaiserrorCode nvarchar(20) = null,
 @RaiserrorMessage nvarchar(510) = null,
 @StoredProcedure nvarchar(256) = null 
)
 
as
begin
	 set nocount on;
  
  if @RaiserrorCodeId = '-1'
    set @RaiserrorCodeId = null;
  
  if @RaiserrorCode = '-1'
    set @RaiserrorCode = null;
  
	 declare @Error int
 
  insert RaiserrorCode
        (RaiserrorCode,
         RaiserrorMessage,
         StoredProcedure)
  select @RaiserrorCode,
         @RaiserrorMessage,
         @StoredProcedure 
  
  select @Error = @@Error, @RaiserrorCodeId = scope_identity()
  
  
  return @Error
  
end
