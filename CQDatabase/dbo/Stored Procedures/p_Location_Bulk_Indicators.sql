﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Bulk_Indicators
  ///   Filename       : p_Location_Bulk_Indicators.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Bulk_Indicators
(
 @StorageUnitId int,
 @LocationId    int
)
 
as
begin
	 set nocount on;
  
  declare @Locations as table
  (
   LocationId        int,
   Used              bit,
   ActiveBinning     bit,
   ActivePicking     bit,
   AvailableQuantity float,
   StockTakeInd      bit,
   PalletQuantity    float,
   Pallets           numeric(13,3)
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @AreaId            int,
          @BiningCount       int,
          @BinningLocationId int,
          @PickingCount      int,
          @PickingLocationId int,
          @ActiveBinning     bit,
          @ActivePicking     bit,
          @PalletQuantity    float
  
  set @Error = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @AreaId = al.AreaId
    from AreaLocation al
   where al.LocationId = @LocationId
  
  if (select AreaCode from Area where AreaId = @AreaId) != 'BK'
    return
  
  select @PalletQuantity = Quantity
    from Pack pk
   where StorageUnitId = @StorageUnitId
     and PackTypeId = 1
  
  -- Get Locations to check
  insert @Locations
        (LocationId,
         Used,
         ActiveBinning,
         ActivePicking,
         AvailableQuantity,
         StockTakeInd,
         PalletQuantity,
         Pallets)
  select l.LocationId,
         l.Used,
         l.ActiveBinning,
         l.ActivePicking,
         sum(subl.ActualQuantity) - sum(subl.ReservedQuantity),
         l.StockTakeInd,
         l.PalletQuantity,
         sum(convert(numeric(13,3), (subl.ActualQuantity + subl.AllocatedQuantity)) / convert(numeric(13,3), @PalletQuantity))
    from Location                    l
    join AreaLocation               al on l.LocationId = al.LocationId
    join StorageUnitBatchLocation subl on al.LocationId = subl.LocationId
    join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
   where sub.StorageUnitId = @StorageUnitId
     and al.AreaId         = @AreaId
  group by l.LocationId,
           l.Used,
           l.ActiveBinning,
           l.ActivePicking,
           subl.ActualQuantity - subl.ReservedQuantity,
           l.StockTakeInd,
           l.PalletQuantity
  
  begin transaction
  
  --if Current Location empty turn binning on
  update Location
     set ActiveBinning = 1
   where LocationId = @LocationId
     and isnull(Used,0) = 0
  
--  --if Current Location empty turn binning on
--  update @Locations
--     set ActiveBinning = 1,
--         ActivePicking = 0
--   where LocationId = @LocationId
--     and isnull(Used,0) = 0
--  
--  if @@rowcount > 0 or (select ActiveBinning from @Locations where LocationId = @LocationId) = 1
--    -- Set all other location's binning off
--    update @Locations
--       set ActiveBinning = 0
--     where LocationId != @LocationId
  
  select @BiningCount = count(1) from @Locations where ActiveBinning = 1 and Pallets < PalletQuantity
  
  -- is there one location for picking?
  if @BiningCount = 0 or @BiningCount > 1
  begin
    -- Set all location binning 0ff
    update @Locations
       set ActiveBinning = 0
    
    -- set location with the least stock bining on
    select top 1 @BinningLocationId = locs.LocationId
      from @Locations               locs
      join StorageUnitBatchLocation subl on locs.LocationId = subl.LocationId
     where locs.ActivePicking = 0
       and Pallets < PalletQuantity
    order by Pallets desc
    
    if @BinningLocationId is null
      select top 1 @BinningLocationId = locs.LocationId
        from @Locations               locs
        join StorageUnitBatchLocation subl on locs.LocationId = subl.LocationId
       where Pallets < PalletQuantity
      order by Pallets desc
    
    if @BinningLocationId is null
      select top 1 @BinningLocationId = locs.LocationId
        from @Locations               locs
        join StorageUnitBatchLocation subl on locs.LocationId = subl.LocationId
      order by Pallets
    
    update @Locations
       set ActiveBinning = 1
     where LocationId = @BinningLocationId
  end
  
  update @Locations
     set ActivePicking = 0 -- make picking false
   where ActivePicking = 1 -- picking true
     and StockTakeInd  = 1 -- stock take true
  
  select @PickingCount = count(1) from @Locations where ActivePicking = 1 and AvailableQuantity >= @PalletQuantity
  
  -- is there one location for picking?
  if @PickingCount = 0 or @PickingCount > 1
  begin
    -- Set all location picking 0ff
    update @Locations
       set ActivePicking = 0
    
    -- set location with the least stock picking on
    select top 1 @PickingLocationId = LocationId
      from @Locations
     where ActiveBinning = 0
       and AvailableQuantity > 0
       and AvailableQuantity >= @PalletQuantity
       and StockTakeInd = 0
    order by AvailableQuantity desc
    
    if @@rowcount = 0
      -- set location with the lest stock picking on
      select top 1 @PickingLocationId = locs.LocationId
        from @Locations               locs
       where AvailableQuantity > 0
         and AvailableQuantity >= @PalletQuantity
      order by AvailableQuantity desc
      
    update @Locations
       set ActivePicking = 1
     where LocationId = @PickingLocationId
  end
  
  declare location_cursor cursor for
   select	LocationId,
          ActiveBinning,
          ActivePicking
  	  from	@Locations
  
  open location_cursor

  fetch location_cursor into @LocationId,
                             @ActiveBinning,
                             @ActivePicking

  while (@@fetch_status = 0)
  begin
    exec p_Location_Update
     @LocationId    = @LocationId,
     @ActiveBinning = @ActiveBinning,
     @ActivePicking = @ActivePicking
    
    if @Error != 0
      goto cursorerror
    
    fetch location_cursor into @LocationId,
                               @ActiveBinning,
                               @ActivePicking
  end
  
  commit transaction
  return
  
  cursorerror:
    close location_cursor
    deallocate location_cursor
  
  error:
    raiserror 900000 'Error executing p_Location_Bulk_Indicators'
    rollback transaction
    return @Error
end
