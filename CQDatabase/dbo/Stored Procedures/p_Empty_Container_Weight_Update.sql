﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Empty_Container_Weight_Update
  ///   Filename       : p_Empty_Container_Weight_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Empty_Container_Weight_Update
(
 @palletId    int,
 @tareWeight  numeric(13,6) = null,
 @nettWeight  numeric(13,6) = null,
 @grossWeight numeric(13,6) = null,
 @emptyWeight numeric(13,6) = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @tareWeight < 0
    set @tareWeight = null
  
  if @grossWeight < 0
    set @grossWeight = null
  
  if @emptyWeight < 0
    set @emptyWeight = null
  
  begin transaction
  
  exec @Error = p_Pallet_Update
   @PalletId    = @palletId,
   @Tare        = @tareWeight,
   @Nett        = @nettWeight,
   @Weight      = @grossWeight,
   @EmptyWeight = @emptyWeight
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Empty_Container_Weight_Update'
    rollback transaction
    return @Error
end
