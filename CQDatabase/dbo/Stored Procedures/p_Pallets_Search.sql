﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallets_Search
  ///   Filename       : p_Pallets_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:55
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Pallets table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Pallets.pallet_id,
  ///   Pallets.stock_no,
  ///   Pallets.sku_code,
  ///   Pallets.lot_code 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallets_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         Pallets.pallet_id
        ,Pallets.stock_no
        ,Pallets.sku_code
        ,Pallets.lot_code
    from Pallets
  
end
