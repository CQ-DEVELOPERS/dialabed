﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceSupplierProduct_Search
  ///   Filename       : p_InterfaceSupplierProduct_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:48
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceSupplierProduct table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceSupplierProduct.SupplierCode,
  ///   InterfaceSupplierProduct.ProductCode,
  ///   InterfaceSupplierProduct.ProcessedDate,
  ///   InterfaceSupplierProduct.RecordStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceSupplierProduct_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceSupplierProduct.SupplierCode
        ,InterfaceSupplierProduct.ProductCode
        ,InterfaceSupplierProduct.ProcessedDate
        ,InterfaceSupplierProduct.RecordStatus
    from InterfaceSupplierProduct
  
end
