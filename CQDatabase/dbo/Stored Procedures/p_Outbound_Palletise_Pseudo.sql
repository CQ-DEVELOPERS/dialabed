﻿/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_Pseudo
  ///   Filename       : p_Outbound_Palletise_Pseudo.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Outbound_Palletise_Pseudo
(
 @WarehouseId        int,
 @ToWarehouseId      int,
 @StoreLocationId    int,
 @OutboundShipmentId int,
 @IssueId            int,
 @OperatorId         int,
 @AlternatePallet    bit = 0,
 @SubstitutePallet   bit = 0,
 @JobId              int = null output
)
 
AS
 
declare @pseudo_DropSequence as table
(
 DropSequence int,
 AreaSequence int
)

declare @pseudo_sku as table
(
 SKUCode nvarchar(15)
)
 
declare @pseudo_stock as table
(
 StorageUnitBatchId int,
 Quantity           float
)

begin
  
  begin transaction
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @DropSequence       int,
          @DropSequenceCount  int,
          @AreaSequence       int,
          @SKUCode            nvarchar(15),
          @SKUCount           int,
          @ProductCount       int,
          @PackQuantity       float,
          @qty                float,
          @IssueLineId        int,
          @StorageUnitBatchId int,
          @jc_qty             float,
          @stock_qty          float,
          @new_qty            float,
          @err                nvarchar(100),
          @PriorityId         int,
          @StatusId           int,
          @GetDate            datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @PriorityId = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'PS'
  
  /**********************/
  /* Process each drop  */
  /**********************/
  insert @pseudo_DropSequence
  select DropSequence,
         AreaSequence
    from PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
   group by DropSequence, AreaSequence
  
  select @DropSequenceCount = @@rowcount
  
  WHILE @DropSequenceCount > 0
  begin
    select top 1
           @DropSequence = DropSequence,
           @AreaSequence = AreaSequence
      from @pseudo_DropSequence
    order by DropSequence desc, AreaSequence
    
    delete @pseudo_DropSequence
     where DropSequence = @DropSequence
       and AreaSequence = @AreaSequence
    
    select @DropSequenceCount = @DropSequenceCount - 1
           
    /*******************************/
    /* Process each stock lot item */
    /* don't process exceptions    */
    /*******************************/
    INSERT @pseudo_sku
    select distinct ps.SKUCode
      from PalletiseStock ps
      join SKU           sku on ps.SKUCode = sku.SKUCode
     where ps.DropSequence       = @DropSequence
       and ps.AreaSequence       = @AreaSequence
       and ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and ps.IssueId            = isnull(@IssueId, -1)
       and ps.SKUCode <> '0'
       and sku.Quantity > 1
    
    select @SKUCount = @@rowcount
    
    WHILE @SKUCount > 0
    begin
      set ROWCOUNT 1
             
      select @SKUCode = SKUCode
        from @pseudo_sku
             
      delete from @pseudo_sku
       where SKUCode = @SKUCode
      
      set ROWCOUNT 0
      
      select @SKUCount = @SKUCount - 1
      
--      select top 1
--             @PackQuantity = p.Quantity
--        from Pack               p
--        join PackType          pt on p.PackTypeId    = pt.PackTypeId
--        join StorageUnit       su on p.StorageUnitId = su.StorageUnitId
--        join SKU              sku on su.SKUid        = sku.SKUId
--       where sku.SKUCode = @SKUCode
--         and pt.PackType = 'Pallets'
      
      if isnull(@AlternatePallet, 0) = 0 and isnull(@SubstitutePallet, 0) = 0
        select @PackQuantity = skui.Quantity
          from SKU      sku
          join SKUInfo skui on sku.SKUId = skui.SKUId
         where sku.SKUCode      = @SKUCode
           and skui.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
      else if @AlternatePallet = 1 -- Palletising change for Branches 2008-05-23
        select @PackQuantity = skui.AlternatePallet
          from SKU      sku
          join SKUInfo skui on sku.SKUId = skui.SKUId
         where sku.SKUCode      = @SKUCode
           and skui.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
      else if @SubstitutePallet = 1 -- Palletising change for Exports 2008-06-10
        select @PackQuantity = skui.SubstitutePallet
          from SKU      sku
          join SKUInfo skui on sku.SKUId = skui.SKUId
         where sku.SKUCode      = @SKUCode
           and skui.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
      
      /**********************************/
      /* Get the actual qty of stock to */
      /* be palletised                  */
      /**********************************/
      select @qty = isnull(SUM(Quantity),0)
        from PalletiseStock
       where DropSequence       = @DropSequence
         and AreaSequence       = @AreaSequence
         and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and IssueId            = isnull(@IssueId, -1)
         and SKUCode            = @SKUCode
      group by DropSequence, SKUCode
      
      /**********************************/
      /* Create each pseudo full pallet */
      /**********************************/
      if @PackQuantity = 0
      begin
        set @Error = -1
        select @ErrorMsg = 'p_pseudo_palletise - Pack Qty = 0. SKU = ' + Convert(nvarchar(13),@SKUCode)
        goto error
      end
      
      while floor(@qty/@PackQuantity) > 0
      begin
        /***************************************/
        /* Determine which stock will make up  */
        /* the pseudo pallet                   */
        /***************************************/
        insert @pseudo_stock
              (StorageUnitBatchId, Quantity)
        select StorageUnitBatchId, Quantity
          from PalletiseStock
         where DropSequence       = @DropSequence
           and AreaSequence       = @AreaSequence
           and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and IssueId            = isnull(@IssueId, -1)
           and SKUCode            = @SKUCode
           and Quantity           > 0
        order by Quantity desc
        
        select @ProductCount = @@RowCount
        
        select @jc_qty = @PackQuantity
        
        select @StatusId = dbo.ufn_StatusId('IS','P')
        
        if (dbo.ufn_Configuration(477, @WarehouseId)) = 0 
          or @JobId is null
		begin
        exec @Error = p_Job_Insert
         @JobId           = @JobId output,
         @PriorityId      = @PriorityId,
         @OperatorId      = @OperatorId,
         @StatusId        = @StatusId,
         @WarehouseId     = @WarehouseId,
         @IssueLineId     = @IssueLineId,
         @DropSequence    = @DropSequence
        
        if @Error <> 0
          goto error
        end
        exec @Error = p_OutboundPerformance_Insert
         @JobId         = @JobId,
         @CreateDate    = @GetDate
        
        if @error <> 0
          goto error
        
        while @jc_qty > 0 and @ProductCount > 0
        begin
          set @StorageUnitBatchId = null
          
          select top 1 @StorageUnitBatchId = StorageUnitBatchId
            from @pseudo_stock
           where Quantity <= @jc_qty
          order by Quantity desc
          
          if @StorageUnitBatchId is null
            select top 1 @StorageUnitBatchId = StorageUnitBatchId
              from @pseudo_stock
            order by Quantity desc
          
          delete @pseudo_stock
           where StorageUnitBatchId = @StorageUnitBatchId
          
          select @ProductCount = @ProductCount - 1
          
          select top 1
                 @stock_qty   = Quantity,
                 @IssueLineId = IssueLineId
            from PalletiseStock
           where DropSequence       = @DropSequence
             and AreaSequence       = @AreaSequence
             and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
             and IssueId            = isnull(@IssueId, -1)
             and StorageUnitBatchId = @StorageUnitBatchId
          
          set rowcount 1
          --select @DropSequence as '@DropSequence', @SKUCode as 'SKUCode', @JobId as 'JobId', @jc_qty as 'jc_qty', @stock_qty as 'stock_qty'
          if @jc_qty > @stock_qty
          begin
            update PalletiseStock
               set Quantity         = Quantity - @stock_qty,
                   OriginalQuantity = OriginalQuantity - @stock_qty
             where DropSequence       = @DropSequence
               and AreaSequence       = @AreaSequence
               and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
               and IssueId            = isnull(@IssueId, -1)
               and StorageUnitBatchId = @StorageUnitBatchId
            
            select @jc_qty = @jc_qty - @stock_qty
            select @new_qty = @stock_qty
          end
          else
          begin
            update PalletiseStock
               set Quantity         = Quantity - @jc_qty,
                   OriginalQuantity = OriginalQuantity - @jc_qty
             where DropSequence       = @DropSequence
               and AreaSequence       = @AreaSequence
               and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
               and IssueId            = isnull(@IssueId, -1)
               and StorageUnitBatchId = @StorageUnitBatchId
            
            select @new_qty = @jc_qty
            select @jc_qty = 0
          end
          set rowcount 0
          
          exec @Error = p_Palletised_Insert
           @WarehouseId         = @WarehouseId,
           @OperatorId          = null,
           @InstructionTypeCode = 'PS',
           @StorageUnitBatchId  = @StorageUnitBatchId,
           @PickLocationId      = null,
           @StoreLocationId     = @StoreLocationId,
           @Quantity            = @new_qty,
           @IssueLineId         = @IssueLineId,
           @JobId               = @JobId,
           @OutboundShipmentId  = @OutboundShipmentId,
           @DropSequence        = @DropSequence
          
          if @Error <> 0  
            goto error
        end
        
        select @qty = @qty - @PackQuantity
        delete @pseudo_stock
      end
    end
    delete @pseudo_sku
  end
  
  delete @pseudo_DropSequence
  
  delete PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
     and Quantity           = 0
  
  if @@trancount > 0
    commit transaction
  return 0
  
  error:
    --raiserror 900000 'Error executing p_Outbound_Palletise_Pseudo'
    select @ErrorMsg
    if @@trancount = 1
      rollback transaction
    else
      commit transaction
    return @Error
end
