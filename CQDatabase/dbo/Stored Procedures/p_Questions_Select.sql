﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questions_Select
  ///   Filename       : p_Questions_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Questions table.
  /// </remarks>
  /// <param>
  ///   @QuestionId int = null 
  /// </param>
  /// <returns>
  ///   Questions.QuestionId,
  ///   Questions.QuestionaireId,
  ///   Questions.Category,
  ///   Questions.Code,
  ///   Questions.Sequence,
  ///   Questions.Active,
  ///   Questions.QuestionText,
  ///   Questions.QuestionType,
  ///   Questions.DateUpdated 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Questions_Select
(
 @QuestionId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Questions.QuestionId
        ,Questions.QuestionaireId
        ,Questions.Category
        ,Questions.Code
        ,Questions.Sequence
        ,Questions.Active
        ,Questions.QuestionText
        ,Questions.QuestionType
        ,Questions.DateUpdated
    from Questions
   where isnull(Questions.QuestionId,'0')  = isnull(@QuestionId, isnull(Questions.QuestionId,'0'))
  order by Category
  
end
