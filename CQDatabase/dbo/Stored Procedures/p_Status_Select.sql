﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Select
  ///   Filename       : p_Status_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:56
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Status table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null 
  /// </param>
  /// <returns>
  ///   Status.StatusId,
  ///   Status.Status,
  ///   Status.StatusCode,
  ///   Status.Type,
  ///   Status.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Select
(
 @StatusId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Status.StatusId
        ,Status.Status
        ,Status.StatusCode
        ,Status.Type
        ,Status.OrderBy
    from Status
   where isnull(Status.StatusId,'0')  = isnull(@StatusId, isnull(Status.StatusId,'0'))
  
end
