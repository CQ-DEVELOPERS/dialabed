﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Update_Job
  ///   Filename       : p_Housekeeping_Stock_Take_Update_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Update_Job
(
 @operatorId int,
 @jobId      int,
 @priorityId int
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_Job_Update
   @jobId      = @jobId,
   @priorityId = @priorityId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Housekeeping_Stock_Take_Update_Job'
    rollback transaction
    return @Error
end
