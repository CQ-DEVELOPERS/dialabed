﻿
 
/*
  /// <summary>
  ///   Procedure Name : p_Import_TakeOnStock_Insert
  ///   Filename       : p_Import_TakeOnStock_Insert.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/  
CREATE procedure p_Import_TakeOnStock_Insert
as
begin
  -- InterfaceImportTakeOnStock
  declare @InterfaceImportTakeOnStockId		int,
		  @Location							nvarchar(50),
		  @LocationId						int,
          @ProductCode						nvarchar(50),
          @StorageUnitId					int,
          @Batch							nvarchar(50),
          @BatchId							int,
          @Quantity							float,
          @UnitPrice						float,
          @ErrorMsg							varchar(100),
          @Error							int,        
          @StatusId							int,
          @GetDate							datetime,
          @StorageUnitBatchId				int,
          @RecordType						varchar(30),
          @InterfaceMessage					varchar(255),
          @InterfaceTableId					int,
          @InterfaceId						int
      
      
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @InterfaceTableId = InterfaceTableId
    from InterfaceTables (nolock)
   where HeaderTable = 'InterfaceImportTakeOnStock'
  
 
  -- Reprocess import which did not process properly
  update InterfaceImportTakeOnStock
     set ProcessedDate = null
   where RecordStatus     = 'N'
     and ProcessedDate   <= dateadd(mi, -10, @Getdate)
  
  update h
     set ProcessedDate = @Getdate,
         RecordType    = 'TOS'
    from InterfaceImportTakeOnStock h
     where RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare header_cursor                 cursor for
  select InterfaceImportTakeOnStockId,
         Location,   -- Location
         ProductCode, -- ProductCode
         Batch,  -- Batch
         Quantity,  -- Quantity
         UnitPrice  -- UnitPrice
    from InterfaceImportTakeOnStock h
   where ProcessedDate = @Getdate
  
  open header_cursor
  
  fetch header_cursor
    into @InterfaceImportTakeOnStockId,
         @Location,
         @ProductCode,
         @Batch,
         @Quantity,
         @UnitPrice
         
  while (@@fetch_status = 0)
  begin

    update InterfaceImportTakeOnStock
       set RecordStatus = 'C'
     where InterfaceImportTakeOnStockId = @InterfaceImportTakeOnStockId
    
    begin transaction xml_import   

    set @LocationId = null
    set @StorageUnitId = null
    set @BatchId = null
    
    set @LocationId = (select l.LocationId
					     from Location l (nolock)
					    where l.Location = @Location)
					    
    if @LocationId is null
	begin
        select @ErrorMsg = 'Location Does Not Exist'
        goto error_header
    end	    
    				 
    set @StorageUnitId = (select top 1 su.StorageUnitId 
						    from Product p (nolock)
						    join StorageUnit su (nolock) on p.ProductId = su.ProductId
						    left join StorageUnitLocation sul (nolock) on su.StorageUnitId = sul.StorageUnitId
						   where p.ProductCode = @ProductCode
						     and isnull(sul.LocationId,@LocationId) = @LocationId)  					   

    if @StorageUnitId is null
	begin
        select @ErrorMsg = 'Product Does Not Exist'
        goto error_header
    end	
    
    set @BatchId = (select top 1 b.BatchId 
					  from Batch b (nolock)
					 where b.Batch = @Batch)

    if @BatchId is null
	begin
        select @ErrorMsg = 'Batch Does Not Exist'
        goto error_header
    end	
    
    
    set @StorageUnitBatchId = (select top 1 sub.StorageUnitBatchId 
						         from StorageUnitBatch sub (nolock)
							    where sub.BatchId = @BatchId
							      and sub.StorageUnitId = @StorageUnitId)						      

     
    
    exec @Error = p_StorageUnitBatchLocation_Insert 
		 @StorageUnitBatchId		= @StorageUnitBatchId,
		 @LocationId				= @LocationId,
		 @ActualQuantity			= @Quantity,
		 @AllocatedQuantity			= 0,
		 @ReservedQuantity			= 0
         
    begin
    
      if @Error != 0
      begin
        select @ErrorMsg = 'Error - Cannot insert duplicate'
        goto error_header
      end
    end
    
  
    Goto Commit_Transaction
    
    error_detail:
    
    close detail_cursor
    deallocate detail_cursor
    
    error_header:
    if @@trancount > 0
    rollback transaction xml_import
    
     --added error message create into InterfaceMessage
    INSERT INTO InterfaceMessage
           ([InterfaceMessageCode]
           ,[InterfaceMessage]
           ,[InterfaceId]
           ,[InterfaceTable]
           ,[Status]
           ,[CreateDate]
           ,[ProcessedDate])
    VALUES
           ('Failed'
           ,LEFT(@ErrorMsg, 255)
           ,@InterfaceImportTakeOnStockId
           ,'InterfaceImportTakeOnStock'
           ,'E'
           ,@GetDate
           ,@GetDate)
           
           --select @ProductCode as 'ProductCode', 'Error 2', @@trancount as '@@trancount'
           
    update InterfaceImportTakeOnStock
       set RecordStatus = 'E',
           RecordType = isnull(@RecordType, RecordType)
     where InterfaceImportTakeOnStockId = @InterfaceImportTakeOnStockId
    
    Goto next_header
    
    commit_Transaction:
    if @@trancount > 0
    commit transaction xml_import
    
    INSERT INTO InterfaceMessage
           ([InterfaceMessageCode]
           ,[InterfaceMessage]
           ,[InterfaceId]
           ,[InterfaceTable]
           ,[Status]
           ,[CreateDate]
           ,[ProcessedDate])
    VALUES
           ('Processed'
           ,'Success'
           ,@InterfaceImportTakeOnStockId
           ,'InterfaceImportTakeOnStock'
           ,'N'
           ,@GetDate
           ,@GetDate)
           
           
    next_header:
      fetch header_cursor
    into @InterfaceImportTakeOnStockId,
         @Location,
         @ProductCode,
         @Batch,
         @Quantity,
         @UnitPrice
         
     
  end
  
  close header_cursor
  deallocate header_cursor
  
  update InterfaceImportTakeOnStock
     set ProcessedDate = null
   where RecordStatus = 'N'
     and ProcessedDate = @GetDate
  
  return
end
 
 
