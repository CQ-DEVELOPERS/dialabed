﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Company_Update
  ///   Filename       : p_Company_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:51
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Company table.
  /// </remarks>
  /// <param>
  ///   @CompanyId int = null,
  ///   @Company nvarchar(100) = null,
  ///   @CompanyCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Company_Update
(
 @CompanyId int = null,
 @Company nvarchar(100) = null,
 @CompanyCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @CompanyId = '-1'
    set @CompanyId = null;
  
  if @Company = '-1'
    set @Company = null;
  
  if @CompanyCode = '-1'
    set @CompanyCode = null;
  
	 declare @Error int
 
  update Company
     set Company = isnull(@Company, Company),
         CompanyCode = isnull(@CompanyCode, CompanyCode) 
   where CompanyId = @CompanyId
  
  select @Error = @@Error
  
  
  return @Error
  
end
