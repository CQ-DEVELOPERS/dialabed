﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocation_Update
  ///   Filename       : p_StorageUnitBatchLocation_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:03
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StorageUnitBatchLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null,
  ///   @LocationId int = null,
  ///   @ActualQuantity float = null,
  ///   @AllocatedQuantity float = null,
  ///   @ReservedQuantity float = null,
  ///   @NettWeight float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Update
(
 @StorageUnitBatchId int = null,
 @LocationId int = null,
 @ActualQuantity float = null,
 @AllocatedQuantity float = null,
 @ReservedQuantity float = null,
 @NettWeight float = null 
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
	 declare @Error int
 
  update StorageUnitBatchLocation
     set ActualQuantity = isnull(@ActualQuantity, ActualQuantity),
         AllocatedQuantity = isnull(@AllocatedQuantity, AllocatedQuantity),
         ReservedQuantity = isnull(@ReservedQuantity, ReservedQuantity),
         NettWeight = isnull(@NettWeight, NettWeight) 
   where StorageUnitBatchId = @StorageUnitBatchId
     and LocationId = @LocationId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
