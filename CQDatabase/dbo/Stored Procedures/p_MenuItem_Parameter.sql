﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Parameter
  ///   Filename       : p_MenuItem_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 16:06:10
  /// </summary>
  /// <remarks>
  ///   Selects rows from the MenuItem table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   MenuItem.MenuItemId,
  ///   MenuItem.MenuItem 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as MenuItemId
        ,'{All}' as MenuItem
  union
  select
         MenuItem.MenuItemId
        ,MenuItem.MenuItem
    from MenuItem
  order by MenuItem
  
end
