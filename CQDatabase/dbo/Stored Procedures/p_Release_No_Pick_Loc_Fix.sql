﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Release_No_Pick_Loc_Fix
  ///   Filename       : p_Release_No_Pick_Loc_Fix.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Mar 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Release_No_Pick_Loc_Fix
 
as
begin
	 set nocount on;
  
  -- select * from MobileLog (nolock) where ErrorMsg = 'RL without location' and StartDate > '2012-03-08 15:09:20.983'
  
  select 'exec p_Mobile_Location_Error @instructionId = ' + convert(varchar(10), Instructionid) + ', @pickError = 1, @storeError = 0, @createStockTake = 0' as 'String', InstructionId
      into #string
      from viewili
     where JobCode = 'RL'
      and InstructionCode = 'W'
      and PickLocation is null
      and CreateDate > '2012-08-01'
      --and InstructionTypeCode != 'P'
    order by OutboundShipmentId

  declare @String        varchar(max),
          @InstructionId int

  while exists(select 1 from #string)
  begin
    select top 1 @String = String,
                 @InstructionId = InstructionId
      from #string
    
    delete #string
     where InstructionId = @InstructionId
    select @String
    exec (@String)
    
    update MobileLog set ErrorMsg = 'RL without location' where InstructionId = @InstructionId and ProcName = 'p_Mobile_Location_Error'
  end

  drop table #string
end

