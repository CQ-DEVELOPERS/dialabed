﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipmentReceipt_Delete
  ///   Filename       : p_InboundShipmentReceipt_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:22
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InboundShipmentReceipt table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null,
  ///   @ReceiptId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipmentReceipt_Delete
(
 @InboundShipmentId int = null,
 @ReceiptId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InboundShipmentReceipt
     where InboundShipmentId = @InboundShipmentId
       and ReceiptId = @ReceiptId
  
  select @Error = @@Error
  
  
  return @Error
  
end
