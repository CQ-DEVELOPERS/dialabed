﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingFile_Search
  ///   Filename       : p_InterfaceMappingFile_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 09:03:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceMappingFile table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingFileId int = null output,
  ///   @PrincipalId int = null,
  ///   @InterfaceFileTypeId int = null,
  ///   @InterfaceDocumentTypeId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceMappingFile.InterfaceMappingFileId,
  ///   InterfaceMappingFile.PrincipalId,
  ///   Principal.Principal,
  ///   InterfaceMappingFile.InterfaceFileTypeId,
  ///   InterfaceFileType.InterfaceFileType,
  ///   InterfaceMappingFile.InterfaceDocumentTypeId,
  ///   InterfaceDocumentType.InterfaceDocumentType,
  ///   InterfaceMappingFile.Delimiter,
  ///   InterfaceMappingFile.StyleSheet,
  ///   InterfaceMappingFile.PrincipalFTPSite,
  ///   InterfaceMappingFile.FTPUserName,
  ///   InterfaceMappingFile.FTPPassword,
  ///   InterfaceMappingFile.HeaderRow,
  ///   InterfaceMappingFile.SubDirectory,
  ///   InterfaceMappingFile.FilePrefix 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingFile_Search
(
 @InterfaceMappingFileId int = null output,
 @PrincipalId int = null,
 @InterfaceFileTypeId int = null,
 @InterfaceDocumentTypeId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceMappingFileId = '-1'
    set @InterfaceMappingFileId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @InterfaceFileTypeId = '-1'
    set @InterfaceFileTypeId = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
 
  select
         InterfaceMappingFile.InterfaceMappingFileId
        ,InterfaceMappingFile.PrincipalId
         ,PrincipalPrincipalId.Principal as 'Principal'
        ,InterfaceMappingFile.InterfaceFileTypeId
         ,InterfaceFileTypeInterfaceFileTypeId.InterfaceFileType as 'InterfaceFileType'
        ,InterfaceMappingFile.InterfaceDocumentTypeId
         ,InterfaceDocumentTypeInterfaceDocumentTypeId.InterfaceDocumentType as 'InterfaceDocumentType'
        ,InterfaceMappingFile.Delimiter
        ,InterfaceMappingFile.StyleSheet
        ,InterfaceMappingFile.PrincipalFTPSite
        ,InterfaceMappingFile.FTPUserName
        ,InterfaceMappingFile.FTPPassword
        ,InterfaceMappingFile.HeaderRow
        ,InterfaceMappingFile.SubDirectory
        ,InterfaceMappingFile.FilePrefix
    from InterfaceMappingFile
    left
    join Principal PrincipalPrincipalId on PrincipalPrincipalId.PrincipalId = InterfaceMappingFile.PrincipalId
    left
    join InterfaceFileType InterfaceFileTypeInterfaceFileTypeId on InterfaceFileTypeInterfaceFileTypeId.InterfaceFileTypeId = InterfaceMappingFile.InterfaceFileTypeId
    left
    join InterfaceDocumentType InterfaceDocumentTypeInterfaceDocumentTypeId on InterfaceDocumentTypeInterfaceDocumentTypeId.InterfaceDocumentTypeId = InterfaceMappingFile.InterfaceDocumentTypeId
   where isnull(InterfaceMappingFile.InterfaceMappingFileId,'0')  = isnull(@InterfaceMappingFileId, isnull(InterfaceMappingFile.InterfaceMappingFileId,'0'))
     and isnull(InterfaceMappingFile.PrincipalId,'0')  = isnull(@PrincipalId, isnull(InterfaceMappingFile.PrincipalId,'0'))
     and isnull(InterfaceMappingFile.InterfaceFileTypeId,'0')  = isnull(@InterfaceFileTypeId, isnull(InterfaceMappingFile.InterfaceFileTypeId,'0'))
     and isnull(InterfaceMappingFile.InterfaceDocumentTypeId,'0')  = isnull(@InterfaceDocumentTypeId, isnull(InterfaceMappingFile.InterfaceDocumentTypeId,'0'))
  order by Delimiter
  
end
