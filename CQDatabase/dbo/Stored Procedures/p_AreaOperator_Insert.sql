﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperator_Insert
  ///   Filename       : p_AreaOperator_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:45
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaOperator table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null output,
  ///   @OperatorId int = null output 
  /// </param>
  /// <returns>
  ///   AreaOperator.AreaId,
  ///   AreaOperator.OperatorId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperator_Insert
(
 @AreaId int = null output,
 @OperatorId int = null output 
)
 
as
begin
	 set nocount on;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
	 declare @Error int
 
  insert AreaOperator
        (AreaId,
         OperatorId)
  select @AreaId,
         @OperatorId 
  
  select @Error = @@Error, @OperatorId = scope_identity()
  
  
  return @Error
  
end
