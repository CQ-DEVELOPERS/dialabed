﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceCustomer_Search
  ///   Filename       : p_InterfaceCustomer_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:53
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceCustomer table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceCustomer.HostId,
  ///   InterfaceCustomer.CustomerCode,
  ///   InterfaceCustomer.CustomerName,
  ///   InterfaceCustomer.Class,
  ///   InterfaceCustomer.DeliveryPoint,
  ///   InterfaceCustomer.Address1,
  ///   InterfaceCustomer.Address2,
  ///   InterfaceCustomer.Address3,
  ///   InterfaceCustomer.Address4,
  ///   InterfaceCustomer.Modified,
  ///   InterfaceCustomer.ContactPerson,
  ///   InterfaceCustomer.Phone,
  ///   InterfaceCustomer.Fax,
  ///   InterfaceCustomer.Email,
  ///   InterfaceCustomer.DeliveryGroup,
  ///   InterfaceCustomer.DepotCode,
  ///   InterfaceCustomer.VisitFrequency,
  ///   InterfaceCustomer.ProcessedDate,
  ///   InterfaceCustomer.RecordStatus,
  ///   InterfaceCustomer.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceCustomer_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceCustomer.HostId
        ,InterfaceCustomer.CustomerCode
        ,InterfaceCustomer.CustomerName
        ,InterfaceCustomer.Class
        ,InterfaceCustomer.DeliveryPoint
        ,InterfaceCustomer.Address1
        ,InterfaceCustomer.Address2
        ,InterfaceCustomer.Address3
        ,InterfaceCustomer.Address4
        ,InterfaceCustomer.Modified
        ,InterfaceCustomer.ContactPerson
        ,InterfaceCustomer.Phone
        ,InterfaceCustomer.Fax
        ,InterfaceCustomer.Email
        ,InterfaceCustomer.DeliveryGroup
        ,InterfaceCustomer.DepotCode
        ,InterfaceCustomer.VisitFrequency
        ,InterfaceCustomer.ProcessedDate
        ,InterfaceCustomer.RecordStatus
        ,InterfaceCustomer.InsertDate
    from InterfaceCustomer
  
end
