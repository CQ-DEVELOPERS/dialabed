﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_Location_By_AisleLevel
  ///   Filename       : p_Location_Search_Location_By_AisleLevel.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_Location_By_AisleLevel
(
	@WarehouseId int,
	@Location   nvarchar(15),
	@StockTakeGroupId int,
	@FromAisle     nvarchar(10),
	@ToAisle       nvarchar(10),
	@FromLevel     nvarchar(10),
	@ToLevel       nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  select l.LocationId,
         l.Location
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId    = a.AreaId
   where a.WarehouseId = @WarehouseId
     and l.Location like '%' + isnull(@Location,'') + '%'
     and l.Ailse   between @FromAisle and @ToAisle
	 and l.[Level] between @FromLevel and @ToLevel
     and not exists
     (select 1 from StockTakeGroupLocation stgl
     --join Location lo (nolock) on @Location = lo.Location 
     where stgl.StockTakeGroupId = @StockTakeGroupId 
     and stgl.LocationId = l.LocationId)
     ORDER BY l.Location
end
