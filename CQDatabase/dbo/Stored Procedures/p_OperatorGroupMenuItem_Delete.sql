﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItem_Delete
  ///   Filename       : p_OperatorGroupMenuItem_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:25
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OperatorGroupMenuItem table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @MenuItemId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItem_Delete
(
 @OperatorGroupId int = null,
 @MenuItemId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete OperatorGroupMenuItem
     where OperatorGroupId = @OperatorGroupId
       and MenuItemId = @MenuItemId
  
  select @Error = @@Error
  
  
  return @Error
  
end
