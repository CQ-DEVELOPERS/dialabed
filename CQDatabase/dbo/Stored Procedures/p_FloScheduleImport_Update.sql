﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FloScheduleImport_Update
  ///   Filename       : p_FloScheduleImport_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:37
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the FloScheduleImport table.
  /// </remarks>
  /// <param>
  ///   @Route varchar(50) = null,
  ///   @Driver varchar(50) = null,
  ///   @Vehicle varchar(50) = null,
  ///   @VehicleClass varchar(20) = null,
  ///   @StopNumber int = null,
  ///   @UniqueNumber varchar(20) = null,
  ///   @DeliveryPoint varchar(50) = null,
  ///   @CustomerNumber varchar(40) = null,
  ///   @CompanyName varchar(255) = null,
  ///   @PickupTime varchar(20) = null,
  ///   @Arrival varchar(20) = null,
  ///   @TimeforJob int = null,
  ///   @DeliveryDate datetime = null,
  ///   @LineNumber int = null,
  ///   @CheckSummary int = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null,
  ///   @FloScheduleImportId int = null,
  ///   @InsertDate datetime = null,
  ///   @ErrorMsg nvarchar(max) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FloScheduleImport_Update
(
 @Route varchar(50) = null,
 @Driver varchar(50) = null,
 @Vehicle varchar(50) = null,
 @VehicleClass varchar(20) = null,
 @StopNumber int = null,
 @UniqueNumber varchar(20) = null,
 @DeliveryPoint varchar(50) = null,
 @CustomerNumber varchar(40) = null,
 @CompanyName varchar(255) = null,
 @PickupTime varchar(20) = null,
 @Arrival varchar(20) = null,
 @TimeforJob int = null,
 @DeliveryDate datetime = null,
 @LineNumber int = null,
 @CheckSummary int = null,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null,
 @FloScheduleImportId int = null,
 @InsertDate datetime = null,
 @ErrorMsg nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
  if @FloScheduleImportId = '-1'
    set @FloScheduleImportId = null;
  
	 declare @Error int
 
  update FloScheduleImport
     set Route = isnull(@Route, Route),
         Driver = isnull(@Driver, Driver),
         Vehicle = isnull(@Vehicle, Vehicle),
         VehicleClass = isnull(@VehicleClass, VehicleClass),
         StopNumber = isnull(@StopNumber, StopNumber),
         UniqueNumber = isnull(@UniqueNumber, UniqueNumber),
         DeliveryPoint = isnull(@DeliveryPoint, DeliveryPoint),
         CustomerNumber = isnull(@CustomerNumber, CustomerNumber),
         CompanyName = isnull(@CompanyName, CompanyName),
         PickupTime = isnull(@PickupTime, PickupTime),
         Arrival = isnull(@Arrival, Arrival),
         TimeforJob = isnull(@TimeforJob, TimeforJob),
         DeliveryDate = isnull(@DeliveryDate, DeliveryDate),
         LineNumber = isnull(@LineNumber, LineNumber),
         CheckSummary = isnull(@CheckSummary, CheckSummary),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         InsertDate = isnull(@InsertDate, InsertDate),
         ErrorMsg = isnull(@ErrorMsg, ErrorMsg) 
   where FloScheduleImportId = @FloScheduleImportId
  
  select @Error = @@Error
  
  
  return @Error
  
end
