﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_GroupPerformance_Update
  ///   Filename       : p_GroupPerformance_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:39
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the GroupPerformance table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @PerformanceType nvarchar(2) = null,
  ///   @Units int = null,
  ///   @Weight float = null,
  ///   @Instructions numeric(13,3) = null,
  ///   @Orders numeric(13,3) = null,
  ///   @OrderLines numeric(13,3) = null,
  ///   @Jobs numeric(13,3) = null,
  ///   @QA numeric(13,3) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_GroupPerformance_Update
(
 @OperatorGroupId int = null,
 @PerformanceType nvarchar(2) = null,
 @Units int = null,
 @Weight float = null,
 @Instructions numeric(13,3) = null,
 @Orders numeric(13,3) = null,
 @OrderLines numeric(13,3) = null,
 @Jobs numeric(13,3) = null,
 @QA numeric(13,3) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update GroupPerformance
     set OperatorGroupId = isnull(@OperatorGroupId, OperatorGroupId),
         PerformanceType = isnull(@PerformanceType, PerformanceType),
         Units = isnull(@Units, Units),
         Weight = isnull(@Weight, Weight),
         Instructions = isnull(@Instructions, Instructions),
         Orders = isnull(@Orders, Orders),
         OrderLines = isnull(@OrderLines, OrderLines),
         Jobs = isnull(@Jobs, Jobs),
         QA = isnull(@QA, QA) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
