﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipmentReceiptHistory_Insert
  ///   Filename       : p_InboundShipmentReceiptHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:21
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundShipmentReceiptHistory table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null,
  ///   @ReceiptId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InboundShipmentReceiptHistory.InboundShipmentId,
  ///   InboundShipmentReceiptHistory.ReceiptId,
  ///   InboundShipmentReceiptHistory.CommandType,
  ///   InboundShipmentReceiptHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipmentReceiptHistory_Insert
(
 @InboundShipmentId int = null,
 @ReceiptId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InboundShipmentReceiptHistory
        (InboundShipmentId,
         ReceiptId,
         CommandType,
         InsertDate)
  select @InboundShipmentId,
         @ReceiptId,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
