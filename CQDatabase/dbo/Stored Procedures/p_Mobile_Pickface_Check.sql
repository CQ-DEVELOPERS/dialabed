﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Pickface_Check
  ///   Filename       : p_Mobile_Pickface_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Mobile_Pickface_Check]
(
 @warehouseId int,
 @location    varchar(15),
 @storageUnitBatchId int
)
 
as
begin
	 set nocount on;
  
  select @location = replace(@location,'L:','')
  
  if @storageUnitbatchId != -1
  begin
    select l.LocationId,
           vs.StorageUnitId,
           vs.StorageUnitBatchId,
           l.Location,
           vs.ProductCode,
           vs.Product,
           vs.SKUCode,
           vs.Batch,
           0 as 'ActualQuantity',
           0 as 'AllocatedQuantity',
           0 as 'ReservedQuantity',
           case when sul.StorageUnitId is null
                then 'Not Allocated'
                else 'Allocated'
                end as 'Status'
      from Location      l
      join AreaLocation al on l.LocationId = al.LocationId
      join Area          a on al.AreaId = a.AreaId
      left
      join viewSUL     sul on l.LocationId = sul.LocationId
      left
      join viewStock vs on vs.StorageUnitBatchId = @StorageUnitBatchId
     where a.WarehouseId = @warehouseId
       and (l.Location    = @location or l.SecurityCode = @location)
  end
  else
  begin
    select l.LocationId,
           sul.StorageUnitId,
           soh.StorageUnitBatchId,
           l.Location,
           sul.ProductCode,
           sul.Product,
           sul.SKUCode,
           isnull(soh.Batch,'Default') as 'Batch',
           isnull(soh.ActualQuantity,0) as 'ActualQuantity',
           isnull(soh.AllocatedQuantity,0) as 'AllocatedQuantity',
           isnull(soh.ReservedQuantity,0) as 'ReservedQuantity',
           case when sul.StorageUnitId is null
                then 'Not Allocated'
                else 'Allocated'
                end as 'Status'
      from Location      l
      join AreaLocation al on l.LocationId = al.LocationId
      join Area          a on al.AreaId = a.AreaId
      left
      join viewSUL     sul on l.LocationId = sul.LocationId
      left
      join viewSOH soh on soh.LocationId = sul.LocationId
                      and soh.StorageUnitId = sul.StorageUnitId
     where a.WarehouseId = @warehouseId
       and (l.Location    = @location or l.SecurityCode = @location)
  end
end
 
