﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CQ_Set_Order_Date
  ///   Filename       : p_CQ_Set_Order_Date.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Apr 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CQ_Set_Order_Date
(
 @OrderNum varchar(20)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @PerDate           datetime
  
  select @GetDate = Getdate()
  
  begin transaction
  
  select @PerDate = min(PerDate)
    from Period (nolock)
   where PerDate > @Getdate
  
  select *
    from Period
   where PerDate = @PerDate
  
  select @PerDate as '@PerDate'
  
--  update invNum
--     set InvDate = @GetDate
--   where OrderNum = @OrderNum
--  
--  select @Error = @@Error
--  
--  if @Error <> 0
--    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_CQ_Set_Order_Archived'
    rollback transaction
    return @Error
end
