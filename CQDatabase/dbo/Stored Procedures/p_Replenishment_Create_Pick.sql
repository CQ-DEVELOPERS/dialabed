﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Replenishment_Create_Pick
  ///   Filename       : p_Replenishment_Create_Pick.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jul 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Replenishment_Create_Pick
(
 @InstructionId int,
 @WaveId        int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error                  int,
          @Errormsg               varchar(500) = 'p_Replenishment_Create_Pick',
          @GetDate                datetime,
          @OutboundDocumentTypeId int,
          @OutboundDocumentId     int,
          @OutboundLineId         int,
          @WarehouseId            int,
          @DeliveryDate           datetime,
          @StorageUnitId          int,
          @Quantity               float,
          @BatchId                int,
          @ExternalCompanyId      int,
          @IssueId                int,
          @IssueLineId            int,
          @StatusId               int,
          @OrderNumber            nvarchar(30),
          @Transaction            bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @DeliveryDate = convert(varchar(10), @getdate, 120)
  
  if @WaveId is not null
  begin
    select @OutboundDocumentTypeId = OutboundDocumentTypeId
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = 'WAV'
    
    select @OutboundDocumentId = od.OutboundDocumentId,
           @IssueId            = IssueId
      from OutboundDocument      od (nolock)
      join Issue                  i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
     where i.WaveId = @WaveId
       and od.OutboundDocumentTypeId = @OutboundDocumentTypeId
    
    select @OrderNumber = Wave
      from Wave (nolock)
     where WaveId = @WaveId
  end
  else
  begin
    select @OutboundDocumentTypeId = OutboundDocumentTypeId
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = 'REP'
  end
  
  select @WarehouseId   = i.WarehouseId,
         @StorageUnitId = sub.StorageUnitId,
         @BatchId       = sub.BatchId,
         @Quantity      = i.Quantity
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.InstructionId = @InstructionId
  
  if @@trancount = 0
  begin
    begin transaction
    set @Transaction = 1
  end
  
  select top 1 @ExternalCompanyId = ExternalCompanyId
    from ExternalCompany (nolock)
  order by ExternalCompanyId
  
  if @OutboundDocumentId is null
  begin
    exec @Error = p_OutboundDocument_Create
     @OutboundDocumentId     = @OutboundDocumentId output,
     @OutboundDocumentTypeId = @OutboundDocumentTypeId,
     @OrderNumber            = @OrderNumber,
     @ExternalCompanyId      = @ExternalCompanyId,
     @WarehouseId            = @WarehouseId,
     @DeliveryDate           = @DeliveryDate
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_OutboundLine_Create
   @OperatorId         = null,
   @OutboundLineId     = @OutboundLineId output,
   @OutboundDocumentId = @OutboundDocumentId,
   @StorageUnitId      = @StorageUnitId,
   @LineNumber         = 1,
   @Quantity           = @Quantity,
   @BatchId            = @BatchId ,
   @IssueId            = @IssueId output,
   @IssueLineId        = @IssueLineId output,
   @WaveId             = @WaveId
  
  if @Error <> 0
    goto error
  
  if @WaveId is not null
  begin
    select @StatusId = dbo.ufn_StatusId('IS','RL')
    
    exec @Error = p_Issue_Update
     @IssueId  = @IssueId,
     @WaveId   = @WaveId,
     @StatusId = @StatusId
    
    if @Error <> 0
      goto error
    
  end
  
  insert IssueLineInstruction
        (OutboundDocumentTypeId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         InstructionId,
         Quantity)
  select @OutboundDocumentTypeId,
         @OutboundDocumentId,
         @IssueId,
         @IssueLineId,
         @InstructionId,
         @Quantity
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @IssueLineId   = @IssueLineId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  result:
    if @Transaction = 1
      commit transaction
    return 0
  
  error:
    if @Transaction = 1
    begin
      raiserror 900000 @Errormsg
      rollback transaction
    end
    return @Error
end
