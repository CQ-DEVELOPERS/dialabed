﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Manual_Palletisation_Create_Select
  ///   Filename       : p_Housekeeping_Manual_Palletisation_Create_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Manual_Palletisation_Create_Select
(
 @StorageUnitBatchId int,
 @PickLocationId     int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   InstructionId      int,
   JobId              int,
   StorageUnitBatchId int,
   ProductCode	       nvarchar(30) null,
   Product	           nvarchar(50) null,
   SKUCode            nvarchar(50) null,
   Quantity           float null,
   StatusId           int null,
   Status             nvarchar(50) null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   StoreLocationId    int null,
   StoreLocation      nvarchar(15) null,
   PalletId           int null,
   CreateDate         datetime null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   PriorityId         int null,
   Priority           nvarchar(50) null
  )
  
  insert @TableResult
        (InstructionId,
         JobId,
         StorageUnitBatchId,
         Quantity,
         StatusId,
         PickLocationId,
         StoreLocationId,
         PalletId,
         CreateDate,
         PriorityId,
         Status)
  select i.InstructionId,
         j.JobId,
         i.StorageUnitBatchId,
         i.Quantity,
         i.StatusId,
         i.PickLocationId,
         i.StoreLocationId,
         i.PalletId,
         i.CreateDate,
         j.PriorityId,
         s.Status
    from Job                 j   (nolock)
    join Instruction         i   (nolock) on j.JobId              = i.JobId
    join InstructionType     it  (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Status               s  (nolock) on i.StatusId           = s.StatusId
   where i.StorageUnitBatchId = @StorageUnitBatchId
     and i.PickLocationId     = @PickLocationId
     and it.InstructionTypeCode in ('M','R','O','RS','HS')
     and s.StatusCode in ('W','S')
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch       = b.Batch,
         ECLNumber   = b.ECLNumber
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p  (nolock) on tr.PriorityId = p.PriorityId
  
  select InstructionId,
         JobId,
         ProductCode,
         Product,
         SKUCode,
         Quantity,
         Status,
         PickLocation,
         StoreLocation,
         PalletId,
         CreateDate,
         Batch,
         ECLNumber,
         PriorityId,
         Priority
    from @TableResult
end
