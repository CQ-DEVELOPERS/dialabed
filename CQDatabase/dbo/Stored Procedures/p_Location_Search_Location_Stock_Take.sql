﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_Location_Stock_Take
  ///   Filename       : p_Location_Search_Location_Stock_Take.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_Location_Stock_Take
(
 @WarehouseId int,
 @Location   nvarchar(15)
)
 
as
begin
	 set nocount on;
  
  select l.LocationId,
         a.Area,
         l.Location,
         l.StockTakeInd,
         convert(bit, case when l.StockTakeInd = 0
              then 1
              else 0
              end) as 'Enabled'
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId    = a.AreaId
   where a.WarehouseId = @WarehouseId
     and l.Location like '%' + isnull(@Location,'') + '%'
  order by l.Location
end
