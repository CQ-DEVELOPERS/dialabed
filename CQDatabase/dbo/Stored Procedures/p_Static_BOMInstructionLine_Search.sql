﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_BOMInstructionLine_Search
  ///   Filename       : p_Static_BOMInstructionLine_Search.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>	 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_BOMInstructionLine_Search
(
    @BOMInstructionId int
)
 
as
begin
	 set nocount on;
	 
	 DECLARE @WarehouseId       int
	         ,@StorageUnitId    int
	 
	 DECLARE @TableResult TABLE
	     (OrderNumber	        nvarchar(30)
	     ,LineNumber	        Int
	     ,Quantity	            float
	     ,ProductCode	        nvarchar(30)
	     ,Product	            nvarchar(50)
	     ,AvailableQuantity     float
	     ,AvailabilityIndicator nvarchar(20)
         ,AvailablePercentage   Numeric(13,2)
	     ,BOMInstructionId	    Int
	     ,BOMLineId             Int
	     ,StorageUnitId         int)

	 INSERT INTO @TableResult
	        (OrderNumber
	        ,LineNumber
	        ,Quantity
	        ,ProductCode
	        ,Product
	        --,AvailableQuantity
	        --,AvailabilityIndicator
	        --,AvailablePercentage
	        ,BOMInstructionId
	        ,BOMLineId
	        ,StorageUnitId)
	SELECT	 bi.OrderNumber
            ,ROW_NUMBER() OVER (Order By bil.BOMLineId, bil.LineNumber) AS LineNumber
            ,bil.Quantity*bp.Quantity As Quantity
            ,P.ProductCode
            ,P.Product
            ,bi.BOMInstructionId
            ,bil.BOMLineId
            ,bp.StorageUnitId
	FROM	BOMInstruction bi
			Inner join BOMInstructionLine bil on bi.BOMInstructionId = bil.BOMInstructionId
			Inner join BOMProduct bp on bil.BOMLineId = bp.BOMLineId
			                        and bil.LineNumber = bp.LineNumber
            Inner join StorageUnit su on bp.StorageUnitId = su.StorageUnitId
			Inner join Product AS P ON SU.ProductId = P.ProductId
    WHERE 
            bi.BOMInstructionId = @BOMInstructionId

    SELECT @WarehouseId = 1
          
            
    UPDATE @TableResult
       SET AvailableQuantity = ISNULL(
                               (select sum(subl.ActualQuantity - subl.ReservedQuantity)
                                from StorageUnitBatchLocation subl (nolock)
                                 join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                                 join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
                                 join Area                        a (nolock) on al.AreaId               = a.AreaId
                                 join BOMProduct                 bp (nolock) on bp.StorageUnitId = sub.StorageUnitId                                                                            
                                where a.StockOnHand    = 1                                 
                                 and bp.StorageUnitId = tr.StorageUnitId
                                 and a.WarehouseId     = @WarehouseId),0)
      FROM @TableResult tr
                                 
    update @TableResult
       set AvailablePercentage = (convert(numeric(13,3), AvailableQuantity) / convert(numeric(13,3), Quantity)) * 100
     where AvailableQuantity != 0
       and Quantity > 0
       
    update @TableResult
       set AvailabilityIndicator = CASE WHEN AvailableQuantity >= Quantity THEN 'Green'
                                        WHEN AvailableQuantity > 0 THEN 'Yellow'
                                        WHEN AvailableQuantity <= 0 THEN 'Red'
                                   END
                      
    SELECT 
          OrderNumber
	     ,LineNumber
	     ,Quantity
	     ,ProductCode
	     ,Product	 
	     ,AvailableQuantity
	     ,AvailabilityIndicator
         ,AvailablePercentage
	     ,BOMInstructionId
	     ,BOMLineId 
    FROM @TableResult
 
end
