﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Intergration_Get_InterfaceFilePrefix
  ///   Filename       : p_Intergration_Get_InterfaceFilePrefix.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Mar 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Intergration_Get_InterfaceFilePrefix
(
 @interfaceMappingFileId int,
 @interfaceFilePrefix  nvarchar(50) output
)
 
as
begin
  set nocount on;
  
  select @interfaceFilePrefix = isnull(imf.FilePrefix,idt.FilePrefix)
    from InterfaceMappingFile   imf (nolock)
    join InterfaceDocumentType  idt (nolock) on imf.InterfaceDocumentTypeId = idt.InterfaceDocumentTypeId
   where imf.InterfaceMappingFileId = @interfaceMappingFileId
end
