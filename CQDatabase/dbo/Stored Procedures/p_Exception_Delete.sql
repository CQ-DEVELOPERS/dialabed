﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Delete
  ///   Filename       : p_Exception_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:05
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Exception table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Delete
(
 @ExceptionId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Exception
     where ExceptionId = @ExceptionId
  
  select @Error = @@Error
  
  
  return @Error
  
end
