﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactList_Search
  ///   Filename       : p_ContactList_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:18:14
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ContactList table.
  /// </remarks>
  /// <param>
  ///   @ContactListId int = null output,
  ///   @ExternalCompanyId int = null,
  ///   @OperatorId int = null,
  ///   @ReportTypeId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ContactList.ContactListId,
  ///   ContactList.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompany,
  ///   ContactList.OperatorId,
  ///   Operator.Operator,
  ///   ContactList.ContactPerson,
  ///   ContactList.Telephone,
  ///   ContactList.Fax,
  ///   ContactList.EMail,
  ///   ContactList.Alias,
  ///   ContactList.Type,
  ///   ContactList.ReportTypeId 
  ///   ReportType.ReportType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContactList_Search
(
 @ContactListId int = null output,
 @ExternalCompanyId int = null,
 @OperatorId int = null,
 @ReportTypeId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ReportTypeId = '-1'
    set @ReportTypeId = null;
  
 
  select
         ContactList.ContactListId
        ,ContactList.ExternalCompanyId
         ,ExternalCompanyExternalCompanyId.ExternalCompany as 'ExternalCompany'
        ,ContactList.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,ContactList.ContactPerson
        ,ContactList.Telephone
        ,ContactList.Fax
        ,ContactList.EMail
        ,ContactList.Alias
        ,ContactList.Type
        ,ContactList.ReportTypeId
         ,ReportTypeReportTypeId.ReportType as 'ReportType'
    from ContactList
    left
    join ExternalCompany ExternalCompanyExternalCompanyId on ExternalCompanyExternalCompanyId.ExternalCompanyId = ContactList.ExternalCompanyId
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = ContactList.OperatorId
    left
    join ReportType ReportTypeReportTypeId on ReportTypeReportTypeId.ReportTypeId = ContactList.ReportTypeId
   where isnull(ContactList.ContactListId,'0')  = isnull(@ContactListId, isnull(ContactList.ContactListId,'0'))
     and isnull(ContactList.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(ContactList.ExternalCompanyId,'0'))
     and isnull(ContactList.OperatorId,'0')  = isnull(@OperatorId, isnull(ContactList.OperatorId,'0'))
     and isnull(ContactList.ReportTypeId,'0')  = isnull(@ReportTypeId, isnull(ContactList.ReportTypeId,'0'))
  order by ContactPerson
  
end
