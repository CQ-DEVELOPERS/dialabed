﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_Delete
  ///   Filename       : p_InstructionType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:54
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_Delete
(
 @InstructionTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InstructionType
     where InstructionTypeId = @InstructionTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
