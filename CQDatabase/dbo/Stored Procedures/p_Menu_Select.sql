﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Menu_Select
  ///   Filename       : p_Menu_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:58
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// <param>
  ///   @MenuId int = null 
  /// </param>
  /// <returns>
  ///   Menu.MenuId,
  ///   Menu.Menu 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Menu_Select
(
 @MenuId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Menu.MenuId
        ,Menu.Menu
    from Menu
   where isnull(Menu.MenuId,'0')  = isnull(@MenuId, isnull(Menu.MenuId,'0'))
  
end
