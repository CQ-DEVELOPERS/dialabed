﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletBulk_Insert
  ///   Filename       : p_PalletBulk_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:41
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PalletBulk table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null 
  /// </param>
  /// <returns>
  ///   PalletBulk.PalletId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletBulk_Insert
(
 @PalletId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PalletBulk
        (PalletId)
  select @PalletId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
