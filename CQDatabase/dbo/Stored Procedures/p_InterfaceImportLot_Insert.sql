﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportLot_Insert
  ///   Filename       : p_InterfaceImportLot_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:40
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportLot table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportLotId int = null,
  ///   @RecordStatus nvarchar(4) = null,
  ///   @InsertDate datetime = null,
  ///   @ProcessedDate datetime = null,
  ///   @LotId nvarchar(100) = null,
  ///   @Code nvarchar(510) = null,
  ///   @WhseId nvarchar(100) = null,
  ///   @Whse nvarchar(510) = null,
  ///   @QtyOnHand nvarchar(100) = null,
  ///   @QtyFree nvarchar(100) = null,
  ///   @LotStatusId nvarchar(100) = null,
  ///   @LotStatus nvarchar(200) = null,
  ///   @ExpiryDate datetime = null,
  ///   @ParentProductCode nvarchar(40) = null,
  ///   @ExpectedYield float = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportLot.InterfaceImportLotId,
  ///   InterfaceImportLot.RecordStatus,
  ///   InterfaceImportLot.InsertDate,
  ///   InterfaceImportLot.ProcessedDate,
  ///   InterfaceImportLot.LotId,
  ///   InterfaceImportLot.Code,
  ///   InterfaceImportLot.WhseId,
  ///   InterfaceImportLot.Whse,
  ///   InterfaceImportLot.QtyOnHand,
  ///   InterfaceImportLot.QtyFree,
  ///   InterfaceImportLot.LotStatusId,
  ///   InterfaceImportLot.LotStatus,
  ///   InterfaceImportLot.ExpiryDate,
  ///   InterfaceImportLot.ParentProductCode,
  ///   InterfaceImportLot.ExpectedYield 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportLot_Insert
(
 @InterfaceImportLotId int = null,
 @RecordStatus nvarchar(4) = null,
 @InsertDate datetime = null,
 @ProcessedDate datetime = null,
 @LotId nvarchar(100) = null,
 @Code nvarchar(510) = null,
 @WhseId nvarchar(100) = null,
 @Whse nvarchar(510) = null,
 @QtyOnHand nvarchar(100) = null,
 @QtyFree nvarchar(100) = null,
 @LotStatusId nvarchar(100) = null,
 @LotStatus nvarchar(200) = null,
 @ExpiryDate datetime = null,
 @ParentProductCode nvarchar(40) = null,
 @ExpectedYield float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportLot
        (InterfaceImportLotId,
         RecordStatus,
         InsertDate,
         ProcessedDate,
         LotId,
         Code,
         WhseId,
         Whse,
         QtyOnHand,
         QtyFree,
         LotStatusId,
         LotStatus,
         ExpiryDate,
         ParentProductCode,
         ExpectedYield)
  select @InterfaceImportLotId,
         @RecordStatus,
         isnull(@InsertDate, getdate()),
         @ProcessedDate,
         @LotId,
         @Code,
         @WhseId,
         @Whse,
         @QtyOnHand,
         @QtyFree,
         @LotStatusId,
         @LotStatus,
         @ExpiryDate,
         @ParentProductCode,
         @ExpectedYield 
  
  select @Error = @@Error
  
  
  return @Error
  
end
