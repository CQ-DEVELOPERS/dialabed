﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_OperatorPickingPerformance
  ///   Filename       : p_Mobile_OperatorPickingPerformance.sql
  ///   Create By      : Junaid Desai 
  ///   Date Created   : 21 NOV 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_OperatorPickingPerformance
(
  @Date            datetime = '01/01/1900',
  @OperatorId      int,
  @OperatorGroupId int 
)
 
as
begin
  set nocount on;
  
  declare @TableResult As Table 
  (
   OperatorId      int,
   OperatorGroupId int,
   Operator        nvarchar(50),
   PickedUnits     int,
   SiteUnits       int,
   PerformanceType nvarchar(1),
   EndDate         datetime,
   FaceOption      nvarchar(1)
  )
  
  if (@Date = '01/01/1900')
  begin
    set @Date = convert(nvarchar(14), GetDate(), 120) + '00:00' 
  end
  
  insert @TableResult
        (OperatorId,
         OperatorGroupId,
         Operator,
         PickedUnits,
         SiteUnits,
         PerformanceType,
         EndDate)
  select o.OperatorId,
         o.OperatorGroupId,
         o.Operator,
         Sum(op.Units),
         (Sum(distinct(og.Units))),
         og.PerformanceType,
         op.EndDate
    from Operator             o (nolock)
    join OperatorPerformance op (nolock) on o.OperatorId      = op.OperatorId
    join GroupPerformance    og (nolock) on o.OperatorGroupId = og.OperatorGroupId
   where o.OperatorGroupId  = @OperatorGroupId
     and OG.PerformanceType = 'S'
     and o.OperatorId       = @OperatorId
     and op.EndDate         = @Date
  group by o.OperatorId,
           o.OperatorGroupId,
           o.Operator,
           og.PerformanceType,
           op.EndDate
  
  update @TableResult
     set FaceOption = 'G'
   where PickedUnits >= SiteUnits
  
  update @TableResult
     set FaceOption = 'Y'
    where PickedUnits < SiteUnits
  
  update @TableResult
     set FaceOption = 'R'
   where PickedUnits < (SiteUnits/2)
  
  select OperatorId,
         convert(nvarchar(17), EndDate, 113) as Date,
         OperatorGroupId,
         Operator,
         PickedUnits,
         SiteUnits,
         PerformanceType,
         FaceOption
    from @TableResult
end
