﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Search
  ///   Filename       : p_Operator_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 13:19:57
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Operator table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null output,
  ///   @OperatorGroupId int = null,
  ///   @WarehouseId int = null,
  ///   @Operator nvarchar(100) = null,
  ///   @OperatorCode nvarchar(100) = null,
  ///   @CultureId int = null,
  ///   @PrincipalId int = null,
  ///   @MenuId int = null,
  ///   @ExternalCompanyId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup,
  ///   Operator.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Operator.Operator,
  ///   Operator.OperatorCode,
  ///   Operator.Password,
  ///   Operator.NewPassword,
  ///   Operator.ActiveIndicator,
  ///   Operator.ExpiryDate,
  ///   Operator.LastInstruction,
  ///   Operator.Printer,
  ///   Operator.Port,
  ///   Operator.IPAddress,
  ///   Operator.CultureId,
  ///   Culture.CultureId,
  ///   Operator.LoginTime,
  ///   Operator.LogoutTime,
  ///   Operator.LastActivity,
  ///   Operator.SiteType,
  ///   Operator.Name,
  ///   Operator.Surname,
  ///   Operator.PrincipalId,
  ///   Principal.Principal,
  ///   Operator.PickAisle,
  ///   Operator.StoreAisle,
  ///   Operator.MenuId,
  ///   Menu.Menu,
  ///   Operator.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompany,
  ///   Operator.Upload 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Search
(
 @OperatorId int = null output,
 @OperatorGroupId int = null,
 @WarehouseId int = null,
 @Operator nvarchar(100) = null,
 @OperatorCode nvarchar(100) = null,
 @CultureId int = null,
 @PrincipalId int = null,
 @MenuId int = null,
 @ExternalCompanyId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Operator = '-1'
    set @Operator = null;
  
  if @OperatorCode = '-1'
    set @OperatorCode = null;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
 
  select
         Operator.OperatorId
        ,Operator.OperatorGroupId
         ,OperatorGroupOperatorGroupId.OperatorGroup as 'OperatorGroup'
        ,Operator.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Operator.Operator
        ,Operator.OperatorCode
        ,Operator.Password
        ,Operator.NewPassword
        ,Operator.ActiveIndicator
        ,Operator.ExpiryDate
        ,Operator.LastInstruction
        ,Operator.Printer
        ,Operator.Port
        ,Operator.IPAddress
        ,Operator.CultureId
        ,Operator.LoginTime
        ,Operator.LogoutTime
        ,Operator.LastActivity
        ,Operator.SiteType
        ,Operator.Name
        ,Operator.Surname
        ,Operator.PrincipalId
         ,PrincipalPrincipalId.Principal as 'Principal'
        ,Operator.PickAisle
        ,Operator.StoreAisle
        ,Operator.MenuId
         ,MenuMenuId.Menu as 'Menu'
        ,Operator.ExternalCompanyId
         ,ExternalCompanyExternalCompanyId.ExternalCompany as 'ExternalCompany'
        ,Operator.Upload
    from Operator
    left
    join OperatorGroup OperatorGroupOperatorGroupId on OperatorGroupOperatorGroupId.OperatorGroupId = Operator.OperatorGroupId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Operator.WarehouseId
    left
    join Culture CultureCultureId on CultureCultureId.CultureId = Operator.CultureId
    left
    join Principal PrincipalPrincipalId on PrincipalPrincipalId.PrincipalId = Operator.PrincipalId
    left
    join Menu MenuMenuId on MenuMenuId.MenuId = Operator.MenuId
    left
    join ExternalCompany ExternalCompanyExternalCompanyId on ExternalCompanyExternalCompanyId.ExternalCompanyId = Operator.ExternalCompanyId
   where isnull(Operator.OperatorId,'0')  = isnull(@OperatorId, isnull(Operator.OperatorId,'0'))
     and isnull(Operator.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(Operator.OperatorGroupId,'0'))
     and isnull(Operator.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Operator.WarehouseId,'0'))
     and isnull(Operator.Operator,'%')  like '%' + isnull(@Operator, isnull(Operator.Operator,'%')) + '%'
     and isnull(Operator.OperatorCode,'%')  like '%' + isnull(@OperatorCode, isnull(Operator.OperatorCode,'%')) + '%'
     and isnull(Operator.CultureId,'0')  = isnull(@CultureId, isnull(Operator.CultureId,'0'))
     and isnull(Operator.PrincipalId,'0')  = isnull(@PrincipalId, isnull(Operator.PrincipalId,'0'))
     and isnull(Operator.MenuId,'0')  = isnull(@MenuId, isnull(Operator.MenuId,'0'))
     and isnull(Operator.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(Operator.ExternalCompanyId,'0'))
  order by Operator
  
end
