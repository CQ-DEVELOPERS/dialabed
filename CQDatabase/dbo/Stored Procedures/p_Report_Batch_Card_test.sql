﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Card_test
  ///   Filename       : p_Report_Batch_Card_test.sql
  ///   Create By      : Karen
  ///   Date Created   : January 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Batch_Card_test
(
 @WarehouseId            int,
 --@OrderNumber	         nvarchar(30),
 @OutboundShipmentId	 int,
 @IssueId				 int,
 @FromDate				 datetime,
 @ToDate				 datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   OutboundShipmentId	int,
   BOMInstructionId		int,
   IssueId				int,
   Quantity				int,
   Product				nvarchar(50),
   ProductCode			nvarchar(30),
   ExternalCompanyid	int,
   ExternalCompany		nvarchar(255),
   OrderNumber			nvarchar(30),
   Batch				nvarchar(50),
   OutboundDocumentId	int,
   JobId				int,
   ToLocation			nvarchar(10),
   BOMHeaderId			int			
  )
  
  declare @TableDetail as table
  (
   BOMInstructionId     int,
   StorageUnitId		int,
   IssueId				int,
   ProductDtl			nvarchar(50),
   ProductCodeDtl		nvarchar(30),
   PlannedQty			float,
   Percentage			float,
   ActualQty			float,
   OrderNumber			nvarchar(30),
   IssueLineId			int,
   BOMHeaderId			int
  )
  
  declare @Indicator		  char(1),
		  @OrderNumber		  nvarchar(30)  

  if @OutboundShipmentId = -1
	set @OutboundShipmentId = null
	
  if @IssueId = -1
	set @IssueId = null
	
  SELECT @OrderNumber = OrderNumber
  from OutboundDocument od
		join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
  WHERE Issueid = @IssueId
	
 	insert @TableHeader
		  (OutboundShipmentId,
		   BOMInstructionId,
		   IssueId,
		   Quantity,
		   Product,
		   ProductCode,
		   OrderNumber,
		   ExternalCompanyId,
		   Batch,
		   OutboundDocumentId,
		   ToLocation,
		   BOMHeaderId)		
	SELECT distinct bi.OutboundShipmentId,
		   bi.BOMInstructionId,
		   bi.IssueId,
		   bi.Quantity,
		   Product,
		   ProductCode,
		   bi.OrderNumber,
		   od.ExternalCompanyid,
		   b.batch,
		   od.OutboundDocumentId,
		   od.ToLocation,
		   bh.BOMHeaderId
	FROM      BOMInstruction    bi (nolock)
	join      BOMHeader         bh (nolock) on bi.BOMHeaderId = bh.BOMHeaderId
	join      StorageUnit       su (nolock) on bh.StorageUnitId = su.StorageUnitId 
	join      StorageUnitBatch sub (nolock) on sub.StorageUnitId = su.StorageUnitId
	join      Product            p (nolock) on su.ProductId = p.ProductId
	Left join OutboundDocument  od (nolock) on bi.OutboundDocumentId = od.OutboundDocumentId
	left join Batch              b (nolock) on b.batchid = od.BatchId
	where od.OrderNumber = @OrderNumber
	
    insert @TableDetail
		  (StorageUnitId,
		   PlannedQty,
		   OrderNumber,
		   ActualQty,
		   IssueLineId,
		   BOMHeaderId)
    select sub.StorageUnitId,
		   ol.Quantity,
		   th.OrderNumber,
		   ins.ConfirmedQuantity,
		   il.IssueLineId,
		   th.BOMHeaderId
	  from @TableHeader th
	  join OutboundDocument  od (nolock) on th.OrderNumber  = od.OrderNumber
      join Issue              i (nolock) on od.OutboundDocumentId            = i.OutboundDocumentId
      join IssueLine         il (nolock) on i.IssueId = il.IssueId
      join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join IssueLineInstruction ili (nolock) on ili.IssueId = i.IssueId and ili.IssueLineId = il.IssueLineId
      join Instruction ins (nolock) on ins.InstructionId = ili.InstructionId
      where not exists(select 1 from BOMPackaging bp where bp.StorageUnitId = sub.StorageUnitId)

	update td
     set ProductCodeDtl = p.ProductCode,
         ProductDtl     = p.Product,
         Percentage     = bp.Quantity
    from @TableDetail td
    join StorageUnit      su  (nolock) on td.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join BOMHeader        bh  (nolock) on bh.BOMHeaderId        = td.BOMHeaderId
    join BOMInstruction   bi  (nolock) on bi.BOMHeaderId        = td.BOMHeaderId
    join BOMInstructionLine bil  (nolock) on bil.BOMInstructionId = bi.BOMInstructionId
    join BOMProduct       bp  (nolock) on su.StorageUnitId		= bp.StorageUnitId
										and bp.BOMLineId = bil.BOMLineId
										and bp.LineNumber = 1

	delete from @TableDetail
	where ProductCodeDtl is null
	and ProductDtl is null
	
	
	update th
		set ExternalCompany = ec.ExternalCompany
	from @TableHeader th
	join externalcompany  ec (nolock) on ec.ExternalCompanyid = th.ExternalCompanyid
	
	
	update th
		set Jobid = (select max(i.jobid) from @TableHeader th
							left join IssueLineInstruction ili (nolock) on th.OutboundDocumentId = ili.OutboundDocumentId
							left join instruction i (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId)
	from @TableHeader th

 
	Select OutboundShipmentId,
		   th.BOMInstructionId,
		   th.BOMHeaderId,
		   Quantity,
		   Product,
		   ProductCode,
		   ExternalCompany,
		   Batch as ReferenceNumber,
		   th.OrderNumber,
		   StorageUnitId,
		   ProductDtl,
		   ProductCodeDtl,
		   PlannedQty,
		   (Percentage * 100) as Percentage,
		   ActualQty,
		   JobId,
		   ToLocation,
		   IssueLineId
	from   @TableHeader   th
	join   @TableDetail	  td on th.OrderNumber = td.OrderNumber
	order by ProductDtl
 
end

