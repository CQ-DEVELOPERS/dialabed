﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Code128_Update
  ///   Filename       : p_Code128_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:28
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Code128 table.
  /// </remarks>
  /// <param>
  ///   @Code128 nvarchar(100) = null,
  ///   @Char nvarchar(100) = null,
  ///   @Variant nvarchar(100) = null,
  ///   @AsciiNumber nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Code128_Update
(
 @Code128 nvarchar(100) = null,
 @Char nvarchar(100) = null,
 @Variant nvarchar(100) = null,
 @AsciiNumber nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @Code128 = '-1'
    set @Code128 = null;
  
	 declare @Error int
 
  update Code128
     set Code128 = isnull(@Code128, Code128),
         Char = isnull(@Char, Char),
         Variant = isnull(@Variant, Variant),
         AsciiNumber = isnull(@AsciiNumber, AsciiNumber) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
