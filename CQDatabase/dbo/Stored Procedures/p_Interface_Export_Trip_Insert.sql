﻿ 
 
  --/// <summary> 
  --///   Procedure Name : p_Interface_Export_Trip_Insert 
  --///   Filename       : p_Interface_Export_Trip_Insert.sql 
  --///   Create By      : Ruan Groenewald 
  --///   Date Created   : 22 May 2012 
  --/// </summary> 
  --/// <remarks> 
  --///    
  --/// </remarks> 
  --/// <param> 
  --///    
  --/// </param> 
  --/// <returns> 
  --///    
  --/// </returns> 
  --/// <newpara> 
  --///   Modified by    :  
  --///   Modified Date  :  
  --///   Details        :  
  --/// </newpara> 
 
CREATE PROCEDURE [dbo].[p_Interface_Export_Trip_Insert]  
( 
     @IssueId int  = NULL
,    @OutboundShipmentId Int = NULL
) 
--with encryption 
as 
begin 
    set nocount on; 
    
    declare @Error                              int,         
            @Errormsg                           varchar(500),         
            @GetDate                            datetime,
            @Rowcount                           int,
            @PrimaryKey                         nVarChar(30),
            @InterfaceExportShipmentId          int,
            @InterfaceExportShipmentDeliveryId  Int,
            @trancount							bit = 1
           
    Declare @OrderLines As Table
        (InterfaceExportShipmentDeliveryId Int
        ,ReferenceNumber    Nvarchar(50)
        ,JobId              Int
        ,ContainerType      NVarChar(50)
        ,DropSequence       Int
        ,Length             Float  
        ,Width              Float
        ,Height             Float
        ,NettWeight         Float
        ,TareWeight         Float
        ,StorageUnitBatchId Int
        ,ProductCode        NVarChar(50)
        ,Product            NVarChar(255)
        ,BarCode            NVarChar(50)
        ,SkuCode            NVarChar(50)
        ,Batch              NVarChar(50)
        ,ExpiryDate         DateTime
        ,PackType           NVarChar(50)
        ,PackItems          Float
        ,PackTare           Float
        ,InstructionId      Int
        ,Quantity           Float
        ,CheckQuantity      Float
        ,ShortQuantity      Float
        ,Weight             Float)
 
    Declare @IssueList As Table
		(IssueId Int)
    
    Declare @IssueJobs As Table
		(IssueId Int
		,JobID Int)
		
	Declare @NoStock As Table
		(IssueId		Int
		,PrimaryKey		NVarchar(30)
		,OrderNumber	NVarchar(30)
		,RecordType		NVarchar(30)
		,RecordStatus	Char(1)
		,InsertDate		DateTime
		,PrincipalCode	NVarChar(30))
 
    select @GetDate = dbo.ufn_Getdate()         
            
    set @Errormsg = 'Error executing p_Interface_Export_Trip_Insert' 
    
    if isnull(@OutboundShipmentId,-1) = -1
      set @OutboundShipmentId = null
    
    if @IssueId is not null
    begin
		Insert Into @IssueList
			(IssueId)
		Values
			(@IssueId)
    end
    else
    begin
		Insert Into @IssueList
			(IssueId)
		Select IssueId
		  From OutboundShipmentIssue
		 Where OutboundShipmentId = @OutboundShipmentId			
    end
    
    If @@ROWCOUNT > 0
    Begin
        
        if @@trancount = 0
		begin
		  begin transaction
		  Set @trancount = 1
		end
		
        Insert Into @IssueJobs
				(IssueId, JobID)
		Select  Iss.IssueId, j.JobId
		FROM	Instruction AS i INNER JOIN
				Job AS j WITH (nolock) ON i.JobId = j.JobId INNER JOIN
				Status AS s WITH (nolock) ON j.StatusId = s.StatusId INNER JOIN
				IssueLineInstruction AS ili WITH (nolock) ON ISNULL(i.InstructionRefId, i.InstructionId) = ili.InstructionId INNER JOIN
				IssueLine AS il WITH (nolock) ON ili.IssueLineId = il.IssueLineId INNER JOIN
				@IssueList AS iss ON il.IssueId = iss.IssueId
		WHERE   ISNULL(i.CheckQuantity, 0) > 0
		  AND   s.StatusCode in ('CD','DC','D','C')
		  AND   NOT EXISTS			-- Make sure all jobs for issue are complete
		        (select iss1.IssueId
				   from Instruction            i1
				   join Job                    j1 (nolock) on i1.JobId              = j1.JobId
				   join Status                 s1 (nolock) on j1.StatusId           = s1.StatusId
				   join IssueLineInstruction ili1 (nolock) on isnull(i1.InstructionRefId, i1.InstructionId) = ili1.InstructionId
				   join IssueLine             il1 (nolock) on ili1.IssueLineId = il1.IssueLineId
				   join Issue                iss1 (nolock) on il1.IssueId = iss1.IssueId
                  Where s1.StatusCode not in ('CD','DC','D','C','NS')
		           And iss1.IssueId = iss.IssueId)
		Group by iss.IssueId, j.JobId
		Order By IssueId, JobId
		
		SELECT @Rowcount = @@rowcount 
              ,@Error = @@Error 
              ,@Errormsg ='Error executing p_Interface_Export_Trip_Insert - No valid jobs for shipment'
        
		Insert Into @NoStock
			(IssueId
			,PrimaryKey
			,OrderNumber
			,RecordType
			,RecordStatus
			,InsertDate
			,PrincipalCode)
		Select Distinct  
		   iss.IssueId 
		  ,CONVERT(VARCHAR, iss.IssueId)	As PrimaryKey  
		  ,od.OrderNumber					As OrderNumber  
		  ,'FinalDespatch'					As RecordType  
		  ,'N'								As RecordStatus  
		  ,@GetDate							As InsertDate  
		  ,p.PrincipalCode
		From OutboundDocument od
		Inner Join Issue iss on od.OutboundDocumentId = iss.OutboundDocumentId
		Inner Join Principal p on od.PrincipalId = p.PrincipalId
		Inner Join @IssueList il on iss.IssueId = il.IssueId
		Where Not Exists (select 1 from @IssueJobs ij where ij.IssueId = iss.IssueId)
		  AND NOT EXISTS			-- Make sure all jobs for issue are complete
		        (select iss1.IssueId
				   from Instruction            i1
				   join Job                    j1 (nolock) on i1.JobId              = j1.JobId
				   join Status                 s1 (nolock) on j1.StatusId           = s1.StatusId
				   join IssueLineInstruction ili1 (nolock) on isnull(i1.InstructionRefId, i1.InstructionId) = ili1.InstructionId
				   join IssueLine             il1 (nolock) on ili1.IssueLineId = il1.IssueLineId
				   join Issue                iss1 (nolock) on il1.IssueId = iss1.IssueId
                  Where s1.StatusCode not in ('CD','DC','D','C','NS')
		           And iss1.IssueId = iss.IssueId)
		
		if @@ROWCOUNT > 0
		Begin
			Insert Into InterfaceExportHeader  
			  (PrimaryKey  
			  ,IssueId
			  ,OrderNumber  
			  ,RecordType  
			  ,RecordStatus  
			  ,InsertDate  
			  ,PrincipalCode)  
			Select 
			   PrimaryKey  
			  ,IssueId
			  ,OrderNumber  
			  ,RecordType  
			  ,RecordStatus  
			  ,InsertDate  
			  ,PrincipalCode
			From @NoStock
			
			Update Issue
			Set Interfaced = 1
			Where IssueId in (Select IssueId From @NoStock)
		End

		if @Rowcount = 0
		begin
			goto result
		end
		      
        Insert Into InterfaceExportShipment
            (   
            RecordStatus
           ,PrimaryKey
           ,OutboundShipmentId
           ,RouteCode
           --,DivisionCode
           ,VehicleRegistration
           ,SequenceNumber
           ,WarehouseCode
           ,CompanyCode
           ,CompanyName
           ,Street
           ,Suburb
           ,Town
           ,Country
           ,PostalCode
           --,ContactPerson
           --,Telephone
           --,EMail
           ,InsertDate
        )
        Select Distinct
            'N'                     As 'RecordStatus'
           ,@OutboundShipmentId     As 'PrimaryKey'
           ,@OutboundShipmentId     As 'OutboundShipmentId'
           ,r.RouteCode             As 'RouteCode'
           --,d.DivisionCode          As 'DivisionCode'
           ,os.VehicleRegistration  As 'VehicleRegistration'
           ,1                       As 'SequenceNumber'
           ,w.WarehouseCode         As 'WarehouseCode'
           ,c.CompanyCode          As 'CompanyCode'
           ,c.Company               As 'CompanyName'
           ,a.Street                As 'Street'
           ,a.Suburb                As 'Suburb'
           ,a.Town                  As 'Town'
           ,a.Country               As 'Country'
           ,a.Code                  As 'PostalCode'
           --,cl.ContactPerson        As 'ContactPerson'
           --,cl.Telephone           As 'Telephone'
           --,cl.EMail                As 'EMail'
           ,GETDATE()
        From           Issue                    i (nolock)
            Left Join OutboundShipmentIssue  osi (nolock) on i.IssueId                     = osi.IssueId
            Left Join OutboundShipment        os (nolock) on os.OutboundShipmentId         = osi.OutboundShipmentId
            Inner Join Warehouse                w (nolock) on w.WarehouseId                 = i.WarehouseId
            Inner Join Company                  c (nolock) on c.CompanyId                   = w.CompanyId
            --Inner Join OutboundDocument        od (nolock) on  od.OutboundDocumentId        =   i.OutboundDocumentId
            --Inner join OutboundDocumentType   odt (nolock) on odt.OutboundDocumentTypeId    =  od.OutboundDocumentTypeId         
            Left Join  Route                    r (nolock) on   r.RouteId                   =  os.RouteId
            Left Join  Address                  a (nolock) on   a.AddressId                 =   w.AddressId
            --Left Join  ContactList             cl (nolock) on  cl.ContactListId             =   w.ContactListId
            --Left Join  Division                 d (nolock) on   d.DivisionId                =  od.DivisionId
         Where i.IssueId in (Select IssueId From @IssueJobs)
 
        select @InterfaceExportShipmentId = scope_identity() 
              ,@Rowcount = @@rowcount 
              ,@Error    = @@Error 
              ,@Errormsg ='p_Interface_Export_Trip_Insert - insert InterfaceExportShipmentDelivery'
              ,@PrimaryKey = @OutboundShipmentId
              
        Insert Into InterfaceExportShipmentDelivery
            (InterfaceExportShipmentId
            ,IssueId
            ,JobId
            ,ForeignKey
            ,WaybillNo
            ,InvoiceNumber
            ,DropSequence
            ,DeliveryMethod
            ,DeliveryNoteNumber
            ,SealNumber
            ,WarehouseCode
            ,ConsigneeName
            ,ConsigneeCode
            ,ConsigneeType
            ,Street
            ,Suburb
            ,Town
            ,Country
            ,PostalCode
            --,ContactName
            --,Telephone
            --,EMail
            ,Comments
            --,TrustedDelivery
            --,ParcelDelivery
            ,DespatchDate
            ,PrincipalCode
            ,RecordStatus
            ,RecordType)
        Select Distinct
             @InterfaceExportShipmentId     As 'InterfaceExportShipmentId'
            ,i.IssueId                      As 'IssueId'
            ,ij.JobId					As 'JobId'
            ,@PrimaryKey                    As 'ForeignKey'
            ,od.OrderNumber                 As 'WaybillNo'
            ,od.ReferenceNumber             As 'InvoiceNumber'
            ,ds.DropSequence                AS 'DropSequence'
            ,NULL                           As 'DeliveryMethod'
            ,i.DeliveryNoteNumber           As 'DeliveryNoteNumber'
            ,i.SealNumber                   As 'SealNumber'
            ,od.ToLocation                  As 'WarehouseCode'
            ,ec.ExternalCompany             As 'ConsigneeName'
            ,ec.ExternalCompanyCode         As 'ConsigneeCode'
            ,ect.ExternalCompanyType        As 'ConsigneeType'
            ,a.Street                       As 'Street'
            ,a.Suburb                       As 'Suburb'
            ,a.Town                         As 'Town'
            ,a.Country                      As 'Country'
            ,a.Code                         As 'PostalCode'
            --,cl.ContactPerson               As 'ContactName'
            --,cl.Telephone                   As 'Telephone'
            --,cl.EMail                       As 'Email'
            ,i.Remarks                      As 'Comments'
            --,TrustedDelivery
            --,ParcelDelivery
            ,i.DeliveryDate                 As 'DespatchDate'
            ,p.PrincipalCode
            ,'N'
            ,OutboundDocumentTypeCode
        FROM           
			Issue                                i (NOLOCK)
            LEFT JOIN OutboundShipmentIssue    osi (NOLOCK) ON i.IssueId                    = osi.IssueId
            LEFT JOIN OutboundShipment          os (nolock) on osi.OutboundShipmentId       =  os.OutboundShipmentId
            INNER JOIN @IssueJobs			    ij		    ON i.IssueId				    =  ij.IssueId
            INNER JOIN OutboundDocument         od (NOLOCK) ON od.OutboundDocumentId        =   i.OutboundDocumentId
            INNER JOIN Principal                 p (NOLOCK) ON od.PrincipalId               =   p.PrincipalId
            INNER JOIN OutboundDocumentType    odt (NOLOCK) on odt.OutboundDocumentTypeId   =  od.OutboundDocumentTypeId         
            INNER JOIN ExternalCompany          ec (NOLOCK) ON ec.ExternalCompanyId         =  od.ExternalCompanyId
            LEFT JOIN  ExternalCompanyType     ect (NOLOCK) ON ect.ExternalCompanyTypeId    =  ec.ExternalCompanyTypeId
            LEFT JOIN  [Route]                   r (NOLOCK) ON r.RouteId                    =  os.RouteId
            LEFT JOIN  [Address]                 a (NOLOCK) ON a.ExternalCompanyId          =   ec.ExternalCompanyId
            --LEFT JOIN  ContactList             cl (NOLOCK) ON  cl.ExternalCompanyId         =   ec.ExternalCompanyId
            LEFT JOIN ( SELECT ROW_NUMBER() OVER (ORDER BY od.ExternalCompanyId) AS DropSequence, od.ExternalCompanyId
                                    FROM OutboundShipmentIssue (NOLOCK) osi
                                    JOIN Issue i (NOLOCK) ON osi.IssueId = i.IssueId
                                    JOIN OutboundDocument od (NOLOCK) ON i.OutboundDocumentId = od.OutboundDocumentId
                                    WHERE osi.OutboundShipmentId = @OutboundShipmentId
                                    GROUP BY od.ExternalCompanyId) AS ds ON ds.ExternalCompanyId = od.ExternalCompanyId
		WHERE i.IssueId in (Select IssueId From @IssueJobs)
		
        update i
           set ContactName = cl.ContactPerson,
               Telephone   = cl.Telephone,
               Email       = cl.EMail
          from InterfaceExportShipmentDelivery i
          JOIN ExternalCompany                ec (NOLOCK) ON i.ConsigneeCode           = ec.ExternalCompanyCode
          JOIN ExternalCompanyType      ect (NOLOCK) ON ect.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
                                                         and i.ConsigneeType           = ect.ExternalCompanyTypeCode
          JOIN ContactList                    cl (NOLOCK) ON cl.ExternalCompanyId      = ec.ExternalCompanyId
          where i.InterfaceExportShipmentId = @InterfaceExportShipmentId
        
        SELECT @Rowcount = @@rowcount 
              ,@Error = @@Error 
              ,@Errormsg ='p_Interface_Export_Pack_Insert - insert InterfaceExportPackHeader'
              
		update sd
		  set WaveId = w.WaveId
		from InterfaceExportShipmentDelivery sd
		  Join Issue iss on sd.IssueId = iss.IssueId
          JOIN Wave w on iss.WaveId = w.WaveId
        Where sd.WaveId is Null
    End
    
    Insert Into @OrderLines
            (InterfaceExportShipmentDeliveryId
            ,ReferenceNumber
            ,JobId
            ,DropSequence
            ,ContainerType
            ,[Length]
            ,Width
            ,Height
            ,TareWeight
            ,StorageUnitBatchId
            ,InstructionId
            ,Quantity
            ,CheckQuantity
            ,ShortQuantity
            ,Weight)
        Select
             InterfaceExportShipmentDeliveryId
            ,ISNULL(j.ReferenceNumber, 'J:' + CONVERT(VARCHAR, j.JobId)) As ReferenceNumber
            ,j.JobId
			,j.DropSequence
            ,ct.ContainerType
            ,ct.[Length]
            ,ct.Width
            ,ct.Height
            ,ct.TareWeight
            ,ins.StorageUnitBatchId
            ,ins.InstructionId
            ,SUM(ISNULL(ins.Quantity, 0))
            ,SUM(ISNULL(ins.CheckQuantity, 0))
            ,SUM(ISNULL(ins.Quantity, 0) - ISNULL(ins.CheckQuantity, 0))
            ,SUM(ISNULL(ins.[Weight], 0))
        From
            InterfaceExportShipmentDelivery ies (nolock)
            Inner Join Issue                  i (nolock) on i.IssueId           = ies.IssueId
            Inner Join IssueLine             il (nolock) on il.IssueId          = i.IssueId
            Inner Join IssueLineInstruction ili (nolock) on ili.IssueLineId     = il.IssueLineId
            Inner Join Instruction          ins (nolock) on isnull(ins.InstructionRefId, ins.InstructionId)   = ili.InstructionId
            Inner Join Job                    j (nolock) on j.JobId             = ins.JobId
														and j.JobId				= ies.JobId
            Left Join  ContainerType         ct (nolock) on ct.ContainerTypeId  = j.ContainerTypeId
        Where ili.IssueId in (select IssueId From @IssueJobs)
        Group By
             ies.InterfaceExportShipmentDeliveryId
            ,j.ReferenceNumber
            ,j.JobId
            ,j.DropSequence
            ,ct.ContainerType
            ,ct.Length
            ,ct.Width
            ,ct.Height
            ,ct.TareWeight
            ,ins.StorageUnitBatchId
            ,ins.InstructionId
           
    Update ol
       Set ProductCode  = pr.ProductCode
          ,Product      = pr.Product
          ,BarCode      = ISNULL(pr.Barcode, pr.Barcode)
          --,PackType     = pt.PackType
          ,Batch        = b.Batch
          ,SkuCode      = SKU.SKUCode
      From @OrderLines ol
      Inner Join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId =  ol.StorageUnitBatchId
      Inner Join StorageUnit       su (nolock) on  su.StorageUnitId      = sub.StorageUnitId
      Inner Join Batch              b (nolock) on   b.BatchId            = sub.BatchId
      --Inner Join Pack              pk (nolock) on  pk.StorageUnitId      =  su.StorageUnitId     
      --Inner Join PackType          pt (nolock) on  pt.PackTypeId         =  pk.PackTypeId
      Inner Join Product           pr (nolock) on  pr.ProductId          =  su.ProductId
      Inner Join SKU                  (nolock) on SKU.SKUId              =  su.SKUId
    
    Insert Into InterfaceExportShipmentDetail
        (InterfaceExportShipmentDeliveryId
        ,ReferenceNumber
        ,JobId
        ,ContainerType
        ,Length
        ,Width
        ,Height
        ,NettWeight
        ,TareWeight
        ,DropSequence
        ,LineNumber
        ,ProductCode
        ,Product
        ,SkuCode
        ,Batch
        ,ExpiryDate
        ,Boxed
        ,Quantity
        ,CheckQuantity
        ,ShortQuantity
        ,Weight
        ,Barcode
        ,SerialNo
        ,PackType
        ,PackItems
        ,PackTare
        ,Trusted)
    Select
         InterfaceExportShipmentDeliveryId
        ,ReferenceNumber
        ,JobId
        ,ContainerType
        ,Length
        ,Width
        ,Height
        ,NettWeight
        ,TareWeight
        ,DropSequence
        ,ROW_NUMBER() Over (Order By JobId)
        ,ProductCode
        ,Product
        ,SkuCode
        ,Batch
        ,ExpiryDate
        ,'' As Boxed
        ,Quantity
        ,CheckQuantity
        ,ShortQuantity
        ,Weight
        ,Barcode
        ,NULL As SerialNo
        ,PackType
        ,PackItems
        ,PackTare
        ,'' As Trusted
    From @OrderLines
    where CheckQuantity > 0
    
    UPDATE Issue
    SET Interfaced = 1
    WHERE IssueId IN
    (Select IssueId From @IssueJobs)
 
	if @Error <> 0  
    goto error  
    
  result:  
      if @TRANCOUNT = 1  
        commit transaction  
      return 0  
      
    error:  
      if @TRANCOUNT = 1  
      begin  
        RAISERROR (@Errormsg,11,1)  
        rollback transaction  
      end  
      return @Error    
End   
    
 
