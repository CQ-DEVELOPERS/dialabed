﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Update
  ///   Filename       : p_COA_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:49
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the COA table.
  /// </remarks>
  /// <param>
  ///   @COAId int = null,
  ///   @COACode nvarchar(510) = null,
  ///   @COA nvarchar(max) = null,
  ///   @StorageUnitBatchId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Update
(
 @COAId int = null,
 @COACode nvarchar(510) = null,
 @COA nvarchar(max) = null,
 @StorageUnitBatchId int = null 
)
 
as
begin
	 set nocount on;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @COACode = '-1'
    set @COACode = null;
  
  if @COA = '-1'
    set @COA = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
	 declare @Error int
 
  update COA
     set COACode = isnull(@COACode, COACode),
         COA = isnull(@COA, COA),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId) 
   where COAId = @COAId
  
  select @Error = @@Error
  
  
  return @Error
  
end
