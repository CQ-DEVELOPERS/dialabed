﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaLocationHistory_Insert
  ///   Filename       : p_AreaLocationHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:40
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaLocationHistory table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @LocationId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   AreaLocationHistory.AreaId,
  ///   AreaLocationHistory.LocationId,
  ///   AreaLocationHistory.CommandType,
  ///   AreaLocationHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaLocationHistory_Insert
(
 @AreaId int = null,
 @LocationId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert AreaLocationHistory
        (AreaId,
         LocationId,
         CommandType,
         InsertDate)
  select @AreaId,
         @LocationId,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
