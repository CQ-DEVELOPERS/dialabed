﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Location_Truncate
  ///   Filename       : p_Wizard_Location_Truncate.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 22 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 	
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Location_Truncate
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
    TRUNCATE TABLE LocationImport
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Wizard_Location_Truncate'
    rollback transaction
    return @Error
end
