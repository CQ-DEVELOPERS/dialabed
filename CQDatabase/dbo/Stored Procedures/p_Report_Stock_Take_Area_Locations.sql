﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_Area_Locations
  ///   Filename       : p_Report_Stock_Take_Area_Locations.sql
  ///   Create By      : Karen
  ///   Date Created   : February 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Report_Stock_Take_Area_Locations]
(
 @WarehouseId			int,
 @FromDate				datetime,
 @ToDate				datetime,
 @AreaId				Int = null,
 @Counted				nvarchar(1),
 @InstructionTypeCode	nvarchar(25)
)
as
begin
	 set nocount on; 

if (@AreaId = 0) or (@AreaId = -1)
Begin
 set @AreaId = null
end


CREATE TABLE #StockTakeSummary 
	
--Declare #StockTakeSummary as  Table 
		(InstructionType nvarchar(50),
		 InstructionTypeCode nvarchar(25),
		 Area				nvarchar(50),
		 AreaId				int,
		 LocationId			int,
		 Location			nvarchar(15),
	 	 Date				Datetime,
		 LocationCount		Int,
		 StockTakes			Int,
		 Cancelled			Int,		
		 Waiting			int,
		 Busy				int,
		 Complete			float,
		 Created			 int)

	If @AreaId = -1 
		Set @AreaId = null

	Declare @AreaLocationCount as Table  
		(InstructionTypeCode nvarchar(10) NOT NULL,
		AreaId				 int NOT NULL,
		LocationCount		 int NULL,
		LocationId			 int null) 

	Insert into @AreaLocationCount 
			(InstructionTypeCode,
			AreaId,
			LocationCount,
			LocationId)
	Select	'STL',
			al.AreaId,
			Count(LocationId)  as LocationCount,
			al.LocationId
	  from AreaLocation al
	  Join Area a on a.AreaId = al.AreaId
	 where Isnull(@AreaId,al.AreaId) = al.AreaId
	   and a.WarehouseId = @WarehouseId
	Group by al.AreaId, al.LocationId

	Insert into @AreaLocationCount 
			(InstructionTypeCode,
			AreaId,
			LocationCount,
			LocationId)
	Select	'STE',
			al.AreaId,
			Count(LocationId)  as LocationCount,
			al.LocationId
	  from AreaLocation al
	  Join Area a on a.AreaId = al.AreaId
	 where Isnull(@AreaId,al.AreaId) = al.AreaId
	   and a.WarehouseId = @WarehouseId
	Group by al.AreaId, al.LocationId

Declare @TempDate Datetime

Set @TempDate = @FromDate

While (@TempDate <= @ToDate)
Begin

	Insert into #StockTakeSummary
	Select Distinct i.InstructionType,
			i.InstructionTypeCode,
			Area,
			c.AreaId,
			c.LocationId,
			l.Location,
			@TempDate,
			LocationCount,
			0 as StockTakes,
			0 as Cancelled,
			0 as Waiting,
			0 as Busy,
			0 as Complete,
			0 as Created
		From @AreaLocationCount c
		Join InstructionType i on i.InstructionTypeCode = c.InstructionTypeCode		
		Join Area a on a.Areaid = c.AreaId
		join Location l on c.LocationId = l.LocationId
		where i.InstructionTypeCode = @InstructionTypeCode
				
	

		Set  @TempDate = Dateadd(dd,1,@TempDate)
End
	Select	vs.InstructionType,
			vs.InstructionTypeCode,
			vs.Area,
			vs.Areaid,
			vs.Status,
			vs.CreateDate,
			Count(PickLocationId) as Cnt,
			PickLocationId as LocationId
	  Into #Instruction_Stock
	  from view_Instruction_Stock vs
	  join #StockTakeSummary s  on cast(s.Date as DATE)= cast(vs.CreateDate as DATE) 
							   and s.InstructionType = vs.InstructionType 
							   and s.Area= vs.Area
  Group by	vs.InstructionType,
			vs.InstructionTypeCode,
			vs.Area,
			vs.Areaid,
			vs.Status,
			vs.CreateDate,
			vs.PickLocationId
			
	--select * from #Instruction_Stock

	Update #StockTakeSummary 
	  Set StockTakes = StockTakes + 1 
	  From #StockTakeSummary s
	  Join #Instruction_Stock vs on vs.InstructionType = s.InstructionType 
								and vs.AreaId = s.AreaId 
								and cast(vs.CreateDate as DATE) = cast(s.Date as DATE)
								and vs.InstructionTypeCode = s.InstructionTypeCode
	 Where vs.Status = 'Finished'
	 and s.LocationId = vs.LocationId

	Update #StockTakeSummary Set Waiting = Waiting + 1
	  From #StockTakeSummary s
	  Join #Instruction_Stock vs on vs.InstructionType = s.InstructionType 
								and vs.AreaId = s.AreaId 
								and cast(vs.CreateDate as DATE) = cast(s.Date as DATE)
								and vs.InstructionTypeCode = s.InstructionTypeCode
	 Where vs.Status = 'Waiting'
	 and s.LocationId = vs.LocationId

	Update #StockTakeSummary Set Cancelled = Cancelled + 1 
	  From #StockTakeSummary s
	  Join #Instruction_Stock vs on vs.InstructionType = s.InstructionType 
								and vs.AreaId = s.AreaId 
								and cast(vs.CreateDate as DATE) = cast(s.Date as DATE)
								and vs.InstructionTypeCode = s.InstructionTypeCode
	 Where vs.Status in ('Cancelled','Deleted')
	 and s.LocationId = vs.LocationId

	Update #StockTakeSummary Set Busy = Busy + 1 
	  From #StockTakeSummary s
	  Join #Instruction_Stock vs on vs.InstructionType = s.InstructionType 
							    and vs.AreaId = s.AreaId 
							    and cast(vs.CreateDate as DATE) = cast(s.Date as DATE)
							    and vs.InstructionTypeCode = s.InstructionTypeCode
	 Where vs.Status = 'Started'
	 and s.LocationId = vs.LocationId
	 
	  Update #StockTakeSummary Set Created = isnull(StockTakes,0) + isnull(Cancelled,0) + isnull(Waiting,0) + isnull(Busy,0) 
	  From #StockTakeSummary s


	----Update #StockTakeSummary 
	----   set Complete = Round(Convert(Float,StockTakes) * 100 / Convert(Float,LocationCount),2)
	   
	----print '@Counted'
	----print @Counted
	
	If @Counted = 'A'
		Select * 
		  from #StockTakeSummary
		 order by 
			stocktakes desc

	If @Counted != 'A'
		Select * 
		  from #StockTakeSummary
		 where Created <= 0
		 order by 
			stocktakes desc
end
