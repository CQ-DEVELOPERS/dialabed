﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Driver_Trip_Sheet
  ///   Filename       : p_Report_Driver_Trip_Sheet.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>QA
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Driver_Trip_Sheet
(
 @ContactListId		int,
 @Route			    nvarchar(50),
 @FromDate			datetime,
 @ToDate			datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   Sequence				int,
   OutboundShipmentId	int,
   IssueId				int,
   OutboundDocumentId	int,
   OrderNumber			nvarchar(30),
   ExternalCompanyId	int,
   ExternalCompany		nvarchar(50),
   DespatchDate			datetime,
   DespatchBay			int,
   DespatchBayName		nvarchar(15),
   BoxesExpected		int,
   BoxesConfirmed		int,
   RouteId				int,
   Route				nvarchar(50),
   DriverId				int,
   Driver				nvarchar(50),
   VehicleRegistration	nvarchar(10),
   InvoiceNumber		nvarchar(30),
   ReferenceNumber      nvarchar(30),
   IntransitLoadId		int,
   JobId				int
  )
  
  declare @sequence int
  
  if @ContactListId = -1
    set @ContactListId = null
    
  if @Route = '-1'
  or @Route = ''
    set @Route = null

  if @ContactListId is null
  and @Route is null
	insert	@TableResult
           (OutboundShipmentId,
			IssueId,
			OutboundDocumentId,
			OrderNumber,
			ExternalCompanyId,
			DespatchDate,
			DespatchBay,
			Route,
			DriverId,
			VehicleRegistration,
			ReferenceNumber,
			IntransitLoadId,
			JobId)
   	 select	distinct os.OutboundShipmentId,
			ili.IssueId,
			od.OutboundDocumentId,
			od.OrderNumber,
			od.ExternalCompanyId,
			itl.CreateDate,
			os.DespatchBay,
			itl.Route,
			itl.DriverId,
			itl.VehicleRegistration,
			j.ReferenceNumber,
			itl.IntransitLoadId,
			j.JobId
    from IntransitLoad itl
    join IntransitLoadJob itlj on itlj.IntransitLoadId = itl.IntransitLoadId
    join Job j on j.JobId = itlj .JobId
    join Instruction i on i.JobId = j.JobId
    left join IssueLineInstruction ili on ili.InstructionId = i.InstructionId
    left join OutboundShipment os on  os.OutboundShipmentId = ili.OutboundShipmentId
    left join OutboundDocument od on od.OutboundDocumentId = ili.OutboundDocumentId
    left join Route r on r.Route = os.Route
	where  itl.CreateDate between @FromDate and @ToDate
	
	else
insert	@TableResult
           (OutboundShipmentId,
			IssueId,
			OutboundDocumentId,
			OrderNumber,
			ExternalCompanyId,
			DespatchDate,
			DespatchBay,
			Route,
			DriverId,
			VehicleRegistration,
			ReferenceNumber,
			IntransitLoadId,
			JobId)
   	 select	distinct os.OutboundShipmentId,
			ili.IssueId,
			od.OutboundDocumentId,
			od.OrderNumber,
			od.ExternalCompanyId,
			itl.CreateDate,
			os.DespatchBay,
			itl.Route,
			itl.DriverId,
			itl.VehicleRegistration,
			j.ReferenceNumber,
			itl.IntransitLoadId,
			j.JobId
    from IntransitLoad itl
    join IntransitLoadJob itlj on itlj.IntransitLoadId = itl.IntransitLoadId
    join Job j on j.JobId = itlj .JobId
    join Instruction i on i.JobId = j.JobId
    left join IssueLineInstruction ili on ili.InstructionId = i.InstructionId
    left join OutboundShipment os on  os.OutboundShipmentId = ili.OutboundShipmentId
    left join OutboundDocument od on od.OutboundDocumentId = ili.OutboundDocumentId
    left join ContactList      cl on cl.ContactListId = itl.DriverId
    left join Route r on r.Route = itl.Route
	where  itl.Route = isnull(@Route, itl.Route)
	and    itl.DriverId = ISNULL(@ContactListId, itl.DriverId)
	and    itl.CreateDate between @FromDate and @ToDate
	

  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult tr
    join ExternalCompany  ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId

  update tr
     set DespatchBayName = l.Location
    from @TableResult tr
    join Location   l (nolock) on tr.DespatchBay = l.LocationId
    
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route   r (nolock) on tr.RouteId = r.RouteId
    
  update tr
     set Driver = cl.ContactPerson
    from @TableResult tr
    join ContactList   cl (nolock) on tr.DriverId = cl.ContactListId
 
 update tr    
 set BoxesExpected  = (select  COUNT(distinct j.JobId)
      from IssueLine              il (nolock)
      join Issue                   iss (nolock) on il.IssueId  = iss.IssueId
      join OutboundDocument       od (nolock) on iss.OutboundDocumentId = od.OutboundDocumentId
      join Instruction      i (nolock) on il.IssueLineId      = i.IssueLineId
      join Job              j (nolock) on i.JobId             = j.JobId
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Status           s (nolock) on s.StatusId = j.StatusId
     where it.InstructionTypeCode in ('P','PM','PS','FM','PR')
     and il.IssueId = tr.IssueId
     and s.StatusCode in ('CD','D','DC','C'))
 from @TableResult tr
 
 update tr1    
 set BoxesConfirmed  = (select  COUNT(distinct tr2.JobId)
      from @TableResult tr2 
      where tr1.OrderNumber = tr2.OrderNumber)
 from @TableResult tr1

 update tr
     set InvoiceNumber = im.InterfaceMessage
    from @TableResult tr
    join InterfaceMessage   im (nolock) on tr.OrderNumber = im.OrderNumber
 where InterfaceTable = 'InterfaceExportSOHeader' 
    and InterfaceMessageCode = 'Processed'
 
  --select rank() OVER (ORDER BY Driver,
		--VehicleRegistration,
		--DespatchDate,
		--Route,
		--OrderNumber) as Sequence, 
  -- from authors a
  -- order by rank 
      
 Select	distinct rank() OVER (partition by VehicleRegistration,
		DespatchDate,
		Route 
		ORDER BY Driver,
		VehicleRegistration,
		DespatchDate,
		Route,
		OrderNumber) as Sequence,
		OrderNumber,
		ExternalCompany,
		DespatchDate,
		BoxesExpected,
		BoxesConfirmed,
		Route,
		Driver,
		VehicleRegistration,
		InvoiceNumber
 from @TableResult
 where BoxesExpected > 0 or BoxesConfirmed > 0
 order by
		Driver,
		VehicleRegistration,
		DespatchDate,
		Route,
		Sequence

 
end
