﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CultureHistory_Insert
  ///   Filename       : p_CultureHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:59
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the CultureHistory table.
  /// </remarks>
  /// <param>
  ///   @CultureId int = null,
  ///   @CultureName nvarchar(100) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   CultureHistory.CultureId,
  ///   CultureHistory.CultureName,
  ///   CultureHistory.CommandType,
  ///   CultureHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CultureHistory_Insert
(
 @CultureId int = null,
 @CultureName nvarchar(100) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert CultureHistory
        (CultureId,
         CultureName,
         CommandType,
         InsertDate)
  select @CultureId,
         @CultureName,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
