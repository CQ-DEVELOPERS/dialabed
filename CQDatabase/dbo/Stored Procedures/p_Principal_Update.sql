﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Principal_Update
  ///   Filename       : p_Principal_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 08:15:17
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Principal table.
  /// </remarks>
  /// <param>
  ///   @PrincipalId int = null,
  ///   @PrincipalCode nvarchar(60) = null,
  ///   @Principal nvarchar(510) = null,
  ///   @Email nvarchar(100) = null,
  ///   @FirstName nvarchar(100) = null,
  ///   @LastName nvarchar(100) = null,
  ///   @Address1 nvarchar(2000) = null,
  ///   @Address2 nvarchar(2000) = null,
  ///   @Address3 nvarchar(2000) = null,
  ///   @Address4 nvarchar(2000) = null,
  ///   @Address5 nvarchar(2000) = null,
  ///   @Address6 nvarchar(2000) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Principal_Update
(
 @PrincipalId int = null,
 @PrincipalCode nvarchar(60) = null,
 @Principal nvarchar(510) = null,
 @Email nvarchar(100) = null,
 @FirstName nvarchar(100) = null,
 @LastName nvarchar(100) = null,
 @Address1 nvarchar(2000) = null,
 @Address2 nvarchar(2000) = null,
 @Address3 nvarchar(2000) = null,
 @Address4 nvarchar(2000) = null,
 @Address5 nvarchar(2000) = null,
 @Address6 nvarchar(2000) = null 
)
 
as
begin
	 set nocount on;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @PrincipalCode = '-1'
    set @PrincipalCode = null;
  
  if @Principal = '-1'
    set @Principal = null;
  
	 declare @Error int
 
  update Principal
     set PrincipalCode = isnull(@PrincipalCode, PrincipalCode),
         Principal = isnull(@Principal, Principal),
         Email = isnull(@Email, Email),
         FirstName = isnull(@FirstName, FirstName),
         LastName = isnull(@LastName, LastName),
         Address1 = isnull(@Address1, Address1),
         Address2 = isnull(@Address2, Address2),
         Address3 = isnull(@Address3, Address3),
         Address4 = isnull(@Address4, Address4),
         Address5 = isnull(@Address5, Address5),
         Address6 = isnull(@Address6, Address6) 
   where PrincipalId = @PrincipalId
  
  select @Error = @@Error
  
  
  return @Error
  
end
