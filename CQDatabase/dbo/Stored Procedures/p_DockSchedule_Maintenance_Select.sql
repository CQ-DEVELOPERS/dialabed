﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DockSchedule_Maintenance_Select
  ///   Filename       : p_DockSchedule_Maintenance_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DockSchedule_Maintenance_Select
(
 @LocationId                      int = null
,@WarehouseId                     int = null
,@OperatorId                      int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @ExternalCompanyId int
	 
	 select @ExternalCompanyId = ExternalCompanyId
	   from Operator (nolock)
	  where OperatorId = @OperatorId
	 
	 if @LocationId = -1
	   set @LocationId = null
  
  select ds.DockScheduleId,
         ds.OutboundShipmentId,
         ds.IssueId,
         ds.InboundShipmentId,
         ds.ReceiptId,
         case when id.ExternalCompanyId = isnull(@ExternalCompanyId, id.ExternalCompanyId)
              then isnull(Subject + ' (' + convert(nvarchar(10), datediff(mi, ds.PlannedStart, ds.PlannedEnd)) + ' minutes) ' + ds.Description ,Subject)
              else 'Slot Already Taken'
              end 'Subject',
         ds.PlannedStart,
         ds.PlannedEnd,
         ds.ActualStart,
         ds.ActualEnd,
         isnull(ds.LocationId, -1) as 'LocationId',
         ds.RecurrenceRule,
         ds.RecurrenceParentID,
         Description,
         case when id.ExternalCompanyId = isnull(@ExternalCompanyId, id.ExternalCompanyId)
              then ds.CssClass
              else 'rsCategoryGray'
              end 'CssClass'
    from DockSchedule ds (nolock)
    left
    join Receipt r (nolock) on ds.ReceiptId = r.ReceiptId
    left
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    left
    join Location l (nolock) on ds.LocationId = l.LocationId
   where (ds.LocationId = @LocationId or @LocationId is null)
     and ds.PlannedStart > '2014-09-22'
  order by LocationId
end
 
 
