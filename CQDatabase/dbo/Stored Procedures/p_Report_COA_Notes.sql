﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_COA_Notes
  ///   Filename       : p_Report_COA_Notes.sql
  ///   Create By      : Karen
  ///   Date Created   : October 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_COA_Notes
(
 @COAId		int
)
 
as
begin
	 set nocount on;

  select	n.Note,
			n.notecode
    from	COADetail coad (nolock)
    join    Note n (nolock) on coad.NoteId = n.NoteId
    where   coad.COAId = @COAId
 
end
