﻿
/*  
  /// <summary>  
  ///   Procedure Name : p_interface_xml_PO_Export_Insert  
  ///   Filename       : p_interface_xml_PO_Export_Insert.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 24 Oct 2008  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    : Daniel Schotter  
  ///   Modified Date  : 2018/04/30
  ///   Details        : CQPMO-3519: Not all PO lines were inserting due to mismatch between Import Line details and inboundline details. Added MAX check to get the latest interfaceimportheaderid
  /// </newpara>  
*/  
CREATE procedure dbo.p_interface_xml_PO_Export_Insert            
(            
 @ReceiptId int            
)            
             
as            
begin            
  set nocount on;            
              
  declare @Error                    int,      
          @Errormsg                 varchar(500),      
          @GetDate                  datetime,      
          @OrderNumber              varchar(30),      
          @InboundDocumentId        int,      
          @InboundDocumentTypeCode  varchar(50),      
          @Delivery                 int,      
          @InterfaceExportHeaderId  int,      
          @InterfaceImportHeaderId  int,      
          @rowcount                 int,      
          @Interfaced               bit,  
          @OrderStatus              varchar(20),  
          @PrincipalCode            nvarchar(30)  
              
  select @GetDate = dbo.ufn_Getdate()            
              
  select @OrderNumber       = id.OrderNumber,      
         @InboundDocumentId = id.InboundDocumentId,      
         @Interfaced        = isnull(r.Interfaced,0),      
         @Delivery          = r.Delivery,          
         @InboundDocumentTypeCode = t.InboundDocumentTypeCode,  
         @PrincipalCode     = p.PrincipalCode  
    from InboundDocument    id  (nolock)          
    Join InboundDocumentType t on t.InboundDocumentTypeId = id.InboundDocumentTypeId           
    join Receipt             r on id.InboundDocumentId = r.InboundDocumentId            
    left  
    join Principal           p on id.PrincipalId = p.PrincipalId  
   where r.ReceiptId = @ReceiptId  
   
  select @OrderStatus =  
    CASE WHEN MAX(ReceiptId) = @ReceiptId   
         THEN 'Complete'   
         ELSE 'Partial'   
    END  
  from InboundDocument id (nolock)  
  inner join Receipt r ON id.InboundDocumentId = r.InboundDocumentId  
  where id.InboundDocumentId = @InboundDocumentId  
  --If @InboundDocumentTypeCode = 'PRV'          
  --begin          
  --  Exec p_interface_xml_Prod_Export_Insert @ReceiptId = @ReceiptId          
  --  Goto Finished          
  --end      
        
  --select @InterfaceImportHeaderId = InterfaceImportHeaderId  
  --  from InterfaceImportHeader (nolock)            
  -- where OrderNumber   = @OrderNumber            
  --   and RecordStatus != 'E'  
  --   and (PrincipalCode = @PrincipalCode or PrincipalCode is null)  
  
  select top 1 @InterfaceImportHeaderId = max(InterfaceImportHeaderId) --CQPMO-3519
    from InterfaceImportHeader (nolock) ih  
   join Warehouse w (nolock) on ih.ToWarehouseCode = w.WarehouseCode  
   join InboundDocument id (nolock) on ih.OrderNumber = id.OrderNumber
   join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
   and RecordStatus = 'C'  
     and (PrincipalCode = @PrincipalCode or PrincipalCode is null)       
     and r.ReceiptId = @ReceiptId
     and r.WarehouseId = w.WarehouseId   
  
  select @InterfaceImportHeaderId as InterfaceImportHeaderId  
              
  if @Interfaced = 0            
  begin            
    begin transaction            
                
    update Receipt            
       set Interfaced = 1            
     where ReceiptId = @ReceiptId            
                
    select @Error = @@Error            
                
    if @Error <> 0            
      goto error            
 If Left(@InboundDocumentTypeCode,2) in ('CR','MO')          
  begin       
  insert InterfaceExportHeader            
     (PrimaryKey,      
      OrderNumber,      
      RecordType,      
      RecordStatus,      
      CompanyCode,      
      Company,      
      Address,      
      FromWarehouseCode,      
      DeliveryNoteNumber,      
      ContainerNumber,      
      SealNumber,      
      DeliveryDate,      
      Remarks,  
      OrderStatus)            
  select distinct     
    i.OrderNumber,    
    i.OrderNumber,   
    @InboundDocumentTypeCode,    
         'N',      
       e.ExternalCompanyCode,    
    e.ExternalCompany,      
    '',    
       w.WarehouseCode,      
       r.DeliveryNoteNumber,      
       Null,    
       r.SealNumber,    
       r.DeliveryDate,      
       r.Remarks,  
       @OrderStatus  
    from Receipt                 r (nolock)           
   Join Warehouse w (nolock) on w.WarehouseId = r.WarehouseId    
   Join InboundDocument i (nolock) on i.InboundDocumentId = r.InboundDocumentId    
   Join ExternalCompany e (nolock) on e.ExternalCompanyId = i.ExternalCompanyId    
   where r.ReceiptId               = @ReceiptId     
   
  select @InterfaceExportHeaderId = scope_identity(), @rowcount = @@rowcount          
               
  if @rowcount > 0      
    insert InterfaceExportDetail      
    (InterfaceExportHeaderId,      
     ForeignKey,      
     LineNumber,      
     ProductCode,      
     Product,      
     SKUCode,      
     Batch,    
     ExpiryDate,  
     ReasonCode,      
     Quantity,      
     RejectQuantity,      
     ReceivedQuantity,      
     AcceptedQuantity,      
     DeliveryNoteQuantity,  
     Additional4)            
    select @InterfaceExportHeaderId,      
     i.OrderNumber,      
     il.LineNumber,      
     p.ProductCode,      
     p.Product,      
     null,    
     b.Batch,      
     b.ExpiryDate,  
     r.ReasonCode,    
     sum(rl.AcceptedQuantity),      
     sum(rl.RejectQuantity),      
     sum(rl.ReceivedQuantity),      
     sum(rl.AcceptedQuantity),      
     sum(rl.DeliveryNoteQuantity) ,  
     rc.DeliveryNoteNumber     
   from InboundLine          il  (nolock)     
    join ReceiptLine      rl  (nolock) on il.InboundLineId   = rl.InboundLineId      
    left join Reason       r  (nolock) on il.ReasonId    = r.ReasonId      
    Join Receipt    rc  (nolock) on rc.ReceiptId    = rl.ReceiptId    
    Join InboundDocument   i  (nolock) on i.InboundDocumentId  = rc.InboundDocumentId    
    Join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId    
    Join Batch      b  (nolock) on b.BatchId     = sub.BatchId    
    Join StorageUnit    su (nolock) on su.StorageUnitId   = sub.StorageUnitId    
    JOin Product     p  (nolock) on p.ProductId    = su.ProductId    
     where rl.ReceiptId  = @ReceiptId      
    and rl.AcceptedQuantity > 0      
    Group by  i.OrderNumber,      
     il.LineNumber,      
     p.ProductCode,      
     p.Product,      
     b.Batch,      
     b.ExpiryDate,  
     r.ReasonCode ,  
     rc.DeliveryNoteNumber   
         
  if @Error <> 0            
    goto error      
      
    exec @Error = p_Export_Kitting_SA  
  @ReceiptId = @ReceiptId  
          
  end     
  else    
  begin                
  insert InterfaceExportHeader            
     (PrimaryKey,      
      OrderNumber,      
      RecordType,      
      RecordStatus,      
      CompanyCode,      
      Company,      
      Address,      
      FromWarehouseCode,      
      ToWarehouseCode,      
      DeliveryNoteNumber,      
      ContainerNumber,      
      SealNumber,      
      DeliveryDate,      
      Remarks,      
      NumberOfLines,      
      Additional1,      
      Additional2,      
      Additional3,      
      Additional4,      
      Additional5,      
      ProcessedDate,  
      PrincipalCode,  
      OrderStatus)  
  select distinct h.PrimaryKey,      
      h.OrderNumber,-- + '_' + convert(varchar(10), isnull(@Delivery,'')),      
      h.RecordType,      
      'N',      
      h.CompanyCode,      
      h.Company,      
      h.Address,      
      h.FromWarehouseCode,      
      h.ToWarehouseCode,      
      r.DeliveryNoteNumber,      
      h.ContainerNumber,      
      h.SealNumber,      
      h.DeliveryDate,      
      r.Remarks,      
      h.NumberOfLines,      
     h.Additional1,      
      h.Additional2,      
      h.Additional3,      
      h.Additional4,      
      h.Additional5,      
      null,  
      h.PrincipalCode,  
      @OrderStatus  
    from InterfaceImportHeader h (nolock),      
      Receipt                 r (nolock)           
   where h.InterfaceImportHeaderId = @InterfaceImportHeaderId            
     and r.ReceiptId               = @ReceiptId            
                 
  select @InterfaceExportHeaderId = scope_identity(), @rowcount = @@rowcount
     
           
  if @InterfaceImportHeaderId is not null      
  Begin      
  update export      
    set Additional3       = isnull(import.Additional3,export.Additional3)      
   from InterfaceImportHeader import (nolock),      
     InterfaceExportHeader export (nolock)      
     where InterfaceImportHeaderId = @InterfaceImportHeaderId      
    and InterfaceExportHeaderId = @InterfaceExportHeaderId      
  end      
           
  if @rowcount > 0      
    insert InterfaceExportDetail      
    (InterfaceExportHeaderId,      
     ForeignKey,      
     LineNumber,      
     ProductCode,      
     Product,      
     SKUCode,      
     Batch,      
     ExpiryDate,  
     Quantity,      
     RejectQuantity,      
     ReceivedQuantity,      
     AcceptedQuantity,      
     DeliveryNoteQuantity,      
     ReasonCode,      
     Weight,      
     Additional1,      
     Additional2,      
     Additional3,      
     Additional4,      
     Additional5)            
    select @InterfaceExportHeaderId,  
     d.ForeignKey,      
     il.LineNumber,      
     p.ProductCode,      
     p.Product,      
     sku.SKUCode,      
     null,      
     null,  
     sum(rl.AcceptedQuantity),      
     sum(rl.RejectQuantity),      
     sum(rl.ReceivedQuantity),      
     sum(rl.AcceptedQuantity),      
     sum(rl.DeliveryNoteQuantity),      
     r.ReasonCode,      
     d.Weight,      
     d.Additional1,      
     d.Additional2,    --max(ExceptionCode),      
     d.Additional3,      
     rc.DeliveryNoteNumber,      
     d.Additional5            
   from InterfaceImportHeader h (nolock)      
   join InterfaceImportDetail d (nolock) on h.InterfaceImportHeaderId = d.InterfaceImportHeaderId      
   join InboundLine          il (nolock) on d.LineNumber              = il.LineNumber      
   join ReceiptLine          rl (nolock) on il.InboundLineId          = rl.InboundLineId     
   join Receipt              rc (nolock) on rl.ReceiptId     = rc.ReceiptId   
   join StorageUnitBatch    sub (nolock) on rl.StorageUnitBatchId     = sub.StorageUnitBatchId   
   join StorageUnit          su (nolock) on sub.StorageUnitId         = su.StorageUnitId  
   join Product               p (nolock) on su.ProductId              = p.ProductId  
   join SKU                 sku (nolock) on su.SKUId                  = sku.SKUId  
                                         and d.ProductCode            = p.ProductCode  
                                         and d.SKUCode                = sku.SKUCode  
   join Batch                 b (nolock) on sub.BatchId               = b.BatchId    
   left      
   join Reason                r (nolock) on il.ReasonId               = r.ReasonId      
   --left       
   --join Exception             e (nolock) on e.ReceiptLineId = rl.ReceiptLineId  
     where h.InterfaceImportHeaderId = @InterfaceImportHeaderId      
    and rl.ReceiptId  = @ReceiptId      
    --and not exists(select 1 from InterfaceExportDetail x where d.ForeignKey = x.ForeignKey and d.LineNumber = x.LineNumber)      
    and rl.AcceptedQuantity > 0      
  group by d.ForeignKey,      
     il.LineNumber,      
     p.ProductCode,      
     p.Product,      
     sku.SKUCode,      
     --b.Batch,      
     --b.ExpiryDate,  
     r.ReasonCode,      
     d.Weight,      
     d.Additional1,      
     d.Additional2,  
   d.Additional3,      
     d.Additional4,      
     d.Additional5,  
     rc.DeliveryNoteNumber      
           
  if @Error <> 0            
    goto error            
     end     
CommitTransaction:  
    commit transaction      
  end      
        
Finished:      
    return      
        
  error:      
    raiserror 900000 'Error executing p_interface_xml_PO_Export_Insert'      
    rollback transaction      
    return @Error      
end   
   
   
   
