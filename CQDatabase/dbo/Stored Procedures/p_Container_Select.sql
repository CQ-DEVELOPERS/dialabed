﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Select
  ///   Filename       : p_Container_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 11:46:28
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Container table.
  /// </remarks>
  /// <param>
  ///   @ContainerId int = null 
  /// </param>
  /// <returns>
  ///   Container.ContainerId,
  ///   Container.PalletId,
  ///   Container.JobId,
  ///   Container.ReferenceNumber,
  ///   Container.StorageUnitBatchId,
  ///   Container.PickLocationId,
  ///   Container.StoreLocationId,
  ///   Container.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Select
(
 @ContainerId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Container.ContainerId
        ,Container.PalletId
        ,Container.JobId
        ,Container.ReferenceNumber
        ,Container.StorageUnitBatchId
        ,Container.PickLocationId
        ,Container.StoreLocationId
        ,Container.Quantity
    from Container
   where isnull(Container.ContainerId,'0')  = isnull(@ContainerId, isnull(Container.ContainerId,'0'))
  
end
