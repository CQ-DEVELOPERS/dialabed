﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Replenishment_Waiting
  ///   Filename       : p_Dashboard_Replenishment_Waiting.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Replenishment_Waiting
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select Area,
           Value as 'Count',
           KPI
      from DashboardReplenishmentWaiting (nolock)
     where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardReplenishmentWaiting
	   
	   insert DashboardReplenishmentWaiting
	         (WarehouseId,
	          Area,
           Value,
           KPI)
    select a.WarehouseId,
           a.Area,
	          count(distinct(i.JobId)) as 'Count',
	          10 as 'KPI'
	     from Instruction      i (nolock)
	     join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
	     join Status           s (nolock) on i.StatusId          = s.StatusId
	     join AreaLocation    al (nolock) on i.PickLocationId    = al.LocationId 
	     join Area             a (nolock) on al.AreaId           = a.AreaId
	    where i.WarehouseId           = isnull(@WarehouseId, i.WarehouseId)
	      and it.InstructionTypeCode in ('R')
	      and s.StatusCode           in ('W','S')
	   group by a.WarehouseId,
	            a.Area
	 end
end
