﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ShipmentIssue_Search
  ///   Filename       : p_ShipmentIssue_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:52
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null output,
  ///   @IssueId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ShipmentIssue.ShipmentId,
  ///   ShipmentIssue.IssueId,
  ///   ShipmentIssue.DropSequence 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ShipmentIssue_Search
(
 @ShipmentId int = null output,
 @IssueId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ShipmentId = '-1'
    set @ShipmentId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
 
  select
         ShipmentIssue.ShipmentId
        ,ShipmentIssue.IssueId
        ,ShipmentIssue.DropSequence
    from ShipmentIssue
   where isnull(ShipmentIssue.ShipmentId,'0')  = isnull(@ShipmentId, isnull(ShipmentIssue.ShipmentId,'0'))
     and isnull(ShipmentIssue.IssueId,'0')  = isnull(@IssueId, isnull(ShipmentIssue.IssueId,'0'))
  
end
