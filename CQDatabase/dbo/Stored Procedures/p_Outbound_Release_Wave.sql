﻿   
/*  
  /// <summary>  
  ///   Procedure Name : p_Outbound_Release_Wave  
  ///   Filename       : p_Outbound_Release_Wave.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 16 Oct 2008  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Outbound_Release_Wave
(  
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @OperatorId         int = null  
)  
   
as  
begin  
  set nocount on;  
    
  declare @TableStock as table
  (
   OrderNumber            nvarchar(30),
   WarehouseId            int,
   AreaType               nvarchar(10),
   StorageUnitId          int,
   LineNumber             int identity,
   Quantity               float
  )
  
  declare @Error                  int,
          @Errormsg               nvarchar(500),
          @GetDate                datetime,
          @WarehouseId            int,
          @AreaType               nvarchar(10),
          @PriorityId             int,
          @OutboundDocumentId     int,
          @OrderNumber            nvarchar(30),
          @OutboundDocumentTypeId int,
          @ExternalCompanyId      int,
          @StatusId               int,
          @DeliveryDate           datetime,
          @StorageUnitId          int,
          @LineNumber             int,
          @Quantity               float
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @OutboundDocumentTypeId = OutboundDocumentTypeId,
         @PriorityId             = PriorityId
    from OutboundDocumentType (nolock)
   where OutboundDocumentTypeCode = 'WAV'
  
  select @StatusId = dbo.ufn_StatusId('OD','I')
  
  select @ExternalCompanyId = ExternalCompanyId
    from ExternalCompany (nolock)  
   where ExternalCompanyCode   = 'WAVE'
     and ExternalCompanyTypeId = 1
  
  if @ExternalCompanyId is null
  begin
    exec @Error = p_ExternalCompany_Insert
     @ExternalCompanyId         = @ExternalCompanyId output,
     @ExternalCompanyTypeId     = 1,
     @ExternalCompany           = 'Wave Pick',
     @ExternalCompanyCode       = 'WAVE'  
      
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
      goto error
    end
  end
  
  select @DeliveryDate = convert(varchar(10), @GetDate, 120)
  
  insert @TableStock
        (OrderNumber,
         WarehouseId,
         AreaType,
         StorageUnitId,
         Quantity)
  select case when osi.OutboundShipmentId is null
              then od.OrderNumber
              else convert(nvarchar(30), osi.OutboundShipmentId)
              end,
         i.WarehouseId,
         i.AreaType,
         ol.StorageUnitId,
         sum(ol.Quantity)
    from OutboundDocument od (nolock)
    join OutboundLine     ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId
    join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    left
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
   where isnull(osi.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(osi.OutboundShipmentId, -1))
     and i.IssueId                          = isnull(@IssueId, i.IssueId)
  group by case when osi.OutboundShipmentId is null
              then od.OrderNumber
              else convert(nvarchar(30), osi.OutboundShipmentId)
              end,
           i.WarehouseId,
           i.AreaType,
           ol.StorageUnitId
  
  select @OrderNumber       = OrderNumber,
         @WarehouseId       = WarehouseId
    from @TableStock
  
  exec @Error = p_OutboundDocument_Insert
   @OutboundDocumentId     = @OutboundDocumentId output,
   @OrderNumber            = @OrderNumber,
   @OutboundDocumentTypeId = @OutboundDocumentTypeId,
   @ExternalCompanyId      = @ExternalCompanyId,
   @WarehouseId            = @WarehouseId,
   @StatusId               = @StatusId,
   @DeliveryDate           = @DeliveryDate,
   @CreateDate             = @GetDate,
   @ModifiedDate           = null
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'
    goto error
  end
  
  while exists(select 1
                 from @TableStock)
  begin
    select @StorageUnitId = StorageUnitId,
           @LineNumber    = LineNumber,
           @Quantity      = Quantity
      from @TableStock
    order by LineNumber
    
    select @StorageUnitId, @LineNumber, @Quantity
    
    delete @TableStock
     where LineNumber = @LineNumber
    
    exec @Error = p_OutboundLine_Insert
     @OutboundDocumentId = @OutboundDocumentId,
     @StorageUnitId      = @StorageUnitId,
     @StatusId           = @StatusId,
     @LineNumber         = @LineNumber,
     @Quantity           = @Quantity,
     @BatchId            = null
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_OutboundLine_Insert'
      goto error
    end
  end
  
  exec @Error = p_Despatch_Create_Issue
   @OutboundDocumentId = @OutboundDocumentId,
   @OperatorId         = null,
   @Remarks            = null
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
    goto error
  end
  
  select @IssueId     = IssueId,
         @WarehouseId = WarehouseId
    from Issue (nolock)
   where OutboundDocumentId = @OutboundDocumentId
  
  exec @Error = p_Issue_Update
   @IssueId  = @IssueId,
   @AreaType = @AreaType
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error updating Issue'
    goto error
  end
  
  exec @Error = p_Outbound_Auto_Load
  @OutboundShipmentId = @OutboundShipmentId output,
  @IssueId            = @IssueId
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
    goto error
  end
  
  exec @Error = p_Outbound_Auto_Release
  @OutboundShipmentId = @OutboundShipmentId,
  @IssueId            = @IssueId
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_Outbound_Auto_Release'
    goto error
  end
  return
      
  error:    
    if @Error = 0    
      set @Error = -1    
        
    raiserror 900000 @ErrorMsg    
    rollback transaction    
    return @Error    
end
