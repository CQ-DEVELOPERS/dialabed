﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PrincipalHistory_Insert
  ///   Filename       : p_PrincipalHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 08:15:16
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PrincipalHistory table.
  /// </remarks>
  /// <param>
  ///   @PrincipalId int = null,
  ///   @PrincipalCode nvarchar(60) = null,
  ///   @Principal nvarchar(510) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @Email nvarchar(100) = null,
  ///   @FirstName nvarchar(100) = null,
  ///   @LastName nvarchar(100) = null,
  ///   @Address1 nvarchar(2000) = null,
  ///   @Address2 nvarchar(2000) = null,
  ///   @Address3 nvarchar(2000) = null,
  ///   @Address4 nvarchar(2000) = null,
  ///   @Address5 nvarchar(2000) = null,
  ///   @Address6 nvarchar(2000) = null 
  /// </param>
  /// <returns>
  ///   PrincipalHistory.PrincipalId,
  ///   PrincipalHistory.PrincipalCode,
  ///   PrincipalHistory.Principal,
  ///   PrincipalHistory.CommandType,
  ///   PrincipalHistory.InsertDate,
  ///   PrincipalHistory.Email,
  ///   PrincipalHistory.FirstName,
  ///   PrincipalHistory.LastName,
  ///   PrincipalHistory.Address1,
  ///   PrincipalHistory.Address2,
  ///   PrincipalHistory.Address3,
  ///   PrincipalHistory.Address4,
  ///   PrincipalHistory.Address5,
  ///   PrincipalHistory.Address6 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PrincipalHistory_Insert
(
 @PrincipalId int = null,
 @PrincipalCode nvarchar(60) = null,
 @Principal nvarchar(510) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @Email nvarchar(100) = null,
 @FirstName nvarchar(100) = null,
 @LastName nvarchar(100) = null,
 @Address1 nvarchar(2000) = null,
 @Address2 nvarchar(2000) = null,
 @Address3 nvarchar(2000) = null,
 @Address4 nvarchar(2000) = null,
 @Address5 nvarchar(2000) = null,
 @Address6 nvarchar(2000) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PrincipalHistory
        (PrincipalId,
         PrincipalCode,
         Principal,
         CommandType,
         InsertDate,
         Email,
         FirstName,
         LastName,
         Address1,
         Address2,
         Address3,
         Address4,
         Address5,
         Address6)
  select @PrincipalId,
         @PrincipalCode,
         @Principal,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @Email,
         @FirstName,
         @LastName,
         @Address1,
         @Address2,
         @Address3,
         @Address4,
         @Address5,
         @Address6 
  
  select @Error = @@Error
  
  
  return @Error
  
end
