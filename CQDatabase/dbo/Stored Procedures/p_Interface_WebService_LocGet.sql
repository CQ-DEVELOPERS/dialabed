﻿create procedure dbo.p_Interface_WebService_LocGet
(
	@doc nvarchar(max) output
)
--with encryption
as
begin
	 declare @ReferenceNumber     varchar(50),
	         @idoc                int,
	         @xml                 nvarchar(max),
	         @JobId               int,
	         @WarehouseId         int,
	         @StorageUnitId       int,
	         @ProductId           int,
	         @SKUId               int,
	         @BatchId             int,
	         @ProductCode         nvarchar(30),
	         @Product             nvarchar(255),
	         @SKUCode             nvarchar(20),
	         @Batch               nvarchar(50),
	         @ConfirmedQuantity   float,
	         @Barcode             nvarchar(50),
	         @Location            nvarchar(50),
	         @rowcount            int,
	         @AreaCode            nvarchar(10),
	         @Operator            nvarchar(50)
	 
	 declare @TableResult as table
	 (
	  JobId           int,
	  ReferenceNumber nvarchar(30)
	 )
	 
	 EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	 
	 SELECT @AreaCode = AreaCode,
	        @Operator = Operator
		 FROM  OPENXML (@idoc, '/root/Header',1)
            WITH (AreaCode nvarchar(10) 'AreaCode',
                  Operator nvarchar(50) 'Operator')
  
  select @WarehouseId = WarehouseId
    from Operator (nolock)
   where Operator = @Operator
  
  if @WarehouseId is null
    set @WarehouseId = 1
  
  IF (ISNULL(@AreaCode, '-1') = '-1')
    SELECT @XML = '<?xml version="1.0" encoding="utf-16"?><root>Location not found</root>'
  ELSE
  BEGIN
    
    SELECT @XML = '<?xml version="1.0" encoding="utf-16"?>' +
    (select
        (SELECT
             l.LocationId  As 'LocationId'
            ,l.Location    As 'Location'
          from Location      l (nolock)
          join AreaLocation al (nolock) on l.LocationId = al.LocationId
          join Area          a (nolock) on al.AreaId    = a.AreaId
         where a.WarehouseId = @WarehouseId
           and a.AreaCode    = @AreaCode
           FOR XML PATH('Locations'), TYPE)
        FOR XML PATH('root'))
      
        
    END
  set @doc = @xml
end
