﻿
/*
  /// <summary>
  ///   Procedure Name : p_Get_ExternalCompanyType
  ///   Filename       : p_Get_ExternalCompanyType.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Get_ExternalCompanyType
(
 @ExternalCompanyTypeId        int
)
 
as
begin
	 set nocount on;
  
  select ExternalCompanyTypeId,
         ExternalCompanyType,
         ExternalCompanyTypeCode
    from ExternalCompanyType 
end
