﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_DetailsByLocation
  ///   Filename       : p_Report_Stock_Take_DetailsByLocation.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Report_Stock_Take_DetailsByLocation]
(
 @StockTakeReferenceId 		int,
 @WarehouseId				int,
 @FromDate					datetime,
 @ToDate					datetime,
 @AreaId					Int = null,
 @Area						nvarchar(50),
 @InstructionTypeCode		nvarchar(10)
)
as
begin
	 set nocount on; 

if (@AreaId = 0) or (@AreaId = -1)
Begin
 set @AreaId = null
end
	
Declare @TableResult as  Table 
		(StockTakeReferenceId		int,
		 JobId						int,
		 InstructionTypeId			int,
		 InstructionType			nvarchar(50),
		 InstructionTypeCode		nvarchar(25),
		 Area						nvarchar(50),
		 AreaId						int,
		 StorageUnitBatchId			int,
		 PickLocationId				int,
		 PickLocation				nvarchar(15),
         StoreLocationId			Int,
         StoreLocation				nvarchar(15),
		 ProductCode				Int,
		 Product					nvarchar(50),		
		 BatchId					int,
		 SKUId						int,
		 SKU						nvarchar(50),
		 Quantity					float,
		 ConfirmedQuantity			float,
		 Variance					float,
		 Weight						float,
		 CreateDate					datetime,
		 StartDate					datetime,
		 CountedById				int,
		 CountedBy					nvarchar(50),
		 AuthorisedById				int,
		 AuthorisedBy				nvarchar(50))

insert @TableResult
        (StockTakeReferenceId,
         JobId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         CreateDate,
         StartDate,
         CountedById,
		 AuthorisedById)
  SELECT    @StockTakeReferenceId,
		    i.JobId, 
            it.InstructionType, 
            i.StorageUnitBatchId, 
            i.PickLocationId, 
            i.StoreLocationId, 
            i.Quantity,
            i.ConfirmedQuantity,
            i.NettWeight, 
            i.CreateDate, 
            i.StartDate,
            j.OperatorId,
            j.CheckedBy
FROM        Instruction AS i WITH (nolock) 
INNER JOIN  InstructionType AS it WITH (nolock) ON i.InstructionTypeId = it.InstructionTypeId
join		Job                 j (nolock) on j.JobId = i.JobId
join		StockTakeReferenceJob strj (nolock) on strj.StockTakeReferenceId = @StockTakeReferenceId
join        view_Instruction_Stock_Ref isr on isr.StockTakeReferenceId = strj.StockTakeReferenceId
WHERE       it.InstructionTypeCode = @InstructionTypeCode
     and i.CreateDate   between @FromDate and @ToDate
	 and i.WarehouseId = Isnull(@WarehouseId,i.WarehouseId)
	 and i.JobId = strj.JobId
	 and isr.Areaid = @AreaId
	 and isr.PicklocationId = i.PickLocationId
  
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
    
  update tr
     set CountedBy = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.CountedById = o.OperatorId
    
  update tr
     set AuthorisedBy = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.AuthorisedById = o.OperatorId
    
  update tr
     set tr.Variance =  isnull(tr.ConfirmedQuantity,0) - isnull(tr.Quantity,0)
    from @TableResult tr
    


  select distinct tr.StockTakeReferenceId,
		 tr.Jobid,
         tr.InstructionType,
         tr.Area,
         tr.PickLocation,
         tr.StoreLocation,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         tr.Quantity,
         tr.ConfirmedQuantity,
         tr.Weight,
         tr.Variance,
         tr.CreateDate,
         tr.StartDate,
         tr.CountedBy,
         tr.AuthorisedBy,
         tr.BatchId
    from @TableResult    tr
    join viewStock       vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
  order by tr.CreateDate
  
end
