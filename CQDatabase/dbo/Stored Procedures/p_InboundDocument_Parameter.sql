﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Parameter
  ///   Filename       : p_InboundDocument_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2013 14:47:31
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InboundDocument.InboundDocumentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as InboundDocumentId
        ,null as 'InboundDocument'
  union
  select
         InboundDocument.InboundDocumentId
        ,InboundDocument.InboundDocumentId as 'InboundDocument'
    from InboundDocument
  
end
