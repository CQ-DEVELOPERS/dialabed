﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Search
  ///   Filename       : p_OutboundDocument_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 07:31:31
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null output,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @PrincipalId int = null,
  ///   @OperatorId int = null,
  ///   @DirectDeliveryCustomerId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OutboundDocument.OutboundDocumentId,
  ///   OutboundDocument.OutboundDocumentTypeId,
  ///   OutboundDocument.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompany,
  ///   OutboundDocument.StatusId,
  ///   Status.Status,
  ///   OutboundDocument.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   OutboundDocument.OrderNumber,
  ///   OutboundDocument.DeliveryDate,
  ///   OutboundDocument.CreateDate,
  ///   OutboundDocument.ModifiedDate,
  ///   OutboundDocument.ReferenceNumber,
  ///   OutboundDocument.FromLocation,
  ///   OutboundDocument.ToLocation,
  ///   OutboundDocument.StorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   OutboundDocument.BatchId,
  ///   Batch.Batch,
  ///   OutboundDocument.Quantity,
  ///   OutboundDocument.PrincipalId,
  ///   Principal.Principal,
  ///   OutboundDocument.OperatorId,
  ///   Operator.Operator,
  ///   OutboundDocument.ExternalOrderNumber,
  ///   OutboundDocument.DirectDeliveryCustomerId,
  ///   ExternalCompany.ExternalCompany,
  ///   OutboundDocument.EmployeeCode,
  ///   OutboundDocument.Collector,
  ///   OutboundDocument.AdditionalText1,
  ///   OutboundDocument.VendorCode,
  ///   OutboundDocument.CustomerOrderNumber,
  ///   OutboundDocument.AdditionalText2,
  ///   OutboundDocument.BOE,
  ///   OutboundDocument.Address1 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Search
(
 @OutboundDocumentId int = null output,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @PrincipalId int = null,
 @OperatorId int = null,
 @DirectDeliveryCustomerId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @DirectDeliveryCustomerId = '-1'
    set @DirectDeliveryCustomerId = null;
  
 
  select
         OutboundDocument.OutboundDocumentId
        ,OutboundDocument.OutboundDocumentTypeId
        ,OutboundDocument.ExternalCompanyId
         ,ExternalCompanyExternalCompanyId.ExternalCompany as 'ExternalCompany'
        ,OutboundDocument.StatusId
         ,StatusStatusId.Status as 'Status'
        ,OutboundDocument.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,OutboundDocument.OrderNumber
        ,OutboundDocument.DeliveryDate
        ,OutboundDocument.CreateDate
        ,OutboundDocument.ModifiedDate
        ,OutboundDocument.ReferenceNumber
        ,OutboundDocument.FromLocation
        ,OutboundDocument.ToLocation
        ,OutboundDocument.StorageUnitId
        ,OutboundDocument.BatchId
         ,BatchBatchId.Batch as 'Batch'
        ,OutboundDocument.Quantity
        ,OutboundDocument.PrincipalId
         ,PrincipalPrincipalId.Principal as 'Principal'
        ,OutboundDocument.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,OutboundDocument.ExternalOrderNumber
        ,OutboundDocument.DirectDeliveryCustomerId
         ,ExternalCompanyDirectDeliveryCustomerId.ExternalCompany as 'ExternalCompanyDirectDeliveryCustomerId'
        ,OutboundDocument.EmployeeCode
        ,OutboundDocument.Collector
        ,OutboundDocument.AdditionalText1
        ,OutboundDocument.VendorCode
        ,OutboundDocument.CustomerOrderNumber
        ,OutboundDocument.AdditionalText2
        ,OutboundDocument.BOE
        ,OutboundDocument.Address1
    from OutboundDocument
    left
    join ExternalCompany ExternalCompanyExternalCompanyId on ExternalCompanyExternalCompanyId.ExternalCompanyId = OutboundDocument.ExternalCompanyId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = OutboundDocument.StatusId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = OutboundDocument.WarehouseId
    left
    join StorageUnit StorageUnitStorageUnitId on StorageUnitStorageUnitId.StorageUnitId = OutboundDocument.StorageUnitId
    left
    join Batch BatchBatchId on BatchBatchId.BatchId = OutboundDocument.BatchId
    left
    join Principal PrincipalPrincipalId on PrincipalPrincipalId.PrincipalId = OutboundDocument.PrincipalId
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = OutboundDocument.OperatorId
    left
    join ExternalCompany ExternalCompanyDirectDeliveryCustomerId on ExternalCompanyDirectDeliveryCustomerId.ExternalCompanyId = OutboundDocument.DirectDeliveryCustomerId
   where isnull(OutboundDocument.OutboundDocumentId,'0')  = isnull(@OutboundDocumentId, isnull(OutboundDocument.OutboundDocumentId,'0'))
     and isnull(OutboundDocument.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(OutboundDocument.ExternalCompanyId,'0'))
     and isnull(OutboundDocument.StatusId,'0')  = isnull(@StatusId, isnull(OutboundDocument.StatusId,'0'))
     and isnull(OutboundDocument.WarehouseId,'0')  = isnull(@WarehouseId, isnull(OutboundDocument.WarehouseId,'0'))
     and isnull(OutboundDocument.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(OutboundDocument.StorageUnitId,'0'))
     and isnull(OutboundDocument.BatchId,'0')  = isnull(@BatchId, isnull(OutboundDocument.BatchId,'0'))
     and isnull(OutboundDocument.PrincipalId,'0')  = isnull(@PrincipalId, isnull(OutboundDocument.PrincipalId,'0'))
     and isnull(OutboundDocument.OperatorId,'0')  = isnull(@OperatorId, isnull(OutboundDocument.OperatorId,'0'))
     and isnull(OutboundDocument.DirectDeliveryCustomerId,'0')  = isnull(@DirectDeliveryCustomerId, isnull(OutboundDocument.DirectDeliveryCustomerId,'0'))
  order by OrderNumber
  
end
