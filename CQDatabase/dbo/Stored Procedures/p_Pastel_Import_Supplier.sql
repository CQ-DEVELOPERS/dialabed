﻿
/*
  /// <summary>
  ///   Procedure Name : p_Pastel_Import_Supplier
  ///   Filename       : p_Pastel_Import_Supplier.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 17 September 2010
  ///   Details        : create error message detail in InterfaceDashboardBulkLog
  /// </newpara>
*/
CREATE procedure p_Pastel_Import_Supplier
 
as
begin
	 set nocount on;
  
  declare @Error               int,
		        @Error2			           int,
          @Errormsg            varchar(500),
          @GetDate             datetime,
          @SupplierCode        varchar(30),
          @SupplierName        varchar(255),
          @Address1            varchar(255),
          @Address2            varchar(255),
          @Address3            varchar(255),
          @Address4            varchar(255),
          @Modified            varchar(10) ,
          @ContactPerson       varchar(100),
          @Phone               varchar(255),
          @Fax                 varchar(255),
          @Email               varchar(255),
          @ExternalCompanyId   int,
          @AddressId           int,
          @ContactListId       int,
          @HostId              varchar(30),
          -- InterfaceDashboardBulkLog  (Karen)
          @FileKeyId                    INT,
		  @FileType                     VARCHAR(100),
	      @ProcessedDate                DATETIME,
	      @FileName                     VARCHAR(50) ,
	      @ErrorCode                    CHAR (5),
	      @ErrorDescription             VARCHAR (255),
       @PrincipalCode                nvarchar(30),
       @PrincipalId                  int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  update InterfaceSupplier
     set ProcessedDate = @Getdate
   where RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare Supplier_cursor cursor for
   select SupplierCode,
          SupplierName,
          Address1,
          Address2,
          Address3,
          Address4,
          Modified,
          ContactPerson,
          Phone,
          Fax,
          Email,
          HostId,
          PrincipalCode
  	  from	InterfaceSupplier
    where ProcessedDate = @Getdate
	  order by	SupplierCode
  
  open Supplier_cursor
  
  fetch Supplier_cursor into @SupplierCode,
                             @SupplierName,
                             @Address1,
                             @Address2,
                             @Address3,
                             @Address4,
                             @Modified,
                             @ContactPerson,
                             @Phone,
                             @Fax,
                             @Email,
                             @HostId,
                             @PrincipalCode

  while (@@fetch_status = 0)
  begin
    begin transaction
    
    set @PrincipalId = null
           
    select @PrincipalId = PrincipalId
      from Principal (nolock)
     where PrincipalCode = @PrincipalCode
    
    set @ExternalCompanyId = null
    
    select @ExternalCompanyId = ExternalCompanyId
      from ExternalCompany (nolock)
     where ExternalCompanyCode = @SupplierCode
       and ExternalCompanyTypeId = 2
       and PrincipalId = @PrincipalId
    
    if @ExternalCompanyId is null
      select @ExternalCompanyId = ExternalCompanyId
        from ExternalCompany (nolock)
       where ExternalCompanyCode = @SupplierCode
         and ExternalCompanyTypeId = 2
         and PrincipalId is null
    
    if @ExternalCompanyId is null
    begin
      exec @Error = p_ExternalCompany_Insert
       @ExternalCompanyId         = @ExternalCompanyId output,
       @ExternalCompanyTypeId     = 2,
       @ExternalCompany           = @SupplierName,
       @ExternalCompanyCode       = @SupplierCode,
       @PrincipalId               = @PrincipalId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
        
        
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error inserting to ExternalCompany - ExternalCompanyId is blank'
								,@SupplierCode
								,'Supplier'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
					
        goto error
      end
    end
    else
    begin
      exec @Error = p_ExternalCompany_Update
       @ExternalCompanyId         = @ExternalCompanyId,
       @ExternalCompanyTypeId     = 2,
       @ExternalCompany           = @SupplierName,
       @ExternalCompanyCode       = @SupplierCode,
       @PrincipalId               = @PrincipalId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
        
      
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error updating ExternalCompany'
								,@SupplierCode
								,'Supplier'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
        goto error
      end
    end
    
    update ExternalCompany
       set HostId = @HostId
     where ExternalCompanyId = @ExternalCompanyId
    
    select @AddressId = AddressId
      from Address
     where ExternalCompanyId = @ExternalCompanyId
    
    if @AddressId is null
    begin
      exec @Error = p_Address_Insert
       @AddressId         = @AddressId output,
       @ExternalCompanyId = @ExternalCompanyId,
       @Street            = @Address1,
       @Suburb            = @Address2,
       @Town              = @Address3,
       @Country           = @Address4
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Address_Insert'
        
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error inserting Address - AddressId is blank'
								,@SupplierCode
								,'Supplier'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
					
        goto error
      end
    end
    else
    begin
      exec @Error = p_Address_Update
       @AddressId         = @AddressId,
       @ExternalCompanyId = @ExternalCompanyId,
       @Street            = @Address1,
       @Suburb            = @Address2,
       @Town              = @Address3,
       @Country           = @Address4
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
        
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error updating Address'
								,@SupplierCode
								,'InterfaceSupplier'
								,'E'
								,NULL
								,@GetDate
								,@GetDate) 
					
        goto error
      end
    end
    
    select @ContactListId = ContactListId
      from ContactList
     where ExternalCompanyId = @ExternalCompanyId
       and ContactPerson = @ContactPerson
    
    if @ContactListId is null
    begin
      exec @Error = p_ContactList_Insert
       @ContactListId     = @ContactListId output,
       @ExternalCompanyId = @ExternalCompanyId,
       @OperatorId        = null,
       @ContactPerson     = @ContactPerson,
       @Telephone         = @Phone,
       @Fax               = @Fax,
       @EMail             = @Email
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ContactList_Insert'
        
        
        
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error inserting ContactList - ContactListId is blank'
								,@SupplierCode
								,'InterfaceSupplier'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
					
        goto error
      end
    end
    else
    begin
      exec @Error = p_ContactList_Update
       @ContactListId     = @ContactListId,
       @ExternalCompanyId = @ExternalCompanyId,
       @OperatorId        = null,
       @ContactPerson     = @ContactPerson,
       @Telephone         = @Phone,
       @Fax               = @Fax,
       @EMail             = @Email
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ContactList_Update'
        
        
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error updating ContactList'
								,@SupplierCode
								,'Supplier'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
					
        goto error
      end
    end
    
    error:
    if @Error = 0
    begin
      update InterfaceSupplier
         set RecordStatus = 'Y'
       where SupplierCode = @SupplierCode
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
      
      commit transaction
    end
    else
    begin
      if @@trancount > 0
        rollback transaction
      
      update InterfaceSupplier
         set RecordStatus = 'E'
       where SupplierCode = @SupplierCode
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
    end
    
    fetch Supplier_cursor into @SupplierCode,
                               @SupplierName,
                               @Address1,
                               @Address2,
                               @Address3,
                               @Address4,
                               @Modified,
                               @ContactPerson,
                               @Phone,
                               @Fax,
                               @Email,
                               @HostId,
                               @PrincipalCode
  end

  close Supplier_cursor
  deallocate Supplier_cursor
end
 
