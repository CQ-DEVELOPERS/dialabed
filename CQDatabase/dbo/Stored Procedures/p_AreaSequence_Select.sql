﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaSequence_Select
  ///   Filename       : p_AreaSequence_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:03
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaSequence table.
  /// </remarks>
  /// <param>
  ///   @AreaSequenceId int = null 
  /// </param>
  /// <returns>
  ///   AreaSequence.AreaSequenceId,
  ///   AreaSequence.PickAreaId,
  ///   AreaSequence.StoreAreaId,
  ///   AreaSequence.AdjustmentType,
  ///   AreaSequence.ReasonCode,
  ///   AreaSequence.InstructionTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaSequence_Select
(
 @AreaSequenceId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         AreaSequence.AreaSequenceId
        ,AreaSequence.PickAreaId
        ,AreaSequence.StoreAreaId
        ,AreaSequence.AdjustmentType
        ,AreaSequence.ReasonCode
        ,AreaSequence.InstructionTypeCode
    from AreaSequence
   where isnull(AreaSequence.AreaSequenceId,'0')  = isnull(@AreaSequenceId, isnull(AreaSequence.AreaSequenceId,'0'))
  
end
