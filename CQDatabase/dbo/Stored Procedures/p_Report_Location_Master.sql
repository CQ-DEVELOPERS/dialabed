﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Location_Master
  ///   Filename       : p_Report_Location_Master.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Location_Master
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  LocationId                      int              null,
   ProductCode                     nvarchar(60)     null,
   SKUCode                         nvarchar(100)    null,
   MinimumQuantity                 float            null,
   HandlingQuantity                float            null,
   MaximumQuantity                 float            null
	 )
	 
	 insert @TableResult
	       (LocationId,
	        ProductCode,
         SKUCode,
         MinimumQuantity,
         HandlingQuantity,
         MaximumQuantity)
  select sul.LocationId,
         p.ProductCode,
         sku.SKUCode,
         sul.MinimumQuantity,
         sul.HandlingQuantity,
         sul.MaximumQuantity
   from StorageUnitLocation  sul (nolock)
    join StorageUnit          su (nolock) on sul.StorageUnitId = su.StorageUnitId
    join Product               p (nolock) on su.ProductId = p.ProductId
    join SKU                 sku (nolock) on su.SKUId = sku.SKUId
    join AreaLocation         al (nolock) on sul.LocationId = al.LocationId
    join Area                  a (nolock) on al.AreaId = a.AreaId
   where a.AreaCode != 'SP'
  
  select null as 'LocationMasterId',
         'Y' as 'RecordStatus',
         null as 'InsertDate',
         null as 'ProcessedDate',
         w.Warehouse,
         w.WarehouseCode,
         a.Area,
         a.AreaCode,
         l.Location,
         l.Ailse,
         l.[Column],
         l.[Level],
         l.SecurityCode,
         l.RelativeValue,
         l.ActiveBinning,
         l.ActivePicking,
         l.PalletQuantity,
         tr.ProductCode,
         tr.SKUCode,
         tr.MinimumQuantity,
         tr.HandlingQuantity,
         tr.MaximumQuantity
    from Location        l (nolock)
    join AreaLocation   al (nolock) on l.LocationId = al.LocationId
    join Area            a (nolock) on al.AreaId    = a.AreaId
    join Warehouse       w (nolock) on a.WarehouseId = w.WarehouseId
    left
    join @TableResult   tr on l.LocationId = tr.LocationId
end
