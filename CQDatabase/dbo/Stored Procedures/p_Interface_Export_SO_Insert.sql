﻿/*
  /// <summary>
  ///   Procedure Name : p_Interface_Export_SO_Insert
  ///   Filename       : p_Interface_Export_SO_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Jan 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE [dbo].[p_Interface_Export_SO_Insert] (
	@IssueId INT
	,@ZeroLines BIT = NULL
	)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Error INT
		,@Errormsg VARCHAR(500)
		,@GetDate DATETIME
		,@OrderNumber VARCHAR(30)
		,@OutboundDocumentId INT
		,@Delivery INT
		,@InterfaceImportHeaderId INT
		,@InterfaceExportHeaderId INT
		,@rowcount INT
		,@ExternalCompanyId INT
		,@Backorder BIT
		,@LoadIndicator BIT
		,@WarehouseCode VARCHAR(30)

	SELECT @GetDate = dbo.ufn_Getdate()

	SET @Errormsg = 'Error executing p_Interface_Export_SO_Insert'

	IF EXISTS (
			SELECT *
			FROM OutboundDocument od(NOLOCK)
			JOIN OutboundDocumentType odt(NOLOCK) ON od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
			JOIN Issue i(NOLOCK) ON od.OutboundDocumentId = i.OutboundDocumentId
			WHERE i.IssueId = @IssueId
				AND odt.OutboundDocumentTypeCode = 'RET'
			)
	BEGIN
		INSERT INTO InterfaceExportSOHeader (OrderNumber)
		VALUES ('Credit Note')

		RETURN
	END

	SELECT @OrderNumber = od.OrderNumber
		,@OutboundDocumentId = od.OutboundDocumentId
		,@Delivery = i.Delivery
		,@ExternalCompanyId = od.ExternalCompanyId
		,@LoadIndicator = i.LoadIndicator
		,@WarehouseCode = w.WarehouseCode
	FROM OutboundDocument od(NOLOCK)
	JOIN OutboundDocumentType odt(NOLOCK) ON od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
	JOIN Issue i(NOLOCK) ON od.OutboundDocumentId = i.OutboundDocumentId
	INNER JOIN Warehouse w(NOLOCK) ON i.WarehouseId = w.WarehouseId
	WHERE i.IssueId = @IssueId
		AND odt.OutboundDocumentTypeCode NOT LIKE 'CR%'

	SELECT @Backorder = Backorder
	FROM ExternalCompany(NOLOCK)
	WHERE ExternalCompanyId = @ExternalCompanyId

	IF @Backorder IS NULL
		SET @Backorder = 0

	IF @LoadIndicator IS NULL
		SET @LoadIndicator = 0

	BEGIN TRANSACTION

	IF NOT EXISTS (
			SELECT TOP 1 1
			FROM InterfaceExportHeader
			WHERE IssueId = @IssueId
				AND RecordType != 'Allocated'
			)
	BEGIN
		IF @LoadIndicator = 0
			AND @Backorder = 1
			AND EXISTS (
				SELECT TOP 1 1
				FROM IssueLine
				WHERE IssueId = @IssueId
					AND (Quantity - ConfirmedQuatity) > 0
				)
		BEGIN
			EXEC @Error = p_Outbound_Backorder_Insert @IssueId = @IssueId

			IF @Error <> 0
				GOTO error
		END

		SELECT @InterfaceImportHeaderId = max(InterfaceImportHeaderId)
		FROM InterfaceImportHeader
		WHERE RecordStatus = 'C'
			AND OrderNumber = @OrderNumber
			AND InterfaceImportHeader.FromWarehouseCode = @WarehouseCode

		IF @InterfaceImportHeaderId IS NULL
			SELECT @InterfaceImportHeaderId = max(InterfaceImportHeaderId)
			FROM InterfaceImportHeader
			WHERE OrderNumber = @OrderNumber
				AND InterfaceImportHeader.FromWarehouseCode = @WarehouseCode

		INSERT InterfaceExportHeader (
			IssueId
			,PrimaryKey
			,OrderNumber
			,RecordType
			,RecordStatus
			,CompanyCode
			,Company
			,DeliveryDate
			,Remarks
			)
		SELECT DISTINCT @IssueId
			,od.OrderNumber
			,od.OrderNumber
			,odt.OutboundDocumentTypeCode
			,'N'
			,ec.ExternalCompanyCode
			,ec.ExternalCompany
			,i.DeliveryDate
			,i.Remarks
		FROM OutboundDocument od(NOLOCK)
		JOIN OutboundDocumentType odt(NOLOCK) ON od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
		JOIN Issue i(NOLOCK) ON od.OutboundDocumentId = i.OutboundDocumentId
		LEFT JOIN ExternalCompany ec(NOLOCK) ON od.ExternalCompanyId = ec.ExternalCompanyId
		WHERE i.IssueId = @IssueId

		SELECT @InterfaceExportHeaderId = scope_identity()
			,@rowcount = @@rowcount
			,@Error = @@Error
			,@Errormsg = 'p_Interface_Export_SO_Insert - insert InterfaceExportHeader'

		IF @Error <> 0
			GOTO error

		IF @InterfaceImportHeaderId IS NOT NULL
		BEGIN
			UPDATE export
			SET PrimaryKey = import.PrimaryKey
				,RecordType = import.RecordType
				,Address = import.Address
				,FromWarehouseCode = import.FromWarehouseCode
				,ToWarehouseCode = import.ToWarehouseCode
				,NumberOfLines = import.NumberOfLines
				,Additional1 = import.Additional1
				,Additional2 = import.Additional2
				,Additional3 = import.Additional3
				,Additional4 = import.Additional4
				,Additional5 = import.Additional5
				,Additional6 = import.Additional6
				,Additional7 = import.Additional7
				,Additional8 = import.Additional8
				,Additional9 = import.Additional9
				,Additional10 = import.Additional10
			FROM InterfaceImportHeader import
				,InterfaceExportHeader export
			WHERE InterfaceImportHeaderId = @InterfaceImportHeaderId
				AND InterfaceExportHeaderId = @InterfaceExportHeaderId

			SELECT @Error = @@Error
				,@Errormsg = 'p_Interface_Export_SO_Insert - update InterfaceExportHeader'

			IF @Error <> 0
				GOTO error
		END

		IF @rowcount > 0
		BEGIN
			INSERT InterfaceExportDetail (
				InterfaceExportHeaderId
				,ForeignKey
				,LineNumber
				,ProductCode
				,Product
				,SKUCode
				,Batch
				,Quantity
				,Weight
				,Additional5
				)
			SELECT @InterfaceExportHeaderId
				,od.OrderNumber
				,ol.LineNumber
				,vs.ProductCode
				,vs.Product
				,vs.SKUCode
				,vs.Batch
				,CASE 
					WHEN @ZeroLines = 1 THEN 0
					WHEN sr.SerialNumber IS NOT NULL THEN 1
					ELSE il.ConfirmedQuatity
					END
				,
				--il.ConfirmedQuatity,
				NULL
				,sr.SerialNumber
			FROM OutboundDocument od(NOLOCK)
			JOIN OutboundLine ol(NOLOCK) ON od.OutboundDocumentId = ol.OutboundDocumentId
			JOIN IssueLine il(NOLOCK) ON ol.OutboundLineId = il.OutboundLineId
			JOIN viewStock vs(NOLOCK) ON il.StorageUnitBatchId = vs.StorageUnitBatchId
			LEFT JOIN SerialNumber sr(NOLOCK) ON il.issuelineid = sr.issuelineid
			WHERE il.IssueId = @IssueId

			--AND il.IssueLineId IN (SELECT IssueLineId FROM Instruction i (nolock) )
			SELECT @Error = @@Error

			IF @Error <> 0
				GOTO error

			IF @InterfaceImportHeaderId IS NOT NULL
			BEGIN
				UPDATE export
				SET ForeignKey = import.ForeignKey
					,Additional1 = import.Additional1
					,Additional2 = import.Additional2
					,Additional3 = import.Additional3
					,Additional4 = import.Additional4
					--,Additional5 = import.Additional5
				FROM InterfaceImportDetail import
				JOIN InterfaceExportDetail export ON import.LineNumber = export.LineNumber
				WHERE InterfaceImportHeaderId = @InterfaceImportHeaderId
					AND InterfaceExportHeaderId = @InterfaceExportHeaderId

				SELECT @Error = @@Error

				IF @Error <> 0
					GOTO error

				SELECT @Error = @@Error

				IF @Error <> 0
					GOTO error
			END
		END
	END

	COMMIT TRANSACTION

	RETURN

	error:

	RAISERROR (900000, -1, -1, @Errormsg)

	ROLLBACK TRANSACTION

	RETURN @Error
END
