﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Priority_Update
  ///   Filename       : p_Priority_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:13
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Priority table.
  /// </remarks>
  /// <param>
  ///   @PriorityId int = null,
  ///   @Priority nvarchar(100) = null,
  ///   @PriorityCode nvarchar(20) = null,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Priority_Update
(
 @PriorityId int = null,
 @Priority nvarchar(100) = null,
 @PriorityCode nvarchar(20) = null,
 @OrderBy int = null 
)
 
as
begin
	 set nocount on;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @Priority = '-1'
    set @Priority = null;
  
  if @PriorityCode = '-1'
    set @PriorityCode = null;
  
	 declare @Error int
 
  update Priority
     set Priority = isnull(@Priority, Priority),
         PriorityCode = isnull(@PriorityCode, PriorityCode),
         OrderBy = isnull(@OrderBy, OrderBy) 
   where PriorityId = @PriorityId
  
  select @Error = @@Error
  
  
  return @Error
  
end
