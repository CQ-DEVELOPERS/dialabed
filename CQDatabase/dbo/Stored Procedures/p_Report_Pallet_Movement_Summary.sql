﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Pallet_Movement_Summary
  ///   Filename       : p_Report_Pallet_Movement_Summary.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Pallet_Movement_Summary
(
 @FromDate          datetime,
 @ToDate            datetime,
 @WarehouseId		int
)
 
as
begin
	 set nocount on;
	 
	 declare @Weeks int,
	         @Count int,
	         @Date  datetime
	 

  
  declare @TableResult as table
  (
	Week				smallint,
	StartDate			datetime,
	EndDate				datetime,
	WarehouseId			int,
	InstructionType     nvarchar(30),
	InstructionTypeCode nvarchar(10),
	Status              nvarchar(50),
	insStatusCode       nvarchar(10),
	Jobid				int,
	CreateDate          datetime,
	Quantity            float,
	ConfirmedQuantity   float
  )
  declare @TableResult2 as table
  (
	Week				smallint,
	StartDate			datetime,
	EndDate				datetime,
	SingleSKUQty		float,
	FullQty				float,
	FullMixedQty		float,
	MixedQty			float,
	CombinedFull		float,
	Total				float
  )
  


  -- how many weeks between from and to date
  
  set @Count = 0
  select @Weeks = round((datediff(dd, @FromDate, @ToDate) / 7.000) + .49, 0)
  
  
  if @Weeks = 0
    set @Weeks = 1
  
  while @Count < @Weeks
  begin
    select @Date = dateadd(wk, @Count, @FromDate)

    select @Count = @Count + 1
    
    insert @TableResult
          (Week,
           StartDate,
           EndDate,
           InstructionType,
           InstructionTypeCode,
           Jobid,
           Quantity,
           ConfirmedQuantity,
           CreateDate,
           WarehouseId,
           Status,
           insStatusCode
)
    select @Count,
           dateadd(dd, (datepart(dw, @Date) * -1) + 2, @Date),
           dateadd(ss, 86399, dateadd(dd, 6, dateadd(dd, (datepart(dw, @Date) * -1) + 2, @Date))),
           it.InstructionType,
           it.InstructionTypeCode,
           i.JobId,
           i.Quantity,
           i.ConfirmedQuantity,
           i.CreateDate,
           i.WarehouseId,
           s.Status,
           s.StatusCode
	from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status           s (nolock) on j.StatusId          = s.StatusId
    join Status          si (nolock) on i.StatusId          = si.StatusId
   where it.InstructionTypeCode in ('P', 'PS', 'FM', 'PM')
   and i.EndDate between (dateadd(dd, (datepart(dw, @Date) * -1) + 2, @Date)) and (dateadd(ss, 86399, dateadd(dd, 6, dateadd(dd, (datepart(dw, @Date) * -1) + 2, @Date))))
  order by isnull(EndDate, isnull(StartDate, CreateDate))		

  end

insert @TableResult2
          (Week,
           StartDate,
           EndDate,
		   SingleSKUQty)
    select distinct tr.Week,
           tr.StartDate,
           tr.EndDate,
           count(distinct(tr.Jobid))
	from @TableResult tr      
   where tr.InstructionTypeCode = 'PS'
   group by tr.Week,
           tr.StartDate,
           tr.EndDate
   
   insert @TableResult2
          (Week,
           StartDate,
           EndDate,
		   FullMixedQty)
    select distinct tr.Week,
           tr.StartDate,
           tr.EndDate,
           count(distinct(tr.Jobid))
	from @TableResult tr      
   where tr.InstructionTypeCode = 'FM'
    group by tr.Week,
           tr.StartDate,
           tr.EndDate
   
   insert @TableResult2
          (Week,
           StartDate,
           EndDate,
		   MixedQty)
    select distinct tr.Week,
           tr.StartDate,
           tr.EndDate,
           count(distinct(tr.Jobid))
	from @TableResult tr      
   where tr.InstructionTypeCode = 'PM'
    group by tr.Week,
           tr.StartDate,
           tr.EndDate
           
    insert @TableResult2
          (Week,
           StartDate,
           EndDate,
		   FullQty)
    select distinct tr.Week,
           tr.StartDate,
           tr.EndDate,
           count(distinct(tr.Jobid))
	from @TableResult tr      
   where tr.InstructionTypeCode = 'P'
    group by tr.Week,
           tr.StartDate,
           tr.EndDate

 update tr
     set CombinedFull = isnull(FullQty,0) + isnull(FullMixedQty,0)
    from @TableResult2 tr
    
     update tr
     set Total = isnull(SingleSKUQty,0) + isnull(FullMixedQty,0) + isnull(MixedQty,0) + isnull(FullQty,0)
    from @TableResult2 tr
    
	select distinct Week,
		StartDate,
		EndDate,
		sum(isnull(FullQty,0)) as FullQty,
		sum(isnull(SingleSKUQty,0)) as SingleSKUQty,
		sum(isnull(FullMixedQty,0)) as FullMixedQty,
		sum(isnull(MixedQty,0)) as MixedQty,
		sum(isnull(CombinedFull,0)) as CombinedFull,
		sum(isnull(Total,0)) as Total	
	from @TableResult2 tr
	group by Week,
		StartDate,
		EndDate
	order by tr.week
end
