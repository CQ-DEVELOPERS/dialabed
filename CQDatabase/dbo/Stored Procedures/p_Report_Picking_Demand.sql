﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Picking_Demand
  ///   Filename       : p_Report_Picking_Demand.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 May 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Picking_Demand
(
 @WarehouseId      int,
 @FromDate         datetime,
 @ToDate           datetime
)
 
as
begin
 set nocount on;
  
  declare @TableTemp as table
  (
   OutboundDocumentTypeId int,
   OutboundDocumentType   nvarchar(30),
   OutboundDocumentId     int,
   IssueId                int,
   IssueLineId            int,
   JobId                  int,
   InstructionId          int,
   StorageUnitBatchId     int,
   ProductCode            nvarchar(30),
   Product                nvarchar(50),
   SKUCode                nvarchar(50),
   Quantity               float,
   ConfirmedQuantity      float,
   Weight                 numeric(13,2),
   ConfirmedWeight        numeric(13,2),
   Volume                 numeric(13,2),
   ConfirmedVolume        numeric(13,2),
   CreateDate             datetime,
   Release                datetime,
   StartDate              datetime,
   EndDate                datetime,
   Checking               datetime,
   Despatch               datetime,
   DespatchChecked        datetime,
   Complete               datetime,
   DeliveryDate           datetime,
   StatusCode             nvarchar(10)
  )
  
  declare @TableResult as table
  (
   SummaryDate            datetime,
   Orders                 numeric(13,2),
   Lines                  numeric(13,2),
   Pieces                 numeric(13,2),
   Weight                 numeric(13,2),
   Bfwd                   numeric(13,3),
   New                    numeric(13,3),
   Daily                  numeric(13,3)
  )
  
  declare @Config int
  
  select @Config = convert(int, Value)
    from Configuration (nolock)
   where WarehouseId = @WarehouseId
     and ConfigurationId = 191
  
  if @Config is null
    set @Config = 7
  
  insert @TableTemp
        (OutboundDocumentTypeId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         CreateDate,
         Release,
         StartDate,
         EndDate,
         Checking,
         Despatch,
         DespatchChecked,
         Complete)
  select ili.OutboundDocumentTypeId,
         ili.OutboundDocumentId,
         ili.IssueId,
         ili.IssueLineId,
         op.JobId,
         i.InstructionId,
         i.StorageUnitBatchId,
         ili.Quantity,
         ili.ConfirmedQuantity,
         op.CreateDate,
         op.Release,
         op.StartDate,
         op.EndDate,
         op.Checking,
         op.Despatch,
         op.DespatchChecked,
         op.Complete
    from OutboundPerformance   op
    join Instruction            i on op.JobId = i.JobId
    join IssueLineInstruction ili on i.InstructionId = ili.InstructionId
   where (op.CreateDate between @FromDate and @ToDate
      or  op.Complete        >= @FromDate -- between @FromDate and @ToDate
      or  op.Complete        is null)
      --and op.CreateDate <= @ToDate -- GS 2011-02-09 Replaced with line below
      and op.CreateDate between dateadd(dd,-@Config, @FromDate) and @ToDate
	  and i.WarehouseId = @WarehouseId
  
  update tr
     set ConfirmedQuantity = 0,
         StatusCode        = s.StatusCode
    from @TableTemp tr
    join Job           j on tr.JobId = j.JobId
    join Status        s on j.StatusId = s.StatusId
   where s.StatusCode = 'NS'
  
--  update tr
--     set StatusCode        = 'NS'
--    from @TableTemp tr
--   where isnull(StatusCode,'') != 'NS'
--     and isnull(ConfirmedQuantity,0) = 0
  
  update tr
     set CreateDate = od.CreateDate
    from @TableTemp     tr
    join OutboundDocument od on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set OutboundDocumentType = odt.OutboundDocumentType
    from @TableTemp          tr
    join OutboundDocumentType odt on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  update tr
     set DeliveryDate = i.DeliveryDate
    from @TableTemp tr
    join Issue         i on tr.IssueId = i.IssueId
  
  update tr
     set ProductCode     = vp.ProductCode,
         Product         = vp.Product,
         SKUCode         = vp.SKUCode,
         Weight          = vp.Weight * tr.Quantity,
         ConfirmedWeight = vp.Weight * tr.ConfirmedQuantity,
         Volume          = vp.Volume * tr.Quantity,
         ConfirmedVolume = vp.Volume * tr.ConfirmedQuantity
    from @TableTemp        tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join viewPack          vp          on sub.StorageUnitId     = vp.StorageUnitId
   where vp.Quantity     = 1
     and vp.WarehouseId  = @WarehouseId
  
  update @TableTemp
     set ConfirmedQuantity = 0
   where ConfirmedQuantity is null
  
  update @TableTemp
     set ConfirmedWeight = 0
   where ConfirmedWeight is null
  
  update @TableTemp
     set ConfirmedVolume = 0
   where ConfirmedVolume is null
  
  declare @Count     int,
          @StartDate datetime,
          @EndDate   datetime
  
  set @StartDate = @FromDate
  set @EndDate   = dateadd(ss, 86399, @FromDate)
  
  select @Count = datediff(dd, @FromDate, @ToDate)
  
  while @Count > 0
  begin
    set @Count = @count - 1
    
    set @StartDate = dateadd(dd, 1, @StartDate)
    set @EndDate = dateadd(dd, 1, @EndDate)
    
    insert @TableResult
          (SummaryDate,
           Orders,
           Lines,
           Pieces,
           Weight)
    select @StartDate,
           count(distinct(OutboundDocumentId)),
           count(distinct(IssueLineId)),
           sum(ConfirmedQuantity),
           sum(ConfirmedWeight)
      from @TableTemp
     where Checking between @StartDate and @EndDate
    
    update tr
       set Bfwd = (select sum(Weight)
                     from @TableTemp
                    where isnull(Checking, @StartDate) >= @StartDate -- isnull(Checking, @ToDate) <= @ToDate
                      and CreateDate > '2008-05-01'
                      and CreateDate < @StartDate)
      from @TableResult tr
     where tr.SummaryDate = @StartDate
    
    update tr
       set New = (select sum(Weight)
                     from @TableTemp
                    where CreateDate between @StartDate and @EndDate)
      from @TableResult tr
     where tr.SummaryDate = @StartDate
    
    update tr
       set Daily = (select sum(Weight)
                      from @TableTemp
                     where datediff(hh, CreateDate, Checking) <= 24
                       and Checking between @StartDate and @EndDate)
      from @TableResult tr
     where tr.SummaryDate = @StartDate
  end
  
  select convert(nvarchar(10), SummaryDate, 111) as 'SummaryDate',
         Orders,
         Lines,
         Pieces,
         Weight,
         Bfwd,
         New,
         isnull(Bfwd,0) + isnull(New,0) as 'Accum',
         Daily
    from @TableResult
  order by SummaryDate
end
