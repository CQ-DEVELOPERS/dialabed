﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Plan_Order_Search
  ///   Filename       : p_Production_Plan_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Plan_Order_Search
(
 @WarehouseId            int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentId        int,
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   NumberOfLines             int,
   DeliveryDate              datetime,
   CreateDate                datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   PriorityId                int,
   Priority                  nvarchar(50),
   LocationId                int,
   Location                  nvarchar(15),
   DespatchBay               int,
   DespatchBayDesc           nvarchar(15),
   AvailabilityIndicator     nvarchar(20),
   Remarks                   nvarchar(255),
   Total                     numeric(13,3),
   Complete                  numeric(13,3),
   PercentageComplete        numeric(13,3),
   StockAvailable            numeric(13,3),
   DropSequence              int
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  insert @TableResult
        (OutboundDocumentId,
         IssueId,
         LocationId,
         DespatchBay,
         OrderNumber,
         StatusId,
         Status,
         PriorityId,
         DeliveryDate,
         CreateDate,
         Remarks,
         NumberOfLines,
         Total,
         Complete,
         DropSequence)
  select od.OutboundDocumentId,
         i.IssueId,
         i.LocationId,
         i.DespatchBay,
         od.OrderNumber,
         i.StatusId,
         s.Status,
         i.PriorityId,
         i.DeliveryDate,
         od.CreateDate,
         i.Remarks,
         i.NumberOfLines,
         i.Total,
         i.Complete,
         i.DropSequence
    from OutboundDocument      od (nolock)
    join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join Status                 s (nolock) on i.StatusId                = s.StatusId
   where s.Type                    = 'IS'
     and s.StatusCode             in ('W','P','SA','M','RL','PC','PS','CK','A','WC','QA')
     and od.WarehouseId            = @WarehouseId
    
  update tr
     set OutboundShipmentId = si.OutboundShipmentId,
         DeliveryDate = os.ShipmentDate,
         LocationId   = os.LocationId,
         DespatchBay  = os.DespatchBay,
         StatusId     = os.StatusId
    from @TableResult  tr
    join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
    join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  
  declare @TableLines as table
  (
   IssueId       int,
   IssueLineId   int,
   StorageUnitId int,
   Quantity      numeric(13,6),
   QuantitySOH   numeric(13,6),
   QuantityQC    numeric(13,6),
   Available     bit,
   AvailableQC   bit,
   Short         bit,
   DropSequence  int
  )
  
  declare @TableStock as table
  (
   StorageUnitId int,
   StatusCode    nvarchar(10),
   Quantity      numeric(13,6)
  )
  
  insert @TableLines
        (IssueId,
         IssueLineId,
         StorageUnitId,
         Quantity,
         DropSequence)
  select il.IssueId,
         il.IssueLineId,
         sub.StorageUnitId,
         il.Quantity,
         DropSequence
    from @TableResult      tr
    join IssueLine         il (nolock) on tr.IssueId = il.IssueId
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
  
  insert @TableStock
        (StorageUnitId,
         StatusCode,
         Quantity)
  select StorageUnitId,
         StatusCode,
         sum(ActualQuantity)
    from viewSOH
   where WarehouseId = @WarehouseId
     and ActualQuantity - AllocatedQuantity > 0
     and StorageUnitId in (select distinct StorageUnitId from @TableLines)
  group by StorageUnitId,
           StatusCode
  
  declare @IssueId        int,
          @IssueLineId    int,
          @StorageUnitId  int,
          @Quantity       numeric(13,6),
          @SOH            numeric(13,6),
          @DropSequence   int
  
  declare CheckSOH cursor for
   select top 2
          IssueId,
          IssueLineId,
          StorageUnitId,
          Quantity,
          DropSequence
     from @TableLines
   where Quantity > 0
  order by DropSequence
  
  open CheckSOH
  
  fetch next
   from CheckSOH into @IssueId,
                      @IssueLineId,
                      @StorageUnitId,
                      @Quantity,
                      @DropSequence
  
  while @@fetch_status = 0
  begin
    set @SOH = null;
    
    select @SOH = Quantity
      from @TableStock
     where StorageUnitId = @StorageUnitId
    
    if @SOH > 0
    begin
      update @TableLines
         set QuantitySOH = case when @SOH >= @Quantity
                                then @Quantity
                                else @SOH
                                end
       where IssueLineId = @IssueLineId
      
      if @SOH > @Quantity
        update @TableStock
           set Quantity = Quantity - @Quantity
         where StorageUnitId = @StorageUnitId
      else
        delete @TableStock
         where StorageUnitId = @StorageUnitId
    end
    
    fetch next 
     from CheckSOH into @IssueId,
                        @IssueLineId,
                        @StorageUnitId,
                        @Quantity,
                        @DropSequence
  end
  
  close CheckSOH
  deallocate CheckSOH
  
  update @TableLines
     set Available = case when isnull(Quantity,0) = isnull(QuantitySOH,0)
                           then 1
                           else 0
                           end
  
  update @TableLines
     set AvailableQC = case when isnull(Quantity,0) = isnull(QuantityQC,0)
                           then 1
                           else 0
                           end
  
  update @TableLines
     set Short = case when isnull(Available,0) = 0 and isnull(QuantityQC,0) = 0
                           then 1
                           else 0
                           end
  
  --select *
  --  from @TableLines
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set DespatchBayDesc = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.DespatchBay = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update tr
     set AvailabilityIndicator = 'Red'
    from @TableResult tr
    join @TableLines  tl on tr.IssueId = tl.IssueId
   where Short = 1
  
  update tr
     set AvailabilityIndicator = 'Yellow'
    from @TableResult tr
    join @TableLines  tl on tr.IssueId = tl.IssueId
   where tl.AvailableQC = 1
     and AvailabilityIndicator is null
  
  update tr
     set AvailabilityIndicator = 'Green'
    from @TableResult tr
    join @TableLines  tl on tr.IssueId = tl.IssueId
   where tl.Available = 1
     and AvailabilityIndicator is null
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailabilityIndicator is null
  
  update @TableResult
     set PercentageComplete = (Complete / Total) * 100
   where Complete > 0
     and Total    > 0
  
  update @TableResult
     set PercentageComplete = 0
   where PercentageComplete is null
  
  select ROW_NUMBER() 
         OVER (ORDER BY DropSequence) AS 'Rank',
         IssueId,
         DropSequence,
         OrderNumber,
         convert(nvarchar(10), DeliveryDate, 120) as 'DeliveryDate',
         Remarks as 'Batch'
    from @TableResult
  order by DropSequence
end
