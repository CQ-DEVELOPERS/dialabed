﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ChildProduct_Insert
  ///   Filename       : p_ChildProduct_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:19
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ChildProduct table.
  /// </remarks>
  /// <param>
  ///   @ChildProductId int = null output,
  ///   @ChildStorageUnitBatchId int = null,
  ///   @ParentStorageUnitBatchId int = null 
  /// </param>
  /// <returns>
  ///   ChildProduct.ChildProductId,
  ///   ChildProduct.ChildStorageUnitBatchId,
  ///   ChildProduct.ParentStorageUnitBatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ChildProduct_Insert
(
 @ChildProductId int = null output,
 @ChildStorageUnitBatchId int = null,
 @ParentStorageUnitBatchId int = null 
)
 
as
begin
	 set nocount on;
  
  if @ChildProductId = '-1'
    set @ChildProductId = null;
  
	 declare @Error int
 
  insert ChildProduct
        (ChildStorageUnitBatchId,
         ParentStorageUnitBatchId)
  select @ChildStorageUnitBatchId,
         @ParentStorageUnitBatchId 
  
  select @Error = @@Error, @ChildProductId = scope_identity()
  
  
  return @Error
  
end
