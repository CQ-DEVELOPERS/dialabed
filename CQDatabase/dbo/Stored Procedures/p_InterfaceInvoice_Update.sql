﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceInvoice_Update
  ///   Filename       : p_InterfaceInvoice_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:21
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceInvoice table.
  /// </remarks>
  /// <param>
  ///   @InterfaceInvoiceId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @InvoiceNumber nvarchar(60) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceInvoice_Update
(
 @InterfaceInvoiceId int = null,
 @OrderNumber nvarchar(60) = null,
 @InvoiceNumber nvarchar(60) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceInvoiceId = '-1'
    set @InterfaceInvoiceId = null;
  
  if @OrderNumber = '-1'
    set @OrderNumber = null;
  
  if @InvoiceNumber = '-1'
    set @InvoiceNumber = null;
  
	 declare @Error int
 
  update InterfaceInvoice
     set OrderNumber = isnull(@OrderNumber, OrderNumber),
         InvoiceNumber = isnull(@InvoiceNumber, InvoiceNumber),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus) 
   where InterfaceInvoiceId = @InterfaceInvoiceId
  
  select @Error = @@Error
  
  
  return @Error
  
end
