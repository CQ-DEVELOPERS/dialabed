﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Inbound_Pallets
  ///   Filename       : p_Report_Inbound_Pallets.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Inbound_Pallets
(
 @WarehouseId int,
 @Production  bit = null
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   Category            nvarchar(30),
   StatusCode          nvarchar(10),
   InstructionTypeCode nvarchar(10),
   InstuctionId        int,
   JobId               int,
   ReceiptLineId       int,
   StorageUnitId       int,
   ConfirmedQuantity   float,
   Litres              float,
   Datevalue           datetime,
   Datecategory        nvarchar(30)
  )

  insert @TableResult
        (Category,
         StatusCode,
         InstructionTypeCode,
         InstuctionId,
         JobId,
         ReceiptLineId,
         StorageUnitId,
         ConfirmedQuantity,
         Litres,
         Datevalue)
  select null as Category,
         s.StatusCode,
         it.InstructionTypeCode,
         i.InstructionId,
         i.JobId,
         i.ReceiptLineId,
         i.StorageUnitBatchId,
         i.ConfirmedQuantity,
         i.ConfirmedQuantity * isnull(sku.Litres,1),
         isnull(i.EndDate,isnull(j.CheckedDate,isnull(i.StartDate,i.CreateDate)))
    from Instruction      i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on j.StatusId           = s.StatusId
   where i.WarehouseId = @WarehouseId
     and it.InstructionTypeCode in ('PR','S','SM')
     and s.StatusCode not in ('Z','CK')
  
  --if @Production = 0
  --  delete @TableResult where InstructionTypeCode = 'PR'
  --else if @Production = 1
  --  delete @TableResult where InstructionTypeCode != 'PR'
  
  update @TableResult
     set Datecategory = 'Today'
   where Datevalue >= convert(varchar(10), getdate(), 120)  + ' 07:00'
  
  update @TableResult
     set Datecategory = 'BFwd'
   where Datevalue < convert(varchar(10), dateadd(dd, -1, getdate()), 120)  + ' 07:00'
  
  update @TableResult
     set Datecategory = 'Yesterday'
   where Datevalue between convert(varchar(10), dateadd(dd, -1, getdate()), 120)  + ' 07:00' and convert(varchar(10), getdate(), 120)  + ' 07:00'
  
  update @TableResult
     set Datecategory = 'One Week'
   where Datevalue < convert(varchar(10), dateadd(dd, -7, getdate()), 120)  + ' 07:00'
  
  delete @TableResult
   where Datecategory not in ('Yesterday','Today')
     and Category = 'Put Away'
  
  select Category,
         Datecategory,
         count(1) as 'Pallets',
         sum(ConfirmedQuantity) as 'Quantity',
         sum(Litres) as 'Litres'
    from @TableResult
  group by Category,
           Datecategory
  order by Category,
           Datecategory desc
           
end
