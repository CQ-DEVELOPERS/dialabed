﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_SO_Combine
  ///   Filename       : p_BOM_SO_Combine.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_SO_Combine
(
 @outboundShipmentId int = null,
 @issueId            int,
 @firstIssueId       int
)
 
as
begin
	 set nocount on;
	 
	 declare @Error             int = 0,
          @Errormsg          nvarchar(500) = 'Error executing p_BOM_SO_Combine',
          @GetDate           datetime,
          @WarehouseId       int,
          @StatusId          int,
          @OldOutboundShipmentId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @outboundShipmentId = -1
    set @outboundShipmentId = null
  
  if @issueId = -1
    set @issueId = null
  
  if @firstIssueId = -1
    set @firstIssueId = null
  
  select top 1 @outboundShipmentId = osi.OutboundShipmentId,
               @WarehouseId        = i.WarehouseId
    from Issue                   i (nolock)
    left
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
   where i.IssueId = @firstIssueId
  
  begin transaction
  
  select @OldOutboundShipmentId = OutboundShipmentId
    from OutboundShipmentIssue
   where IssueId = @issueId
  
  if @outboundShipmentId is null
  begin
    select @StatusId = dbo.ufn_StatusId('IS','W')
    
    exec @Error = p_OutboundShipment_Insert
     @outboundShipmentId = @outboundShipmentId output,
     @StatusId           = @StatusId,
     @WarehouseId        = @WarehouseId,
     @ShipmentDate       = @Getdate
    
    exec @Error = p_OutboundShipmentIssue_Link
     @OutboundShipmentId = @outboundShipmentId,
     @issueId            = @issueId
    
    if @Error <> 0
      goto error
  end
  
  if @OldOutboundShipmentId is not null
  begin
    exec @Error = p_OutboundShipmentIssue_Unlink
     @OutboundShipmentId = @OldOutboundShipmentId,
     @issueId            = @issueId
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_OutboundShipmentIssue_Link
   @OutboundShipmentId = @OutboundShipmentId,
   @issueId            = @issueId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end

