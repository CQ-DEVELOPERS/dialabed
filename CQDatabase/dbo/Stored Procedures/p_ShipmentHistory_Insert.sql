﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ShipmentHistory_Insert
  ///   Filename       : p_ShipmentHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:48
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ShipmentHistory table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Route nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   ShipmentHistory.ShipmentId,
  ///   ShipmentHistory.StatusId,
  ///   ShipmentHistory.WarehouseId,
  ///   ShipmentHistory.LocationId,
  ///   ShipmentHistory.ShipmentDate,
  ///   ShipmentHistory.Remarks,
  ///   ShipmentHistory.SealNumber,
  ///   ShipmentHistory.VehicleRegistration,
  ///   ShipmentHistory.Route,
  ///   ShipmentHistory.CommandType,
  ///   ShipmentHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ShipmentHistory_Insert
(
 @ShipmentId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(500) = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Route nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ShipmentHistory
        (ShipmentId,
         StatusId,
         WarehouseId,
         LocationId,
         ShipmentDate,
         Remarks,
         SealNumber,
         VehicleRegistration,
         Route,
         CommandType,
         InsertDate)
  select @ShipmentId,
         @StatusId,
         @WarehouseId,
         @LocationId,
         @ShipmentDate,
         @Remarks,
         @SealNumber,
         @VehicleRegistration,
         @Route,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
