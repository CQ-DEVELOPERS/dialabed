﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Incubation_Report
  ///   Filename       : p_Report_Incubation_Report.sql
  ///   Create By      : Karen
  ///   Date Created   : January 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Incubation_Report

 
as
begin

set nocount on;

declare @TableResult as Table
  (
   ProductCode		nvarchar(30),
   Product			nvarchar(50),
   Batch			nvarchar(50),
   ActualYield		int,
   CreateDate		datetime,
   ShelfLifeDays	int,
   IncubationDays	int,
   DaysRemaining	int,
   ExpiryDate       datetime,
   Status			nvarchar(50),
   BatchActiveDate	datetime
  );

insert  @TableResult 
        (ProductCode,
         Product,
         Batch,
         ActualYield,
         CreateDate,
         ShelfLifeDays,
         IncubationDays,
         ExpiryDate,
         Status)
 select  p.ProductCode,
		 p.Product,
		 b.Batch,
		 b.ActualYield,
		 b.CreateDate,
		 b.ShelfLifeDays,
		 b.IncubationDays,
		 b.ExpiryDate,
		 s.Status
from batch				b
join storageunitbatch	sub (nolock) on sub.BatchId = b.BatchId
join StorageUnit		su  (nolock) on sub.StorageUnitId = su.StorageUnitId
join product			p   (nolock) on p.ProductId = su.ProductId
join status				s   (nolock) on b.StatusId = s.StatusId

  update tr
     set BatchActiveDate = DATEADD (Day, 14, CreateDate)
    from @TableResult tr
    
  update tr
     set DaysRemaining = DATEDIFF (DAY, GETDATE ( ), BatchActiveDate)
  from @TableResult tr
		
select	ProductCode,
		Product,
		Batch,
		ActualYield,
		CreateDate,
		ShelfLifeDays,
		IncubationDays,
		DaysRemaining,
		ExpiryDate,
		Status,
		BatchActiveDate
from @TableResult	
		
end	   		
