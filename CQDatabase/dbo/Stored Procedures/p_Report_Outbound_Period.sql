﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Outbound_Period
  ///   Filename       : p_Report_Outbound_Period.sql
  ///   Create By      : Willis
  ///   Date Created   : 28 July 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Outbound_Period
(

 @WarehouseId       int
 
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
	OutboundDocumentId		int,
	SortId					int,
	OutboundDocumentType	nvarchar(30),
	Period					nvarchar(50),
	Weight					numeric(13,3)
  )

--  insert @TableResult
--        (OutboundDocumentId,
--		       SortId,
--         OutboundDocumentType,
--         Period)
  select case	when	dbo.ufn_Configuration_Value(44, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate)	then 1
              when	dbo.ufn_Configuration_Value(45, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate)		then 2
              when	dbo.ufn_Configuration_Value(46, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate)		then 3
              when dbo.ufn_Configuration_Value(47, @warehouseId) <= Datediff(hh,getdate(),i.deliverydate)			then 4
         else 
              5
         end as 'SortId',
         odt.OutboundDocumentType,
         case	when	dbo.ufn_Configuration_Value(44, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) 
						        then dbo.ufn_Configuration_Description(44,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(44, @warehouseId))
              when	dbo.ufn_Configuration_Value(45, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) 
						        then dbo.ufn_Configuration_Description(45,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(45, @warehouseId))
              when	dbo.ufn_Configuration_Value(46, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) 
						        then dbo.ufn_Configuration_Description(46,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(46, @warehouseId))
              when dbo.ufn_Configuration_Value(47, @warehouseId) <= Datediff(hh,getdate(),i.deliverydate) 
						        then dbo.ufn_Configuration_Description(47,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(47, @warehouseId))
         else 
              'Unknown'
         end as 'Period',
      sum(p.Weight * il.Quantity) as Weight
    from Issue					              i	(nolock)
    join IssueLine              il (nolock) on i.IssueId             = il.IssueId
		  join	status				            	si	(nolock)	on i.StatusId            = si.StatusId
    join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
		  join	StorageUnit			        	su	(nolock)	on sub.StorageUnitId			  = su.StorageUnitId
    join	Pack				               	p	(nolock)	on su.StorageUnitId			   = p.StorageUnitId
    join	PackType			           	pt	(nolock)	on p.PackTypeId				      = pt.PackTypeId
    join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType  odt (nolocK) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where	si.Type        = 'IS'
		   and si.StatusCode  = 'RL' --in ('M','RL','PC','PS','CK','CD','A','WC','QA')
		   and pt.PackType    = 'Units'
  group by case	when	dbo.ufn_Configuration_Value(44, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate)	then 1
              when	dbo.ufn_Configuration_Value(45, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate)		then 2
              when	dbo.ufn_Configuration_Value(46, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate)		then 3
              when dbo.ufn_Configuration_Value(47, @warehouseId) <= Datediff(hh,getdate(),i.deliverydate)			then 4
           else 
                5
           end,
           odt.OutboundDocumentType,
           case	when	dbo.ufn_Configuration_Value(44, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) 
						          then dbo.ufn_Configuration_Description(44,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(44, @warehouseId))
                when	dbo.ufn_Configuration_Value(45, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) 
						          then dbo.ufn_Configuration_Description(45,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(45, @warehouseId))
                when	dbo.ufn_Configuration_Value(46, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) 
						          then dbo.ufn_Configuration_Description(46,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(46, @warehouseId))
                when dbo.ufn_Configuration_Value(47, @warehouseId) <= Datediff(hh,getdate(),i.deliverydate) 
						          then dbo.ufn_Configuration_Description(47,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(47, @warehouseId))
           else 
                'Unknown'
           end
--  update tr
--     set Weight = (select sum(p.Weight * ol.Quantity)
--                     from OutboundLine ol (nolock)
--                     join StorageUnit  su (nolock) on ol.StorageUnitId = su.StorageUnitId
--                     join Pack          p (nolock) on su.StorageUnitId = p.StorageUnitId
--                     join PackType     pt (nolock) on p.PackTypeId     = pt.PackTypeId
--                    where pt.PackType = 'Units'
--                      and tr.OutboundDocumentId = ol.OutboundDocumentId)
--    from @TableResult tr
--  
--  select OutboundDocumentType,
--		 SortId,
--         Period,
--         Weight
--    from @TableResult
end
