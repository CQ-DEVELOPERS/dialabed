﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Inbound_Move
  ///   Filename       : p_Pallet_Rebuild_Inbound_Move.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Inbound_Move
(
 @oldJobId         int,
 @newJobId         int
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  InstructionId int,
	  ReceiptLineId   int,
	  Quantity      float
	 )
  
  declare @Error         int,
          @Errormsg      varchar(500),
          @GetDate       datetime,
          @ReceiptLineId   int,
          @InstructionId int,
          @Quantity      float
	 
	 select @GetDate = dbo.ufn_Getdate()
	 
	 set @Errormsg = 'Error executing p_Pallet_Rebuild_Inbound_Move'
	 
	 --being transaction
	 
	 insert @TableResult
	       (InstructionId,
	        ReceiptLineId,
	        Quantity)
  select Distinct i.InstructionId,
         rl.ReceiptLineId,
         i.ConfirmedQuantity
    from ReceiptLine          rl (nolock)
    join Instruction            i (nolock) on rl.ReceiptLineId     = i.ReceiptLineId
    join Status                 s (nolock) on i.StatusId           = s.StatusId
   where i.JobId = @oldJobId
     and i.ConfirmedQuantity > 0
  
  while exists(select top 1 1
                 from @TableResult)
  begin
    select @ReceiptLineId   = ReceiptLineId,
           @InstructionId = InstructionId,
           @Quantity      = Quantity
      from @TableResult
    
    delete @TableResult
     where ReceiptLineId   = @ReceiptLineId
       and InstructionId = @InstructionId
       and Quantity      = @Quantity
    
    exec p_Pallet_Rebuild_Inbound_Link
     @JobId         = @newJobId,
     @ReceiptLineId = @ReceiptLineId,
     @InstructionId = @InstructionId,
     @Quantity      = @Quantity
    
    if @Error <> 0
      goto error
  end
  
  --commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    --rollback transaction
    return @Error
end
