﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfacePackInfo_Insert
  ///   Filename       : p_InterfacePackInfo_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:36
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfacePackInfo table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @PackCode nvarchar(60) = null,
  ///   @PackDescription nvarchar(510) = null,
  ///   @UOM nvarchar(60) = null,
  ///   @SaleUOM nvarchar(60) = null,
  ///   @Ingredients nvarchar(510) = null,
  ///   @Quantity float = null,
  ///   @UnitSize int = null,
  ///   @Weight float = null,
  ///   @Length int = null,
  ///   @Width int = null,
  ///   @Height int = null 
  /// </param>
  /// <returns>
  ///   InterfacePackInfo.HostId,
  ///   InterfacePackInfo.ProductCode,
  ///   InterfacePackInfo.PackCode,
  ///   InterfacePackInfo.PackDescription,
  ///   InterfacePackInfo.UOM,
  ///   InterfacePackInfo.SaleUOM,
  ///   InterfacePackInfo.Ingredients,
  ///   InterfacePackInfo.Quantity,
  ///   InterfacePackInfo.UnitSize,
  ///   InterfacePackInfo.Weight,
  ///   InterfacePackInfo.Length,
  ///   InterfacePackInfo.Width,
  ///   InterfacePackInfo.Height 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfacePackInfo_Insert
(
 @HostId nvarchar(60) = null,
 @ProductCode nvarchar(60) = null,
 @PackCode nvarchar(60) = null,
 @PackDescription nvarchar(510) = null,
 @UOM nvarchar(60) = null,
 @SaleUOM nvarchar(60) = null,
 @Ingredients nvarchar(510) = null,
 @Quantity float = null,
 @UnitSize int = null,
 @Weight float = null,
 @Length int = null,
 @Width int = null,
 @Height int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfacePackInfo
        (HostId,
         ProductCode,
         PackCode,
         PackDescription,
         UOM,
         SaleUOM,
         Ingredients,
         Quantity,
         UnitSize,
         Weight,
         Length,
         Width,
         Height)
  select @HostId,
         @ProductCode,
         @PackCode,
         @PackDescription,
         @UOM,
         @SaleUOM,
         @Ingredients,
         @Quantity,
         @UnitSize,
         @Weight,
         @Length,
         @Width,
         @Height 
  
  select @Error = @@Error
  
  
  return @Error
  
end
