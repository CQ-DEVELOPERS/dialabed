﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Delete
  ///   Filename       : p_InboundDocument_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2013 14:47:30
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Delete
(
 @InboundDocumentId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InboundDocument
     where InboundDocumentId = @InboundDocumentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
