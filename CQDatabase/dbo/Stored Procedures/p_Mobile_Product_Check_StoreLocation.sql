﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_StoreLocation
  ///   Filename       : p_Mobile_Product_Check_StoreLocation.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_StoreLocation
(
 @jobId         int,
 @storeLocation nvarchar(50)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @WarehouseId       int,
          @LocationId        int,
          @OutboundShipmentId int,
          @IssueId            int
  
  declare @TableJobs as table
  (
   JobId int
  )
  
  select @WarehouseId = WarehouseId
    from Job (nolock)
   where JobId = @jobId
  
  select @LocationId = l.LocationId
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId
   where a.WarehouseId = @WarehouseId
     and l.Location = @storeLocation
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @LocationId is null
  begin
    set @Error = -1
    goto error
  end
  
  begin transaction
  
  insert @TableJobs
        (JobId)
  select @jobId
  
  select top 1
         @OutboundShipmentId = ili.OutboundShipmentId,
         @IssueId            = ili.IssueId
    from Instruction            i (nolock)
    join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
    join Issue                iss (nolock) on ili.IssueId = iss.IssueId
    where i.JobId = @jobId
  
  select @Error = @@error
  
  if @OutboundShipmentId is not null
  begin
    insert @TableJobs
          (JobId)
    select distinct j.JobId
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
      join Job                    j (nolock) on i.JobId           = j.JobId
      join Status                 s (nolock) on j.StatusId        = s.StatusId
     where s.StatusCode in ('RL','CK','S')
       and ili.OutboundShipmentId = @OutboundShipmentId
  end
  else if @IssueId is not null
  begin
    insert @TableJobs
          (JobId)
    select distinct j.JobId
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
      join Job                    j (nolock) on i.JobId           = j.JobId
      join Status                 s (nolock) on j.StatusId        = s.StatusId
     where s.StatusCode in ('RL','CK','S')
       and ili.IssueId = @IssueId
  end
  
  while exists(select 1 from @TableJobs)
  begin
    select top 1 @jobId = JobId from @TableJobs
    
    delete @TableJobs where JobId = @jobId
    
    exec p_Job_Change_Location
     @JobId      = @JobId,
     @LocationId = @LocationId
    
    if @Error <> 0
      goto error
  end
  
  --update iss
  --   set LocationId = i.StoreLocationId
  --  from Instruction            i (nolock)
  --  join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
  --  join Issue                iss (nolock) on ili.IssueId = iss.IssueId
  --  where i.JobId = @jobId
  
  exec @Error = p_Issue_Update
   @IssueId    = @IssueId,
   @LocationId = @LocationId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Mobile_Product_Check_StoreLocation'
    rollback transaction
    return @Error
end
