﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_static_BOMReleaseInstruction
  ///   Filename       : p_static_BOMReleaseInstruction.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   <@BOMInstructionId>
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_static_BOMReleaseInstruction
(
    @BOMInstructionId Int
)
As
Begin      
    Declare
         @OrderNumber               NVarChar(30)
        ,@StorageUnitId             Int
        ,@Quantity                  float
        ,@WarehouseId               Int         
        
    Select
         @OrderNumber = bi.OrderNumber
        ,@Quantity  = bi.Quantity
        ,@StorageUnitId= bh.StorageUnitId
    From BOMInstruction bi (nolock)
    Join BOMHeader      bh (nolock) on bi.BOMHeaderId = bh.BOMHeaderId
    Where bi.BOMInstructionId = @BOMInstructionId
    
    
    
    --Create Pick
    Exec p_Kit_CreatePickInstruction
         @BOMInstructionId  = @BOMInstructionId
        ,@OrderNumber       = @OrderNumber
        ,@StorageUnitId     = @StorageUnitId
        ,@WarehouseId       = 1
        ,@Quantity          = @Quantity
    
    --Create Receipt

    Exec p_Kit_CreateReceiptInstruction
         @BOMInstructionId  = @BOMInstructionId
        ,@OrderNumber       = @OrderNumber
        ,@StorageUnitId     = @StorageUnitId
        ,@WarehouseId       = 1
        ,@Quantity          = @Quantity
    
End

