﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Manual_Palletisation_Create
  ///   Filename       : p_Receiving_Manual_Palletisation_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Manual_Palletisation_Create
(
 @ReceiptLineId    int,
 @NumberOfLines    int
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @LineQuantity       float,
          @ReceiptId          int,
          @WarehouseId        int,
          @PickLocationId     int,
          @StorageUnitBatchId int,
          @AcceptedQuantity   float,
          @PalletQuantity     float,
          @PalletCount        int,
          @RemainingQuantity  float,
          @StatusId           int,
          @InstructionId      int,
          @Count              int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Receiving_Manual_Palletisation_Create'
  
  exec @Error = p_Palletise_Reverse @ReceiptLineId = @ReceiptLineId
  
  if @error <> 0
  begin
    set @ErrorMsg = 'Re-palletise failed'
    goto error
  end
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'R'
     and Type       = 'R';
  
  select @ReceiptId          = r.ReceiptId,
         @WarehouseId        = r.WarehouseId,
         @PickLocationId     = r.LocationId,
         @StorageUnitBatchId = rl.StorageUnitBatchId,
         @AcceptedQuantity   = rl.AcceptedQuantity
    from ReceiptLine rl (nolock)
    join Receipt      r (nolock) on rl.ReceiptId = r.ReceiptId
    join Status       s (nolock) on rl.StatusId = s.StatusId
   where rl.ReceiptLineId = @ReceiptLineId
     and s.Type = 'R'
     and s.StatusCode in ('P','R')
  
  if @StorageUnitBatchId is null
    return -1
  
  select top 1 @PalletQuantity = p.Quantity
    from StorageUnitBatch sub (nolock)
    join Pack             p   (nolock) on sub.StorageUnitId = p.StorageUnitId
    join PackType         pt  (nolock) on p.PackTypeId      = pt.PackTypeId
   where StorageUnitBatchId = @StorageUnitBatchId
  order by InboundSequence
  
  begin transaction
  
  if (@AcceptedQuantity / @PalletQuantity) > @NumberOfLines
  begin
    set @Error = 100
    set @Errormsg = 'p_Receiving_Manual_Palletisation_Create - Cannot put more than a full pallet quantity onto single line'
    goto error
  end
    
  if @AcceptedQuantity < @NumberOfLines 
  begin
    set @Error = 200
    set @Errormsg = 'p_Receiving_Manual_Palletisation_Create - Cannot put a Quantity of ' + convert(nvarchar(10), @AcceptedQuantity) + ' on ' + convert(nvarchar(10), @NumberOfLines) + ' lines'
    goto error
  end
  
  if @AcceptedQuantity > @PalletQuantity
  begin
    set @PalletCount = floor(@AcceptedQuantity / @PalletQuantity)
    set @RemainingQuantity = @AcceptedQuantity - (@PalletQuantity * @PalletCount)
    
    if @PalletCount > 0
    begin
      while @PalletCount > 0
      begin
        set @PalletCount = @PalletCount - 1
        set @NumberOfLines = @NumberOfLines - 1
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @PickLocationId,
         @StoreLocationId     = null,
         @Quantity            = @PalletQuantity,
         @ReceiptLineId       = @ReceiptLineId
        
        if @error <> 0
          goto error
      end
    end
  end
  
  if @RemainingQuantity is null
  begin
    set @LineQuantity      = floor(@AcceptedQuantity / @NumberOfLines)
    set @RemainingQuantity = @AcceptedQuantity - (floor(@AcceptedQuantity / @NumberOfLines) * @NumberOfLines)
  end
  else
  begin
    set @LineQuantity = (@RemainingQuantity / @NumberOfLines)
    set @RemainingQuantity = @RemainingQuantity - (@LineQuantity * @NumberOfLines)
    set @NumberOfLines = @NumberOfLines
    
    select @LineQuantity, @NumberOfLines, @RemainingQuantity
  end
  
  while @NumberOfLines > 0
  begin
    set @NumberOfLines = @NumberOfLines -1
    
    set @PalletQuantity = @LineQuantity + @RemainingQuantity
    
    if @RemainingQuantity > 0
    begin
      set @RemainingQuantity = 0
    end
    
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = null,
     @Quantity            = @PalletQuantity,
     @ReceiptLineId       = @ReceiptLineId
    
    if @error <> 0
      goto error
  end
  
--  if @RemainingQuantity > 0
--  begin
--    exec @Error = p_Palletised_Insert
--     @WarehouseId         = @WarehouseId,
--     @OperatorId          = null,
--     @StorageUnitBatchId  = @StorageUnitBatchId,
--     @PickLocationId      = @PickLocationId,
--     @StoreLocationId     = null,
--     @Quantity            = @RemainingQuantity,
--     @ReceiptLineId       = @ReceiptLineId
--    
--    if @error <> 0
--      goto error
--  end
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'P' -- Palletised
     and Type       = 'R'
  
  exec @Error = p_ReceiptLine_Update
   @ReceiptLineId = @ReceiptLineId,
   @StatusId      = @StatusId
  
  if @error <> 0
    goto error
  
  exec @Error = p_Receipt_Update_Status
   @ReceiptId = @ReceiptId,
   @StatusId  = @StatusId
  
  if @error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @ErrorMsg
    rollback transaction
    return @Error
end
