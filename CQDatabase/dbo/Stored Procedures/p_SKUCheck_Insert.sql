﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKUCheck_Insert
  ///   Filename       : p_SKUCheck_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:33
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the SKUCheck table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null 
  /// </param>
  /// <returns>
  ///   SKUCheck.JobId,
  ///   SKUCheck.SKUCode,
  ///   SKUCheck.Quantity,
  ///   SKUCheck.ConfirmedQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKUCheck_Insert
(
 @JobId int = null,
 @SKUCode nvarchar(20) = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert SKUCheck
        (JobId,
         SKUCode,
         Quantity,
         ConfirmedQuantity)
  select @JobId,
         @SKUCode,
         @Quantity,
         @ConfirmedQuantity 
  
  select @Error = @@Error
  
  
  return @Error
  
end
