﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerHeader_Search
  ///   Filename       : p_ContainerHeader_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:54
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ContainerHeader table.
  /// </remarks>
  /// <param>
  ///   @ContainerHeaderId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ContainerHeader.ContainerHeaderId,
  ///   ContainerHeader.PalletId,
  ///   ContainerHeader.JobId,
  ///   ContainerHeader.ReferenceNumber,
  ///   ContainerHeader.PickLocationId,
  ///   ContainerHeader.StoreLocationId,
  ///   ContainerHeader.CreatedBy,
  ///   ContainerHeader.CreateDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerHeader_Search
(
 @ContainerHeaderId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ContainerHeaderId = '-1'
    set @ContainerHeaderId = null;
  
 
  select
         ContainerHeader.ContainerHeaderId
        ,ContainerHeader.PalletId
        ,ContainerHeader.JobId
        ,ContainerHeader.ReferenceNumber
        ,ContainerHeader.PickLocationId
        ,ContainerHeader.StoreLocationId
        ,ContainerHeader.CreatedBy
        ,ContainerHeader.CreateDate
    from ContainerHeader
   where isnull(ContainerHeader.ContainerHeaderId,'0')  = isnull(@ContainerHeaderId, isnull(ContainerHeader.ContainerHeaderId,'0'))
  
end
