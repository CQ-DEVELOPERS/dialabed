﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_Issue
  ///   Filename       : p_Location_Search_Issue.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Location_Search_Issue]
(
 @warehouseId        int,
 @outboundShipmentId int,
 @issueId            int
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  LocationId int,
	  Location   nvarchar(15)
	 )
	 
	 if @outboundShipmentId = -1
	   set @outboundShipmentId = null
  
  if @issueId = -1
    set @issueId = null
  
  insert @TableResult
        (LocationId,
	        Location)
  select -1     as 'LocationId',
         'None' as 'Location'
  
  insert @TableResult
        (LocationId,
	        Location)
  select distinct l.LocationId,
         l.Location
    from OutboundShipmentIssue osi (nolock)
    join OutboundShipment       os (nolock) on osi.OutboundShipmentId = os.OutboundShipmentId
    join Location                l (nolock) on os.LocationId = l.LocationId
   where os.WarehouseId        = @warehouseId
     and os.OutboundShipmentId = isnull(@outboundShipmentId, os.OutboundShipmentId)
     and osi.IssueId           = isnull(@issueId, osi.IssueId)
  
  --if @@rowcount = 0
    insert @TableResult
          (LocationId,
	          Location)
    select l.LocationId,
           l.Location
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId      = al.AreaId
      join Location      l (nolock) on al.LocationId = l.LocationId
     where a.WarehouseId = @warehouseId
       and a.AreaCode IN ('D', 'CK')
  
  select LocationId,
         Location
    from @TableResult
end
