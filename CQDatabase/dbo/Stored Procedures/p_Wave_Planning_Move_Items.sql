﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Move_Items
  ///   Filename       : p_Wave_Planning_Move_Items.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Move_Items
(
 @WarehouseId       int,
 @JobId             int,
 @PickLocationId    int,
 @StoreLocationid   int,
 @InstructionTypeId int,
 @PalletId          int
)
 
as
begin
	 set nocount on;
  
  exec p_Location_Update_Used
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @InstructionId      int,
          @PriorityId         int,
          @StatusId           int,
          @rowcount           int,
          @StorageUnitBatchId int,
          @Quantity           numeric(13,6),
          @Transaction        bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare @TableResult as table
  (
   StorageUnitBatchId int,
   Quantity           float
  )
  
  if @@trancount = 0
  begin
    begin transaction
    set @Transaction = 1
  end
  
  insert @TableResult
        (StorageUnitBatchId,
         Quantity)
  select StorageUnitBatchId,
         ActualQuantity - ReservedQuantity
    from StorageUnitBatchLocation subl (nolock)
   where LocationId = @PickLocationId
     and ActualQuantity > 0
     and ActualQuantity - ReservedQuantity > 0
  
  set @rowcount = @@rowcount
  
  select @PriorityId = PriorityId
    from InstructionType (nolock)
   where InstructionTypeId = @InstructionTypeId
  
  set @StatusId = dbo.ufn_StatusId('I','W')
  
  if @JobId is null
  begin
    exec @Error = p_Job_Insert
     @JobId       = @JobId output,
     @PriorityId  = @PriorityId,
     @OperatorId  = null,
     @StatusId    = @StatusId,
     @WarehouseId = @WarehouseId
  
    if @Error <> 0 or @JobId is null
      goto error
  end
  
  while @rowcount > 0
  begin
    set @rowcount = @rowcount - 1
    
    select @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = Quantity
      from @TableResult
    
    delete @TableResult where StorageUnitBatchId = @StorageUnitBatchId
    
    exec @Error = p_Instruction_Insert
     @InstructionId       = @InstructionId output,
     @InstructionTypeId   = @InstructionTypeId,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @WarehouseId         = @WarehouseId,
     @StatusId            = @StatusId,
     @JobId               = @JobId,
     @OperatorId          = null,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = @StoreLocationId,
     @InstructionRefId    = null,
     @Quantity            = @Quantity,
     @ConfirmedQuantity   = @Quantity,
     @CreateDate          = @GetDate,
     @PalletId            = @PalletId,
     @AutoComplete        = 1
    
    if @Error <> 0 or @InstructionId is null
    begin
      set @InstructionId = -1
      goto error
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
  end
  
  result:
    if @Transaction = 1
      commit transaction
    return 0
  
  error:
    if @Transaction = 1
    begin
      raiserror 900000 @Errormsg
      rollback transaction
    end
    return @Error
end
