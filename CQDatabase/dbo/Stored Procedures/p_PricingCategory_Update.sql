﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PricingCategory_Update
  ///   Filename       : p_PricingCategory_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:51:23
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the PricingCategory table.
  /// </remarks>
  /// <param>
  ///   @PricingCategoryId int = null,
  ///   @PricingCategory nvarchar(510) = null,
  ///   @PricingCategoryCode nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PricingCategory_Update
(
 @PricingCategoryId int = null,
 @PricingCategory nvarchar(510) = null,
 @PricingCategoryCode nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
  if @PricingCategoryId = '-1'
    set @PricingCategoryId = null;
  
  if @PricingCategory = '-1'
    set @PricingCategory = null;
  
  if @PricingCategoryCode = '-1'
    set @PricingCategoryCode = null;
  
	 declare @Error int
 
  update PricingCategory
     set PricingCategory = isnull(@PricingCategory, PricingCategory),
         PricingCategoryCode = isnull(@PricingCategoryCode, PricingCategoryCode) 
   where PricingCategoryId = @PricingCategoryId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
