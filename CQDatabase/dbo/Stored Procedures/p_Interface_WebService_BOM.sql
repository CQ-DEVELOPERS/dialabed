﻿Create procedure [dbo].[p_Interface_WebService_BOM]
(
 @XMLBody nvarchar(max) output
)
--with encryption
as
begin
	
	DECLARE @doc xml
	DECLARE @idoc int
	DECLARE @InsertDate datetime
	DECLARE @ERROR AS VARCHAR(255)
	
	SELECT @doc = convert(xml,@XMLBody)
	     , @InsertDate = Getdate()
	     
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
	BEGIN TRY
		                
	    INSERT [InterfaceImportBOM]
           ([RecordStatus],
            [InsertDate],
            [ProductCode],
            [Product],
            Quantity,
            PrimaryKey,
            [HostId])
	    SELECT 'W',
	           @InsertDate,
	           nodes.entity.value('ProductCode[1]',        'varchar(50)')  'ProductCode',
	           nodes.entity.value('Product[1]',            'varchar(255)') 'Product',
	           nodes.entity.value('Quantity[1]',           'Float')        'Quantity',
	           nodes.entity.value('PrimaryKey[1]',         'varchar(50)')  'PrimaryKey',
	           nodes.entity.value('PrimaryKey[1]',         'varchar(50)')  'HostId'
	       FROM @doc.nodes('/root/Body/Item') AS nodes(entity)
    	where nodes.entity.value('ProductCode[1]', 'varchar(50)') is not null
    	
	    INSERT [InterfaceImportBOMDetail]
           (InsertDate,
            InterfaceImportBOMId,
            ForeignKey,
            CompIndex,
            CompProductCode,
            CompProduct,
            CompQuantity)
     SELECT @InsertDate,
     	      b.InterfaceImportBOMID,
	           nodes.entity.value('ForeignKey[1]',         'varchar(50)')  'ForeignKey',
            nodes.entity.value('CompIndex[1]',          'int')          'CompIndex',
            nodes.entity.value('CompProductCode[1]',    'varchar(50)')  'CompProductCode',
            nodes.entity.value('CompProduct[1]',        'varchar(255)') 'CompProduct',
            nodes.entity.value('CompQuantity[1]',       'Float')        'CompQuantity'
       from @doc.nodes('/root/Body/Item/ItemLine') AS nodes(entity)
	      join InterfaceImportBOM b  on b.PrimaryKey = nodes.entity.value('ForeignKey[1]', 'varchar(50)') and b.InsertDate = @InsertDate
	     where b.RecordStatus = 'W'
    	
    	insert InterfaceImportBOMSpecialInstruction
    	      (InterfaceImportBOMId,
            ForeignKey,
            ProcedureLine1,
            ProcedureLine2,
            ProcedureLine3,
            ProcedureLine4,
            ProcedureLine5,
            ProcedureLine6,
            ProcedureLine7,
            ProcedureLine8,
            ProcedureLine9,
            ProcedureLine10,
            ProcedureLine11,
            ProcedureLine12,
            SpecialInstructionsLine1,
            SpecialInstructionsLine2,
            SpecialInstructionsLine3,
            SpecialInstructionsLine4,
            SpecialInstructionsLine5,
            SpecialInstructionsLine6,
            SpecialInstructionsLine7,
            SpecialInstructionsLine8,
            SpecialInstructionsLine9,
            SpecialInstructionsLine10,
            SpecialInstructionsLine11,
            SpecialInstructionsLine12)
     select distinct b.InterfaceImportBOMID,
            nodes.entity.value('ForeignKey[1]',                'varchar(50)')  'ForeignKey',
            nodes.entity.value('ProcedureLine1[1]',            'varchar(255)')  'ProcedureLine1',
            nodes.entity.value('ProcedureLine2[1]',            'varchar(255)')  'ProcedureLine2',
            nodes.entity.value('ProcedureLine3[1]',            'varchar(255)')  'ProcedureLine3',
            nodes.entity.value('ProcedureLine4[1]',            'varchar(255)')  'ProcedureLine4',
            nodes.entity.value('ProcedureLine5[1]',            'varchar(255)')  'ProcedureLine5',
            nodes.entity.value('ProcedureLine6[1]',            'varchar(255)')  'ProcedureLine6',
            nodes.entity.value('ProcedureLine7[1]',            'varchar(255)')  'ProcedureLine7',
            nodes.entity.value('ProcedureLine8[1]',            'varchar(255)')  'ProcedureLine8',
            nodes.entity.value('ProcedureLine9[1]',            'varchar(255)')  'ProcedureLine9',
            nodes.entity.value('ProcedureLine10[1]',           'varchar(255)')  'ProcedureLine10',
            nodes.entity.value('ProcedureLine11[1]',           'varchar(255)')  'ProcedureLine11',
            nodes.entity.value('ProcedureLine12[1]',           'varchar(255)')  'ProcedureLine12',
            nodes.entity.value('SpecialInstructionsLine1[1]',  'varchar(255)')  'SpecialInstructionsLine1',
            nodes.entity.value('SpecialInstructionsLine2[1]',  'varchar(255)')  'SpecialInstructionsLine2',
            nodes.entity.value('SpecialInstructionsLine3[1]',  'varchar(255)')  'SpecialInstructionsLine3',
            nodes.entity.value('SpecialInstructionsLine4[1]',  'varchar(255)')  'SpecialInstructionsLine4',
            nodes.entity.value('SpecialInstructionsLine5[1]',  'varchar(255)')  'SpecialInstructionsLine5',
            nodes.entity.value('SpecialInstructionsLine6[1]',  'varchar(255)')  'SpecialInstructionsLine6',
            nodes.entity.value('SpecialInstructionsLine7[1]',  'varchar(255)')  'SpecialInstructionsLine7',
            nodes.entity.value('SpecialInstructionsLine8[1]',  'varchar(255)')  'SpecialInstructionsLine8',
            nodes.entity.value('SpecialInstructionsLine9[1]',  'varchar(255)')  'SpecialInstructionsLine9',
            nodes.entity.value('SpecialInstructionsLine10[1]', 'varchar(255)')  'SpecialInstructionsLine10',
            nodes.entity.value('SpecialInstructionsLine11[1]', 'varchar(255)')  'SpecialInstructionsLine11',
            nodes.entity.value('SpecialInstructionsLine12[1]', 'varchar(255)')  'SpecialInstructionsLine12'
       from @doc.nodes('/root/Body/Item/Instructions') AS nodes(entity)
	      join InterfaceImportBOM b on b.PrimaryKey = nodes.entity.value('ForeignKey[1]', 'varchar(50)')
	                               and b.InsertDate = @InsertDate
	     where b.RecordStatus = 'W'
	       and nodes.entity.value('ProcedureLine1[1]',                'varchar(255)') is not null
    	
	    update bom
	       set RecordStatus = 'N'
	      from InterfaceImportBom        bom
	      join InterfaceImportBomDetail bomd on bomd.InterfaceImportBOMId = bom.InterfaceImportBomId 
	    where RecordSTatus = 'W' 
	      --and InsertDate = @InsertDate
	
	END TRY
	BEGIN CATCH

	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	   
	   PRINT @ERROR

	END CATCH
	
	exec p_Pastel_Import_BOM
	
	SET @XMLBody = '<root><status>' 
	    + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR
	           ELSE 'BOM successfully imported'
	      END
	    + '</status></root>'
End
