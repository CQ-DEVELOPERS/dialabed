﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_DropSequence
  ///   Filename       : p_Instruction_DropSequence.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_DropSequence
(
 @JobId int
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  Id            int identity
	 ,InstructionId int
	 )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @RelativeValue     int,
          @DropSequence      int,
          @Count             int,
          @InstructionId     int,
          @rowcount          int
  
  begin transaction
  
  if exists(select top 1 1
              from Instruction (nolock)
             where JobId         = @JobId
               and DropSequence is null)
  begin
    update Instruction
       set DropSequence = null
     where JobId = @JobId
    
    select @Error = @@Error, @Count = @@rowcount
    
    if @Error <> 0
      goto error
    
    insert @TableResult
          (InstructionId)
    select InstructionId
      from Instruction        i (nolock)
      join Location           l (nolock) on i.PickLocationId = l.LocationId
      --join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      --join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
     where i.JobId = @JobId
    order by --su.ProductCategory,
             l.RelativeValue,
             l.Location,
             i.InstructionId
      
    select @Error = @@Error, @Count = @@rowcount

    update i
       set DropSequence = tr.Id
      from Instruction i
      join @TableResult tr on i.InstructionId = tr.InstructionId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return @Error
  
  error:
    rollback transaction
    return 0
end
