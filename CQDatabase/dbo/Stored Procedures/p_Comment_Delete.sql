﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Delete
  ///   Filename       : p_Comment_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2014 08:27:23
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Comment table.
  /// </remarks>
  /// <param>
  ///   @CommentId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Delete
(
 @CommentId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Comment
     where CommentId = @CommentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
