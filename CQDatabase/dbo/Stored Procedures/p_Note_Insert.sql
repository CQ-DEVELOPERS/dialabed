﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Note_Insert
  ///   Filename       : p_Note_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:45
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Note table.
  /// </remarks>
  /// <param>
  ///   @NoteId int = null output,
  ///   @NoteCode nvarchar(510) = null,
  ///   @Note nvarchar(max) = null 
  /// </param>
  /// <returns>
  ///   Note.NoteId,
  ///   Note.NoteCode,
  ///   Note.Note 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Note_Insert
(
 @NoteId int = null output,
 @NoteCode nvarchar(510) = null,
 @Note nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
  if @NoteId = '-1'
    set @NoteId = null;
  
  if @NoteCode = '-1'
    set @NoteCode = null;
  
  if @Note = '-1'
    set @Note = null;
  
	 declare @Error int
 
  insert Note
        (NoteCode,
         Note)
  select @NoteCode,
         @Note 
  
  select @Error = @@Error, @NoteId = scope_identity()
  
  
  return @Error
  
end
