﻿
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Master_Import
  ///   Filename       : p_ExternalCompany_Master_Import.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Apr 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Master_Import
(
 @xmlstring nvarchar(max)
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   ExternalCompany                 nvarchar(max)    null,
   ExternalCompanyCode             nvarchar(max)    null,
   ExternalCompanyTypeCode         nvarchar(max)    null,
   PricingCategoryCode             nvarchar(max)    null,
   LabelingCategoryCode            nvarchar(max)    null,
   ContainerTypeCode               nvarchar(max)    null,
   RouteCode                       nvarchar(max)    null,
   HostId                          nvarchar(max)    null,
   Rating                          int              null,
   Backorder                       bit              null,
   RedeliveryIndicator             bit              null,
   QualityAssuranceIndicator       bit              null,
   OrderLineSequence               bit              null,
   
   ExternalCompanyTypeId           int null,
   PricingCategoryId               int null,
   LabelingCategoryId              int null,
   ContainerTypeId                 int null,
   RouteId                         int null
  )
  
  declare @DesktopLogId int
  
  insert DesktopLog
        (ProcName,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         getdate()
  
  select @DesktopLogId = scope_identity()
  
  declare @Error                   int = 0,
          @Errormsg                varchar(max),
          @GetDate                 datetime,
          @command                 varchar(max),
          @InterfaceImportHeaderId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Errormsg = 'p_ExternalCompany_Master_Import - Error executing procedure'
  
  begin transaction
  
  if @xmlstring != ''
  begin
    declare @doc xml

    set @doc = convert(xml,@xmlstring)

    DECLARE @idoc int,
            @InsertDate datetime

    Set @InsertDate = Getdate()

    EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
    
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error inserting into @TableResult'
    
		  insert @TableResult
								  (ExternalCompany,
           ExternalCompanyCode,
           ExternalCompanyTypeCode,
           PricingCategoryCode,
           LabelingCategoryCode,
           ContainerTypeCode,
           RouteCode,
           HostId,
           Rating,
           Backorder,
           RedeliveryIndicator,
           QualityAssuranceIndicator,
           OrderLineSequence)
	   select ExternalCompany,
           ExternalCompanyCode,
           ExternalCompanyTypeCode,
           PricingCategoryCode,
           LabelingCategoryCode,
           ContainerTypeCode,
           RouteCode,
           HostId,
           case when isnumeric(Rating) = 1
                then Rating
                else null
                end,
           case when Backorder in ('1','true')
                then 1
                else 0
                end,
           case when RedeliveryIndicator in ('1','true')
                then 1
                else 0
                end,
           case when QualityAssuranceIndicator in ('1','true')
                then 1
                else 0
                end,
           case when OrderLineSequence in ('1','true')
                then 1
                else 0
                end
           
		    from OPENXML (@idoc, '/root/Line',1)
              WITH (
                    ExternalCompany           nvarchar(50) 'ExternalCompany',
                    ExternalCompanyCode       nvarchar(50) 'ExternalCompanyCode',
                    ExternalCompanyTypeCode   nvarchar(50) 'ExternalCompanyTypeCode',
                    PricingCategoryCode       nvarchar(50) 'PricingCategoryCode',
                    LabelingCategoryCode      nvarchar(50) 'LabelingCategoryCode',
                    ContainerTypeCode         nvarchar(50) 'ContainerTypeCode',
                    RouteCode                 nvarchar(50) 'RouteCode',
                    HostId                    nvarchar(50) 'HostId',
                    Rating                    nvarchar(50) 'Rating',
                    Backorder                 nvarchar(50) 'Backorder',
                    RedeliveryIndicator       nvarchar(50) 'RedeliveryIndicator',
                    QualityAssuranceIndicator nvarchar(50) 'QualityAssuranceIndicator',
                    OrderLineSequence         nvarchar(50) 'OrderLineSequence'
                    ) 
      where PricingCategoryCode not like '%ExternalCompany%'
    
    if @Error != 0
    begin
      set @Errormsg = 'p_ExternalCompany_Master_Import - Error inserting into @TableResult'
      goto error
    end
  end
  
  update tr
     set ExternalCompanyTypeId = a.ExternalCompanyTypeId
    from @TableResult tr
    join ExternalCompanyType a (nolock) on tr.ExternalCompanyTypeCode = a.ExternalCompanyTypeCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating @TableResult'
    goto error
  end
  
  update tr
     set PricingCategoryId = a.PricingCategoryId
    from @TableResult tr
    join PricingCategory a (nolock) on tr.PricingCategoryCode = a.PricingCategoryCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating @TableResult'
    goto error
  end
  
  update tr
     set LabelingCategoryId = a.LabelingCategoryId
    from @TableResult tr
    join LabelingCategory a (nolock) on tr.LabelingCategoryCode = a.LabelingCategoryCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating @TableResult'
    goto error
  end
  
  update tr
     set ContainerTypeId = a.ContainerTypeId
    from @TableResult tr
    join ContainerType a (nolock) on tr.ContainerTypeCode = a.ContainerTypeCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating @TableResult'
    goto error
  end
  
  update tr
     set RouteId = a.RouteId
    from @TableResult tr
    join Route a (nolock) on tr.RouteCode = a.RouteCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating @TableResult'
    goto error
  end
  
  update ec
     set ExternalCompany = tr.ExternalCompany,
         ExternalCompanyTypeId = tr.ExternalCompanyTypeId,
         PricingCategoryId = tr.PricingCategoryId,
         LabelingCategoryId = tr.LabelingCategoryId,
         ContainerTypeId = tr.ContainerTypeId,
         RouteId = tr.RouteId,
         HostId = tr.HostId,
         Rating = tr.Rating,
         Backorder = tr.Backorder,
         RedeliveryIndicator = tr.RedeliveryIndicator,
         QualityAssuranceIndicator = tr.QualityAssuranceIndicator,
         OrderLineSequence = tr.OrderLineSequence
     from @TableResult tr
     join ExternalCompany ec on tr.ExternalCompanyCode = ec.ExternalCompanyCode
                            and tr.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating ExternalCompany'
    goto error
  end
  
  insert ExternalCompany
        (ExternalCompany,
         ExternalCompanyCode,
         ExternalCompanyTypeId,
         PricingCategoryId,
         LabelingCategoryId,
         ContainerTypeId,
         RouteId,
         HostId,
         Rating,
         Backorder,
         RedeliveryIndicator,
         QualityAssuranceIndicator,
         OrderLineSequence)
  select tr.ExternalCompany,
         tr.ExternalCompanyCode,
         tr.ExternalCompanyTypeId,
         tr.PricingCategoryId,
         tr.LabelingCategoryId,
         tr.ContainerTypeId,
         tr.RouteId,
         tr.HostId,
         tr.Rating,
         tr.Backorder,
         tr.RedeliveryIndicator,
         tr.QualityAssuranceIndicator,
         tr.OrderLineSequence
    from @TableResult tr
    left
    join ExternalCompany ec on tr.ExternalCompanyCode = ec.ExternalCompanyCode
                           and tr.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
   where ec.ExternalCompanyId is null
     and tr.ExternalCompanyTypeId is not null
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error inserting into ExternalCompany'
    goto error
  end
  select * from @TableResult
  commit transaction
  
  update DesktopLog
     set Errormsg = 'Successful',
         EndDate  = getdate()
   where DesktopLogId = @DesktopLogId
  
  return 0
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    
    update DesktopLog
       set Errormsg = isnull(@xmlstring, 'Error executing ' + OBJECT_NAME(@@PROCID)),
           EndDate  = getdate()
     where DesktopLogId = @DesktopLogId
    
    return @Error
end

