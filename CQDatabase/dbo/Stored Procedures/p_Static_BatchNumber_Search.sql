﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_BatchNumber_Search
  ///   Filename       : p_Static_BatchNumber_Search.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 26 February 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_BatchNumber_Search
(
 @WarehouseId	int,	
 @Batch         nvarchar(50) = null,
 @BatchReferenceNumber nvarchar(50) = null
)
 
as
begin
	 set nocount on;
	   SELECT BatchId,
           StatusId,
           WarehouseId,
           Batch,
           CreateDate,
           ExpiryDate,
           IncubationDays,
           ShelfLifeDays,
           ExpectedYield,
           ActualYield,
           ECLNumber,
           BatchReferenceNumber,
           ProductionDateTime,
           FilledYield,
           isnull(COACertificate,0) as COACertificate,
           StatusModifiedBy,
           StatusModifiedDate,
           CreatedBy,
           ModifiedBy,
           ModifiedDate
      FROM Batch
     where 
		--WarehouseId = @WarehouseId and
        isnull(Batch,'%')                like '%' + isnull(@Batch, isnull(Batch,'%')) + '%'
       and isnull(BatchReferenceNumber,'%') like '%' + isnull(@BatchReferenceNumber, isnull(BatchReferenceNumber,'%')) + '%'
end
