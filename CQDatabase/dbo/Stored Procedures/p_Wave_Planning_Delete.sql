﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Delete
  ///   Filename       : p_Wave_Planning_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2013 17:32:28
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Wave table.
  /// </remarks>
  /// <param>
  ///   @WaveId int = null output,
  ///   @Wave nvarchar(100) = null,
  ///   @CreateDate datetime = null,
  ///   @ReleasedDate datetime = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   Wave.WaveId,
  ///   Wave.Wave,
  ///   Wave.CreateDate,
  ///   Wave.ReleasedDate,
  ///   Wave.StatusId,
  ///   Wave.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Delete
(
 @WaveId int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if exists(select top 1 1
              from Issue
             where WaveId = @WaveId)
  begin
    set @Error = -1
    goto error
  end
  
  exec @Error = p_Wave_Delete
   @WaveId       = @WaveId
  
  select @Error = @@Error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Wave_Planning_Delete'
    rollback transaction
    return @Error
  
end
 
