﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_PalletId_SUBL
  ///   Filename       : p_Report_PalletId_SUBL.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Dec 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_PalletId_SUBL
(
 @StorageUnitBatchId int,
 @LocationId         int
)
 
as
begin
  set nocount on;
  
  select 'P:' + convert(nvarchar(10), PalletId) as 'PalletId', Quantity
    from Pallet (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
     and LocationId = @LocationId
end
