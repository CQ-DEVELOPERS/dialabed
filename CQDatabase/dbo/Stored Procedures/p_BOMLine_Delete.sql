﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMLine_Delete
  ///   Filename       : p_BOMLine_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:11
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the BOMLine table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMLine_Delete
(
 @BOMLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete BOMLine
     where BOMLineId = @BOMLineId
  
  select @Error = @@Error
  
  
  return @Error
  
end
