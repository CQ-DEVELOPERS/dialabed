﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_List
  ///   Filename       : p_InboundShipment_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:20
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundShipment table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InboundShipment.InboundShipmentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InboundShipmentId
        ,null as 'InboundShipment'
  union
  select
         InboundShipment.InboundShipmentId
        ,InboundShipment.InboundShipmentId as 'InboundShipment'
    from InboundShipment
  
end
