﻿
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Export_Extract
  ///   Filename       : p_Plascon_Export_Extract.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Plascon_Export_Extract]
(
 @RecordType varchar(30) = null
)
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'p_Plascon_Export_Extract - Error executing p_Plascon_Export_Extract'
  
  begin transaction
  
  update InterfaceExtract
     set RecordStatus  = 'Y',
         ProcessedDate = @getdate,
         ExtractCounter = isnull(ExtractCounter, 0) + 1
   where RecordStatus = 'N'
     and RecordType   = isnull(@RecordType,RecordType)
  
  select @Error = @@Error, @Errormsg = 'p_Plascon_Export_Extract - Error updating InterfaceExtract'
  
  if @Error <> 0
    goto error
  
  select Distinct 
		 Data
		,CASE RecordType
			WHEN 'MLT' THEN 'mlttoh.tmp'
			WHEN 'MPR' THEN 'mprtoh.tmp'
			WHEN 'MSK' THEN 'msktoh.tmp'
			WHEN 'MSS' THEN 'msstoh.tmp'
			WHEN 'MTR' THEN 'mtrtoh.tmp'
			WHEN 'MPC' THEN 'mpctoh.tmp'
			ELSE 'tohost.tmp'
		END As FileName
    from InterfaceExtract
   where ProcessedDate = @GetDate
   order by 
		CASE RecordType
			WHEN 'MLT' THEN 'mlttoh.tmp'
			WHEN 'MPR' THEN 'mprtoh.tmp'
			WHEN 'MSK' THEN 'msktoh.tmp'
			WHEN 'MSS' THEN 'msstoh.tmp'
			WHEN 'MTR' THEN 'mtrtoh.tmp'
			WHEN 'MPC' THEN 'mpctoh.tmp'
			ELSE 'tohost.tmp'
		END
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error  
end

