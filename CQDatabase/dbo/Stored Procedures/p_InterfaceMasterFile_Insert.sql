﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMasterFile_Insert
  ///   Filename       : p_InterfaceMasterFile_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:29:19
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceMasterFile table.
  /// </remarks>
  /// <param>
  ///   @MasterFileId int = null output,
  ///   @MasterFile varchar(50) = null,
  ///   @MasterFileDescription varchar(255) = null,
  ///   @LastDate datetime = null,
  ///   @SendFlag bit = null 
  /// </param>
  /// <returns>
  ///   InterfaceMasterFile.MasterFileId,
  ///   InterfaceMasterFile.MasterFile,
  ///   InterfaceMasterFile.MasterFileDescription,
  ///   InterfaceMasterFile.LastDate,
  ///   InterfaceMasterFile.SendFlag 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMasterFile_Insert
(
 @MasterFileId int = null output,
 @MasterFile varchar(50) = null,
 @MasterFileDescription varchar(255) = null,
 @LastDate datetime = null,
 @SendFlag bit = null 
)
 
as
begin
	 set nocount on;
  
  if @MasterFileId = '-1'
    set @MasterFileId = null;
  
	 declare @Error int
 
  insert InterfaceMasterFile
        (MasterFile,
         MasterFileDescription,
         LastDate,
         SendFlag)
  select @MasterFile,
         @MasterFileDescription,
         @LastDate,
         @SendFlag 
  
  select @Error = @@Error, @MasterFileId = scope_identity()
  
  
  return @Error
  
end
