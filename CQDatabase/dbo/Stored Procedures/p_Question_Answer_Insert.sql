﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Question_Answer_Insert
  ///   Filename       : p_Question_Answer_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Question_Answer_Insert](
		@OrderNumber					 nvarchar(30),
		@jobId                           int,
		@palletId                        int = null,
		@ReceiptId						 int = null,
		@questionId                      int,
		@answer                          nvarchar(50),
		@dateAsked						 datetime = null
 )

as
begin
	 set nocount on;

  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  

  begin transaction
	If Exists(Select top 1 1 from QuestionAnswers where QuestionId = @QuestionId and orderNumber = @OrderNumber and JobId = @Jobid 
			and Isnull(PalletId,1) = Isnull(@PalletId,Isnull(Palletid,1)) and isnull(ReceiptId,1) = Isnull(@ReceiptID,Isnull(ReceiptId,1)))
		Begin
			Set @Error = 9001
			Goto error
		End
	if @JobId = -1 set @JobId = null
	If @PalletId = -1 set @PalletId = null
	If @ReceiptId = -1 set @ReceiptId = null

	Insert Into QuestionAnswers (OrderNumber,QuestionId,Answer,JobId,ReceiptId,PalletId,DateAsked) 
Values (@OrderNumber,@QuestionId,@Answer,@JobId,@ReceiptId,@PalletId,Isnull(@DateAsked,Getdate()))
  
  
  
	if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Question_Answer_Insert'
    rollback transaction
    return @Error
end