﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_List_Principals
  ///   Filename       : p_List_Principals.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2013
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Principal table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_List_Principals
as
begin
	 set nocount on
	 declare @Error int
  
 
  select
         PrincipalId,
         PrincipalCode,
         Principal 
    from Principal
  order by Principal 
  
end
