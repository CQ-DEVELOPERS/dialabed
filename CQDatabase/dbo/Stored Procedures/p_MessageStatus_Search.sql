﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MessageStatus_Search
  ///   Filename       : p_MessageStatus_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:06
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the MessageStatus table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   MessageStatus.StatusId,
  ///   MessageStatus.StatusCode,
  ///   MessageStatus.StatusDesc 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MessageStatus_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         MessageStatus.StatusId
        ,MessageStatus.StatusCode
        ,MessageStatus.StatusDesc
    from MessageStatus
  
end
