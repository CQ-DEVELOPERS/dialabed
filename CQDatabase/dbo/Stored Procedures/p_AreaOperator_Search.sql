﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperator_Search
  ///   Filename       : p_AreaOperator_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:46
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the AreaOperator table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null output,
  ///   @OperatorId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   AreaOperator.AreaId,
  ///   AreaOperator.OperatorId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperator_Search
(
 @AreaId int = null output,
 @OperatorId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
 
  select
         AreaOperator.AreaId
        ,AreaOperator.OperatorId
    from AreaOperator
   where isnull(AreaOperator.AreaId,'0')  = isnull(@AreaId, isnull(AreaOperator.AreaId,'0'))
     and isnull(AreaOperator.OperatorId,'0')  = isnull(@OperatorId, isnull(AreaOperator.OperatorId,'0'))
  
end
