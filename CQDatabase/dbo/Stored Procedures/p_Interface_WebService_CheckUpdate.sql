﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_CheckUpdate
  ///   Filename       : p_Interface_WebService_CheckUpdate.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Aug 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Interface_WebService_CheckUpdate
(
	@XMLBody nvarchar(max) output
)
--with encryption
as
begin
	 set nocount on;
  
	set nocount on;
  
	DECLARE @doc xml
	DECLARE @idoc int
	DECLARE @InsertDate datetime
	DECLARE @ERROR AS nvarchar(255)
	DECLARE @InterfaceImportHeaderId int
    DECLARE @OperatorId int
	DECLARE @JobId int
	DECLARE @ReferenceNumber nvarchar(30)
	DECLARE @NewBox bit
	
	DECLARE @CheckingTable AS Table
	(
	     Status             Char(1)
	    ,InsertDate         DateTime
	    ,StorageUnitBatchId int
	    ,ProductCode        nvarchar(100)
	    ,Product            nvarchar(255)
	    ,SKUCode            nvarchar(10)
	    ,Batch              nvarchar(50)
	    ,ExpectedQuantity   int	
	    ,PickedQuantity	    int
	    ,CheckedQuantity    int
	)
	
	SELECT @doc = convert(xml,@XMLBody)
	     , @InsertDate = Getdate()
	     
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
		
	BEGIN TRY
	
	    select @OperatorId = OperatorId
	    from Job
	    where JobId = @JobId
		
		select @JobId           = nodes.entity.value('JobId[1]',            'int')       
		      ,@ReferenceNumber = nodes.entity.value('ReferenceNumber[1]',  'nvarchar(30)')
		      ,@NewBox          = nodes.entity.value('NewBox[1]',           'bit')        
		FROM
	           @doc.nodes('/root') AS nodes(entity)
		                
	    INSERT INTO @CheckingTable
	    SELECT  'W'
	           ,@InsertDate
	           ,nodes.entity.value('StorageUnitBatchId[1]', 'int')           'StorageUnitBatchId'
	           ,nodes.entity.value('ProductCode[1]',        'nvarchar(30)')  'ProductCode'
	           ,nodes.entity.value('Product[1]',			'nvarchar(30)')  'Product'
	           ,nodes.entity.value('SKUCode[1]',		    'nvarchar(30)')  'SKUCode'
               ,nodes.entity.value('Batch[1]',		        'nvarchar(30)')  'Batch'
               ,nodes.entity.value('ExpectedQuantity[1]',   'nvarchar(30)')  'ExpectedQuantity'
               ,nodes.entity.value('PickedQuantity[1]',     'nvarchar(30)')  'PickedQuantity'
               ,nodes.entity.value('CheckedQuantity[1]',    'nvarchar(30)')  'CheckedQuantity'
	    FROM
	            @doc.nodes('/root/Instruction') AS nodes(entity)
	            
        --select @JobId
        --      ,@ReferenceNumber
        --      ,@NewBox
              
        --select *
        --from @CheckingTable        
        
        update i
        set CheckQuantity = ct.CheckedQuantity
        from Instruction i
        inner join @CheckingTable ct on i.StorageUnitBatchId = ct.StorageUnitBatchId
        where i.JobId = @JobId
    	
    	If @NewBox = 1
    	begin
    	    exec @JobId = p_Mobile_Product_Check_New_Box
                         @JobId           = @JobId,
                         @operatorId      = @OperatorId,
                         @ReferenceNumber = @ReferenceNumber
            
            Declare @XML nvarchar(MAX)
                 
            IF (ISNULL(@ReferenceNumber,'') <> '')
                select @XML =  '<root><Header><ReferenceNumber>' + @ReferenceNumber + '</ReferenceNumber></Header></root>'
            ELSE
                select @XML =  '<root><Header><ReferenceNumber>J:' + CONVERT(VARCHAR(20), @JobId) + '</ReferenceNumber></Header></root>'
            
			exec p_Interface_WebService_CheckGet @XML OUT
			
			SET @xmlBody = @xml
    	end
    	else
    	begin
    	    exec @Error = p_Status_Rollup @JobId = @JobId
    		SET @XMLBody = '<root><status>Success</status></root>'
    	end
    		
	END TRY
	BEGIN CATCH
	    
	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	   SET @XMLBody = '<root><status>'+@ERROR+'</status></root>'
	END CATCH
end
