﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COADetail_Insert
  ///   Filename       : p_COADetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:21
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the COADetail table.
  /// </remarks>
  /// <param>
  ///   @COADetailId int = null output,
  ///   @COAId int = null,
  ///   @NoteId int = null,
  ///   @NoteCode nvarchar(510) = null,
  ///   @Note nvarchar(max) = null 
  /// </param>
  /// <returns>
  ///   COADetail.COADetailId,
  ///   COADetail.COAId,
  ///   COADetail.NoteId,
  ///   COADetail.NoteCode,
  ///   COADetail.Note 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COADetail_Insert
(
 @COADetailId int = null output,
 @COAId int = null,
 @NoteId int = null,
 @NoteCode nvarchar(510) = null,
 @Note nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
  if @COADetailId = '-1'
    set @COADetailId = null;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @NoteId = '-1'
    set @NoteId = null;
  
	 declare @Error int
 
  insert COADetail
        (COAId,
         NoteId,
         NoteCode,
         Note)
  select @COAId,
         @NoteId,
         @NoteCode,
         @Note 
  
  select @Error = @@Error, @COADetailId = scope_identity()
  
  
  return @Error
  
end
