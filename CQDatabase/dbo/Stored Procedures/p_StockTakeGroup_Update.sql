﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeGroup_Update
  ///   Filename       : p_StockTakeGroup_Update.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2011
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StockTakeReference table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeGroup_Update
(
 @StockTakeGroupCode		varchar(20) = null,
 @StockTakeGroup			varchar(50) = null,
 @StockTakeGroupId			int
)
 
as
begin
	 set nocount on;
  
  declare @Error int
  update StockTakeGroup
     set StockTakeGroup = @StockTakeGroup,
         StockTakeGroupCode = @StockTakeGroupCode
   where StockTakeGroupId = @StockTakeGroupId
   
  select @Error = @@Error
  
end
