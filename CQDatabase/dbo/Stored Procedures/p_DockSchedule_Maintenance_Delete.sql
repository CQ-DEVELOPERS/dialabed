﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DockSchedule_Maintenance_Delete
  ///   Filename       : p_DockSchedule_Maintenance_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DockSchedule_Maintenance_Delete
(
 @DockScheduleId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  DELETE FROM [DockSchedule] WHERE [DockScheduleId] = @DockScheduleId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_DockSchedule_Maintenance_Delete'
    rollback transaction
    return @Error
end
