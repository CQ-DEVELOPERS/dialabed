﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mixed_Job_Unlink_Instruction
  ///   Filename       : p_Mixed_Job_Unlink_Instruction.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mixed_Job_Unlink_Instruction
(
 @JobId         int,
 @InstructionId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @New_JobId         int,
          @Old_JobId         int,
          @Old_PriorityId    int,
          @Old_OperatorId    int,
          @Old_StatusId      int,
          @Old_WarehouseId   int,
          @Old_ReceiptLineId int,
          @Old_IssueLineId   int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @Old_JobId         = JobId,
         @Old_PriorityId    = PriorityId,
         @Old_OperatorId    = OperatorId,
         @Old_StatusId      = StatusId,
         @Old_WarehouseId   = WarehouseId,
         @Old_ReceiptLineId = ReceiptLineId,
         @Old_IssueLineId   = IssueLineId
     from Job (nolock)
    where JobId = @JobId
  
  begin transaction
  
  exec @Error = p_Job_Insert
   @JobId         = @New_JobId output,
   @PriorityId    = @Old_PriorityId,
   @OperatorId    = @Old_OperatorId,
   @StatusId      = @Old_StatusId,
   @WarehouseId   = @Old_WarehouseId,
   @ReceiptLineId = @Old_ReceiptLineId,
   @IssueLineId   = @Old_IssueLineId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @JobId         = @New_JobId
  
  if @Error <> 0
    goto error
  
  if (select count(1) from Job where JobId = @JobId) > 1
  begin
    exec @Error = p_Job_Delete
     @JobId         = @JobId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Mixed_Job_Unlink_Instruction'
    rollback transaction
    return @Error
end
