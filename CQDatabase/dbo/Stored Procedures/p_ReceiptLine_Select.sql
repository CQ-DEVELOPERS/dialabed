﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLine_Select
  ///   Filename       : p_ReceiptLine_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2013 07:02:53
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ReceiptLine table.
  /// </remarks>
  /// <param>
  ///   @ReceiptLineId int = null 
  /// </param>
  /// <returns>
  ///   ReceiptLine.ReceiptLineId,
  ///   ReceiptLine.ReceiptId,
  ///   ReceiptLine.InboundLineId,
  ///   ReceiptLine.StorageUnitBatchId,
  ///   ReceiptLine.StatusId,
  ///   ReceiptLine.OperatorId,
  ///   ReceiptLine.RequiredQuantity,
  ///   ReceiptLine.ReceivedQuantity,
  ///   ReceiptLine.AcceptedQuantity,
  ///   ReceiptLine.RejectQuantity,
  ///   ReceiptLine.DeliveryNoteQuantity,
  ///   ReceiptLine.SampleQuantity,
  ///   ReceiptLine.AcceptedWeight,
  ///   ReceiptLine.ChildStorageUnitBatchId,
  ///   ReceiptLine.NumberOfPallets,
  ///   ReceiptLine.AssaySamples,
  ///   ReceiptLine.BOELineNumber,
  ///   ReceiptLine.CountryofOrigin,
  ///   ReceiptLine.TariffCode,
  ///   ReceiptLine.Reference1,
  ///   ReceiptLine.Reference2,
  ///   ReceiptLine.UnitPrice 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLine_Select
(
 @ReceiptLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ReceiptLine.ReceiptLineId
        ,ReceiptLine.ReceiptId
        ,ReceiptLine.InboundLineId
        ,ReceiptLine.StorageUnitBatchId
        ,ReceiptLine.StatusId
        ,ReceiptLine.OperatorId
        ,ReceiptLine.RequiredQuantity
        ,ReceiptLine.ReceivedQuantity
        ,ReceiptLine.AcceptedQuantity
        ,ReceiptLine.RejectQuantity
        ,ReceiptLine.DeliveryNoteQuantity
        ,ReceiptLine.SampleQuantity
        ,ReceiptLine.AcceptedWeight
        ,ReceiptLine.ChildStorageUnitBatchId
        ,ReceiptLine.NumberOfPallets
        ,ReceiptLine.AssaySamples
        ,ReceiptLine.BOELineNumber
        ,ReceiptLine.CountryofOrigin
        ,ReceiptLine.TariffCode
        ,ReceiptLine.Reference1
        ,ReceiptLine.Reference2
        ,ReceiptLine.UnitPrice
		,ReceiptLine.ReceivedDate
		,ReceiptLine.PutawayStarted
		,ReceiptLine.PutawayEnded 
    from ReceiptLine
   where isnull(ReceiptLine.ReceiptLineId,'0')  = isnull(@ReceiptLineId, isnull(ReceiptLine.ReceiptLineId,'0'))
  
end
