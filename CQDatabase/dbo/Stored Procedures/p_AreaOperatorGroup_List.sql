﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperatorGroup_List
  ///   Filename       : p_AreaOperatorGroup_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:44
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaOperatorGroup table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   AreaOperatorGroup.AreaId,
  ///   AreaOperatorGroup.OperatorGroupId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperatorGroup_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as AreaId
        ,null as 'AreaOperatorGroup'
        ,'-1' as OperatorGroupId
        ,null as 'AreaOperatorGroup'
  union
  select
         AreaOperatorGroup.AreaId
        ,AreaOperatorGroup.AreaId as 'AreaOperatorGroup'
        ,AreaOperatorGroup.OperatorGroupId
        ,AreaOperatorGroup.OperatorGroupId as 'AreaOperatorGroup'
    from AreaOperatorGroup
  
end
