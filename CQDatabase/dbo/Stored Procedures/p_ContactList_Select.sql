﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactList_Select
  ///   Filename       : p_ContactList_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:18:15
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ContactList table.
  /// </remarks>
  /// <param>
  ///   @ContactListId int = null 
  /// </param>
  /// <returns>
  ///   ContactList.ContactListId,
  ///   ContactList.ExternalCompanyId,
  ///   ContactList.OperatorId,
  ///   ContactList.ContactPerson,
  ///   ContactList.Telephone,
  ///   ContactList.Fax,
  ///   ContactList.EMail,
  ///   ContactList.Alias,
  ///   ContactList.Type,
  ///   ContactList.ReportTypeId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContactList_Select
(
 @ContactListId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ContactList.ContactListId
        ,ContactList.ExternalCompanyId
        ,ContactList.OperatorId
        ,ContactList.ContactPerson
        ,ContactList.Telephone
        ,ContactList.Fax
        ,ContactList.EMail
        ,ContactList.Alias
        ,ContactList.Type
        ,ContactList.ReportTypeId
    from ContactList
   where isnull(ContactList.ContactListId,'0')  = isnull(@ContactListId, isnull(ContactList.ContactListId,'0'))
  order by ContactPerson
  
end
