﻿
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Update_Check
  ///   Filename       : p_InboundDocument_Update_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007 15:29:06
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null,
  ///   @InboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(30) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Update_Check
(
 @InboundDocumentId int = null,
 @InboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(30) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @ReferenceNumber nvarchar(30) = null,
 @ReasonId int = null,
 @Remarks nvarchar(255) = null,
 @ContactListId int = null
)
 
as
begin
	 set nocount on;
  
	 declare @Error int,
          @Rowcount int
  
  if @ExternalCompanyId = 0
    set @ExternalCompanyId = null
  
  if @ReferenceNumber is null
    update InboundDocument
       set ReferenceNumber = null
     where InboundDocumentId = @InboundDocumentId
  
  if @ReasonId = -1
    set @ReasonId = null
  
  update InboundDocument
     set ReasonId = @ReasonId
   where InboundDocumentId = @InboundDocumentId 
  
  exec @Error = p_InboundDocument_Update
   @InboundDocumentId     = @InboundDocumentId,
   @InboundDocumentTypeId = @InboundDocumentTypeId,
   @ExternalCompanyId     = @ExternalCompanyId,
   @StatusId              = @StatusId,
   @WarehouseId           = @WarehouseId,
   @OrderNumber           = @OrderNumber,
   @DeliveryDate          = @DeliveryDate,
   @ReferenceNumber       = @ReferenceNumber,
   @Remarks               = @Remarks
   
  exec @Error = p_InboundDocumentContactList_Update
   @InboundDocumentId     = @InboundDocumentId,
   @ContactListId		  = @ContactListId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Rowcount = 0
  begin
    set @Error = -1
    goto error
  end
  
  exec @Error = p_InboundDocument_Update_Reason
   @InboundDocumentId = @InboundDocumentId,
   @ReasonId          = @ReasonId
  
  if @Error != 0
    goto error
  
  error:
  return @Error
  
end
