﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Renumber
  ///   Filename       : p_Job_Renumber.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Renumber
(
 @JobId int
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   OutboundShipmentId int,
   IssueId            int
  )
  
  declare @TableDetail as table
  (
   Id                 int identity primary key,
   JobId              int
  )
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @rowcount          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  insert @TableHeader
        (OutboundShipmentId,
         IssueId)
  select distinct isnull(ili.OutboundShipmentId,-1),
         ili.IssueId
    from Instruction            i (nolock)
    join IssueLineInstruction ili (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
   where i.JobId = @JobId
  
  if (select dbo.ufn_Configuration(221, 1)) = 1
  begin
    insert @TableDetail
          (JobId)
    select distinct j.JobId
      from @TableHeader          th
      join IssueLineInstruction ili (nolock) on isnull(th.IssueId,ili.IssueId) = ili.IssueId
      join Instruction            i (nolock) on ili.InstructionId     = isnull(i.InstructionRefId, i.InstructionId)
      join Job                    j (nolock) on i.JobId               = j.JobId
     where i.ConfirmedQuantity > 0
    order by j.JobId
  end
  else
  begin
    update @TableHeader
       set IssueId = null
     where OutboundShipmentId != -1
     
    insert @TableDetail
          (JobId)
    select distinct j.JobId
      from @TableHeader          th
      join IssueLineInstruction ili (nolock) on th.OutboundShipmentId = isnull(ili.OutboundShipmentId, -1)
                                            and isnull(th.IssueId,ili.IssueId) = ili.IssueId
      join Instruction            i (nolock) on ili.InstructionId     = isnull(i.InstructionRefId, i.InstructionId)
      join Job                    j (nolock) on i.JobId               = j.JobId
     where i.ConfirmedQuantity > 0
    order by j.JobId
  end
  
  select @rowcount = @@rowcount
  
  begin transaction
  
  update j
     set DropSequence = td.Id,
         Pallets      = @rowcount
    from @TableDetail td
    join Job           j (nolock) on td.JobId = j.JobId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Job_Renumber'
    rollback transaction
    return @Error
end
