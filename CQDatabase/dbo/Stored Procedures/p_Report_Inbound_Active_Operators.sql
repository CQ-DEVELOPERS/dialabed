﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Inbound_Active_Operators
  ///   Filename       : p_Report_Inbound_Active_Operators.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Inbound_Active_Operators
(
 @WarehouseId int,
 @Production  bit = null
)
 
as
begin
	 set nocount on;
  
  select o.Operator,
         count(distinct(j.JobId)) as 'Pallets',
         sum(datediff(mi, j.CheckedDate, i.EndDate)) / count(i.InstructionId) as 'Minutes'
    from Instruction      i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Operator           o (nolock) on j.OperatorId         = o.OperatorId
    join Status             s (nolock) on j.StatusId           = s.StatusId
   where i.WarehouseId = @WarehouseId
     and it.InstructionTypeCode in ('PR','S','SM')
     and (j.CheckedDate > convert(varchar(10), getdate(), 120)  + ' 07:00' 
     or   i.EndDate     > convert(varchar(10), getdate(), 120)  + ' 07:00')
     --and isnull(j.CheckedDate, i.StartDate) > convert(varchar(10), getdate(), 120)
     -- and j.CheckedDate < i.EndDate
  group by o.Operator
  order by o.Operator
end
