﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Interface
  ///   Filename       : p_Report_Interface.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 14 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Interface
(
    @WarehouseId int,
    @FromDate datetime,
    @ToDate datetime
)
 
as
begin
	 set nocount on;
  
--  select OutboundDocument.OrderNumber,
--         OutboundDocument.CreateDate,
--         OutboundDocumentType.OutboundDocumentType as DocType,
--         DateDiff(ss, OutboundDocument.CreateDate, getdate()) as elapsedTime,
--         OutboundDocument.WarehouseId,
--         'Y' as 'RecordStatus'
--    from OutboundDocument INNER JOIN
--         OutboundDocumentType ON OutboundDocument.OutboundDocumentTypeId = OutboundDocumentType.OutboundDocumentTypeId
--   where OutboundDocument.CreateDate between @FromDate and @ToDate
--     and OutboundDocument.WarehouseId = @WarehouseId
--  union
--  select InboundDocument.OrderNumber,
--         InboundDocument.CreateDate,
--         InboundDocumentType.InboundDocumentType as Doctype,
--         DateDiff(ss, InboundDocument.CreateDate, getdate()) as elapsedTime,
--         InboundDocument.WarehouseId,
--         'Y'
--    FROM InboundDocument INNER JOIN
--         InboundDocumentType ON InboundDocument.InboundDocumentTypeId = InboundDocumentType.InboundDocumentTypeId
--   where InboundDocument.CreateDate between @FromDate and @ToDate
--     and InboundDocument.WarehouseId = @WarehouseId
--  union
  select ProductCode as 'OrderNumber',
         ProcessedDate as 'CreateDate',
         'Product Master' as 'Doctype',
         DateDiff(ss, ProcessedDate, getdate()) as elapsedTime,
         RecordStatus, 
         StatusDescription
    FROM InterfaceProduct
    join InterfaceStatus  on RecordStatus = StatusCode
   where ProcessedDate between @FromDate and @ToDate
  union
  select CustomerName,
         ProcessedDate,
         'Customer Master' as Doctype,
         DateDiff(ss, ProcessedDate, getdate()) as elapsedTime,
         RecordStatus, 
         StatusDescription
    FROM InterfaceCustomer
    join InterfaceStatus on RecordStatus = StatusCode
   where ProcessedDate between @FromDate and @ToDate
  union
  select SupplierName,
         ProcessedDate,
         'Supplier Master' as Doctype,
         DateDiff(ss, ProcessedDate, getdate()) as elapsedTime,
         RecordStatus, 
         StatusDescription
    FROM InterfaceSupplier
    join InterfaceStatus on RecordStatus = StatusCode
   where ProcessedDate between @FromDate and @ToDate
  union
  select isnull(Location,'') + isnull(LocationDescription,''),
         ProcessedDate,
         'Location Master' as Doctype,
         DateDiff(ss, ProcessedDate, getdate()) as elapsedTime,
         RecordStatus, 
         StatusDescription
    FROM InterfaceLocation
    join InterfaceStatus on RecordStatus = StatusCode
   where ProcessedDate between @FromDate and @ToDate
  union
  select PrimaryKey,
         ProcessedDate,
         'PO Import' as Doctype,
         DateDiff(ss, ProcessedDate, getdate()) as elapsedTime,
         RecordStatus, 
         StatusDescription
    FROM InterfaceImportPOHeader
        join InterfaceStatus on RecordStatus = StatusCode
   where RecordType = 'PUR'
     and ProcessedDate between @FromDate and @ToDate
  union
  select PrimaryKey,
         ProcessedDate,
         'SR Export' as Doctype,
         DateDiff(ss, ProcessedDate, getdate()) as elapsedTime,
         RecordStatus, 
         StatusDescription
    FROM InterfaceExportPOHeader
        join InterfaceStatus on RecordStatus = StatusCode
   where RecordType = 'SR'
     and ProcessedDate between @FromDate and @ToDate
  union
  select PrimaryKey,
         ProcessedDate,
         'PO Export' as Doctype,
         DateDiff(ss, ProcessedDate, getdate()) as elapsedTime,
         RecordStatus, 
         StatusDescription
    FROM InterfaceExportPOHeader
        join InterfaceStatus on RecordStatus = StatusCode
   where RecordType = 'PUR'
     and ProcessedDate between @FromDate and @ToDate
  order by OrderNumber
end
