﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shifts_Insert
  ///   Filename       : p_Shifts_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:46
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Shifts table.
  /// </remarks>
  /// <param>
  ///   @ShiftId int = null output,
  ///   @Starttime int = null,
  ///   @EndTime int = null 
  /// </param>
  /// <returns>
  ///   Shifts.ShiftId,
  ///   Shifts.Starttime,
  ///   Shifts.EndTime 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shifts_Insert
(
 @ShiftId int = null output,
 @Starttime int = null,
 @EndTime int = null 
)
 
as
begin
	 set nocount on;
  
  if @ShiftId = '-1'
    set @ShiftId = null;
  
	 declare @Error int
 
  insert Shifts
        (Starttime,
         EndTime)
  select @Starttime,
         @EndTime 
  
  select @Error = @@Error, @ShiftId = scope_identity()
  
  
  return @Error
  
end
