﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_Product
  ///   Filename       : p_FamousBrands_Import_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Import_Product
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @ProductCode        varchar(30),
          @ProductDescription varchar(255),
          @PalletQuantity     int,
          @Class              varchar(50),
          @Modified           varchar(30),
          @PackCode           varchar(30),
          @PackDescription    varchar(255),
          @UOM                varchar(30),
          @SaleUOM            varchar(30),
          @Ingredients        varchar(255),
          @Quantity           int,
          @UnitSize           int,
          @Weight             numeric(13,3),
          @Length             int,
          @Width              int,
          @Height             int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  update InterfaceProduct
     set ProcessedDate = @Getdate
   where RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare Product_cursor cursor for
   select distinct pr.ProductCode,
          left(pr.ProductDescription,50),
          pr.PalletQuantity,
          pr.Class,
          pr.Modified,
          isnull(pk.PackCode,'0'),
          isnull(pk.PackDescription,''),
          isnull(pk.UOM,'Each'),
          isnull(pk.SaleUOM,'Each'),
          pk.Ingredients,
          pk.Quantity,
          pk.UnitSize,
          pk.Weight,
          pk.Length,
          pk.Width,
          pk.Height
     from InterfaceProduct pr
     left
     join InterfacePackInfo pk on pr.ProductCode = pk.ProductCode
    where ProcessedDate = @Getdate
--   order by pr.ProductCode, pk.PackCode

  open Product_cursor

  fetch Product_cursor into @ProductCode,
                            @ProductDescription,
                            @PalletQuantity,
                            @Class,
                            @Modified,
                            @PackCode,
                            @PackDescription,
                            @UOM,
                            @SaleUOM,
                            @Ingredients,
                            @Quantity,
                            @UnitSize,
                            @Weight,
                            @Length,
                            @Width,
                            @Height

  while (@@fetch_status = 0)
  begin
    begin transaction
    
    exec @Error = p_FamousBrands_Insert_Product
     @ProductCode        = @ProductCode,
     @Product            = @ProductDescription,
     @SKUCode            = @SaleUOM,
     @SKU                = @UOM,
     @WarehouseId        = 1,
     @UOM                = @UOM,
     @PalletQuantity     = null, -- We don't allow Accpak to manage pallets anymore
     @PackQuantity       = @Quantity,
     @Weight             = @Weight,
     @Length             = @Length,
     @Width              = @Width,
     @Height             = @Height
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
      goto error
    end
    
    --if exists(select top 1 1
    --           from FloProductMasterImport
    --           where Code = @ProductCode)
    --begin
    --  update FloProductMasterImport
    --     set Code  = @ProductCode,
    --         Name  = @ProductDescription,
    --         Class = @Class
    --   where Code  = @ProductCode
    --end
    --else
    --begin
    --  insert FloProductMasterImport
    --        (Code,
    --         Name,
    --         Class)
    --  select @ProductCode,
    --         @ProductDescription,
    --         @Class
    --end
    
    --if exists(select top 1 1
    --           from FloProductDetailImport
    --           where Code  = @ProductCode
    --             and isnull(Units,'') = isnull(@PackCode,''))
    --begin
    --  update FloProductDetailImport
    --     set ID        = 1,
    --         Code      = @ProductCode,
    --         Units     = @PackCode,
    --         Quantity  = @Quantity,
    --         Unitsize  = @Quantity,
    --         Weight    = @Weight,
    --         Length    = @Length,
    --         Width     = @Width,
    --         Height    = @Height
    --   where Code  = @ProductCode
    --     and Units = @PackCode
    --end
    --else
    --begin
    --  insert FloProductDetailImport
    --        (ID,
    --         Code,
    --         Units,
    --         Quantity,
    --         Unitsize,
    --         Weight,
    --         Length,
    --         Width,
    --         Height)
    --  select 1,
    --         @ProductCode,
    --         @PackCode,
    --         @Quantity,
    --         @Quantity,
    --         @Weight,
    --         @Length,
    --         @Width,
    --         @Height
    --end
    
    error:
    if @Error = 0
    begin
      update InterfaceProduct
         set RecordStatus = 'X'
       where ProductCode = @ProductCode
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
      
      commit transaction
    end
    else
    begin
      if @@trancount > 0
        rollback transaction
      
      update InterfaceProduct
         set RecordStatus = 'E'
       where ProductCode = @ProductCode
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
    end
    
    fetch Product_cursor into @ProductCode,
                              @ProductDescription,
                              @PalletQuantity,
                              @Class,
                              @Modified,
                              @PackCode,
                              @PackDescription,
                              @UOM,
                              @SaleUOM,
                              @Ingredients,
                              @Quantity,
                              @UnitSize,
                              @Weight,
                              @Length,
                              @Width,
                              @Height
  end
  
  close Product_cursor
  deallocate Product_cursor
end

 
