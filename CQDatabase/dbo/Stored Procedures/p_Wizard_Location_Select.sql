﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Location_Select
  ///   Filename       : p_Wizard_Location_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Location_Select
 
as
begin
	 set nocount on;
  
  Execute p_Wizard_Location_Check -1
  
  select LocationImportId,
         WarehouseCode,
         Area,
         AreaCode,
         Location,
         Ailse,
         [Column],
         [Level],
         PalletQuantity,
         SecurityCode,
         RelativeValue,
         NumberOfSUB,
         Width,
         Height,
         [Length],
         HasError,
         ErrorLog.value('(Root)[1]','nvarchar(50)') as ErrorLog
    from LocationImport
  order by HasError desc
end
