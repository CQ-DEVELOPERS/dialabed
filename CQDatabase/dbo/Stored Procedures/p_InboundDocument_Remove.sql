﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Remove
  ///   Filename       : p_InboundDocument_Remove.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Remove
(
 @InboundDocumentId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @ReceiptId         int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @ReceiptId = ReceiptId
    from Receipt
   where InboundDocumentId = @InboundDocumentId
  
  begin transaction
  
  exec @Error = p_Receipt_Delete
   @ReceiptId = @ReceiptId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_InboundDocumentContactList_Delete
   @InboundDocumentId = @InboundDocumentId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_InboundDocument_Delete
   @InboundDocumentId = @InboundDocumentId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_InboundDocument_Remove'
    rollback transaction
    return @Error
end
