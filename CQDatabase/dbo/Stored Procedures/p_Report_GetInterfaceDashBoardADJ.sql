﻿
-- =============================================

-- Author:        <Karen Sucksmith>

--Create date:    <23 August 2010>

--Description:    <Count all Adjustments with Reason Code depending on parameters passed from level 2 dashboard drilldown>

--DROP PROCEDURE [dbo].[p_Report_GetInterfaceDashBoardADJ]

-- =============================================

Create PROCEDURE [dbo].[p_Report_GetInterfaceDashBoardADJ]
(
@FromDate datetime,
@ToDate datetime,
@fromtohost char(1),
@extracttype char(3),
@ExtractDescription nvarchar(30),
@Show char(1)
)
AS

begin

--drop table @temp

Declare @temp as table
(
 InterfaceReprocessId	int,
 PrimaryKey				nvarchar(30),
 OrderNumber			nvarchar(30),
 OtherKey				nvarchar(30),
 TableType				nvarchar(30),
 RepCount				int
)

insert @temp (PrimaryKey, OrderNumber, OtherKey, TableType, RepCount) 
select PrimaryKey, OrderNumber, OtherKey, TableType, count(InterfaceReprocessId)
from InterfaceReprocessCount
group by PrimaryKey, OrderNumber, OtherKey, TableType

end

IF @extracttype = 'EXT'

BEGIN 

SET NOCOUNT ON;
       

DECLARE @ProductCode nvarchar(30),
        @RecordType CHAR(3),
        @RecordStatus CHAR(1),
        @Batch nvarchar(50),
        @ProcessedDate DATETIME,
        @ReprocessCount int
          
    
SELECT t1.InterfaceExportStockAdjustmentId,
	   t1.ProductCode,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.Batch,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
       
		
FROM InterfaceExportStockAdjustment t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportStockAdjustmentId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportStockAdjustmentId 
left join @temp t4	on	TableType = 'SA' 
					and t4.PrimaryKey = t1.RecordType 
					and t4.OrderNumber = t1.ProductCode 
					and t4.OtherKey = t1.Batch 
WHERE t1.RecordStatus = 'Y' and t1.RecordType = 'SOH' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate

SELECT @ProductCode [Product Code],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @Batch [Batch],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ReprocessCount [Reprocess Count]  

             
END



IF @extracttype = 'NEX'

BEGIN 

SET NOCOUNT ON;
        
SELECT t1.InterfaceExportStockAdjustmentId,
	   t1.ProductCode,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.Batch,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
       
		
FROM InterfaceExportStockAdjustment t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportStockAdjustmentId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportStockAdjustmentId 
left join @temp t4	on	TableType = 'SA' 
					and t4.PrimaryKey = t1.RecordType 
					and t4.OrderNumber = t1.ProductCode 
					and t4.OtherKey = t1.Batch 
WHERE t1.RecordStatus = 'F' and t1.RecordType = 'SOH' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate

SELECT @ProductCode [Product Code],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @Batch [Batch],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ReprocessCount [Reprocess Count]  

             
END

IF @extracttype = 'TWA'

BEGIN 

SET NOCOUNT ON;
     
SELECT t1.InterfaceExportStockAdjustmentId,
	   t1.ProductCode,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.Batch,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
       
		
FROM InterfaceExportStockAdjustment t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportStockAdjustmentId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportStockAdjustmentId 
left join @temp t4	on	TableType = 'SA' 
					and t4.PrimaryKey = t1.RecordType 
					and t4.OrderNumber = t1.ProductCode 
					and t4.OtherKey = t1.Batch 
WHERE t1.RecordStatus = 'Y' and t1.RecordType = 'SOH' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate


SELECT @ProductCode [Product Code],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @Batch [Batch],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ReprocessCount [Reprocess Count]  

END

IF @extracttype = 'SSU'

BEGIN 

SET NOCOUNT ON;
     
SELECT t1.InterfaceExportStockAdjustmentId,
	   t1.ProductCode,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.Batch,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
       
		
FROM InterfaceExportStockAdjustment t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportStockAdjustmentId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportStockAdjustmentId 
left join @temp t4	on	TableType = 'SA' 
					and t4.PrimaryKey = t1.RecordType 
					and t4.OrderNumber = t1.ProductCode 
					and t4.OtherKey = t1.Batch 
WHERE t2.Status <> 'N' and t1.RecordType = 'SOH' and t2.ProcessedDate BETWEEN  @FromDate and @ToDate


SELECT @ProductCode [Product Code],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @Batch [Batch],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ReprocessCount [Reprocess Count]  

             
END

IF @extracttype = 'SER'

BEGIN 

SET NOCOUNT ON;
     
SELECT t1.InterfaceExportStockAdjustmentId,
	   t1.ProductCode,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.Batch,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
       
		
FROM InterfaceExportStockAdjustment t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportStockAdjustmentId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportStockAdjustmentId 
left join @temp t4	on	TableType = 'SA' 
					and t4.PrimaryKey = t1.RecordType 
					and t4.OrderNumber = t1.ProductCode 
					and t4.OtherKey = t1.Batch 
WHERE t2.Status = 'E' and t1.RecordType = 'SOH' and t2.ProcessedDate BETWEEN  @FromDate and @ToDate

SELECT @ProductCode [Product Code],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @Batch [Batch],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ReprocessCount [Reprocess Count]  

             
END
 
 

--exec [p_Report_GetInterfaceDashBoardADJ] '2010-01-10', '2010-08-10', 'T', 'EXT', 'Extracted', 'Y'
--exec [p_Report_GetInterfaceDashBoardADJ] '2010-01-10', '2010-08-10', 'T', 'NEX', 'Not Extracted'
--exec [p_Report_GetInterfaceDashBoardADJ] '2010-01-10', '2010-08-10', 'T', 'TWA', 'Waiting'
--exec [p_Report_GetInterfaceDashBoardADJ] '2010-01-10', '2010-08-10', 'T', 'SSU', 'Sent Successfully'
--exec [p_Report_GetInterfaceDashBoardADJ] '2010-01-10', '2010-08-10', 'T', 'SER', 'Sent With Errors'


