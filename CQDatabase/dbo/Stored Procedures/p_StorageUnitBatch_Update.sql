﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Update
  ///   Filename       : p_StorageUnitBatch_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:01
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StorageUnitBatch table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null,
  ///   @BatchId int = null,
  ///   @StorageUnitId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Update
(
 @StorageUnitBatchId int = null,
 @BatchId int = null,
 @StorageUnitId int = null 
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
	 declare @Error int
 
  update StorageUnitBatch
     set BatchId = isnull(@BatchId, BatchId),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId) 
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select @Error = @@Error
  
  
  return @Error
  
end
