﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UserOperator_Verify
  ///   Filename       : p_UserOperator_Verify.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_UserOperator_Verify]
(
 @UserName      nvarchar(512),
 @Password		nvarchar(128),
 @IpAddress		nvarchar(50),
 @Result		nvarchar(50) = null Output,
 @ServerName    nvarchar(256) = null output,
 @DatabaseName  nvarchar(256) = null output
 
)
as
begin
	 set nocount on;
  
  Set @Result = 'Fail'
 
  select @Result = (Case when m.Password <> @Password Then 'Password Error'
					  when (@Ipaddress <> Isnull(uo.IpAddress,'')) or (len(@IpAddress) = 0) Then 'IP Error'
				Else 'Pass' end ),
		 @DatabaseName = uo.DatabaseName,
		 @ServerName = uo.ServerName
    from CQSecurity.dbo.aspnet_Users  u (nolock)
		Join CQSecurity.dbo.aspnet_Membership m on m.UserId = u.UserId
		join CQCommon.dbo.UserOperator   uo (nolock) on uo.UserId = u.UserId
		
   where u.UserName = @UserName 
  
  select @Result

end
