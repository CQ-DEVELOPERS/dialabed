﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Auto_Load
  ///   Filename       : p_Outbound_Auto_Load.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Auto_Load
(
 @OutboundShipmentId int output,
 @IssueId            int,
 @Remarks            nvarchar(255) = null,
 @TotalOrders        int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error                    int,
          @Errormsg                 nvarchar(500),
          @GetDate                  datetime,
          @WarehouseId              int,
          @AutoRelease              bit,
          @AddToShipment            bit,
          @MultipleOnShipment       bit,
          @OutboundDocumentTypeId   int,
          @OutboundDocumentTypeCode nvarchar(10),
          @LocationId               int,
          @DeliveryDate             datetime,
          @ExternalCompanyId        int,
          @RouteId                  int,
          @OrderNumber              nvarchar(30),
          @DI                       nvarchar(30),
          @charindex                int,
          @DropSequence              int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @IssueId = -1
    set @IssueId = null
  
  if @IssueId is not null
  begin
    select @WarehouseId            = od.WarehouseId,
           @OutboundDocumentTypeId = od.OutboundDocumentTypeId,
           @ExternalCompanyId      = od.ExternalCompanyId,
           @RouteId                = Isnull(i.RouteId,ec.RouteId),
           @DeliveryDate           = i.DeliveryDate,
           @OrderNumber            = od.OrderNumber
      from Issue             i (nolock)
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
     where i.IssueId = @IssueId
  end
  
  select @OutboundDocumentTypeId   = OutboundDocumentTypeId,
         @AddToShipment            = AddToShipment,
         @MultipleOnShipment       = MultipleOnShipment,
         @OutboundDocumentTypeCode = OutboundDocumentTypeCode
    from OutboundDocumentType (nolock)
   where OutboundDocumentTypeId =  @OutboundDocumentTypeId
  
  begin transaction
  
  set @OutboundShipmentId = null
  
  if @AddToShipment = 1
  begin
    if @OutboundDocumentTypeCode = 'CNC'
      select @LocationId = convert(int, Value)
        from Configuration
       where ConfigurationId = 72
         and WarehouseId = @WarehouseId
    else
      select @LocationId = convert(int, Value)
        from Configuration
       where ConfigurationId = 43
         and WarehouseId = @WarehouseId
    
    if @MultipleOnShipment = 1
    begin
      set @OutboundShipmentId = null
      
      if dbo.ufn_Configuration(413, @WarehouseId) = 1
      begin
        set @charindex = CHARINDEX('_', @OrderNumber)
        
        if @charindex > 0
        begin
          select @DI = substring(@OrderNumber, 1, @charindex + 2)
          
          select @OutboundShipmentId = os.OutboundShipmentId
            from OutboundShipment       os
            join Status                  s (nolock) on os.StatusId           = s.StatusId
            join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
            join Issue                   i (nolock) on osi.IssueId           = i.IssueId
            join OutboundDocument       od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
           where s.StatusCode         = 'W'
             and os.WarehouseId       = @WarehouseId
             and substring(od.OrderNumber, 1, CHARINDEX('_', OrderNumber) + 2) = @DI
        end
      end
      else
      begin
        if dbo.ufn_Configuration(135, @WarehouseId) = 0
        -- Find Shipment to add to by Customer
        select @OutboundShipmentId = os.OutboundShipmentId
          from OutboundShipment       os
          join Status                  s (nolock) on os.StatusId           = s.StatusId
          join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
          join Issue                   i (nolock) on osi.IssueId           = i.IssueId
          join OutboundDocument       od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
         where s.StatusCode         = 'W'
           and os.ShipmentDate      = @DeliveryDate
           and od.ExternalCompanyId = @ExternalCompanyId
           and os.WarehouseId       = @WarehouseId
        else
        -- Find Shipment to add to by Route
        select @OutboundShipmentId = os.OutboundShipmentId
          from OutboundShipment       os
          join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
          join Issue                   i (nolock) on osi.IssueId           = i.IssueId
          join Status                  s (nolock) on i.StatusId            = s.StatusId
          join OutboundDocument       od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
          join ExternalCompany        ec (nolock) on od.ExternalCompanyId  = ec.ExternalCompanyId
         where s.StatusCode                 = 'W'
           and os.ShipmentDate              = @DeliveryDate
           and Isnull(i.RouteId,ec.RouteId) = @RouteId
           and os.WarehouseId               = @WarehouseId
      end
      
      if @OutboundShipmentId is null
      begin
        exec @Error = p_OutboundShipment_Create
         @WarehouseId        = @WarehouseId,
         @ShipmentDate       = @Getdate,
         @Remarks            = 'Inserted via interface - p_interface_xml_to_insert',
         @OutboundShipmentId = @OutboundShipmentId output,
         @TotalOrders        = @TotalOrders,
         @ReferenceNumber    = @DI
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_OutboundShipment_Create'
          goto error
        end
      end
      
      if @OutboundShipmentId is not null
      begin
        if dbo.ufn_Configuration(221, @WarehouseId) = 0
          set @DropSequence = 1
        
        exec @Error = p_OutboundShipmentIssue_Link
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId,
         @DropSequence        = @DropSequence
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_OutboundShipmentIssue_Link'
          goto error
        end
        
        exec @Error = p_Outbound_Shipment_Edit
         @outboundShipmentId = @OutboundShipmentId,
         @locationId         = @LocationId,
         @routeId            = @RouteId,
         @shipmentDate       = @DeliveryDate
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Outbound_Shipment_Edit'
          goto error
        end
      end
    end
    else
    begin
      exec @Error = p_OutboundShipment_Create
       @WarehouseId        = @WarehouseId,
       @ShipmentDate       = @GetDate,
       @Remarks            = 'Inserted via interface - p_interface_xml_to_insert',
       @OutboundShipmentId = @OutboundShipmentId output,
       @ReferenceNumber    = @DI
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundShipment_Create'
        goto error
      end
      
      exec @Error = p_OutboundShipmentIssue_Link
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId
             
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundShipmentIssue_Link'
        goto error
      end
      
      exec @Error = p_Outbound_Shipment_Edit
       @outboundShipmentId = @OutboundShipmentId,
       @locationId         = @LocationId,
       @routeId            = @RouteId,
       @shipmentDate       = @GetDate
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Shipment_Edit'
        goto error
      end
    end
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @ErrorMsg
    rollback transaction
    return @Error
end
