﻿	
/*
  /// <summary>
  ///   Procedure Name : p_Report_Customer_Returns
  ///   Filename       : p_Report_Customer_Returns.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Nov 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Customer_Returns

(
	@WarehouseId	int,
	@FromDate Datetime,
	@ToDate	DateTime,
	@Customer	nvarchar(50),
	@Status		nvarchar(50),
	@RfcNumber	nvarchar(50),
	@InvoiceNumber	nvarchar(50)
)

	
 
as
begin
	 set nocount on;
	
	If (@Status = 'All' or @Status = '-1' or len(@Status) = 0) 
	  Begin
			Set @Status = null
	  End
	If (@InvoiceNumber = 'All' or @InvoiceNumber = '-1' or len(@InvoiceNumber) = 0) 
	  Begin
			Set @InvoiceNumber = null
	  End
	If (@RfcNumber = 'All' or @RfcNumber = '-1' or len(@RfcNumber) = 0) 
	  Begin
			Set @RfcNumber = null
	  End

	If (@Status = 'All' or @Status = '-1' or len(@Status) = 0) 
	  Begin
			Set @Status = null
	  End

	If (@Customer = 'All' or @Customer = '-1' or len(@Customer) = 0) 
	  Begin
			Set @Customer = null
	  End

--	if (@FromDate = '-1')
--	 Begin
--		Set @FromDate = null
--	 End
--  
--	if (@ToDate = '-1')
--	 Begin
--		Set @ToDate = null
--	 End
  
	Select	i.ReferenceNumber as InvoiceNumber,
			i.OrderNumber as RFCNumber,
			i.CreateDate,
			i.ReasonId,
			e.ExternalCompanyCode as CustomerCode,
			e.ExternalCompany as Customer,
			p.ProductCode,
			p.Product,
			l.Quantity,
			s.Status
	from InboundDocument i
		
		Join InboundDocumentType t on t.InboundDocumentTypeId = i.InboundDocumentTypeId
		Join ExternalCompany e on e.ExternalCompanyId = i.ExternalCompanyId
		Join InboundLine l on l.InboundDocumentId = i.InboundDocumentId
		Join Status s on s.StatusId = l.StatusId
		Join StorageUnit su on su.StorageUnitId = l.StorageUnitId
		Join Product p on p.ProductId = su.ProductId
  where i.WarehouseId = Isnull(@WarehouseId,i.WarehouseId) and
		t.InboundDocumentTypeCode = 'RET' and
		i.ReferenceNumber like '%' +  isnull(@InvoiceNumber,'') + '%' and
		i.OrderNumber Like '%' + Isnull(@RfcNumber,'') + '%' and
		--(i.CreateDate between @FromDate and @ToDate) and
		i.CreateDate >= Isnull(@FromDate,i.CreateDate) and
		i.CreateDate <= Isnull(@ToDate,i.CreateDate) and
		i.ExternalCompanyID = Isnull(@Customer,i.ExternalCompanyID ) and
		s.Status = isnull(@Status,s.Status)
		
		
oRDER BY CreateDate desc
end
