﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Lines_By_Job
  ///   Filename       : p_Receiving_Lines_By_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Lines_By_Job
(
 @JobId               int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   ReceiptId             int,
   ReceiptLineId         int,
   JobId                 int,
   InstructionId         int,
   OrderNumber           nvarchar(30),
   StorageUnitBatchId    int,
   StorageUnitId         int,
   ProductCode           nvarchar(30),
   Product               nvarchar(50),
   BatchId               int,
   Batch                 nvarchar(50),
   SKUCode               nvarchar(50),
   PickLocationId        int,
   PickLocation          nvarchar(15),
   StoreLocationId       int,
   StoreLocation         nvarchar(15),
   Quantity              numeric(13,6),
   ConfirmedQuantity     numeric(13,6),
   InstructionType       nvarchar(30),
   Status                nvarchar(50),
   AvailabilityIndicator nvarchar(20),
   OperatorId            int,
   Operator              nvarchar(50)
  )
  
  if @JobId = -1
    set @JobId = null
  
  insert @TableResult
        (ReceiptLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         StorageUnitId,
         BatchId,
         Quantity,
         ConfirmedQuantity,
         Status,
         InstructionType,
         PickLocationId,
         StoreLocationId)
  select i.ReceiptLineId,
         i.JobId,
         i.InstructionId,
         sub.StorageUnitBatchId,
         sub.StorageUnitId,
         sub.BatchId,
         i.Quantity,
         i.ConfirmedQuantity,
         s.Status,
         it.InstructionType,
         i.PickLocationId,
         i.StoreLocationId
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on i.StatusId           = s.StatusId
   where j.JobId = @JobId
  
--  if exists(select top 1 1 from @TableResult where IssueLineId is null)
--  update tr
--     set IssueLineId = ili.IssueLineId
--    from @TableResult          tr
--    join IssueLineInstruction ili on tr.InstructionId = ili.InstructionId
  
  update tr
     set ReceiptId = rl.ReceiptId
    from @TableResult tr
    join ReceiptLine  rl on tr.ReceiptLineId = rl.ReceiptLineId
  
  update tr
     set OrderNumber = id.OrderNumber
    from @TableResult    tr
    join Receipt          r (nolock) on tr.ReceiptId = r.ReceiptId
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         Batch       = b.Batch,
         SKUCode     = sku.SKUCode
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch            b   (nolock) on tr.BatchId           = b.BatchId
  
  update @TableResult
     set AvailabilityIndicator = 'Blue'
   where PickLocation is not null
  
  update @TableResult
     set AvailabilityIndicator = 'Standard'
   where PickLocation is null
  
  select JobId,
         InstructionId,
         OrderNumber,
         StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         Product,
         Batch,
         SKUCode,
         Quantity,
         ConfirmedQuantity,
         PickLocation,
         StoreLocation,
         InstructionType,
         Status,
         AvailabilityIndicator,
         Operator,
         null as 'PackageLineId'
    from @TableResult
  order by PickLocation,
           Quantity
end
 
