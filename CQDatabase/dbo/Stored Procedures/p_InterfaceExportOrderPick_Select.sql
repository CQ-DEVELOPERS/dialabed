﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportOrderPick_Select
  ///   Filename       : p_InterfaceExportOrderPick_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportOrderPick table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportOrderPickId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportOrderPick.InterfaceExportOrderPickId,
  ///   InterfaceExportOrderPick.IssueId,
  ///   InterfaceExportOrderPick.OrderNumber,
  ///   InterfaceExportOrderPick.RecordStatus,
  ///   InterfaceExportOrderPick.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportOrderPick_Select
(
 @InterfaceExportOrderPickId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceExportOrderPick.InterfaceExportOrderPickId
        ,InterfaceExportOrderPick.IssueId
        ,InterfaceExportOrderPick.OrderNumber
        ,InterfaceExportOrderPick.RecordStatus
        ,InterfaceExportOrderPick.ProcessedDate
    from InterfaceExportOrderPick
   where isnull(InterfaceExportOrderPick.InterfaceExportOrderPickId,'0')  = isnull(@InterfaceExportOrderPickId, isnull(InterfaceExportOrderPick.InterfaceExportOrderPickId,'0'))
  
end
