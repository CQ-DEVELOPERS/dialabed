﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Card_Packaging
  ///   Filename       : p_Report_Batch_Card_Packaging.sql
  ///   Create By      : Karen
  ///   Date Created   : January 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Batch_Card_Packaging
(
 @WarehouseId            int,
 @OrderNumber	         nvarchar(30)
)
 
as
begin
	 set nocount on;
 
	Select p.ProductCode, 
		   p.Product,
	       bp.Quantity,
	       MIN(JobId) as JobId
	from BOMPackaging     bp (nolock)
	join BOMInstruction   bi (nolock)on bp.bominstructionid = bi.bominstructionid
	join StorageUnit      su (nolock)on su.StorageUnitId = bp.StorageUnitId
	join Product          p  (nolock)on p.ProductId = su.ProductId
	join IssueLineInstruction ili (nolock) on bi.OutboundDocumentId = ili.OutboundDocumentId
	join instruction i (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
	where bi.OrderNumber = isnull(@OrderNumber, bi.OrderNumber)
	group by p.ProductCode, 
		     p.Product,
	         bp.Quantity
 
end
