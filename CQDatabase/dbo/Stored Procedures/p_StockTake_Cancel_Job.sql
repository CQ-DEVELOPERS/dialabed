﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTake_Cancel_Job
  ///   Filename       : p_StockTake_Cancel_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Jan 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTake_Cancel_Job
(
 @JobId      int,
 @OperatorId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @InstructionId     int,
          @PickLocationId    int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction

  set @InstructionId = -1
  
  declare delete_cursor cursor for
    select i.InstructionId,
           i.JobId,
           i.PickLocationId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Status           s (nolock) on i.StatusId = s.StatusId
     where i.JobId = @JobId
       and s.StatusCode not in ('F','Z')
       and it.InstructionTypeCode in ('STE','STL','STA','STP')
	  order by	i.InstructionId

  open delete_cursor

  fetch delete_cursor into @InstructionId,
                           @JobId,
                           @PickLocationId

  while (@@fetch_status = 0)
  begin
    exec @Error = p_Housekeeping_Instruction_Delete
     @InstructionId = @InstructionId,
     @JobId         = @JobId

    if @Error <> 0
      goto error
    
    exec @Error = p_Location_Update
     @LocationId   = @PickLocationId,
     @StockTakeInd = 0

    if @Error <> 0
      goto error
    
    fetch delete_cursor into @InstructionId,
                             @JobId,
                             @PickLocationId
  end

  close delete_cursor
  deallocate delete_cursor
  
  if (select count(1)
        from Instruction i (nolock)
        join Status      s (nolock) on i.StatusId = s.StatusId
       where i.JobId       = @JobId
         and s.Type        = 'I'
         and s.StatusCode  = 'W') = 0
  begin
    select @StatusId = dbo.ufn_StatusId('J','F')
    
    exec @Error = p_Job_Update
     @JobId     = @JobId,
     @StatusId  = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_StockTake_Cancel_Job'
    rollback transaction
    return @Error
end
