﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportLotStatus_Search
  ///   Filename       : p_InterfaceImportLotStatus_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:46
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportLotStatus table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportLotStatus.StatusDescription,
  ///   InterfaceImportLotStatus.StatusID,
  ///   InterfaceImportLotStatus.HostId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportLotStatus_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportLotStatus.StatusDescription
        ,InterfaceImportLotStatus.StatusID
        ,InterfaceImportLotStatus.HostId
    from InterfaceImportLotStatus
  
end
