﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Reason_Select
  ///   Filename       : p_Reason_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:23
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Reason table.
  /// </remarks>
  /// <param>
  ///   @ReasonId int = null 
  /// </param>
  /// <returns>
  ///   Reason.ReasonId,
  ///   Reason.Reason,
  ///   Reason.ReasonCode,
  ///   Reason.AdjustmentType,
  ///   Reason.ToWarehouseCode,
  ///   Reason.RecordType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Reason_Select
(
 @ReasonId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Reason.ReasonId
        ,Reason.Reason
        ,Reason.ReasonCode
        ,Reason.AdjustmentType
        ,Reason.ToWarehouseCode
        ,Reason.RecordType
    from Reason
   where isnull(Reason.ReasonId,'0')  = isnull(@ReasonId, isnull(Reason.ReasonId,'0'))
  
end
