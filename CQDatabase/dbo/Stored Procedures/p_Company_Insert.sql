﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Company_Insert
  ///   Filename       : p_Company_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:50
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Company table.
  /// </remarks>
  /// <param>
  ///   @CompanyId int = null output,
  ///   @Company nvarchar(100) = null,
  ///   @CompanyCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   Company.CompanyId,
  ///   Company.Company,
  ///   Company.CompanyCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Company_Insert
(
 @CompanyId int = null output,
 @Company nvarchar(100) = null,
 @CompanyCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @CompanyId = '-1'
    set @CompanyId = null;
  
  if @Company = '-1'
    set @Company = null;
  
  if @CompanyCode = '-1'
    set @CompanyCode = null;
  
	 declare @Error int
 
  insert Company
        (Company,
         CompanyCode)
  select @Company,
         @CompanyCode 
  
  select @Error = @@Error, @CompanyId = scope_identity()
  
  
  return @Error
  
end
