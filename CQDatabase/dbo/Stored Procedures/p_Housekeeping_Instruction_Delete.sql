﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Instruction_Delete
  ///   Filename       : p_Housekeeping_Instruction_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Instruction_Delete
(
 @InstructionId int,
 @JobId         int = null,
 @OperatorId    int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int = 0,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @StatusCode        nvarchar(10),
          @WarehouseId       int,
          @Exception         nvarchar(255),
          @Operator          nvarchar(50),
          @MoveId            int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('Z','Z')
  
  select @Operator = Operator
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  --if @JobId is null
  select @JobId = i.JobId,
         @StatusCode = s.StatusCode,
         @WarehouseId = i.WarehouseId
    from Instruction i (nolock)
    join Status s (nolock) on i.StatusId = s.StatusId
   where i.InstructionId = @InstructionId
  
  select @MoveId = InstructionId
    from Instruction i
    join Status     si on i.StatusId = si.StatusId
                      and si.StatusCode in ('W','S')
    join Job         j on i.JobId = j.JobId
    join Status     js on j.StatusId = js.StatusId
                      and js.StatusCode in ('PS','E')
   where i.InstructionRefId = @InstructionId
  
  begin transaction
  
  if (dbo.ufn_Configuration(319, @WarehouseId)) = 0 -- Pallet Id re-print
    if @StatusCode = 'RE'
      goto error
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId       = @InstructionId
  
  if @Error <> 0
    goto error
    
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @StatusId      = @StatusId,
   @ConfirmedQuantity = 0,
   @CheckQuantity = 0
  
  if @Error <> 0
    goto error
  
  if @MoveId is not null
  begin
    exec @Error = p_Housekeeping_Instruction_Delete
     @InstructionId = @MoveId
    
    if @Error <> 0
      goto error
  end
  
  if not exists (select top 1 1 from Instruction where JobId = @JobId and StatusId != @StatusId)
  begin
    select @StatusId = dbo.ufn_StatusId('IS','CD')
    
    exec @Error = p_Job_Update
     @JobId = @JobId,
     @StatusId = @StatusId
    
    if @Error <> 0
      goto error
    
    exec p_Status_Rollup
     @JobId = @JobId
    
    if @Error <> 0
      goto error
  end
  
  select @Exception = 'Operator ' + @Operator + ' logged in.'
  
  exec @Error = p_Exception_Insert
   @ExceptionId   = null,
   @ExceptionCode = 'PALDELETE',
   @Exception     = @Exception,
   @OperatorId    = @OperatorId,
   @CreateDate    = @GetDate,
   @ExceptionDate = @Getdate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Housekeeping_Instruction_Delete'
    rollback transaction
    return @Error
end
