﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_StorageUnit
  ///   Filename       : p_Location_Search_StorageUnit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_StorageUnit
(
 @InstructionId int,
 @AreaId        int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   LocationId        int,
   Location          nvarchar(15),
   StorageUnitId     int,
   BatchId           int,
   ProductCode       nvarchar(30) null,
   Product           nvarchar(50) null,
   SKUCode           nvarchar(50) null,
   Batch	            nvarchar(50),
   CreateDate        datetime,
   ActualQuantity    float,
   AllocatedQuantity float,
   ReservedQuantity  float
  );
  
  declare @StorageUnitId       int,
          @InstructionTypeCode nvarchar(10)
  
  select @StorageUnitId       = sub.StorageUnitId,
         @InstructionTypeCode = it.InstructionTypeCode
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.InstructionId = @InstructionId
  
  if @InstructionTypeCode in ('S','SM','PR','M')
    insert @TableResult
          (LocationId,
           Location)
    select l.LocationId,
           l.Location
      from StorageUnitArea sua (nolock)
      join AreaLocation     al (nolock) on sua.AreaId = al.AreaId
      join Location          l (nolock) on al.LocationId = l.LocationId
      join Area              a (nolock) on al.AreaId = a.AreaId
     where sua.StorageUnitId = @StorageUnitId
       and al.AreaId         = @AreaId
       and l.Used <= case when AreaCode = 'OF' then 1
                                     else 0
                                     end
  
  if @InstructionTypeCode in ('O')
  begin
    insert @TableResult
          (LocationId,
           Location)
    select l.LocationId,
           l.Location
      from AreaLocation     al (nolock)
      join Location          l (nolock) on al.LocationId = l.LocationId
     where al.AreaId         = @AreaId
  end
  else
  begin
    insert @TableResult
          (LocationId,
           Location,
           StorageUnitId,
           BatchId,
           ActualQuantity,
           AllocatedQuantity,
           ReservedQuantity)
    select l.LocationId,
           l.Location,
           sub.StorageUnitId,
           sub.BatchId,
           subl.ActualQuantity,
           subl.AllocatedQuantity,
           subl.ReservedQuantity
      from AreaLocation               al (nolock)
      join Location                    l (nolock) on al.LocationId          = l.LocationId
      join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
      join StorageUnitBatchLocation subl (nolock) on l.LocationId           = subl.LocationId
      join StorageUnitBatch          sub (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
     where sub.StorageUnitId = @StorageUnitId
       and al.AreaId         = @AreaId
       and subl.ActualQuantity - subl.ReservedQuantity > 0
    
    insert @TableResult
          (LocationId,
           Location)
    select l.LocationId,
           l.Location
      from StorageUnitLocation sul (nolock)
      join Location              l (nolock) on sul.LocationId = l.LocationId
      join AreaLocation         al (nolock) on l.LocationId   = al.LocationId
      join Area                  a (nolock) on al.AreaId      = a.AreaId
     where sul.StorageUnitId = @StorageUnitId
       and a.AreaId          = @AreaId
       and not exists(select 1 from @TableResult tr where l.LocationId = tr.LocationId)
  end
  
  update tr
     set ProductCode       = p.ProductCode,
         Product           = p.Product
    from @TableResult         tr
    join StorageUnit su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product      p (nolock) on su.ProductId     = p.ProductId
  
  update tr
     set SKUCode = sku.SKUCode
    from @TableResult         tr
    join StorageUnit su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join SKU        sku (nolock) on su.SKUId         = sku.SKUId
  
  update tr
     set Batch      = b.Batch,
         CreateDate = b.CreateDate
    from @TableResult tr
    join Batch b (nolock) on tr.BatchId = b.BatchId
  
  select LocationId,
         Location,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity
    from @TableResult
   order by Location
end
