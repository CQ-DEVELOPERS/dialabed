﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_StorageUnitBatchId
  ///   Filename       : p_Location_Search_StorageUnitBatchId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_StorageUnitBatchId
(
 @WarehouseId   int,
 @StorageUnitBatchId int
)
 
as
begin
	 set nocount on;
  
  select l.LocationId,
         l.Location
    from StorageUnitBatchLocation subl
    join Location                    l on subl.Locationid         = l.LocationId
    join AreaLocation               al on l.LocationId            = al.LocationId
    join Area                        a on al.AreaId               = a.AreaId
   where a.WarehouseId           = @WarehouseId
     and subl.StorageUnitBatchId = @StorageUnitBatchId
     and a.StockOnHand           = 1
end
