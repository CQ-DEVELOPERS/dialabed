﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_AverageDwellTime
  ///   Filename       : p_Report_AverageDwellTime.sql
  ///   Create By      :Junaid Desai    
  ///   Date Created   : 28 July    2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_AverageDwellTime
(
 @WarehouseId       int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
 
  declare @GetDate datetime
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 declare @TableResult as table
	 (
	  JobId              int,
   InstructionType    nvarchar(50),
   StorageUnitBatchId int,
   Quantity           float,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   CreateDate         datetime,
   StartDate          datetime,
   EndDate            datetime,
   Hours              nvarchar(8),
   Minutes            nvarchar(2),
   Seconds            nvarchar(2)
	 )

declare @AvgTableresult as table
(
	instructionType nvarchar(50),
	Jobs				int,
	avgdwelltime		int
)
	 
	 insert @TableResult
        (JobId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
         Quantity,
         CreateDate,
         StartDate)
  select i.JobId,
         it.InstructionType,
         i.StorageUnitBatchId,
         i.PickLocationId,
         i.StoreLocationId,
         i.Quantity,
         i.CreateDate,
         i.StartDate
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where i.CreateDate   between @FromDate and @ToDate 
   and i.WarehouseId = @WarehouseId
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
  
  update @TableResult
     set Hours   =  DateDiff(ss, CreateDate, StartDate) / 3600,
         Minutes = (DateDiff(ss, CreateDate, StartDate) % 3600) / 60,
         Seconds =  DateDiff(ss, CreateDate, StartDate) % 60
  
  update @TableResult
     set Minutes = '0' + Minutes
   where datalength(Minutes) = 1
  
  update @TableResult
     set Seconds = '0' + Seconds
   where datalength(Seconds) = 1
  

insert into @AvgTableresult
  select InstructionType,
	COUNT (InstructionType) as jobs,
	AVG((convert(int,isnull(tr.Hours,'0')) * 3600) +  (Convert(int,isnull(tr.Minutes,'00')) * 60) + Convert(int,isnull(tr.Seconds,'0'))) as AVGDwellTimeSeconds
    from @TableResult    tr
Group by InstructionType

Select instructionType,Jobs, convert(nvarchar(10), avgdwelltime / 3600) + ':' + convert(nvarchar(2), (avgdwelltime % 3600) / 60) + ':' + convert(nvarchar(2),  avgdwelltime % 60) As AverageDwellTime
from @AvgTableresult
end
