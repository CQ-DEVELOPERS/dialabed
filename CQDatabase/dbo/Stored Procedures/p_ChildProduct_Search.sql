﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ChildProduct_Search
  ///   Filename       : p_ChildProduct_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:20
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ChildProduct table.
  /// </remarks>
  /// <param>
  ///   @ChildProductId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ChildProduct.ChildProductId,
  ///   ChildProduct.ChildStorageUnitBatchId,
  ///   ChildProduct.ParentStorageUnitBatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ChildProduct_Search
(
 @ChildProductId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ChildProductId = '-1'
    set @ChildProductId = null;
  
 
  select
         ChildProduct.ChildProductId
        ,ChildProduct.ChildStorageUnitBatchId
        ,ChildProduct.ParentStorageUnitBatchId
    from ChildProduct
   where isnull(ChildProduct.ChildProductId,'0')  = isnull(@ChildProductId, isnull(ChildProduct.ChildProductId,'0'))
  
end
