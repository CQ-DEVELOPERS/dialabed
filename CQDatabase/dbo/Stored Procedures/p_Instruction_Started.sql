﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Started
  ///   Filename       : p_Instruction_Started.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Started
(
 @instructionId int,
 @operatorId    int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('I','S')
  
  begin transaction
  
  exec @Error = p_Instruction_Update
   @InstructionId = @instructionId,
   @OperatorId    = @operatorId,
   @StatusId      = @StatusId,
   @StartDate     = @GetDate
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Operator_Update
   @OperatorId      = @operatorId,
   @LastInstruction = @GetDate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Instruction_Started'
    rollback transaction
    return @Error
end
