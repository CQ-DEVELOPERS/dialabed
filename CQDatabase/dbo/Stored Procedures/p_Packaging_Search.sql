﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Packaging_Search
  ///   Filename       : p_Packaging_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Packaging_Search
(
 @jobId int
)
 
as
begin
	 set nocount on;
	 
	 --declare @ReceiptId     int,
	 --        @ReceiptLineId int
	 
	 --select @ReceiptLineId = i.ReceiptLineId
	 --  from Job         j (nolock)
	 --  join Instruction i (nolock) on j.JobId = i.JobId
	 -- where j.JobId = @jobId
  
  --select @ReceiptId = ReceiptId
  --  from ReceiptLine (nolock)
  -- where ReceiptLineId = @ReceiptLineId
  
  select rlp.PackageLineId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         rlp.AcceptedQuantity
    from ReceiptLinePackaging rlp
    join StorageUnitBatch     sub (nolock) on rlp.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit           su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join Product                p (nolock) on su.ProductId           = p.ProductId
    join SKU                  sku (nolock) on su.SKUId               = sku.SKUId
    join Batch                  b (nolock) on sub.BatchId            = b.BatchId
  where rlp.JobId = @jobId
end
