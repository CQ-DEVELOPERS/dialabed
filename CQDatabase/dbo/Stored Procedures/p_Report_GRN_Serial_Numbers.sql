﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_GRN_Serial_Numbers
  ///   Filename       : p_Report_GRN_Serial_Numbers.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_GRN_Serial_Numbers
(
 @InboundShipmentId int = null,
 @ReceiptId         int = null
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as Table
  (
   InboundShipmentId		int,
   InboundDocumentId		int,
   ReceiptId				int,
   WarehouseId				int
  );
  
  declare @TableDetails as Table
  (
   ReceiptId			int,
   ReceiptLineId		int,
   StorageUnitBatchId	int,
   StorageUnitId		int,
   ProductCode			nvarchar(30),
   Product				nvarchar(255),
   SKUCode				nvarchar(50),
   SerialNumber			nvarchar(50)
  );
  
  declare @Matrix as Table
  (
   ProductCode			nvarchar(30),
   Product				nvarchar(255),
   SerialNumber			nvarchar(50),
   Sequence				int,
   SetNumber			int
  );
  
  declare @count int
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  if @InboundShipmentId is not null
    insert @TableHeader
          (InboundShipmentId,
           InboundDocumentId,
           ReceiptId,
           WarehouseId)
    select isr.InboundShipmentId,
           id.InboundDocumentId,
           r.ReceiptId,
           id.WarehouseId
      from InboundShipmentReceipt isr (nolock)
      join Receipt                  r (nolock) on isr.ReceiptId = r.ReceiptId
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
     where isr.InboundShipmentId = isnull(@InboundShipmentId, isr.InboundShipmentId)
  else 
  if @ReceiptId is not null
    insert @TableHeader
          (InboundDocumentId,
           ReceiptId,
           WarehouseId)
    select id.InboundDocumentId,
           r.ReceiptId,
           id.WarehouseId
      from Receipt                  r (nolock)
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
      where r.ReceiptId = isnull(@ReceiptId, r.ReceiptId)
  
 
  insert @TableDetails
        (ReceiptId,
         ReceiptLineId,
         StorageUnitBatchId,
         ProductCode,
         Product,
         StorageUnitId,
         SerialNumber)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         rl.StorageUnitBatchId,
         p.ProductCode,
         p.Product,
         su.StorageUnitId,
         sn.SerialNumber
    from @TableHeader th 
    join ReceiptLine  rl (nolock) on th.ReceiptId     = rl.ReceiptId
    join InboundLine  il (nolock) on rl.InboundLineId = il.InboundLineId
    left join Operator o (nolock) on rl.OperatorId    = o.OperatorId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    left join SerialNumber     sn  (nolock) on rl.ReceiptId		= sn.ReceiptId
											and rl.ReceiptLineId = sn.ReceiptLineId       

  insert @Matrix
        (ProductCode,
         Product,
         SerialNumber,
         sequence,
         SetNumber)
  select distinct 
         td.ProductCode,
         td.Product,
         td.SerialNumber,
         ROW_NUMBER() OVER (partition by ProductCode ORDER BY ProductCode) Rank,
         1
    from @TableDetails td 
   where ProductCode is not null
     and SerialNumber is not null 
 

	while exists (select sequence from @Matrix
		where Sequence > 6)
	begin
		update mx
		   set Sequence = (Sequence - 6),
			   SetNumber = SetNumber + 1
	      from @Matrix mx
	     where Sequence > 6
	end
    
	select * from @Matrix
  
end

 
