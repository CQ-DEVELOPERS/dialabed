﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Picking_Achieved
  ///   Filename       : p_Dashboard_Outbound_Picking_Achieved.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Picking_Achieved
(
 @WarehouseId int = null,
 @Summarise   bit = 0,
 @GroupBy     nvarchar(30) = null
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
    if @GroupBy = 'Operator' or @GroupBy is null
	     select convert(nvarchar(2), d.CreateDate, 108) + 'H00' as 'Legend',
             o.Operator,
             OperatorKPI,
             WarehouseKPI,
             sum(d.Value) as 'Value'
        from DashboardOutboundPickingAchieved d
        join Operator o on d.OperatorId = o.OperatorId
        join DashboardKPI kpi on kpi.FunctionId = 'PICKING'
       where d.WarehouseId = @WarehouseId
      group by o.Operator,
               kpi.OperatorKPI,
               kpi.WarehouseKPI,
               convert(nvarchar(2), d.CreateDate, 108) + 'H00'
      order by convert(nvarchar(2), d.CreateDate, 108) + 'H00',
               o.Operator

    if @GroupBy = 'Warehouse'
	     select convert(nvarchar(2), d.CreateDate, 108) + 'H00' as 'Legend',
             WarehouseKPI,
             sum(d.Value) as 'Value'
        from DashboardOutboundPickingAchieved d
        join Operator o on d.OperatorId = o.OperatorId
        join DashboardKPI kpi on kpi.FunctionId = 'PICKING'
       where d.WarehouseId = @WarehouseId
      group by kpi.WarehouseKPI,
               convert(nvarchar(2), d.CreateDate, 108) + 'H00'
      order by convert(nvarchar(2), d.CreateDate, 108) + 'H00'

    if @GroupBy = 'Average'
	     select convert(nvarchar(2), d.CreateDate, 108) + 'H00' as 'Legend',
             OperatorKPI,
             sum(d.Value) / count(distinct(o.OperatorId)) as 'Value'
        from DashboardOutboundPickingAchieved d
        join Operator o on d.OperatorId = o.OperatorId
        join DashboardKPI kpi on kpi.FunctionId = 'PICKING'
       where d.WarehouseId = @WarehouseId
      group by kpi.OperatorKPI,
               convert(nvarchar(2), d.CreateDate, 108) + 'H00'
      order by convert(nvarchar(2), d.CreateDate, 108) + 'H00'
	 end
	 else
	 begin
	   truncate table DashboardOutboundPickingAchieved
	   
	   insert DashboardOutboundPickingAchieved
	         (WarehouseId,
           OperatorId,
           CreateDate,
           Legend,
           Value,
           KPI)
	   select i.WarehouseId,
           i.OperatorId,
           i.EndDate,
           null,
           SUM(i.ConfirmedQuantity),
           4400
	     from Instruction i (nolock)
	     join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
	                                        and it.InstructionTypeCode in ('PM','PS','FM','P')
	     join Job         j (nolock) on i.JobId = j.JobId
	     join Status      s (nolock) on j.StatusId = s.StatusId
	    where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and s.StatusCode in ('CK','CD','D','DC','C','S')
	      and i.EndDate >= CONVERT(nvarchar(10), getdate(), 120)
	   group by i.WarehouseId,
             i.OperatorId,
             i.EndDate
	 end
end
