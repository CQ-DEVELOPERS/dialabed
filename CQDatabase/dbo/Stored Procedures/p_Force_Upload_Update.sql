﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Force_Upload_Update
  ///   Filename       : p_Force_Upload_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Force_Upload_Update
(
 @InterfaceExportHeaderId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update InterfaceExportHeader
     set InsertDate = dateadd(hh, -6, InsertDate),
         WebServiceMsg = 'Added 6 hours to InsertDate (Previous : ' + convert(varchar(30), InsertDate, 120) + ')'
   where InterfaceExportHeaderId = @InterfaceExportHeaderId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Force_Upload_Update'
    rollback transaction
    return @Error
end
