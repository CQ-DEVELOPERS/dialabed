﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Linked_Products
  ///   Filename       : p_Static_Linked_Products.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Linked_Products
(
 @WarehouseId int,
 @BatchId     int
)
 
as
begin
	 set nocount on;
  
  select sub.StorageUnitBatchId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
   where sub.BatchId = @BatchId
end
