﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_Performance_Total
  ///   Filename       : p_Report_Operator_Performance_Total.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 06 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/

CREATE procedure p_Report_Operator_Performance_Total
(
	@StartDate				Datetime,
	@OperatorGroupId		int,
	@UOM                   nvarchar(10) = 'Units'
)
 
as
begin
	 set nocount on;
	

Declare @GroupPerformance As Table
(
	[OperatorGroupId]   int,
	[PerformanceType]	int,
	[Units]				float,
	[Weight]			float,
	[Instructions]		float,
	[Orders]			float,
	[OrderLines]		float,
	[Jobs]				int
)


Insert Into @GroupPerformance
(
	[OperatorGroupId]
	,[PerformanceType]
	 ,[Units]
	 ,[Weight]
	 ,[Instructions]
	 ,[Orders]
	 ,[OrderLines]
	 ,[Jobs]
)

(
Select		 O.OperatorGroupId,
			 GP.PerformanceType,
			Count(O.OperatorId) *  Sum(Distinct(GP.Units)) AS Units,
			Count(O.OperatorId) *  Sum(Distinct(GP.Weight)) As Weight,
			Count(O.OperatorId) *  Sum(Distinct(GP.Instructions)) As Instructions,
			Count(O.OperatorId) *  Sum(Distinct(GP.Orders)) As Orders,
			Count(O.OperatorId) *  Sum(Distinct(GP.OrderLines)) As OrderLines,
			Count(O.OperatorId) *  Sum(Distinct(GP.Jobs)) As Jobs	
FROM        OperatorPerformance Op INNER JOIN
                      Operator O ON Op.OperatorId = O.OperatorId INNER JOIN
					(SELECT 
						[OperatorGroupId]
					  ,[PerformanceType]
					  ,[Units]
					  ,[Weight]
					  ,[Instructions]
					  ,[Orders]
					  ,[OrderLines]
					  ,[Jobs]
							FROM [dbo].[GroupPerformance]
									Where OperatorGroupID = @OperatorGroupId
									And [PerformanceType] = 1
				Union
					SELECT [OperatorGroupId]
					  ,[PerformanceType]
					  ,[Units]
					  ,[Weight]
					  ,[Instructions]
					  ,[Orders]
					  ,[OrderLines]
					  ,[Jobs]
							FROM [dbo].[GroupPerformance]
									Where OperatorGroupID = @OperatorGroupId
									And [PerformanceType] = 2
					) AS GP ON O.OperatorGroupId = GP.OperatorGroupId 
Where convert(nvarchar,OP.EndDate,111) = convert(nvarchar,@StartDate,111)
And O.OperatorGroupId = @OperatorGroupId
Group By O.OperatorGroupId,GP.[PerformanceType]
)


	Declare @TempTable As Table
(
	OperatorGroupId int,
	Actual			int,
	SiteTar			int,
	Industry		int,
	Measure			nvarchar(10)	
)


if @UOM = 'Units'
  begin

					Insert Into @TempTable(Industry,SiteTar,OperatorGroupId)
					(
					Select X.Units,Y.Units,X.OperatorGroupId from 
					(Select Units,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = 6
					And GP.PerformanceType = 2
					) X Inner join
					 
					(Select Units,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = 6
					And GP.PerformanceType = 1
					) Y 
					
					On X.OperatorGroupId = Y.OperatorGroupId

					)

					Update @TempTable Set Actual = 
					(
					SELECT      
								Sum(OperatorPerformance.Units) as Units 
					FROM         OperatorPerformance INNER JOIN
										  Operator ON OperatorPerformance.OperatorId = Operator.OperatorId
					INNER JOIN
										  OperatorGroup ON Operator.OperatorGroupId = OperatorGroup.OperatorGroupId 
					WHERE convert(nvarchar,OperatorPerformance.EndDate,111) = convert(nvarchar,@StartDate,111)
					And Operator.OperatorGroupId = @OperatorGroupId
					),Measure = 'Units'
					Where OperatorGroupId = @OperatorGroupId
    
  end
   else if @UOM = 'Weight'
  
	begin

					Insert Into @TempTable(Industry,SiteTar,OperatorGroupId)
					(
					Select X.Weight,Y.Weight,X.OperatorGroupId from 
					(Select Weight,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = 6
					And GP.PerformanceType = 2
					) X Inner join
					 
					(Select Weight,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = 6
					And GP.PerformanceType = 1
					) Y 

					On X.OperatorGroupId = Y.OperatorGroupId

					)




					Update @TempTable Set Actual = 
						(
						SELECT      
									Sum(OperatorPerformance.Weight) as Weight 
						FROM         OperatorPerformance INNER JOIN
											  Operator ON OperatorPerformance.OperatorId = Operator.OperatorId
						INNER JOIN
											  OperatorGroup ON Operator.OperatorGroupId = OperatorGroup.OperatorGroupId 
						WHERE convert(nvarchar,OperatorPerformance.EndDate,111) = convert(nvarchar,@StartDate,111)
						And Operator.OperatorGroupId = @OperatorGroupId
						),Measure = 'KG'
						Where OperatorGroupId = @OperatorGroupId
      

	end
 else if @UOM = 'Jobs'
  
	begin

					Insert Into @TempTable(Industry,SiteTar,OperatorGroupId)
					(
					Select X.Jobs,Y.Jobs,X.OperatorGroupId from 
					(Select Jobs,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = 6
					And GP.PerformanceType = 2
					) X Inner join
					 
					(Select Jobs,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = 6
					And GP.PerformanceType = 1
					) Y 

					On X.OperatorGroupId = Y.OperatorGroupId

					)


					Update @TempTable Set Actual = 
						(
						SELECT      
									Sum(OperatorPerformance.Jobs) as Jobs 
						FROM         OperatorPerformance INNER JOIN
											  Operator ON OperatorPerformance.OperatorId = Operator.OperatorId
						INNER JOIN
											  OperatorGroup ON Operator.OperatorGroupId = OperatorGroup.OperatorGroupId 
						WHERE convert(nvarchar,OperatorPerformance.EndDate,111) = convert(nvarchar,@StartDate,111)
						And Operator.OperatorGroupId = @OperatorGroupId
						),Measure = 'Jobs'
						Where OperatorGroupId = @OperatorGroupId
      
	end

Select OperatorGroupId,  
	   Actual,			
	   SiteTar,			
       Industry,Measure
		 From @TempTable


end	   		
