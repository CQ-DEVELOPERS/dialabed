﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLine_Update
  ///   Filename       : p_ReceiptLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2013 07:02:51
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ReceiptLine table.
  /// </remarks>
  /// <param>
  ///   @ReceiptLineId int = null,
  ///   @ReceiptId int = null,
  ///   @InboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @RequiredQuantity float = null,
  ///   @ReceivedQuantity float = null,
  ///   @AcceptedQuantity float = null,
  ///   @RejectQuantity float = null,
  ///   @DeliveryNoteQuantity float = null,
  ///   @SampleQuantity numeric(13,6) = null,
  ///   @AcceptedWeight numeric(13,6) = null,
  ///   @ChildStorageUnitBatchId int = null,
  ///   @NumberOfPallets int = null,
  ///   @AssaySamples float = null,
  ///   @BOELineNumber nvarchar(50) = null,
  ///   @CountryofOrigin nvarchar(50) = null,
  ///   @TariffCode nvarchar(50) = null,
  ///   @Reference1 nvarchar(50) = null,
  ///   @Reference2 nvarchar(50) = null,
  ///   @UnitPrice decimal(13,2) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLine_Update
(
 @ReceiptLineId int = null,
 @ReceiptId int = null,
 @InboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @RequiredQuantity float = null,
 @ReceivedQuantity float = null,
 @AcceptedQuantity float = null,
 @RejectQuantity float = null,
 @DeliveryNoteQuantity float = null,
 @SampleQuantity numeric(13,6) = null,
 @AcceptedWeight numeric(13,6) = null,
 @ChildStorageUnitBatchId int = null,
 @NumberOfPallets int = null,
 @AssaySamples float = null,
 @BOELineNumber nvarchar(50) = null,
 @CountryofOrigin nvarchar(50) = null,
 @TariffCode nvarchar(50) = null,
 @Reference1 nvarchar(50) = null,
 @Reference2 nvarchar(50) = null,
 @UnitPrice decimal(13,2) = null,
 @ReceivedDate datetime = null,
 @PutawayStarted datetime = null,
 @PutawayEnded datetime = null
)
 
as
begin
	 set nocount on;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @InboundLineId = '-1'
    set @InboundLineId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ChildStorageUnitBatchId = '-1'
    set @ChildStorageUnitBatchId = null;
  
	 declare @Error int
 
  update ReceiptLine
     set ReceiptId = isnull(@ReceiptId, ReceiptId),
         InboundLineId = isnull(@InboundLineId, InboundLineId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         StatusId = isnull(@StatusId, StatusId),
         OperatorId = isnull(@OperatorId, OperatorId),
         RequiredQuantity = isnull(@RequiredQuantity, RequiredQuantity),
         ReceivedQuantity = isnull(@ReceivedQuantity, ReceivedQuantity),
         AcceptedQuantity = isnull(@AcceptedQuantity, AcceptedQuantity),
         RejectQuantity = isnull(@RejectQuantity, RejectQuantity),
         DeliveryNoteQuantity = isnull(@DeliveryNoteQuantity, DeliveryNoteQuantity),
         SampleQuantity = isnull(@SampleQuantity, SampleQuantity),
         AcceptedWeight = isnull(@AcceptedWeight, AcceptedWeight),
         ChildStorageUnitBatchId = isnull(@ChildStorageUnitBatchId, ChildStorageUnitBatchId),
         NumberOfPallets = isnull(@NumberOfPallets, NumberOfPallets),
         AssaySamples = isnull(@AssaySamples, AssaySamples),
         BOELineNumber = isnull(@BOELineNumber, BOELineNumber),         
         CountryofOrigin = isnull(@CountryofOrigin, CountryofOrigin),
         TariffCode = isnull(@TariffCode, TariffCode),
         Reference1 = isnull(@Reference1, Reference1),
         Reference2 = isnull(@Reference2, Reference2),
         UnitPrice = isnull(@UnitPrice, UnitPrice),
		 ReceivedDate = isnull(@ReceivedDate, ReceivedDate),
		 PutawayStarted = isnull(@PutawayStarted, PutawayStarted),
		 PutawayEnded  = isnull(@PutawayEnded, PutawayEnded)
   where ReceiptLineId = @ReceiptLineId
  
  select @Error = @@Error
  
  
  return @Error
  
end
