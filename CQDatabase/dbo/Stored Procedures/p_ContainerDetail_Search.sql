﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerDetail_Search
  ///   Filename       : p_ContainerDetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:56
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ContainerDetail table.
  /// </remarks>
  /// <param>
  ///   @ContainerDetailId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ContainerDetail.ContainerDetailId,
  ///   ContainerDetail.ContainerHeaderId,
  ///   ContainerDetail.StorageUnitBatchId,
  ///   ContainerDetail.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerDetail_Search
(
 @ContainerDetailId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ContainerDetailId = '-1'
    set @ContainerDetailId = null;
  
 
  select
         ContainerDetail.ContainerDetailId
        ,ContainerDetail.ContainerHeaderId
        ,ContainerDetail.StorageUnitBatchId
        ,ContainerDetail.Quantity
    from ContainerDetail
   where isnull(ContainerDetail.ContainerDetailId,'0')  = isnull(@ContainerDetailId, isnull(ContainerDetail.ContainerDetailId,'0'))
  
end
