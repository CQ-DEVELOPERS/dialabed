﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_List
  ///   Filename       : p_InstructionType_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:54
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InstructionType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InstructionType.InstructionTypeId,
  ///   InstructionType.InstructionType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InstructionTypeId
        ,'{All}' as InstructionType
  union
  select
         InstructionType.InstructionTypeId
        ,InstructionType.InstructionType
    from InstructionType
  
end
