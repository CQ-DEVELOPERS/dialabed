﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Question_Insert
  ///   Filename       : p_Question_Insert.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Question_Insert
(
	@QuestionaireId		int,
	@Category			nvarchar(50),
	@Code				nvarchar(50),
	@Sequence			int,
	@Active				bit,
	@QuestionText		nvarchar(255) ,
	@QuestionType		nvarchar(50),
	@QuestionId int = null output
	
)
as
begin
	 set nocount on;
  If not exists(Select * from Questions where QuestionText = @QuestionText and QuestionType = @QuestionType and QuestionaireId = @QuestionaireId) 
Begin

Insert into Questions (QuestionaireId,Category,Code,Sequence,Active,QuestionText,QuestionType)
select @QuestionaireId,
	@Category,
	@Code,
	@Sequence,
	@Active,
	@QuestionText,
	@QuestionType

   Select @QuestionId = Max(QuestionId)  from Questions where QuestionaireId = @QuestionaireId

end

End
