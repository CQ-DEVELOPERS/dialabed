﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Pickface_Check_Reject
  ///   Filename       : p_Mobile_Pickface_Check_Reject.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Mobile_Pickface_Check_Reject]
(
 @operatorId    int,
 @storageUnitId int,
 @locationId    int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
  
  begin transaction
  
  exec @Error = p_StorageUnitLocation_Delete
   @StorageUnitId = @StorageUnitId,
   @LocationId    = @LocationId
  
  if @Error <> 0
    goto error
  
  commit transaction
  select @Error
  return @Error
  
  error:
    raiserror 900000 'Error executing p_Mobile_Pickface_Check_Reject'
    rollback transaction
    select @Error
    return @Error
end
