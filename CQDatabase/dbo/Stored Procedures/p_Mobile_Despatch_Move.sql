﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Despatch_Move
  ///   Filename       : p_Mobile_Despatch_Move.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Despatch_Move
(
 @Barcode        nvarchar(30),
 @Location       nvarchar(15) = null output,
 @Pallets        int = null output,
 @Total          int = null output
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @OutboundShipmentId int,
          @OutboundDocumentId int,
          @JobId              int,
          @StatusCode         nvarchar(10),
          @PalletId           int,
          @ReferenceNumber    nvarchar(30),
          @IssueId            int,
          @LocationId         int,
          @InstructionId      int,
          @DespatchBay        int,
          @WarehouseId        int,
          @RepackLocationId   int,
          @DespatchCode       nvarchar(15)
  
  declare @InstructionTable table
  (
	InstructionId int
  )
  
  select @GetDate = dbo.ufn_Getdate()
  select @Location = replace(@Location,'L:','')
  set @Error = 0
  set @Pallets = 0
  set @Total = 0
  
  select @LocationId = LocationId
    from Location (nolock)
   where Location = @Location
  
  --begin transaction
  
  if @Location != '-1'
    if @LocationId is null
    begin
      set @Error = 900009
      set @Errormsg = 'Invalid Location'
      goto error
    end
  
  if @barcode is not null
  begin
    if isnumeric(replace(@barcode,'J:','')) = 1
    begin
      select @JobId           = replace(@barcode,'J:',''),
             @barcode = null
      
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId,
             @WarehouseId        = j.WarehouseId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
        join Job                    j (nolock) on i.JobId           = j.JobId
        join Status                 s (nolock) on j.StatusId        = s.StatusId
       where j.JobId = @JobId
    end
    else if isnumeric(replace(@barcode,'P:','')) = 1
    begin
      select @PalletId        = replace(@barcode,'P:',''),
             @barcode = null
      
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId,
             @WarehouseId        = j.WarehouseId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
        join Job                    j (nolock) on i.JobId           = j.JobId
        join Status                 s (nolock) on j.StatusId        = s.StatusId
       where i.PalletId = @PalletId
    end
    else if isnumeric(replace(@barcode,'R:','')) = 1
      select @ReferenceNumber = @barcode,
             @barcode         = null
    begin
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId,
             @WarehouseId        = j.WarehouseId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
        join Job                    j (nolock) on i.JobId           = j.JobId
        join Status                 s (nolock) on j.StatusId        = s.StatusId
       where j.ReferenceNumber = @ReferenceNumber
    end
    
    if @JobId is null
    begin
      set @Error = 900006
      set @ErrorMsg = 'Could not find the pallet'
      goto error
    end
    
    if @StatusCode = 'C'
    begin
      set @Error = 900005
      set @ErrorMsg = 'The pallet is already complete'
      goto error
    end
    
    if (dbo.ufn_Configuration(187, @WarehouseId) = 1)
    begin
      if exists(select 1 
                  from Instruction            i (nolock)
                  join IssueLineInstruction ili (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
                  join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
                  join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
                  join PackExternalCompany  pec (nolock) on i.WarehouseId          = pec.WarehouseId
                                                       and sub.StorageUnitId       = pec.StorageUnitId
                                                       and od.ExternalCompanyId    = pec.ExternalCompanyId
                where i.JobId = @JobId)
      begin
        select @RepackLocationId = l.LocationId
          from Location      l (nolock)
          join AreaLocation al (nolock) on l.LocationId = al.LocationId
          join Area          a (nolock) on al.AreaId    = a.AreaId
         where a.AreaCode = 'RP'
      end
    end
    
    if @OutboundShipmentId is not null
    begin
      select @DespatchBay = DespatchBay
        from OutboundShipment (nolock)
       where OutboundShipmentId = @OutboundShipmentId
      
      select @DespatchCode = Location
        from Location
       where LocationId = @DespatchBay
      
      select @DespatchBay = isnull(@RepackLocationId, @DespatchBay)
      
      if @DespatchBay is null
      begin
        update OutboundShipment
           set DespatchBay = @LocationId
         where OutboundShipmentId = @OutboundShipmentId
      end
      else if @DespatchBay != @LocationId and @Location != 'Consolidate' and @DespatchCode != 'Consolidate'
      begin
        set @Error = 900009
        set @Errormsg = 'Invalid Location'
        goto error
      end
      
      -- Need to update this later
      --update Instruction
      --   set StoreLocationId = @LocationId
      --  where JobId = @JobId
      
      select @Pallets = count(distinct(ins.JobId))
        from Instruction           ins
        join Job                     j on ins.JobId = j.JobId
        join Status                  s on j.StatusId = s.StatusId
       where ins.OutboundShipmentId = @OutboundShipmentId
         and s.StatusCode != 'D'
         and j.ReferenceNumber like '%NB%'
      
      select @Total = count(distinct(ins.JobId))
        from Instruction           ins (nolock)
        join Job                     j (nolock) on ins.JobId = j.JobId
        join Status                  s (nolock) on j.StatusId = s.StatusId
       where ins.OutboundShipmentId = @OutboundShipmentId
         and j.ReferenceNumber like '%NB%'
    end
    else if @IssueId is not null
    begin
      select @DespatchBay = DespatchBay
        from Issue (nolock)
       where IssueId = @IssueId
      
      select @DespatchCode = Location
        from Location
       where LocationId = @DespatchBay
      
      select @DespatchBay = isnull(@RepackLocationId, @DespatchBay)
      
      if @DespatchBay is null
      begin
        update Issue
           set DespatchBay = @LocationId
         where IssueId = @IssueId
      end
      else if @DespatchBay != @LocationId and @Location != 'Consolidate' and @DespatchCode != 'Consolidate'
      begin
        set @Error = 900009
        set @Errormsg = 'Invalid Location'
        goto error
      end
      
      -- Need to update this later
      --update Instruction
      --   set StoreLocationId = @LocationId
      --  where JobId = @JobId
      
      select @Pallets = count(distinct(ins.JobId))
        from IssueLineInstruction  ili (nolock)
        join Instruction           ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId)
        join Job                     j (nolock) on ins.JobId = j.JobId
        join Status                  s (nolock) on j.StatusId = s.StatusId
       where ili.IssueId = @IssueId
         and s.StatusCode != 'D'
      
      select @Total = count(distinct(ins.JobId))
        from IssueLineInstruction  ili (nolock)
        join Instruction           ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId)
       where ili.IssueId = @IssueId
    end
    
    if @Location != 'Consolidate'
    begin
      if @StatusCode = 'CD' and @Location != '-1'
      begin
        exec @Error = p_Status_Rollup
         @jobId = @jobId
        
        if @Error <> 0
          goto error
      end
      else
      begin
        if @StatusCode = 'CD' and @Location = '-1'
        begin
          set @Error = 0
        end
        else
        begin
          set @Error = 900003
          set @ErrorMsg = 'Incorrect Status'
          goto error
        end
      end
    end
  end
    
  insert @InstructionTable 
		(InstructionId) 
  select InstructionId 
    from Instruction (nolock)
   where JobId = @JobId
  
    
  while exists (select InstructionId from @InstructionTable)
	begin

    select top 1 @InstructionId = InstructionId
    from @InstructionTable
    order by InstructionId asc

    declare @Stored int,
            @Picked int,
            @StorageUnitBatchId int
     select @Stored = Stored,
            @Picked = Picked,
            @StorageUnitBatchId = StorageUnitBatchId
       from Instruction
      where Instructionid = @InstructionId
      
       exec	@Error = p_StorageUnitBatchLocation_Reverse
			@instructionId = @instructionId,
			@Store = @Stored,
			@Confirmed = 1
       
       exec @Error = p_StorageUnitBatchLocation_Deallocate
			@instructionId = @instructionId,
			@Store = @Stored,
			@Confirmed = 1
       
       if @Error <> 0
       begin
         set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Deallocate'
         goto error
       end
       
       exec @Error = p_Instruction_Update
			 @instructionId      = @instructionId,
			 @Stored = null,
			 @storeLocationId    = @LocationId
	    
       if @Error <> 0
       begin
         set @ErrorMsg = 'Error executing p_Instruction_Update'
         goto error
       end
       
       exec @Error = p_StorageUnitBatchLocation_Reserve
			@instructionId = @instructionId,
			@Store = 1,
			@Confirmed = 1
       
       if @Error <> 0
       begin
         set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
         goto error
       end
       
       exec @Error = p_StorageUnitBatchLocation_Allocate
			@instructionId = @instructionId,
			@Store = 1,
			@Confirmed = 1
       
       if @Error <> 0
       begin
         set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
         goto error
       end 

    delete @InstructionTable
    where InstructionId = @InstructionId

	end
  
  if @DespatchBay is not null
    select @Location = Location
      from Location
     where LocationId = @DespatchBay
  
  if @Pallets = 0
    set @Pallets = 1
  
  --commit transaction
  select @Error
  return @Error
  
  error:
    --raiserror @Error @Errormsg
    --rollback transaction
    select @Error
    return @Error
end
