﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_Flo_ID
  ///   Filename       : p_FamousBrands_Export_Flo_ID.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Export_Flo_ID
(
 @FileName varchar(30) = null output
)
 
as
begin
	 set nocount on;
  --select @FileName = 'INVOICEDETAIL.TXT'
  select @FileName = ''
  
  select convert(varchar(10), ID) + ',"' +
         UniqueNumber + '","' + 
         ProductCode + '","' + 
         Units + '",' + 
         convert(varchar(20), Quantity) + ',"' + 
         PickUp + '","' + 
         isnull(convert(varchar(10), ShippedQuantity),'') + '"'
    from FloInvoiceDetailExport
   where RecordStatus = 'N'
  
  update FloInvoiceDetailExport
     set RecordStatus = 'Y'
   where RecordStatus = 'N'
end
