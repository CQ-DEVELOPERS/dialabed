﻿--IF OBJECT_ID('dbo.p_Report_Stock_On_Hand') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Report_Stock_On_Hand
--    IF OBJECT_ID('dbo.p_Report_Stock_On_Hand') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Report_Stock_On_Hand >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Report_Stock_On_Hand >>>'
--END
--go

 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_On_Hand
  ///   Filename       : p_Report_Stock_On_Hand.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Jan 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Report_Stock_On_Hand]
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @BatchId           int = null,
 @StoreLocationId   int = null,
 @PickLocationId    int = null,
 @LocationTypeId    int = null,
 @BatchMore         int = -1,
 @BatchQuantity     float = 0,
 @AllocatedMore     int = -1,
 @AllocatedQuantity float = 0,
 @ReservedMore      int = -1,
 @ReservedQuantity  float = 0,
 @PrincipalId       int = null,
 @ProductId			int = null
)
 


as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	   ProductCode        nvarchar(30),
	   Product            nvarchar(255),
	   SKUCode            nvarchar(50),
	   SKU				  nvarchar(50),
	   Batch              nvarchar(30),
	   ExpiryDate		  datetime,
	   Area               nvarchar(50),
	   Location           nvarchar(15),
	   ActualQuantity     numeric(13,6),
	   AllocatedQuantity  numeric(13,6),
	   ReservedQuantity   numeric(13,6),
	   BatchQuantity      numeric(13,6),
	   Status			  nvarchar(50),
	   NettWeight		  float,
	   PalletId			  int,
	   StorageUnitId	  int,
	   ProductCategory    char(1),
	   Size				  nvarchar(100),
	   Colour			  nvarchar(100),
	   Style			  nvarchar(100),
	   Principal		  nvarchar(50)
	 )
	 
	declare @InboundSequence	int,
			@ShowWeight			bit = null,
			@StoreLocation		varchar(15),
			@PickLocation		varchar(15)
			

  select 	@StoreLocation = location
  from		Location
  where		locationid =  @StoreLocationId 
  
  select 	@PickLocation = location
  from		Location
  where		locationid =  @PickLocationId 
	 
  if @StorageUnitId = -1 set @StorageUnitId = null
  if @BatchId = -1 set @BatchId = null
  if @StoreLocationId = -1 set @StoreLocationId = null
  if @PickLocationId = -1 set @PickLocationId = null
  if @StoreLocationId = -1 set @StoreLocation = null
  if @PickLocationId = -1 set @PickLocation = null
  if @LocationTypeId = -1 set @LocationTypeId = null
  if @BatchMore = -1 set @BatchMore = null
  if @AllocatedMore = -1 set @AllocatedMore = null
  if @ReservedMore = -1 set @ReservedMore = null
  if @PrincipalId = -1 set @PrincipalId = null
  if @ProductId = -1 set @ProductId = null
  
	if @ProductId is not null
		 insert @TableResult
	       (ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ExpiryDate,
         Area,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         BatchQuantity,
         Status,
         StorageUnitId,
         ProductCategory,
         NettWeight,
         Size,
         Colour,
         Style,
		 Principal)
  select p.ProductCode,
         p.Product,
         sku.SKUCode,
         SKU,
         case when s.StatusCode != 'A'
              then b.Batch + '('+ s.StatusCode + ')'
            
  else b.Batch
              end as 'Batch',
         b.ExpiryDate,
         a.Area,
         l.Location,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         b.ActualYield,
         s.Status,
         sub.StorageUnitId,
         su.ProductCategory,
         subl.NettWeight,
         su.Size,
         su.Colour,
         su.Style,
		 pr.Principal
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
	left
	join Principal				   pr(nolock) on p.PrincipalId = pr.PrincipalId
    join SKU                      sku (nolock) on su.SKUId                = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
   where a.WarehouseId      = @WarehouseId
     and su.StorageUnitId  in (SELECT StorageUnitId from StorageUnit where ProductId = @ProductId )
     and b.BatchId          = isnull(@BatchId, b.BatchId)
     --and l.LocationId between isnull(@StoreLocationId, l.LocationId) and isnull(@PickLocationId, l.LocationId)
     and l.Location between isnull(@StoreLocation, l.Location) and isnull(@PickLocation, l.Location)
     and l.LocationTypeId   = isnull(@LocationTypeId, l.LocationTypeId)
     and a.StockOnHand      = 1
     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
	else
	 insert @TableResult
	       (ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ExpiryDate,
         Area,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         BatchQuantity,
         Status,
         StorageUnitId,
         ProductCategory,
         NettWeight,
         Size,
         Colour,
         Style,Principal)
  select p.ProductCode,
         p.Product,
         sku.SKUCode,
         SKU,   
      case when s.StatusCode != 'A'
              then b.Batch + '('+ s.StatusCode + ')'
              else b.Batch
              end as 'Batch',
         b.ExpiryDate,
         a.Area,
         l.Location,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         b.ActualYield,
         s.Status,
         sub.StorageUnitId,
         su.ProductCategory,
         subl.NettWeight,
         su.Size,
         su.Colour,
         su.Style,
		 pr.Principal
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
	left
	join Principal				   pr(nolock) on p.PrincipalId = pr.PrincipalId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
   where a.WarehouseId      = @WarehouseId
     and su.StorageUnitId   = isnull(@StorageUnitId, su.StorageUnitId)
     and b.BatchId          = isnull(@BatchId, b.BatchId)
     --and l.LocationId between isnull(@StoreLocationId, l.LocationId) and isnull(@PickLocationId, l.LocationId)
     and l.Location between isnull(@StoreLocation, l.Location) and isnull(@PickLocation, l.Location)
     and l.LocationTypeId   = isnull(@LocationTypeId, l.LocationTypeId)
     and a.StockOnHand      = 1
     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
  
  if @@rowcount = 0
    insert @TableResult
          (ProductCode, AllocatedQuantity)
    select '', 0
  
  if @BatchMore is not null
    if @BatchMore = 0
      delete @TableResult where BatchQuantity >= @BatchQuantity
    else
      delete @TableResult where BatchQuantity <= @BatchQuantity
  
  if @AllocatedMore is not null
    if @AllocatedMore = 0
      delete @TableResult where AllocatedQuantity >= @AllocatedQuantity
    else
      delete @TableResult where AllocatedQuantity <= @AllocatedQuantity
  
  if @ReservedMore is not null
    if @ReservedMore = 0
      delete @TableResult where ReservedQuantity >= @ReservedQuantity
    else
      delete @TableResult where ReservedQuantity <= @ReservedQuantity
  
  select @InboundSequence = max(InboundSequence)
    from PackType (nolock)
  
  select @ShowWeight = Indicator
  from Configuration where ConfigurationId = 206
  

  if  @ShowWeight = 1
  update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                             from StorageUnitBatch       sub (nolock) 
					                        join StorageUnit             su (nolock) on sub.StorageUnitId = su.StorageUnitId
					                        join Product                  p (nolock) on su.ProductId      = p.ProductId
					                        join Pack                    pk (nolock) on su.StorageUnitId  = pk.StorageUnitId
					                      
  join PackType                pt (nolock) on pk.PackTypeId     = pt.PackTypeId
                            where pt.InboundSequence = @InboundSequence
                              and p.ProductCode = tr.ProductCode
                              and isnull(su.ProductCategory,'') != 'V')* tr.ActualQuantity)
   from @TableResult tr
  where tr.ProductCategory != 'V'
  
  if  @ShowWeight = 1 and (select dbo.ufn_Configuration(302, @WarehouseId)) = 1 -- SOH - Show pallet id?
  update tr
     set tr.PalletId = (select max(pl.PalletId)
                          from StorageUnitBatchLocation subl (nolock)
						                    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
						                    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
						                    join Product                     p (nolock) on su.ProductId            = p.ProductId
						                    left join Pallet				          		pl (nolock) on pl.StorageUnitBatchId   = subl.StorageUnitBatchId
			where p.ProductCode = tr.ProductCode)
    from @TableResult tr
  
  select ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ExpiryDate,
         Area,    
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         Status,
         NettWeight,
         PalletId,
         ProductCategory,
         Size,
         Colour,
         Style,
		 Principal,
        (select Indicator from Configuration where ConfigurationId = 206 and WarehouseId = @WarehouseId) as 'PrtWeight',
        (select Indicator from Configuration where ConfigurationId = 302 and WarehouseId = @WarehouseId) as 'PrtPallet'
    from @TableResult
  order by ProductCode


end

--go
--IF OBJECT_ID('dbo.p_Report_Stock_On_Hand') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Report_Stock_On_Hand >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Report_Stock_On_Hand >>>'
--go


