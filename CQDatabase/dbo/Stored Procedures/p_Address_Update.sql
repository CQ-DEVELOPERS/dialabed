﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Address_Update
  ///   Filename       : p_Address_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:19
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Address table.
  /// </remarks>
  /// <param>
  ///   @AddressId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @Street nvarchar(200) = null,
  ///   @Suburb nvarchar(200) = null,
  ///   @Town nvarchar(200) = null,
  ///   @Country nvarchar(200) = null,
  ///   @Code nvarchar(40) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Address_Update
(
 @AddressId int = null,
 @ExternalCompanyId int = null,
 @Street nvarchar(200) = null,
 @Suburb nvarchar(200) = null,
 @Town nvarchar(200) = null,
 @Country nvarchar(200) = null,
 @Code nvarchar(40) = null 
)
 
as
begin
	 set nocount on;
  
  if @AddressId = '-1'
    set @AddressId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
	 declare @Error int
 
  update Address
     set ExternalCompanyId = isnull(@ExternalCompanyId, ExternalCompanyId),
         Street = isnull(@Street, Street),
         Suburb = isnull(@Suburb, Suburb),
         Town = isnull(@Town, Town),
         Country = isnull(@Country, Country),
         Code = isnull(@Code, Code) 
   where AddressId = @AddressId
  
  select @Error = @@Error
  
  
  return @Error
  
end
