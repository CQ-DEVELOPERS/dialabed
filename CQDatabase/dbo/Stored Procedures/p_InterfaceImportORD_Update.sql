﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportORD_Update
  ///   Filename       : p_InterfaceImportORD_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:48
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportORD table.
  /// </remarks>
  /// <param>
  ///   @RecordStatus nvarchar(40) = null,
  ///   @InsertDate datetime = null,
  ///   @RecordType nvarchar(40) = null,
  ///   @ID nvarchar(40) = null,
  ///   @Ordernumber nvarchar(100) = null,
  ///   @StatusId nvarchar(100) = null,
  ///   @OrderDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportORD_Update
(
 @RecordStatus nvarchar(40) = null,
 @InsertDate datetime = null,
 @RecordType nvarchar(40) = null,
 @ID nvarchar(40) = null,
 @Ordernumber nvarchar(100) = null,
 @StatusId nvarchar(100) = null,
 @OrderDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceImportORD
     set RecordStatus = isnull(@RecordStatus, RecordStatus),
         InsertDate = isnull(@InsertDate, InsertDate),
         RecordType = isnull(@RecordType, RecordType),
         ID = isnull(@ID, ID),
         Ordernumber = isnull(@Ordernumber, Ordernumber),
         StatusId = isnull(@StatusId, StatusId),
         OrderDate = isnull(@OrderDate, OrderDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
