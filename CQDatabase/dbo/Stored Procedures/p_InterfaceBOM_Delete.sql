﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOM_Delete
  ///   Filename       : p_InterfaceBOM_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:46
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceBOM table.
  /// </remarks>
  /// <param>
  ///   @InterfaceBOMId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOM_Delete
(
 @InterfaceBOMId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceBOM
     where InterfaceBOMId = @InterfaceBOMId
  
  select @Error = @@Error
  
  
  return @Error
  
end
