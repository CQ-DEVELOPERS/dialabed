﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_GetSwitch
  ///   Filename       : p_Configuration_GetSwitch.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_GetSwitch
(
 @configurationId int,
 @WarehouseId int = 1
)
 
as
begin
  declare @result bit
  
  select @result = dbo.ufn_Configuration(@configurationId, @warehouseId)
  
  select @result
end
