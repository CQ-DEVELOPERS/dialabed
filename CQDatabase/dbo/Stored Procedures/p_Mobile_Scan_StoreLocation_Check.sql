﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Scan_StoreLocation_Check
  ///   Filename       : p_Mobile_Scan_StoreLocation_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Scan_StoreLocation_Check
(
 @ReceiptLineId int
)
 
as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
         ,@StorageUnitId int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction StoreLocation_Check;

		  -- Do the actual work here
    select @StorageUnitId = sub.StorageUnitId
      from ReceiptLine       rl (nolock)
      join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
     where rl.ReceiptLineId = @ReceiptLineId
     
	   if (select top 1 isnull(a.AutoStore,0)
          from Area a
          join StorageUnitArea sua on a.AreaId = sua.AreaId
         where sua.StorageUnitId = @StorageUnitId
         order by sua.StoreOrder) = 1
      RAISERROR ('p_Mobile_Scan_StoreLocation_Check',11,1);
    
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction StoreLocation_Check;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
