﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_Select
  ///   Filename       : p_OutboundLine_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jan 2014 07:56:01
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundLine table.
  /// </remarks>
  /// <param>
  ///   @OutboundLineId int = null 
  /// </param>
  /// <returns>
  ///   OutboundLine.OutboundLineId,
  ///   OutboundLine.OutboundDocumentId,
  ///   OutboundLine.StorageUnitId,
  ///   OutboundLine.StatusId,
  ///   OutboundLine.LineNumber,
  ///   OutboundLine.Quantity,
  ///   OutboundLine.BatchId,
  ///   OutboundLine.UnitPrice,
  ///   OutboundLine.UOM,
  ///   OutboundLine.BOELineNumber,
  ///   OutboundLine.TariffCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_Select
(
 @OutboundLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         OutboundLine.OutboundLineId
        ,OutboundLine.OutboundDocumentId
        ,OutboundLine.StorageUnitId
        ,OutboundLine.StatusId
        ,OutboundLine.LineNumber
        ,OutboundLine.Quantity
        ,OutboundLine.BatchId
        ,OutboundLine.UnitPrice
        ,OutboundLine.UOM
        ,OutboundLine.BOELineNumber
        ,OutboundLine.TariffCode
    from OutboundLine
   where isnull(OutboundLine.OutboundLineId,'0')  = isnull(@OutboundLineId, isnull(OutboundLine.OutboundLineId,'0'))
  order by UOM
  
end
