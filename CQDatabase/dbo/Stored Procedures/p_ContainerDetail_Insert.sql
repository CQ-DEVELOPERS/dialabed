﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerDetail_Insert
  ///   Filename       : p_ContainerDetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:55
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ContainerDetail table.
  /// </remarks>
  /// <param>
  ///   @ContainerDetailId int = null output,
  ///   @ContainerHeaderId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   ContainerDetail.ContainerDetailId,
  ///   ContainerDetail.ContainerHeaderId,
  ///   ContainerDetail.StorageUnitBatchId,
  ///   ContainerDetail.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerDetail_Insert
(
 @ContainerDetailId int = null output,
 @ContainerHeaderId int = null,
 @StorageUnitBatchId int = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
  if @ContainerDetailId = '-1'
    set @ContainerDetailId = null;
  
	 declare @Error int
 
  insert ContainerDetail
        (ContainerHeaderId,
         StorageUnitBatchId,
         Quantity)
  select @ContainerHeaderId,
         @StorageUnitBatchId,
         @Quantity 
  
  select @Error = @@Error, @ContainerDetailId = scope_identity()
  
  
  return @Error
  
end
