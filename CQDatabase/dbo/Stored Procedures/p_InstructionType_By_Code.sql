﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_By_Code
  ///   Filename       : p_InstructionType_By_Code.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_By_Code
(
 @InstructionTypeCode nvarchar(10)
)
 
as
begin
	 set nocount on;
	 
	 if @InstructionTypeCode = 'M'
	 begin
	   select InstructionTypeId,
	          InstructionType
      from InstructionType
     where InstructionTypeCode in ('M','R','O','HS','RS')
    union
    select -1,
           '{ALL}'
    order by InstructionTypeId
  end
  else if @InstructionTypeCode = 'D'
	 begin
	   select InstructionTypeId,
	          InstructionType
      from InstructionType
     where InstructionTypeCode in ('M','R','O','S','SM','PR')
  end
  else
	 begin
	   select InstructionTypeId,
	          InstructionType
      from InstructionType
     where InstructionTypeCode = @InstructionTypeCode
  end
  
end
