﻿
/*
 /// <summary>
 ///   Procedure Name : p_Replenishment_Stock_Request
 ///   Filename       : p_Replenishment_Stock_Request.sql
 ///   Create By      : Grant Schultz
 ///   Date Created   : 15 Mar 2010
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
 /// <newpara>
 ///   Modified by    : 
 ///   Modified Date  : 
 ///   Details        : 
 /// </newpara>
*/
CREATE procedure p_Replenishment_Stock_Request
(
 @WarehouseId int,
 @OutboundShipmentId int,
 @IssueId int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   IssueLineId                     int,
   IssueId                         int,
   OrderNumber                     nvarchar(30),
   StorageUnitBatchId              int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   BatchId                         int,
   Batch                           nvarchar(50),
   SKUCode                         nvarchar(50),
   RequiredQuantity                float default 0,
   ConfirmedQuatity                float default 0,
   AllocatedQuantity               float default 0,
   ManufacturingQuantity           float default 0,
   RawQuantity                     float default 0,
   ReplenishmentQuantity            float default 0,
   AvailableQuantity               float default 0,
   AvailablePercentage             numeric(13,2),
   AreaType                        nvarchar(10),
   WarehouseId                     int,
   DropSequence                    int
  )

  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @EmptyLocations     int,
          @StorageUnitId      int,
          @StorageUnitBatchId int,
          @StoreLocationId    int,
          @Quantity           numeric(13,6),
          @InstructionId      int,
          @OriginalQuantity   numeric(13,6),
          @RepQuantity        numeric(13,6)

  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null;
  
  if @IssueId = -1
    set @IssueId = null;
  
  if @IssueId is not null
  begin
    insert @TableResult
          (IssueLineId,
           IssueId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           RequiredQuantity,
           ConfirmedQuatity,
           AreaType,
           WarehouseId,
           DropSequence)
    select il.IssueLineId,
           il.IssueId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           il.ConfirmedQuatity,
           isnull(i.AreaType,''),
           i.WarehouseId,
           i.DropSequence
      from IssueLine         il
      join Issue              i (nolock) on il.IssueId            = i.IssueId
      join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument  od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status             s (nolock) on il.StatusId           = s.StatusId
     where il.IssueId = @IssueId
  end
  else if @OutboundShipmentId is not null
  begin
    insert @TableResult
          (IssueLineId,
           IssueId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           RequiredQuantity,
           ConfirmedQuatity,
           AreaType,
           WarehouseId,
           DropSequence)
    select il.IssueLineId,
           il.IssueId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           il.ConfirmedQuatity,
           isnull(i.AreaType,''),
           i.WarehouseId,
           i.DropSequence
      from IssueLine         il
      join Issue              i (nolock) on il.IssueId            = i.IssueId
      join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument  od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status             s (nolock) on il.StatusId           = s.StatusId
      join OutboundShipmentIssue osi (nolock) on il.IssueId       = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  
  update tr
     set AllocatedQuantity = a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ManufacturingQuantity = a.ActualQuantity - a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set RAWQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set AvailableQuantity = ManufacturingQuantity + RAWQuantity
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), (ManufacturingQuantity - AllocatedQuantity) + isnull(ReplenishmentQuantity,0)) / convert(numeric(13,3), RequiredQuantity)) * 100
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Batch       = b.Batch
    from @TableResult tr
    join Batch            b   (nolock) on tr.BatchId            = b.BatchId
  
  select @EmptyLocations = count(1) from @TableResult where AvailablePercentage < 100
  
  select @EmptyLocations as '@EmptyLocations', * from @TableResult
  
  set @StoreLocationId = null

  select top 1 @StoreLocationId = l.LocationId
    from Area a
    join AreaLocation al on a.AreaId = al.AreaId
    join Location l on al.LocationId = l.LocationId
   where a.AreaCode = 'SP'
     and a.Replenish = 1
  order by l.LocationId desc
  
  begin transaction
  
  while @EmptyLocations > 0
  begin
    set @StorageUnitId = null

    select top 1 @StorageUnitId    = StorageUnitId,
                 @Quantity         = RequiredQuantity,
                 @ORIGINALQUANTITY = RequiredQuantity
      from @TableResult
     where AvailablePercentage < 100
    order by AvailablePercentage
    
    if @StorageUnitId is null
    begin
      set @EmptyLocations = 0
    end
    else
    begin
      select @StorageUnitBatchId = StorageUnitBatchId
        from StorageUnitBatch sub (nolock)
        join Batch              b (nolock) on sub.BatchId = b.BatchId
       where sub.StorageUnitId = @StorageUnitId
         and b.Batch = 'Default'
      
      if @StorageUnitBatchId is null
        select @StorageUnitBatchId = StorageUnitBatchId
          from StorageUnitBatch sub (nolock)
          join Batch              b (nolock) on sub.BatchId = b.BatchId
         where sub.StorageUnitId = @StorageUnitId
      
      set @InstructionId = null
      
      exec @Error = p_Mobile_Replenishment_Request 
       @storeLocationId    = @StoreLocationId,
       @storageUnitBatchId = @StorageUnitBatchId,
       @operatorId         = null,
       @ShowMsg            = 0,
       @ManualReq          = 0,
       @AnyLocation        = 1,
       @AreaType           = 'Backup',
       @RequestedQuantity  = @Quantity output,
       @InstructionId      = @InstructionId output
      
      if @Error = 9 and @InstructionId is not null
      begin
      select * from viewins where InstructionId = @InstructionId
        select @RepQuantity = Quantity
          from Instruction (nolock)
         where InstructionId = @InstructionId
        
        set @Error = 0
        
        exec p_StorageUnitBatchLocation_Deallocate
         @InstructionId = @InstructionId
        
        if @Error <> 0
          goto Error
        
        select @RepQuantity = @RepQuantity + @OriginalQuantity
        
        exec @Error = p_Instruction_Update
         @InstructionId     = @InstructionId,
         @Quantity          = @RepQuantity,
         @ConfirmedQuantity = @RepQuantity
        
        if @Error <> 0
          goto Error
        
        exec @Error = p_StorageUnitBatchLocation_Reserve
         @InstructionId       = @instructionId
        
        if @Error <> 0
          goto Error
        
        select * from viewins where InstructionId = @InstructionId
      end
      
      if @Quantity >= @OriginalQuantity or @InstructionId is null or @Error <> 0
      begin
        select @EmptyLocations = @EmptyLocations - 1
        delete @TableResult where StorageUnitId = @StorageUnitId
      end
      else
      begin
        update @TableResult set RequiredQuantity = RequiredQuantity - @Quantity where StorageUnitId = @StorageUnitId
      end
      
      --select @EmptyLocations as '@EmptyLocations', @OriginalQuantity as '@OriginalQuantity', * from @TableResult
      
      if @Error <> 0
        select @Error as '@Error', @StorageUnitId as '@StorageUnitId', @StoreLocationId as '@StoreLocationId', @InstructionId as '@InstructionId'
      else if @InstructionId is not null
      begin
        exec @Error = p_Replenishment_Create_Pick
         @InstructionId = @InstructionId
        
        if @Error <> 0
          goto Error
      end
    end
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Replenishment_Stock_Request'
    rollback transaction
    return @Error
end
