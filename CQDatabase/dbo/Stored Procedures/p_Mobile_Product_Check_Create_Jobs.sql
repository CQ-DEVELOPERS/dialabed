﻿
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Create_Jobs
  ///   Filename       : p_Mobile_Product_Check_Create_Jobs.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Sep 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Create_Jobs
(
 @JobId			int,
 @Jobs			int,
 @operatorId	int = null,
 @instructionId int = null,
 @newJobId      int = null,
 @Weight        numeric(13,6) = null
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Mobile_Product_Check_Create_Jobs',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @NewBox            int,
          @TotalJobs         int = @Jobs,
          @updOriginal       bit = 1,
          @Quantity          numeric(13,6)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  if @newJobId = -1
    set @newJobId = null
  
  --if exists(select top 1 1
  --             from Instruction
  --            where JobId = @JobId
  --              and CheckQuantity > 0
  --              and (InstructionId = @instructionId or @instructionId is null)
  --              and ((convert(numeric(13,6), CheckQuantity) / @Jobs) % 1) != 0) -- Checks for any deciamls
  --begin
  --  set @Error = -1
  --  set @Errormsg = @Errormsg + ' - This would create decimals which is not allowed'
  --  goto Error
  --end
  
  if exists(select top 1 1
               from Instruction
              where JobId = @JobId
                and CheckQuantity > 0
                and (InstructionId = @instructionId or @instructionId is null)
                and CheckQuantity > ConfirmedQuantity) -- Checks for any mis checks
  begin
    set @Error = -1
    set @Errormsg = @Errormsg + ' - This would create negatives which is not allowed'
    goto Error
  end
  
  --set @Jobs = @Jobs - 1
  
  --if @instructionId is not null and @Jobs = 0
  --begin
  --  set @Jobs = 1
  --  set @updOriginal = 0
  --end
  
  if @instructionId is null
  begin
    set @Jobs = @Jobs - 1
  end
  else
  begin
    set @updOriginal = 0
  end
  
  while @Jobs > 0
  begin
    select @Jobs = @Jobs -1
    
    if @NewJobId is null
    begin
      set @NewBox = NULL
      
      exec p_Label_Reference @Value=@NewBox output,@WarehouseId=1
      
      insert Job
            (PriorityId,
             OperatorId,
             StatusId,
             WarehouseId,
             ReferenceNumber,
             ContainerTypeId,
             TareWeight,
             Weight,
             NettWeight,
             CheckedBy,
             CheckedDate)
      select PriorityId,
             @operatorId,
             dbo.ufn_StatusId('IS','CD'),
             WarehouseId,
             'NB:' + CONVERT(nvarchar(10), @NewBox),
             ContainerTypeId,
             TareWeight,
             @Weight,
             NettWeight,
             CheckedBy,
             @GetDate
        from Job
       where JobId = @JobId
      
      select @Error = @@Error, @NewJobId = scope_identity()
      
      if @Error <> 0
        goto error
    end
    
    if exists(select top 1 1 
                from Instruction
               where JobId = @newJobId
                 and InstructionRefId = @instructionId)
    begin
      select @Quantity = CheckQuantity / @TotalJobs
        from Instruction
       where InstructionId = @instructionId
      
      set rowcount 1

      update Instruction
         set Quantity = Quantity + @Quantity,
             ConfirmedQuantity = ConfirmedQuantity + @Quantity,
             CheckQuantity = CheckQuantity + @Quantity
       where JobId = @newJobId
         and InstructionRefId = @instructionId
      set rowcount 0
      
      --set rowcount 1
      --update Instruction
      --   set Quantity = Quantity - @Quantity,
      --       ConfirmedQuantity = ConfirmedQuantity - @Quantity
      -- where JobId = @newJobId
      --   and InstructionRefId = @instructionId
      --set rowcount 0
    end
    else
    begin
      insert Instruction
            (InstructionTypeId,
             StorageUnitBatchId,
             WarehouseId,
             StatusId,
             JobId,
             OperatorId,
             PickLocationId,
             StoreLocationId,
             ReceiptLineId,
             IssueLineId,
             InstructionRefId,
             Quantity,
             ConfirmedQuantity,
             Weight,
             ConfirmedWeight,
             PalletId,
             CreateDate,
             StartDate,
             EndDate,
             Picked,
             Stored,
             CheckQuantity,
             CheckWeight,
             OutboundShipmentId,
             DropSequence)
      select InstructionTypeId,
             StorageUnitBatchId,
             WarehouseId,
             StatusId,
             @NewJobId,
             OperatorId,
             PickLocationId,
             StoreLocationId,
             ReceiptLineId,
             IssueLineId,
             isnull(InstructionRefId, InstructionId),
             CheckQuantity / @TotalJobs,
             CheckQuantity / @TotalJobs,
             null,
             null,
             PalletId,
             CreateDate,
             StartDate,
             EndDate,
             Picked,
             Stored,
             CheckQuantity / @TotalJobs,
             0,
             OutboundShipmentId,
             DropSequence
        from Instruction
       where JobId = @JobId
         --and CheckQuantity > 0 -- Only those which were checked - unchecked will be handled below
         and (InstructionId = @instructionId or @instructionId is null)
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      set @NewJobId = null
    end
  end
  
  --if @updOriginal = 1
  --begin
    -- now update the original job and take off extra qty - leave only what was checked
    if @updOriginal = 1
      update Instruction
         set Quantity          = (Quantity - CheckQuantity) + (CheckQuantity / @TotalJobs),
             ConfirmedQuantity = (ConfirmedQuantity - CheckQuantity) + (CheckQuantity / @TotalJobs),
             CheckQuantity     = (CheckQuantity / @TotalJobs) -- This is one of the Jobs
       where JobId = @jobId
         and CheckQuantity > 0 -- Only those which were checked - unchecked will be handled below
         and (InstructionId = @instructionId or @instructionId is null)
    else
      update Instruction
         set Quantity          = (Quantity) - (CheckQuantity),
             ConfirmedQuantity = (ConfirmedQuantity) - (CheckQuantity),
             CheckQuantity     = CheckQuantity - (CheckQuantity) -- New Job was used, remove from here altogether
       where JobId = @jobId
         and CheckQuantity > 0 -- Only those which were checked - unchecked will be handled below
         and (InstructionId = @instructionId or @instructionId is null)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  --end
  --else
  --begin
  --  -- now update the original job and take off extra qty - leave only what was checked
  --  update Instruction
  --     set Quantity          = 0,
  --         ConfirmedQuantity = 0,
  --         CheckQuantity     = 0
  --   where JobId = @jobId
  --     and CheckQuantity > 0 -- Only those which were checked - unchecked will be handled below
  --     and (InstructionId = @instructionId or @instructionId is null)
    
  --  select @Error = @@Error
    
  --  if @Error <> 0
  --    goto error
  --end
  
  delete CheckingProduct
   where JobId = @jobId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  exec p_Job_Renumber_Boxing @JobId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
