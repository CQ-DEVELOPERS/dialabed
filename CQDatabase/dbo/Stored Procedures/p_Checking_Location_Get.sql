﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Checking_Location_Get
  ///   Filename       : p_Checking_Location_Get.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Jan 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Checking_Location_Get
(
 @WarehouseId              int,
 @OutboundDocumentTypeCode nvarchar(10),
 @PriorityId               int = null,
 @PrincipalId              int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @LocationId int,
	         @AreaType   nvarchar(10) = '',
	         @AreaId     int
  
  if @PriorityId is not null
    select @AreaId = p.CheckingAreaId
      from Priority (nolock) p
      join Area     (nolock) a on a.AreaId = p.CheckingAreaId
     where PriorityId = @PriorityId
  
  if @AreaId is null
    select @AreaId   = CheckingAreaId,
           @AreaType = isnull(AreaType,'')
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = @OutboundDocumentTypeCode
  
  if @AreaId is null
    select @AreaId   = CheckingAreaId,
           @AreaType = isnull(AreaType,'')
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = @OutboundDocumentTypeCode
  
  -- Old way
  if @OutboundDocumentTypeCode in ('COL','CNC')
  begin
    select @LocationId = convert(int, Value)
      from Configuration
     where ConfigurationId = 72
       and WarehouseId     = @WarehouseId
  end
  else if @OutboundDocumentTypeCode in ('KIT') 
  begin
    select @LocationId = convert(int, Value)
      from Configuration
     where ConfigurationId = 255
       and WarehouseId     = @WarehouseId
  end
  else
  begin
    select @LocationId = convert(int, Value)
      from Configuration
     where ConfigurationId = 43
       and WarehouseId     = @WarehouseId
  end
  
  if @LocationId is null
    select @LocationId = CheckingLane
      from OutboundDocumentType
     where OutboundDocumentTypeCode = @OutboundDocumentTypeCode
  
  if @LocationId is null
    -- New way
    select top 1
           @LocationId = al.LocationId
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId = al.AreaId
      join Location      l (nolock) on al.LocationId = l.LocationId
      left
      join AreaPrincipal ap (nolock) on a.AreaId = ap.AreaId
                                    and isnull(ap.PrincipalId, -1) = isnull(@PrincipalId, isnull(ap.PrincipalId, -1))
     where a.AreaId      = @AreaId
       and a.AreaType    = @AreaType
       and a.WarehouseId = @WarehouseId
    order by l.Used, l.RelativeValue
  
  if @LocationId is null
    set @LocationId = -1
  
  return @LocationId
end
 
