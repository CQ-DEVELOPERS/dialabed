﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Search
  ///   Filename       : p_Batch_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Sep 2014 15:23:47
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Batch table.
  /// </remarks>
  /// <param>
  ///   @BatchId int = null output,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @Batch nvarchar(100) = null,
  ///   @StatusModifiedBy int = null,
  ///   @CreatedBy int = null,
  ///   @ModifiedBy int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Batch.BatchId,
  ///   Batch.StatusId,
  ///   Status.Status,
  ///   Batch.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Batch.Batch,
  ///   Batch.CreateDate,
  ///   Batch.ExpiryDate,
  ///   Batch.IncubationDays,
  ///   Batch.ShelfLifeDays,
  ///   Batch.ExpectedYield,
  ///   Batch.ActualYield,
  ///   Batch.ECLNumber,
  ///   Batch.BatchReferenceNumber,
  ///   Batch.ProductionDateTime,
  ///   Batch.FilledYield,
  ///   Batch.COACertificate,
  ///   Batch.StatusModifiedBy,
  ///   Operator.Operator,
  ///   Batch.StatusModifiedDate,
  ///   Batch.CreatedBy,
  ///   Operator.Operator,
  ///   Batch.ModifiedBy,
  ///   Operator.Operator,
  ///   Batch.ModifiedDate,
  ///   Batch.Prints,
  ///   Batch.ParentProductCode,
  ///   Batch.RI,
  ///   Batch.Density,
  ///   Batch.BOELineNumber,
  ///   Batch.BillOfEntry,
  ///   Batch.ReferenceNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Search
(
 @BatchId int = null output,
 @StatusId int = null,
 @WarehouseId int = null,
 @Batch nvarchar(100) = null,
 @StatusModifiedBy int = null,
 @CreatedBy int = null,
 @ModifiedBy int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Batch = '-1'
    set @Batch = null;
  
  if @StatusModifiedBy = '-1'
    set @StatusModifiedBy = null;
  
  if @CreatedBy = '-1'
    set @CreatedBy = null;
  
  if @ModifiedBy = '-1'
    set @ModifiedBy = null;
  
 
  select
         Batch.BatchId
        ,Batch.StatusId
         ,StatusStatusId.Status as 'Status'
        ,Batch.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Batch.Batch
        ,Batch.CreateDate
        ,Batch.ExpiryDate
        ,Batch.IncubationDays
        ,Batch.ShelfLifeDays
        ,Batch.ExpectedYield
        ,Batch.ActualYield
        ,Batch.ECLNumber
        ,Batch.BatchReferenceNumber
        ,Batch.ProductionDateTime
        ,Batch.FilledYield
        ,Batch.COACertificate
        ,Batch.StatusModifiedBy
         ,OperatorStatusModifiedBy.Operator as 'OperatorStatusModifiedBy'
        ,Batch.StatusModifiedDate
        ,Batch.CreatedBy
         ,OperatorCreatedBy.Operator as 'OperatorCreatedBy'
        ,Batch.ModifiedBy
         ,OperatorModifiedBy.Operator as 'OperatorModifiedBy'
        ,Batch.ModifiedDate
        ,Batch.Prints
        ,Batch.ParentProductCode
        ,Batch.RI
        ,Batch.Density
        ,Batch.BOELineNumber
        ,Batch.BillOfEntry
        ,Batch.ReferenceNumber
    from Batch
    left
    join Status StatusStatusId on StatusStatusId.StatusId = Batch.StatusId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Batch.WarehouseId
    left
    join Operator OperatorStatusModifiedBy on OperatorStatusModifiedBy.OperatorId = Batch.StatusModifiedBy
    left
    join Operator OperatorCreatedBy on OperatorCreatedBy.OperatorId = Batch.CreatedBy
    left
    join Operator OperatorModifiedBy on OperatorModifiedBy.OperatorId = Batch.ModifiedBy
   where isnull(Batch.BatchId,'0')  = isnull(@BatchId, isnull(Batch.BatchId,'0'))
     and isnull(Batch.StatusId,'0')  = isnull(@StatusId, isnull(Batch.StatusId,'0'))
     and isnull(Batch.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Batch.WarehouseId,'0'))
     and isnull(Batch.Batch,'%')  like '%' + isnull(@Batch, isnull(Batch.Batch,'%')) + '%'
     and isnull(Batch.StatusModifiedBy,'0')  = isnull(@StatusModifiedBy, isnull(Batch.StatusModifiedBy,'0'))
     and isnull(Batch.CreatedBy,'0')  = isnull(@CreatedBy, isnull(Batch.CreatedBy,'0'))
     and isnull(Batch.ModifiedBy,'0')  = isnull(@ModifiedBy, isnull(Batch.ModifiedBy,'0'))
  order by Batch
  
end
