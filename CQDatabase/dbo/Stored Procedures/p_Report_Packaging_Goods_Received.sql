﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Packaging_Goods_Received
  ///   Filename       : p_Report_Packaging_Goods_Received.sql
  ///   Create By      : Karen
  ///   Date Created   : 22 Oct 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Packaging_Goods_Received
(
 @InboundShipmentId int = null,
 @ReceiptId         int = null
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as Table
  (
   InboundShipmentId  int,
   ReceiptId          int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   Supplier           nvarchar(255),
   SupplierCode       nvarchar(30),
   DeliveryNoteNumber nvarchar(30),
   DeliveryDate       datetime,   
   SealNumber		  nvarchar(30),
   VehicleRegistration nvarchar(10)  
  );
  
  declare @TableDetails as Table
  (
   ReceiptId          int,
   ReceiptLineId      int,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   SKUCode            nvarchar(50),
   RequiredQuantity   numeric(13,6),
   ReceivedQuantity   numeric(13,6),
   AcceptedQuantity   numeric(13,6),
   RejectQuantity     numeric(13,6),
   DeliveryNoteQuantity numeric(13,6),
   ReasonId           int,
   Reason             nvarchar(50),
   Exception          nvarchar(255),
   Batch              nvarchar(50),
   ExpiryDate         datetime,
   SampleQty		  numeric(13,6),
   OperatorCode		  nvarchar(10)
  );
  
         
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  if @InboundShipmentId is not null
    insert @TableHeader
          (InboundShipmentId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration)
    select isr.InboundShipmentId,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.DeliveryNoteNumber,
           r.SealNumber,
           r.DeliveryDate,
           r.VehicleRegistration
      from InboundShipmentReceipt isr
      join Receipt                  r on isr.ReceiptId = r.ReceiptId
      join InboundDocument         id on r.InboundDocumentId = id.InboundDocumentId
     where isr.InboundShipmentId = isnull(@InboundShipmentId, isr.InboundShipmentId)
  else if @ReceiptId is not null
    insert @TableHeader
          (ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration)
    select r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.DeliveryNoteNumber,
           r.DeliveryDate,
           r.SealNumber,
           VehicleRegistration
      from Receipt                  r
      join InboundDocument         id on r.InboundDocumentId = id.InboundDocumentId
      where r.ReceiptId = isnull(@ReceiptId, r.ReceiptId)
  
  insert @TableDetails
        (ReceiptId,
         ReceiptLineId,
         StorageUnitBatchId,
         OperatorCode)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         rl.StorageUnitBatchId,
         o.OperatorCode
    from @TableHeader th
    join ReceiptLine  rl on th.ReceiptId     = rl.ReceiptId
    join InboundLine  il on rl.InboundLineId = il.InboundLineId
    left join Operator     o  on rl.OperatorId    = o.OperatorId
  
  update th
     set Supplier     = ec.ExternalCompany,
         SupplierCode = ec.ExternalCompanyCode
    from @TableHeader th
    join ExternalCompany ec on th.ExternalCompanyId = ec.ExternalCompanyId
  
  update t
     set ReasonId = r.ReasonId,
         Reason     = r.Reason,
         Exception  = e.Exception
    from @TableDetails t
    join Exception     e (nolock) on t.ReceiptLineId = e.ReceiptLineId
    join Reason        r (nolock) on e.ReasonId      = r.ReasonId
  
  update td
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch		     = b.Batch,
         ExpiryDate  = b.ExpiryDate,
         RequiredQuantity = rlp.RequiredQuantity,
         ReceivedQuantity = rlp.ReceivedQuantity,
         AcceptedQuantity = rlp.AcceptedQuantity,
         RejectQuantity = rlp.RejectQuantity,
         DeliveryNoteQuantity = rlp.DeliveryNoteQuantity
    from @TableDetails    td
    join StorageUnitBatch sub on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId     = su.StorageUnitId
    join ReceiptLinePackaging rlp on rlp.StorageUnitBatchId = sub.StorageUnitBatchId
    join Product          p   on su.ProductId          = p.ProductId
    join SKU              sku on su.SKUId              = sku.SKUId
    join Batch            b   on sub.BatchId		   = b.BatchId
  
  select th.InboundShipmentId,
         th.OrderNumber,
         DeliveryNoteNumber,
         DeliveryDate,
         th.Supplier,
         th.SupplierCode,
         td.ProductCode,
         td.Product,
         td.RequiredQuantity,
         td.ReceivedQuantity,
         td.AcceptedQuantity,
         td.RejectQuantity,
         td.DeliveryNoteQuantity,
         Exception,
         SealNumber,
         VehicleRegistration,
         td.Batch,
         td.ExpiryDate,
         td.SampleQty,
         td.OperatorCode
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
end
