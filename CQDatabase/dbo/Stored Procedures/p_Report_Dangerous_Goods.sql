﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Dangerous_Goods
  ///   Filename       : p_Report_Dangerous_Goods.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Dec 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Dangerous_Goods
(
 @OutboundShipmentId int = null,
 @IssueId            int = null
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId			   int,
   WarehouseId					   int,
   StorageUnitId                   int,
   SKUId                           int,
   SKU                             nvarchar(50),
   IssueId                         int,
   ExternalCompanyId               int,
   ExternalCompany                 nvarchar(255),
   ExternalCompanyAddress          nvarchar(255),
   ExternalCompanyCode             nvarchar(30),
   DangerousGoodsCode              nvarchar(20),
   DangerousGoods                  nvarchar(100),
   Class                           float,
   UNNumber                        nvarchar(60),
   PackClass                       nvarchar(60),
   DangerFactor                    int,
   WeightPicked                    float,
   ConfirmedQuantity               float                          
  )
  
  if @OutboundShipmentId = -1
	set @OutboundShipmentId = null
 
  
  if @OutboundShipmentId is not null
    insert @TableResult
          (OutboundShipmentId,
           WarehouseId,
           StorageUnitId,
           SKUId,
           IssueId,
           DangerousGoodsCode,
           DangerousGoods,
           Class,
           UNNumber,
           PackClass,
           DangerFactor,
           ConfirmedQuantity)
    select osi.OutboundShipmentId,
		   i.WarehouseId,
           su.StorageUnitId,
           su.SKUId,
           il.IssueId,
           dg.DangerousGoodsCode,
           dg.DangerousGoods,
           dg.Class,
           dg.UNNumber,
           dg.PackClass,
           dg.DangerFactor,
           il.ConfirmedQuatity
      from OutboundShipmentIssue osi (nolock)
      join IssueLine              il (nolock) on osi.IssueId             = il.IssueId
      join Issue				   i (nolock) on osi.IssueId = i.IssueId
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                 p (nolock) on su.ProductId          = p.ProductId
      join DangerousGoods         dg (nolock) on isnull(p.DangerousGoodsId,dg.DangerousGoodsId)    = dg.DangerousGoodsId
     where osi.OutboundShipmentId = isnull(@OutboundShipmentId, osi.OutboundShipmentId)
       and il.ConfirmedQuatity > 0
       and p.DangerousGoodsId > 0
  else
    insert @TableResult
          (OutboundShipmentId,
           WarehouseId,
           StorageUnitId,
           su.SKUId,
           IssueId,
           DangerousGoodsCode,
           DangerousGoods,
           Class,
           UNNumber,
           PackClass,
           DangerFactor,
           ConfirmedQuantity)
    select null,
		   i.WarehouseId,
		   su.StorageUnitId,
           su.SKUId,
           il.IssueId,
           dg.DangerousGoodsCode,
           dg.DangerousGoods,
           dg.Class,
           dg.UNNumber,
           dg.PackClass,
           dg.DangerFactor,
           il.ConfirmedQuatity
      from IssueLine              il (nolock)
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Issue				   i (nolock) on il.IssueId = i.IssueId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                 p (nolock) on su.ProductId          = p.ProductId
      join DangerousGoods         dg (nolock) on isnull(p.DangerousGoodsId,dg.DangerousGoodsId)    = dg.DangerousGoodsId
   where il.IssueId = isnull(@IssueId, il.IssueId)
     and il.ConfirmedQuatity > 0
     and p.DangerousGoodsId > 0
  
  update tr
     set WeightPicked = tr.ConfirmedQuantity * p.Weight
    from @TableResult      tr
    join Pack               p (nolock) on tr.StorageUnitId = p.StorageUnitId
   where p.Quantity    = 1
     --and p.WarehouseId = @WarehouseId
  
  update tr
     set SKU = sku.SKU
    from @TableResult tr
    join SKU         sku (nolock) on tr.SKUId = sku.SKUId
  
  update tr
     set ExternalCompany     = ec.ExternalCompany,
         ExternalCompanyCode = ec.ExternalCompanyCode
    from @TableResult      tr
    join Issue              i (nolock) on tr.IssueId           = i.IssueId
    join OutboundDocument  od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
    join ExternalCompany   ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set ExternalCompanyAddress = isnull(a.Street,'') + ' '  + isnull(a.Suburb,'') + ' ' + isnull(a.Town,'')
    from @TableResult      tr
    join Address            a (nolock) on tr.ExternalCompanyId = a.ExternalCompanyId
  
  select 
		 ec.ExternalCompany as ConsignorName,
		 a.Street as ConsignorAddress1,
		 a.Suburb as ConsignorAddress2,
		 a.Town as ConsignorAddress3, 
		 tr.ExternalCompany,
		 ExternalCompanyAddress,
         DangerousGoodsCode,
         DangerousGoods,
         Class,
         UNNumber,
         PackClass,
         SKU,
         sum(ConfirmedQuantity) as 'ConfirmedQuantity',
         sum(WeightPicked) as 'TotalWeight',
         sum(WeightPicked * DangerFactor) as 'Weight*Factor'
    from @TableResult tr
    join Warehouse    w (nolock) on w.WarehouseId = tr.WarehouseId
    join Address      a (nolock) on a.AddressId = w.AddressId
    join ExternalCompany ec (nolock) on a.ExternalCompanyId = ec.ExternalCompanyId
   group by ec.ExternalCompany,
			a.Street,
			a.Suburb,
			a.Town,
			tr.ExternalCompany,
		    ExternalCompanyAddress,
            DangerousGoodsCode,
            DangerousGoods,
            Class,
            UNNumber,
            PackClass,
            SKU
end
