﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitLocationHistory_Insert
  ///   Filename       : p_StorageUnitLocationHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:07
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitLocationHistory table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @MinimumQuantity float = null,
  ///   @HandlingQuantity float = null,
  ///   @MaximumQuantity float = null 
  /// </param>
  /// <returns>
  ///   StorageUnitLocationHistory.StorageUnitId,
  ///   StorageUnitLocationHistory.LocationId,
  ///   StorageUnitLocationHistory.CommandType,
  ///   StorageUnitLocationHistory.InsertDate,
  ///   StorageUnitLocationHistory.MinimumQuantity,
  ///   StorageUnitLocationHistory.HandlingQuantity,
  ///   StorageUnitLocationHistory.MaximumQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitLocationHistory_Insert
(
 @StorageUnitId int = null,
 @LocationId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @MinimumQuantity float = null,
 @HandlingQuantity float = null,
 @MaximumQuantity float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StorageUnitLocationHistory
        (StorageUnitId,
         LocationId,
         CommandType,
         InsertDate,
         MinimumQuantity,
         HandlingQuantity,
         MaximumQuantity)
  select @StorageUnitId,
         @LocationId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @MinimumQuantity,
         @HandlingQuantity,
         @MaximumQuantity 
  
  select @Error = @@Error
  
  
  return @Error
  
end
