﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_Delete
  ///   Filename       : p_OutboundLine_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jan 2014 07:56:01
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OutboundLine table.
  /// </remarks>
  /// <param>
  ///   @OutboundLineId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_Delete
(
 @OutboundLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete OutboundLine
     where OutboundLineId = @OutboundLineId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OutboundLineHistory_Insert
         @OutboundLineId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
