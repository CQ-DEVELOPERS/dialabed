﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Priority_Delete
  ///   Filename       : p_Priority_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:14
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Priority table.
  /// </remarks>
  /// <param>
  ///   @PriorityId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Priority_Delete
(
 @PriorityId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Priority
     where PriorityId = @PriorityId
  
  select @Error = @@Error
  
  
  return @Error
  
end
