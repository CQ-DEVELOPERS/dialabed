﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItem_Search
  ///   Filename       : p_OperatorGroupMenuItem_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:25
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorGroupMenuItem table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null output,
  ///   @MenuItemId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorGroupMenuItem.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup,
  ///   OperatorGroupMenuItem.MenuItemId,
  ///   MenuItem.MenuItem,
  ///   OperatorGroupMenuItem.Access 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItem_Search
(
 @OperatorGroupId int = null output,
 @MenuItemId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
 
  select
         OperatorGroupMenuItem.OperatorGroupId
         ,OperatorGroupOperatorGroupId.OperatorGroup as 'OperatorGroup'
        ,OperatorGroupMenuItem.MenuItemId
         ,MenuItemMenuItemId.MenuItem as 'MenuItem'
        ,OperatorGroupMenuItem.Access
    from OperatorGroupMenuItem
    left
    join OperatorGroup OperatorGroupOperatorGroupId on OperatorGroupOperatorGroupId.OperatorGroupId = OperatorGroupMenuItem.OperatorGroupId
    left
    join MenuItem MenuItemMenuItemId on MenuItemMenuItemId.MenuItemId = OperatorGroupMenuItem.MenuItemId
   where isnull(OperatorGroupMenuItem.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(OperatorGroupMenuItem.OperatorGroupId,'0'))
     and isnull(OperatorGroupMenuItem.MenuItemId,'0')  = isnull(@MenuItemId, isnull(OperatorGroupMenuItem.MenuItemId,'0'))
  
end
