﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Release
  ///   Filename       : p_Wave_Planning_Release.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Release
(
 @WaveId     int,
 @OperatorId int = null,
 @Replenish  bit = 0,
 @Debug      bit = 0,
 @RepStorageUnitId int = null,
 @Result           int = null output
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   IssueLineId                     int,
   IssueId                         int,
   OrderNumber                     nvarchar(30),
   StorageUnitBatchId              int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   BatchId                         int,
   Batch                           nvarchar(50),
   SKUCode                         nvarchar(50),
   RequiredQuantity                float default 0,
   ConfirmedQuatity                float default 0,
   AllocatedQuantity               float default 0,
   ManufacturingQuantity           float default 0,
   NarrowAisle                     float default 0,
   ReplenishmentQuantity           float default 0,
   AvailableQuantity               float default 0,
   AvailablePercentage             numeric(13,2),
   AreaType                        nvarchar(10),
   WarehouseId                     int,
   DropSequence                    int
  )

  declare @Error              int,
          @Errormsg           nvarchar(500) = 'Error executing - p_Wave_Planning_Release',
          @GetDate            datetime,
          @EmptyLocations     int,
          @StorageUnitId      int,
          @StorageUnitBatchId int,
          @StoreLocationId    int,
          @Quantity           numeric(13,6),
          @InstructionId      int,
          @OriginalQuantity   numeric(13,6),
          @RepQuantity        numeric(13,6),
          @WarehouseId        int,
          @JobId              int,
          @PickLocationId     int,
          @StatusId           int,
          @InstructionTypeId  int,
          @PalletId           int,
          @Transaction        bit = 0,
          @PriorityId         int

  select @GetDate = dbo.ufn_Getdate()
  
  select @WarehouseId = WarehouseId
    from Wave (nolock)
   where WaveId = @WaveId
  
  insert @TableResult
        (StorageUnitId,
         RequiredQuantity,
         ConfirmedQuatity,
         AreaType,
         WarehouseId)
  select sub.StorageUnitId,
         sum(il.Quantity),
         sum(il.ConfirmedQuatity),
         isnull(i.AreaType,''),
         i.WarehouseId
    from IssueLine         il
    join Issue              i (nolock) on il.IssueId            = i.IssueId
    join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
    join OutboundDocument  od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
    join Status             s (nolock) on il.StatusId           = s.StatusId
                                      and s.StatusCode not in ('CK','CD','DC','D','C')
   where i.WaveId = @WaveId
     and odt.OutboundDocumentTypeCode != 'WAV'
     and il.Quantity > 0
  group by sub.StorageUnitId,
           isnull(i.AreaType,''),
           i.WarehouseId
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  update @TableResult
     set RequiredQuantity = RequiredQuantity - ConfirmedQuatity
  
  update @TableResult
     set RequiredQuantity = 0
   where RequiredQuantity < 0
  
  delete @TableResult where RequiredQuantity = 0
  
  startagain:
  
  if @Replenish = 1
  begin
    update tr
       set AllocatedQuantity = a.ReservedQuantity - tr.RequiredQuantity
      from @TableResult tr
      join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                          and a.AreaType       = ''
                          and tr.StorageUnitId = a.StorageUnitId
    
    update @TableResult
       set AllocatedQuantity = 0
     where AllocatedQuantity < 0
  end
  else
  begin
    update tr
       set AllocatedQuantity = a.ReservedQuantity
      from @TableResult tr
      join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                          and a.AreaType       = ''
                          and tr.StorageUnitId = a.StorageUnitId
  end
  
  --update tr
  --   set ManufacturingQuantity = case when a.ActualQuantity - a.ReservedQuantity < 0
  --                                    then 0
  --                                    else a.ActualQuantity - a.ReservedQuantity
  --                                    end
  --  from @TableResult tr
  --  join viewAvailablePickfaces a on tr.WarehouseId   = a.WarehouseId -- CHanged from viewAvailable to viewAvailablePickfaces for Moresport
  --                               and a.AreaType       = ''
  --                               and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ManufacturingQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailablePickfaces a on tr.WarehouseId   = a.WarehouseId -- CHanged from viewAvailable to viewAvailablePickfaces for Moresport
                                 and a.AreaType       = ''
                                 and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set NarrowAisle = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set AvailableQuantity = ManufacturingQuantity + NarrowAisle
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), (ManufacturingQuantity - AllocatedQuantity) + isnull(ReplenishmentQuantity,0)) / convert(numeric(13,3), RequiredQuantity)) * 100
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  -- Replenishment Quantity only partially avaiable, still keep alive
  if exists(select top 1 1 
             from @TableResult
             where StorageUnitId = @RepStorageUnitId
               and AvailablePercentage > 0)
    set @Result = 0
  
  if @Debug = 1
  begin
    update tr
       set ProductCode = p.ProductCode,
           Product     = p.Product,
           SKUCode     = sku.SKUCode
      from @TableResult tr
      join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
      join Product          p   (nolock) on su.ProductId          = p.ProductId
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    
    update tr
       set Batch       = b.Batch
      from @TableResult tr
      join Batch            b   (nolock) on tr.BatchId            = b.BatchId
  end
  
  select @EmptyLocations = count(1) from @TableResult where AvailablePercentage < 100
  
  if @Debug = 1
    select @EmptyLocations as '@EmptyLocations', * from @TableResult
  
  while @EmptyLocations > 0
  begin
    set @StorageUnitId = null

    select top 1 @StorageUnitId    = StorageUnitId,
                 @Quantity         = RequiredQuantity,
                 @OriginalQuantity = RequiredQuantity
      from @TableResult
     where AvailablePercentage < 100
    order by AvailablePercentage
    
    if @StorageUnitId is null
    begin
      set @EmptyLocations = 0
    end
    else
    begin
      select @StorageUnitBatchId = StorageUnitBatchId
        from StorageUnitBatch sub (nolock)
        join Batch              b (nolock) on sub.BatchId = b.BatchId
       where sub.StorageUnitId = @StorageUnitId
         and b.Batch = 'Default'
      
      if @StorageUnitBatchId is null
        select @StorageUnitBatchId = StorageUnitBatchId
          from StorageUnitBatch sub (nolock)
          join Batch              b (nolock) on sub.BatchId = b.BatchId
         where sub.StorageUnitId = @StorageUnitId
      
      set @InstructionId = null
      
      set @StoreLocationId = null
      
      select top 1 @StoreLocationId = l.LocationId
        from Area a
        join AreaLocation al on a.AreaId = al.AreaId
        join Location l on al.LocationId = l.LocationId
       where a.AreaCode    = 'SP'
         and a.Replenish   = 1
         --and l.Used        = 0 -- Rather added to Order by Clause
         and a.WarehouseId = @WarehouseId
         and a.AreaType    = ''
         and a.Area = 'WavePick'
         and l.ActiveBinning = 1
      order by l.Used, l.StocktakeInd, l.LocationId desc
      
      exec @Error = p_Mobile_Replenishment_Request
       @storeLocationId    = @StoreLocationId,
       @storageUnitBatchId = @StorageUnitBatchId,
       @operatorId         = null,
       @ShowMsg            = 0,
       @ManualReq          = 0,
       @AnyLocation        = 1,
       @AreaType           = 'backup',
       @RequestedQuantity  = @Quantity output,
       @InstructionId      = @InstructionId output
      
      if @Error = 9 and @InstructionId is not null -- Mean the Requested quantity changed
      begin
        select @RepQuantity = Quantity
          from Instruction (nolock)
         where InstructionId = @InstructionId
        
        set @Error = 0
        
        exec p_StorageUnitBatchLocation_Deallocate
         @InstructionId = @InstructionId
        
        if @Error <> 0
          goto Error
        
        select @RepQuantity = @RepQuantity + @OriginalQuantity
        
        exec @Error = p_Instruction_Update
         @InstructionId     = @InstructionId,
         @Quantity          = @RepQuantity,
         @ConfirmedQuantity = @RepQuantity
        
        if @Error <> 0
          goto Error
        
        exec @Error = p_StorageUnitBatchLocation_Reserve
         @InstructionId       = @instructionId
        
        if @Error <> 0
          goto Error
      end
      
      if @Debug = 1
        select @Quantity as '@Quantity', @OriginalQuantity as '@OriginalQuantity', @InstructionId as '@InstructionId', @Error as '@Error'
      
      --if @Debug = 1
      --begin
      --  rollback
      --  return
      --end
      
      if @Quantity >= @OriginalQuantity or @InstructionId is null or @Error <> 0
      begin
        select @EmptyLocations = @EmptyLocations - 1
        delete @TableResult where StorageUnitId = @StorageUnitId
      end
      --else
      --begin
      --  update @TableResult set RequiredQuantity = RequiredQuantity - @Quantity where StorageUnitId = @StorageUnitId
      --end
      
      if @StorageUnitId = @RepStorageUnitId and @Error = 0
         set @Result = 0
      else if @StorageUnitId = @RepStorageUnitId and @Error != 0 and @Result is null
      begin
        --if exists(select 1 
        --            from Instruction i
        --            join Status          si (nolock) on i.StatusId           = si.StatusId
        --            join InstructionType it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
        --                                            and it.InstructionTypeCode in ('P','M','R')
        --            join Job              j (nolock) on i.JobId              = j.JobId
        --            join Status           s (nolock) on j.StatusId           = s.StatusId
        --            join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
        --                                              and sub.StorageUnitId = @RepStorageUnitId
        --            left
        --            join AreaLocation  pickAL (nolock) on i.PickLocationId = pickAL.LocationId
        --            left
        --            join Area           picka (nolock) on pickAL.AreaId
        --            left
        --            join viewLocation store (nolock) on i.StoreLocationId = store.LocationId
        --           where 
        --             and PickAreaType = 'Backup')
        -- set @Result = 0
        -- else
         set @Result = -1
      end
      if @Debug = 1
        if @Error <> 0
          select @Error as '@Error', @StorageUnitId as '@StorageUnitId', @StoreLocationId as '@StoreLocationId', @InstructionId as '@InstructionId'
      
      if @InstructionId is not null
      begin
        select @InstructionTypeId = InstructionTypeId
          from InstructionType (nolock)
         where InstructionTypeCode = 'P'
        
        select @JobId             = JobId,
               @WarehouseId       = WarehouseId,
               @PickLocationId    = PickLocationId
          from Instruction (nolock)
         where InstructionId = @InstructionId
        
        if @Replenish = 0
        begin
          select @PriorityId        = PriorityId
            from InstructionType
           where InstructionTypeCode = 'M'
          
          exec @Error = p_Job_Update
           @JobId      = @JobId,
           @PriorityId = @PriorityId
          
          if @Error <> 0
            goto Error
        end
        
        set @PalletId = null
        
        select @PalletId = PalletId
          from Pallet (nolock)
         where LocationId = @PickLocationId
           and StorageUnitBatchId = @StorageUnitBatchId
        
        if @PalletId is null
        begin
          select @PalletId = max(PalletId)
            from Pallet (nolock)
           where LocationId = @PickLocationId
        end
        
        --if @PalletId is null
        --begin
        --  exec @Error = p_Pallet_Insert
        --   @PalletId = @PalletId output,
        --   @Prints   = 0
          
        --  if @Error <> 0
        --    goto Error
        --end
        
        exec @Error = p_Instruction_Update
         @InstructionId     = @InstructionId,
         @InstructionTypeId = @InstructionTypeId,
         @PalletId          = @PalletId,
         @AutoComplete      = 1
        
        if @Error <> 0
          goto Error
        
        exec @Error = p_Wave_Planning_Move_Items
         @WarehouseId        = @WarehouseId,
         @JobId              = @JobId,
         @PickLocationId     = @PickLocationId,
         @StoreLocationId    = @StoreLocationId,
         @InstructionTypeId  = @InstructionTypeId,
         @PalletId           = @PalletId
        
        if @Error <> 0
          goto Error
        
        declare @TableInstructions as table
        (
         InstructionId int,
         Quantity      numeric(13,6)
        )
        
        insert @TableInstructions
              (InstructionId,
               Quantity)
        select InstructionId,
               Quantity
          from Instruction
         where JobId = @JobId
        
        while exists(select top 1 1 from @TableInstructions)
        begin
          select top 1 @InstructionId = InstructionId,
                       @Quantity      = Quantity
            from @TableInstructions
          
          delete @TableInstructions
           where InstructionId = @InstructionId
          
          set @Error = 0
          
          exec @Error = p_Replenishment_Create_Pick
           @InstructionId = @InstructionId,
           @WaveId        = @WaveId
          
          if @Error <> 0
            goto Error
        end
        
        --select @EmptyLocations as '@EmptyLocations', @OriginalQuantity as '@OriginalQuantity', * from @TableResult
        goto startagain
      end
      --else if @InstructionId is not null
      --begin
      --  exec @Error = p_Replenishment_Create_Pick
      --   @InstructionId = @InstructionId,
      --   @WaveId        = @WaveId
        
      --  if @Error <> 0
      --    goto Error
      --end
    end
  end
  
  if @Replenish = 0 -- Only release on first request, after that it's just replenishment requests for short picks
  begin
    if exists(select top 1 1
                from Issue i (nolock)
                join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
                join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                                      and odt.OutboundDocumentTypeCode = 'WAV'
               where i.WaveId = @WaveId)
    begin
      select @StatusId = dbo.ufn_StatusId('W','RL')
      
      exec @Error = p_Wave_Update
       @WaveId   = @WaveId,
       @StatusId = @StatusId,
       @ReleasedDate = @GetDate
      
      if @Error <> 0
        goto Error
      
      exec @Error = p_Wave_Planning_Release_Linked
       @WaveId   = @WaveId
      
      if @Error <> 0
        goto Error
    end
    else if exists(select top 1 1
                    from @TableResult
                   where AvailablePercentage = 100)
    begin
      select @StatusId = dbo.ufn_StatusId('W','RL')
      
      exec @Error = p_Wave_Update
       @WaveId   = @WaveId,
       @StatusId = @StatusId,
       @ReleasedDate = @GetDate
      
      if @Error <> 0
        goto Error
      
      exec @Error = p_Wave_Planning_Release_Linked
       @WaveId   = @WaveId
      
      if @Error <> 0
        goto Error
    end
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
