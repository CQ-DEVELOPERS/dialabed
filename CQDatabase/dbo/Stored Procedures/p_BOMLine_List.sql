﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMLine_List
  ///   Filename       : p_BOMLine_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:12
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMLine table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   BOMLine.BOMLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMLine_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as BOMLineId
        ,null as 'BOMLine'
  union
  select
         BOMLine.BOMLineId
        ,BOMLine.BOMLineId as 'BOMLine'
    from BOMLine
  
end
