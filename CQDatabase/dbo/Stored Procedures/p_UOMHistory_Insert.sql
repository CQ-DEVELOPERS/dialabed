﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UOMHistory_Insert
  ///   Filename       : p_UOMHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:09
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the UOMHistory table.
  /// </remarks>
  /// <param>
  ///   @UOMId int = null,
  ///   @UOM nvarchar(100) = null,
  ///   @UOMCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   UOMHistory.UOMId,
  ///   UOMHistory.UOM,
  ///   UOMHistory.UOMCode,
  ///   UOMHistory.CommandType,
  ///   UOMHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_UOMHistory_Insert
(
 @UOMId int = null,
 @UOM nvarchar(100) = null,
 @UOMCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert UOMHistory
        (UOMId,
         UOM,
         UOMCode,
         CommandType,
         InsertDate)
  select @UOMId,
         @UOM,
         @UOMCode,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
