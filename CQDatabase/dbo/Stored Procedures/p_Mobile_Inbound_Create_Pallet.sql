﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Inbound_Create_Pallet
    ///   Filename       : p_Mobile_Inbound_Create_Pallet.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 2 March 2009 20:04:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_Inbound_Create_Pallet
(
 @receiptId           int,
 @jobId               int,
 @storageUnitId       int,
 @storageUnitBatchId  int,
 @quantity            float,
 @instructionTypeCode nvarchar(10)
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @GetDate            datetime,
          @WarehouseId        int,
          @PriorityId         int,
          @StatusId           int,
          @ReceiptLineId      int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @JobId = -1
    set @JobId = null
  
  select @WarehouseId = WarehouseId
    from Receipt (nolock)
   where ReceiptId = @receiptId
  
  select @ReceiptLineId = rl.ReceiptLineId
    from ReceiptLine       rl (nolock)
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
   where rl.ReceiptId      = @receiptId
     and sub.StorageUnitId = @StorageUnitId
  
  begin transaction
  
  if @instructionTypeCode = 'S'
    select @PriorityId = PriorityId
      from InstructionType (nolock)
     where InstructionTypeCode = 'S'
  else if @instructionTypeCode = 'SM'
    select @PriorityId = PriorityId
      from InstructionType (nolock)
     where InstructionTypeCode = 'SM'
  
  select @StatusId = dbo.ufn_StatusId('R','W')
  
  if @JobId is null
  begin
    exec @Error = p_Job_Insert
     @JobId         = @JobId output,
     @PriorityId    = @PriorityId,
     @StatusId      = @StatusId,
     @WarehouseId   = @WarehouseId
    
    if @Error <> 0
    begin
      set @jobId = -1
      goto error
    end
  end
  
  exec @Error = p_Palletised_Insert
   @WarehouseId         = @WarehouseId,
   @OperatorId          = null,
   @InstructionTypeCode = @instructionTypeCode,
   @StorageUnitBatchId  = @storageUnitBatchId,
   @PickLocationId      = null,
   @StoreLocationId     = null,
   @Quantity            = @quantity,
   @PriorityId          = @PriorityId,
   @ReceiptLineId       = @receiptLineId,
   @StatusId            = @StatusId,
   @JobId               = @JobId
  
  if @Error <> 0
  begin
    set @jobId = -1
    goto error
  end
  
  commit transaction
  select @jobId
  return @JobId
  
  error:
    rollback transaction
    select @jobId
    return @JobId
end
