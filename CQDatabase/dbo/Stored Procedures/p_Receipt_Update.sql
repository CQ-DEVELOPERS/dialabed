﻿
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Update
  ///   Filename       : p_Receipt_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 13:28:36
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Receipt table.
  /// </remarks>
  /// <param>
  ///   @ReceiptId int = null,
  ///   @InboundDocumentId int = null,
  ///   @PriorityId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @CreditAdviceIndicator bit = null,
  ///   @Delivery int = null,
  ///   @AllowPalletise bit = null,
  ///   @Interfaced bit = null,
  ///   @StagingLocationId int = null,
  ///   @NumberOfLines int = null,
  ///   @ReceivingCompleteDate datetime = null,
  ///   @ParentReceiptId int = null,
  ///   @GRN nvarchar(60) = null,
  ///   @PlannedDeliveryDate datetime = null,
  ///   @ShippingAgentId int = null,
  ///   @ContainerNumber nvarchar(100) = null,
  ///   @AdditionalText1 nvarchar(510) = null,
  ///   @AdditionalText2 nvarchar(510) = null,
  ///   @BOE nvarchar(510) = null,
  ///   @Incoterms nvarchar(100) = null,
  ///   @ReceiptConfirmed datetime = null,
  ///   @ReceivingStarted datetime = null,
  ///   @VehicleId int = null,
  ///   @ParentIssueId int = null,
  ///   @VehicleTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : Kevin Wilson
  ///   Modified Date  : 
  ///   Details        : Force operators to click finish when receiving
  /// </newpara>
*/
CREATE PROCEDURE p_Receipt_Update (
	@ReceiptId INT = NULL
	,@InboundDocumentId INT = NULL
	,@PriorityId INT = NULL
	,@WarehouseId INT = NULL
	,@LocationId INT = NULL
	,@StatusId INT = NULL
	,@OperatorId INT = NULL
	,@DeliveryNoteNumber NVARCHAR(60) = NULL
	,@DeliveryDate DATETIME = NULL
	,@SealNumber NVARCHAR(60) = NULL
	,@VehicleRegistration NVARCHAR(20) = NULL
	,@Remarks NVARCHAR(500) = NULL
	,@CreditAdviceIndicator BIT = NULL
	,@Delivery INT = NULL
	,@AllowPalletise BIT = NULL
	,@Interfaced BIT = NULL
	,@StagingLocationId INT = NULL
	,@NumberOfLines INT = NULL
	,@ReceivingCompleteDate DATETIME = NULL
	,@ParentReceiptId INT = NULL
	,@GRN NVARCHAR(60) = NULL
	,@PlannedDeliveryDate DATETIME = NULL
	,@ShippingAgentId INT = NULL
	,@ContainerNumber NVARCHAR(100) = NULL
	,@AdditionalText1 NVARCHAR(510) = NULL
	,@AdditionalText2 NVARCHAR(510) = NULL
	,@BOE NVARCHAR(510) = NULL
	,@Incoterms NVARCHAR(100) = NULL
	,@ReceiptConfirmed DATETIME = NULL
	,@ReceivingStarted DATETIME = NULL
	,@VehicleId INT = NULL
	,@ParentIssueId INT = NULL
	,@VehicleTypeId INT = NULL
	,@LockReceipt BIT = 0
	)
AS
BEGIN
	SET NOCOUNT ON;

	IF @ReceiptId = '-1'
		SET @ReceiptId = NULL;

	IF @InboundDocumentId = '-1'
		SET @InboundDocumentId = NULL;

	IF @PriorityId = '-1'
		SET @PriorityId = NULL;

	IF @WarehouseId = '-1'
		SET @WarehouseId = NULL;

	IF @LocationId = '-1'
		SET @LocationId = NULL;

	IF @StatusId = '-1'
		SET @StatusId = NULL;

	IF @OperatorId = '-1'
		SET @OperatorId = NULL;

	IF @ShippingAgentId = '-1'
		SET @ShippingAgentId = NULL;

	IF @VehicleId = '-1'
		SET @VehicleId = NULL;

	IF @VehicleTypeId = '-1'
		SET @VehicleTypeId = NULL;

	DECLARE @Error INT

	IF (@LockReceipt = 1)
	BEGIN
		UPDATE Receipt
		SET Locked = 1
		WHERE ReceiptId = @ReceiptId
	END

	UPDATE Receipt
	SET InboundDocumentId = isnull(@InboundDocumentId, InboundDocumentId)
		,PriorityId = isnull(@PriorityId, PriorityId)
		,WarehouseId = isnull(@WarehouseId, WarehouseId)
		,LocationId = isnull(@LocationId, LocationId)
		,StatusId = isnull(@StatusId, StatusId)
		,OperatorId = isnull(@OperatorId, OperatorId)
		,DeliveryNoteNumber = isnull(@DeliveryNoteNumber, DeliveryNoteNumber)
		,DeliveryDate = isnull(@DeliveryDate, DeliveryDate)
		,SealNumber = isnull(@SealNumber, SealNumber)
		,VehicleRegistration = isnull(@VehicleRegistration, VehicleRegistration)
		,Remarks = isnull(@Remarks, Remarks)
		,CreditAdviceIndicator = isnull(@CreditAdviceIndicator, CreditAdviceIndicator)
		,Delivery = isnull(@Delivery, Delivery)
		,AllowPalletise = isnull(@AllowPalletise, AllowPalletise)
		,Interfaced = isnull(@Interfaced, Interfaced)
		,StagingLocationId = isnull(@StagingLocationId, StagingLocationId)
		,NumberOfLines = isnull(@NumberOfLines, NumberOfLines)
		,ReceivingCompleteDate = isnull(@ReceivingCompleteDate, ReceivingCompleteDate)
		,ParentReceiptId = isnull(@ParentReceiptId, ParentReceiptId)
		,GRN = isnull(@GRN, GRN)
		,PlannedDeliveryDate = isnull(@PlannedDeliveryDate, PlannedDeliveryDate)
		,ShippingAgentId = isnull(@ShippingAgentId, ShippingAgentId)
		,ContainerNumber = isnull(@ContainerNumber, ContainerNumber)
		,AdditionalText1 = isnull(@AdditionalText1, AdditionalText1)
		,AdditionalText2 = isnull(@AdditionalText2, AdditionalText2)
		,BOE = isnull(@BOE, BOE)
		,Incoterms = isnull(@Incoterms, Incoterms)
		,ReceiptConfirmed = isnull(@ReceiptConfirmed, ReceiptConfirmed)
		,ReceivingStarted = isnull(@ReceivingStarted, ReceivingStarted)
		,VehicleId = isnull(@VehicleId, VehicleId)
		,ParentIssueId = isnull(@ParentIssueId, ParentIssueId)
		,VehicleTypeId = isnull(@VehicleTypeId, VehicleTypeId)
	WHERE ReceiptId = @ReceiptId

	SELECT @Error = @@Error

	RETURN @Error
END
