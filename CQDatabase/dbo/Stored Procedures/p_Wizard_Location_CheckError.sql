﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Location_CheckError
  ///   Filename       : p_Wizard_Location_CheckError.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 23 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Location_CheckError

 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
		  @ErrorFlag		 bit	
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
 
	If Exists(Select Distinct(HasError) From LocationImport Where HasError = 1)
	
			Begin
				Set @ErrorFlag = 1
				
			end
	else
			Begin
				Set @ErrorFlag = 0
			End

		Select @ErrorFlag as Error

  if @Error <> 0
    goto error

   
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Wizard_Location_CheckError'
    rollback transaction
    return @Error
end
