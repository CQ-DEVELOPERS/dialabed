﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Manual_Palletisation_Select
  ///   Filename       : p_Receiving_Manual_Palletisation_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Manual_Palletisation_Select
(
 @ReceiptLineId int
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  ReceiptLineId    int,
	  StorageUnitId    int,
	  ProductId        int,
	  SKUId            int,
	  ProductCode      nvarchar(30),
	  Product          nvarchar(50),
	  SKUCode          nvarchar(50),
	  AcceptedQuantity float,
	  NumberOfLines    int,
	  PalletQuantity   float
	 )
  
  insert @TableResult
        (ReceiptLineId,
         StorageUnitId,
         ProductId,
         SKUId,
         AcceptedQuantity)
  select rl.ReceiptLineId,
         su.StorageUnitId,
         su.ProductId,
         su.SKUId,
         rl.AcceptedQuantity
    from ReceiptLine rl
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  (nolock) on sub.StorageUnitId     = su.StorageUnitId
   where ReceiptLineId = @ReceiptLineId
  
  update tr
     set NumberOfLines = (select count(1)
                            from Instruction i (nolock)
                           where tr.ReceiptLineId = i.ReceiptLineId)
    from @TableResult tr
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode
    from @TableResult     tr
    join Product          p   (nolock) on tr.ProductId          = p.ProductId
    join SKU              sku (nolock) on tr.SKUId              = sku.SKUId
  
  update tr
     set PalletQuantity = p.Quantity
    from @TableResult     tr
    join Pack              p  (nolock) on tr.StorageUnitId = p.StorageUnitId
  
  select ReceiptLineId,
         ProductCode,
         Product,
         SKUCode,
         AcceptedQuantity,
         NumberOfLines,
         PalletQuantity
    from @TableResult
end
