﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COATest_Update
  ///   Filename       : p_COATest_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:07
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the COATest table.
  /// </remarks>
  /// <param>
  ///   @COATestId int = null,
  ///   @COAId int = null,
  ///   @TestId int = null,
  ///   @ResultId int = null,
  ///   @MethodId int = null,
  ///   @ResultCode nvarchar(20) = null,
  ///   @Result nvarchar(510) = null,
  ///   @Pass bit = null,
  ///   @StartRange float = null,
  ///   @EndRange float = null,
  ///   @Value sql_variant = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COATest_Update
(
 @COATestId int = null,
 @COAId int = null,
 @TestId int = null,
 @ResultId int = null,
 @MethodId int = null,
 @ResultCode nvarchar(20) = null,
 @Result nvarchar(510) = null,
 @Pass bit = null,
 @StartRange float = null,
 @EndRange float = null,
 @Value sql_variant = null 
)
 
as
begin
	 set nocount on;
  
  if @COATestId = '-1'
    set @COATestId = null;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @TestId = '-1'
    set @TestId = null;
  
  if @ResultId = '-1'
    set @ResultId = null;
  
  if @MethodId = '-1'
    set @MethodId = null;
  
	 declare @Error int
 
  update COATest
     set COAId = isnull(@COAId, COAId),
         TestId = isnull(@TestId, TestId),
         ResultId = isnull(@ResultId, ResultId),
         MethodId = isnull(@MethodId, MethodId),
         ResultCode = isnull(@ResultCode, ResultCode),
         Result = isnull(@Result, Result),
         Pass = isnull(@Pass, Pass),
         StartRange = isnull(@StartRange, StartRange),
         EndRange = isnull(@EndRange, EndRange),
         Value = isnull(@Value, Value) 
   where COATestId = @COATestId
  
  select @Error = @@Error
  
  
  return @Error
  
end
