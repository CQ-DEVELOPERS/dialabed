﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shifts_Delete
  ///   Filename       : p_Shifts_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:47
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Shifts table.
  /// </remarks>
  /// <param>
  ///   @ShiftId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shifts_Delete
(
 @ShiftId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Shifts
     where ShiftId = @ShiftId
  
  select @Error = @@Error
  
  
  return @Error
  
end
