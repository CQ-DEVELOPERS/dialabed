﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentTypeHistory_Insert
  ///   Filename       : p_OutboundDocumentTypeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jan 2014 09:16:03
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundDocumentTypeHistory table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentTypeId int = null,
  ///   @OutboundDocumentType nvarchar(60) = null,
  ///   @PriorityId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @MinimumShelfLife numeric(13,3) = null,
  ///   @AlternatePallet bit = null,
  ///   @SubstitutePallet bit = null,
  ///   @OutboundDocumentTypeCode nvarchar(20) = null,
  ///   @AutoRelease bit = null,
  ///   @AddToShipment bit = null,
  ///   @MultipleOnShipment bit = null,
  ///   @LIFO bit = null,
  ///   @AreaType nvarchar(20) = null,
  ///   @AutoSendup bit = null,
  ///   @CheckingLane int = null,
  ///   @DespatchBay int = null,
  ///   @PlanningComplete bit = null,
  ///   @AutoInvoice bit = null,
  ///   @Backorder bit = null,
  ///   @AutoCheck bit = null,
  ///   @ReserveBatch bit = null 
  /// </param>
  /// <returns>
  ///   OutboundDocumentTypeHistory.OutboundDocumentTypeId,
  ///   OutboundDocumentTypeHistory.OutboundDocumentType,
  ///   OutboundDocumentTypeHistory.PriorityId,
  ///   OutboundDocumentTypeHistory.CommandType,
  ///   OutboundDocumentTypeHistory.InsertDate,
  ///   OutboundDocumentTypeHistory.MinimumShelfLife,
  ///   OutboundDocumentTypeHistory.AlternatePallet,
  ///   OutboundDocumentTypeHistory.SubstitutePallet,
  ///   OutboundDocumentTypeHistory.OutboundDocumentTypeCode,
  ///   OutboundDocumentTypeHistory.AutoRelease,
  ///   OutboundDocumentTypeHistory.AddToShipment,
  ///   OutboundDocumentTypeHistory.MultipleOnShipment,
  ///   OutboundDocumentTypeHistory.LIFO,
  ///   OutboundDocumentTypeHistory.AreaType,
  ///   OutboundDocumentTypeHistory.AutoSendup,
  ///   OutboundDocumentTypeHistory.CheckingLane,
  ///   OutboundDocumentTypeHistory.DespatchBay,
  ///   OutboundDocumentTypeHistory.PlanningComplete,
  ///   OutboundDocumentTypeHistory.AutoInvoice,
  ///   OutboundDocumentTypeHistory.Backorder,
  ///   OutboundDocumentTypeHistory.AutoCheck,
  ///   OutboundDocumentTypeHistory.ReserveBatch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentTypeHistory_Insert
(
 @OutboundDocumentTypeId int = null,
 @OutboundDocumentType nvarchar(60) = null,
 @PriorityId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @MinimumShelfLife numeric(13,3) = null,
 @AlternatePallet bit = null,
 @SubstitutePallet bit = null,
 @OutboundDocumentTypeCode nvarchar(20) = null,
 @AutoRelease bit = null,
 @AddToShipment bit = null,
 @MultipleOnShipment bit = null,
 @LIFO bit = null,
 @AreaType nvarchar(20) = null,
 @AutoSendup bit = null,
 @CheckingLane int = null,
 @DespatchBay int = null,
 @PlanningComplete bit = null,
 @AutoInvoice bit = null,
 @Backorder bit = null,
 @AutoCheck bit = null,
 @ReserveBatch bit = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OutboundDocumentTypeHistory
        (OutboundDocumentTypeId,
         OutboundDocumentType,
         PriorityId,
         CommandType,
         InsertDate,
         MinimumShelfLife,
         AlternatePallet,
         SubstitutePallet,
         OutboundDocumentTypeCode,
         AutoRelease,
         AddToShipment,
         MultipleOnShipment,
         LIFO,
         AreaType,
         AutoSendup,
         CheckingLane,
         DespatchBay,
         PlanningComplete,
         AutoInvoice,
         Backorder,
         AutoCheck,
         ReserveBatch)
  select @OutboundDocumentTypeId,
         @OutboundDocumentType,
         @PriorityId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @MinimumShelfLife,
         @AlternatePallet,
         @SubstitutePallet,
         @OutboundDocumentTypeCode,
         @AutoRelease,
         @AddToShipment,
         @MultipleOnShipment,
         @LIFO,
         @AreaType,
         @AutoSendup,
         @CheckingLane,
         @DespatchBay,
         @PlanningComplete,
         @AutoInvoice,
         @Backorder,
         @AutoCheck,
         @ReserveBatch 
  
  select @Error = @@Error
  
  
  return @Error
  
end
