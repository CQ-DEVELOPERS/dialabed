﻿
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Import_Main
  ///   Filename       : p_Plascon_Import_Main.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 30 Oct 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Plascon_Import_Main]
(
 @RecordType varchar(30) = null
)
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'p_Plascon_Import_Main - Error executing p_Plascon_Import_Main'
  
  begin transaction
  
    if (select COUNT(1)
        from InterfaceImportLot
        where RecordStatus = 'N'
          and ProcessedDate is null) > 0
    begin
        exec p_plascon_Import_Lot
    end
  
    if (select count(1) 
        from InterfaceImportSKU
        where RecordStatus = 'N'
           and ProcessedDate is null) > 0
    begin
        exec p_Plascon_Import_SKU
    end

    if (select count(1) 
        from InterfaceImportProduct
        where RecordStatus = 'N'
           and ProcessedDate is null) > 0
    begin
        exec p_Plascon_Import_Product
    end

    if (select count(1) 
        from InterfaceImportHeader
        where RecordStatus = 'N'
           and ProcessedDate is null
           and InsertDate > DATEADD(DD, -5, GETDATE())) > 0
    begin
        exec @Error = p_Plascon_Import_Order
    end
    
    if (select count(1) 
        from FloScheduleImport
        where RecordStatus = 'N'
           and ProcessedDate is null) > 0
    begin
        exec @Error = p_Plascon_Import_Schedule
    end
    
    select @Error = @@Error, @Errormsg = 'p_Plascon_Import_Main - Error updating InterfaceExtract'
  
  if @Error <> 0
    goto error
    
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error  
end

