﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StatusRollupLog_Update
  ///   Filename       : p_StatusRollupLog_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:39
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StatusRollupLog table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @JobCode varchar(10) = null,
  ///   @CreateDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StatusRollupLog_Update
(
 @JobId int = null,
 @JobCode varchar(10) = null,
 @CreateDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update StatusRollupLog
     set JobId = isnull(@JobId, JobId),
         JobCode = isnull(@JobCode, JobCode),
         CreateDate = isnull(@CreateDate, CreateDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
