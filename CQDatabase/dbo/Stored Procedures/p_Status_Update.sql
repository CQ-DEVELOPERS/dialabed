﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Update
  ///   Filename       : p_Status_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:54
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Status table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null,
  ///   @Status nvarchar(100) = null,
  ///   @StatusCode nvarchar(20) = null,
  ///   @Type char(2) = null,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Update
(
 @StatusId int = null,
 @Status nvarchar(100) = null,
 @StatusCode nvarchar(20) = null,
 @Type char(2) = null,
 @OrderBy int = null 
)
 
as
begin
	 set nocount on;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @Status = '-1'
    set @Status = null;
  
  if @StatusCode = '-1'
    set @StatusCode = null;
  
	 declare @Error int
 
  update Status
     set Status = isnull(@Status, Status),
         StatusCode = isnull(@StatusCode, StatusCode),
         Type = isnull(@Type, Type),
         OrderBy = isnull(@OrderBy, OrderBy) 
   where StatusId = @StatusId
  
  select @Error = @@Error
  
  
  return @Error
  
end
