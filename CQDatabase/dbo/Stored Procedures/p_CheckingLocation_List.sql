﻿/*
  /// <summary>
  ///   Procedure Name : p_CheckingLocation_List
  ///   Filename       : p_CheckingLocation_List.sql
  ///   Create By      : Gareth Yeoman
  ///   Date Created   : 2020-12-08
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Route table.
  /// </remarks>
  /// <param>
  ///   @RouteId int = null 
  /// </param>
  /// <returns>
  ///   Route.CheckingLaneId,
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE [dbo].[p_CheckingLocation_List] 
AS
BEGIN
	 SET NOCOUNT ON;
 
SELECT l.LocationId,
		 l.Location 
  FROM Area A (NOLOCK)
  JOIN AreaLocation (NOLOCK) al ON al.AreaId = a.AreaId
  JOIN Location l (NOLOCK) ON l.LocationId = al.LocationId
  WHERE AreaCode in ('D')
  
END
