﻿USE [DialaBed]
GO
/****** Object:  StoredProcedure [dbo].[p_Label_Variables]    Script Date: 07/02/2021 11:41:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
/*      
  /// <summary>      
  ///   Procedure Name : p_Label_Variables      
  ///   Filename       : p_Label_Variables.sql      
  ///   Create By      : Grant Schultz      
  ///   Date Created   : 31 Jul 2015      
  /// </summary>      
  /// <remarks>      
  ///         
  /// </remarks>      
  /// <param>      
  ///         
  /// </param>      
  /// <returns>      
  ///         
  /// </returns>      
  /// <newpara>      
  ///   Modified by    : Daniel Schotter      
  ///   Modified Date  : April 2018      
  ///   Details        : Increased the securitycode parameter size.      
  /// </newpara>      
*/      
CREATE procedure [dbo].[p_Label_Variables]      
(      
 @label              nvarchar(50),      
 @Title              nvarchar(200) = null,      
 @PalletId           int = null,      
 @instructionId      int = null,      
 @JobId              int = null,      
 @barcode            nvarchar(50) = null,      
 @KeyId              nvarchar(50) = null,      
 @WarehouseId        int = 1,      
 @locationId         int = null      
)      
--with encryption      
as      
begin      
  set nocount on;      
        
  declare @GetDate            datetime      
         ,@LabelId            int      
         ,@ProductCode        nvarchar(63)      
         ,@Product            nvarchar(200)      
         ,@SKUCode            nvarchar(50)      
         ,@Batch              nvarchar(30)      
         ,@StartDate          datetime      
         ,@Location           nvarchar(15)      
         ,@Prints             nvarchar(20)      
         ,@Pallet             nvarchar(10)      
         ,@OrderNumber        nvarchar(max)      
         ,@ExternalCompanyId  int      
         ,@Customer           nvarchar(255)      
         ,@DeliveryDate       datetime      
         ,@RouteId            int      
         ,@Route              nvarchar(50)      
         ,@IssueId            int      
         ,@IssueLineId        int      
         ,@Weight             numeric(13,3)      
         ,@Quantity           numeric(13,6)      
         ,@Reason             nvarchar(300)      
         ,@OutboundShipmentId int      
         ,@StorageUnitId      int      
         ,@ProductAlias       nvarchar(30)      
         ,@StatusCode         nvarchar(10)      
         ,@BatchId            int      
         ,@ExpiryDate         datetime      
         ,@Count              int      
         ,@DropSequence       int      
         ,@OutboundDocumentId int      
         ,@NettWeight         numeric(13,6)      
         ,@GrossWeight        numeric(13,6)      
         ,@OperatorGroup      nvarchar(50)      
         ,@Operator           nvarchar(50)      
         ,@Password           nvarchar(50)      
         ,@ReferenceNumber    nvarchar(30)      
         ,@SecurityCode       nvarchar(50) --Increased from 10 to 50.      
         ,@ChildProductCode   nvarchar(30)      
         ,@LCP                bit = 0      
         ,@Level              nvarchar(10)      
         ,@Error              int      
         ,@ValueLabel         nvarchar(20)      
         ,@InboundDocumentId  int      
         ,@Remarks            nvarchar(255)      
         ,@Division           nvarchar(50)      
         ,@NetworkNumber      nvarchar(50)      
         ,@Case               numeric(13,6)      
         ,@CustomerAddress    nvarchar(500)      
         ,@CustomerCode       nvarchar(20)      
         ,@CustomsCode        nvarchar(20)      
         ,@CustomerContact    nvarchar(50)      
         ,@CustomerTelephone  nvarchar(20)      
         ,@Email              nvarchar(50)      
         ,@Fax                nvarchar(20)      
         ,@Instructions       nvarchar(500)      
         ,@PhysicalAddress    nvarchar(500)      
         ,@PickedDate         nvarchar(20)      
         ,@PostalAddress      nvarchar(500)      
         ,@RegNo              nvarchar(20)      
         ,@TelephoneNumber    nvarchar(20)      
         ,@StorageUnitBatchId int      
         ,@Size               nvarchar(50)      
,@Colour             nvarchar(50)      
         ,@Style              nvarchar(50)      
         ,@UnitPrice          numeric(13,2)      
         ,@WaveId             int      
         ,@Wave               nvarchar(50)      
         ,@FileName           nvarchar(500)      
         ,@BOE                nvarchar(50)      
         ,@BOL                nvarchar(50)      
         ,@ContainerNumber    nvarchar(50)      
         ,@SerialNumber   nvarchar(50)      
         ,@BatchReference     nvarchar(50)      
         ,@Command            nvarchar(max)      
         ,@DatabaseName       sysname      
         ,@rowcount           int      
         -- Added by Darren for Plumblink Despatch By Order Label      
         ,@PickerId     int      
         ,@CheckerId    int      
         ,@Picker     nvarchar(50)      
         ,@Checker     nvarchar(50)      
         ,@PickDate     datetime      
         ,@CheckDate    datetime      
         ,@PickSlipNumber   nvarchar(50)      
         ,@DeliveryAddress   nvarchar(500)    
   -- / Added by Darren for Plumblink Despatch By Order Label      
        
  select @GetDate = dbo.ufn_Getdate()      
        
  --select @LabelId = LabelId      
  --  from CQCommon.dbo.Label (nolock)      
  -- where Label = @label      
        
  declare @Labels as table      
  (      
   LabelId int      
  )      
    create table #LabelVariables       
  --declare @LabelVariables as table      
  (      
   LabelVariablesId int,      
   LabelId          int,      
   Name             nvarchar(200),      
   Value            nvarchar(255),      
   Length           int,      
   FixedLength      bit,      
   IsMultiline      bit,      
   LineLength       int,      
   LineCount        int,      
   FormatID         int,      
   Prompt           nvarchar(50),      
   ValueRequired    bit      
  )      
        
    select top 1 @DatabaseName = convert(sysname, Value)      
    from Configuration (nolock)      
    where ConfigurationId = 201      
        
  select @Command = 'select LabelId from [' + @DatabaseName + '].dbo.Label (nolock) where Label = ''' + replace(@label,' (Instruction)','') + ''''      
        
  insert @Labels      
  exec (@Command)      
        
  if @@rowcount > 0      
    select top 1 @LabelId = LabelId from @Labels      
        
  set @Command = null      
        
  select @Command = '      
  select Name,      
         Value,      
         Length,      
         FixedLength,      
         IsMultiline,      
         LineLength,      
         LineCount,      
         FormatID,      
         Prompt,      
         ValueRequired      
    from [' + @DatabaseName + '].dbo.LabelVariables (nolock)      
   where LabelId = ' + convert(nvarchar(10), @LabelId)      
        
  insert #LabelVariables      
        (Name,      
         Value,      
         Length,      
         FixedLength,      
         IsMultiline,      
         LineLength,      
         LineCount,      
         FormatID,      
         Prompt,      
         ValueRequired)      
  exec (@Command)      
        
  select @rowcount = count(1)      
    from #LabelVariables      
        
  if @rowcount = 0 and @label like '%Location%'      
  begin      
    set @Command = null      
          
    select @Command = '      
    select Name,      
           Value,      
           Length,      
           FixedLength,      
           IsMultiline,      
           LineLength,      
           LineCount,      
           FormatID,      
           Prompt,      
           ValueRequired      
      from [' + @DatabaseName + '].dbo.Label           l (nolock)      
      join [' + @DatabaseName + '].dbo.LabelVariables lv (nolock) on l.LabelId = lv.LabelId      
     where l.Label = ''Pickface Label Large.lbl'''      
          
    insert #LabelVariables      
          (Name,      
           Value,      
           Length,      
           FixedLength,      
           IsMultiline,      
           LineLength,      
           LineCount,      
           FormatID,      
           Prompt,      
           ValueRequired)      
    exec (@Command)      
  end      
        
  -- *** Pallet Label.lbl','Receiving & Staging Label Large.lbl','Pallet Label Small.lbl ***      
        
  if (@PalletId is not null or @instructionId is not null) and @Label in ('Pallet Label.lbl','Receiving & Staging Label Large.lbl','Pallet Label Small.lbl')      
  begin      
    if @PalletId is not null      
    begin      
      select @ProductCode        = p.ProductCode,      
             @Product     = p.Product,      
             @SKUCode            = sku.SKUCode,      
             @Batch              = b.Batch,      
             @ExpiryDate         = b.ExpiryDate,      
             @StartDate          = @GetDate,      
             @LocationId         = pl.LocationId,      
             @PalletId           = pl.PalletId,      
             @Prints             = 'Copy ' + convert(nvarchar(20), isnull(pl.Prints, 1)),      
             @Quantity           = pl.Quantity,      
             @StorageUnitId      = su.StorageUnitId,      
             @StorageUnitBatchId = sub.StorageUnitBatchId      
     from Pallet            pl (nolock)      
        join StorageUnitBatch sub (nolock) on pl.StorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
        join Batch              b (nolock) on sub.BatchId           = b.BatchId      
       where pl.PalletId = @PalletId      
            
      select @ChildProductCode = p.ProductCode      
        from ChildProduct      cp (nolock)      
        join StorageUnitBatch sub (nolock) on cp.ChildStorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId          = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId               = p.ProductId      
       where cp.ParentStorageUnitBatchId = @StorageUnitBatchId      
            
      if @ChildProductCode is not null      
        set @ProductCode = @ProductCode + ' / ' + @ChildProductCode      
            
      if exists(select 1      
                 from AreaLocation al (nolock)      
                 join Area          a (nolock) on al.AreaId = a.AreaId      
                where al.LocationId = @LocationId      
                  and a.AreaCode in ('BK','R'))      
      begin      
        select @Quantity = subl.ActualQuantity / pk.Quantity      
          from Pallet                      p (nolock)      
          join StorageUnitBatch          sub (nolock) on p.StorageUnitBatchId = sub.StorageUnitBatchId      
          join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId      
                                                     and p.LocationId = subl.LocationId      
          join Pack                       pk (nolock) on sub.StorageUnitId    = pk.StorageUnitId      
         where pk.PackTypeId = 1      
           and p.PalletId    = @PalletId      
           and p.LocationId  = @LocationId      
           and pk.Quantity  != 999999      
      end      
    end      
    else      
      select @WarehouseId   = i.WarehouseId,      
             @ProductCode   = p.ProductCode,      
             @Product       = p.Product,      
             @SKUCode       = sku.SKUCode,      
             @Batch         = b.Batch,      
             @ExpiryDate    = b.ExpiryDate,      
             @StartDate     = @GetDate,      
             @LocationId    = i.StoreLocationId,      
             @PalletId      = pl.PalletId,      
             @Prints        = 'Copy ' + convert(nvarchar(20), isnull(pl.Prints, 1)),      
             @Quantity      = i.ConfirmedQuantity,      
             @StorageUnitId = su.StorageUnitId,      
             @NettWeight    = j.NettWeight,      
             @GrossWeight   = j.Weight      
        from Instruction        i (nolock)      
        join Job                j (nolock) on i.JobId              = j.JobId      
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId         = p.ProductId      
        join SKU              sku (nolock) on su.SKUId             = sku.SKUId      
        join Batch              b (nolock) on sub.BatchId          = b.BatchId      
        left outer      
        join Pallet     pl (nolock) on i.PalletId           = pl.PalletId      
       where i.InstructionId = @InstructionId      
          
    select @Case = min(Quantity)      
      from Pack (nolock)      
     where WarehouseId   = @WarehouseId      
       and StorageUnitId = @StorageUnitId      
       and Quantity > 1      
          
    if dbo.ufn_Configuration(183, @WarehouseId) = 1      
    begin      
      update #LabelVariables set Value = 'Gross Weight' where Value = '@WeightDesc'      
      update #LabelVariables set Value = CONVERT(nvarchar(20), @GrossWeight) where Value = '@Weight'      
    end      
    else if dbo.ufn_Configuration(184, @WarehouseId) = 1      
    begin      
      update #LabelVariables set Value = 'Nett Weight' where Value = '@WeightDesc'      
 update #LabelVariables set Value = CONVERT(nvarchar(20), @NettWeight) where Value = '@Weight'      
    end      
          
    select @Location = Location      
      from Location      
     where LocationId = @LocationId      
          
    if @Location is null      
    begin      
      set @Location = null      
            
      select top 1 @Location = a.AreaCode + '-'      
        from AreaLocation     al (nolock)      
        join Area              a (nolock) on al.AreaId = a.AreaId      
        join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId      
       where sua.StorageUnitId = @StorageUnitId      
      order by StoreOrder      
            
      select top 1 @Location = isnull(@Location,'') + isnull(l.Ailse,l.Location)      
        from StorageUnitLocation sul (nolock)      
        join Location              l (nolock) on sul.LocationId = l.LocationId      
       where sul.StorageUnitId = @StorageUnitId      
            
      if @Location is null      
        set @Location = 'NOT SETUP'      
    end      
          
    update #LabelVariables set Value = null where Value = '@WeightDesc'      
    update #LabelVariables set Value = null where Value = '@Weight'      
          
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @SKUCode where Value = '@SKUCode'      
    update #LabelVariables set Value = @Batch where Value = '@Batch'      
    update #LabelVariables set Value = convert(nvarchar(20), @StartDate, 120) where Value = '@StartDate'      
    update #LabelVariables set Value = convert(nvarchar(10), @ExpiryDate, 120) where Value = '@ExpiryDate'      
    update #LabelVariables set Value = @Location where Value = '@Location'      
    update #LabelVariables set Value = 'P:' + isnull(convert(nvarchar(12), @PalletId),'none') where Value = '@PalletId'      
    update #LabelVariables set Value = @Prints where Value = '@Prints'      
    update #LabelVariables set Value = convert(nvarchar(50), convert(numeric(13), @Quantity)) where Value = '@Quantity'      
    update #LabelVariables set Value = convert(nvarchar(50), convert(numeric(13), @Case)) where Value = '@Case'      
  end      
        
  -- *** / Pallet Label.lbl','Receiving & Staging Label Large.lbl','Pallet Label Small.lbl ***      
        
  -- *** Reject Label.lbl ***      
        
  if (@JobId is not null) and @Label = 'Reject Label.lbl'      
  begin      
    if @PalletId is not null      
      select top 1 @PalletId    = PalletId,      
             @InstructionId = InstructionId      
        from Instruction (nolock)      
       where PalletId = @PalletId      
      order by InstructionId      
    else if @instructionId is not null      
      select top 1 @PalletId    = PalletId,      
             @InstructionId = InstructionId      
        from Instruction (nolock)      
       where InstructionId = @InstructionId      
      order by InstructionId      
    else if @jobId is not null      
      select top 1 @PalletId      = PalletId,      
             @InstructionId = InstructionId      
        from Instruction (nolock)      
       where JobId = @jobId      
      order by InstructionId      
          
    select top 1 @Reason = Reason      
      from Exception e (nolock)      
      join Reason    r (nolock) on e.ReasonId = r.ReasonId         where e.InstructionId = @InstructionId      
       and r.ReasonCode like 'RW%'      
    order by e.ExceptionId desc      
          
    update #LabelVariables set Value = 'P:' + isnull(convert(nvarchar(12), @PalletId),'none') where Value = '@PalletId'      
    update #LabelVariables set Value = isnull(@Reason, 'Reason could not be found') where Value = '@Reason'      
  end      
        
  -- *** / Reject Label.lbl ***      
        
  -- *** Receiving & Staging Label.lbl ***      
        
  if (@PalletId is not null or @instructionId is not null or @jobId is not null) and @Label = 'Receiving & Staging Label.lbl'      
  begin      
    select @Title = 'Production Label'      
          
    if @PalletId is not null      
      select @ProductCode = p.ProductCode,      
             @Product     = p.Product,      
             @SKUCode     = sku.SKUCode,      
            @Batch       = b.Batch,      
             @StartDate   = @GetDate,      
   @PalletId    = pl.PalletId,      
             @Prints      = 'Copy ' + convert(nvarchar(20), isnull(pl.Prints, 1)),      
             @Quantity    = pl.Quantity,      
             @StorageUnitId = su.StorageUnitId      
        from Pallet            pl (nolock)      
        join StorageUnitBatch sub (nolock) on pl.StorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
        join Batch              b (nolock) on sub.BatchId           = b.BatchId      
       where pl.PalletId = @PalletId      
    else if @instructionId is not null      
      select @ProductCode = p.ProductCode,      
             @Product     = p.Product,      
             @SKUCode     = sku.SKUCode,      
             @Batch       = b.Batch,      
             @StartDate   = i.CreateDate,      
             @PalletId    = pl.PalletId,      
             @Prints      = 'Copy ' + convert(nvarchar(20), isnull(pl.Prints, 1)),      
             @Quantity    = i.Quantity,      
             @StorageUnitId = su.StorageUnitId      
        from Instruction        i (nolock)      
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
        join Batch              b (nolock) on sub.BatchId           = b.BatchId      
        left outer      
        join Pallet            pl (nolock) on i.PalletId           = pl.PalletId      
       where i.InstructionId = @InstructionId      
    else if @jobId is not null      
      select @ProductCode = p.ProductCode,      
             @Product     = p.Product,      
             @SKUCode     = sku.SKUCode,      
             @Batch       = b.Batch,      
             @StartDate   = i.CreateDate,      
             @PalletId    = pl.PalletId,      
             @Prints      = 'Copy ' + convert(nvarchar(20), isnull(pl.Prints, 1)),      
             @Quantity    = i.Quantity,      
             @StorageUnitId = su.StorageUnitId      
        from Instruction        i (nolock)      
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
        join Batch              b (nolock) on sub.BatchId           = b.BatchId      
        left outer      
        join Pallet            pl (nolock) on i.PalletId           = pl.PalletId      
       where i.JobId = @jobId      
          
    --select top 1 @Location = a.AreaCode      
    --  from AreaLocation     al (nolock)      
    --  join Area              a (nolock) on al.AreaId = a.AreaId      
    --  join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId      
    -- where sua.StorageUnitId = @StorageUnitId      
    --order by StoreOrder      
          
    --select @Location = Location      
    --  from Location      
    -- where LocationId = @LocationId      
    --select @Location      
    if @Location is null      
    begin      
      set @Location = null      
            
      select top 1 @Location = a.AreaCode + '-'      
        from AreaLocation     al (nolock)      
        join Area              a (nolock) on al.AreaId = a.AreaId      
        join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId      
       where sua.StorageUnitId = @StorageUnitId      
      order by StoreOrder      
            
      select top 1 @Location = isnull(@Location,'') + isnull(l.Ailse,l.Location)      
        from StorageUnitLocation sul (nolock)      
        join Location              l (nolock) on sul.LocationId = l.LocationId      
       where sul.StorageUnitId = @StorageUnitId              
      if @Location is null      
        set @Location = 'NOT SETUP'      
  end      
          
    if @Location is not null      
      set @ProductCode = @ProductCode + '   ' + @Location      
          
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @SKUCode where Value = '@SKUCode'      
    update #LabelVariables set Value = @Batch where Value = '@Batch'      
    update #LabelVariables set Value = convert(nvarchar(20), @StartDate, 120) where Value = '@StartDate'      
    update #LabelVariables set Value = @Location where Value = '@Location'      
    update #LabelVariables set Value = 'P:' + isnull(convert(nvarchar(12), @PalletId),'none') where Value = '@PalletId'      
    update #LabelVariables set Value = @Prints where Value = '@Prints'      
    update #LabelVariables set Value = convert(nvarchar(10), @Quantity) where Value = '@Quantity'      
    update #LabelVariables set Value = @Title where Value = '@Title'      
  end      
        
  -- *** / Receiving & Staging Label.lbl ***      
        
  -- *** Builders Product Label Large.lbl ***      
        
  if (@JobId is not null) and @Label in ('Builders Product Label Large.lbl')      
  begin      
    if @JobId < 0      
    begin      
      select @StorageUnitId      = su.StorageUnitId,      
             --@barcode            = p.Barcode,      
             @ProductAlias       = pt.PackType + ' = ' + convert(nvarchar(10), pk.Quantity),      
             @ProductCode        = p.ProductCode,      
             @Product            = p.Product,      
             @SKUCode            = sku.SKUCode,      
             @PickedDate         = convert(nvarchar(20), @GetDate, 120),      
             @Quantity           = pk.Quantity      
        from Pack              pk (nolock)      
        join PackType          pt (nolock) on pk.PackTypeId        = pt.PackTypeId      
        join StorageUnit       su (nolock) on pk.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId         = p.ProductId      
        join SKU              sku (nolock) on su.SKUId             = sku.SKUId      
       where pk.PackId = @JobId*-1      
    end      
    else      
    begin      
      select @WarehouseId        = i.WarehouseId,      
             @StorageUnitId      = su.StorageUnitId,      
             --@barcode            = p.Barcode,      
             @ProductCode        = p.ProductCode,      
             @Product            = p.Product,      
             @SKUCode            = sku.SKUCode,      
             @PickedDate         = convert(nvarchar(20), max(i.EndDate), 120),      
             @Quantity           = sum(i.ConfirmedQuantity)      
        from Job                j (nolock)      
        join Instruction        i (nolock) on j.JobId              = i.JobId      
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId            join Product            p (nolock) on su.ProductId         = p.ProductId      
        join SKU              sku (nolock) on su.SKUId             = sku.SKUId      
       where j.JobId = @JobId      
      group by i.WarehouseId,      
               su.StorageUnitId,      
               su.StorageUnitId,      
               p.Barcode,      
               p.ProductCode,      
               p.Product,      
               sku.SKUCode      
    end      
            
      select @ProductAlias = pt.PackType + ' = ' + convert(nvarchar(10), pk.Quantity),      
             @Barcode      = isnull(pk.Barcode, @barcode)      
        from Pack     pk (nolock)      
        join PackType pt (nolock) on pk.PackTypeId = pt.PackTypeId      
       where pk.StorageUnitId = @StorageUnitId      
         and pk.WarehouseId   = @WarehouseId      
         and pk.Quantity      = @Quantity      
            
      select @Barcode      = isnull(pk.Barcode, @barcode)      
        from Pack     pk (nolock)      
        join PackType pt (nolock) on pk.PackTypeId = pt.PackTypeId      
       where pk.StorageUnitId = @StorageUnitId      
         and pk.WarehouseId   = @WarehouseId      
         and pk.Barcode is not null      
          
    if @ProductAlias is null      
      select @ProductAlias = 'Undefined'      
          
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'      
    update #LabelVariables set Value = @SKUCode where Value = '@SKUCode'      
    update #LabelVariables set Value = rtrim(@Barcode) where Value = '@Barcode'      
    update #LabelVariables set Value = convert(nvarchar(20), @PickedDate, 120) where Value = '@DateAndTime'      
    update #LabelVariables set Value = convert(int, @Quantity) where Value = '@Quantity'      
  end      
        
  -- *** / Builders Product Label Large.lbl ***      
        
  -- *** Clothing Label Large.lbl', 'Clothing Label Small.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Clothing Label Large.lbl','Clothing Label Small.lbl')      
  begin      
    select @barcode            = isnull(pk.Barcode, p.Barcode),      
           @ProductCode        = p.ProductCode,      
           @Product            = p.Product,      
           @SKUCode            = pt.PackType,      
           @Size               = isnull(su.Size,'none'),      
           @Colour             = isnull(su.Colour,'none'),      
           @Style              = isnull(su.Style,'none'),      
           @UnitPrice          = su.UnitPrice,      
           --@FileName           = 'C:\Program Files (x86)\EuroPlus\NiceLabel WebSDK\NicePrintServer\labels\Cquential Solutions\Generic\Images\' + isnull(pn.PrincipalCode,'ApparelX') + ' - ' + convert(nvarchar(10), p.HostId) + '.jpg',      
      @FileName           = 'C:\Program Files (x86)\EuroPlus\NiceLabel WebSDK\NicePrintServer\labels\Cquential Solutions\Generic\Images\ApparelX - 11061.jpg',      
           @OrderNumber        = '',      
           @ProductAlias       = '',      
           @Quantity           = pk.Quantity,      
           @Weight             = pk.Weight      
      from StorageUnit       su (nolock)      
      join Pack              pk (nolock) on su.StorageUnitId     = pk.StorageUnitId      
                                        and pk.WarehouseId       = 1      
      join PackType          pt (nolock) on pk.PackTypeId        = pt.PackTypeId      
      join Product            p (nolock) on su.ProductId         = p.ProductId      
      join SKU              sku (nolock) on su.SKUId             = sku.SKUId      
      left      
      join Principal         pn (nolock) on p.PrincipalId        = pn.PrincipalId      
     where pk.PackId in (@KeyId, @KeyId*-1)      
          
    select @ProductAlias = pt.PackType + ' = ' + convert(nvarchar(10), pk.Quantity),      
           @Barcode      = isnull(pk.Barcode, @barcode)      
      from Pack     pk (nolock)      
      join PackType pt (nolock) on pk.PackTypeId = pt.PackTypeId      
     where pk.StorageUnitId = @StorageUnitId      
       and pk.WarehouseId   = @WarehouseId      
       and pk.Quantity      = @Quantity      
          
    if @ProductAlias is null      
      select @ProductAlias = 'Undefined'      
          
    select @Batch = Batch      
      from Batch      
     where BatchId = @LocationId      
          
    set @barcode = @ProductCode      
    set @OrderNumber = @Batch      
          
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'      
    update #LabelVariables set Value = @SKUCode where Value = '@SKUCode'      
    update #LabelVariables set Value = rtrim(@Barcode) where Value = '@Barcode'      
    update #LabelVariables set Value = convert(nvarchar(20), @PickedDate, 120) where Value = '@DateAndTime'      
    update #LabelVariables set Value = convert(int, @Quantity) where Value = '@Quantity'      
    update #LabelVariables set Value = @Size where Value = '@Size'      
    update #LabelVariables set Value = @Colour where Value = '@Colour'      
    update #LabelVariables set Value = @Style where Value = '@Style'      
    update #LabelVariables set Value = @Weight where Value = '@Weight'      
    update #LabelVariables set Value = @FileName where Value = '@FileName'      
    update #LabelVariables set Value = @OrderNumber where Value = '@OrderNumber'      
  end      
        
  -- *** / Clothing Label Large.lbl', 'Clothing Label Small.lbl ***      
        
  -- *** Moresport Ticket.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Moresport Ticket.lbl')      
  begin      
    select @barcode            = sku.SKUCode,      
           @ProductCode        = p.ProductCode,      
           @Product            = p.Product,      
           @SKUCode            = sku.SKUCode,      
           @Size               = su.Size,      
           @Colour             = su.Colour,      
           @Style              = su.Style,      
           @UnitPrice          = su.UnitPrice      
      from StorageUnit       su (nolock)      
      join Product            p (nolock) on su.ProductId         = p.ProductId      
      join SKU              sku (nolock) on su.SKUId             = sku.SKUId      
     where su.StorageUnitId in (@KeyId, @KeyId*-1)      
          
    if @KeyId < 0      
      set @UnitPrice = null      
          
    select @ProductAlias = pt.PackType + ' = ' + convert(nvarchar(10), pk.Quantity),      
           @Barcode      = isnull(pk.Barcode, @barcode)      
      from Pack     pk (nolock)      
      join PackType pt (nolock) on pk.PackTypeId = pt.PackTypeId      
     where pk.StorageUnitId = @StorageUnitId      
       and pk.WarehouseId   = @WarehouseId      
       and pk.Quantity      = @Quantity      
          
    if @ProductAlias is null      
      select @ProductAlias = 'Undefined'      
          
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'      
    update #LabelVariables set Value = @SKUCode where Value = '@SKUCode'      
    update #LabelVariables set Value = @Barcode where Value = '@Barcode'      
    update #LabelVariables set Value = @Size where Value = '@Size'      
    update #LabelVariables set Value = @Colour where Value = '@Colour'      
    update #LabelVariables set Value = @Style where Value = '@Style'      
    update #LabelVariables set Value = convert(nvarchar(20), @PickedDate, 120) where Value = '@DateAndTime'      
    update #LabelVariables set Value = convert(nvarchar(10), @Quantity) where Value = '@Quantity'      
    update #LabelVariables set Value = 'R' + convert(nvarchar(16), @UnitPrice) where Value = '@UnitPrice'      
  end      
        
  -- *** / Moresport Ticket.lbl ***      
        
  -- *** Despatch By Route Label.lbl', 'Despatch By Order Label.lbl', 'Despatch By Order Label Plumblink.lbl',       
  -- *** 'Pick Job Label.lbl ***      
        
  if (@JobId is not null) and @Label in ('Despatch By Route Label.lbl',      
           'Despatch By Order Label.lbl',      
           'Despatch By Order Label Plumblink.lbl',      
           'Pick Job Label.lbl')      
  begin      
          
    --if @Label = 'Pick Job Label.lbl'      
    begin      
      update Job      
         set ReferenceNumber = isnull(ReferenceNumber, 'J:' + convert(nvarchar(10), JobId)),      
             Prints = isnull(Prints, 0) + 1      
       where JobId = @JobId      
    end      
          
    select @OutboundShipmentId = min(i.OutboundShipmentId),      
           @IssueLineId        = min(i.IssueLineId),      
           @Weight             = isnull(max(j.Weight),0),      
           @StartDate          = convert(nvarchar(20), min(i.StartDate), 113),      
           @LocationId         = min(i.StoreLocationId),      
           @InstructionId      = min(i.InstructionId),      
           @WarehouseId        = min(i.WarehouseId),      
           @ReferenceNumber    = min(j.ReferenceNumber),      
           --@Quantity           = sum(i.ConfirmedQuantity),      
           @Quantity           = sum(i.CheckQuantity),      
           @DropSequence       = min(isnull(j.DropSequence,1)),      
           @Count              = max(isnull(j.Pallets,1)),      
           @PalletId           = max(i.PalletId),      
           @StatusCode         = min(s.StatusCode),      
           @Prints             = 'Copy ' + convert(nvarchar(20), isnull(min(j.Prints), 1))      
           -- Added by Darren for Plumblink Despatch By Order Label      
          --@PickerId     = j.OperatorId,      
           --@CheckerId     = j.CheckedBy,      
           --@Picker     = (select Operator from Operator where OperatorId = j.OperatorId),      
     --@Checker     = (select Operator from Operator where OperatorId = j.CheckedBy),      
     --@CheckDate     = j.CheckedDate      
           --@Picker     = j.OperatorId,      
           --@Checker     = j.CheckedBy,      
           --@PickDate     = obp.StartDate,      
           --@CheckDate     = obp.Checked      
           -- / Added by Darren for Plumblink Despatch By Order Label      
      from Job         j (nolock)      
      join Instruction i (nolock) on j.JobId = i.JobId      
      join [Status]      s (nolock) on j.StatusId = s.StatusId      
      -- Added by Darren for Plumblink Despatch By Order Label      
      --join OutboundPerformance obp (nolock) on j.JobId = obp.JobId      
      -- / Added by Darren for Plumblink Despatch By Order Label      
     where j.JobId = @JobId      
          
    -- ***      
          
    -- Added by Darren for Plumblink Despatch By Order Label      
          
    select @PickerId    = OperatorId,      
           @CheckerId    = CheckedBy,      
     @CheckDate    = CheckedDate      
 from Job (nolock)      
 where JobId = @JobId      
          
    select @Picker = Operator      
    from Operator      
    where OperatorId = @PickerId      
          
    select @Checker = Operator      
    from Operator      
    where OperatorId = @CheckerId      
          
    select @PickDate = ( select top 1 EndDate      
 from Instruction with(nolock)      
 where JobId = @JobId      
 order by EndDate desc)      
       
 -- Delivery Address      
            
    declare @OutboundLineId int      
          
    select @OutboundLineId = OutboundLineId      
 from IssueLine with(nolock)      
 where IssueLineId = @IssueLineId      
       
 --select @OutboundLineId as OutboundLineId      
       
    select @OutboundDocumentId = OutboundDocumentId      
 from OutboundLine with(nolock)      
 where OutboundLineId = @OutboundLineId      
       
 --select @OutboundDocumentId as OutboundDocumentId      
            
    select @DeliveryAddress = Address1      
 from OutboundDocument      
 where OutboundDocumentId = @OutboundDocumentId      
        
 if (@DeliveryAddress is null)      
  begin      
   select @DeliveryAddress = 'No Address Specified'      
  end      
           
    --select @DeliveryAddress as DeliveryAddress      
            
 -- / Delivery Address      
          
    -- / Added by Darren for Plumblink Despatch By Order Label      
          
    -- ***      
          
    if @OutboundShipmentId IN (0, -1)      
      set @OutboundShipmentId = null      
          
    if @ReferenceNumber is null or @ReferenceNumber like 'J:%'      
      set @ReferenceNumber = 'P:' + convert(nvarchar(10), @PalletId)      
          
    select @Route = convert(nvarchar(10), @OutboundShipmentId) + ' - ' + Route      
      from OutboundShipment      
     where OutboundShipmentId = @OutboundShipmentId      
          
    -- TODO Check If @IssueLineId is null or not      
          
    if @IssueLineId is null      
    begin      
      select @DeliveryDate      = i.DeliveryDate,      
             @RouteId           = i.RouteId,      
             @IssueId           = i.IssueId,      
             @OrderNumber       = od.OrderNumber,      
             @ExternalCompanyId = od.ExternalCompanyId,      
             @WaveId            = i.WaveId      
        from Issue                   i (nolock)      
        join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId      
        join IssueLine              il (nolock) on i.IssueId = il.IssueId      
        join IssueLineInstruction  ili (nolock) on il.IssueLineId = ili.IssueLineId      
        join Instruction           ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId)      
       where ins.InstructionId = @InstructionId      
            
      if dbo.ufn_Configuration(69, @WarehouseId) = 1      
      begin      
        select --@ExternalCompanyId = od.ExternalCompanyId,      
               @DeliveryDate      = i.DeliveryDate,      
               @RouteId           = i.RouteId      
               --@OrderNumber       = od.OrderNumber      
          from Issue             i      
         where i.IssueId = @IssueId      
      end      
    end      
    else      
    begin      
      select @IssueId = IssueId      
        from IssueLine (nolock)      
       where IssueLineId = @IssueLineId      
            
      select @ExternalCompanyId = od.ExternalCompanyId,      
             @DeliveryDate      = i.DeliveryDate,      
             @RouteId           = i.RouteId,      
             @OrderNumber       = od.OrderNumber,      
             @Remarks           = case when i.Remarks like 'Kit job for %'      
                                       then replace(i.Remarks, 'Kit job for ','')      
                                       else null      
                  end,      
             @WaveId            = i.WaveId      
        from OutboundDocument od (nolock)      
        join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId      
       where @IssueId = i.IssueId      
            
      if @Remarks is not null      
         set @OrderNumber = @OrderNumber + '/' + @Remarks      
    end      
          
    if dbo.ufn_Configuration(248, @WarehouseId) = 1      
    begin      
      if @OutboundShipmentId is not null      
        select @Pallet = count(distinct(JobId))      
          from Instruction (nolock)      
         where OutboundShipmentId = @OutboundShipmentId      
           and JobId <= @JobId      
      else      
        select @Pallet = count(distinct(JobId))      
          from Instruction i (nolock)      
          join IssueLine  il (nolock) on i.IssueLineId = il.IssueLineId      
         where il.IssueId = @IssueId      
           and JobId <= @JobId      
            
      --select @Pallet = convert(nvarchar(10), @DropSequence)      
      select @Pallet = convert(nvarchar(10), @Pallet)      
    end      
    else      
    begin      
      if @Count = -1      
        set @Pallet = 'Re-release'      
      else      
        select @Pallet = convert(nvarchar(10), @DropSequence)      
                       + ' of '      
                       + convert(nvarchar(10), @Count)      
    end      
          
    --select top 1 @OrderNumber = od.OrderNumber      
    --  from JobBOMParent    jbp (nolock)      
    --  join BOMInstruction   bi (nolock) on jbp.BOMInstructionId = bi.BOMInstructionId      
    --  join OutboundDocument od (nolock) on bi.OutboundDocumentId = od.OutboundDocumentId      
    -- where jbp.JobId = @JobId      
          
    select @Customer = ExternalCompanyCode + ' ' + substring(ExternalCompany, 1, 30)      
      from ExternalCompany (nolock)      
     where ExternalCompanyId = @ExternalCompanyId      
          
    if @RouteId is not null      
      select @Route = Route      
        from Route (nolock)      
       where RouteId = @RouteId      
          
    --if @WaveId is not null      
    --  select @Wave = Wave       
    --    from Wave (nolock)      
    --   where WaveId = @WaveId      
          
    if dbo.ufn_Configuration(69, @WarehouseId) = 1 and @Label = 'Despatch By Route Label.lbl' AND @OutboundShipmentId IS NULL      
    begin      
      select @Route = @OrderNumber + isnull(' (' + @Route + ')','') + ' - ' + @Customer      
    end      
    else      
    begin      
          
    if @Route is null      
      set @Route = isnull(convert(nvarchar(10), @OutboundShipmentId),isnull(@OrderNumber,''))      
    else if @OutboundShipmentId is not null      
      set @Route = 'Shipment: ' + convert(nvarchar(10), isnull(@OutboundShipmentId,'')) + ' - ' + @Route      
    else if @OrderNumber is not null      
      set @Route = convert(nvarchar(30), isnull(@OrderNumber,'')) + ' - ' + @Route      
    end      
          
    if @WaveId is not null      
      set @Route = convert(nvarchar(10), @WaveId)      
          
    if exists(select top 1 1       
                from Instruction        i (nolock)      
                join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId      
                join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId      
                join Product    p (nolock) on p.ProductId = su.ProductId      
               where p.ProductType in ('LCP','PERLA')      
                 and i.JobId = @JobId)      
      set @LCP = 1      
          
    if @Label = 'Despatch By Order Label.lbl' and @LCP != 1 and @OutboundShipmentId is not null      
      set @OrderNumber = @OrderNumber + '-' + @Route      
    else if @Label = 'Despatch By Order Label Plumblink.lbl'      
		set @OrderNumber = @OrderNumber      
    else      
      set @OrderNumber = @Route      
          
    if @LCP = 1      
      set @Customer = 'LCP / PERLA'      
          
    if @Weight is null or @Weight = 0      
     select @Weight = sum(i.ConfirmedQuantity * p.Weight)      
       from Instruction        i (nolock)      
       join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId      
       join Pack               p (nolock) on sub.StorageUnitId    = p.StorageUnitId      
                                         and i.WarehouseId        = p.WarehouseId      
       join PackType          pt (nolock) on p.PackTypeId         = pt.PackTypeId      
      where pt.OutboundSequence in (select max(OutboundSequence) from PackType)      
        and i.JobId = @JobId      
          
    select @Location = Location      
      from Location (nolock)      
     where LocationId = @LocationId      
          
    if @StatusCode = 'QA'      
    begin      
      set @Location = 'QA'      
    end      
          
    if datalength(convert(varchar(30), @Weight)) > 10      
      set @Weight = null      
          
    -- Picking Slip #      
          
    declare @PickingSlipNumber varchar(10), @PickingSlipNumberLength int      
          
 select @PickSlipNumber = @OrderNumber      
      
 select @PickingSlipNumberLength = LEN(@PickSlipNumber) - 2      
      
 --select @PickingSlipNumber = RIGHT(@OrderNumber, 2)      
      
 if (@Label = 'Despatch By Order Label Plumblink.lbl')      
  begin      
   select @OrderNumber = LEFT(@PickSlipNumber, @PickingSlipNumberLength)      
  end      
      
 --select @OrderNumber as OrderNumber      
            
    -- / Picking Slip #      
          
    --update #LabelVariables set Value = @PickDate where Value = '@PickDate'      
    --update #LabelVariables set Value = @CheckDate where Value = '@CheckDate'      
    update #LabelVariables set Value = convert(nvarchar(10), @PickDate, 120) where Value = '@PickDate'      
    update #LabelVariables set Value = convert(nvarchar(10), @CheckDate, 120) where Value = '@CheckDate'      
    update #LabelVariables set Value = @Picker where Value = '@Picker'      
    update #LabelVariables set Value = @Checker where Value = '@Checker'      
    update #LabelVariables set Value = @PickSlipNumber where Value = '@PickSlipNumber'      
    update #LabelVariables set Value = @DeliveryAddress where Value = '@DeliveryAddress'      
          
    update #LabelVariables set Value = @Prints where Value = '@Prints'      
    update #LabelVariables set Value = @Pallet where Value = '@Pallet'      
    update #LabelVariables set Value = left(@Customer,20) where Value = '@Customer'      
    update #LabelVariables set Value = @Location where Value = '@Location'      
    update #LabelVariables set Value = 'J:' + convert(nvarchar(10), @JobId) where Value = '@JobId'      
    update #LabelVariables set Value = left(@OrderNumber, 20) where Value = '@OrderNumber'      
    update #LabelVariables set Value = @Pallet where Value = '@PickJob'      
    update #LabelVariables set Value = convert(nvarchar(10), @StartDate, 120) where Value = '@StartDate'      
    update #LabelVariables set Value = convert(nvarchar(10), @DeliveryDate, 120) where Value = '@DeliveryDate'      
    update #LabelVariables set Value = left(convert(nvarchar(20), @Weight),5) where Value = '@Weight'      
    --update #LabelVariables set Value = '0' where Value = '@Weight'      
    update #LabelVariables set Value = @Route where Value = '@Route'      
    update #LabelVariables set Value = @ReferenceNumber where Value = '@ReferenceNumber'      
    update #LabelVariables set Value = convert(nvarchar(50), convert(int, @Quantity)) where Value = '@Units'      
  end      
        
 -- *** / Despatch By Route Label.lbl', 'Despatch By Order Label.lbl', 'Despatch By Order Label Plumblink.lbl',       
 -- *** / 'Pick Job Label.lbl' ***      
        
  -- *** Multi Purpose Label.lbl ***      
        
  if @Label = 'Multi Purpose Label.lbl'      
  begin      
    if @Title = 'Reference Label'      
      set @Barcode = 'R:' + @Barcode      
          
    if @Title = 'Pallet Label'      
      set @Barcode = 'P:' + @Barcode      
          
    if @barcode like 'J:%' and isnumeric(replace(@barcode,'J:','')) = 1      
    begin      
      set @JobId = replace(@barcode,'J:','')      
            
    exec p_Instruction_Print @JobId = @JobId      
    end      
          
    update #LabelVariables set Value = @Barcode where Value = '@Barcode'      
    update #LabelVariables set Value = @Title where Value = '@Title'      
  end      
        
  -- / *** Multi Purpose Label.lbl ***      
        
  -- *** Product Label Small.lbl ***      
        
  if @Label = 'Product Label Small.lbl'      
  begin      
    update #LabelVariables set Value = @Barcode where Value = '@ProductCode'      
    --update #LabelVariables set Value = replicate(' ',50 - datalength(@Title)) + @Title where Value = '@Product'      
    update #LabelVariables set Value = '     ' + @Title + '     ' where Value = '@Product'      
    --update #LabelVariables set Value = @Title where Value = '@Product'      
    --update #LabelVariables set Value = substring(@Title,1,3) where Value = '@Product'      
  end      
        
  -- *** / Product Label Small.lbl ***      
        
  -- *** Pickface Label Small.lbl','Pickface Label Large.lbl','Second Level Label Small.lbl','Second Level Label Large.lbl','Second Level Plain Label Small.lbl','Second Level Plain Label Large.lbl',      
  -- *** 'Left Location Label Small.lbl','Left Location Label Large.lbl','Right Location Label Small.lbl','Right Location Label Large.lbl',      
  -- *** 'Right Location Label Small.lbl','Right Location Label Large.lbl','Location Label Small.lbl','Location Label Large.lbl', 'Location Up Label Small.lbl', 'Location Down Label Small.lbl      
        
  if @Label in ('Pickface Label Small.lbl','Pickface Label Large.lbl','Second Level Label Small.lbl','Second Level Label Large.lbl','Second Level Plain Label Small.lbl','Second Level Plain Label Large.lbl',      
                'Left Location Label Small.lbl','Left Location Label Large.lbl','Right Location Label Small.lbl','Right Location Label Large.lbl',      
                'Right Location Label Small.lbl','Right Location Label Large.lbl','Location Label Small.lbl','Location Label Large.lbl', 'Location Up Label Small.lbl', 'Location Down Label Small.lbl')      
  begin      
        
 -- *** Location Label Small.lbl','Second Level Label Small.lbl ***      
      
 if @Label in ('Location Label Small.lbl','Second Level Label Small.lbl')      
   set @Title = SUBSTRING(@Title,3, 198)      
      
 -- *** Location Label Small.lbl','Second Level Label Small.lbl ***      
      
 update #LabelVariables set Value = @Title where Value = '@Location'      
 update #LabelVariables set Value = right(@Barcode,6) where Value = '@Level'      
      
 if (select dbo.ufn_Configuration(217, 1)) = 1 -- NB - Warehouse Parameter Defaulted to 1      
  begin      
     set @SecurityCode = @Title      
  end      
 else      
  begin      
    select @SecurityCode = SecurityCode      
   from Location (nolock)      
     where Location = @Title      
  end      
          
 update #LabelVariables set Value = @SecurityCode where Value = '@SecurityCode'      
      
 end      
      
  -- *** / 'Pickface Label Small.lbl','Pickface Label Large.lbl','Second Level Label Small.lbl','Second Level Label Large.lbl','Second Level Plain Label Small.lbl','Second Level Plain Label Large.lbl',      
  -- *** / 'Left Location Label Small.lbl','Left Location Label Large.lbl','Right Location Label Small.lbl','Right Location Label Large.lbl',      
  -- *** / 'Right Location Label Small.lbl','Right Location Label Large.lbl','Location Label Small.lbl','Location Label Large.lbl', 'Location Up Label Small.lbl', 'Location Down Label Small.lbl'      
        
  -- *** Racking Label Small.lbl','Racking Label Large.lbl ***      
        
  if @Label in ('Racking Label Small.lbl','Racking Label Large.lbl')      
  begin      
    update #LabelVariables set Value = right(@Title,1) where Value = '@Level'      
  end      
        
  -- *** / Racking Label Small.lbl','Racking Label Large.lbl ***      
        
  -- *** Security Code Label Small.lbl','Security Code Label Large.lbl ***      
        
  if @Label in ('Security Code Label Small.lbl','Security Code Label Large.lbl')      
  begin      
    update #LabelVariables set Value = @Title where Value = '@SecurityCode'      
  end      
        
  -- *** / Security Code Label Small.lbl','Security Code Label Large.lbl ***      
        
  -- *** Product Bin Down Label Small.lbl','Product Bin Down Label Large.lbl','Product Bin Up Label Small.lbl',      
  -- *** 'Product Bin Up Label Large.lbl ***      
        
  if @Label in ('Product Bin Down Label Small.lbl','Product Bin Down Label Large.lbl','Product Bin Up Label Small.lbl','Product Bin Up Label Large.lbl')      
  begin      
    select @LocationId = l.LocationId      
      from Location      l (nolock)      
      join AreaLocation al (nolock) on l.LocationId = al.LocationId      
      join Area          a (nolock) on al.AreaId = a.AreaId      
     where Location = @Title      
       and WarehouseId = @WarehouseId      
          
    select @Location     = l.Location,      
           @SecurityCode = l.SecurityCode,      
           @Level        = l.Level      
      from Location l (nolock)      
     where LocationId = @LocationId      
          
    select @ProductCode = p.ProductCode,      
           @Product     = p.Product,      
           @Barcode     = p.ProductCode,      
           @SKUCode     = sku.SKUCode      
      from StorageUnitLocation sul (nolock)      
      join StorageUnit          su (nolock) on sul.StorageUnitId = su.StorageUnitId      
      join Product               p (nolock) on su.ProductId = p.ProductId      
      join SKU                 sku (nolock) on su.SKUId = sku.SKUId      
     where sul.LocationId = @LocationId      
          
    update #LabelVariables set Value = @Location     where Value = '@Location'      
    update #LabelVariables set Value = @SecurityCode where Value = '@SecurityCode'      
    update #LabelVariables set Value = left(@Level,1)        where Value = '@Level'      
    update #LabelVariables set Value = @ProductCode  where Value = '@ProductCode'      
    update #LabelVariables set Value = @Product      where Value = '@Product'      
    update #LabelVariables set Value = @Barcode      where Value = '@Barcode'      
    update #LabelVariables set Value = @SKUCode      where Value = '@SKUCode'      
  end      
        
  -- *** / Product Bin Down Label Small.lbl','Product Bin Down Label Large.lbl','Product Bin Up Label Small.lbl',      
  -- *** / 'Product Bin Up Label Large.lbl ***      
        
  -- *** Sample Label Small.lbl','Sample Label Large.lbl','Product Batch Label Small.lbl',      
  -- *** 'Product Batch Label Large.lbl','Batch Label Small.lbl','Batch Label Large.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Sample Label Small.lbl','Sample Label Large.lbl','Product Batch Label Small.lbl','Product Batch Label Large.lbl','Batch Label Small.lbl','Batch Label Large.lbl')      
  begin      
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @KeyId      
          
    if (select dbo.ufn_Configuration(178, 1)) = 1 -- NB - Warehouse Parameter Defaulted to 1      
    begin      
      select @StorageUnitId = sub.StorageUnitId,      
             @Batch         = b.Batch,      
             @Prints        = 'Copy ' + convert(nvarchar(20), isnull(b.Prints, 1)),      
             @StartDate     = b.CreateDate,      
             @Product       = p.Product,      
             @ProductAlias  = isnull(suec.ProductCode,''),      
             @ProductCode   = p.ProductCode,      
             @StatusCode    = s.StatusCode      
        from ReceiptLine       rl (nolock)      
        join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId      
        join Batch              b (nolock) on sub.BatchId           = b.BatchId      
        join Status             s (nolock) on b.StatusId            = s.StatusId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
        left      
        join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId      
       where rl.ReceiptLineId = @KeyId      
    end      
    else      
    begin      
      select @StorageUnitId = sub.StorageUnitId,      
             @Batch         = b.Batch,      
             @Prints        = 'Copy ' + convert(nvarchar(20), isnull(b.Prints, 1)),      
             @StartDate     = b.CreateDate,      
             @Product       = p.Product,      
             @ProductAlias  = isnull(suec.ProductCode,''),      
             @ProductCode   = p.ProductCode,      
             @StatusCode    = s.StatusCode      
        from Batch              b (nolock)      
        join Status             s (nolock) on b.StatusId            = s.StatusId      
        join StorageUnitBatch sub (nolock) on b.BatchId             = sub.BatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
        left      
        join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId      
       where b.BatchId = @KeyId      
    end      
          
    if @Location is null      
    begin      
      set @Location = null      
            
      select top 1 @Location = a.AreaCode + '-'      
        from AreaLocation al (nolock)      
        join Area          a (nolock) on al.AreaId = a.AreaId      
        join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId      
       where sua.StorageUnitId = @StorageUnitId      
      order by StoreOrder      
            
      select top 1 @Location = isnull(@Location,'') + isnull(l.Ailse,l.Location)      
        from StorageUnitLocation sul (nolock)      
        join Location              l (nolock) on sul.LocationId = l.LocationId      
       where sul.StorageUnitId = @StorageUnitId      
            
      if @Location is null      
        set @Location = 'NOT SETUP'      
    end      
          
    select @SKUCode  = pt.PackType,      
           @Quantity = p.Quantity      
      from Pack               p (nolock)      
      join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId      
    where pt.InboundSequence > 1      
      and p.Quantity         > 1      
      and p.StorageUnitId    = @StorageUnitId      
          
    if dbo.ufn_Configuration(348, @WarehouseId) = 1      
    begin      
      update #LabelVariables set Value = @ProductCode where Value = '@Status'      
      update #LabelVariables set Value = 'PRODUCT' where Value = '@Title'      
    end      
    else      
    begin      
      update #LabelVariables set Value = @StatusCode + ':' + @Batch where Value = '@Batch'      
            
      if @StatusCode = 'A'      
      begin      
        update #LabelVariables set Value = 'RELEASED' where Value = '@Status'      
        update #LabelVariables set Value = 'PRODUCT' where Value = '@Title'      
      end      
      else      
      begin      
        update #LabelVariables set Value = 'QUARANTINE' where Value = '@Status'      
        update #LabelVariables set Value = 'RECEIVING' where Value = '@Title'      
      end      
    end      
          
    update #LabelVariables set Value = @Batch where Value = '@Batch'      
    update #LabelVariables set Value = @Prints where Value = '@Copy'      
    update #LabelVariables set Value = convert(nvarchar(16), @StartDate, 120) where Value = '@DateAndTime'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'      
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'    
    update #LabelVariables set Value = @Location where Value = '@Aisle'      
    update #LabelVariables set Value = @SKUCode where Value = '@SKUCode'      
    update #LabelVariables set Value = @Quantity where Value = '@Quantity'      
  end      
        
  -- *** / Sample Label Small.lbl','Sample Label Large.lbl','Product Batch Label Small.lbl',      
  -- *** / 'Product Batch Label Large.lbl','Batch Label Small.lbl','Batch Label Large.lbl ***      
        
  -- *** Picking Label Small.lbl','Picking Label Large.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Picking Label Small.lbl','Picking Label Large.lbl')      
  begin      
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @KeyId      
          
    select @InstructionId = isnull(i.InstructionRefId, i.InstructionId),      
           @barcode      = j.ReferenceNumber,      
           @Batch        = b.Batch,      
           @Prints       = 'Copy ' + convert(nvarchar(20), isnull(b.Prints, 1)),      
           @StartDate    = b.CreateDate,      
           @Product      = p.Product,      
           @ProductAlias = isnull(suec.ProductCode,''),      
  @ProductCode  = p.ProductCode,      
           @StatusCode   = s.StatusCode      
      from Job                j (nolock)      
      join Instruction        i (nolock) on j.JobId               = i.JobId      
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId  = sub.StorageUnitBatchId      
      join Batch              b (nolock) on sub.BatchId           = b.BatchId      
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
      join Status             s (nolock) on b.StatusId            = s.StatusId      
      join Product            p (nolock) on su.ProductId          = p.ProductId      
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
      left      
      join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId      
     where j.JobId = @KeyId      
          
    if @barcode is null      
    begin      
      select @barcode = od.OrderNumber,      
             @OutboundDocumentId = od.OutboundDocumentId      
        from IssueLineInstruction ili (nolock)      
        join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId      
       where ili.InstructionId = @InstructionId      
            
      select @Count = count(1),      
             @JobId = min(i.JobId)      
        from IssueLineInstruction ili (nolock)      
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId      
       where ili.OutboundDocumentId = @OutboundDocumentId      
            
      select @barcode = 'R:' + @barcode +':'+ convert(nvarchar(10), (@KeyId - @JobId + 1)) + 'of' + convert(nvarchar(10), @Count)      
            
      update Job      
         set ReferenceNumber = @barcode      
       where JobId = @KeyId        
    end      
          
    if dbo.ufn_Configuration(348, @WarehouseId) = 1      
    begin      
      update #LabelVariables set Value = @ProductCode where Value = '@Status'      
      update #LabelVariables set Value = 'PRODUCT' where Value = '@Title'      
    end      
    else      
    begin      
      update #LabelVariables set Value = @StatusCode + ':' + @Batch where Value = '@Batch'      
            
      if @StatusCode = 'A'      
      begin      
        update #LabelVariables set Value = 'RELEASED' where Value = '@Status'      
        update #LabelVariables set Value = 'PRODUCT' where Value = '@Title'      
      end      
      else      
      begin      
        update #LabelVariables set Value = 'QUARANTINE' where Value = '@Status'      
        update #LabelVariables set Value = 'RECEIVING' where Value = '@Title'      
      end      
    end      
          
    update #LabelVariables set Value = @Batch where Value = '@Batch'      
    update #LabelVariables set Value = @Prints where Value = '@Copy'      
    update #LabelVariables set Value = convert(nvarchar(16), @StartDate, 120) where Value = '@DateAndTime'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'      
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = isnull(@barcode,'') where Value = '@ReferenceNumber'      
  end      
      
    if (@KeyId is not null) and @Label in ('Picking Job Label Small.lbl')      
  begin      
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @KeyId      
          
    select @InstructionId = isnull(i.InstructionRefId, i.InstructionId),      
           @barcode          = 'J:' + CONVERT(nvarchar(20),j.JobId),    
           @Location         = 'Area: ' + a.Area      
      from Job                j (nolock)      
      join Instruction        i (nolock) on j.JobId               = i.JobId        
      left      
      join Location                    l (nolock) on i.PickLocationId        = l.LocationId    
      join LocationType               lt (nolock) on l.LocationTypeId        = lt.LocationTypeId      
      join AreaLocation               al (nolock) on l.LocationId            = al.LocationId      
      join Area                        a (nolock) on al.AreaId               = a.AreaId     
     where j.JobId = @KeyId  
     
   if @barcode is not null      
    begin      
      select @OrderNumber = 'Order Number: ' + od.OrderNumber    
        from IssueLineInstruction ili (nolock)      
        join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId      
       where ili.InstructionId = @InstructionId
       end     
          
    update #LabelVariables set Value = @Location where Value = '@Location'       
    update #LabelVariables set Value = isnull(@barcode,'') where Value = '@JobNumber'
    update #LabelVariables set Value = isnull(@OrderNumber,'') where Value = '@OrderNumber'
  end      
        
  -- *** / Picking Label Small.lbl','Picking Label Large.lbl ***      
        
  -- *** Verify Batch Label Large Version 2.lbl','Verify Batch Label Small.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Verify Batch Label Large Version 2.lbl','Verify Batch Label Small.lbl')      
  begin      
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @KeyId      
          
    select @Batch        = b.Batch,      
           @Prints       = 'Copy ' + convert(nvarchar(20), isnull(b.Prints, 1)),      
           @StartDate    = b.CreateDate,      
           @ExpiryDate   = b.ExpiryDate,      
           @Product      = p.Product,      
           @ProductAlias = isnull(suec.ProductCode,''),      
           @ProductCode  = p.ProductCode,      
           @StatusCode   = s.StatusCode,      
           @barcode      = p.ProductCode      
      from Batch              b (nolock)      
      join Status             s (nolock) on b.StatusId            = s.StatusId      
      join StorageUnitBatch sub (nolock) on b.BatchId             = sub.BatchId      
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
 join Product            p (nolock) on su.ProductId          = p.ProductId      
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
      left      
      join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId      
     where sub.StorageUnitBatchId = @KeyId      
          
    select @Quantity = ActualQuantity      
      from StorageUnitBatchLocation      
     where StorageUnitBatchId = @KeyId      
       and LocationId = @LocationId      
          
    if dbo.ufn_Configuration(348, @WarehouseId) = 1      
    begin      
      update #LabelVariables set Value = @ProductCode where Value = '@Status'      
      update #LabelVariables set Value = 'PRODUCT' where Value = '@Title'      
    end      
    else      
    begin      
      update #LabelVariables set Value = @StatusCode + ':' + @Batch where Value = '@Batch'      
            
      if @StatusCode = 'A'      
      begin      
        update #LabelVariables set Value = 'RELEASED' where Value = '@Status'      
        update #LabelVariables set Value = 'PRODUCT' where Value = '@Title'      
      end      
      else      
      begin      
        update #LabelVariables set Value = 'QUARANTINE' where Value = '@Status'      
        update #LabelVariables set Value = 'RECEIVING' where Value = '@Title'      
      end      
    end      
          
    update #LabelVariables set Value = @Batch where Value = '@Batch'      
    update #LabelVariables set Value = @Prints where Value = '@Copy'      
    update #LabelVariables set Value = convert(nvarchar(16), @StartDate, 120) where Value = '@DateAndTime'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'      
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = convert(int, @Quantity) where Value = '@Quantity'      
    update #LabelVariables set Value = isnull(convert(nvarchar(10), @ExpiryDate, 120),'None') where Value = '@ExpiryDate'      
    update #LabelVariables set Value = @barcode where Value = '@barcode'      
  end      
        
  -- *** / Verify Batch Label Large Version 2.lbl','Verify Batch Label Small.lbl ***      
        
  -- *** Receiving Batch Label Large.lbl','Receiving Batch Label Small.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Receiving Batch Label Large.lbl','Receiving Batch Label Small.lbl')      
  begin      
    select @BatchId            = b.BatchId,      
           @Batch              = b.Batch,      
           @Prints             = 'Copy ' + convert(nvarchar(20), isnull(b.Prints, 1)),      
           @StartDate          = b.CreateDate,      
           @ExpiryDate         = b.ExpiryDate,      
           @Product            = p.Product,      
           @ProductAlias       = isnull(suec.ProductCode,''),      
           @ProductCode        = p.ProductCode,      
           @StatusCode         = s.StatusCode,      
           @Quantity           = rl.AcceptedQuantity,      
           @barcode            = p.ProductCode,      
           @StorageUnitBatchId = sub.StorageUnitBatchId      
      from ReceiptLine       rl (nolock)      
      join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId      
      join Batch              b (nolock) on sub.BatchId           = b.BatchId      
      join Status             s (nolock) on b.StatusId            = s.StatusId      
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
      join Product            p (nolock) on su.ProductId          = p.ProductId      
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
      left      
      join StorageUnitExternalCompany suec (nolock) on su.StorageUnitId    = suec.StorageUnitId      
     where rl.ReceiptLineId = @KeyId      
          
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @BatchId      
          
    if dbo.ufn_Configuration(348, @WarehouseId) = 1      
    begin      
      update #LabelVariables set Value = @ProductCode where Value = '@Status'      
      update #LabelVariables set Value = 'PRODUCT' where Value = '@Title'      
    end      
    else      
    begin      
      update #LabelVariables set Value = @StatusCode + ':' + @Batch where Value = '@Batch'      
            
      if @StatusCode = 'A'      
      begin      
        update #LabelVariables set Value = 'RELEASED' where Value = '@Status'      
        update #LabelVariables set Value = 'PRODUCT' where Value = '@Title'      
      end      
      else      
      begin      
        update #LabelVariables set Value = 'QUARANTINE' where Value = '@Status'      
        update #LabelVariables set Value = 'RECEIVING' where Value = '@Title'      
      end      
    end      
          
    if dbo.ufn_Configuration(317, @WarehouseId) = 1      
    begin      
      exec @Error = p_Pallet_Insert      
       @PalletId           = @Pallet output,      
       @StorageUnitBatchId = @StorageUnitBatchId,      
       @LocationId         = @LocationId,      
       @Weight             = null,      
       @Tare               = null,      
       @Quantity           = @Quantity,     
       @Prints             = 1      
            
      if @Error <> 0      
        set @barcode = @ProductCode      
      else      
        set @barcode = 'P:' + convert(varchar(10), @Pallet)      
            
      set @ValueLabel = 'Weight:'      
      set @Quantity = null      
    end      
          
    update #LabelVariables set Value = @Batch where Value = '@Batch'      
    update #LabelVariables set Value = @Prints where Value = '@Copy'      
    update #LabelVariables set Value = convert(nvarchar(16), @StartDate, 120) where Value = '@DateAndTime'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'      
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = convert(int, @Quantity) where Value = '@Quantity'      
    update #LabelVariables set Value = isnull(convert(nvarchar(10), @ExpiryDate, 120),'None') where Value = '@ExpiryDate'      
    update #LabelVariables set Value = @barcode where Value = '@barcode'      
    update #LabelVariables set Value = @ValueLabel where Value = '@ValueLabel'      
  end      
        
  -- *** / Receiving Batch Label Large.lbl','Receiving Batch Label Small.lbl ***      
        
  -- *** Customer Product Label Small.lbl','EAN Product Label Small.lbl','NOTEAN Product Label Small.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Customer Product Label Small.lbl','EAN Product Label Small.lbl','NOTEAN Product Label Small.lbl')      
  begin      
    if @KeyId < 0      
      select @Product            = p.Product,      
             @ProductCode        = p.ProductCode,      
             @Customer           = p.Description3,      
             @barcode            = isnull(p.Barcode,'')      
             --@StorageUnitId      = su.StorageUnitId,      
             --@ExternalCompanyId  = suec.ExternalCompanyId      
        from StorageUnit       su (nolock)      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        --left      
        --join StorageUnitExternalCompany suec (nolock) on su.StorageUnitId    = suec.StorageUnitId      
       where su.StorageUnitId = @KeyId*-1      
    else      
      select @Product            = p.Product,      
             @ProductCode        = p.ProductCode,      
             @Customer           = p.Description3,      
             @barcode            = isnull(p.Barcode,'')      
        from ReceiptLine       rl (nolock)      
        join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
       where rl.ReceiptLineId = @KeyId      
             --@StorageUnitId      = su.StorageUnitId,      
             --@InboundDocumentId  = r.InboundDocumentId,      
             --@ExternalCompanyId  = suec.ExternalCompanyId      
       -- from Receipt            r (nolock)      
       -- join ReceiptLine       rl (nolock) on r.ReceiptId           = rl.ReceiptId      
       -- join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId      
       -- join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
       -- join Product            p (nolock) on su.ProductId          = p.ProductId      
       -- left      
       -- join StorageUnitExternalCompany suec (nolock) on su.StorageUnitId    = suec.StorageUnitId      
       --where rl.ReceiptLineId = @KeyId      
          
    --if @ExternalCompanyId is null      
    --  select @ExternalCompanyId = ExternalCompanyId      
    --    from InboundDocument (nolock)      
    --   where InboundDocumentId = @InboundDocumentId      
          
    --select @Customer = ExternalCompany      
    --  from ExternalCompany      
    -- where ExternalCompanyId = @ExternalCompanyId      
          
    if @barcode = '' and @Label != 'EAN Product Label Small.lbl'      
      set @barcode = @ProductCode      
          
    if @Customer is null      
      set @Customer = ''      
          
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = @barcode where Value = '@Barcode'      
    update #LabelVariables set Value = @Customer where Value = '@Customer'      
    update #LabelVariables set Value = convert(nvarchar(16), @GetDate, 120) where Value = '@DateAndTime'      
  end      
        
  -- *** / Customer Product Label Small.lbl','EAN Product Label Small.lbl','NOTEAN Product Label Small.lbl ***      
        
  -- *** UTI Product Label Large.lbl','UTI Product Label Small.lbl ***      
        
  if (@KeyId is not null) and @Label in ('UTI Product Label Large.lbl','UTI Product Label Small.lbl')      
  begin      
    if @KeyId < 0      
      select @Product            = p.Product,      
             @ProductCode        = p.ProductCode,      
             @Customer           = p.Description3,      
             @barcode            = isnull(p.Barcode,'')      
             --@StorageUnitId      = su.StorageUnitId,      
             --@ExternalCompanyId  = suec.ExternalCompanyId      
        from StorageUnit       su (nolock)      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        --left      
        --join StorageUnitExternalCompany suec (nolock) on su.StorageUnitId    = suec.StorageUnitId      
       where su.StorageUnitId = @KeyId*-1      
    else      
      select @StorageUnitId      = su.StorageUnitId,      
             @Product            = p.Product,      
             @ProductCode        = p.ProductCode,      
             @SKUCode            = sku.SKUCode,      
             @Customer           = p.Description3,      
             @barcode            = p.Barcode,      
             @Batch              = case when idt.InboundDocumentTypeCode = 'BOND'      
                                        then b.BatchReferenceNumber      
                                        else b.Batch      
                                        end,      
             @ExpiryDate         = b.ExpiryDate,      
             @BatchReference     = case when idt.InboundDocumentTypeCode = 'BOND'      
                                        then b.Batch      
                                        else b.BatchReferenceNumber      
                                        end,      
             @Quantity           = rl.AcceptedQuantity,      
             @StorageUnitId      = su.StorageUnitId,      
             @ReferenceNumber    = r.AdditionalText1,      
             @Weight             = rl.AcceptedWeight,      
             @BOE                = r.BOE,      
             @BOL                = il.BOELineNumber,      
             @ContainerNumber    = r.ContainerNumber      
        from ReceiptLine       rl (nolock)      
        join InboundLine       il (nolock) on rl.InboundLineId      = il.InboundLineId      
        join Receipt            r (nolock) on rl.ReceiptId       = r.ReceiptId      
        join InboundDocument   id (nolock) on r.InboundDocumentId   = id.InboundDocumentId      
        join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId      
        join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId      
        join Batch              b (nolock) on sub.BatchId           = b.BatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
       where rl.ReceiptLineId = @KeyId      
          
    if isnull(@barcode,'') = ''      
      set @Barcode = @ProductCode      
          
    declare @Length   nvarchar(20),      
            @Width    nvarchar(20),      
            @Height   nvarchar(20),      
            @DIMS     nvarchar(60)      
          
    select @Length   = convert(nvarchar(20), [Length]),      
           @Width    = convert(nvarchar(20), [Width]),      
           @Height   = convert(nvarchar(20), [Height])      
      from Pack      p (nolock)      
      join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId      
     where p.StorageUnitId = @StorageUnitId      
       and p.WarehouseId   = @WarehouseId      
          
    select @DIMS = @Length + 'X' + @Width + 'X' + @Height      
    select @BOE = isnull(@BOE,'') + isnull(@BOL,'')      
          
    if @ContainerNumber is null set @ContainerNumber = 'None'      
    if @BOE is null set @BOE = 'None'      
    if @DIMS is null set @DIMS = 'None'      
    if @ReferenceNumber is null set @ReferenceNumber = 'None'      
    if @SerialNumber is null set @SerialNumber = 'None'      
    if @Weight is null set @Weight = 0.00      
          
    update #LabelVariables set Value = @Product          where Value = '@Product'      
    update #LabelVariables set Value = @ProductCode      where Value = '@ProductCode'      
    update #LabelVariables set Value = @SKUCode          where Value = '@SKUCode'      
    update #LabelVariables set Value = @barcode          where Value = '@Barcode'      
    update #LabelVariables set Value = @Batch            where Value = '@Batch'      
    update #LabelVariables set Value = convert(nvarchar(16), @ExpiryDate, 120) where Value = '@ExpiryDate'      
    update #LabelVariables set Value = @BatchReference   where Value = '@BatchReference'      
    update #LabelVariables set Value = @ReferenceNumber  where Value = '@ReferenceNumber'      
    update #LabelVariables set Value = @ContainerNumber  where Value = '@ContainerNumber'      
    update #LabelVariables set Value = @BOE              where Value = '@BOE'      
    update #LabelVariables set Value = @BOL              where Value = '@BOL'      
    update #LabelVariables set Value = @Weight           where Value = '@Weight'      
    update #LabelVariables set Value = @DIMS             where Value = '@DIMS'      
    update #LabelVariables set Value = @Quantity         where Value = '@Quantity'      
    update #LabelVariables set Value = @SerialNumber     where Value = '@SerialNumber'      
          
    update #LabelVariables set Value = convert(nvarchar(16), @GetDate, 120) where Value = '@DateAndTime'      
  end      
        
  -- *** / UTI Product Label Large.lbl','UTI Product Label Small.lbl ***      
        
  -- *** Product Division Label Small.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Product Division Label Small.lbl')      
  begin      
    if @KeyId < 0      
      select @Product            = p.Product,      
             @ProductCode        = p.ProductCode,      
             @barcode            = isnull(p.Barcode,'')      
        from StorageUnit       su (nolock)      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
       where su.StorageUnitId = @KeyId*-1      
    else      
      select @Product            = p.Product,      
             @ProductCode        = p.ProductCode,      
             @barcode            = isnull(p.Barcode,'')      
        from ReceiptLine       rl (nolock)      
        join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId      
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
        join Product            p (nolock) on su.ProductId          = p.ProductId      
       where rl.ReceiptLineId = @KeyId      
            
      select @Division = d.Division,      
             @NetworkNumber = id.ReferenceNumber      
        from InboundDocument id      
        join InboundLine     il on id.InboundDocumentId = il.InboundDocumentId      
        join StorageUnit     su on il.StorageUnitId = su.StorageUnitId      
        join Product          p on su.ProductId = p.ProductId      
        left      
        join Division         d on id.DivisionId = d.DivisionId      
       where p.ProductCode = @ProductCode      
          
    if @Division is null      
      set @Division = ''      
          
    if @NetworkNumber is null      
      set @NetworkNumber = ''      
          
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = @barcode where Value = '@Barcode'      
    update #LabelVariables set Value = @Division where Value = '@Division'      
    update #LabelVariables set Value = @NetworkNumber where Value = '@NetworkNumber'      
  end      
        
  -- *** / Product Division Label Small.lbl ***      
        
  -- *** Sign In Card.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Sign In Card.lbl')      
  begin      
    select @OperatorGroup = og.OperatorGroup,      
           @Operator      = o.Operator,      
           @Password      = o.Password      
      from Operator       o (nolock)      
      join OperatorGroup og (nolock)on o.OperatorGroupId = og.OperatorGroupId      
     where o.OperatorId = @KeyId      
          
    update #LabelVariables set Value = @OperatorGroup where Value = '@OperatorGroup'      
    update #LabelVariables set Value = @Operator where Value = '@Operator'      
    update #LabelVariables set Value = @Password where Value = '@Password'      
  end      
        
  -- *** / Sign In Card.lbl ***      
        
  -- *** Sensory FX Label Large.lbl ***      
        
  if (@KeyId is not null) and @Label in ('Sensory FX Label Large.lbl')      
  begin      
    select top 1      
           --@instructionId = i.InstructionId,      
           @WarehouseId = isnull(b.WarehouseId,1),      
           @ProductCode = p.ProductCode,      
           @Product     = p.Product,      
           @SKUCode     = sku.SKUCode,      
           @Batch       = b.Batch,      
           @ExpiryDate  = b.ExpiryDate,      
           --@Quantity    = i.Quantity,      
           @StorageUnitId = su.StorageUnitId      
           --@PickedDate    = i.EndDate      
      from StorageUnitBatch sub (nolock)      
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId      
      join Product            p (nolock) on su.ProductId          = p.ProductId      
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId      
      join Batch              b (nolock) on sub.BatchId           = b.BatchId      
     where b.BatchId = @KeyId      
          
    select @OutboundDocumentId = OutboundDocumentId      
      from IssueLineInstruction (nolock)      
     where InstructionId = @instructionId      
          
    select @ExternalCompanyId = ec.ExternalCompanyId,      
           @Customer          = ec.ExternalCompany,      
           @CustomerCode      = ec.ExternalCompanyCode,      
           @CustomerAddress   = isnull(a.Street,'_') + ' ' + isnull(a.Suburb,'') + ' ' + isnull(a.Town,''),      
           @CustomerContact   = cl.ContactPerson      
      from OutboundDocument od (nolock)      
      join ExternalCompany  ec (nolock) on od.OutboundDocumentId = ec.ExternalCompanyId      
      left      
      join Address           a (nolock) on ec.ExternalCompanyId = a.ExternalCompanyId      
      left      
      join ContactList      cl (nolock) on ec.ExternalCompanyId = cl.ExternalCompanyId      
     where od.OutboundDocumentId = @OutboundDocumentId      
          
    select @PhysicalAddress   = isnull(a.Street,'_') + ' ' + isnull(a.Suburb,'') + ' ' + isnull(a.Town,''),      
           @PostalAddress     = isnull(pa.Street,'_') + ' ' + isnull(pa.Suburb,'') + ' ' + isnull(pa.Town,''),      
           @Email             = isnull(cl.EMail, 'info@sensoryfx.co.za'),      
           @Fax               = isnull(cl.Fax,''),      
           @TelephoneNumber   = isnull(cl.Telephone, ''),      
           @CustomsCode       = c.CustomsCode,      
           @RegNo             = c.Registration      
      from Warehouse         w (nolock)      
      join Company           c (nolock) on w.CompanyId = c.CompanyId      
      left      
      join Address           a (nolock) on w.AddressId = a.AddressId      
      left      
      join Address          pa (nolock) on w.PostalAddressId = pa.AddressId      
      left      
      join ContactList      cl (nolock) on w.ContactListId = cl.ContactListId      
     where w.WarehouseId = @WarehouseId      
          
    update #LabelVariables set Value = @ProductCode where Value = '@ProductCode'      
    update #LabelVariables set Value = @Product where Value = '@Product'      
    update #LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'      
    update #LabelVariables set Value = @Batch where Value = '@Batch'      
    update #LabelVariables set Value = convert(nvarchar(10), @ExpiryDate, 120) where Value = '@ExpiryDate'      
    update #LabelVariables set Value = convert(nvarchar(10), @PickedDate, 120) where Value = '@PickedDate'      
    update #LabelVariables set Value = convert(nvarchar(20), convert(numeric(13), @Quantity)) where Value = '@Quantity'      
    update #LabelVariables set Value = 'KG' where Value = '@SKUCode'      
          
    update #LabelVariables set Value = @Customer where Value = '@Customer'      
    update #LabelVariables set Value = @CustomerAddress where Value = '@CustomerAddress'      
    update #LabelVariables set Value = @CustomsCode where Value = '@CustomsCode'      
    update #LabelVariables set Value = @CustomerContact where Value = '@CustomerContact'      
    update #LabelVariables set Value = @CustomerTelephone where Value = '@CustomerTelephone'      
    update #LabelVariables set Value = @Email where Value = '@Email'      
          
    update #LabelVariables set Value = @Fax where Value = '@Fax'      
    update #LabelVariables set Value = @Instructions where Value = '@Instructions'      
    update #LabelVariables set Value = @PhysicalAddress where Value = '@PhysicalAddress'      
    update #LabelVariables set Value = @PostalAddress where Value = '@PostalAddress'      
    update #LabelVariables set Value = @RegNo where Value = '@RegNo'      
    update #LabelVariables set Value = @TelephoneNumber where Value = '@TelephoneNumber'      
  end      
        
  -- *** / Sensory FX Label Large.lbl ***      
        
  SELECT '"' + Name + '": "' + Value + '",' AS Variables
	FROM #LabelVariables      
end 