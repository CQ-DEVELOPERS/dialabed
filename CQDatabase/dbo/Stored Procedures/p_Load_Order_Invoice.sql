﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Load_Order_Invoice
  ///   Filename       : p_Load_Order_Invoice.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Dec 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Load_Order_Invoice
(
 @outboundShipmentId  int,
 @issueId             int
)
 
as
begin
  set nocount on;
  
  declare @DesktopLogId int
  
  insert DesktopLog
        (ProcName,
         OutboundShipmentId,
         IssueId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @outboundShipmentId,
         @issueId,
         getdate()
  
  select @DesktopLogId = scope_identity()
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @Rowcount              int,
          @NewOutboundShipmentId int,
          @NewIssueId            int,
          @IssueLineId           int,
          @NewIssueLineId        int,
          @Count                 int,
          @InstructionTypeCode   nvarchar(10)
  
  declare @instructions as table
  (
   WarehouseId           int,
   PriorityId            int,
   OutboundShipmentId    int,
   OutboundDocumentId    int,
   IssueId               int,
   IssueLineId           int,
   NewOutboundShipmentId int,
   NewIssueId            int,
   NewIssueLineId        int,
   JobId                 int,
   InstructionTypeCode   nvarchar(10),
   InstructionId         int,
   InstructionRefId      int,
   ConfirmedQuantity     numeric(13,6),
   StatusCode            nvarchar(10)
  )
  
  set @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  set @Errormsg = 'Error executing p_Load_Order_Invoice'
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  begin transaction
  
  if @OutboundShipmentId is not null
  begin
    insert @instructions -- Be careful - there will be duplicate instructions in this table
          (WarehouseId,
           PriorityId,
           OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           IssueLineId,
           JobId,
           InstructionTypeCode,
           InstructionId,
           InstructionRefId,
           ConfirmedQuantity,
           StatusCode)
    select distinct
           j.WarehouseId,
           j.PriorityId,
           ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ili.IssueId,
           ili.IssueLineId,
           i.JobId,
           it.InstructionTypeCode,
           i.InstructionId,
           i.InstructionRefId,
           i.ConfirmedQuantity,
           s.StatusCode
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId   in (i.InstructionId, i.InstructionRefId)
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job                    j (nolock) on i.JobId             = j.JobId
      join Status                 s (nolock) on j.StatusId          = s.StatusId
     where ili.OutboundShipmentId = @OutboundShipmentId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
  end
  else if @IssueId is not null
  begin
    insert @instructions -- Be careful - there will be duplicate instructions in this table
          (WarehouseId,
           PriorityId,
           OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           IssueLineId,
           JobId,
           InstructionTypeCode,
           InstructionId,
           InstructionRefId,
           ConfirmedQuantity,
           StatusCode)
    select distinct
           j.WarehouseId,
           j.PriorityId,
           ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ili.IssueId,
           ili.IssueLineId,
           i.JobId,
           it.InstructionTypeCode,
           i.InstructionId,
           i.InstructionRefId,
           i.ConfirmedQuantity,
           s.StatusCode
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId   = i.InstructionId or ili.InstructionId   = i.InstructionRefId
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job                    j (nolock) on i.JobId           = j.JobId
      join Status                 s (nolock) on j.StatusId        = s.StatusId
     where ili.IssueId = @IssueId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
  end
  
  set @Count = 0
  
  select @Count = count(distinct OutboundShipmentId)
    from @instructions
   where StatusCode = 'C'
  
  if @Count = 0 and @OutboundShipmentId is not null
  begin
    set @Errormsg = @Errormsg + ' - No jobs are in correct status for OutboundShipment'
    goto Error
  end
  
  if @Count > 0 and (select count(distinct OutboundShipmentId)
                       from @instructions
                      where StatusCode not in ('C','NS')) = 0
  begin
    update @Instructions
       set NewOutboundShipmentId = OutboundShipmentId,
           NewIssueId            = IssueId
    
    goto Invoice
  end
  
  declare @JobId       int, 
          @StatusId    int,
          @WarehouseId int,
          @PriorityId  int
  
  if exists(select top 1 1 from @instructions where ConfirmedQuantity = 0 and @InstructionTypeCode != 'P')
  begin
    set @StatusId = dbo.ufn_StatusId('I','NS')
    
    select top 1 @WarehouseId = WarehouseId,
                 @PriorityId  = PriorityId
      from @instructions
    
    -- Create the new job
    exec @Error = p_Job_Insert
     @JobId           = @JobId output,
     @PriorityId      = @PriorityId,
     @StatusId        = @StatusId,
     @WarehouseId     = @WarehouseId
    
    if @Error <> 0
      goto error
    
    update i
       set JobId = @JobId
      from @instructions t
      join Instruction   i on t.InstructionId = i.InstructionId
     where t.ConfirmedQuantity = 0
       and t.InstructionTypeCode != 'P'
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
    
    delete @instructions
     where ConfirmedQuantity = 0
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
  end
  
  while @Count > 0
  begin
    select @Count = @Count - 1
    
    select @OutboundShipmentId = min(OutboundShipmentId)
      from @instructions
     where NewOutboundShipmentId is null
       and StatusCode = 'C'
    
    insert OutboundShipment
          (StatusId,
           WarehouseId,
           LocationId,
           ShipmentDate,
           Remarks,
           SealNumber,
           VehicleRegistration,
           Route,
           RouteId,
           Weight,
           Volume,
           FP,
           SS,
           LP,
           FM,
           MP,
           PP,
           AlternatePallet,
           SubstitutePallet,
           DespatchBay)
    select StatusId,
           WarehouseId,
           LocationId,
           ShipmentDate,
           Remarks,
           SealNumber,
           VehicleRegistration,
           Route,
           RouteId,
           Weight,
           Volume,
           FP,
           SS,
           LP,
           FM,
           MP,
           PP,
           AlternatePallet,
           SubstitutePallet,
           DespatchBay
      from OutboundShipment (nolock)
     where OutboundShipmentId = @OutboundShipmentId
    
    select @Error = @@Error, @rowcount = @@rowcount, @NewOutboundShipmentId = scope_identity()
    
    if @Error <> 0
      goto error
    
    update @instructions
       set NewOutboundShipmentId = @NewOutboundShipmentId
      where OutboundShipmentId = @OutboundShipmentId
  end
  
  set @Count = 0
  
  select @Count = count(distinct IssueId)
    from @instructions
   where StatusCode = 'C'
  
  if @Count = 0
  begin
    set @Errormsg = @Errormsg + ' - No jobs are in correct status for Issue'
    goto Error
  end
  
  while @Count > 0
  begin
    select @Count = @Count - 1
    
    select @IssueId = min(IssueId)
      from @instructions
     where NewIssueId is null
       and StatusCode = 'C'
    
    insert Issue
          (OutboundDocumentId,
           PriorityId,
           WarehouseId,
           LocationId,
           StatusId,
           OperatorId,
           RouteId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration,
           Remarks,
           DropSequence,
           LoadIndicator,
           DespatchBay,
           RoutingSystem,
           Delivery,
           NumberOfLines,
           Total,
           Complete,
           AreaType)
    select OutboundDocumentId,
           PriorityId,
           WarehouseId,
           LocationId,
           StatusId,
           OperatorId,
           RouteId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration,
           Remarks,
           DropSequence,
           LoadIndicator,
           DespatchBay,
           RoutingSystem,
           isnull(Delivery,1) + 1,
           NumberOfLines,
           Total,
           Complete,
           AreaType
      from Issue (nolock)
     where IssueId = @IssueId
    
    select @Error = @@Error, @rowcount = @@rowcount, @NewIssueId = scope_identity()
    
    if @Error <> 0
      goto error
    
    update @instructions
       set NewIssueId = @NewIssueId
      where IssueId = @IssueId
  end
  
  set @Count = 0
  
  select @Count = count(distinct IssueLineId)
    from @instructions
   where StatusCode = 'C'
  
  if @Count = 0
  begin
    set @Errormsg = @Errormsg + ' - No jobs are in correct status for IssueLine'
    goto Error
  end
  
  while @Count > 0
  begin
    select @Count = @Count - 1
    
    select @IssueLineId = min(IssueLineId)
      from @instructions
     where NewIssueLineId is null
       and StatusCode = 'C'
    
    select @NewIssueId = NewIssueId
      from @instructions
     where IssueLineId = @IssueLineId
    
    insert IssueLine
          (IssueId,
           OutboundLineId,
           StorageUnitBatchId,
           StatusId,
           OperatorId,
           Quantity,
           ConfirmedQuatity)
    select @NewIssueId,
           OutboundLineId,
           StorageUnitBatchId,
           StatusId,
           OperatorId,
           Quantity,
           0
      from IssueLine (nolock)
     where IssueLineId = @IssueLineId
    
    select @Error = @@Error, @rowcount = @@rowcount, @NewIssueLineId = scope_identity()
    
    if @Error <> 0
      goto error
    
    update @instructions
       set NewIssueLineId = @NewIssueLineId
     where IssueLineId = @IssueLineId
       and StatusCode = 'C'
  end
  
  insert OutboundShipmentIssue
        (OutboundShipmentId,
         IssueId,
         DropSequence)
  select distinct NewOutboundShipmentId,
         NewIssueId,
         null
    from @instructions
   where NewIssueId is not null
     and NewOutboundShipmentId is not null

  select @Error = @@Error, @rowcount = @@rowcount
  
  if @Error <> 0
    goto error
  
  update ili
     set OutboundShipmentId = NewOutboundShipmentId,
         IssueId            = NewIssueId,
         IssueLineId        = NewIssueLineId
    from IssueLineInstruction ili
    join @instructions          t on ili.InstructionId = t.InstructionId and ili.IssueLineId = t.IssueLineId
   where t.StatusCode = 'C'
  
  select @Error = @@Error, @rowcount = @@rowcount
  
  if @Error <> 0
    goto error
  
  update i
     set OutboundShipmentId = NewOutboundShipmentId
    from Instruction   i
    join @instructions t on i.InstructionId = t.InstructionId
   where t.StatusCode = 'C'
  
  select @Error = @@Error, @rowcount = @@rowcount
  
  if @Error <> 0
    goto error
  
  update il
     set ConfirmedQuatity = 0
    from @instructions t
    join IssueLine    il on t.IssueLineId = il.IssueLineId
  
  select @Error = @@Error, @rowcount = @@rowcount
  
  if @Error <> 0
    goto error
  
  update ili
     set ConfirmedQuantity = 0
    from IssueLineInstruction ili
    join @instructions          t on ili.InstructionId = t.InstructionId
  
  select @Error = @@Error, @rowcount = @@rowcount
  
  if @Error <> 0
    goto error
  
  Invoice:
  declare	@InstructionId     int,
          @InstructionRefId  int,
          @ConfirmedQuantity numeric(13,6)
  
  declare instruction_cursor cursor for
   select	distinct
          InstructionId,
          InstructionRefId,
          ConfirmedQuantity
  	  from	@instructions
   order by InstructionId
  
  open instruction_cursor
  
  fetch instruction_cursor into @InstructionId,
                                @InstructionRefId,
                                @ConfirmedQuantity
  
  while (@@fetch_status = 0)
  begin
    exec @Error = p_Instruction_Update_ili
     @InstructionId    = @InstructionId,
     @InstructionRefId = @InstructionRefId,
     @insConfirmed     = @ConfirmedQuantity
    
    if @Error <> 0
      goto error
    
    fetch instruction_cursor into @InstructionId,
                                  @InstructionRefId,
                                  @ConfirmedQuantity
  end
  
  close instruction_cursor
  deallocate instruction_cursor
  
  declare issue_cursor cursor for
   select	distinct
          NewOutboundShipmentId,
          NewIssueId
  	  from	@instructions
   order by NewOutboundShipmentId,
            NewIssueId
  
  open issue_cursor
  
  fetch issue_cursor into @NewOutboundShipmentId,
                          @NewIssueId
  
  while (@@fetch_status = 0)
  begin
    exec @Error = p_Palletise_Close
     @OutboundShipmentId = @NewOutboundShipmentId,
     @IssueId            = @NewIssueId
    
    if @Error <> 0
      goto error
    
    If Exists (Select top 1 1 
     from  IssueLine i
     where IssueId = @NewIssueId
          and ConfirmedQuatity > Quantity)
    begin
	  Set @Errormsg = 'Quantity cannot be greater than ordered'
      set @error = -1
      goto error
    end
    
    exec @Error = p_Interface_Export_SO_Insert
     @IssueId = @NewIssueId
    
    if @Error <> 0
      goto error
  
    update i
       set Loaded = (select count(distinct (j.JobId))
                       from IssueLineInstruction ili (nolock)
                       join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
                       join Job                    j (nolock) on ins.JobId         = j.JobId
                       join Status                 s (nolock) on j.StatusId        = s.StatusId
                      where i.IssueId   = ili.IssueId
                        and s.StatusCode in ('C'))
      from Issue i
    where i.IssueId = @NewIssueId
    
    update i
       set Pallets = (select count(distinct (JobId))
                           from IssueLineInstruction ili (nolock)
                           join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
                          where i.IssueId   = ili.IssueId)
      from Issue i
    where i.IssueId = @NewIssueId
    
    fetch issue_cursor into @NewOutboundShipmentId,
                            @NewIssueId
  end
  
  close issue_cursor
  deallocate issue_cursor
  
  commit transaction
    
  update DesktopLog
     set Errormsg = 'Successful',
         EndDate  = getdate()
   where DesktopLogId = @DesktopLogId
  
  return @Error
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    
    update DesktopLog
       set Errormsg = isnull(@Errormsg, 'None'),
           EndDate  = getdate()
     where DesktopLogId = @DesktopLogId
    
    return @Error
end
