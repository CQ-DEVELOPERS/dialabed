﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaLocation_Update
  ///   Filename       : p_AreaLocation_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Jun 2012 09:46:36
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the AreaLocation table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaLocation_Update
(
 @AreaId int = null,
 @LocationId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  update AreaLocation
     set AreaId = isnull(@AreaId, AreaId) 
   where LocationId = isnull(@LocationId, LocationId) 
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_AreaLocationHistory_Insert
         @AreaId = @AreaId,
         @LocationId = @LocationId,
         @CommandType = 'Update'
  
  return @Error
  
end
