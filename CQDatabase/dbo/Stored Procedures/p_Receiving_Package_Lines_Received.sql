﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Package_Lines_Received
  ///   Filename       : p_Receiving_Package_Lines_Received.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Package_Lines_Received
(
 @receiptId         int 
)
 
as
begin
	 set nocount on;
	 
	 declare @TotalLines    int,
	         @LinesReceived int
  

    select @TotalLines = count(1)
      from ReceiptLinePackaging             
		Where ReceiptId = @ReceiptId

    select @LinesReceived = count(1)
      from ReceiptLinePackaging r
			Join Status s on s.StatusId = r.StatusId and s.StatusCode in ('R','RC')
		Where ReceiptId = @ReceiptId
 
  
  select @LinesReceived     as 'LinesReceived',
         @TotalLines        as 'TotalLines'
end
 
