﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WIP_Job_Search
  ///   Filename       : p_WIP_Job_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WIP_Job_Search
(
 @OutboundShipmentId  int,
 @IssueId             int,
 @InstructionTypeCode nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  create table #TableHeader
  (
   DocumentNumber nvarchar(30),
   OrderNumber    nvarchar(30),
   IssueLineId    int,
   StatusCode     nvarchar(10)
  )
  
  create table #TableResult
  (
   JobId               int,
   ReferenceNumber       nvarchar(30),
   InstructionType     nvarchar(30),
   PriorityId          int,
   Priority            nvarchar(50),
   StatusId            int,
   Status              nvarchar(50),
   StatusCode          nvarchar(10),
   OperatorId          int,
   Operator            nvarchar(50)
  );
  
  declare @WarehouseId int
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is not null
  begin
    select @WarehouseId = WarehouseId
      from OutboundShipment
     where OutboundShipmentId = @OutboundShipmentId
    
    if dbo.ufn_Configuration(418, @WarehouseId) = 0
      set @IssueId = null
  end
  
  if @IssueId is not null
  begin
    insert #TableHeader
          (IssueLineId,
           OrderNumber)
    select il.IssueLineId,
           od.OrderNumber
      from IssueLine              il (nolock)
      join Issue                   i (nolock) on il.IssueId  = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where il.IssueId = @IssueId
    
    insert #TableResult
          (InstructionType,
           JobId,
           PriorityId,
           StatusId,
           OperatorId,
           ReferenceNumber)
    select distinct it.InstructionType,
           i.JobId,
           j.PriorityId,
           j.StatusId,
           j.OperatorId,
           case when it.InstructionTypeCode = 'P'
                then 'P:' + convert(nvarchar(10), i.PalletId)
                else case when j.ReferenceNumber is null
                          then 'J:' + convert(nvarchar(10), j.JobId)
                          else j.ReferenceNumber
                          end
                end
      from #TableHeader th
      join IssueLineInstruction ili (nolock) on th.IssueLineId      = ili.IssueLineId
      join Instruction      i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
      join Job              j (nolock) on i.JobId             = j.JobId
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where it.InstructionTypeCode in ('P','PM','PS','FM','PR','M')
  end
  else if @OutboundShipmentId is not null
  begin
    insert #TableHeader
          (IssueLineId,
           DocumentNumber,
           OrderNumber)
    select il.IssueLineId,
           convert(nvarchar(30), osi.OutboundShipmentId),
           od.OrderNumber
      from OutboundShipmentIssue osi (nolock)
      join IssueLine              il (nolock) on osi.IssueId = il.IssueId
      join Issue                   i (nolock) on il.IssueId  = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where osi.OutboundShipmentId = @OutboundShipmentId
    
    insert #TableResult
          (InstructionType,
           JobId,
           PriorityId,
           StatusId,
           OperatorId,
           ReferenceNumber)
    select distinct it.InstructionType,
           i.JobId,
           j.PriorityId,
           j.StatusId,
           j.OperatorId,
           case when it.InstructionTypeCode = 'P'
                then 'P:' + convert(nvarchar(10), i.PalletId)
                else case when j.ReferenceNumber is null
                          then 'J:' + convert(nvarchar(10), j.JobId)
                          else j.ReferenceNumber
                          end
                end
      from #TableHeader th
      join IssueLineInstruction ili (nolock) on th.IssueLineId      = ili.IssueLineId
      join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
      join Job                    j (nolock) on i.JobId             = j.JobId
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where it.InstructionTypeCode in ('P','PM','PS','FM','PR','M') -- and i.InstructionId > 2083864
  end
  
  update tr
     set Status = s.Status,
         StatusCode = s.StatusCode
    from #TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update tr
     set Operator = o.Operator
    from #TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set Priority = p.Priority
    from #TableResult tr
    join Priority      p (nolock) on tr.PriorityId = p.PriorityId
  
  select JobId,
         ReferenceNumber,
         max(InstructionType) 'InstructionType',
         Priority,
         Status,
         Operator,
         convert(bit, case when StatusCode in ('CD','D','DC','C')
              then 1
              else 0
              end) as 'Enabled'
    from #TableResult
  group by JobId,
           ReferenceNumber,
           Priority,
           Status,
           Operator,
           convert(bit, case when StatusCode in ('CD','D','DC','C')
              then 1
              else 0
              end)
end
 
 
