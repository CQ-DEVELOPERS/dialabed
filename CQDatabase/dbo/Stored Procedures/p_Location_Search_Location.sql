﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_Location
  ///   Filename       : p_Location_Search_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_Location
(
 @WarehouseId int,
 @Location   nvarchar(15)
)
 
as
begin
	 set nocount on;
  
  select l.LocationId,
         l.Location
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId    = a.AreaId
   where a.WarehouseId = @WarehouseId
     and l.Location like '%' + isnull(@Location,'') + '%'
end
