﻿CREATE procedure [dbo].[p_Interface_WebService_SO]
(
 @doc2			nvarchar(max) output
)
--with encryption
as
begin
	
	declare @doc xml
	set @doc = convert(xml,@doc2)
	DECLARE @idoc int
	Declare @InsertDate datetime
	
	Set @InsertDate = Getdate()
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	

		Insert Into 
		InterfaceImportSOHeader(RecordStatus,
								InsertDate,
								PrimaryKey,
								OrderNumber,
								CustomerCode,
								Additional2,
								Additional3,
								Additional4,
								Additional5,
								Additional6,
								Additional7,
								DeliveryDate,
								Additional10,
								RecordType)	
		SELECT    'W',@InsertDate,*
		FROM       OPENXML (@idoc, '/root/SalesOrder',1)
            WITH (PrimaryKey varchar(50) 'IDSO',
				  OrderNumber  nvarchar(50) 'cOrderNumber',
                  CustomerCode nvarchar(50) 'iCustomerCode',
				  Additional2 nvarchar(50) 'dCreateDate',
				  Additional3 nvarchar(255) 'Comments',
				  Additional4 nvarchar(255) 'DeliveryAdd1',
				  Additional5 nvarchar(255) 'DeliveryAdd2',
				  Additional6 nvarchar(255) 'DeliveryAdd3',
				  Additional7 nvarchar(255) 'DeliveryAdd4',
				  
				  DeliveryDate varchar(50) 'dDeliveryDate',
				  Additional10 nvarchar(50)  'iStatus',
				  RecordType nvarchar(50) 'ulIDSOrdOCType')
				  
		Insert Into InterfaceImportSODetail (ForeignKey,
										 LineNumber,
										 ProductCode,
										 Product,
										 Quantity,
										 Additional1)
		Select *											
		FROM       OPENXML (@idoc, '/root/SalesOrder/SalesOrderLine',1)
            WITH (
					ForeignKey  varchar(50) 'IDSO',
					LineNumber varchar(50) 'IDSOLine',
					ProductCode nvarchar(50) 'iStockID',
					Product		nvarchar(255) 'Product',
					Quantity varchar(50) 'fQty',
					Additional1 nvarchar(50)  'iWhseID')	
					
		update d
		set InterfaceImportSOHeaderId = h.InterfaceImportSOHeaderid
		from InterfaceImportSoDetail d
			Join InterfaceImportSOHeader h on h.PrimaryKey = d.ForeignKey
		where h.RecordStatus = 'W' and InsertDate = @InsertDate and d.InterfaceImportSOHeaderid is null
		
		Update InterfaceImportSOHeader set RecordStatus = 'N'
		Where RecordSTatus = 'W' and InsertDate = @InsertDate
		update InterfaceImportSOHeader set RecordType = 'SAL' Where len(isnull(Recordtype,'')) = 0
		exec p_Pastel_Import_SO
		Set @doc2 = ''
End








