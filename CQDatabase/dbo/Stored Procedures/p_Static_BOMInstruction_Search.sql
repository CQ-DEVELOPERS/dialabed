﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_BOMInstruction_Search
  ///   Filename       : p_Static_BOMInstruction_Search.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>	 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_BOMInstruction_Search

 
as
begin
	 set nocount on;
	 
  declare @TableResult TABLE
  (OrderNumber           nvarchar(30)
  ,LineNumber            Int
  ,Quantity              float
  ,ProductCode           nvarchar(30)
  ,Product               nvarchar(50)
  ,BOMInstructionId      Int
  ,BOMLineId             Int
  ,StorageUnitId         int
  ,RequiredQuantity      float default 0
  ,ConfirmedQuatity      float default 0
  ,AllocatedQuantity     float default 0
  ,ManufacturingQuantity float default 0
  ,RawQuantity           float default 0
  ,ReplenishmentQuantity float default 0
  ,AvailableQuantity     float default 0
  ,AvailabilityIndicator nvarchar(20)
  ,AvailablePercentage   float default 0
  ,AreaType              nvarchar(10) default ''
  ,WarehouseId           int
  ,DropSequence          int
  ,MaximumQuantity       float
  )
  
  INSERT @TableResult
           (WarehouseId
           ,AreaType
           ,OrderNumber
           ,LineNumber
           ,RequiredQuantity
           ,ProductCode
           ,Product
           ,BOMInstructionId
           ,BOMLineId
           ,StorageUnitId)
  SELECT od.WarehouseId
           ,i.AreaType
           ,case when osi.OutboundShipmentId is not null
                 then convert(varchar(10), osi.OutboundShipmentId)
                 else od.OrderNumber
                 end
           ,ol.LineNumber
           ,sum(il.Quantity*bp.Quantity)
           ,P.ProductCode
           ,P.Product
           --,null as BOMInstructionId
           ,BOMInstructionId
           ,-1 as BOMLineId
           ,bp.StorageUnitId
      FROM	OutboundDocument        od (nolock)
      join OutboundLine            ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId
      join Issue                    i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
      left
      join OutboundShipmentIssue  osi (nolock) on i.IssueId = osi.IssueId
      join IssueLine               il (nolock) on i.IssueId = il.IssueId
      join StorageUnitBatch       sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join BOMheader               bh (nolock) on sub.StorageUnitId = bh.StorageUnitId
      join BOMLine                 bl (nolock) on bh.BOMHeaderId = bl.BOMHeaderId
      join BOMProduct              bp (nolock) on bl.BOMLineId = bp.BOMLineId
      join StorageUnit             su (nolock) on bp.StorageUnitId = su.StorageUnitId 
      join Product                  p (nolock) on su.ProductId = p.ProductId
      join Status                   s (nolock) on p.StatusId = s.StatusId
      join BOMInstruction          bi (nolock) on bi.BOMHeaderId = bh.BOMHeaderId
                                              and bi.IssueId = i.IssueId
     WHERE bi.InboundDocumentId Is Null
       AND bi.OutboundDocumentId Is Null
    group by od.WarehouseId
            ,i.AreaType
            ,case when osi.OutboundShipmentId is not null
                 then convert(varchar(10), osi.OutboundShipmentId)
                 else od.OrderNumber
                 end
            ,ol.LineNumber
            ,P.ProductCode
            ,P.Product
            ,BOMInstructionId
            ,bp.StorageUnitId
  
  /* Start
     keep this same as in p_BOM_SO_InstructionLine */
  update tr
     set AllocatedQuantity = a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ManufacturingQuantity = a.ActualQuantity - a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set RAWQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set AvailableQuantity = ManufacturingQuantity + RAWQuantity
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), AvailableQuantity) / convert(numeric(13,3), RequiredQuantity)) * 100
   where AllocatedQuantity + AvailableQuantity != 0
     and convert(numeric(13,3), RequiredQuantity) > 0
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity   > ManufacturingQuantity
     and RequiredQuantity   > ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity  <= ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where ManufacturingQuantity >= RequiredQuantity
  
  /* End
     keep this same as in p_BOM_SO_InstructionLine */
  
  update @TableResult set MaximumQuantity = convert(numeric(13,6), RequiredQuantity) * (convert(numeric(13,6), AvailablePercentage) / 100.000000)
  --select * from @TableResult
  ----update @TableResult set MaximumQuantity = convert(numeric(13,6), RequiredQuantity) / (convert(numeric(13,6), MaximumQuantity)) * 100 where MaximumQuantity > 0
  update @TableResult set MaximumQuantity = convert(numeric(13,6), MaximumQuantity) / (convert(numeric(13,6), RequiredQuantity)) * 100 where MaximumQuantity > 0
  --select 6 * .8
  --select * from @TableResult
  update tr
     set MaximumQuantity = (select min(MaximumQuantity)
                              from @TableResult mx
                             where tr.BOMInstructionId = mx.BOMInstructionId)
    from @TableResult tr
  
  select	bi.BOMInstructionId
			     ,bi.OrderNumber
        ,bi.Quantity
        ,convert(numeric(13,3), convert(numeric(13,6), bi.Quantity) * (convert(numeric(13,6), (select min(MaximumQuantity) from @TableResult tr where bi.BOMInstructionId = tr.BOMInstructionId)) / 100.000000)) as 'MaximumQuantity'
        ,bh.BOMHeaderId
        ,bh.Description
        ,bh.Remarks
        ,bh.BOMType
        ,p.ProductCode
        ,p.Product
        ,id.InboundDocumentId
        --,id.OrderNumber 
        ,od.OutboundDocumentId
        --,od.OrderNumber
        ,case when (select max(bp.BOMPackagingId) from BOMPackaging bp (nolock) where bi.BOMInstructionId = bp.BOMInstructionId) is null
              then convert(bit, 0)
              else convert(bit, 1)
              end as 'ReleaseEnabled'
        FROM	BOMInstruction   bi (nolock)
        join BOMHeader        bh (nolock) on bi.BOMHeaderId = bh.BOMHeaderId
        join StorageUnit      su (nolock) on bh.StorageUnitId = su.StorageUnitId 
        join Product           p (nolock) on su.ProductId = p.ProductId
   Left join InboundDocument  id (nolock) on bi.InboundDocumentId = id.InboundDocumentId
   Left join OutboundDocument od (nolock) on bi.OutboundDocumentId = od.OutboundDocumentId
   WHERE bi.InboundDocumentId Is Null
     AND bi.OutboundDocumentId Is Null
end
