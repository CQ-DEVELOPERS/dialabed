﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItemCultureHistory_Insert
  ///   Filename       : p_MenuItemCultureHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:53
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MenuItemCultureHistory table.
  /// </remarks>
  /// <param>
  ///   @MenuItemCultureId int = null,
  ///   @MenuItemId int = null,
  ///   @CultureId int = null,
  ///   @MenuItemText nvarchar(100) = null,
  ///   @ToolTip nvarchar(510) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   MenuItemCultureHistory.MenuItemCultureId,
  ///   MenuItemCultureHistory.MenuItemId,
  ///   MenuItemCultureHistory.CultureId,
  ///   MenuItemCultureHistory.MenuItemText,
  ///   MenuItemCultureHistory.ToolTip,
  ///   MenuItemCultureHistory.CommandType,
  ///   MenuItemCultureHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItemCultureHistory_Insert
(
 @MenuItemCultureId int = null,
 @MenuItemId int = null,
 @CultureId int = null,
 @MenuItemText nvarchar(100) = null,
 @ToolTip nvarchar(510) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert MenuItemCultureHistory
        (MenuItemCultureId,
         MenuItemId,
         CultureId,
         MenuItemText,
         ToolTip,
         CommandType,
         InsertDate)
  select @MenuItemCultureId,
         @MenuItemId,
         @CultureId,
         @MenuItemText,
         @ToolTip,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
