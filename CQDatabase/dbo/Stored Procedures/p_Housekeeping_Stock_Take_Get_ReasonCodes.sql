﻿CREATE PROCEDURE p_Housekeeping_Stock_Take_Get_ReasonCodes
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Reason,ReasonCode FROM Reason WHERE RecordType = 'ADJ'
END
