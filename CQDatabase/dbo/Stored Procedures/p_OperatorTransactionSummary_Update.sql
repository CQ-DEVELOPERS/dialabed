﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorTransactionSummary_Update
  ///   Filename       : p_OperatorTransactionSummary_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:33
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorTransactionSummary table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @InstructionTypeId int = null,
  ///   @EndDate datetime = null,
  ///   @Units int = null,
  ///   @Weight float = null,
  ///   @Instructions int = null,
  ///   @Jobs int = null,
  ///   @ActiveTime int = null,
  ///   @DwellTime int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorTransactionSummary_Update
(
 @OperatorId int = null,
 @InstructionTypeId int = null,
 @EndDate datetime = null,
 @Units int = null,
 @Weight float = null,
 @Instructions int = null,
 @Jobs int = null,
 @ActiveTime int = null,
 @DwellTime int = null,
 @WarehouseId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update OperatorTransactionSummary
     set OperatorId = isnull(@OperatorId, OperatorId),
         InstructionTypeId = isnull(@InstructionTypeId, InstructionTypeId),
         EndDate = isnull(@EndDate, EndDate),
         Units = isnull(@Units, Units),
         Weight = isnull(@Weight, Weight),
         Instructions = isnull(@Instructions, Instructions),
         Jobs = isnull(@Jobs, Jobs),
         ActiveTime = isnull(@ActiveTime, ActiveTime),
         DwellTime = isnull(@DwellTime, DwellTime),
         WarehouseId = isnull(@WarehouseId, WarehouseId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
