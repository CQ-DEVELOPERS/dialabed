﻿/*  
  /// <summary>  
  ///   Procedure Name : p_Report_Transaction_Product  
  ///   Filename       : p_Report_Transaction_Product.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 19 Dec 2007  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/
CREATE PROCEDURE p_Report_Transaction_Product (
	@WarehouseId INT
	,@StorageUnitId INT = NULL
	,@BatchId INT = NULL
	,@FromDate DATETIME
	,@ToDate DATETIME
	,@PrincipalId INT
	)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TableResult AS TABLE (
		InstructionId INT
		,InstructionTypeId INT
		,InstructionType NVARCHAR(30)
		,InstructionTypeCode NVARCHAR(10)
		,StatusId INT
		,STATUS NVARCHAR(50)
		,CreateDate DATETIME
		,StartDate DATETIME
		,EndDate DATETIME
		,OperatorId INT
		,Operator NVARCHAR(50)
		,StorageUnitBatchId INT
		,ProductCode NVARCHAR(50)
		,Product NVARCHAR(255)
		,SKUCode NVARCHAR(50)
		,SKU NVARCHAR(50)
		,Batch NVARCHAR(50)
		,ExpiryDate DATETIME
		,Location NVARCHAR(15)
		,PickLocationId INT NULL
		,PickLocation NVARCHAR(15)
		,StoreLocationId INT NULL
		,StoreLocation NVARCHAR(15)
		,Quantity FLOAT
		,ConfirmedQuantity FLOAT
		,ReceiptLineId INT
		,CheckQuantity FLOAT
		,NettWeight FLOAT
		,DRCR INT
		,PalletId INT
		,OrderNumber NVARCHAR(30)
		,PrincipalId INT
		,PrincipalCode NVARCHAR(50)
		)

	IF @StorageUnitId = - 1
		SET @StorageUnitId = NULL

	IF @BatchId = - 1
		SET @BatchId = NULL

	IF @PrincipalId = - 1
		SET @PrincipalId = NULL

	INSERT @TableResult (
		InstructionId
		,InstructionTypeId
		,StatusId
		,CreateDate
		,StartDate
		,EndDate
		,OperatorId
		,StorageUnitBatchId
		,PickLocationId
		,StoreLocationId
		,Quantity
		,ConfirmedQuantity
		,ReceiptLineId
		,CheckQuantity
		,PalletId
		,OrderNumber
		)
	SELECT i.InstructionId
		,i.InstructionTypeId
		,i.StatusId
		,i.CreateDate
		,i.StartDate
		,i.EndDate
		,i.OperatorId
		,i.StorageUnitBatchId
		,i.PickLocationId
		,i.StoreLocationId
		,i.Quantity
		,i.ConfirmedQuantity
		,i.ReceiptLineId
		,i.CheckQuantity
		,i.PalletId
		,isnull(od.OrderNumber, id.OrderNumber)
	FROM Instruction i(NOLOCK)
	JOIN StorageUnitBatch sub(NOLOCK) ON i.StorageUnitBatchId = sub.StorageUnitBatchId
	LEFT JOIN IssueLineInstruction ili(NOLOCK) ON isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
	LEFT JOIN OutboundDocument od(NOLOCK) ON od.OutboundDocumentId = ili.OutboundDocumentId
	LEFT JOIN ReceiptLine rl(NOLOCK) ON i.ReceiptLineId = rl.ReceiptLineId
	LEFT JOIN Inboundline il(NOLOCK) ON il.InboundLineId = rl.InboundLineId
	LEFT JOIN InboundDocument id(NOLOCK) ON id.InboundDocumentId = il.InboundDocumentId
	WHERE i.WarehouseId = @WarehouseId
		AND (
			i.CreateDate BETWEEN @FromDate
				AND @ToDate
			OR EndDate BETWEEN @FromDate
				AND @ToDate
			)
		AND sub.StorageUnitId = isnull(@StorageUnitId, sub.StorageUnitId)
		AND sub.BatchId = isnull(@BatchId, sub.BatchId)

	UPDATE tr
	SET ProductCode = p.ProductCode
		,Product = p.Product
		,SKUCode = sku.SKUCode
		,Batch = b.Batch
		,ExpiryDate = b.ExpiryDate
		,SKU = sku.SKU
		,PrincipalId = p.PrincipalId
	FROM @TableResult tr
	JOIN StorageUnitBatch sub(NOLOCK) ON tr.StorageUnitBatchId = sub.StorageUnitBatchId
	JOIN StorageUnit su(NOLOCK) ON sub.StorageUnitId = su.StorageUnitId
	JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
	JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
	JOIN Batch b(NOLOCK) ON sub.BatchId = b.BatchId

	UPDATE tr
	SET Batch = b.Batch + ' *'
	FROM @TableResult tr
	JOIN ReceiptLine rl(NOLOCK) ON tr.ReceiptLineId = rl.ReceiptLineId
	JOIN StorageUnitBatch sub(NOLOCK) ON rl.StorageUnitBatchId = sub.StorageUnitBatchId
	JOIN Batch b(NOLOCK) ON sub.BatchId = b.BatchId
	WHERE tr.Batch = 'Default'

	UPDATE tr
	SET InstructionType = it.InstructionType
		,InstructionTypeCode = it.InstructionTypeCode
	FROM @TableResult tr
	JOIN InstructionType it(NOLOCK) ON tr.InstructionTypeId = it.InstructionTypeId

	UPDATE @TableResult
	SET InstructionType = InstructionType + '*User'
	WHERE CheckQuantity = 1
		AND InstructionTypeCode = 'R'

	UPDATE tr
	SET STATUS = s.STATUS
	FROM @TableResult tr
	JOIN STATUS s(NOLOCK) ON tr.StatusId = s.StatusId

	UPDATE tr
	SET PrincipalCode = p.PrincipalCode
	FROM @TableResult tr
	JOIN Principal p(NOLOCK) ON tr.PrincipalId = p.PrincipalId

	UPDATE tr
	SET Operator = o.Operator
	FROM @TableResult tr
	JOIN Operator o(NOLOCK) ON tr.OperatorId = o.OperatorId

	UPDATE tr
	SET PickLocation = l.Location
	FROM @TableResult tr
	JOIN Location l(NOLOCK) ON tr.PickLocationId = l.LocationId

	UPDATE tr
	SET StoreLocation = l.Location
	FROM @TableResult tr
	JOIN Location l(NOLOCK) ON tr.StoreLocationId = l.LocationId

	UPDATE tr
	SET DRCR = ConfirmedQuantity
	FROM @TableResult tr

	UPDATE tr
	SET DRCR = ConfirmedQuantity * - 1
	FROM @TableResult tr
	WHERE InstructionTypeCode IN (
			'M'
			,'P'
			,'O'
			,'PM'
			,'PS'
			,'FM'
			)

	UPDATE tr
	SET DRCR = ConfirmedQuantity
	FROM @TableResult tr
	WHERE InstructionTypeCode IN (
			'R'
			,'S'
			,'SM'
			,'PR'
			,'HS'
			,'RS'
			)

	UPDATE tr
	SET DRCR = isnull(ConfirmedQuantity, 0) - isnull(Quantity, 0)
	FROM @TableResult tr
	WHERE InstructionTypeCode IN (
			'STE'
			,'STL'
			,'STA'
			,'STP'
			)

	UPDATE tr
	SET DRCR = 0
	FROM @TableResult tr
	WHERE tr.STATUS IN (
			'Waiting'
			,'Deleted'
			)

	SELECT InstructionType
		,STATUS
		,Operator
		,ProductCode
		,Product
		,SKUCode
		,SKU
		,Batch
		,ExpiryDate
		,InstructionId
		,PickLocation
		,StoreLocation
		,Quantity
		,ConfirmedQuantity
		,CreateDate
		,StartDate
		,EndDate
		,NettWeight
		,DRCR
		,PalletId
		,OrderNumber
		,PrincipalCode
	FROM @TableResult
	WHERE isnull(PrincipalId, - 1) = isnull(@PrincipalId, isnull(PrincipalId, - 1))
	ORDER BY ProductCode
		,SKUCode
		,isnull(EndDate, isnull(StartDate, CreateDate))
END