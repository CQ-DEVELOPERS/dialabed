﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceField_Search
  ///   Filename       : p_InterfaceField_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:46:47
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceField table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFieldId int = null output,
  ///   @InterfaceFieldCode nvarchar(60) = null,
  ///   @InterfaceField nvarchar(100) = null,
  ///   @InterfaceDocumentTypeId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceField.InterfaceFieldId,
  ///   InterfaceField.InterfaceFieldCode,
  ///   InterfaceField.InterfaceField,
  ///   InterfaceField.InterfaceDocumentTypeId,
  ///   InterfaceDocumentType.InterfaceDocumentType,
  ///   InterfaceField.Datatype 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceField_Search
(
 @InterfaceFieldId int = null output,
 @InterfaceFieldCode nvarchar(60) = null,
 @InterfaceField nvarchar(100) = null,
 @InterfaceDocumentTypeId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceFieldId = '-1'
    set @InterfaceFieldId = null;
  
  if @InterfaceFieldCode = '-1'
    set @InterfaceFieldCode = null;
  
  if @InterfaceField = '-1'
    set @InterfaceField = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
 
  select
         InterfaceField.InterfaceFieldId
        ,InterfaceField.InterfaceFieldCode
        ,InterfaceField.InterfaceField
        ,InterfaceField.InterfaceDocumentTypeId
         ,InterfaceDocumentTypeInterfaceDocumentTypeId.InterfaceDocumentType as 'InterfaceDocumentType'
        ,InterfaceField.Datatype
    from InterfaceField
    left
    join InterfaceDocumentType InterfaceDocumentTypeInterfaceDocumentTypeId on InterfaceDocumentTypeInterfaceDocumentTypeId.InterfaceDocumentTypeId = InterfaceField.InterfaceDocumentTypeId
   where isnull(InterfaceField.InterfaceFieldId,'0')  = isnull(@InterfaceFieldId, isnull(InterfaceField.InterfaceFieldId,'0'))
     and isnull(InterfaceField.InterfaceFieldCode,'%')  like '%' + isnull(@InterfaceFieldCode, isnull(InterfaceField.InterfaceFieldCode,'%')) + '%'
     and isnull(InterfaceField.InterfaceField,'%')  like '%' + isnull(@InterfaceField, isnull(InterfaceField.InterfaceField,'%')) + '%'
     and isnull(InterfaceField.InterfaceDocumentTypeId,'0')  = isnull(@InterfaceDocumentTypeId, isnull(InterfaceField.InterfaceDocumentTypeId,'0'))
  order by InterfaceFieldCode
  
end
