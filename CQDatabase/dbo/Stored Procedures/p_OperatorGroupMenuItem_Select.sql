﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItem_Select
  ///   Filename       : p_OperatorGroupMenuItem_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:25
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroupMenuItem table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @MenuItemId int = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupMenuItem.OperatorGroupId,
  ///   OperatorGroupMenuItem.MenuItemId,
  ///   OperatorGroupMenuItem.Access 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItem_Select
(
 @OperatorGroupId int = null,
 @MenuItemId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         OperatorGroupMenuItem.OperatorGroupId
        ,OperatorGroupMenuItem.MenuItemId
        ,OperatorGroupMenuItem.Access
    from OperatorGroupMenuItem
   where isnull(OperatorGroupMenuItem.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(OperatorGroupMenuItem.OperatorGroupId,'0'))
     and isnull(OperatorGroupMenuItem.MenuItemId,'0')  = isnull(@MenuItemId, isnull(OperatorGroupMenuItem.MenuItemId,'0'))
  
end
