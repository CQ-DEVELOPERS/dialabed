﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoadJob_Insert
  ///   Filename       : p_IntransitLoadJob_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012 11:58:11
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IntransitLoadJob table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadJobId int = null output,
  ///   @IntransitLoadId int = null,
  ///   @StatusId int = null,
  ///   @JobId int = null,
  ///   @LoadedById int = null,
  ///   @UnLoadedById int = null,
  ///   @CreateDate datetime = null,
  ///   @ReceivedDate datetime = null 
  /// </param>
  /// <returns>
  ///   IntransitLoadJob.IntransitLoadJobId,
  ///   IntransitLoadJob.IntransitLoadId,
  ///   IntransitLoadJob.StatusId,
  ///   IntransitLoadJob.JobId,
  ///   IntransitLoadJob.LoadedById,
  ///   IntransitLoadJob.UnLoadedById,
  ///   IntransitLoadJob.CreateDate,
  ///   IntransitLoadJob.ReceivedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoadJob_Insert
(
 @IntransitLoadJobId int = null output,
 @IntransitLoadId int = null,
 @StatusId int = null,
 @JobId int = null,
 @LoadedById int = null,
 @UnLoadedById int = null,
 @CreateDate datetime = null,
 @ReceivedDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  insert IntransitLoadJob
        (IntransitLoadId,
         StatusId,
         JobId,
         LoadedById,
         UnLoadedById,
         CreateDate,
         ReceivedDate)
  select @IntransitLoadId,
         @StatusId,
         @JobId,
         @LoadedById,
         @UnLoadedById,
         @CreateDate,
         @ReceivedDate 
  
  select @Error = @@Error, @IntransitLoadJobId = scope_identity()
  
  if @Error = 0
    exec @Error = p_IntransitLoadJobHistory_Insert
         @IntransitLoadJobId = @IntransitLoadJobId,
         @IntransitLoadId = @IntransitLoadId,
         @StatusId = @StatusId,
         @JobId = @JobId,
         @LoadedById = @LoadedById,
         @UnLoadedById = @UnLoadedById,
         @CreateDate = @CreateDate,
         @ReceivedDate = @ReceivedDate,
         @CommandType = 'Insert'
  
  return @Error
  
end
