﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerHeader_Insert
  ///   Filename       : p_ContainerHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:53
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ContainerHeader table.
  /// </remarks>
  /// <param>
  ///   @ContainerHeaderId int = null output,
  ///   @PalletId int = null,
  ///   @JobId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @CreatedBy int = null,
  ///   @CreateDate datetime = null 
  /// </param>
  /// <returns>
  ///   ContainerHeader.ContainerHeaderId,
  ///   ContainerHeader.PalletId,
  ///   ContainerHeader.JobId,
  ///   ContainerHeader.ReferenceNumber,
  ///   ContainerHeader.PickLocationId,
  ///   ContainerHeader.StoreLocationId,
  ///   ContainerHeader.CreatedBy,
  ///   ContainerHeader.CreateDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerHeader_Insert
(
 @ContainerHeaderId int = null output,
 @PalletId int = null,
 @JobId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @CreatedBy int = null,
 @CreateDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @ContainerHeaderId = '-1'
    set @ContainerHeaderId = null;
  
	 declare @Error int
 
  insert ContainerHeader
        (PalletId,
         JobId,
         ReferenceNumber,
         PickLocationId,
         StoreLocationId,
         CreatedBy,
         CreateDate)
  select @PalletId,
         @JobId,
         @ReferenceNumber,
         @PickLocationId,
         @StoreLocationId,
         @CreatedBy,
         @CreateDate 
  
  select @Error = @@Error, @ContainerHeaderId = scope_identity()
  
  
  return @Error
  
end
