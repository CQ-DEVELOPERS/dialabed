﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_Flo_PM
  ///   Filename       : p_FamousBrands_Export_Flo_PM.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Export_Flo_PM
(
 @FileName varchar(30) = null output
)
 
as
begin
	 set nocount on;
  --select @FileName = 'ProductMaster.txt'
  select @FileName = ''
  
  select distinct isnull(Code,'') + ',"' + isnull(Name,'') + '","' + isnull(Class,'') + '"'
    from FloProductMasterExport
   where RecordStatus = 'N'
  
  update FloProductMasterExport
     set RecordStatus = 'Y'
   where RecordStatus = 'N'
end
