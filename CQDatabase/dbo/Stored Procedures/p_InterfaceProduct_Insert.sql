﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceProduct_Insert
  ///   Filename       : p_InterfaceProduct_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:38
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceProduct table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @ProductDescription nvarchar(510) = null,
  ///   @Barcode nvarchar(510) = null,
  ///   @OuterCartonBarcode nvarchar(510) = null,
  ///   @PalletQuantity float = null,
  ///   @Class nvarchar(100) = null,
  ///   @Modified nvarchar(60) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @CaseQuantity float = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceProduct.HostId,
  ///   InterfaceProduct.ProductCode,
  ///   InterfaceProduct.ProductDescription,
  ///   InterfaceProduct.Barcode,
  ///   InterfaceProduct.OuterCartonBarcode,
  ///   InterfaceProduct.PalletQuantity,
  ///   InterfaceProduct.Class,
  ///   InterfaceProduct.Modified,
  ///   InterfaceProduct.ProcessedDate,
  ///   InterfaceProduct.RecordStatus,
  ///   InterfaceProduct.CaseQuantity,
  ///   InterfaceProduct.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceProduct_Insert
(
 @HostId nvarchar(60) = null,
 @ProductCode nvarchar(60) = null,
 @ProductDescription nvarchar(510) = null,
 @Barcode nvarchar(510) = null,
 @OuterCartonBarcode nvarchar(510) = null,
 @PalletQuantity float = null,
 @Class nvarchar(100) = null,
 @Modified nvarchar(60) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @CaseQuantity float = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceProduct
        (HostId,
         ProductCode,
         ProductDescription,
         Barcode,
         OuterCartonBarcode,
         PalletQuantity,
         Class,
         Modified,
         ProcessedDate,
         RecordStatus,
         CaseQuantity,
         InsertDate)
  select @HostId,
         @ProductCode,
         @ProductDescription,
         @Barcode,
         @OuterCartonBarcode,
         @PalletQuantity,
         @Class,
         @Modified,
         @ProcessedDate,
         @RecordStatus,
         @CaseQuantity,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
