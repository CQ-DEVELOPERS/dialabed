﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ShipmentIssue_Select
  ///   Filename       : p_ShipmentIssue_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:52
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null,
  ///   @IssueId int = null 
  /// </param>
  /// <returns>
  ///   ShipmentIssue.ShipmentId,
  ///   ShipmentIssue.IssueId,
  ///   ShipmentIssue.DropSequence 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ShipmentIssue_Select
(
 @ShipmentId int = null,
 @IssueId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ShipmentIssue.ShipmentId
        ,ShipmentIssue.IssueId
        ,ShipmentIssue.DropSequence
    from ShipmentIssue
   where isnull(ShipmentIssue.ShipmentId,'0')  = isnull(@ShipmentId, isnull(ShipmentIssue.ShipmentId,'0'))
     and isnull(ShipmentIssue.IssueId,'0')  = isnull(@IssueId, isnull(ShipmentIssue.IssueId,'0'))
  
end
