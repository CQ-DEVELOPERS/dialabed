﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitArea_Delete
  ///   Filename       : p_StorageUnitArea_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012 14:20:19
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StorageUnitArea table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitArea_Delete
(
 @StorageUnitId int = null,
 @AreaId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  delete StorageUnitArea
     where StorageUnitId = @StorageUnitId
       and AreaId = @AreaId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_StorageUnitAreaHistory_Insert
         @StorageUnitId,
         @AreaId,
         @CommandType = 'Delete'
  
  return @Error
  
end
