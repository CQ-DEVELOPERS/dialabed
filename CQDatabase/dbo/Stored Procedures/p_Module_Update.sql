﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Module_Update
  ///   Filename       : p_Module_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:47
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Module table.
  /// </remarks>
  /// <param>
  ///   @ModuleId int = null,
  ///   @Module nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Module_Update
(
 @ModuleId int = null,
 @Module nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @ModuleId = '-1'
    set @ModuleId = null;
  
  if @Module = '-1'
    set @Module = null;
  
	 declare @Error int
 
  update Module
     set Module = isnull(@Module, Module) 
   where ModuleId = @ModuleId
  
  select @Error = @@Error
  
  
  return @Error
  
end
