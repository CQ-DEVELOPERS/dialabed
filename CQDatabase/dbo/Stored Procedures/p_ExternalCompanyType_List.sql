﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyType_List
  ///   Filename       : p_ExternalCompanyType_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:40
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ExternalCompanyType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ExternalCompanyType.ExternalCompanyTypeId,
  ///   ExternalCompanyType.ExternalCompanyType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyType_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ExternalCompanyTypeId
        ,'{All}' as ExternalCompanyType
  union
  select
         ExternalCompanyType.ExternalCompanyTypeId
        ,ExternalCompanyType.ExternalCompanyType
    from ExternalCompanyType
  
end
