﻿create procedure [dbo].[p_Interface_WebService_PO]
(
 @doc2			varchar(max) output
)
--with encryption
as
begin
	
	declare @doc xml
	set @doc = convert(xml,@doc2)
	DECLARE @idoc int
	Declare @InsertDate datetime
	
	Set @InsertDate = Getdate()
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
                    
		Insert Into 
		InterfaceImportPOHeader(RecordStatus,
								InsertDate,
								PrimaryKey,
								OrderNumber,
								SupplierCode,
								DeliveryNoteNumber,
								Additional2,
								Additional1,
								ContainerNumber,
								Additional3)	
		SELECT    'W',@InsertDate,*
		FROM       OPENXML (@idoc, '/root/PurchaseOrder',1)
            WITH (PrimaryKey varchar(50) 'IDPO',
				  OrderNumber  varchar(50) 'cOrderNumber',
                  SupplierCode varchar(50) 'iSupplierCode',
				  DeliveryNoteNumber varchar(50) 'cDelNoteNumber',
				  Additional2 varchar(50) 'iStatus',
				  Additional1 varchar(50)  'dCreateDate',
				  ContainerNumber varchar(50)  'cContainerNumber',
				  Additional3 varchar(50)  'cVehicleRegNumber')

		Insert Into InterfaceImportPODetail (ForeignKey,
										 LineNumber,
										 ProductCode,
										 Product,
										 Additional3,
										 Quantity,
										 Additional1)
		Select *											
		FROM       OPENXML (@idoc, '/root/PurchaseOrder/PurchaseOrderLine',1)
            WITH (
					ForeignKey  varchar(50) 'IDPO',
					LineNumber varchar(50) 'IDPOLine',
					ProductCode varchar(50) 'ProductCode',
					Product varchar(50) 'Product ',
					Additional3 varchar(50) 'iStockID',
					Quantity varchar(50) 'fQty',
					Additional1 varchar(50)  'iWhseID')	
					
		update d
		set InterfaceImportPOHeaderId = h.InterfaceImportPOHeaderid
		from InterfaceImportPODetail d
			Join InterfaceImportPOHeader h on h.PrimaryKey = d.ForeignKey
		where h.RecordStatus = 'W' and InsertDate = @InsertDate and d.InterfaceImportPOHeaderid is null
		
		Update InterfaceImportPOHeader set RecordStatus = 'N'
		Where RecordSTatus = 'W' and InsertDate = @InsertDate
		exec p_Pastel_Import_PO
		Set @doc2 = ''
End






