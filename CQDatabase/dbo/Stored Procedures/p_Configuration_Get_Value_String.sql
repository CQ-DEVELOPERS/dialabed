﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Get_Value_String
  ///   Filename       : p_Configuration_Get_Value_String.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Jan 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Get_Value_String
(
 @ConfigurationId int,
 @WarehouseId int = 1
)
 
as
begin
	 set nocount on;
  
  select convert(nvarchar(max), [Value]) as 'Value'
    from Configuration (nolock)
   where ConfigurationId = @ConfigurationId
     and WarehouseId = @warehouseId
end
