﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questions
  ///   Filename       : p_Questions.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Questions
(
	@WarehouseId      int,
	@QuestionaireId		int = null
)

as
begin
	 set nocount on;
  
  select a.QuestionAireId,
		a.QuestionAireId,
		A.QuestionaireType,
		q.QuestionId,
		q.Category,
		q.Code,
		q.Sequence,
		q.Active,
		q.QuestionText,
		q.QuestionType,
		q.DateUpdated 
	from QuestionAire a
		Join Questions q on q.QuestionaireId = a.QuestionaireId
	where warehouseid = @WarehouseId and isnull(@QuestionaireId,a.QuestionaireId) = a.QuestionaireId
	Order by category,
			sequence
      
  
end
