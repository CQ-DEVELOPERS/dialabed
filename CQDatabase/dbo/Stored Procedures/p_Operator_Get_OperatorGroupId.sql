﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Get_OperatorGroupId
  ///   Filename       : p_Operator_Get_OperatorGroupId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:56
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Operator table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null 
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.OperatorGroupId
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Operator_Get_OperatorGroupId
(
 @Operator nvarchar(50) = null 
)
as
begin
	 set nocount on
	 
  declare @OperatorGroupId int
  
  select OperatorGroupId
    from Operator
   where Operator = isnull(@Operator, Operator)
end
