﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportSOHeader_Parameter
  ///   Filename       : p_InterfaceExportSOHeader_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:13
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportSOHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceExportSOHeader.InterfaceExportSOHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportSOHeader_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceExportSOHeaderId
        ,null as 'InterfaceExportSOHeader'
  union
  select
         InterfaceExportSOHeader.InterfaceExportSOHeaderId
        ,InterfaceExportSOHeader.InterfaceExportSOHeaderId as 'InterfaceExportSOHeader'
    from InterfaceExportSOHeader
  
end
