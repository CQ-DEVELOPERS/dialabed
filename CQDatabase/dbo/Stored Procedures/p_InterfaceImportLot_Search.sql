﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportLot_Search
  ///   Filename       : p_InterfaceImportLot_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:41
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportLot table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportLot.InterfaceImportLotId,
  ///   InterfaceImportLot.RecordStatus,
  ///   InterfaceImportLot.InsertDate,
  ///   InterfaceImportLot.ProcessedDate,
  ///   InterfaceImportLot.LotId,
  ///   InterfaceImportLot.Code,
  ///   InterfaceImportLot.WhseId,
  ///   InterfaceImportLot.Whse,
  ///   InterfaceImportLot.QtyOnHand,
  ///   InterfaceImportLot.QtyFree,
  ///   InterfaceImportLot.LotStatusId,
  ///   InterfaceImportLot.LotStatus,
  ///   InterfaceImportLot.ExpiryDate,
  ///   InterfaceImportLot.ParentProductCode,
  ///   InterfaceImportLot.ExpectedYield 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportLot_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportLot.InterfaceImportLotId
        ,InterfaceImportLot.RecordStatus
        ,InterfaceImportLot.InsertDate
        ,InterfaceImportLot.ProcessedDate
        ,InterfaceImportLot.LotId
        ,InterfaceImportLot.Code
        ,InterfaceImportLot.WhseId
        ,InterfaceImportLot.Whse
        ,InterfaceImportLot.QtyOnHand
        ,InterfaceImportLot.QtyFree
        ,InterfaceImportLot.LotStatusId
        ,InterfaceImportLot.LotStatus
        ,InterfaceImportLot.ExpiryDate
        ,InterfaceImportLot.ParentProductCode
        ,InterfaceImportLot.ExpectedYield
    from InterfaceImportLot
  
end
