﻿create procedure dbo.p_Interface_WebService_MOGet
(
	@doc nvarchar(max) output
)
--with encryption
as
begin
	 declare @ReferenceNumber     varchar(50),
	         @idoc                int,
	         @xml                 nvarchar(max),
	         @JobId               int,
	         @WarehouseId         int,
	         @StorageUnitId       int,
	         @ProductId           int,
	         @SKUId               int,
	         @BatchId             int,
	         @ProductCode         nvarchar(30),
	         @Product             nvarchar(255),
	         @SKUCode             nvarchar(20),
	         @Batch               nvarchar(50),
	         @ConfirmedQuantity   float,
	         @Barcode             nvarchar(50),
	         @Location            nvarchar(50),
	         @rowcount            int
	 
	 declare @TableResult as table
	 (
	  JobId           int,
	  ReferenceNumber nvarchar(30)
	 )
	 
	 EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	 
	 SELECT @ReferenceNumber = ReferenceNumber,
	        @Location        = Location
		 FROM  OPENXML (@idoc, '/root/Header',1)
            WITH (ReferenceNumber varchar(50) 'ReferenceNumber',
                  Location        varchar(50) 'Location')
  
  IF LEFT(@ReferenceNumber, 2) = 'J:'
  BEGIN
        insert @TableResult
              (JobId,
               ReferenceNumber)
        select distinct JobId,
               isnull(ReferenceNumber,'J:' + convert(nvarchar(10), JobId))
          from Job (nolock)
         where JobId = CONVERT(Int, SUBSTRING(@ReferenceNumber, 3, LEN(@ReferenceNumber) - 2))
  END
  ELSE if LEFT(@ReferenceNumber, 2) = 'R:'
  BEGIN
        insert @TableResult
              (JobId,
               ReferenceNumber)
        select distinct  j.JobId,
               isnull(j.ReferenceNumber,'J:' + convert(nvarchar(10), j.JobId))
          from Job    j (nolock)
          join Status s (nolock) on j.StatusId = s.StatusId
         where j.ReferenceNumber = @ReferenceNumber
  END
  ELSE
  BEGIN
        insert @TableResult
              (JobId,
               ReferenceNumber)
        select distinct j.JobId,
               isnull(j.ReferenceNumber,'J:' + convert(nvarchar(10), j.JobId))
          from Job         j (nolock)
          join Status      s (nolock) on j.StatusId = s.StatusId
          join Instruction i (nolock) on j.JobId = i.JobId
          join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
          join Location    l (nolock) on i.PickLocationId = l.LocationId
         where s.StatusCode     in ('CK')
           and it.InstructionTypeCode in ('P','PM','PS','FM')
         --  and l.Location        = @Location
  END
  
  update tr
     set ReferenceNumber = tr.ReferenceNumber + isnull(' (' + od.OrderNumber + ')','')
   from @TableResult          tr
   join Instruction            i on tr.JobId = i.JobId
   join IssueLineInstruction ili on i.InstructionId = ili.InstructionId
   join OutboundDocument      od on ili.OutboundDocumentId = od.OutboundDocumentId
  
  select @rowcount = count(1) from @TableResult
  
  IF (ISNULL(@rowcount, 0) = 0)
    SELECT @XML = '<?xml version="1.0" encoding="utf-16"?><root>Reference not found</root>'
  ELSE
  BEGIN
    
    SELECT @XML = '<?xml version="1.0" encoding="utf-16"?><root>' +
        (SELECT
             tr.JobId                                 As 'JobId'
            ,tr.ReferenceNumber                       As 'ReferenceNumber'
          from @TableResult tr
        FOR XML PATH('MO')) + '</root>'
        
    END
  set @doc = @xml
end
