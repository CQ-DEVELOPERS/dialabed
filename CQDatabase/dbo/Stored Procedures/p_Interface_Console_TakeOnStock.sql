﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Console_TakeOnStock
  ///   Filename       : p_Interface_Console_TakeOnStock.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Interface_Console_TakeOnStock
(
 @WarehouseId int = null,
 @productCode nvarchar(50),
 @location     nvarchar(255),
 @batch       nvarchar(50),
 @status      nvarchar(50),
 @message     nvarchar(max),
 @recordType  nvarchar(50),
 @fromDate    datetime,
 @toDate      datetime
)
 
as
begin
	 set nocount on;
  
  select im.InterfaceMessageId,
         im.InterfaceMessageCode,
         im.InterfaceMessage,
         im.Status,
         isnull(im.InterfaceMessageCode, 'No Response') as 'InterfaceMessageCode',
         im.CreateDate as 'MessageDate',

         (select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceImportTakeOnStock') as 'InterfaceTableId',
         h.InterfaceImportTakeOnStockId as 'InterfaceId',
         h.RecordType,
         h.RecordStatus,
         h.ProcessedDate,
         h.InsertDate,
         h.ProductCode,
         h.Location,
         h.Batch,
         h.Quantity
  from InterfaceImportTakeOnStock   h (nolock)
  left
  join InterfaceMessage       im (nolock) on h.InterfaceImportTakeOnStockId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceImportTakeOnStock'
                                         
  where h.InsertDate between @fromDate and @toDate
    and isnull(h.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(h.ProductCode,'%')) + '%'
    and isnull(h.Location,'%')     like '%' + isnull(@location, isnull(h.Location,'%')) + '%'
    and isnull(h.Batch,'%')     like '%' + isnull(@Batch, isnull(h.Batch,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
end
 
 
 
