﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Printer_Get
  ///   Filename       : p_Operator_Printer_Get.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Printer_Get
(
 @OperatorId        int,
 @Printer           nvarchar(50) output,
 @Port              nvarchar(50) output,
 @IPAddress         nvarchar(50) output
)
 
as
begin
	 set nocount on;
  
  select @Printer   = Printer,
         @Port      = Port,
         @IPAddress = IPAddress
    from Operator
   where OperatorId = @OperatorId
end
