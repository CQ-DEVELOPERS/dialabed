﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Backorder_Insert
  ///   Filename       : p_Outbound_Backorder_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Backorder_Insert
(
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @IssueLineId        int = null,
 @OperatorId         int = null,
 @Lines              bit = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableIssue as table
	 (
	  IssueId int
	 )
	 
	 declare @TableIssueLine as table
	 (
	  OutboundLineId     int,
      StorageUnitBatchId int,
      RequiredQuantity   float
	 )
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @NewIssueId            int,
          @NewIssueLineId        int,
          @NewRequiredQuantity   float,
          @OutboundDocumentId    int,
          @PriorityId            int,
          @WarehouseId           int,
          @StatusId              int,
          @DeliveryNoteNumber    nvarchar(30),
          @DeliveryDate          datetime,
          @OutboundLineId        int,
          @StorageUnitBatchId    int,
          @Count                 int,
          @Delivery              int,
          @RouteId               int,
          @DropSequence          int,
          @LoadIndicator         bit,
          @DespatchBay           int,
          @OutboundDocumentTypeId int,
          @OldOutboundDocumentTypeId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @IssueLineId in ('-1','0')
    set @IssueLineId = null
  
  if @OutboundShipmentId in ('-1','0')
    set @OutboundShipmentId = null
  
  if @IssueLineId is not null
  begin
    select @IssueId = IssueId
      from IssueLine (nolock)
     where IssueLineId = @IssueLineId
    
    if @Lines = 0
      set @IssueLineId = null
  end
--  if @OutboundShipmentId is not null
--    set @IssueId = null
  
  if @IssueId is null
    insert @TableIssue
          (IssueId)
    select IssueId
      from OutboundShipmentIssue
     where OutboundShipmentId = @OutboundShipmentId
  else if @IssueId is not null
    insert @TableIssue
          (IssueId)
    select @IssueId
  
  begin transaction
  
  while exists(select top 1 1 from @TableIssue)
  begin
    select @IssueId = IssueId
      from @TableIssue
    
    delete @TableIssue where IssueId = @IssueId
    
    exec @Error = p_Issue_Update
     @IssueId             = @IssueId,
     @LoadIndicator       = 1
    
    if @Error <> 0
      goto error
    
    select @OutboundDocumentId    = OutboundDocumentId,
           @PriorityId            = PriorityId,
           @WarehouseId           = WarehouseId,
           @DeliveryNoteNumber    = DeliveryNoteNumber,
           @DeliveryDate          = DeliveryDate,
           @Delivery              = isnull(Delivery, 1) + 1,
           @RouteId               = RouteId,
           @DropSequence          = DropSequence,
           @LoadIndicator         = 0,
           @DespatchBay           = DespatchBay
      from Issue
     where IssueId = @IssueId
    
    select @StatusId = dbo.ufn_StatusId('IS', 'BP')
    
    exec @Error = p_Issue_Insert
     @IssueId             = @NewIssueId output,
     @OutboundDocumentId     = @OutboundDocumentId,
     @PriorityId            = @PriorityId,
     @WarehouseId           = @WarehouseId,
     @LocationId            = null,
     @StatusId              = @StatusId,
     @OperatorId            = null,
     @RouteId               = @RouteId,
     @DeliveryNoteNumber    = @DeliveryNoteNumber,
     @DeliveryDate          = @DeliveryDate,
     @SealNumber            = null,
     @VehicleRegistration   = null,
     @Remarks               = null,
     @DropSequence          = @DropSequence,
     @LoadIndicator         = @LoadIndicator,
     @DespatchBay           = @DespatchBay
    
    if @Error <> 0
      goto error
    
    if @IssueLineId is not null -- Release only amount requested
    begin
      insert @TableIssueLine
            (OutboundLineId,
             StorageUnitBatchId,
             RequiredQuantity)
      select OutboundLineId,
             StorageUnitBatchId,
             case when @IssueLineId is null
                  then Quantity - ConfirmedQuatity
                  else ConfirmedQuatity
                  end
        from IssueLine (nolock)
       where IssueId = @IssueId
         and IssueLineId = isnull(@IssueLineId, IssueLineId)
         and (Quantity - ConfirmedQuatity) > 0
         and ConfirmedQuatity > 0
      
      update IssueLine
         set --ConfirmedQuatity = Quantity,
             Quantity = Quantity - ConfirmedQuatity,
             ConfirmedQuatity = 0,
             StatusId = dbo.ufn_StatusId('IS','BP')
       where IssueId = @IssueId
         and IssueLineId = isnull(@IssueLineId, IssueLineId)
         and (Quantity - ConfirmedQuatity) > 0
         and ConfirmedQuatity > 0
      
      set @Count = @@rowcount
    end
    
    if @IssueLineId is null -- Release full quantity for release order
    begin
      insert @TableIssueLine
            (OutboundLineId,
             StorageUnitBatchId,
             RequiredQuantity)
      select OutboundLineId,
             StorageUnitBatchId,
             case when @IssueLineId is null
                  then Quantity - ConfirmedQuatity
                  else ConfirmedQuatity
                  end
        from IssueLine (nolock)
       where IssueId = @IssueId
         and IssueLineId = isnull(@IssueLineId, IssueLineId)
         and (Quantity - ConfirmedQuatity) > 0
      
      update IssueLine
         set ConfirmedQuatity = Quantity,
             StatusId = dbo.ufn_StatusId('IS','W')
       where IssueId = @IssueId
         and IssueLineId = isnull(@IssueLineId, IssueLineId)
         and (Quantity - ConfirmedQuatity) > 0
      
      set @Count = @@rowcount
    end
    
    if isnull(@Count,0) <= 0
    begin
      rollback transaction
      return 0
    end
    
    while @Count > 0
    begin
      set @Count = @Count - 1
      
      select top 1 @OutboundLineId       = OutboundLineId,
                   @StorageUnitBatchId  = StorageUnitBatchId,
                   @NewRequiredQuantity = RequiredQuantity
        from @TableIssueLine
      
      delete @TableIssueLine
       where OutboundLineId      = @OutboundLineId
         and StorageUnitBatchId = @StorageUnitBatchId
      
      exec @Error = p_IssueLine_Insert
       @IssueLineId          = @NewIssueLineId output,
       @IssueId              = @NewIssueId,
       @OutboundLineId       = @OutboundLineId,
       @StorageUnitBatchId   = @StorageUnitBatchId,
       @StatusId             = @StatusId,
       @OperatorId           = null,
       @Quantity             = @NewRequiredQuantity,
       @ConfirmedQuatity     = 0
      
      if @Error <> 0
        goto error
    end
  end
  
  select @OldOutboundDocumentTypeId = odt.OutboundDocumentTypeId
    from OutboundDocument      od (nolock)
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where od.OutboundDocumentId = @OutboundDocumentId
     and odt.OutboundDocumentTypeCode in ('BORD','BAC')
  
  select @OutboundDocumentTypeId = OutboundDocumentTypeId
    from OutboundDocumentType (nolock)
   where OutboundDocumentTypeCode = 'SAL'
  
  if @OldOutboundDocumentTypeId is not null
  begin
    exec @Error = p_OutboundDocument_Update
     @OutboundDocumentId = @OutboundDocumentId,
     @OutboundDocumentTypeId = @OutboundDocumentTypeId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_OutboundDocument_Update (@OldOutboundDocumentTypeId)'
      goto error
    end
  end
  
  exec @Error = p_Outbound_Auto_Load
   @OutboundShipmentId = @OutboundShipmentId output,
   @IssueId            = @NewIssueId
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
    goto error
  end
  
  exec @Error = p_Outbound_Auto_Release
   @OutboundShipmentId = @OutboundShipmentId,
   @IssueId            = @NewIssueId
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
    goto error
  end
  
  if @OldOutboundDocumentTypeId is not null
  begin
    exec @Error = p_OutboundDocument_Update
     @OutboundDocumentId = @OutboundDocumentId,
     @OutboundDocumentTypeId = @OldOutboundDocumentTypeId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_OutboundDocument_Update (@OldOutboundDocumentTypeId)'
      goto error
    end
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Backorder_Insert'
    rollback transaction
    return @Error
end
