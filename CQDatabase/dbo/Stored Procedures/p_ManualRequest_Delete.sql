﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualRequest_Delete
  ///   Filename       : p_ManualRequest_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualRequest_Delete
(
 @OutboundLineId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @IssueLineId       int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_OutboundLine_Delete_Line
   @OutboundLineId = @OutboundLineId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_ManualRequest_Delete'
    rollback transaction
    return @Error
end
