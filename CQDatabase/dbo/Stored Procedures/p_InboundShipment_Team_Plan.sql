﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Team_Plan
  ///   Filename       : p_InboundShipment_Team_Plan.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Team_Plan
(
 @InboundShipmentId int = null
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   InboundShipmentId int,
   InstructionTypeId  int,
   InstructionType    nvarchar(30),
   RequiredQuantity   numeric(13,6),
   DockTime           datetime,
   OperatorGroup      nvarchar(30),
   TeamStart          datetime,
   TeamEnd            datetime
  )
  
  declare @TableTeams as table
  (
   OperatorGroupId    int,
   OperatorGroup      nvarchar(30),
   InstructionTypeId  int,
   HourlyPickRate     numeric(13,6),
   Available          datetime,
   RequiredStart      datetime
  )
  
  insert @TableResult
        (InboundShipmentId,
         InstructionTypeId,
         RequiredQuantity,
         OperatorGroup,
         TeamStart,
         TeamEnd,
         DockTime)
  select oi.InboundShipmentId,
         oi.InstructionTypeId,
         oi.Quantity,
         og.OperatorGroup,
         ts.PlannedStart,
         ts.PlannedEnd,
         ds.PlannedStart
    from InboundShipmentInstructionType oi (nolock)
    left
    join DockSchedule                    ds (nolock) on oi.InboundShipmentId = ds.InboundShipmentId
    left
    join TeamSchedule                    ts (nolock) on oi.InboundShipmentId = ts.InboundShipmentId
                                                    and oi.InstructionTypeId = ts.InstructionTypeId
    left
    join OperatorGroup                   og (nolock) on ts.OperatorGroupId = og.OperatorGroupId
   where oi.InboundShipmentId = @InboundShipmentId
  
  update tr
     set InstructionType = it.InstructionType
    from @TableResult    tr
    join InstructionType it (nolock) on tr.InstructionTypeId = it.InstructionTypeId
  
  select InboundShipmentId,
         InstructionType,
         RequiredQuantity,
         OperatorGroup,
         convert(nvarchar(5), TeamStart, 108) as 'TeamStart',
         convert(nvarchar(5), TeamEnd, 108) as 'TeamEnd',
         convert(nvarchar(5), DockTime, 108) as 'DockTime'
    from @TableResult
end
