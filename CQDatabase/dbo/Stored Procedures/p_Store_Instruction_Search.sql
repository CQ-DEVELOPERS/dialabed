﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Store_Instruction_Search
  ///   Filename       : p_Store_Instruction_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Store_Instruction_Search
(
 @warehouseId int,
 @palletId    int,
 @jobId       int,
 @fromDate    datetime,
 @toDate      datetime,
 @statusId    int,
 @productCode nvarchar(30),
 @product     nvarchar(255),
 @skuCode     nvarchar(50),
 @batch       nvarchar(30)
)
 
as
begin
	 set nocount on;
  declare @TableHeader as table
  (
   OrderNumber       nvarchar(30),
   InboundShipmentId int null,
   ReceiptId         int,
   ReceiptLineId     int,
   NumberOfPallets		  int
  )
  
  declare @TableResult as table
  (
   InstructionId      int,
   ReceiptLineId      int,
   StorageUnitBatchId int,
   BatchId            int,
   ProductCode	       nvarchar(30) null,
   Product	           nvarchar(255) null,
   SKUCode            nvarchar(50) null,
   Quantity           float null,
   JobId              int null,
   Status             nvarchar(50) null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   StoreLocationId    int null,
   StoreLocation      nvarchar(15) null,
   PalletId           int null,
   CreateDate         datetime null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   InstructionType	   nvarchar(30),
   OperatorId         int null,
   Operator           nvarchar(50) null,
   Dwell              nvarchar(50) null,
   StockLevel         nvarchar(50) null
  )
  
  declare @StatsuCode nvarchar(10)
  
  if @palletId = -1
    set @palletId = null
  
  if @jobId = -1
    set @jobId = null
  
  if @statusId = -1
    set @statusId = null
  
  select @StatsuCode = StatusCode
    from Status (nolock)
   where StatusId = @statusId
  
  insert @TableResult
        (InstructionId,
         ReceiptLineId,
         StorageUnitBatchId,
         Quantity,
         JobId,
         Status,
         PickLocationId,
         StoreLocationId,
         PalletId,
         CreateDate,
         InstructionType,
         OperatorId)
  select i.InstructionId,
         i.ReceiptLineId,
         i.StorageUnitBatchId,
         i.Quantity,
         i.JobId,
         s.Status,
         i.PickLocationId,
         i.StoreLocationId,
         i.PalletId,
         i.CreateDate,
         it.InstructionType,
         i.OperatorId
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status           s (nolock) on j.StatusId          = s.StatusId
    join Status          si (nolock) on i.StatusId          = si.StatusId
   where i.CreateDate      between @FromDate and @ToDate
     and s.Type                  = 'R'
     and s.StatusCode           in ('RC','PR','LA','RE')
     and si.StatusCode          in ('W','S')
     and i.JobId                 = isnull(@JobId, i.JobId)
     and isnull(i.PalletId,-1)   = isnull(@palletId, isnull(i.PalletId,-1))
     --and s.StatusId              = isnull(@StatusId, s.StatusId)
     and s.StatusCode            = isnull(@StatsuCode, s.StatusCode)
     and it.InstructionTypeCode in ('S','SM')
     and i.WarehouseId           = @warehouseId
  
  insert @TableHeader
        (OrderNumber,
         InboundShipmentId,
         ReceiptId,
         ReceiptLineId,
         NumberOfPallets)
  select distinct
         id.OrderNumber,
         isr.InboundShipmentId,
         r.ReceiptId,
         rl.ReceiptLineId,
         rl.NumberOfPallets
    from InboundDocument         id (nolock)
    join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join ReceiptLine             rl (nolock) on r.ReceiptId          = rl.ReceiptId
    left outer
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId          = isr.ReceiptId
    join @TableResult            tr          on rl.ReceiptLineId     = tr.ReceiptLineId
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         BatchId       = b.BatchId,
         Batch         = b.Batch,
         ECLNumber     = b.ECLNumber
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId

  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId

  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator     o  (nolock) on tr.OperatorId = o.OperatorId
  
  update @TableResult
     set Dwell = 'RedFlag.gif'
   where datediff(hh, CreateDate, GetDate()) > 24
  
  update @TableResult
     set Dwell = 'GreenFlag.gif'
   where datediff(hh, CreateDate, GetDate()) < 6
     and Dwell is null
  
  update @TableResult
     set Dwell = 'YellowFlag.gif'
   where datediff(hh, CreateDate, GetDate()) > 6
     and Dwell is null
  
  update @TableResult
     set StockLevel = 'RedFlag.gif'
   where datediff(hh, CreateDate, GetDate()) > 24
  
  update @TableResult
     set StockLevel = 'GreenFlag.gif'
   where datediff(hh, CreateDate, GetDate()) < 6
     and StockLevel is null
  
  update @TableResult
     set StockLevel = 'YellowFlag.gif'
   where datediff(hh, CreateDate, GetDate()) > 6
     and StockLevel is null
  
  select InstructionId,
         ProductCode,
         Product,
         SKUCode,
         Quantity,
         JobId,
         Status,
         PickLocation,
         StoreLocation,
         PalletId,
         CreateDate,
         Batch,
         ECLNumber,
         OrderNumber,
         InboundShipmentId,
         InstructionType,
         Operator,
         Dwell,
         StockLevel,
         tr.ReceiptLineId,
         BatchId,
         isnull(NumberOfPallets,999) as NumberOfPallets
    from @TableHeader th
    join @TableResult tr on th.ReceiptLineId = tr.ReceiptLineId
   where ProductCode like '%' + @productCode + '%'
     and Product     like '%' + @product + '%'
     and SKUCode     like '%' + @skuCode + '%'
     and Batch       like '%' + @batch + '%'
end
