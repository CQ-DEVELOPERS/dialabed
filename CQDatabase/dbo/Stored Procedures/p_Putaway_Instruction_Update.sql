﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Putaway_Instruction_Update
  ///   Filename       : p_Putaway_Instruction_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Putaway_Instruction_Update
(
 @instructionId     int,
 @confirmedQuantity float,
 @operatorId        int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  set @Errormsg = 'Error executing p_Putaway_Instruction_Update'
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @confirmedQuantity > (Select Quantity from Instruction where InstructionId = @instructionId)
  begin
    set @ErrorMsg = 'Confirmed quantity cannot be greater than expected.'
  end
  
  exec @Error = p_Instruction_Update
   @InstructionId     = @instructionId,
   @ConfirmedQuantity = @confirmedQuantity,
   @OperatorId        = @operatorId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @ErrorMsg
    rollback transaction
    return @Error
end
