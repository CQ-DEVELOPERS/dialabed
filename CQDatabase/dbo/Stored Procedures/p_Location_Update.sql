﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Update
  ///   Filename       : p_Location_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2013 08:13:21
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Location table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null,
  ///   @LocationTypeId int = null,
  ///   @Location nvarchar(30) = null,
  ///   @Ailse nvarchar(20) = null,
  ///   @Column nvarchar(20) = null,
  ///   @Level nvarchar(20) = null,
  ///   @PalletQuantity float = null,
  ///   @SecurityCode int = null,
  ///   @RelativeValue numeric(13,3) = null,
  ///   @NumberOfSUB int = null,
  ///   @StocktakeInd bit = null,
  ///   @ActiveBinning bit = null,
  ///   @ActivePicking bit = null,
  ///   @Used bit = null,
  ///   @Latitude decimal(16,13) = null,
  ///   @Longitude decimal(16,13) = null,
  ///   @Direction nvarchar(20) = null,
  ///   @Height numeric(13,6) = null,
  ///   @Length numeric(13,6) = null,
  ///   @Width numeric(13,6) = null,
  ///   @Volume numeric(13,6) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter
  ///   Modified Date  : April 2018
  ///   Details        : CQPMO-2806: Allowed for alpha numeric security codes.
  /// </newpara>
*/
CREATE procedure p_Location_Update
(
 @LocationId int = null,
 @LocationTypeId int = null,
 @Location nvarchar(30) = null,
 @Ailse nvarchar(20) = null,
 @Column nvarchar(20) = null,
 @Level nvarchar(20) = null,
 @PalletQuantity float = null,
 @SecurityCode NVARCHAR(50) = null, --CQPMO-2806: Allowed for alpha numeric security codes.
 @RelativeValue numeric(13,3) = null,
 @NumberOfSUB int = null,
 @StocktakeInd bit = null,
 @ActiveBinning bit = null,
 @ActivePicking bit = null,
 @Used bit = null,
 @Latitude decimal(16,13) = null,
 @Longitude decimal(16,13) = null,
 @Direction nvarchar(20) = null,
 @Height numeric(13,6) = null,
 @Length numeric(13,6) = null,
 @Width numeric(13,6) = null,
 @Volume numeric(13,6) = null 
)
 
as
begin
	 set nocount on;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @LocationTypeId = '-1'
    set @LocationTypeId = null;
  
  if @Location = '-1'
    set @Location = null;
  
	 declare @Error int
 
  update Location
     set LocationTypeId = isnull(@LocationTypeId, LocationTypeId),
         Location = isnull(@Location, Location),
         Ailse = isnull(@Ailse, Ailse),
         [Column] = isnull(@Column, [Column]),
         Level = isnull(@Level, Level),
         PalletQuantity = isnull(@PalletQuantity, PalletQuantity),
         SecurityCode = isnull(@SecurityCode, SecurityCode),
         RelativeValue = isnull(@RelativeValue, RelativeValue),
         NumberOfSUB = isnull(@NumberOfSUB, NumberOfSUB),
         StocktakeInd = isnull(@StocktakeInd, StocktakeInd),
         ActiveBinning = isnull(@ActiveBinning, ActiveBinning),
         ActivePicking = isnull(@ActivePicking, ActivePicking),
         Used = isnull(@Used, Used),
         Latitude = isnull(@Latitude, Latitude),
         Longitude = isnull(@Longitude, Longitude),
         Direction = isnull(@Direction, Direction),
         Height = isnull(@Height, Height),
         Length = isnull(@Length, Length),
         Width = isnull(@Width, Width),
         Volume = isnull(@Volume, Volume) 
   where LocationId = @LocationId
  
  select @Error = @@Error
  
  
  return @Error
  
end
