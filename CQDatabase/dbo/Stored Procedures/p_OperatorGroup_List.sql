﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroup_List
  ///   Filename       : p_OperatorGroup_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2012 07:59:18
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroup table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OperatorGroup.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroup_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
  select
         '-1' as OperatorGroupId,
         '{All}' as OperatorGroup 
  union
  select
         OperatorGroup.OperatorGroupId,
         OperatorGroup.OperatorGroup 
    from OperatorGroup
  
end
