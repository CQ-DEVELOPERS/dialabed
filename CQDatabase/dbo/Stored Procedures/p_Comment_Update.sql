﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Update
  ///   Filename       : p_Comment_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2014 08:27:23
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Comment table.
  /// </remarks>
  /// <param>
  ///   @CommentId int = null,
  ///   @Comment nvarchar(max) = null,
  ///   @OperatorId int = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null,
  ///   @ReadDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Update
(
 @CommentId int = null,
 @Comment nvarchar(max) = null,
 @OperatorId int = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @ReadDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @CommentId = '-1'
    set @CommentId = null;
  
  if @Comment = '-1'
    set @Comment = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
	 declare @Error int
 
  update Comment
     set Comment = isnull(@Comment, Comment),
         OperatorId = isnull(@OperatorId, OperatorId),
         CreateDate = isnull(@CreateDate, CreateDate),
         ModifiedDate = isnull(@ModifiedDate, ModifiedDate),
         ReadDate = isnull(@ReadDate, ReadDate) 
   where CommentId = @CommentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
