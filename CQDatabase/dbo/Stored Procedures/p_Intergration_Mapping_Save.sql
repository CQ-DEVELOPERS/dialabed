﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Intergration_Mapping_Save
  ///   Filename       : p_Intergration_Mapping_Save.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Mar 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Intergration_Mapping_Save
(
 @interfaceMappingFileId   int,
 @interfaceMappingColumnId int,
 @interfaceFieldId         int,
 @orderBy                  int,
 @startPosition            int,
 @endPosition              int,
 @format                   nvarchar(30)
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Intergration_Mapping_Save',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @InterfaceDocumentTypeId int,
          @InterfaceFieldCode      nvarchar(30),
          @InterfaceField          nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @interfaceMappingColumnId = -1
    set @interfaceMappingColumnId = null
       
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  select @InterfaceDocumentTypeId = InterfaceDocumentTypeId
    from InterfaceMappingFile (nolock)
   where InterfaceMappingFileId = @interfaceMappingFileId
  
  select @InterfaceFieldCode = InterfaceFieldCode,
         @InterfaceField     = InterfaceField
    from InterfaceField (nolock)
   where InterfaceFieldId = @interfaceFieldId
  
  if @InterfaceMappingColumnId is null
  begin
    insert InterfaceMappingColumn
          (InterfaceDocumentTypeId,
           InterfaceMappingFileId,
           InterfaceMappingColumnCode,
           InterfaceMappingColumn,
           InterfaceFieldId,
           ColumnNumber,
           StartPosition,
           EndPostion,
           Format)
    select @InterfaceDocumentTypeId,
           @InterfaceMappingFileId,
           @InterfaceFieldCode,
           @InterfaceField,
           @interfaceFieldId,
           @orderBy,
           @startPosition,
           @endPosition,
           @format
    
    if @Error <> 0
      goto error
    end
  else
  begin
    update InterfaceMappingColumn
       set ColumnNumber                = @orderBy,
           StartPosition               = @startPosition,
           EndPostion                  = @endPosition,
           Format                      = @format
     where InterfaceMappingColumnId = @InterfaceMappingColumnId
    
    if @Error <> 0
      goto error
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
