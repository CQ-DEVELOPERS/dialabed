﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_StorageUnitLocation_Search
  ///   Filename       : p_Static_StorageUnitLocation_Search.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 19 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@StorageUnitId> 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_StorageUnitLocation_Search
(
 @StorageUnitId            int,
 @WarehouseId				int
)
 
as
begin
	 set nocount on;

SELECT     sul.StorageUnitId, l.LocationId, l.Location, sul.MinimumQuantity, sul.HandlingQuantity, sul.MaximumQuantity
FROM				     StorageUnitLocation 		sul (nolock)  
					JOIN Location					l  (nolock)	ON sul.LocationId	=	l.LocationId
					join areaLocation				al (nolock)	on al.LocationID	=	l.LocationId
					join Area						a  (nolock)	on a.AreaId			=	al.AreaId	
                      Where sul.StorageUnitId = @StorageUnitId  
						and a.WarehouseId	= @WarehouseId
 
end
