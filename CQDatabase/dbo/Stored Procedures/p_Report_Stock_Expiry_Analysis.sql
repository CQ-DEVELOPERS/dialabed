﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Expiry_Analysis
  ///   Filename       : p_Report_Stock_Expiry_Analysis.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 21 July 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : March 2011
  ///   Details        : Add weight for Enterprise
  /// </newpara>
*/
CREATE procedure p_Report_Stock_Expiry_Analysis
(
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @BatchId           int = null,
 @Dayvalue          int = 0
)

 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   StorageUnitId				int, 
   SKUId					    int, 
   ProductId				    int, 
   StorageUnitBatchId		    int,
   StatusId				        int, 
   ProductCode				    nvarchar(50), 
   Product					    nvarchar(50), 
   Barcode					    nvarchar(50), 
   Status					    nvarchar(50),
   BatchId					    int,
   WareHouseID				    int,
   Batch					    nvarchar(50),
   CreateDate				    datetime,
   ExpiryDate				    datetime,
   IncubationDays				int,
   ShelfLifeDays			    int,
   ShelfLifeDaysRemaining		int,
   ShelfLifePercentRemaining	numeric(10,2),
   Quantity				        float,
   NettWeight					numeric(13,6),
   DCLExpiryDate			    datetime, 
   DCLDaysRemaining				int,
   Location						nvarchar(15),
   DCLDays						int,
   DCLPercentRemaining			numeric(10,2)
  )
  
  Declare @datefrom               datetime,
          @ShelfLifeDaysRemaining int,
          @Incubationdays         int,
          @DefaultDCLDays		  float
  
  if @Dayvalue is null or @Dayvalue = 0
    set @Dayvalue = 1
  
  --Set @datefrom = DateAdd(D,Convert(int,@Days * -1),getdate())
  
  if @StorageUnitId = -1
    set @StorageUnitId = null
  
  if @BatchId = -1
    set @BatchId = null
    
 
  select @DefaultDCLDays = convert(float, Value)
  from Configuration
  where ConfigurationId = 228

  print @DefaultDCLDays
  set   @DefaultDCLDays = @DefaultDCLDays/100
  print @DefaultDCLDays
  insert @TableResult
        (StorageUnitId, 
         SKUId, 
         ProductId, 
         StorageUnitBatchId,
         WarehouseId,
         ProductCode,			
         Product,				
         Barcode,				
         Status,				
         BatchId,
         Batch,				
         CreateDate,
         ExpiryDate,
         IncubationDays,       
         ShelfLifeDays,
         Quantity,
         Location)
  SELECT su.StorageUnitId, 
         su.SKUId, 
         su.ProductId, 
         sub.StorageUnitBatchId,
         @WarehouseId,
         p.ProductCode, 
         p.Product, 
         null,
         s.Status,				
         b.BatchId,
         b.Batch, 
         b.CreateDate,
         b.ExpiryDate,
         isnull(b.IncubationDays, (select Incubationdays From Batch Where Batch = 'Default')),
         isnull(p.ShelfLifeDays, datediff(dd, CreateDate, ExpiryDate)),
         subl.ActualQuantity,
         vl.Location
    FROM StorageUnitBatchLocation subl (nolock)
    join viewLocation               vl          on subl.LocationId         = vl.LocationId
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
   WHERE b.Batch != 'Default' 
     and su.StorageUnitId    = isnull(@StorageUnitId, su.StorageUnitId)
     and b.BatchId           = isnull(@BatchId, b.BatchId)
--     and b.CreateDate       <= isnull(@datefrom,'')
     and subl.ActualQuantity > 0
     and vl.StockOnHand      = 1
     and vl.WarehouseId      = @WarehouseId
  
  update @TableResult
	    Set ExpiryDate = DateAdd(D,ShelfLifeDays,CreateDate)
   where ExpiryDate is null
  
  update @TableResult
	    Set ShelfLifeDaysRemaining = DateDiff(D,getDate(),ExpiryDate)
  
  update @TableResult
	    Set ShelfLifeDaysRemaining = 0
		 where ShelfLifeDaysRemaining <= 0
  
  update @TableResult
	    Set ShelfLifePercentRemaining = Convert(float,ShelfLifeDaysRemaining) / Convert(float,ShelfLifeDays) * 100
  where Convert(float,ShelfLifeDays) > 0
  
  update @TableResult
	    Set ShelfLifePercentRemaining = 0
	  where ShelfLifePercentRemaining is null
  
  update @TableResult
     set ShelfLifePercentRemaining = 100
   where ShelfLifePercentRemaining > 100
   
    declare @InboundSequence smallint,
		 @PackTypeId	int,
		 @PackType   	nvarchar(30),
		 @ShowWeight	bit = null
		 
  select @ShowWeight = Indicator
  from Configuration where ConfigurationId = 206
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence
  
  if  @ShowWeight = 1
     update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   where pk.PackTypeId = @PackTypeId
                       and p.ProductCode = tr.ProductCode
                       and isnull(su.ProductCategory,'') != 'V')* tr.Quantity)
    from @TableResult tr
  
 --  update @TableResult
--	    Set DCLDaysRemaining = (DateDiff(D,getDate(),ExpiryDate) * 0.3333)
	    
   update @TableResult
    Set DCLDays = (ShelfLifeDays * @DefaultDCLDays)
   
   update @TableResult
    Set DCLExpiryDate = DateAdd(D,DCLDays,CreateDate)
   where DCLExpiryDate is null
   
  update @TableResult
    Set DCLDaysRemaining = DateDiff(D,getDate(),DCLExpiryDate)
    
  update @TableResult
     set DCLDaysRemaining = 0
   where DCLDaysRemaining < 0
   
  update @TableResult
	    Set DCLPercentRemaining = Convert(float,DCLDaysRemaining) / Convert(float,DCLDays) * 100
  where Convert(float,DCLDays) > 0
  
  update @TableResult
	    Set DCLPercentRemaining = 0
	  where DCLPercentRemaining is null
  
  update @TableResult
     set DCLPercentRemaining = 100
   where DCLPercentRemaining > 100
  
  select StorageUnitId, 
         SKUId, 
         ProductId, 
         StorageUnitBatchId,
         WarehouseId,
         ProductCode,				
         Product,					
         Barcode,					
         Status,					
         BatchId,
         Batch,					
         CreateDate,				
         ExpiryDate,				
         IncubationDays,			
         ShelfLifeDays,			
         ShelfLifeDaysRemaining,  
         ShelfLifePercentRemaining,
         sum(Quantity) as 'Quantity',
         NettWeight,
         Location,
		 DCLExpiryDate, 
         DCLDaysRemaining,
         DCLPercentRemaining
    from    @TableResult
    where   DCLDaysRemaining <= @Dayvalue
   group by StorageUnitId, 
            SKUId, 
            ProductId, 
			StorageUnitBatchId,
            WarehouseId,
            ProductCode,				
            Product,					
            Barcode,					
            Status,					
            BatchId,
            Batch,					
            CreateDate,				
            ExpiryDate,				
            IncubationDays,
            ShelfLifeDays,			
            ShelfLifeDaysRemaining,  
            ShelfLifePercentRemaining,
            NettWeight,
            Location,
			DCLExpiryDate, 
			DCLDaysRemaining,
			DCLPercentRemaining
  order by  ShelfLifePercentRemaining
  
end
