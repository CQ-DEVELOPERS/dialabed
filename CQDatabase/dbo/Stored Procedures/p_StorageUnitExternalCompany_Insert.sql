﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitExternalCompany_Insert
  ///   Filename       : p_StorageUnitExternalCompany_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:56
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null output,
  ///   @ExternalCompanyId int = null output,
  ///   @AllocationCategory numeric(13,3) = null,
  ///   @MinimumShelfLife numeric(13,3) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @DefaultQC bit = null,
  ///   @SendToQC bit = null 
  /// </param>
  /// <returns>
  ///   StorageUnitExternalCompany.StorageUnitId,
  ///   StorageUnitExternalCompany.ExternalCompanyId,
  ///   StorageUnitExternalCompany.AllocationCategory,
  ///   StorageUnitExternalCompany.MinimumShelfLife,
  ///   StorageUnitExternalCompany.ProductCode,
  ///   StorageUnitExternalCompany.SKUCode,
  ///   StorageUnitExternalCompany.DefaultQC,
  ///   StorageUnitExternalCompany.SendToQC 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitExternalCompany_Insert
(
 @StorageUnitId int = null output,
 @ExternalCompanyId int = null output,
 @AllocationCategory numeric(13,3) = null,
 @MinimumShelfLife numeric(13,3) = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(20) = null,
 @DefaultQC bit = null,
 @SendToQC bit = null 
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
	 declare @Error int
 
  insert StorageUnitExternalCompany
        (StorageUnitId,
         ExternalCompanyId,
         AllocationCategory,
         MinimumShelfLife,
         ProductCode,
         SKUCode,
         DefaultQC,
         SendToQC)
  select @StorageUnitId,
         @ExternalCompanyId,
         @AllocationCategory,
         @MinimumShelfLife,
         @ProductCode,
         @SKUCode,
         @DefaultQC,
         @SendToQC 
  
  select @Error = @@Error, @ExternalCompanyId = scope_identity()
  
  
  return @Error
  
end
