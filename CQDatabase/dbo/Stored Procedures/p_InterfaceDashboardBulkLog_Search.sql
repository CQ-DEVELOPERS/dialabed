﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDashboardBulkLog_Search
  ///   Filename       : p_InterfaceDashboardBulkLog_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:55
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceDashboardBulkLog table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceDashboardBulkLog.FileKeyId,
  ///   InterfaceDashboardBulkLog.FileType,
  ///   InterfaceDashboardBulkLog.ProcessedDate,
  ///   InterfaceDashboardBulkLog.FileName,
  ///   InterfaceDashboardBulkLog.ErrorCode,
  ///   InterfaceDashboardBulkLog.ErrorDescription 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDashboardBulkLog_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceDashboardBulkLog.FileKeyId
        ,InterfaceDashboardBulkLog.FileType
        ,InterfaceDashboardBulkLog.ProcessedDate
        ,InterfaceDashboardBulkLog.FileName
        ,InterfaceDashboardBulkLog.ErrorCode
        ,InterfaceDashboardBulkLog.ErrorDescription
    from InterfaceDashboardBulkLog
  
end
