﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UOM_Delete
  ///   Filename       : p_UOM_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:00
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the UOM table.
  /// </remarks>
  /// <param>
  ///   @UOMId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_UOM_Delete
(
 @UOMId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete UOM
     where UOMId = @UOMId
  
  select @Error = @@Error
  
  
  return @Error
  
end
