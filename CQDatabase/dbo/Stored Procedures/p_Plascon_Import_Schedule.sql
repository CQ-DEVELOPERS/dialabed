﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Import_Schedule
  ///   Filename       : p_Plascon_Import_Schedule.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Plascon_Import_Schedule
as
begin
  
  declare @Table as table
  (
   FloScheduleImportId int
  )
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @Route              varchar(50),
          @Driver             varchar(50),
          @Vehicle            varchar(50),
          @VehicleClass       varchar(20),
          @StopNumber         int        ,
          @UniqueNumber       varchar(20),
          @DeliveryPoint      varchar(50),
          @CustomerNumber     varchar(40),
          @CompanyName        varchar(255),
          @PickupTime         varchar(20),
          @Arrival            varchar(20),
          @TimeforJob         int        ,
          @WarehouseId        int,
          @OutboundShipmentId int,
          @IssueId            int,
          @DeliveryDate       datetime,
          @LocationId         int,
          @DropSequence       int,
          @TotalOrders        int,
          @RouteId            int,
          @FloScheduleImportId int,
          @count               int
        
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  declare @TableName nvarchar(128)
  
  select @TableName = '##' + db_name() + '_' + OBJECT_NAME(@@PROCID);
  
  if(object_id('tempdb.dbo.' + @TableName) is null)
    execute ('create table ' + @TableName + ' (objname sysname)')
  else
  begin
    select @TableName + ' Already Running'
    return
  end
  
  insert @Table
        (FloScheduleImportId)
  select FloScheduleImportId
    from FloScheduleImport
   where RecordStatus = 'N'
  
  select @count = count(1) from @Table
  
  while @count > 0
  begin
    select @count = @count - 1
    
    set @FloScheduleImportId = null
    
    select top 1 @FloScheduleImportId = FloScheduleImportId from @Table
    
    delete @Table where FloScheduleImportId = @FloScheduleImportId
    
    select @Route          = Route,
           @Driver         = Driver,
           @Vehicle        = Vehicle,
           @VehicleClass   = VehicleClass,
           @StopNumber     = StopNumber,
           @UniqueNumber   = UniqueNumber,
           @DeliveryPoint  = DeliveryPoint,
           @CustomerNumber = CustomerNumber,
           @CompanyName    = CompanyName,
           @PickupTime     = PickupTime,
           @Arrival        = Arrival,
           @TimeforJob     = TimeforJob,
           @DropSequence   = LineNumber,
           @TotalOrders    = CheckSummary,
           @DeliveryDate   = DeliveryDate
      from FloScheduleImport (nolock)
     where FloScheduleImportId = @FloScheduleImportId
    
    begin transaction xml_import
    
    set @RouteId = null
    
    select @RouteId = RouteId
      from Route (nolock)
     where Route = @Route
    
    if @RouteId is null
    begin
      exec @Error = p_Route_Insert
       @RouteId   = @RouteId output,
       @Route     = @Route,
       @RouteCode = @Route 
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Shipment_Edit'
        goto error
      end
    end
    
    set @IssueId = null
    
    select @IssueId      = i.IssueId,
           @WarehouseId  = i.WarehouseId
      from OutboundDocument       od (nolock)
      join Issue                   i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
      join Status                  s (nolock) on i.StatusId            = s.StatusId
      left
      join OutboundShipmentIssue osi (nolock) on i.IssueId             = osi.IssueId
     where od.OrderNumber          = @UniqueNumber
       and s.StatusCode            = 'W'
       and osi.OutboundShipmentId is null
    
    if @IssueId is null
    begin
      set @Error = -1
      select @ErrorMsg = 'Error executing @IssueId is null ' + @UniqueNumber
      goto Error
    end
    
    set @OutboundShipmentId = null
    
    select @OutboundShipmentId = OutboundShipmentId
      from OutboundShipment (nolock)
     where WarehouseId  = @WarehouseId
       and RouteId      = @RouteId
       and ShipmentDate = @DeliveryDate
       and TotalOrders  = @TotalOrders
    
    if @OutboundShipmentId is null -- Create Shipment for route
    begin
      exec @Error = p_OutboundShipment_Create
       @WarehouseId        = @WarehouseId,
       @ShipmentDate       = @DeliveryDate,
       @Remarks            = 'Inserted via FLO Schedule',
       @OutboundShipmentId = @OutboundShipmentId output,
       @RouteId            = @RouteId,
       @TotalOrders        = @TotalOrders
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundShipment_Create'
        goto error
      end
    end
    
    exec @Error = p_OutboundShipmentIssue_Link
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @DropSequence       = @DropSequence
           
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_OutboundShipmentIssue_Link'
      goto error
    end
    
    exec @Error = p_Issue_Update
     @IssueId            = @IssueId,
     @DeliveryDate       = @DeliveryDate
           
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_OutboundShipmentIssue_Link'
      goto error
    end
    
    error:
    
    if @Error = 0
    begin
      update FloScheduleImport
         set RecordStatus = 'C'
       where FloScheduleImportId = @FloScheduleImportId
      
      if @@trancount > 0
        commit transaction xml_import
    end
    else
    begin
      if @@trancount > 0
        rollback transaction xml_import
      
      update FloScheduleImport
         set RecordStatus = 'E',
             ErrorMsg     = @ErrorMsg
       where FloScheduleImportId = @FloScheduleImportId
    end
  end
  
  --exec @Error = p_Outbound_Auto_Release
  -- @outboundShipmentId = @OutboundShipmentId,
  -- @IssueId            = @IssueId
  
  --if @Error != 0
  --begin
  --  select @ErrorMsg = 'Error executing p_Outbound_Auto_Release'
  --  goto error
  --end
  
  execute ('Drop  table ' + @TableName)
  
  return
end
