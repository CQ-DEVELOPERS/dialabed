﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Company_Parameter
  ///   Filename       : p_Company_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:51
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Company table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Company.CompanyId,
  ///   Company.Company 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Company_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as CompanyId
        ,'{All}' as Company
  union
  select
         Company.CompanyId
        ,Company.Company
    from Company
  
end
