﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Delete
  ///   Filename       : p_Job_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Aug 2014 12:07:51
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Job table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Delete
(
 @JobId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Job
     where JobId = @JobId
  
  select @Error = @@Error
  
  
  return @Error
  
end
