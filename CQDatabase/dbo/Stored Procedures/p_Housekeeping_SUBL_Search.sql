﻿/*
      <summary>
        Procedure Name : p_Housekeeping_SUBL_Search
        Filename       : p_Housekeeping_SUBL_Search.sql
        Create By      : Grant Schultz
        Date Created   : 23 May 2007 09:32:20
      </summary>
      <remarks>
        Searches for available product and location to move / replenish from
      </remarks>
      <param>
        @WarehouseId         int,
        @InstructionTypeCode nvarchar(10),
        @StorageUnitBatchId  int = null,
        @StorageUnitId       int = null,
        @ProductId           int = null,
        @SKUId               int = null,
        @BatchId             int = null,
        @PickLocationId          int = null
      </param>
      <returns>
        StorageUnitBatchId,
        LocationId,
        Location,
        ProductCode,
        Product,
        ProductStatus,
        SKUCode,
        SKU,
        Batch,
        BatchStatus,
        ActualQuantity,
        AllocatedQuantity,
        ReservedQuantity,
        StocktakeInd,
        ActiveBinning,
        ActivePicking
      </returns>
      <newpara>
        Modified by    : 
        Modified Date  : 
        Details        : 
      </newpara>
*/
create procedure p_Housekeeping_SUBL_Search
(
 @WarehouseId         int,
 @InstructionTypeCode nvarchar(10),
 @ProductCode         nvarchar(30) = null,
 @Product             nvarchar(50) = null,
 @SKUCode             nvarchar(50) = null,
 @Batch               nvarchar(50) = null,
 @PickLocation        nvarchar(15) = null
)
as
begin
	 set nocount on
  
  declare @AreaLocationTable as
  table(
        AreaId                          int              null,
        Area                            nvarchar(50)      null,
        LocationId                      int              null,
        Location                        nvarchar(15)      null,
        StocktakeInd                    int              null,
        ActiveBinning                   int              null,
        ActivePicking                   int              null
       );
  
  declare @StorageUnitBatchTable as
  table(
        StorageUnitBatchId              int              null,
        StorageUnitId                   int              null,
        ProductId                       int              null,
        ProductCode                     nvarchar(30)      null,
        Product                         nvarchar(50)      null,
        ProductStatusId                 int              null,
        ProductStatus                   nvarchar(50)      null,
        SKUId                           int              null,
        SKU                             nvarchar(50)      null,
        SKUCode                         nvarchar(50)      null,
        BatchId                         int              null,
        Batch                           nvarchar(50)      null,
        BatchStatusId                   int              null,
        BatchStatus                     nvarchar(50)      null
       );
  
--  declare @ResultTable as
--  table(
--        StorageUnitBatchId              int              null,
--        LocationId                      int              null,
--        Location                        nvarchar(15)      null,
--        ProductCode                     nvarchar(30)      null,
--        Product                         nvarchar(50)      null,
--        ProductStatus                   nvarchar(50)      null,
--        SKUCode                         nvarchar(50)      null,
--        SKU                             nvarchar(50)      null,
--        Batch                           nvarchar(50)      null,
--        BatchStatus                     nvarchar(50)      null,
--        ActualQuantity                  float              null,
--        AllocatedQuantity               float              null,
--        ReservedQuantity                float              null,
--        StocktakeInd                    bit              null,
--        ActiveBinning                   bit              null,
--        ActivePicking                   bit              null
--       );
  
--  if @StorageUnitBatchId = '-1'
--    set @StorageUnitBatchId = null
--  
--  if @StorageUnitId = '-1'
--    set @StorageUnitId = null
--  
--  if @ProductId = '-1'
--    set @ProductId = null
--  
--  if @SKUId = '-1'
--    set @SKUId = null
--  
--  if @BatchId = '-1'
--    set @BatchId = null
--  
--  if @PickLocationId = '-1'
--    set @PickLocationId = null
  
  if @ProductCode='%'
  and @Product='%'
  and @SKUCode='%'
  and @Batch='%'
  and @PickLocation='%'
   goto result  
  -- Replenishments - Show only racking, bulk etc.
  if @InstructionTypeCode = 'R'
  begin
    insert @AreaLocationTable
          (AreaId,
           Area,
           LocationId,
           Location,
           StocktakeInd,
           ActiveBinning,
           ActivePicking)
    select a.AreaId,
           a.Area,
           l.LocationId,
           l.Location,
           l.StocktakeInd,
           l.ActiveBinning,
           l.ActivePicking
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId      = al.AreaId
      join Location      l (nolock) on al.LocationId = l.LocationId
     where a.WarehouseId = @WarehouseId
       and a.AreaCode in ('BK','RK')
       and l.Location like isnull(@PickLocation + '%', l.Location)
  end
  
  -- Movements - Show everything
  if @InstructionTypeCode in ('M','O','HS','RS')
  begin
    insert @AreaLocationTable
          (AreaId,
           Area,
           LocationId,
           Location,
           StocktakeInd,
           ActiveBinning,
           ActivePicking)
    select a.AreaId,
           a.Area,
           l.LocationId,
           l.Location,
           l.StocktakeInd,
           l.ActiveBinning,
           l.ActivePicking
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId      = al.AreaId
      join Location      l (nolock) on al.LocationId = l.LocationId
     where a.WarehouseId = @WarehouseId
       and a.StockOnHand = 1
       and l.Location like isnull(@PickLocation + '%', l.Location)
  end
  
  -- Stock Take - Show only locations not already with a Stock Take allocated
  if @InstructionTypeCode = 'ST'
  begin
    insert @AreaLocationTable
          (AreaId,
           Area,
           LocationId,
           Location,
           StocktakeInd,
           ActiveBinning,
           ActivePicking)
    select a.AreaId,
           a.Area,
           l.LocationId,
           l.Location,
           l.StocktakeInd,
           l.ActiveBinning,
           l.ActivePicking
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId      = al.AreaId
      join Location      l (nolock) on al.LocationId = l.LocationId
     where a.WarehouseId  = @WarehouseId
       and a.StockOnHand  = 1
       and l.StocktakeInd = 0
       and l.Location like isnull(@PickLocation, l.Location)
  end
  
  insert @StorageUnitBatchTable
        (StorageUnitBatchId,
         StorageUnitId,
         ProductId,
         ProductCode,
         Product,
         ProductStatusId,
         SKUId,
         SKUCode,
         SKU,
         BatchId,
         Batch,
         BatchStatusId)
  select sub.StorageUnitBatchId,
         su.StorageUnitId,
         p.ProductId,
         p.ProductCode,
         p.Product,
         p.StatusId,
         sku.SKUId,
         sku.SKUCode,
         sku.SKU,
         b.BatchId,
         b.Batch,
         b.StatusId
    from StorageUnitBatch         sub  (nolock)
    join Batch                    b    (nolock) on b.BatchId               = sub.BatchId
    join StorageUnit              su   (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                  p    (nolock) on su.ProductId            = p.ProductId
    join SKU                      sku  (nolock) on su.SKUId                = sku.SKUId
   where b.Batch                like isnull(@Batch + '%',       b.Batch)
     and p.Product              like isnull(@Product + '%',     p.Product)
     and p.ProductCode          like isnull(@ProductCode + '%', p.ProductCode)
     and sku.SKUCode            like isnull(@SKUCode + '%',     sku.SKUCode)
  
  update t
     set ProductStatus = s.Status
    from @StorageUnitBatchTable t
    join Status                 s (nolock) on t.ProductStatusId = s.StatusId

  update t
     set BatchStatus = s.Status
    from @StorageUnitBatchTable t
    join Status                 s (nolock) on t.BatchStatusId = s.StatusId
  
  result:
  select subt.StorageUnitBatchId,
         subl.LocationId as 'PickLocationId',
         alt.Location,
         subt.ProductCode,
         subt.Product,
         subt.ProductStatus,
         subt.SKUCode,
         subt.SKU,
         subt.Batch,
         subt.BatchStatus,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         alt.StocktakeInd,
         alt.ActiveBinning,
         alt.ActivePicking
    from StorageUnitBatchLocation subl (nolock),
         @StorageUnitBatchTable   subt,
         @AreaLocationTable       alt
   where subl.StorageUnitBatchId = subt.StorageUnitBatchId
     and subl.LocationId        = alt.LocationId
     and subl.ActualQuantity - subl.ReservedQuantity > 0
end
