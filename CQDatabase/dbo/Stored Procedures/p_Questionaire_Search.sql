﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questionaire_Search
  ///   Filename       : p_Questionaire_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:09
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Questionaire table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Questionaire.QuestionaireId,
  ///   Questionaire.WarehouseId,
  ///   Questionaire.QuestionaireType,
  ///   Questionaire.QuestionaireDesc 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Questionaire_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         Questionaire.QuestionaireId
        ,Questionaire.WarehouseId
        ,Questionaire.QuestionaireType
        ,Questionaire.QuestionaireDesc
    from Questionaire
  
end
