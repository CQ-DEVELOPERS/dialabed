﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerTypeHistory_Insert
  ///   Filename       : p_ContainerTypeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:57
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ContainerTypeHistory table.
  /// </remarks>
  /// <param>
  ///   @ContainerTypeId int = null,
  ///   @ContainerType nvarchar(100) = null,
  ///   @ContainerTypeCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   ContainerTypeHistory.ContainerTypeId,
  ///   ContainerTypeHistory.ContainerType,
  ///   ContainerTypeHistory.ContainerTypeCode,
  ///   ContainerTypeHistory.CommandType,
  ///   ContainerTypeHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerTypeHistory_Insert
(
 @ContainerTypeId int = null,
 @ContainerType nvarchar(100) = null,
 @ContainerTypeCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ContainerTypeHistory
        (ContainerTypeId,
         ContainerType,
         ContainerTypeCode,
         CommandType,
         InsertDate)
  select @ContainerTypeId,
         @ContainerType,
         @ContainerTypeCode,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
