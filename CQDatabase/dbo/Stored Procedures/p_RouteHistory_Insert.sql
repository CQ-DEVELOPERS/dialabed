﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RouteHistory_Insert
  ///   Filename       : p_RouteHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:43
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the RouteHistory table.
  /// </remarks>
  /// <param>
  ///   @RouteId int = null,
  ///   @Route nvarchar(100) = null,
  ///   @RouteCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   RouteHistory.RouteId,
  ///   RouteHistory.Route,
  ///   RouteHistory.RouteCode,
  ///   RouteHistory.CommandType,
  ///   RouteHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RouteHistory_Insert
(
 @RouteId int = null,
 @Route nvarchar(100) = null,
 @RouteCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert RouteHistory
        (RouteId,
         Route,
         RouteCode,
         CommandType,
         InsertDate)
  select @RouteId,
         @Route,
         @RouteCode,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
