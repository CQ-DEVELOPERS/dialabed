﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportOrderPick_Update
  ///   Filename       : p_InterfaceExportOrderPick_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:03
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceExportOrderPick table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportOrderPickId int = null,
  ///   @IssueId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportOrderPick_Update
(
 @InterfaceExportOrderPickId int = null,
 @IssueId int = null,
 @OrderNumber nvarchar(60) = null,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceExportOrderPickId = '-1'
    set @InterfaceExportOrderPickId = null;
  
	 declare @Error int
 
  update InterfaceExportOrderPick
     set IssueId = isnull(@IssueId, IssueId),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate) 
   where InterfaceExportOrderPickId = @InterfaceExportOrderPickId
  
  select @Error = @@Error
  
  
  return @Error
  
end
