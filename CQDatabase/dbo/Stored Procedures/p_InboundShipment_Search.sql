﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Search
  ///   Filename       : p_InboundShipment_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:20
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundShipment table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InboundShipment.InboundShipmentId,
  ///   InboundShipment.StatusId,
  ///   InboundShipment.WarehouseId,
  ///   InboundShipment.ShipmentDate,
  ///   InboundShipment.Remarks 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Search
(
 @InboundShipmentId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InboundShipmentId = '-1'
    set @InboundShipmentId = null;
  
 
  select
         InboundShipment.InboundShipmentId
        ,InboundShipment.StatusId
        ,InboundShipment.WarehouseId
        ,InboundShipment.ShipmentDate
        ,InboundShipment.Remarks
    from InboundShipment
   where isnull(InboundShipment.InboundShipmentId,'0')  = isnull(@InboundShipmentId, isnull(InboundShipment.InboundShipmentId,'0'))
  
end
