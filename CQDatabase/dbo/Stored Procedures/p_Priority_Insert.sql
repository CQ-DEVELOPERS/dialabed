﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Priority_Insert
  ///   Filename       : p_Priority_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:13
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Priority table.
  /// </remarks>
  /// <param>
  ///   @PriorityId int = null output,
  ///   @Priority nvarchar(100) = null,
  ///   @PriorityCode nvarchar(20) = null,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  ///   Priority.PriorityId,
  ///   Priority.Priority,
  ///   Priority.PriorityCode,
  ///   Priority.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Priority_Insert
(
 @PriorityId int = null output,
 @Priority nvarchar(100) = null,
 @PriorityCode nvarchar(20) = null,
 @OrderBy int = null 
)
 
as
begin
	 set nocount on;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @Priority = '-1'
    set @Priority = null;
  
  if @PriorityCode = '-1'
    set @PriorityCode = null;
  
	 declare @Error int
 
  insert Priority
        (Priority,
         PriorityCode,
         OrderBy)
  select @Priority,
         @PriorityCode,
         @OrderBy 
  
  select @Error = @@Error, @PriorityId = scope_identity()
  
  
  return @Error
  
end
