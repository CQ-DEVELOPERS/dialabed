﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_SKU_By_Job
  ///   Filename       : p_Report_SKU_By_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_SKU_By_Job
(
 @JobId int
)
 
as
begin
	 set nocount on;
	 
	 select i.JobId, sku.SKUCode, sum(isnull(ili.ConfirmedQuantity,0)) as 'ConfirmedQuantity', sc.ConfirmedQuantity as 'CheckQuantity'
	   from IssueLineInstruction ili (nolock)
	   join Instruction        i (nolock) on ili.InstructionId    = i.InstructionId
	   join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
	   join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
	   join SKU              sku (nolock) on su.SKUId             = sku.SKUId
	   left
	   join SKUCheck          sc (nolock) on i.JobId = sc.JobId
	                                     and sku.SKUCode = sc.SKUCode
	  where i.JobId = @JobId
	 group by i.JobId, sku.SKUCode, sc.ConfirmedQuantity
end
