﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPOHeader_Select
  ///   Filename       : p_InterfaceImportPOHeader_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:59
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportPOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportPOHeaderId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportPOHeader.InterfaceImportPOHeaderId,
  ///   InterfaceImportPOHeader.PrimaryKey,
  ///   InterfaceImportPOHeader.OrderNumber,
  ///   InterfaceImportPOHeader.RecordType,
  ///   InterfaceImportPOHeader.RecordStatus,
  ///   InterfaceImportPOHeader.SupplierCode,
  ///   InterfaceImportPOHeader.Supplier,
  ///   InterfaceImportPOHeader.Address,
  ///   InterfaceImportPOHeader.FromWarehouseCode,
  ///   InterfaceImportPOHeader.ToWarehouseCode,
  ///   InterfaceImportPOHeader.DeliveryNoteNumber,
  ///   InterfaceImportPOHeader.ContainerNumber,
  ///   InterfaceImportPOHeader.SealNumber,
  ///   InterfaceImportPOHeader.DeliveryDate,
  ///   InterfaceImportPOHeader.Remarks,
  ///   InterfaceImportPOHeader.NumberOfLines,
  ///   InterfaceImportPOHeader.Additional1,
  ///   InterfaceImportPOHeader.Additional2,
  ///   InterfaceImportPOHeader.Additional3,
  ///   InterfaceImportPOHeader.Additional4,
  ///   InterfaceImportPOHeader.Additional5,
  ///   InterfaceImportPOHeader.ProcessedDate,
  ///   InterfaceImportPOHeader.Additional6,
  ///   InterfaceImportPOHeader.Additional7,
  ///   InterfaceImportPOHeader.Additional8,
  ///   InterfaceImportPOHeader.Additional9,
  ///   InterfaceImportPOHeader.Additional10,
  ///   InterfaceImportPOHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPOHeader_Select
(
 @InterfaceImportPOHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceImportPOHeader.InterfaceImportPOHeaderId
        ,InterfaceImportPOHeader.PrimaryKey
        ,InterfaceImportPOHeader.OrderNumber
        ,InterfaceImportPOHeader.RecordType
        ,InterfaceImportPOHeader.RecordStatus
        ,InterfaceImportPOHeader.SupplierCode
        ,InterfaceImportPOHeader.Supplier
        ,InterfaceImportPOHeader.Address
        ,InterfaceImportPOHeader.FromWarehouseCode
        ,InterfaceImportPOHeader.ToWarehouseCode
        ,InterfaceImportPOHeader.DeliveryNoteNumber
        ,InterfaceImportPOHeader.ContainerNumber
        ,InterfaceImportPOHeader.SealNumber
        ,InterfaceImportPOHeader.DeliveryDate
        ,InterfaceImportPOHeader.Remarks
        ,InterfaceImportPOHeader.NumberOfLines
        ,InterfaceImportPOHeader.Additional1
        ,InterfaceImportPOHeader.Additional2
        ,InterfaceImportPOHeader.Additional3
        ,InterfaceImportPOHeader.Additional4
        ,InterfaceImportPOHeader.Additional5
        ,InterfaceImportPOHeader.ProcessedDate
        ,InterfaceImportPOHeader.Additional6
        ,InterfaceImportPOHeader.Additional7
        ,InterfaceImportPOHeader.Additional8
        ,InterfaceImportPOHeader.Additional9
        ,InterfaceImportPOHeader.Additional10
        ,InterfaceImportPOHeader.InsertDate
    from InterfaceImportPOHeader
   where isnull(InterfaceImportPOHeader.InterfaceImportPOHeaderId,'0')  = isnull(@InterfaceImportPOHeaderId, isnull(InterfaceImportPOHeader.InterfaceImportPOHeaderId,'0'))
  
end
