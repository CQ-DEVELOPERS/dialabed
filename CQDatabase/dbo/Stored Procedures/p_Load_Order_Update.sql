﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Load_Order_Update
  ///   Filename       : p_Load_Order_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Load_Order_Update
(
 @outboundShipmentId  int,
 @issueId             int,
 @priorityId          int,
 @vehicleRegistration nvarchar(10),
 @shipmentDate        datetime,
 @remarks             nvarchar(255)
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
  (
   OutboundShipmentId              int,
   IssueId                         int,
   IssueLineId                     int,
   InstructionId                   int,
   JobId                           int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500)
  
  if @outboundShipmentId = -1
    set @outboundShipmentId = null
  
  if @issueId = -1
    set @issueId = null
  
  if @priorityId = -1
  begin
    set @Error = -1
    goto error
  end
  
  if @outboundShipmentId is not null
    insert @TableResult
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           InstructionId,
           JobId)
    select ili.OutboundShipmentId,
           ili.IssueId,
           ili.IssueLineId,
           ili.InstructionId,
           i.JobId
      from IssueLineInstruction ili
      join Instruction            i on ili.InstructionId = i.InstructionId
     where ili.OutboundShipmentId = isnull(@outboundShipmentId, -1)
  else
    insert @TableResult
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           InstructionId,
           JobId)
    select ili.OutboundShipmentId,
           ili.IssueId,
           ili.IssueLineId,
           ili.InstructionId,
           i.JobId
      from IssueLineInstruction ili
      join Instruction            i on ili.InstructionId = i.InstructionId
     where ili.IssueId = isnull(@issueId, -1)
  
  begin transaction
  
  update u
     set PriorityId          = @priorityId,
         VehicleRegistration = @VehicleRegistration,
         DeliveryDate        = @ShipmentDate,
         Remarks             = @Remarks
    from @TableResult tr
    join Issue         u (nolock) on tr.IssueId = u.IssueId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update u
     set VehicleRegistration = @VehicleRegistration,
         ShipmentDate        = @ShipmentDate,
         Remarks             = @Remarks
    from @TableResult     tr
    join OutboundShipment  u (nolock) on tr.OutboundShipmentId = u.OutboundShipmentId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update u
     set PriorityId = @priorityId
    from @TableResult tr
    join Job           u (nolock) on tr.JobId = u.JobId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Load_Order_Update'
    rollback transaction
    return @Error
end
