﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoad_Select
  ///   Filename       : p_IntransitLoad_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:33
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IntransitLoad table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadId int = null 
  /// </param>
  /// <returns>
  ///   IntransitLoad.IntransitLoadId,
  ///   IntransitLoad.WarehouseId,
  ///   IntransitLoad.StatusId,
  ///   IntransitLoad.DeliveryNoteNumber,
  ///   IntransitLoad.DeliveryDate,
  ///   IntransitLoad.SealNumber,
  ///   IntransitLoad.VehicleRegistration,
  ///   IntransitLoad.Route,
  ///   IntransitLoad.DriverId,
  ///   IntransitLoad.Remarks,
  ///   IntransitLoad.CreatedById,
  ///   IntransitLoad.ReceivedById,
  ///   IntransitLoad.CreateDate,
  ///   IntransitLoad.LoadClosed,
  ///   IntransitLoad.ReceivedDate,
  ///   IntransitLoad.Offloaded 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoad_Select
(
 @IntransitLoadId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         IntransitLoad.IntransitLoadId
        ,IntransitLoad.WarehouseId
        ,IntransitLoad.StatusId
        ,IntransitLoad.DeliveryNoteNumber
        ,IntransitLoad.DeliveryDate
        ,IntransitLoad.SealNumber
        ,IntransitLoad.VehicleRegistration
        ,IntransitLoad.Route
        ,IntransitLoad.DriverId
        ,IntransitLoad.Remarks
        ,IntransitLoad.CreatedById
        ,IntransitLoad.ReceivedById
        ,IntransitLoad.CreateDate
        ,IntransitLoad.LoadClosed
        ,IntransitLoad.ReceivedDate
        ,IntransitLoad.Offloaded
    from IntransitLoad
   where isnull(IntransitLoad.IntransitLoadId,'0')  = isnull(@IntransitLoadId, isnull(IntransitLoad.IntransitLoadId,'0'))
  
end
