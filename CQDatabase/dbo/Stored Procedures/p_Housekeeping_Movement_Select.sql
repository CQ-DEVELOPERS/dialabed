﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Movement_Select
  ///   Filename       : p_Housekeeping_Movement_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Movement_Select
(
 @StorageUnitBatchId int,
 @PickLocationId     int
)
 
as
begin
	 set nocount on;
  
  select subl.StorageUnitBatchId,
         l.LocationId as 'PickLocationId',
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         l.Location,
         subl.ActualQuantity - subl.ReservedQuantity as 'AvailableQuantity',
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Location                    l (nolock) on subl.LocationId         = l.LocationId
 where sub.StorageUnitBatchId = @StorageUnitBatchId
   and l.LocationId           = @PickLocationId
end
