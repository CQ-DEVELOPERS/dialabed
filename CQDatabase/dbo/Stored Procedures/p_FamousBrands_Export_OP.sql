﻿/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_OP
  ///   Filename       : p_FamousBrands_Export_OP.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_FamousBrands_Export_OP
(
 @FileName varchar(30) = null output
)
as
begin
  set nocount on
  
  declare @TableResult as table
  (
   Data varchar(500)
  )
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @InterfaceExportOrderPickId int,
          @OrderNumber          varchar(30),
          -- Internal
          @print_string         varchar(255)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select top 1
         @InterfaceExportOrderPickId = InterfaceExportOrderPickId,
         @OrderNumber                = OrderNumber
    from InterfaceExportOrderPick
   where RecordStatus = 'N'
  order by InterfaceExportOrderPickId
  
  if @@Error <> 0
    goto Error
  
  if @InterfaceExportOrderPickId is null or @OrderNumber is null
    return
  
  begin transaction
  
  update InterfaceExportOrderPick
     set RecordStatus = 'Y',
         ProcessedDate = @GetDate
   where InterfaceExportOrderPickId = @InterfaceExportOrderPickId
    
  if @@Error <> 0
    goto Error
  
  set @FileName = @OrderNumber + '(' + convert(varchar(10), @InterfaceExportOrderPickId) + ')'
  
  if @@Error <> 0
    goto Error
  
  insert @TableResult (Data) select '<?xml version="1.0" encoding="Windows-1252" standalone="yes" ?>'
  
  if @@Error <> 0
    goto Error
  
  insert @TableResult (Data) select '<root>'
  
  if @@Error <> 0
    goto Error
  
  insert @TableResult (Data) select '<OrderPick>'
  
  if @@Error <> 0
    goto Error
  
  insert @TableResult (Data) select '<OrderNumber>' + @OrderNumber + '</OrderNumber>'
  
  if @@Error <> 0
    goto Error
  
  insert @TableResult (Data) select '</OrderPick>'
  
  if @@Error <> 0
    goto Error
  
  insert @TableResult (Data) select '</root>'
  
  if @@Error <> 0
    goto Error
  
  select Data
    from @TableResult
  
  if @@Error <> 0
    goto Error
  
  commit transaction
  return
  
  Error:
  rollback transaction
  return
end
