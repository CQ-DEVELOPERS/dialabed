﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Outstanding_Jobs
  ///   Filename       : p_Dashboard_Outbound_Outstanding_Jobs.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Outstanding_Jobs
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select InstructionType,
           Value
      from DashboardOutboundOutstandingJobs
     where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardOutboundOutstandingJobs
	   
	   insert DashboardOutboundOutstandingJobs
	         (WarehouseId,
	          InstructionType,
           Value)
	   select ins.WarehouseId,
	          it.InstructionType,
           count(distinct(ins.JobId))
      from IssueLineInstruction ili
      join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
      join Status                 s (nolock) on ins.StatusId = s.StatusId
      join InstructionType       it (nolock) on ins.InstructionTypeId = it.InstructionTypeId
     where ins.WarehouseId = isnull(@WarehouseId, ins.WarehouseId)
       and ins.EndDate > dateadd(dd, -5, getdate())
    group by ins.WarehouseId,
             it.InstructionType
	 end
end
