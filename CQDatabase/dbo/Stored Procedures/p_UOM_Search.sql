﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UOM_Search
  ///   Filename       : p_UOM_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:00
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the UOM table.
  /// </remarks>
  /// <param>
  ///   @UOMId int = null output,
  ///   @UOM nvarchar(100) = null,
  ///   @UOMCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   UOM.UOMId,
  ///   UOM.UOM,
  ///   UOM.UOMCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_UOM_Search
(
 @UOMId int = null output,
 @UOM nvarchar(100) = null,
 @UOMCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @UOMId = '-1'
    set @UOMId = null;
  
  if @UOM = '-1'
    set @UOM = null;
  
  if @UOMCode = '-1'
    set @UOMCode = null;
  
 
  select
         UOM.UOMId
        ,UOM.UOM
        ,UOM.UOMCode
    from UOM
   where isnull(UOM.UOMId,'0')  = isnull(@UOMId, isnull(UOM.UOMId,'0'))
     and isnull(UOM.UOM,'%')  like '%' + isnull(@UOM, isnull(UOM.UOM,'%')) + '%'
     and isnull(UOM.UOMCode,'%')  like '%' + isnull(@UOMCode, isnull(UOM.UOMCode,'%')) + '%'
  
end
