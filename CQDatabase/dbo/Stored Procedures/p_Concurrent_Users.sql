﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Concurrent_Users
  ///   Filename       : p_Concurrent_Users.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Nov 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Concurrent_Users
(
 @WarehouseId int,
 @SiteType    varchar(20)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Maximum           int,
          @Warning           int,
          @Current           int,
          @LastActivity      datetime,
          @Exception         nvarchar(255),
          @Warehouse         nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Concurrent_Users'
  set @Error = 0
  
  begin transaction
  /*
  update Operator set LastActivity = dateadd(mi, -60, GETDATE())
  update Operator set LogoutTime = dateadd(mi, -59, GETDATE())
  
  update Operator set LogoutTime = dateadd(mi, -25, GETDATE()) where OperatorId = 2
  update Operator set LastActivity = dateadd(mi, -21, GETDATE()) where OperatorId = 2
  
  select datediff(mi, LastActivity, GETDATE()) from Operator
  */
  --select * from Operator where LastActivity > isnull(LogoutTime,LastActivity) and datediff(mi, LastActivity, GETDATE()) > 20
  
  update Operator set LogoutTime = GETDATE() where LastActivity > isnull(LogoutTime,LastActivity) and datediff(mi, LastActivity, GETDATE()) > 20
  
  if @SiteType = 'Desktop'
  begin
    select @Current = COUNT(1)
      from Operator (nolock)
     where WarehouseId = @WarehouseId
       and SiteType     = 'Desktop'
       and LastActivity >= isnull(LogoutTime,LastActivity)
       and datediff(mi, LastActivity, GETDATE()) <= 20
    
    select @Warehouse = isnull(Warehouse,''),
           @Maximum   = isnull(DesktopMaximum,10),
           @Warning   = isnull(DesktopWarning,10)
      from Warehouse (nolock)
     where WarehouseId = @WarehouseId
    
    select @Exception = 'Concurrent users logged on to ' + @Warehouse +  ' is ' + convert(varchar(10), @Current) + ', Maximum is ' + convert(varchar(10), @Maximum)
    
    exec @Error = p_Exception_Insert
     @ExceptionId   = null,
     @ExceptionCode = 'DTCURUSERS',
     @Exception     = @Exception,
     @CreateDate    = @GetDate,
     @ExceptionDate = @Getdate,
     @Quantity      = @Current
    
    if @Error <> 0
      goto error
    
    if @Current >= @Warning
    begin
      select @Exception = 'Warning - concurrent desktop users is high for ' + @Warehouse +  ', warning is triggered at ' + convert(varchar(10), @Warning) + ', the maximum is ' + convert(varchar(10), @Maximum)
      
      exec @Error = p_Exception_Insert
       @ExceptionId   = null,
       @ExceptionCode = 'DTCURWARN',
       @Exception     = @Exception,
       @CreateDate    = @GetDate,
       @ExceptionDate = @Getdate,
       @Quantity      = @Warning
      
      if @Error <> 0
        goto error
    end
    
    if @Current >= @Maximum
    begin
      set @Errormsg = 'Error executing p_Concurrent_Users - maximum desktop users reached'
      goto Error
    end
  end
  else if @SiteType = 'Mobile'
  begin
    select @Current = COUNT(1)
      from Operator (nolock)
     where WarehouseId = @WarehouseId
       and SiteType     = 'Mobile'
       and LastActivity >= isnull(LogoutTime,LastActivity)
       and datediff(mi, LastActivity, GETDATE()) <= 20
    
    select @Warehouse = isnull(Warehouse,''),
           @Maximum   = isnull(MobileMaximum,10),
           @Warning   = isnull(MobileWarning,10)
      from Warehouse (nolock)
     where WarehouseId = @WarehouseId
    
    select @Exception = 'Concurrent users logged on to ' + @Warehouse +  ' is ' + convert(varchar(10), @Current) + ', Maximum is ' + convert(varchar(10), @Maximum)
    
    exec @Error = p_Exception_Insert
     @ExceptionId   = null,
     @ExceptionCode = 'MBCURUSERS',
     @Exception     = @Exception,
     @CreateDate    = @GetDate,
     @ExceptionDate = @Getdate,
     @Quantity      = @Current
    
    if @Error <> 0
      goto error
    
    if @Current >= @Warning
    begin
      select @Exception = 'Warning - concurrent mobile users is high for ' + @Warehouse +  ', warning is triggered at ' + convert(varchar(10), @Warning) + ', the maximum is ' + convert(varchar(10), @Maximum)
      
      exec @Error = p_Exception_Insert
       @ExceptionId   = null,
       @ExceptionCode = 'MBCURWARN',
       @Exception     = @Exception,
       @CreateDate    = @GetDate,
       @ExceptionDate = @Getdate,
       @Quantity      = @Warning
      
      if @Error <> 0
        goto error
    end
    
    if @Current >= @Maximum
    begin
      set @Errormsg = 'Error executing p_Concurrent_Users - maximum mobile users reached'
      goto Error
    end
  end
  
  commit transaction
  return @Error
  
  error:
    raiserror 900000 'Error executing p_Concurrent_Users'
    rollback transaction
    return @Error
end
