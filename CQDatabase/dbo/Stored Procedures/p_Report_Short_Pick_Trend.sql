﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Short_Pick_Trend
  ///   Filename       : p_Report_Short_Pick_Trend.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Short_Pick_Trend
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @OperatorId        int = null,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitId = -1
    set @StorageUnitId =  null
  
  if @OperatorId = -1
    set @OperatorId = null
  
  select o.Operator,
         ili.Quantity - ili.ConfirmedQuantity as 'Short'
    from IssueLineInstruction ili
    join Instruction        i on ili.InstructionId = i.InstructionId
    join Operator           o on i.OperatorId         = o.OperatorId
    join StorageUnitBatch sub on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su on sub.StorageUnitId    = su.StorageUnitId
   where i.WarehouseId    = @WarehouseId
     and su.StorageUnitId = isnull(@StorageUnitId, su.StorageUnitId)
     and i.OperatorId     = isnull(@OperatorId, i.OperatorId)
     and i.EndDate  between @FromDate and @ToDate
     --and i.Quantity       > i.ConfirmedQuantity
end
