﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockMaster_Insert
  ///   Filename       : p_StockMaster_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:41
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StockMaster table.
  /// </remarks>
  /// <param>
  ///   @SKUCode nvarchar(max) = null,
  ///   @ProductCode nvarchar(max) = null,
  ///   @ParentSKUCode nvarchar(max) = null,
  ///   @ParentProductCode nvarchar(max) = null,
  ///   @Product nvarchar(max) = null,
  ///   @Barcode nvarchar(max) = null,
  ///   @WarehouseCode nvarchar(max) = null,
  ///   @PackTypeId nvarchar(max) = null,
  ///   @PackType nvarchar(max) = null,
  ///   @OutboundSequence nvarchar(max) = null,
  ///   @Quantity nvarchar(max) = null,
  ///   @Weight nvarchar(max) = null,
  ///   @Volume nvarchar(max) = null 
  /// </param>
  /// <returns>
  ///   StockMaster.SKUCode,
  ///   StockMaster.ProductCode,
  ///   StockMaster.ParentSKUCode,
  ///   StockMaster.ParentProductCode,
  ///   StockMaster.Product,
  ///   StockMaster.Barcode,
  ///   StockMaster.WarehouseCode,
  ///   StockMaster.PackTypeId,
  ///   StockMaster.PackType,
  ///   StockMaster.OutboundSequence,
  ///   StockMaster.Quantity,
  ///   StockMaster.Weight,
  ///   StockMaster.Volume 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockMaster_Insert
(
 @SKUCode nvarchar(max) = null,
 @ProductCode nvarchar(max) = null,
 @ParentSKUCode nvarchar(max) = null,
 @ParentProductCode nvarchar(max) = null,
 @Product nvarchar(max) = null,
 @Barcode nvarchar(max) = null,
 @WarehouseCode nvarchar(max) = null,
 @PackTypeId nvarchar(max) = null,
 @PackType nvarchar(max) = null,
 @OutboundSequence nvarchar(max) = null,
 @Quantity nvarchar(max) = null,
 @Weight nvarchar(max) = null,
 @Volume nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StockMaster
        (SKUCode,
         ProductCode,
         ParentSKUCode,
         ParentProductCode,
         Product,
         Barcode,
         WarehouseCode,
         PackTypeId,
         PackType,
         OutboundSequence,
         Quantity,
         Weight,
         Volume)
  select @SKUCode,
         @ProductCode,
         @ParentSKUCode,
         @ParentProductCode,
         @Product,
         @Barcode,
         @WarehouseCode,
         @PackTypeId,
         @PackType,
         @OutboundSequence,
         @Quantity,
         @Weight,
         @Volume 
  
  select @Error = @@Error
  
  
  return @Error
  
end
