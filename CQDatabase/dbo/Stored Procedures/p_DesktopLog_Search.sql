﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DesktopLog_Search
  ///   Filename       : p_DesktopLog_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:35
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the DesktopLog table.
  /// </remarks>
  /// <param>
  ///   @DesktopLogId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   DesktopLog.DesktopLogId,
  ///   DesktopLog.ProcName,
  ///   DesktopLog.WarehouseId,
  ///   DesktopLog.OutboundShipmentId,
  ///   DesktopLog.IssueId,
  ///   DesktopLog.OperatorId,
  ///   DesktopLog.ErrorMsg,
  ///   DesktopLog.StartDate,
  ///   DesktopLog.EndDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DesktopLog_Search
(
 @DesktopLogId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @DesktopLogId = '-1'
    set @DesktopLogId = null;
  
 
  select
         DesktopLog.DesktopLogId
        ,DesktopLog.ProcName
        ,DesktopLog.WarehouseId
        ,DesktopLog.OutboundShipmentId
        ,DesktopLog.IssueId
        ,DesktopLog.OperatorId
        ,DesktopLog.ErrorMsg
        ,DesktopLog.StartDate
        ,DesktopLog.EndDate
    from DesktopLog
   where isnull(DesktopLog.DesktopLogId,'0')  = isnull(@DesktopLogId, isnull(DesktopLog.DesktopLogId,'0'))
  
end
