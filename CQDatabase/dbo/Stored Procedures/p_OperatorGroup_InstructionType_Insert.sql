﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroup_InstructionType_Insert
  ///   Filename       : p_OperatorGroup_InstructionType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Dec 2007 11:23:30
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroupInstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null output,
  ///   @OperatorGroupId int = null output,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupInstructionType.InstructionTypeId,
  ///   OperatorGroupInstructionType.OperatorGroupId,
  ///   OperatorGroupInstructionType.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroup_InstructionType_Insert
(
 @InstructionTypeId int = null output,
 @OperatorGroupId int = null output
)
 
as
begin
	 set nocount on;
  
	 declare @Error int,
			 @OrderBy int

     set @OrderBy = 1
 		 
  insert OperatorGroupInstructionType
        (InstructionTypeId,
         OperatorGroupId,
         OrderBy)
  select @InstructionTypeId,
         @OperatorGroupId,
         @OrderBy 
  
  select @Error = @@Error, @OperatorGroupId = scope_identity()
  
  if @Error = 0
    exec @Error = p_OperatorGroupInstructionTypeHistory_Insert
         @InstructionTypeId,
         @OperatorGroupId,
         @OrderBy 
        ,@CommandType = 'Insert'
  
  return @Error
  
end
