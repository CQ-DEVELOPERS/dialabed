﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Weight
  ///   Filename       : p_Mobile_Product_Check_Weight.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Weight
(
 @jobId         int,
 @weight	  float
)
 
as
begin
	 set nocount on;

	 declare @Error int = 0
  
	 exec @Error = p_job_update
		@jobId = @jobId,
		@weight = @weight

	return @Error
end
 
