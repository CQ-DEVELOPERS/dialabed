﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Inbound_Order_Update
    ///   Filename       : p_Mobile_Inbound_Order_Update.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 2 March 2009 20:04:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : Kevin Wilson
    ///   Modified Date  : 
    ///   Details		 : Force users to click finish when receiving a PO

       : 
    /// </newpara>
*/
CREATE PROCEDURE p_Mobile_Inbound_Order_Update (
	@receiptId INT
	,@OperatorId INT = NULL
	,@DeliveryNoteNumber NVARCHAR(30) = NULL
	,@DeliveryDate NVARCHAR(30) = NULL
	,@SealNumber NVARCHAR(30) = NULL
	,@VehicleRegistration NVARCHAR(10) = NULL
	,@Remarks NVARCHAR(250) = NULL
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Error INT
		,@GetDate DATETIME
		,@WarehouseId INT
		,@StatusId INT
		,@ContainerNumber NVARCHAR(50)
		,@ReceivingStarted DATETIME

	SELECT @GetDate = dbo.ufn_Getdate()

	SELECT @WarehouseId = WarehouseId
	FROM Receipt(NOLOCK)
	WHERE ReceiptId = @receiptId

	BEGIN TRANSACTION

	IF @DeliveryNoteNumber = ''
		SET @DeliveryNoteNumber = NULL

	IF @DeliveryNoteNumber IS NOT NULL
	BEGIN
		IF @DeliveryNoteNumber != (
				SELECT DeliveryNoteNumber
				FROM Receipt
				WHERE ReceiptId = @receiptId
				)
		BEGIN
			SET @ReceiptId = - 2

			GOTO error
		END
	END

	SELECT @StatusId = dbo.ufn_StatusId('R', 'R')

	SET @ReceivingStarted = (
			SELECT ReceivingStarted
			FROM Receipt
			WHERE ReceiptId = @receiptId
			)

	IF @ReceivingStarted IS NULL
		SET @ReceivingStarted = getdate()

	IF @ReceiptId IS NOT NULL
	BEGIN
		EXEC @Error = p_Receipt_Update @ReceiptId = @ReceiptId
			,@StatusId = @StatusId
			,@OperatorId = @OperatorId
			,@DeliveryNoteNumber = @DeliveryNoteNumber
			,@DeliveryDate = @DeliveryDate
			,@SealNumber = @SealNumber
			,@VehicleRegistration = @VehicleRegistration
			,@Remarks = @Remarks
			,@ContainerNumber = @ContainerNumber
			,@ReceivingStarted = @ReceivingStarted
			,@LockReceipt = 1

		IF @Error <> 0
		BEGIN
			SET @ReceiptId = - 1

			GOTO error
		END
	END

	COMMIT TRANSACTION

	SELECT @ReceiptId

	RETURN @ReceiptId

	error:

	ROLLBACK TRANSACTION

	SELECT @ReceiptId

	RETURN @ReceiptId
END
