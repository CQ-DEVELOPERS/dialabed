﻿/*  
  /// <summary>  
  ///   Procedure Name : p_Label_Variables  
  ///   Filename       : p_Label_Variables.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 20 Nov 2007  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
create procedure p_Interface_WebService_LabelVariables
(
	@XMLBody nvarchar(max) output
)
as
begin
    set nocount on;  
    
    declare
      @doc           xml
	 ,@idoc          int
	
     ,@label         varchar(50)
     ,@Title         varchar(200) = null
     ,@PalletId      int = null  
     ,@instructionId int = null 
     ,@JobId         int = null 
     ,@barcode       varchar(50) = null  
     ,@KeyId         varchar(50) = null 
  
     ,@GetDate            datetime  
     ,@LabelId            int  
     ,@ProductCode        varchar(63)  
     ,@Product            varchar(200)  
     ,@SKUCode            varchar(20)  
     ,@Batch              varchar(30)  
     ,@StartDate          datetime  
     ,@Location           varchar(15)  
     ,@Prints             varchar(20)  
     ,@LocationId         int  
     ,@Pallet             varchar(10)  
     ,@OrderNumber        varchar(30)  
     ,@ExternalCompanyId  int  
     ,@Customer           varchar(255)  
     ,@DeliveryDate       datetime  
     ,@RouteId            int  
     ,@Route              varchar(50)  
     ,@IssueId            int  
     ,@IssueLineId        int  
     ,@Weight             int  
     ,@Quantity           numeric(13,6)  
     ,@Reason             varchar(300)  
     ,@OutboundShipmentId int  
     ,@WarehouseId        int  
     ,@StorageUnitId      int  
     ,@ProductAlias       varchar(30)  
     ,@StatusCode         varchar(10)  
     ,@BatchId            int  
     ,@ExpiryDate         datetime  
     ,@Count              int  
     ,@OutboundDocumentId int  
     ,@NettWeight         float  
     ,@GrossWeight        float  
     ,@OperatorGroup      varchar(50)  
     ,@Operator           varchar(50)  
     ,@Password           varchar(50)  
     ,@ReferenceNumber    varchar(30)  
     ,@SecurityCode       varchar(10)  
     ,@StorageUnitBatchId int  
     ,@ChildProductCode   varchar(30)  
  
    SELECT @doc = convert(xml,@XMLBody)
	     
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
		
	--BEGIN TRY
  
    select 
      @label           = nodes.entity.value('Label[1]',         'nvarchar(50)')
     ,@Title           = nodes.entity.value('Title[1]',         'nvarchar(200)')
     ,@PalletId        = nodes.entity.value('PalletId[1]',      'int')
     ,@instructionId   = nodes.entity.value('InstructionId[1]', 'int')
     ,@JobId           = nodes.entity.value('JobId[1]',         'int')       
     ,@barcode         = nodes.entity.value('Barcode[1]',       'nvarchar(50)')
     ,@KeyId           = nodes.entity.value('KeyId[1]',         'nvarchar(50)')
    FROM
	  @doc.nodes('/root') AS nodes(entity)
    
  select @GetDate = dbo.ufn_Getdate()  
    
  select @LabelId = LabelId  
    from CQCommon.dbo.Label (nolock)  
   where Label = @label  
    
  declare @LabelVariables as table  
  (  
   LabelVariablesId int,  
   LabelId          int,  
   Name             varchar(200),  
   Value            varchar(255),  
   Length           int,  
   FixedLength      bit,  
   IsMultiline      bit,  
   LineLength       int,  
   LineCount        int,  
   FormatID         int,  
   Prompt           varchar(50),  
   ValueRequired    bit  
  )  
    
  declare @TablePallet as table  
  (  
   IdentCol int identity,  
   KeyCol   int  
  )  
    
  insert @LabelVariables  
        (Name,  
         Value,  
         Length,  
         FixedLength,  
         IsMultiline,  
         LineLength,  
         LineCount,  
         FormatID,  
         Prompt,  
         ValueRequired)  
  select Name,  
         Value,  
         Length,  
         FixedLength,  
         IsMultiline,  
         LineLength,  
         LineCount,  
         FormatID,  
         Prompt,  
         ValueRequired  
    from CQCommon.dbo.LabelVariables (nolock)  
   where LabelId = @LabelId  
    
  if (@PalletId is not null or @instructionId is not null) and @Label in ('Pallet Label.lbl','Receiving & Staging Label Large.lbl')  
  begin  
    if @PalletId is not null  
    begin  
      select @ProductCode        = p.ProductCode,  
             @Product            = p.Product,  
             @SKUCode            = sku.SKUCode,  
             @Batch              = b.Batch,  
             @StartDate          = @GetDate,  
             @LocationId         = pl.LocationId,  
             @PalletId           = pl.PalletId,  
             @Prints             = 'Copy ' + convert(varchar(20), isnull(pl.Prints, 1)),  
             @Quantity           = pl.Quantity,  
             @StorageUnitId      = su.StorageUnitId,  
             @StorageUnitBatchId = sub.StorageUnitBatchId  
        from Pallet            pl (nolock)  
        join StorageUnitBatch sub (nolock) on pl.StorageUnitBatchId = sub.StorageUnitBatchId  
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
        join Product            p (nolock) on su.ProductId          = p.ProductId  
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
        join Batch              b (nolock) on sub.BatchId           = b.BatchId  
       where pl.PalletId = @PalletId  
        
      select @ChildProductCode = p.ProductCode  
        from ChildProduct      cp (nolock)  
        join StorageUnitBatch sub (nolock) on cp.ChildStorageUnitBatchId = sub.StorageUnitBatchId  
        join StorageUnit       su (nolock) on sub.StorageUnitId          = su.StorageUnitId  
        join Product            p (nolock) on su.ProductId               = p.ProductId  
       where cp.ParentStorageUnitBatchId = @StorageUnitBatchId  
        
      if @ChildProductCode is not null  
        set @ProductCode = @ProductCode + ' / ' + @ChildProductCode  
        
      if exists(select 1  
                 from AreaLocation al (nolock)  
                 join Area          a (nolock) on al.AreaId = a.AreaId  
                where al.LocationId = @LocationId  
                  and a.AreaCode in ('BK','R'))  
      begin  
        select @Quantity = subl.ActualQuantity / pk.Quantity  
          from Pallet                      p (nolock)  
          join StorageUnitBatch          sub (nolock) on p.StorageUnitBatchId = sub.StorageUnitBatchId  
          join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId  
                                                     and p.LocationId = subl.LocationId  
          join Pack                       pk (nolock) on sub.StorageUnitId    = pk.StorageUnitId  
         where pk.PackTypeId = 1  
           and p.PalletId    = @PalletId  
           and p.LocationId  = @LocationId  
           and pk.Quantity  != 999999  
      end  
    end  
    else  
      select @WarehouseId   = i.WarehouseId,  
             @ProductCode   = p.ProductCode,  
             @Product       = p.Product,  
             @SKUCode       = sku.SKUCode,  
             @Batch         = b.Batch,  
             @StartDate     = @GetDate,  
             @LocationId    = i.StoreLocationId,  
             @PalletId      = pl.PalletId,  
             @Prints        = 'Copy ' + convert(varchar(20), isnull(pl.Prints, 1)),  
             @Quantity      = i.ConfirmedQuantity,  
             @StorageUnitId = su.StorageUnitId,  
             @NettWeight    = j.NettWeight,  
             @GrossWeight   = j.Weight  
        from Instruction        i (nolock)  
        join Job                j (nolock) on i.JobId              = j.JobId  
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId  
        join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId  
        join Product            p (nolock) on su.ProductId         = p.ProductId  
        join SKU              sku (nolock) on su.SKUId             = sku.SKUId  
        join Batch              b (nolock) on sub.BatchId          = b.BatchId  
        left outer  
        join Pallet            pl (nolock) on i.PalletId           = pl.PalletId  
       where i.InstructionId = @InstructionId  
      
    if dbo.ufn_Configuration(183, @WarehouseId) = 1  
    begin  
      update @LabelVariables set Value = 'Gross Weight' where Value = '@WeightDesc'  
      update @LabelVariables set Value = CONVERT(varchar(20), @GrossWeight) where Value = '@Weight'  
    end  
    else if dbo.ufn_Configuration(184, @WarehouseId) = 1  
    begin  
      update @LabelVariables set Value = 'Nett Weight' where Value = '@WeightDesc'  
      update @LabelVariables set Value = CONVERT(varchar(20), @NettWeight) where Value = '@Weight'  
    end  
      
    select @Location = Location  
      from Location  
     where LocationId = @LocationId  
      
    if @Location is null  
    begin  
      set @Location = null  
        
      select top 1 @Location = a.AreaCode + '-'  
        from AreaLocation     al (nolock)  
        join Area              a (nolock) on al.AreaId = a.AreaId  
        join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId  
       where sua.StorageUnitId = @StorageUnitId  
      order by StoreOrder  
        
      select top 1 @Location = isnull(@Location,'') + isnull(l.Ailse,l.Location)  
        from StorageUnitLocation sul (nolock)  
        join Location              l (nolock) on sul.LocationId = l.LocationId  
       where sul.StorageUnitId = @StorageUnitId  
        
      if @Location is null  
        set @Location = 'NOT SETUP'  
    end  
      
    update @LabelVariables set Value = @ProductCode where Value = '@ProductCode'  
    update @LabelVariables set Value = @Product where Value = '@Product'  
    update @LabelVariables set Value = @SKUCode where Value = '@SKUCode'  
    update @LabelVariables set Value = @Batch where Value = '@Batch'  
    update @LabelVariables set Value = convert(varchar(20), @StartDate, 120) where Value = '@StartDate'  
    update @LabelVariables set Value = @Location where Value = '@Location'  
    update @LabelVariables set Value = 'P:' + isnull(convert(varchar(12), @PalletId),'none') where Value = '@PalletId'  
    update @LabelVariables set Value = @Prints where Value = '@Prints'  
    update @LabelVariables set Value = convert(varchar(50), convert(int, @Quantity)) where Value = '@Quantity'  
  end  
    
  if (@JobId is not null) and @Label = 'Reject Label.lbl'  
  begin  
    if @PalletId is not null  
      select top 1 @PalletId    = PalletId,  
             @InstructionId = InstructionId  
        from Instruction (nolock)  
       where PalletId = @PalletId  
      order by InstructionId  
    else if @instructionId is not null  
      select top 1 @PalletId    = PalletId,  
             @InstructionId = InstructionId  
        from Instruction (nolock)  
       where InstructionId = @InstructionId  
      order by InstructionId  
    else if @jobId is not null  
      select top 1 @PalletId      = PalletId,  
             @InstructionId = InstructionId  
        from Instruction (nolock)  
       where JobId = @jobId  
      order by InstructionId  
      
    select top 1 @Reason = Reason  
      from Exception e (nolock)  
      join Reason    r (nolock) on e.ReasonId = r.ReasonId  
     where e.InstructionId = @InstructionId  
       and r.ReasonCode like 'RW%'  
    order by e.ExceptionId desc  
      
    update @LabelVariables set Value = 'P:' + isnull(convert(varchar(12), @PalletId),'none') where Value = '@PalletId'  
    update @LabelVariables set Value = isnull(@Reason, 'Reason could not be found') where Value = '@Reason'  
  end  
    
  if (@PalletId is not null or @instructionId is not null or @jobId is not null) and @Label = 'Receiving & Staging Label.lbl'  
  begin  
    select @Title = 'Production Label'  
      
    if @PalletId is not null  
      select @ProductCode = p.ProductCode,  
             @Product     = p.Product,  
             @SKUCode     = sku.SKUCode,  
             @Batch       = b.Batch,  
             @StartDate   = @GetDate,  
             @PalletId    = pl.PalletId,  
             @Prints      = 'Copy ' + convert(varchar(20), isnull(pl.Prints, 1)),  
             @Quantity    = pl.Quantity  
        from Pallet            pl (nolock)  
        join StorageUnitBatch sub (nolock) on pl.StorageUnitBatchId = sub.StorageUnitBatchId  
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
        join Product            p (nolock) on su.ProductId          = p.ProductId  
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
        join Batch              b (nolock) on sub.BatchId           = b.BatchId  
       where pl.PalletId = @PalletId  
    else if @instructionId is not null  
      select @ProductCode = p.ProductCode,  
             @Product     = p.Product,  
             @SKUCode     = sku.SKUCode,  
             @Batch       = b.Batch,  
             @StartDate   = i.CreateDate,  
             @PalletId    = pl.PalletId,  
             @Prints      = 'Copy ' + convert(varchar(20), isnull(pl.Prints, 1)),  
             @Quantity    = i.Quantity  
        from Instruction        i (nolock)  
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId  
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
        join Product            p (nolock) on su.ProductId          = p.ProductId  
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
        join Batch              b (nolock) on sub.BatchId           = b.BatchId  
        left outer  
        join Pallet            pl (nolock) on i.PalletId           = pl.PalletId  
       where i.InstructionId = @InstructionId  
    else if @jobId is not null  
      select @ProductCode = p.ProductCode,  
             @Product     = p.Product,  
             @SKUCode     = sku.SKUCode,  
             @Batch       = b.Batch,  
             @StartDate   = i.CreateDate,  
             @PalletId    = pl.PalletId,  
             @Prints      = 'Copy ' + convert(varchar(20), isnull(pl.Prints, 1)),  
             @Quantity    = i.Quantity  
        from Instruction        i (nolock)  
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId  
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
        join Product            p (nolock) on su.ProductId          = p.ProductId  
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
        join Batch              b (nolock) on sub.BatchId           = b.BatchId  
        left outer  
        join Pallet            pl (nolock) on i.PalletId           = pl.PalletId  
       where i.JobId = @jobId  
      
    update @LabelVariables set Value = @ProductCode where Value = '@ProductCode'  
    update @LabelVariables set Value = @Product where Value = '@Product'  
    update @LabelVariables set Value = @SKUCode where Value = '@SKUCode'  
    update @LabelVariables set Value = @Batch where Value = '@Batch'  
    update @LabelVariables set Value = convert(varchar(20), @StartDate, 120) where Value = '@StartDate'  
    update @LabelVariables set Value = @Location where Value = '@Location'  
    update @LabelVariables set Value = 'P:' + isnull(convert(varchar(12), @PalletId),'none') where Value = '@PalletId'  
    update @LabelVariables set Value = @Prints where Value = '@Prints'  
    update @LabelVariables set Value = convert(varchar(10), @Quantity) where Value = '@Quantity'  
    update @LabelVariables set Value = @Title where Value = '@Title'  
  end  
    
  if (@JobId is not null) and @Label in ('Despatch By Route Label.lbl','Despatch By Order Label.lbl','Pick Job Label.lbl')  
  begin  
    select @OutboundShipmentId = min(i.OutboundShipmentId),  
           @IssueLineId        = min(i.IssueLineId),  
           @Weight             = isnull(max(j.Weight),0),  
           @StartDate          = convert(varchar(20), min(i.StartDate), 113),  
           @LocationId         = min(i.StoreLocationId),  
           @InstructionId      = min(i.InstructionId),  
           @WarehouseId        = min(i.WarehouseId),  
           @ReferenceNumber    = min(j.ReferenceNumber),  
           @Quantity           = sum(i.ConfirmedQuantity)  
      from Job         j (nolock)  
      join Instruction i (nolock) on j.JobId = i.JobId  
     where j.JobId = @JobId  
      
    --if @Label = 'Pick Job Label.lbl'  
    begin  
      update Job  
         set ReferenceNumber = 'J:' + convert(varchar(10), JobId)  
       where JobId = @JobId  
         and ReferenceNumber is null  
    end  
      
    if @OutboundShipmentId is not null  
    begin  
      select @DeliveryDate      = i.DeliveryDate,  
             @RouteId           = i.RouteId,  
             @IssueId           = i.IssueId  
        from Issue                   i (nolock)  
        join IssueLine              il (nolock) on i.IssueId = il.IssueId  
        join IssueLineInstruction  ili (nolock) on il.IssueLineId = ili.IssueLineId  
       where ili.InstructionId = @InstructionId  
        
      if dbo.ufn_Configuration(69, @WarehouseId) = 1  
      begin  
        select @ExternalCompanyId = od.ExternalCompanyId,  
               @DeliveryDate      = i.DeliveryDate,  
               @RouteId           = i.RouteId,  
               @OrderNumber       = od.OrderNumber  
          from OutboundDocument od (nolock)  
          join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId  
         where i.IssueId = @IssueId  
      end  
        
      insert @TablePallet  
            (KeyCol)  
      select distinct JobId  
        from Instruction (nolock)  
       where OutboundShipmentId = @OutboundShipmentId  
      order by JobId  
    end  
    else  
    begin  
      select @IssueId = IssueId  
        from IssueLine (nolock)  
       where IssueLineId = @IssueLineId  
        
      select @ExternalCompanyId = od.ExternalCompanyId,  
             @DeliveryDate      = i.DeliveryDate,  
             @RouteId           = i.RouteId,  
             @OrderNumber       = od.OrderNumber  
        from OutboundDocument od (nolock)  
        join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId  
       where @IssueId = i.IssueId  
        
      insert @TablePallet  
            (KeyCol)  
      select distinct ins.JobId  
        from Instruction ins (nolock)  
        join IssueLine    il (nolock) on ins.IssueLineId = il.IssueLineId  
       where il.IssueId = @IssueId  
      order by ins.JobId  
    end  
      
    if dbo.ufn_Configuration(248, @WarehouseId) = 1  
      select @Pallet = convert(varchar(10), (select min(IdentCol) from @TablePallet where KeyCol = @JobId))  
    else  
      select @Pallet = convert(varchar(10), (select min(IdentCol) from @TablePallet where KeyCol = @JobId))  
                     + ' of '  
                     + convert(varchar(10), (select count(1) from @TablePallet))  
      
    select @Customer = ExternalCompany  
      from ExternalCompany (nolock)  
     where ExternalCompanyId = @ExternalCompanyId  
      
    if dbo.ufn_Configuration(69, @WarehouseId) = 1  
    begin  
      select @Route = @OrderNumber + ' - ' + @Customer  
      --select @Route = substring(@Route, 1, 30)  
    end  
    else  
    begin  
      select @Route = Route  
        from Route (nolock)  
       where RouteId = @RouteId  
    end  
      
    if @Route is null  
      set @Route = 'NONE'  
      
    if @Weight is null or @Weight = 0  
     select @Weight = sum(i.ConfirmedQuantity * p.Weight)  
       from Instruction        i (nolock)  
       join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId  
       join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId  
      where p.Quantity = 1  
        and i.JobId = @JobId  
      
    select @Location = Location  
      from Location (nolock)  
     where LocationId = @LocationId  
      
    select @Prints = 'Copy 1'  
      
    update @LabelVariables set Value = @Prints where Value = '@Prints'  
    update @LabelVariables set Value = @Pallet where Value = '@Pallet'  
    update @LabelVariables set Value = @Customer where Value = '@Customer'  
    update @LabelVariables set Value = @Location where Value = '@Location'  
    update @LabelVariables set Value = 'J:' + convert(varchar(10), @JobId) where Value = '@JobId'  
    update @LabelVariables set Value = @OrderNumber where Value = '@OrderNumber'  
    update @LabelVariables set Value = @Pallet where Value = '@PickJob'  
    update @LabelVariables set Value = convert(varchar(20), @StartDate, 120) where Value = '@StartDate'  
    update @LabelVariables set Value = convert(varchar(20), @DeliveryDate, 120) where Value = '@DeliveryDate'  
    update @LabelVariables set Value = @Weight where Value = '@Weight'  
    update @LabelVariables set Value = @Route where Value = '@Route'  
    update @LabelVariables set Value = @ReferenceNumber where Value = '@ReferenceNumber'  
    update @LabelVariables set Value = convert(varchar(50), convert(int, @Quantity)) where Value = '@Units'  
  end  
    
  if @Label = 'Multi Purpose Label.lbl'  
  begin  
    set @Title = @Title  
      
    if @Title = 'Reference Label'  
      set @Barcode = 'R:' + @Barcode  
      
    if @Title = 'Pallet Label'  
      set @Barcode = 'P:' + @Barcode  
      
    update @LabelVariables set Value = @Barcode where Value = '@Barcode'  
    update @LabelVariables set Value = @Title where Value = '@Title'  
  end  
    
  if @Label = 'Product Label Small.lbl'  
  begin  
    update @LabelVariables set Value = @Barcode where Value = '@ProductCode'  
    --update @LabelVariables set Value = replicate(' ',50 - datalength(@Title)) + @Title where Value = '@Product'  
    update @LabelVariables set Value = '     ' + @Title + '     ' where Value = '@Product'  
    --update @LabelVariables set Value = @Title where Value = '@Product'  
    --update @LabelVariables set Value = substring(@Title,1,3) where Value = '@Product'  
  end  
    
  if @Label in ('Pickface Label Small.lbl','Pickface Label Large.lbl','Second Level Label Small.lbl','Second Level Label Large.lbl','Left Location Label Small.lbl','Left Location Label Large.lbl','Right Location Label Small.lbl','Right Location Label Larg
e.lbl')  
  begin  
    update @LabelVariables set Value = @Title where Value = '@Location'  
    update @LabelVariables set Value = @Barcode where Value = '@Level'  
      
    if (select dbo.ufn_Configuration(217, 1)) = 1 -- NB - Warehouse Parameter Defaulted to 1  
    begin  
      set @SecurityCode = @Title  
    end  
    else  
    begin  
      select @SecurityCode = SecurityCode  
        from Location (nolock)  
       where Location = @Title  
 end  
   
    update @LabelVariables set Value = @SecurityCode where Value = '@SecurityCode'  
  end  
    
  if @Label in ('Racking Label Small.lbl','Racking Label Large.lbl')  
  begin  
    update @LabelVariables set Value = @Title where Value = '@Level'  
  end  
    
  if @Label in ('Security Code Label Small.lbl','Security Code Label Large.lbl')  
  begin  
    update @LabelVariables set Value = @Title where Value = '@SecurityCode'  
  end  
    
  if (@KeyId is not null) and @Label in ('Sample Label Small.lbl','Sample Label Large.lbl','Product Batch Label Small.lbl','Product Batch Label Large.lbl')  
  begin  
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @KeyId  
      
    if (select dbo.ufn_Configuration(178, 1)) = 1 -- NB - Warehouse Parameter Defaulted to 1  
    begin  
      select @StorageUnitId = sub.StorageUnitId,  
             @Batch         = b.Batch,  
             @Prints        = 'Copy ' + convert(varchar(20), isnull(b.Prints, 1)),  
             @StartDate     = b.CreateDate,  
             @Product       = p.Product,  
             @ProductAlias  = isnull(suec.ProductCode,''),  
             @ProductCode   = p.ProductCode,  
             @StatusCode    = s.StatusCode  
        from ReceiptLine       rl (nolock)  
        join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId  
        join Batch              b (nolock) on sub.BatchId           = b.BatchId  
        join Status             s (nolock) on b.StatusId            = s.StatusId  
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
        join Product            p (nolock) on su.ProductId          = p.ProductId  
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
        left  
        join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId  
       where rl.ReceiptLineId = @KeyId  
    end  
    else  
    begin  
      select @StorageUnitId = sub.StorageUnitId,  
             @Batch         = b.Batch,  
             @Prints        = 'Copy ' + convert(varchar(20), isnull(b.Prints, 1)),  
             @StartDate     = b.CreateDate,  
             @Product       = p.Product,  
             @ProductAlias  = isnull(suec.ProductCode,''),  
             @ProductCode   = p.ProductCode,  
             @StatusCode    = s.StatusCode  
        from Batch              b (nolock)  
        join Status             s (nolock) on b.StatusId            = s.StatusId  
        join StorageUnitBatch sub (nolock) on b.BatchId             = sub.BatchId  
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
        join Product            p (nolock) on su.ProductId          = p.ProductId  
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
        left  
        join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId  
       where b.BatchId = @KeyId  
    end  
      
    if @Location is null  
    begin  
      set @Location = null  
        
      select top 1 @Location = a.AreaCode + '-'  
        from AreaLocation al (nolock)  
        join Area          a (nolock) on al.AreaId = a.AreaId  
        join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId  
       where sua.StorageUnitId = @StorageUnitId  
      order by StoreOrder  
        
      select top 1 @Location = isnull(@Location,'') + isnull(l.Ailse,l.Location)  
        from StorageUnitLocation sul (nolock)  
        join Location              l (nolock) on sul.LocationId = l.LocationId  
       where sul.StorageUnitId = @StorageUnitId  
        
      if @Location is null  
        set @Location = 'NOT SETUP'  
    end  
      
    select @SKUCode  = pt.PackType,  
           @Quantity = p.Quantity  
      from Pack               p (nolock)  
      join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId  
    where pt.InboundSequence > 1  
      and p.Quantity         > 1  
      and p.StorageUnitId    = @StorageUnitId  
      
    if @StatusCode = 'A'  
    begin  
      update @LabelVariables set Value = 'RELEASED' where Value = '@Status'  
      update @LabelVariables set Value = 'PRODUCT' where Value = '@Title'  
    end  
    else  
    begin  
      update @LabelVariables set Value = 'QUARANTINE' where Value = '@Status'  
      update @LabelVariables set Value = 'RECEIVING' where Value = '@Title'  
    end  
      
    update @LabelVariables set Value = @StatusCode + ':' + @Batch where Value = '@Batch'  
    update @LabelVariables set Value = @Prints where Value = '@Copy'  
    update @LabelVariables set Value = convert(varchar(16), @StartDate, 120) where Value = '@DateAndTime'  
    update @LabelVariables set Value = @Product where Value = '@Product'  
    update @LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'  
    update @LabelVariables set Value = @ProductCode where Value = '@ProductCode'  
    update @LabelVariables set Value = @Location where Value = '@Aisle'  
    update @LabelVariables set Value = @SKUCode where Value = '@SKUCode'  
    update @LabelVariables set Value = @Quantity where Value = '@Quantity'  
  end  
    
  if (@KeyId is not null) and @Label in ('Picking Label Small.lbl','Picking Label Large.lbl')  
  begin  
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @KeyId  
      
    select @InstructionId = isnull(i.InstructionRefId, i.InstructionId),  
           @barcode      = j.ReferenceNumber,  
           @Batch        = b.Batch,  
           @Prints       = 'Copy ' + convert(varchar(20), isnull(b.Prints, 1)),  
           @StartDate    = b.CreateDate,  
           @Product      = p.Product,  
           @ProductAlias = isnull(suec.ProductCode,''),  
           @ProductCode  = p.ProductCode,  
           @StatusCode   = s.StatusCode  
      from Job                j (nolock)  
      join Instruction        i (nolock) on j.JobId               = i.JobId  
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId  = sub.StorageUnitBatchId  
      join Batch              b (nolock) on sub.BatchId           = b.BatchId  
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
      join Status             s (nolock) on b.StatusId            = s.StatusId  
      join Product            p (nolock) on su.ProductId          = p.ProductId  
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
      left  
      join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId  
     where j.JobId = @KeyId  
      
    if @barcode is null  
    begin  
      select @barcode = od.OrderNumber,  
             @OutboundDocumentId = od.OutboundDocumentId  
        from IssueLineInstruction ili (nolock)  
        join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId  
       where ili.InstructionId = @InstructionId  
        
      select @Count = count(1),  
             @JobId = min(i.JobId)  
        from IssueLineInstruction ili (nolock)  
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId  
       where ili.OutboundDocumentId = @OutboundDocumentId  
        
      select @barcode = 'R:' + @barcode +':'+ convert(varchar(10), (@KeyId - @JobId + 1)) + 'of' + convert(varchar(10), @Count)  
        
      update Job  
         set ReferenceNumber = @barcode  
       where JobId = @KeyId    
    end  
      
    if @StatusCode = 'A'  
    begin  
      update @LabelVariables set Value = 'RELEASED' where Value = '@Status'  
      update @LabelVariables set Value = 'PRODUCT' where Value = '@Title'  
    end  
    else  
    begin  
      update @LabelVariables set Value = 'QUARANTINE' where Value = '@Status'  
      update @LabelVariables set Value = 'RECEIVING' where Value = '@Title'  
    end  
      
    update @LabelVariables set Value = @StatusCode + ':' + @Batch where Value = '@Batch'  
    update @LabelVariables set Value = @Prints where Value = '@Copy'  
    update @LabelVariables set Value = convert(varchar(16), @StartDate, 120) where Value = '@DateAndTime'  
    update @LabelVariables set Value = @Product where Value = '@Product'  
    update @LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'  
    update @LabelVariables set Value = @ProductCode where Value = '@ProductCode'  
    update @LabelVariables set Value = isnull(@barcode,'') where Value = '@ReferenceNumber'  
  end  
    
  if (@KeyId is not null) and @Label in ('Verify Batch Label Large Version 2.lbl','Verify Batch Label Small.lbl')  
  begin  
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @KeyId  
      
    select @Batch        = b.Batch,  
           @Prints       = 'Copy ' + convert(varchar(20), isnull(b.Prints, 1)),  
           @StartDate    = b.CreateDate,  
           @ExpiryDate   = b.ExpiryDate,  
           @Product      = p.Product,  
           @ProductAlias = isnull(suec.ProductCode,''),  
           @ProductCode  = p.ProductCode,  
           @StatusCode   = s.StatusCode  
      from Batch              b (nolock)  
      join Status             s (nolock) on b.StatusId            = s.StatusId  
      join StorageUnitBatch sub (nolock) on b.BatchId             = sub.BatchId  
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
      join Product            p (nolock) on su.ProductId          = p.ProductId  
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
      left  
      join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId  
     where sub.StorageUnitBatchId = @KeyId  
      
    if @StatusCode = 'A'  
    begin  
      update @LabelVariables set Value = 'RELEASED' where Value = '@Status'  
      update @LabelVariables set Value = 'PRODUCT' where Value = '@Title'  
    end  
    else  
    begin  
      update @LabelVariables set Value = 'QUARANTINE' where Value = '@Status'  
      update @LabelVariables set Value = 'RECEIVING' where Value = '@Title'  
    end  
      
    update @LabelVariables set Value = @StatusCode + ':' + @Batch where Value = '@Batch'  
    update @LabelVariables set Value = @Prints where Value = '@Copy'  
    update @LabelVariables set Value = convert(varchar(16), @StartDate, 120) where Value = '@DateAndTime'  
    update @LabelVariables set Value = @Product where Value = '@Product'  
    update @LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'  
    update @LabelVariables set Value = @ProductCode where Value = '@ProductCode'  
    update @LabelVariables set Value = '' where Value = '@Quantity'  
    update @LabelVariables set Value = isnull(convert(varchar(10), @ExpiryDate, 120),'None') where Value = '@ExpiryDate'  
  end  
    
  if (@KeyId is not null) and @Label in ('Receiving Batch Label Large.lbl','Receiving Batch Label Small.lbl')  
  begin  
    select @BatchId      = b.BatchId,  
           @Batch        = b.Batch,  
           @Prints       = 'Copy ' + convert(varchar(20), isnull(b.Prints, 1)),  
           @StartDate    = b.CreateDate,  
           @ExpiryDate   = b.ExpiryDate,  
           @Product      = p.Product,  
           @ProductAlias = isnull(suec.ProductCode,''),  
           @ProductCode  = p.ProductCode,  
           @StatusCode   = s.StatusCode,  
           @Quantity     = rl.AcceptedQuantity  
      from ReceiptLine       rl (nolock)  
      join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId  
      join Batch              b (nolock) on sub.BatchId           = b.BatchId  
      join Status             s (nolock) on b.StatusId            = s.StatusId  
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
      join Product            p (nolock) on su.ProductId          = p.ProductId  
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
      left  
      join StorageUnitExternalCompany suec on su.StorageUnitId    = suec.StorageUnitId  
     where rl.ReceiptLineId = @KeyId  
      
    update Batch set Prints = isnull(Prints,0) + 1 where BatchId = @BatchId  
      
    if @StatusCode = 'A'  
    begin  
      update @LabelVariables set Value = 'RELEASED' where Value = '@Status'  
      update @LabelVariables set Value = 'PRODUCT' where Value = '@Title'  
    end  
    else  
    begin  
      update @LabelVariables set Value = 'QUARANTINE' where Value = '@Status'  
      update @LabelVariables set Value = 'RECEIVING' where Value = '@Title'  
    end  
      
    update @LabelVariables set Value = @StatusCode + ':' + @Batch where Value = '@Batch'  
    update @LabelVariables set Value = @Prints where Value = '@Copy'  
    update @LabelVariables set Value = convert(varchar(16), @StartDate, 120) where Value = '@DateAndTime'  
    update @LabelVariables set Value = @Product where Value = '@Product'  
    update @LabelVariables set Value = @ProductAlias where Value = '@ProductAlias'  
    update @LabelVariables set Value = @ProductCode where Value = '@ProductCode'  
    update @LabelVariables set Value = convert(int, @Quantity) where Value = '@Quantity'  
    update @LabelVariables set Value = isnull(convert(varchar(10), @ExpiryDate, 120),'None') where Value = '@ExpiryDate'  
  end  
    
  if (@KeyId is not null) and @Label in ('Sign In Card.lbl')  
  begin  
    select @OperatorGroup = og.OperatorGroup,  
           @Operator      = o.Operator,  
           @Password      = o.Password  
      from Operator       o (nolock)  
      join OperatorGroup og (nolock)on o.OperatorGroupId = og.OperatorGroupId  
     where o.OperatorId = @KeyId  
      
    update @LabelVariables set Value = @OperatorGroup where Value = '@OperatorGroup'  
    update @LabelVariables set Value = @Operator where Value = '@Operator'  
    update @LabelVariables set Value = @Password where Value = '@Password'  
  end  
  
  select @XMLBody =  '<?xml version="1.0" encoding="utf-16"?>' +
  (select
        (select Name,  
             Value,  
             Length,  
             FixedLength,  
             IsMultiline,  
             LineLength,  
             LineCount,  
             FormatID,  
             Prompt,  
             ValueRequired  
        from @LabelVariables  
        for XML Path('Variable'), TYPE)
    for XML PATH('root'))
end  

