﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ShipmentIssue_Update
  ///   Filename       : p_ShipmentIssue_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:51
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null,
  ///   @IssueId int = null,
  ///   @DropSequence smallint = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ShipmentIssue_Update
(
 @ShipmentId int = null,
 @IssueId int = null,
 @DropSequence smallint = null 
)
 
as
begin
	 set nocount on;
  
  if @ShipmentId = '-1'
    set @ShipmentId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
	 declare @Error int
 
  update ShipmentIssue
     set DropSequence = isnull(@DropSequence, DropSequence) 
   where ShipmentId = @ShipmentId
     and IssueId = @IssueId
  
  select @Error = @@Error
  
  
  return @Error
  
end
