﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundPerformance_Select
  ///   Filename       : p_OutboundPerformance_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:36
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundPerformance table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null 
  /// </param>
  /// <returns>
  ///   OutboundPerformance.JobId,
  ///   OutboundPerformance.CreateDate,
  ///   OutboundPerformance.Release,
  ///   OutboundPerformance.StartDate,
  ///   OutboundPerformance.EndDate,
  ///   OutboundPerformance.Checking,
  ///   OutboundPerformance.Despatch,
  ///   OutboundPerformance.DespatchChecked,
  ///   OutboundPerformance.Complete,
  ///   OutboundPerformance.Checked,
  ///   OutboundPerformance.PlanningComplete,
  ///   OutboundPerformance.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundPerformance_Select
(
 @JobId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         OutboundPerformance.JobId
        ,OutboundPerformance.CreateDate
        ,OutboundPerformance.Release
        ,OutboundPerformance.StartDate
        ,OutboundPerformance.EndDate
        ,OutboundPerformance.Checking
        ,OutboundPerformance.Despatch
        ,OutboundPerformance.DespatchChecked
        ,OutboundPerformance.Complete
        ,OutboundPerformance.Checked
        ,OutboundPerformance.PlanningComplete
        ,OutboundPerformance.WarehouseId
    from OutboundPerformance
   where isnull(OutboundPerformance.JobId,'0')  = isnull(@JobId, isnull(OutboundPerformance.JobId,'0'))
  
end
