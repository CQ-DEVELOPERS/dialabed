﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Exception
  ///   Filename       : p_Report_Exception.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Exception 
(
 @WarehouseId int
)
 
as
begin
  set nocount on;

  Select e.Exception,
         e.ExceptionDate,
         Isnull(rl.OrderNumber,il.OrderNumber) as OrderNumber,
         rl.SupplierCode as 'CompanyCode',
         rl.SupplierName as 'Company',
         rl.ProductCode,
         rl.Product,
         rl.SkuCode,
         rl.RequiredQuantity,
         rl.Batch,
         rl.ExpiryDate,
         rl.OperatorId
    from exception e
    Left Join viewReceiptLine rl on rl.ReceiptLineId = e.ReceiptLineId
    Left Join viewIssueLine il on il.IssueLineId = e.IssueLineId  
   where isnull(rl.warehouseId,il.warehouseId) = @WarehouseId
end
