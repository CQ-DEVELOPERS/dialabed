﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Unlinked_Jobs
  ///   Filename       : p_Pallet_Rebuild_Unlinked_Jobs.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Unlinked_Jobs
(
 @OutboundShipmentId int,
 @IssueId            int
)
 
as
begin
	 set nocount on;
	 
	 if @OutboundShipmentId = -1
	   set @OutboundShipmentId = null
	 
	 if @IssueId = -1
	   set @IssueId = null
  
  select Distinct j.JobId,
         j.ReferenceNumber,
         s.Status,
         o.Operator
    from IssueLineInstruction ili (nolock)
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
    join Job                    j (nolock) on i.JobId           = j.JobId
    join Status                 s (nolock) on j.StatusId        = s.StatusId
    left
    join Operator               o (nolock) on j.OperatorId      = o.OperatorId
   where isnull(ili.OutboundShipmentId,-1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId,-1))
     and ili.IssueId                       = isnull(@IssueId, ili.IssueId)
end
