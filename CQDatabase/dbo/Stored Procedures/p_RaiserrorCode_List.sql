﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RaiserrorCode_List
  ///   Filename       : p_RaiserrorCode_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:36
  /// </summary>
  /// <remarks>
  ///   Selects rows from the RaiserrorCode table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   RaiserrorCode.RaiserrorCodeId,
  ///   RaiserrorCode.RaiserrorCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RaiserrorCode_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as RaiserrorCodeId
        ,'{All}' as RaiserrorCode
  union
  select
         RaiserrorCode.RaiserrorCodeId
        ,RaiserrorCode.RaiserrorCode
    from RaiserrorCode
  
end
