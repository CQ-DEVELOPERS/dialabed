﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_Check
  ///   Filename       : p_Outbound_Palletise_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Palletise_Check
(
 @OutboundShipmentId int,
 @IssueId            int
)
 
as
begin
	 set nocount on;
  
  declare @ilQuantity        numeric(13,6),
          @iliQuantity       numeric(13,6),
          @insQuantity       numeric(13,6)
  
  if @OutboundShipmentId is not null
  begin
    select @ilQuantity = sum((isnull(AllocationCategory, 100.000) / 100.000) * convert(numeric(13,6), isnull(il.SOHQuantity, il.Quantity)))
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId           = i.IssueId
      join IssueLine              il (nolock) on i.IssueId             = il.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                 p (nolock) on su.ProductId          = p.ProductId
      left 
      join StorageUnitExternalCompany suec on su.StorageUnitId     = suec.StorageUnitId
                                          and od.ExternalCompanyId = suec.ExternalCompanyId
     where osi.OutboundShipmentId    = @OutboundShipmentId
       and isnull(p.ProductType,'') != 'PRODUCTION'
    
    select @iliQuantity = sum(ili.Quantity)
      from Instruction            i
      join IssueLineInstruction ili on i.InstructionId = ili.InstructionId
     where ili.OutboundShipmentId = @OutboundShipmentId
    
    select @insQuantity = sum(i.Quantity)
      from Instruction            i
     where i.InstructionId in (select ili.InstructionId from IssueLineInstruction ili where ili.OutboundShipmentId = @OutboundShipmentId)
    
    --select @ilQuantity as '@ilQuantity', @iliQuantity as '@iliQuantity', @insQuantity as '@insQuantity'
  end
  else if @IssueId is not null
  begin
    select @ilQuantity = sum((isnull(AllocationCategory, 100.000) / 100.000) * convert(numeric(13,6), isnull(il.SOHQuantity, il.Quantity)))
      from Issue                   i (nolock)
      join IssueLine              il (nolock) on i.IssueId             = il.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                 p (nolock) on su.ProductId          = p.ProductId
      left 
      join StorageUnitExternalCompany suec on su.StorageUnitId     = suec.StorageUnitId
                                          and od.ExternalCompanyId = suec.ExternalCompanyId
     where i.IssueId = @IssueId
       and isnull(p.ProductType,'') != 'PRODUCTION'
    
    select @iliQuantity = sum(ili.Quantity)
      from Instruction            i
      join IssueLineInstruction ili on i.InstructionId = ili.InstructionId
     where ili.IssueId               = @IssueId
    
    select @insQuantity = sum(i.Quantity)
      from Instruction            i
     where i.InstructionId in (select ili.InstructionId
                                 from IssueLineInstruction ili
                                where ili.IssueId = @IssueId)
    
    --select @ilQuantity as '@ilQuantity', @iliQuantity as '@iliQuantity', @insQuantity as '@insQuantity'
  end
  
  if @ilQuantity = @insQuantity and @ilQuantity = @iliQuantity
    return 0
  
  print isnull(convert(nvarchar(30), @ilQuantity),'null') + ' IssueLine Quantity'
  print isnull(convert(nvarchar(30), @insQuantity),'null') + ' Instruction Quantity'
  print isnull(convert(nvarchar(30), @iliQuantity),'null') + ' IssueLineInstruction Quantity'
  return -1
end
