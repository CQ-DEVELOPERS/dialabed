﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportHeading_Delete
  ///   Filename       : p_ReportHeading_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:25
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ReportHeading table.
  /// </remarks>
  /// <param>
  ///   @ReportHeadingId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportHeading_Delete
(
 @ReportHeadingId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ReportHeading
     where ReportHeadingId = @ReportHeadingId
  
  select @Error = @@Error
  
  
  return @Error
  
end
