﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Valid_Stock_Link
  ///   Filename       : p_Mobile_Check_Valid_Stock_Link.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_Valid_Stock_Link
(
 @location nvarchar(15),
 @product  nvarchar(30),
 @warehouseId int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
      WarehouseId        int,
      StorageUnitBatchId int,
      StorageUnitId      int,
      Location           nvarchar(15),
      ProductCode        nvarchar(30),
      Product            nvarchar(255),
      SKUCode            nvarchar(max),
      Batch              nvarchar(50),
      ActualQuantity     float,
      AllocatedQuantity  float,
      ReservedQuantity   float,
      Boxes              nvarchar(50),
      BoxesQuantity      nvarchar(10),
      Pickface           nvarchar(10)
	 )
	 
	 declare @bool               bit,
	         @PalletId           int,
	         @StorageUnitBatchId int,
          @LocationId         int,
          @StorageUnitId      int
  
  select @location = replace(@location,'L:','')
  
  if @product like 'P:%'
    if isnumeric(replace(@product,'P:','')) = 1
      select @PalletId = replace(@product,'P:',''),
             @product  = null
  
  if @PalletId is not null
  begin
    select @StorageUnitBatchId = StorageUnitBatchId
      from Pallet
     where PalletId = @PalletId
    
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch
     where StorageUnitBatchId = @StorageUnitBatchId
    
    if 'BK' in (select a.AreaCode
                  from Location      l (nolock)
                  join AreaLocation al (nolock) on l.LocationId = al.LocationId
                  join Area          a (nolock) on al.AreaId    = a.AreaId)
    begin
      insert @TableResult
            (WarehouseId,
             StorageUnitBatchId,
             StorageUnitId,
             Location,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select a.WarehouseId,
             sub.StorageUnitBatchId,
             su.StorageUnitId,
             l.Location,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             subl.ActualQuantity,
             subl.AllocatedQuantity,
             subl.ReservedQuantity
        from StorageUnitBatch          sub (nolock)
        join StorageUnit                su (nolock) on sub.StorageUnitId     = su.StorageUnitId
        join Product                     p (nolock) on su.ProductId          = p.ProductId
        join SKU                       sku (nolock) on su.SKUId              = sku.SKUId
        join Batch                       b (nolock) on sub.BatchId           = b.BatchId
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
        join Location                    l (nolock) on subl.LocationId        = l.LocationId
        join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
       where su.StorageUnitId = @StorageUnitId
         and l.Location       = @Location
    end
    else
    begin
      insert @TableResult
            (WarehouseId,
             StorageUnitBatchId,
             StorageUnitId,
             Location,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select a.WarehouseId,
             sub.StorageUnitBatchId,
             su.StorageUnitId,
             l.Location,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             subl.ActualQuantity,
             subl.AllocatedQuantity,
             subl.ReservedQuantity
        from StorageUnitBatch          sub (nolock)
        join StorageUnit                su (nolock) on sub.StorageUnitId     = su.StorageUnitId
        join Product                     p (nolock) on su.ProductId          = p.ProductId
        join SKU                       sku (nolock) on su.SKUId              = sku.SKUId
        join Batch                       b (nolock) on sub.BatchId           = b.BatchId
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
        join Location                    l (nolock) on subl.LocationId        = l.LocationId
        join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
       where subl.StorageUnitBatchId = @StorageUnitBatchId
         and l.Location              = @Location
    end
  end
  
  if @product is not null
  begin
    select @StorageUnitId = StorageUnitId
      from Product p
      join StorageUnit su on p.ProductId = su.ProductId
     where p.ProductCode = @product
        or p.Barcode     = @product
    
    if @StorageUnitId is null
    begin
      select @StorageUnitId = p.StorageUnitId
        from StorageUnit su
        join Pack         p on p.StorageUnitId = su.StorageUnitId
       where p.Barcode     = @product
    end
    
    if @StorageUnitId is null
    begin
      insert @TableResult
            (WarehouseId,
             StorageUnitBatchId,
             StorageUnitId,
             Location,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select a.WarehouseId,
             sub.StorageUnitBatchId,
             su.StorageUnitId,
             l.Location,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             subl.ActualQuantity,
             subl.AllocatedQuantity,
             subl.ReservedQuantity
        from StorageUnitBatch          sub (nolock)
        join StorageUnit                su (nolock) on sub.StorageUnitId     = su.StorageUnitId
        join Product                     p (nolock) on su.ProductId          = p.ProductId
        join SKU                       sku (nolock) on su.SKUId              = sku.SKUId
        join Batch                       b (nolock) on sub.BatchId           = b.BatchId
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
        join Location                    l (nolock) on subl.LocationId        = l.LocationId
        join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
       where p.Product like '%' + @product + '%'
         and l.Location       = @Location
    end
    else
    begin
      insert @TableResult
            (WarehouseId,
             StorageUnitBatchId,
             StorageUnitId,
             Location,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select a.WarehouseId,
             sub.StorageUnitBatchId,
             su.StorageUnitId,
             l.Location,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             subl.ActualQuantity,
             subl.AllocatedQuantity,
             subl.ReservedQuantity
        from StorageUnitBatch          sub (nolock)
        join StorageUnit                su (nolock) on sub.StorageUnitId     = su.StorageUnitId
        join Product                     p (nolock) on su.ProductId          = p.ProductId
        join SKU                       sku (nolock) on su.SKUId              = sku.SKUId
        join Batch                       b (nolock) on sub.BatchId           = b.BatchId
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
        join Location                    l (nolock) on subl.LocationId        = l.LocationId
        join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
       where su.StorageUnitId = @StorageUnitId
         and l.Location       = @Location
    end
  end
  
  if @PalletId is null and @product is null
  begin
      insert @TableResult
            (WarehouseId,
             StorageUnitBatchId,
             StorageUnitId,
             Location,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
    select soh.WarehouseId,
           soh.StorageUnitBatchId,
           soh.StorageUnitId,
           soh.Location,
           soh.ProductCode,
           soh.Product,
           soh.SKUCode,
           soh.Batch,
           soh.ActualQuantity,
           soh.AllocatedQuantity,
           soh.ReservedQuantity
      from viewSOH soh
     where 1 = 2
  end
  
  update tr
     set Boxes = pt.PackType,
         BoxesQuantity = p.Quantity
    from @TableResult      tr
    join Pack               p (nolock) on tr.WarehouseId        = p.WarehouseId
                                      and tr.StorageUnitId      = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
  where pt.InboundSequence > 1
    and p.Quantity         > 1
  
  update @TableResult
     set SKUCode = SKUCode + isnull(' (' +  Boxes + ' of ' + BoxesQuantity + ' )','')
   where convert(float, BoxesQuantity) >= 1
  
  update tr
     set Pickface = l.Location
    from @TableResult         tr
    join StorageUnitLocation sul (nolock) on tr.StorageUnitid = sul.StorageUnitId
    join Location              l (nolock) on sul.LocationId = l.LocationId
    join AreaLocation         al (nolock) on l.LocationId = al.LocationId
    join Area                  a (nolock) on al.Areaid = a.AreaId
   where a.AreaCode in ('PK','SP')
  
  select StorageUnitBatchId,
         Location,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         Pickface
    from @TableResult
end
