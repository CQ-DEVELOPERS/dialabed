﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Lines_Received
  ///   Filename       : p_Receiving_Lines_Received.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Lines_Received
(
 @inboundShipmentId int = null,
 @receiptId         int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TotalLines    int,
	         @LinesReceived int
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @InboundShipmentId in ('-1','0')
    set @InboundShipmentId = null
  
  if @InboundShipmentId is not null
    set @ReceiptId = null
  
  if @InboundShipmentId is null
  begin
    select @TotalLines = count(1)
      from ReceiptLine (nolock)
     where ReceiptId = @ReceiptId
    
    select @LinesReceived = count(1)
      from ReceiptLine rl
      join Status       s (nolock) on rl.StatusId         = s.StatusId
     where rl.ReceiptId = @ReceiptId
       and s.Type       = 'R'
       and s.StatusCode in ('R','P','LA')
  end
  else
  begin
    select @TotalLines = count(1)
      from ReceiptLine             rl (nolock)
      join InboundShipmentReceipt isr (nolock) on rl.ReceiptId = isr.ReceiptId
     where isr.InboundShipmentId = @InboundShipmentId
    
    select @LinesReceived = count(1)
      from ReceiptLine             rl (nolock)
      join Status                   s (nolock) on rl.StatusId  = s.StatusId
      join InboundShipmentReceipt isr (nolock) on rl.ReceiptId = isr.ReceiptId
     where isr.InboundShipmentId = @InboundShipmentId
       and s.Type = 'R'
       and s.StatusCode in ('R','P','LA')
  end
  
  select @LinesReceived     as 'LinesReceived',
         @TotalLines        as 'TotalLines'
end
 
