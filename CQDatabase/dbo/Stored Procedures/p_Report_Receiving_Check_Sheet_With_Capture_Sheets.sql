﻿/*
  /// <summary>
  ///   Procedure Name : p_Report_Receiving_Check_Sheet_With_Capture_Sheets
  ///   Filename       : p_Report_Receiving_Check_Sheet_With_Capture_Sheets.sql
  ///   Create By      : Karen
  ///   Date Created   : Jan 2014
  /// </summary>
  /// <remarks>
  ///  
  /// </remarks>
  /// <param>
  ///  
  /// </param>
  /// <returns>
  ///  
  /// </returns>
  /// <newpara>
  ///   Modified by    :
  ///   Modified Date  :
  ///   Details        :
  /// </newpara>
*/
CREATE procedure p_Report_Receiving_Check_Sheet_With_Capture_Sheets
(
@ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
@InboundShipmentId int = null,
@ReceiptId         int = null,
@ReceiptLineId     int = null
)
 
as
begin

  set nocount on;
  
  declare @PrintSerial bit,
		  @PrintBatch bit
		  
   select @PrintSerial = 1
    from  Receipt r 
	join ReceiptLine rl (nolock) on r.ReceiptId = rl.ReceiptId
	join StorageUnitBatch sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
	join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
   where su.SerialTracked = 1 
     and r.ReceiptId = @ReceiptId
     
  select @PrintBatch = 1
    from  Receipt r 
	join ReceiptLine rl (nolock) on r.ReceiptId = rl.ReceiptId
	join StorageUnitBatch sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
	join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
   where su.LotAttributeRule != 'None' 
     and r.ReceiptId = @ReceiptId
   
   
 
  select @ConnectionString as ConnectionString,
		 @InboundShipmentId as InboundShipmentId,
		 @ReceiptId as ReceiptId,
		 @ReceiptLineId as ReceiptLineId,
		 @PrintSerial as PrintSerial,
		 @PrintBatch as PrintBatch

end
 
 
