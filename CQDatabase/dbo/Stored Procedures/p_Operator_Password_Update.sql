﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Password_Update
  ///   Filename       : p_Operator_Password_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Dec 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Password_Update
(
 @OperatorId int,
 @Password   nvarchar(50)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_Operator_Update
  @OperatorId = @OperatorId,
  @Password   = @Password
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Operator_Password_Update'
    rollback transaction
    return @Error
end
