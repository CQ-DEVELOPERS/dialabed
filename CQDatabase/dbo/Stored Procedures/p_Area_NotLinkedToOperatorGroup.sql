﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_NotLinkedToOperatorGroup
  ///   Filename       : p_Area_NotLinkedToOperatorGroup.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_NotLinkedToOperatorGroup
( 
@OperatorGroupId	int
)

 
as
begin
	 set nocount on;
  
  select a.AreaId,
		 a.Area,
		 a.AreaCode
    from Area a
    where not exists(select 1 from AreaOperatorGroup aog where a.AreaId = aog.AreaId
														and aog.OperatorGroupId = @OperatorGroupId)
	order by Area
end
