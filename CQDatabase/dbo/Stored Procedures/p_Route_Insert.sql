﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Route_Insert
  ///   Filename       : p_Route_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:06
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Route table.
  /// </remarks>
  /// <param>
  ///   @RouteId int = null output,
  ///   @Route nvarchar(100) = null,
  ///   @RouteCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   Route.RouteId,
  ///   Route.Route,
  ///   Route.RouteCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Route_Insert
(
 @RouteId int = null output,
 @Route nvarchar(100) = null,
 @RouteCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @Route = '-1'
    set @Route = null;
  
  if @RouteCode = '-1'
    set @RouteCode = null;
  
	 declare @Error int
 
  insert Route
        (Route,
         RouteCode)
  select @Route,
         @RouteCode 
  
  select @Error = @@Error, @RouteId = scope_identity()
  
  
  return @Error
  
end
