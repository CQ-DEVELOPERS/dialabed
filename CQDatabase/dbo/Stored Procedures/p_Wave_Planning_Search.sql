﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Search
  ///   Filename       : p_Wave_Planning_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Search
(
 @WarehouseId            int,
 @Wave               	   nvarchar(50),
 @StatusId              int,
 @FromDate	              datetime,
 @ToDate	                datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId    int,
   TotalOrders           int,
   NumberOfOrders        int,
   ShipmentDate         datetime,
   StatusId             int,
   Status               nvarchar(50),
   LocationId           int,
   Location             nvarchar(15),
   RouteId              int,
   Route                nvarchar(50),
   CreateDate           datetime,
   SealNumber			nvarchar(30),
   VehicleRegistration	nvarchar(10),
   Remarks				nvarchar(250),
   DespatchBay			int,
   DespatchBayName      nvarchar(15),
   ContactListId		int,
   ContactPerson		nvarchar(50)
  );
  
  if @Wave is null
    set @Wave = ''
  
  if @StatusId = -1
     set @StatusId = null
  
  select w.WaveId,
         w.Wave,
         w.CreateDate,
         s.StatusId,
         s.Status
    from Wave w (nolock)
    join Status s (nolock) on w.StatusId = s.StatusId
                          and s.StatusId = ISNULL(@StatusId, s.StatusId)
   where w.WarehouseId      = @WarehouseId
     and w.Wave          like @Wave + '%'
     and w.CreateDate between @FromDate and @ToDate
end
