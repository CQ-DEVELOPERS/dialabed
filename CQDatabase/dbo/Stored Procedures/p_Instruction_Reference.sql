﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Reference
  ///   Filename       : p_Instruction_Reference.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Reference
(
 @instructionId int output
)
 
as
begin
	 set nocount on;
  
  declare @Error                int,
          @Errormsg             nvarchar(500),
          @GetDate              datetime,
          @RemainingQuantity    numeric(13,6),
          @ConfirmedQuantity    numeric(13,6),
          @ConfirmedWeight      numeric(13,6),
          @InstructionRefId     int,
          @StatusId             int,
          @WarehouseId          int,
          @PickLocationId       int,
          @StorageUnitBatchId   int,
          @OldStorageUnitBatchId int,
          @StorageUnitId        int,
          @InstructionTypeCode  nvarchar(10),
          @AreaType             nvarchar(10),
          @InsertNS             bit,
          @Pickface             int,
          @Quantity             numeric(13,6),
          @StoreLocationId      int,
          @RepLocationId        int,
          @RepStorageUnitBatchId int,
          @InstructionTypeId     int,
          @OutboundDocumentTypeCode nvarchar(10),
          @RepInstructionId      int,
          @IssueId               int,
          @WaveId                int,
          @ReserveBatch          bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @InsertNS = dbo.ufn_Configuration(207, @WarehouseId)
  
  select @Quantity            = i.Quantity,
         @ConfirmedQuantity   = i.ConfirmedQuantity,
         @RemainingQuantity   = i.Quantity - i.ConfirmedQuantity,
         @ConfirmedWeight     = i.Weight - i.ConfirmedWeight,
         @InstructionRefId    = isnull(i.InstructionRefId, i.InstructionId),
         @WarehouseId         = i.WarehouseId,
         @PickLocationId      = i.PickLocationId,
         @StoreLocationId     = i.StoreLocationId,
         @StorageUnitBatchId  = i.StorageUnitBatchId,
         @OldStorageUnitBatchId = i.StorageUnitBatchId,
         @InstructionTypeCode = it.InstructionTypeCode
    from Instruction      i
    join InstructionType it on i.InstructionTypeId = it.InstructionTypeId
   where i.InstructionId = @instructionId
  
  select @StorageUnitId = StorageUnitId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select @OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode,
         @ReserveBatch             = odt.ReserveBatch,
         @IssueId                  = ili.IssueId
    from IssueLineInstruction ili (nolock)
    join OutboundDocumentType odt (nolock) on ili.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where InstructionId = @InstructionRefId
  
  select @WaveId = isnull(os.WaveId, i.WaveId)
    from Issue                   i (nolock)
    left
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
    left
    join OutboundShipment       os (nolock) on osi.OutboundShipmentId = os.OutboundShipmentId
   where i.IssueId = @IssueId
  
  select @AreaType = a.AreaType
    from AreaLocation al (nolock)
    join Area          a (nolock) on al.AreaId = a.AreaId
   where al.LocationId = @PickLocationId
  
  if((select dbo.ufn_Configuration(67, @warehouseId)) = 0 and @RemainingQuantity > 0)
  begin
    if @InstructionTypeCode = 'R'
    begin
      set @Error = 0
    end
    else if @InstructionTypeCode = 'P'
    begin
      if @OutboundDocumentTypeCode = 'WAV'
      begin
        exec @Error = p_Wave_Planning_Release
         @WaveId     = @WaveId,
         @OperatorId = null,
         @Replenish  = 1
        
        -- Ignore Errors and don't insert another instruction
        set @Error = -1
      end
      else
      begin
        select top 1 @Error = 0,
                     @StorageUnitBatchId = sub.StorageUnitBatchId,
                     @PickLocationId     = subl.LocationId
          from Location                    l (nolock)
          join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
          join Area                        a (nolock) on al.AreaId               = a.AreaId
          join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join Batch                       b (nolock) on sub.BatchId             = b.BatchId
          join Status                      s (nolock) on b.StatusId              = s.StatusId
         where l.ActivePicking   = 1 -- Yes
           and sub.StorageUnitId    = @StorageUnitId
           and a.WarehouseId        = @WarehouseId
           and a.AreaCode          in ('RK','BK')
           and subl.ActualQuantity >= @RemainingQuantity
           and a.AreaType          in (isnull(@AreaType,''),'Backup')
           and s.StatusCode         = 'A'
        order by convert(nvarchar(10), b.ExpiryDate, 120),
                 Batch
      end
    end
    else
    begin
     --if (select dbo.ufn_Configuration(373, @warehouseId)) = 1 -- Production Pick - If short req. rep
     --begin
     --  select @OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode,
     --         @IssueId                  = ili.IssueId
     --    from IssueLineInstruction ili (nolock)
     --    join OutboundDocumentType odt (nolock) on ili.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     --   where InstructionId = @InstructionRefId
     --end
     
    if @OutboundDocumentTypeCode = 'KIT' and (select dbo.ufn_Configuration(373, @warehouseId)) = 1 -- Production Pick - If short req. rep
    begin
        if (select isnull(SUM(ActualQuantity - ReservedQuantity),0)
                        from viewAvailableArea
                       where AreaCode = 'POT'
                         and StorageUnitId = @StorageUnitId) < @Quantity
        begin
          exec @Error = p_Mobile_Replenishment_Request 
           @storeLocationId    = @PickLocationId,
           @storageUnitBatchId = @StorageUnitBatchId,
           @operatorId         = null,
           @ShowMsg            = 0,
           @ManualReq          = 0,
           @AnyLocation        = 1,
           @AreaType           = 'Backup',
           @RequestedQuantity  = @Quantity,
           @InstructionId      = @RepInstructionId output
          
          if @RepInstructionId is not null
          begin
            exec @Error = p_Replenishment_Create_Pick
             @InstructionId = @RepInstructionId
            
            if @Error <> 0
              goto Error
            
            set @Error = 0
          end
        end
    end
    else if (select dbo.ufn_Configuration(336, @warehouseId)) = 1 -- Request Replenishments on 0
    begin
      if @WaveId is not null
      begin
        exec p_Wave_Planning_Release
         @WaveId           = @WaveId,
         @OperatorId       = null,
         @Replenish        = 1,
         @RepStorageUnitId = @StorageUnitId,
         @Result           = @Error output
      end
      else
      begin
        exec @Error = p_Mobile_Request_Replenishment @InstructionId = @InstructionId, @ShowMsg = 0
        
        if @Error = 2 and dbo.ufn_Configuration(364, @WarehouseId) = 1 -- If invalid location, try get pickface
        begin
          select top 1 @Error    = 0,
                       @Pickface = sul.LocationId
            from StorageUnitLocation sul (nolock)
            join AreaLocation         al (nolock) on sul.LocationId = al.LocationId
            join Area                  a (nolock) on al.AreaId      = a.AreaId
           where a.AreaCode = 'PK'
          
          if @Pickface is not null
          begin
            set @PickLocationId = @Pickface
          end
        end
        else
        begin
          select top 1 @Error = 0,
                       @StorageUnitBatchId = sub.StorageUnitBatchId,
                       @PickLocationId     = subl.LocationId
            from Location                    l (nolock)
            join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
            join Area                        a (nolock) on al.AreaId               = a.AreaId
            join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
            join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
            join Batch                       b (nolock) on sub.BatchId             = b.BatchId
            join Status                      s (nolock) on b.StatusId              = s.StatusId
           where l.ActivePicking      = 1 -- Yes
             and sub.StorageUnitId    = @StorageUnitId
             and a.WarehouseId        = @WarehouseId
             and ((a.AreaCode          in ('RK','SP','PK'))
             or   (a.AreaCode          in (case when dbo.ufn_Configuration(179, @WarehouseId) = 1 -- Original Value = 1 for Pick part pallets from bulk
                                                then 'BK'
                                                else 'PK'
                                                end)))
             and subl.ActualQuantity >= 1
             and a.AreaType          in (isnull(@AreaType,''),'Backup')
             and s.StatusCode         = 'A'
             and case when @ReserveBatch = 1
                      then @OldStorageUnitBatchId
                      else subl.StorageUnitBatchId
                      end = subl.StorageUnitBatchId
          order by convert(nvarchar(10), b.ExpiryDate, 120),
                   Batch
        end
      end
    end
    else
    begin
      if @WaveId is not null
      begin
        exec p_Wave_Planning_Release
         @WaveId           = @WaveId,
         @OperatorId       = null,
         @Replenish        = 1,
         @RepStorageUnitId = @StorageUnitId,
         @Result           = @Error output
      end
      else
      begin
        exec @Error = p_Replenishment_Check
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @PickLocationId
        
        if @ReserveBatch = 1
        begin
          set @Error = 99
          
          select top 1 @Error = 0
            from Location                    l (nolock)
            join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
            join Area                        a (nolock) on al.AreaId               = a.AreaId
            join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
            join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
            join Batch                       b (nolock) on sub.BatchId             = b.BatchId
            join Status                      s (nolock) on b.StatusId              = s.StatusId
           where l.ActivePicking   = 1 -- Yes
             and sub.StorageUnitId    = @StorageUnitId
             and a.WarehouseId        = @WarehouseId
             and ((a.AreaCode          in ('RK','SP'))
             or   (a.AreaCode          in (case when dbo.ufn_Configuration(179, @WarehouseId) = 1 -- Original Value = 1 for Pick part pallets from bulk
                                                then 'BK'
                                                else 'PK'
                                                end)))
             and subl.ActualQuantity >= 1
             and a.AreaType          in (isnull(@AreaType,''),'Backup')
             and s.StatusCode         = 'A'
             and case when @ReserveBatch = 1
                      then @OldStorageUnitBatchId
                      else subl.StorageUnitBatchId
                      end = subl.StorageUnitBatchId
          order by convert(nvarchar(10), b.ExpiryDate, 120),
                   Batch
        end
      end
    end
    
    if @ConfirmedQuantity > 0
    if dbo.ufn_Configuration(399, @WarehouseId) = 1 -- Mixed pick - Keep inserting remainder unless picked 0
    begin
      set @Error = 0
    end
  end
    
    if @Error in (0,9) or @InsertNS = 1
    begin
      if @Error not in (0,9) and @InsertNS = 1
        select @StatusId = dbo.ufn_StatusId('I','NS')
      else
        select @StatusId = dbo.ufn_StatusId('I','S')
      
      -- Set the previous instruction to amount picked - remianing to follow..
      exec @Error = p_Instruction_Update
       @InstructionId    = @instructionId,
       @Quantity         = @ConfirmedQuantity,
       @PreviousQuantity = @Quantity -- Used for 
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Instruction_Update_ili
       @InstructionId     = @instructionId,
       @InstructionRefId  = @InstructionRefId,
       @insConfirmed      = @ConfirmedQuantity
      
      if @Error <> 0
        goto error
      
      -- Give the instruction a reference
      set @InstructionRefId = isnull(@InstructionRefId, @instructionId)
      
      insert Instruction
            (InstructionTypeId,
             StorageUnitBatchId,
             WarehouseId,
             StatusId,
             JobId,
             OperatorId,
             PickLocationId,
             StoreLocationId,
             ReceiptLineId,
             IssueLineId,
             InstructionRefId,
             Quantity,
             ConfirmedQuantity,
             Weight,
             ConfirmedWeight,
             PalletId,
             CreateDate,
             StartDate,
             EndDate,
             Picked,
             Stored,
             CheckQuantity,
             CheckWeight,
             OutboundShipmentId,
             DropSequence)
      select InstructionTypeId,
             @StorageUnitBatchId,
             WarehouseId,
             @StatusId,
             JobId,
             OperatorId,
             @PickLocationId,
             StoreLocationId,
             ReceiptLineId,
             IssueLineId,
             @InstructionRefId,
             @RemainingQuantity,
             @RemainingQuantity,
             @ConfirmedWeight,
             @ConfirmedWeight,
             PalletId,
             @GetDate,
             null,
             null,
             null,
             null,
             null,
             null,
             OutboundShipmentId,
             DropSequence
        from Instruction
       where InstructionId = @InstructionId
      
      select @Error = @@Error, @instructionId = scope_identity()
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Instruction_Update_ili
       @InstructionId     = @instructionId,
       @InstructionRefId  = @InstructionRefId,
       @insConfirmed      = @RemainingQuantity
      
      if @Error <> 0
        goto error
      
      if @Error in (0,9)
      begin
        exec @error = p_StorageUnitBatchLocation_Reserve
         @InstructionId       = @InstructionId
        
        if @error <> 0
          goto error
      end
    end
  end
  
  return
  
  error:
    raiserror 900000 'Error executing p_Instruction_Reference'
    return @Error
end

