﻿



 
/*
  /// <summary>
  ///   Procedure Name : p_WIP_Order_Search_Test
  ///   Filename       : p_WIP_Order_Search_Test.sql
  ///   Create By      : Kevin Wilson
  ///   Date Created   : 14 September 2018
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_WIP_Order_Search_Test]
(
	@WarehouseId            INT,
	@OutboundShipmentId     INT,
	@OutboundDocumentTypeId INT,
	@WaveId                 INT = NULL,
	@ExternalCompanyCode    NVARCHAR(30),
	@ExternalCompany        NVARCHAR(255),
	@OrderNumber            NVARCHAR(30),
	@FromDate               DATETIME,
	@ToDate                 DATETIME,
	@PrincipalId            INT = NULL
)

AS
BEGIN
	SET NOCOUNT ON;

	IF @OrderNumber = -1
		SET @OrderNumber = NULL

	IF @OutboundShipmentId = -1
		SET @OutboundShipmentId = NULL

	DECLARE	@lGetDate					DATETIME,
			@lWarehouseId				INT,
			@lOutboundShipmentId		INT,
			@lOutboundDocumentTypeId	INT,
			@lWaveId					INT,
			@lExternalCompanyCode		NVARCHAR(30),
			@lExternalCompany			NVARCHAR(255),
			@lOrderNumber				NVARCHAR(30),
			@lFromDate					DATETIME,
			@lToDate					DATETIME,
			@lPrincipalId				INT
	SET		@lGetDate					= dbo.ufn_Getdate()
	SET		@lWarehouseId				= @WarehouseId
	SET		@lOutboundShipmentId		= @OutboundShipmentId
	SET		@lOutboundDocumentTypeId	= COALESCE(@OutboundDocumentTypeId, -1)
	SET		@lWaveId					= COALESCE(@WaveId,-1)
	SET		@lExternalCompanyCode		= COALESCE(@ExternalCompanyCode, '')
	SET		@lExternalCompany			= COALESCE(@ExternalCompany, '')
	SET		@lOrderNumber				= COALESCE(@OrderNumber, '%')
	SET		@lFromDate					= @FromDate
	SET		@lToDate					= @ToDate
	SET		@lPrincipalId				= @PrincipalId

	CREATE TABLE #TableResult 
	(
		Wave NVARCHAR(50)
		,OutboundDocumentId INT
		,OutboundDocumentType NVARCHAR(30)
		,IssueId INT
		,OrderNumber NVARCHAR(30)
		,OutboundShipmentId INT
		,ExternalCompanyId INT
		,CustomerCode NVARCHAR(30)
		,Customer NVARCHAR(255)
		,RouteId INT
		,Route NVARCHAR(50)
		,NumberOfLines INT
		,ShortPicks INT
		,DeliveryDate DATETIME
		,CreateDate DATETIME
		,StatusId INT
		,STATUS NVARCHAR(50)
		,PriorityId INT
		,Priority NVARCHAR(50)
		,LocationId INT
		,Location NVARCHAR(15)
		,Rating INT
		,AvailabilityIndicator NVARCHAR(20)
		,Remarks NVARCHAR(255)
		,Total NUMERIC(13, 3)
		,Complete NUMERIC(13, 3)
		,PercentageComplete NUMERIC(13, 3)
		,Units INT
		,Releases INT
		,Weight FLOAT
		,Orderby INT
		,PrincipalId INT
		,PrincipalCode NVARCHAR(30)
		,CommentId INT
		,Comment NVARCHAR(max)
		,CheckingCount INT
		,PrintCollectionSlip NVARCHAR(30)
	);

	IF @OutboundShipmentId IS NULL
	BEGIN
		INSERT	#TableResult
				(Wave,
				OutboundShipmentId,
				OutboundDocumentId,
				IssueId,
				LocationId,
				Location,
				OrderNumber,
				ExternalCompanyId,
				CustomerCode,
				Customer,
				RouteId,
				Route,
				StatusId,
				Status,
				PriorityId,
				Priority,
				OrderBy,
				DeliveryDate,
				CreateDate,
				OutboundDocumentType,
				Rating,
				Remarks,
				NumberOfLines,
				ShortPicks,
				Total,
				Complete,
				PercentageComplete,
				Units,
				Releases,
				Weight,
				PrincipalId,
				PrincipalCode,
				CheckingCount,
				PrintCollectionSlip)
		SELECT	NULL,
				osi.OutboundShipmentId,
				od.OutboundDocumentId,
				i.IssueId,
				COALESCE(os.LocationId, i.LocationId),
				lc.Location,
				od.OrderNumber,
				od.ExternalCompanyId,
				ec.ExternalCompanyCode,
				ec.ExternalCompany,
				COALESCE(os.RouteId, i.RouteId),
				COALESCE(ort.Route, irt.Route),
				COALESCE(os.StatusId, i.StatusId),
				s.STATUS,
				i.PriorityId,
				pr.Priority,
				pr.OrderBy,
				COALESCE(os.ShipmentDate, i.DeliveryDate),
				od.CreateDate,
				odt.OutboundDocumentType,
				NULL,
				i.Remarks,
				i.NumberOfLines,
				i.ShortPicks,
				i.Total,
				i.Complete,
				CASE
					WHEN i.Total > 0 AND i.Complete > 0
					THEN (i.Complete / i.Total) * 100
					ELSE 0
				END AS PercentageComplete,
				i.Units,
				i.Releases,
				i.Weight,
				od.PrincipalId,
				prn.PrincipalCode,
				COALESCE(i.CheckingCount, 0),
				a.Area AS PrintCollectionSlip
		FROM	OutboundDocument od(NOLOCK)
		INNER JOIN	OutboundDocumentType odt(NOLOCK) ON od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
		INNER JOIN	Issue i(NOLOCK) ON od.OutboundDocumentId = i.OutboundDocumentId
		INNER JOIN	[Status] s(NOLOCK) ON i.StatusId = s.StatusId
		LEFT OUTER JOIN	Priority pr (NOLOCK) ON i.PriorityId = pr.PriorityId
		LEFT OUTER JOIN	Principal prn (NOLOCK) ON od.PrincipalId = prn.PrincipalId
		LEFT OUTER JOIN	OutboundShipmentIssue osi(NOLOCK) ON i.IssueId = osi.IssueId
		LEFT OUTER JOIN	OutboundShipment os(NOLOCK) ON osi.OutboundShipmentId = os.OutboundShipmentId
		LEFT OUTER JOIN Route ort (NOLOCK) ON os.RouteId = ort.RouteId
		LEFT OUTER JOIN Route irt (NOLOCK) ON i.RouteId = irt.RouteId
		LEFT OUTER JOIN	Wave w(NOLOCK) ON i.WaveId = w.WaveId
		LEFT OUTER JOIN	ExternalCompany ec (NOLOCK) ON od.ExternalCompanyId = ec.ExternalCompanyId
		LEFT OUTER JOIN Location lc (NOLOCK) ON COALESCE(os.LocationId,i.LocationId) = lc.LocationId
		OUTER APPLY
		(
			SELECT	TOP(1) a.Area 
			FROM	IssueLineInstruction ili (NOLOCK)
			INNER JOIN	Instruction ins (NOLOCK) ON ili.InstructionId = ins.InstructionId
			INNER JOIN	AreaLocation al (NOLOCK) ON ins.PickLocationId = al.LocationId
			INNER JOIN	Area a (NOLOCK) ON al.AreaId = a.AreaId
									and a.AutoCheck = 1
			WHERE	ili.IssueId = i.IssueId OR ili.IssueId = osi.IssueId
		
		) AS a
		WHERE	i.WarehouseId = @lWarehouseId
		AND		s.StatusCode IN ('M','RL','PC','PS','CK','A','WC','QA','S')
		AND		odt.OutboundDocumentTypeCode NOT IN ('REP','KIT')
		AND		od.OrderNumber LIKE @lOrderNumber
		AND		((i.DeliveryDate BETWEEN @lFromDate AND @lToDate) OR (od.CreateDate BETWEEN @lFromDate AND @lToDate))
		AND		COALESCE(i.WaveId, -1) = @lWaveId
	END
	ELSE
	BEGIN
		INSERT	#TableResult
				(Wave,
				OutboundShipmentId,
				OutboundDocumentId,
				IssueId,
				LocationId,
				Location,
				OrderNumber,
				CustomerCode,
				Customer,
				RouteId,
				Route,
				StatusId,
				Status,
				PriorityId,
				Priority,
				OrderBy,
				DeliveryDate,
				CreateDate,
				OutboundDocumentType,
				Rating,
				Remarks,
				NumberOfLines,
				ShortPicks,
				Total,
				Complete,
				PercentageComplete,
				Units,
				Releases,
				Weight,
				PrincipalId,
				PrincipalCode,
				CheckingCount,
				PrintCollectionSlip)
		SELECT	w.Wave,
				osi.OutboundShipmentId,
				od.OutboundDocumentId,
				i.IssueId,
				i.LocationId,
				lc.Location,
				od.OrderNumber,
				ec.ExternalCompanyCode,
				ec.ExternalCompany,
				i.RouteId,
				r.Route,
				i.StatusId,
				s.Status,
				i.PriorityId,
				pr.Priority,
				pr.OrderBy,
				i.DeliveryDate,
				od.CreateDate,
				odt.OutboundDocumentType,
				ec.Rating,
				i.Remarks,
				i.NumberOfLines,
				i.ShortPicks,
				i.Total,
				i.Complete,
				CASE
					WHEN i.Total > 0 AND i.Complete > 0
					THEN (i.Complete / i.Total) * 100
					ELSE 0
				END AS PercentageComplete,
				i.Units,
				i.Releases,
				i.Weight,
				od.PrincipalId,
				prn.PrincipalCode,
				COALESCE(i.CheckingCount,0),
				a.Area AS PrintCollectionSlip
		FROM	OutboundDocument od (NOLOCK)
		INNER JOIN	ExternalCompany ec (NOLOCK) ON od.ExternalCompanyId      = ec.ExternalCompanyId
		INNER JOIN	Issue i (NOLOCK) ON od.OutboundDocumentId = i.OutboundDocumentId
		INNER JOIN	Status s (NOLOCK) ON i.StatusId = s.StatusId
		INNER JOIN	OutboundDocumentType  odt (NOLOCK) ON od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
		INNER JOIN	OutboundShipmentIssue osi (NOLOCK) ON i.IssueId = osi.IssueId
		LEFT OUTER JOIN	Priority pr (NOLOCK) ON i.PriorityId = pr.PriorityId
		LEFT OUTER JOIN	Principal prn (NOLOCK) ON od.PrincipalId = prn.PrincipalId
		LEFT OUTER JOIN	Wave w (NOLOCK) ON i.WaveId = w.WaveId
		LEFT OUTER JOIN Route r (NOLOCK) ON i.RouteId = r.RouteId
		LEFT OUTER JOIN Location lc (NOLOCK) ON i.LocationId = lc.LocationId
		OUTER APPLY
		(
			SELECT	TOP(1) a.Area 
			FROM	IssueLineInstruction ili (NOLOCK)
			INNER JOIN	Instruction ins (NOLOCK) ON ili.InstructionId = ins.InstructionId
			INNER JOIN	AreaLocation al (NOLOCK) ON ins.PickLocationId = al.LocationId
			INNER JOIN	Area a (NOLOCK) ON al.AreaId = a.AreaId
									and a.AutoCheck = 1
			WHERE	ili.IssueId = i.IssueId OR ili.IssueId = osi.IssueId
		
		) AS a
		WHERE	i.WarehouseId = @lWarehouseId
		AND		s.Type = 'IS'
		AND		s.StatusCode IN (
					'M'
					,'RL'
					,'PC'
					,'PS'
					,'CK'
					,'A'
					,'WC'
					,'QA'
					,'S'
				)
		AND		osi.OutboundShipmentId = @lOutboundShipmentId
		AND		COALESCE(i.WaveId, -1) = @lWaveId
		AND		COALESCE(od.PrincipalId, -1) = COALESCE(@lPrincipalId, od.PrincipalId, -1)
	END

	DECLARE @lStandard INT,
			@lYellow INT,
			@lOrange INT,
			@lRed INT
	SET		@lStandard = dbo.ufn_Configuration_Value(47, @WarehouseId)
	SET		@lYellow = dbo.ufn_Configuration_Value(46, @WarehouseId)
	SET		@lOrange = dbo.ufn_Configuration_Value(45, @WarehouseId)
	SET		@lRed = dbo.ufn_Configuration_Value(44, @WarehouseId)
  
	UPDATE	#TableResult
	SET		AvailabilityIndicator = CASE
										WHEN @lRed > DateDiff(hh, @lGetDate, DeliveryDate)
										THEN 'Red'
										WHEN @lOrange > DateDiff(hh, @lGetDate, DeliveryDate)
										THEN 'Orange'
										WHEN @lYellow > DateDiff(hh, @lGetDate, DeliveryDate)
										THEN 'Yellow'
										WHEN @lStandard <= DateDiff(hh, @lGetDate, DeliveryDate)
										THEN 'Standard'
									END
  
	IF dbo.ufn_Configuration(286, @warehouseId) = 1 -- WIP - show only route
		SELECT Wave
			,MIN(IssueId) AS 'IssueId'
			,COALESCE(OutboundShipmentId, - 1) AS 'OutboundShipmentId'
			,CASE 
				WHEN OutboundShipmentId IS NULL
					THEN OrderNumber
				END AS 'OrderNumber'
			,CASE 
				WHEN OutboundShipmentId IS NULL
					THEN CustomerCode
				END AS 'CustomerCode'
			,CASE 
				WHEN OutboundShipmentId IS NULL
					THEN Customer
				END AS 'Customer'
			,COALESCE(RouteId, - 1) AS 'RouteId'
			,Route
			,SUM(NumberOfLines) AS 'NumberOfLines'
			,SUM(ShortPicks) AS 'ShortPicks'
			,SUM(ShortPicks) AS UnitShort
			,MIN(DeliveryDate) AS 'DeliveryDate'
			,MIN(CreateDate) AS 'CreateDate'
			,STATUS
			,PriorityId
			,Priority
			,OutboundDocumentType
			,COALESCE(LocationId, - 1) AS 'LocationId'
			,MAX(Location) AS 'Location'
			,Rating
			,AvailabilityIndicator
			,MAX(Remarks) AS 'Remarks'
			,MAX(Comment) AS 'Comment'
			,SUM(Complete) AS 'Complete'
			,SUM(Total) AS 'Total'
			,SUM(CONVERT(INT, ROUND(PercentageComplete, 0))) / COUNT(1) AS 'PercentageComplete'
			,SUM(Units) AS 'Units'
			,SUM(Releases) AS 'Releases'
			,SUM(Weight) AS 'Weight'
			,PrincipalCode
			,MAX(CheckingCount)
		FROM #TableResult
		GROUP BY Wave
			,COALESCE(OutboundShipmentId, - 1)
			,CASE 
				WHEN OutboundShipmentId IS NULL
					THEN OrderNumber
				END
			,CASE 
				WHEN OutboundShipmentId IS NULL
					THEN CustomerCode
				END
			,CASE 
				WHEN OutboundShipmentId IS NULL
					THEN Customer
				END
			,COALESCE(RouteId, - 1)
			,Route
			,STATUS
			,PriorityId
			,Priority
			,OutboundDocumentType
			,COALESCE(LocationId, - 1)
			,Location
			,Rating
			,AvailabilityIndicator
			,PrincipalCode
		ORDER BY Location
			,OutboundShipmentId
			,OrderNumber
	ELSE
		SELECT Wave
			,IssueId
			,COALESCE(OutboundShipmentId, - 1) AS 'OutboundShipmentId'
			,OrderNumber
			,CustomerCode
			,Customer
			,COALESCE(RouteId, - 1) AS 'RouteId'
			,Route
			,NumberOfLines
			,ShortPicks
			,ShortPicks AS UnitShort
			,DeliveryDate
			,CreateDate
			,STATUS
			,PriorityId
			,Priority
			,OutboundDocumentType
			,COALESCE(LocationId, - 1) AS 'LocationId'
			,Location
			,Rating
			,AvailabilityIndicator
			,Remarks
			,Comment
			,Complete
			,Total
			,CONVERT(INT, ROUND(PercentageComplete, 0)) AS 'PercentageComplete'
			,Units
			,Releases
			,Weight
			,PrincipalCode
			,CheckingCount
			,PrintCollectionSlip
		FROM #TableResult
		ORDER BY Location
			,OrderBy
			,OutboundShipmentId
			,OrderNumber
	END
