﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Move_Items
  ///   Filename       : p_Container_Move_Items.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Move_Items
(
 @warehouseId       int,
 @operatorId        int,
 @containerHeaderId int,
 @storeLocation     nvarchar(50),
 @barcode           nvarchar(50) = null,
 @batch             nvarchar(50) = null,
 @quantity          float = null
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           varchar(500) = 'Error executing p_Container_Move_Items',
          @GetDate            datetime,
          @rowcount           int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @StorageUnitId      int,
          @ContainerDetailId  int,
          @StorageUnitBatchId int,
          @PalletId           int,
          @JobId              int,
          @InstructionId      int,
          @InstructionTypeId  int,
          @PriorityId         int,
          @StatusId           int,
          @PandD              bit = 0,
          @PandDJobId         int,
          @PanDInstructionId  int,
          @InstructionTypeCode nvarchar(10)
  
  declare @TableResult as table
  (
   ContainerDetailId  int,
   StorageUnitBatchId int,
   Quantity           float,
   PalletId           int
  )
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @batch = ''
    set @batch = null
    
  if @quantity = 0
	set @quantity = null
  
  select @PickLocationId = isnull(StoreLocationId, PickLocationId),
         @JobId          = JobId
    from ContainerHeader (nolock)
   where ContainerHeaderId = @containerHeaderId
  
  select @StoreLocationId = l.LocationId
    from Area a
    join AreaLocation al on a.AreaId      = al.AreaId
    join Location      l on al.LocationId = l.LocationId
   where a.WarehouseId = @warehouseId
     and l.Location    = replace(@storeLocation,'L:','')
  
  if @barcode is not null
    select @StorageUnitId = su.StorageUnitId
      from StorageUnit su (nolock)
      join Product      p (nolock) on su.ProductId = p.ProductId
                                  and (p.ProductCode = @Barcode
                                   or  p.Barcode     = @Barcode)
      left
      join Pack        pk (nolock) on su.StorageUnitId = pk.StorageUnitId
                                  and pk.WarehouseId = @WarehouseId
                                  and pk.Barcode    = @Barcode
  
  begin transaction
  
  if @StoreLocationId is null or (@StoreLocationId = @PickLocationId)
  begin
    select @StoreLocationId as '@StoreLocationId', @PickLocationId as '@PickLocationId'
    set @Errormsg = 'Location Error'
    set @Error = -1
    goto error
  end
  
  update i
     set StoreLocationId = @StoreLocationId
    from Instruction i
    join InstructionType it on i.InstructionTypeId = it.InstructionTypeId
                           and it.InstructionTypeCode in ('P','PM','FM','PS')
   where i.JobId = @JobId
   
   -- Reset the jobId 
   set @JobId = null;
  
  --if @quantity is null
  --  update subl
  --     set ActualQuantity = ActualQuantity - cd.Quantity
  --    from ContainerDetail cd
  --    join StorageUnitBatchLocation subl on cd.StorageUnitBatchId = subl.StorageUnitBatchId
  --                                      and subl.LocationId       = @PickLocationId
  --     and cd.ContainerHeaderId = @containerHeaderId
  --else
  --begin
  --  update subl
  --     set ActualQuantity = ActualQuantity - @quantity
  --    from ContainerDetail   cd (nolock)
  --    join StorageUnitBatchLocation subl on cd.StorageUnitBatchId = subl.StorageUnitBatchId
  --                                      and subl.LocationId       = @PickLocationId
  --    join StorageUnitBatch sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  --                                      and sub.StorageUnitId       = @StorageUnitId
  --     and cd.ContainerHeaderId = @containerHeaderId
  --end
  --select @Error = @@error, @rowcount = @@rowcount
  ----select @rowcount as '@rowcount', @StoreLocationId as '@StoreLocationId', @PickLocationId as '@PickLocationId', @StorageUnitId as '@StorageUnitId', @quantity as '@quantity'
  --if @Error <> 0
  --  goto error
  
  --delete StorageUnitBatchLocation where ActualQuantity = 0 and AllocatedQuantity = 0 and ReservedQuantity = 0 and LocationId = @PickLocationId
  
  --select @Error = @@error, @rowcount = @@rowcount
  
  --if @Error <> 0
  --  goto error
  
  --if @quantity is null
  --  update subl
  --     set ActualQuantity = ActualQuantity + cd.Quantity
  --    from ContainerDetail cd
  --    join StorageUnitBatchLocation subl on cd.StorageUnitBatchId = subl.StorageUnitBatchId
  --                                      and subl.LocationId       = @StoreLocationId
  --else
  --  update subl
  --     set ActualQuantity = ActualQuantity + @quantity
  --    from ContainerDetail cd
  --    join StorageUnitBatchLocation subl on cd.StorageUnitBatchId = subl.StorageUnitBatchId
  --                                      and subl.LocationId       = @StoreLocationId
  --    join StorageUnitBatch sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  --                                      and sub.StorageUnitId       = @StorageUnitId
  --     and cd.ContainerHeaderId = @containerHeaderId
  
  --select @Error = @@error, @rowcount = @@rowcount
  
  --if @Error <> 0
  --  goto error
  
  --if @quantity is null
  --  insert StorageUnitBatchLocation
  --        (StorageUnitBatchId,
  --         LocationId,
  --         ActualQuantity)
  --  select cd.StorageUnitBatchId,
  --         @StoreLocationId,
  --         cd.Quantity
  --    from ContainerDetail cd
  --    left
  --    join StorageUnitBatchLocation subl on cd.StorageUnitBatchId = subl.StorageUnitBatchId
  --                                      and subl.LocationId       = @StoreLocationId
  --   where subl.LocationId is null
  --     and cd.ContainerHeaderId = @containerHeaderId
  --else
  --  insert StorageUnitBatchLocation
  --        (StorageUnitBatchId,
  --         LocationId,
  --         ActualQuantity)
  --  select cd.StorageUnitBatchId,
  --         @StoreLocationId,
  --         @quantity
  --    from ContainerDetail cd
  --    left
  --    join StorageUnitBatchLocation subl on cd.StorageUnitBatchId = subl.StorageUnitBatchId
  --                                      and subl.LocationId       = @StoreLocationId
  --    left
  --    join StorageUnitBatch sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  --                                      and sub.StorageUnitId       = @StorageUnitId
  --   where subl.LocationId is null
  --     and cd.ContainerHeaderId = @containerHeaderId
  
  --select @Error = @@error, @rowcount = @@rowcount
  
  --if @Error <> 0
  --  goto error
  
  if @quantity is null
    exec @Error = p_ContainerHeader_Update
     @containerHeaderId = @containerHeaderId,
     @PickLocationId    = @PickLocationId,
     @StoreLocationId   = @StoreLocationId
  else
  begin
    select @StorageUnitBatchId = sub.StorageUnitBatchId
      from ContainerDetail   cd (nolock)
      join StorageUnitBatch sub (nolock) on cd.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      join Product            p (nolock) on su.ProductId = p.ProductId
      join SKU              sku (nolock) on su.SKUId = sku.SKUId
      join Batch              b (nolock) on sub.BatchId = b.BatchId
     where cd.ContainerHeaderId = @containerHeaderId
      and su.StorageUnitId      = isnull(@StorageUnitId, su.StorageUnitId)
      and b.Batch               = isnull(@batch, b.Batch)
    
    if @StorageUnitBatchId is null
    begin
      select @StoreLocationId as '@StoreLocationId', @PickLocationId as '@PickLocationId'
      set @Errormsg = 'Batch Error'
      set @Error = -2
      goto error
    end
    
    update ContainerDetail
       set Quantity = Quantity - @quantity
     where ContainerHeaderId = @containerHeaderId
       and StorageUnitBatchId = @StorageUnitBatchId
  end
  
  if @Error <> 0
    goto error
  
  insert @TableResult
        (ContainerDetailId,
         StorageUnitBatchId,
         Quantity,
         PalletId)
  select cd.ContainerDetailId,
         cd.StorageUnitBatchId,
         isnull(@quantity, Quantity),
         ch.PalletId
    from ContainerDetail cd (nolock)
    join ContainerHeader ch (nolock) on cd.ContainerHeaderId = ch.ContainerHeaderId
   where cd.ContainerHeaderId = @containerHeaderId
     and cd.StorageUnitBatchId  = isnull(@StorageUnitBatchId, cd.StorageUnitBatchId)
     and isnull(@quantity, Quantity) is not null
     and isnull(@quantity, Quantity) > 0
  
  set @rowcount = @@rowcount
  
  if @rowcount > 0
  begin
    select @InstructionTypeId = InstructionTypeId,
           @PriorityId        = PriorityId
      from InstructionType (nolock)
     where InstructionTypeCode = 'M'
    
    set @StatusId = dbo.ufn_StatusId('I','W')
    
    exec @Error = p_Job_Insert
     @JobId       = @JobId output,
     @PriorityId  = @PriorityId,
     @OperatorId  = @OperatorId,
     @StatusId    = @StatusId,
     @WarehouseId = @WarehouseId
    
    if @Error <> 0 or @JobId is null
      goto error
    
    if @PandD = 1
    begin
      exec @Error = p_Job_Insert
       @JobId       = @PandDJobId output,
       @PriorityId  = @PriorityId,
       @OperatorId  = null,
       @StatusId    = @StatusId,
       @WarehouseId = @WarehouseId
      
      if @Error <> 0 or @PandDJobId is null
        goto error
    end
    
    while @rowcount > 0
    begin
      set @rowcount = @rowcount - 1
      
      select @StorageUnitBatchId = StorageUnitBatchId,
             @quantity           = Quantity,
             @ContainerDetailId  = ContainerDetailId,
             @PalletId           = PalletId
        from @TableResult
      
      delete @TableResult where ContainerDetailId = @ContainerDetailId
      
      exec @Error = p_Instruction_Insert
       @InstructionId       = @InstructionId output,
       @InstructionTypeId   = @InstructionTypeId,
       @StorageUnitBatchId  = @StorageUnitBatchId,
       @WarehouseId         = @WarehouseId,
       @StatusId            = @StatusId,
       @JobId               = @JobId,
       @OperatorId          = @OperatorId,
       @PickLocationId      = @PickLocationId,
       @StoreLocationId     = @StoreLocationId,
       @InstructionRefId    = null,
       @Quantity            = @Quantity,
       @ConfirmedQuantity   = @Quantity,
       @CreateDate          = @GetDate,
       @PalletId            = @PalletId
      
      if @Error <> 0 or @InstructionId is null
      begin
        set @InstructionId = -1
        goto error
      end
      
      set @PanDInstructionId = null
      
      if @PandD = 1
      begin
        exec @Error = p_Instruction_Insert
         @InstructionId       = @PanDInstructionId output,
         @InstructionTypeId   = @InstructionTypeId,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @WarehouseId         = @WarehouseId,
         @StatusId            = @StatusId,
         @JobId               = @PandDJobId,
         @OperatorId          = null,
         @PickLocationId      = @StoreLocationId,
         @StoreLocationId     = null,
         @InstructionRefId    = null,
         @Quantity            = @Quantity,
         @ConfirmedQuantity   = @Quantity,
         @CreateDate          = @GetDate,
         @PalletId            = @PalletId
        
        if @Error <> 0 or @PanDInstructionId is null
        begin
          set @InstructionId = -1
          goto error
        end
      end
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId = @InstructionId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Instruction_Started
       @InstructionId = @InstructionId,
       @operatorId    = @operatorId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_StorageUnitBatchLocation_Allocate
       @InstructionId       = @instructionId,
       @Confirmed           = 1
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Instruction_Finished
       @InstructionId = @InstructionId,
       @operatorId    = @operatorId
      
      if @Error <> 0
        goto error
      
      if @PanDInstructionId is not null
      begin
        exec @Error = p_StorageUnitBatchLocation_Reserve
         @InstructionId = @PanDInstructionId
        
        if @Error <> 0
          goto error
        
        exec @Error = p_Instruction_Started
         @InstructionId = @PanDInstructionId,
         @operatorId    = null
        
        if @Error <> 0
          goto error
      end
    end
  end
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
 
