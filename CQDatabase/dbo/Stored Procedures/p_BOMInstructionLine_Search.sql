﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstructionLine_Search
  ///   Filename       : p_BOMInstructionLine_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:09
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the BOMInstructionLine table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   BOMInstructionLine.BOMInstructionId,
  ///   BOMInstructionLine.BOMLineId,
  ///   BOMInstructionLine.LineNumber,
  ///   BOMInstructionLine.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMInstructionLine_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         BOMInstructionLine.BOMInstructionId
        ,BOMInstructionLine.BOMLineId
        ,BOMInstructionLine.LineNumber
        ,BOMInstructionLine.Quantity
    from BOMInstructionLine
  
end
