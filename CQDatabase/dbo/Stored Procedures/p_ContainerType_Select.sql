﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerType_Select
  ///   Filename       : p_ContainerType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:31
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ContainerType table.
  /// </remarks>
  /// <param>
  ///   @ContainerTypeId int = null 
  /// </param>
  /// <returns>
  ///   ContainerType.ContainerTypeId,
  ///   ContainerType.ContainerType,
  ///   ContainerType.ContainerTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerType_Select
(
 @ContainerTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ContainerType.ContainerTypeId
        ,ContainerType.ContainerType
        ,ContainerType.ContainerTypeCode
    from ContainerType
   where isnull(ContainerType.ContainerTypeId,'0')  = isnull(@ContainerTypeId, isnull(ContainerType.ContainerTypeId,'0'))
  
end
