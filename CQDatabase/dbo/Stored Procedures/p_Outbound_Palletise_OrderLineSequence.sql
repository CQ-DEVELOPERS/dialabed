﻿
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_OrderLineSequence
  ///   Filename       : p_Outbound_Palletise_OrderLineSequence.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Outbound_Palletise_OrderLineSequence
(
 @WarehouseId        int,
 @StoreLocationId    int,
 @OutboundShipmentId int,
 @IssueId            int,
 @OperatorId         int,
 @Weight             float,
 @Volume             float
)
AS
 
declare @TableItems as table
(
 LineNumber         int,
 DropSequence       int,
 AreaSequence       int,
 IssueLineId        int,
 StorageUnitBatchId int,
 Quantity           float,
 PalletQuantity     float
)

begin
  begin transaction tran_mix_pallet
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @DropSequence       int,
          @OldDropSequence    int,
          @DropSequenceCount  int,
          @AreaSequence       int,
          @OldAreaSequence    int,
          @JobId              int,
          @ItemQuantity       float,
          @ItemWeight         float,
          @StatusId           int,
          @PriorityId         int,
          @IssueLineId        int,
          @StorageUnitBatchId int,
          @GetDate            datetime,
          @LineNumber         int,
          @PalletQuantity     float,
          @InstructionTypeCode nvarchar(10),
          @Percentage         float,
          @RunPercentage      float,
          @Required           float
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @PriorityId = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'PM'
  
  /*******************************/
  /* Create a new job card for   */
  /* the customer                */
  /*******************************/
  select @StatusId = dbo.ufn_StatusId('IS','P')

  exec @Error = p_Job_Insert
   @JobId           = @JobId output,
   @PriorityId      = @PriorityId,
   @OperatorId      = @OperatorId,
   @StatusId        = @StatusId,
   @WarehouseId     = @WarehouseId

  if @Error <> 0
    goto error
  
  /**************************************************/
  /* Add the customers stock items to the pallet    */
  /**************************************************/
  INSERT @TableItems
  select LineNumber,
         DropSequence,
         AreaSequence,
         IssueLineId,
         StorageUnitBatchId,
         OriginalQuantity,
         PalletQuantity
    from PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
     and Quantity           > 0
  order by DropSequence, LineNumber, PalletQuantity, Quantity desc
  
  set @RunPercentage = 0
  
  while exists(select top 1 1 from @TableItems)
  begin
    select top 1
           @LineNumber         = LineNumber,
           @DropSequence       = DropSequence,
           @AreaSequence       = AreaSequence,
           @IssueLineId        = IssueLineId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @ItemQuantity       = Quantity,
           @PalletQuantity     = PalletQuantity
      from @TableItems
     where Quantity > 0
    
    if @OldDropSequence != @DropSequence
    begin
      set @RunPercentage = 0
      
      /*******************************/
      /* Create a new job card for   */
      /* the customer                */
      /*******************************/
      select @StatusId = dbo.ufn_StatusId('IS','P')
      
      exec @Error = p_Job_Insert
       @JobId           = @JobId output,
       @PriorityId      = @PriorityId,
       @OperatorId      = @OperatorId,
       @StatusId        = @StatusId,
       @WarehouseId     = @WarehouseId
      
      if @Error <> 0
        goto error
    end
    
    select @OldAreaSequence = @AreaSequence,
           @OldDropSequence = @DropSequence
    
    --select @ItemQuantity as '@ItemQuantity'
    
    set @Percentage = @ItemQuantity / (@PalletQuantity / 100)
    
    if (@RunPercentage + @Percentage) >= 100
    begin
      select @Required = floor(100 - @RunPercentage)
      
      while @Required < @Percentage and @ItemQuantity > 1
      begin
        set @ItemQuantity = @ItemQuantity - 1
        
        set @Percentage = floor(@ItemQuantity / (@PalletQuantity / 100))
        
        --if @Required >= @Percentage
        --  select floor(@Required) as '@Required', @ItemQuantity as '@ItemQuantity', @Percentage as '@Percentage', floor(@ItemQuantity / (@PalletQuantity / 100)) as 'Floor', @Percentage 
        
        if @ItemQuantity <= 0
          goto JobInsert
      end
    end
    
    set @RunPercentage = @RunPercentage + @Percentage
    
    set @InstructionTypeCode = 'PM'
    
    if @Percentage = 100
    if @ItemQuantity >= @PalletQuantity
    begin
      set @RunPercentage = 100
      set @InstructionTypeCode = 'PM' -- 'P'
      set @ItemQuantity = @PalletQuantity
    end
    
    --select @ItemQuantity as '@ItemQuantity', @PalletQuantity as '@PalletQuantity', @Percentage as '@Percentage', @RunPercentage
    
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = @InstructionTypeCode,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = null,
     @StoreLocationId     = @StoreLocationId,
     @Quantity            = @ItemQuantity,
     @Weight              = @ItemWeight,
     @IssueLineId         = @IssueLineId,
     @JobId               = @JobId,
     @OutboundShipmentId  = @OutboundShipmentId,
     @DropSequence        = @DropSequence
    
    if @Error <> 0  
      goto error
    
    if @RunPercentage >= 95
    begin
      JobInsert:
      set @RunPercentage = 0
      
      /*******************************/
      /* Create a new job card for   */
      /* the customer                */
      /*******************************/
      select @StatusId = dbo.ufn_StatusId('IS','P')
      
      exec @Error = p_Job_Insert
       @JobId           = @JobId output,
       @PriorityId      = @PriorityId,
       @OperatorId      = @OperatorId,
       @StatusId        = @StatusId,
       @WarehouseId     = @WarehouseId
      
      if @Error <> 0
        goto error
    end
    
    update PalletiseStock
       set Quantity = p.Quantity - @ItemQuantity
      from PalletiseStock    p
     where p.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and p.IssueId            = isnull(@IssueId, -1)
       and p.LineNumber         = @LineNumber
       and p.DropSequence       = @DropSequence
       and p.AreaSequence       = @AreaSequence
    
    update @TableItems
       set Quantity = Quantity - @ItemQuantity
     where isnull(IssueLineId, -1) = isnull(@IssueLineId, -1)
       and StorageUnitBatchId      = @StorageUnitBatchId
       and LineNumber              = @LineNumber
       and DropSequence            = @DropSequence
       and AreaSequence            = @AreaSequence
    
    delete @TableItems
     where isnull(IssueLineId, -1) = isnull(@IssueLineId, -1)
       and Quantity               <= 0
  end
  
  delete @TableItems
  
  delete PalletiseStock
   where DropSequence       = @DropSequence
     and AreaSequence       = @AreaSequence
     and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
  
  --select JobId, InstructionId, InstructionType, ProductCode, Product, Quantity, PalletQuantity, convert(float,Quantity) / (convert(float,PalletQuantity)/100)  from viewins where InstructionId > 7774 order by JobId
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_pseudo_palletise2'
    rollback transaction
    return @Error
end
