﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Code128_Insert
  ///   Filename       : p_Code128_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:27
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Code128 table.
  /// </remarks>
  /// <param>
  ///   @Code128 nvarchar(100) = null,
  ///   @Char nvarchar(100) = null,
  ///   @Variant nvarchar(100) = null,
  ///   @AsciiNumber nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   Code128.Code128,
  ///   Code128.Char,
  ///   Code128.Variant,
  ///   Code128.AsciiNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Code128_Insert
(
 @Code128 nvarchar(100) = null,
 @Char nvarchar(100) = null,
 @Variant nvarchar(100) = null,
 @AsciiNumber nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @Code128 = '-1'
    set @Code128 = null;
  
	 declare @Error int
 
  insert Code128
        (Code128,
         Char,
         Variant,
         AsciiNumber)
  select @Code128,
         @Char,
         @Variant,
         @AsciiNumber 
  
  select @Error = @@Error
  
  
  return @Error
  
end
