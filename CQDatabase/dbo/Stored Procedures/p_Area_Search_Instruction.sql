﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Search_Instruction
  ///   Filename       : p_Area_Search_Instruction.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Search_Instruction
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
  declare @StorageUnitId       int,
          @WarehouseId         int,
          @InstructionTypeCode nvarchar(10)
  
  select @StorageUnitId = sub.StorageUnitId,
         @WarehouseId   = i.WarehouseId,
         @InstructionTypeCode = it.InstructionTypeCode
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.InstructionId = @InstructionId
  
  if @InstructionTypeCode = 'O'
  begin
    select a.AreaId,
           a.Area
      from Area                  a (nolock)
      join AreaLocation         al (nolock) on al.AreaId = a.AreaId
     where a.WarehouseId     = @WarehouseId
       and a.AreaCode = 'JJ'
  end
  else
  begin
    select a.AreaId,
           a.Area
      from StorageUnitArea sua (nolock)
      join Area              a (nolock) on sua.AreaId = a.AreaId
     where sua.StorageUnitId = @StorageUnitId
       and a.WarehouseId     = @WarehouseId
    union
    select a.AreaId,
           a.Area
      from StorageUnitLocation sul (nolock)
      join AreaLocation         al (nolock) on sul.LocationId = al.LocationId
      join Area                  a (nolock) on al.AreaId = a.AreaId
     where sul.StorageUnitId = @StorageUnitId
       and a.WarehouseId     = @WarehouseId
  end
end
