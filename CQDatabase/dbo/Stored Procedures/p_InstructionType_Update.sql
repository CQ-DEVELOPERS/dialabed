﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_Update
  ///   Filename       : p_InstructionType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:53
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null,
  ///   @PriorityId int = null,
  ///   @InstructionType nvarchar(60) = null,
  ///   @InstructionTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_Update
(
 @InstructionTypeId int = null,
 @PriorityId int = null,
 @InstructionType nvarchar(60) = null,
 @InstructionTypeCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @InstructionType = '-1'
    set @InstructionType = null;
  
  if @InstructionTypeCode = '-1'
    set @InstructionTypeCode = null;
  
	 declare @Error int
 
  update InstructionType
     set PriorityId = isnull(@PriorityId, PriorityId),
         InstructionType = isnull(@InstructionType, InstructionType),
         InstructionTypeCode = isnull(@InstructionTypeCode, InstructionTypeCode) 
   where InstructionTypeId = @InstructionTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
