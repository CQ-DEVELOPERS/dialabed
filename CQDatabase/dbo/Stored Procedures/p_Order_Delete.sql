﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Order_Delete
  ///   Filename       : p_Order_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Order_Delete
(
 @OrderNumber nvarchar(30),
 @Interface   bit = 0
)
 
as
begin
         set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @OutboundDocumentId int,
          @InboundDocumentId  int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  select @OutboundDocumentId = OutboundDocumentId
    from OutboundDocument
   where OrderNumber = @OrderNumber
  
  select @InboundDocumentId = id.InboundDocumentId
    from InboundDocument         id (nolock)
    join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    left
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId          = isr.ReceiptId
    join Status                   s (nolock) on r.StatusId           = s.StatusId
   where OrderNumber = @OrderNumber
     and s.StatusCode = 'W'
     and isr.InboundShipmentId is null
  
  begin transaction
  
  if @OutboundDocumentId is not null
  begin
    --delete IssueLineInstruction
    -- where OutboundDocumentId = @OutboundDocumentId
    
    --if @@Error <> 0
    --  goto error
    
    delete il
      from IssueLine il
      join Issue      i on il.IssueId = i.IssueId
     where i.OutboundDocumentId = @OutboundDocumentId
    
    if @@Error <> 0
      goto error
    
    --delete osi
    --  from OutboundShipmentIssue osi
    --  join Issue                   i on osi.IssueId = i.IssueId
    -- where i.OutboundDocumentId = @OutboundDocumentId
    
    --if @@Error <> 0
    --  goto error
    
    delete Issue
     where OutboundDocumentId = @OutboundDocumentId
    
    if @@Error <> 0
      goto error
    
    delete OutboundLine
     where OutboundDocumentId = @OutboundDocumentId
    
    if @@Error <> 0
      goto error
    
    delete OutboundDocument
     where OutboundDocumentId = @OutboundDocumentId
    
    if @@Error <> 0
      goto error
  end
  
  if @InboundDocumentId is not null
  begin
    delete il
      from ReceiptLine il
      join Receipt      i on il.ReceiptId = i.ReceiptId
     where i.InboundDocumentId = @InboundDocumentId
    
    if @@Error <> 0
      goto error
    
    delete osi
      from InboundShipmentReceipt osi
      join Receipt                  i on osi.ReceiptId = i.ReceiptId
     where i.InboundDocumentId = @InboundDocumentId
    
    if @@Error <> 0
      goto error
    
    delete Receipt
     where InboundDocumentId = @InboundDocumentId
    
    if @@Error <> 0
      goto error
    
    delete InboundLine
     where InboundDocumentId = @InboundDocumentId
    
    if @@Error <> 0
      goto error
    
    delete InboundDocument
     where InboundDocumentId = @InboundDocumentId
    
    if @@Error <> 0
      goto error
  end
  
--  if @Interface = 0
--  begin
--    update InterfaceImportSOHeader
--       set RecordStatus = 'N',
--           ProcessedDate = null
--     where OrderNumber = @OrderNumber
--    
--    update InterfaceImportPOHeader
--       set RecordStatus = 'N',
--           ProcessedDate = null
--     where OrderNumber = @OrderNumber
--  end
--  
--  if @Interface = 1
--  begin
--    delete d
--      from InterfaceImportSOHeader h
--      join InterfaceImportSODetail d on h.PrimaryKey = d.ForeignKey
--     where h.OrderNumber = @OrderNumber
--    
--    if @@Error <> 0
--      goto error
--    
--    delete InterfaceImportSOHeader
--     where OrderNumber = @OrderNumber
--    
--    if @@Error <> 0
--      goto error
--    
--    delete d
--      from InterfaceExportSOHeader h
--      join InterfaceExportSODetail d on h.PrimaryKey = d.ForeignKey
--     where h.OrderNumber = @OrderNumber
--    
--    if @@Error <> 0
--      goto error
--    
--    delete InterfaceExportSOHeader
--     where OrderNumber = @OrderNumber
--    
--    if @@Error <> 0
--      goto error
--    
--    delete d
--      from InterfaceImportPOHeader h
--      join InterfaceImportPODetail d on h.PrimaryKey = d.ForeignKey
--     where h.OrderNumber = @OrderNumber
--    
--    if @@Error <> 0
--      goto error
--    
--    delete InterfaceImportPOHeader
--     where OrderNumber = @OrderNumber
--    
--    if @@Error <> 0
--      goto error
--    
--    delete d
--      from InterfaceExportPOHeader h
--      join InterfaceExportPODetail d on h.PrimaryKey = d.ForeignKey
--     where h.OrderNumber = @OrderNumber
--    
--    if @@Error <> 0
--      goto error
--    
--    delete InterfaceExportPOHeader
--     where OrderNumber = @OrderNumber
--    
--    if @@Error <> 0
--      goto error
--  end
  
  commit transaction
  return
  
  error:
    set @Error = -1
    raiserror 900000 'Error executing p_Order_Delete'
    rollback transaction
    return @Error
end


