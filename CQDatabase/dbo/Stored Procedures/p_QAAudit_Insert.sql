﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QAAudit_Insert
  ///   Filename       : p_QAAudit_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:05
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the QAAudit table.
  /// </remarks>
  /// <param>
  ///   @QAAuditId int = null output,
  ///   @JobId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @OrderedQuantity float = null,
  ///   @PickedQuantity float = null,
  ///   @CheckedQuantity float = null,
  ///   @WHQuantity float = null,
  ///   @PickedBy int = null,
  ///   @PickedDate datetime = null,
  ///   @CheckedBy int = null,
  ///   @CheckedDate datetime = null 
  /// </param>
  /// <returns>
  ///   QAAudit.QAAuditId,
  ///   QAAudit.JobId,
  ///   QAAudit.StorageUnitBatchId,
  ///   QAAudit.OrderedQuantity,
  ///   QAAudit.PickedQuantity,
  ///   QAAudit.CheckedQuantity,
  ///   QAAudit.WHQuantity,
  ///   QAAudit.PickedBy,
  ///   QAAudit.PickedDate,
  ///   QAAudit.CheckedBy,
  ///   QAAudit.CheckedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QAAudit_Insert
(
 @QAAuditId int = null output,
 @JobId int = null,
 @StorageUnitBatchId int = null,
 @OrderedQuantity float = null,
 @PickedQuantity float = null,
 @CheckedQuantity float = null,
 @WHQuantity float = null,
 @PickedBy int = null,
 @PickedDate datetime = null,
 @CheckedBy int = null,
 @CheckedDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @QAAuditId = '-1'
    set @QAAuditId = null;
  
	 declare @Error int
 
  insert QAAudit
        (JobId,
         StorageUnitBatchId,
         OrderedQuantity,
         PickedQuantity,
         CheckedQuantity,
         WHQuantity,
         PickedBy,
         PickedDate,
         CheckedBy,
         CheckedDate)
  select @JobId,
         @StorageUnitBatchId,
         @OrderedQuantity,
         @PickedQuantity,
         @CheckedQuantity,
         @WHQuantity,
         @PickedBy,
         @PickedDate,
         @CheckedBy,
         @CheckedDate 
  
  select @Error = @@Error, @QAAuditId = scope_identity()
  
  
  return @Error
  
end
