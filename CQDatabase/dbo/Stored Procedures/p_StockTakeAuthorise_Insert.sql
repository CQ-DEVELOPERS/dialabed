﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeAuthorise_Insert
  ///   Filename       : p_StockTakeAuthorise_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:45
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StockTakeAuthorise table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null,
  ///   @StatusCode nvarchar(20) = null,
  ///   @OperatorId int = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   StockTakeAuthorise.InstructionId,
  ///   StockTakeAuthorise.StatusCode,
  ///   StockTakeAuthorise.OperatorId,
  ///   StockTakeAuthorise.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeAuthorise_Insert
(
 @InstructionId int = null,
 @StatusCode nvarchar(20) = null,
 @OperatorId int = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StockTakeAuthorise
        (InstructionId,
         StatusCode,
         OperatorId,
         InsertDate)
  select @InstructionId,
         @StatusCode,
         @OperatorId,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
