﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Pallet_Query
  ///   Filename       : p_Report_Pallet_Query.sql
  ///   Create By      : Karen
  ///   Date Created   : Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Pallet_Query
(
 @JobId				nvarchar(30),
 @PalletId			nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(255),
   RouteId            int,
   Route              nvarchar(50),
   JobId              int,
   InstructionId      int,
   Pallet             nvarchar(10),
   StorageUnitBatchId int,
   StorageUnitId	  int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   EndDate            datetime,
   SKUCode            nvarchar(50),
   SKU				  nvarchar(50),
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   OperatorId         int,
   Operator           nvarchar(50),
   DropSequence       int,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   PickArea           nvarchar(50),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   StoreArea          nvarchar(50),
   NettWeight		  float,
   SupplierCode		  nvarchar(50),
   PrincipalId		  int,
   Principal	      nvarchar(50)
  )
  
  declare @OutboundShipmentId int,
          @WarehouseId        int,
          @Indicator		        char(1)
  
  if @JobId = -1
	set @JobId = null
	
  if @PalletId = -1
	set @PalletId = null
  
  set @JobId = replace(@JobId, 'J:', '')
   
  if @JobId like 'R:%'
  	 select @JobId = convert(nvarchar(30), JobId)
  	   from Job (nolock)
  	  where ReferenceNumber = @JobId
  
  if @JobId is not null
	  insert @TableResult
			(OutboundShipmentId,
			 IssueId,
			 IssueLineId,
			 JobId,
			 InstructionId,
			 StorageUnitBatchId,
			 EndDate,
			 Quantity,
			 ConfirmedQuantity,
			 ShortQuantity,
			 OperatorId,
			 OutboundDocumentId,
			 PickLocationId,
			 StoreLocationId)
	  select ili.OutboundShipmentId,
			 ili.IssueId,
			 ili.IssueLineId,
			 ins.JobId,
			 ins.InstructionId,
			 ins.StorageUnitBatchId,
			 ins.EndDate,
			 ili.Quantity,
			 isnull(ili.ConfirmedQuantity,0),
			 ili.Quantity - isnull(ili.ConfirmedQuantity,0),
			 ins.OperatorId,
			 ili.OutboundDocumentId,
			 ins.PickLocationId,
			 ins.StoreLocationId
		from IssueLineInstruction ili (nolock)
		join Instruction           ins (nolock) on ili.InstructionId    = ins.InstructionId
	   where ins.JobId = @JobId
   else
	insert @TableResult
        (OutboundShipmentId,
         IssueId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         OperatorId,
         OutboundDocumentId,
         PickLocationId,
         StoreLocationId)
  select ili.OutboundShipmentId,
         ili.IssueId,
         ili.IssueLineId,
         ins.JobId,
         ins.InstructionId,
         ins.StorageUnitBatchId,
         ins.EndDate,
         ili.Quantity,
         isnull(ili.ConfirmedQuantity,0),
         ili.Quantity - isnull(ili.ConfirmedQuantity,0),
         ins.OperatorId,
         ili.OutboundDocumentId,
         ins.PickLocationId,
         ins.StoreLocationId
    from IssueLineInstruction ili (nolock)
    join Instruction           ins (nolock) on ili.InstructionId    = ins.InstructionId
   where ins.PalletId = @PalletId
   
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set RouteId = i.RouteId
    from @TableResult     tr
    join Issue i (nolock) on tr.IssueId = i.IssueId
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         SKU		 = sku.SKU,
         Batch       = b.Batch,
         StorageUnitId = su.StorageUnitId,
         PrincipalId   = p.PrincipalId
    from @TableResult    tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
  
  update t
     set SupplierCode     = p.Barcode
    from @TableResult t
    join Pack        p (nolock) on t.StorageUnitId      = p.StorageUnitId
    join PackType   pt (nolock) on p.PackTypeId         = pt.PackTypeId
   where pt.PackType = 'Supplier'
   
  update tr
     set StoreLocation = l.Location,
         StoreArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId       = al.LocationId
    join Area          a (nolock) on al.AreaId          = a.AreaId
  
  update tr
     set PickLocation = l.Location,
         PickArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId       = al.LocationId
    join Area          a (nolock) on al.AreaId          = a.AreaId
  
  select @OutboundShipmentId = OutboundShipmentId
    from @TableResult
  
  update tr
     set Pallet = convert(nvarchar(10), j.DropSequence)
               + ' of '
               + convert(nvarchar(10), j.Pallets)
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
    
   update tr
     set Principal = p.Principal
    from @TableResult tr
    join Principal p (nolock) on tr.PrincipalId = p.PrincipalId
  
  select @WarehouseId = WarehouseId
    from Instruction (nolock)
   where JobId = @JobId
  
  if dbo.ufn_Configuration(71, @warehouseId) = 0
    update @TableResult
       set ExternalCompany = null
  
  if dbo.ufn_Configuration(219, @warehouseId) = 0
    update @TableResult
       set ConfirmedQuantity = null
     
  declare @InboundSequence smallint,
		 @PackTypeId	int,
		 @PackType   	nvarchar(30),
		 @ShowWeight	bit = null
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence

  
  select @ShowWeight = Indicator
  from Configuration where ConfigurationId = 206

  if  @ShowWeight = 1
	update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   where pk.PackTypeId = @PackTypeId
                       and p.ProductCode = tr.ProductCode
                       and isnull(su.ProductCategory,'') != 'V')* tr.ConfirmedQuantity)
    
    from @TableResult tr
  
  select i.OutboundShipmentId,
         null as 'Route',
         null as 'OrderNumber',
         0 as 'DropSequence',
         i.JobId,
         '0' as 'Pallet',
         null as 'ExternalCompany',
         p.ProductCode,
         p.Product,
         max(i.EndDate) as 'EndDate',
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         null as 'PickArea',
         pick.Location as 'PickLocation',
         null as 'StoreArea',
         min(store.Location) as 'StoreLocation',
         sum(i.Quantity) as 'Quantity',
         sum(isnull(i.ConfirmedQuantity,0)) as 'ConfirmedQuantity',
         sum(i.Quantity - isnull(i.ConfirmedQuantity,0)) as 'ShortQuantity',
         null as 'QuantityOnHand',
         isnull(o.Operator,'Unknown') as 'Operator',
         null as 'NettWeight',
         null as 'StorageUnitBatchId',
         pr.Principal as 'Principal'
    from Instruction        i (nolock)
    join Status            si (nolock) on i.StatusId           = si.StatusId
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on j.StatusId           = s.StatusId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
    join Batch              b (nolock) on sub.BatchId          = b.BatchId
    join Location        pick (nolock) on i.PickLocationId     = pick.LocationId
    left
    join Location       store (nolock) on i.StoreLocationId    = store.LocationId
    left
    join Operator           o (nolock) on j.OperatorId         = o.OperatorId
    join Principal         pr (nolock) on p.PrincipalId = pr.PrincipalId
   where j.JobId = @JobId
  group by i.OutboundShipmentId,
         i.JobId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         pick.Location,
         isnull(Operator,'Unknown'),
         pr.Principal
union
select OutboundShipmentId,
         Route,
         OrderNumber,
         isnull(DropSequence,1) as 'DropSequence',
         JobId,
         Pallet,
         ExternalCompany,
         'Blank',
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         MAX(StoreLocation),
         null,
         null,
         null,
         null,
         MAX(Operator),
         sum(NettWeight),
         SupplierCode,
         Principal as 'Principal'
    from @TableResult
   group by OutboundShipmentId,
         Route,
         OrderNumber,
         isnull(DropSequence,1),
         JobId,
         Pallet,
         ExternalCompany,
         SupplierCode,
         Principal 
   order by OutboundShipmentId,
         JobId,
         Pallet desc,
         OrderNumber,
         SKUCode,
         ProductCode,
         Principal
  

end
