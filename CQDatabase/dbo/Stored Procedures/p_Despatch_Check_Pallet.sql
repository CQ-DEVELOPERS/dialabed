﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Despatch_Check_Pallet
  ///   Filename       : p_Despatch_Check_Pallet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Despatch_Check_Pallet
(
 @barcode    nvarchar(50),
 @tareWeight numeric(13,3),
 @weight     numeric(13,3)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int,
          @PalletId          int
  
  set @Errormsg = 'Error executing p_Despatch_Check_Pallet'
  set @Error = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId           = replace(@barcode,'J:',''),
           @barcode = null
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId        = replace(@barcode,'P:',''),
           @barcode = null
  
  if @PalletId is not null
    select @JobId = JobId
      from Instruction (nolock)
     where PalletId = @PalletId
  
  begin transaction
  
  if @JobId is null
  begin
      set @Error = 900001
      set @Errormsg = 'Invlid barcode'
      goto error
  end
  
  if not exists(select 1 from Job where JobId = @JobId)
  begin
      set @Error = 900001
      set @Errormsg = 'Invlid barcode'
      goto error
  end
  
  if @tareWeight = 0 and @weight = 0
    goto result
  
  if isnull(@tareWeight,0) < 0
  begin
      set @Error = 900001
      set @Errormsg = 'Tare weight cannot be less than zero'
      goto error
  end
  
  if isnull(@weight,0) <= 0
  begin
      set @Error = 900001
      set @Errormsg = 'Weight cannot be zero'
      goto error
  end
  
  exec @Error = p_Job_Update
   @JobId      = @JobId,
   @TareWeight = @tareWeight,
   @Weight     = @weight
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Status_Rollup
   @jobId = @JobId
  
  if @Error <> 0
    goto error
  
  if @PalletId is not null
  begin
    exec @Error = p_Pallet_Update
     @PalletId = @PalletId,
     @Weight   = @weight,
     @Tare     = @tareWeight
    
    if @Error <> 0
      goto error
  end
  
  result:
    commit transaction
    select @Error
    return @Error
    return
  
  error:
    --raiserror @Error @ErrorMsg
    rollback transaction
    select @Error
    return @Error
end
