﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitArea_Search
  ///   Filename       : p_StorageUnitArea_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012 14:20:20
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StorageUnitArea table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null output,
  ///   @AreaId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StorageUnitArea.StorageUnitId,
  ///   StorageUnit.StorageUnit,
  ///   StorageUnitArea.AreaId,
  ///   Area.Area,
  ///   StorageUnitArea.StoreOrder,
  ///   StorageUnitArea.PickOrder,
  ///   StorageUnitArea.MinimumQuantity,
  ///   StorageUnitArea.ReorderQuantity,
  ///   StorageUnitArea.MaximumQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitArea_Search
(
 @StorageUnitId int = null output,
 @AreaId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  select
         StorageUnitArea.StorageUnitId,
         ufn_StorageUnit.StorageUnit,
         StorageUnitArea.AreaId,
         Area.Area,
         StorageUnitArea.StoreOrder,
         StorageUnitArea.PickOrder,
         StorageUnitArea.MinimumQuantity,
         StorageUnitArea.ReorderQuantity,
         StorageUnitArea.MaximumQuantity 
    from StorageUnitArea
    join ufn_StorageUnit(null) on ufn_StorageUnit.StorageUnitId = StorageUnitArea.StorageUnitId
    join Area on Area.AreaId = StorageUnitArea.AreaId
   where isnull(StorageUnitArea.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(StorageUnitArea.StorageUnitId,'0'))
     and isnull(StorageUnitArea.AreaId,'0')  = isnull(@AreaId, isnull(StorageUnitArea.AreaId,'0'))
  
end
