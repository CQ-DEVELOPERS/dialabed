﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Test_Delete
  ///   Filename       : p_COA_Test_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Test_Delete
 (
 @COATestId  int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec p_COATest_Delete
   @COATestId  = @COATestId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_COA_Test_Delete'
    rollback transaction
    return @Error
end
