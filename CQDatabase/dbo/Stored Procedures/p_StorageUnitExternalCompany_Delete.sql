﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitExternalCompany_Delete
  ///   Filename       : p_StorageUnitExternalCompany_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:57
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StorageUnitExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @ExternalCompanyId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitExternalCompany_Delete
(
 @StorageUnitId int = null,
 @ExternalCompanyId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete StorageUnitExternalCompany
     where StorageUnitId = @StorageUnitId
       and ExternalCompanyId = @ExternalCompanyId
  
  select @Error = @@Error
  
  
  return @Error
  
end
