﻿
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentContactList_Delete
  ///   Filename       : p_InboundDocumentContactList_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jan 2013 13:16:47
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InboundDocumentContactList table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentContactList_Delete
(
@InboundDocumentId int
)
 
as
begin
      set nocount on;
  
       declare @Error int

  delete InboundDocumentContactList where InboundDocumentId = @InboundDocumentId
  
  select @Error = @@Error
  
  
  return @Error
  
end

