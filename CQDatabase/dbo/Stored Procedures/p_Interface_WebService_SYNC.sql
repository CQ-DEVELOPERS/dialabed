﻿create procedure [dbo].[p_Interface_WebService_SYNC]
(
	@doc varchar(max) output
)
--with encryption
as
begin
	Declare @doc2 xml
	set @doc2 = convert(xml,@doc)
	DECLARE @idoc int
	Declare @InsertDate datetime
	DEclare @MasterFile varchar(50)
	DEclare @SendFlag bit
	
	Set @InsertDate = Getdate()
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc2

	SELECT    @MasterFile = MasterFile
		FROM       OPENXML (@idoc, '/Root/Header',1)
            WITH (MasterFile varchar(50) 'MasterFile')
            
     Select @SendFlag = SendFlag from InterfaceMasterFile (nolock) where MAsterFile = @MasterFile
     If @SendFlag = 1
     Begin
		update i set SendFlag = 0, 
					LastDate = @InsertDate
		From InterfaceMasterFile i where MAsterFile = @MasterFile
     End
     Set @Doc = @SendFlag
	 Set @doc2 = ''
End


