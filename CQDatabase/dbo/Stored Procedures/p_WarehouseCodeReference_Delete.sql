﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WarehouseCodeReference_Delete
  ///   Filename       : p_WarehouseCodeReference_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 09:52:56
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the WarehouseCodeReference table.
  /// </remarks>
  /// <param>
  ///   @WarehouseCodeReferenceId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WarehouseCodeReference_Delete
(
 @WarehouseCodeReferenceId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete WarehouseCodeReference
     where WarehouseCodeReferenceId = @WarehouseCodeReferenceId
  
  select @Error = @@Error
  
  
  return @Error
  
end
