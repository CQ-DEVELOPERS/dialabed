﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_List
  ///   Filename       : p_COA_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:50
  /// </summary>
  /// <remarks>
  ///   Selects rows from the COA table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   COA.COAId,
  ///   COA.COA 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as COAId
        ,'{All}' as COA
  union
  select
         COA.COAId
        ,COA.COA
    from COA
  order by COA
  
end
