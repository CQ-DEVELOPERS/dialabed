﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Build_Result
  ///   Filename       : p_Planning_Build_Result.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Build_Result
(
 @OutboundShipmentId int = null,
 @IssueId            int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableIssueLine as table
	 (
	  IssueLineId int
	 )
	 
	 declare @TableResult as table
	 (
	  JobId  int,
	  InstructionTypeCode nvarchar(10),
	  Lines               int
	 )
	 
	 if @OutboundShipmentId = -1
	   set @OutboundShipmentId = null
	 
	 if @IssueId = -1
	   set @IssueId = null
	 
	 if @IssueId is not null
	 begin
	   insert @TableIssueLine
	         (IssueLineId)
	   select il.IssueLineId
	     from IssueLine il (nolock)
	    where il.IssueId = @IssueId
  	 
	   insert @TableResult
	         (JobId,
	          InstructionTypeCode,
	          Lines)
	   select i.JobId,
	          it.InstructionTypeCode,
	          count(1)
	     from @TableIssueLine  t
	     join Instruction      i (nolock) on t.IssueLineId       = i.IssueLineId
	     join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
	   group by i.JobId,
	            it.InstructionTypeCode
	 end
	 else if @OutboundShipmentId is not null
	 begin
	   insert @TableIssueLine
	         (IssueLineId)
	   select il.IssueLineId
	     from IssueLine il (nolock)
	     join OutboundShipmentIssue osi (nolock) on il.IssueId = osi.IssueId
	    where osi.OutboundShipmentId = @OutboundShipmentId
  	 
	   insert @TableResult
	         (JobId,
	          InstructionTypeCode,
	          Lines)
	   select i.JobId,
	          it.InstructionTypeCode,
	          count(1)
	     from Instruction      i (nolock)
	     join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
	    where i.OutboundShipmentId = @OutboundShipmentId
	   group by i.JobId,
	            it.InstructionTypeCode
	 end
	 
	 declare @Full  int,
	         @Mixed int,
	         @Part  int
	 
	 select @Full  = count(1) from @TableResult where InstructionTypeCode = 'P'
	 select @Mixed = count(1) from @TableResult where InstructionTypeCode != 'P' and Lines > 1
	 select @Part  = count(1) from @TableResult where InstructionTypeCode != 'P' and Lines = 1
	 
	 select @Full  as 'Full',
	        @Mixed as 'Mixed',
	        @Part  as 'Part'
end
