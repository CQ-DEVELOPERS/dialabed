﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceInvoice_Insert
  ///   Filename       : p_InterfaceInvoice_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:20
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceInvoice table.
  /// </remarks>
  /// <param>
  ///   @InterfaceInvoiceId int = null output,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @InvoiceNumber nvarchar(60) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null 
  /// </param>
  /// <returns>
  ///   InterfaceInvoice.InterfaceInvoiceId,
  ///   InterfaceInvoice.OrderNumber,
  ///   InterfaceInvoice.InvoiceNumber,
  ///   InterfaceInvoice.ProcessedDate,
  ///   InterfaceInvoice.RecordStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceInvoice_Insert
(
 @InterfaceInvoiceId int = null output,
 @OrderNumber nvarchar(60) = null,
 @InvoiceNumber nvarchar(60) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceInvoiceId = '-1'
    set @InterfaceInvoiceId = null;
  
  if @OrderNumber = '-1'
    set @OrderNumber = null;
  
  if @InvoiceNumber = '-1'
    set @InvoiceNumber = null;
  
	 declare @Error int
 
  insert InterfaceInvoice
        (OrderNumber,
         InvoiceNumber,
         ProcessedDate,
         RecordStatus)
  select @OrderNumber,
         @InvoiceNumber,
         @ProcessedDate,
         @RecordStatus 
  
  select @Error = @@Error, @InterfaceInvoiceId = scope_identity()
  
  
  return @Error
  
end
