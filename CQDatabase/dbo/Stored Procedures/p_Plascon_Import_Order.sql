﻿CREATE procedure [dbo].[p_Plascon_Import_Order]  
--with encryption  
as  
begin 
	set nocount on

    declare  
        --Header variables
         @InterfaceImportHeaderId       int
        ,@PrimaryKey                    nvarchar(30)
        ,@OrderNumber                   nvarchar(30)
        ,@InvoiceNumber                 nvarchar(30)
        ,@RecordType                    nvarchar(30)
        ,@RecordStatus                  char(1)
        ,@CompanyCode                   nvarchar(30)
        ,@Company                       nvarchar(255)
        ,@Address                       nvarchar(255)
        ,@FromWarehouseCode             nvarchar(10)
        ,@ToWarehouseCode               nvarchar(10)
        ,@Route                         nvarchar(50)
        ,@DeliveryNoteNumber            nvarchar(30)
        ,@ContainerNumber               nvarchar(30)
        ,@SealNumber                    nvarchar(30)
        ,@DeliveryDate                  nvarchar(20)
        ,@Remarks                       nvarchar(255)
        ,@NumberOfLines                 int
        ,@VatPercentage                 decimal(13,3)
        ,@VatSummary                    decimal(13,3)
        ,@Total                         decimal(13,3)

        --Detail variables
        ,@ForeignKey                    nvarchar(30)
        ,@LineNumber                    int
        ,@ProductCode                   nvarchar(30)
        ,@Product                       nvarchar(50)
        ,@SKUCode                       nvarchar(10)
        ,@Batch                         nvarchar(50)
        ,@Quantity                      int
        ,@Weight                        decimal(13,3)
        ,@RetailPrice                   decimal(13,3)
        ,@NetPrice                      decimal(31,2)
        ,@LineTotal                     decimal(13,2)
        ,@Volume                        decimal(13,3)

        --Proc variables
        ,@GetDate                       datetime
        ,@Days                          int  
        ,@ReleaseDate                   datetime
        ,@Error                         int
        ,@ErrorMsg                      nvarchar(100)
        ,@InboundDocumentId             int
        ,@InboundDocumentTypeId         int
        ,@InboundDocumentTypeCode       nvarchar(10)
        ,@InboundLineId                 int
        ,@OutboundDocumentId            int
        ,@OutboundDocumentTypeId        int    
        ,@OutboundDocumentTypeCode      nvarchar(10)
        ,@OutboundLineId                int
        ,@FromWarehouseId               int
        ,@ToWarehouseId                 int
        ,@InPriorityId                  int
        ,@OutPriorityId                 int
        ,@AutoRelease                   bit
        ,@AddToShipment                 bit
        ,@MultipleOnShipment            bit
        ,@ExternalCompanyId             int
        ,@ExternalCompanyCode           nvarchar(30)
        ,@ExternalCompany               nvarchar(255)
        ,@ODStatusId                    int
        ,@IDStatusId                    int
        ,@StorageUnitBatchId            int
        ,@StorageUnitId                 int
        ,@BatchId                       int
        ,@SequenceNumber                int
        ,@AddressId						               int
        ,@RouteId                       int
        ,@IssueId int
        ,@OutboundShipmentId int
          
    select @GetDate = dbo.ufn_Getdate()  

    if(object_id('tempdb.dbo.##p_Plascon_Import_Order_Test') is null)  
        create table ##p_Plascon_Import_Order_Test (objname sysname)  
    else  
    begin  
        select 'RETURN'  
        RETURN  
    end  
  
    --Added timeout lock due to update hanging from time to time
    SET LOCK_TIMEOUT 30000

	BEGIN TRY

	  BEGIN TRAN
		update d  
		set InterfaceImportHeaderId = (select max(InterfaceImportHeaderId)  
										from InterfaceImportHeader h  
									   where d.ForeignKey = h.PrimaryKey)  
		from InterfaceImportDetail d  
		where InterfaceImportHeaderId is null  
	    
	  COMMIT  

	  IF @@error = 0
		PRINT 'Interface foreignkey match update successful'

	END TRY
	BEGIN CATCH
		PRINT ERROR_MESSAGE()
		ROLLBACK
	END CATCH

	SET LOCK_TIMEOUT -1
  
    set @Days = 0  

    select @ReleaseDate = convert(datetime, convert(varchar(10), @GetDate, 120))  
          ,@ReleaseDate = dateadd(hh, 12, @ReleaseDate)

    if @Getdate <= @ReleaseDate set @ReleaseDate = dateadd(hh, 48, @ReleaseDate)  
    else set @ReleaseDate = dateadd(hh, 72, @ReleaseDate)  
   
    if datename(dw, @ReleaseDate) = 'Saturday' set @Days = 2  
    if datename(dw, @ReleaseDate) = 'Sunday'  set @Days = 2  
   
    select @ReleaseDate = dateadd(dd, @Days, @ReleaseDate)  

    -- Reprocess Order which did not process properly  
    update InterfaceImportHeader  
    set ProcessedDate = null  
    where RecordStatus     = 'N'  
      and ProcessedDate   <= dateadd(mi, -5, @Getdate)  
   
    SET ROWCOUNT 250
   
    update h  
    set ProcessedDate = @Getdate  
    from InterfaceImportHeader h  
      Left Join InterfaceImportDetail d on d.InterfaceImportHeaderId = h.InterfaceImportHeaderId   
    where (d.InterfaceImportHeaderId is not null)  
      and h.RecordStatus in ('N','U')  
      and ProcessedDate is null  

    declare header_cursor cursor for  
    select 
         InterfaceImportHeaderId
        ,PrimaryKey
        ,OrderNumber
        ,InvoiceNumber
        ,RecordType
        ,RecordStatus
        ,CompanyCode
        ,ISNULL(Company, CompanyCode)
        ,RTRIM(Address)
        ,FromWarehouseCode
        ,ToWarehouseCode
        ,Route
        ,DeliveryNoteNumber
        ,ContainerNumber
        ,SealNumber
        ,Isnull(DeliveryDate,@GetDate)
        ,Remarks
        ,NumberOfLines
        ,VatPercentage
        ,VatSummary
        ,Total
        --,Additional1
        --,Additional2
        --,Additional3
        --,Additional4
        --,Additional5
        --,Additional6
        --,Additional7
        --,Additional8
        --,Additional9
        --,Additional10
    from InterfaceImportHeader
    where ProcessedDate = @Getdate  
    order by OrderNumber  
   
    open header_cursor  
   
    fetch header_cursor  
    into @InterfaceImportHeaderId
        ,@PrimaryKey
        ,@OrderNumber
        ,@InvoiceNumber
        ,@RecordType
        ,@RecordStatus
        ,@CompanyCode
        ,@Company
        ,@Address
        ,@FromWarehouseCode
        ,@ToWarehouseCode
        ,@Route
        ,@DeliveryNoteNumber
        ,@ContainerNumber
        ,@SealNumber
        ,@DeliveryDate
        ,@Remarks
        ,@NumberOfLines
        ,@VatPercentage
        ,@VatSummary
        ,@Total 
    
    while (@@fetch_status = 0)  
    begin  
        exec @Error = p_Order_Delete @OrderNumber  

        if @Error != 0  
        begin  
            select @ErrorMsg = 'Error executing p_Order_Delete'  
            goto error_header  
        end
        
        set @Address = substring(@Address,1,100)  

        begin transaction xml_import 

        update InterfaceImportHeader  
        set RecordStatus = 'C',  
            ProcessedDate = @GetDate  
        where InterfaceImportHeaderId = @InterfaceImportHeaderId  

        select   
             @InboundDocumentTypeId = null  
            ,@OutboundDocumentTypeId = null  
            ,@InboundDocumentTypeCode = null  
            ,@OutboundDocumentTypeCode = null  
            ,@FromWarehouseId = null  
            ,@ToWarehouseId = null  
            
        select 
             @InboundDocumentTypeId  = InboundDocumentTypeId
            ,@OutboundDocumentTypeId = OutboundDocumentTypeId  
            ,@FromWarehouseId        = WarehouseId
            ,@ToWarehouseId          = ToWarehouseId  
        from WarehouseCodeReference  
        where DownloadType  = @RecordType
          --and WarehouseCode = @FromWarehouseCode          
          --and ToWarehouseCode = @ToWarehouseCode

        if not exists(select 1 from Warehouse where WarehouseId in (@FromWarehouseId,@ToWarehouseId))  
        begin  
            select @ErrorMsg = 'Error executing p_Plascon_Import_Order - Warehouse does not exist'  
            goto error_header  
        end
        
        select  @ExternalCompanyCode = @CompanyCode 
               ,@ExternalCompany = @Company
        
        /*Check for outbound document*/
        if @OutboundDocumentTypeId is not null  
        begin 
        
            IF @RecordType in ('TCP','PIC','IBT')
            Begin
            /*Send Pic confirmation*/
            
                exec p_Sequence_Number 
                     @Value = @SequenceNumber output
                                
                insert InterfaceExtract
                (
                    RecordType,
                    SequenceNumber,
                    OrderNumber,
                    LineNumber,
                    RecordStatus,
                    ExtractedDate,
                    Data
                )
                select 
                    'DCP',
                    @SequenceNumber,
                    @OrderNumber,
                    1,
                    'N',
                    @GetDate,
                    replicate(' ',9) + 
                    'DCP' + 
                    '045' +
                    --replicate('0',3 - datalength(convert(varchar(3),@ToWarehouseCode))) + convert(varchar(3),@ToWarehouseCode) + /**** If 045 warehouse is a problem, use warehouse lookup*/
                    convert(varchar(8),ltrim(@OrderNumber)) +  replicate(' ',8 - datalength(convert(varchar(8),ltrim(@OrderNumber))))

                if @@error <> 0
                begin
                    
                    select @ErrorMsg = 'Error inserting confirmation into interfaceextract'
                    goto error_header
                end
            End
            
            if @RecordType = 'PIC' and @Route = 'COL'
            begin
              select @OutboundDocumentTypeId   = OutboundDocumentTypeId
                    ,@OutboundDocumentTypeCode = OutboundDocumentTypeCode
                    ,@OutPriorityId            = PriorityId
                    ,@AutoRelease              = AutoRelease  
                    ,@AddToShipment            = AddToShipment  
                    ,@MultipleOnShipment       = MultipleOnShipment  
                from OutboundDocumentType  
               where OutboundDocumentTypeCode = @Route
            end
            else
            begin
              select @OutboundDocumentTypeCode = OutboundDocumentTypeCode
                    ,@OutPriorityId            = PriorityId
                    ,@AutoRelease              = AutoRelease  
                    ,@AddToShipment            = AddToShipment  
                    ,@MultipleOnShipment       = MultipleOnShipment  
              from OutboundDocumentType  
              where OutboundDocumentTypeId = @OutboundDocumentTypeId
            end
            
            set @RouteId = null
            
            select @RouteId = RouteId
              from Route
             where RouteCode = @Route
            
            if @RouteId is null
            begin
              exec @Error = p_Route_Insert
               @RouteId   = @RouteId output
              ,@Route     = @Route
              ,@RouteCode = @Route
              
              if @Error != 0
              begin
                  select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
                  goto error_header
              end
            end
            
            set @ExternalCompanyId = null
                          
            if @ExternalCompanyCode is not null  
            begin  
                select @ExternalCompanyId   = ExternalCompanyId
                      ,@ExternalCompanyCode = ExternalCompanyCode
                      ,@ExternalCompany     = ExternalCompany  
                  from ExternalCompany (nolock)  
                 where ExternalCompanyCode = @ExternalCompanyCode  
          
                if @ExternalCompanyId is null  
                begin  
                    exec @Error = p_ExternalCompany_Insert  
                                        @ExternalCompanyId         = @ExternalCompanyId output
                                       ,@ExternalCompanyTypeId     = 2
                                       ,@RouteId                   = null  
                                       ,@ExternalCompany           = @ExternalCompany
                                       ,@ExternalCompanyCode       = @ExternalCompanyCode  
               
                    if @Error != 0  
                    begin  
                        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'  
                        goto error_header  
                    end  
                end  
                
                if @Address Is Not Null
                Begin
                  exec @Error = p_Address_Insert 
						                  @AddressId         = @AddressId OUTPUT
						                , @ExternalCompanyId = @ExternalCompanyId
						                , @Street            = @Address
						                , @Suburb            = Null
						                , @Town              = Null
						                , @Country           = Null
						                , @Code              = Null
                End                
            end
       
            select @ODStatusId = dbo.ufn_StatusId('OD','I')  
           
            exec @Error = p_OutboundDocument_Insert  
                   @OutboundDocumentId     = @OutboundDocumentId output,  
                   @OutboundDocumentTypeId = @OutboundDocumentTypeId,  
                   @ExternalCompanyId      = @ExternalCompanyId,  
                   @StatusId               = @ODStatusId,  
                   @WarehouseId            = @FromWarehouseId,  
                   @OrderNumber            = @OrderNumber,  
                   @DeliveryDate           = @DeliveryDate,  
                   @CreateDate             = @GetDate,  
                   @ModifiedDate           = null  
           
            if @Error != 0  
            begin  
                select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'  
                goto error_header  
            end  
        end  
     
        /*Check for inbound document*/
        if @InboundDocumentTypeId is not null
        begin  
                          
            select @InboundDocumentTypeCode = InboundDocumentTypeCode,  
                   @InPriorityId            = PriorityId  
            from InboundDocumentType  
            where InboundDocumentTypeId = @InboundDocumentTypeId  
          
            set @ExternalCompanyId = null  
       
            if @ExternalCompanyCode is not null  
            begin  
                select @ExternalCompanyId   = ExternalCompanyId,  
                       @ExternalCompanyCode = ExternalCompanyCode,  
                       @ExternalCompany     = ExternalCompany  
                  from ExternalCompany (nolock)  
                 where ExternalCompanyCode = @ExternalCompanyCode
         
                if @ExternalCompanyId is null  
                begin  
                    exec @Error = p_ExternalCompany_Insert  
                                       @ExternalCompanyId         = @ExternalCompanyId output,  
                                       @ExternalCompanyTypeId     = 2,  
                                       @RouteId                   = null,  
                                       @ExternalCompany           = @ExternalCompany,  
                                       @ExternalCompanyCode       = @ExternalCompanyCode  
           
                    if @Error != 0  
                    begin  
                        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'  
                        goto error_header  
                    end 
                    
                    if @Address Is Not Null
					Begin
					 exec @Error = p_Address_Insert 
										  @AddressId         = @AddressId OUTPUT
										, @ExternalCompanyId = @ExternalCompanyId
										, @Street            = @Address
										, @Suburb            = Null
										, @Town              = Null
										, @Country           = Null
										, @Code              = Null
					End  
                end  
            end  
            else  
            begin  
                select @ExternalCompanyId   = ExternalCompanyId,  
                       @ExternalCompanyCode = ExternalCompanyCode,  
                       @ExternalCompany     = ExternalCompany  
                  from ExternalCompany (nolock)  
                 where ExternalCompanyCode = isnull(@FromWarehouseCode, @ToWarehouseCode)  
         
                if @ExternalCompanyId is null  
                begin  
                    set @ExternalCompanyCode = isnull(@FromWarehouseCode, @ToWarehouseCode)  
           
                    exec @Error = p_ExternalCompany_Insert  
                                       @ExternalCompanyId         = @ExternalCompanyId output,  
                                       @ExternalCompanyTypeId     = 1,  
                                       @RouteId                   = null,  
                                       --@drop  
                                       @ExternalCompany           = @ExternalCompanyCode, 
                          @ExternalCompanyCode       = @ExternalCompanyCode  
           
                    if @Error != 0  
                    begin  
                        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'  
                        goto error_header  
                    end  
                    
                   	if @Address Is Not Null
					Begin
						exec @Error = p_Address_Insert 
										  @AddressId         = @AddressId OUTPUT
										, @ExternalCompanyId = @ExternalCompanyId
										, @Street            = @Address
										, @Suburb            = Null
										, @Town              = Null
										, @Country           = Null
										, @Code              = Null
					End 
                end  
            end  
       
            select @IDStatusId = dbo.ufn_StatusId('ID','W')  
       
            exec @Error = p_InboundDocument_Insert  
                                 @InboundDocumentId     = @InboundDocumentId output,  
                                 @InboundDocumentTypeId = @InboundDocumentTypeId,  
                                 @ExternalCompanyId     = @ExternalCompanyId,  
                                 @StatusId              = @IDStatusId,  
                                 @WarehouseId           = @ToWarehouseId,  
                                 @OrderNumber           = @OrderNumber,  
                                 @DeliveryDate          = @DeliveryDate,  
                                 @CreateDate            = @GetDate,  
                                 @ModifiedDate          = null  
       
            if @Error != 0  
            begin  
                select @ErrorMsg = 'Error executing p_InboundDocument_Insert'  
                goto error_header  
            end  
        end
        
        declare detail_cursor cursor for         
        select ForeignKey
              ,LineNumber
              ,RTrim(LTrim(ProductCode))
              ,Product
              ,SKUCode
              ,Batch
              ,Quantity
              ,Weight
              ,RetailPrice
              ,NetPrice
              ,LineTotal
              ,Volume
        from   InterfaceImportDetail
        where  InterfaceImportHeaderId = @InterfaceImportHeaderId 
        group by
			   ForeignKey
              ,LineNumber
              ,RTrim(LTrim(ProductCode))
              ,Product
              ,SKUCode
              ,Batch
              ,Quantity
              ,Weight
              ,RetailPrice
              ,NetPrice
              ,LineTotal
              ,Volume
              ,InterfaceImportHeaderId
        order by 
               InterfaceImportHeaderId
              ,LineNumber
        
        open   detail_cursor 
        
        fetch  detail_cursor  
        into   @ForeignKey
              ,@LineNumber
              ,@ProductCode
              ,@Product
              ,@SKUCode
              ,@Batch
              ,@Quantity
              ,@Weight
              ,@RetailPrice
              ,@NetPrice
              ,@LineTotal
              ,@Volume
        
        while (@@fetch_status = 0)  
        begin
            select  @StorageUnitBatchId = null  
                   ,@StorageUnitId = null  
           
            exec @Error = p_interface_xml_Product_Insert  
                               @ProductCode        = @ProductCode,  
                               @SKUCode            = @SKUCode,
                               @Product            = @Product,  
                               @Batch              = @Batch,  
                               @WarehouseId        = @FromWarehouseId,  
                               @StorageUnitId      = @StorageUnitId output,  
                               @StorageUnitBatchId = @StorageUnitBatchId output,  
                               @BatchId            = @BatchId output,  
                               @ExternalCompanyId  = @ExternalCompanyId   
            
            if @Error != 0 or @StorageUnitBatchId is null  
            begin  
               select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'  
               goto error_detail  
            end              
            
            select @StorageUnitBatchId = null  
    ,@StorageUnitId = null  
                  ,@BatchId = null      
   
            select  @StorageUnitId   = su.StorageUnitId
                   ,@StorageUnitBatchId = sub.StorageUnitBatchId
                   ,@BatchId            = b.BatchId  
               from Product            p (nolock)
               join StorageUnit       su (nolock) on p.ProductId      = su.ProductId  
               join SKU              sku (nolock) on su.SKUId         = sku.SKUId
               join StorageUnitBatch sub (nolock) on su.StorageUnitId = sub.StorageUnitId  
               join Batch              b (nolock) on sub.BatchId      = b.BatchId  
              where ltrim(p.ProductCode) = ltrim(@ProductCode)
                and sku.SKUCode = @SKUCode
                and b.Batch = 'Default'
            
            --select @OrderNumber,
            --       @ProductCode,
            --       @SKUCode,
            --       @StorageUnitId,
            --       @StorageUnitBatchId,
            --       @BatchId
            
            if @OutboundDocumentTypeCode is not null  
            begin  
                exec @Error = p_OutboundLine_Insert  
                                     @OutboundLineId     = @OutboundLineId output,  
                                     @OutboundDocumentId = @OutboundDocumentId,  
                                     @StorageUnitId      = @StorageUnitId,  
                                     @StatusId           = @ODStatusId,  
                                     @LineNumber         = @LineNumber,  
                                     @Quantity           = @Quantity,  
                                     @BatchId            = @BatchId  
                
                if @Error != 0  
                begin  
                    select @ErrorMsg = 'Error executing p_OutboundLine_Insert'  
                    goto error_detail  
                end  
         
                --exec @Error = p_Despatch_Create_Issue  
                --                     @OutboundDocumentId = @OutboundDocumentId,  
                --                     @OutboundLineId     = @OutboundLineId,  
                --                     @OperatorId         = null  
                
                --if @Error != 0  
                --begin  
                --    select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'  
                --    goto error_detail  
                --end  
            end  
       
            if @InboundDocumentTypeCode is not null  
            begin  
                exec @Error = p_InboundLine_Insert  
                                     @InboundLineId     = @InboundLineId output,  
                                     @InboundDocumentId = @InboundDocumentId,  
                                     @StorageUnitId     = @StorageUnitId,  
                                     @StatusId          = @IDStatusId,  
                                     @LineNumber        = @LineNumber,  
                                     @Quantity          = @Quantity,  
                                     @BatchId           = @BatchId  
         
                if @Error != 0  
                begin  
                    select @ErrorMsg = 'Error executing p_InboundLine_Insert'  
                    goto error_detail  
                end  
             
                exec @Error = p_Receiving_Create_Receipt  
                                     @InboundDocumentId = @InboundDocumentId,  
                                     @InboundLineId     = @InboundLineId,  
                                     @OperatorId        = null  
                    
                if @Error != 0  
                begin  
                    select @ErrorMsg = 'Error executing p_Receiving_Create_Receipt'  
                    goto error_detail  
                end
            end
            
            fetch  detail_cursor  
            into   @ForeignKey
                  ,@LineNumber
                  ,@ProductCode
                  ,@Product
        ,@SKUCode
            ,@Batch
                  ,@Quantity
                  ,@Weight
                  ,@RetailPrice
                  ,@NetPrice
                  ,@LineTotal
                  ,@Volume
        end

        error_detail:
     
        close detail_cursor  
        deallocate detail_cursor  
    
    IF @OutboundDocumentTypeCode is not null
    begin
      exec @Error = p_Despatch_Create_Issue
       @OutboundDocumentId = @OutboundDocumentId,
       @OperatorId         = null,
       @Remarks            = @Remarks
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
        goto error_detail
      end
      
      select @IssueId = IssueId
        from Issue (nolock)
       where OutboundDocumentId = @OutboundDocumentId
      
      exec @Error = p_Outbound_Auto_Load
       @OutboundShipmentId = @OutboundShipmentId output,
       @IssueId            = @IssueId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
        goto error_detail
      end
      
      exec @Error = p_Outbound_Auto_Release
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
        goto error_detail
      end
 end
     
        error_header:  
     
        if @Error = 0  
        begin  
            INSERT InterfaceMessage
				([InterfaceMessageCode]
				,[InterfaceMessage]
				,[InterfaceId]
				,[InterfaceTable]
				,[Status]
				,[OrderNumber]
				,[CreateDate]
				,[ProcessedDate])
			select 'Processed'
				,'Success'
				,@InterfaceImportHeaderId
				,'InterfaceImportHeader'
				,'N'
				,@OrderNumber
				,@GetDate
				,@GetDate
			
			
            if @@trancount > 0  
                        				     
            commit transaction xml_import  
        end  
        else  
        begin  
            Print @ErrorMsg
            
            INSERT InterfaceMessage
				([InterfaceMessageCode]
				,[InterfaceMessage]
				,[InterfaceId]
				,[InterfaceTable]
				,[Status]
				,[OrderNumber]
				,[CreateDate]
				,[ProcessedDate])
			select 'Failed'
				,@ErrorMsg
				,@InterfaceImportHeaderId
				,'InterfaceImportHeader'
				,'E'
				,@OrderNumber
				,@GetDate
				,@GetDate
				
			if @@trancount > 0  
            
            rollback transaction xml_import  
           
            update InterfaceImportHeader  
               set RecordStatus = 'E',  
                   Additional10 = @ErrorMsg,  
                   ProcessedDate = @GetDate  
             where InterfaceImportHeaderId = @InterfaceImportHeaderId  
        end  
     
        Print @OrderNumber
     
        fetch header_cursor  
        into @InterfaceImportHeaderId
            ,@PrimaryKey
            ,@OrderNumber
            ,@InvoiceNumber
            ,@RecordType
            ,@RecordStatus
            ,@CompanyCode
            ,@Company
            ,@Address
            ,@FromWarehouseCode
            ,@ToWarehouseCode
            ,@Route
            ,@DeliveryNoteNumber
            ,@ContainerNumber
            ,@SealNumber
            ,@DeliveryDate
            ,@Remarks
            ,@NumberOfLines
            ,@VatPercentage
            ,@VatSummary
            ,@Total   
    end  
   
    close header_cursor  
    deallocate header_cursor  
    
    update InterfaceImportHeader  
       set ProcessedDate = null  
     where RecordStatus = 'N'  
       and ProcessedDate = @GetDate  
   
    if(object_id('tempdb.dbo.##p_Plascon_Import_Order_Test') is not null)  
    drop table ##p_Plascon_Import_Order_Test  
   
    return  
end  

