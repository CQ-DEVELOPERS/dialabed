﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPOHeader_Parameter
  ///   Filename       : p_InterfaceImportPOHeader_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:59
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportPOHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportPOHeader.InterfaceImportPOHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPOHeader_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceImportPOHeaderId
        ,null as 'InterfaceImportPOHeader'
  union
  select
         InterfaceImportPOHeader.InterfaceImportPOHeaderId
        ,InterfaceImportPOHeader.InterfaceImportPOHeaderId as 'InterfaceImportPOHeader'
    from InterfaceImportPOHeader
  
end
