﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PricingCategory_Parameter
  ///   Filename       : p_PricingCategory_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:51:24
  /// </summary>
  /// <remarks>
  ///   Selects rows from the PricingCategory table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   PricingCategory.PricingCategoryId,
  ///   PricingCategory.PricingCategory 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PricingCategory_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as PricingCategoryId
        ,'{All}' as PricingCategory
  union
  select
         PricingCategory.PricingCategoryId
        ,PricingCategory.PricingCategory
    from PricingCategory
  order by PricingCategory
  
end
 
