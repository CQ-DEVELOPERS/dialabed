﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Select
  ///   Filename       : p_Receipt_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 13:28:39
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Receipt table.
  /// </remarks>
  /// <param>
  ///   @ReceiptId int = null 
  /// </param>
  /// <returns>
  ///   Receipt.ReceiptId,
  ///   Receipt.InboundDocumentId,
  ///   Receipt.PriorityId,
  ///   Receipt.WarehouseId,
  ///   Receipt.LocationId,
  ///   Receipt.StatusId,
  ///   Receipt.OperatorId,
  ///   Receipt.DeliveryNoteNumber,
  ///   Receipt.DeliveryDate,
  ///   Receipt.SealNumber,
  ///   Receipt.VehicleRegistration,
  ///   Receipt.Remarks,
  ///   Receipt.CreditAdviceIndicator,
  ///   Receipt.Delivery,
  ///   Receipt.AllowPalletise,
  ///   Receipt.Interfaced,
  ///   Receipt.StagingLocationId,
  ///   Receipt.NumberOfLines,
  ///   Receipt.ReceivingCompleteDate,
  ///   Receipt.ParentReceiptId,
  ///   Receipt.GRN,
  ///   Receipt.PlannedDeliveryDate,
  ///   Receipt.ShippingAgentId,
  ///   Receipt.ContainerNumber,
  ///   Receipt.AdditionalText1,
  ///   Receipt.AdditionalText2,
  ///   Receipt.BOE,
  ///   Receipt.Incoterms,
  ///   Receipt.ReceiptConfirmed,
  ///   Receipt.ReceivingStarted,
  ///   Receipt.VehicleId,
  ///   Receipt.ParentIssueId,
  ///   Receipt.VehicleTypeId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_Select
(
 @ReceiptId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Receipt.ReceiptId
        ,Receipt.InboundDocumentId
        ,Receipt.PriorityId
        ,Receipt.WarehouseId
        ,Receipt.LocationId
        ,Receipt.StatusId
        ,Receipt.OperatorId
        ,Receipt.DeliveryNoteNumber
        ,Receipt.DeliveryDate
        ,Receipt.SealNumber
        ,Receipt.VehicleRegistration
        ,Receipt.Remarks
        ,Receipt.CreditAdviceIndicator
        ,Receipt.Delivery
        ,Receipt.AllowPalletise
        ,Receipt.Interfaced
        ,Receipt.StagingLocationId
        ,Receipt.NumberOfLines
        ,Receipt.ReceivingCompleteDate
        ,Receipt.ParentReceiptId
        ,Receipt.GRN
        ,Receipt.PlannedDeliveryDate
        ,Receipt.ShippingAgentId
        ,Receipt.ContainerNumber
        ,Receipt.AdditionalText1
        ,Receipt.AdditionalText2
        ,Receipt.BOE
        ,Receipt.Incoterms
        ,Receipt.ReceiptConfirmed
        ,Receipt.ReceivingStarted
        ,Receipt.VehicleId
        ,Receipt.ParentIssueId
        ,Receipt.VehicleTypeId
    from Receipt
   where isnull(Receipt.ReceiptId,'0')  = isnull(@ReceiptId, isnull(Receipt.ReceiptId,'0'))
  order by DeliveryNoteNumber
  
end
