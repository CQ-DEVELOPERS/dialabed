﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMHeader_List
  ///   Filename       : p_BOMHeader_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:05
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   BOMHeader.BOMHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMHeader_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as BOMHeaderId
        ,null as 'BOMHeader'
  union
  select
         BOMHeader.BOMHeaderId
        ,BOMHeader.BOMHeaderId as 'BOMHeader'
    from BOMHeader
  
end
