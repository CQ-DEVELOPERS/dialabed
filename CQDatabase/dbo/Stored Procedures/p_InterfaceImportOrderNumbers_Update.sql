﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportOrderNumbers_Update
  ///   Filename       : p_InterfaceImportOrderNumbers_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:51
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportOrderNumbers table.
  /// </remarks>
  /// <param>
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @RecordStatus nvarchar(20) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @OrderDate datetime = null,
  ///   @Status nvarchar(60) = null,
  ///   @OutboundDocument bit = null,
  ///   @InboundDocument bit = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportOrderNumbers_Update
(
 @PrimaryKey nvarchar(60) = null,
 @RecordStatus nvarchar(20) = null,
 @OrderNumber nvarchar(60) = null,
 @OrderDate datetime = null,
 @Status nvarchar(60) = null,
 @OutboundDocument bit = null,
 @InboundDocument bit = null,
 @ProcessedDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceImportOrderNumbers
     set PrimaryKey = isnull(@PrimaryKey, PrimaryKey),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         OrderDate = isnull(@OrderDate, OrderDate),
         Status = isnull(@Status, Status),
         OutboundDocument = isnull(@OutboundDocument, OutboundDocument),
         InboundDocument = isnull(@InboundDocument, InboundDocument),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
