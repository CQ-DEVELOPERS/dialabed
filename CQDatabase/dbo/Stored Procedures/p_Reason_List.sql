﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Reason_List
  ///   Filename       : p_Reason_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:23
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Reason table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Reason.ReasonId,
  ///   Reason.Reason 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Reason_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ReasonId
        ,'{All}' as Reason
  union
  select
         Reason.ReasonId
        ,Reason.Reason
    from Reason
  
end
