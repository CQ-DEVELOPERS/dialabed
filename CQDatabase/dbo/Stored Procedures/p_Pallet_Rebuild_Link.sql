﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Link
  ///   Filename       : p_Pallet_Rebuild_Link.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Link
(
 @JobId         int,
 @IssueLineId   int,
 @InstructionId int,
 @Quantity      float,
 @OutboundShipmentId int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @NewInstructionId  int,
          @InstructionRefId  int,
          @ConfirmedQuantity float,
          @NewIssueLineId    int,
          @NewIssueId        int,
          @IssueId           int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Pallet_Rebuild_Link'
  
  select @IssueId = IssueId
    from IssueLine (nolock)
   where IssueLineId = @IssueLineId
  
  begin transaction
  
  --select * from viewINS where InstructionId = @InstructionId
  --select * from IssueLineInstruction where IssueLineId=54
  
  if @OutboundShipmentId is not null
  begin
    insert Issue
          (OutboundDocumentId,
           PriorityId,
           WarehouseId,
           LocationId,
           StatusId,
           OperatorId,
           RouteId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration,
           Remarks,
           DropSequence,
           LoadIndicator,
           DespatchBay,
           RoutingSystem,
           Delivery,
           NumberOfLines,
           Total,
           Complete,
           AreaType,
           Units,
           ShortPicks,
           Releases,
           Weight,
           ConfirmedWeight,
           FirstLoaded,
           Interfaced,
           Loaded,
           Pallets,
           Volume,
           ContactListId,
           ManufacturingId,
           ReservedId,
           ParentIssueId)
    select OutboundDocumentId,
           PriorityId,
           WarehouseId,
           LocationId,
           StatusId,
           OperatorId,
           RouteId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration,
           Remarks,
           DropSequence,
           LoadIndicator,
           DespatchBay,
           RoutingSystem,
           Delivery,
           NumberOfLines,
           Total,
           Complete,
           AreaType,
           Units,
           ShortPicks,
           Releases,
           Weight,
           ConfirmedWeight,
           FirstLoaded,
           Interfaced,
           Loaded,
           Pallets,
           Volume,
           ContactListId,
           ManufacturingId,
           ReservedId,
           @IssueId
      from Issue
     where IssueId = @IssueId
    
    select @Error = @@Error, @NewIssueId = SCOPE_IDENTITY()
    
    if @Error <> 0
      goto error
    
    insert IssueLine
          (IssueId,
           OutboundLineId,
           StorageUnitBatchId,
           StatusId,
           OperatorId,
           Quantity,
           ConfirmedQuatity,
           Weight,
           ConfirmedWeight,
           ParentIssueLineId)
    select @NewIssueId,
           OutboundLineId,
           StorageUnitBatchId,
           StatusId,
           OperatorId,
           Quantity,
           ConfirmedQuatity,
           Weight,
           ConfirmedWeight,
           @IssueLineId
      from IssueLine
     where IssueLineId = @IssueLineId
    
    select @Error = @@Error, @NewIssueLineId = SCOPE_IDENTITY()
    
    if @Error <> 0
      goto error
    
    insert OutboundShipmentIssue
          (OutboundShipmentId,
           IssueId,
           DropSequence)
    select @OutboundShipmentId,
           @NewIssueId,
           null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  insert Instruction
        (ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         CheckQuantity,
         CheckWeight,
         OutboundShipmentId,
         DropSequence)
  select ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         @JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         @Quantity,
         @Quantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         @Quantity,
         null,
         isnull(@OutboundShipmentId, OutboundShipmentId),
         DropSequence
    from Instruction
   where InstructionId = @InstructionId
  
  select @Error = @@Error, @NewInstructionId = SCOPE_IDENTITY()
  
  insert IssueLineInstruction
        (OutboundDocumentTypeId,
         OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         InstructionId,
         DropSequence,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight)
  select OutboundDocumentTypeId,
         isnull(@OutboundShipmentId, OutboundShipmentId),
         OutboundDocumentId,
         isnull(@NewIssueId, IssueId),
         isnull(@NewIssueLineId, IssueLineId),
         @NewInstructionId,
         DropSequence,
         @Quantity,
         @Quantity,
         null,--Weight,
         null --ConfirmedWeight
    from IssueLineInstruction
   where InstructionId = @InstructionId
     and IssueLineId   = @IssueLineId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  update IssueLineInstruction
     set ConfirmedQuantity = ConfirmedQuantity - @Quantity
         --ConfirmedWeight   = isnull(ConfirmedWeight, 1) / ???
   where InstructionId = @InstructionId
     and IssueLineId   = @IssueLineId
  
  select @Error = @@Error
  
  update Instruction
     set ConfirmedQuantity = ConfirmedQuantity - @Quantity,
         Quantity = Quantity - @Quantity,
         CheckQuantity = isnull(CheckQuantity - @Quantity,0)
         --ConfirmedWeight   = isnull(ConfirmedWeight, 1) / ???
   where InstructionId = @InstructionId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  select @InstructionRefId  = InstructionRefId,
         @ConfirmedQuantity = ConfirmedQuantity
    from Instruction
   where InstructionId = @InstructionId
  
  exec @Error = p_Instruction_Update_ili
   @InstructionId    = @InstructionId,
   @InstructionRefId = @InstructionRefId,
   @insConfirmed     = @ConfirmedQuantity
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update_ili
   @InstructionId    = @NewInstructionId,
   @InstructionRefId = null,
   @insConfirmed     = @Quantity
  
  if @Error <> 0
    goto error
  
  --select * from viewINS where InstructionId = @InstructionId or InstructionId = @NewInstructionId
  --select * from IssueLineInstruction where IssueLineId=54
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
