﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Stock_Take_Empty
  ///   Filename       : p_Mobile_Stock_Take_Empty.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Stock_Take_Empty
(
 @WarehouseId int,
 @Location    nvarchar(15)
)
 
as
begin
	 set nocount on;
	 
	 select @Location = replace(@Location,'L:','')
	 
select l.LocationId,
       subl.StorageUnitBatchId,
       l.Location,
       p.ProductCode,
       p.Product,
       sku.SKUCode,
       b.Batch,
       subl.ActualQuantity,
       subl.AllocatedQuantity,
       subl.ReservedQuantity
    from Location                    l (nolock)
    join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
    left
    join StorageUnitBatchLocation subl (nolock) on l.LocationId = subl.LocationId
    left
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    left
    join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    left
    join Product                     p (nolock) on su.ProductId           = p.ProductId
    left
    join SKU                       sku (nolock) on su.SKUId               = sku.SKUId
    left
    join Batch                       b (nolock) on sub.BatchId            = b.BatchId
   where a.StockOnHand = 1
     and a.WarehouseId = @WarehouseId
     and l.Location    = @Location
end
