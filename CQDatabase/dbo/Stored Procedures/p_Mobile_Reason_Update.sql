﻿ 
/*
/// <summary>
///   Procedure Name : p_Mobile_Reason_Update
///   Filename       : p_Mobile_Reason_Update.sql
///   Create By      : Grant Schultz
///   Date Created   : 12 Jun 2008
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
CREATE procedure p_Mobile_Reason_Update
(
@instructionId int,
@reasonId      int
)
 
as
begin
     set nocount on;
         
  declare @Error                        int,
          @Errormsg                     nvarchar(500),
          @GetDate                      datetime,
          @Result                       bit,
          @AdjustmentType               nvarchar(20),
          @ToWarehouseCode              nvarchar(50),
          @RecordType                   nvarchar(20),
          @CheckQuantity                float,
          @WarehouseCode                nvarchar(20),
          @InterfaceExportHeaderId      int,
          @ProductCode                  nvarchar(30),
          @Product                      nvarchar(50),
          @SkuCode                      nvarchar(50),
          @Batch                        nvarchar(50),
          @Quantity                     float
  
  
  select @GetDate = dbo.ufn_Getdate()
         
     set @Result = 1
         
  begin transaction
  
  Select @AdjustmentType=AdjustmentType,
         @ToWarehouseCode = ToWarehousecode,
         @RecordType = RecordType
    from Reason where ReasonId = @reasonId
         
  Select @CheckQuantity=CheckQuantity,
         @WarehouseCode = WarehouseCode,
         @ProductCode = ProductCode,
         @Product = Product,
         @SkuCode = SKUCode,
         @Batch = Batch,
         @Quantity = Quantity
    from viewInstruction i
         Join Warehouse w on w.ParentWarehouseId = i.WarehouseId
   where InstructionId = @instructionId
  
  exec @Error = p_Instruction_Update
  @InstructionId = @instructionId,
  @ReasonId      = @reasonId
  
  If @AdjustmentType is not null
     and @ToWarehouseCode is not null
     and @RecordType is not null
     and @CheckQuantity is null
  Begin
    Insert into InterfaceExportHeader (RecordType,RecordStatus,FromWarehouseCode,ToWarehouseCode,Remarks,Additional1,InsertDate)
    Select @RecordType,
           'W',
           @WarehouseCode,
           @ToWarehouseCode,
           'Stock Write off. CQ ref:' + RTRIM(ltrim( CONVERT(char(50),@InstructionId) )),
           @AdjustmentType,
           GETDATE()
    Select @InterfaceExportHeaderId = @@IDENTITY
           
    Insert into InterfaceExportDetail (InterfaceExportHeaderId,LineNumber,ProductCode,Product,SKUCode,Batch,Quantity,Additional3)
    Select @InterfaceExportHeaderId,
           1,
           @ProductCode,
           @Product,
           @SkuCode,
           @Batch,
           @Quantity,
           @ToWarehouseCode
           Update InterfaceExportHeader set RecordStatus = 'N' where InterfaceExportHeaderId = @InterfaceExportHeaderId
  End
  if @Error <> 0
  goto error
  
  commit transaction
  select @Result
  return
  
  error:
     set @Result = 0
  select @Result
  return
end
