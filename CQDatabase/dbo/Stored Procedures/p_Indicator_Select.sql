﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Indicator_Select
  ///   Filename       : p_Indicator_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:25
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Indicator table.
  /// </remarks>
  /// <param>
  ///   @IndicatorId int = null 
  /// </param>
  /// <returns>
  ///   Indicator.IndicatorId,
  ///   Indicator.Header,
  ///   Indicator.Indicator,
  ///   Indicator.IndicatorValue,
  ///   Indicator.IndicatorDisplay,
  ///   Indicator.ThresholdGreen,
  ///   Indicator.ThresholdYellow,
  ///   Indicator.ThresholdRed,
  ///   Indicator.ModifiedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Indicator_Select
(
 @IndicatorId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Indicator.IndicatorId
        ,Indicator.Header
        ,Indicator.Indicator
        ,Indicator.IndicatorValue
        ,Indicator.IndicatorDisplay
        ,Indicator.ThresholdGreen
        ,Indicator.ThresholdYellow
        ,Indicator.ThresholdRed
        ,Indicator.ModifiedDate
    from Indicator
   where isnull(Indicator.IndicatorId,'0')  = isnull(@IndicatorId, isnull(Indicator.IndicatorId,'0'))
  
end
