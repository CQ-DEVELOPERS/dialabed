﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMProduct_Update
  ///   Filename       : p_BOMProduct_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:13
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the BOMProduct table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null,
  ///   @LineNumber int = null,
  ///   @StorageUnitId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMProduct_Update
(
 @BOMLineId int = null,
 @LineNumber int = null,
 @StorageUnitId int = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
  if @BOMLineId = '-1'
    set @BOMLineId = null;
  
  if @LineNumber = '-1'
    set @LineNumber = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
	 declare @Error int
 
  update BOMProduct
     set Quantity = isnull(@Quantity, Quantity) 
   where BOMLineId = @BOMLineId
     and LineNumber = @LineNumber
     and StorageUnitId = @StorageUnitId
  
  select @Error = @@Error
  
  
  return @Error
  
end
