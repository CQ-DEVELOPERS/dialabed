﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Search
  ///   Filename       : p_Receipt_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 13:28:38
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Receipt table.
  /// </remarks>
  /// <param>
  ///   @ReceiptId int = null output,
  ///   @InboundDocumentId int = null,
  ///   @PriorityId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @ShippingAgentId int = null,
  ///   @VehicleId int = null,
  ///   @VehicleTypeId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Receipt.ReceiptId,
  ///   Receipt.InboundDocumentId,
  ///   InboundDocument.InboundDocumentId,
  ///   Receipt.PriorityId,
  ///   Priority.Priority,
  ///   Receipt.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Receipt.LocationId,
  ///   Location.Location,
  ///   Receipt.StatusId,
  ///   Status.Status,
  ///   Receipt.OperatorId,
  ///   Operator.Operator,
  ///   Receipt.DeliveryNoteNumber,
  ///   Receipt.DeliveryDate,
  ///   Receipt.SealNumber,
  ///   Receipt.VehicleRegistration,
  ///   Receipt.Remarks,
  ///   Receipt.CreditAdviceIndicator,
  ///   Receipt.Delivery,
  ///   Receipt.AllowPalletise,
  ///   Receipt.Interfaced,
  ///   Receipt.StagingLocationId,
  ///   Receipt.NumberOfLines,
  ///   Receipt.ReceivingCompleteDate,
  ///   Receipt.ParentReceiptId,
  ///   Receipt.GRN,
  ///   Receipt.PlannedDeliveryDate,
  ///   Receipt.ShippingAgentId,
  ///   ExternalCompany.ExternalCompany,
  ///   Receipt.ContainerNumber,
  ///   Receipt.AdditionalText1,
  ///   Receipt.AdditionalText2,
  ///   Receipt.BOE,
  ///   Receipt.Incoterms,
  ///   Receipt.ReceiptConfirmed,
  ///   Receipt.ReceivingStarted,
  ///   Receipt.VehicleId,
  ///   Vehicle.VehicleId,
  ///   Receipt.ParentIssueId,
  ///   Receipt.VehicleTypeId 
  ///   VehicleType.Id 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_Search
(
 @ReceiptId int = null output,
 @InboundDocumentId int = null,
 @PriorityId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @ShippingAgentId int = null,
 @VehicleId int = null,
 @VehicleTypeId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ShippingAgentId = '-1'
    set @ShippingAgentId = null;
  
  if @VehicleId = '-1'
    set @VehicleId = null;
  
  if @VehicleTypeId = '-1'
    set @VehicleTypeId = null;
  
 
  select
         Receipt.ReceiptId
        ,Receipt.InboundDocumentId
        ,Receipt.PriorityId
         ,PriorityPriorityId.Priority as 'Priority'
        ,Receipt.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Receipt.LocationId
         ,LocationLocationId.Location as 'Location'
        ,Receipt.StatusId
         ,StatusStatusId.Status as 'Status'
        ,Receipt.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,Receipt.DeliveryNoteNumber
        ,Receipt.DeliveryDate
        ,Receipt.SealNumber
        ,Receipt.VehicleRegistration
        ,Receipt.Remarks
        ,Receipt.CreditAdviceIndicator
        ,Receipt.Delivery
        ,Receipt.AllowPalletise
        ,Receipt.Interfaced
        ,Receipt.StagingLocationId
        ,Receipt.NumberOfLines
        ,Receipt.ReceivingCompleteDate
        ,Receipt.ParentReceiptId
        ,Receipt.GRN
        ,Receipt.PlannedDeliveryDate
        ,Receipt.ShippingAgentId
         ,ExternalCompanyShippingAgentId.ExternalCompany as 'ExternalCompanyShippingAgentId'
        ,Receipt.ContainerNumber
        ,Receipt.AdditionalText1
        ,Receipt.AdditionalText2
        ,Receipt.BOE
        ,Receipt.Incoterms
        ,Receipt.ReceiptConfirmed
        ,Receipt.ReceivingStarted
        ,Receipt.VehicleId
        ,Receipt.ParentIssueId
        ,Receipt.VehicleTypeId
    from Receipt
    left
    join InboundDocument InboundDocumentInboundDocumentId on InboundDocumentInboundDocumentId.InboundDocumentId = Receipt.InboundDocumentId
    left
    join Priority PriorityPriorityId on PriorityPriorityId.PriorityId = Receipt.PriorityId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Receipt.WarehouseId
    left
    join Location LocationLocationId on LocationLocationId.LocationId = Receipt.LocationId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = Receipt.StatusId
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = Receipt.OperatorId
    left
    join ExternalCompany ExternalCompanyShippingAgentId on ExternalCompanyShippingAgentId.ExternalCompanyId = Receipt.ShippingAgentId
    left
    join Vehicle VehicleVehicleId on VehicleVehicleId.VehicleId = Receipt.VehicleId
    left
    join VehicleType VehicleTypeVehicleTypeId on VehicleTypeVehicleTypeId.Id = Receipt.VehicleTypeId
   where isnull(Receipt.ReceiptId,'0')  = isnull(@ReceiptId, isnull(Receipt.ReceiptId,'0'))
     and isnull(Receipt.InboundDocumentId,'0')  = isnull(@InboundDocumentId, isnull(Receipt.InboundDocumentId,'0'))
     and isnull(Receipt.PriorityId,'0')  = isnull(@PriorityId, isnull(Receipt.PriorityId,'0'))
     and isnull(Receipt.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Receipt.WarehouseId,'0'))
     and isnull(Receipt.LocationId,'0')  = isnull(@LocationId, isnull(Receipt.LocationId,'0'))
     and isnull(Receipt.StatusId,'0')  = isnull(@StatusId, isnull(Receipt.StatusId,'0'))
     and isnull(Receipt.OperatorId,'0')  = isnull(@OperatorId, isnull(Receipt.OperatorId,'0'))
     and isnull(Receipt.ShippingAgentId,'0')  = isnull(@ShippingAgentId, isnull(Receipt.ShippingAgentId,'0'))
     and isnull(Receipt.VehicleId,'0')  = isnull(@VehicleId, isnull(Receipt.VehicleId,'0'))
     and isnull(Receipt.VehicleTypeId,'0')  = isnull(@VehicleTypeId, isnull(Receipt.VehicleTypeId,'0'))
  order by DeliveryNoteNumber
  
end
