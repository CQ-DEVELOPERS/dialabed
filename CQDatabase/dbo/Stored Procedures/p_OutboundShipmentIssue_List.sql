﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssue_List
  ///   Filename       : p_OutboundShipmentIssue_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jun 2013 12:36:35
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundShipmentIssue table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OutboundShipmentIssue.OutboundShipmentId,
  ///   OutboundShipmentIssue.IssueId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssue_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as OutboundShipmentId
        ,null as 'OutboundShipmentIssue'
        ,'-1' as IssueId
        ,null as 'OutboundShipmentIssue'
  union
  select
         OutboundShipmentIssue.OutboundShipmentId
        ,OutboundShipmentIssue.OutboundShipmentId as 'OutboundShipmentIssue'
        ,OutboundShipmentIssue.IssueId
        ,OutboundShipmentIssue.IssueId as 'OutboundShipmentIssue'
    from OutboundShipmentIssue
  
end
