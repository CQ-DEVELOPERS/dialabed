﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Active
  ///   Filename       : p_Operator_Active.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Active
(
 @OperatorId        int,
 @SiteType          nvarchar(20)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 begin transaction
  
  exec @Error = p_Operator_Update
   @OperatorId   = @OperatorId,
   @SiteType     = @SiteType,
   @LastActivity = @GetDate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Operator_Active'
    rollback transaction
    return @Error
end
