﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportOrderNumbers_Search
  ///   Filename       : p_InterfaceImportOrderNumbers_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:51
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportOrderNumbers table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportOrderNumbers.PrimaryKey,
  ///   InterfaceImportOrderNumbers.RecordStatus,
  ///   InterfaceImportOrderNumbers.OrderNumber,
  ///   InterfaceImportOrderNumbers.OrderDate,
  ///   InterfaceImportOrderNumbers.Status,
  ///   InterfaceImportOrderNumbers.OutboundDocument,
  ///   InterfaceImportOrderNumbers.InboundDocument,
  ///   InterfaceImportOrderNumbers.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportOrderNumbers_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportOrderNumbers.PrimaryKey
        ,InterfaceImportOrderNumbers.RecordStatus
        ,InterfaceImportOrderNumbers.OrderNumber
        ,InterfaceImportOrderNumbers.OrderDate
        ,InterfaceImportOrderNumbers.Status
        ,InterfaceImportOrderNumbers.OutboundDocument
        ,InterfaceImportOrderNumbers.InboundDocument
        ,InterfaceImportOrderNumbers.ProcessedDate
    from InterfaceImportOrderNumbers
  
end
