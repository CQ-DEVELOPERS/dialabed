﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_TO
  ///   Filename       : p_FamousBrands_Import_TO.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_FamousBrands_Import_TO
as
begin
          -- InterfaceImportSOHeader
  declare @InterfaceImportSOHeaderId    int,
          @OrderNumber                  varchar(30),
          @CustomerCode                 varchar(30),
          @DeliveryDate                 datetime,
          @PreSchedule                  varchar(255),
          @AmountOfInvoice              varchar(255),
          @COD                          varchar(255),
          @PickUpCustomer               varchar(255),
          @LoadPriority                 varchar(255),
          @JobWindowStart               varchar(255),
          @JobWindowEnd                 varchar(255),
          @JobDuration                  varchar(255),
          @HoldStatus                   varchar(255),
          @Remarks                      varchar(255),
          @Instructions                 varchar(255),
          @NumberOfLines                int,
          -- InterfaceImportSODetail
          @LineNumber                   int,
          @ProductCode                  varchar(30),
          @Quantity                     numeric(13,3),
          @WhsCode                      varchar(255),
          @PickUp                       varchar(255),
          -- Internal variables
          @ExternalCompanyId            int ,
          @DropSequence                 int,
          @Weight                       numeric(13,3),
          @OutboundDocumentTypeCode     varchar(10),
          @PriorityId                   int,
          @ErrorMsg                     varchar(100),
          @Error                        int,
          @OutboundDocumentTypeId       int,
          @OutboundDocumentId           int,
          @StatusId                     int,
          @GetDate                      datetime,
          @ProductId                    int,
          @StorageUnitId                int,
          @StorageUnitBatchId           int,
          @OutboundLineId               int,
          @Exception                    varchar(255),
          @Delete                       bit,
          @OutboundShipmentId           int,
          @IssueId                      int,
          @WarehouseId                  int,
          @RecordType                   varchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  update d
     set InterfaceImportSOHeaderId = (select max(InterfaceImportSOHeaderId)
                                        from InterfaceImportSOHeader h
                                       where d.ForeignKey = h.PrimaryKey)
    from InterfaceImportSODetail d
   where InterfaceImportSOHeaderId is null
  
  update InterfaceImportSOHeader
     set RecordType = 'IBT'
   where RecordType is null
     and ToWarehouseCode is not null
  
  -- Reprocess Order which did not process properly
  update InterfaceImportSOHeader
     set ProcessedDate = null
   where RecordStatus     = 'N'
     and ToWarehouseCode is not null
     and ProcessedDate   <= dateadd(mi, -10, @Getdate)
  
  update h
     set ProcessedDate = @Getdate
    from InterfaceImportSOHeader h
   where exists(select 1 from InterfaceImportSODetail d where h.InterfaceImportSOHeaderId = d.InterfaceImportSOHeaderId)
     -- and not exists(select 1 from OutboundDocument od where h.PrimaryKey = od.OrderNumber)
     and RecordStatus in ('N','U')
     and ProcessedDate is null
     and ToWarehouseCode is not null
  
  declare header_cursor cursor for
   select InterfaceImportSOHeaderId,
          PrimaryKey,   -- OrderNumber
          ToWarehouseCode, -- CustomerCode
          convert(datetime, DeliveryDate, 103), -- DeliveryDate
          Additional2,  -- PreSchedule
          Additional3,  -- AmountOfInvoice
          Additional4,  -- COD
          Additional5,  -- PickUpCustomer
          Additional6,  -- LoadPriority
          Additional7,  -- JobWindowStart
          Additional8,  -- JobWindowEnd
          Additional9,  -- JobDuration
          Additional10, -- HoldStatus
          Remarks,      -- Remarks
          Additional1,  -- Instructions
          NumberOfLines,-- NoOfLines
          RecordType    -- Collection
     from InterfaceImportSOHeader h
    where ProcessedDate = @Getdate
   order by	OrderNumber
  
  open header_cursor
  
  fetch header_cursor
   into @InterfaceImportSOHeaderId,
        @OrderNumber,
        @CustomerCode,
        @DeliveryDate,
        @PreSchedule,
        @AmountOfInvoice,
        @COD,
        @PickUpCustomer,
        @LoadPriority,
        @JobWindowStart,
        @JobWindowEnd,
        @JobDuration,
        @HoldStatus,
        @Remarks,
        @Instructions,
        @NumberOfLines,
        @RecordType
  
  while (@@fetch_status = 0)
  begin
    begin transaction xml_import
    
    set rowcount 1
    update InterfaceImportSOHeader
       set RecordStatus = 'C',
           OrderNumber = @OrderNumber
     where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
    set rowcount 0
    
    exec @Error = p_Interface_Outbound_Order_Delete
     @OrderNumber = @OrderNumber
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Interface_Outbound_Order_Delete'
      goto error_header
    end
    
    if @CustomerCode is null
      set @CustomerCode = 'None'
    
    set @ExternalCompanyId = null
    
    select @ExternalCompanyId = ExternalCompanyId
      from ExternalCompany
     where ExternalCompanyCode = @CustomerCode
    
    if @ExternalCompanyId is null
    begin
      exec @Error = p_ExternalCompany_Insert
       @ExternalCompanyId         = @ExternalCompanyId output,
       @ExternalCompanyTypeId     = 2,
       @ExternalCompany           = @CustomerCode,
       @ExternalCompanyCode       = @CustomerCode
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
        goto error_header
      end
      
    end
    
    set @OutboundDocumentTypeCode = null
    
--    select @OutboundDocumentTypeCode = OutboundDocumentTypeCode
--      from OutboundDocumentType (nolock)
--     where OutboundDocumentTypeCode = @RecordType
    
    if @OutboundDocumentTypeCode is null
      set @OutboundDocumentTypeCode = 'IBT'
    
    select @OutboundDocumentTypeId = OutboundDocumentTypeId,
           @PriorityId             = PriorityId
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = @OutboundDocumentTypeCode
    
    update InterfaceImportSOHeader
       set RecordType = @OutboundDocumentTypeCode
     where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
    
    if @@error != 0
    begin
      select @ErrorMsg = 'Error updating InterfaceImportSOHeader RecordType'
      goto error_header
    end
    
    select @OutboundDocumentTypeId = OutboundDocumentTypeId,
           @PriorityId             = PriorityId
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = @OutboundDocumentTypeCode
    
    select @StatusId = dbo.ufn_StatusId('OD','I')
    
    if not exists (select 1
                     from OutboundDocument
                    where OrderNumber = @OrderNumber
                      and OutboundDocumentTypeId = @OutboundDocumentTypeId)
    begin
      exec @Error = p_OutboundDocument_Insert
       @OutboundDocumentId     = @OutboundDocumentId output,
       @OrderNumber            = @OrderNumber,
       @OutboundDocumentTypeId = @OutboundDocumentTypeId,
       @ExternalCompanyId      = @ExternalCompanyId,
--       @PriorityId             = @PriorityId,
       @WarehouseId            = 1,
       @StatusId               = @StatusId,
       @DeliveryDate           = @DeliveryDate,
       @CreateDate             = @GetDate,
       @ModifiedDate           = null
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'
        goto error_header
      end
    end
    
    declare detail_cursor cursor for
     select LineNumber,  -- LineNumber
            ProductCode, -- ProductCode
            Additional1, -- Location
            Quantity,    -- Quantity
            Additional2  -- PickUp
       from InterfaceImportSODetail
      where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
    
    open detail_cursor
    
    fetch detail_cursor
     into @LineNumber,
          @ProductCode,
          @WhsCode,
          @Quantity,
          @PickUp
    
    while (@@fetch_status = 0)
    begin
      set @WarehouseId = null
      
      select @WarehouseId = WarehouseId
        from Warehouse
       where WarehouseCode = @WhsCode
      
      if @WarehouseId is null
        select @WarehouseId = WarehouseId
          from WarehouseCodeReference
         where WarehouseCode = @WhsCode
           and DownloadType = 'SALE'
      
      if @WarehouseId is null
        set @WarehouseId = 1
      
      exec @Error = p_OutboundDocument_Update
       @OutboundDocumentId     = @OutboundDocumentId,
       @WarehouseId            = @WarehouseId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundDocument_Update'
        goto error_header
      end
      
      set @StorageUnitBatchId = null
      set @StorageUnitId = null
      
      exec @Error = p_interface_xml_Product_Insert
       @ProductCode        = @ProductCode,
       @Product            = @ProductCode,
       @WarehouseId        = @WarehouseId,
       @StorageUnitId      = @StorageUnitId output,
       @StorageUnitBatchId = @StorageUnitBatchId output
      
      if @Error != 0 or @StorageUnitBatchId is null
      begin
        select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
        goto error_detail
      end
      
      if exists (select 1
                   from OutboundLine
                  where OutboundDocumentId = @OutboundDocumentId
                    and LineNumber = @LineNumber)
      begin
        select @Exception = 'Attempt to insert duplicate line '
                          + @ProductCode
                          + '  Order No:'
                          + @OrderNumber
        exec @Error = p_Exception_Insert
         @Exception     = @Exception,
         @ExceptionCode = 'INTERROR02',
         @CreateDate    = @GetDate
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Exception_Insert'
          goto error_detail
        end
      end
      else
      begin
        exec @Error = p_OutboundLine_Insert
         @OutboundLineId     = @OutboundLineId output,
         @OutboundDocumentId = @OutboundDocumentId,
         @StorageUnitId      = @StorageUnitId,
         @StatusId           = @StatusId,
         @LineNumber         = @LineNumber,
         @Quantity           = @Quantity,
         --@Weight             = @di_weight * @Quatity,
         @BatchId            = null
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_OutboundLine_Insert'
          goto error_detail
        end
      end
      
      fetch detail_cursor
       into @LineNumber,
            @ProductCode,
            @WhsCode,
            @Quantity,
            @PickUp
    end
    close detail_cursor
    deallocate detail_cursor
    
    exec @Error = p_Despatch_Create_Issue
     @OutboundDocumentId = @OutboundDocumentId,
     @OperatorId         = null,
     @Remarks            = @Remarks
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
      goto error_detail
    end
    
    select @IssueId = IssueId
      from Issue (nolock)
     where OutboundDocumentId = @OutboundDocumentId
    
    exec @Error = p_Outbound_Auto_Load
     @OutboundShipmentId = @OutboundShipmentId output,
     @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
      goto error_detail
    end
    
    exec @Error = p_Outbound_Auto_Release
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
      goto error_detail
    end
    
    error_detail:
    error_header:
    
    if @Error = 0
    begin
      --select @OrderNumber as 'OrderNumber', 'Commit 1', @@trancount as '@@trancount'
      if @@trancount > 0
        commit transaction xml_import
      --select @OrderNumber as 'OrderNumber', 'Commit 2', @@trancount as '@@trancount'
    end
    else
    begin
      --select @OrderNumber as 'OrderNumber', 'Error 1', @@trancount as '@@trancount'
      if @@trancount > 0
        rollback transaction xml_import
      --select @OrderNumber as 'OrderNumber', 'Error 2', @@trancount as '@@trancount'
      update InterfaceImportSOHeader
         set RecordStatus = 'E',
             RecordType = isnull(@OutboundDocumentTypeCode, RecordType)
       where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
    end
    
    fetch header_cursor
     into @InterfaceImportSOHeaderId,
          @OrderNumber,
          @CustomerCode,
          @DeliveryDate,
          @PreSchedule,
          @AmountOfInvoice,
          @COD,
          @PickUpCustomer,
          @LoadPriority,
          @JobWindowStart,
          @JobWindowEnd,
          @JobDuration,
          @HoldStatus,
          @Remarks,
          @Instructions,
          @NumberOfLines,
          @RecordType
  end
  
  close header_cursor
  deallocate header_cursor
  
  return
end
