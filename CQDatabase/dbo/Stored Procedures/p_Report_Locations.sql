﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Locations
  ///   Filename       : p_Report_Locations.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Locations
(
	@WarehouseId      int,
	@StartLocation		nvarchar(15),
	@EndLocation		nvarchar(15)
)
 
as
begin
	 set nocount on;
  
  select Location,
		dbo.ConvertTo128(Location) as LocationPrint
		
    from Location      
where Location >= @StartLocation and Location <= @EndLocation
Order by Location
      
  
end
