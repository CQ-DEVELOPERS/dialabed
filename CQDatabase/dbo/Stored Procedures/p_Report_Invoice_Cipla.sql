﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Invoice_Cipla
  ///   Filename       : p_Report_Invoice_Cipla.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Invoice_Cipla
(
 @OutboundShipmentId int = null,
 @IssueId int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @InterfaceImportIPHeaderId int
	 
	 if @OutboundShipmentId = -1
	   set @OutboundShipmentId = null
	 
	 if @IssueId = -1
	   set @IssueId = null
	 
	 if @OutboundShipmentId is null and @IssueId is null -- Auto invoice
	 begin
    select top 1 @InterfaceImportIPHeaderId = min(h.InterfaceImportIPHeaderId)
	     from InterfaceImportIPHeader h (nolock)
		    join InterfaceImportIPDetail d (nolock) on h.InterfaceImportIPHeaderId = d.InterfaceImportIPHeaderId	
	    where isnull(printed,0) = 0 
	 end
	 else
	 begin
	   select @InterfaceImportIPHeaderId = max(h.InterfaceImportIPHeaderId)
	     from InterfaceImportIPHeader h (nolock)
      join InterfaceImportIPDetail d (nolock) on h.InterfaceImportIPHeaderId = d.InterfaceImportIPHeaderId
     where isnull(h.OutboundShipmentId,-1) = isnull(@OutboundShipmentId, isnull(h.OutboundShipmentId,-1))
       and isnull(h.IssueId,-1) = isnull(@IssueId, isnull(h.IssueId,-1))
	 end
	 
	 update InterfaceImportIPHeader
	    set Printed = isnull(Printed,0) + 1
	   from InterfaceImportIPHeader
	  where InterfaceImportIPHeaderId = @InterfaceImportIPHeaderId
	 
  select h.PrimaryKey as 'InvoiceNumber',
         h.InvoiceDate,
         h.CustomerAddress1,
         h.CustomerAddress2,
         h.CustomerAddress3,
         h.CustomerAddress4,
         null as CustomerAddress5,
         null as CustomerAddress6,
         h.DeliveryAddress1,
         h.DeliveryAddress2,
         h.DeliveryAddress3,
         h.DeliveryAddress4,
         null as DeliveryAddress5,
         null as DeliveryAddress6,
         h.OrderNumber,
         h.CustomerName,
         h.CustomerCode as 'AccountNumber',
         h.ReferenceNumber,
         Case when h.ChargeTax = 1 then 'True' Else 'False' end as ChargeTax,
         h.TaxReference,
         h.SalesCode,
         d.ProductCode,
         d.Product,
         d.Batch,
         d.Quantity,
         d.Excluding,
         d.Including,
         d.NettValue,
         h.TotalNettValue,
         h.LogisticsDataFee,
         h.NettExcLogDataFee,
         h.Tax,
         h.TotalDue,
         b.ExpiryDate
    from InterfaceImportIPHeader h (nolock)
    join InterfaceImportIPDetail d (nolock) on h.InterfaceImportIPHeaderId = d.InterfaceImportIPHeaderId
    join Batch                   b (nolock) on d.Batch		      = b.Batch
   where h.InterfaceImportIPHeaderId = @InterfaceImportIPHeaderId
end
