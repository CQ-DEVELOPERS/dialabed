﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipmentReceipt_Parameter
  ///   Filename       : p_InboundShipmentReceipt_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:23
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundShipmentReceipt table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InboundShipmentReceipt.InboundShipmentId,
  ///   InboundShipmentReceipt.ReceiptId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipmentReceipt_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InboundShipmentId
        ,null as 'InboundShipmentReceipt'
        ,null as ReceiptId
        ,null as 'InboundShipmentReceipt'
  union
  select
         InboundShipmentReceipt.InboundShipmentId
        ,InboundShipmentReceipt.InboundShipmentId as 'InboundShipmentReceipt'
        ,InboundShipmentReceipt.ReceiptId
        ,InboundShipmentReceipt.ReceiptId as 'InboundShipmentReceipt'
    from InboundShipmentReceipt
  
end
