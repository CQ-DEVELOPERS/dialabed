﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Reason_Update
  ///   Filename       : p_Reason_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:22
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Reason table.
  /// </remarks>
  /// <param>
  ///   @ReasonId int = null,
  ///   @Reason nvarchar(100) = null,
  ///   @ReasonCode nvarchar(20) = null,
  ///   @AdjustmentType varchar(20) = null,
  ///   @ToWarehouseCode varchar(10) = null,
  ///   @RecordType varchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Reason_Update
(
 @ReasonId int = null,
 @Reason nvarchar(100) = null,
 @ReasonCode nvarchar(20) = null,
 @AdjustmentType varchar(20) = null,
 @ToWarehouseCode varchar(10) = null,
 @RecordType varchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @Reason = '-1'
    set @Reason = null;
  
  if @ReasonCode = '-1'
    set @ReasonCode = null;
  
	 declare @Error int
 
  update Reason
     set Reason = isnull(@Reason, Reason),
         ReasonCode = isnull(@ReasonCode, ReasonCode),
         AdjustmentType = isnull(@AdjustmentType, AdjustmentType),
         ToWarehouseCode = isnull(@ToWarehouseCode, ToWarehouseCode),
         RecordType = isnull(@RecordType, RecordType) 
   where ReasonId = @ReasonId
  
  select @Error = @@Error
  
  
  return @Error
  
end
