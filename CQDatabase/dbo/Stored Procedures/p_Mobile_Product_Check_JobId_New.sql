﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_JobId_New
  ///   Filename       : p_Mobile_Product_Check_JobId_New.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_JobId_New
(
 @warehouseId     int,
 @operatorId      int,
 @jobId           int,
 @newJobId        int output,
 @Weight          numeric(13,6)
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Mobile_Product_Check_JobId_New',
          @GetDate           datetime,
          @Transaction       bit = 1,
          @NewBox            int,
          @ReferenceNumber   nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  --update Job
  --   set Weight = @Weight
  -- where JobId = @jobId
  
  --select @Error = @@ERROR
  
  --if @Error != 0
  --  goto Error
  
  set @NewBox=NULL
  
  exec p_Label_Reference @Value=@NewBox output,@WarehouseId=1
  
  set @ReferenceNumber = 'NB:' + CONVERT(nvarchar(10), @NewBox) 
  
  insert Job
        (PriorityId,
         OperatorId,
         StatusId,
         WarehouseId,
         ReferenceNumber,
         CheckedBy,
         CheckedDate,
         Weight)
  select PriorityId,
         OperatorId,
         dbo.ufn_StatusId('IS','CD'),
         WarehouseId,
         @ReferenceNumber,
         @operatorId,
         @GetDate,
         @Weight
    from Job
   where JobId = @JobId
  
  select @Error = @@Error, @NewJobId = scope_identity()
  
  if @Error <> 0
    goto error
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
