﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOH_Insert
  ///   Filename       : p_InterfaceImportSOH_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:10
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportSOH table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHId int = null output,
  ///   @Location nvarchar(20) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @UnitPrice numeric(13,3) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @RecordType nvarchar(40) = null,
  ///   @InsertDate datetime = null,
  ///   @SKUCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportSOH.InterfaceImportSOHId,
  ///   InterfaceImportSOH.Location,
  ///   InterfaceImportSOH.ProductCode,
  ///   InterfaceImportSOH.Batch,
  ///   InterfaceImportSOH.Quantity,
  ///   InterfaceImportSOH.UnitPrice,
  ///   InterfaceImportSOH.ProcessedDate,
  ///   InterfaceImportSOH.RecordStatus,
  ///   InterfaceImportSOH.RecordType,
  ///   InterfaceImportSOH.InsertDate,
  ///   InterfaceImportSOH.SKUCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOH_Insert
(
 @InterfaceImportSOHId int = null output,
 @Location nvarchar(20) = null,
 @ProductCode nvarchar(60) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @UnitPrice numeric(13,3) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @RecordType nvarchar(40) = null,
 @InsertDate datetime = null,
 @SKUCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportSOHId = '-1'
    set @InterfaceImportSOHId = null;
  
	 declare @Error int
 
  insert InterfaceImportSOH
        (Location,
         ProductCode,
         Batch,
         Quantity,
         UnitPrice,
         ProcessedDate,
         RecordStatus,
         RecordType,
         InsertDate,
         SKUCode)
  select @Location,
         @ProductCode,
         @Batch,
         @Quantity,
         @UnitPrice,
         @ProcessedDate,
         @RecordStatus,
         @RecordType,
         isnull(@InsertDate, getdate()),
         @SKUCode 
  
  select @Error = @@Error, @InterfaceImportSOHId = scope_identity()
  
  
  return @Error
  
end
