﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Despatch_Manual_Palletisation_IssueLine
  ///   Filename       : p_Despatch_Manual_Palletisation_IssueLine.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Despatch_Manual_Palletisation_IssueLine
(
 @IssueLineId    int,
 @Quantity       float,
 @PickLocationId int,
 @Allocated      bit = 0 output
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @LineQuantity       float,
          @IssueId            int,
          @WarehouseId        int,
          @StoreLocationId     int,
          @StorageUnitBatchId int,
          @AcceptedQuantity   numeric(10),
          @PalletQuantity     numeric(10),
          @PalletCount        int,
          @RemainingQuantity  float,
          @StatusId           int,
          @NumberOfLines      int,
          @PalletisedQuantity float
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Despatch_Manual_Palletisation_IssueLine'
  set @NumberOfLines = 1
  set @Allocated = 0
  
  exec @Error = p_Palletise_Reverse @IssueLineId = @IssueLineId
  
  if @error <> 0
  begin
    set @ErrorMsg = 'Re-palletise failed'
    goto error
  end
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'W'
     and Type       = 'IS';
  
  select @IssueId            = i.IssueId,
         @WarehouseId        = i.WarehouseId,
         @StoreLocationId    = i.LocationId,
         @StorageUnitBatchId = il.StorageUnitBatchId,
         @AcceptedQuantity   = il.Quantity
    from IssueLine il (nolock)
    join Issue     i  (nolock) on il.IssueId = i.IssueId
   where il.IssueLineId = @IssueLineId
     and il.StatusId   = @StatusId
  
  select top 1 @StorageUnitBatchId = subl.StorageUnitBatchId
    from StorageUnitBatchLocation subl
    join StorageUnitBatch sub1 on subl.StorageUnitBatchId = sub1.StorageUnitBatchId
    join StorageUnitBatch sub2 on sub1.StorageUnitId      = sub2.StorageUnitId
   where LocationId = @PickLocationId
  order by subl.ActualQuantity - ReservedQuantity desc
  
  select @PalletisedQuantity = sum(Quantity)
    from Instruction (nolock)
   where IssueLineId = @IssueLineId
  
  if (@PalletisedQuantity + @Quantity) > @AcceptedQuantity
  begin
    set @Quantity = @AcceptedQuantity - @PalletisedQuantity
    if (@PalletisedQuantity + @Quantity) > @AcceptedQuantity or @Quantity = 0
    begin
      set @Allocated = 1
      select @Allocated
      return;
    end
  end
  
  if (select sum(ActualQuantity - ReservedQuantity)
        from StorageUnitBatchLocation
       where StorageUnitBatchId = @StorageUnitBatchId
         and LocationId         = @PickLocationId) < @Quantity
  begin
    begin transaction
    set @Error = -1
    set @Errormsg = 'There is not enough stock in the Location (p_Despatch_Manual_Palletisation_IssueLine).'
    goto error
  end
  
  set @AcceptedQuantity = @Quantity
  
  if @StorageUnitBatchId is null
    return -1
  
  select top 1 @PalletQuantity = p.Quantity
    from StorageUnitBatch sub (nolock)
    join Pack             p   (nolock) on sub.StorageUnitId = p.StorageUnitId
    join PackType         pt  (nolock) on p.PackTypeId      = pt.PackTypeId
   where StorageUnitBatchId = @StorageUnitBatchId
  order by InboundSequence
  
  begin transaction
  
--  if (@AcceptedQuantity / @PalletQuantity) > @NumberOfLines
--  begin
--    set @Error = 100
--    set @Errormsg = 'p_Despatch_Manual_Palletisation_IssueLine - Cannot put more than a full pallet quantity onto single line'
--    goto error
--  end
  
  if @AcceptedQuantity < @NumberOfLines 
  begin
    set @Error = 200
    set @Errormsg = 'p_Despatch_Manual_Palletisation_IssueLine - Cannot put a Quantity of ' + convert(nvarchar(10), @AcceptedQuantity) + ' on ' + convert(nvarchar(10), @NumberOfLines) + ' lines'
    goto error
  end
  
  if @AcceptedQuantity > @PalletQuantity
  begin
    set @PalletCount = floor(@AcceptedQuantity / @PalletQuantity)
    set @RemainingQuantity = @AcceptedQuantity - (@PalletQuantity * @PalletCount)
    
    if @PalletCount > 0
    begin
      while @PalletCount > 0
      begin
        set @PalletCount = @PalletCount - 1
        set @NumberOfLines = @NumberOfLines - 1
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @InstructionTypeCode = 'S', -- Store
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @StoreLocationId     = @StoreLocationId,
         @PickLocationId      = @PickLocationId,
         @Quantity            = @PalletQuantity,
         @IssueLineId         = @IssueLineId
        
        if @error <> 0
          goto error
      end
    end
  end
  
  if @RemainingQuantity is null
  begin
    set @LineQuantity      = floor(@AcceptedQuantity / @NumberOfLines)
    set @RemainingQuantity = @AcceptedQuantity - (floor(@AcceptedQuantity / @NumberOfLines) * @NumberOfLines)
  end
  else
  begin
    set @LineQuantity = @RemainingQuantity / @NumberOfLines
    set @NumberOfLines = @NumberOfLines - 1
  end
  
  while @NumberOfLines > 0
  begin
    set @NumberOfLines = @NumberOfLines -1
    
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = 'S', -- Store
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @StoreLocationId      = @StoreLocationId,
     @PickLocationId     = @PickLocationId,
     @Quantity            = @LineQuantity,
     @IssueLineId         = @IssueLineId
    
    if @error <> 0
      goto error
  end
  
  if @RemainingQuantity > 0
  begin
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = 'S', -- Store
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @StoreLocationId      = @StoreLocationId,
     @PickLocationId     = @PickLocationId,
     @Quantity            = @RemainingQuantity,
     @IssueLineId         = @IssueLineId

    if @error <> 0
      goto error
  end
  
  select @AcceptedQuantity = Quantity
    from IssueLine (nolock)
   where IssueLineId = @IssueLineId
  
  select @PalletisedQuantity = sum(Quantity)
    from Instruction (nolock)
   where IssueLineId = @IssueLineId
  --select @PalletisedQuantity, @AcceptedQuantity
  if @PalletisedQuantity <= @AcceptedQuantity
  begin
    set @Allocated = 1
    
    select @StatusId = StatusId
      from Status
     where StatusCode = 'P' -- Palletised
       and Type       = 'IS'

    exec @Error = p_IssueLine_Update
     @IssueLineId = @IssueLineId,
     @StatusId    = @StatusId
    
    if @error <> 0
      goto error
    
    exec @Error = p_Issue_Update_Status
     @IssueId = @IssueId,
     @StatusId  = @StatusId
    
    if @error <> 0
      goto error
  end
  
  rollback transaction
  return
  
  error:
    raiserror 900000 @ErrorMsg
    rollback transaction
    return @Error
end
