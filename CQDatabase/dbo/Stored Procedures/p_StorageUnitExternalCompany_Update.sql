﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitExternalCompany_Update
  ///   Filename       : p_StorageUnitExternalCompany_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:57
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StorageUnitExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @AllocationCategory numeric(13,3) = null,
  ///   @MinimumShelfLife numeric(13,3) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @DefaultQC bit = null,
  ///   @SendToQC bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitExternalCompany_Update
(
 @StorageUnitId int = null,
 @ExternalCompanyId int = null,
 @AllocationCategory numeric(13,3) = null,
 @MinimumShelfLife numeric(13,3) = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(20) = null,
 @DefaultQC bit = null,
 @SendToQC bit = null 
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
	 declare @Error int
 
  update StorageUnitExternalCompany
     set AllocationCategory = isnull(@AllocationCategory, AllocationCategory),
         MinimumShelfLife = isnull(@MinimumShelfLife, MinimumShelfLife),
         ProductCode = isnull(@ProductCode, ProductCode),
         SKUCode = isnull(@SKUCode, SKUCode),
         DefaultQC = isnull(@DefaultQC, DefaultQC),
         SendToQC = isnull(@SendToQC, SendToQC) 
   where StorageUnitId = @StorageUnitId
     and ExternalCompanyId = @ExternalCompanyId
  
  select @Error = @@Error
  
  
  return @Error
  
end
