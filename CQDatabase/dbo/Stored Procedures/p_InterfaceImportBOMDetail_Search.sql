﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportBOMDetail_Search
  ///   Filename       : p_InterfaceImportBOMDetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:22
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportBOMDetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportBOMDetail.InterfaceImportBOMId,
  ///   InterfaceImportBOMDetail.ForeignKey,
  ///   InterfaceImportBOMDetail.CompIndex,
  ///   InterfaceImportBOMDetail.CompProductCode,
  ///   InterfaceImportBOMDetail.CompProduct,
  ///   InterfaceImportBOMDetail.CompQuantity,
  ///   InterfaceImportBOMDetail.HostId,
  ///   InterfaceImportBOMDetail.InsertDate,
  ///   InterfaceImportBOMDetail.Additional1,
  ///   InterfaceImportBOMDetail.Additional2,
  ///   InterfaceImportBOMDetail.Additional3,
  ///   InterfaceImportBOMDetail.Additional4,
  ///   InterfaceImportBOMDetail.Additional5,
  ///   InterfaceImportBOMDetail.Additional6,
  ///   InterfaceImportBOMDetail.Additional7,
  ///   InterfaceImportBOMDetail.Additional8,
  ///   InterfaceImportBOMDetail.Additional9 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportBOMDetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportBOMDetail.InterfaceImportBOMId
        ,InterfaceImportBOMDetail.ForeignKey
        ,InterfaceImportBOMDetail.CompIndex
        ,InterfaceImportBOMDetail.CompProductCode
        ,InterfaceImportBOMDetail.CompProduct
        ,InterfaceImportBOMDetail.CompQuantity
        ,InterfaceImportBOMDetail.HostId
        ,InterfaceImportBOMDetail.InsertDate
        ,InterfaceImportBOMDetail.Additional1
        ,InterfaceImportBOMDetail.Additional2
        ,InterfaceImportBOMDetail.Additional3
        ,InterfaceImportBOMDetail.Additional4
        ,InterfaceImportBOMDetail.Additional5
        ,InterfaceImportBOMDetail.Additional6
        ,InterfaceImportBOMDetail.Additional7
        ,InterfaceImportBOMDetail.Additional8
        ,InterfaceImportBOMDetail.Additional9
    from InterfaceImportBOMDetail
  
end
