﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Question_Update
  ///   Filename       : p_Question_Update.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Question_Update
(
	@QuestionaireId		int,
	@QuestionId			int,
	@Category			nvarchar(50) = null,
	@Code				nvarchar(50) = null,
	@Sequence			int = null,
	@Active				int = null,
	@QuestionText		nvarchar(255) = null,
	@QuestionType		nvarchar(50) = null 
)
as
begin
	 set nocount on;

update Questions set Category = Isnull(@Category,Category),
					Code = Isnull(@Code,Code),
					Sequence =Isnull(@Sequence,Sequence),
					Active = Isnull(@Active,Active),
					QuestionText = Isnull(@QuestionText,QuestionText),
					Questiontype= Isnull(@QuestionType,QuestionType)
where QuestionaireId = @QuestionaireId and QuestionId = @QuestionId

End
