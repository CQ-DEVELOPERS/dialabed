﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorQA_Update
  ///   Filename       : p_OperatorQA_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:28
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorQA table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @EndDate datetime = null,
  ///   @JobId int = null,
  ///   @QA bit = null,
  ///   @InstructionTypeId int = null,
  ///   @ActivityStatus char(1) = null,
  ///   @PickingRate int = null,
  ///   @Units int = null,
  ///   @Weight float = null,
  ///   @Instructions int = null,
  ///   @Orders int = null,
  ///   @OrderLines int = null,
  ///   @Jobs int = null,
  ///   @ActiveTime int = null,
  ///   @DwellTime int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorQA_Update
(
 @OperatorId int = null,
 @EndDate datetime = null,
 @JobId int = null,
 @QA bit = null,
 @InstructionTypeId int = null,
 @ActivityStatus char(1) = null,
 @PickingRate int = null,
 @Units int = null,
 @Weight float = null,
 @Instructions int = null,
 @Orders int = null,
 @OrderLines int = null,
 @Jobs int = null,
 @ActiveTime int = null,
 @DwellTime int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update OperatorQA
     set OperatorId = isnull(@OperatorId, OperatorId),
         EndDate = isnull(@EndDate, EndDate),
         JobId = isnull(@JobId, JobId),
         QA = isnull(@QA, QA),
         InstructionTypeId = isnull(@InstructionTypeId, InstructionTypeId),
         ActivityStatus = isnull(@ActivityStatus, ActivityStatus),
         PickingRate = isnull(@PickingRate, PickingRate),
         Units = isnull(@Units, Units),
         Weight = isnull(@Weight, Weight),
         Instructions = isnull(@Instructions, Instructions),
         Orders = isnull(@Orders, Orders),
         OrderLines = isnull(@OrderLines, OrderLines),
         Jobs = isnull(@Jobs, Jobs),
         ActiveTime = isnull(@ActiveTime, ActiveTime),
         DwellTime = isnull(@DwellTime, DwellTime) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
