﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Create_Pallet
  ///   Filename       : p_Mobile_Create_Pallet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   @type       int,
  ///   @operatorId int,
  ///   @palletId   int,
  ///   @barcode    nvarchar(50) = null,
  ///   @batch      nvarchar(50) = null,
  ///   @quantity   float         = null
  /// </param>
  /// <returns>
  ///   Result    = 2 -- Incorrect status
  ///               3 -- Incorrect barcode
  //                4 -- Incorrect batch
  ///               5 -- Incorrect quantity
  ///   DwellTime = 0 -- OK
  ///               1 -- Reason Screen
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Create_Pallet
(
 @type       int,
 @operatorId int,
 @palletId   int,
 @barcode    nvarchar(50) = null,
 @batch      nvarchar(50) = null,
 @quantity   float         = null
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @Rowcount           int,
          @StorageUnitBatchId int,
          @DwellTime          int,
          @WarehouseId        int
    
  set @Errormsg = 'You may continue.'
  set @Error = 0
  
  exec @DwellTime = p_Mobile_Dwell_Time
   @Type       = 2,
   @OperatorId = @OperatorId
  
  select @WarehouseId = WarehouseId
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  select @StorageUnitBatchId = StorageUnitBatchId
    from Pallet (nolock)
   where PalletId = @PalletId
  
  select @Rowcount = @@rowcount
  
  if @StorageUnitBatchId is not null
  begin
    set @Error = 2
    set @Errormsg = 'Pallet already exists for another stock item please scan different label.'
    goto result
  end
  
  if @Rowcount = 0
  begin
   SET IDENTITY_INSERT Pallet ON
   insert Pallet (PalletId, Prints) select @palletId, 1
   
   exec p_PalletHistory_Insert @PaleltId = @palletId, @prints = 1
   
   SET IDENTITY_INSERT Pallet OFF
  end
  
  if @Type = 0 -- check barcode (Product or Pack and Batch)
  begin
    if @Barcode is null
    begin
      set @Error = 3
      set @Errormsg = 'Incorrect barcode'
      goto result
    end
    
    select @StorageUnitBatchId = sub.StorageUnitBatchId
      from Product p (nolock)
      join StorageUnit       su (nolock) on p.ProductId      = su.ProductId
      join Pack              pk (nolock) on su.StorageUnitId = pk.StorageUnitId
      join StorageUnitBatch sub (nolock) on su.StorageUnitId = sub.StorageUnitId
      join Batch              b (nolock) on sub.BatchId      = b.BatchId
     where (p.Barcode  = @barcode
       or   pk.Barcode = @barcode)
       and b.Batch     = @batch
    
    if @StorageUnitBatchId is null
    begin
      set @Error = 3
      set @Errormsg = 'Incorrect barcode'
      goto result
    end
  end
  --else if @type = 2 -- Check quantity
  begin
    if @quantity is null
    begin
      set @Error = 5
      set @Errormsg = 'Incorrect quantity'
      goto result
    end
  end
  
  exec @Error = p_Pallet_Update
   @PalletId           = @palletId,
   @StorageUnitBatchId = @StorageUnitBatchId
  
  if @Error <> 0
  begin
    set @Error = 7
    set @Errormsg = 'Error updating Pallet'
    goto result
  end
  
  exec @Error = p_Palletised_Insert
   @WarehouseId         = @WarehouseId,
   @OperatorId          = @OperatorId,
   @InstructionTypeCode = 'M',
   @StorageUnitBatchId  = @StorageUnitBatchId,
   @PickLocationId      = null,
   @StoreLocationId     = null,
   @Quantity            = @quantity,
   @PalletId            = @palletId
  
  if @Error <> 0
  begin
    set @Error = 6
    set @Errormsg = 'Error inserting into Pallet'
    goto result
  end
  
  result:
    select @Error             as 'Result',
           @Errormsg          as 'Message',
           @DwellTime         as 'DwellTime'
end
