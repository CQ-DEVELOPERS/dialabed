﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletMatch_Search
  ///   Filename       : p_PalletMatch_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:47
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the PalletMatch table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   PalletMatch.PalletId,
  ///   PalletMatch.InstructionId,
  ///   PalletMatch.StorageUnitBatchId,
  ///   PalletMatch.ProductCode,
  ///   PalletMatch.SKUCode,
  ///   PalletMatch.Batch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletMatch_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         PalletMatch.PalletId
        ,PalletMatch.InstructionId
        ,PalletMatch.StorageUnitBatchId
        ,PalletMatch.ProductCode
        ,PalletMatch.SKUCode
        ,PalletMatch.Batch
    from PalletMatch
  
end
