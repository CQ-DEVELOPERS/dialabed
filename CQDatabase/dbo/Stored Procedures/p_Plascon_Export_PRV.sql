﻿
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Export_PRV
  ///   Filename       : p_Plascon_Export_PRV.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 2011-11-04
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Plascon_Export_PRV]
as
begin
	 set nocount on;
  
    declare @Error             int,
            @Errormsg          varchar(500),
            @GetDate           datetime,
            @RecordType        char(3),
            @SequenceNumber    int,
            @Increment         int
  
    select @RecordType = 'PRV'
          ,@GetDate = dbo.ufn_Getdate()
          ,@Errormsg = 'p_Plascon_Export_PRV - Error executing p_Plascon_Export_Order'
  
    begin transaction
  
    update InterfaceExportHeader
       set RecordStatus  = 'Y',
           ProcessedDate = @getdate
     where RecordStatus = 'N'
       and RecordType   = isnull(@RecordType,RecordType)
       
    set @Increment = @@ROWCOUNT
  
    select @Error = @@Error, @Errormsg = 'p_Plascon_Export_PRV - Error updating InterfaceExportHeader'
  
    if @Error <> 0
        goto error

    exec p_Sequence_Number 
                 @Value = @SequenceNumber output
                ,@Increment = @Increment
    
    insert InterfaceExtract
           (
            RecordType,
            SequenceNumber,
            OrderNumber,
            LineNumber,
            RecordStatus,
            ExtractedDate,
            Data
           )
    select  @RecordType,
            sequence_no,
            doc_no,
            di_no,
            'N',
            extract_date,            
            replicate('0',9  - datalength(convert(varchar(9),sequence_no))) + convert(varchar(9),sequence_no) + 
            record_type + 
            replicate('0',3  - datalength(convert(varchar(3),branch_code))) + convert(varchar(3),branch_code) + 
            convert(varchar(6),ltrim(lot_code)) +  replicate(' ',6 - datalength(convert(varchar(6),ltrim(lot_code)))) +
            replicate(' ',9 - datalength(convert(varchar(9),ltrim(fxa_code)))) + convert(varchar(9),ltrim(fxa_code)) +  
            convert(varchar(6),ltrim(dil_ref_no)) +  replicate(' ',6 - datalength(convert(varchar(6),ltrim(dil_ref_no)))) +
            extract_date + 
            convert(varchar(20),ltrim(remarks)) +  replicate(' ',20 - datalength(convert(varchar(20),ltrim(remarks)))) +
            replicate(' ',9 - datalength(convert(varchar(9),ltrim(stock_no)))) + convert(varchar(9),ltrim(stock_no))     +  
            replicate('0',4  - datalength(convert(varchar(4),sku_code))) + convert(varchar(4),sku_code) + 
            replicate('0',6 - datalength(convert(varchar(6),convert(int,dil_qty)))) + convert(varchar(6),convert(int,dil_qty))
    From
        (Select  s.sequence_no
                ,h.PrimaryKey As doc_no
                ,d.LineNumber As di_no
                ,@RecordType As record_type
                ,ISNULL(h.ToWarehouseCode,'045') As branch_code /*???*/
                ,d.Batch As lot_code
                ,ISNULL(d.Additional1,'') As fxa_code
                ,CONVERT(VarChar(6), d.LineNumber) As dil_ref_no
                ,CONVERT(Varchar(10), @GetDate, 120) As extract_date
                ,ISNULL(h.Remarks,'none') As remarks
                ,d.ProductCode As stock_no
                ,d.SKUCode As sku_code
                ,d.Quantity As dil_qty
         From 
            (select InterfaceExportHeaderId, ROW_NUMBER() Over (Order By InterfaceExportHeaderId) + @SequenceNumber As sequence_no 
             from InterfaceExportHeader Where ProcessedDate = @GetDate) s
            Inner Join InterfaceExportHeader h on s.InterfaceExportHeaderId = h.InterfaceExportHeaderId
            Inner Join InterfaceExportDetail d on h.InterfaceExportHeaderId = d.InterfaceExportHeaderId
            where d.Quantity > 0
            and OrderNumber is not null
            ) ExtractTable
        Order By sequence_no, lot_code
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end

