﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPOHeader_Delete
  ///   Filename       : p_InterfaceImportPOHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:59
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceImportPOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportPOHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPOHeader_Delete
(
 @InterfaceImportPOHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceImportPOHeader
     where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
