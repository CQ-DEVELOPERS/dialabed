﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Delete
  ///   Filename       : p_Wave_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013 14:17:31
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Wave table.
  /// </remarks>
  /// <param>
  ///   @WaveId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Delete
(
 @WaveId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Wave
     where WaveId = @WaveId
  
  select @Error = @@Error
  
  
  return @Error
  
end
