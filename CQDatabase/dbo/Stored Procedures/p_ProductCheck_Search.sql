﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCheck_Search
  ///   Filename       : p_ProductCheck_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:02
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ProductCheck table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ProductCheck.JobId,
  ///   ProductCheck.OperatorId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCheck_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         ProductCheck.JobId
        ,ProductCheck.OperatorId
    from ProductCheck
  
end
