﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AddressHistory_Insert
  ///   Filename       : p_AddressHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:32
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AddressHistory table.
  /// </remarks>
  /// <param>
  ///   @AddressId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @Street nvarchar(200) = null,
  ///   @Suburb nvarchar(200) = null,
  ///   @Town nvarchar(200) = null,
  ///   @Country nvarchar(200) = null,
  ///   @Code nvarchar(40) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   AddressHistory.AddressId,
  ///   AddressHistory.ExternalCompanyId,
  ///   AddressHistory.Street,
  ///   AddressHistory.Suburb,
  ///   AddressHistory.Town,
  ///   AddressHistory.Country,
  ///   AddressHistory.Code,
  ///   AddressHistory.CommandType,
  ///   AddressHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AddressHistory_Insert
(
 @AddressId int = null,
 @ExternalCompanyId int = null,
 @Street nvarchar(200) = null,
 @Suburb nvarchar(200) = null,
 @Town nvarchar(200) = null,
 @Country nvarchar(200) = null,
 @Code nvarchar(40) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert AddressHistory
        (AddressId,
         ExternalCompanyId,
         Street,
         Suburb,
         Town,
         Country,
         Code,
         CommandType,
         InsertDate)
  select @AddressId,
         @ExternalCompanyId,
         @Street,
         @Suburb,
         @Town,
         @Country,
         @Code,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
