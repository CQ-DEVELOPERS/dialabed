﻿ 
/* 
  /// <summary> 
  ///   Procedure Name : p_Issue_Count_Update 
  ///   Filename       : p_Issue_Count_Update.sql 
  ///   Create By      : Grant Schultz 
  ///   Date Created   : 09 Nov 2009 
  /// </summary> 
  /// <remarks> 
  ///    
  /// </remarks> 
  /// <param> 
  ///    
  /// </param> 
  /// <returns> 
  ///    
  /// </returns> 
  /// <newpara> 
  ///   Modified by    :  Daniel Schotter
  ///   Modified Date  :  2021-02-17
  ///   Details        :  Added a 1 year date range to the updates.
  /// </newpara> 
*/ 
CREATE procedure p_Issue_Count_Update 
  
as 
begin 
    SET LOCK_TIMEOUT 10000
 
      BEGIN TRY
        update tr 
         set Complete = (select count(1) 
                             from IssueLineInstruction ili (nolock) 
                             join Instruction            i (nolock) on ili.InstructionId = i.InstructionId 
                             join Status                 s (nolock) on i.StatusId        = s.StatusId 
                            where tr.IssueId   = ili.IssueId 
                              and s.StatusCode in ('F','NS')) 
        from Issue tr where deliverydate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 12, 0) --DanielS
 
        update tr 
         set Loaded = (select count(distinct (j.JobId)) 
                         from IssueLineInstruction ili (nolock) 
                         join Instruction            i (nolock) on ili.InstructionId = i.InstructionId 
                         join Job                    j (nolock) on i.JobId           = j.JobId 
                         join Status                 s (nolock) on j.StatusId        = s.StatusId 
                        where tr.IssueId   = ili.IssueId 
                          and s.StatusCode in ('C')) 
        from Issue tr where deliverydate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 12, 0) --DanielS
 
        update tr 
         set ShortPicks = (select isnull(sum(ili.Quantity),0) - isnull(sum(ili.ConfirmedQuantity),0) 
                             from IssueLineInstruction ili (nolock) 
                            where tr.IssueId   = ili.IssueId 
                              and ili.Quantity > isnull(ili.confirmedQuantity,0)) 
        from Issue tr where deliverydate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 12, 0) --DanielS
 
        update i 
         set Weight = (select sum(pk.Weight * il.Quantity) 
                         from IssueLine    il (nolock) 
                         join OutboundLine ol (nolock) on ol.OutboundLineId = il.OutboundLineId 
                         join Pack         pk (nolock) on ol.StorageUnitId = pk.StorageUnitId 
                         join PackType     pt (nolock) on pk.PackTypeId = pt.PackTypeId 
                        where pt.OutboundSequence in (select max(OutboundSequence) from PackType (nolock)) 
                          and il.IssueId = i.IssueId 
                          and il.Quantity > 0 
                          and pk.Weight > 0
                          and pk.WarehouseId = i.WarehouseId) 
        from Issue         i (nolock) 
        where i.Weight is null 
			  and deliverydate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 12, 0) --DanielS
 
        update i 
         set Volume = (select sum(pk.Volume * il.Quantity) 
                         from IssueLine    il (nolock) 
                         join OutboundLine ol (nolock) on ol.OutboundLineId = il.OutboundLineId 
                         join Pack         pk (nolock) on ol.StorageUnitId = pk.StorageUnitId 
                         join PackType     pt (nolock) on pk.PackTypeId = pt.PackTypeId 
                        where pt.OutboundSequence in (select max(OutboundSequence) from PackType (nolock)) 
                          and il.IssueId = i.IssueId 
                          and il.Quantity > 0 
                          and pk.Volume > 0
                          and pk.WarehouseId = i.WarehouseId) 
        from Issue         i (nolock) 
        where i.Volume is null 
			  and deliverydate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 12, 0) --DanielS
 
        update rl
           set NumberOfPallets = pk.Quantity * il.Quantity
          from Receipt       r (nolock)
          join ReceiptLine  rl (nolock) on r.ReceiptId = rl.ReceiptId
          join InboundLine  il (nolock) on il.InboundLineId = rl.InboundLineId
          join Pack         pk (nolock) on il.StorageUnitId = pk.StorageUnitId 
          join PackType     pt (nolock) on pk.PackTypeId = pt.PackTypeId 
         where pt.InboundSequence in (select min(InboundSequence) from PackType (nolock))
           and pk.WarehouseId = r.WarehouseId
           and rl.NumberOfPallets is null
         
        IF @@error = 0
            PRINT 'Completed'
 
      END TRY
      BEGIN CATCH
            PRINT ERROR_MESSAGE()
      END CATCH
end
