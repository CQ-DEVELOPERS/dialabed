﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Release_Detail_Search
  ///   Filename       : p_Release_Detail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Release_Detail_Search
(
 @OutboundShipmentId  int,
 @IssueId             int
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   DocumentNumber nvarchar(30),
   OrderNumber    nvarchar(30),
   IssueLineId    int,
   AreaType       nvarchar(10)
  )
  
  declare @TableResult as table
  (
   OrderNumber    nvarchar(30),
   StorageUnitId  int,
   ProductCode    nvarchar(30),
   Product        nvarchar(255),
   SKUCode        nvarchar(50),
   Batch          nvarchar(50),
   OrderQuantity  float,
   ShortQuantity  float,
   SOH            float,
   AreaType       nvarchar(10)
  );
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is not null
    set @IssueId = null
  
  if @IssueId is not null
  begin
    insert @TableHeader
          (IssueLineId,
           OrderNumber,
           AreaType)
    select il.IssueLineId,
           od.OrderNumber,
           i.AreaType
      from IssueLine              il (nolock)
      join Issue                   i (nolock) on il.IssueId  = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where il.IssueId = @IssueId
    
    insert @TableResult
          (OrderNumber,
           StorageUnitId,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           OrderQuantity,
           ShortQuantity,
           AreaType)
    select th.OrderNumber,
           sub.StorageUnitId,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           b.Batch,
           sum(i.Quantity),
           sum(i.Quantity - i.ConfirmedQuantity),
           th.AreaType
      from @TableHeader th
      join IssueLineInstruction ili (nolock) on th.IssueLineId       = ili.IssueLineId
      join Instruction            i (nolock) on ili.InstructionId    = isnull(i.InstructionRefId, i.InstructionId)
      join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit           su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join Product                p (nolock) on su.ProductId         = p.ProductId
      join SKU                  sku (nolock) on su.SKUId             = sku.SKUId
      join Batch                  b (nolock) on sub.BatchId          = b.BatchId
     where i.Quantity > i.ConfirmedQuantity
    group by th.OrderNumber,
             sub.StorageUnitId,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             th.AreaType
  end
  
  if @OutboundShipmentId is not null
  begin
    insert @TableHeader
          (IssueLineId,
           DocumentNumber,
           OrderNumber,
           AreaType)
    select il.IssueLineId,
           convert(nvarchar(30), osi.OutboundShipmentId),
           od.OrderNumber,
           i.AreaType
      from OutboundShipmentIssue osi (nolock)
      join IssueLine              il (nolock) on osi.IssueId = il.IssueId
      join Issue                   i (nolock) on il.IssueId  = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where osi.OutboundShipmentId = @OutboundShipmentId
    
    insert @TableResult
          (OrderNumber,
           StorageUnitId,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           OrderQuantity,
           ShortQuantity,
           AreaType)
    select th.OrderNumber,
           sub.StorageUnitId,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           b.Batch,
           sum(i.Quantity),
           sum(i.Quantity - i.ConfirmedQuantity),
           th.AreaType
      from @TableHeader th
      join IssueLineInstruction ili (nolock) on th.IssueLineId       = ili.IssueLineId
      join Instruction            i (nolock) on ili.InstructionId    = isnull(i.InstructionRefId, i.InstructionId)
      join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit           su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join Product                p (nolock) on su.ProductId         = p.ProductId
      join SKU                  sku (nolock) on su.SKUId             = sku.SKUId
      join Batch                  b (nolock) on sub.BatchId          = b.BatchId
     where i.Quantity > i.ConfirmedQuantity
    group by th.OrderNumber,
             sub.StorageUnitId,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             th.AreaType
  end
  
  update tr
     set SOH = (select SUM(ActualQuantity)
                  from StorageUnitBatch          sub (nolock)
                  join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                  join Location                    l (nolock) on subl.LocationId        = l.LocationId
                  join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
                  join Area                        a (nolock) on al.AreaId              = a.AreaId
                 where a.StockOnHand = 1
                   and isnull(tr.AreaType,'') = isnull(a.AreaType,'')
                   and a.AreaCode in ('PK','RK','BK','SP')
                   and tr.StorageUnitId          = sub.StorageUnitId)
    from @TableResult tr
  
  select distinct OrderNumber,
                  StorageUnitId,
                  ProductCode,
                  Product,
                  SKUCode,
                  Batch,
                  OrderQuantity,
                  ShortQuantity,
                  isnull(SOH, 0) as 'SOH'
    from @TableResult
end
