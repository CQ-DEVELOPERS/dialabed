﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Update
  ///   Filename       : p_ExternalCompany_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:44:47
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId int = null,
  ///   @ExternalCompanyTypeId int = null,
  ///   @RouteId int = null,
  ///   @ExternalCompany nvarchar(510) = null,
  ///   @ExternalCompanyCode nvarchar(60) = null,
  ///   @Rating int = null,
  ///   @RedeliveryIndicator bit = null,
  ///   @QualityAssuranceIndicator bit = null,
  ///   @ContainerTypeId int = null,
  ///   @Backorder bit = null,
  ///   @HostId nvarchar(60) = null,
  ///   @OrderLineSequence bit = null,
  ///   @DropSequence int = null,
  ///   @AutoInvoice bit = null,
  ///   @OneProductPerPallet bit = null,
  ///   @PrincipalId int = null,
  ///   @ProcessId int = null,
  ///   @TrustedDelivery bit = null,
  ///   @OutboundSLAHours int = null,
  ///   @PricingCategoryId int = null,
  ///   @LabelingCategoryId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Update
(
 @ExternalCompanyId int = null,
 @ExternalCompanyTypeId int = null,
 @RouteId int = null,
 @ExternalCompany nvarchar(510) = null,
 @ExternalCompanyCode nvarchar(60) = null,
 @Rating int = null,
 @RedeliveryIndicator bit = null,
 @QualityAssuranceIndicator bit = null,
 @ContainerTypeId int = null,
 @Backorder bit = null,
 @HostId nvarchar(60) = null,
 @OrderLineSequence bit = null,
 @DropSequence int = null,
 @AutoInvoice bit = null,
 @OneProductPerPallet bit = null,
 @PrincipalId int = null,
 @ProcessId int = null,
 @TrustedDelivery bit = null,
 @OutboundSLAHours int = null,
 @PricingCategoryId int = null,
 @LabelingCategoryId int = null 
)
 
as
begin
	 set nocount on;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @ExternalCompanyTypeId = '-1'
    set @ExternalCompanyTypeId = null;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @ExternalCompany = '-1'
    set @ExternalCompany = null;
  
  if @ExternalCompanyCode = '-1'
    set @ExternalCompanyCode = null;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @ProcessId = '-1'
    set @ProcessId = null;
  
  if @PricingCategoryId = '-1'
    set @PricingCategoryId = null;
  
  if @LabelingCategoryId = '-1'
    set @LabelingCategoryId = null;
  
	 declare @Error int
 
  update ExternalCompany
     set ExternalCompanyTypeId = isnull(@ExternalCompanyTypeId, ExternalCompanyTypeId),
         RouteId = isnull(@RouteId, RouteId),
         ExternalCompany = isnull(@ExternalCompany, ExternalCompany),
         ExternalCompanyCode = isnull(@ExternalCompanyCode, ExternalCompanyCode),
         Rating = isnull(@Rating, Rating),
         RedeliveryIndicator = isnull(@RedeliveryIndicator, RedeliveryIndicator),
         QualityAssuranceIndicator = isnull(@QualityAssuranceIndicator, QualityAssuranceIndicator),
         ContainerTypeId = isnull(@ContainerTypeId, ContainerTypeId),
         Backorder = isnull(@Backorder, Backorder),
         HostId = isnull(@HostId, HostId),
         OrderLineSequence = isnull(@OrderLineSequence, OrderLineSequence),
         DropSequence = isnull(@DropSequence, DropSequence),
         AutoInvoice = isnull(@AutoInvoice, AutoInvoice),
         OneProductPerPallet = isnull(@OneProductPerPallet, OneProductPerPallet),
         PrincipalId = isnull(@PrincipalId, PrincipalId),
         ProcessId = isnull(@ProcessId, ProcessId),
         TrustedDelivery = isnull(@TrustedDelivery, TrustedDelivery),
         OutboundSLAHours = isnull(@OutboundSLAHours, OutboundSLAHours),
         PricingCategoryId = isnull(@PricingCategoryId, PricingCategoryId),
         LabelingCategoryId = isnull(@LabelingCategoryId, LabelingCategoryId) 
   where ExternalCompanyId = @ExternalCompanyId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
