﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroup_Update
  ///   Filename       : p_OperatorGroup_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2012 07:59:18
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @OperatorGroup varchar(30) = null,
  ///   @RestrictedAreaIndicator bit = null,
  ///   @OperatorGroupCode varchar(10) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroup_Update
(
 @OperatorGroupId int = null,
 @OperatorGroup varchar(30) = null,
 @RestrictedAreaIndicator bit = null,
 @OperatorGroupCode varchar(10) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  update OperatorGroup
     set OperatorGroup = isnull(@OperatorGroup, OperatorGroup),
         RestrictedAreaIndicator = isnull(@RestrictedAreaIndicator, RestrictedAreaIndicator),
         OperatorGroupCode = isnull(@OperatorGroupCode, OperatorGroupCode) 
   where OperatorGroupId = @OperatorGroupId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OperatorGroupHistory_Insert
         @OperatorGroupId = @OperatorGroupId,
         @OperatorGroup = @OperatorGroup,
         @RestrictedAreaIndicator = @RestrictedAreaIndicator,
         @OperatorGroupCode = @OperatorGroupCode,
         @CommandType = 'Update'
  
  return @Error
  
end
