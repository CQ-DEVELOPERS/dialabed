﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Parameter
  ///   Filename       : p_Configuration_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 12:12:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Configuration table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Configuration.ConfigurationId,
  ///   Configuration.WarehouseId,
  ///   Configuration.Configuration 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as ConfigurationId
        ,null as WarehouseId
        ,'{All}' as Configuration
  union
  select
         Configuration.ConfigurationId
        ,Configuration.WarehouseId
        ,Configuration.Configuration
    from Configuration
  order by Configuration
  
end
 
