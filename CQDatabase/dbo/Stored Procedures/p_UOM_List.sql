﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UOM_List
  ///   Filename       : p_UOM_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:00
  /// </summary>
  /// <remarks>
  ///   Selects rows from the UOM table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   UOM.UOMId,
  ///   UOM.UOM 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_UOM_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as UOMId
        ,'{All}' as UOM
  union
  select
         UOM.UOMId
        ,UOM.UOM
    from UOM
  
end
