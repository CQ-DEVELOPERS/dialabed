﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Count_Green_Labels
  ///   Filename       : p_Exception_Count_Green_Labels.sql
  ///   Create By      : Karen	
  ///   Date Created   : March 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Count_Green_Labels
(
	@ReceiptLineId	int
)

 
as
begin
	 set nocount on;
	  declare @Reasonid int
 
 SET @Reasonid = (SELECT ReasonId FROM Reason WHERE ReasonCode = 'LAB02')
  
 Select sum(e.Quantity) as Quantity
 from   Exception e
 where  @ReceiptLineId	= e.ReceiptLineId
 and	@Reasonid		= e.ReasonId
 
end

