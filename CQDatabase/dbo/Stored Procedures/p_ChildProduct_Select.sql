﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ChildProduct_Select
  ///   Filename       : p_ChildProduct_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:21
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ChildProduct table.
  /// </remarks>
  /// <param>
  ///   @ChildProductId int = null 
  /// </param>
  /// <returns>
  ///   ChildProduct.ChildProductId,
  ///   ChildProduct.ChildStorageUnitBatchId,
  ///   ChildProduct.ParentStorageUnitBatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ChildProduct_Select
(
 @ChildProductId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ChildProduct.ChildProductId
        ,ChildProduct.ChildStorageUnitBatchId
        ,ChildProduct.ParentStorageUnitBatchId
    from ChildProduct
   where isnull(ChildProduct.ChildProductId,'0')  = isnull(@ChildProductId, isnull(ChildProduct.ChildProductId,'0'))
  
end
