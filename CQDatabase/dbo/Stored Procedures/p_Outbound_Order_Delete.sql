﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Order_Delete
  ///   Filename       : p_Outbound_Order_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Order_Delete
(
 @IssueId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @StatusId          int
  
  set @Errormsg = 'Error executing p_Outbound_Order_Delete'
  
  select @StatusId = dbo.ufn_StatusId('Z','Z')
  
  begin transaction
  
  if (select s.StatusCode
        from Issue i (nolock)
        join Status s (nolock) on i.StatusId = s.StatusId
       where i.IssueId = @IssueId) not in ('W','P','OH')
  begin
    set @Errormsg = 'Error executing p_Outbound_Order_Delete - Issue not in correct status'
    goto error
  end
  
  if exists(select top 1 1
              from OutboundShipmentIssue
             where IssueId = @IssueId)
  begin
    set @Errormsg = 'Error executing p_Outbound_Order_Delete - Issue is linked to a shipment'
    goto error
  end
  
  exec @Error = p_Issue_Update
   @IssueId = @IssueId,
   @StatusId = @StatusId
  
  if @Error <> 0
    goto error
  
  update IssueLine
     set StatusId = @StatusId
   where IssueId = @IssueId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
 
