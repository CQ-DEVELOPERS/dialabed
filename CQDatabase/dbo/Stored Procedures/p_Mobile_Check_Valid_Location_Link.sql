﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Valid_Location_Link
  ///   Filename       : p_Mobile_Check_Valid_Location_Link.sql
  ///   Create By      : Edgar Blount
  ///   Date Created   : 31 Jul 2018
  /// </summary>
  /// <remarks>
  ///   
  

/// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Mobile_Check_Valid_Location_Link]


(
 @barcode nvarchar(30),
 @locationId int = null
)
 
as
begin
	 set nocount on;
  
  declare	@bool				bit,
			@ScanCode           bit = 0
			
  select @ScanCode = dbo.ufn_Configuration(217, 47)
  			
  select @barcode = replace(@barcode,'L:','')
  
  if @ScanCode = 0
  begin
	  if @locationId is null
	  begin
		if exists(select top 1 1 from Location (nolock) where convert(nvarchar(10), SecurityCode) = @barcode)
		  set @bool = 1
		else
		  set @bool = 0
	    
		select @bool
	  end
	  else

	  begin
	   
		if @locationId in (select locationId from Location (nolock) where convert(nvarchar(10), SecurityCode) = @barcode)
		  set @bool = 1
		else
		  set @bool = 0
	    
		select @bool
	  end
  end
  
  if @ScanCode = 1
  begin
	  if @locationId is null
	  begin
		if exists(select top 1 1 from Location (nolock) where Location = @barcode)
		  set @bool = 1
		else
		  set @bool = 0
	    
		select @bool
	  end
	  else
	  begin
		if @locationId in (select locationId from Location (nolock) where Location = @barcode)
		  set @bool = 1
		else
		  set @bool = 0
	    
		select @bool
	  end
  end
end