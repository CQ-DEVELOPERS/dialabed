﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundPerformance_Update
  ///   Filename       : p_OutboundPerformance_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:35
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OutboundPerformance table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @CreateDate datetime = null,
  ///   @Release datetime = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null,
  ///   @Checking datetime = null,
  ///   @Despatch datetime = null,
  ///   @DespatchChecked datetime = null,
  ///   @Complete datetime = null,
  ///   @Checked datetime = null,
  ///   @PlanningComplete datetime = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundPerformance_Update
(
 @JobId int = null,
 @CreateDate datetime = null,
 @Release datetime = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @Checking datetime = null,
 @Despatch datetime = null,
 @DespatchChecked datetime = null,
 @Complete datetime = null,
 @Checked datetime = null,
 @PlanningComplete datetime = null,
 @WarehouseId int = null 
)
 
as
begin
	 set nocount on;
  
  if @JobId = '-1'
    set @JobId = null;
  
	 declare @Error int
 
  update OutboundPerformance
     set CreateDate = isnull(@CreateDate, CreateDate),
         Release = isnull(@Release, Release),
         StartDate = isnull(@StartDate, StartDate),
         EndDate = isnull(@EndDate, EndDate),
         Checking = isnull(@Checking, Checking),
         Despatch = isnull(@Despatch, Despatch),
         DespatchChecked = isnull(@DespatchChecked, DespatchChecked),
         Complete = isnull(@Complete, Complete),
         Checked = isnull(@Checked, Checked),
         PlanningComplete = isnull(@PlanningComplete, PlanningComplete),
         WarehouseId = isnull(@WarehouseId, WarehouseId) 
   where JobId = @JobId
  
  select @Error = @@Error
  
  
  return @Error
  
end
