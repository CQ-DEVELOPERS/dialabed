﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Invoice_Advice
  ///   Filename       : p_Report_Invoice_Advice.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Invoice_Advice
(
 @ConnectionString   nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @OutboundShipmentId int,
 @IssueId            int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   DespatchDate       datetime,
   DropSequence       int,
   RouteId            int,
   Route              nvarchar(50),
   InvoiceNumber      nvarchar(50),
   InvoiceTotal       numeric(13,2)
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
	 
	 if @OutboundShipmentId is not null
	   set @IssueId = null
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         OutboundDocumentId)
  select distinct
         ili.OutboundShipmentId,
         ili.IssueId,
         ili.OutboundDocumentId
    from IssueLineInstruction  ili (nolock)
   where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))
     and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
  
  update tr
     set DespatchDate = os.ShipmentDate,
         RouteId      = os.RouteId,
         DropSequence = osi.DropSequence
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
   where tr.OutboundShipmentId is not null
  
  update tr
     set DespatchDate = i.DeliveryDate,
         RouteId      = i.RouteId,
         DropSequence = i.DropSequence
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
   where tr.OutboundShipmentId is null
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  select OutboundShipmentId,
         OrderNumber,
         DropSequence,
         ExternalCompany,
         Route,
         DespatchDate,
         InvoiceNumber,
         InvoiceTotal
    from @TableResult
  order by OutboundShipmentId,
           DropSequence,
           OrderNumber
end
