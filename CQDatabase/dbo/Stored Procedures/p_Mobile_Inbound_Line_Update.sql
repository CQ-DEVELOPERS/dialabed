﻿
 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Inbound_Line_Update
  ///   Filename       : p_Mobile_Inbound_Line_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Aug 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter
  ///   Modified Date  : 2017-09-12
  ///   Details        : Added WarehouseId Control by sending the WarehouseId to palletise stored proc.
  /// </newpara>
*/
CREATE procedure p_Mobile_Inbound_Line_Update
(
 @receiptLineId       int,
 @OperatorId          int           = null,
 @batch               nvarchar(30)  = null,
 @ExpiryDate          nvarchar(30)  = null,
 @quantity            numeric(13,6) = null,
 @sampleQuantity      numeric(13,6) = null,
 @rejectQuantity      numeric(13,6) = null,
 @reasonId            int           = null,
 @storeLocation       nvarchar(30)  = null,
 @palletId            nvarchar(30)  = null,
 @NumberOfPallets	    int           = null,
 @DeliveryNoteQuantity numeric(13,6) = null,
 @boe                  nvarchar(50) = null
)
as
begin
	 set nocount on
    
  declare @MobileLogId int  
    
  insert MobileLog
        (ProcName,
         ReceiptLineId,
         PalletId,
         Barcode,
         Store,
         Batch,
         Quantity,
         OperatorId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @receiptLineId,
         @palletId,
         @storeLocation,
         @storeLocation,
         @batch,
         @quantity,
         @OperatorId,
         getdate()
  
  select @MobileLogId = scope_identity()

  declare @TableResult as table
  (
   ReceiptLineId    int,
   AcceptedQuantity numeric(13,6) default 0,
   SampleQuantity   numeric(13,6) default 0,
   AssaySamples     numeric(13,6) default 0,
   RetentionSamples numeric(13,6) default 0
  )
  
  declare @Error              int,
          @Errormsg           nvarchar(255) = 'Error executing ' + OBJECT_NAME(@@PROCID),
          @GetDate            datetime,
          @WarehouseId        int,
          @StatusId           int,
          @StorageUnitBatchId int,
          @ReceiptId          int,
          @ExternalCompanyId  int,
          @BatchId            int,
          @StorageUnitId      int,
          @OldBatchId         int,
          @RequiredQuantity   numeric(13,6),
          @OverReceipt        numeric(13,6),
          @StoreLocationId    int,
          @Rowcount           int,
          @ReceivedQuantity   float,
          @InboundLineId      int,
          @ChildStorageUnitId int,
          @ChildStorageUnitBatchId int,
          @ChildProductId          int,
          @ProductId               int,
          @AssaySamples            float,
          @RetentionSamples        float,
          @NewReceiptLineId        int,
          @InstructionId           int,
          @IntPalletId             int,
          @AcceptedQuantity        numeric(13,6),
          @InboundDocumentTypeCode nvarchar(10),
          @InboundDocumentId       int,
          @BOELineNumber           nvarchar(50),
          @OrderNumber             nvarchar(30),
          @ReferenceNumber         nvarchar(50),
          @OverReceiptDoc          bit = 0,
          @ScannedLocationId       int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @batch = replace(@batch,'A:','')
  select @batch = replace(@batch,'Q:','')
  select @batch = replace(@batch,'QC:','')
  
  if @receiptLineId = -1
    set @receiptLineId = null
  
  select @ReceiptId        = rl.ReceiptId,
         @StorageUnitId    = sub.StorageUnitId,
         @OldBatchId       = sub.BatchId,
         @RequiredQuantity = rl.RequiredQuantity,
         @InboundLineId    = rl.InboundLineId,
         @ProductId        = su.ProductId
    from ReceiptLine       rl (nolock)
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
   where rl.ReceiptLineId = @ReceiptLineId
  
  select @InboundDocumentId = InboundDocumentId
    from InboundLine
   where InboundLineId = @InboundLineId
  
  select @RetentionSamples = RetentionSamples
    from Product (nolock)
   where ProductId = @ProductId
  
  if @RetentionSamples is null
    set @RetentionSamples = 0
  
  select @WarehouseId = WarehouseId
    from Operator (nolock)
   where OperatorId = @OperatorId
   
  
  
  select @WarehouseId       = isnull(@WarehouseId, id.WarehouseId),
         @ExternalCompanyId = id.ExternalCompanyId,
         @StoreLocationId   = r.LocationId,
         @InboundDocumentTypeCode = idt.InboundDocumentTypeCode,
         @OrderNumber       = id.OrderNumber,
         @OverReceiptDoc    = idt.OverReceiveIndicator
    from Receipt          r (nolock)
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
   where r.ReceiptId = @ReceiptId
  
  insert @TableResult
        (ReceiptLineId,
         AcceptedQuantity,
         SampleQuantity,
         AssaySamples)
  select ReceiptLineId,
         isnull(AcceptedQuantity,0),
         isnull(SampleQuantity,0),
         isnull(AssaySamples,0)
    from ReceiptLine (nolock)
   where ReceiptLineId = @receiptLineId
  
  --exec p_Container_Scan
  -- @WarehouseId = 1,
  -- @operatorId  = @operatorId,
  -- @barcode     = @OrderNumber,
  -- @In          = 1,
  -- @Version     = 2,
  -- @showMsg     = 0
  
  if (select dbo.ufn_Configuration(414, @WarehouseId)) = 1 -- Batch UPPER case
	   set @Batch = UPPER(@Batch)
  
  begin transaction
  
  if @InboundDocumentTypeCode != 'BOND'
  If @batch = ''
    if (select dbo.ufn_Configuration(129,@WarehouseId)) = 0
		    Select @batch= dbo.DateFormat1(GETDATE())
		  else 
		    select @batch = 'Default'
	 
	 if @boe is not null
	 begin
	   update b
	      set Batch = @Batch + @boe,
	          BatchReferenceNumber = @Batch
	     from Batch              b 
	     join StorageUnitBatch sub (nolock) on b.BatchId = sub.BatchId
	     join ReceiptLine       rl (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
	    where rl.ReceiptLineId = @receiptLineId
	 end
	 else
  if @batch is not null
  begin
    if (select dbo.ufn_Configuration(129,@WarehouseId)) = 0
    and @batch = 'Default'
    begin
      select @Error = -1
      goto error
    end
    
    if @InboundDocumentTypeCode != 'BOND'
    begin
      if (dbo.ufn_Configuration(455,@WarehouseId)) = 1
      begin
        select @ReferenceNumber = @OrderNumber
        if ((select dbo.ufn_Configuration(492,@WarehouseId)) = 1) -- Allow products to Share same BatchId
          select @batchId = b.BatchId
            from Batch              b (nolock)
            join StorageUnitBatch sub (nolock) on b.BatchId              = sub.BatchId    -- GS 2012-10-02 Add for Tacoma - Batch must be per product
            join ReceiptLine       rl (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
                                              and rl.ReceiptId           = @ReceiptId
           where b.Batch = @batch
        else
          select @batchId = b.BatchId
            from Batch              b (nolock)
            join StorageUnitBatch sub (nolock) on b.BatchId              = sub.BatchId    -- GS 2012-10-02 Add for Tacoma - Batch must be per product
                                              and sub.StorageUnitId      = @StorageUnitId
            join ReceiptLine       rl (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
                                              and rl.ReceiptId           = @ReceiptId
           where b.Batch = @batch
      end
      else
      begin
        if ((select dbo.ufn_Configuration(492,@WarehouseId)) = 1) -- Allow products to Share same BatchId
        select @batchId = b.BatchId
          from Batch              b (nolock)
         where b.Batch = @batch
        else        
        select @batchId = b.BatchId
          from Batch              b (nolock)
          join StorageUnitBatch sub (nolock) on b.BatchId         = sub.BatchId    -- GS 2012-10-02 Add for Tacoma - Batch must be per product
                                            and sub.StorageUnitId = @StorageUnitId
         where b.Batch = @batch
      end
    end
    else
      select @batchId = b.BatchId
        from Batch              b (nolock)
        join StorageUnitBatch sub (nolock) on b.BatchId         = sub.BatchId    -- GS 2012-10-02 Add for Tacoma - Batch must be per product
                                          and sub.StorageUnitId = @StorageUnitId
       where b.Batch = @batch
         and b.BOELineNumber is not null
    
    if @InboundDocumentTypeCode = 'BOND'
	   begin
	     select @InboundLineId = il.InboundLineId,
	            @BOELineNumber = il.BOELineNumber
	       from InboundLine     il (nolock),
	            Batch            b (nolock)
	      where il.InboundDocumentId = @InboundDocumentId
	        and b.BillOfEntry + b.BOELineNumber = @Batch
	        and b.BOELineNumber = il.BOELineNumber
	        and exists(select top 1 1
	                     from ReceiptLine rl (nolock)
	                    where rl.ReceiptId = @ReceiptId
	                      and rl.InboundLineId = il.InboundLineId
	                      and rl.RequiredQuantity > isnull(ReceivedQuantity, 0))
	     
	     if @@ROWCOUNT = 0
	     begin
	       set @Error = -1
	       goto error
	     end
	   end
  	 
    if @batchId is null
	   begin
	     if ((select dbo.ufn_Configuration(13,@WarehouseId)) = 1) --or (@InboundDocumentTypeCode = 'BOND')
	     begin
        select @Error = -1
        goto error
      end
      
	     if @ReceiptId is not null
	     begin
	       if (select DefaultQC
	             from StorageUnitExternalCompany (nolock)
	            where StorageUnitId     = @StorageUnitId
	              and ExternalCompanyId = @ExternalCompanyId) = 1
	       or (select DefaultQC
              from StorageUnit (nolock)
             where StorageUnitId     = @StorageUnitId) = 1
	       begin
	         select @StatusId = StatusId
	           from Status (nolock)
	          where Type       = 'B'
	            and StatusCode = 'QC'
	       end
	     end
    	 
	     if @StatusId is null
	     begin
	       select @StatusId = StatusId
	         from Status        s (nolock)
	         join Configuration c (nolock) on s.StatusCode = convert(nvarchar(10), c.Value)
	        where s.Type = 'B'
	          and c.ConfigurationId = 127
	     end
	     
  	   exec @Error = p_Batch_Insert
  	    @BatchId     = @BatchId output,
  	    @StatusId    = @StatusId,
       @WarehouseId = @WarehouseId,
       @Batch       = @Batch,
       @CreateDate  = @GetDate,
       @ExpiryDate  = @ExpiryDate,
       @ReferenceNumber = @ReferenceNumber
      
      if @Error <> 0
        goto error
      
  	   if @InboundDocumentTypeCode = 'BOND'
  	   begin
  	     update new
  	        set BatchReferenceNumber = old.BatchReferenceNumber,
  	            BillOfEntry          = old.BillOfEntry,
  	            BOELineNumber        = @BOELineNumber
  	       from Batch new,
  	            Batch Old
  	       where new.BatchId = @BatchId
  	         and old.BatchId = @OldBatchId
        
        if @Error <> 0
          goto error
  	   end
	   end
	   
    select @StorageUnitBatchId = StorageUnitBatchId
      from StorageUnitBatch
     where StorageUnitId = @StorageUnitId
       and BatchId       = @BatchId
    
    if @StorageUnitBatchId is null
    begin
      exec @Error = p_StorageUnitBatch_Insert
       @StorageUnitBatchId = @StorageUnitBatchId output,
       @StorageUnitId      = @StorageUnitId,
       @BatchId            = @BatchId
      
      if @Error <> 0
        goto error
    end
    
    select @ChildStorageUnitId      = il.ChildStorageUnitId
      from InboundLine       il (nolock)
     where il.InboundLineId = @InboundLineId
    
    select @ChildStorageUnitBatchId = sub.StorageUnitBatchId,
           @ChildProductId          = su.ProductId
      from StorageUnitBatch sub (nolock)
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
     where sub.StorageUnitId = @ChildStorageUnitId
       and sub.BatchId       = @BatchId
    
    if @ChildStorageUnitBatchId is null and @ChildStorageUnitId is not null
    begin
      exec @Error = p_StorageUnitBatch_Insert
       @StorageUnitBatchId = @ChildStorageUnitBatchId output,
       @StorageUnitId      = @ChildStorageUnitId,
       @BatchId            = @BatchId
      
      if @Error <> 0
        goto error
      
      insert ChildProduct (ChildStorageUnitBatchId, ParentStorageUnitBatchId)
      select distinct rl.ChildStorageUnitBatchId,rl.StorageUnitBatchId
        from ReceiptLine  rl (nolock)
        left
        join ChildProduct cp (nolock) on rl.StorageUnitBatchId = cp.ParentStorageUnitBatchId
                                     and rl.ChildStorageUnitBatchId = cp.ChildStorageUnitBatchId
       where cp.ChildProductId is null
         and rl.ChildStorageUnitBatchId is not null
         and rl.ReceiptLineId = @receiptLineId
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
    end
    
    if @ChildProductId is not null
    begin
      exec @Error = p_Product_Update
       @ProductId = @ChildProductId,
       @ProductType = 'FPC'
      
      if @Error <> 0
        goto error
    end
    
    exec @Error = p_ReceiptLine_Update
     @ReceiptLineId           = @ReceiptLineId,
     @ChildStorageUnitBatchId = @ChildStorageUnitBatchId
    
    if @Error <> 0
      goto error
  end
  
    update Receipt
       set ReceivingStarted = @GetDate
     where ReceiptId = @ReceiptId
       and ReceivingStarted is null
  
  update ReceiptLine
     set ReceivedDate = @GetDate
   where ReceiptlineId = @ReceiptlineId
     and ReceivedDate is null
  
  if @ExpiryDate is not null
  begin
    if @BatchId is null
      set @BatchId = @OldBatchId
    
    exec @Error = p_Batch_Update
     @BatchId    = @BatchId,
     @ExpiryDate = @ExpiryDate
    
    if @Error <> 0
      goto error
  end
  
  if @ReceiptLineId is not null
  begin
    insert InterfaceExportHeader
          (PrimaryKey,
           OrderNumber,
           ReceiptId,
           RecordType,
           RecordStatus,
           PrincipalCode)
    select convert(nvarchar(10), id.InboundDocumentId),
           id.OrderNumber,
           r.ReceiptId,
           'POSTARTED',
           'N',
           p.PrincipalCode
      from Receipt               r (nolock)
      join InboundDocument      id (nolock) on r.InboundDocumentId = id.InboundDocumentId
      join Principal             p (nolock) on id.PrincipalId = p.PrincipalId
      left
      join InterfaceExportHeader h (nolock) on id.OrderNumber = h.OrderNumber -- Only insert 1 record per Inbound Document
                                           and h.RecordType = 'POSTARTED'
     where r.ReceiptId = @ReceiptId
       and h.InterfaceExportHeaderId is null
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
    
    update Receipt
       set DeliveryDate = @GetDate
     where ReceiptId = @ReceiptId
       and DeliveryDate is null
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
    
    if ISNULL(@reasonId, -1) != -1
    begin
      exec @Error = p_InboundLine_Update
       @InboundLineId = @InboundLineId,
       @reasonId      = @reasonId
      
  if @Error <> 0
        goto error
    end
    
    if exists(select top 1 1
                from ReceiptLine rl (nolock)
               where ReceiptLineId = @ReceiptLineId
                 and StorageUnitBatchId != @StorageUnitBatchId
                 and isnull(ReceivedQuantity, 0) = 0)
    begin
      select @NewReceiptLineId = ReceiptLineId
        from ReceiptLine rl (nolock)
        join Status       s (nolock) on rl.StatusId = s.StatusId
                                    and s.StatusCode = 'W'
       where ReceiptId = @ReceiptId
         and StorageUnitBatchId = @StorageUnitBatchId
      
      if @NewReceiptLineId is not null
      begin
        select @StatusId = dbo.ufn_StatusId('R','W')
        
        exec @Error = p_ReceiptLine_Update
         @ReceiptLineId        = @ReceiptLineId,
         @StatusId             = @StatusId
        
        if @Error <> 0
          goto error
        
        select @receiptLineId = @NewReceiptLineId
      end
    end
    
    update r
       set LocationId = al.LocationId
      from Receipt       r (nolock),
           Area          a (nolock),
           AreaLocation al (nolock)
     where r.ReceiptId   = @ReceiptId
       and a.WarehouseId = @WarehouseId
       and a.AreaId = al.AreaId
       and a.AreaType  = 'QC'
       and a.AreaCode  = 'R'
       and r.LocationId is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    -- If operators warehouse is not the same - make null
    update r
       set LocationId = null
      from Receipt       r (nolock)
      join AreaLocation al (nolock) on r.LocationId = al.LocationId
      join Area          a (nolock) on al.AreaId    = a.AreaId
     where r.ReceiptId   = @ReceiptId
       and a.WarehouseId != @WarehouseId
    
    update r
       set LocationId = al.LocationId
      from Receipt       r (nolock),
           Area          a (nolock),
           AreaLocation al (nolock)
     where r.ReceiptId   = @ReceiptId
       and a.WarehouseId = @WarehouseId
       and a.AreaId = al.AreaId
       and a.AreaCode = 'R'
       and r.LocationId is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    --update subl
    --   set ActualQuantity = case when subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0) - case when (isnull(rl.SampleQuantity,0) + isnull(rl.AssaySamples,0)) > @RetentionSamples then (isnull(rl.SampleQuantity,0) + isnull(rl.AssaySamples,0)) - @RetentionSamples else 0 end) < 0
    --                             then 0
    --                             else subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0) - case when (isnull(rl.SampleQuantity,0) + isnull(rl.AssaySamples,0)) > @RetentionSamples then (isnull(rl.SampleQuantity,0) + isnull(rl.AssaySamples,0)) - @RetentionSamples else 0 end)
    --                             end
    --  from @TableResult               tr
    --  join ReceiptLine                rl on tr.ReceiptLineId        = rl.ReceiptLineId
    --  join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    --  join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
    --                                    and subl.LocationId         = r.LocationId
    -- where rl.ReceiptLineId = @ReceiptLineId
    
    --select @Error = @@Error
    
    --if @Error <> 0
    --  goto error
    
    if @quantity is not null
      select @StatusId = dbo.ufn_StatusId('R','R')
    
    select @AssaySamples = AssaySamples
      from Product (nolock)
     where ProductId = @ProductId
    
    if @SampleQuantity > 0 and @AssaySamples > 0
    begin
      select @SampleQuantity = @SampleQuantity - @AssaySamples
      
      if @SampleQuantity < 0
        set @SampleQuantity = 0
    end
    
    if dbo.ufn_Configuration(422, @WarehouseId) = 1
    begin
      select @ReceivedQuantity   = isnull(rl.AcceptedQuantity,0) + isnull(rl.RejectQuantity,0) + @quantity,
             @AcceptedQuantity   = isnull(rl.AcceptedQuantity,0) + @quantity
        from ReceiptLine rl
       where ReceiptLineId = @ReceiptLineId
    end
    else
    begin
      select @ReceivedQuantity   = @quantity,
             @AcceptedQuantity   = @quantity
    end
    
    if exists(select top 1 1
                from ReceiptLine rl
               where ReceiptLineId = @receiptLineId
                 and InboundLineId = @InboundLineId)
    begin
      exec @Error = p_ReceiptLine_Update
       @ReceiptLineId        = @ReceiptLineId,
       @StorageUnitBatchId   = @StorageUnitBatchId,
       @StatusId             = @StatusId,
       @OperatorId           = @OperatorId,
       @ReceivedQuantity     = @ReceivedQuantity,
       @AcceptedQuantity     = @AcceptedQuantity,
       @RejectQuantity       = @rejectQuantity,
       @DeliveryNoteQuantity = @quantity,
       @SampleQuantity       = @sampleQuantity,
       @AssaySamples         = @AssaySamples,
       @NumberOfPallets	     = @NumberOfPallets,
       @InboundLineId        = @InboundLineId
    end
    else
    begin
      select @StatusId = dbo.ufn_StatusId('R','W')
      
      select @RequiredQuantity = SUM(il.Quantity)
        from InboundLine il (nolock)
       where il.InboundLineId = @InboundLineId
      
      select @RequiredQuantity = isnull(@RequiredQuantity, 0) - SUM(rl.RequiredQuantity)
        from ReceiptLine rl (nolock)
       where rl.InboundLineId = @InboundLineId
      
      if @RequiredQuantity <= 0
        set @RequiredQuantity = @ReceivedQuantity
      
      exec @Error = p_ReceiptLine_Insert
       @ReceiptLineId        = @ReceiptLineId output,
       @ReceiptId            = @ReceiptId,
       @StorageUnitBatchId   = @StorageUnitBatchId,
       @StatusId             = @StatusId,
       @OperatorId           = @OperatorId,
       @RequiredQuantity     = @RequiredQuantity,
       @ReceivedQuantity     = @ReceivedQuantity,
       @AcceptedQuantity     = @AcceptedQuantity,
       @RejectQuantity       = @rejectQuantity,
       @DeliveryNoteQuantity = @quantity,
       @SampleQuantity       = @sampleQuantity,
       @AssaySamples         = @AssaySamples,
       @NumberOfPallets	     = @NumberOfPallets,
       @InboundLineId        = @InboundLineId
    end
    
    if @Error <> 0
      goto error
    
    if dbo.ufn_Configuration(359, @WarehouseId) = 1
    begin
      update ReceiptLine
         set AcceptedQuantity = isnull(ReceivedQuantity,0) - isnull(RejectQuantity,0)
       where ReceiptLineId = @ReceiptLineId
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
    end
    
    if @quantity is not null
    begin
      exec @Error = p_Receiving_DeliveryNoteQuantity_Update
       @ReceiptId = @ReceiptId,
       @InboundLineId = @InboundLineId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Receiving_RequiredQuantity_Update
       @ReceiptId = @ReceiptId,
       @InboundLineId = @InboundLineId
      
      if @Error <> 0
        goto error
    end
    
    select @OverReceipt = p.OverReceipt
      from StorageUnit su (nolock)
      join Product      p (nolock) on su.ProductId = p.ProductId
     where su.StorageUnitId = @StorageUnitId
    
    if @OverReceipt is null
      set @OverReceipt = 0
    
    if isnull(@OverReceiptDoc,0) != 1
    if (select ((convert(numeric(13,3),sum(ReceivedQuantity))/convert(numeric(13,3),sum(RequiredQuantity)))*convert(numeric(13,3),100))
          from ReceiptLine       rl (nolock)
          join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
         where rl.ReceiptId = @ReceiptId
           and sub.StorageUnitId = @StorageUnitId
           and rl.InboundLineId = @inboundLineId) > @OverReceipt + 100
      goto error
      
    --select 'Pre Update', Location, ActualQuantity, AllocatedQuantity, ReservedQuantity from viewSOH where ProductCode = 'GR00-Afr-S' and AreaCode = 'R'
    --update subl
    --   set ActualQuantity = subl.ActualQuantity + ((isnull(rl.AcceptedQuantity,0) - tr.AcceptedQuantity) - case when ((isnull(rl.SampleQuantity,0) - isnull(tr.SampleQuantity, 0)) + (isnull(rl.AssaySamples,0) - isnull(tr.AssaySamples, 0))) > @RetentionSamples then ((isnull(rl.SampleQuantity,0) - isnull(tr.SampleQuantity, 0)) + (isnull(rl.AssaySamples,0) - isnull(tr.AssaySamples, 0))) - @RetentionSamples else 0 end)
   --  from @TableResult               tr
    --  join ReceiptLine                rl on tr.ReceiptLineId        = rl.ReceiptLineId
    --  join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    --  join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
    --                                    and subl.LocationId         = r.LocationId
    -- where rl.ReceiptLineId = @ReceiptLineId
    
    --select @Error = @@Error

    --if @Error <> 0
    --  goto error

    -- delete subl
    --   from ReceiptLine                rl
    --   join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    --   join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
    --                                     and subl.LocationId         = r.LocationId
    --  where rl.ReceiptLineId = @ReceiptLineId
    --    and ActualQuantity    = 0
    --    and AllocatedQuantity = 0
    --    and ReservedQuantity  = 0
    
    --select @Error = @@Error

    --if @Error <> 0
    --  goto error
        
    -- select @Error = @@Error
        
    -- if @Error <> 0
    --   goto error
    --select 'Post Update', Location, ActualQuantity, AllocatedQuantity, ReservedQuantity from viewSOH where ProductCode = 'GR00-Afr-S' and AreaCode = 'R'
    --select @Error = @@Error, @Rowcount = @@rowcount
    
    ----if @Rowcount = 0
    ----begin
    --  insert StorageUnitBatchLocation
    --        (StorageUnitBatchId,
    --         LocationId,
    --         ActualQuantity,
    --         AllocatedQuantity,
    --         ReservedQuantity)
    --  select rl.StorageUnitBatchId,
    --         r.LocationId,
    --         rl.AcceptedQuantity  - case when ((isnull(tr.SampleQuantity, 0) - isnull(rl.SampleQuantity,0)) + (isnull(tr.AssaySamples, 0) - isnull(rl.AssaySamples,0))) > @RetentionSamples then ((isnull(tr.SampleQuantity, 0) - isnull(rl.SampleQuantity,0)) + (isnull(tr.AssaySamples, 0) - isnull(rl.AssaySamples,0))) - @RetentionSamples else 0 end,
    --         0,
    --         0
    --    from @TableResult               tr
    --    join ReceiptLine                rl on tr.ReceiptLineId      = rl.ReceiptLineId
    --    join Receipt                     r on rl.ReceiptId          = r.ReceiptId
    --    left
    --    join StorageUnitBatchLocation subl on r.LocationId          = subl.LocationId
    --                                      and rl.StorageUnitBatchId = subl.StorageUnitBatchId
    --   where rl.ReceiptLineId = @ReceiptLineId
    --     and r.LocationId is not null
    --     and tr.AcceptedQuantity = 0
    --     and subl.LocationId is null
      
    --  select @Error = @@Error
      
    --  if @Error <> 0
    --    goto error
    ----end
    
    if @Error <> 0
      goto error
  end
  
  if @storeLocation is not null
  begin
    select @ScannedLocationId = LocationId
      from viewLocation (nolock)
     where (Location    = @storeLocation or SecurityCode = @storeLocation)
	       and WarehouseId = @WarehouseId
    
    if exists(select 1
                from StorageUnitArea sua (nolock)
                join AreaLocation al (nolock) on sua.AreaId = al.AreaId
               where al.LocationId = @ScannedLocationId
                 and sua.StorageUnitId = @StorageUnitId)
    begin
       if @PalletId is null
         exec @Error = p_Pallet_Insert
          @PalletId           = @PalletId output,
          @StorageUnitBatchId = @StorageUnitBatchId,
          @LocationId         = @ScannedLocationId,
          @Weight             = null,
          @Tare               = null,
          @Quantity           = 1,
          @Prints             = 1
       
         if @Error <> 0
           goto error
       
       exec @Error = p_Receiving_Palletise @ReceiptLineId = @receiptLineId, @palletId = @palletId, @WarehouseId = @WarehouseId --Added Daniel Schotter: Control by WarehouseId
       
       if @Error <> 0
         goto error
       
       set @InstructionId = -1

       while @InstructionId is not null
       begin
          set @InstructionId = null
          
          select @InstructionId = min(InstructionId)
            from Instruction (nolock)
           where ReceiptLineId = @ReceiptLineId
             and isnull(StoreLocationId,-1) != @ScannedLocationId
          
          if @InstructionId is not null
          begin

          exec @Error = p_StorageUnitBatchLocation_Deallocate
           @instructionId = @instructionId
  
          if @Error <> 0
          begin
            set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Deallocate'
            goto error
          end
    
          exec @Error = p_Instruction_Update
            @instructionId      = @instructionId,
            @storeLocationId    = @ScannedLocationId
    
          if @Error <> 0
          begin
            set @ErrorMsg = 'Error executing p_Instruction_Update'
            goto error
          end

          exec @Error = p_StorageUnitBatchLocation_Reserve
           @instructionId = @instructionId
  
          if @Error <> 0
          begin
            set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
            goto error
          end

          exec @Error = p_Instruction_Started
           @instructionId = @instructionId,
           @operatorId = @operatorId
  
          if @Error <> 0
          begin
            set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
            goto error
          end

          exec @Error = p_Instruction_Finished
           @instructionId = @instructionId,
           @operatorId = @operatorId
  
          if @Error <> 0
          begin
            set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
            goto error
          end
          end
       end
       
       select @StoreLocationId = @ScannedLocationId
       select @PalletId = null
    end
    
    if @ScannedLocationId = @StoreLocationId
    begin
      --update subl
      --   set ActualQuantity = subl.ActualQuantity - (tr.AcceptedQuantity - isnull(rl.AcceptedQuantity,0))
      --  from @TableResult               tr
      --  join ReceiptLine                rl on tr.ReceiptLineId        = rl.ReceiptLineId
      --  join Receipt                     r on rl.ReceiptId            = r.ReceiptId
      --  join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
      --                                    and subl.LocationId         = r.LocationId
      -- where rl.ReceiptLineId = @ReceiptLineId
      
      --select @Error = @@Error, @rowcount = @@rowcount
      
      --if @Error <> 0
      --  goto error
      
      --if @rowcount = 0
      ----begin
      --  delete subl
      --    from ReceiptLine                rl
      --    join Receipt                     r on rl.ReceiptId            = r.ReceiptId
      --    join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
      --                                      and subl.LocationId         = r.LocationId
      --   where rl.ReceiptLineId = @ReceiptLineId
      --     and ActualQuantity    = 0
      --     and AllocatedQuantity = 0
      --     and ReservedQuantity  = 0
        
 --  select @Error = @@Error
        
      --  if @Error <> 0
      --    goto error
      ----end
    
    update subl
       set ActualQuantity = subl.ActualQuantity + (rl.AcceptedQuantity - isnull(rl.AcceptedQuantity,0)) - ((isnull(tr.SampleQuantity, 0) - isnull(rl.SampleQuantity,0)) + (isnull(tr.AssaySamples, 0) - isnull(rl.AssaySamples,0)))
      from @TableResult               tr
      join ReceiptLine                rl on tr.ReceiptLineId        = rl.ReceiptLineId
      join Receipt                     r on rl.ReceiptId            = r.ReceiptId
      join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                        and subl.LocationId         = r.StagingLocationId
     where rl.ReceiptLineId = @ReceiptLineId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    --if @@rowcount = 0
    --begin
      insert StorageUnitBatchLocation
            (StorageUnitBatchId,
             LocationId,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select rl.StorageUnitBatchId,
             r.StagingLocationId,
             rl.AcceptedQuantity,
             0,
             0
        from @TableResult               tr
        join ReceiptLine                rl on tr.ReceiptLineId      = rl.ReceiptLineId
        join Receipt                     r on rl.ReceiptId          = r.ReceiptId
        left
        join StorageUnitBatchLocation subl on r.LocationId          = subl.LocationId
                                          and rl.StorageUnitBatchId = subl.StorageUnitBatchId
       where rl.ReceiptLineId = @ReceiptLineId
         and r.StagingLocationId is not null
         and tr.AcceptedQuantity = 0
         and subl.LocationId is null
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
    --end
    end
    else
    begin
      select @Error = -1
      goto error
    end
  end
  
  if @palletId is not null
  begin
  
    exec @Error = p_Palletise_Reverse
     @ReceiptId          = @ReceiptId,
     @ReceiptLineId      = @ReceiptLineId
    
    if @error <> 0
      goto error
    
    exec @Error = p_Receiving_Palletise @ReceiptLineId = @receiptLineId, @palletId = @palletId, @WarehouseId = @WarehouseId --Added Daniel Schotter: To control by WarehouseId
    
    if @Error <> 0
      goto error
    
    if isnumeric(replace(@palletId,'P:','')) = 1
      set @IntPalletId = replace(@palletId,'P:','')
    
    if @IntPalletId is not null
    if dbo.ufn_Configuration(119, @WarehouseId) = 1
    begin
      select top 1 @InstructionId = InstructionId
        from Instruction (nolock)
       where ReceiptLineId = @receiptLineId
         and PalletId = @IntPalletId
      order by InstructionId desc
      
      exec @Error = p_Instruction_CrossDocking
       @InstructionId = @InstructionId,
       @OperatorId    = @OperatorId,
       @PalletId      = @IntPalletId
      
      if @Error <> 0
        goto error
    end
  end
  
  update subl
     set ActualQuantity = subl.ActualQuantity + ((isnull(rl.AcceptedQuantity,0) - tr.AcceptedQuantity) - case when ((isnull(rl.SampleQuantity,0) - isnull(tr.SampleQuantity, 0)) + (isnull(rl.AssaySamples,0) - isnull(tr.AssaySamples, 0))) > @RetentionSamples then ((isnull(rl.SampleQuantity,0) - isnull(tr.SampleQuantity, 0)) + (isnull(rl.AssaySamples,0) - isnull(tr.AssaySamples, 0))) - @RetentionSamples else 0 end)
    from @TableResult               tr
    join ReceiptLine                rl on tr.ReceiptLineId        = rl.ReceiptLineId
    join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                      and subl.LocationId         = r.LocationId
   where rl.ReceiptLineId = @ReceiptLineId
    
  select @Error = @@Error

  if @Error <> 0
    goto error

   delete subl
     from ReceiptLine                rl
     join Receipt                     r on rl.ReceiptId            = r.ReceiptId
     join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                       and subl.LocationId         = r.LocationId
    where rl.ReceiptLineId = @ReceiptLineId
      and ActualQuantity    = 0
      and AllocatedQuantity = 0
      and ReservedQuantity  = 0
    
  select @Error = @@Error

  if @Error <> 0
    goto error
        
   select @Error = @@Error
        
   if @Error <> 0
     goto error
  
  select @Error = @@Error, @Rowcount = @@rowcount
    
  --if @Rowcount = 0
  --begin
    insert StorageUnitBatchLocation
          (StorageUnitBatchId,
           LocationId,
           ActualQuantity,
           AllocatedQuantity,
           ReservedQuantity)
    select rl.StorageUnitBatchId,
           r.LocationId,
           isnull(rl.AcceptedQuantity,0) - case when ((isnull(tr.SampleQuantity, 0) - isnull(rl.SampleQuantity,0)) + (isnull(tr.AssaySamples, 0) - isnull(rl.AssaySamples,0))) > @RetentionSamples then ((isnull(tr.SampleQuantity, 0) - isnull(rl.SampleQuantity,0)) + (isnull(tr.AssaySamples, 0) - isnull(rl.AssaySamples,0))) - @RetentionSamples else 0 end,
           0,
           0
      from @TableResult               tr
      join ReceiptLine                rl on tr.ReceiptLineId      = rl.ReceiptLineId
      join Receipt                     r on rl.ReceiptId          = r.ReceiptId
      left
      join StorageUnitBatchLocation subl on r.LocationId          = subl.LocationId
                                        and rl.StorageUnitBatchId = subl.StorageUnitBatchId
     where rl.ReceiptLineId = @ReceiptLineId
       and r.LocationId is not null
       and tr.AcceptedQuantity = 0
       and subl.LocationId is null
      
    select @Error = @@Error
      
    if @Error <> 0
      goto error
  --end
  
  commit transaction
  
  if (select dbo.ufn_Configuration(256,@WarehouseId)) = 1
  begin
    select @ReceivedQuantity = sum(RequiredQuantity) - sum(ReceivedQuantity)
      from ReceiptLine (nolock)
     where ReceiptId        = @ReceiptId
       and ReceivedQuantity > 0
    
    if @@rowcount > 0 and @ReceivedQuantity = 0
    begin
      exec @Error = p_Inbound_Release
       @inboundShipmentId = -1,
       @receiptId         = @ReceiptId,
       @receiptLineId     = -1,
       @statusCode        = N'RC'
      
      if @Error <> 0
        goto error
    end
  end
  
  update MobileLog
     set EndDate    = getdate(),
         ErrorMsg   = @Errormsg
   where MobileLogId = @MobileLogId
  
  select @receiptLineId as '@receiptLineId'
  return @receiptLineId
  error:
    rollback transaction
    
    update MobileLog
       set EndDate    = getdate(),
           ErrorMsg   = @Errormsg
     where MobileLogId = @MobileLogId
    
    set @receiptLineId = -1
    select @receiptLineId
    return @receiptLineId
end
