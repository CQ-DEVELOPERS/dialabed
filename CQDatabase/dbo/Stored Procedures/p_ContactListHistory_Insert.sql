﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactListHistory_Insert
  ///   Filename       : p_ContactListHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:18:12
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ContactListHistory table.
  /// </remarks>
  /// <param>
  ///   @ContactListId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @OperatorId int = null,
  ///   @ContactPerson nvarchar(510) = null,
  ///   @Telephone nvarchar(510) = null,
  ///   @Fax nvarchar(510) = null,
  ///   @EMail nvarchar(510) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @Alias nvarchar(100) = null,
  ///   @Type nvarchar(40) = null,
  ///   @ReportTypeId int = null 
  /// </param>
  /// <returns>
  ///   ContactListHistory.ContactListId,
  ///   ContactListHistory.ExternalCompanyId,
  ///   ContactListHistory.OperatorId,
  ///   ContactListHistory.ContactPerson,
  ///   ContactListHistory.Telephone,
  ///   ContactListHistory.Fax,
  ///   ContactListHistory.EMail,
  ///   ContactListHistory.CommandType,
  ///   ContactListHistory.InsertDate,
  ///   ContactListHistory.Alias,
  ///   ContactListHistory.Type,
  ///   ContactListHistory.ReportTypeId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContactListHistory_Insert
(
 @ContactListId int = null,
 @ExternalCompanyId int = null,
 @OperatorId int = null,
 @ContactPerson nvarchar(510) = null,
 @Telephone nvarchar(510) = null,
 @Fax nvarchar(510) = null,
 @EMail nvarchar(510) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @Alias nvarchar(100) = null,
 @Type nvarchar(40) = null,
 @ReportTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ContactListHistory
        (ContactListId,
         ExternalCompanyId,
         OperatorId,
         ContactPerson,
         Telephone,
         Fax,
         EMail,
         CommandType,
         InsertDate,
         Alias,
         Type,
         ReportTypeId)
  select @ContactListId,
         @ExternalCompanyId,
         @OperatorId,
         @ContactPerson,
         @Telephone,
         @Fax,
         @EMail,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @Alias,
         @Type,
         @ReportTypeId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
