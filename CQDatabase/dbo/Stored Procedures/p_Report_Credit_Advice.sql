﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Credit_Advice
  ///   Filename       : p_Report_Credit_Advice.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Credit_Advice
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @InboundShipmentId     int = null,
 @ReceiptId             int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        InboundShipmentId         int,
        ReceiptLineId             int,
        InboundLineId             int,
        ExternalCompanyId         int,
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        OrderNumber               nvarchar(30),
        DeliveryDate              datetime,
        DeliveryNoteNumber        nvarchar(30),
        StorageUnitBatchId        int,
        ProductCode               nvarchar(30),
        Product                   nvarchar(50),
        SKUCode                   nvarchar(50),
        Quantity                  float,
        DeliveryNoteQuantity      float,
        ReceivedQuantity          float,
        CreditAdviceQuantity      float,
        ReasonId                  int,
        Reason                    nvarchar(50),
        Exception                 nvarchar(255)
       );
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  declare @StatusId int
  
  if @InboundShipmentId is null
    insert @TableResult
          (ReceiptLineId,
           InboundLineId,
           DeliveryDate,
           DeliveryNoteNumber,
           StorageUnitBatchId,
           DeliveryNoteQuantity,
           ReceivedQuantity)
    select rl.ReceiptLineId,
           rl.InboundLineId,
           r.DeliveryDate,
           r.DeliveryNoteNumber,
           rl.StorageUnitBatchId,
           rl.DeliveryNoteQuantity,
           rl.ReceivedQuantity
      from ReceiptLine     rl (nolock)
      join Receipt          r  (nolock) on rl.ReceiptId = r.ReceiptId
     where r.ReceiptId             = isnull(@ReceiptId, r.ReceiptId)
       --and r.CreditAdviceIndicator = 1
       and DeliveryNoteQuantity > ReceivedQuantity
  else
    insert @TableResult
          (InboundShipmentId,
           ReceiptLineId,
           InboundLineId,
           DeliveryDate,
           DeliveryNoteNumber,
           StorageUnitBatchId,
           DeliveryNoteQuantity,
           ReceivedQuantity)
    select isr.InboundShipmentId,
           rl.ReceiptLineId,
           rl.InboundLineId,
           r.DeliveryDate,
           r.DeliveryNoteNumber,
           rl.StorageUnitBatchId,
           rl.DeliveryNoteQuantity,
           rl.ReceivedQuantity
      from ReceiptLine     rl (nolock)
      join Receipt          r  (nolock) on rl.ReceiptId = r.ReceiptId
      join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
     where isr.InboundShipmentId   = isnull(@InboundShipmentId, isr.InboundShipmentId)
       --and r.CreditAdviceIndicator = 1
       and DeliveryNoteQuantity > ReceivedQuantity
  
  update r
     set OrderNumber       = id.OrderNumber,
         Quantity          = il.Quantity,
         ExternalCompanyId = id.ExternalCompanyId
    from @TableResult             r
    join InboundLine              il (nolock) on r.InboundLineId = il.InboundLineId
    join InboundDocument          id (nolock) on il.InboundDocumentId = id.InboundDocumentId
  
  update r
     set SupplierCode = ec.ExternalCompanyCode,
         Supplier     = ec.ExternalCompany
    from @TableResult             r
    join ExternalCompany ec (nolock) on r.ExternalCompanyId = ec.ExternalCompanyId
  
  update t
     set ReasonId = r.ReasonId,
         Reason     = r.Reason,
         Exception  = e.Exception
    from @TableResult t
    join Exception    e (nolock) on t.ReceiptLineId = e.ReceiptLineId
    join Reason       r (nolock) on e.ReasonId      = r.ReasonId
  
  update t
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode
    from @TableResult t
    join StorageUnitBatch sub (nolock) on t.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
  
  update @TableResult
     set CreditAdviceQuantity = DeliveryNoteQuantity - ReceivedQuantity
  
  select InboundShipmentId,
         ReceiptLineId,
         SupplierCode,
         Supplier,
         OrderNumber,
         DeliveryDate,
         DeliveryNoteNumber,
         ProductCode,
         Product,
         SKUCode,
         Quantity,
         DeliveryNoteQuantity,
         ReceivedQuantity,
         CreditAdviceQuantity,
         isnull(ReasonId,0) as 'ReasonId',
         Reason,
         Exception
    from @TableResult
end
