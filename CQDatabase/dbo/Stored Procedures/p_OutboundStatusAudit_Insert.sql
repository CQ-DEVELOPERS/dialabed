﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundStatusAudit_Insert
  ///   Filename       : p_OutboundStatusAudit_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:36
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundStatusAudit table.
  /// </remarks>
  /// <param>
  ///   @IssueId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @CreateDate datetime = null,
  ///   @PlanningComplete datetime = null,
  ///   @Checking datetime = null,
  ///   @Checked datetime = null,
  ///   @Despatch datetime = null,
  ///   @DespatchChecked datetime = null,
  ///   @Complete datetime = null,
  ///   @PickComplete int = null,
  ///   @PickTotal int = null,
  ///   @PickPercentage numeric(13,3) = null,
  ///   @CheckingComplete int = null,
  ///   @CheckingTotal int = null,
  ///   @CheckingPercentage numeric(13,3) = null 
  /// </param>
  /// <returns>
  ///   OutboundStatusAudit.IssueId,
  ///   OutboundStatusAudit.ExternalCompanyId,
  ///   OutboundStatusAudit.CreateDate,
  ///   OutboundStatusAudit.PlanningComplete,
  ///   OutboundStatusAudit.Checking,
  ///   OutboundStatusAudit.Checked,
  ///   OutboundStatusAudit.Despatch,
  ///   OutboundStatusAudit.DespatchChecked,
  ///   OutboundStatusAudit.Complete,
  ///   OutboundStatusAudit.PickComplete,
  ///   OutboundStatusAudit.PickTotal,
  ///   OutboundStatusAudit.PickPercentage,
  ///   OutboundStatusAudit.CheckingComplete,
  ///   OutboundStatusAudit.CheckingTotal,
  ///   OutboundStatusAudit.CheckingPercentage 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundStatusAudit_Insert
(
 @IssueId int = null,
 @ExternalCompanyId int = null,
 @CreateDate datetime = null,
 @PlanningComplete datetime = null,
 @Checking datetime = null,
 @Checked datetime = null,
 @Despatch datetime = null,
 @DespatchChecked datetime = null,
 @Complete datetime = null,
 @PickComplete int = null,
 @PickTotal int = null,
 @PickPercentage numeric(13,3) = null,
 @CheckingComplete int = null,
 @CheckingTotal int = null,
 @CheckingPercentage numeric(13,3) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OutboundStatusAudit
        (IssueId,
         ExternalCompanyId,
         CreateDate,
         PlanningComplete,
         Checking,
         Checked,
         Despatch,
         DespatchChecked,
         Complete,
         PickComplete,
         PickTotal,
         PickPercentage,
         CheckingComplete,
         CheckingTotal,
         CheckingPercentage)
  select @IssueId,
         @ExternalCompanyId,
         @CreateDate,
         @PlanningComplete,
         @Checking,
         @Checked,
         @Despatch,
         @DespatchChecked,
         @Complete,
         @PickComplete,
         @PickTotal,
         @PickPercentage,
         @CheckingComplete,
         @CheckingTotal,
         @CheckingPercentage 
  
  select @Error = @@Error
  
  
  return @Error
  
end
