﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfacePackInfo_Update
  ///   Filename       : p_InterfacePackInfo_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:36
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfacePackInfo table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @PackCode nvarchar(60) = null,
  ///   @PackDescription nvarchar(510) = null,
  ///   @UOM nvarchar(60) = null,
  ///   @SaleUOM nvarchar(60) = null,
  ///   @Ingredients nvarchar(510) = null,
  ///   @Quantity float = null,
  ///   @UnitSize int = null,
  ///   @Weight float = null,
  ///   @Length int = null,
  ///   @Width int = null,
  ///   @Height int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfacePackInfo_Update
(
 @HostId nvarchar(60) = null,
 @ProductCode nvarchar(60) = null,
 @PackCode nvarchar(60) = null,
 @PackDescription nvarchar(510) = null,
 @UOM nvarchar(60) = null,
 @SaleUOM nvarchar(60) = null,
 @Ingredients nvarchar(510) = null,
 @Quantity float = null,
 @UnitSize int = null,
 @Weight float = null,
 @Length int = null,
 @Width int = null,
 @Height int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfacePackInfo
     set HostId = isnull(@HostId, HostId),
         ProductCode = isnull(@ProductCode, ProductCode),
         PackCode = isnull(@PackCode, PackCode),
         PackDescription = isnull(@PackDescription, PackDescription),
         UOM = isnull(@UOM, UOM),
         SaleUOM = isnull(@SaleUOM, SaleUOM),
         Ingredients = isnull(@Ingredients, Ingredients),
         Quantity = isnull(@Quantity, Quantity),
         UnitSize = isnull(@UnitSize, UnitSize),
         Weight = isnull(@Weight, Weight),
         Length = isnull(@Length, Length),
         Width = isnull(@Width, Width),
         Height = isnull(@Height, Height) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
