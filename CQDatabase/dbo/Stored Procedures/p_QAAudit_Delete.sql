﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QAAudit_Delete
  ///   Filename       : p_QAAudit_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:07
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the QAAudit table.
  /// </remarks>
  /// <param>
  ///   @QAAuditId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QAAudit_Delete
(
 @QAAuditId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete QAAudit
     where QAAuditId = @QAAuditId
  
  select @Error = @@Error
  
  
  return @Error
  
end
