﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Process_Search
  ///   Filename       : p_Process_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Nov 2013 12:48:39
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Process table.
  /// </remarks>
  /// <param>
  ///   @ProcessId int = null output,
  ///   @ProcessCode nvarchar(60) = null,
  ///   @Process nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Process.ProcessId,
  ///   Process.ProcessCode,
  ///   Process.Process 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Process_Search
(
 @ProcessId int = null output,
 @ProcessCode nvarchar(60) = null,
 @Process nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ProcessId = '-1'
    set @ProcessId = null;
  
  if @ProcessCode = '-1'
    set @ProcessCode = null;
  
  if @Process = '-1'
    set @Process = null;
  
 
  select
         Process.ProcessId
        ,Process.ProcessCode
        ,Process.Process
    from Process
   where isnull(Process.ProcessId,'0')  = isnull(@ProcessId, isnull(Process.ProcessId,'0'))
     and isnull(Process.ProcessCode,'%')  like '%' + isnull(@ProcessCode, isnull(Process.ProcessCode,'%')) + '%'
     and isnull(Process.Process,'%')  like '%' + isnull(@Process, isnull(Process.Process,'%')) + '%'
  order by ProcessCode
  
end
