﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingProduct_Update
  ///   Filename       : p_CheckingProduct_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:18
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the CheckingProduct table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber varchar(max) = null,
  ///   @JobId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StorageUnitId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @ProductBarcode nvarchar(100) = null,
  ///   @PackBarcode nvarchar(100) = null,
  ///   @Product nvarchar(510) = null,
  ///   @SKUCode nvarchar(100) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @ExpiryDate datetime = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null,
  ///   @CheckQuantity float = null,
  ///   @Checked int = null,
  ///   @Total int = null,
  ///   @Lines nvarchar(510) = null,
  ///   @InstructionRefId int = null,
  ///   @SkipUpdate bit = null,
  ///   @InsertDate datetime = null,
  ///   @ErrorMsg nvarchar(max) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingProduct_Update
(
 @InstructionId int = null,
 @WarehouseId int = null,
 @OrderNumber varchar(max) = null,
 @JobId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @StorageUnitBatchId int = null,
 @StorageUnitId int = null,
 @ProductCode nvarchar(60) = null,
 @ProductBarcode nvarchar(100) = null,
 @PackBarcode nvarchar(100) = null,
 @Product nvarchar(510) = null,
 @SKUCode nvarchar(100) = null,
 @Batch nvarchar(100) = null,
 @ExpiryDate datetime = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null,
 @CheckQuantity float = null,
 @Checked int = null,
 @Total int = null,
 @Lines nvarchar(510) = null,
 @InstructionRefId int = null,
 @SkipUpdate bit = null,
 @InsertDate datetime = null,
 @ErrorMsg nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update CheckingProduct
     set InstructionId = isnull(@InstructionId, InstructionId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         JobId = isnull(@JobId, JobId),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         ProductCode = isnull(@ProductCode, ProductCode),
         ProductBarcode = isnull(@ProductBarcode, ProductBarcode),
         PackBarcode = isnull(@PackBarcode, PackBarcode),
         Product = isnull(@Product, Product),
         SKUCode = isnull(@SKUCode, SKUCode),
         Batch = isnull(@Batch, Batch),
         ExpiryDate = isnull(@ExpiryDate, ExpiryDate),
         Quantity = isnull(@Quantity, Quantity),
         ConfirmedQuantity = isnull(@ConfirmedQuantity, ConfirmedQuantity),
         CheckQuantity = isnull(@CheckQuantity, CheckQuantity),
         Checked = isnull(@Checked, Checked),
         Total = isnull(@Total, Total),
         Lines = isnull(@Lines, Lines),
         InstructionRefId = isnull(@InstructionRefId, InstructionRefId),
         SkipUpdate = isnull(@SkipUpdate, SkipUpdate),
         InsertDate = isnull(@InsertDate, InsertDate),
         ErrorMsg = isnull(@ErrorMsg, ErrorMsg) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
