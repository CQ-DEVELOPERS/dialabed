﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_Search
  ///   Filename       : p_InstructionType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:54
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null output,
  ///   @InstructionType nvarchar(60) = null,
  ///   @InstructionTypeCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InstructionType.InstructionTypeId,
  ///   InstructionType.PriorityId,
  ///   InstructionType.InstructionType,
  ///   InstructionType.InstructionTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_Search
(
 @InstructionTypeId int = null output,
 @InstructionType nvarchar(60) = null,
 @InstructionTypeCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @InstructionType = '-1'
    set @InstructionType = null;
  
  if @InstructionTypeCode = '-1'
    set @InstructionTypeCode = null;
  
 
  select
         InstructionType.InstructionTypeId
        ,InstructionType.PriorityId
        ,InstructionType.InstructionType
        ,InstructionType.InstructionTypeCode
    from InstructionType
   where isnull(InstructionType.InstructionTypeId,'0')  = isnull(@InstructionTypeId, isnull(InstructionType.InstructionTypeId,'0'))
     and isnull(InstructionType.InstructionType,'%')  like '%' + isnull(@InstructionType, isnull(InstructionType.InstructionType,'%')) + '%'
     and isnull(InstructionType.InstructionTypeCode,'%')  like '%' + isnull(@InstructionTypeCode, isnull(InstructionType.InstructionTypeCode,'%')) + '%'
  
end
