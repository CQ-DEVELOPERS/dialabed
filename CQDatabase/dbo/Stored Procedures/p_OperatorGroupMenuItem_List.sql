﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItem_List
  ///   Filename       : p_OperatorGroupMenuItem_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:25
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroupMenuItem table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OperatorGroupMenuItem.OperatorGroupId,
  ///   OperatorGroupMenuItem.MenuItemId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItem_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as OperatorGroupId
        ,null as 'OperatorGroupMenuItem'
        ,'-1' as MenuItemId
        ,null as 'OperatorGroupMenuItem'
  union
  select
         OperatorGroupMenuItem.OperatorGroupId
        ,OperatorGroupMenuItem.OperatorGroupId as 'OperatorGroupMenuItem'
        ,OperatorGroupMenuItem.MenuItemId
        ,OperatorGroupMenuItem.MenuItemId as 'OperatorGroupMenuItem'
    from OperatorGroupMenuItem
  
end
