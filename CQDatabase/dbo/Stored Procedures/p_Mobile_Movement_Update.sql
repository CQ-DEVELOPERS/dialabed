﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Movement_Update
  ///   Filename       : p_Mobile_Movement_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Movement_Update
(
 @instructionId int,
 @palletId      int,
 @quantity      float
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StorageUnitBatchId  int,
          @StoreLocationId     int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StorageUnitBatchId = StorageUnitBatchId,
         @StoreLocationId    = StoreLocationId
    from Instruction
   where InstructionId = @instructionId
  
  if exists(select 1
              from Pallet
             where PalletId = @PalletId)
  begin
    exec @Error = p_Pallet_Update
     @PalletId           = @palletId,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @LocationId         = @StoreLocationId,
     @Quantity           = @quantity
    
    if @Error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_Pallet_Insert
     @PalletId           = @palletId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @LocationId         = @StoreLocationId,
     @Quantity           = @quantity,
     @Prints             = 1
    
    if @Error <> 0
      goto error
  end
  
  begin transaction
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId       = @instructionId
  
  if @Error <> 0
    goto Error
  
  exec @Error = p_Instruction_Update
   @instructionId       = @instructionId,
   @Quantity            = @quantity,
   @PalletId            = @palletId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @instructionId
  
  if @Error <> 0
    goto Error
  
  commit transaction
  return
  
  error:
    rollback transaction
    return @Error
end
