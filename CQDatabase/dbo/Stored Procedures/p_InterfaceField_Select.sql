﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceField_Select
  ///   Filename       : p_InterfaceField_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:46:48
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceField table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFieldId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceField.InterfaceFieldId,
  ///   InterfaceField.InterfaceFieldCode,
  ///   InterfaceField.InterfaceField,
  ///   InterfaceField.InterfaceDocumentTypeId,
  ///   InterfaceField.Datatype 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceField_Select
(
 @InterfaceFieldId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceField.InterfaceFieldId
        ,InterfaceField.InterfaceFieldCode
        ,InterfaceField.InterfaceField
        ,InterfaceField.InterfaceDocumentTypeId
        ,InterfaceField.Datatype
    from InterfaceField
   where isnull(InterfaceField.InterfaceFieldId,'0')  = isnull(@InterfaceFieldId, isnull(InterfaceField.InterfaceFieldId,'0'))
  order by InterfaceFieldCode
  
end
