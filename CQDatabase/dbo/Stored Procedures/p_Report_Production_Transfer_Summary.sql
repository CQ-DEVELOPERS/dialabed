﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Production_Transfer_Summary
  ///   Filename       : p_Report_Production_Transfer_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Production_Transfer_Summary
(
 @WarehouseId       int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   StorageUnitBatchId int,
   SKUId              int,
   ConfirmedQuantity  float,
   Status             nvarchar(50),
   StatusCode         nvarchar(10),
   StatusOrder        int,
   CreateDate         datetime,
   StartDate          datetime,
   Litres             numeric(13,3)
  )
	 
	 insert @TableResult
        (StorageUnitBatchId,
         ConfirmedQuantity,
         Status,
         StatusCode,
         CreateDate,
         StartDate)
  select i.StorageUnitBatchId,
         i.ConfirmedQuantity,
         s.Status,
         s.StatusCode,
         i.CreateDate,
         i.StartDate
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
    join Job                j (nolock) on i.JobId               = j.JobId
    join Status             s (nolock) on j.StatusId            = s.StatusId
   where it.InstructionTypeCode = 'PR'
     and i.CreateDate between @FromDate and @ToDate
     --and s.StatusCode in ('W','S','F')
  
  update tr
     set Litres = tr.ConfirmedQuantity * sku.Litres
    from @TableResult tr
    join StorageUnitBatch sub on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su on sub.StorageUnitId     = su.StorageUnitId
    join SKU              sku on su.SKUId              = sku.SKUId
  
  update @TableResult
     set StatusOrder = 0
   where StatusCode = 'W'
  
  update @TableResult
     set StatusOrder = 1
   where StatusCode = 'PR'
  
  update @TableResult
     set StatusOrder = 2
   where StatusCode in ('A','S')
  
  update @TableResult
     set StatusOrder = 3
   where StatusCode = 'C'
  
  select vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         tr.Status,
         convert(nvarchar(10), tr.CreateDate, 120) as 'CreateDate',
         sum(tr.ConfirmedQuantity) as 'ConfirmedQuantity',
         sum(tr.Litres) as 'Litres',
         tr.StatusOrder
    from @TableResult    tr
    join viewStock       vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
  group by vs.ProductCode,
           vs.Product,
           vs.SKUCode,
           tr.Status,
           convert(nvarchar(10), tr.CreateDate, 120),
           tr.StatusOrder
  order by convert(nvarchar(10), tr.CreateDate, 120),
           tr.StatusOrder,
           ProductCode
end
