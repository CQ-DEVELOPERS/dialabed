﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RawPallet_Update
  ///   Filename       : p_RawPallet_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:15
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the RawPallet table.
  /// </remarks>
  /// <param>
  ///   @PalletId nvarchar(max) = null,
  ///   @ProductCode nvarchar(max) = null,
  ///   @SKUCode nvarchar(max) = null,
  ///   @Batch nvarchar(max) = null,
  ///   @CreateDate nvarchar(max) = null,
  ///   @ExpiryDate nvarchar(max) = null,
  ///   @Weight nvarchar(max) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RawPallet_Update
(
 @PalletId nvarchar(max) = null,
 @ProductCode nvarchar(max) = null,
 @SKUCode nvarchar(max) = null,
 @Batch nvarchar(max) = null,
 @CreateDate nvarchar(max) = null,
 @ExpiryDate nvarchar(max) = null,
 @Weight nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update RawPallet
     set PalletId = isnull(@PalletId, PalletId),
         ProductCode = isnull(@ProductCode, ProductCode),
         SKUCode = isnull(@SKUCode, SKUCode),
         Batch = isnull(@Batch, Batch),
         CreateDate = isnull(@CreateDate, CreateDate),
         ExpiryDate = isnull(@ExpiryDate, ExpiryDate),
         Weight = isnull(@Weight, Weight) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
