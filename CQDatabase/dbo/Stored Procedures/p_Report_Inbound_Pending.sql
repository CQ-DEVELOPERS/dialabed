﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Inbound_Pending
  ///   Filename       : p_Report_Inbound_Pending.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Inbound_Pending
(
 @WarehouseId int,
 @Production  bit = null
)
 
as
begin
	 set nocount on;
  
  select case when it.InstructionTypeCode in ('PR','S')
              then 'Full Pallets'
              else 'Mixed Pallets'
              end as 'Type',
         count(distinct(j.JobId)) as 'Pallets'
    from Instruction      i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on j.StatusId           = s.StatusId
   where i.WarehouseId = @WarehouseId
     and it.InstructionTypeCode in ('PR','S','SM')
     and s.StatusCode in ('A','S')
  group by case when it.InstructionTypeCode in ('PR','S')
              then 'Full Pallets'
              else 'Mixed Pallets'
              end
end
