﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Free_Location
  ///   Filename       : p_Report_Free_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Free_Location
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int,
 @LocationTypeId   int = null,
 @AreaId           int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  LocationId   int,
	  Location     nvarchar(15),
	  LocationType nvarchar(50),
	  Area         nvarchar(50),
	  ProductId    int,
	  Product      nvarchar(50),
	  ProductCode  nvarchar(50),
	  SKUId        int,
	  SKUCode      nvarchar(50),
	  Reserved     bit default 0,
   Ailse        nvarchar(10),
   [Column]     nvarchar(10),
   [Level]      nvarchar(10),
   RelativeValue int
	 )
  
  if @LocationTypeId = -1
    set @LocationTypeId = null
  
  if @AreaId = -1
    set @AreaId = null
  
  insert @TableResult
        (LocationId,
         Location,
         LocationType,
         Area,
         Ailse,
         [Column],
         [Level],
         RelativeValue)
  select LocationId,
         Location,
         LocationType,
         Area,
         Ailse,
         [Column],
         [Level],
         RelativeValue
    from viewLocation vl (nolock)
   where WarehouseId = @WarehouseId
     --and LocationTypeId = isnull(@LocationTypeId, LocationTypeId)
     and AreaId         = isnull(@AreaId, AreaId)
     and ActivePicking  = 1
     and ActiveBinning  = 1
     and StockTakeInd   = 0
     and Used           = 0
  
  update tr
     set Reserved  = 1,
         ProductId = su.ProductId,
         SKUId     = su.SKUId
    from @TableResult         tr
    join StorageUnitLocation sul (nolock) on tr.LocationId = sul.LocationId
    join StorageUnit          su (nolock) on sul.StorageUnitId = su.StorageUnitId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product
    from @TableResult tr
    join Product       p (nolock) on tr.ProductId = p.ProductId
  
  update tr
     set SKUCode     = sku.SKUCode
    from @TableResult tr
    join SKU         sku (nolock) on tr.SKUId = sku.SKUId
  
  select Location,
         LocationType,
         Area,
         Reserved,
         ProductCode,
         Product,
         SKUCode
    from @TableResult tr
   where not exists(select 1 from StorageUnitBatchLocation subl (nolock) where tr.LocationId = subl.LocationId)
  order by Ailse,
           RelativeValue
end
