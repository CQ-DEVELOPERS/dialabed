﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questions_Update
  ///   Filename       : p_Questions_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:04
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Questions table.
  /// </remarks>
  /// <param>
  ///   @QuestionId int = null,
  ///   @QuestionaireId int = null,
  ///   @Category nvarchar(100) = null,
  ///   @Code nvarchar(100) = null,
  ///   @Sequence int = null,
  ///   @Active bit = null,
  ///   @QuestionText nvarchar(510) = null,
  ///   @QuestionType nvarchar(510) = null,
  ///   @DateUpdated datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Questions_Update
(
 @QuestionId int = null,
 @QuestionaireId int = null,
 @Category nvarchar(100) = null,
 @Code nvarchar(100) = null,
 @Sequence int = null,
 @Active bit = null,
 @QuestionText nvarchar(510) = null,
 @QuestionType nvarchar(510) = null,
 @DateUpdated datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @QuestionId = '-1'
    set @QuestionId = null;
  
	 declare @Error int
 
  update Questions
     set QuestionaireId = isnull(@QuestionaireId, QuestionaireId),
         Category = isnull(@Category, Category),
         Code = isnull(@Code, Code),
         Sequence = isnull(@Sequence, Sequence),
         Active = isnull(@Active, Active),
         QuestionText = isnull(@QuestionText, QuestionText),
         QuestionType = isnull(@QuestionType, QuestionType),
         DateUpdated = isnull(@DateUpdated, DateUpdated) 
   where QuestionId = @QuestionId
  
  select @Error = @@Error
  
  
  return @Error
  
end
