﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Scheduling_Update
  ///   Filename       : p_Scheduling_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Scheduling_Update
(
 @InboundShipmentId   int,
 @ReceiptId           int,
 @PlannedDeliveryDate datetime,
 @ActualDeliveryDate  datetime,
 @LocationId          int,
 @StatusId            int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @InboundShipmentId = -1
    select @InboundShipmentId = null
  
  if @LocationId = -1
    select @LocationId = null
  
  if @ActualDeliveryDate = convert(datetime, '1900/01/01 00:00:00.000')
    set @ActualDeliveryDate = null
  
  begin transaction
  
  if @InboundShipmentId is null
    update id
       set DeliveryDate = @PlannedDeliveryDate
      from InboundDocument id
      join Receipt          r on id.InboundDocumentId = r.InboundDocumentId
     where r.ReceiptId = @ReceiptId
    
    update Receipt
       set DeliveryDate = @ActualDeliveryDate,
           LocationId   = @LocationId,
           StatusId     = @StatusId
     where ReceiptId = @ReceiptId
    
    if @Error <> 0
      goto error
  else
  begin
    update r
       set DeliveryDate = @ActualDeliveryDate,
           LocationId   = @LocationId,
           StatusId     = @StatusId
      from InboundShipmentReceipt isr (nolock)
      join Receipt                  r          on isr.ReceiptId = r.ReceiptId
     where isr.InboundShipmentId = @InboundShipmentId
    
    if @Error <> 0
      goto error
    
    update InboundShipment
       set ShipmentDate = @PlannedDeliveryDate
     where InboundShipmentId = @InboundShipmentId
    
    if @Error <> 0
      goto error
  end
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Scheduling_Update'
    rollback transaction
    return @Error
end
