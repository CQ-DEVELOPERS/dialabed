﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_Location
  ///   Filename       : p_FamousBrands_Import_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Import_Location
 
as
begin
	 set nocount on;
  
  declare @Error               int,
          @Errormsg            varchar(500),
          @GetDate             datetime,
          @Location            varchar(30),
          @LocationDescription varchar(255),
          @Address1            varchar(255),
          @Address2            varchar(255),
          @Address3            varchar(255),
          @Address4            varchar(255),
          @City                varchar(255),
          @State               varchar(255),
          @Zip                 varchar(255),
          @WarehouseId        int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  update InterfaceLocation
     set ProcessedDate = @Getdate,
         RecordStatus = 'E'
   where RecordStatus in ('N','U')
     and ProcessedDate is null
     and Location is null
  
  update InterfaceLocation
     set ProcessedDate = @Getdate
   where RecordStatus in ('N','U')
     and ProcessedDate is null

  declare Customer_cursor cursor for
   select Location,
          isnull(LocationDescription, Location),
          Address1,
          Address2,
          Address3,
          Address4,
          City,
          State,
          Zip
     from InterfaceLocation
    where ProcessedDate = @Getdate
      and RecordStatus != 'E'
   order by Location

  open Customer_cursor

  fetch Customer_cursor into @Location,
                             @LocationDescription,
                             @Address1,
                             @Address2,
                             @Address3,
                             @Address4,
                             @City,
                             @State,
                             @Zip

  while (@@fetch_status = 0)
  begin
    begin transaction
    
    set @WarehouseId = null
    
    select @WarehouseId = WarehouseId
      from Warehouse (nolock)
     where WarehouseCode = @Location
     
    if @WarehouseId is null
    begin
      exec @Error = p_Warehouse_Insert
       @WarehouseId         = @WarehouseId output,
       @CompanyId           = 1,
       @Warehouse           = @LocationDescription,
       @WarehouseCode       = @Location
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Warehouse_Insert'
        goto error
      end
    end
    else
    begin
      exec @Error = p_Warehouse_Update
       @WarehouseId         = @WarehouseId,
       @CompanyId           = 1,
       @Warehouse           = @LocationDescription,
       @WarehouseCode       = @Location
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Warehouse_Update'
        goto error
      end
    end
    
    error:
    if @Error = 0
    begin
      update InterfaceLocation
         set RecordStatus = 'Y'
       where Location = @Location
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
      
      commit transaction
    end
    else
    begin
      if @@trancount > 0
        rollback transaction
      
      update InterfaceLocation
         set RecordStatus = 'E'
       where Location = @Location
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
    end
    
    fetch Customer_cursor into @Location,
                               @LocationDescription,
                               @Address1,
                               @Address2,
                               @Address3,
                               @Address4,
                               @City,
                               @State,
                               @Zip
  end

  close Customer_cursor
  deallocate Customer_cursor
end
