﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletMatch_Insert
  ///   Filename       : p_PalletMatch_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:46
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PalletMatch table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null,
  ///   @InstructionId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(60) = null,
  ///   @Batch nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   PalletMatch.PalletId,
  ///   PalletMatch.InstructionId,
  ///   PalletMatch.StorageUnitBatchId,
  ///   PalletMatch.ProductCode,
  ///   PalletMatch.SKUCode,
  ///   PalletMatch.Batch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletMatch_Insert
(
 @PalletId int = null,
 @InstructionId int = null,
 @StorageUnitBatchId int = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(60) = null,
 @Batch nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PalletMatch
        (PalletId,
         InstructionId,
         StorageUnitBatchId,
         ProductCode,
         SKUCode,
         Batch)
  select @PalletId,
         @InstructionId,
         @StorageUnitBatchId,
         @ProductCode,
         @SKUCode,
         @Batch 
  
  select @Error = @@Error
  
  
  return @Error
  
end
