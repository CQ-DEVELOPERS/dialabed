﻿   
/*  
  /// <summary>  
  ///   Procedure Name : p_Report_Location_In_StockTake  
  ///   Filename       : p_Report_Location_In_StockTake.sql  
  ///   Create By      : Karen  
  ///   Date Created   : October 2015  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Report_Location_In_StockTake  
(  
-- @StockTakeReferenceId  int,  
 @WarehouseId    int  
-- @FromDate     datetime,  
-- @ToDate     datetime,  
-- @AreaId     Int  
)  
as  
begin  
  set nocount on;   
  
--if (@AreaId = 0) or (@AreaId = -1)  
--Begin  
-- set @AreaId = null  
--end  
   
Declare @StockTakeSummary as  Table   
  (StockTakeReferenceId int,  
   InstructionType   Varchar(50),  
   InstructionTypeCode  Varchar(25),  
   Area      Varchar(50),  
   AreaId      int,  
    Date      Datetime,  
   LocationCount    Int,  
   StockTakes     Int,  
   Cancelled     Int,    
   Waiting     int,  
   Busy      int,  
   Complete     float,     
   ActualStockTakes   int,  
   StockTakesPerRef   int,  
   StockTakeComplete   float)  
  
--If @AreaId = -1 Set @AreaId = null  
  
--If @StockTakeReferenceId = -1 Set @StockTakeReferenceId = null  
  
Declare @AreaLocationCount as Table    
 (WarehouseId   int,  
  AreaId     int NOT NULL,  
  LocationCount   int NULL,  
  StockTake    int,  
  NoStockTake   int,  
  Percentage    decimal(7,3))   
  
  Insert into @AreaLocationCount   
    (WarehouseId,  
    AreaId,  
    LocationCount)  
  Select a.WarehouseId,  
    al.AreaId,  
    Count(LocationId)  as LocationCount  
    from AreaLocation al  
    Join Area a on a.AreaId = al.AreaId  
    where a.WarehouseId = isnull(@WarehouseId,a.WarehouseId)  
    Group by a.WarehouseId,  
       al.AreaId  
  
update alc  
set StockTake = (select COUNT(Location) from AreaLocation al   
     Join Area a on a.AreaId = al.AreaId  
     join Location l on al.LocationId = l.LocationId  
     where l.StocktakeInd = 1  
     and  al.AreaId = alc.AreaId)  
from @AreaLocationCount alc  
  
  
update alc  
set noStockTake = (select COUNT(Location) from AreaLocation al   
     Join Area a on a.AreaId = al.AreaId  
     join Location l on al.LocationId = l.LocationId  
     where l.StocktakeInd = 0  
     and  al.AreaId = alc.AreaId)  
from @AreaLocationCount alc  
  
update alc  
set Percentage = ((isnull(alc.NoStockTake,0) / isnull(alc.LocationCount,1) * 100))  
from @AreaLocationCount alc  
  
SET ARITHABORT OFF  
SET ANSI_WARNINGS OFF  
     
select  alc.AreaId,  
  a.Area,  
  alc.LocationCount,  
  alc.StockTake,  
  alc.NoStockTake,  
  alc.Percentage
  --((isnull(alc.StockTake,0) / isnull(alc.LocationCount,1) * 100)) as Percentage   
from @AreaLocationCount alc  
join Area a on alc.AreaId = a.AreaId  
  
  
end  