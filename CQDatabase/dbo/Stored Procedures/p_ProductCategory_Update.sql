﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCategory_Update
  ///   Filename       : p_ProductCategory_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:59
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ProductCategory table.
  /// </remarks>
  /// <param>
  ///   @ProductCategoryId int = null,
  ///   @ProductCategoryType nvarchar(100) = null,
  ///   @ProductType nvarchar(40) = null,
  ///   @ProductCategory char(1) = null,
  ///   @PackingCategory char(1) = null,
  ///   @StackingCategory int = null,
  ///   @MovementCategory int = null,
  ///   @ValueCategory int = null,
  ///   @StoringCategory int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCategory_Update
(
 @ProductCategoryId int = null,
 @ProductCategoryType nvarchar(100) = null,
 @ProductType nvarchar(40) = null,
 @ProductCategory char(1) = null,
 @PackingCategory char(1) = null,
 @StackingCategory int = null,
 @MovementCategory int = null,
 @ValueCategory int = null,
 @StoringCategory int = null 
)
 
as
begin
	 set nocount on;
  
  if @ProductCategoryId = '-1'
    set @ProductCategoryId = null;
  
  if @ProductCategory = '-1'
    set @ProductCategory = null;
  
	 declare @Error int
 
  update ProductCategory
     set ProductCategoryType = isnull(@ProductCategoryType, ProductCategoryType),
         ProductType = isnull(@ProductType, ProductType),
         ProductCategory = isnull(@ProductCategory, ProductCategory),
         PackingCategory = isnull(@PackingCategory, PackingCategory),
         StackingCategory = isnull(@StackingCategory, StackingCategory),
         MovementCategory = isnull(@MovementCategory, MovementCategory),
         ValueCategory = isnull(@ValueCategory, ValueCategory),
         StoringCategory = isnull(@StoringCategory, StoringCategory) 
   where ProductCategoryId = @ProductCategoryId
  
  select @Error = @@Error
  
  
  return @Error
  
end
