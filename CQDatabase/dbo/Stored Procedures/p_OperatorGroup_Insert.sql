﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroup_Insert
  ///   Filename       : p_OperatorGroup_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2012 07:59:17
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null output,
  ///   @OperatorGroup varchar(30) = null,
  ///   @RestrictedAreaIndicator bit = null,
  ///   @OperatorGroupCode varchar(10) = null 
  /// </param>
  /// <returns>
  ///   OperatorGroup.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup,
  ///   OperatorGroup.RestrictedAreaIndicator,
  ///   OperatorGroup.OperatorGroupCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroup_Insert
(
 @OperatorGroupId int = null output,
 @OperatorGroup varchar(30) = null,
 @RestrictedAreaIndicator bit = null,
 @OperatorGroupCode varchar(10) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  insert OperatorGroup
        (OperatorGroup,
         RestrictedAreaIndicator,
         OperatorGroupCode)
  select @OperatorGroup,
         @RestrictedAreaIndicator,
         @OperatorGroupCode 
  
  select @Error = @@Error, @OperatorGroupId = scope_identity()
  
  if @Error = 0
    exec @Error = p_OperatorGroupHistory_Insert
         @OperatorGroupId = @OperatorGroupId,
         @OperatorGroup = @OperatorGroup,
         @RestrictedAreaIndicator = @RestrictedAreaIndicator,
         @OperatorGroupCode = @OperatorGroupCode,
         @CommandType = 'Insert'
  
  return @Error
  
end
