﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Issue_List
  ///   Filename       : p_Issue_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2015 09:30:36
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Issue table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Issue.IssueId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Issue_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as IssueId
        ,null as 'Issue'
  union
  select
         Issue.IssueId
        ,Issue.IssueId as 'Issue'
    from Issue
  
end
