﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnit_Delete
  ///   Filename       : p_StorageUnit_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:02:12
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StorageUnit table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnit_Delete
(
 @StorageUnitId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete StorageUnit
     where StorageUnitId = @StorageUnitId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_StorageUnitHistory_Insert
         @StorageUnitId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
