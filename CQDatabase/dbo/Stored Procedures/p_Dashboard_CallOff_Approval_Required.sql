﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_CallOff_Approval_Required
  ///   Filename       : p_Dashboard_CallOff_Approval_Required.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_CallOff_Approval_Required
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	   select distinct CallOffNumber
	     from CallOffHeader
	    where CallOffNumber is not null
    else
	   select distinct CallOffNumber
	     from CallOffHeader
	    where CallOffNumber is not null
end
