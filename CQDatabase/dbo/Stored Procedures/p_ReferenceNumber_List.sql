﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReferenceNumber_List
  ///   Filename       : p_ReferenceNumber_List.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_ReferenceNumber_List
(
 @ReferenceNumber varchar(30) = null
)
as
begin
	 set nocount on
	 
	 declare @JobId int,
	         @PalletId int,
	         @WarehouseId int
  
  if isnumeric(replace(@ReferenceNumber,'J:','')) = 1
    select @JobId           = replace(@ReferenceNumber,'J:',''),
           @ReferenceNumber = null
  
  if isnumeric(replace(@ReferenceNumber,'P:','')) = 1
    select @PalletId        = replace(@ReferenceNumber,'P:',''),
           @ReferenceNumber = null
  
  if isnumeric(replace(@ReferenceNumber,'R:','')) = 1
    select @JobId = JobId,
           @WarehouseId = WarehouseId
       from Job
     where ReferenceNumber = @ReferenceNumber
  
  if @PalletId is not null
    select @JobId = JobId,
           @WarehouseId = WarehouseId
      from Instruction (nolock)
     where PalletId = @PalletId
  
  select distinct
         j.ReferenceNumber,
         od.OrderNumber,
         ili.IssueId,
         case when dbo.ufn_Configuration(418, @WarehouseId) = 1
              then -1
              else isnull(ili.OutboundShipmentId,-1)
              end as 'OutboundShipmentId'
    from Instruction            i (nolock)
    join Job                    j (nolock) on i.JobId = j.JobId
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
    join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
   where i.JobId = @JobId
end
