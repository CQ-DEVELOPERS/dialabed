﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKU_Update
  ///   Filename       : p_SKU_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:06
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the SKU table.
  /// </remarks>
  /// <param>
  ///   @SKUId int = null,
  ///   @UOMId int = null,
  ///   @SKU nvarchar(100) = null,
  ///   @SKUCode nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @AlternatePallet int = null,
  ///   @SubstitutePallet int = null,
  ///   @Litres numeric(13,3) = null,
  ///   @ParentSKUCode nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKU_Update
(
 @SKUId int = null,
 @UOMId int = null,
 @SKU nvarchar(100) = null,
 @SKUCode nvarchar(100) = null,
 @Quantity float = null,
 @AlternatePallet int = null,
 @SubstitutePallet int = null,
 @Litres numeric(13,3) = null,
 @ParentSKUCode nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @SKUId = '-1'
    set @SKUId = null;
  
  if @UOMId = '-1'
    set @UOMId = null;
  
  if @SKU = '-1'
    set @SKU = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
	 declare @Error int
 
  update SKU
     set UOMId = isnull(@UOMId, UOMId),
         SKU = isnull(@SKU, SKU),
         SKUCode = isnull(@SKUCode, SKUCode),
         Quantity = isnull(@Quantity, Quantity),
         AlternatePallet = isnull(@AlternatePallet, AlternatePallet),
         SubstitutePallet = isnull(@SubstitutePallet, SubstitutePallet),
         Litres = isnull(@Litres, Litres),
         ParentSKUCode = isnull(@ParentSKUCode, ParentSKUCode) 
   where SKUId = @SKUId
  
  select @Error = @@Error
  
  
  return @Error
  
end
