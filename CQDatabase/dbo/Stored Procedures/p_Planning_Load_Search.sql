﻿USE [DialaBed]
GO
/****** Object:  StoredProcedure [dbo].[p_Planning_Load_Search]    Script Date: 07/02/2021 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Load_Search
  ///   Filename       : p_Planning_Load_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Planning_Load_Search]
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        OutboundDocumentId        int,
        OutboundDocumentType      nvarchar(30),
        IssueId                   int,
        OrderNumber               nvarchar(30),
        OutboundShipmentId        int,
        CustomerCode              nvarchar(30),
        Customer                  nvarchar(255),
        RouteId                   int,
        Route                     nvarchar(50),
        NumberOfLines             int,
        DeliveryDate              datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        PriorityId                int,
        Priority                  nvarchar(50),
        LocationId                int,
        Location                  nvarchar(15),
        Rating                    int,
        AvailabilityIndicator     nvarchar(20),
        Remarks                   nvarchar(255),
        AreaType                  nvarchar(10),
        ContactPerson			  nvarchar(100)
       );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (OutboundShipmentId,
           LocationId,
           RouteId,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           Remarks,
           NumberOfLines,
           AreaType,
           ContactPerson)
    select os.OutboundShipmentId,
           os.LocationId,
           os.RouteId,
           os.StatusId,
           s.Status,
           min(i.PriorityId),
           os.ShipmentDate,
           min(od.CreateDate),
           os.Remarks,
           sum(i.NumberOfLines),
           isnull(i.AreaType, odt.AreaType),
           i.ContactPerson
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue  osi (nolock) on i.IssueId                 = osi.IssueId
      join OutboundShipment        os (nolock) on os.OutboundShipmentId     = osi.OutboundShipmentId
     where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
       and os.ShipmentDate      between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('W','P','SA') -- Waiting, Palletised, Stock Allocated
       and od.WarehouseId            = @WarehouseId
  group by os.OutboundShipmentId,
           os.LocationId,
           os.RouteId,
           os.StatusId,
           s.Status,
           os.ShipmentDate,
           os.Remarks,
           isnull(i.AreaType, odt.AreaType),
           i.ContactPerson
    
    update tr
       set OutboundShipmentId = si.OutboundShipmentId,
           DeliveryDate       = os.ShipmentDate,
           LocationId         = os.LocationId
      from @TableResult  tr
      join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
      join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  end
  else
    insert @TableResult
          (OutboundShipmentId,
           LocationId,
           RouteId,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           Remarks,
           NumberOfLines,
           AreaType,
           ContactPerson)
    select os.OutboundShipmentId,
           os.LocationId,
           os.RouteId,
           os.StatusId,
           s.Status,
           min(i.PriorityId),
           os.ShipmentDate,
           min(od.CreateDate),
           os.Remarks,
           sum(i.NumberOfLines),
           isnull(i.AreaType, odt.AreaType),
           i.ContactPerson
      from OutboundDocument        od (nolock)
      join ExternalCompany         ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                    i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                   s (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType   odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue  osi (nolock) on i.IssueId                 = osi.IssueId
      join OutboundShipment        os (nolock) on os.OutboundShipmentId     = osi.OutboundShipmentId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                    = 'IS'
       and s.StatusCode             in ('W','P','SA') -- Waiting, Palletised, Stock Allocated
  group by os.OutboundShipmentId,
           os.LocationId,
           os.RouteId,
           os.StatusId,
           s.Status,
           os.ShipmentDate,
           os.Remarks,
           isnull(i.AreaType, odt.AreaType),
           i.ContactPerson
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update @TableResult
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @warehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  select IssueId,
         OutboundShipmentId,
         OrderNumber,
         CustomerCode,
         Customer,
         isnull(RouteId,-1) as 'RouteId',
         Route,
         NumberOfLines,
         DeliveryDate,
         CreateDate,
         Status,
         PriorityId,
         Priority,
         OutboundDocumentType,
         isnull(LocationId,-1) as 'LocationId',
         Location,
         Rating,
         AvailabilityIndicator,
         Remarks,
         AreaType,
         ContactPerson
    from @TableResult
   where OutboundShipmentId is not null
  order by DeliveryDate, OutboundShipmentId, OrderNumber
end

