﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Insert
  ///   Filename       : p_Exception_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:04
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Exception table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null output,
  ///   @ReceiptLineId int = null,
  ///   @InstructionId int = null,
  ///   @IssueLineId int = null,
  ///   @ReasonId int = null,
  ///   @Exception nvarchar(510) = null,
  ///   @ExceptionCode nvarchar(20) = null,
  ///   @CreateDate datetime = null,
  ///   @OperatorId int = null,
  ///   @ExceptionDate datetime = null,
  ///   @JobId int = null,
  ///   @Detail sql_variant = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   Exception.ExceptionId,
  ///   Exception.ReceiptLineId,
  ///   Exception.InstructionId,
  ///   Exception.IssueLineId,
  ///   Exception.ReasonId,
  ///   Exception.Exception,
  ///   Exception.ExceptionCode,
  ///   Exception.CreateDate,
  ///   Exception.OperatorId,
  ///   Exception.ExceptionDate,
  ///   Exception.JobId,
  ///   Exception.Detail,
  ///   Exception.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Insert
(
 @ExceptionId int = null output,
 @ReceiptLineId int = null,
 @InstructionId int = null,
 @IssueLineId int = null,
 @ReasonId int = null,
 @Exception nvarchar(510) = null,
 @ExceptionCode nvarchar(20) = null,
 @CreateDate datetime = null,
 @OperatorId int = null,
 @ExceptionDate datetime = null,
 @JobId int = null,
 @Detail sql_variant = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
  if @ExceptionId = '-1'
    set @ExceptionId = null;
  
  if @Exception = '-1'
    set @Exception = null;
  
  if @ExceptionCode = '-1'
    set @ExceptionCode = null;
  
	 declare @Error int
 
  insert Exception
        (ReceiptLineId,
         InstructionId,
         IssueLineId,
         ReasonId,
         Exception,
         ExceptionCode,
         CreateDate,
         OperatorId,
         ExceptionDate,
         JobId,
         Detail,
         Quantity)
  select @ReceiptLineId,
         @InstructionId,
         @IssueLineId,
         @ReasonId,
         @Exception,
         @ExceptionCode,
         @CreateDate,
         @OperatorId,
         @ExceptionDate,
         @JobId,
         @Detail,
         @Quantity 
  
  select @Error = @@Error, @ExceptionId = scope_identity()
  
  
  return @Error
  
end
