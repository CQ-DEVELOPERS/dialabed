﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LabelingCategory_Update
  ///   Filename       : p_LabelingCategory_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:56:43
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the LabelingCategory table.
  /// </remarks>
  /// <param>
  ///   @LabelingCategoryId int = null,
  ///   @LabelingCategory nvarchar(510) = null,
  ///   @LabelingCategoryCode nvarchar(60) = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @ManualLabel bit = null,
  ///   @ScreenGaurd bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LabelingCategory_Update
(
 @LabelingCategoryId int = null,
 @LabelingCategory nvarchar(510) = null,
 @LabelingCategoryCode nvarchar(60) = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @ManualLabel bit = null,
 @ScreenGaurd bit = null 
)
 
as
begin
	 set nocount on;
  
  if @LabelingCategoryId = '-1'
    set @LabelingCategoryId = null;
  
  if @LabelingCategory = '-1'
    set @LabelingCategory = null;
  
  if @LabelingCategoryCode = '-1'
    set @LabelingCategoryCode = null;
  
	 declare @Error int
 
  update LabelingCategory
     set LabelingCategory = isnull(@LabelingCategory, LabelingCategory),
         LabelingCategoryCode = isnull(@LabelingCategoryCode, LabelingCategoryCode),
         Additional1 = isnull(@Additional1, Additional1),
         Additional2 = isnull(@Additional2, Additional2),
         Additional3 = isnull(@Additional3, Additional3),
         Additional4 = isnull(@Additional4, Additional4),
         Additional5 = isnull(@Additional5, Additional5),
         ManualLabel = isnull(@ManualLabel, ManualLabel),
         ScreenGaurd = isnull(@ScreenGaurd, ScreenGaurd) 
   where LabelingCategoryId = @LabelingCategoryId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
