﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitLocation_List
  ///   Filename       : p_StorageUnitLocation_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:08
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitLocation table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnitLocation.StorageUnitId,
  ///   StorageUnitLocation.LocationId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitLocation_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as StorageUnitId
        ,null as 'StorageUnitLocation'
        ,'-1' as LocationId
        ,null as 'StorageUnitLocation'
  union
  select
         StorageUnitLocation.StorageUnitId
        ,StorageUnitLocation.StorageUnitId as 'StorageUnitLocation'
        ,StorageUnitLocation.LocationId
        ,StorageUnitLocation.LocationId as 'StorageUnitLocation'
    from StorageUnitLocation
  
end
