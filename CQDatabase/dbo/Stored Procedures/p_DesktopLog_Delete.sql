﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DesktopLog_Delete
  ///   Filename       : p_DesktopLog_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:35
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the DesktopLog table.
  /// </remarks>
  /// <param>
  ///   @DesktopLogId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DesktopLog_Delete
(
 @DesktopLogId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete DesktopLog
     where DesktopLogId = @DesktopLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
