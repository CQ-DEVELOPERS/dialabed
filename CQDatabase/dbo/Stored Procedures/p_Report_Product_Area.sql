﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Area
  ///   Filename       : p_Report_Product_Area.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 22 Jan 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/

CREATE procedure p_Report_Product_Area
(
 @WarehouseId     int,
 @ProductCode        nvarchar(50),
 @Product           nvarchar(50)
)
 
as
begin

if (@ProductCode = 'All')
BEGIN
	Set @ProductCode = ''
END

if (@Product = 'All' or @Product = '-1')
BEGIN
	Set @Product = ''
END

	 set nocount on;
SELECT     P.ProductCode, P.Product, SU.StorageUnitId, A.AreaId, A.Area, A.AreaCode,SUA.StoreOrder,SUA.PickOrder
FROM         StorageUnitArea AS SUA INNER JOIN
                      Area AS A ON SUA.AreaId = A.AreaId INNER JOIN
                      StorageUnit AS SU ON SUA.StorageUnitId = SU.StorageUnitId INNER JOIN
                      Product AS P ON SU.ProductId = P.ProductId 	
where A.WarehouseId = @WarehouseId
and P.ProductCode like @ProductCode + '%'
and P.Product like @Product + '%'
ORDER BY P.ProductCode,SUA.storeOrder

end
