﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Authorise_Job
  ///   Filename       : p_Outbound_Authorise_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Authorise_Job
(
 @JobId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @StatusCode        nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusCode = s.StatusCode
    from Job    j
    join Status s on j.StatusId = s.StatusId
   where j.JobId = @JobId
  
  if @StatusCode = 'A'
  begin
    select @StatusId = dbo.ufn_StatusId('IS','CK')
    set @StatusCode = 'CK'
  end
  else if @StatusCode = 'QA'
  begin
    select @StatusId = dbo.ufn_StatusId('IS','D')
    set @StatusCode = 'D'
  end
  
  begin transaction
  
  exec @Error = p_Job_Update
   @JobId    = @JobId,
   @StatusId = @StatusId
  
  if @Error <> 0
    goto error
  
  if exists(select top 1 1
              from SKUCheck
             where JobId = @JobId)
  begin
    delete SKUCheck
     where SKUCode <> 'SKU''s'
       and JobId = @JobId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert SKUCheck
          (JobId,
           SKUCode,
           Quantity,
           ConfirmedQuantity)
    select i.JobId,
           sku.SKUCode,
           sum(i.ConfirmedQuantity),
           sku.Quantity
      from Instruction        i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join SKU              sku (nolock) on su.SKUId             = sku.SKUId
     where i.JobId = @JobId
       and i.ConfirmedQuantity > 0 
    group by i.JobId,
             sku.SKUCode,
             sku.Quantity
    order by sku.Quantity
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Authorise_Job'
    rollback transaction
    return @Error
end
