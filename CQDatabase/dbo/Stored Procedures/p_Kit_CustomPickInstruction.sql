﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Kit_CustomPickInstruction
  ///   Filename       : p_Kit_CustomPickInstruction.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@BOMInstructionId>
  ///       <@OrderNumber>
  ///       <@StorageUnitId>
  ///       <@WarehouseId>
  ///       <@Quantity>>	 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Kit_CustomPickInstruction
(
     @BOMInstructionId  int
    ,@OrderNumber       varchar(50)
    ,@StorageUnitId     int
    ,@WarehouseId       int
    ,@Quantity          float
)
As
Begin 
    Declare        
         @ExternalCompanyId         int
        ,@IssueId                   int
        ,@OutboundDocumentId        int
        ,@OutboundDocumentTypeId    int
        ,@OutboundLineId            int
        ,@OutboundShipmentId        int
        ,@StorageUnitBatchId        int   
        ,@StatusId                  int
        ,@LineNumber                int
        ,@DeliveryDate              datetime
        ,@GetDate                   datetime
        ,@Error                     int
        ,@ErrorMsg                  varchar(100)
        ,@ProductCode               varchar(30)
        ,@Product                   varchar(255)
     
    select
         @OutboundDocumentTypeId = 8                --Pick better doc type
        ,@WarehouseId = 1                           --Double check this
        ,@StatusId = dbo.ufn_StatusId('OD','I')     
        ,@DeliveryDate = dbo.ufn_Getdate()
        ,@GetDate = dbo.ufn_Getdate()

    begin transaction KitPick
    
    select @ExternalCompanyId   = ExternalCompanyId
    from ExternalCompany (nolock)  
    where ExternalCompanyCode = 'KIT'
     
    if @ExternalCompanyId is null  
    begin  
      exec @Error = p_ExternalCompany_Insert  
       @ExternalCompanyId         = @ExternalCompanyId output,  
       @ExternalCompanyTypeId     = 1,  
       @RouteId                   = null,  
       --@drop  
       @ExternalCompany           = 'Internal kitting area',  
       @ExternalCompanyCode       = 'KIT'  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'  
        goto error_header  
      end  
    end
    
    exec @Error = p_OutboundDocument_Insert
       @OutboundDocumentId      = @OutboundDocumentId output
      ,@OutboundDocumentTypeId  = @OutboundDocumentTypeId
      ,@ExternalCompanyId       = @ExternalCompanyId
      ,@StatusId                = @StatusId
      ,@WarehouseId             = @WarehouseId
      ,@OrderNumber             = @OrderNumber
      ,@DeliveryDate            = @DeliveryDate
      ,@CreateDate              = @GetDate
      ,@ModifiedDate            = null
      ,@ReferenceNumber         = null
      ,@FromLocation            = null
      ,@ToLocation              = 'KBay1'
       
    if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'
        goto error_header
      end
      
-- Get Kit product
    
    declare kit_cursor cursor for
    select 
         p.ProductCode
        ,p.Product
        ,1
        ,bp.Quantity * bil.Quantity
        ,ROW_NUMBER() OVER (order by bil.BOMLineId, bil.LineNumber)
    from BOMInstruction bi
    inner join BOMInstructionLine bil on bi.BOMInstructionId = bil.BOMInstructionId
    inner join BOMProduct bp on bil.BOMLineId = bp.BOMLineId
                            and bil.LineNumber = bp.LineNumber
    inner join StorageUnit su on su.StorageUnitId = bp.StorageUnitId
    inner join Product p on su.ProductId = p.ProductId
    where bi.BOMInstructionId = @BOMInstructionId
    order by 
         bil.BOMLineId
        ,bil.LineNumber
            
    open kit_cursor
    
    fetch kit_cursor
     into  
         @ProductCode
        ,@Product
        ,@WarehouseId
        ,@Quantity
        ,@LineNumber

    while (@@fetch_status = 0)
    begin
        
        exec @Error = p_interface_xml_Product_Insert
             @ProductCode        = @ProductCode
            ,@Product            = @Product
            ,@WarehouseId        = @WarehouseId
            ,@StorageUnitId      = @StorageUnitId output
            ,@StorageUnitBatchId = @StorageUnitBatchId output
           
        if @Error != 0 or @StorageUnitBatchId is null
        begin
            select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
            goto error_detail
        end
           
        exec @Error = p_OutboundLine_Insert
             @OutboundLineId     = @OutboundLineId output,
             @OutboundDocumentId = @OutboundDocumentId,
             @StorageUnitId      = @StorageUnitId,
             @StatusId           = @StatusId,
             @LineNumber         = @LineNumber,
             @Quantity           = @Quantity,
             --@Weight             = @di_weight * @Quatity,
             @BatchId            = null
             
        if @Error != 0
        begin
            select @ErrorMsg = 'Error executing p_OutboundLine_Insert'
            goto error_detail
        end
        
        fetch kit_cursor
         into  
             @ProductCode
            ,@Product
            ,@WarehouseId
            ,@Quantity
            ,@LineNumber
        
    end   

    close kit_cursor
    deallocate kit_cursor        

    exec @Error = p_Despatch_Create_Issue
     @OutboundDocumentId = @OutboundDocumentId,
     @OperatorId         = null,
     @Remarks            = 'Pick instruction for kit'
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
      goto error_detail
    end
    
    select @IssueId = IssueId
      from Issue (nolock)
     where OutboundDocumentId = @OutboundDocumentId
        
    exec @Error = p_Outbound_Auto_Load
     @OutboundShipmentId = @OutboundShipmentId output,
     @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
      goto error_detail
    end
    
    exec @Error = p_Outbound_Auto_Release
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
      goto error_detail
    end
        
    error_detail:
        
        if (CURSOR_STATUS ('local','kit_cursor') = 1)
        begin
            print 'Closing failed cursor'
            close kit_cursor
            deallocate kit_cursor 
        end
    error_header:
    
    if @Error = 0
    begin     
      if @@trancount > 0
        commit transaction KitPick
      
    end
    else
    begin      
      if @@trancount > 0
        rollback transaction KitPick
      
    end
end
