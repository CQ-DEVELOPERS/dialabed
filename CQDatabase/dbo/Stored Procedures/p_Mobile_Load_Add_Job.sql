﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Load_Add_Job
  ///   Filename       : p_Mobile_Load_Add_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Load_Add_Job
(
 @warehouseId     int,
 @operatorId      int,
 @intransitLoadId int,
 @barcode         nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @IntransitLoadJobId int,
          @StatusId           int,
          @JobId              int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Errormsg = 'Error executing p_Mobile_Load_Add_Job'
  
  select @StatusId = dbo.ufn_StatusId('IT','LG')
  
  begin transaction
  
  select @IntransitLoadJobId = ilj.IntransitLoadJobId,
         @JobId              = j.JobId
    from Job                j (nolock)
    left
    join IntransitLoadJob ilj (nolock) on ilj.JobId = j.JobId
                                      and IntransitLoadId = @intransitLoadId
   where (j.ReferenceNumber = @barcode
     or   convert(nvarchar(10), j.JobId) = replace(@barcode, 'J:', ''))
  
  if @JobId is null
  begin
    select @Error = -1
    goto error
  end
  
  if @IntransitLoadJobId is null and @JobId is not null
  begin
    exec @Error = p_IntransitLoadJob_Insert
     @intransitLoadJobId = @intransitLoadJobId output,
     @IntransitLoadId    = @IntransitLoadId,
     @StatusId           = @StatusId,
     @JobId              = @JobId,
     @LoadedById         = @operatorId,
     @CreateDate         = @getdate
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  select @intransitLoadJobId
  return @intransitLoadJobId
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    select @Error
    return @Error
end
