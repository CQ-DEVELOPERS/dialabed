﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COATest_Search
  ///   Filename       : p_COATest_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:09
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the COATest table.
  /// </remarks>
  /// <param>
  ///   @COATestId int = null output,
  ///   @COAId int = null,
  ///   @TestId int = null,
  ///   @ResultId int = null,
  ///   @MethodId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   COATest.COATestId,
  ///   COATest.COAId,
  ///   COA.COA,
  ///   COATest.TestId,
  ///   Test.Test,
  ///   COATest.ResultId,
  ///   Result.Result,
  ///   COATest.MethodId,
  ///   Method.Method,
  ///   COATest.ResultCode,
  ///   COATest.Result,
  ///   COATest.Pass,
  ///   COATest.StartRange,
  ///   COATest.EndRange,
  ///   COATest.Value 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COATest_Search
(
 @COATestId int = null output,
 @COAId int = null,
 @TestId int = null,
 @ResultId int = null,
 @MethodId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @COATestId = '-1'
    set @COATestId = null;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @TestId = '-1'
    set @TestId = null;
  
  if @ResultId = '-1'
    set @ResultId = null;
  
  if @MethodId = '-1'
    set @MethodId = null;
  
 
  select
         COATest.COATestId
        ,COATest.COAId
         ,COACOAId.COA as 'COA'
        ,COATest.TestId
         ,TestTestId.Test as 'Test'
        ,COATest.ResultId
         ,ResultResultId.Result as 'Result'
        ,COATest.MethodId
         ,MethodMethodId.Method as 'Method'
        ,COATest.ResultCode
        ,COATest.Result
        ,COATest.Pass
        ,COATest.StartRange
        ,COATest.EndRange
        ,COATest.Value
    from COATest
    left
    join COA COACOAId on COACOAId.COAId = COATest.COAId
    left
    join Test TestTestId on TestTestId.TestId = COATest.TestId
    left
    join Result ResultResultId on ResultResultId.ResultId = COATest.ResultId
    left
    join Method MethodMethodId on MethodMethodId.MethodId = COATest.MethodId
   where isnull(COATest.COATestId,'0')  = isnull(@COATestId, isnull(COATest.COATestId,'0'))
     and isnull(COATest.COAId,'0')  = isnull(@COAId, isnull(COATest.COAId,'0'))
     and isnull(COATest.TestId,'0')  = isnull(@TestId, isnull(COATest.TestId,'0'))
     and isnull(COATest.ResultId,'0')  = isnull(@ResultId, isnull(COATest.ResultId,'0'))
     and isnull(COATest.MethodId,'0')  = isnull(@MethodId, isnull(COATest.MethodId,'0'))
  order by ResultCode
  
end
