﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Search_Product
  ///   Filename       : p_Batch_Search_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Search_Product
(
 @ProductId int,
 @Batch     nvarchar(50) = null,
 @ECLNumber nvarchar(10) = null
)
 
as
begin
	 set nocount on;
	 
--	 if @StorageUnitId = -1
--	   set @StorageUnitId = null
  
  select sub.StorageUnitBatchId,
         b.Batch,
         b.BatchId,
         b.ECLNumber,
         b.CreateDate
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Batch              b (nolock) on sub.BatchId       = b.BatchId
   where su.ProductId          = isnull(@ProductId, su.ProductId)
     and isnull(b.Batch,'%')     like '%' + isnull(@Batch, isnull(b.Batch,'%')) + '%'
     and isnull(b.ECLNumber,'%') like '%' + isnull(@ECLNumber, isnull(b.ECLNumber,'%')) + '%'
end
