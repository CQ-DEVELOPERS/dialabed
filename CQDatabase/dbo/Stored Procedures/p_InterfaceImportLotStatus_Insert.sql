﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportLotStatus_Insert
  ///   Filename       : p_InterfaceImportLotStatus_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:45
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportLotStatus table.
  /// </remarks>
  /// <param>
  ///   @StatusDescription nvarchar(510) = null,
  ///   @StatusID int = null,
  ///   @HostId varchar(10) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportLotStatus.StatusDescription,
  ///   InterfaceImportLotStatus.StatusID,
  ///   InterfaceImportLotStatus.HostId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportLotStatus_Insert
(
 @StatusDescription nvarchar(510) = null,
 @StatusID int = null,
 @HostId varchar(10) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportLotStatus
        (StatusDescription,
         StatusID,
         HostId)
  select @StatusDescription,
         @StatusID,
         @HostId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
