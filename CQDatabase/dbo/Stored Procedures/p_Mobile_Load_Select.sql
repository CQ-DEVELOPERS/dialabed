﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Load_Select
  ///   Filename       : p_Mobile_Load_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Load_Select 
(
 @IntransitLoadId int
)
 
as
begin
	 set nocount on;
  
  select il.IntransitLoadId,
         COUNT(distinct(ilj.JobId)) as 'NumberOfJobs'
    from IntransitLoad     il (nolock)
    left
    join IntransitLoadJob ilj (nolock) on il.IntransitLoadId = ilj.IntransitLoadId
   where il.IntransitLoadId = @IntransitLoadId
  group by il.IntransitLoadId
end
