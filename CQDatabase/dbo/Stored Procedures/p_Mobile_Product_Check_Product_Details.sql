﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Product_Details
  ///   Filename       : p_Mobile_Product_Check_Product_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Product_Details
(
 @jobId int
)
 
as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         JobId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @JobId,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  select ProductCode,
         Product,
         SKUCode,
         sum(Quantity) as 'Quantity',
         sum(isnull(ConfirmedQuantity,0)) as 'ConfirmedQuantity',
         sum(isnull(CheckQuantity,0)) as 'CheckQuantity'
    from CheckingProduct (nolock)
   where JobId = @jobId
  group by ProductCode,
           Product,
           SKUCode
  
  update MobileLog
     set EndDate = getdate()
   where MobileLogId = @MobileLogId
end
