﻿
CREATE procedure [dbo].[p_Interface_PRD_Pack_Generate]
AS
BEGIN      
CREATE TABLE #Temp
(
    InterfaceImportProductId int, 
    ProductCode NVARCHAR(MAX)
)

INSERT INTO #Temp
SELECT	InterfaceImportProductId,
		ProductCode
FROM	InterfaceImportProduct 
WHERE	RecordStatus = 'W'

DECLARE @InterfaceImportProductId INT
DECLARE @ProductCode NVARCHAR(MAX)

DECLARE db_cursor CURSOR FOR
SELECT	InterfaceImportProductId,
		ProductCode
FROM	#Temp

OPEN db_cursor  
FETCH NEXT FROM db_cursor INTO @InterfaceImportProductId, @ProductCode

WHILE @@FETCH_STATUS = 0  
BEGIN  
      INSERT INTO [InterfaceImportPack]      
           (InterfaceImportProductId
           ,[PackCode]
           ,[PackDescription]
           ,[Quantity]
           ,[Barcode]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[Volume]
           ,[NettWeight]
           ,[GrossWeight]
           ,[TareWeight]
           ,[ProductCategory]
           ,[PackingCategory]
           ,[PickEmpty]
           ,[StackingCategory]
           ,[MovementCategory]
           ,[ValueCategory]
           ,[StoringCategory]
           ,[PickPartPallet]
		   ,[HostId]
           ,[ForeignKey])      
		SELECT	@InterfaceImportProductId     
				,'Unit'
				,NULL  
				,0
				,NULL      
				,0    
				,0    
				,0     
				,0      
				,0      
				,0      
				,0     
				,NULL      
				,NULL  
				,0    
				,0  
				,0  
				,0
				,0 
				,0
				,NULL
				,@ProductCode

      FETCH NEXT FROM db_cursor INTO @InterfaceImportProductId, @ProductCode
END 

CLOSE db_cursor  
DEALLOCATE db_cursor

UPDATE	InterfaceImportProduct
SET		RecordStatus = 'N'
WHERE	InterfaceImportProductId IN (SELECT InterfaceImportProductId FROM #Temp)

END

