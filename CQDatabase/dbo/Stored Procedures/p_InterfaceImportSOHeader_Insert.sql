﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOHeader_Insert
  ///   Filename       : p_InterfaceImportSOHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:15
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportSOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHeaderId int = null output,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordType nvarchar(60) = null,
  ///   @RecordStatus char(1) = null,
  ///   @CustomerCode nvarchar(60) = null,
  ///   @Customer nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @Route nvarchar(100) = null,
  ///   @DeliveryDate datetime = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportSOHeader.InterfaceImportSOHeaderId,
  ///   InterfaceImportSOHeader.PrimaryKey,
  ///   InterfaceImportSOHeader.OrderNumber,
  ///   InterfaceImportSOHeader.RecordType,
  ///   InterfaceImportSOHeader.RecordStatus,
  ///   InterfaceImportSOHeader.CustomerCode,
  ///   InterfaceImportSOHeader.Customer,
  ///   InterfaceImportSOHeader.Address,
  ///   InterfaceImportSOHeader.FromWarehouseCode,
  ///   InterfaceImportSOHeader.ToWarehouseCode,
  ///   InterfaceImportSOHeader.Route,
  ///   InterfaceImportSOHeader.DeliveryDate,
  ///   InterfaceImportSOHeader.Remarks,
  ///   InterfaceImportSOHeader.NumberOfLines,
  ///   InterfaceImportSOHeader.Additional1,
  ///   InterfaceImportSOHeader.Additional2,
  ///   InterfaceImportSOHeader.Additional3,
  ///   InterfaceImportSOHeader.Additional4,
  ///   InterfaceImportSOHeader.Additional5,
  ///   InterfaceImportSOHeader.Additional6,
  ///   InterfaceImportSOHeader.Additional7,
  ///   InterfaceImportSOHeader.Additional8,
  ///   InterfaceImportSOHeader.Additional9,
  ///   InterfaceImportSOHeader.Additional10,
  ///   InterfaceImportSOHeader.ProcessedDate,
  ///   InterfaceImportSOHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOHeader_Insert
(
 @InterfaceImportSOHeaderId int = null output,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @RecordType nvarchar(60) = null,
 @RecordStatus char(1) = null,
 @CustomerCode nvarchar(60) = null,
 @Customer nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @Route nvarchar(100) = null,
 @DeliveryDate datetime = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportSOHeaderId = '-1'
    set @InterfaceImportSOHeaderId = null;
  
	 declare @Error int
 
  insert InterfaceImportSOHeader
        (PrimaryKey,
         OrderNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Customer,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         Route,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         ProcessedDate,
         InsertDate)
  select @PrimaryKey,
         @OrderNumber,
         @RecordType,
         @RecordStatus,
         @CustomerCode,
         @Customer,
         @Address,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @Route,
         @DeliveryDate,
         @Remarks,
         @NumberOfLines,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         @ProcessedDate,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error, @InterfaceImportSOHeaderId = scope_identity()
  
  
  return @Error
  
end
