﻿/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_CR
  ///   Filename       : p_FamousBrands_Export_CR.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_FamousBrands_Export_CR
(
 @FileName varchar(30) = null output
)
as
begin
  set nocount on
  
  declare @TableResult as table
  (
   Id   int identity,
   Data varchar(500)
  )
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @InterfaceExportSOHeaderId int,
          @OrderNumber          varchar(30),
          @CustomerCode         varchar(30),
          @InvoiceNumber        varchar(30),
          @ProcessedDate        varchar(20),
          @FromWarehouseCode    varchar(10),
          @ForeignKey           varchar(30),
          @LineNumber           varchar(10),
          @ProductCode          varchar(30),
          @Batch                varchar(50),
          @Quantity             varchar(10),
          @NumberOfLines        varchar(10),
          @TotalQuantity        varchar(20),
          @UnitPrice            varchar(20),
          @ReasonCode           varchar(10),
          @Remarks              varchar(255),
          -- Internal
          @fetch_status_header  int,
          @fetch_status_detail  int,
          @print_string         varchar(255)
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare extract_header_cursor cursor for
  select top 1
         h.InterfaceExportSOHeaderId,
         h.PrimaryKey,
         isnull(h.Additional1, ''),
         isnull(replace(h.CustomerCode,'&','&amp;'), ''),
         convert(varchar(20), isnull(h.ProcessedDate, @Getdate), 120),
         convert(varchar(10), count(1)),
         convert(varchar(10), sum(convert(numeric(13,2), Quantity))),
         isnull(Remarks,''),
         isnull(h.Additional2, '')
    from InterfaceExportSOHeader h
    join InterfaceExportSODetail d on h.InterfaceExportSOHeaderId = d.InterfaceExportSOHeaderId
   where h.RecordStatus = 'N'
     and h.RecordType = 'CR'
   group by h.InterfaceExportSOHeaderId,
            h.PrimaryKey,
            isnull(h.Additional1, ''),
            isnull(replace(h.CustomerCode,'&','&amp;'), ''),
            convert(varchar(20), isnull(h.ProcessedDate, @Getdate), 120),
            isnull(FromWarehouseCode, ''),
            isnull(Remarks,''),
            isnull(h.Additional2, '')
   order by PrimaryKey
  
  open extract_header_cursor
  
  fetch extract_header_cursor into @InterfaceExportSOHeaderId,
                                   @OrderNumber,
                                   @InvoiceNumber,
                                   @CustomerCode,
                                   @ProcessedDate,
                                   @NumberOfLines,
                                   @TotalQuantity,
                                   @Remarks,
                                   @ReasonCode
  
  select @fetch_status_header = @@fetch_status
  
  while (@fetch_status_header = 0)
  begin
    update InterfaceExportSOHeader
       set RecordStatus = 'Y',
           ProcessedDate = @GetDate
     where InterfaceExportSOHeaderId = @InterfaceExportSOHeaderId
    
    set @FileName = @InvoiceNumber + '(' + convert(varchar(10), @InterfaceExportSOHeaderId) + ')'
    
    insert @TableResult (Data) select '<?xml version="1.0" encoding="Windows-1252" standalone="yes" ?>'
    insert @TableResult (Data) select '<root>'
    insert @TableResult (Data) select '  <CustomerReturn>'
    insert @TableResult (Data) select '    <CreditNo>' + @OrderNumber + '</CreditNo>'
    insert @TableResult (Data) select '    <InvoiceNumber>' + @InvoiceNumber + '</InvoiceNumber>'
    insert @TableResult (Data) select '    <CustomerCode>' + @CustomerCode + '</CustomerCode>'
    insert @TableResult (Data) select '    <CreateDate>' + @ProcessedDate + '</CreateDate>'
    insert @TableResult (Data) select '    <NoOfLines>' + @NumberOfLines + '</NoOfLines>'
    insert @TableResult (Data) select '    <TotalQty>' + @TotalQuantity + '</TotalQty>'
    insert @TableResult (Data) select '    <Remarks>' + @Remarks + '</Remarks>'
    insert @TableResult (Data) select '    <ReasonCode>' + replace(@ReasonCode,'CR','') + '</ReasonCode>'
    insert @TableResult (Data) select '  <CreditDetail>'
    
    
    declare extract_detail_cursor cursor for
    select ForeignKey,
           convert(varchar(10), LineNumber),
           isnull(ProductCode, ''),
           isnull(Batch, ''),
           convert(varchar(10), Quantity),
           isnull(Additional1, ''),
           isnull(Additional2, '')
      from InterfaceExportSODetail
     where InterfaceExportSOHeaderId = @InterfaceExportSOHeaderId
    order by LineNumber
    
    open extract_detail_cursor
    
    fetch extract_detail_cursor into @ForeignKey,
                                     @LineNumber,
                                     @ProductCode,
                                     @Batch,
                                     @Quantity,
                                     @FromWarehouseCode,
                                     @UnitPrice
    
    select @fetch_status_detail = @@fetch_status
    
    while (@fetch_status_detail = 0)
    begin
      insert @TableResult (Data) select '    <CreditLine>'
      insert @TableResult (Data) select '      <LineNumber>' + @LineNumber + '</LineNumber>'
      insert @TableResult (Data) select '      <ProductCode>' + @ProductCode + '</ProductCode>'
      insert @TableResult (Data) select '      <Location>' + @FromWarehouseCode + '</Location>'
      insert @TableResult (Data) select '      <Quantity>' + @Quantity + '</Quantity>'
      insert @TableResult (Data) select '      <UnappliedCredit>' + @UnitPrice + '</UnappliedCredit>'
      
      insert @TableResult (Data) select '    </CreditLine>'
      
      fetch extract_detail_cursor into @ForeignKey,
                                       @LineNumber,
                                       @ProductCode,
                                       @Batch,
                                       @Quantity,
                                       @FromWarehouseCode,
                                       @UnitPrice
      
      select @fetch_status_detail = @@fetch_status
    end
    
    close extract_detail_cursor
    deallocate extract_detail_cursor
    
    insert @TableResult (Data) select '    </CreditDetail>'
    insert @TableResult (Data) select '  </CustomerReturn>'
    insert @TableResult (Data) select '</root>'
    
    fetch extract_header_cursor into @InterfaceExportSOHeaderId,
                                     @OrderNumber,
                                     @InvoiceNumber,
                                     @CustomerCode,
                                     @ProcessedDate,
                                     @NumberOfLines,
                                     @TotalQuantity,
                                     @Remarks,
                                     @ReasonCode
    
    select @fetch_status_header = @@fetch_status
  end
  
  close extract_header_cursor
  deallocate extract_header_cursor
  
  select Data
    from @TableResult
  order by Id
end
