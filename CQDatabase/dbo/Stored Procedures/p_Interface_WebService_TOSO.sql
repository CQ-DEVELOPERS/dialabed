﻿create procedure [dbo].p_Interface_WebService_TOSO
(
@doc   varchar(max) output
)
--with encryption
as
begin
  declare @doc2                         xml
     set @doc2 = convert(xml,@doc)
  declare @idoc                         int
  declare @InsertDate                   datetime
  
     Set @InsertDate = Getdate()
         EXEC sp_xml_preparedocument @idoc OUTPUT, @doc2
         
  Insert Into
         InterfaceImportSOHeader(  RecordStatus,
                                 InsertDate,
                                        PrimaryKey ,
                                        OrderNumber  ,
                                        RecordType ,
                                        Additional1 ,
                                        Remarks ,
                                        FromWarehouseCode ,
                                        ToWarehouseCode ,
                                        Additional4 ,
                                        Additional10 ,
                                        Additional5 ,
                                        Additional6 ,
                                        Additional7 ,
                                        DeliveryDate ,
                                        Additional3)
  SELECT 'W',@InsertDate,*
    FROM OPENXML (@idoc, '/root/TransferOrder',1)
         WITH (PrimaryKey varchar(50) 'IDWhseIBT',
         OrderNumber  varchar(50) 'cIBTNumber',
         RecordType varchar(50) 'RecordType',
         Additional1 varchar(50) 'CreateDate',
         Remarks varchar(50) 'cIBTDescription',
         FromWarehouseCode varchar(50)  'iWhseIDFrom',
         ToWarehouseCode varchar(50) 'iWhseIDTo',
         Additional4 varchar(50) 'iWhseIDIntransit',
         Additional10 varchar(50) 'iIBTStatus',
         Additional5 varchar(50) 'cDelNoteNumber',
         Additional6 varchar(50) 'iWhseIDVariance',
         Additional7 varchar(50) 'iWhseIDDamaged',
         DeliveryDate varchar(50) 'dDateIssued',
         Additional3 varchar(50) 'dDateReceived'
         )
         
  Insert Into InterfaceImportSODetail (ForeignKey,
                                              LineNumber,
                                              ProductCode,
                                              Quantity,
                                              Additional1,
                                              Additional2,
                                              Additional3)
  Select *
    FROM OPENXML (@idoc, '/root/TransferOrder/TransferOrderLine',1)
         WITH (
                      ForeignKey  varchar(50) 'IDWhseIBT',
                      LineNumber varchar(50) 'IDWhseIBTLines',
                      ProductCode varchar(50) 'iStockID',
                      Quantity varchar(50) 'fQtyIssued',
                      Additional1 varchar(50)  'fQtyReceived',
                      Additional2 varchar(50)  'fQtyDamaged',
                      Additional3 varchar(50)  'fQtyVariance')
         update d
     set InterfaceImportSOHeaderId = h.InterfaceImportSOHeaderid
    from InterfaceImportSoDetail d
         Join InterfaceImportSOHeader h on h.PrimaryKey = d.ForeignKey
   where h.RecordStatus = 'W' and InsertDate = @InsertDate and d.InterfaceImportSOHeaderid is null
  
  Update InterfaceImportSOHeader set RecordStatus = 'N'
   Where RecordSTatus = 'W' and InsertDate = @InsertDate
  
  exec p_Pastel_Import_TO
  
  Set @doc2 = ''
End
