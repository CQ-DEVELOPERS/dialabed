﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Result_Search
  ///   Filename       : p_Result_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:08
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Result table.
  /// </remarks>
  /// <param>
  ///   @ResultId int = null output,
  ///   @ResultCode nvarchar(20) = null,
  ///   @Result nvarchar(510) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Result.ResultId,
  ///   Result.ResultCode,
  ///   Result.Result,
  ///   Result.Pass,
  ///   Result.StartRange,
  ///   Result.EndRange 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Result_Search
(
 @ResultId int = null output,
 @ResultCode nvarchar(20) = null,
 @Result nvarchar(510) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ResultId = '-1'
    set @ResultId = null;
  
  if @ResultCode = '-1'
    set @ResultCode = null;
  
  if @Result = '-1'
    set @Result = null;
  
 
  select
         Result.ResultId
        ,Result.ResultCode
        ,Result.Result
        ,Result.Pass
        ,Result.StartRange
        ,Result.EndRange
    from Result
   where isnull(Result.ResultId,'0')  = isnull(@ResultId, isnull(Result.ResultId,'0'))
     and isnull(Result.ResultCode,'%')  like '%' + isnull(@ResultCode, isnull(Result.ResultCode,'%')) + '%'
     and isnull(Result.Result,'%')  like '%' + isnull(@Result, isnull(Result.Result,'%')) + '%'
  order by ResultCode
  
end
