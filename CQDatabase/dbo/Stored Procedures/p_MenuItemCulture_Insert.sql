﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItemCulture_Insert
  ///   Filename       : p_MenuItemCulture_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:32
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MenuItemCulture table.
  /// </remarks>
  /// <param>
  ///   @MenuItemCultureId int = null output,
  ///   @MenuItemId int = null,
  ///   @CultureId int = null,
  ///   @MenuItem nvarchar(100) = null,
  ///   @ToolTip nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   MenuItemCulture.MenuItemCultureId,
  ///   MenuItemCulture.MenuItemId,
  ///   MenuItemCulture.CultureId,
  ///   MenuItemCulture.MenuItem,
  ///   MenuItemCulture.ToolTip 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItemCulture_Insert
(
 @MenuItemCultureId int = null output,
 @MenuItemId int = null,
 @CultureId int = null,
 @MenuItem nvarchar(100) = null,
 @ToolTip nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
  if @MenuItemCultureId = '-1'
    set @MenuItemCultureId = null;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
	 declare @Error int
 
  insert MenuItemCulture
        (MenuItemId,
         CultureId,
         MenuItem,
         ToolTip)
  select @MenuItemId,
         @CultureId,
         @MenuItem,
         @ToolTip 
  
  select @Error = @@Error, @MenuItemCultureId = scope_identity()
  
  
  return @Error
  
end
