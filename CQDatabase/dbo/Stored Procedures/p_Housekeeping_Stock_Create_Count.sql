﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Create_Count
  ///   Filename       : p_Housekeeping_Stock_Create_Count.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Create_Count
(
 @WarehouseId int = null
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Housekeeping_Stock_Create_Count',
          @GetDate           datetime,
          @Transaction       bit = 1,
          @StartDate         datetime,
          @EndDate           datetime,
          @Days              int,
          @ProductCategory   char(1),
          @StorageUnitId     int,
          @LocalWarehouseId  int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  set @StartDate = getdate()
  select @EndDate = dateadd(millisecond, -3,
                    dateadd(year, datediff(year, 0, getdate()) + 1, 0))
  
  select @Days = (datediff(dd, @StartDate, @EndDate) + 1)
                -(datediff(wk, @StartDate, @EndDate) * 2)
                -(case when datename(dw, @StartDate) = 'Sunday' then 1 else 0 end)
                -(case when datename(dw, @EndDate) = 'Saturday' then 1 else 0 end)
  
  if @Error <> 0 or @Days is null
    goto error
  
  insert StockTakeStorageUnit
        (WarehouseId,
         StorageUnitId,
         StockTakeCounts)
  select distinct a.WarehouseId,
         su.StorageUnitId,
         0
    from StorageUnit                su (nolock)
    join StorageUnitBatch          sub (nolock) on su.StorageUnitId = sub.StorageUnitId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join AreaLocation               al (nolock) on subl.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId = a.AreaId
    left
    join StockTakeStorageUnit       ss (nolock) on su.StorageUnitId = ss.StorageUnitId
                                               and a.WarehouseId = ss.WarehouseId
   where ss.StockTakeStorageUnitId is null
  
  update StockTakeStorageUnit
     set SOH = 0
  
  update ss
     set SOH = 1
    from StorageUnit                su (nolock)
    join StorageUnitBatch          sub (nolock) on su.StorageUnitId = sub.StorageUnitId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join AreaLocation               al (nolock) on subl.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId = a.AreaId
    join StockTakeStorageUnit       ss (nolock) on su.StorageUnitId = ss.StorageUnitId
                                               and a.WarehouseId = ss.WarehouseId
  
  update stc
     set StorageUnitTotal = (select count(1)
                               from StockTakeStorageUnit ss
                               join StorageUnit          su on ss.StorageUnitId = su.StorageUnitId
                              where stc.WarehouseId = ss.WarehouseId
                                and stc.ProductCategory = su.ProductCategory
                                and ss.SOH = 1)
    from StockTakeCycleCount stc
  
  update stc
     set StockTakeAlreadyCounted = (select sum(isnull(ss.StockTakeCounts,0))
                                      from StockTakeStorageUnit ss
                                      join StorageUnit          su on ss.StorageUnitId = su.StorageUnitId
                                     where stc.WarehouseId = ss.WarehouseId
                                       and stc.ProductCategory = su.ProductCategory
                                       and ss.SOH = 1)
    from StockTakeCycleCount stc
  
  update StockTakeCycleCount set CountsPerYear = 1 where ProductCategory = 'C'
  update StockTakeCycleCount set StorageUnitTotal = 0 where StorageUnitTotal IS NULL 
  update StockTakeCycleCount set CountsPerYear = 0 where CountsPerYear IS NULL 
  update StockTakeCycleCount set StockTakeAlreadyCounted = 0 where StockTakeAlreadyCounted IS NULL 
  
  update stc
     set RequiredDailyCount = ((StorageUnitTotal * CountsPerYear) - StockTakeAlreadyCounted) / @Days
    from StockTakeCycleCount stc
  
  select *, @Days as 'Days' from StockTakeCycleCount
  --select * from StockTakeStorageUnit
  
  while exists(select top 1 1
                 from StockTakeCycleCount (nolock)
                where RequiredDailyCount > 0
                  and WarehouseId = isnull(@WarehouseId, WarehouseId))
  begin
    set @StorageUnitId = null
    
    select top 1
           @LocalWarehouseId = stc.WarehouseId,
           @ProductCategory  = stc.ProductCategory,
           @StorageUnitId    = su.StorageUnitId
      from StockTakeCycleCount  stc (nolock)
      join StockTakeStorageUnit ssu (nolock) on stc.WarehouseId = ssu.WarehouseId
      join StorageUnit           su (nolock) on ssu.StorageUnitId = su.StorageUnitId
                                            and stc.ProductCategory = su.ProductCategory
     where stc.WarehouseId = isnull(@WarehouseId, stc.WarehouseId)
       and stc.RequiredDailyCount > 0
    order by SOH, ssu.StockTakeCounts asc
    
    select @LocalWarehouseId as '@LocalWarehouseId',
           @ProductCategory  as '@ProductCategory',
           @StorageUnitId    as '@StorageUnitId'
    
    --if @@ROWCOUNT = 0
    --  update StockTakeCycleCount
    --     set RequiredDailyCount = 0
    --   where WarehouseId     = @LocalWarehouseId
    --     and ProductCategory = @ProductCategory
    --else
    update StockTakeCycleCount
       set RequiredDailyCount = RequiredDailyCount - 1
     where WarehouseId     = @LocalWarehouseId
       and ProductCategory = @ProductCategory
    
    update StockTakeStorageUnit
       set StockTakeCounts = StockTakeCounts + 1
     where WarehouseId     = @LocalWarehouseId
       and StorageUnitId   = @StorageUnitId
    
    exec p_Housekeeping_Stock_Take_Create_Product
     @warehouseId			       = @LocalWarehouseId,
     @operatorId			        = null,
     @jobId					           = null,
     @locationId           = null,
     @storageUnitId			     = @StorageUnitId
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
