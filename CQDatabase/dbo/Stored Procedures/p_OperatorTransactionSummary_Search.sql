﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorTransactionSummary_Search
  ///   Filename       : p_OperatorTransactionSummary_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:33
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorTransactionSummary table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorTransactionSummary.OperatorId,
  ///   OperatorTransactionSummary.InstructionTypeId,
  ///   OperatorTransactionSummary.EndDate,
  ///   OperatorTransactionSummary.Units,
  ///   OperatorTransactionSummary.Weight,
  ///   OperatorTransactionSummary.Instructions,
  ///   OperatorTransactionSummary.Jobs,
  ///   OperatorTransactionSummary.ActiveTime,
  ///   OperatorTransactionSummary.DwellTime,
  ///   OperatorTransactionSummary.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorTransactionSummary_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OperatorTransactionSummary.OperatorId
        ,OperatorTransactionSummary.InstructionTypeId
        ,OperatorTransactionSummary.EndDate
        ,OperatorTransactionSummary.Units
        ,OperatorTransactionSummary.Weight
        ,OperatorTransactionSummary.Instructions
        ,OperatorTransactionSummary.Jobs
        ,OperatorTransactionSummary.ActiveTime
        ,OperatorTransactionSummary.DwellTime
        ,OperatorTransactionSummary.WarehouseId
    from OperatorTransactionSummary
  
end
