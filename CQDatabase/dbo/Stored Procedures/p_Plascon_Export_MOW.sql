﻿
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Export_MOW
  ///   Filename       : p_Plascon_Export_MOW.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 2011-11-04
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Plascon_Export_MOW]
as
begin
	 set nocount on;
  
    declare @Error             int,
            @Errormsg          varchar(500),
            @GetDate           datetime,
            @RecordType        char(3),
            @SequenceNumber    int,
            @Increment         int
  
    select @RecordType = 'MOW'
          ,@GetDate = dbo.ufn_Getdate()
          ,@Errormsg = 'p_Plascon_Export_MOW - Error executing p_Plascon_Export_MOW'
  
    begin transaction
    
    update InterfaceExportStockAdjustment
       set RecordStatus  = 'Y',
           ProcessedDate = @getdate
     where RecordStatus = 'N'
       and RecordType   = isnull(@RecordType,RecordType)
               
    select @Increment = @@ROWCOUNT
    
    select @Error = @@Error, @Errormsg = 'p_Plascon_Export_MOW - Error updating InterfaceExportStockAdjustment'
  
    if @Error <> 0
        goto error
  
    exec p_Sequence_Number 
                 @Value = @SequenceNumber output
                ,@Increment = @Increment
    
    insert interfaceextract
           (
            RecordType,
            SequenceNumber,
            OrderNumber,
            LineNumber,
            RecordStatus,
            ExtractedDate,
            Data
           )
    select  @RecordType,
            sequence_no,
            null,
            null,
            'N',
            extract_date,            
            replicate('0',9  - datalength(convert(varchar(9),sequence_no))) + convert(varchar(9),sequence_no) + 
            record_type + 
            replicate('0',3  - datalength(convert(varchar(3),branch_code))) + convert(varchar(3),branch_code) + 
            replicate(' ',9 - datalength(convert(varchar(9),ltrim(stock_no)))) + convert(varchar(9),ltrim(stock_no))     +  
            replicate('0',4  - datalength(convert(varchar(4),sku_code))) + convert(varchar(4),sku_code) + 
            reason_code +
            replicate('0',6 - datalength(convert(varchar(6),convert(int,qty)))) + convert(varchar(6),convert(int,qty))
    From
        (Select  s.sequence_no
                ,@RecordType As record_type
                ,'045' As branch_code 
                ,h.ProductCode As stock_no 
                ,h.SKUCode As sku_code
                ,isnull(h.Additional3,'52') As reason_code
                ,h.Quantity As qty 
                ,CONVERT(Varchar(10), @GetDate, 120) As extract_date
         From 
            (select InterfaceExportStockAdjustmentId, ROW_NUMBER() Over (Order By InterfaceExportStockAdjustmentId) + @SequenceNumber As sequence_no 
             from InterfaceExportStockAdjustment where ProcessedDate = @GetDate) s 
            Inner Join InterfaceExportStockAdjustment h on s.InterfaceExportStockAdjustmentId = h.InterfaceExportStockAdjustmentId 
            ) ExtractTable
        Order By sequence_no
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end

