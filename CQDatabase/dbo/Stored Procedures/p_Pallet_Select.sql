﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Select
  ///   Filename       : p_Pallet_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:30
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Pallet table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null 
  /// </param>
  /// <returns>
  ///   Pallet.PalletId,
  ///   Pallet.StorageUnitBatchId,
  ///   Pallet.LocationId,
  ///   Pallet.Weight,
  ///   Pallet.Tare,
  ///   Pallet.Quantity,
  ///   Pallet.Prints,
  ///   Pallet.CreateDate,
  ///   Pallet.Nett,
  ///   Pallet.ReasonId,
  ///   Pallet.EmptyWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Select
(
 @PalletId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Pallet.PalletId
        ,Pallet.StorageUnitBatchId
        ,Pallet.LocationId
        ,Pallet.Weight
        ,Pallet.Tare
        ,Pallet.Quantity
        ,Pallet.Prints
        ,Pallet.CreateDate
        ,Pallet.Nett
        ,Pallet.ReasonId
        ,Pallet.EmptyWeight
    from Pallet
   where isnull(Pallet.PalletId,'0')  = isnull(@PalletId, isnull(Pallet.PalletId,'0'))
  
end
