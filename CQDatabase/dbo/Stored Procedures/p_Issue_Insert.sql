﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Issue_Insert
  ///   Filename       : p_Issue_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2015 09:30:34
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Issue table.
  /// </remarks>
  /// <param>
  ///   @IssueId int = null output,
  ///   @OutboundDocumentId int = null,
  ///   @PriorityId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @RouteId int = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @DropSequence int = null,
  ///   @LoadIndicator bit = null,
  ///   @DespatchBay int = null,
  ///   @RoutingSystem bit = null,
  ///   @Delivery int = null,
  ///   @NumberOfLines int = null,
  ///   @Total int = null,
  ///   @Complete int = null,
  ///   @AreaType nvarchar(20) = null,
  ///   @Units int = null,
  ///   @ShortPicks int = null,
  ///   @Releases int = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null,
  ///   @FirstLoaded datetime = null,
  ///   @Interfaced bit = null,
  ///   @Loaded int = null,
  ///   @Pallets int = null,
  ///   @Volume float = null,
  ///   @ContactListId int = null,
  ///   @ManufacturingId int = null,
  ///   @ReservedId int = null,
  ///   @ParentIssueId int = null,
  ///   @ASN bit = null,
  ///   @WaveId int = null,
  ///   @ReserveBatch bit = null,
  ///   @InvoiceNumber nvarchar(60) = null,
  ///   @TotalDue numeric(13,2) = null,
  ///   @AddressLine1 nvarchar(510) = null,
  ///   @AddressLine2 nvarchar(510) = null,
  ///   @AddressLine3 nvarchar(510) = null,
  ///   @AddressLine4 nvarchar(510) = null,
  ///   @AddressLine5 nvarchar(510) = null,
  ///   @PlanningComplete datetime = null,
  ///   @PlannedBy int = null,
  ///   @Release datetime = null,
  ///   @ReleasedBy int = null,
  ///   @Checking datetime = null,
  ///   @Checked datetime = null,
  ///   @Despatch datetime = null,
  ///   @DespatchChecked datetime = null,
  ///   @CompleteDate datetime = null,
  ///   @Availability nvarchar(40) = null,
  ///   @CheckingCount int = null,
  ///   @Branding bit = null,
  ///   @Downsizing bit = null,
  ///   @CheckSheetPrinted bit = null,
  ///   @CreateDate datetime = null,
  ///   @VehicleId int = null 
  /// </param>
  /// <returns>
  ///   Issue.IssueId,
  ///   Issue.OutboundDocumentId,
  ///   Issue.PriorityId,
  ///   Issue.WarehouseId,
  ///   Issue.LocationId,
  ///   Issue.StatusId,
  ///   Issue.OperatorId,
  ///   Issue.RouteId,
  ///   Issue.DeliveryNoteNumber,
  ///   Issue.DeliveryDate,
  ///   Issue.SealNumber,
  ///   Issue.VehicleRegistration,
  ///   Issue.Remarks,
  ///   Issue.DropSequence,
  ///   Issue.LoadIndicator,
  ///   Issue.DespatchBay,
  ///   Issue.RoutingSystem,
  ///   Issue.Delivery,
  ///   Issue.NumberOfLines,
  ///   Issue.Total,
  ///   Issue.Complete,
  ///   Issue.AreaType,
  ///   Issue.Units,
  ///   Issue.ShortPicks,
  ///   Issue.Releases,
  ///   Issue.Weight,
  ///   Issue.ConfirmedWeight,
  ///   Issue.FirstLoaded,
  ///   Issue.Interfaced,
  ///   Issue.Loaded,
  ///   Issue.Pallets,
  ///   Issue.Volume,
  ///   Issue.ContactListId,
  ///   Issue.ManufacturingId,
  ///   Issue.ReservedId,
  ///   Issue.ParentIssueId,
  ///   Issue.ASN,
  ///   Issue.WaveId,
  ///   Issue.ReserveBatch,
  ///   Issue.InvoiceNumber,
  ///   Issue.TotalDue,
  ///   Issue.AddressLine1,
  ///   Issue.AddressLine2,
  ///   Issue.AddressLine3,
  ///   Issue.AddressLine4,
  ///   Issue.AddressLine5,
  ///   Issue.PlanningComplete,
  ///   Issue.PlannedBy,
  ///   Issue.Release,
  ///   Issue.ReleasedBy,
  ///   Issue.Checking,
  ///   Issue.Checked,
  ///   Issue.Despatch,
  ///   Issue.DespatchChecked,
  ///   Issue.CompleteDate,
  ///   Issue.Availability,
  ///   Issue.CheckingCount,
  ///   Issue.Branding,
  ///   Issue.Downsizing,
  ///   Issue.CheckSheetPrinted,
  ///   Issue.CreateDate,
  ///   Issue.VehicleId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Issue_Insert
(
 @IssueId int = null output,
 @OutboundDocumentId int = null,
 @PriorityId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @RouteId int = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Remarks nvarchar(500) = null,
 @DropSequence int = null,
 @LoadIndicator bit = null,
 @DespatchBay int = null,
 @RoutingSystem bit = null,
 @Delivery int = null,
 @NumberOfLines int = null,
 @Total int = null,
 @Complete int = null,
 @AreaType nvarchar(20) = null,
 @Units int = null,
 @ShortPicks int = null,
 @Releases int = null,
 @Weight float = null,
 @ConfirmedWeight float = null,
 @FirstLoaded datetime = null,
 @Interfaced bit = null,
 @Loaded int = null,
 @Pallets int = null,
 @Volume float = null,
 @ContactListId int = null,
 @ManufacturingId int = null,
 @ReservedId int = null,
 @ParentIssueId int = null,
 @ASN bit = null,
 @WaveId int = null,
 @ReserveBatch bit = null,
 @InvoiceNumber nvarchar(60) = null,
 @TotalDue numeric(13,2) = null,
 @AddressLine1 nvarchar(510) = null,
 @AddressLine2 nvarchar(510) = null,
 @AddressLine3 nvarchar(510) = null,
 @AddressLine4 nvarchar(510) = null,
 @AddressLine5 nvarchar(510) = null,
 @PlanningComplete datetime = null,
 @PlannedBy int = null,
 @Release datetime = null,
 @ReleasedBy int = null,
 @Checking datetime = null,
 @Checked datetime = null,
 @Despatch datetime = null,
 @DespatchChecked datetime = null,
 @CompleteDate datetime = null,
 @Availability nvarchar(40) = null,
 @CheckingCount int = null,
 @Branding bit = null,
 @Downsizing bit = null,
 @CheckSheetPrinted bit = null,
 @CreateDate datetime = null,
 @VehicleId int = null 
)
 
as
begin
	 set nocount on;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @ManufacturingId = '-1'
    set @ManufacturingId = null;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
  if @PlannedBy = '-1'
    set @PlannedBy = null;
  
  if @ReleasedBy = '-1'
    set @ReleasedBy = null;
  
  if @VehicleId = '-1'
    set @VehicleId = null;
  
	 declare @Error int
 
  insert Issue
        (OutboundDocumentId,
         PriorityId,
         WarehouseId,
         LocationId,
         StatusId,
         OperatorId,
         RouteId,
         DeliveryNoteNumber,
         DeliveryDate,
         SealNumber,
         VehicleRegistration,
         Remarks,
         DropSequence,
         LoadIndicator,
         DespatchBay,
         RoutingSystem,
         Delivery,
         NumberOfLines,
         Total,
         Complete,
         AreaType,
         Units,
         ShortPicks,
         Releases,
         Weight,
         ConfirmedWeight,
         FirstLoaded,
         Interfaced,
         Loaded,
         Pallets,
         Volume,
         ContactListId,
         ManufacturingId,
         ReservedId,
         ParentIssueId,
         ASN,
         WaveId,
         ReserveBatch,
         InvoiceNumber,
         TotalDue,
         AddressLine1,
         AddressLine2,
         AddressLine3,
         AddressLine4,
         AddressLine5,
         PlanningComplete,
         PlannedBy,
         Release,
         ReleasedBy,
         Checking,
         Checked,
         Despatch,
         DespatchChecked,
         CompleteDate,
         Availability,
         CheckingCount,
         Branding,
         Downsizing,
         CheckSheetPrinted,
         CreateDate,
         VehicleId)
  select @OutboundDocumentId,
         @PriorityId,
         @WarehouseId,
         @LocationId,
         @StatusId,
         @OperatorId,
         @RouteId,
         @DeliveryNoteNumber,
         @DeliveryDate,
         @SealNumber,
         @VehicleRegistration,
         @Remarks,
         @DropSequence,
         @LoadIndicator,
         @DespatchBay,
         @RoutingSystem,
         @Delivery,
         @NumberOfLines,
         @Total,
         @Complete,
         @AreaType,
         @Units,
         @ShortPicks,
         @Releases,
         @Weight,
         @ConfirmedWeight,
         @FirstLoaded,
         @Interfaced,
         @Loaded,
         @Pallets,
         @Volume,
         @ContactListId,
         @ManufacturingId,
         @ReservedId,
         @ParentIssueId,
         @ASN,
         @WaveId,
         @ReserveBatch,
         @InvoiceNumber,
         @TotalDue,
         @AddressLine1,
         @AddressLine2,
         @AddressLine3,
         @AddressLine4,
         @AddressLine5,
         @PlanningComplete,
         @PlannedBy,
         @Release,
         @ReleasedBy,
         @Checking,
         @Checked,
         @Despatch,
         @DespatchChecked,
         @CompleteDate,
         @Availability,
         @CheckingCount,
         @Branding,
         @Downsizing,
         @CheckSheetPrinted,
         @CreateDate,
         @VehicleId 
  
  select @Error = @@Error, @IssueId = scope_identity()
  
  if @Error = 0
    exec @Error = p_IssueHistory_Insert
         @IssueId = @IssueId,
         @OutboundDocumentId = @OutboundDocumentId,
         @PriorityId = @PriorityId,
         @WarehouseId = @WarehouseId,
         @LocationId = @LocationId,
         @StatusId = @StatusId,
         @OperatorId = @OperatorId,
         @RouteId = @RouteId,
         @DeliveryNoteNumber = @DeliveryNoteNumber,
         @DeliveryDate = @DeliveryDate,
         @SealNumber = @SealNumber,
         @VehicleRegistration = @VehicleRegistration,
         @Remarks = @Remarks,
         @DropSequence = @DropSequence,
         @LoadIndicator = @LoadIndicator,
         @DespatchBay = @DespatchBay,
         @RoutingSystem = @RoutingSystem,
         @Delivery = @Delivery,
         @NumberOfLines = @NumberOfLines,
         @Total = @Total,
         @Complete = @Complete,
         @AreaType = @AreaType,
         @Units = @Units,
         @ShortPicks = @ShortPicks,
         @Releases = @Releases,
         @Weight = @Weight,
         @ConfirmedWeight = @ConfirmedWeight,
         @FirstLoaded = @FirstLoaded,
         @Interfaced = @Interfaced,
         @Loaded = @Loaded,
         @Pallets = @Pallets,
         @Volume = @Volume,
         @ContactListId = @ContactListId,
         @ManufacturingId = @ManufacturingId,
         @ReservedId = @ReservedId,
         @ParentIssueId = @ParentIssueId,
         @ASN = @ASN,
         @WaveId = @WaveId,
         @ReserveBatch = @ReserveBatch,
         @InvoiceNumber = @InvoiceNumber,
         @TotalDue = @TotalDue,
         @AddressLine1 = @AddressLine1,
         @AddressLine2 = @AddressLine2,
         @AddressLine3 = @AddressLine3,
         @AddressLine4 = @AddressLine4,
         @AddressLine5 = @AddressLine5,
         @PlanningComplete = @PlanningComplete,
         @PlannedBy = @PlannedBy,
         @Release = @Release,
         @ReleasedBy = @ReleasedBy,
         @Checking = @Checking,
         @Checked = @Checked,
         @Despatch = @Despatch,
         @DespatchChecked = @DespatchChecked,
         @CompleteDate = @CompleteDate,
         @Availability = @Availability,
         @CheckingCount = @CheckingCount,
         @Branding = @Branding,
         @Downsizing = @Downsizing,
         @CheckSheetPrinted = @CheckSheetPrinted,
         @CreateDate = @CreateDate,
         @VehicleId = @VehicleId,
         @CommandType = 'Insert'
  
  return @Error
  
end
