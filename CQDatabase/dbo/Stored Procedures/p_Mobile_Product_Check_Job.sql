﻿--IF OBJECT_ID('dbo.p_Mobile_Product_Check_Job') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Mobile_Product_Check_Job
--    IF OBJECT_ID('dbo.p_Mobile_Product_Check_Job') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Mobile_Product_Check_Job >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Mobile_Product_Check_Job >>>'
--END
--go
 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Job
  ///   Filename       : p_Mobile_Product_Check_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Job
(
 @WarehouseId     int,
 @ReferenceNumber nvarchar(50)
)
 
as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         WarehouseId,
         ReferenceNumber,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @WarehouseId,
         @ReferenceNumber,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @TableResult as table
  (
   OrderNumber     nvarchar(30),
   JobId           int,
   ReferenceNumber nvarchar(50),
   Checked         int,
   Total           int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int,
          @PalletId          int,
          @Barcode           nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(@ReferenceNumber) = 1
    set @ReferenceNumber = 'J:' + @ReferenceNumber
  
  if isnumeric(replace(@ReferenceNumber,'J:','')) = 1
  begin
    -- First Check if Inter Branch Transfer Putaway
    select @JobId = max(j.JobId)
      from Job         j (nolock)
      join Instruction i (nolock) on j.JobId = i.JobId
     where j.ReferenceNumber = @ReferenceNumber
       and j.WarehouseId     = @WarehouseId
    
    if @JobId is null
    begin
      select @JobId           = replace(@ReferenceNumber,'J:',''),
             @ReferenceNumber = null
      
      select top 1
             @JobId = JobId
        from Job j (nolock)
       where JobId       = @JobId
         and WarehouseId = @WarehouseId
    end
  end
  else if isnumeric(replace(@ReferenceNumber,'P:','')) = 1
  begin
    select @PalletId        = replace(@ReferenceNumber,'P:',''),
           @ReferenceNumber = null
    
    select top 1
           @JobId      = j.JobId
      from Instruction i (nolock)
      join Job         j (nolock) on i.JobId = j.JobId
      join Status      s (nolock) on j.StatusId = s.StatusId
     where i.PalletId    = @PalletId
       and s.StatusCode in ('CK','PR')--,'CD','A')
       and j.WarehouseId = @WarehouseId
  end
  else if isnumeric(replace(@ReferenceNumber,'R:','')) = 1 or isnumeric(replace(@ReferenceNumber,'NB:','')) = 1
  begin
    select @Barcode         = @ReferenceNumber,
           @ReferenceNumber = null
    
    select top 1
           @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @Barcode
       and WarehouseId     = @WarehouseId
  end
  
  --insert @TableResult
  --      (JobId,
  --       ReferenceNumber)
  --select JobId,
  --       ReferenceNumber
  --  from Job (nolock)
  -- where JobId = @JobId
  
  --update tr
  --   set Total = (select COUNT(1)
  --                   from Instruction i (nolock)
  --                  where tr.JobId = i.JobId)
  --  from @TableResult tr
  
  --update tr
  --   set Checked = (select COUNT(1)
  --                    from Instruction i (nolock)
  --                   where tr.JobId = i.JobId
  --                     and i.CheckQuantity is not null)
  --  from @TableResult tr
  
  If (Select s.statusCode 
		From Job j (nolock)
			Join Status s (nolock) on s.StatusId = j.StatusId
		where j.JobId = @JobId) not in ('CK','QA','PR'             )
 Begin
	select 0 as 'Finished',
         OrderNumber,
         JobId,
         ReferenceNumber,
         Lines,
         Quantity,
         ConfirmedQuantity,
         CheckQuantity
    from CheckingProduct (nolock)
   where JobId = -100
 
	
	Return
 End
  
  
  if not exists(select 1 from CheckingProduct (nolock) where JobId = @JobId)
  begin
    begin transaction
    if dbo.ufn_Configuration(34, @WarehouseId) = 1
    begin
      insert CheckingProduct
            (InstructionId,
             InstructionRefId,
             WarehouseId,
             JobId,
             ReferenceNumber,
             StorageUnitBatchId,
             Quantity,
             ConfirmedQuantity,
             CheckQuantity)
      select i.InstructionId,
             isnull(i.InstructionRefId, i.InstructionId),
             j.WarehouseId,
             j.JobId,
             j.ReferenceNumber,
             i.StorageUnitBatchId,
             i.Quantity,
             i.ConfirmedQuantity,
             i.CheckQuantity
        from Job         j (nolock)
        join Instruction i (nolock) on j.JobId = i.JobId
       where j.JobId = @JobId 
         and i.ConfirmedQuantity > 0
      
      if @@Error != 0
      begin
        rollback transaction
        return
      end
    end
    else
    begin
      insert CheckingProduct
            (InstructionId,
             InstructionRefId,
             WarehouseId,
             JobId,
             ReferenceNumber,
             StorageUnitBatchId,
             Quantity,
             ConfirmedQuantity,
             CheckQuantity)
      select i.InstructionId,
             isnull(i.InstructionRefId, i.InstructionId),
             j.WarehouseId,
             j.JobId,
             j.ReferenceNumber,
             sub2.StorageUnitBatchId,
             i.Quantity,
             i.ConfirmedQuantity,
             i.CheckQuantity
        from Job                 j (nolock)
        join Instruction         i (nolock) on j.JobId = i.JobId
        join StorageUnitBatch sub1 (nolock) on i.StorageUnitBatchId = sub1.StorageUnitBatchId
        join StorageUnitBatch sub2 (nolock) on sub1.StorageUnitId   = sub2.StorageUnitId
        join Batch               b (nolock) on sub2.BatchId         = b.BatchId
                                           and b.batch             = 'Default'
       where j.JobId = @JobId
         and i.ConfirmedQuantity > 0
      
      if @@Error != 0
      begin
        rollback transaction
        return
      end
    end
    
    update cp
       set StorageUnitId = su.StorageUnitId,
           ProductCode    = p.ProductCode,
           ProductBarcode = p.Barcode,
           --PackBarcode    = pk.Barcode,
           Product        = p.Product,
           SKUCode        = sku.SKUCode,
           Batch          = b.Batch,
           ExpiryDate     = b.ExpiryDate
      from CheckingProduct   cp (nolock)
      join StorageUnitBatch sub (nolock) on cp.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product            p (nolock) on su.ProductId          = p.ProductId
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId
      join Batch              b (nolock) on sub.BatchId           = b.BatchId
     where cp.JobId = @JobId 
    
    if @@Error != 0
    begin
      rollback transaction
      return
    end
    
    commit transaction
    
    update cp
       set OrderNumber = od.OrderNumber + ' / ' + ExternalCompany
      from CheckingProduct       cp (nolock)
      join IssueLineInstruction ili (nolock) on cp.InstructionRefId    = ili.InstructionId
      join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
      join ExternalCompany       ec (nolock) on od.ExternalCompanyId   = ec.ExternalCompanyId
     where cp.JobId = @JobId
  end
  
  update CheckingProduct
     set Checked = (select COUNT(1)
                      from CheckingProduct (nolock)
                     where JobId = @JobId
                       and CheckQuantity is not null)
   where JobId = @JobId
  
  update CheckingProduct
     set Total = (select COUNT(1)
                    from CheckingProduct (nolock)
                   where JobId = @JobId)
   where JobId = @JobId
  
  update CheckingProduct
     set Lines = CONVERT(nvarchar(10), Checked) + ' of ' + CONVERT(nvarchar(10), Total)
   where JobId = @JobId
  
  select case when sum(CheckQuantity) >= sum(ConfirmedQuantity)
              then 1
              else 0
              end as 'Finished',
         OrderNumber,
         JobId,
         ReferenceNumber,
         Lines,
         sum(Quantity) as 'Quantity',
         sum(ConfirmedQuantity) as 'ConfirmedQuantity',
         sum(CheckQuantity) as 'CheckQuantity'
    from CheckingProduct (nolock)
   where JobId = @JobId
  group by OrderNumber,
           JobId,
           ReferenceNumber,
           Lines
  
  update MobileLog
     set EndDate = getdate()
   where MobileLogId = @MobileLogId
end
--go
--IF OBJECT_ID('dbo.p_Mobile_Product_Check_Job') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Mobile_Product_Check_Job >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Mobile_Product_Check_Job >>>'
--go


