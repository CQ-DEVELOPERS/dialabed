﻿/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_SR
  ///   Filename       : p_FamousBrands_Export_SR.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_FamousBrands_Export_SR
(
 @FileName varchar(30) = null output
)
as
begin
  set nocount on
  return
  declare @TableResult as table
  (
   Data varchar(500)
  )
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @OrderNumber          varchar(30),
          @SupplierCode         varchar(30),
          @DeliveryNoteNumber   varchar(30),
          @ProcessedDate        varchar(20),
          @FromWarehouseCode    varchar(10),
          @ForeignKey           varchar(30),
          @LineNumber           varchar(10),
          @ProductCode          varchar(30),
          @Batch                varchar(50),
          @Quantity             varchar(10),
          @NumberOfLines        varchar(10),
          @TotalQuantity        varchar(20),
          -- Internal
          @fetch_status_header  int,
          @fetch_status_detail  int,
          @print_string         varchar(255)
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare extract_header_cursor cursor for
  select top 1
         PrimaryKey,
         isnull(DeliveryNoteNumber, ''),
         isnull(replace(SupplierCode,'&','&amp;'), ''),
         convert(varchar(20), isnull(ProcessedDate, @Getdate), 120),
         convert(varchar(10), count(1)),
         convert(varchar(10), sum(convert(numeric(13,2), Quantity)))
    from InterfaceExportPOHeader h
    join InterfaceExportPODetail d on h.PrimaryKey = d.ForeignKey
   where RecordStatus = 'N'
     and RecordType = 'SR'
   group by PrimaryKey,
            isnull(DeliveryNoteNumber, ''),
            isnull(replace(SupplierCode,'&','&amp;'), ''),
            convert(varchar(20), isnull(ProcessedDate, @Getdate), 120),
            isnull(FromWarehouseCode, '')
   order by PrimaryKey
  
  open extract_header_cursor
  
  fetch extract_header_cursor into @OrderNumber,
                                   @DeliveryNoteNumber,
                                   @SupplierCode,
                                   @ProcessedDate,
                                   @NumberOfLines,
                                   @TotalQuantity
  
  select @fetch_status_header = @@fetch_status
  
  while (@fetch_status_header = 0)
  begin
    update InterfaceExportPOHeader
       set RecordStatus = 'Y',
           ProcessedDate = @GetDate
     where PrimaryKey  = @OrderNumber
    
    set @FileName = @OrderNumber
    
    insert @TableResult (Data) select '<?xml version="1.0" encoding="Windows-1252" standalone="yes" ?>'
    insert @TableResult (Data) select '<root>'
    insert @TableResult (Data) select '  <SupplierReturn>'
    insert @TableResult (Data) select '    <OrderNumber>' + @OrderNumber + '</OrderNumber>'
    insert @TableResult (Data) select '    <DeliveryNoteNumber>' + @DeliveryNoteNumber + '</DeliveryNoteNumber>'
    insert @TableResult (Data) select '    <SupplierCode>' + @SupplierCode + '</SupplierCode>'
    insert @TableResult (Data) select '    <CreateDate>' + @ProcessedDate + '</CreateDate>'
		  insert @TableResult (Data) select '    <NoOfLines>' + @NumberOfLines + '</NoOfLines>'
		  insert @TableResult (Data) select '    <TotalQty>' + @TotalQuantity + '</TotalQty>'
		  insert @TableResult (Data) select '  <ReturnDetail>'
    
    
    declare extract_detail_cursor cursor for
    select ForeignKey,
           convert(varchar(10), LineNumber),
           isnull(ProductCode, ''),
           isnull(Batch, ''),
           convert(varchar(10), Quantity),
           isnull(Additional1, '')
      from InterfaceExportPODetail
     where ForeignKey = @OrderNumber
     order by LineNumber
      
    open extract_detail_cursor
    
    fetch extract_detail_cursor into @ForeignKey,
                                     @LineNumber,
                                     @ProductCode,
                                     @Batch,
                                     @Quantity,
                                     @FromWarehouseCode
    select @fetch_status_detail = @@fetch_status
    
    while (@fetch_status_detail = 0)
    begin
      insert @TableResult (Data) select '    <ReturnLine>'
      insert @TableResult (Data) select '      <LineNumber>' + @LineNumber + '</LineNumber>'
      insert @TableResult (Data) select '      <ProductCode>' + @ProductCode + '</ProductCode>'
      insert @TableResult (Data) select '      <Location>' + @FromWarehouseCode + '</Location>'
      insert @TableResult (Data) select '      <Quantity>' + @Quantity + '</Quantity>'
--				  insert @TableResult (Data) select '      <ReasonCode>1</ReasonCode>'
      
      insert @TableResult (Data) select '    </ReturnLine>'
      
      fetch extract_detail_cursor into @ForeignKey,
                                       @LineNumber,
                                       @ProductCode,
                                       @Batch,
                                       @Quantity,
                                       @FromWarehouseCode
      
      select @fetch_status_detail = @@fetch_status
    end
    
    close extract_detail_cursor
    deallocate extract_detail_cursor
    
		  insert @TableResult (Data) select '    </ReturnDetail>'
    insert @TableResult (Data) select '  </SupplierReturn>'
    insert @TableResult (Data) select '</root>'
    
    fetch extract_header_cursor into @OrderNumber,
                                     @DeliveryNoteNumber,
                                     @SupplierCode,
                                     @ProcessedDate,
                                     @NumberOfLines,
                                     @TotalQuantity
    
    select @fetch_status_header = @@fetch_status
  end
  
  close extract_header_cursor
  deallocate extract_header_cursor
  
  select Data
    from @TableResult
end
