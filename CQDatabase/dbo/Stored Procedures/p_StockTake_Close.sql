﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTake_Close
  ///   Filename       : p_StockTake_Close.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2011
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StockTakeReference table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTake_Close
(
 @connectionStringName		nvarchar(255) = null,
 @WarehouseId int = null,
 --@StockTakeType nvarchar(50) = null,
 --@StartDate datetime = null,
 @EndDate datetime = null,
 @OperatorId int = null,
 @StockTakeReferenceId int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error int
  update StockTakeReference
     set EndDate = @EndDate
   where StockTakeReferenceId = @StockTakeReferenceId
   
   update StockTakeReferenceJob
     set StatusCode = 'Closed'
   where StockTakeReferenceId = @StockTakeReferenceId
   
  select @Error = @@Error
  
end
