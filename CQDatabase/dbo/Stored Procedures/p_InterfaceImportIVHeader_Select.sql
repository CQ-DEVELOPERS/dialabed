﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIVHeader_Select
  ///   Filename       : p_InterfaceImportIVHeader_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:39
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportIVHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIVHeaderId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportIVHeader.InterfaceImportIVHeaderId,
  ///   InterfaceImportIVHeader.PrimaryKey,
  ///   InterfaceImportIVHeader.OrderNumber,
  ///   InterfaceImportIVHeader.InvoiceNumber,
  ///   InterfaceImportIVHeader.RecordType,
  ///   InterfaceImportIVHeader.RecordStatus,
  ///   InterfaceImportIVHeader.CustomerCode,
  ///   InterfaceImportIVHeader.Customer,
  ///   InterfaceImportIVHeader.Address,
  ///   InterfaceImportIVHeader.FromWarehouseCode,
  ///   InterfaceImportIVHeader.ToWarehouseCode,
  ///   InterfaceImportIVHeader.Route,
  ///   InterfaceImportIVHeader.DeliveryDate,
  ///   InterfaceImportIVHeader.Remarks,
  ///   InterfaceImportIVHeader.NumberOfLines,
  ///   InterfaceImportIVHeader.VatPercentage,
  ///   InterfaceImportIVHeader.VatSummary,
  ///   InterfaceImportIVHeader.Total,
  ///   InterfaceImportIVHeader.Additional1,
  ///   InterfaceImportIVHeader.Additional2,
  ///   InterfaceImportIVHeader.Additional3,
  ///   InterfaceImportIVHeader.Additional4,
  ///   InterfaceImportIVHeader.Additional5,
  ///   InterfaceImportIVHeader.Additional6,
  ///   InterfaceImportIVHeader.Additional7,
  ///   InterfaceImportIVHeader.Additional8,
  ///   InterfaceImportIVHeader.Additional9,
  ///   InterfaceImportIVHeader.Additional10,
  ///   InterfaceImportIVHeader.ProcessedDate,
  ///   InterfaceImportIVHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIVHeader_Select
(
 @InterfaceImportIVHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceImportIVHeader.InterfaceImportIVHeaderId
        ,InterfaceImportIVHeader.PrimaryKey
        ,InterfaceImportIVHeader.OrderNumber
        ,InterfaceImportIVHeader.InvoiceNumber
        ,InterfaceImportIVHeader.RecordType
        ,InterfaceImportIVHeader.RecordStatus
        ,InterfaceImportIVHeader.CustomerCode
        ,InterfaceImportIVHeader.Customer
        ,InterfaceImportIVHeader.Address
        ,InterfaceImportIVHeader.FromWarehouseCode
        ,InterfaceImportIVHeader.ToWarehouseCode
        ,InterfaceImportIVHeader.Route
        ,InterfaceImportIVHeader.DeliveryDate
        ,InterfaceImportIVHeader.Remarks
        ,InterfaceImportIVHeader.NumberOfLines
        ,InterfaceImportIVHeader.VatPercentage
        ,InterfaceImportIVHeader.VatSummary
        ,InterfaceImportIVHeader.Total
        ,InterfaceImportIVHeader.Additional1
        ,InterfaceImportIVHeader.Additional2
        ,InterfaceImportIVHeader.Additional3
        ,InterfaceImportIVHeader.Additional4
        ,InterfaceImportIVHeader.Additional5
        ,InterfaceImportIVHeader.Additional6
        ,InterfaceImportIVHeader.Additional7
        ,InterfaceImportIVHeader.Additional8
        ,InterfaceImportIVHeader.Additional9
        ,InterfaceImportIVHeader.Additional10
        ,InterfaceImportIVHeader.ProcessedDate
        ,InterfaceImportIVHeader.InsertDate
    from InterfaceImportIVHeader
   where isnull(InterfaceImportIVHeader.InterfaceImportIVHeaderId,'0')  = isnull(@InterfaceImportIVHeaderId, isnull(InterfaceImportIVHeader.InterfaceImportIVHeaderId,'0'))
  
end
