﻿
/*
  /// <summary>
  ///   Procedure Name : p_IssueLineInstruction_Update
  ///   Filename       : p_IssueLineInstruction_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Mar 2008 17:07:03
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the IssueLineInstruction table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentTypeId int = null,
  ///   @OutboundShipmentId int = null,
  ///   @OutboundDocumentId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @InstructionId int = null,
  ///   @DropSequence int = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLineInstruction_Update
(
 @OutboundDocumentTypeId int = null,
 @OutboundShipmentId int = null,
 @OutboundDocumentId int = null,
 @IssueId int = null,
 @IssueLineId int = null,
 @InstructionId int = null,
 @DropSequence int = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null,
 @Weight float = null,
 @ConfirmedWeight float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
	 
  update IssueLineInstruction
     set OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, OutboundDocumentTypeId),
         OutboundShipmentId = isnull(@OutboundShipmentId, OutboundShipmentId),
         OutboundDocumentId = isnull(@OutboundDocumentId, OutboundDocumentId),
         IssueId = isnull(@IssueId, IssueId),
         IssueLineId = isnull(@IssueLineId, IssueLineId),
         InstructionId = isnull(@InstructionId, InstructionId),
         DropSequence = isnull(@DropSequence, DropSequence),
         Quantity = isnull(@Quantity, Quantity),
         ConfirmedQuantity = isnull(@ConfirmedQuantity, ConfirmedQuantity),
         Weight = isnull(@Weight, Weight),
         ConfirmedWeight = isnull(@ConfirmedWeight, ConfirmedWeight) 
   where IssueLineId   = @IssueLineId
     and InstructionId = @InstructionId
  
  select @Error = @@Error
  
  return @Error
  
end
