﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Line_Edit_InboundLine
  ///   Filename       : p_Production_Line_Edit_InboundLine.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Line_Edit_InboundLine
(
 @inboundLineId int,
 @quantity      float
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @ReceiptLineId      int,
          @StatusId           int,
          @StorageUnitBatchId int,
          @WarehouseId        int,
          @OldStatusId        int,
          @InboundDocumentId  int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('ID','W')
  
  begin transaction
  
  select @OldStatusId       = StatusId,
         @InboundDocumentId = InboundDocumentId
    from InboundLine
   where InboundLineId = @InboundLineId
  
  if @StatusId != @OldStatusId
  begin
    set @Error = -1
    set @Errormsg = 'Line not in the correct status'
    goto error
  end
  
  exec @Error = p_InboundLine_Update
   @InboundLineId = @inboundLineId,
   @Quantity      = @quantity
  
  if @Error <> 0
    goto error
  
  select @ReceiptLineId      = rl.ReceiptLineId,
         @StorageUnitBatchId = rl.StorageUnitBatchId,
         @WarehouseId        = r.WarehouseId
    from Receipt      r
    join ReceiptLine rl on r.ReceiptId = rl.ReceiptId
   where rl.InboundLineId = @inboundLineId
  
  exec @Error = p_ReceiptLine_Update
   @ReceiptLineId    = @ReceiptLineId,
   @InboundLineId    = @inboundLineId,
   @RequiredQuantity = @quantity
  
  if @Error <> 0
    goto error
  
  if (select InboundDocumentTypeCode
        from InboundDocument id
        join InboundDocumentType idt on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
       where InboundDocumentId = @InboundDocumentId) = 'PRV'
  begin
    if exists(select 1
                from Instruction i
                join Status      s on i.StatusId = s.StatusId
               where i.ReceiptLineId = @ReceiptLineId
                 and s.StatusCode != 'W')
    begin
      set @Error = -1
      goto error
    end

    delete Instruction where ReceiptLineId = @ReceiptLineId
    
    if @@Error <> 0
    begin
      set @Error = -1
      goto error
    end

    exec @Error = p_Production_Line_Insert
    @WarehouseId        = @WarehouseId,
    @OperatorId         = null,
    @StorageUnitBatchId = @StorageUnitBatchId,
    @quantity           = @quantity,
    @ReceiptLineId      = @ReceiptLineId
    select @Error
    if @Error <> 0
      goto error
  end
  select @Error
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Production_Line_Edit_InboundLine'
    rollback transaction
    return @Error
end
