﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COADetail_Select
  ///   Filename       : p_COADetail_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:23
  /// </summary>
  /// <remarks>
  ///   Selects rows from the COADetail table.
  /// </remarks>
  /// <param>
  ///   @COADetailId int = null 
  /// </param>
  /// <returns>
  ///   COADetail.COADetailId,
  ///   COADetail.COAId,
  ///   COADetail.NoteId,
  ///   COADetail.NoteCode,
  ///   COADetail.Note 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COADetail_Select
(
 @COADetailId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         COADetail.COADetailId
        ,COADetail.COAId
        ,COADetail.NoteId
        ,COADetail.NoteCode
        ,COADetail.Note
    from COADetail
   where isnull(COADetail.COADetailId,'0')  = isnull(@COADetailId, isnull(COADetail.COADetailId,'0'))
  order by NoteCode
  
end
