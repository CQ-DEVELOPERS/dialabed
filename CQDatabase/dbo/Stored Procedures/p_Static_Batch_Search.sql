﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Batch_Search
  ///   Filename       : dbo.p_Static_Batch_Search.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 3 September 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Batch_Search
(
 @StorageUnitId int,
 @Batch         nvarchar(50) = null,
 @ECLNumber     nvarchar(10) = null,
 @StartRowIndex int,
 @MaximumRows int
)
 
as
begin
	 set nocount on;
	 
--	 if @StorageUnitId = -1
--	   set @StorageUnitId = null
 Set @StartRowIndex = @StartRowIndex + 1 
  
Select StorageUnitBatchId,
        Batch,
         BatchId,
         ECLNumber,
         CreateDate from (
select sub.StorageUnitBatchId,
         b.Batch,
         b.BatchId,
         b.ECLNumber,
         b.CreateDate,
		ROW_NUMBER() Over(Order by sub.StorageUnitBatchId)  as rownum
    from StorageUnitBatch sub (nolock)
    join Batch            b   (nolock) on sub.BatchId = b.BatchId  
    where sub.StorageUnitId          = isnull(@StorageUnitId, sub.StorageUnitId)
     and isnull(b.Batch,'%')     like '%' + isnull(@Batch, isnull(b.Batch,'%')) + '%'
     and isnull(b.ECLNumber,'%') like '%' + isnull(@ECLNumber, isnull(b.ECLNumber,'%')) + '%') a
where rownum between @StartRowIndex and (@StartRowIndex + @MaximumRows) -1
end
