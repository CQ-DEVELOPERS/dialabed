﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackType_Search
  ///   Filename       : p_PackType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:40:54
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the PackType table.
  /// </remarks>
  /// <param>
  ///   @PackTypeId int = null output,
  ///   @PackType varchar(30) = null,
  ///   @PackTypeCode varchar(10) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   PackType.PackTypeId,
  ///   PackType.PackType,
  ///   PackType.InboundSequence,
  ///   PackType.OutboundSequence,
  ///   PackType.PackTypeCode,
  ///   PackType.Unit,
  ///   PackType.Pallet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackType_Search
(
 @PackTypeId int = null output,
 @PackType varchar(30) = null,
 @PackTypeCode varchar(10) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @PackTypeId = '-1'
    set @PackTypeId = null;
  
  if @PackType = '-1'
    set @PackType = null;
  
  if @PackTypeCode = '-1'
    set @PackTypeCode = null;
  
 
  select
         PackType.PackTypeId
        ,PackType.PackType
        ,PackType.InboundSequence
        ,PackType.OutboundSequence
        ,PackType.PackTypeCode
        ,PackType.Unit
        ,PackType.Pallet
    from PackType
   where isnull(PackType.PackTypeId,'0')  = isnull(@PackTypeId, isnull(PackType.PackTypeId,'0'))
     and isnull(PackType.PackType,'%')  like '%' + isnull(@PackType, isnull(PackType.PackType,'%')) + '%'
     and isnull(PackType.PackTypeCode,'%')  like '%' + isnull(@PackTypeCode, isnull(PackType.PackTypeCode,'%')) + '%'
  order by PackType
  
end
