﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingLog_Delete
  ///   Filename       : p_CheckingLog_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:16
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the CheckingLog table.
  /// </remarks>
  /// <param>
  ///   @CheckingLogId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingLog_Delete
(
 @CheckingLogId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete CheckingLog
     where CheckingLogId = @CheckingLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
