﻿
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstruction_Delete
  ///   Filename       : BOM.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 01 Sep 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   [@StorageUnitId] [int] NOT NULL,

  /// </param>
*/
create procedure p_BOMInstruction_Delete
    @BOMInstructionId int
as
begin
  SET NOCOUNT ON;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  delete BOMPackaging
   where BOMInstructionId = @BOMInstructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update i
     set StatusId = dbo.ufn_StatusId('IS','W')
    from BOMInstruction bi (nolock)
    join Issue           i (nolock) on bi.IssueId = i.IssueId
   where bi.BOMInstructionId = @BOMInstructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update i
     set StatusId = dbo.ufn_StatusId('IS','W')
    from BOMInstruction         bi (nolock)
    join OutboundShipmentIssue osi (nolock) on bi.OutboundShipmentId = osi.OutboundShipmentId
    join Issue                   i (nolock) on osi.IssueId = i.IssueId
   where bi.BOMInstructionId = @BOMInstructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update il
     set ConfirmedQuatity = ConfirmedQuatity - bi.Quantity
    from BOMInstruction         bi (nolock)
    join IssueLine              il (nolock) on bi.IssueId = il.IssueId
    left
    join OutboundShipmentIssue osi (nolock) on osi.IssueId = il.IssueId
   where bi.BOMInstructionId = @BOMInstructionId
     and osi.OutboundShipmentId is null
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update il
     set ConfirmedQuatity = ConfirmedQuatity - bi.Quantity
    from BOMInstruction         bi (nolock)
    join IssueLine              il (nolock) on bi.IssueId = il.IssueId
    join OutboundShipmentIssue osi (nolock) on osi.IssueId = il.IssueId
   where bi.BOMInstructionId = @BOMInstructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  DELETE FROM BOMInstructionLine
  WHERE BOMInstructionId = @BOMInstructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error

  DELETE FROM BOMInstruction
  WHERE BOMInstructionId = @BOMInstructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return 0
  
  error:
    raiserror 900000 'Error executing p_BOMInstruction_Delete'
    rollback transaction
    return @Error
end

