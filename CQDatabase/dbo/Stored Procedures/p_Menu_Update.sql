﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Menu_Update
  ///   Filename       : p_Menu_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:56
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Menu table.
  /// </remarks>
  /// <param>
  ///   @MenuId int = null,
  ///   @Menu nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Menu_Update
(
 @MenuId int = null,
 @Menu nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @Menu = '-1'
    set @Menu = null;
  
	 declare @Error int
 
  update Menu
     set Menu = isnull(@Menu, Menu) 
   where MenuId = @MenuId
  
  select @Error = @@Error
  
  
  return @Error
  
end
