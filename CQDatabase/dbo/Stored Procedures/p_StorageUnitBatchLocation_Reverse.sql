﻿/*
      <summary>
        Procedure Name : p_StorageUnitBatchLocation_Reverse
        Filename       : p_StorageUnitBatchLocation_Reverse.sql
        Create By      : Grant Schultz
        Date Created   : 23 May 2007 14:10:21
      </summary>
      <remarks>
        Deallocates Quantity on StorageUnitBatchLocation
      </remarks>
      <param>
        @InstructionId int
      </param>
      <returns>
        @error
      </returns>
      <newpara>
        Modified by    : 
        Modified Date  : 
        Details        : 
      </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Reverse
(
 @InstructionId int
,@Pick          bit = 1 -- (0 = false, 1 = true)
,@Store         bit = 1 -- (0 = false, 1 = true)
,@Confirmed     bit = 1 -- (0 = false, 1 = true)
)
 
as
begin
	 set nocount on
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @StorageUnitBatchId int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @Quantity           numeric(13,6),
          @ActualQuantity     numeric(13,6),
          @AllocatedQuantity  numeric(13,6),
          @ReservedQuantity   numeric(13,6)
   
   if @Confirmed = 0
     select @PickLocationId      = PickLocationId,
            @StoreLocationId     = StoreLocationId,
            @StorageUnitBatchId  = StorageUnitBatchId,
            @Quantity            = Quantity
       from Instruction
      where InstructionId = @InstructionId
   else
     select @PickLocationId      = PickLocationId,
            @StoreLocationId     = StoreLocationId,
            @StorageUnitBatchId  = StorageUnitBatchId,
            @Quantity            = ConfirmedQuantity
       from Instruction
      where InstructionId = @InstructionId
  
  if @PickLocationId is not null and @Pick = 1
  begin
    if (select Picked
          from Instruction
         where InstructionId = @InstructionId) = 1
    begin
      select @ActualQuantity   = ActualQuantity,
             @ReservedQuantity = ReservedQuantity
        from StorageUnitBatchLocation
       where StorageUnitBatchId = @StorageUnitBatchId
         and LocationId = @PickLocationId
      
      if @ActualQuantity is null -- No row found
      begin
        -- Insert a row into StorageUnitBatchLocation.ActualQuantity
        exec @error = p_StorageUnitBatchLocation_Insert
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @PickLocationId,
         @ActualQuantity     = @Quantity,
         @AllocatedQuantity  = 0,
         @ReservedQuantity   = @Quantity
        
        if @error <> 0
          goto error
      end
      else
      begin
        set @ActualQuantity = @ActualQuantity + @Quantity
        set @ReservedQuantity = @ReservedQuantity + @Quantity
        
        if @ActualQuantity < 0
          set @ActualQuantity = 0
        
        if @ReservedQuantity < 0
          set @ReservedQuantity = 0
        
        -- Update the StorageUnitBatchLocation.ReservedQuantity Column
        exec @error = p_StorageUnitBatchLocation_Update
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @PickLocationId,
         @ActualQuantity     = @ActualQuantity,
         @AllocatedQuantity  = null, -- Don't update
         @ReservedQuantity   = @ReservedQuantity
        
        if @error <> 0
          goto error
      end
      
      update Instruction
         set Picked        = 0
       where InstructionId = @InstructionId
      
      select @Error = @@Error
      
      if @error <> 0
        goto error
     end
  end
  
  if @StoreLocationId is not null and @Store = 1
  begin
    if (select Stored
          from Instruction
         where InstructionId = @InstructionId) = 1
    begin
      select @ActualQuantity = ActualQuantity,
             @AllocatedQuantity = AllocatedQuantity
        from StorageUnitBatchLocation
       where StorageUnitBatchId = @StorageUnitBatchId
         and LocationId = @StoreLocationId
      
      set @ActualQuantity = @ActualQuantity - @Quantity
      set @AllocatedQuantity = @AllocatedQuantity + @Quantity
      
      if @ActualQuantity < 0
        set @ActualQuantity = 0
      
      if @AllocatedQuantity < 0
        set @AllocatedQuantity = 0
      
      -- Update the StorageUnitBatchLocation.ActualQuantity Column
      exec @error = p_StorageUnitBatchLocation_Update
       @StorageUnitBatchId = @StorageUnitBatchId,
       @LocationId         = @StoreLocationId,
       @ActualQuantity     = @ActualQuantity,
       @AllocatedQuantity  = @AllocatedQuantity,
       @ReservedQuantity   = null  -- Don't update
      
      if @error <> 0
        goto error
      
      if exists(select top 1 1
                  from StorageUnitBatchLocation
                 where StorageUnitBatchId = @StorageUnitBatchId
                   and LocationId         = @StoreLocationId
                   and ActualQuantity     = 0
                   and AllocatedQuantity  = 0
                   and ReservedQuantity   = 0)
      begin
        -- Delete StorageUnitBatchLocation if all values 0
        exec @error = p_StorageUnitBatchLocation_Delete
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @StoreLocationId
        
        if @error <> 0
          goto error
      end
      
      update Instruction
         set Stored        = 0
       where InstructionId = @InstructionId
      
      select @Error = @@Error
      
      if @error <> 0
        goto error
     end
  end
  
  return
  
  error:
    return @error
end
