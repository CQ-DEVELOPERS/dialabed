﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Select
  ///   Filename       : p_StorageUnitBatch_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitBatch table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null 
  /// </param>
  /// <returns>
  ///   StorageUnitBatch.StorageUnitBatchId,
  ///   StorageUnitBatch.BatchId,
  ///   StorageUnitBatch.StorageUnitId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Select
(
 @StorageUnitBatchId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         StorageUnitBatch.StorageUnitBatchId
        ,StorageUnitBatch.BatchId
        ,StorageUnitBatch.StorageUnitId
    from StorageUnitBatch
   where isnull(StorageUnitBatch.StorageUnitBatchId,'0')  = isnull(@StorageUnitBatchId, isnull(StorageUnitBatch.StorageUnitBatchId,'0'))
  
end
