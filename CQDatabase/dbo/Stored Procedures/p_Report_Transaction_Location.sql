﻿--IF OBJECT_ID('dbo.p_Report_Transaction_Location') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Report_Transaction_Location
--    IF OBJECT_ID('dbo.p_Report_Transaction_Location') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Report_Transaction_Location >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Report_Transaction_Location >>>'
--END
--go
 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Transaction_Location
  ///   Filename       : p_Report_Transaction_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Transaction_Location
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int,
 @LocationId       int = null,
 @FromDate         datetime,
 @ToDate           datetime,
 @PrincipalId       int 
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  InstructionTypeId   int,
	  InstructionType     nvarchar(30),
	  InstructionTypeCode nvarchar(10),
	  StatusId            int,
	  Status              nvarchar(50),
	  CreateDate          datetime,
	  StartDate           datetime,
	  EndDate             datetime,
	  OperatorId          int,
	  Operator            nvarchar(50),
	  StorageUnitBatchId  int,
	  ProductCode         nvarchar(30),
	  Product             nvarchar(255),
	  SKUCode             nvarchar(50),
	  SKU		          nvarchar(50),
	  Batch               nvarchar(50),
	  ExpiryDate		  datetime,
	  Location            nvarchar(15),
	  PickLocationId      int null,
	  PickLocation        nvarchar(15),
	  StoreLocationId     int null,
	  StoreLocation       nvarchar(15),
	  Quantity            float,
	  ConfirmedQuantity   float,
	  CheckQuantity       float,
	  DRCR				  int,
	  PalletId			  int,
	  OrderNumber		  nvarchar(30),
	  Principal			  nvarchar(50)
	 )
	 
	 if @LocationId = -1
	   set @LocationId = null
  
  insert	@TableResult
			(InstructionTypeId,
	        StatusId,
	        CreateDate,
	        StartDate,
	        EndDate,
	        OperatorId,
	        StorageUnitBatchId,
            PickLocationId,
            StoreLocationId,
	        Quantity,
	        ConfirmedQuantity,
	        CheckQuantity,
	        PalletId,
	        OrderNumber)
  select    i.InstructionTypeId,
            i.StatusId,
	        i.CreateDate,
	        i.StartDate,
	        i.EndDate,
	        isnull(i.OperatorId, j.OperatorId),
	        i.StorageUnitBatchId,
            i.PickLocationId,
            i.StoreLocationId,
	        i.Quantity,
	        i.ConfirmedQuantity,
	        i.CheckQuantity,
	        i.PalletId,
	        isnull(od.OrderNumber, id.OrderNumber)
    from Instruction			     i (nolock)
    join Job						 j (nolock) on i.JobId = j.JobId
    left join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
    left join OutboundDocument      od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
    left join ReceiptLine           rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
    left join Inboundline           il (nolock) on il.InboundLineId = rl.InboundLineId
    left join InboundDocument	    id (nolock) on id.InboundDocumentId = il.InboundDocumentId
   where i.WarehouseId      = @WarehouseId
     and (i.CreateDate between @FromDate and @ToDate
     or   i.EndDate    between @FromDate and @ToDate)
     and(isnull(i.StoreLocationId,-1)  = isnull(@LocationId, isnull(i.StoreLocationId,-1))
     or  isnull(i.PickLocationId,-1)   = isnull(@LocationId, isnull(i.PickLocationId,-1)))
  
  if @LocationId is null
    insert @TableResult
			(InstructionTypeId
			,StatusId
			,Status
			,CreateDate
			,StartDate
			,EndDate
			,Operator
			,StorageUnitBatchId
			,ProductCode
			,Product
			,SKUCode
			,Batch
			,ExpiryDate
			,Location
			,PickLocationId
			,StoreLocationId
			,Quantity
			,ConfirmedQuantity
			,CheckQuantity
			,PalletId
			,OrderNumber)
    select InstructionTypeId
			,StatusId
			,Status
			,CreateDate
			,StartDate
			,EndDate
			,Operator
			,StorageUnitBatchId
			,ProductCode
			,Product
			,SKUCode
			,Batch
			,ExpiryDate
			,StoreLocation
			,PickLocationId
			,StoreLocationId
			,Quantity
			,ConfirmedQuantity
			,CheckQuantity
			,PalletId
			,OrderNumber
      from @TableResult
  
  --delete @TableResult where Location is null
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ExpiryDate    = b.ExpiryDate,
         SKU		   = sku.SKU,
		 Principal	   = pr.Principal
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit          su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product               p (nolock) on su.ProductId          = p.ProductId
	left
	join Principal			  pr (nolock) on p.PrincipalId = pr.PrincipalId
    join SKU                 sku (nolock) on su.SKUId              = sku.SKUId
    join Batch                 b (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set InstructionType     = it.InstructionType,
         InstructionTypeCode = it.InstructionTypeCode
    from @TableResult    tr
    join InstructionType it (nolock) on tr.InstructionTypeId = it.InstructionTypeId
  
  update @TableResult
     set InstructionType = InstructionType + '*User'
    where CheckQuantity = 1
      and InstructionTypeCode = 'R'
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set Location      = l.Location,
         StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
  
  if @LocationId is not null
--    update tr
--       set Location      = l.Location,
--           StoreLocation = l.Location
--      from @TableResult tr
--      join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
--  else
	update @TableResult
		set Location      = l.Location
		from Location l (nolock)
		where l.LocationId = @LocationId
     
	update tr
		set DRCR = ConfirmedQuantity * -1
		from @TableResult tr
		where InstructionTypeCode in ('M', 'P', 'O', 'PM', 'PS', 'FM')
   
	update tr
		set DRCR = ConfirmedQuantity 
		from @TableResult tr
		where InstructionTypeCode in ('R', 'S', 'SM', 'PR', 'HS', 'RS')
   
	update tr
		set DRCR = isnull(ConfirmedQuantity,0) - isnull(Quantity,0)
		from @TableResult tr
		where InstructionTypeCode in ('STE', 'STL', 'STA', 'STP')

	update tr
		set DRCR = ConfirmedQuantity 
		from @TableResult tr
		where InstructionTypeCode in ('R', 'M')
		and   tr.Location != tr.PickLocation
   
	update tr
		set DRCR = 0
		from @TableResult tr
		where tr.Status in ('Waiting', 'Deleted')
  
  select distinct InstructionType
		,Status
		,CreateDate
		,StartDate
		,EndDate
		,Operator
		,ProductCode
		,Product
		,SKUCode
		,SKU
		,Batch
		,Principal as PrincipalCode
		,ExpiryDate
		,isnull(Location,PickLocation) as 'Location'
		,PickLocation
		,StoreLocation
		,Quantity
		,ConfirmedQuantity
		,DRCR
		,PalletId
		,OrderNumber
		,isnull(EndDate, isnull(StartDate, CreateDate)) as OrderDate
    from @TableResult
  order by Location, 
			isnull(EndDate, isnull(StartDate, CreateDate))
end
 
 
 
 
--go
--IF OBJECT_ID('dbo.p_Report_Transaction_Location') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Report_Transaction_Location >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Report_Transaction_Location >>>'
--go


