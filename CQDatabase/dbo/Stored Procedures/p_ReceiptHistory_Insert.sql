﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptHistory_Insert
  ///   Filename       : p_ReceiptHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 13:28:35
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ReceiptHistory table.
  /// </remarks>
  /// <param>
  ///   @ReceiptId int = null,
  ///   @InboundDocumentId int = null,
  ///   @PriorityId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @CreditAdviceIndicator bit = null,
  ///   @Delivery int = null,
  ///   @AllowPalletise bit = null,
  ///   @Interfaced bit = null,
  ///   @StagingLocationId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @NumberOfLines int = null,
  ///   @ReceivingCompleteDate datetime = null,
  ///   @ParentReceiptId int = null,
  ///   @GRN nvarchar(60) = null,
  ///   @PlannedDeliveryDate datetime = null,
  ///   @ShippingAgentId int = null,
  ///   @ContainerNumber nvarchar(100) = null,
  ///   @AdditionalText1 nvarchar(510) = null,
  ///   @AdditionalText2 nvarchar(510) = null,
  ///   @BOE nvarchar(510) = null,
  ///   @Incoterms nvarchar(100) = null,
  ///   @ReceiptConfirmed datetime = null,
  ///   @ReceivingStarted datetime = null,
  ///   @VehicleId int = null,
  ///   @ParentIssueId int = null,
  ///   @VehicleTypeId int = null 
  /// </param>
  /// <returns>
  ///   ReceiptHistory.ReceiptId,
  ///   ReceiptHistory.InboundDocumentId,
  ///   ReceiptHistory.PriorityId,
  ///   ReceiptHistory.WarehouseId,
  ///   ReceiptHistory.LocationId,
  ///   ReceiptHistory.StatusId,
  ///   ReceiptHistory.OperatorId,
  ///   ReceiptHistory.DeliveryNoteNumber,
  ///   ReceiptHistory.DeliveryDate,
  ///   ReceiptHistory.SealNumber,
  ///   ReceiptHistory.VehicleRegistration,
  ///   ReceiptHistory.Remarks,
  ///   ReceiptHistory.CreditAdviceIndicator,
  ///   ReceiptHistory.Delivery,
  ///   ReceiptHistory.AllowPalletise,
  ///   ReceiptHistory.Interfaced,
  ///   ReceiptHistory.StagingLocationId,
  ///   ReceiptHistory.CommandType,
  ///   ReceiptHistory.InsertDate,
  ///   ReceiptHistory.NumberOfLines,
  ///   ReceiptHistory.ReceivingCompleteDate,
  ///   ReceiptHistory.ParentReceiptId,
  ///   ReceiptHistory.GRN,
  ///   ReceiptHistory.PlannedDeliveryDate,
  ///   ReceiptHistory.ShippingAgentId,
  ///   ReceiptHistory.ContainerNumber,
  ///   ReceiptHistory.AdditionalText1,
  ///   ReceiptHistory.AdditionalText2,
  ///   ReceiptHistory.BOE,
  ///   ReceiptHistory.Incoterms,
  ///   ReceiptHistory.ReceiptConfirmed,
  ///   ReceiptHistory.ReceivingStarted,
  ///   ReceiptHistory.VehicleId,
  ///   ReceiptHistory.ParentIssueId,
  ///   ReceiptHistory.VehicleTypeId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptHistory_Insert
(
 @ReceiptId int = null,
 @InboundDocumentId int = null,
 @PriorityId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Remarks nvarchar(500) = null,
 @CreditAdviceIndicator bit = null,
 @Delivery int = null,
 @AllowPalletise bit = null,
 @Interfaced bit = null,
 @StagingLocationId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @NumberOfLines int = null,
 @ReceivingCompleteDate datetime = null,
 @ParentReceiptId int = null,
 @GRN nvarchar(60) = null,
 @PlannedDeliveryDate datetime = null,
 @ShippingAgentId int = null,
 @ContainerNumber nvarchar(100) = null,
 @AdditionalText1 nvarchar(510) = null,
 @AdditionalText2 nvarchar(510) = null,
 @BOE nvarchar(510) = null,
 @Incoterms nvarchar(100) = null,
 @ReceiptConfirmed datetime = null,
 @ReceivingStarted datetime = null,
 @VehicleId int = null,
 @ParentIssueId int = null,
 @VehicleTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ReceiptHistory
        (ReceiptId,
         InboundDocumentId,
         PriorityId,
         WarehouseId,
         LocationId,
         StatusId,
         OperatorId,
         DeliveryNoteNumber,
         DeliveryDate,
         SealNumber,
         VehicleRegistration,
         Remarks,
         CreditAdviceIndicator,
         Delivery,
         AllowPalletise,
         Interfaced,
         StagingLocationId,
         CommandType,
         InsertDate,
         NumberOfLines,
         ReceivingCompleteDate,
         ParentReceiptId,
         GRN,
         PlannedDeliveryDate,
         ShippingAgentId,
         ContainerNumber,
         AdditionalText1,
         AdditionalText2,
         BOE,
         Incoterms,
         ReceiptConfirmed,
         ReceivingStarted,
         VehicleId,
         ParentIssueId,
         VehicleTypeId)
  select @ReceiptId,
         @InboundDocumentId,
         @PriorityId,
         @WarehouseId,
         @LocationId,
         @StatusId,
         @OperatorId,
         @DeliveryNoteNumber,
         @DeliveryDate,
         @SealNumber,
         @VehicleRegistration,
         @Remarks,
         @CreditAdviceIndicator,
         @Delivery,
         @AllowPalletise,
         @Interfaced,
         @StagingLocationId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @NumberOfLines,
         @ReceivingCompleteDate,
         @ParentReceiptId,
         @GRN,
         @PlannedDeliveryDate,
         @ShippingAgentId,
         @ContainerNumber,
         @AdditionalText1,
         @AdditionalText2,
         @BOE,
         @Incoterms,
         @ReceiptConfirmed,
         @ReceivingStarted,
         @VehicleId,
         @ParentIssueId,
         @VehicleTypeId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
