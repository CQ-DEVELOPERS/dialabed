﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_Flo_PD
  ///   Filename       : p_FamousBrands_Export_Flo_PD.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Export_Flo_PD
(
 @FileName varchar(30) = null output
)
 
as
begin
  set nocount on;
  --select @FileName = 'ProductDetail.txt'
  select @FileName = ''
  
  select distinct convert(varchar(10), ID) + ',' +
         Code + ',"' + 
         isnull(Units,'') + ',"' + 
         isnull(convert(varchar(10), Quantity),'1') + '","' + 
         convert(varchar(10), isnull(Unitsize,0)) + '","' + 
         convert(varchar(16), isnull(Weight,0)) + '","' + 
         convert(varchar(16), isnull(Length,0)) +  '","' + 
         convert(varchar(16), isnull(Width,0)) +  '","' + 
         convert(varchar(16), isnull(Height,0)) + '"'
    from FloProductDetailExport
   where RecordStatus = 'N'
  
  update FloProductDetailExport
     set RecordStatus = 'Y'
   where RecordStatus = 'N'
end
