﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Manufacturing_Order_Search
  ///   Filename       : p_Receiving_Manufacturing_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Manufacturing_Order_Search
(
 @WarehouseId           int,
 @InboundDocumentTypeId	int,
 @InboundShipmentId     int,
 @ExternalCompanyCode	  nvarchar(30),
 @ExternalCompany	      nvarchar(255),
 @OrderNumber	          nvarchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        InboundDocumentId         int,
        ReceiptId                 int,
        OrderNumber               nvarchar(30),
        InboundShipmentId         int,
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        NumberOfLines             int,
        DeliveryDate              datetime,
        PlannedDeliveryDate       datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        InboundDocumentType       nvarchar(30),
        LocationId                int,
        Location                  nvarchar(15),
        Rating                    int,
        Delivery                  int,
        DeliveryNoteNumber		  nvarchar(30),
        SealNumber				  nvarchar(30),
        PriorityID				  int,
        Priority				  nvarchar(50),
        VehicleRegistration		  nvarchar(10),
        Remarks					  nvarchar(250),
        AllowPalletise bit
       );
  
  if @InboundDocumentTypeId = -1
    set @InboundDocumentTypeId = null
  
  insert @TableResult
        (InboundDocumentId,
         ReceiptId,
         LocationId,
         OrderNumber,
         SupplierCode,
         Supplier,
         StatusId,
         Status,
         DeliveryDate,
         PlannedDeliveryDate,
         CreateDate,
         InboundDocumentType,
         Rating,
         Delivery,
         DeliveryNoteNumber,	
         SealNumber,			
         PriorityID,						
         VehicleRegistration,	
         Remarks,
         AllowPalletise)
  select id.InboundDocumentId,
         r.ReceiptId,
         r.LocationId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         r.StatusId,
         s2.Status,
         r.DeliveryDate,
         id.DeliveryDate,
         id.CreateDate,
         idt.InboundDocumentType,
         ec.Rating,
         r.Delivery,
         r.DeliveryNoteNumber,	
         r.SealNumber,			
         r.PriorityID,						
         r.VehicleRegistration,	
         r.Remarks,
         r.AllowPalletise
         FROM      BOMInstruction    bi (nolock)
	join      BOMHeader         bh (nolock) on bi.BOMHeaderId = bh.BOMHeaderId
	join      StorageUnit       su (nolock) on bh.StorageUnitId = su.StorageUnitId 
	join      StorageUnitBatch sub (nolock) on sub.StorageUnitId = su.StorageUnitId
	join      Product            p (nolock) on su.ProductId = p.ProductId
	Left join InboundDocument   id (nolock) on bi.InboundDocumentId = id.InboundDocumentId
	join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
	join Status              s   (nolock) on r.StatusId               = s.StatusId
	join receiptline  rl (nolock) on r.receiptid = rl.receiptid
	join Status              s2   (nolock) on rl.StatusId               = s2.StatusId
	join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
	join ExternalCompany     ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
	left join Batch              b (nolock) on b.batchid = id.BatchId
    --from InboundDocument     id  (nolock)
    --join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
    --join Status              s   (nolock) on r.StatusId               = s.StatusId
    --join receiptline  rl (nolock) on r.receiptid = rl.receiptid
    --join Status              s2   (nolock) on rl.StatusId               = s2.StatusId
    --join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    --left outer
    --join ExternalCompany     ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
   where id.InboundDocumentTypeId    = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
     and s.Type                      = 'R'
     and (s2.StatusCode in ('R','P','RC','C') or s.StatusCode in ('R','P','RC','C'))
     and idt.InboundDocumentTypeCode != 'PRV'
     and id.WarehouseId              = @WarehouseId
  
  update r
     set InboundShipmentId = isr.InboundShipmentId
    from @TableResult             r
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from ReceiptLine rl
                           where t.ReceiptId = rl.ReceiptId)
    from @TableResult t
  
  update r
     set Location = l.Location
    from @TableResult r
    join Location     l (nolock) on r.LocationId = l.LocationId
	
  update r
     set Priority = P.Priority
    from @TableResult r
    join Priority    P (nolock) on r.PriorityId = P.PriorityID  



  select InboundShipmentId,
         ReceiptId,
         OrderNumber,
         SupplierCode,
         Supplier,
         NumberOfLines,
         DeliveryDate,
         PlannedDeliveryDate,
         Status,
         InboundDocumentType,
         isnull(LocationId,-1) as LocationId,	
         Location,
         Rating,
         Delivery,
         DeliveryNoteNumber,	
         SealNumber,			
         PriorityID,
         Priority,						
         VehicleRegistration,	
         Remarks,
         AllowPalletise
    from @TableResult
end
