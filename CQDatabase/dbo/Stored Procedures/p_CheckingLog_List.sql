﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingLog_List
  ///   Filename       : p_CheckingLog_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:16
  /// </summary>
  /// <remarks>
  ///   Selects rows from the CheckingLog table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   CheckingLog.CheckingLogId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingLog_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as CheckingLogId
        ,null as 'CheckingLog'
  union
  select
         CheckingLog.CheckingLogId
        ,CheckingLog.CheckingLogId as 'CheckingLog'
    from CheckingLog
  
end
