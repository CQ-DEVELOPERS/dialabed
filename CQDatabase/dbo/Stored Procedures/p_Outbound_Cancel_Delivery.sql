﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Cancel_Delivery
  ///   Filename       : p_Outbound_Cancel_Delivery.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jan 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Cancel_Delivery
(
 @OutboundShipmentId int = null
,@IssueId int = null
)
 
as
begin
  set nocount on;
  
  declare @TableHeader as table
  (
   OutboundShipmentId int
  ,IssueId int
  ,StatusCode nvarchar(10)
  )
  
  declare @TableDetail as table
  (
   OutboundShipmentId int
  ,IssueId int
  ,JobId int
  ,JobCode nvarchar(10)
  ,InstructionId int
  ,InstructionCode nvarchar(10)
  )
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
         ,@JobId int
         ,@JobCode nvarchar(10)
         ,@InstructionId int
         ,@InstructionCode nvarchar(10)
  
  set @trancount = @@trancount;
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_Outbound_Cancel_Delivery;
    
		  -- If cancellation takes place while in in-progress, no-stock it's open jobs and create receipt
		  -- If cancellation takes place after is picked, create receipt
		  -- If cancellation takes place while in checking, create receipt
    insert @TableHeader
          (OutboundShipmentId
          ,IssueId)
    select osi.OutboundShipmentId
          ,i.IssueId
      from Issue i (nolock)
      left
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
                                             and osi.OutboundShipmentId = @OutboundShipmentId
     where (i.IssueId = @IssueId or (@IssueId is null and @OutboundShipmentId is not null))
	   
	   -- Get all the instructions and their statuses
	   insert @TableDetail
	         (JobId
	         ,JobCode
	         ,InstructionId
	         ,InstructionCode)
	   select distinct i.JobId
	         ,sj.StatusCode
	         ,i.InstructionId
	         ,si.StatusCode
	     from @TableHeader th
	     join IssueLineInstruction ili (nolock) on isnull(th.OutboundShipmentId, -1) = isnull(ili.OutboundShipmentId, -1)
	                                           and th.IssueId = ili.IssueId
	     join Instruction i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
	     join Status si (nolock) on i.StatusId = si.StatusId
	     join Job j (nolock) on i.JobId = j.JobId
	     join Status sj (nolock) on j.StatusId = sj.StatusId
	   
	   while exists(select top 1 1 from @TableDetail)
	   begin
	     select @JobId = JobId
	           ,@JobCode = JobCode
	           ,@InstructionId = InstructionId
	           ,@InstructionCode = InstructionCode
	       from @TableDetail
	     
	     delete @TableDetail where InstructionId = @InstructionId
	     
	     if @InstructionCode = 'W' -- Set no-stock - if instruction is already started let the picker finish picking the current line.
	     begin
	       exec @ErrorId = p_Instruction_No_Stock
	        @InstructionId = @InstructionId
	       
	       if @ErrorId != 0
	         raiserror 900000 'Error executing p_Outbound_Cancel_Delivery, p_Instruction_No_Stock'
	       
	       exec @ErrorId = p_Status_Rollup
	        @Jobid = @JobId,
	        @InstructionId = @InstructionId
	       
	       if @ErrorId != 0
	         raiserror 900000 'Error executing p_Outbound_Cancel_Delivery, p_Status_Rollup'
	     end
	   end
	   
	   while exists(select top 1 1 from @TableHeader)
	   begin
	     select @OutboundShipmentId = OutboundShipmentId
	           ,@IssueId = IssueId
	       from @TableHeader
	    
	     delete @TableHeader where IssueId = @IssueId
	    
	     exec @ErrorId = p_InboundDocument_Create_From_Issue
	      @OutboundShipmentId = @OutboundShipmentId
	     ,@IssueId = @IssueId
	     ,@InboundDocumentTypeCode = 'RET'
	     
	     insert into InterfaceExportHeader
            (
            PrimaryKey,
            OrderNumber,
            RecordType,
            RecordStatus,
            CompanyCode,
            Company,
            FromWarehouseCode,
            DeliveryDate,
            Additional6
            )
	     select OrderNumber
	     ,OrderNumber
	     ,odt.OutboundDocumentTypeCode
	     ,'N'
	     ,ec.ExternalCompanyCode
	     ,ec.ExternalCompany
	     ,w.WarehouseCode
	     ,od.DeliveryDate
	     ,'Canceled'
	     from OutboundDocument od
	     join Issue i (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
	     join ExternalCompany ec (nolock) on ec.ExternalCompanyId = od.ExternalCompanyId
	     join Warehouse w (nolock ) on w.WarehouseId = od.WarehouseId
	     join OutboundDocumentType odt (nolock) on odt.OutboundDocumentTypeId = od.OutboundDocumentTypeId
	     where i.IssueId = @IssueId
	   
	   update Issue set Interfaced = 1
	   where IssueId = @IssueId
	  
	     
      if @ErrorId != 0
        raiserror 900000 'Error executing p_Outbound_Cancel_Delivery, p_Instruction_No_Stock'
	   end
	    
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Wizard_Receipt_Insert;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
 
