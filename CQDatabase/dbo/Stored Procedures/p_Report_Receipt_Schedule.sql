﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Receipt_Schedule
  ///   Filename       : p_Report_Receipt_Schedule.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Receipt_Schedule
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @LocationId int,
 @FromDate   datetime,
 @ToDate     datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as Table
  (
   DeliveryDate      datetime,
   LocationId        int,
   Location          nvarchar(15),
   ExternalCompanyId int,
   ExternalCompany   nvarchar(50)
  )
  
  if @LocationId = -1
    set @LocationId = null
  
  insert @TableResult
        (DeliveryDate,
         LocationId,
         ExternalCompanyId)
  select isnull(r.DeliveryDate, id.DeliveryDate),
         r.LocationId,
         id.ExternalCompanyId
    from InboundDocument id
    join Receipt          r on id.InboundDocumentId = r.InboundDocumentId
   where isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
     and r.LocationId = isnull(@LocationId, r.LocationId)
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location      l on tr.LocationId = l.LocationId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  select convert(nvarchar(10), DeliveryDate, 110) as 'ConfirmedDate',
         convert(nvarchar(8), DeliveryDate, 108) as 'ConfirmedTime',
         Location,
         ExternalCompany
    from @TableResult
  order by Location, DeliveryDate
end
