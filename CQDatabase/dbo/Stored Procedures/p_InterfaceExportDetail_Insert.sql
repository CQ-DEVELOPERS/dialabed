﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportDetail_Insert
  ///   Filename       : p_InterfaceExportDetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExportDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(100) = null,
  ///   @SKUCode nvarchar(100) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @OrderQuantity float = null,
  ///   @RejectQuantity float = null,
  ///   @ReceivedQuantity float = null,
  ///   @AcceptedQuantity float = null,
  ///   @DeliveryNoteQuantity float = null,
  ///   @ReasonCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportDetail.InterfaceExportHeaderId,
  ///   InterfaceExportDetail.ForeignKey,
  ///   InterfaceExportDetail.LineNumber,
  ///   InterfaceExportDetail.ProductCode,
  ///   InterfaceExportDetail.Product,
  ///   InterfaceExportDetail.SKUCode,
  ///   InterfaceExportDetail.Batch,
  ///   InterfaceExportDetail.Quantity,
  ///   InterfaceExportDetail.Weight,
  ///   InterfaceExportDetail.Additional1,
  ///   InterfaceExportDetail.Additional2,
  ///   InterfaceExportDetail.Additional3,
  ///   InterfaceExportDetail.Additional4,
  ///   InterfaceExportDetail.Additional5,
  ///   InterfaceExportDetail.OrderQuantity,
  ///   InterfaceExportDetail.RejectQuantity,
  ///   InterfaceExportDetail.ReceivedQuantity,
  ///   InterfaceExportDetail.AcceptedQuantity,
  ///   InterfaceExportDetail.DeliveryNoteQuantity,
  ///   InterfaceExportDetail.ReasonCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportDetail_Insert
(
 @InterfaceExportHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(100) = null,
 @SKUCode nvarchar(100) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @OrderQuantity float = null,
 @RejectQuantity float = null,
 @ReceivedQuantity float = null,
 @AcceptedQuantity float = null,
 @DeliveryNoteQuantity float = null,
 @ReasonCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceExportDetail
        (InterfaceExportHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         OrderQuantity,
         RejectQuantity,
         ReceivedQuantity,
         AcceptedQuantity,
         DeliveryNoteQuantity,
         ReasonCode)
  select @InterfaceExportHeaderId,
         @ForeignKey,
         @LineNumber,
         @ProductCode,
         @Product,
         @SKUCode,
         @Batch,
         @Quantity,
         @Weight,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @OrderQuantity,
         @RejectQuantity,
         @ReceivedQuantity,
         @AcceptedQuantity,
         @DeliveryNoteQuantity,
         @ReasonCode 
  
  select @Error = @@Error
  
  
  return @Error
  
end
