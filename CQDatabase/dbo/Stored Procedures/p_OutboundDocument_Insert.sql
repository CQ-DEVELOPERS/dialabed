﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Insert
  ///   Filename       : p_OutboundDocument_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 07:31:30
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null output,
  ///   @OutboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @FromLocation nvarchar(20) = null,
  ///   @ToLocation nvarchar(20) = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @Quantity float = null,
  ///   @PrincipalId int = null,
  ///   @OperatorId int = null,
  ///   @ExternalOrderNumber nvarchar(60) = null,
  ///   @DirectDeliveryCustomerId int = null,
  ///   @EmployeeCode nvarchar(100) = null,
  ///   @Collector nvarchar(100) = null,
  ///   @AdditionalText1 nvarchar(510) = null,
  ///   @VendorCode nvarchar(60) = null,
  ///   @CustomerOrderNumber nvarchar(60) = null,
  ///   @AdditionalText2 nvarchar(510) = null,
  ///   @BOE nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   OutboundDocument.OutboundDocumentId,
  ///   OutboundDocument.OutboundDocumentTypeId,
  ///   OutboundDocument.ExternalCompanyId,
  ///   OutboundDocument.StatusId,
  ///   OutboundDocument.WarehouseId,
  ///   OutboundDocument.OrderNumber,
  ///   OutboundDocument.DeliveryDate,
  ///   OutboundDocument.CreateDate,
  ///   OutboundDocument.ModifiedDate,
  ///   OutboundDocument.ReferenceNumber,
  ///   OutboundDocument.FromLocation,
  ///   OutboundDocument.ToLocation,
  ///   OutboundDocument.StorageUnitId,
  ///   OutboundDocument.BatchId,
  ///   OutboundDocument.Quantity,
  ///   OutboundDocument.PrincipalId,
  ///   OutboundDocument.OperatorId,
  ///   OutboundDocument.ExternalOrderNumber,
  ///   OutboundDocument.DirectDeliveryCustomerId,
  ///   OutboundDocument.EmployeeCode,
  ///   OutboundDocument.Collector,
  ///   OutboundDocument.AdditionalText1,
  ///   OutboundDocument.VendorCode,
  ///   OutboundDocument.CustomerOrderNumber,
  ///   OutboundDocument.AdditionalText2,
  ///   OutboundDocument.BOE,
  ///   OutboundDocument.Address1 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Insert
(
 @OutboundDocumentId int = null output,
 @OutboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @ReferenceNumber nvarchar(60) = null,
 @FromLocation nvarchar(20) = null,
 @ToLocation nvarchar(20) = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @Quantity float = null,
 @PrincipalId int = null,
 @OperatorId int = null,
 @ExternalOrderNumber nvarchar(60) = null,
 @DirectDeliveryCustomerId int = null,
 @EmployeeCode nvarchar(100) = null,
 @Collector nvarchar(100) = null,
 @AdditionalText1 nvarchar(510) = null,
 @VendorCode nvarchar(60) = null,
 @CustomerOrderNumber nvarchar(60) = null,
 @AdditionalText2 nvarchar(510) = null,
 @BOE nvarchar(510) = null,
 @Address1 nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @DirectDeliveryCustomerId = '-1'
    set @DirectDeliveryCustomerId = null;
  
	 declare @Error int
 
  insert OutboundDocument
        (OutboundDocumentTypeId,
         ExternalCompanyId,
         StatusId,
         WarehouseId,
         OrderNumber,
         DeliveryDate,
         CreateDate,
         ModifiedDate,
         ReferenceNumber,
         FromLocation,
         ToLocation,
         StorageUnitId,
         BatchId,
         Quantity,
         PrincipalId,
         OperatorId,
         ExternalOrderNumber,
         DirectDeliveryCustomerId,
         EmployeeCode,
         Collector,
         AdditionalText1,
         VendorCode,
         CustomerOrderNumber,
         AdditionalText2,
         BOE,
         Address1)
  select @OutboundDocumentTypeId,
         @ExternalCompanyId,
         @StatusId,
         @WarehouseId,
         @OrderNumber,
         @DeliveryDate,
         @CreateDate,
         @ModifiedDate,
         @ReferenceNumber,
         @FromLocation,
         @ToLocation,
         @StorageUnitId,
         @BatchId,
         @Quantity,
         @PrincipalId,
         @OperatorId,
         @ExternalOrderNumber,
         @DirectDeliveryCustomerId,
         @EmployeeCode,
         @Collector,
         @AdditionalText1,
         @VendorCode,
         @CustomerOrderNumber,
         @AdditionalText2,
         @BOE,
         @Address1 
  
  select @Error = @@Error, @OutboundDocumentId = scope_identity()
  
  if @Error = 0
    exec @Error = p_OutboundDocumentHistory_Insert
         @OutboundDocumentId = @OutboundDocumentId,
         @OutboundDocumentTypeId = @OutboundDocumentTypeId,
         @ExternalCompanyId = @ExternalCompanyId,
         @StatusId = @StatusId,
         @WarehouseId = @WarehouseId,
         @OrderNumber = @OrderNumber,
         @DeliveryDate = @DeliveryDate,
         @CreateDate = @CreateDate,
         @ModifiedDate = @ModifiedDate,
         @ReferenceNumber = @ReferenceNumber,
         @FromLocation = @FromLocation,
         @ToLocation = @ToLocation,
         @StorageUnitId = @StorageUnitId,
         @BatchId = @BatchId,
         @Quantity = @Quantity,
         @PrincipalId = @PrincipalId,
         @OperatorId = @OperatorId,
         @ExternalOrderNumber = @ExternalOrderNumber,
         @DirectDeliveryCustomerId = @DirectDeliveryCustomerId,
         @EmployeeCode = @EmployeeCode,
         @Collector = @Collector,
         @AdditionalText1 = @AdditionalText1,
         @VendorCode = @VendorCode,
         @CustomerOrderNumber = @CustomerOrderNumber,
         @AdditionalText2 = @AdditionalText2,
         @BOE = @BOE,
         @Address1 = @Address1,
         @CommandType = 'Insert'
  
  return @Error
  
end
