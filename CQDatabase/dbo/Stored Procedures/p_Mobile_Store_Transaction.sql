﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Store_Transaction
  ///   Filename       : p_Mobile_Store_Transaction.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Store_Transaction
(
 @instructionId int
)
 
as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         InstructionId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int,
          @StatusId          int,
          @OperatorId        int,
          @ReceiptLineId	 int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @JobId      = JobId,
         @OperatorId = OperatorId
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  exec @Error = p_StorageUnitBatchLocation_Allocate
   @InstructionId = @instructionId,
   @Pick          = 0
  
  if @Error <> 0
  begin
    set @Error = 1
    goto Result
  end
  
  set @ReceiptLineId = (select ReceiptLineId from Instruction i where i.InstructionId = @instructionId)
  
  Update ReceiptLine 
  set PutawayStarted = @GetDate
  where ReceiptLineId = @ReceiptLineId
  and PutawayStarted is null
  
  exec @Error = p_Instruction_Finished
   @InstructionId = @instructionId,
   @OperatorId    = @OperatorId
  
  if @Error <> 0
  begin
    set @Error = 1
    goto Result
  end
  
--  if not exists(select 1
--                  from Instruction i (nolock)
--                  join Status      s (nolock) on i.StatusId = s.StatusId
--                 where i.JobId = @JobId
--                   and s.Type = 'I'
--                   and s.StatusCode = 'F')
--  begin
--    exec @Error = p_Job_Update
--     @JobId      = @JobId,
--     @StatusId   = @StatusId,
--     @operatorId = null
--    
--    if @Error <> 0
--    begin
--      set @Error = 1
--      goto Result
--    end
--  end
  
  result:
    update MobileLog
       set EndDate = getdate()
     where MobileLogId = @MobileLogId
    
    select @Error
    return @Error
end
 
