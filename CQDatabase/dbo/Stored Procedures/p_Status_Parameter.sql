﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Parameter
  ///   Filename       : p_Status_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:55
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Status table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Status.StatusId,
  ///   Status.Status 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as StatusId
        ,'{All}' as Status
  union
  select
         Status.StatusId
        ,Status.Status
    from Status
  
end
