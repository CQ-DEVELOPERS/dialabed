﻿--IF OBJECT_ID('dbo.p_Interface_Webservice_ORD') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Interface_Webservice_ORD
--    IF OBJECT_ID('dbo.p_Interface_Webservice_ORD') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Interface_Webservice_ORD >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Interface_Webservice_ORD >>>'
--END
--go


CREATE PROCEDURE p_Interface_Webservice_ORD (@XMLBody NVARCHAR(MAX) OUTPUT)
AS
BEGIN
	CREATE TABLE #ImportIds (InterfaceImportHeaderId INT PRIMARY KEY)

	DECLARE @doc XML
	DECLARE @idoc INT
	DECLARE @InsertDate DATETIME
	DECLARE @ERROR AS VARCHAR(255)

	SELECT @doc = CONVERT(XML, @XMLBody),
		@InsertDate = GETDATE()

	EXEC sp_xml_preparedocument @idoc OUTPUT,
		@doc

	BEGIN TRY
		INSERT [InterfaceImportHeader] (
			[RecordStatus],
			[InsertDate],
			[PrimaryKey],
			[OrderNumber],
			[InvoiceNumber],
			[RecordType],
			[CompanyCode],
			[Company],
			[Address],
			[FromWarehouseCode],
			[ToWarehouseCode],
			[PrincipalCode],
			[Route],
			[DeliveryNoteNumber],
			[ContainerNumber],
			[SealNumber],
			[DeliveryDate],
			[Remarks],
			[NumberOfLines],
			[VatPercentage],
			[VatSummary],
			[Total],
			[Additional1],
			[Additional2],
			[Additional3],
			[Additional4],
			[Additional5],
			[Additional6],
			[Additional7],
			[Additional8],
			[Additional9],
			[Additional10],
			[ItemLine]
			)
		OUTPUT INSERTED.InterfaceImportHeaderId
		INTO #ImportIds
		SELECT 'W',
			@InsertDate,
			header.h.value('PrimaryKey[1]', 'varchar(30)') 'PrimaryKey',
			header.h.value('OrderNumber[1]', 'varchar(30)') 'OrderNumber',
			header.h.value('InvoiceNumber[1]', 'varchar(30)') 'InvoiceNumber',
			header.h.value('ItemType[1]', 'varchar(30)') 'ItemType',
			--,header.h.value('RecordStatus[1]',       'char(1)') ''
			header.h.value('CompanyCode[1]', 'varchar(30)') 'CompanyCode',
			header.h.value('Company[1]', 'varchar(255)') 'Company',
			--,header.h.value('Address[1]',            'varchar(255)') 'Address'
			header.h.value('Address1[1]', 'varchar(255)') + ' ' + header.h.value('Address2[1]', 'varchar(255)') + ' ' + header.h.value('Address3[1]', 'varchar(255)') 'Address',
			header.h.value('FromWarehouseCode[1]', 'varchar(10)') 'FromWarehouseCode',
			header.h.value('ToWarehouseCode[1]', 'varchar(10)') 'ToWarehouseCode',
			header.h.value('PrincipalCode[1]', 'varchar(10)') 'PrincipalCode',
			header.h.value('Route[1]', 'varchar(50)') 'Route',
			header.h.value('DeliveryNoteNumber[1]', 'varchar(30)') 'DeliveryNoteNumber',
			header.h.value('ContainerNumber[1]', 'varchar(30)') 'ContainerNumber',
			header.h.value('SealNumber[1]', 'varchar(30)') 'SealNumber',
			header.h.value('DeliveryDate[1]', 'varchar(20)') 'DeliveryDate',
			header.h.value('Remarks[1]', 'varchar(255)') 'Remarks',
			header.h.value('NumberOfLines[1]', 'int') 'NumberOfLines',
			CAST(ISNULL(header.h.value('VatPercentage[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 3)) 'VatPercentage',
			CAST(ISNULL(header.h.value('VatSummary[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 3)) 'VatSummary',
			CAST(ISNULL(header.h.value('Total[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 3)) 'Total',
			header.h.value('Additional1[1]', 'varchar(255)') 'Additional1',
			header.h.value('Additional2[1]', 'varchar(255)') 'Additional2',
			header.h.value('Additional3[1]', 'varchar(255)') 'Additional3',
			header.h.value('Additional4[1]', 'varchar(255)') 'Additional4',
			header.h.value('Additional5[1]', 'varchar(255)') 'Additional5',
			header.h.value('Address1[1]', 'varchar(255)') 'Additional6',
			header.h.value('Address2[1]', 'varchar(255)') 'Additional7',
			header.h.value('Address3[1]', 'varchar(255)') 'Additional8',
			header.h.value('Address4[1]', 'varchar(255)') 'Additional9',
			header.h.value('Address5[1]', 'varchar(255)') 'Additional10',
			--,header.h.value('ProcessedDate[1]',      'datetime
			--,header.h.value('InsertDate[1]',         'datetime		
			header.h.query('ItemLine')
		FROM @doc.nodes('/root/Body/Item') AS header(h)

		INSERT [InterfaceImportDetail] (
			[InterfaceImportHeaderId],
			[ForeignKey],
			[LineNumber],
			[ProductCode],
			[Product],
			[SKUCode],
			[Batch],
			[Quantity],
			[Weight],
			[RetailPrice],
			[NetPrice],
			[LineTotal],
			[Volume],
			[Additional1],
			[Additional2],
			[Additional3],
			[Additional4],
			[Additional5],
			[SupplierCostPrice]
			)
		SELECT h.InterfaceImportHeaderId,
			detail.d.value('ForeignKey[1]', 'varchar(30)') 'ForeignKey',
			detail.d.value('LineNumber[1]', 'int') 'LineNumber',
			detail.d.value('ProductCode[1]', 'varchar(30)') 'ProductCode',
			detail.d.value('Product[1]', 'varchar(50)') 'Product',
			detail.d.value('SKUCode[1]', 'varchar(10)') 'SKUCode',
			detail.d.value('Batch[1]', 'varchar(50)') 'Batch',
			detail.d.value('Quantity[1]', 'numeric(13,6)') 'Quantity',
			CAST(ISNULL(detail.d.value('Weight[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 3)) 'Weight',
			CAST(ISNULL(detail.d.value('RetailPrice[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 2)) 'RetailPrice',
			CAST(ISNULL(detail.d.value('NetPrice[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 2)) 'NetPrice',
			CAST(ISNULL(detail.d.value('LineTotal[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 2)) 'LineTotal',
			CAST(ISNULL(detail.d.value('Volume[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 3)) 'Volume',
			detail.d.value('Additional1[1]', 'varchar(255)') 'Additional1',
			detail.d.value('Additional2[1]', 'varchar(255)') 'Additional2',
			detail.d.value('Additional3[1]', 'varchar(255)') 'Additional3',
			detail.d.value('Additional4[1]', 'varchar(255)') 'Additional4',
			detail.d.value('Additional5[1]', 'varchar(255)') 'Additional5',
			CAST(ISNULL(detail.d.value('SupplierCostPrice[1]', 'varchar(50)'), '0.00') AS DECIMAL(13, 2)) 'SupplierCostPrice'
		--,detail.d.value('Additional6[1]',        'varchar(255)') 'Additional6'
		--,detail.d.value('Additional7[1]',        'varchar(255)') 'Additional7'
		--,detail.d.value('Additional8[1]',        'varchar(255)') 'Additional8'
		--,detail.d.value('Additional9[1]',        'varchar(255)') 'Additional9'
		--,detail.d.value('Additional10[1]',       'varchar(255)') 'Additional10'
		FROM InterfaceImportHeader AS h
		INNER JOIN #ImportIds ii ON h.InterfaceImportHeaderId = ii.InterfaceImportHeaderId
		CROSS APPLY h.ItemLine.nodes('/ItemLine') AS detail(d)

		UPDATE InterfaceImportHeader
		SET ToWarehouseCode = NULL
		WHERE InterfaceImportHeaderId IN (
				SELECT ii.InterfaceImportHeaderId
				FROM #ImportIds ii
				)
			AND RecordType IN ('SAL', 'COD', 'EPD', 'COL', '3RD', 'DEL', 'FAR', 'EXP', 'NAC', 'DBO')

		UPDATE InterfaceImportHeader
		SET RecordStatus = 'N'
		WHERE InterfaceImportHeaderId IN (
				SELECT ii.InterfaceImportHeaderId
				FROM #ImportIds ii
				)
	END TRY

	BEGIN CATCH
		SELECT @ERROR = LEFT(ERROR_MESSAGE(), 255)
	END CATCH

	EXEC p_Generic_Import_Order

	SET @XMLBody = '<root><status>' + CASE 
			WHEN LEN(@ERROR) > 0
				THEN @ERROR
			ELSE 'Order(s) successfully imported'
			END + '</status></root>'
END

--go
--IF OBJECT_ID('dbo.p_Interface_Webservice_ORD') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Interface_Webservice_ORD >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Interface_Webservice_ORD >>>'
--go


