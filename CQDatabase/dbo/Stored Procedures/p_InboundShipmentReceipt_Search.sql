﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipmentReceipt_Search
  ///   Filename       : p_InboundShipmentReceipt_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:23
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundShipmentReceipt table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null output,
  ///   @ReceiptId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InboundShipmentReceipt.InboundShipmentId,
  ///   InboundShipmentReceipt.ReceiptId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipmentReceipt_Search
(
 @InboundShipmentId int = null output,
 @ReceiptId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InboundShipmentId = '-1'
    set @InboundShipmentId = null;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
 
  select
         InboundShipmentReceipt.InboundShipmentId
        ,InboundShipmentReceipt.ReceiptId
    from InboundShipmentReceipt
   where isnull(InboundShipmentReceipt.InboundShipmentId,'0')  = isnull(@InboundShipmentId, isnull(InboundShipmentReceipt.InboundShipmentId,'0'))
     and isnull(InboundShipmentReceipt.ReceiptId,'0')  = isnull(@ReceiptId, isnull(InboundShipmentReceipt.ReceiptId,'0'))
  
end
