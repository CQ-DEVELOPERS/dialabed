﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Shipment_Get
  ///   Filename       : p_Outbound_Shipment_Get.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Shipment_Get
(
 @OutboundShipmentId	    int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   ReceiptId            int,
   OutboundDocumentId    int,
   OutboundShipmentId    int,
   OrderNumber          nvarchar(30),
   ExternalCompanyCode  nvarchar(30),
   ExternalCompany      nvarchar(255),
   NumberOfLines        int,
   DeliveryDate         datetime,
   StatusId             int,
   Status               nvarchar(50),
   OutboundDocumentType  nvarchar(50),
   LocationId           int,
   Location             nvarchar(15),
   CreateDate           datetime,
   Rating               int
  );
  
  insert @TableResult
        (OutboundShipmentId,
         DeliveryDate,
         OrderNumber)
  select OutboundShipmentId,
         ins.ShipmentDate,
         'No Orders Linked'
    from OutboundShipment ins
  where ins.OutboundShipmentId = isnull(@OutboundShipmentId, ins.OutboundShipmentId)
    
  select OutboundShipmentId,
         ReceiptId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         StatusId,
         Status,
         OutboundDocumentType,
         LocationId,
         Location,
         CreateDate,
         Rating
    from @TableResult
end
