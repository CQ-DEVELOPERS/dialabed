﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Receiving_Check_Sheet_Job
  ///   Filename       : p_Report_Receiving_Check_Sheet_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Receiving_Check_Sheet_Job
(
 @JobId int = null
)
 
as
begin
	 set nocount on;
  
  declare @WarehouseId int
  
  declare @TableHeader as Table
  (
   InboundShipmentId  int,
   ReceiptId          int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   Supplier           nvarchar(255),
   SupplierCode       nvarchar(30)
  );
  
  declare @TableDetails as Table
  (
   ReceiptId          int,
   ReceiptLineId      int,
   LineNumber         int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   ExpectedQuantity   float,
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   SKUCode            nvarchar(50),
   PalletQuantity     float,
   PalletCount        int,
   LayerQuantity      float,
   LayerCount         int,
   CasesQuantity      float,
   CasesCount         int,
   UnitsQuantity      float,
   UnitsCount         int,
   UnitPrice          numeric(13,2),
   SampleQuantity     float default 0
  );
  
  insert @TableDetails
        (ReceiptId,
         ReceiptLineId,
         LineNumber,
         StorageUnitBatchId,
         ExpectedQuantity)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         il.LineNumber,
         i.StorageUnitBatchId,
         i.Quantity
    from Instruction   i
    join ReceiptLine  rl on i.ReceiptLineId  = rl.ReceiptLineId
    join InboundLine  il on rl.InboundLineId = il.InboundLineId
   where i.JobId = @JobId
  
  select @WarehouseId = WarehouseId
    from Job
   where JobId = @JobId
  
  insert @TableHeader
        (InboundShipmentId,
         ReceiptId,
         OrderNumber,
         ExternalCompanyId)
  select isr.InboundShipmentId,
         r.ReceiptId,
         id.OrderNumber,
         id.ExternalCompanyId
    from @TableDetails   td
    join Receipt          r (nolock) on td.ReceiptId = r.ReceiptId
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    left outer
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
  
  if dbo.ufn_Configuration(122, @WarehouseId) = 0
  begin
    update @TableDetails
       set ExpectedQuantity = 0
  end
  
  update th
     set Supplier     = ec.ExternalCompany,
         SupplierCode = ec.ExternalCompanyCode
    from @TableHeader th
    join ExternalCompany ec on th.ExternalCompanyId = ec.ExternalCompanyId
  
  update td
     set ProductCode   = p.ProductCode,
         Product       = p.Product + ' - ' + sku.SKUCode,
         SKUCode       = sku.SKUCode,
         StorageUnitId = su.StorageUnitId
    from @TableDetails    td
    join StorageUnitBatch sub on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId     = su.StorageUnitId
    join Product          p   on su.ProductId          = p.ProductId
    join SKU              sku on su.SKUId              = sku.SKUId
  
  update td
     set PalletQuantity = pk.Quantity
    from @TableDetails td
    join Pack          pk on td.StorageUnitId = pk.StorageUnitId
    join PackType      pt on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence = 1
  
  update td
     set LayerQuantity = pk.Quantity
    from @TableDetails td
    join Pack          pk on td.StorageUnitId = pk.StorageUnitId
    join PackType      pt on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence = 2
  
  update td
     set CasesQuantity = pk.Quantity
    from @TableDetails td
    join Pack          pk on td.StorageUnitId = pk.StorageUnitId
    join PackType      pt on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence = 3
     and isnull(LayerQuantity,0) <= 0 -- Changed - cannot have both
  
  update td
     set UnitsQuantity = pk.Quantity
    from @TableDetails td
    join Pack          pk on td.StorageUnitId = pk.StorageUnitId
    join PackType      pt on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence = 4
  
  update @TableDetails
     set PalletCount = ExpectedQuantity / PalletQuantity
  
  update @TableDetails
     set LayerCount = (ExpectedQuantity - isnull((PalletQuantity * PalletCount),0)) / LayerQuantity
  
--  update @TableDetails
--     set CasesCount = (ExpectedQuantity - (isnull((PalletQuantity * PalletCount),0) + isnull((LayerQuantity * LayerCount),0))) / CasesQuantity
  
  update @TableDetails
     set UnitsCount = (ExpectedQuantity - (isnull((PalletQuantity * PalletCount),0) + isnull((LayerQuantity * LayerCount),0))) / UnitsQuantity
  
  update td
     set UnitPrice = Additional2
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
    join InterfaceImportPODetail d on th.OrderNumber = d.ForeignKey
                                  and td.LineNumber  = d.LineNumber
  
  select th.InboundShipmentId,
         th.OrderNumber,
          dbo.ConvertTo128( th.OrderNumber) as OrderNumberBarcode,
         th.Supplier,
         th.SupplierCode,
         td.ProductCode,
         td.Product,
         td.ExpectedQuantity,
         td.PalletQuantity,
         td.PalletCount,
         td.LayerQuantity,
         td.LayerCount,
         td.CasesQuantity,
         td.CasesCount,
         td.UnitsQuantity,
         td.UnitsCount,
         td.UnitPrice,
         td.SampleQuantity
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
end
