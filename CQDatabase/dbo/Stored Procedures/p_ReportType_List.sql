﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportType_List
  ///   Filename       : p_ReportType_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 15:24:50
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ReportType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ReportType.ReportTypeId,
  ///   ReportType.ReportType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportType_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ReportTypeId
        ,'{All}' as ReportType
  union
  select
         ReportType.ReportTypeId
        ,ReportType.ReportType
    from ReportType
  order by ReportType
  
end
 
