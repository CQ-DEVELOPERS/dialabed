﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_GroupPerformance_Search
  ///   Filename       : p_GroupPerformance_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:40
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the GroupPerformance table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   GroupPerformance.OperatorGroupId,
  ///   GroupPerformance.PerformanceType,
  ///   GroupPerformance.Units,
  ///   GroupPerformance.Weight,
  ///   GroupPerformance.Instructions,
  ///   GroupPerformance.Orders,
  ///   GroupPerformance.OrderLines,
  ///   GroupPerformance.Jobs,
  ///   GroupPerformance.QA 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_GroupPerformance_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         GroupPerformance.OperatorGroupId
        ,GroupPerformance.PerformanceType
        ,GroupPerformance.Units
        ,GroupPerformance.Weight
        ,GroupPerformance.Instructions
        ,GroupPerformance.Orders
        ,GroupPerformance.OrderLines
        ,GroupPerformance.Jobs
        ,GroupPerformance.QA
    from GroupPerformance
  
end
