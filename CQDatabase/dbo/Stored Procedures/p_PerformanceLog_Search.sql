﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PerformanceLog_Search
  ///   Filename       : p_PerformanceLog_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:57
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the PerformanceLog table.
  /// </remarks>
  /// <param>
  ///   @PerformanceLogId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   PerformanceLog.PerformanceLogId,
  ///   PerformanceLog.ProcedureName,
  ///   PerformanceLog.Remarks,
  ///   PerformanceLog.StartDate,
  ///   PerformanceLog.LogDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PerformanceLog_Search
(
 @PerformanceLogId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @PerformanceLogId = '-1'
    set @PerformanceLogId = null;
  
 
  select
         PerformanceLog.PerformanceLogId
        ,PerformanceLog.ProcedureName
        ,PerformanceLog.Remarks
        ,PerformanceLog.StartDate
        ,PerformanceLog.LogDate
    from PerformanceLog
   where isnull(PerformanceLog.PerformanceLogId,'0')  = isnull(@PerformanceLogId, isnull(PerformanceLog.PerformanceLogId,'0'))
  
end
