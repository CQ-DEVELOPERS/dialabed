﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Confirm_Inbound_Order
  ///   Filename       : p_Mobile_Confirm_Inbound_Order.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   

  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Confirm_Inbound_Order
(
 @warehouseId int,
 @orderNumber nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @ReceiptId         int,
          @InboundDocumentId int,
          @OldWarehouseId    int
  
  set @Error = 0

  
  set @OldWarehouseId = @warehouseId
  
  if (select dbo.ufn_Configuration(260, @WarehouseId)) = 1
    set @WarehouseId = null
  
  select @ReceiptId         = r.ReceiptId,
         @InboundDocumentId = id.InboundDocumentId
    from InboundDocument id (nolock)
    join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join Status           s (nolock) on r.StatusId = s.StatusId
   where id.WarehouseId = isnull(@warehouseid, id.WarehouseId)
     and id.OrderNumber = @orderNumber
     and s.StatusCode  in ('W','R')
  
  if @ReceiptId is null
  begin
    set @ReceiptId = -1
    goto result
  end
  else
  begin
    if @warehouseId is null -- update to new warehouse
    begin
      exec @Error = p_InboundDocument_Update
       @InboundDocumentId = @InboundDocumentId,
       @WarehouseId = @OldWarehouseId
      
      if @Error <> 0
      begin
        set @ReceiptId = -1
        goto result
      end
      
      exec @Error = p_Receipt_Update
       @ReceiptId   = @ReceiptId,
   
    @WarehouseId = @OldWarehouseId
      
      if @Error <> 0
      begin
        set @ReceiptId = -1
        goto result
      end
    end
    
    set @Error = -1
    goto result
  end
  
  result:
    select @ReceiptId as '@ReceiptId'
    return @ReceiptId
end

 
