﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_FPC_Products
  ///   Filename       : p_Report_FPC_Products.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jan 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure dbo.p_Report_FPC_Products
(
 @StorageUnitId int = null
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitId = -1
    set @StorageUnitId = null
  
  select psu.StorageUnitId,
         pp.ProductCode,
         pp.Product,
         csu.StorageUnitId,
         cc.ProductCode as 'ChildProductCode',
         cc.Product     as 'ChildProduct',
         b.Batch,
         b.ExpiryDate
    from ChildProduct        cp (nolock) -- Parent prefixed by "p" - Child (FPC) prefix by "c"
    join StorageUnitBatch psub (nolock) on cp.ParentStorageUnitBatchId = psub.StorageUnitBatchId
    join StorageUnitBatch csub (nolock) on cp.ChildStorageUnitBatchId = csub.StorageUnitBatchId
                                       and psub.BatchId       = csub.BatchId -- Joined by batch
                                       and psub.StorageUnitId != csub.StorageUnitId
    join StorageUnit       psu (nolock) on psub.StorageUnitId = psu.StorageUnitId
    join Product            pp (nolock) on psu.ProductId      = pp.ProductId
    
    join StorageUnit       csu (nolock) on csub.StorageUnitId = csu.StorageUnitId
    join Product            cc (nolock) on csu.ProductId      = cc.ProductId
                                       --and pp.ProductCode     = cp.ParentProductCode
    join Batch               b (nolock) on psub.BatchId       = b.BatchId
  where (psub.StorageUnitId = isnull(@StorageUnitId, psub.StorageUnitId)
    or   csub.StorageUnitId = isnull(@StorageUnitId, csub.StorageUnitId))
    and b.Batch != 'Default'
end
