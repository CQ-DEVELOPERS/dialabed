﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentType_Select
  ///   Filename       : p_OutboundDocumentType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jan 2014 09:16:08
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentTypeId int = null 
  /// </param>
  /// <returns>
  ///   OutboundDocumentType.OutboundDocumentTypeId,
  ///   OutboundDocumentType.OutboundDocumentTypeCode,
  ///   OutboundDocumentType.OutboundDocumentType,
  ///   OutboundDocumentType.PriorityId,
  ///   OutboundDocumentType.AlternatePallet,
  ///   OutboundDocumentType.SubstitutePallet,
  ///   OutboundDocumentType.AutoRelease,
  ///   OutboundDocumentType.AddToShipment,
  ///   OutboundDocumentType.MultipleOnShipment,
  ///   OutboundDocumentType.LIFO,
  ///   OutboundDocumentType.MinimumShelfLife,
  ///   OutboundDocumentType.AreaType,
  ///   OutboundDocumentType.AutoSendup,
  ///   OutboundDocumentType.CheckingLane,
  ///   OutboundDocumentType.DespatchBay,
  ///   OutboundDocumentType.PlanningComplete,
  ///   OutboundDocumentType.AutoInvoice,
  ///   OutboundDocumentType.Backorder,
  ///   OutboundDocumentType.AutoCheck,
  ///   OutboundDocumentType.ReserveBatch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentType_Select
(
 @OutboundDocumentTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         OutboundDocumentType.OutboundDocumentTypeId
        ,OutboundDocumentType.OutboundDocumentTypeCode
        ,OutboundDocumentType.OutboundDocumentType
        ,OutboundDocumentType.PriorityId
        ,OutboundDocumentType.AlternatePallet
        ,OutboundDocumentType.SubstitutePallet
        ,OutboundDocumentType.AutoRelease
        ,OutboundDocumentType.AddToShipment
        ,OutboundDocumentType.MultipleOnShipment
        ,OutboundDocumentType.LIFO
        ,OutboundDocumentType.MinimumShelfLife
        ,OutboundDocumentType.AreaType
        ,OutboundDocumentType.AutoSendup
        ,OutboundDocumentType.CheckingLane
        ,OutboundDocumentType.DespatchBay
        ,OutboundDocumentType.PlanningComplete
        ,OutboundDocumentType.AutoInvoice
        ,OutboundDocumentType.Backorder
        ,OutboundDocumentType.AutoCheck
        ,OutboundDocumentType.ReserveBatch
    from OutboundDocumentType
   where isnull(OutboundDocumentType.OutboundDocumentTypeId,'0')  = isnull(@OutboundDocumentTypeId, isnull(OutboundDocumentType.OutboundDocumentTypeId,'0'))
  order by OutboundDocumentTypeCode
  
end
