﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QAAudit_Select
  ///   Filename       : p_QAAudit_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the QAAudit table.
  /// </remarks>
  /// <param>
  ///   @QAAuditId int = null 
  /// </param>
  /// <returns>
  ///   QAAudit.QAAuditId,
  ///   QAAudit.JobId,
  ///   QAAudit.StorageUnitBatchId,
  ///   QAAudit.OrderedQuantity,
  ///   QAAudit.PickedQuantity,
  ///   QAAudit.CheckedQuantity,
  ///   QAAudit.WHQuantity,
  ///   QAAudit.PickedBy,
  ///   QAAudit.PickedDate,
  ///   QAAudit.CheckedBy,
  ///   QAAudit.CheckedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QAAudit_Select
(
 @QAAuditId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         QAAudit.QAAuditId
        ,QAAudit.JobId
        ,QAAudit.StorageUnitBatchId
        ,QAAudit.OrderedQuantity
        ,QAAudit.PickedQuantity
        ,QAAudit.CheckedQuantity
        ,QAAudit.WHQuantity
        ,QAAudit.PickedBy
        ,QAAudit.PickedDate
        ,QAAudit.CheckedBy
        ,QAAudit.CheckedDate
    from QAAudit
   where isnull(QAAudit.QAAuditId,'0')  = isnull(@QAAuditId, isnull(QAAudit.QAAuditId,'0'))
  
end
