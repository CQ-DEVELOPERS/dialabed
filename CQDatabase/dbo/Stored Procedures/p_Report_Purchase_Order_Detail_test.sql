﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Purchase_Order_Detail_test
  ///   Filename       : p_Report_Purchase_Order_Detail_test.sql
  ///   Create By      : Karen
  ///   Date Created   : 01 Nov 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Purchase_Order_Detail_test
(
 @OrderNumber       nvarchar(30),
 @ProductCode       nvarchar(30),
 @Batch             nvarchar(50),
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin

	 set nocount on;
	 
begin

--drop table @temp

Declare @temp as table
(
		 BatchId		int,
         ProductCode	nvarchar(30),
         WarehouseId	int,
         SKUCode		nvarchar(50),
         StockOnHand	int
)

insert @temp (BatchId, ProductCode, WarehouseId, SKUCode, StockOnHand) 
select sub.BatchId, 
	   p.ProductCode, 
	   a.WarehouseId,
	   sku.SKUCode,
	   sum(subl.ActualQuantity)
    from StorageUnitBatch          sub (nolock)
    join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId           = p.ProductId
    join SKU                       sku (nolock) on su.SKUId               = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId            = b.BatchId
    join Status                      s (nolock) on b.StatusId             = s.StatusId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId        = l.LocationId
    join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
    left
    join Pack                       pk (nolock) on su.StorageUnitId       = pk.StorageUnitId
                                               and a.WarehouseId          = pk.WarehouseId
                                               and pk.PackTypeId          = 1
group by sub.BatchId, 
		 p.ProductCode,
		 a.WarehouseId,
		 sku.SKUCode
 end

 
    
  declare @TableDetails as Table
  (
   OrderNumber			nvarchar(30),
   ExternalOrderNumber	nvarchar(30),
   OrderDate			datetime,
   ExpectedDeliveryDate	datetime,
   ProductCode			nvarchar(30),
   ProductDesc			nvarchar(255),
   Batch	            nvarchar(50),
   OrderQty				numeric(13,6),
   ReceivedQty		    numeric(13,6),
   OutstandingBal		numeric(13,6),
   StockOnHand		    numeric(13,6)
  )
  
   if @ProductCode = '-1'
	   set @ProductCode = null
  
   if @OrderNumber = '-1'
	   set @OrderNumber = null
	 
   if @Batch = '-1'
	   set @Batch = null
  
  insert   @TableDetails
          (OrderNumber,
           OrderDate,
           ExpectedDeliveryDate,
           ProductCode,
           ProductDesc,
           Batch,
           OrderQty,
           ReceivedQty,
           OutstandingBal,
           StockOnHand
           )
    select id.OrderNumber,
           id.CreateDate,
           id.DeliveryDate,
           p.ProductCode,
           p.Product,
           b.Batch,
           sum(rl.RequiredQuantity),
           sum(rl.ReceivedQuantity),
           sum(rl.RequiredQuantity - isnull(rl.ReceivedQuantity,0)),
           sum(StockOnHand)
           
      from  InboundDocument         id
      join  InboundLine  il on      id.InboundDocumentId = il.InboundDocumentId
      join  Batch        b  on       b.BatchId = il.BatchId
      join  StorageUnit  su on		il.StorageUnitId     = su.StorageUnitId
      join  SKU         sku on      su.SKUId               = sku.SKUId
      join  Product      p  on      su.ProductId         = p.ProductId
      join  ReceiptLine  rl on      il.InboundLineId     = rl.InboundLineId
      left
      join @temp	    soh on		il.BatchId			 = soh.BatchId
							and		soh.ProductCode		 = p.ProductCode
							and		id.WarehouseId		 = soh.WarehouseId
							and		sku.SKUCode			 = soh.SKUCode
      where id.CreateDate           between @FromDate and @ToDate
      and   id.InboundDocumentTypeId = '1' 
      and   (rl.StatusId = (select StatusId = dbo.ufn_StatusId('R','RC'))
      or     rl.StatusId = (select StatusId = dbo.ufn_StatusId('R','R')))
      group by	id.OrderNumber,
				id.CreateDate,
				id.DeliveryDate,
				p.ProductCode,
				p.Product,
				b.Batch
 
  select	OrderNumber,
			ExternalOrderNumber,
			OrderDate,
			ExpectedDeliveryDate,
			ProductCode,
			ProductDesc,
			Batch,
			OrderQty,
			ReceivedQty,
			OutstandingBal,
			StockOnHand 
    from	@TableDetails  
    where   OrderNumber = isnull(@OrderNumber, OrderNumber)
    and		ProductCode = isnull(@ProductCode, ProductCode)
    and		Batch = isnull(@Batch, Batch)
    order by ProductCode
    
    end
