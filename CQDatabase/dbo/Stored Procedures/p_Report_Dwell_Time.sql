﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Dwell_Time
  ///   Filename       : p_Report_Dwell_Time.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Dwell_Time
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int,
 @InstructionTypeId int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
  
  declare @GetDate datetime
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 declare @TableResult as table
	 (
	  JobId              int,
   InstructionType    nvarchar(50),
   StorageUnitBatchId int,
   Quantity           float,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   CreateDate         datetime,
   StartDate          datetime,
   EndDate            datetime,
   Hours              nvarchar(8),
   Minutes            nvarchar(2),
   Seconds            nvarchar(2),
   jobStatus          nvarchar(50),
   insStatus          nvarchar(50)
	 )
	 
	 if @InstructionTypeId = -1
	   set @InstructionTypeId = null
	 
	 insert @TableResult
        (JobId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
         Quantity,
         CreateDate,
         StartDate,
         jobStatus,
         insStatus)
  select i.JobId,
         it.InstructionType,
         i.StorageUnitBatchId,
         i.PickLocationId,
         i.StoreLocationId,
         i.Quantity,
         i.CreateDate,
         i.StartDate,
         sj.Status,
         null --si.Status
    from Instruction      i (nolock)
    join Status          si (nolock) on i.StatusId          = si.StatusId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status          sj (nolock) on j.StatusId          = sj.StatusId
   where i.WarehouseId        = @WarehouseId
     and it.InstructionTypeId = isnull(@InstructionTypeId, it.InstructionTypeId)
     and i.CreateDate   between @FromDate and @ToDate
  
--  update tr
--     set StartDate = jh.InsertDate
--    from @TableResult tr
--    join JobHistory   jh (nolock) on tr.JobId    = jh.JobId
--    join Status        s (nolock) on jh.StatusId = s.StatusId
--   where s.StatusCode = 'A'
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
  
  update @TableResult
     set Hours   =  DateDiff(ss, CreateDate, StartDate) / 3600,
         Minutes = (DateDiff(ss, CreateDate, StartDate) % 3600) / 60,
         Seconds =  DateDiff(ss, CreateDate, StartDate) % 60
  
  update @TableResult
     set Minutes = '0' + Minutes
   where datalength(Minutes) = 1
  
  update @TableResult
     set Seconds = '0' + Seconds
   where datalength(Seconds) = 1
  
--  update @TableResult
--     set DwellTime = convert(nvarchar(10), DateDiff(ss, CreateDate, StartDate) / 3600) + ':' + 
--                     convert(nvarchar(2), (DateDiff(ss, CreateDate, StartDate) % 3600) / 60) + ':' + 
--                     convert(nvarchar(2),  DateDiff(ss, CreateDate, StartDate) % 60)
  
  select tr.InstructionType,
         tr.PickLocation,
         tr.StoreLocation,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         tr.Quantity,
         tr.CreateDate,
         tr.StartDate,
         tr.Hours + ':' + tr.Minutes + ':' + tr.Seconds as 'DwellTime',
         tr.jobStatus,
         tr.insStatus
    from @TableResult    tr
    join viewStock       vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
  order by tr.CreateDate
end
