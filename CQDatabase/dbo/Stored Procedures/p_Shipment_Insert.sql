﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shipment_Insert
  ///   Filename       : p_Shipment_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:48
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Shipment table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null output,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Route nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   Shipment.ShipmentId,
  ///   Shipment.StatusId,
  ///   Shipment.WarehouseId,
  ///   Shipment.LocationId,
  ///   Shipment.ShipmentDate,
  ///   Shipment.Remarks,
  ///   Shipment.SealNumber,
  ///   Shipment.VehicleRegistration,
  ///   Shipment.Route 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shipment_Insert
(
 @ShipmentId int = null output,
 @StatusId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(500) = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Route nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @ShipmentId = '-1'
    set @ShipmentId = null;
  
	 declare @Error int
 
  insert Shipment
        (StatusId,
         WarehouseId,
         LocationId,
         ShipmentDate,
         Remarks,
         SealNumber,
         VehicleRegistration,
         Route)
  select @StatusId,
         @WarehouseId,
         @LocationId,
         @ShipmentDate,
         @Remarks,
         @SealNumber,
         @VehicleRegistration,
         @Route 
  
  select @Error = @@Error, @ShipmentId = scope_identity()
  
  
  return @Error
  
end
