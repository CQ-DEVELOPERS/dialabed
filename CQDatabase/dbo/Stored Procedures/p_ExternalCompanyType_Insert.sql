﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyType_Insert
  ///   Filename       : p_ExternalCompanyType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:38
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ExternalCompanyType table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyTypeId int = null output,
  ///   @ExternalCompanyType nvarchar(100) = null,
  ///   @ExternalCompanyTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   ExternalCompanyType.ExternalCompanyTypeId,
  ///   ExternalCompanyType.ExternalCompanyType,
  ///   ExternalCompanyType.ExternalCompanyTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyType_Insert
(
 @ExternalCompanyTypeId int = null output,
 @ExternalCompanyType nvarchar(100) = null,
 @ExternalCompanyTypeCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @ExternalCompanyTypeId = '-1'
    set @ExternalCompanyTypeId = null;
  
  if @ExternalCompanyType = '-1'
    set @ExternalCompanyType = null;
  
  if @ExternalCompanyTypeCode = '-1'
    set @ExternalCompanyTypeCode = null;
  
	 declare @Error int
 
  insert ExternalCompanyType
        (ExternalCompanyType,
         ExternalCompanyTypeCode)
  select @ExternalCompanyType,
         @ExternalCompanyTypeCode 
  
  select @Error = @@Error, @ExternalCompanyTypeId = scope_identity()
  
  
  return @Error
  
end
