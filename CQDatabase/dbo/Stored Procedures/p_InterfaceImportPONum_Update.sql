﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPONum_Update
  ///   Filename       : p_InterfaceImportPONum_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:00
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportPONum table.
  /// </remarks>
  /// <param>
  ///   @id nvarchar(40) = null,
  ///   @Ordernumber nvarchar(100) = null,
  ///   @ReferenceNumber nvarchar(100) = null,
  ///   @docstate nvarchar(100) = null,
  ///   @statusId nvarchar(100) = null,
  ///   @OrderDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPONum_Update
(
 @id nvarchar(40) = null,
 @Ordernumber nvarchar(100) = null,
 @ReferenceNumber nvarchar(100) = null,
 @docstate nvarchar(100) = null,
 @statusId nvarchar(100) = null,
 @OrderDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceImportPONum
     set id = isnull(@id, id),
         Ordernumber = isnull(@Ordernumber, Ordernumber),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         docstate = isnull(@docstate, docstate),
         statusId = isnull(@statusId, statusId),
         OrderDate = isnull(@OrderDate, OrderDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
