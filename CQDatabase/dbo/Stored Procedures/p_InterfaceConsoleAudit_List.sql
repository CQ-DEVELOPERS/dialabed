﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceConsoleAudit_List
  ///   Filename       : p_InterfaceConsoleAudit_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:51
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceConsoleAudit table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceConsoleAudit.InterfaceConsoleAuditId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceConsoleAudit_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceConsoleAuditId
        ,null as 'InterfaceConsoleAudit'
  union
  select
         InterfaceConsoleAudit.InterfaceConsoleAuditId
        ,InterfaceConsoleAudit.InterfaceConsoleAuditId as 'InterfaceConsoleAudit'
    from InterfaceConsoleAudit
  
end
