﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaLocation_Delete
  ///   Filename       : p_AreaLocation_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:42
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the AreaLocation table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaLocation_Delete
(
 @AreaId int = null,
 @LocationId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete AreaLocation
     where AreaId = @AreaId
       and LocationId = @LocationId
  
  select @Error = @@Error
  
  
  return @Error
  
end
