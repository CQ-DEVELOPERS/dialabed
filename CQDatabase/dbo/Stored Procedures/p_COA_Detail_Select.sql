﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Detail_Select
  ///   Filename       : p_COA_Detail_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Detail_Select
(
 @COAId int,
 @StorageUnitId int
)
 
as
begin
	 set nocount on;
  
  select coa.COAId,
         cd.COADetailId,
         n.NoteId,
         n.Note
    from COA coa
    join COADetail cd on coa.COAId = cd.COAId
    left
    join Note       n on cd.NoteId = n.NoteId
   where coa.COAId = @COAId
end
