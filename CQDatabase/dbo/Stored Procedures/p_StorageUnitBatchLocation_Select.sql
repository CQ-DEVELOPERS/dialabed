﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocation_Select
  ///   Filename       : p_StorageUnitBatchLocation_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitBatchLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null,
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  ///   StorageUnitBatchLocation.StorageUnitBatchId,
  ///   StorageUnitBatchLocation.LocationId,
  ///   StorageUnitBatchLocation.ActualQuantity,
  ///   StorageUnitBatchLocation.AllocatedQuantity,
  ///   StorageUnitBatchLocation.ReservedQuantity,
  ///   StorageUnitBatchLocation.NettWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Select
(
 @StorageUnitBatchId int = null,
 @LocationId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         StorageUnitBatchLocation.StorageUnitBatchId
        ,StorageUnitBatchLocation.LocationId
        ,StorageUnitBatchLocation.ActualQuantity
        ,StorageUnitBatchLocation.AllocatedQuantity
        ,StorageUnitBatchLocation.ReservedQuantity
        ,StorageUnitBatchLocation.NettWeight
    from StorageUnitBatchLocation
   where isnull(StorageUnitBatchLocation.StorageUnitBatchId,'0')  = isnull(@StorageUnitBatchId, isnull(StorageUnitBatchLocation.StorageUnitBatchId,'0'))
     and isnull(StorageUnitBatchLocation.LocationId,'0')  = isnull(@LocationId, isnull(StorageUnitBatchLocation.LocationId,'0'))
  
end
