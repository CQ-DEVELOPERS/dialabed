﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Request_Replenishment
  ///   Filename       : p_Mobile_Request_Replenishment.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Request_Replenishment
(
 @instructionId int,
 @ShowMsg       bit = 1
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @LocationId         int,
          @Location           nvarchar(15),
          @StorageUnitBatchId int,
          @Barcode            nvarchar(50),
          @OperatorId         int,
          @ActualQuantity     float
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 11
  
  select @Location           = l.Location,
         @LocationId         = l.LocationId,
         @StorageUnitBatchId = i.StorageUnitBatchId,
         @OperatorId         = i.OperatorId
    from Instruction i (nolock)
    join Location    l (nolock) on i.PickLocationId = l.LocationId
   where i.InstructionId = @instructionId
  
  select @ActualQuantity = ActualQuantity
    from StorageUnitBatchLocation
   where LocationId = @LocationId
     and StorageUnitBatchId = @StorageUnitBatchId
     and ActualQuantity <= 0
  
  if(@ActualQuantity is null or @ActualQuantity <= 0)
  begin
    exec @Error = p_Mobile_Replenishment_Request
     @storeLocationId    = @LocationId,
     @storageUnitBatchId = @StorageUnitBatchId,
     @operatorId         = @OperatorId,
     @ShowMsg            = @ShowMsg,
     @ManualReq          = 0
  end
  
  result:
    if @ShowMsg = 1
      select @Error
    return @Error
end
