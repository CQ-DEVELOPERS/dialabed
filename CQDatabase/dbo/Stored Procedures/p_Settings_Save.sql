﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Settings_Save
  ///   Filename       : p_Settings_Save.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Settings_Save
(
 @operatorId  int,
 @settingCode nvarchar(100),
 @setting     nvarchar(max)
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Settings_Save',
          @GetDate           datetime,
          @Transaction       bit = 1,
          @SettingId         int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  select @SettingId = SettingId
    from Setting (nolock)
   where SettingCode = @settingCode
     and OperatorId  = @operatorId
  
  if @SettingId is null
  begin
    insert Setting
          (SettingCode,
           Setting,
           OperatorId)
    select @settingCode,
           @setting,
           @operatorId
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
  end
  else
  begin
    update Setting
       set Setting = @setting
     where SettingId = @SettingId
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
