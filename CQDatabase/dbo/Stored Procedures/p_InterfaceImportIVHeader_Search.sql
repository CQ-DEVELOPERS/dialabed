﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIVHeader_Search
  ///   Filename       : p_InterfaceImportIVHeader_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:39
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportIVHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIVHeaderId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportIVHeader.InterfaceImportIVHeaderId,
  ///   InterfaceImportIVHeader.PrimaryKey,
  ///   InterfaceImportIVHeader.OrderNumber,
  ///   InterfaceImportIVHeader.InvoiceNumber,
  ///   InterfaceImportIVHeader.RecordType,
  ///   InterfaceImportIVHeader.RecordStatus,
  ///   InterfaceImportIVHeader.CustomerCode,
  ///   InterfaceImportIVHeader.Customer,
  ///   InterfaceImportIVHeader.Address,
  ///   InterfaceImportIVHeader.FromWarehouseCode,
  ///   InterfaceImportIVHeader.ToWarehouseCode,
  ///   InterfaceImportIVHeader.Route,
  ///   InterfaceImportIVHeader.DeliveryDate,
  ///   InterfaceImportIVHeader.Remarks,
  ///   InterfaceImportIVHeader.NumberOfLines,
  ///   InterfaceImportIVHeader.VatPercentage,
  ///   InterfaceImportIVHeader.VatSummary,
  ///   InterfaceImportIVHeader.Total,
  ///   InterfaceImportIVHeader.Additional1,
  ///   InterfaceImportIVHeader.Additional2,
  ///   InterfaceImportIVHeader.Additional3,
  ///   InterfaceImportIVHeader.Additional4,
  ///   InterfaceImportIVHeader.Additional5,
  ///   InterfaceImportIVHeader.Additional6,
  ///   InterfaceImportIVHeader.Additional7,
  ///   InterfaceImportIVHeader.Additional8,
  ///   InterfaceImportIVHeader.Additional9,
  ///   InterfaceImportIVHeader.Additional10,
  ///   InterfaceImportIVHeader.ProcessedDate,
  ///   InterfaceImportIVHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIVHeader_Search
(
 @InterfaceImportIVHeaderId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceImportIVHeaderId = '-1'
    set @InterfaceImportIVHeaderId = null;
  
 
  select
         InterfaceImportIVHeader.InterfaceImportIVHeaderId
        ,InterfaceImportIVHeader.PrimaryKey
        ,InterfaceImportIVHeader.OrderNumber
        ,InterfaceImportIVHeader.InvoiceNumber
        ,InterfaceImportIVHeader.RecordType
        ,InterfaceImportIVHeader.RecordStatus
        ,InterfaceImportIVHeader.CustomerCode
        ,InterfaceImportIVHeader.Customer
        ,InterfaceImportIVHeader.Address
        ,InterfaceImportIVHeader.FromWarehouseCode
        ,InterfaceImportIVHeader.ToWarehouseCode
        ,InterfaceImportIVHeader.Route
        ,InterfaceImportIVHeader.DeliveryDate
        ,InterfaceImportIVHeader.Remarks
        ,InterfaceImportIVHeader.NumberOfLines
        ,InterfaceImportIVHeader.VatPercentage
        ,InterfaceImportIVHeader.VatSummary
        ,InterfaceImportIVHeader.Total
        ,InterfaceImportIVHeader.Additional1
        ,InterfaceImportIVHeader.Additional2
        ,InterfaceImportIVHeader.Additional3
        ,InterfaceImportIVHeader.Additional4
        ,InterfaceImportIVHeader.Additional5
        ,InterfaceImportIVHeader.Additional6
        ,InterfaceImportIVHeader.Additional7
        ,InterfaceImportIVHeader.Additional8
        ,InterfaceImportIVHeader.Additional9
        ,InterfaceImportIVHeader.Additional10
        ,InterfaceImportIVHeader.ProcessedDate
        ,InterfaceImportIVHeader.InsertDate
    from InterfaceImportIVHeader
   where isnull(InterfaceImportIVHeader.InterfaceImportIVHeaderId,'0')  = isnull(@InterfaceImportIVHeaderId, isnull(InterfaceImportIVHeader.InterfaceImportIVHeaderId,'0'))
  
end
