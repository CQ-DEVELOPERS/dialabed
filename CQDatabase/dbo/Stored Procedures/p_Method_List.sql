﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Method_List
  ///   Filename       : p_Method_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:05
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Method table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Method.MethodId,
  ///   Method.Method 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Method_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as MethodId
        ,'{All}' as Method
  union
  select
         Method.MethodId
        ,Method.Method
    from Method
  order by Method
  
end
