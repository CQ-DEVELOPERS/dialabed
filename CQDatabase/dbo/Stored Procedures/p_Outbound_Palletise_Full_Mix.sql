﻿
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_Full_Mix
  ///   Filename       : p_Outbound_Palletise_Full_Mix.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Outbound_Palletise_Full_Mix
(
 @WarehouseId        int,
 @StoreLocationId    int,
 @OutboundShipmentId int,
 @IssueId            int,
 @OperatorId         int,
 @Weight             numeric(13,6),
 @Volume             numeric(13,6),
 @JobId              int = null output
)
AS
 
declare @fm_working_stock as table
(StorageUnitBatchId             int,
 Quantity                       float NULL,
 weight                         float NULL)
 
declare @fm_lower_stock as table
(StorageUnitBatchId             int,
 Quantity                       float,
 SKUQuantity                    float,
 SKUCode                        nvarchar(50),
 UnitWeight                     float,
 UnitVolume                     float)
 
declare @fm_DropSequence as table
(
 DropSequence int,
 AreaSequence int
)
 
declare @fm_stock as table
(StorageUnitBatchId             int,
 Quantity                       float,
 SKUQuantity                    float,
 SkuCode                        nvarchar(50),
 UnitWeight                     float,
 UnitVolume                     float,
 DropSequence                   int,
 AreaSequence                   int)
 
declare @fm_item as table
(
 IssueLineId                    int,
 StorageUnitBatchId             int,
 Quantity                       float,
 weight                         float)

begin
  begin transaction
  
  declare @Error                        int,
          @Errormsg                     nvarchar(500),
          @DropSequence                 int,
          @DropSequenceCount            int,
          @AreaSequence                 int,
          @PackQuantity                 float,
          @PackWeight                   float,
          @PackVolume                   float,
          @Quantity                     float,
          @StorageUnitBatchId           int,
          @ProductCount                 int,
          @jc_qty                       float,
          @ProductQuantity              float,
          @run_weight                   float,
          @run_vol                      float,
          @LowerCount                   int,
          @LowerProduct                 numeric(10),
          @LowerQuantity                float,
          @LowerWeight                  float,
          @LowerVolume                  float,
          @WeightQuantity               float,
          @VolumeQuantity               float,
          @ItemCount                    int,
          @ItemQuantity                 float,
          @ItemWeight                   float,
          @ProductCode                  nvarchar(30),
          @IssueLineId                  int,
          @PriorityId                   int,
          @StatusId                     int,
          @SKUCode                      nvarchar(50),
          @SKUQuantity                  float,
          @GetDate                      datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @PriorityId = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'FM'
  
  /**********************/
  /* Process each drop  */
  /**********************/
  insert @fm_DropSequence
  select DropSequence,
         AreaSequence
    from PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
  group by DropSequence, AreaSequence
  
  select @DropSequenceCount = @@rowcount
         
  while @DropSequenceCount > 0
  begin
    select top 1
           @DropSequence = DropSequence,
           @AreaSequence = AreaSequence
      from @fm_DropSequence
    order by DropSequence desc, AreaSequence
    
    delete @fm_DropSequence
     where DropSequence = @DropSequence
       and AreaSequence = @AreaSequence
    
    select @DropSequenceCount = @DropSequenceCount - 1
    
    /*******************************/
    /* Process each stock lot item */
    /*******************************/
    insert @fm_stock
          (StorageUnitBatchId,
           Quantity,
           SKUQuantity,
           SKUCode,
           UnitWeight,
           UnitVolume,
           DropSequence,
           AreaSequence)
    select StorageUnitBatchId,
           Quantity,
           PalletQuantity,
           SKUCode,
           UnitWeight,
           UnitVolume,
           DropSequence,
           AreaSequence
      from PalletiseStock
     where DropSequence       = @DropSequence
       and AreaSequence       = @AreaSequence
       and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
    order by PalletQuantity, Quantity desc
    --order by (s.sku_multiply_factor / s.sku_divide_factor)  desc  -- added 07/12/2000
    
    select @ProductCount = @@rowcount
    
    while @ProductCount > 0
    begin -- while @ProductCount > 0
      select top 1
             @StorageUnitBatchId = StorageUnitBatchId,
             @Quantity           = Quantity,
             @SKUQuantity        = SKUQuantity,
             @SKUCode            = SKUCode,
             @PackWeight         = UnitWeight,
             @PackVolume         = UnitVolume
        from @fm_stock
      
      delete @fm_stock
       where StorageUnitBatchId = @StorageUnitBatchId
      
      select @ProductCount = @ProductCount - 1
      
      select @run_weight = @Weight,
             @run_vol    = @Volume
      
      /**********************************/
      /* Insert the first stock item  */
      /**********************************/
      insert @fm_working_stock
      select @StorageUnitBatchId,
             @Quantity,
             (@PackWeight * @Quantity)
      
      /*********************************/
      /* Update the running totals for */
      /* weight & volume               */
      /*********************************/
      select @run_weight = @run_weight - (@PackWeight * @Quantity),
             @run_vol    = @run_vol - (@PackVolume * @Quantity)
      
      /***********************************/
      /* Remove the stock from available */
      /* stock                           */
      /***********************************/
      update PalletiseStock
         set Quantity = Quantity - @Quantity
       where DropSequence       = @DropSequence
         and AreaSequence       = @AreaSequence
         and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and IssueId            = isnull(@IssueId, -1)
         and StorageUnitBatchId = @StorageUnitBatchId
      
      /***********************************/
      /* Get all other stock that can be */
      /* added with this pallet          */
      /***********************************/
      insert @fm_lower_stock
            (StorageUnitBatchId,
             Quantity,
             SKUQuantity,
             SKUCode,
             UnitWeight,
             UnitVolume)
      select StorageUnitBatchId,
             Quantity,
             SKUQuantity,
             SKUCode,
             UnitWeight,
             UnitVolume
        from @fm_stock
       where DropSequence = @DropSequence
         and AreaSequence = @AreaSequence
      order by SKUQuantity,
               Quantity
      
      select @LowerCount = @@rowcount
      
      while @LowerCount > 0
      begin
        set @LowerProduct = null
        select top 1
               @LowerProduct = StorageUnitBatchId,
               @LowerWeight  = UnitWeight,
               @LowerVolume  = UnitVolume
          from @fm_lower_stock
         where SKUCode = @SKUCode -- try get the same sku
        order by SKUQuantity
        
        if @LowerProduct is null
        begin
          select top 1
                 @LowerProduct = StorageUnitBatchId,
                 @SKUCode      = SKUCode,
                 @LowerWeight  = UnitWeight,
                 @LowerVolume  = UnitVolume
            from @fm_lower_stock
          order by SKUQuantity desc, Quantity desc
        end
        
        delete @fm_lower_stock
         where StorageUnitBatchId = @LowerProduct
        
        select @LowerCount = @LowerCount - 1
        
        IF @LowerWeight = 0
        begin
          select @ProductCode = p.ProductCode
            from StorageUnitBatch sub (nolock)
            join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
            join Product            p (nolock) on su.ProductId      = p.ProductId
           where StorageUnitBatchId = @LowerProduct
          
          select @ErrorMsg = 'Unit Weight = 0 for Stock Item = ' + @ProductCode + '. Change unit weight and paletise again.'
          RAISERROR 50100 @ErrorMsg
--          rollback transaction
          commit transaction
          return -1
          RETURN
        end
        
        IF @LowerVolume = 0
        begin
          select @ProductCode = p.ProductCode
            from StorageUnitBatch sub (nolock)
            join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
            join Product            p (nolock) on su.ProductId      = p.ProductId
           where StorageUnitBatchId = @LowerProduct
          
          select @ErrorMsg = 'Unit Volume = 0 for Stock Item = ' + @ProductCode + '. Change unit volume and paletise again.'
          RAISERROR 50101 @ErrorMsg
--          rollback transaction
          commit transaction
          return -1
          RETURN
        end
        
        select @WeightQuantity = Floor(@run_weight/@LowerWeight),
               @VolumeQuantity = Floor(@run_vol/@LowerVolume)
        
        IF @WeightQuantity < @VolumeQuantity
          select @Quantity = @WeightQuantity
        ELSE
          select @Quantity = @VolumeQuantity
        
        /******************************/
        /* Get the available stock    */
        /******************************/
        IF @Quantity > 0
        begin
          select @LowerQuantity = Quantity
            from PalletiseStock
           where DropSequence       = @DropSequence
             and AreaSequence       = @AreaSequence
             and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
             and IssueId            = isnull(@IssueId, -1)
             and StorageUnitBatchId = @LowerProduct
          
          /***************************************/
          /* Allocate the lower of the two qty's */
          /***************************************/
          IF @LowerQuantity <= @Quantity
          begin
            select @Quantity = @LowerQuantity
            
            /*******************************************/
            /* IF there is no units left of the stock  */
            /* remove it from the available stock list */
            /*******************************************/
            delete @fm_stock
             where StorageUnitBatchId = @LowerProduct
            
            select @ProductCount  = @ProductCount - 1
            
            insert @fm_working_stock
            select @LowerProduct,
                   @Quantity,
                   (@LowerWeight * @Quantity)
            
            update PalletiseStock
               set Quantity = Quantity - @Quantity
             where DropSequence       = @DropSequence
               and AreaSequence       = @AreaSequence
               and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
               and IssueId            = isnull(@IssueId, -1)
               and StorageUnitBatchId = @LowerProduct
            
            select @run_weight = @run_weight - (@LowerWeight * @Quantity),
                   @run_vol = @run_vol - (@LowerVolume * @Quantity)
          end
        end
      end
      
      delete @fm_lower_stock
      if @ProductCount > 0
      begin
        /**********************************/
        /* Create the document item lines */
        /**********************************/
        insert @fm_item
        select p.IssueLineId,
               p.StorageUnitBatchId,
               w.Quantity ,
               w.Weight
          from PalletiseStock    p
          join @fm_working_stock w on p.StorageUnitBatchId = w.StorageUnitBatchId
         where p.DropSequence       = @DropSequence
           and p.AreaSequence       = @AreaSequence
           and p.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and p.IssueId            = isnull(@IssueId, -1)
        
        select @ItemCount = @@RowCount
        
        /***********************/
        /* Create the job card */
        /***********************/
        if @ItemCount > 0
        begin
          select @StatusId = dbo.ufn_StatusId('IS','P')
          
          if (dbo.ufn_Configuration(477, @WarehouseId)) = 0 
          or @JobId is null
          begin
          exec @Error = p_Job_Insert
           @JobId           = @JobId output,
           @PriorityId      = @PriorityId,
           @OperatorId      = @OperatorId,
           @StatusId        = @StatusId,
           @WarehouseId     = @WarehouseId,
           @DropSequence    = @DropSequence
          
          if @Error <> 0
            goto error
          end
          
          exec @Error = p_OutboundPerformance_Insert
           @JobId         = @JobId,
           @CreateDate    = @GetDate
          
          if @error <> 0
            goto error
        end
        
        while @ItemCount > 0
        begin
          select top 1
                 @IssueLineId        = IssueLineId,
                 @StorageUnitBatchId = StorageUnitBatchId,
                 @ItemQuantity       = Quantity,
                 @ItemWeight         = weight
            from @fm_item
          
          set rowcount 1
          
          delete @fm_item
           where isnull(IssueLineId,-1) = isnull(@IssueLineId,-1)
            and StorageUnitBatchId      = @StorageUnitBatchId
            and Quantity                = @ItemQuantity
            and weight                  = @ItemWeight
          
          set rowcount 0
          
          select @ItemCount = @ItemCount - 1
          
          select @StoreLocationId = al.LocationId
            from StorageUnitBatch sub (nolock)
            join StorageUnitArea sua (nolock) on sub.StorageUnitId = sua.StorageUnitId
            join Area              a (nolock) on sua.AreaId = a.AreaId
            join AreaLocation     al (nolock) on a.CheckingAreaId = al.AreaId
           where sub.StorageUnitBatchId = @StorageUnitBatchId
          
          exec @Error = p_Palletised_Insert
           @WarehouseId         = @WarehouseId,
           @OperatorId          = null,
           @InstructionTypeCode = 'FM',
           @StorageUnitBatchId  = @StorageUnitBatchId,
           @PickLocationId      = null,
           @StoreLocationId     = @StoreLocationId,
           @Quantity            = @ItemQuantity,
           @Weight              = @ItemWeight,
           @IssueLineId         = @IssueLineId,
           @JobId               = @JobId,
           @OutboundShipmentId  = @OutboundShipmentId,
           @DropSequence        = @DropSequence
          
          if @Error <> 0  
            goto error
        end
      end
      else
      /**********************************/
      /* If the pallet is not full add  */
      /* the values back                */
      /**********************************/
      begin
        update PalletiseStock
           set Quantity = p.Quantity + w.Quantity,
               JobId    = @JobId
          from PalletiseStock    p
          join @fm_working_stock w on p.StorageUnitBatchId = w.StorageUnitBatchId
         where p.DropSequence       = @DropSequence
           and p.AreaSequence       = @AreaSequence
           and p.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and p.IssueId            = isnull(@IssueId, -1)
      end
      
      delete @fm_item
      delete @fm_working_stock
    end -- while @ProductCount > 0
    
    delete @fm_stock
    delete @fm_lower_stock
  end
  delete @fm_stock
  delete @fm_lower_stock
  delete @fm_working_stock
  delete @fm_DropSequence
  
  if (dbo.ufn_Configuration(477, @WarehouseId)) = 0 
    set @JobId = null;
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_pseudo_palletise2'
    rollback transaction
    return @Error
end
