﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitLocation_Parameter
  ///   Filename       : p_StorageUnitLocation_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:09
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitLocation table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnitLocation.StorageUnitId,
  ///   StorageUnitLocation.LocationId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitLocation_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as StorageUnitId
        ,null as 'StorageUnitLocation'
        ,null as LocationId
        ,null as 'StorageUnitLocation'
  union
  select
         StorageUnitLocation.StorageUnitId
        ,StorageUnitLocation.StorageUnitId as 'StorageUnitLocation'
        ,StorageUnitLocation.LocationId
        ,StorageUnitLocation.LocationId as 'StorageUnitLocation'
    from StorageUnitLocation
  
end
