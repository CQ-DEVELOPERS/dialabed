﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Search_Full
  ///   Filename       : p_Batch_Search_Full.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Search_Full
(
 @WarehouseId   int = null,
 @BatchNumber	  nvarchar(50) = null,
 @ProductCode			nvarchar(50) = null,
 @Product		     nvarchar(50) = null,
 @BatchStatusId	int = null,
 @DaysoldLt				 int = null,
 @DaysOldGt				 int = null,
 @FromDate	     datetime = null,
 @ToDate	       datetime = null,
 @SOHOnly				   nvarchar(50) = null	
)
 
as
begin
  set nocount on;
  
  create table #TableResult
  (
   BatchId                         int              null,
   StorageUnitId                   int              null,
   StorageUnitBatchId              int              null,
   Indicator                       varchar(6)       null,
   StatusCode                      nvarchar(10)     null,
   Status                          nvarchar(50)     null,
   Batch                           nvarchar(50)     null,
   BatchReferenceNumber            nvarchar(50)     null,
   CreateDate                      datetime         null,
   ExpiryDate                      datetime         null,
   Remaining                       int              null,
   Product                         nvarchar(255)    null,
   ProductCode                     nvarchar(30)     null,
   Quantity                        float            null,
   COACertificate                  varchar(1)       null
  )
  
  declare @GetDate  datetime,
          @ShowZero nvarchar(50)
		
  if @SOHOnly = null set @SOHOnly  = 'false'
  
  if @SOHOnly = 'true'
    set @ShowZero = 'false'
  else 
    set @ShowZero = 'true'
  
  select @GetDate = dbo.ufn_Getdate()
  
	if @BatchNumber = '-1' set @BatchNumber = null  
	if @Product = '-1' set @Product = null
	if @ProductCode = '-1' set @ProductCode = null
 if (@BatchStatusId = -1) or (@BatchStatusId = 0) set @BatchStatusId = null  
	if @DaysoldLt = -1 set @DaysoldLt = null  
	if @DaysOldGt = -1 set @DaysOldGt = null  
	if @FromDate = -1 Set @FromDate = null
	if @ToDate = -1 Set @ToDate = null
	
	if (@BatchNumber is not null) or (@DaysOldGt  is not null) or (@DaysoldLt is not null)
	 begin
		  set @FromDate = null
		  set @ToDate = null
	 end
	 
	 if @Product is null or @ProductCode is null
	 begin
	   insert #TableResult
	         (BatchId,
           Status,
           StatusCode,
           Batch,
           BatchReferenceNumber,
           CreateDate,
           ExpiryDate,
           COACertificate)
		  select top 100 b.BatchId,
           s.Status,
           s.StatusCode,
           b.Batch,
           b.BatchReferenceNumber,
           b.CreateDate,
           b.ExpiryDate,
           b.COACertificate
		   from Batch                       b (nolock)
		   Join Status                      s (nolock) on s.StatusId              = b.StatusId
  		                                            and s.StatusId = isnull(@BatchStatusId,s.StatusId)
		  where Batch like isnull('%' + @BatchNumber + '%',Batch)
		    and b.CreateDate >= isnull(@FromDate,CreateDate)
		  	 and b.CreateDate <= isnull(@ToDate,CreateDate)
  		
    update tr
       set Indicator          = case when @GetDate >= isnull(ExpiryDate,Dateadd(dd, isnull(p.ShelfLifeDays,0), tr.CreateDate)) then 'Red'
                                     when datediff(dd,@GetDate, isnull(tr.ExpiryDate, Dateadd(dd, isnull(p.ShelfLifeDays,0), tr.CreateDate))) < 60 then 'Blue'
                                     when tr.StatusCode = 'A' then 'Green'
                                     else 'Yellow'
                                     end,
           StorageUnitId      = sub.StorageUnitId,
           StorageUnitBatchId = sub.StorageUnitBatchId,
           Product            = p.Product,
           ProductCode        = p.ProductCode,
           ExpiryDate         = isnull(tr.ExpiryDate,Dateadd(dd,isnull(p.ShelfLifeDays,0),tr.CreateDate)),
           Remaining          = datediff(dd,@GetDate,isnull(ExpiryDate,Dateadd(dd,isnull(p.ShelfLifeDays,0),createdate)))
      from #TableResult tr
		    join StorageUnitBatch          sub (nolock) on sub.BatchId             = tr.BatchId
		    join StorageUnit                su (nolock) on su.StorageUnitId        = sub.StorageUnitId
		    join Product                     p (nolock) on p.ProductId             = su.ProductId
		end
		else
		begin
	   insert #TableResult
	         (BatchId,
           Status,
           StatusCode,
           Batch,
           BatchReferenceNumber,
           CreateDate,
           ExpiryDate,
           COACertificate,
           Indicator,
           StorageUnitId,
           StorageUnitBatchId,
           Product,
           ProductCode,
           Remaining)
		  select distinct b.BatchId,
           s.Status,
           s.StatusCode,
           b.Batch,
           b.BatchReferenceNumber,
           b.CreateDate,
           isnull(b.ExpiryDate,Dateadd(dd,isnull(p.ShelfLifeDays,0),b.CreateDate)),
           b.COACertificate,
           case when @GetDate >= isnull(ExpiryDate,Dateadd(dd, isnull(p.ShelfLifeDays,0), b.CreateDate)) then 'Red'
                when datediff(dd,@GetDate, isnull(b.ExpiryDate, Dateadd(dd, isnull(p.ShelfLifeDays,0), b.CreateDate))) < 60 then 'Blue'
                when s.StatusCode = 'A' then 'Green'
                else 'Yellow'
                end,
           sub.StorageUnitId,
           sub.StorageUnitBatchId,
           p.Product,
           p.ProductCode,
           datediff(dd,@GetDate,isnull(ExpiryDate,Dateadd(dd,isnull(p.ShelfLifeDays,0),createdate)))
		   from Batch                       b (nolock)
		   Join Status                      s (nolock) on s.StatusId              = b.StatusId
		   join StorageUnitBatch          sub (nolock) on sub.BatchId             = b.BatchId
		   join StorageUnit                su (nolock) on su.StorageUnitId        = sub.StorageUnitId
		   join Product                     p (nolock) on p.ProductId             = su.ProductId
		                                              and isnull(Product,'') like isnull('%' + @Product + '%',isnull(Product,''))
			                                             and isnull(ProductCode,'') like isnull('%' + @ProductCode + '%',isnull(ProductCode,''))
  		                                            and s.StatusId = isnull(@BatchStatusId,s.StatusId)
		  where b.CreateDate >= isnull(@FromDate,CreateDate)
		  	 and b.CreateDate <= isnull(@ToDate,CreateDate)
		end
  
  update tr
     set Quantity = (select Sum(isnull(subl.ActualQuantity,0) + isnull(subl.AllocatedQuantity,0))
                       from StorageUnitBatchLocation subl (nolock)
                                                                       --and case when subl.Locationid is null
                                                                       --    then @ShowZero
                                                                       --    else 'true'
                                                                       --    end  = 'true'
	                           join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
	                           join Area                        a (nolock) on al.AreaId               = a.AreaId
	                                                                      and a.WarehouseId           = @WarehouseId
	                     where subl.StorageUnitBatchId = tr.StorageUnitBatchId)
    from #TableResult tr
		
		select BatchId,
         StorageUnitId,
         Indicator,
         Status,
         Batch,
         BatchReferenceNumber,
         CreateDate,
         ExpiryDate,
         Remaining,
         Product,
         ProductCode,
         Quantity,
         COACertificate
   from #TableResult
		--where isnull(@DaysOldGt,-99999) <= datediff(ExpiryDate,CreateDate))
		--  and isnull(@DaysOldLt,9999)  >= datediff(ExpiryDate,CreateDate)))
  Order by createdate
end
