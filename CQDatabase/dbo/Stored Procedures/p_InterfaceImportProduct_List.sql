﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportProduct_List
  ///   Filename       : p_InterfaceImportProduct_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:03
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportProduct table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportProduct.InterfaceImportProductId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportProduct_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceImportProductId
        ,null as 'InterfaceImportProduct'
  union
  select
         InterfaceImportProduct.InterfaceImportProductId
        ,InterfaceImportProduct.InterfaceImportProductId as 'InterfaceImportProduct'
    from InterfaceImportProduct
  
end
