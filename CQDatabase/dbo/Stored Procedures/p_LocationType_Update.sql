﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationType_Update
  ///   Filename       : p_LocationType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:59
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the LocationType table.
  /// </remarks>
  /// <param>
  ///   @LocationTypeId int = null,
  ///   @LocationType nvarchar(100) = null,
  ///   @LocationTypeCode nvarchar(20) = null,
  ///   @DeleteIndicator bit = null,
  ///   @ReplenishmentIndicator bit = null,
  ///   @MultipleProductsIndicator bit = null,
  ///   @BatchTrackingIndicator bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationType_Update
(
 @LocationTypeId int = null,
 @LocationType nvarchar(100) = null,
 @LocationTypeCode nvarchar(20) = null,
 @DeleteIndicator bit = null,
 @ReplenishmentIndicator bit = null,
 @MultipleProductsIndicator bit = null,
 @BatchTrackingIndicator bit = null 
)
 
as
begin
	 set nocount on;
  
  if @LocationTypeId = '-1'
    set @LocationTypeId = null;
  
  if @LocationType = '-1'
    set @LocationType = null;
  
  if @LocationTypeCode = '-1'
    set @LocationTypeCode = null;
  
	 declare @Error int
 
  update LocationType
     set LocationType = isnull(@LocationType, LocationType),
         LocationTypeCode = isnull(@LocationTypeCode, LocationTypeCode),
         DeleteIndicator = isnull(@DeleteIndicator, DeleteIndicator),
         ReplenishmentIndicator = isnull(@ReplenishmentIndicator, ReplenishmentIndicator),
         MultipleProductsIndicator = isnull(@MultipleProductsIndicator, MultipleProductsIndicator),
         BatchTrackingIndicator = isnull(@BatchTrackingIndicator, BatchTrackingIndicator) 
   where LocationTypeId = @LocationTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
