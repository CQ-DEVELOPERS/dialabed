﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportBOM_Insert
  ///   Filename       : p_InterfaceImportBOM_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:19
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportBOM table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportBOMId int = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Quantity float = null,
  ///   @Product nvarchar(510) = null,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @HostID nvarchar(60) = null,
  ///   @InsertDate datetime = null,
  ///   @Additional1 varchar(255) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportBOM.InterfaceImportBOMId,
  ///   InterfaceImportBOM.RecordStatus,
  ///   InterfaceImportBOM.ProcessedDate,
  ///   InterfaceImportBOM.ProductCode,
  ///   InterfaceImportBOM.Quantity,
  ///   InterfaceImportBOM.Product,
  ///   InterfaceImportBOM.PrimaryKey,
  ///   InterfaceImportBOM.HostID,
  ///   InterfaceImportBOM.InsertDate,
  ///   InterfaceImportBOM.Additional1,
  ///   InterfaceImportBOM.Additional2,
  ///   InterfaceImportBOM.Additional3,
  ///   InterfaceImportBOM.Additional4,
  ///   InterfaceImportBOM.Additional5,
  ///   InterfaceImportBOM.Additional6,
  ///   InterfaceImportBOM.Additional7,
  ///   InterfaceImportBOM.Additional8,
  ///   InterfaceImportBOM.Additional9 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportBOM_Insert
(
 @InterfaceImportBOMId int = null,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null,
 @ProductCode nvarchar(60) = null,
 @Quantity float = null,
 @Product nvarchar(510) = null,
 @PrimaryKey nvarchar(60) = null,
 @HostID nvarchar(60) = null,
 @InsertDate datetime = null,
 @Additional1 varchar(255) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportBOM
        (InterfaceImportBOMId,
         RecordStatus,
         ProcessedDate,
         ProductCode,
         Quantity,
         Product,
         PrimaryKey,
         HostID,
         InsertDate,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9)
  select @InterfaceImportBOMId,
         @RecordStatus,
         @ProcessedDate,
         @ProductCode,
         @Quantity,
         @Product,
         @PrimaryKey,
         @HostID,
         isnull(@InsertDate, getdate()),
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9 
  
  select @Error = @@Error
  
  
  return @Error
  
end
