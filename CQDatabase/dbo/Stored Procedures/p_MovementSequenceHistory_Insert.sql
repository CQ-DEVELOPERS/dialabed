﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MovementSequenceHistory_Insert
  ///   Filename       : p_MovementSequenceHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:01
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MovementSequenceHistory table.
  /// </remarks>
  /// <param>
  ///   @MovementSequenceId int = null,
  ///   @PickAreaId int = null,
  ///   @StoreAreaId int = null,
  ///   @StageAreaId int = null,
  ///   @OperatorGroupId int = null,
  ///   @StatusCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   MovementSequenceHistory.MovementSequenceId,
  ///   MovementSequenceHistory.PickAreaId,
  ///   MovementSequenceHistory.StoreAreaId,
  ///   MovementSequenceHistory.StageAreaId,
  ///   MovementSequenceHistory.OperatorGroupId,
  ///   MovementSequenceHistory.StatusCode,
  ///   MovementSequenceHistory.CommandType,
  ///   MovementSequenceHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MovementSequenceHistory_Insert
(
 @MovementSequenceId int = null,
 @PickAreaId int = null,
 @StoreAreaId int = null,
 @StageAreaId int = null,
 @OperatorGroupId int = null,
 @StatusCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert MovementSequenceHistory
        (MovementSequenceId,
         PickAreaId,
         StoreAreaId,
         StageAreaId,
         OperatorGroupId,
         StatusCode,
         CommandType,
         InsertDate)
  select @MovementSequenceId,
         @PickAreaId,
         @StoreAreaId,
         @StageAreaId,
         @OperatorGroupId,
         @StatusCode,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
