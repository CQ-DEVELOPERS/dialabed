﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipment_Update
  ///   Filename       : p_OutboundShipment_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Oct 2013 09:31:10
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OutboundShipment table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Route nvarchar(80) = null,
  ///   @RouteId int = null,
  ///   @Weight float = null,
  ///   @Volume decimal(13,3) = null,
  ///   @FP bit = null,
  ///   @SS bit = null,
  ///   @LP bit = null,
  ///   @FM bit = null,
  ///   @MP bit = null,
  ///   @PP bit = null,
  ///   @AlternatePallet bit = null,
  ///   @SubstitutePallet bit = null,
  ///   @DespatchBay int = null,
  ///   @NumberOfOrders int = null,
  ///   @ContactListId int = null,
  ///   @TotalOrders int = null,
  ///   @IDN bit = null,
  ///   @WaveId int = null,
  ///   @ReferenceNumber nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipment_Update
(
 @OutboundShipmentId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(500) = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Route nvarchar(80) = null,
 @RouteId int = null,
 @Weight float = null,
 @Volume decimal(13,3) = null,
 @FP bit = null,
 @SS bit = null,
 @LP bit = null,
 @FM bit = null,
 @MP bit = null,
 @PP bit = null,
 @AlternatePallet bit = null,
 @SubstitutePallet bit = null,
 @DespatchBay int = null,
 @NumberOfOrders int = null,
 @ContactListId int = null,
 @TotalOrders int = null,
 @IDN bit = null,
 @WaveId int = null,
 @ReferenceNumber nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
  if @OutboundShipmentId = '-1'
    set @OutboundShipmentId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
	 declare @Error int
 
  update OutboundShipment
     set StatusId = isnull(@StatusId, StatusId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         LocationId = isnull(@LocationId, LocationId),
         ShipmentDate = isnull(@ShipmentDate, ShipmentDate),
         Remarks = isnull(@Remarks, Remarks),
         SealNumber = isnull(@SealNumber, SealNumber),
         VehicleRegistration = isnull(@VehicleRegistration, VehicleRegistration),
         Route = isnull(@Route, Route),
         RouteId = isnull(@RouteId, RouteId),
         Weight = isnull(@Weight, Weight),
         Volume = isnull(@Volume, Volume),
         FP = isnull(@FP, FP),
         SS = isnull(@SS, SS),
         LP = isnull(@LP, LP),
         FM = isnull(@FM, FM),
         MP = isnull(@MP, MP),
         PP = isnull(@PP, PP),
         AlternatePallet = isnull(@AlternatePallet, AlternatePallet),
         SubstitutePallet = isnull(@SubstitutePallet, SubstitutePallet),
         DespatchBay = isnull(@DespatchBay, DespatchBay),
         NumberOfOrders = isnull(@NumberOfOrders, NumberOfOrders),
         ContactListId = isnull(@ContactListId, ContactListId),
         TotalOrders = isnull(@TotalOrders, TotalOrders),
         IDN = isnull(@IDN, IDN),
         WaveId = isnull(@WaveId, WaveId),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber) 
   where OutboundShipmentId = @OutboundShipmentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
