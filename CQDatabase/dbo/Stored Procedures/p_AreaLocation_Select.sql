﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaLocation_Select
  ///   Filename       : p_AreaLocation_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:42
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaLocation table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  ///   AreaLocation.AreaId,
  ///   AreaLocation.LocationId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaLocation_Select
(
 @AreaId int = null,
 @LocationId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         AreaLocation.AreaId
        ,AreaLocation.LocationId
    from AreaLocation
   where isnull(AreaLocation.AreaId,'0')  = isnull(@AreaId, isnull(AreaLocation.AreaId,'0'))
     and isnull(AreaLocation.LocationId,'0')  = isnull(@LocationId, isnull(AreaLocation.LocationId,'0'))
  
end
