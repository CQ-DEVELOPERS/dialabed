﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitArea_Select
  ///   Filename       : p_StorageUnitArea_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012 14:20:20
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitArea table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  ///   StorageUnitArea.StorageUnitId,
  ///   StorageUnitArea.AreaId,
  ///   StorageUnitArea.StoreOrder,
  ///   StorageUnitArea.PickOrder,
  ///   StorageUnitArea.MinimumQuantity,
  ///   StorageUnitArea.ReorderQuantity,
  ///   StorageUnitArea.MaximumQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitArea_Select
(
 @StorageUnitId int = null,
 @AreaId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  select
         StorageUnitArea.StorageUnitId,
         StorageUnitArea.AreaId,
         StorageUnitArea.StoreOrder,
         StorageUnitArea.PickOrder,
         StorageUnitArea.MinimumQuantity,
         StorageUnitArea.ReorderQuantity,
         StorageUnitArea.MaximumQuantity 
    from StorageUnitArea
   where isnull(StorageUnitArea.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(StorageUnitArea.StorageUnitId,'0'))
     and isnull(StorageUnitArea.AreaId,'0')  = isnull(@AreaId, isnull(StorageUnitArea.AreaId,'0'))
  
end
