﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletBulk_Update
  ///   Filename       : p_PalletBulk_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:41
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the PalletBulk table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletBulk_Update
(
 @PalletId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update PalletBulk
     set PalletId = isnull(@PalletId, PalletId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
