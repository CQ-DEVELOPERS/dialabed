﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_Insert
  ///   Filename       : p_InstructionType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:53
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null output,
  ///   @PriorityId int = null,
  ///   @InstructionType nvarchar(60) = null,
  ///   @InstructionTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   InstructionType.InstructionTypeId,
  ///   InstructionType.PriorityId,
  ///   InstructionType.InstructionType,
  ///   InstructionType.InstructionTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_Insert
(
 @InstructionTypeId int = null output,
 @PriorityId int = null,
 @InstructionType nvarchar(60) = null,
 @InstructionTypeCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @InstructionType = '-1'
    set @InstructionType = null;
  
  if @InstructionTypeCode = '-1'
    set @InstructionTypeCode = null;
  
	 declare @Error int
 
  insert InstructionType
        (PriorityId,
         InstructionType,
         InstructionTypeCode)
  select @PriorityId,
         @InstructionType,
         @InstructionTypeCode 
  
  select @Error = @@Error, @InstructionTypeId = scope_identity()
  
  
  return @Error
  
end
