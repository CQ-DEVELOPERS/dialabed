﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportHeading_List
  ///   Filename       : p_ReportHeading_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:25
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ReportHeading table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ReportHeading.ReportHeadingId,
  ///   ReportHeading.ReportHeading 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportHeading_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ReportHeadingId
        ,'{All}' as ReportHeading
  union
  select
         ReportHeading.ReportHeadingId
        ,ReportHeading.ReportHeading
    from ReportHeading
  
end
