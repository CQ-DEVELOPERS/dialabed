﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Short_Pick_Advice
  ///   Filename       : p_Report_Short_Pick_Advice.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Short_Pick_Advice
(
 @ConnectionString   nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @OutboundShipmentId int,
 @IssueId            int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   LineNumber         int,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
	 
	 if @OutboundShipmentId is not null
	   set @IssueId = null
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         IssueLineId,
         OutboundDocumentId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity)
--  select ili.OutboundShipmentId,
--         ili.IssueId,
--         ili.IssueLineId,
--         ili.OutboundDocumentId,
--         i.JobId,
--         i.StorageUnitBatchId,
--         ili.Quantity,
--         isnull(ili.ConfirmedQuantity, 0),
--         ili.Quantity - isnull(ili.ConfirmedQuantity, 0)
--    from IssueLineInstruction  ili (nolock)
--    join Instruction             i (nolock) on ili.InstructionId    = i.InstructionId
--   where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))
--     and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
--     and ili.Quantity - isnull(ili.ConfirmedQuantity, 0) > 0
  select osi.OutboundShipmentId,
         osi.IssueId,
         il.IssueLineId,
         i.OutboundDocumentId,
         il.StorageUnitBatchId,
         il.Quantity,
         isnull(il.ConfirmedQuatity, 0),
         il.Quantity - isnull(il.ConfirmedQuatity, 0)
    from OutboundShipmentIssue osi (nolock)
    join Issue                   i (nolock) on osi.IssueId = i.IssueId
    join IssueLine              il (nolock) on i.IssueId = il.IssueId
   where osi.OutboundShipmentId = @OutboundShipmentId
     and il.Quantity - isnull(il.ConfirmedQuatity, 0) > 0
  
  update tr
     set LineNumber = ol.LineNumber
    from @TableResult tr
    join IssueLine    il (nolock) on tr.IssueLineId    = il.IssueLineId
    join OutboundLine ol (nolock) on il.OutboundLineId = ol.OutboundLineId
  
  update tr
     set OrderNumber       = od.OrderNumber
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode
    from @TableResult    tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  select OrderNumber,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         sum(Quantity) as 'Quantity',
         sum(ConfirmedQuantity) as 'ConfirmedQuantity',
         sum(ShortQuantity) as 'ShortQuantity'
    from @TableResult
  group by OrderNumber,
           LineNumber,
           ProductCode,
           Product,
           SKUCode
  order by OrderNumber,
           LineNumber
end
