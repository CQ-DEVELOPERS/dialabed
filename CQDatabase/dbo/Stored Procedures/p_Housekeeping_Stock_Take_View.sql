﻿--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_View') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Housekeeping_Stock_Take_View
--    IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_View') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Housekeeping_Stock_Take_View >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Housekeeping_Stock_Take_View >>>'
--END
--go
 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_View
  ///   Filename       : p_Housekeeping_Stock_Take_View.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Jun 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_View
(
 @jobId int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
   InstructionId      int,
   JobId              int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   BatchId            int,
   LineNumber         int identity,
   LocationId         int,
   Location           nvarchar(15),
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   ExpectedQuantity   float,
   PreviousCount2     float,
   PreviousCount1     float,
   LatestCount        float,
   Status             nvarchar(50),
   RelativeValue      numeric(13,3),
   PrincipalId		  int,
   Principal		  nvarchar(50)
	 )
	 
	 insert @TableResult
	       (InstructionId,
         JobId,
         StorageUnitBatchId,
         LocationId,
         PreviousCount1,
         LatestCount,
         Status,
         Location,
         RelativeValue)
  select i.InstructionId,
         i.JobId,
         i.StorageUnitBatchId,
         i.PickLocationId,
         i.Quantity,
         i.ConfirmedQuantity,
         s.Status,
         l.Location,
         l.RelativeValue
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId    = j.JobId
    join Status           s (nolock) on i.StatusId = s.StatusId
    join Location         l (nolock) on i.PickLocationId = l.LocationId
   where it.InstructionTypeCode in ('STE','STL','STA','STP')
     and s.Type        = 'I'
     and j.JobId       = isnull(@jobId, j.JobId)
  order by l.RelativeValue, l.Location, i.CreateDate
  
  update tr
     set StorageUnitId = sub.StorageUnitId,
         BatchId       = sub.BatchId
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = left(p.Product, 50),
         PrincipalId = p.PrincipalId,
         Principal   = pr.Principal
    from @TableResult tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product       p (nolock) on su.ProductId      = p.ProductId
    left
    join Principal    pr (nolock) on p.PrincipalId = pr.PrincipalId
  
  update tr
     set SKUCode = sku.SKUCode
    from @TableResult tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join SKU         sku (nolock) on su.SKUId          = sku.SKUId
  
  update tr
     set Batch = b.Batch
    from @TableResult tr
    join Batch         b (nolock) on tr.BatchId = b.BatchId
  
  --update tr
  --   set Location = l.Location,
  --       RelativeValue = l.RelativeValue
  --  from @TableResult tr
  --  join Location      l (nolock) on tr.LocationId = l.LocationId
  
  select InstructionId,
         JobId,
         StorageUnitBatchId,
         LineNumber,
         Location,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ExpectedQuantity,
         PreviousCount2,
         PreviousCount1,
         LatestCount,
         Status,
         Principal
    from @TableResult
   order by RelativeValue, Location
end
--go
--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_View') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Housekeeping_Stock_Take_View >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Housekeeping_Stock_Take_View >>>'
--go


