﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber_Search_Pallet
  ///   Filename       : p_Register_SerialNumber_Search_Pallet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber_Search_Pallet
(
 @instructionId int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        InstructionId             int,
        OrderNumber               nvarchar(30),
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        StorageUnitBatchId        int,
        PalletId                  int
       );
  
  declare @PalletId           int,
          @StorageUnitBatchId int,
          @ReceiptLineId      int,
          @IssueLineId        int
  
  select @PalletId           = PalletId,
         @StorageUnitBatchId = StorageUnitBatchId,
         @ReceiptLineId      = ReceiptLineId,
         @IssueLineId        = IssueLineId
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  if @ReceiptLineId is null
  begin
    insert @TableResult
          (InstructionId,
           OrderNumber,
           SupplierCode,
           Supplier,
           StorageUnitBatchId,
           PalletId)
    select @instructionId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           @StorageUnitBatchId,
           @PalletId
      from Issue             i (nolock)
      join IssueLine        il (nolock) on i.IssueId = il.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
     where il.IssueLineId = @IssueLineId
  end
  else
  begin
    insert @TableResult
          (InstructionId,
           OrderNumber,
           SupplierCode,
           Supplier,
           StorageUnitBatchId,
           PalletId)
    select @instructionId,
           id.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           @StorageUnitBatchId,
           @PalletId
      from Receipt          r (nolock)
      join ReceiptLine     rl (nolock) on r.ReceiptId = rl.ReceiptId
      join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
      join ExternalCompany ec (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
     where rl.ReceiptLineId = @ReceiptLineId
  end
  
  select t.InstructionId,
         su.StorageUnitId,
         t.OrderNumber,
         t.SupplierCode,
         t.Supplier,
         t.PalletId,
         p.ProductCode,
         p.Product
    from @TableResult t
    join StorageUnitBatch sub on t.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId    = su.StorageUnitId
    join Product          p   on su.ProductId         = p.ProductId
end
