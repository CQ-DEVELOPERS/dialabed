﻿
 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_ReceiptLine_Update
  ///   Filename       : p_Receiving_ReceiptLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter
  ///   Modified Date  : 2017-09-12
  ///   Details        : Added the passing of the WarehouseId to the palletise SP to control which warehouse is controlled by AreaCheck
  /// </newpara>
*/
CREATE procedure p_Receiving_ReceiptLine_Update
(
 @ReceiptLineId        int,
 @StorageUnitBatchId   int = null,
 @Batch                nvarchar(30) = null,
 @OperatorId           int = null,
 @ReceivedQuantity     numeric(13,6) = null,
 @AcceptedWeight       numeric(13,6) = null,
 @DeliveryNoteQuantity numeric(13,6) = null,
 @SampleQuantity       numeric(13,6) = null,
 @AssaySamples         numeric(13,6) = null,
 @RejectQuantity       numeric(13,6) = null,
 @ReasonId             int = null,
 @alternateBatch       bit = 0
)
 
as
begin
	 set nocount on;
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @StatusId            int,
          @AcceptedQuantity    numeric(13,6),
          @RequiredQuantity    numeric(13,6),
          @NewReceiptLineId    int,
          @ReceiptId           int,
          @InboundLineId       int,
          @InboundDocumentId   int,
          @StorageUnitId       int,
          @WarehouseId         int,
          @BatchId             int,
          @OverReceipt         numeric(13,3),
          @Exception           nvarchar(255),
          @OldSUB              int,
          @ProductCategory     char(1),
          @InboundSequence     int,
          @OperatorWarehouseId int,
          @OverReceiptDoc      bit,
          @OLDReceivedQuantity numeric(13,3)
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
  
  if @StorageUnitBatchId in (-1,0)
    set @StorageUnitBatchId = null
  
  if (select dbo.ufn_Configuration(260, @WarehouseId)) = 1
    select @OperatorWarehouseId = WarehouseId
      from Operator
     where OperatorId = @OperatorId
  
  select @AcceptedQuantity   = case when dbo.ufn_Configuration(359, @WarehouseId) = 1
                                    then isnull(@ReceivedQuantity,0) - isnull(rl.RejectQuantity,0)
                                    else isnull(@ReceivedQuantity,0)
                                    end,
         @RequiredQuantity   = rl.RequiredQuantity,
         @ReceiptId          = rl.ReceiptId,
         @InboundLineId      = rl.InboundLineId,
         @InboundDocumentId  = r.InboundDocumentId,
         @StorageUnitBatchId = case when @StorageUnitBatchId is null
                                    then rl.StorageUnitBatchId
                                    else @StorageUnitBatchId
                                    end,
         @OldSUB             = rl.StorageUnitBatchId,
         @WarehouseId        = r.WarehouseId,
         @OverReceiptDoc     = idt.OverReceiveIndicator,
         @OLDReceivedQuantity = rl.ReceivedQuantity
    from Receipt      r (nolock)
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    join ReceiptLine rl (nolock) on r.ReceiptId = rl.ReceiptId
   where rl.ReceiptLineId = @ReceiptLineId
  
  select @StorageUnitId = StorageUnitId,
         @BatchId       = BatchId
    from StorageUnitBatch
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select @OverReceipt = p.OverReceipt,
         @ProductCategory = su.ProductCategory
    from StorageUnit su (nolock)
    join Product      p (nolock) on su.ProductId = p.ProductId
   where su.StorageUnitId = @StorageUnitId
  
  if @OverReceipt is null
    set @OverReceipt = 0
  
  select @InboundSequence = max(InboundSequence)
    from PackType (nolock)
    
  begin transaction
  
  if exists(select 1
              from Receipt (nolock)
             where InboundDocumentId = @InboundDocumentId
               and ReceiptId > @ReceiptId)
  begin
    set @Error = -3
    goto error
  end
  
  if (select StorageUnitId
        from InboundLine (nolock)
       where InboundLineId = @InboundLineId) != @StorageUnitId
  begin
    set @Error = -1
    goto error
  end
  
  if @RequiredQuantity = 0
    select @RequiredQuantity = sum(RequiredQuantity)
      from ReceiptLine
     where InboundLineId = @InboundLineId
  
  if isnull(@OverReceiptDoc,0) != 1
  if ((convert(numeric(13,3),@ReceivedQuantity)/convert(numeric(13,3),@RequiredQuantity))*convert(numeric(13,3),100)) > @OverReceipt + 100
  begin
    set @Error = -1
    goto error
  end
  
  if @ReceivedQuantity > @RequiredQuantity
  begin
    set @Exception = 'Quantity over recipted Received:' + convert(nvarchar(255), @ReceivedQuantity) + ' Req:' + convert(nvarchar(255), @ReceivedQuantity)
    
    exec @Error = p_Exception_insert
     @ReceiptLineId = @ReceiptLineId,
     @Exception     = @Exception,
     @ExceptionCode = 'RLUPD',
     @CreateDate    = @Getdate,
     @OperatorId    = @OperatorId,
     @ExceptionDate = @Getdate
  end
      
  if (select dbo.ufn_Configuration(205,@WarehouseId)) = 1
  begin
    exec @Error = p_Batch_Update
     @BatchId         = @BatchId,
     @CreateDate      = @getdate
    
    if @Error <> 0
      goto error
  end
  
  --if @ProductCategory = 'P'
  --begin
  --  if (select Batch from Batch where BatchId = @BatchId) = 'Default'
  --  begin
  --    select @StatusId = dbo.ufn_StatusId('B','A')
      
  --    select @Batch = replace(replace(replace(convert(nvarchar(20), @GetDate, 120),' ',''),':',''),'-','')
      
  --    exec @Error = p_Batch_Insert
  --     @BatchId         = @BatchId output,
  --     @StatusId        = @StatusId,
  --     @WarehouseId     = @WarehouseId,
  --     @Batch           = @Batch,
  --     @CreateDate      = @getdate
      
  --    if @Error <> 0
  --      goto error
      
  --    exec @Error = p_StorageUnitBatch_Insert
  --     @StorageUnitBatchId = @StorageUnitBatchId output,
  --     @BatchId            = @BatchId,
  --     @StorageUnitId      = @StorageUnitId
      
  --    if @Error <> 0
  --      goto error
  --  end
  --end
  --else
  --begin
  --  if (select dbo.ufn_Configuration(129,@WarehouseId)) = 0
  --    if (select Batch from Batch where BatchId = @BatchId) = 'Default'
  --    begin
  --      set @Error = -2
  --      goto error
  --    end
  --end
  
  if isnull(@ProductCategory,'') != 'P'
  begin
    if (select dbo.ufn_Configuration(129,@WarehouseId)) = 0
      if (select Batch from Batch where BatchId = @BatchId) = 'Default'
      begin
        set @Error = -2
        goto error
      end
  end
  
--  if @newStorageUnitBatchId is null
--  begin
--    select @StatusId = dbo.ufn_StatusId('B','A')
--    
--    exec @Error = p_Batch_Insert
--     @BatchId         = @BatchId output,
--     @StatusId        = @StatusId,
--     @WarehouseId     = @WarehouseId,
--     @Batch           = @Batch,
--     @CreateDate      = @getdate
--    
--    if @Error <> 0
--      goto error
--    
--    exec @Error = p_StorageUnitBatch_Insert
--     @StorageUnitBatchId = @newStorageUnitBatchId output,
--     @BatchId            = @BatchId,
--     @StorageUnitId      = @StorageUnitId
--    
--    if @Error <> 0
--      goto error
--  end
  
  if exists(select top 1 1
              from InboundDocument id
              join Status           s on id.StatusId = s.StatusId
             where s.Type       = 'ID'
               and s.StatusCode = 'N')
  begin
    select @StatusId = dbo.ufn_StatusId('ID','W')
    
    exec @Error = p_InboundDocument_Update
     @InboundDocumentId = @InboundDocumentId,
     @StatusId          = @StatusId,
     @WarehouseId       = @OperatorWarehouseId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_InboundLine_Update
     @InboundDocumentId = @InboundDocumentId,
     @StatusId          = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  insert InterfaceExportHeader
        (PrimaryKey,
         OrderNumber,
         ReceiptId,
         RecordType,
         RecordStatus,
         PrincipalCode)
  select convert(nvarchar(10), id.InboundDocumentId),
         id.OrderNumber,
         r.ReceiptId,
         'POSTARTED',
         'N',
         p.PrincipalCode
    from Receipt               r (nolock)
    join InboundDocument      id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join Principal             p (nolock) on id.PrincipalId = p.PrincipalId
    left
    join InterfaceExportHeader h (nolock) on id.OrderNumber = h.OrderNumber -- Only insert 1 record per Inbound Document
                                         and h.RecordType = 'POSTARTED'
   where r.ReceiptId = @ReceiptId
     and h.InterfaceExportHeaderId is null
  
  select @StatusId = dbo.ufn_StatusId('R','R')
  
  update r
     set LocationId = al.LocationId
    from Receipt       r (nolock),
         Area          a (nolock),
         AreaLocation al (nolock)
   where r.ReceiptId   = @ReceiptId
     and a.WarehouseId = @WarehouseId
     and a.AreaId = al.AreaId
     and a.AreaType  = 'QC'
     and r.LocationId is null
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update r
     set LocationId = al.LocationId
    from Receipt       r (nolock),
         Area          a (nolock),
         AreaLocation al (nolock)
   where r.ReceiptId   = @ReceiptId
     and a.WarehouseId = @WarehouseId
     and a.AreaId = al.AreaId
     and a.AreaCode = 'R'
     and r.LocationId is null
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update subl
     set ActualQuantity = case when subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)) < 0
                               then 0
                               else subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0)  - isnull(rl.SampleQuantity,0))
                               end
    from ReceiptLine                rl
    join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                      and subl.LocationId         = r.LocationId
   where rl.ReceiptLineId = @ReceiptLineId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  select @AcceptedWeight = @AcceptedQuantity * pk.NettWeight
    from ReceiptLine             rl
    join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit             su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product                  p (nolock) on su.ProductId          = p.ProductId
    join Pack                    pk (nolock) on su.StorageUnitId      = pk.StorageUnitId
    join PackType                pt (nolock) on pk.PackTypeId         = pt.PackTypeId
   where rl.ReceiptLineId               = @ReceiptLineId
     and pk.WarehouseId                 = @WarehouseId
     and pt.InboundSequence             = @InboundSequence
     and isnull(su.ProductCategory,'') != 'V'
  
  if @RequiredQuantity > @ReceivedQuantity and @alternateBatch = 1
  begin
    exec @Error = p_ReceiptLine_Update
     @ReceiptLineId        = @ReceiptLineId,
     @StorageUnitBatchId   = @StorageUnitBatchId,
     @StatusId             = @StatusId,
     @OperatorId           = @OperatorId,
     @RequiredQuantity     = @ReceivedQuantity,
     @ReceivedQuantity     = @ReceivedQuantity,
     @AcceptedQuantity     = @AcceptedQuantity,
     @AcceptedWeight       = @AcceptedWeight,
     @DeliveryNoteQuantity = @DeliveryNoteQuantity,
     @SampleQuantity       = @SampleQuantity,
     @AssaySamples         = @AssaySamples,
     @RejectQuantity       = @RejectQuantity
     
    if @Error <> 0
      goto error
    
    select @StatusId = dbo.ufn_StatusId('R','W')
    
    set @RequiredQuantity = @RequiredQuantity - @ReceivedQuantity
    
    exec @Error = p_ReceiptLine_Insert
     @ReceiptLineId        = @NewReceiptLineId output,
     @ReceiptId            = @ReceiptId,
     @InboundLineId        = @InboundLineId,
     @StorageUnitBatchId   = @OldSUB,
     @StatusId             = @StatusId,
     @OperatorId           = @OperatorId,
     @RequiredQuantity     = @RequiredQuantity
    
    if @Error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_ReceiptLine_Update
     @ReceiptLineId        = @ReceiptLineId,
     @StorageUnitBatchId   = @StorageUnitBatchId,
     @StatusId             = @StatusId,
     @OperatorId           = @OperatorId,
     @ReceivedQuantity     = @ReceivedQuantity,
     @AcceptedQuantity     = @AcceptedQuantity,
     @AcceptedWeight       = @AcceptedWeight,
     @DeliveryNoteQuantity = @DeliveryNoteQuantity,
     @SampleQuantity       = @SampleQuantity,
     @AssaySamples         = @AssaySamples,
     @RejectQuantity       = @RejectQuantity
    
    if @Error <> 0
      goto error
  end
  
  update subl
     set ActualQuantity = subl.ActualQuantity + isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)
    from ReceiptLine                rl
    join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                      and subl.LocationId         = r.LocationId
   where rl.ReceiptLineId = @ReceiptLineId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  --if @@rowcount = 0
  --begin
    insert StorageUnitBatchLocation
          (StorageUnitBatchId,
           LocationId,
           ActualQuantity,
           AllocatedQuantity,
           ReservedQuantity)
    select rl.StorageUnitBatchId,
           r.LocationId,
           sum(rl.AcceptedQuantity  - isnull(rl.SampleQuantity,0)),
           0,
           0
      from ReceiptLine                rl
      join Receipt                     r on rl.ReceiptId            = r.ReceiptId
      left
      join StorageUnitBatchLocation subl on r.LocationId          = subl.LocationId
                                        and rl.StorageUnitBatchId = subl.StorageUnitBatchId
     where rl.ReceiptLineId = @ReceiptLineId
       and r.LocationId is not null
       and rl.AcceptedQuantity > 0
       and subl.LocationId is null
    group by rl.StorageUnitBatchId,
             r.LocationId
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
  --end
  
  if (isnull(@OLDReceivedQuantity,0) != isnull(@ReceivedQuantity,0)) 
  begin
    declare @TablePallets as table
    (
     InstructionId int,
     StatusCode    nvarchar(10),
     ReceiptLineId int,
     PalletId      int
    )
    
    declare @palletId int,
            @InstructionId int
    
    insert @TablePallets
          (InstructionId,
           StatusCode,
           ReceiptLineId,
           PalletId)
    select i.InstructionId,
           s.StatusCode,
           i.ReceiptLineId,
           i.PalletId
      from Instruction i (nolock)
      join Status     s (nolock) on i.StatusId = s.StatusId
     where i.ReceiptLineId = @ReceiptLineId
     
     -- Remove pallet and instructions when Quantity zeroed
    
    if exists(select top 1 1 from @TablePallets)
    begin
    
      exec @Error = p_Palletise_Reverse
       @ReceiptId          = @ReceiptId,
       @ReceiptLineId      = @ReceiptLineId
      
      
      if @error <> 0
        goto error
      
      while exists(select top 1 1 from @TablePallets)
      begin
        select top 1 @palletId = PalletId,
                     @InstructionId = InstructionId
          from @TablePallets
        
        delete @TablePallets
         where PalletId = @palletId
           and InstructionId = @InstructionId -- Take this out to remove duplicate scanned pallets???
        
        exec @Error = p_Receiving_Palletise @ReceiptLineId = @receiptLineId, @palletId = @palletId, @WarehouseId = @WarehouseId --add Daniel Schotter: To Control the WarehouseId
        
        if @Error <> 0
          goto error
      end
    end
  end
  update Receipt
     set ReceivingStarted = @GetDate
   where ReceiptId = @ReceiptId
     and ReceivingStarted is null
     
  update ReceiptLine
     set ReceivedDate = @GetDate
   where ReceiptlineId = @ReceiptlineId
     and ReceivedDate is null     
  
  update Receipt
     set DeliveryDate = @GetDate
   where ReceiptId = @ReceiptId
     and DeliveryDate is null
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  if @ReceivedQuantity = 0

   select @StatusId = dbo.ufn_StatusId('R','W')
  
  if @ReceivedQuantity > 0
  
  select @StatusId = dbo.ufn_StatusId('R','R')
  
  exec @Error = p_Receipt_Update
   @ReceiptId   = @ReceiptId,
   @WarehouseId = @OperatorWarehouseId
  
  exec @Error = p_ReceiptLine_Update
   @ReceiptId   = @ReceiptId,
  @ReceiptLineId =  @ReceiptLineId,
   @StatusId = @StatusId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Receipt_Update_Status
   @ReceiptId = @ReceiptId,
   @StatusId  = @StatusId
  
  if @Error <> 0
    goto error
  
  if @ReasonId <> -1
  begin
    exec @Error = p_Credit_Advice_Update
        @ReceiptLineId  = @ReceiptLineId
       ,@RejectQuantity = @RejectQuantity
       ,@ReasonId       = @ReasonId
       ,@Exception      = 'Rejected receiving stock'
  end
  
  if @Error <> 0
    goto error
  
  if isnull(@DeliveryNoteQuantity,0) = 0
  begin
    exec @Error = p_Receiving_DeliveryNoteQuantity_Update
     @ReceiptId = @ReceiptId,
     @InboundLineId = @InboundLineId
    
    if @Error <> 0
      goto error
  end
  commit transaction
  select @Error
  return @Error
  
  error:
--    raiserror 900000 'Error executing p_Receiving_ReceiptLine_Update'
    rollback transaction
    select @Error
    return @Error
end
