﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Menu_Delete
  ///   Filename       : p_Menu_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:57
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Menu table.
  /// </remarks>
  /// <param>
  ///   @MenuId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Menu_Delete
(
 @MenuId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Menu
     where MenuId = @MenuId
  
  select @Error = @@Error
  
  
  return @Error
  
end
