﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Pick_Mixed
    ///   Filename       : p_Mobile_Pick_Mixed.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 11 June 2007 14:15:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    ///   @Type                int,
    ///           0 -- Get Next Tranaction
    ///           1 -- Cancel Pick
    ///           2 -- Pick Transaction
    ///           3 -- Request Replenishment
    ///           4 -- Store Transaction
    ///           5 -- Loation Error
    ///           6 -- Confirm Quantity
    ///           7 -- Tare Pallet
    ///           8 -- Confirm Product
    ///   @OperatorId          int,
    ///   @ReferenceNumber     nvarchar(30),
    ///   @InstructionId       int output,
    ///   @Barcode             nvarchar(30),
    ///   @ConfirmedQuantity   float,
    ///   @TareWeight          numeric(13,3)
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : Daniel Schotter
    ///   Modified Date  : April 2018
    ///   Details        : CQPMO-2806: Allowed for alpha numeric security codes.
    /// </newpara>
*/
CREATE procedure p_Mobile_Pick_Mixed
(
 @Type                int,
 @OperatorId          int,
 @ReferenceNumber     nvarchar(30),
 @InstructionId       int output,
 @Barcode             nvarchar(30),
 @ConfirmedQuantity   float,
 @TareWeight          numeric(13,3) = null
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @WarehouseId        int,
          @PickLocationId     int,
          @ProductCode        nvarchar(30),
          @SKUCode            nvarchar(50),
          @Batch              nvarchar(50),
          @StorageUnitBatchId int,
          @Quantity           float,
          @Weight             float,
          @StatusId           int,
          @InstructionTypeId  int,
          @StoreLocationId    int,
          @PickLocation       nvarchar(15),
          @PickSecurityCode   NVARCHAR(50), --CQPMO-2806: Allowed for alpha numeric security codes.
          @StoreLocation      nvarchar(15),
          @StoreSecurityCode  NVARCHAR(50), --CQPMO-2806: Allowed for alpha numeric security codes.
          @CurrentLine        int,
          @TotalLines         int,
          @DwellTime          int,
          @JobId              int,
          @PalletId           int,
          @StorageUnitId      int,
          @ProductId          int,
          @ProductBarcode     nvarchar(50),
          @PackBarcode        nvarchar(50)

  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(replace(@ReferenceNumber,'J:','')) = 1
    select @JobId           = replace(@ReferenceNumber,'J:',''),
           @ReferenceNumber = null
  
  if isnumeric(replace(@ReferenceNumber,'P:','')) = 1
    select @PalletId        = replace(@ReferenceNumber,'P:',''),
           @ReferenceNumber = null
  
  if @PalletId is not null
    select @JobId = JobId
      from Instruction (nolock)
     where PalletId = @PalletId
  
  exec @DwellTime = p_Mobile_Dwell_Time
   @Type       = 2,
   @OperatorId = @OperatorId
  
  if @Type = 0 -- Get Next Tranaction
  begin
    begin transaction
    
    -- Notes Operator Group must be setup for to Area or in MovementSequence table.
    exec @Error = p_Mobile_Next_Job
     @OperatorId          = @OperatorId,
     @JobId               = @JobId output,
     @InstructionTypeCode = 'PM', -- Pick Mixed
     @ReferenceNumber     = @ReferenceNumber
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Mobile_Next_Line
     @JobId         = @JobId,
     @InstructionId = @InstructionId output
    
    if @Error <> 0
      goto error
    
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'S'
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @operatorId    = @operatorId,
     @StartDate     = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    commit transaction
  end
  
  select @PalletId           = PalletId,
         @WarehouseId        = WarehouseId,
         @StorageUnitBatchId = StorageUnitBatchId,
         @Quantity           = Quantity,
         @InstructionTypeId  = InstructionTypeId,
         @JobId              = JobId
    from Instruction
   where InstructionId = @InstructionId
  
  exec @Error = p_Mobile_Pallet_Information
   @PalletId       = @PalletId,
   @InstructionId  = @InstructionId,
   @ProductCode    = @ProductCode output,
   @SKUCode        = @SKUCode     output,
   @Batch          = @Batch       output
  
  if @Error <> 0
  begin
    set @Error = 1
    goto Result
  end
  
  if @Type = 1 -- Cancel Pick
  begin
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'W'
    
    exec @Error = p_Job_Update
     @jobId      = @jobId,
     @StatusId   = @StatusId,
     @operatorId = null
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @operatorId    = null,
     @StartDate     = null
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  else if @Type = 2 -- Pick Transaction
  begin
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_Instruction_Update
     @InstructionId     = @InstructionId,
     @confirmedQuantity = @confirmedQuantity
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId = @InstructionId,
     @Store         = 0
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  else if @Type = 3 -- Request Replenishment
  begin
    return
  end
  else if @Type = 4 -- Store Transaction
  begin
    begin transaction
    
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId = @InstructionId,
     @Pick          = 0
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'F'
    
    exec @Error = p_Job_Update
     @JobId      = @jobId,
     @StatusId   = @StatusId,
     @operatorId = null
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @EndDate       = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    commit transaction
  end
  else if @Type = 5 -- Loation Error
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId       = @InstructionId,
     @Store               = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec p_Pick_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @PickLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity
    
    exec @Error = p_Instruction_Update
     @InstructionId      = @InstructionId,
     @PickLocationId     = @PickLocationId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId,
     @Store               = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
--    exec @Error = p_Mobile_Exception_Stock_Take
--     @OperatorId          = @OperatorId,
--     @StorageUnitBatchId  = 1,
--     @PickLocationId      = @PickLocationId,
--     @Quantity            = @Quantity
--    
--    if @Error <> 0
--    begin
--      set @Error = 1
--      goto Result
--    end
    
    exec @Error = p_Mobile_Pallet_Information
     @PalletId       = @PalletId,
     @InstructionId  = @InstructionId,
     @ProductCode    = @ProductCode output,
     @SKUCode        = @SKUCode     output,
     @Batch          = @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 6 -- Confirm Quantity
  begin
    exec @Error = p_Mobile_Pallet_Information
     @PalletId       = @PalletId,
     @InstructionId  = @InstructionId,
     @ProductCode    = @ProductCode output,
     @SKUCode        = @SKUCode     output,
     @Batch          = @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    if @ConfirmedQuantity > @Quantity
    begin
      set @Error = 7
      goto Result
    end
    else if @ConfirmedQuantity = @Quantity
    begin
      set @Error = 0
      goto Result
    end
    else
    begin
      exec @Error = p_Instruction_Update
       @InstructionId       = @InstructionId,
       @ConfirmedQuantity   = @ConfirmedQuantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      set @StatusId = dbo.ufn_StatusId('I','W')
      
      set @Quantity = @Quantity - @ConfirmedQuantity
      
      exec @Error = p_Instruction_Insert
       @InstructionId       = @InstructionId output,
       @InstructionTypeId   = @InstructionTypeId,
       @StorageUnitBatchId  = @StorageUnitBatchId,
       @WarehouseId         = @WarehouseId,
       @StatusId            = @StatusId,
       @JobId               = @JobId,
       @OperatorId          = @OperatorId,
       @PickLocationId      = @PickLocationId,
       @StoreLocationId     = @StoreLocationId,
       @InstructionRefId    = null,
       @Quantity            = @Quantity,
       @ConfirmedQuantity   = 0,
       @Weight              = @Weight,
       @ConfirmedWeight     = 0,
       @PalletId            = @PalletId,
       @CreateDate          = @GetDate
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      if (select dbo.ufn_Configuration(21, @WarehouseId)) = 1
      begin
        exec p_Location_Get
         @WarehouseId        = @WarehouseId,
         @LocationId         = @StoreLocationId output,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @Quantity           = @Quantity
        
        exec @Error = p_Instruction_Update
         @InstructionId      = @InstructionId,
         @StoreLocationId    = @StoreLocationId
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
        
        exec @Error = p_StorageUnitBatchLocation_Reserve
         @InstructionId       = @InstructionId,
         @Pick                = 0 -- false
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
      end
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 7 -- Tare Pallet
  begin
    exec p_Job_Update
     @JobId      = @JobId,
     @TareWeight = @TareWeight
    
    set @Error = 0
  end
  else if @Type = 8 -- Confirm Product
  begin
    if @Barcode is null
    begin
      set @Error = 3
      set @Errormsg = 'Incorrect barcode'
      goto result
    end
    
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch (nolock)
     where StorageUnitBatchId = @StorageUnitBatchId
    
    select @ProductId = ProductId
      from StorageUnit (nolock)
     where StorageUnitId = @StorageUnitId
    
    select @ProductBarcode = Barcode
      from Product (nolock)
     where ProductId = @ProductId
    
    if @ProductBarcode != @barcode
    begin
      select @PackBarcode = Barcode
        from Pack
       where StorageUnitId = @StorageUnitId
         and Barcode = @barcode
      
      if @PackBarcode is null
      begin
        set @Error = 3
        set @Errormsg = 'Incorrect barcode'
        goto result
      end
    end
  end
  
  goto result
  
  error:
    rollback transaction
  
  result:
    select @CurrentLine = Count(1)
      from Instruction i (nolock)
      join Status      s (nolock) on i.StatusId = s.StatusId
     where i.JobId = @JobId
       and s.Type = 'I'
       and s.StatusCode = 'F'
    
    if @CurrentLine = 0
      set @CurrentLine = 1
    
    select @TotalLines = Count(1)
      from Instruction (nolock)
     where JobId = @JobId
    
    select @InstructionId     = i.InstructionId,
           @PickLocation      = pick.Location,
           @PickSecurityCode  = pick.SecurityCode,
           @StoreLocation     = store.Location,
           @StoreSecurityCode = store.SecurityCode,
           @Quantity          = i.Quantity,
           @PalletId          = i.PalletId
      from Instruction     i (nolock)
      left outer
      join Location     pick (nolock) on i.PickLocationId   = pick.LocationId
      left outer
      join Location    store (nolock) on i.StoreLocationId = store.LocationId
    where InstructionId = @InstructionId
    
    select @Error             as 'Result',
           @DwellTime         as 'DwellTime',
           @JobId             as 'JobId',
           @PalletId          as 'PalletId',
           @InstructionId     as 'InstructionId',
           @ProductCode       as 'ProductCode',
           @SKUCode           as 'SKUCode',
           @Batch             as 'Batch',
           @PickLocation      as 'PickLocation',
           @PickSecurityCode  as 'PickSecurityCode',
           @StoreLocation     as 'StoreLocation',
           @StoreSecurityCode as 'StoreSecurityCode',
           @Quantity          as 'Quantity',
           @CurrentLine       as 'CurrentLine',
           @TotalLines        as 'TotalLines'
    
    return @Error
end
