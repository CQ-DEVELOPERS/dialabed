﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Unlink
  ///   Filename       : p_Wave_Planning_Unlink.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Unlink
(
 @OutboundShipmentId int,
 @IssueId            int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @WaveId            int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  begin transaction
  
  if @OutboundShipmentId is not null
  begin
    select @WaveId = WaveId
      from OutboundShipment (nolock)
     where OutboundShipmentId = @OutboundShipmentId
    
    update OutboundShipment
       set WaveId  = null
     where OutboundShipmentId = @OutboundShipmentId
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
    
    update i
       set WaveId = null
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
     where osi.OutboundShipmentId = @outboundShipmentId
    
    select @error = @@error
    
    if @Error <> 0
      goto error
  end
  else
  begin
    if @WaveId is null
      select @WaveId = WaveId
        from Issue (nolock)
       where IssueId = @IssueId
    
    update Issue
       set WaveId = null
      where IssueId = @IssueId
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
    
    if not exists(select top 1 1
                from Wave w
                join Status s on s.StatusId = w.StatusId
                             and s.StatusCode = 'W'
               where w.WaveId = @waveId)
    begin
      set @Error = -1
      goto Error
    end
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
