﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoadJob_Search
  ///   Filename       : p_IntransitLoadJob_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:35
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the IntransitLoadJob table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadJobId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   IntransitLoadJob.IntransitLoadJobId,
  ///   IntransitLoadJob.IntransitLoadId,
  ///   IntransitLoadJob.StatusId,
  ///   IntransitLoadJob.JobId,
  ///   IntransitLoadJob.LoadedById,
  ///   IntransitLoadJob.UnLoadedById,
  ///   IntransitLoadJob.CreateDate,
  ///   IntransitLoadJob.ReceivedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoadJob_Search
(
 @IntransitLoadJobId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @IntransitLoadJobId = '-1'
    set @IntransitLoadJobId = null;
  
 
  select
         IntransitLoadJob.IntransitLoadJobId
        ,IntransitLoadJob.IntransitLoadId
        ,IntransitLoadJob.StatusId
        ,IntransitLoadJob.JobId
        ,IntransitLoadJob.LoadedById
        ,IntransitLoadJob.UnLoadedById
        ,IntransitLoadJob.CreateDate
        ,IntransitLoadJob.ReceivedDate
    from IntransitLoadJob
   where isnull(IntransitLoadJob.IntransitLoadJobId,'0')  = isnull(@IntransitLoadJobId, isnull(IntransitLoadJob.IntransitLoadJobId,'0'))
  
end
