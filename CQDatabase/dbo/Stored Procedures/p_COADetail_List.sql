﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COADetail_List
  ///   Filename       : p_COADetail_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:23
  /// </summary>
  /// <remarks>
  ///   Selects rows from the COADetail table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   COADetail.COADetailId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COADetail_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as COADetailId
        ,null as 'COADetail'
  union
  select
         COADetail.COADetailId
        ,COADetail.COADetailId as 'COADetail'
    from COADetail
  
end
