﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Select
  ///   Filename       : p_SerialNumber_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2013 14:12:41
  /// </summary>
  /// <remarks>
  ///   Selects rows from the SerialNumber table.
  /// </remarks>
  /// <param>
  ///   @SerialNumberId int = null 
  /// </param>
  /// <returns>
  ///   SerialNumber.SerialNumberId,
  ///   SerialNumber.StorageUnitId,
  ///   SerialNumber.LocationId,
  ///   SerialNumber.ReceiptId,
  ///   SerialNumber.StoreInstructionId,
  ///   SerialNumber.PickInstructionId,
  ///   SerialNumber.SerialNumber,
  ///   SerialNumber.ReceivedDate,
  ///   SerialNumber.DespatchDate,
  ///   SerialNumber.ReferenceNumber,
  ///   SerialNumber.BatchId,
  ///   SerialNumber.IssueId,
  ///   SerialNumber.IssueLineId,
  ///   SerialNumber.ReceiptLineId,
  ///   SerialNumber.StoreJobId,
  ///   SerialNumber.PickJobId,
  ///   SerialNumber.Remarks,
  ///   SerialNumber.StatusId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Select
(
 @SerialNumberId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         SerialNumber.SerialNumberId
        ,SerialNumber.StorageUnitId
        ,SerialNumber.LocationId
        ,SerialNumber.ReceiptId
        ,SerialNumber.StoreInstructionId
        ,SerialNumber.PickInstructionId
        ,SerialNumber.SerialNumber
        ,SerialNumber.ReceivedDate
        ,SerialNumber.DespatchDate
        ,SerialNumber.ReferenceNumber
        ,SerialNumber.BatchId
        ,SerialNumber.IssueId
        ,SerialNumber.IssueLineId
        ,SerialNumber.ReceiptLineId
        ,SerialNumber.StoreJobId
        ,SerialNumber.PickJobId
        ,SerialNumber.Remarks
        ,SerialNumber.StatusId
    from SerialNumber
   where isnull(SerialNumber.SerialNumberId,'0')  = isnull(@SerialNumberId, isnull(SerialNumber.SerialNumberId,'0'))
  order by SerialNumber
  
end
