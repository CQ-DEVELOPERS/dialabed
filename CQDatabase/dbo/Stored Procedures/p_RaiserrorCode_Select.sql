﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RaiserrorCode_Select
  ///   Filename       : p_RaiserrorCode_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:36
  /// </summary>
  /// <remarks>
  ///   Selects rows from the RaiserrorCode table.
  /// </remarks>
  /// <param>
  ///   @RaiserrorCodeId int = null 
  /// </param>
  /// <returns>
  ///   RaiserrorCode.RaiserrorCodeId,
  ///   RaiserrorCode.RaiserrorCode,
  ///   RaiserrorCode.RaiserrorMessage,
  ///   RaiserrorCode.StoredProcedure 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RaiserrorCode_Select
(
 @RaiserrorCodeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         RaiserrorCode.RaiserrorCodeId
        ,RaiserrorCode.RaiserrorCode
        ,RaiserrorCode.RaiserrorMessage
        ,RaiserrorCode.StoredProcedure
    from RaiserrorCode
   where isnull(RaiserrorCode.RaiserrorCodeId,'0')  = isnull(@RaiserrorCodeId, isnull(RaiserrorCode.RaiserrorCodeId,'0'))
  
end
