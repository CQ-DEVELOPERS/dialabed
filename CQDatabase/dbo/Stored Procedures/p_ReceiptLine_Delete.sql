﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLine_Delete
  ///   Filename       : p_ReceiptLine_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2013 07:02:52
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ReceiptLine table.
  /// </remarks>
  /// <param>
  ///   @ReceiptLineId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLine_Delete
(
 @ReceiptLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ReceiptLine
     where ReceiptLineId = @ReceiptLineId
  
  select @Error = @@Error
  
  
  return @Error
  
end
