﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Daily_Recon
  ///   Filename       : p_Report_Daily_Recon.sql
  ///   Create By      : Karen
  ///   Date Created   : February 2016
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Report_Daily_Recon]
(
 @WarehouseId	int,
 @WarehouseCode nvarchar(30) = null,
 @StorageUnitId int = null,
 @VarianceOnly	nvarchar(1) = 'A'
)
 
as
begin
	 set nocount on;

  declare @ComparisonDate datetime,
		  @GetDate		  datetime
  
  declare @TableResult as table
  (
   WarehouseId			int,
   WarehouseCode		nvarchar(30),
   StorageUnitId		int,
   ProductCode          nvarchar(30),
   Product				nvarchar(50),
   WMSTotal				float,
   ERPTotal				float,
   WMSSOH				float,
   Variance				float,
   RecUnconfirmed		float,
   RecConfirmed			float,
   HKStockPosted        float,
   HKStockNotPosted		float,
   CHKPosted			float,
   CHKNotPosted			float
  )
  
 
	if @WarehouseCode = ''
		set @WarehouseCode = null
	
	if @StorageUnitId = -1
		set @StorageUnitId = null
	
	select i.Quantity,
		   sub.StorageUnitId,
		   s.StatusCode
	  into #tempPosted
      from Instruction i (nolock)
      join Job J (nolock) on i.JobId = j.JobId
      join Status s on j.StatusId = s.StatusId
      join StorageUnitBatch sub on sub.StorageUnitBatchId = i.StorageUnitBatchId
     where s.StatusCode in ('CK','CD')
       and i.WarehouseId = @WarehouseId

	select Quantity,
		   RecordStatus,
		   ProductCode		 	
	  into #tempHKStock
	  from InterfaceExportStockAdjustment sa
      left
      join InterfaceMessage im on sa.InterfaceExportStockAdjustmentId = im.InterfaceId
                              and im.InterfaceTable = 'InterfaceExportStockAdjustment'
      left
      join Operator o on sa.CreatedBy = o.OperatorId
     where sa.Additional1 in (select WarehouseCode from Warehouse where ParentWarehouseId = @WarehouseId)
      --from InterfaceExportStockAdjustment sa
      --left
      --join InterfaceMessage im on sa.InterfaceExportStockAdjustmentId = im.InterfaceId
						--      and im.InterfaceTable = 'InterfaceExportStockAdjustment'
      --left
      --join Operator o on sa.CreatedBy = o.OperatorId
					      
						      	 
	select distinct rl.RequiredQuantity as Quantity,
		   sub.StorageUnitId,
	       l.locationid
	  into #tempRecUnconfirmed 
	   from ReceiptLine		   rl (nolock) --on i.ReceiptLineId = rl.ReceiptLineId
	  join Receipt				r (nolock) on rl.ReceiptId = r.ReceiptId 
	  join Location				l (nolock) on r.LocationId = l.LocationId
	  join AreaLocation		   al (nolock) on l.LocationId = al.LocationId
	  join Area					a (nolock) on al.AreaId = a.AreaId
	  join StorageUnitBatch	  sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
	  join Status s on s.StatusId = rl.StatusId						      
	 where a.AreaCode = 'R'
	   and s.StatusCode in ('R')
	   and sub.StorageUnitId = isnull(@StorageUnitId,sub.StorageUnitId)
	   and a.WarehouseId = @WarehouseId
   
	select distinct rl.AcceptedQuantity as Quantity,
	       sub.StorageUnitId,
	       l.locationid
	  into #tempRecConfirmed 
	  --from Instruction			i (nolock) 
	  from ReceiptLine		   rl (nolock) --on i.ReceiptLineId = rl.ReceiptLineId
	  join Receipt				r (nolock) on rl.ReceiptId = r.ReceiptId 
	  join Location				l (nolock) on r.LocationId = l.LocationId
	  join AreaLocation		   al (nolock) on l.LocationId = al.LocationId
	  join Area					a (nolock) on al.AreaId = a.AreaId
	  join StorageUnitBatch   sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
	  join Status s on s.StatusId = rl.StatusId						      
	 where a.AreaCode = 'R'
	   and s.StatusCode in ('RC')
	   and sub.StorageUnitId = isnull(@StorageUnitId,sub.StorageUnitId)
	   and a.WarehouseId = @WarehouseId
   
			   
	select a.WarehouseId,
		   a.WarehouseCode,
		   a.AreaId,
		   a.Area,
		   a.areacode,
		   l.LocationId,
		   sub.StorageUnitBatchId,
		   su.StorageUnitId,
		   subl.ActualQuantity,
		   subl.AllocatedQuantity,
		   subl.ReservedQuantity,
		   l.StockTakeInd,
		   a.StockOnHand
	  into #tempsoh
	  from StorageUnitBatch          sub (nolock)
	  join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
	  join Product                     p (nolock) on su.ProductId           = p.ProductId
	  join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
	  join Location                    l (nolock) on subl.LocationId        = l.LocationId
	  join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
	  join Area                        a (nolock) on al.AreaId              = a.AreaId
	 where a.WarehouseCode = isnull(@WarehouseCode,a.WarehouseCode)
	   and su.StorageUnitId = isnull(@StorageUnitId,su.StorageUnitId)
	   and a.WarehouseId = @WarehouseId
	
	
	select @GetDate = dbo.ufn_Getdate()
   
	--select @WarehouseId = (select WarehouseId 
	--						 from Warehouse 
	--					    where WarehouseCode = @WarehouseCode)
   
	set @ComparisonDate = (select top 1 ComparisonDate 
							 from HousekeepingCompare
						    order 
						       by ComparisonDate desc)
  
	insert @TableResult
		  (WarehouseId,
		   WarehouseCode,
		   StorageUnitId,
		   ProductCode,
		   Product)
	select WarehouseId,
		   WarehouseCode,
		   su.StorageUnitId,
		   p.ProductCode,
		   p.Product
	  from StorageUnit			su (nolock) --on hc.StorageUnitId = su.StorageUnitId
	  join Product				 p (nolock) on p.ProductId = su.ProductId
	  join StorageUnitArea     sua (nolock) on su.StorageUnitId = sua.StorageUnitId
	  join Area					 a (nolock) on sua.AreaId = a.AreaId
	 where su.StorageUnitId = isnull(@StorageUnitId,su.StorageUnitId)
	   and WarehouseCode = isnull(@WarehouseCode,WarehouseCode)
	   and WarehouseId = @WarehouseId
	   
 --   insert @TableResult
	--	  (WarehouseId,
	--	   WarehouseCode,
	--	   StorageUnitId,
	--	   ProductCode,
	--	   Product)
	--select WarehouseId,
	--	   WarehouseCode,
	--	   hc.StorageUnitId,
	--	   ProductCode,
	--	   Product
	--  from HousekeepingCompare	hc (nolock)
	--  join StorageUnit			su (nolock) on hc.StorageUnitId = su.StorageUnitId
	--  join Product				 p (nolock) on p.ProductId = su.ProductId
	-- where hc.StorageUnitId = isnull(@StorageUnitId,hc.StorageUnitId)
	--   and hc.WarehouseCode = isnull(@WarehouseCode,hc.WarehouseCode)
	--   and hc.WarehouseId = @WarehouseId
  
    update tr
	   set WMSTotal = (select sum(hc.WMSQuantity) 
					     from HousekeepingCompare hc (nolock)  
					    where hc.WarehouseId = tr.WarehouseId
					      and hc.StorageUnitId = tr.StorageUnitId
					  	  and hc.ComparisonDate = @ComparisonDate
						  and tr.StorageUnitId  = hc.StorageUnitId)						
      from @TableResult     tr  
   
	update tr
	   set ERPTotal = (select sum(hc.HostQuantity) 
						 from HousekeepingCompare hc (nolock)  
					    where hc.WarehouseId = tr.WarehouseId
					      and hc.StorageUnitId = tr.StorageUnitId
					      and hc.ComparisonDate = @ComparisonDate
					      and tr.StorageUnitId  = hc.StorageUnitId)
	  from @TableResult     tr
    
	update tr
	   set WMSSOH = (select sum(soh.ActualQuantity) 
					   from #tempsoh   soh  
					  where soh.WarehouseCode = tr.WarehouseCode
					    and soh.StorageUnitId = tr.StorageUnitId
					    and soh.Area != 'Despatch')
	  from @TableResult     tr    
    
	update tr
	   set Variance = isnull(tr.WMSTotal,0) - isnull(ERPTotal,0)
	  from @TableResult     tr    
  
	update tr
	   set RecUnconfirmed = (select SUM(ru.quantity) 
						       from #tempRecUnconfirmed ru 					      
						      where tr.StorageUnitId =  ru.StorageUnitId)
	from @TableResult     tr   
    
	update tr
	   set RecConfirmed = (select SUM(rc.ActualQuantity) 
						     from #tempsoh rc					      
						    where rc.StorageUnitId =  tr.StorageUnitId
						      and Area = 'Receiving')
	  from @TableResult     tr   
    
    update tr
	   set RecConfirmed = isnull(RecConfirmed,0) - isnull(RecUnconfirmed,0)
	  from @TableResult     tr
	  
	--update tr
	--   set HKStockPosted = (select sum(hks.Quantity) 
	--						     from #tempHKStock hks
	--						    where hks.RecordStatus = 'Y'
	--						      and hks.productcode = tr.productcode)
	--  from @TableResult     tr    
	
    --update tr
	   --set HKStockPosted = (select sum(hc.WMSQuantity) 
				--	     from #tempsoh soh
				--	     where AreaCode not in ('D','OF','R'))						
    --  from @TableResult     tr 
      
      update tr
      set HKStockPosted = (select sum(soh.ActualQuantity) 
					   from #tempsoh   soh  
					  where soh.WarehouseCode = tr.WarehouseCode
					    and soh.StorageUnitId = tr.StorageUnitId
					    and soh.AreaCode not in ('D','JJ'))
	  from @TableResult     tr  
    
	update tr
	   set HKStockNotPosted = (select sum(hks.Quantity) 
							     from #tempHKStock hks
							    where hks.RecordStatus = 'W'
							      and hks.productcode = tr.productcode)
	  from @TableResult     tr    
    
	update tr
	   set CHKNotPosted = (select sum(Quantity) 
							 from #tempPosted ck
							where ck.StatusCode = 'CK'
							  and ck.StorageUnitId = tr.StorageUnitId)
	  from @TableResult     tr 
    
	update tr
	   set CHKPosted = (select sum(Quantity) 
						  from #tempPosted ck
						 where ck.StatusCode = 'CK'
						   and ck.StorageUnitId = tr.StorageUnitId)
	  from @TableResult     tr  
	  
	  --select * from  #tempPosted ck
			--			 where ck.StorageUnitId = 1188

  
	if @VarianceOnly = 'V'
		select distinct 
			   *, 
			   @ComparisonDate as ComparisonDate, 
			   @GetDate as GetDate
		  from @TableResult 
		 where isnull(Variance,0) > 0
	else   
		select distinct 
			   *, 
			   @ComparisonDate as ComparisonDate, 
			   @GetDate as GetDate
		  from @TableResult 
 
 end 
 
 
 
 
