﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Tracked
  ///   Filename       : p_SerialNumber_Tracked.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Mar 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Tracked
(
 @ReceiptLineId int = null,
 @StorageUnitId int = null,
 @StorageUnitBatchId int = null,
 @InstructionId int = null,
 @ExternalCompanyId int = null
)
 
as
begin
  set nocount on;
  
  declare @SerialTracked bit = 0
  
  if @ReceiptLineId is not null
    select @SerialTracked = su.SerialTracked
      from ReceiptLine rl (nolock)
      join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit su (nolock) on sub.StorageUnitId = su.StorageUnitId
     where rl.ReceiptLineId = @ReceiptLineId
  
  if @StorageUnitId is not null
    select @SerialTracked = su.SerialTracked
      from StorageUnit su (nolock)
     where su.StorageUnitId = @StorageUnitId
     
  if @StorageUnitId is not null and @ExternalCompanyId is not null
    select @SerialTracked = suet.SerialTracked
      from StorageUnitExternalCompany suet (nolock)
     where suet.StorageUnitId = @StorageUnitId  
	   and suet.ExternalCompanyId = @ExternalCompanyId 
  
  if @StorageUnitBatchId is not null
    select @SerialTracked = su.SerialTracked
      from StorageUnitBatch sub (nolock)
      join StorageUnit su (nolock) on sub.StorageUnitId = su.StorageUnitId
     where sub.StorageUnitBatchId = @StorageUnitBatchId
  
  if @InstructionId is not null
    select @SerialTracked = su.SerialTracked
      from Instruction i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit su (nolock) on sub.StorageUnitId = su.StorageUnitId
     where i.InstructionId = @InstructionId
  
  if @SerialTracked is null
    set @SerialTracked = 0
  
  select @SerialTracked
  return @SerialTracked
end
