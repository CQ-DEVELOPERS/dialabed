﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMHeader_Delete
  ///   Filename       : p_BOMHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:04
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the BOMHeader table.
  /// </remarks>
  /// <param>
  ///   @BOMHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMHeader_Delete
(
 @BOMHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete BOMHeader
     where BOMHeaderId = @BOMHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
