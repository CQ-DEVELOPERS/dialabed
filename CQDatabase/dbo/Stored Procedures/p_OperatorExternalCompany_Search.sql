﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorExternalCompany_Search
  ///   Filename       : p_OperatorExternalCompany_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:17
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorExternalCompany.OperatorId,
  ///   OperatorExternalCompany.ExternalCompanyId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorExternalCompany_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OperatorExternalCompany.OperatorId
        ,OperatorExternalCompany.ExternalCompanyId
    from OperatorExternalCompany
  
end
