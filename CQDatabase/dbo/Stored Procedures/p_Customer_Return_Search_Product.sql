﻿
CREATE procedure p_Customer_Return_Search_Product
(
 @InboundDocumentId int,
 @ProductCode       nvarchar(30) = null,
 @Product           nvarchar(50) = null,
 @Barcode           nvarchar(50) = null,
 @SKUCode           nvarchar(50) = null,
 @SKU               nvarchar(50) = null
)
 
as
begin
  set nocount on;
  
  declare @OutboundDocumentId int,
          @ReferenceNumber    nvarchar(30)
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @Barcode = '-1'
    set @Barcode = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
  select @ReferenceNumber = ReferenceNumber
    from InboundDocument  id (nolock)
    join InterfaceInvoice ii (nolock) on id.ReferenceNumber = ii.InvoiceNumber
   where id.InboundDocumentId = @InboundDocumentId
  
  if @ReferenceNumber is null
  begin
    select su.StorageUnitId,
           p.ProductCode,
           p.Product,
           p.Barcode,
           sku.SKUCode,
           sku.SKU
      from StorageUnit  su (nolock)
      join SKU         sku (nolock) on su.SKUId = sku.SKUId
      join Product       p (nolock) on su.ProductId = p.ProductId
     where isnull(p.ProductCode,'%')  like '%' + isnull(@ProductCode, isnull(p.ProductCode,'%')) + '%'
       and isnull(p.Product,'%')  like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
       and isnull(sku.SKUCode,'%')  like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
       and isnull(sku.SKU,'%')  like '%' + isnull(@SKU, isnull(sku.SKU,'%')) + '%'
       and isnull(p.Barcode, '%') like '%' + isnull(@Barcode, isnull(p.Barcode,'%')) + '%'
  end
  else
  begin
    select @OutboundDocumentId = OutboundDocumentId
      from OutboundDocument (nolock)
     where ReferenceNumber = @ReferenceNumber
    
    select distinct su.StorageUnitId,
           p.ProductCode,
           p.Product,
           p.Barcode,
           sku.SKUCode,
           sku.SKU
      from OutboundLine ol (nolock)
      join StorageUnit  su (nolock) on ol.StorageUnitId = su.StorageUnitId
      join SKU         sku (nolock) on su.SKUId = sku.SKUId
      join Product       p (nolock) on su.ProductId = p.ProductId
     where ol.OutboundDocumentId = @OutboundDocumentId
       and isnull(p.ProductCode,'%')  like '%' + isnull(@ProductCode, isnull(p.ProductCode,'%')) + '%'
       and isnull(p.Product,'%')  like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
       and isnull(sku.SKUCode,'%')  like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
       and isnull(sku.SKU,'%')  like '%' + isnull(@SKU, isnull(sku.SKU,'%')) + '%'
       and isnull(p.Barcode, '%') like '%' + isnull(@Barcode, isnull(p.Barcode,'%')) + '%'
  end
end


