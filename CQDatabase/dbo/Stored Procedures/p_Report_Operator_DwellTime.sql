﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_DwellTime
  ///   Filename       : p_Report_Operator_DwellTime.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 13 August 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Operator_DwellTime
(
 @WarehouseId		int,
 @FromDate         datetime,
 @ToDate           datetime,
 @ElapsedTime      int
)
 
as
begin
	 set nocount on;
  
  select R.ReasonCode,
         R.Reason,
         O.Operator,
         E.CreateDate,
         E.ExceptionDate,
         E.ExceptionCode,
         E.Exception,
         DateDiff(ss, E.ExceptionDate, E.CreateDate) as 'TotalSeconds'
    from [Exception] AS E INNER JOIN
         Operator    AS O ON E.OperatorId = O.OperatorId INNER JOIN
         Reason      AS R ON E.ReasonId = R.ReasonId
 where (E.ExceptionCode = 'DWELL')
   and (E.CreateDate between @FromDate and @ToDate)
   and DateDiff(ss, E.ExceptionDate, E.CreateDate) > isnull(@ElapsedTime,0)
	and O.WarehouseId = Isnull(@WarehouseId,O.WarehouseId)
end

