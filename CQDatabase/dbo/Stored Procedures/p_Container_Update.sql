﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Update
  ///   Filename       : p_Container_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 11:46:27
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Container table.
  /// </remarks>
  /// <param>
  ///   @ContainerId int = null,
  ///   @PalletId int = null,
  ///   @JobId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @StorageUnitBatchId int = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Update
(
 @ContainerId int = null,
 @PalletId int = null,
 @JobId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @StorageUnitBatchId int = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
  if @ContainerId = '-1'
    set @ContainerId = null;
  
	 declare @Error int
 
  update Container
     set PalletId = isnull(@PalletId, PalletId),
         JobId = isnull(@JobId, JobId),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         PickLocationId = isnull(@PickLocationId, PickLocationId),
         StoreLocationId = isnull(@StoreLocationId, StoreLocationId),
         Quantity = isnull(@Quantity, Quantity) 
   where ContainerId = @ContainerId
  
  select @Error = @@Error
  
  
  return @Error
  
end
