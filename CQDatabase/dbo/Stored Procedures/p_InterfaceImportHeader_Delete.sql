﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportHeader_Delete
  ///   Filename       : p_InterfaceImportHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:27
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceImportHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportHeader_Delete
(
 @InterfaceImportHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceImportHeader
     where InterfaceImportHeaderId = @InterfaceImportHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
