﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentType_Search
  ///   Filename       : p_OutboundDocumentType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jan 2014 09:16:07
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentTypeId int = null output,
  ///   @OutboundDocumentTypeCode nvarchar(20) = null,
  ///   @OutboundDocumentType nvarchar(60) = null,
  ///   @PriorityId int = null,
  ///   @CheckingLane int = null,
  ///   @DespatchBay int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OutboundDocumentType.OutboundDocumentTypeId,
  ///   OutboundDocumentType.OutboundDocumentTypeCode,
  ///   OutboundDocumentType.OutboundDocumentType,
  ///   OutboundDocumentType.PriorityId,
  ///   Priority.Priority,
  ///   OutboundDocumentType.AlternatePallet,
  ///   OutboundDocumentType.SubstitutePallet,
  ///   OutboundDocumentType.AutoRelease,
  ///   OutboundDocumentType.AddToShipment,
  ///   OutboundDocumentType.MultipleOnShipment,
  ///   OutboundDocumentType.LIFO,
  ///   OutboundDocumentType.MinimumShelfLife,
  ///   OutboundDocumentType.AreaType,
  ///   OutboundDocumentType.AutoSendup,
  ///   OutboundDocumentType.CheckingLane,
  ///   Location.Location,
  ///   OutboundDocumentType.DespatchBay,
  ///   Location.Location,
  ///   OutboundDocumentType.PlanningComplete,
  ///   OutboundDocumentType.AutoInvoice,
  ///   OutboundDocumentType.Backorder,
  ///   OutboundDocumentType.AutoCheck,
  ///   OutboundDocumentType.ReserveBatch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentType_Search
(
 @OutboundDocumentTypeId int = null output,
 @OutboundDocumentTypeCode nvarchar(20) = null,
 @OutboundDocumentType nvarchar(60) = null,
 @PriorityId int = null,
 @CheckingLane int = null,
 @DespatchBay int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @OutboundDocumentTypeId = '-1'
    set @OutboundDocumentTypeId = null;
  
  if @OutboundDocumentTypeCode = '-1'
    set @OutboundDocumentTypeCode = null;
  
  if @OutboundDocumentType = '-1'
    set @OutboundDocumentType = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @CheckingLane = '-1'
    set @CheckingLane = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
 
  select
         OutboundDocumentType.OutboundDocumentTypeId
        ,OutboundDocumentType.OutboundDocumentTypeCode
        ,OutboundDocumentType.OutboundDocumentType
        ,OutboundDocumentType.PriorityId
         ,PriorityPriorityId.Priority as 'Priority'
        ,OutboundDocumentType.AlternatePallet
        ,OutboundDocumentType.SubstitutePallet
        ,OutboundDocumentType.AutoRelease
        ,OutboundDocumentType.AddToShipment
        ,OutboundDocumentType.MultipleOnShipment
        ,OutboundDocumentType.LIFO
        ,OutboundDocumentType.MinimumShelfLife
        ,OutboundDocumentType.AreaType
        ,OutboundDocumentType.AutoSendup
        ,OutboundDocumentType.CheckingLane
         ,LocationCheckingLane.Location as 'LocationCheckingLane'
        ,OutboundDocumentType.DespatchBay
         ,LocationDespatchBay.Location as 'LocationDespatchBay'
        ,OutboundDocumentType.PlanningComplete
        ,OutboundDocumentType.AutoInvoice
        ,OutboundDocumentType.Backorder
        ,OutboundDocumentType.AutoCheck
        ,OutboundDocumentType.ReserveBatch
    from OutboundDocumentType
    left
    join Priority PriorityPriorityId on PriorityPriorityId.PriorityId = OutboundDocumentType.PriorityId
    left
    join Location LocationCheckingLane on LocationCheckingLane.LocationId = OutboundDocumentType.CheckingLane
    left
    join Location LocationDespatchBay on LocationDespatchBay.LocationId = OutboundDocumentType.DespatchBay
   where isnull(OutboundDocumentType.OutboundDocumentTypeId,'0')  = isnull(@OutboundDocumentTypeId, isnull(OutboundDocumentType.OutboundDocumentTypeId,'0'))
     and isnull(OutboundDocumentType.OutboundDocumentTypeCode,'%')  like '%' + isnull(@OutboundDocumentTypeCode, isnull(OutboundDocumentType.OutboundDocumentTypeCode,'%')) + '%'
     and isnull(OutboundDocumentType.OutboundDocumentType,'%')  like '%' + isnull(@OutboundDocumentType, isnull(OutboundDocumentType.OutboundDocumentType,'%')) + '%'
     and isnull(OutboundDocumentType.PriorityId,'0')  = isnull(@PriorityId, isnull(OutboundDocumentType.PriorityId,'0'))
     and isnull(OutboundDocumentType.CheckingLane,'0')  = isnull(@CheckingLane, isnull(OutboundDocumentType.CheckingLane,'0'))
     and isnull(OutboundDocumentType.DespatchBay,'0')  = isnull(@DespatchBay, isnull(OutboundDocumentType.DespatchBay,'0'))
  order by OutboundDocumentTypeCode
  
end
