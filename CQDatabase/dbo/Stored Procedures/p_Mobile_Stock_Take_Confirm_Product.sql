﻿--IF OBJECT_ID('dbo.p_Mobile_Stock_Take_Confirm_Product') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Mobile_Stock_Take_Confirm_Product
--    IF OBJECT_ID('dbo.p_Mobile_Stock_Take_Confirm_Product') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Mobile_Stock_Take_Confirm_Product >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Mobile_Stock_Take_Confirm_Product >>>'
--END
--go

/*
 /// <summary>
 ///   Procedure Name : p_Mobile_Stock_Take_Confirm_Product
 ///   Filename       : p_Mobile_Stock_Take_Confirm_Product.sql
 ///   Create By      : Grant Schultz
 ///   Date Created   : 20 Nov 2007
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
 /// <newpara>
 ///   Modified by    : 
 ///   Modified Date  : 
 ///   Details        : 
 /// </newpara>
*/
CREATE procedure p_Mobile_Stock_Take_Confirm_Product
(
@barcode            nvarchar(50),
@batch              nvarchar(50),
@locationId         int = null,
@type               nvarchar(10) = null,
@principalId		int = null
)
 
as
begin
	 set nocount on;

 declare @BatchId            int,
         @StorageUnitId      int,
         @StorageUnitBatchId int,
         @ProductId          int,
         @CountSUId          int

 if @batch = '-1' or @batch is null
   set @batch = 'Default'

 set @CountSUId = 0

 if (select max(AreaCode)
      from AreaLocation al (nolock)
      join Area          a (nolock) on al.AreaId    = a.AreaId
     where al.LocationId = @locationId) = 'PK'
 begin
   select @StorageUnitId = su.StorageUnitId
     from Product               p (nolock)
     join StorageUnit          su (nolock) on p.ProductId = su.ProductId
     join StorageUnitLocation sul (nolock) on su.StorageUnitId = sul.StorageUnitId
                                          and sul.LocationId = @locationId
     left join SKU            sku (nolock) on sku.SKUId = su.SKUId
    where isnull(p.ProductType,'') != 'FPC'
      and (Barcode = @barcode
       or  ProductCode = @barcode
       or  SKUCode = @barcode)
      and p.PrincipalId = isnull(@principalId,p.PrincipalId)
 end

 if @StorageUnitId is null and isnull(@type,'') = 'STK'
 begin
   select @StorageUnitId = su.StorageUnitId,
          @ProductId     = p.ProductId
     from Product      p (nolock)
     join StorageUnit su (nolock) on p.ProductId = su.ProductId
     left join SKU            sku (nolock) on sku.SKUId = su.SKUId
    where isnull(p.ProductType,'') != 'FPC'
      and (Barcode = @barcode
       or  ProductCode = @barcode
       or  SKUCode = @barcode)
      and p.PrincipalId = isnull(@principalId,p.PrincipalId) 

    select @CountSUId = count(1) from StorageUnit (nolock) where ProductId = @ProductId

    if @CountSUId > 1 -- If linked to more than one sku - return error
      set @StorageUnitId = null
 end
 else
 begin
   select @StorageUnitId = su.StorageUnitId
     from Product      p (nolock)
     join StorageUnit su (nolock) on p.ProductId = su.ProductId
     left join SKU            sku (nolock) on sku.SKUId = su.SKUId
    where isnull(p.ProductType,'') != 'FPC'
      and (Barcode = @barcode
       or  ProductCode = @barcode
       or  SKUCode = @barcode)
      and  p.PrincipalId = isnull(@principalId,p.PrincipalId)
 end

 if @StorageUnitId is null
 begin
   set @CountSUId = 0

   select @CountSUId = count(distinct(StorageUnitId))
     from Pack (nolock)
    where Barcode = @barcode

   if @CountSUId = 1
     select @StorageUnitId = StorageUnitId
       from Pack (nolock)
      where Barcode = @barcode
 end

 if @StorageUnitId is not null
 begin
   select @BatchId = b.BatchId
     from StorageUnitBatch sub (nolock)
     join Batch              b (nolock) on sub.BatchId = b.BatchId
    where sub.StorageUnitId = @StorageUnitId
      and b.Batch           = @batch

   select @StorageUnitBatchId = StorageUnitBatchId
     from StorageUnitBatch
    where StorageUnitId = @StorageUnitId
      and BatchId       = @BatchId
 end
 if @StorageUnitBatchId is null
   set @StorageUnitBatchId = -1

 return @StorageUnitBatchId
end
--go
--IF OBJECT_ID('dbo.p_Mobile_Stock_Take_Confirm_Product') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Mobile_Stock_Take_Confirm_Product >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Mobile_Stock_Take_Confirm_Product >>>'
--go


