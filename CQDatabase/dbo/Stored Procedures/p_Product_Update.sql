﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Update
  ///   Filename       : p_Product_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2014 14:16:32
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(510) = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @MinimumQuantity float = null,
  ///   @ReorderQuantity float = null,
  ///   @MaximumQuantity float = null,
  ///   @CuringPeriodDays int = null,
  ///   @ShelfLifeDays int = null,
  ///   @QualityAssuranceIndicator bit = null,
  ///   @ProductType nvarchar(40) = null,
  ///   @OverReceipt numeric(13,3) = null,
  ///   @HostId nvarchar(60) = null,
  ///   @RetentionSamples int = null,
  ///   @Samples int = null,
  ///   @ParentProductCode nvarchar(60) = null,
  ///   @DangerousGoodsId int = null,
  ///   @Description2 nvarchar(510) = null,
  ///   @Description3 nvarchar(510) = null,
  ///   @PrincipalId int = null,
  ///   @AssaySamples float = null,
  ///   @Category nvarchar(100) = null,
  ///   @ProductCategoryId int = null,
  ///   @ProductAlias nvarchar(100) = null,
  ///   @ProductGroup nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Update
(
 @ProductId int = null,
 @StatusId int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(510) = null,
 @Barcode nvarchar(100) = null,
 @MinimumQuantity float = null,
 @ReorderQuantity float = null,
 @MaximumQuantity float = null,
 @CuringPeriodDays int = null,
 @ShelfLifeDays int = null,
 @QualityAssuranceIndicator bit = null,
 @ProductType nvarchar(40) = null,
 @OverReceipt numeric(13,3) = null,
 @HostId nvarchar(60) = null,
 @RetentionSamples int = null,
 @Samples int = null,
 @ParentProductCode nvarchar(60) = null,
 @DangerousGoodsId int = null,
 @Description2 nvarchar(510) = null,
 @Description3 nvarchar(510) = null,
 @PrincipalId int = null,
 @AssaySamples float = null,
 @Category nvarchar(100) = null,
 @ProductCategoryId int = null,
 @ProductAlias nvarchar(100) = null,
 @ProductGroup nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
  if @ProductId = '-1'
    set @ProductId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @DangerousGoodsId = '-1'
    set @DangerousGoodsId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @ProductCategoryId = '-1'
    set @ProductCategoryId = null;
  
	 declare @Error int
 
  update Product
     set StatusId = isnull(@StatusId, StatusId),
         ProductCode = isnull(@ProductCode, ProductCode),
         Product = isnull(@Product, Product),
         Barcode = isnull(@Barcode, Barcode),
         MinimumQuantity = isnull(@MinimumQuantity, MinimumQuantity),
         ReorderQuantity = isnull(@ReorderQuantity, ReorderQuantity),
         MaximumQuantity = isnull(@MaximumQuantity, MaximumQuantity),
         CuringPeriodDays = isnull(@CuringPeriodDays, CuringPeriodDays),
         ShelfLifeDays = isnull(@ShelfLifeDays, ShelfLifeDays),
         QualityAssuranceIndicator = isnull(@QualityAssuranceIndicator, QualityAssuranceIndicator),
         ProductType = isnull(@ProductType, ProductType),
         OverReceipt = isnull(@OverReceipt, OverReceipt),
         HostId = isnull(@HostId, HostId),
         RetentionSamples = isnull(@RetentionSamples, RetentionSamples),
         Samples = isnull(@Samples, Samples),
         ParentProductCode = isnull(@ParentProductCode, ParentProductCode),
         DangerousGoodsId = isnull(@DangerousGoodsId, DangerousGoodsId),
         Description2 = isnull(@Description2, Description2),
         Description3 = isnull(@Description3, Description3),
         PrincipalId = isnull(@PrincipalId, PrincipalId),
         AssaySamples = isnull(@AssaySamples, AssaySamples),
         Category = isnull(@Category, Category),
         ProductCategoryId = isnull(@ProductCategoryId, ProductCategoryId),
         ProductAlias = isnull(@ProductAlias, ProductAlias),
         ProductGroup = isnull(@ProductGroup, ProductGroup) 
   where ProductId = @ProductId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_ProductHistory_Insert
         @ProductId = @ProductId,
         @StatusId = @StatusId,
         @ProductCode = @ProductCode,
         @Product = @Product,
         @Barcode = @Barcode,
         @MinimumQuantity = @MinimumQuantity,
         @ReorderQuantity = @ReorderQuantity,
         @MaximumQuantity = @MaximumQuantity,
         @CuringPeriodDays = @CuringPeriodDays,
         @ShelfLifeDays = @ShelfLifeDays,
         @QualityAssuranceIndicator = @QualityAssuranceIndicator,
         @ProductType = @ProductType,
         @OverReceipt = @OverReceipt,
         @HostId = @HostId,
         @RetentionSamples = @RetentionSamples,
         @Samples = @Samples,
         @ParentProductCode = @ParentProductCode,
         @DangerousGoodsId = @DangerousGoodsId,
         @Description2 = @Description2,
         @Description3 = @Description3,
         @PrincipalId = @PrincipalId,
         @AssaySamples = @AssaySamples,
         @Category = @Category,
         @ProductCategoryId = @ProductCategoryId,
         @ProductAlias = @ProductAlias,
         @ProductGroup = @ProductGroup,
         @CommandType = 'Update'
  
  return @Error
  
end
