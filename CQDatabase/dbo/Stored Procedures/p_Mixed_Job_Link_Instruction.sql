﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mixed_Job_Link_Instruction
  ///   Filename       : p_Mixed_Job_Link_Instruction.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mixed_Job_Link_Instruction
(
 @JobId         int,
 @InstructionId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @JobId         = @JobId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Mixed_Job_Link_Instruction'
    rollback transaction
    return @Error
end
