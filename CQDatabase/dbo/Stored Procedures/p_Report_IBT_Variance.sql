﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_IBT_Variance
  ///   Filename       : p_Report_IBT_Variance.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_IBT_Variance
(
 @OrderNumber		nvarchar(30),
 @AllOrVar			nvarchar(1)
)
 
as
begin
	 set nocount on;
  
 
  declare @TableHeader as Table
  (
   OrderNumber			nvarchar(30),
   OutboundDocumentId	int,
   InboundDocumentId	int,
   ExternalCompanyId	int,
   ExternalCompany		nvarchar(255),
   IssueId				int,
   ReceiptId			int   
  );
  
  declare @TableDetails as Table
  (
   OutboundDocumentId	int,
   InboundDocumentId	int,
   IssueId				int,
   IssueLineId			int,
   ReceiptId			int,
   ReceiptLineId		int,
   StorageUnitBatchId	int,
   ProductCode          nvarchar(30),
   Product				nvarchar(255),
   SKUCode				nvarchar(50),
   Batch				nvarchar(50),
   ExpiryDate			datetime,
   IssueQty				float,
   ReceiptQty			float,
   Variance				float   
  );
  
  if @OrderNumber = '-1' 
    set @OrderNumber = null
    
  if @OrderNumber = ' ' 
    set @OrderNumber = null
    
  insert @TableHeader
        (OrderNumber,
         OutboundDocumentId,
         InboundDocumentId,
         IssueId,
         ReceiptId,
         ExternalCompanyId)
  select id.OrderNumber,
         od.OutboundDocumentId,
         id.InboundDocumentId,
         i.IssueId,
         r.ReceiptId,
         id.ExternalCompanyId
  from InboundDocument			id
	join OutboundDocument		od  (nolock) on id.OrderNumber = od.OrderNumber
	join OutboundDocumentType	odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
	join InboundDocumentType	idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
	join Issue					i   (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
	join Receipt				r   (nolock) on id.InboundDocumentId = r.InboundDocumentId
	where	id.OrderNumber = isnull(@OrderNumber, id.OrderNumber)
	and		od.OrderNumber = isnull(@OrderNumber, od.OrderNumber)
    and 	odt.OutboundDocumentTypeCode = 'IBT'
	and		idt.InboundDocumentTypeCode  = 'IBT'
  
  update th
     set th.ExternalCompany  = ec.ExternalCompany
    from @TableHeader th
    join ExternalCompany     ec (nolock) on th.ExternalCompanyId = ec.ExternalCompanyId
    
  insert @TableDetails 
        (OutboundDocumentId,
         InboundDocumentId,
         IssueId,
         IssueLineId,
         ReceiptId,
         ReceiptLineId,
         StorageUnitBatchId,
         IssueQty,
         ReceiptQty)
  select th.OutboundDocumentId,
         th.InboundDocumentId,
         th.IssueId,
         il.IssueLineId,
         th.ReceiptId,
         rl.ReceiptLineId,
         il.StorageUnitBatchId,
         il.ConfirmedQuatity,
         rl.AcceptedQuantity
  from	 @TableHeader th
  join	 IssueLine il on il.IssueId = th.IssueId
  join	 ReceiptLine rl on rl.ReceiptId = th.ReceiptId
  where  il.StorageUnitBatchId = rl.StorageUnitBatchId
   
  update td
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch		 = b.Batch,
         ExpiryDate  = b.ExpiryDate
    from @TableDetails    td
    join StorageUnitBatch sub on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId     = su.StorageUnitId
    join Product          p   on su.ProductId          = p.ProductId
    join SKU              sku on su.SKUId              = sku.SKUId
    join Batch            b   on sub.BatchId		   = b.BatchId
  
  update td
     set Variance = isnull(td.IssueQty,0) - isnull(td.ReceiptQty,0)
    from @TableDetails    td
    
    
  if @AllOrVar = 'A'    
  select	th.OrderNumber,
			th.OutboundDocumentId,
			th.InboundDocumentId,
			th.ExternalCompanyId,
			th.ExternalCompany,
			th.IssueId,
			td.IssueLineId,
			th.ReceiptId,
			td.ReceiptLineId,
			td.StorageUnitBatchId,
			td.ProductCode,
			td.Product,
			td.SKUCode,
			td.Batch,
			td.ExpiryDate,
			td.IssueQty,
			td.ReceiptQty,
			td.Variance
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
    
 if @AllOrVar = 'V'    
  select	th.OrderNumber,
			th.OutboundDocumentId,
			th.InboundDocumentId,
			th.ExternalCompanyId,
			th.ExternalCompany,
			th.IssueId,
			td.IssueLineId,
			th.ReceiptId,
			td.ReceiptLineId,
			td.StorageUnitBatchId,
			td.ProductCode,
			td.Product,
			td.SKUCode,
			td.Batch,
			td.ExpiryDate,
			td.IssueQty,
			td.ReceiptQty,
			td.Variance
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
    where td.Variance !=  0 or td.Variance != null
  
 
end
