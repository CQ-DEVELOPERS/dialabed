﻿create procedure dbo.p_Interface_WebService_BatchCardGet
(
	@doc nvarchar(max) output
)
--with encryption
as
begin
  set nocount on
  
	 declare @ReferenceNumber     varchar(50),
	         @idoc                int,
	         @xml                 nvarchar(max),
	         @JobId               int,
	         @WarehouseId         int,
	         @StorageUnitId       int,
	         @ProductId           int,
	         @SKUId               int,
	         @BatchId             int,
	         @ProductCode         nvarchar(30),
	         @Product             nvarchar(255),
	         @SKUCode             nvarchar(20),
	         @Batch               nvarchar(50),
	         @ConfirmedQuantity   float,
	         @Barcode             nvarchar(50),
	         @Location            nvarchar(50),
	         @rowcount            int
	 
	 declare @TableResult as table
	 (
	  JobId           int,
	  ReferenceNumber nvarchar(30),
	  InstructionId   int,
	  StorageUnitId   int,
	  BatchId         int,
	  ProductCode     nvarchar(50),
	  Product         nvarchar(255),
	  Batch           nvarchar(50),
	  Quantity        nvarchar(50),
	  Remarks         nvarchar(max),
	  ProductionInstructions nvarchar(max),
	  IssueId                int,
	  OutboundDocumentId     int,
	  BOMHeaderId            int
	 )
	 
	 declare @TableInstruction as table
	 (
	  InstructionId      int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   ProductCode        nvarchar(50),
   Product            nvarchar(255),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   ExpectedQuantity   numeric(13,6),
   PickedQuantity     numeric(13,6),
   CheckedQuantity    numeric(13,6),
   Location           nvarchar(15)
	 )
	 
	 declare @TableLocation as table
	 (
	  Location      nvarchar(15),
	  StorageUnitId int,
	  ActualQuantity numeric(13,6)
	 )
	 
	 EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	 
	 SELECT @ReferenceNumber = ReferenceNumber,
	        @Location        = Location
		 FROM  OPENXML (@idoc, '/root/Header',1)
            WITH (ReferenceNumber varchar(50) 'ReferenceNumber',
                  Location        varchar(50) 'Location')
  
  IF LEFT(@ReferenceNumber, 2) = 'J:'
  BEGIN
        insert @TableResult
              (JobId,
               ReferenceNumber)
        select distinct JobId,
               isnull(ReferenceNumber, 'J:' + convert(nvarchar(10), JobId))
          from Job (nolock)
         where JobId = CONVERT(Int, SUBSTRING(@ReferenceNumber, 3, LEN(@ReferenceNumber) - 2))
  END
  ELSE if LEFT(@ReferenceNumber, 2) = 'J:'
  BEGIN
        insert @TableResult
              (JobId,
               ReferenceNumber)
        select distinct j.JobId,
               isnull(j.ReferenceNumber, 'J:' + convert(nvarchar(10), j.JobId))
          from Job    j (nolock)
          join Status s (nolock) on j.StatusId = s.StatusId
         where j.ReferenceNumber = @ReferenceNumber
           and s.StatusCode     in ('RL','S')
  END
  ELSE
  BEGIN
        insert @TableResult
              (JobId,
               ReferenceNumber)
        select distinct j.JobId,
               isnull(j.ReferenceNumber, 'J:' + convert(nvarchar(10), j.JobId))
          from Job         j (nolock)
          join Status      s (nolock) on j.StatusId = s.StatusId
          join Instruction i (nolock) on j.JobId = i.JobId
          join Location    l (nolock) on i.PickLocationId = l.LocationId
         where s.StatusCode     in ('RL','S')
           and l.Location        = @Location
  END
  
  update tr
     set StorageUnitId      = od.StorageUnitId,
         BatchId            = od.BatchId,
         Remarks            = i.Remarks,
         Quantity           = od.Quantity,
         IssueId            = ili.IssueId,
         OutboundDocumentId = ili.OutboundDocumentId
    from @TableResult          tr
    join Instruction          ins (nolock) on tr.JobId = ins.JobId
    join IssueLineInstruction ili (nolock) on isnull(ins.InstructionRefId, ins.InstructionId) = ili.InstructionId
    join Issue                  i (nolock) on ili.IssueId = i.IssueId
    join OutboundDocument      od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
  
  update @TableResult set ProductionInstructions = 'This is where the production instructions go'
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         Batch       = b.Batch
    from @TableResult tr
    join StorageUnit  su on tr.StorageUnitId = su.StorageUnitId
    join Product       p on su.ProductId = p.ProductId
    left
    join Batch         b on tr.BatchId = b.BatchId
  
  update tr
     set BOMHeaderId = bh.BOMHeaderId
    from @TableResult tr
    join BOMHeader    bh (nolock) on tr.StorageUnitId = bh.StorageUnitId
  
  update tr
     set ProductionInstructions = isnull(ProcedureLine1  + char(13), '') + 
                                  isnull(ProcedureLine2  + char(13), '') + 
                                  isnull(ProcedureLine3  + char(13), '') + 
                                  isnull(ProcedureLine4  + char(13), '') + 
                                  isnull(ProcedureLine5  + char(13), '') + 
                                  isnull(ProcedureLine6  + char(13), '') + 
                                  isnull(ProcedureLine7  + char(13), '') + 
                                  isnull(ProcedureLine8  + char(13), '') + 
                                  isnull(ProcedureLine9  + char(13), '') + 
                                  isnull(ProcedureLine10 + char(13), '') + 
                                  isnull(ProcedureLine11 + char(13), '') + 
                                  isnull(ProcedureLine12 + char(13), '')
    from @TableResult tr
    join BOMSpecialInstruction si on tr.BOMHeaderId = si.BOMHeaderId
  
  update tr
     set Remarks = isnull(Remarks + char(13), '') + isnull(SpecialInstructionsLine1  + char(13), '') + 
                                  isnull(SpecialInstructionsLine2  + char(13), '') + 
                                  isnull(SpecialInstructionsLine3  + char(13), '') + 
                                  isnull(SpecialInstructionsLine4  + char(13), '') + 
                                  isnull(SpecialInstructionsLine5  + char(13), '') + 
                                  isnull(SpecialInstructionsLine6  + char(13), '') + 
                                  isnull(SpecialInstructionsLine7  + char(13), '') + 
                                  isnull(SpecialInstructionsLine8  + char(13), '') + 
                                  isnull(SpecialInstructionsLine9  + char(13), '') + 
                                  isnull(SpecialInstructionsLine10 + char(13), '') + 
                                  isnull(SpecialInstructionsLine11 + char(13), '') + 
                                  isnull(SpecialInstructionsLine12 + char(13), '')
    from @TableResult tr
    join BOMSpecialInstruction si on tr.BOMHeaderId = si.BOMHeaderId
  
  insert @TableInstruction
        (InstructionId,
         StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ExpectedQuantity,
         PickedQuantity,
         CheckedQuantity,
         Location)
  SELECT i.InstructionId,
         sub.StorageUnitBatchId,
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sk.SKUCode,
         b.Batch,
         SUM(convert(numeric(13,6), i.Quantity))                   as 'ExpectedQuantity',
         sum(convert(numeric(13,6), i.ConfirmedQuantity))          as 'PickedQuantity',
         SUM(ISNULL(convert(numeric(13,6), i.CheckQuantity),0))    as 'CheckedQuantity',
         isnull(l.Location,'None')                                 as 'Location'
    from @TableResult       j
    join Instruction        i (nolock) on j.JobId              = i.JobId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on p.ProductId          = su.ProductId
    join SKU               sk (nolock) on sk.SKUId             = su.SKUId
    join Batch              b (nolock) on sub.BatchId          = b.BatchId
    left
    join Location           l (nolock) on i.StoreLocationId    = l.LocationId
  group by i.InstructionId,
           p.ProductCode,
           p.Product,
           sk.SKUCode,
           b.Batch,
           l.Location,
           su.StorageUnitId,
           sub.StorageUnitBatchId
  
  insert @TableLocation
        (Location,
         StorageUnitid,
         ActualQuantity)
  select l.Location,
         sub.StorageUnitid,
         sum(subl.ActualQuantity)
    from StorageUnitBatch          sub (nolock)
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId = a.AreaId
   where sub.StorageUnitId in (select StorageUnitId from @TableInstruction)
     and a.AreaCode = 'POT'
     and subl.ActualQuantity > 0
  group by l.Location,
           sub.StorageUnitid
  
  select StorageUnitId,
         max(ActualQuantity) as 'ActualQuantity'
    into #temp
    from @TableLocation
  group by StorageUnitId
  
  update ti
     set Location = tl.Location,
         PickedQuantity = tl.ActualQuantity
    from @TableLocation tl
    join @TableInstruction ti on tl.StorageUnitId = ti.StorageUnitId
    join #temp              t on tl.StorageUnitId = t.StorageUnitId
                             and tl.ActualQuantity = t.ActualQuantity
  
  --select * from @TableLocation order by StorageUnitId, ActualQuantity desc
  --select * from @TableInstruction
  select @rowcount = count(1) from @TableResult
  
  IF (ISNULL(@rowcount, 0) = 0)
    SELECT @XML = '<?xml version="1.0" encoding="utf-16"?><root>Reference not found</root>'
  ELSE
  BEGIN
    SELECT @XML = '<?xml version="1.0" encoding="utf-16"?>' +
        (SELECT
             tr.JobId                              as 'JobId'
            ,isnull(tr.ReferenceNumber,'')         as 'ReferenceNumber'
            ,isnull(tr.ProductCode,'')             as 'ProductCode'
	           ,isnull(tr.Product,'')                 as 'Product'
	           ,isnull(tr.Batch,'')                   as 'Batch'
	           ,isnull(tr.Quantity, 0)                as 'Quantity'
	           ,isnull(tr.Remarks, '')                as 'Remarks'
	           ,isnull(tr.ProductionInstructions, '') as 'ProductionInstructions'
	           ,(SELECT InstructionId
                    ,StorageUnitBatchId
                    ,ProductCode
                    ,Product
                    ,SKUCode
                    ,Batch
                    ,ExpectedQuantity
                    ,PickedQuantity
                    ,CheckedQuantity
                    ,Location
                    ,(SELECT distinct 
                        ISNULL(k.Barcode, ti.ProductCode)                       as 'Barcode'
                       ,convert(int, k.Quantity)                               as 'Quantity'
                      from Pack k
                      where k.StorageUnitId = ti.StorageUnitId
                        and (k.Barcode is not null
                             or k.Quantity = 1)
                      FOR XML PATH('Barcodes'), TYPE)
               from @TableInstruction ti
             order by ProductCode
            FOR XML PATH('Instruction'), TYPE)  
          from @TableResult tr
        FOR XML PATH('root'))
        
    END
  set @doc = @xml
end
