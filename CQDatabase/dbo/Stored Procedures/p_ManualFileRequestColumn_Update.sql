﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestColumn_Update
  ///   Filename       : p_ManualFileRequestColumn_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:01
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ManualFileRequestColumn table.
  /// </remarks>
  /// <param>
  ///   @ManualFileRequestTypeId int = null,
  ///   @ColumnName nvarchar(100) = null,
  ///   @ColumnLength int = null,
  ///   @ColumnDescription nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestColumn_Update
(
 @ManualFileRequestTypeId int = null,
 @ColumnName nvarchar(100) = null,
 @ColumnLength int = null,
 @ColumnDescription nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update ManualFileRequestColumn
     set ManualFileRequestTypeId = isnull(@ManualFileRequestTypeId, ManualFileRequestTypeId),
         ColumnName = isnull(@ColumnName, ColumnName),
         ColumnLength = isnull(@ColumnLength, ColumnLength),
         ColumnDescription = isnull(@ColumnDescription, ColumnDescription) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
