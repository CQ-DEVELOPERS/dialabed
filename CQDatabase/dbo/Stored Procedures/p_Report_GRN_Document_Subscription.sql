﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_GRN_Document_Subscription
  ///   Filename       : p_Report_GRN_Document_Subscription.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_GRN_Document_Subscription
 
as
begin
	 set nocount on;
	 
	 declare @ReceiptId		int,
			 @ReportTypeId	int
	
	select @ReportTypeId = rt.reporttypeid
	from   ReportType    rt (nolock)
	where  rt.reporttypecode = 'GRN'
	
	
	insert into ReportPrinted 
			   (ReportTypeId, 
				ReceiptId, 
				PrintedCopies,
				CreateDate)
	select      @ReportTypeId, 
				ReceiptId, 
				0,
				GETDATE() 
	  from Receipt               r (nolock)
	  join InboundDocument      id (nolock) on r.InboundDocumentId = id.InboundDocumentId
	  join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
	 where idt.InboundDocumentTypeCode in ('RCP', 'BOND','RET', 'REWORK','Standard')
	   --and r.DeliveryDate > '2013-01-01'
	   and r.statusid in (dbo.ufn_StatusId('R','RC'))
	   and not exists (select top 1 ReceiptId, ReportTypeId 
						 from ReportPrinted rp 
						where rp.ReceiptId = r.ReceiptId
						and rp.ReportTypeId = @ReportTypeId)
	 
	select (select Value from Configuration where ConfigurationId = 200 and WarehouseId = id.WarehouseId) as 'ServerName',
			db_name()    as 'DatabaseName',
			'Auto Email' as 'UserName',
			-1           as InboundShipmentId,
			r.ReceiptId,
			cl.EMail     as 'Email',
			cl.EMail     as 'TO',
			'TEST SUB'   as 'Subject'
	from  Receipt                 r (nolock) 
	join  InboundDocument        id (nolock) on r.InboundDocumentId = id.InboundDocumentId
	join  ContactList            cl (nolock) on id.ExternalCompanyId = cl.ExternalCompanyId
	join  ReportType             rt (nolock) on cl.ReportTypeId = rt.ReportTypeId
	join  ReportPrinted          rp (nolock) on r.ReceiptId = rp.ReceiptId
											and rp.PrintedCopies = 0
    where rp.ReportTypeId = @ReportTypeId
 
	 
	update  ReportPrinted
	   set  PrintedCopies = 1,
	        EmailDate = getdate()
	 where  PrintedCopies = 0
	 and    ReportTypeId = @ReportTypeId	
   
end 
 
