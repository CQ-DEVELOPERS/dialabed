﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Location_Area_Type
  ///   Filename       : p_Report_Location_Area_Type.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Report_Location_Area_Type]
(
	@WarehouseId		int,
	@AreaId				nvarchar(15) = null,
	@LocationTypeId		nvarchar(15) = null
)
--with encryption
as
begin
	 set nocount on;
	Declare @Area nvarchar(50),
			@LocationType nvarchar(50),
			@OldArea nvarchar(50),
			@OldLocationType nvarchar(50),
			@Location nvarchar(50),
			@ColumnCnt int,
			@RowCnt int,
			@Maxrows int
	Set @Maxrows = 35
	Declare @TableResult Table(RowCnt int,
								ColumnCnt int,
								Area nvarchar(50),
								LocationType nvarchar(50),
								Location nvarchar(50))
	
	If @LocationTypeId = -1 set @LocationTypeId = null
	If @AreaId = -1 set @AreaId = null
	
	Declare Location1 Cursor for 
	select ar.Area,
			t.LocationType,
			l.Location 
	From Location l
		join AreaLocation a on a.LocationId = l.LocationId
		Join Area ar on ar.AreaId = a.AreaId
		Join LocationType t on t.LocationTypeID = l.LocationTypeId
	Where warehouseid = @WarehouseId 
			and a.Areaid = Isnull(@AreaId,a.Areaid) and l.LocationTypeId = Isnull(@LocationTypeId,l.LocationTypeID)
	Order By Ar.Area,t.LocationType,l.Location

			Open Location1
				Set @RowCnt = 1
				Set @ColumnCnt = 1

				Fetch next from Location1 into @Area,@LocationType,@Location
				Insert into @TableResult Select @RowCnt,@ColumnCnt,@Area,@LocationType,@Location
				--pRINT @Location
				While @@Fetch_Status = 0
				Begin
					Set @RowCnt = (@RowCnt + 1)
					If @RowCnt > @Maxrows
					Begin
						Set @ColumnCnt = @ColumnCnt + 1
						Set @RowCnt = 1
					End
					
					Fetch next from Location1 into @Area,@LocationType,@Location
					
					If @OldArea <> @Area
					Begin
						Set @ColumnCnt = 1
						Set @RowCnt = 1
					end
					If @OldLocationType <> @LocationType
					Begin
						Set @ColumnCnt = 1
						Set @RowCnt = 1
					End
					Insert into @TableResult 
						Select @RowCnt,@ColumnCnt,@Area,@LocationType,@Location
					Set @OldArea = @Area
					Set @OldLocationType = @LocationType
					--pRINT @Location
				End 
				Close Location1
				DEallocate Location1
				Select * from @TableResult
				

end
