﻿/*  
  /// <summary>  
  ///   Procedure Name : p_Receiving_Document_Search  
  ///   Filename       : p_Receiving_Document_Search.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 18 Jun 2007  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/
CREATE PROCEDURE [dbo].[p_Receiving_Document_Search] (
	 @WarehouseId INT
	,@InboundDocumentTypeId INT
	,@InboundShipmentId INT
	,@ExternalCompanyCode NVARCHAR(30)
	,@ExternalCompany NVARCHAR(255)
	,@OrderNumber NVARCHAR(30)
	,@FromDate DATETIME
	,@ToDate DATETIME
	,@PrincipalId INT = NULL
	,@OperatorId INT = NULL
	,@DeliveryNoteNumber NVARCHAR(30) = NULL
	)
AS

BEGIN
	SET NOCOUNT ON;

	DECLARE @TableResult AS TABLE (
		Original BIT
		,InboundDocumentId INT
		,ReceiptId INT
		,OrderNumber NVARCHAR(60)
		,ReferenceNumber NVARCHAR(30)
		,InboundShipmentId INT
		,SupplierCode NVARCHAR(30)
		,Supplier NVARCHAR(255)
		,NumberOfLines INT
		,DeliveryDate DATETIME
		,PlannedDeliveryDate DATETIME
		,CreateDate DATETIME
		,StatusId INT
		,StatusCode NVARCHAR(10)
		,STATUS NVARCHAR(50)
		,InboundDocumentType NVARCHAR(30)
		,InboundDocumentTypeCode NVARCHAR(10)
		,LocationId INT
		,Location NVARCHAR(15)
		,Rating INT
		,Delivery INT
		,DeliveryNoteNumber NVARCHAR(30)
		,SealNumber NVARCHAR(30)
		,PriorityID INT
		,Priority NVARCHAR(50)
		,VehicleRegistration NVARCHAR(10)
		,Remarks NVARCHAR(500)
		,AllowPalletise BIT
		,PrincipalId INT
		,PrincipalCode NVARCHAR(30)
		,ShippingAgentId INT
		,ShippingAgent NVARCHAR(255)
		,ContainerNumber NVARCHAR(50)
		,ContainerSize NVARCHAR(50)
		,BOE NVARCHAR(255)
		,AdditionalText1 NVARCHAR(255)
		,AdditionalText2 NVARCHAR(255)
		,QuantityMismatch BIT DEFAULT 0
		,Complete NVARCHAR(50)
		,WarehouseId INT
		,VehicleTypeId INT
		,VehicleType NVARCHAR(40)
		,GRN NVARCHAR(30)
		,Locked BIT
		);
	DECLARE @InboundDocumentTypeCode NVARCHAR(10)
		,@ExternalCompanyId INT

	SET @ExternalCompanyId = NULL
	SET @ExternalCompanyId = (
			SELECT ExternalCompanyId
			FROM Operator o
			WHERE o.OperatorId = @OperatorId
			)

	IF @InboundDocumentTypeId IN (- 1, 0)
		SET @InboundDocumentTypeId = NULL

	IF @ExternalCompanyCode IS NULL
		SET @ExternalCompanyCode = ''

	IF @ExternalCompany IS NULL
		SET @ExternalCompany = ''

	IF @PrincipalId = - 1
		SET @PrincipalId = NULL

	IF datalength(@OrderNumber) > 1
		SET @InboundShipmentId = NULL
	SET @InboundDocumentTypeCode = ''

	IF (
			SELECT dbo.ufn_Configuration(211, @WarehouseId)
			) = 0
		SET @InboundDocumentTypeCode = 'PRV'

	IF (
			SELECT dbo.ufn_Configuration(260, @WarehouseId)
			) = 1
		SET @WarehouseId = NULL

	INSERT @TableResult (
		Original
		,InboundShipmentId
		,InboundDocumentId
		,ReceiptId
		,LocationId
		,OrderNumber
		,ReferenceNumber
		,SupplierCode
		,Supplier
		,StatusId
		,StatusCode
		,STATUS
		,DeliveryDate
		,PlannedDeliveryDate
		,CreateDate
		,InboundDocumentType
		,InboundDocumentTypeCode
		,Rating
		,Delivery
		,DeliveryNoteNumber
		,SealNumber
		,PriorityID
		,VehicleRegistration
		,Remarks
		,AllowPalletise
		,NumberOfLines
		,PrincipalId
		,ShippingAgentId
		,ContainerNumber
		,ContainerSize
		,BOE
		,AdditionalText1
		,AdditionalText2
		,WarehouseId
		,VehicleTypeId
		,GRN
		,Locked
		)
	SELECT 1
		,isr.InboundShipmentId
		,id.InboundDocumentId
		,r.ReceiptId
		,r.LocationId
		,id.OrderNumber
		,id.ReferenceNumber
		,ec.ExternalCompanyCode
		,ec.ExternalCompany
		,r.StatusId
		,s.StatusCode
		,s.STATUS
		,isnull(r.DeliveryDate, convert(NVARCHAR(10), id.CreateDate, 120))
		,isnull(r.PlannedDeliveryDate, id.DeliveryDate)
		,id.CreateDate
		,idt.InboundDocumentType
		,idt.InboundDocumentTypeCode
		,ec.Rating
		,r.Delivery
		,r.DeliveryNoteNumber
		,r.SealNumber
		,r.PriorityID
		,r.VehicleRegistration
		,r.Remarks
		,r.AllowPalletise
		,r.NumberOfLines
		,id.PrincipalId
		,r.ShippingAgentId
		,r.ContainerNumber
		,NULL
		,r.BOE
		,r.AdditionalText1
		,r.AdditionalText2
		,r.WarehouseId
		,r.VehicleTypeId
		,r.GRN
		,r.Locked
	FROM InboundDocument id(NOLOCK)
	JOIN Receipt r(NOLOCK) ON id.InboundDocumentId = r.InboundDocumentId
	LEFT JOIN InboundShipmentReceipt isr(NOLOCK) ON r.ReceiptId = isr.ReceiptId
	JOIN STATUS s(NOLOCK) ON r.StatusId = s.StatusId
	JOIN InboundDocumentType idt(NOLOCK) ON id.InboundDocumentTypeId = idt.InboundDocumentTypeId
	LEFT OUTER JOIN ExternalCompany ec(NOLOCK) ON id.ExternalCompanyId = ec.ExternalCompanyId
	WHERE isnull(isr.InboundShipmentId, - 1) = isnull(@InboundShipmentId, isnull(isr.InboundShipmentId, - 1)) AND id.InboundDocumentTypeId = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId) AND isnull(ec.ExternalCompanyCode, '-1') LIKE isnull(@ExternalCompanyCode + '%', Isnull(ec.ExternalCompanyCode, '-1')) AND isnull(ec.ExternalCompany, '-1') LIKE isnull(@ExternalCompany + '%', isnull(ec.ExternalCompany, '-1')) AND (id.OrderNumber LIKE isnull(@OrderNumber + '%', id.OrderNumber) OR id.ReferenceNumber LIKE isnull(@OrderNumber + '%', id.OrderNumber)) AND isnull(r.DeliveryDate, isnull(r.DeliveryDate, id.CreateDate)) BETWEEN @FromDate AND @ToDate AND s.Type = 'R' AND s.StatusCode IN ('W', 'C', 'D', 'S', 'P', 'R', 'LA', 'OH') -- Waiting, Confirmed, Delivered, Started, Palletised, Received, Locations Allocated, On Hold 
		AND idt.InboundDocumentTypeCode != @InboundDocumentTypeCode AND id.WarehouseId = isnull(@WarehouseId, id.WarehouseId) AND id.OrderNumber NOT LIKE 'PRV2%' AND idt.InboundDocumentTypeCode != 'MO' AND isnull(id.PrincipalId, - 1) = isnull(@PrincipalId, isnull(id.PrincipalId, - 1)) AND id.ExternalCompanyId = isnull(@ExternalCompanyId, id.ExternalCompanyId)
		AND isnull(r.DeliveryNoteNumber, - 1) = isnull(@DeliveryNoteNumber, isnull(r.DeliveryNoteNumber, - 1))

	INSERT @TableResult (
		Original
		,InboundShipmentId
		,InboundDocumentId
		,ReceiptId
		,LocationId
		,OrderNumber
		,ReferenceNumber
		,SupplierCode
		,Supplier
		,StatusId
		,StatusCode
		,STATUS
		,DeliveryDate
		,PlannedDeliveryDate
		,CreateDate
		,InboundDocumentType
		,InboundDocumentTypeCode
		,Rating
		,Delivery
		,DeliveryNoteNumber
		,SealNumber
		,PriorityID
		,VehicleRegistration
		,Remarks
		,AllowPalletise
		,NumberOfLines
		,PrincipalId
		,ShippingAgentId
		,ContainerNumber
		,ContainerSize
		,BOE
		,AdditionalText1
		,AdditionalText2
		,WarehouseId
		,VehicleTypeId
		,GRN
		,Locked
		)
	SELECT 0
		,isnull(isr.InboundShipmentId, id.ReferenceNumber)
		,id.InboundDocumentId
		,r.ReceiptId
		,r.LocationId
		,tr.OrderNumber + ' / ' + id.OrderNumber
		,id.ReferenceNumber
		,tr.SupplierCode
		,tr.Supplier
		,r.StatusId
		,s.StatusCode
		,s.STATUS
		,isnull(r.DeliveryDate, convert(NVARCHAR(10), id.CreateDate, 120))
		,isnull(r.PlannedDeliveryDate, id.DeliveryDate)
		,id.CreateDate
		,idt.InboundDocumentType
		,idt.InboundDocumentTypeCode
		,tr.Rating
		,r.Delivery
		,r.DeliveryNoteNumber
		,r.SealNumber
		,r.PriorityID
		,r.VehicleRegistration
		,r.Remarks
		,r.AllowPalletise
		,r.NumberOfLines
		,id.PrincipalId
		,r.ShippingAgentId
		,r.ContainerNumber
		,NULL
		,r.BOE
		,r.AdditionalText1
		,r.AdditionalText2
		,r.WarehouseId
		,r.VehicleTypeId
		,r.GRN
		,r.Locked
	FROM @TableResult tr
	JOIN InboundDocument id(NOLOCK) ON id.ReferenceNumber = convert(NVARCHAR(30), tr.InboundShipmentId)
	JOIN Receipt r(NOLOCK) ON id.InboundDocumentId = r.InboundDocumentId
	LEFT JOIN InboundShipmentReceipt isr(NOLOCK) ON r.ReceiptId = isr.ReceiptId
	JOIN STATUS s(NOLOCK) ON r.StatusId = s.StatusId
	JOIN InboundDocumentType idt(NOLOCK) ON id.InboundDocumentTypeId = idt.InboundDocumentTypeId
	WHERE s.StatusCode IN ('W', 'C', 'D', 'S', 'P', 'R', 'LA', 'OH') -- Waiting, Confirmed, Delivered, Started, Palletised, Received, Locations Allocated, On Hold  

	DELETE tr1
	FROM @TableResult tr1
	JOIN @TableResult tr2 ON tr1.InboundShipmentId = tr2.InboundShipmentId
	WHERE tr1.Original = 1 AND tr2.Original = 0

	UPDATE @TableResult
	SET InboundShipmentId = NULL
	WHERE Original = 0

	--  update tr  
	--     set ReceiptId = -2,  
	--         Status    = 'Unit Price not captured'  
	--    from @TableResult tr  
	--    join InboundLine  il (nolock) on tr.InboundDocumentId = il.InboundDocumentId  
	--   where il.UnitPrice is null  
	--     and tr.InboundDocumentTypeCode = 'RET'  
	--     and isnull(tr.ReferenceNumber,'') = ''  
	UPDATE r
	SET Location = l.Location
	FROM @TableResult r
	JOIN Location l(NOLOCK) ON r.LocationId = l.LocationId

	UPDATE r
	SET VehicleType = vt.Name
	FROM @TableResult r
	JOIN VehicleType vt(NOLOCK) ON r.VehicleTypeId = vt.Id

	UPDATE r
	SET Priority = P.Priority
	FROM @TableResult r
	JOIN Priority P(NOLOCK) ON r.PriorityId = P.PriorityId

	UPDATE tr
	SET PrincipalCode = p.PrincipalCode
	FROM @TableResult tr
	JOIN Principal p(NOLOCK) ON tr.PrincipalId = p.PrincipalId

	UPDATE tr
	SET ShippingAgent = ec.ExternalCompany
	FROM @TableResult tr
	JOIN ExternalCompany ec(NOLOCK) ON tr.ShippingAgentId = ec.ExternalCompanyId

	IF (
			SELECT dbo.ufn_Configuration(446, @WarehouseId)
			) = 1 -- Receiving - Only allow active StorageUnits  
		UPDATE tr
		SET STATUS = 'On Hold'
		FROM @TableResult tr
		JOIN ReceiptLine rl(NOLOCK) ON tr.ReceiptId = rl.ReceiptId
		JOIN StorageUnitBatch sub(NOLOCK) ON rl.StorageUnitBatchId = sub.StorageUnitBatchId
		JOIN Pack pk(NOLOCK) ON sub.StorageUnitId = pk.StorageUnitId AND tr.WarehouseId = pk.WarehouseId
		JOIN STATUS s(NOLOCK) ON pk.StatusId = s.StatusId AND s.StatusCode != 'A'

	IF (
			SELECT dbo.ufn_Configuration(447, @WarehouseId)
			) = 1 -- Receiving - Only supervisors send up PO which does not balance  
		UPDATE tr
		SET QuantityMismatch = 1
		FROM @TableResult tr
		JOIN ReceiptLine rl(NOLOCK) ON tr.ReceiptId = rl.ReceiptId
		WHERE rl.DeliveryNoteQuantity != rl.ReceivedQuantity

	UPDATE @TableResult
	SET Complete = 'Complete'
	WHERE StatusCode IN ('R', 'P', 'LA') AND Locked = 0

	UPDATE @TableResult
	SET Complete = 'Qty Mismatch'
	WHERE Complete = 'Complete' AND QuantityMismatch = 1 AND Locked = 0

	SELECT isnull(InboundShipmentId, - 1) AS 'InboundShipmentId'
		,ReceiptId
		,OrderNumber
		,ReferenceNumber
		,SupplierCode
		,Supplier
		,NumberOfLines
		,DeliveryDate
		,PlannedDeliveryDate
		,STATUS
		,InboundDocumentType
		,isnull(LocationId, - 1) AS LocationId
		,Location
		,Rating
		,Delivery
		,DeliveryNoteNumber
		,SealNumber
		,PriorityID
		,Priority
		,VehicleRegistration
		,Remarks
		,isnull(AllowPalletise, 1) AS 'AllowPalletise'
		,PrincipalCode
		,Complete
		,CASE WHEN StatusCode IN ('R', 'P', 'LA') THEN 'Redelivery' ELSE '' END AS 'Redelivery'
		,PrincipalCode
		,ShippingAgent
		,isnull(ShippingAgentid, - 1) AS ShippingAgentId
		,ContainerNumber
		,ContainerSize
		,BOE
		,AdditionalText1
		,AdditionalText2
		,isnull(VehicleTypeId, - 1) AS 'VehicleTypeId'
		,VehicleType
		,GRN
	FROM @TableResult
END