﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingFile_Select
  ///   Filename       : p_InterfaceMappingFile_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 09:03:35
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceMappingFile table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingFileId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceMappingFile.InterfaceMappingFileId,
  ///   InterfaceMappingFile.PrincipalId,
  ///   InterfaceMappingFile.InterfaceFileTypeId,
  ///   InterfaceMappingFile.InterfaceDocumentTypeId,
  ///   InterfaceMappingFile.Delimiter,
  ///   InterfaceMappingFile.StyleSheet,
  ///   InterfaceMappingFile.PrincipalFTPSite,
  ///   InterfaceMappingFile.FTPUserName,
  ///   InterfaceMappingFile.FTPPassword,
  ///   InterfaceMappingFile.HeaderRow,
  ///   InterfaceMappingFile.SubDirectory,
  ///   InterfaceMappingFile.FilePrefix 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingFile_Select
(
 @InterfaceMappingFileId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceMappingFile.InterfaceMappingFileId
        ,InterfaceMappingFile.PrincipalId
        ,InterfaceMappingFile.InterfaceFileTypeId
        ,InterfaceMappingFile.InterfaceDocumentTypeId
        ,InterfaceMappingFile.Delimiter
        ,InterfaceMappingFile.StyleSheet
        ,InterfaceMappingFile.PrincipalFTPSite
        ,InterfaceMappingFile.FTPUserName
        ,InterfaceMappingFile.FTPPassword
        ,InterfaceMappingFile.HeaderRow
        ,InterfaceMappingFile.SubDirectory
        ,InterfaceMappingFile.FilePrefix
    from InterfaceMappingFile
   where isnull(InterfaceMappingFile.InterfaceMappingFileId,'0')  = isnull(@InterfaceMappingFileId, isnull(InterfaceMappingFile.InterfaceMappingFileId,'0'))
  order by Delimiter
  
end
