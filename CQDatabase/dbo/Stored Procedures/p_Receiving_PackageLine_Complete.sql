﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_PackageLine_Complete
  ///   Filename       : p_Receiving_PackageLine_Complete.sql
  ///   Create By      : Willis
  ///   Date Created   : Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_PackageLine_Complete
(
	@WarehouseId		int,
	@ReceiptId			 int
		
)
 
as
begin

declare @LocationId         int,		
        @StorageUnitId      int,
		@StorageUnitBatchId	int,
		@StatusCode			nvarchar(10),
		@NewStatus			nvarchar(10),
		@BatchId			int,
		@PackageLineId		int,
		@ActualQuantity		numeric(13,6),
		@Result				int
	
	Set @Result = -1
	
	While Exists(Select top 1 1 From ReceiptLinePackaging r
						Join Status s on s.StatusId = r.StatusId and s.StatusCode = 'R'
					where ReceiptId = @ReceiptId)
	Begin
			Select @PackageLineId=PackageLineId,
					@StorageUnitBatchId=StorageUnitBatchId,
					@ActualQuantity=AcceptedQuantity
				From ReceiptLinePackaging r
						Join Status s on s.StatusId = r.StatusId and s.StatusCode = 'R'
					where ReceiptId = @ReceiptId

			Select @StorageUnitId=StorageUnitId,@BatchId=BatchId from StorageUnitBatch where StorageUnitBatchId = @StorageUnitBatchId
			
			exec p_Location_Get @WarehouseId,@LocationId output,@StorageUnitBatchId,@ActualQuantity


			Select @NewStatus=StatusId From Status where [Type] = 'R' and StatusCode = 'RC'
	
			If  Exists(Select top 1 1 from StorageUnitBatchLocation where StorageUnitBatchId = @StorageUnitBatchId and LocationId = @LocationId)
			Begin
				
				Update StorageUnitBatchLocation set ActualQuantity=ActualQuantity+@ActualQuantity
					where StorageUnitBatchId=@StorageUnitBatchId and LocationId = @LocationId
				
			End
			Else
				Insert into StorageUnitBatchLocation (StorageUnitBatchId,LocationId,ActualQuantity,AllocatedQuantity,ReservedQuantity) 
						Values (@StorageUnitBatchId,@LocationId,@ActualQuantity,0,0)

			update ReceiptLinePackaging set StatusId = @NewStatus where PackageLineId = @PackageLineId

			Set @Result = 1
	End
	
	Select @Result

end
