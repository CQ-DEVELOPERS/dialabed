﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorShortPickPerformance_Update
  ///   Filename       : p_OperatorShortPickPerformance_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:31
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorShortPickPerformance table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @InstructionTypeId int = null,
  ///   @EndDate datetime = null,
  ///   @ActivityStatus char(1) = null,
  ///   @PickingRate int = null,
  ///   @Units int = null,
  ///   @UnitsShort int = null,
  ///   @Weight float = null,
  ///   @WeightShort float = null,
  ///   @Instructions int = null,
  ///   @Orders int = null,
  ///   @OrderLines int = null,
  ///   @Jobs int = null,
  ///   @ActiveTime int = null,
  ///   @DwellTime int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorShortPickPerformance_Update
(
 @OperatorId int = null,
 @InstructionTypeId int = null,
 @EndDate datetime = null,
 @ActivityStatus char(1) = null,
 @PickingRate int = null,
 @Units int = null,
 @UnitsShort int = null,
 @Weight float = null,
 @WeightShort float = null,
 @Instructions int = null,
 @Orders int = null,
 @OrderLines int = null,
 @Jobs int = null,
 @ActiveTime int = null,
 @DwellTime int = null,
 @WarehouseId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update OperatorShortPickPerformance
     set OperatorId = isnull(@OperatorId, OperatorId),
         InstructionTypeId = isnull(@InstructionTypeId, InstructionTypeId),
         EndDate = isnull(@EndDate, EndDate),
         ActivityStatus = isnull(@ActivityStatus, ActivityStatus),
         PickingRate = isnull(@PickingRate, PickingRate),
         Units = isnull(@Units, Units),
         UnitsShort = isnull(@UnitsShort, UnitsShort),
         Weight = isnull(@Weight, Weight),
         WeightShort = isnull(@WeightShort, WeightShort),
         Instructions = isnull(@Instructions, Instructions),
         Orders = isnull(@Orders, Orders),
         OrderLines = isnull(@OrderLines, OrderLines),
         Jobs = isnull(@Jobs, Jobs),
         ActiveTime = isnull(@ActiveTime, ActiveTime),
         DwellTime = isnull(@DwellTime, DwellTime),
         WarehouseId = isnull(@WarehouseId, WarehouseId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
