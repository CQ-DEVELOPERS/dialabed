﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Merge
  ///   Filename       : p_Location_Merge.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Apr 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Merge
(
 @StorageUnitBatchId int,
 @LocationId         int,
 @ConfirmedQuantity  float,
 @CurrentSUB         int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Rowcount          int,
          @StorageUnitId     int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StorageUnitId = StorageUnitId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select top 1
         @CurrentSUB     = subl.StorageUnitBatchId
    from StorageUnitBatchLocation subl
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  where subl.LocationId          = @LocationId
    and sub.StorageUnitId        = @StorageUnitId      -- Is the same product
    and subl.StorageUnitBatchId != @StorageUnitBatchId -- Not the new SUB
  order by subl.ActualQuantity desc                    -- One with most quantity
  
  if @CurrentSUB is null
    return 0;
  
  update StorageUnitBatchLocation
     set ActualQuantity = ActualQuantity + @ConfirmedQuantity
   where StorageUnitBatchId = @CurrentSUB
     and LocationId         = @LocationId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  delete StorageUnitBatchLocation
   where StorageUnitBatchId = @StorageUnitBatchId
     and LocationId         = @LocationId
     and ActualQuantity     = @ConfirmedQuantity
     and ReservedQuantity   = 0
     and AllocatedQuantity  = 0
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  if @Rowcount = 0
  begin
    update StorageUnitBatchLocation
       set ActualQuantity = ActualQuantity - @ConfirmedQuantity
     where StorageUnitBatchId = @StorageUnitBatchId
       and LocationId         = @LocationId
    
    select @Error = @@Error, @Rowcount = @@Rowcount
    
    if @Error <> 0
      goto error
  end
  
  return
  
  error:
    set @Error = -1
    return @Error
end
