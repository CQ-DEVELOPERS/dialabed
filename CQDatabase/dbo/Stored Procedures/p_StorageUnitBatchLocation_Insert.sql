﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocation_Insert
  ///   Filename       : p_StorageUnitBatchLocation_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:02
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitBatchLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null output,
  ///   @LocationId int = null output,
  ///   @ActualQuantity float = null,
  ///   @AllocatedQuantity float = null,
  ///   @ReservedQuantity float = null,
  ///   @NettWeight float = null 
  /// </param>
  /// <returns>
  ///   StorageUnitBatchLocation.StorageUnitBatchId,
  ///   StorageUnitBatchLocation.LocationId,
  ///   StorageUnitBatchLocation.ActualQuantity,
  ///   StorageUnitBatchLocation.AllocatedQuantity,
  ///   StorageUnitBatchLocation.ReservedQuantity,
  ///   StorageUnitBatchLocation.NettWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Insert
(
 @StorageUnitBatchId int = null output,
 @LocationId int = null output,
 @ActualQuantity float = null,
 @AllocatedQuantity float = null,
 @ReservedQuantity float = null,
 @NettWeight float = null 
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
	 declare @Error int
 
  insert StorageUnitBatchLocation
        (StorageUnitBatchId,
         LocationId,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         NettWeight)
  select @StorageUnitBatchId,
         @LocationId,
         @ActualQuantity,
         @AllocatedQuantity,
         @ReservedQuantity,
         @NettWeight 
  
  select @Error = @@Error, @LocationId = scope_identity()
  
  
  return @Error
  
end
