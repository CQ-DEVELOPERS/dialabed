﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber
  ///   Filename       : p_Register_SerialNumber.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remark
s>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber
(
 @ReceiptId    
      int = null,
 @ReceiptLineId      int = null,
 @StoreInstructionId int = null,
 @PickInstructionId  int = null,
 @SerialNumber       nvarchar(50),
 @StorageUnitId      int,
 @BatchId            int = null,
 @ReferenceNumber    nvarchar(30) = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @SerialNumberId    int,
          @LocationId        int,
          @ReceivedQuantity	 int,
          @CountSerial		 int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @LocationId = StoreLocationId
    from Instruction (nolock)
   where InstructionId = @StoreInstructionId
  
  begin transaction
  
  if @ReceiptLineId is not null
	select @ReceivedQuantity = ReceivedQuantity
      from ReceiptLine (nolock)
     where ReceiptLineId = @ReceiptLineId
  
  
  if @StoreInstructionId is not null
	select @ReceivedQuantity = i.ConfirmedQuantity
	  from instruction i (nolock)
	 where instructionid  = @StoreInstructionId
   
   select @CountSerial = count(sn.SerialNumberId) 
     from SerialNumber sn (nolock)
    where (sn.ReceiptId = @ReceiptId
      and sn.ReceiptLineId = @ReceiptLineId )
      or sn.StoreInstructionId = @StoreInstructionId  
      
   if @ReceivedQuantity <= @CountSerial
	goto error
 
   select @SerialNumberId = SerialNumberId
    from SerialNumber (nolock)
   where SerialNumber = @SerialNumber
  
  if @SerialNumberId is null
  exec @Error = p_SerialNumber_Insert
   @SerialNumberId     = @SerialNumberId output,
   @StorageUnitId      = @StorageUnitId,
   @LocationId         = @LocationId,
   @ReceiptId          = @ReceiptId,
   @ReceiptLineId      = @ReceiptLineId,
   @StoreInstructionId = @StoreInstructionId,
   @PickInstructionId  = @PickInstructionId,
   @SerialNumber       = @SerialNumber,
   @ReceivedDate       = @GetDate,
   @DespatchDate       = null,
   @ReferenceNumber    = @ReferenceNumber
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Register_SerialNumber'
    rollback transaction
    return @Error
end
