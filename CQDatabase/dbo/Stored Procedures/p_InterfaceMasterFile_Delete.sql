﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMasterFile_Delete
  ///   Filename       : p_InterfaceMasterFile_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:29:21
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceMasterFile table.
  /// </remarks>
  /// <param>
  ///   @MasterFileId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMasterFile_Delete
(
 @MasterFileId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceMasterFile
     where MasterFileId = @MasterFileId
  
  select @Error = @@Error
  
  
  return @Error
  
end
