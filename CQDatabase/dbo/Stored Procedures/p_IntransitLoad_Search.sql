﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoad_Search
  ///   Filename       : p_IntransitLoad_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:32
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the IntransitLoad table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   IntransitLoad.IntransitLoadId,
  ///   IntransitLoad.WarehouseId,
  ///   IntransitLoad.StatusId,
  ///   IntransitLoad.DeliveryNoteNumber,
  ///   IntransitLoad.DeliveryDate,
  ///   IntransitLoad.SealNumber,
  ///   IntransitLoad.VehicleRegistration,
  ///   IntransitLoad.Route,
  ///   IntransitLoad.DriverId,
  ///   IntransitLoad.Remarks,
  ///   IntransitLoad.CreatedById,
  ///   IntransitLoad.ReceivedById,
  ///   IntransitLoad.CreateDate,
  ///   IntransitLoad.LoadClosed,
  ///   IntransitLoad.ReceivedDate,
  ///   IntransitLoad.Offloaded 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoad_Search
(
 @IntransitLoadId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @IntransitLoadId = '-1'
    set @IntransitLoadId = null;
  
 
  select
         IntransitLoad.IntransitLoadId
        ,IntransitLoad.WarehouseId
        ,IntransitLoad.StatusId
        ,IntransitLoad.DeliveryNoteNumber
        ,IntransitLoad.DeliveryDate
        ,IntransitLoad.SealNumber
        ,IntransitLoad.VehicleRegistration
        ,IntransitLoad.Route
        ,IntransitLoad.DriverId
        ,IntransitLoad.Remarks
        ,IntransitLoad.CreatedById
        ,IntransitLoad.ReceivedById
        ,IntransitLoad.CreateDate
        ,IntransitLoad.LoadClosed
        ,IntransitLoad.ReceivedDate
        ,IntransitLoad.Offloaded
    from IntransitLoad
   where isnull(IntransitLoad.IntransitLoadId,'0')  = isnull(@IntransitLoadId, isnull(IntransitLoad.IntransitLoadId,'0'))
  
end
