﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOHeader_List
  ///   Filename       : p_InterfaceImportSOHeader_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:17
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportSOHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportSOHeader.InterfaceImportSOHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOHeader_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceImportSOHeaderId
        ,null as 'InterfaceImportSOHeader'
  union
  select
         InterfaceImportSOHeader.InterfaceImportSOHeaderId
        ,InterfaceImportSOHeader.InterfaceImportSOHeaderId as 'InterfaceImportSOHeader'
    from InterfaceImportSOHeader
  
end
