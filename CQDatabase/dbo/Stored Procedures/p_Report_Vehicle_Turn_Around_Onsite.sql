﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Vehicle_Turn_Around_Onsite
  ///   Filename       : p_Report_Vehicle_Turn_Around_Onsite.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Vehicle_Turn_Around_Onsite
(
 @ExternalCompanyId int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
  set nocount on;
  
  if @ExternalCompanyId = -1
    set @ExternalCompanyId = null
  
  select ec.ExternalCompany,
         ec.ExternalCompanyCode,
         id.OrderNumber,
         pln.PlannedStart as 'PlannedStart',
         act.PlannedStart as 'ActualStart',
         100.000 as 'StartDiff',
         pln.PlannedEnd as 'PlannedEnd',
         act.PlannedEnd as 'ActualEnd',
         90 as 'EndDiff'
    from DockSchedule   pln (nolock)
    left
    join DockSchedule   act (nolock) on pln.ReceiptId = act.ReceiptId
                                    and act.Version = 1
    join Receipt          r (nolock) on pln.ReceiptId = r.ReceiptId
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join ExternalCompany ec (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
   where (id.ExternalCompanyId = @ExternalCompanyId or @ExternalCompanyId is null)
     and pln.PlannedStart between @FromDate and @ToDate
     and pln.Version = 0
end
 
