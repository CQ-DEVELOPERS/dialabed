﻿
-- =============================================

-- Author:        <Karen Sucksmith>

--Create date:    <08 October 2010>

--Description:    <Display all Sales Order Details and Reprocess if requested>

--DROP PROCEDURE [dbo].[p_Report_GetInterfaceDashBoardPODtls]

-- =============================================
							
Create  PROCEDURE [dbo].[p_Report_GetInterfaceDashBoardPODtls]
(
@ExtractDescription nvarchar(30),
@InterfaceExportPOHeaderId INT,
@ReportType char(1),
@IntExt nvarchar(20) = null
)
AS

BEGIN 

SET NOCOUNT ON;
       
DECLARE @PrimaryKey			nvarchar(30),
		@OrderNumber		nvarchar(30),
	    @RecordType			char(3),
	    @RecordStatus		char(1),
	    @SupplierCode		nvarchar(30),
	    @Supplier			nvarchar(255),
	    @Address			nvarchar(255),
        @FromWarehouseCode	nvarchar(50),
        @ToWarehouseCode	nvarchar (50),
        @DeliveryNoteNumber	nvarchar (30),
        @ContainerNumber	nvarchar (30),
        @SealNumber			nvarchar(30),
        @DeliveryDate		datetime,
        @Remarks			nvarchar(255),
        @NumberOfLines		int,
        @Additional1		nvarchar(255),
        @Additional2		nvarchar(255),
        @Additional3		nvarchar(255),
        @Additional4		nvarchar(255),
        @Additional5		nvarchar(255),
        @ProcessedDate		datetime,
        @Additional6		nvarchar(255),
        @Additional7		nvarchar(255),
        @Additional8		nvarchar(255),
        @Additional9		nvarchar(255),
        @Additional10		nvarchar(255),
        @LineNumber			int,
		@ProductCode		nvarchar(30),
		@Product			nvarchar(50),
		@SKUCode			nvarchar(50),
		@Batch				nvarchar(50),
		@Quantity			float,
		@Weight				decimal(13,3)
		
		

     
SELECT  t1.InterfaceExportPOHeaderId, 
		t1.PrimaryKey, 
		t1.OrderNumber,
		t1.RecordType, 
		t1.RecordStatus,
		t1.SupplierCode,
		t1.Supplier,
		t1.Address,
		t1.FromWarehouseCode,
		t1.ToWarehouseCode,
		t1.DeliveryNoteNumber,
		t1.ContainerNumber,
		t1.SealNumber,
		t1.DeliveryDate,
		t1.Remarks,
		t1.NumberOfLines,
		t1.Additional1,
		t1.Additional2,
		t1.Additional3,
		t1.Additional4,
		t1.Additional5,
		t1.ProcessedDate,
		t1.Additional6,
		t1.Additional7,
		t1.Additional8,
		t1.Additional9,
		t1.Additional10,
		t2.LineNumber,
		t2.ProductCode,
		t2.Product,
		t2.SKUCode,
		t2.Batch,
		t2.Quantity,
		t2.Weight
		
FROM InterfaceExportPOHeader t1
left join InterfaceExportPODetail t2 on t1.InterfaceExportPOHeaderId = t2.InterfaceExportPOHeaderId
where @InterfaceExportPOHeaderId = t1.InterfaceExportPOHeaderId

SELECT	@InterfaceExportPOHeaderId [ID],
		@PrimaryKey [PrimaryKey],
		@OrderNumber [Order Number],
		@RecordType [Record Type],
		@RecordStatus [Record Status],
		@SupplierCode [Supplier Code],
		@Supplier [Supplier],
		@Address [Address],
		@FromWarehouseCode [From Warehouse],
		@ToWarehouseCode [To Warehouse],
		@DeliveryNoteNumber [Delivery Note Number],
		@ContainerNumber [Container Number],
		@SealNumber [Seal Number],
		@DeliveryDate [Delivery Date],
		@Remarks [Remarks],
		@NumberOfLines [Number of Lines],
		@Additional1 [Additional 1],
		@Additional2 [Additional 2],
		@Additional3 [Additional 3],
		@Additional4 [Additional 4],
		@Additional5 [Additional 5],
		@ProcessedDate [Processed Date],
		@Additional1 [Additional 6],
		@Additional1 [Additional 7],
		@Additional1 [Additional 8],
		@Additional1 [Additional 9],
		@Additional1 [Additional 10],
		@LineNumber [Line Number],
		@ProductCode [Product Code],
		@Product [Product],
		@SKUCode [SKUCode],
		@Batch [Batch],
		@Quantity [Quantity],
		@Weight [Weight]

end
IF @ReportType = 'R'   

begin     
             
exec [InterfaceDashboardPO_UI] @InterfaceExportPOHeaderId             
  
end  




--exec [p_Report_GetInterfaceDashBoardPODtls] 'Failed', '126', 'V'


