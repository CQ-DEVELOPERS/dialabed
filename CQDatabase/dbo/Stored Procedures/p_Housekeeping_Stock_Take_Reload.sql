﻿--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Reload') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Housekeeping_Stock_Take_Reload
--    IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Reload') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Housekeeping_Stock_Take_Reload >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Housekeeping_Stock_Take_Reload >>>'
--END
--go

/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Reload
  ///   Filename       : p_Housekeeping_Stock_Take_Reload.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Oct 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter
  ///   Modified Date  : 2018-01-02
  ///   Details        : Added the multiwarehouse functionallity so that the screens do not overwrite each other.
  /// </newpara>
*/
CREATE PROCEDURE [dbo].[p_Housekeeping_Stock_Take_Reload] (@WarehouseId INT)
AS
--DECLARE @WarehouseId int = 1
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #SOH (
		WarehouseId INT
		,WarehouseCode NVARCHAR(20)
		,StorageUnitId NVARCHAR(30)
		,BatchId NVARCHAR(50)
		,Quantity FLOAT
		,NettWeight FLOAT
		,StdCostPrice FLOAT
		)

	DECLARE @Error INT
		,@Errormsg NVARCHAR(500)
		,@GetDate DATETIME
		,@ProcessedDate DATETIME
		,@Currentdate DATETIME
		,@OldCompareDate DATETIME
		,@OldWarehouseId INT
		,@Rows INT

	CREATE TABLE #InsertDates (
		InsertDate DATETIME
		,Location VARCHAR(50)
		,PrincipalCode VARCHAR(50)
		)

	SET @OldWarehouseId = @WarehouseId

	DELETE
	FROM HousekeepingCompare
	WHERE WarehouseId = @WarehouseId

	CREATE TABLE #InsertDatesForDelete (
		InsertDate DATETIME
		,Location VARCHAR(50)
		,PrincipalCode VARCHAR(50)
		)

	INSERT INTO #InsertDatesForDelete
	SELECT MAX(InsertDate)
		,Location
		,PrincipalCode --Added location to split by warehouse
	FROM InterfaceImportSOH NOLOCK
	GROUP BY Location
		,PrincipalCode

	DECLARE @InsertDate DATETIME
		,@Location VARCHAR(50)
		,@PrincipalCode VARCHAR(50)

	IF (
			SELECT dbo.ufn_Configuration(266, @OldWarehouseId)
			) = 1 -- ST Upload - Combine warehouses
		SET @WarehouseId = NULL

	SELECT @GetDate = CONVERT(date, dbo.ufn_Getdate())

	select @Insertdate      = max(InsertDate)     from InterfaceImportSOH  (nolock)
	SELECT @CurrentDate = max(HostDate)
	FROM HousekeepingCompare(NOLOCK)
	WHERE WarehouseId = @WarehouseId

	SELECT @OldCompareDate = max(ComparisonDate)
	FROM HousekeepingCompare(NOLOCK)
	WHERE WarehouseId = @WarehouseId

	--if @Insertdate is null
	--  set @Insertdate = '1900-01-02' -- 1 day older than create date to force fresh start
	IF @CurrentDate IS NULL
		SET @CurrentDate = '1900-01-01'

	SELECT @GetDate AS '@GetDate'
		,
		@Insertdate     as '@Insertdate',
		@CurrentDate AS '@CurrentDate'
		,@OldCompareDate AS '@OldCompareDate'

	BEGIN TRANSACTION

	INSERT INTO #InsertDates
	SELECT MAX(InsertDate)
		,Location
		,PrincipalCode --Added location to split by warehouse
	FROM InterfaceImportSOH
	WHERE InsertDate >= @Currentdate
	GROUP BY Location
		,PrincipalCode

	SELECT @Rows = @@ROWCOUNT

	UPDATE InterfaceImportSOH
	SET Batch = NULL
	WHERE Batch = ''

	UPDATE HousekeepingCompare
	SET Sent = NULL
	WHERE Sent = 0

	-------------------------------
	-- There is a new SOH from Host
	-------------------------------
	IF (@Rows > 0)
	BEGIN
		IF (
				SELECT dbo.ufn_Configuration(233, @OldWarehouseId)
				) = 1 -- ST Upload - Batches on
		BEGIN
			INSERT HousekeepingCompare (
				WarehouseId
				,WarehouseCode
				,ComparisonDate
				,StorageUnitId
				,BatchId
				,WMSQuantity
				,HostQuantity
				,Sent
				)
			SELECT @WarehouseId
				,ISNULL(a.WarehouseCode, 'Unknown') AS WarehouseCode
				,@GetDate
				,sub.StorageUnitId
				,sub.BatchId
				,SUM(subl.ActualQuantity)
				,0
				,0
			FROM StorageUnitBatch sub(NOLOCK)
			INNER JOIN StorageUnitBatchLocation subl(NOLOCK) ON sub.StorageUnitBatchId = subl.StorageUnitBatchId
			INNER JOIN Location l(NOLOCK) ON subl.LocationId = l.LocationId
			INNER JOIN AreaLocation al(NOLOCK) ON l.LocationId = al.LocationId
			INNER JOIN Area a(NOLOCK) ON al.AreaId = a.AreaId
				AND a.WarehouseCode IN (
					SELECT WarehouseCode
					FROM Warehouse(NOLOCK)
					WHERE isnull(UploadToHost, 0) = 1
					)
			WHERE a.StockOnHand = 1
				AND a.WarehouseId = isnull(@WarehouseId, a.WarehouseId)
			GROUP BY a.WarehouseCode
				,sub.StorageUnitId
				,sub.BatchId
		END
		ELSE
		BEGIN -- ST Upload - Batches off
			INSERT HousekeepingCompare (
				WarehouseId
				,WarehouseCode
				,ComparisonDate
				,StorageUnitId
				,BatchId
				,WMSQuantity
				,HostQuantity
				,Sent
				)
			SELECT @WarehouseId
				,Isnull(b.WarehouseCode, 'Unknown') AS WarehouseCode
				,@GetDate
				,b.StorageUnitId
				,a.BatchId
				,sum(b.ActualQuantity)
				,0
				,0
			FROM (
				SELECT sub.StorageUnitId
					,b.BatchId
				FROM StorageUnitBatch sub(NOLOCK)
				INNER JOIN StorageUnit su(NOLOCK) ON sub.StorageUnitId = su.StorageUnitId
				INNER JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
				INNER JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
				INNER JOIN Batch b(NOLOCK) ON sub.BatchId = b.BatchId
				INNER JOIN STATUS s(NOLOCK) ON b.StatusId = s.StatusId
				WHERE b.Batch = 'default'
				) a
			INNER JOIN (
				SELECT sub.StorageUnitId
					,subl.ActualQuantity
					,a.WarehouseId
					,a.WarehouseCode
				FROM StorageUnitBatch sub
				JOIN StorageUnitBatchLocation subl ON sub.StorageUnitBatchId = subl.StorageUnitBatchId
				JOIN Location l ON subl.LocationId = l.LocationId
				JOIN AreaLocation al ON l.LocationId = al.LocationId
				JOIN Area a ON al.AreaId = a.AreaId
					AND a.WarehouseCode IN (
						SELECT WarehouseCode
						FROM Warehouse(NOLOCK)
						WHERE isnull(UploadToHost, 0) = 1
						)
				WHERE a.StockOnHand = 1
				) b ON a.StorageUnitId = b.StorageUnitId
			WHERE WarehouseId = isnull(@WarehouseId, WarehouseId)
			GROUP BY b.WarehouseCode
				,b.StorageUnitId
				,a.BatchId
		END
	END
	ELSE
		----------------------------------------------------
		-- Update the WMS Quantities from SOH On all UNSENT
		----------------------------------------------------
	BEGIN
		UPDATE HousekeepingCompare
		SET ComparisonDate = @GetDate
		WHERE CONVERT(DATE, ComparisonDate) = CONVERT(DATE, @OldCompareDate)
			AND WarehouseCode IN (
				SELECT WarehouseCode
				FROM Warehouse(NOLOCK)
				WHERE isnull(UploadToHost, 0) = 1
				)

		IF (
				SELECT dbo.ufn_Configuration(233, @OldWarehouseId)
				) = 1 -- ST Upload - Batches on
		BEGIN
			INSERT #SOH (
				WarehouseId
				,WarehouseCode
				,StorageUnitId
				,BatchId
				,Quantity
				,NettWeight
				)
			SELECT @WarehouseId
				,isnull(WarehouseCode, 'Unknown')
				,StorageUnitId
				,BatchId
				,sum(ActualQuantity)
				,SUM(Isnull(subl.NettWeight, 0))
			FROM StorageUnitBatch sub(NOLOCK)
			JOIN StorageUnitBatchLocation subl(NOLOCK) ON sub.StorageUnitBatchId = subl.StorageUnitBatchId
			JOIN Location l(NOLOCK) ON subl.LocationId = l.LocationId
			JOIN AreaLocation al(NOLOCK) ON l.LocationId = al.LocationId
			JOIN Area a(NOLOCK) ON al.AreaId = a.AreaId
				AND a.WarehouseCode IN (
					SELECT WarehouseCode
					FROM Warehouse(NOLOCK)
					WHERE isnull(UploadToHost, 0) = 1
					)
			WHERE a.StockOnHand = 1
				AND a.WarehouseId = isnull(@WarehouseId, a.WarehouseId)
			GROUP BY a.WarehouseCode
				,sub.StorageUnitId
				,sub.BatchId
		END
		ELSE
		BEGIN -- ST Upload - Batches off
			INSERT #SOH (
				WarehouseId
				,WarehouseCode
				,StorageUnitId
				,BatchId
				,Quantity
				,NettWeight
				)
			SELECT @WarehouseId
				,isnull(b.WarehouseCode, 'Unknown')
				,b.StorageUnitId
				,a.BatchId
				,sum(b.ActualQuantity)
				,SUM(Isnull(b.NettWeight, 0))
			FROM (
				SELECT sub.StorageUnitId
					,b.BatchId
				FROM StorageUnitBatch sub(NOLOCK)
				JOIN StorageUnit su(NOLOCK) ON sub.StorageUnitId = su.StorageUnitId
				JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
				JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
				JOIN Batch b(NOLOCK) ON sub.BatchId = b.BatchId
				JOIN STATUS s(NOLOCK) ON b.StatusId = s.StatusId
				WHERE b.Batch = 'default'
				) a
			JOIN (
				SELECT sub.StorageUnitId
					,subl.ActualQuantity
					,a.WarehouseId
					,a.WarehouseCode
					,subl.NettWeight
				FROM StorageUnitBatch sub
				JOIN StorageUnitBatchLocation subl ON sub.StorageUnitBatchId = subl.StorageUnitBatchId
				JOIN Location l ON subl.LocationId = l.LocationId
				JOIN AreaLocation al ON l.LocationId = al.LocationId
				JOIN Area a ON al.AreaId = a.AreaId
					AND a.WarehouseCode IN (
						SELECT WarehouseCode
						FROM Warehouse(NOLOCK)
						WHERE isnull(UploadToHost, 0) = 1
						)
				WHERE a.StockOnHand = 1
				) b ON a.StorageUnitId = b.StorageUnitId
			WHERE WarehouseId = isnull(@WarehouseId, WarehouseId)
			GROUP BY b.WarehouseCode
				,b.StorageUnitId
				,a.BatchId
		END

		DECLARE @InboundSequence INT

		SELECT @InboundSequence = max(InboundSequence)
		FROM PackType(NOLOCK)

		UPDATE hc
		SET WMSQuantity = isnull(s.Quantity, 0)
			,CustomField1 = isnull(s.NettWeight, 0)
			,Sent = 0
			,StdCostPrice = s.StdCostPrice
		FROM HousekeepingCompare hc(NOLOCK)
		LEFT JOIN #SOH S ON isnull(s.warehouseId, - 1) = isnull(hc.WarehouseId, - 1)
			AND s.WarehouseCode = hc.Warehousecode
			AND s.StorageUnitId = hc.StorageUnitId
			AND s.BatchId = hc.BatchId
		WHERE CONVERT(DATE, ComparisonDate) = CONVERT(DATE, @GetDate)
			AND isnull(hc.sent, 0) = 0

		---------------------------------------------
		-- Insert new SOH Quantities
		---------------------------------------------
		INSERT HousekeepingCompare (
			WarehouseId
			,WarehouseCode
			,ComparisonDate
			,StorageUnitId
			,BatchId
			,WMSQuantity
			,HostQuantity
			,Sent
			,StdCostPrice
			)
		SELECT @WarehouseId
			,s.WarehouseCode
			,@GetDate
			,s.StorageUnitId
			,s.BatchId
			,s.Quantity
			,0
			,0
			,s.StdCostPrice
		FROM #SOH s
		LEFT JOIN HousekeepingCompare hc(NOLOCK) ON hc.WarehouseCode = s.WarehouseCode
			AND hc.StorageUnitId = s.StorageUnitId
			AND hc.BatchId = s.BatchId
			AND CONVERT(DATE, hc.Comparisondate) = CONVERT(DATE, @GetDate)
			AND hc.WarehouseId = isnull(@WarehouseId, - 1)
		WHERE hc.StorageUnitId IS NULL
	END

	UPDATE hc
	SET WarehouseCode = wm.HostCode
	FROM HousekeepingCompare hc(NOLOCK)
	JOIN StorageUnit su(NOLOCK) ON hc.StorageUnitId = su.StorageUnitId
	JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
	JOIN Principal pn(NOLOCK) ON p.PrincipalId = pn.PrincipalId
	JOIN WarehouseMatrix wm(NOLOCK) ON pn.PrincipalId = wm.PrincipalId
		AND hc.WarehouseCode = wm.WarehouseCode
	WHERE CONVERT(DATE, hc.ComparisonDate) = CONVERT(DATE, @GetDate)

	---------------------------------------------
	-- Update Host Quantities from Latest Import
	---------------------------------------------
	IF (
			SELECT dbo.ufn_Configuration(232, @OldWarehouseId)
			) = 1 -- ST Upload - Host Id on|off
	BEGIN
		UPDATE hc
		SET HostQuantity = host.Quantity
			,HostDate = host.InsertDate
			,UnitPrice = host.UnitPrice
			,StdCostPrice = host.StdCostPrice
		FROM InterfaceImportSOH host(NOLOCK)
		JOIN #InsertDates ins ON CONVERT(DATE, host.insertDate) = CONVERT(DATE, ins.InsertDate)
			AND isnull(host.PrincipalCode, '-1') = isnull(ins.PrincipalCode, '-1')
		JOIN Warehouse w(NOLOCK) ON w.HostId = host.Location
		left
		join Principal pr (nolock) on pr.PrincipalCode = host.PrincipalCode
		JOIN Product p(NOLOCK) ON host.ProductCode = p.HostId
			AND isnull(w.PrincipalId, - 1) = isnull(p.PrincipalId, - 1)
		JOIN StorageUnit su(NOLOCK) ON p.ProductId = su.ProductId
		JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
			AND isnull(host.SKUCode, sku.SKUCode) = sku.SKUCode
		JOIN Batch b(NOLOCK) ON isnull(host.Batch, 'Default') = b.Batch
		JOIN HousekeepingCompare hc(NOLOCK) ON su.StorageUnitId = hc.StorageUnitId
			AND b.BatchId = hc.BatchId
			AND isnull(w.WarehouseCode, 'Unknown') = hc.WarehouseCode
		WHERE CONVERT(DATE, hc.ComparisonDate) = CONVERT(DATE, @GetDate)
			AND isnull(hc.sent, 0) = 0
	END
	ELSE
	BEGIN
		UPDATE hc
		SET HostQuantity = host.Quantity
			,HostDate = host.Insertdate
			,UnitPrice = host.UnitPrice
			,StdCostPrice = host.StdCostPrice
		FROM InterfaceImportSOH host(NOLOCK)
		JOIN #InsertDates ins ON CONVERT(DATE, host.insertDate) = CONVERT(DATE, ins.InsertDate)
			AND isnull(host.PrincipalCode, '-1') = isnull(ins.PrincipalCode, '-1')
		LEFT JOIN Product p(NOLOCK) ON host.ProductCode = p.ProductCode
		LEFT JOIN Principal pr(NOLOCK) ON host.PrincipalCode = pr.PrincipalCode
			AND p.PrincipalId = pr.PrincipalId
		LEFT JOIN StorageUnit su(NOLOCK) ON p.ProductId = su.ProductId
		LEFT JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
			AND isnull(host.SKUCode, sku.SKUCode) = sku.SKUCode
		LEFT JOIN Batch b(NOLOCK) ON isnull(host.Batch, 'Default') = b.Batch
		JOIN HousekeepingCompare hc(NOLOCK) ON su.StorageUnitId = hc.StorageUnitId
			AND b.BatchId = hc.BatchId
			--AND isnull(host.Location, 'Unknown') = hc.WarehouseCode
		WHERE 
			hc.ComparisonDate = @GetDate
			AND isnull(hc.sent, 0) = 0
			and CONVERT(DATE, host.InsertDate) = CONVERT(DATE, @Insertdate)
	END

	------------------------------------------------------
	-- Insert products from host that do not exist on WMS
	------------------------------------------------------
	IF (
			SELECT dbo.ufn_Configuration(232, @OldWarehouseId)
			) = 1 -- ST Upload - Host Id on|off
	BEGIN
		INSERT HousekeepingCompare (
			WarehouseId
			,WarehouseCode
			,ComparisonDate
			,HostDate
			,StorageUnitId
			,BatchId
			,WMSQuantity
			,HostQuantity
			,UnitPrice
			,Sent
			,StdCostPrice
			)
		SELECT @WarehouseId
			,w.WarehouseCode
			,@getdate
			,s.Insertdate
			,su.StorageUnitId
			,b.BatchId
			,0
			,sum(s.Quantity)
			,min(s.UnitPrice) AS UnitPrice
			,0
			,s.StdCostPrice
		FROM InterfaceImportSOH s(NOLOCK)
		left 
		join  Principal pr (nolock) on pr.PrincipalCode = s.PrincipalCode
		JOIN #InsertDates ins ON CONVERT(DATE, s.insertDate) = CONVERT(DATE, ins.InsertDate)
			AND isnull(s.PrincipalCode, '-1') = isnull(ins.PrincipalCode, '-1')
		--Left
		JOIN Product p(NOLOCK) ON s.ProductCode = p.HostId
		JOIN StorageUnit su(NOLOCK) ON p.ProductId = su.ProductId
		JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
			AND isnull(s.SKUCode, sku.SKUCode) = sku.SKUCode
		JOIN Batch b(NOLOCK) ON isnull(s.Batch, 'Default') = b.Batch
		JOIN StorageUnitBatch sub(NOLOCK) ON b.batchId = sub.batchId
			AND su.StorageUnitId = sub.StorageUnitId
		JOIN Warehouse w(NOLOCK) ON w.HostId = s.Location
			AND isnull(w.PrincipalId, - 1) = isnull(p.PrincipalId, - 1)
		LEFT JOIN HousekeepingCompare hc(NOLOCK) ON hc.WarehouseCode = w.WarehouseCode
			AND hc.StorageUnitId = su.StorageUnitId
			AND hc.BatchId = b.BatchId
			AND CONVERT(DATE, hc.Comparisondate) = CONVERT(DATE, @GetDate)
		WHERE hc.HousekeepingCompareId IS NULL
			AND s.Location IN (
				SELECT HostId
				FROM Warehouse(NOLOCK)
				WHERE isnull(UploadToHost, 0) = 1
				)
		GROUP BY w.WarehouseCode
			,s.InsertDate
			,su.StorageUnitId
			,b.BatchId
			,su.StdCostPrice
			,s.StdCostPrice

	END
	ELSE
	BEGIN
		INSERT HousekeepingCompare (
			WarehouseId
			,WarehouseCode
			,ComparisonDate
			,HostDate
			,StorageUnitId
			,BatchId
			,WMSQuantity
			,HostQuantity
			,UnitPrice
			,Sent
			,StdCostPrice
			)
		SELECT @WarehouseId
			,s.Location
			,@getdate
			,s.Insertdate
			,su.StorageUnitId
			,b.BatchId
			,0
			,sum(s.Quantity)
			,min(s.UnitPrice) AS UnitPrice
			,0
			,s.StdCostPrice
		FROM InterfaceImportSOH s(NOLOCK)
		JOIN #InsertDates ins ON CONVERT(DATE, s.insertDate) = CONVERT(DATE, ins.InsertDate)
			AND isnull(s.PrincipalCode, '-1') = isnull(ins.PrincipalCode, '-1')
		LEFT JOIN Product p(NOLOCK) ON s.ProductCode = p.ProductCode
		LEFT JOIN Principal pr(NOLOCK) ON p.PrincipalId = pr.PrincipalId
			AND pr.PrincipalCode = s.PrincipalCode
		LEFT JOIN StorageUnit su(NOLOCK) ON p.ProductId = su.ProductId
		LEFT JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
			AND isnull(s.SKUCode, sku.SKUCode) = sku.SKUCode
		LEFT JOIN Batch b(NOLOCK) ON isnull(s.Batch, 'Default') = b.Batch
		JOIN Warehouse w(NOLOCK) ON w.WarehouseCode = s.Location
			AND w.ParentWarehouseId = isnull(@WarehouseId, w.WarehouseId)
		LEFT JOIN HousekeepingCompare hc(NOLOCK) ON hc.WarehouseCode = s.location
			AND hc.StorageUnitId = su.StorageUnitId
			AND hc.BatchId = b.BatchId
			AND CONVERT(DATE, hc.Comparisondate) = @GetDate
		WHERE hc.HousekeepingCompareId IS NULL
			AND s.Location IN (
				SELECT WarehouseCode
				FROM Warehouse(NOLOCK)
				WHERE isnull(UploadToHost, 0) = 1
				)
		GROUP BY s.Location
			,s.InsertDate
			,su.StorageUnitId
			,b.BatchId
			,su.StdCostPrice
			,s.StdCostPrice

	END

	--UPDATE hc
	--SET StdCostPrice = isoh.StdCostPrice,
	--	PrincipalId = isnull(pr.PrincipalId,p.PrincipalId),
	--	DCCode = isoh.DCCode,
	--	hc.HostQuantity = isoh.Quantity
	--FROM HousekeepingCompare hc(NOLOCK)
	--JOIN StorageUnit su(NOLOCK) ON hc.StorageUnitId = su.StorageUnitId
	--JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
	--JOIN InterfaceImportSOH isoh(NOLOCK) ON p.ProductCode = isoh.ProductCode
	--left
	--join Principal pr (nolock) on pr.PrincipalCode = isoh.PrincipalCode
	--where hc.PrincipalId = pr.PrincipalId

	
	update hc
	set PrincipalId = pr.PrincipalId,
		DCCode = isoh.DCCode,
		StdCostPrice = isoh.StdCostPrice,
		hc.HostQuantity = isoh.Quantity
	from InterfaceImportSOH isoh(NOLOCK)
	join Product p(NOLOCK) ON isoh.ProductCode = p.ProductCode
	join StorageUnit su(NOLOCK) ON p.ProductId = su.ProductId
	join Principal pr (nolock) on p.PrincipalId = pr.PrincipalId
	join HousekeepingCompare hc (nolock) on hc.StorageUnitId = su.StorageUnitId
	where pr.Principalcode = 'DAB'
	and isoh.PrincipalCode = 'DAB'
	--and hc.PrincipalId is not null

	update hc
	set PrincipalId = pr.PrincipalId,
		DCCode = isoh.DCCode,
		StdCostPrice = isoh.StdCostPrice,
		hc.HostQuantity = isoh.Quantity
	from InterfaceImportSOH isoh(NOLOCK)
	join Product p(NOLOCK) ON isoh.ProductCode = p.ProductCode
	join StorageUnit su(NOLOCK) ON p.ProductId = su.ProductId
	join Principal pr (nolock) on p.PrincipalId = pr.PrincipalId
	join HousekeepingCompare hc (nolock) on hc.StorageUnitId = su.StorageUnitId
	where pr.Principalcode = 'TBS'
	and isoh.PrincipalCode = 'TBS'
	--and hc.PrincipalId is not null

	update hc
	set PrincipalId = p.PrincipalId,
		hc.HostQuantity = 0,
		StdCostPrice = 0
	from HousekeepingCompare hc (nolock)
	join StorageUnit su (nolock) on hc.StorageUnitId = su.StorageUnitId
	join Product p (nolock) on p.ProductId = su.ProductId
	where hc.PrincipalId is null

	DROP TABLE #SOH

	DROP TABLE #InsertDates

	DROP TABLE #InsertDatesForDelete

	COMMIT TRANSACTION
END
--go
--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Reload') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Housekeeping_Stock_Take_Reload >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Housekeeping_Stock_Take_Reload >>>'
--go


