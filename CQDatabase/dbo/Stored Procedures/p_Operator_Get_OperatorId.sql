﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Get_OperatorId
  ///   Filename       : p_Operator_Get_OperatorId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Get_OperatorId
(
 @Operator nvarchar(50) = null 
)
 
as
begin
	 set nocount on;
  
  declare @OperatorId int
  
  select @OperatorId = OperatorId
    from Operator
   where Operator = @Operator
  
  if @OperatorId is null
    set @OperatorId = -1
  
  select @OperatorId as 'OperatorId'
end
