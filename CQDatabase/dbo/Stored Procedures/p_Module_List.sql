﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Module_List
  ///   Filename       : p_Module_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:48
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Module table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Module.ModuleId,
  ///   Module.Module 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Module_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ModuleId
        ,'{All}' as Module
  union
  select
         Module.ModuleId
        ,Module.Module
    from Module
  
end
