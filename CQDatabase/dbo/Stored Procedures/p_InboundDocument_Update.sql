﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Update
  ///   Filename       : p_InboundDocument_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jan 2013 11:27:22
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null,
  ///   @InboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,

  ///   @OrderNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @FromLocation nvarchar(100) = null,
  ///   @ToLocation nvarchar(100) = null,
  ///   @ReasonId int = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @DivisionId int = null,
  ///   @PrincipalId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Update
(
 @InboundDocumentId int = null,
 @InboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @ReferenceNumber nvarchar(60) = null,
 @FromLocation nvarchar(100) = null,
 @ToLocation nvarchar(100) = null,
 @ReasonId int = null,
 @Remarks nvarchar(510) = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @DivisionId int = null,
 @PrincipalId int = null 
)
 
as
begin
	 set nocount on;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @DivisionId = '-1'
    set @DivisionId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
	 declare @Error int
 
  update InboundDocument
     set InboundDocumentTypeId = isnull(@InboundDocumentTypeId, InboundDocumentTypeId),
         ExternalCompanyId = isnull(@ExternalCompanyId, ExternalCompanyId),
         StatusId = isnull(@StatusId, StatusId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         DeliveryDate = isnull(@DeliveryDate, DeliveryDate),
         CreateDate = isnull(@CreateDate, CreateDate),
         ModifiedDate = isnull(@ModifiedDate, ModifiedDate),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         FromLocation = isnull(@FromLocation, FromLocation),
         ToLocation = isnull(@ToLocation, ToLocation),
         ReasonId = isnull(@ReasonId, ReasonId),
         Remarks = isnull(@Remarks, Remarks),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         BatchId = isnull(@BatchId, BatchId),
         DivisionId = isnull(@DivisionId, DivisionId),
         PrincipalId = isnull(@PrincipalId, PrincipalId) 
   where InboundDocumentId = @InboundDocumentId
  
  select @Error = @@Error
  
  
  return @Error
  
end

 
