﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_GetStorageUnitId
  ///   Filename       : p_Mobile_GetStorageUnitId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_GetStorageUnitId
	@Barcode		Varchar(100)
   ,@PrincipalId	int = NULL
   ,@WarehouseId	int = NULL
 
as
begin
  set nocount on;
  
  Declare @StorageUnitId Int
  
  if isnumeric(replace(@barcode,'P:','')) = 1 and @barcode like 'P:%'
  Begin
    
	select @StorageUnitId = StorageUnitId
	from Pallet  p (nolock)
	inner join StorageUnitBatch sub (nolock) on p.StorageUnitBatchId = sub.StorageUnitBatchId
	where p.PalletId = replace(@barcode,'P:','')
	
  end
  
  if @StorageUnitId Is Null And exists (select 1 from Pack where Barcode = @Barcode)
	select @StorageUnitId = StorageUnitId
	from Pack pk	(nolock)
	where Barcode = @Barcode
	and pk.WarehouseId = ISNULL(@WarehouseId, pk.WarehouseId)

  if @StorageUnitId Is Null And exists (select 1 from Product where ProductCode = @Barcode OR Barcode = @Barcode)
    select @StorageUnitId = StorageUnitId
    from Product p	(nolock)
    inner join StorageUnit su (nolock) on p.ProductId = su.ProductId
	where ISNULL(p.principalId, -1) = COALESCE(@PrincipalId, p.principalId, -1)
	 AND (ProductCode = @Barcode 
	  OR Barcode = @Barcode)

  if @StorageUnitId Is Null And exists (select 1 from SKU Where SKUCode = @Barcode)
    select @StorageUnitId = StorageUnitId
    from SKU (nolock)
    Inner Join StorageUnit su (nolock) on sku.SKUId = su.SKUId
    where SKUCode = @Barcode
    
  Select @StorageUnitId
  
  RETURN 0;
  
end
