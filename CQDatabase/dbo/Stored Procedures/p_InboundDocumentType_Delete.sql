﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentType_Delete
  ///   Filename       : p_InboundDocumentType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 10:28:17
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentType_Delete
(
 @InboundDocumentTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InboundDocumentType
     where InboundDocumentTypeId = @InboundDocumentTypeId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_InboundDocumentTypeHistory_Insert
         @InboundDocumentTypeId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
