﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Ageing
  ///   Filename       : p_Report_Ageing.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 21 July 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : March 2011
  ///   Details        : Add weight for Enterprise
  /// </newpara>
*/
CREATE procedure p_Report_Ageing
(
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @BatchId           int = null,
 @Dayvalue          int = 0,
 @PrincipalId		int = null
)

 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   StorageUnitId				int, 
   SKUId					    int, 
   ProductId				    int, 
   StorageUnitBatchId		    int,
   StatusId				        int, 
   ProductCode				    nvarchar(50), 
   Product					    nvarchar(50), 
   Barcode					    nvarchar(50), 
   Status					    nvarchar(50),
   BatchId					    int,
   WareHouseID				    int,
   Batch					    nvarchar(50),
   CreateDate				    datetime,
   ExpiryDate				    datetime,
   IncubationDays				int,
   ShelfLifeDays			    int,
   ShelfLifeDaysRemaining		int,
   ShelfLifePercentRemaining	numeric(10,2),
   Quantity				        float,
   NettWeight					float,
   PrincipalId					int,
   PrincipalCode				nvarchar(50),
   AreaId						int,
   Area							nvarchar(50),
   StockAge						int
  )
  
  Declare @datefrom               datetime,
          @ShelfLifeDaysRemaining int,
          @Incubationdays         int
  
  if @Dayvalue is null or @Dayvalue = 0
    set @Dayvalue = 1
  
  set @datefrom = DateAdd(D,Convert(int,@Dayvalue * -1),getdate())
  
  if @StorageUnitId = -1
    set @StorageUnitId = null
  
  if @BatchId = -1
    set @BatchId = null
    
  if @PrincipalId = -1 
	set @PrincipalId = null
  
  insert @TableResult
        (StorageUnitId, 
         SKUId, 
         ProductId, 
         StorageUnitBatchId,
         WarehouseId,
         ProductCode,			
         Product,				
         Barcode,				
         Status,				
         BatchId,
         Batch,				
         CreateDate,
         ExpiryDate,
         IncubationDays,       
         ShelfLifeDays,
         Quantity,
         PrincipalId,
         AreaId,
         Area,
         StockAge)
  SELECT su.StorageUnitId, 
         su.SKUId, 
         su.ProductId, 
         sub.StorageUnitBatchId,
         @WarehouseId,
         p.ProductCode, 
         p.Product, 
         null,
         s.Status,				
         b.BatchId,
         b.Batch, 
         b.CreateDate,
         b.ExpiryDate,
         isnull(b.IncubationDays, (select top 1 Incubationdays From Batch Where Batch = 'Default')),
		  CASE 
			 WHEN p.ShelfLifeDays =  0 or p.ShelfLifeDays is null THEN datediff(dd, b.CreateDate, b.ExpiryDate)
			  ELSE p.ShelfLifeDays
		  END,
 
         subl.ActualQuantity,
         p.PrincipalId,
         vl.AreaId,
         vl.Area,
         datediff(dd, b.CreateDate, GETDATE())
    FROM StorageUnitBatchLocation subl (nolock)
    join viewLocation               vl          on subl.LocationId         = vl.LocationId
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
   WHERE b.Batch != 'Default' 
     and su.StorageUnitId    = isnull(@StorageUnitId, su.StorageUnitId)
     and b.BatchId           = isnull(@BatchId, b.BatchId)
     and b.CreateDate       <= isnull(@datefrom,'')
     and subl.ActualQuantity > 0
     and vl.StockOnHand      = 1
     and vl.WarehouseId      = @WarehouseId
     and (p.PrincipalId = @PrincipalId or @PrincipalId is null)
     
 
  update @TableResult
	    Set ExpiryDate = DateAdd(D,ShelfLifeDays,CreateDate)
   where ExpiryDate is null
  
  update @TableResult
	    Set ShelfLifeDaysRemaining = DateDiff(D,getDate(),ExpiryDate)
  
  update @TableResult
	    Set ShelfLifeDaysRemaining = 0
		 where ShelfLifeDaysRemaining <= 0
  
  update @TableResult
	    Set ShelfLifePercentRemaining = Convert(float,ShelfLifeDaysRemaining) / Convert(float,ShelfLifeDays) * 100
   where Convert(float,ShelfLifeDays) > 0
  
  update @TableResult
	    Set ShelfLifePercentRemaining = 0
	  where ShelfLifePercentRemaining is null
  
  update @TableResult
     set ShelfLifePercentRemaining = 100
   where ShelfLifePercentRemaining > 100
   
  declare @InboundSequence smallint,
		 @PackTypeId	int,
		 @PackType   	nvarchar(30),
		 @ShowWeight	bit = null
		 
  select @ShowWeight = Indicator
  from Configuration where ConfigurationId = 206
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence
  
  if  @ShowWeight = 1
     update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   where pk.PackTypeId = @PackTypeId
                       and p.ProductCode = tr.ProductCode
                       and isnull(su.ProductCategory,'') != 'V')* tr.Quantity)
    
    from @TableResult tr
    
  update  td
      set td.PrincipalCode = p.PrincipalCode
     from @TableResult td
     join Principal p  (nolock) on p.PrincipalId = td.PrincipalId
  
  select StorageUnitId, 
         SKUId, 
         ProductId, 
         StorageUnitBatchId,
         WarehouseId,
         ProductCode,				
         Product,					
         Barcode,					
         Status,					
         BatchId,
         Batch,					
         CreateDate,				
         ExpiryDate,				
         IncubationDays,			
         ShelfLifeDays,			
         ShelfLifeDaysRemaining,  
         ShelfLifePercentRemaining,
         sum(Quantity) as 'Quantity',
         NettWeight,
         PrincipalCode,
         Area,
         StockAge
    from @TableResult
   group by StorageUnitId, 
            SKUId, 
            ProductId, 
            StorageUnitBatchId,
            WarehouseId,
            ProductCode,				
            Product,					
            Barcode,					
            Status,					
            BatchId,
            Batch,					
            CreateDate,				
            ExpiryDate,				
            IncubationDays,
            ShelfLifeDays,			
            ShelfLifeDaysRemaining,  
            ShelfLifePercentRemaining,
            NettWeight,
            PrincipalCode,
            Area,
            StockAge
  order by ShelfLifeDaysRemaining
  
end
