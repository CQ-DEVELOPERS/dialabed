﻿/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Store_Mixed_By_Warehouse
  ///   Filename       : p_Mobile_Store_Mixed_By_Warehouse.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Mobile_Store_Mixed_By_Warehouse (
	@operatorId INT
	,@barcode NVARCHAR(30)
	,@warehouseId INT
	,@instructionId INT OUTPUT
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Error INT
		,@Errormsg NVARCHAR(500)
		,@GetDate DATETIME
		,@Rowcount INT
		,@StatusId INT
		,@JobId INT
		,@PalletId INT
		,@StoreLocationId INT
		,@InstructionTypeCode NVARCHAR(10)
		,@PalletStatusId INT

	SELECT @GetDate = dbo.ufn_Getdate()

	IF isnumeric(@barcode) = 1
		SET @barcode = 'J:' + @barcode

	IF isnumeric(replace(@barcode, 'J:', '')) = 1
	BEGIN
		-- First Check if Inter Branch Transfer Putaway
		SELECT @JobId = max(JobId)
		FROM Job(NOLOCK)
		WHERE ReferenceNumber = @barcode

		IF @JobId IS NULL
			SELECT @JobId = replace(@barcode, 'J:', '')
				,@barcode = NULL
	END

	IF isnumeric(replace(@barcode, 'P:', '')) = 1
		SELECT @PalletId = replace(@barcode, 'P:', '')
			,@barcode = NULL

	IF @PalletId IS NOT NULL
	BEGIN
		SELECT @PalletStatusId = p.StatusId
		FROM Pallet p
		INNER JOIN Instruction ins ON p.PalletId = ins.PalletId
		WHERE p.PalletId = @palletId AND ins.WarehouseId = @warehouseId

		IF @PalletStatusId = dbo.ufn_StatusId('P', 'NA')
		BEGIN
			BEGIN TRANSACTION

			SET @InstructionId = - 4

			GOTO error
		END
	END

	SELECT @JobId = JobId
	FROM Instruction i(NOLOCK)
	JOIN STATUS s(NOLOCK) ON i.StatusId = s.StatusId AND s.StatusCode IN ('W', 'S')
	JOIN InstructionType it(NOLOCK) ON i.InstructionTypeId = it.InstructionTypeId
	WHERE it.InstructionTypeCode IN ('S', 'SM', 'PR') AND i.PalletId = @PalletId AND i.WarehouseId = @warehouseId

	IF @@ROWCOUNT = 0
		SELECT @JobId = JobId
		FROM Instruction i(NOLOCK)
		JOIN STATUS s(NOLOCK) ON i.StatusId = s.StatusId AND s.StatusCode IN ('W', 'S')
		JOIN InstructionType it(NOLOCK) ON i.InstructionTypeId = it.InstructionTypeId
		WHERE it.InstructionTypeCode IN ('M') AND i.PalletId = @PalletId AND i.WarehouseId = @warehouseId

	UPDATE Instruction
	SET AutoComplete = 0
	WHERE JobId = @JobId

	IF @barcode IS NOT NULL
	BEGIN
		SELECT @JobId = max(JobId)
		FROM Job j(NOLOCK)
		JOIN STATUS s(NOLOCK) ON j.StatusId = s.StatusId
		WHERE j.ReferenceNumber = @barcode AND s.StatusCode NOT IN ('C', 'F')

		SELECT @Rowcount = @@rowcount

		IF @@rowcount = 0
			SELECT @JobId = max(JobId)
			FROM Job j(NOLOCK)
			WHERE j.ReferenceNumber = @barcode
	END

	BEGIN TRANSACTION

	SELECT TOP 1 @jobId = j.JobId
		,@instructionId = i.InstructionId
		,@StoreLocationId = i.StoreLocationId
		,@InstructionTypeCode = it.InstructionTypeCode
		,@WarehouseId = i.WarehouseId
	FROM Job j(NOLOCK)
	JOIN STATUS s(NOLOCK) ON j.StatusId = s.StatusId
	JOIN Instruction i(NOLOCK) ON j.JobId = i.JobId
	JOIN STATUS si(NOLOCK) ON i.StatusId = si.StatusId
	JOIN InstructionType it(NOLOCK) ON i.InstructionTypeId = it.InstructionTypeId
	WHERE isnull(j.OperatorId, @operatorId) = @operatorId AND j.JobId = @jobId AND si.StatusCode IN ('W', 'S') AND ((it.InstructionTypeCode = 'SM' AND s.StatusCode IN ('PR', 'A', 'S')) OR (it.InstructionTypeCode = 'PR' AND s.StatusCode IN ('PR', 'A', 'S')) OR (it.InstructionTypeCode = 'M' AND s.StatusCode IN ('S', 'W')))

	--     and isnull(i.PalletId,0)   = isnull(@PalletId,0)
	SELECT @Rowcount = @@rowcount

	IF @Rowcount = 0
	BEGIN
		SET @InstructionId = - 1
		SET @Error = 2

		GOTO error
	END

	IF @StoreLocationId IS NULL AND @InstructionTypeCode != 'M' AND dbo.ufn_Configuration(468, @WarehouseId) = 0
	BEGIN
		EXEC @Error = p_Receiving_Manual_Palletisation_Auto_Locations @InstructionId = @InstructionId

		IF @Error <> 0
			GOTO error
	END

	SELECT @StatusId = dbo.ufn_StatusId('I', 'S')

	EXEC @Error = p_Job_Update @JobId = @JobId
		,@StatusId = @StatusId
		,@OperatorId = @operatorId

	IF @Error <> 0
		GOTO error

	EXEC @Error = p_Instruction_Update @InstructionId = @InstructionId
		,@StatusId = @StatusId
		,@OperatorId = @OperatorId
		,@StartDate = @GetDate

	IF @Error <> 0
		GOTO error

	EXEC @Error = p_Operator_Update @OperatorId = @OperatorId
		,@LastInstruction = @GetDate

	IF @Error <> 0
		GOTO error

	COMMIT TRANSACTION

	RETURN

	error:

	ROLLBACK TRANSACTION

	RETURN @Error
END
