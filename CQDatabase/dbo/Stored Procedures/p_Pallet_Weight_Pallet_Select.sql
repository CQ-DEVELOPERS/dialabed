﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Weight_Pallet_Select
  ///   Filename       : p_Pallet_Weight_Pallet_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Weight_Pallet_Select
(
 @barcode varchar(30)
)
 
as
begin
	 set nocount on;
	 
	 declare @JobId              int,
          @PalletId           int
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  declare @TableResult as table
  (
   PalletId           int null,
   ProductCode	       nvarchar(30) null,
   Product	           nvarchar(50) null,
   SKUCode            nvarchar(50) null,
   Batch              nvarchar(50) null,
   Quantity           float null,
   CreateDate         datetime null
  )

  insert @TableResult
        (PalletId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         CreateDate)
  select pl.PalletId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         pl.Quantity,
         pl.CreateDate
    from Pallet             pl (nolock)
    join StorageUnitBatch  sub (nolock) on pl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit        su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product             p (nolock) on su.ProductId = p.ProductId
    join SKU               sku (nolock) on su.SKUId = sku.SKUId
    join Batch               b (nolock) on sub.BatchId = b.BatchId
   where pl.PalletId =  @PalletId
  
  select PalletId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         CreateDate
    from @TableResult tr 
end
