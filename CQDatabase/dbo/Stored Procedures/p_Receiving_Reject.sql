﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Reject
  ///   Filename       : p_Receiving_Reject.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Reject
(
 @jobId      int,
 @reasonId   int = null,
 @operatorId int
)
 
as
begin
	 set nocount on;
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @Exception           nvarchar(255),
          @ExceptionCode       nvarchar(10),
          @InstructionId       int,
          @StatusId            int,
          @InstructionTypeCode nvarchar(10),
          @WarehouseId         int,
          @StatusCode          nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @reasonId is not null
  begin
    select @Exception     = Reason,
           @ExceptionCode = ReasonCode
      from Reason (nolock)
     where ReasonId = @reasonId
  end
  
  select top 1 @InstructionId       = i.InstructionId,
               @InstructionTypeCode = it.InstructionTypeCode,
               @WarehouseId         = i.WarehouseId,
               @StatusCode          = s.StatusCode
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status           s (nolock) on j.StatusId          = s.StatusId
   where j.JobId = @JobId
  order by i.InstructionId
  
  exec @Error = p_Exception_Insert 
   @ExceptionId    = null,
   @InstructionId  = @InstructionId,
   @ReasonId       = @ReasonId,
   @Exception      = @Exception,
   @ExceptionCode  = @ExceptionCode,
   @CreateDate     = @GetDate
  
  if @Error <> 0
    goto error
  
  if @InstructionTypeCode in ('S','SM')
  begin
    select @StatusId = dbo.ufn_StatusId('R','RE')
    
    exec @Error = p_Job_Update
     @JobId          = @JobId,
     @StatusId       = @StatusId
    
    if @Error <> 0
      goto error
  end
  else if @InstructionTypeCode = 'PR'
  begin
    select @StatusId = dbo.ufn_StatusId('PR','RE')
    
    exec @Error = p_Job_Update
     @JobId          = @JobId,
     @StatusId       = @StatusId
    
    if @Error <> 0
      goto error
    
    if (select dbo.ufn_Configuration(197, @WarehouseId)) = 1 and @StatusCode = 'PR'
    begin
      exec @Error = p_Production_Adjustment
       @InstructionId = @InstructionId,
       @Sign          = '-'
      
      if @Error <> 0
        goto error
    end
    
    if ((select dbo.ufn_Configuration(210, @WarehouseId)) = 1 and @StatusCode = 'PR') --or ((select dbo.ufn_Configuration(285, @WarehouseId)) = 1 and @StatusCode = 'PR') -- will make it negative
    begin
      exec @Error = p_Production_Update_Accepted_Quantity
       @InstructionId = @InstructionId,
       @Sign          = '-'
      
      if @Error <> 0
        goto error
    end
  end
  
  if dbo.ufn_Configuration(251,@WarehouseId) = 1
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @InstructionId,
     @Pick          = 1,-- (0 = false, 1 = true)
     @Store         = 0,-- (0 = false, 1 = true)
     @Confirmed     = 1 -- (0 = false, 1 = true)
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Receiving_Reject'
    rollback transaction
    return @Error
end
