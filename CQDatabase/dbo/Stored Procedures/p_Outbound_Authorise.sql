﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Authorise
  ///   Filename       : p_Outbound_Authorise.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jan 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Authorise
(
 @outboundShipmentId int,
 @issueId            int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusCode        nvarchar(10),
          @StatusId          int,
          @InstructionId     int,
          @ConfirmedQuantity float,
          @CheckQuantity     float,
          @JobId             int
  
  declare @TableUpdate as table
  (
   InstructionId     int,
   ConfirmedQuantity float,
   CheckQuantity     float
  )
  
  declare @TableJobs as table
  (
   JobId      int,
   StatusId   int,
   StatusCode nvarchar(10)
  )
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is not null
  begin
    select @StatusCode = s.StatusCode
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
      join Status                  s (nolock) on i.StatusId = s.StatusId
     where osi.OutboundShipmentId = @outboundShipmentId
    
    insert @TableUpdate
          (InstructionId,
           ConfirmedQuantity,
           CheckQuantity)
    select ins.InstructionId,
           ins.ConfirmedQuantity,
           ins.CheckQuantity
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId)
     where ili.OutboundShipmentId = @outboundShipmentId
       and ins.ConfirmedQuantity <> ins.CheckQuantity
  end
  else if @IssueId is not null
  begin
    select @StatusCode = s.StatusCode
      from Issue  i
      join Status s on i.StatusId = s.StatusId
     where i.IssueId = @IssueId
    
    insert @TableUpdate
          (InstructionId,
           ConfirmedQuantity,
           CheckQuantity)
    select ins.InstructionId,
           ins.ConfirmedQuantity,
           ins.CheckQuantity
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId)
     where ili.IssueId            = @IssueId
       and ins.ConfirmedQuantity <> ins.CheckQuantity
  end
  
--  insert @TableJobs
--        (JobId,
--         StatusId,
--         StatusCode)
--  select distinct j.JobId,
--                  s.StatusId,
--                  s.StatusCode
--    from @TableUpdate tu
--    join Instruction   i (nolock) on tu.InstructionId = i.InstructionId
--    join Job           j (nolock) on i.JobId = j.JobId
--    join Status        s (nolock) on j.StatusId = s.StatusId
  
  insert @TableJobs
        (JobId,
         StatusId,
         StatusCode)
  select distinct j.JobId,
                  s.StatusId,
                  s.StatusCode
    from IssueLineInstruction ili (nolock)
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
    join Job                    j (nolock) on i.JobId           = j.JobId
    join Status                 s (nolock) on j.StatusId        = s.StatusId
   where (ili.OutboundShipmentId = isnull(@outboundShipmentId, ili.OutboundShipmentId) or (ili.OutboundShipmentId is null and @issueId is not null))
     and ili.IssueId            = isnull(@IssueId, ili.IssueId)
     and s.StatusCode          in ('A','QA')
  
--  if @StatusCode = 'A'
--  begin
--    select @StatusId = dbo.ufn_StatusId('IS','CK')
--    set @StatusCode = 'CK'
--  end
--  else if @StatusCode = 'QA'
--  begin
--    select @StatusId = dbo.ufn_StatusId('IS','CD')
--    set @StatusCode = 'CD'
--  end
  
  begin transaction
  
  while exists(select top 1 1
                 from @TableUpdate)
  begin
    select top 1
           @InstructionId     = InstructionId,
           @ConfirmedQuantity = ConfirmedQuantity,
           @CheckQuantity     = CheckQuantity
      from @TableUpdate
    
    delete @TableUpdate
     where InstructionId = @InstructionId
    
    exec @Error = p_StorageUnitBatchLocation_Reverse
     @InstructionId = @InstructionId
    ,@Pick          = 1 -- (0 = false, 1 = true)
    ,@Store         = 1 -- (0 = false, 1 = true)
    ,@Confirmed     = 1 -- (0 = false, 1 = true)
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Update
     @InstructionId     = @InstructionId
    ,@ConfirmedQuantity = @CheckQuantity
    
    if @Error <> 0
      goto error
    
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId = @InstructionId
    ,@Pick          = 1 -- (0 = false, 1 = true)
    ,@Store         = 1 -- (0 = false, 1 = true)
    ,@Confirmed     = 1 -- (0 = false, 1 = true)
    
    if @Error <> 0
      goto error
    
    update IssueLineInstruction
       set ConfirmedQuantity = 0
     where InstructionId = @InstructionId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Update_ili
     @InstructionId    = @InstructionId,
     @insConfirmed     = @CheckQuantity
    
    if @Error <> 0
      goto error
  end
  
  if not exists(select 1 from @TableJobs)
  begin
    if @StatusCode = 'A'
    begin
      select @StatusId = dbo.ufn_StatusId('IS','CK')
      set @StatusCode = 'CK'
    end
    else if @StatusCode = 'QA'
    begin
      select @StatusId = dbo.ufn_StatusId('IS','CK')
      set @StatusCode = 'CK'
    end
    
    if @StatusId is not null
    begin
      if @IssueId is not null
      begin
        update il
           set StatusId = @StatusId
          from IssueLine              il /*lock*/
          join Status                  s (nolock) on il.StatusId = s.StatusId
         where s.StatusCode in ('QA','A')
           and il.IssueId = @IssueId
        
        if @@Error <> 0
          goto error
        
        update i
           set StatusId = @StatusId
          from Issue                   i /*lock*/
          join Status                  s (nolock) on i.StatusId = s.StatusId
         where s.StatusCode in ('QA','A')
           and i.IssueId = @IssueId
        
        if @@Error <> 0
          goto error
      end
      
      if @OutboundShipmentId is not null
      begin
        update il
           set StatusId = @StatusId
          from OutboundShipmentIssue osi (nolock)
          join IssueLine              il /*lock*/ on osi.IssueId = il.IssueId
          join Status                  s (nolock) on il.StatusId = s.StatusId
         where s.StatusCode in ('QA','A')
           and osi.OutboundShipmentId = @OutboundShipmentId
        
        if @@Error <> 0
          goto error
        
        update i
           set StatusId = @StatusId
          from OutboundShipmentIssue osi (nolock)
          join Issue                   i /*lock*/ on osi.IssueId = i.IssueId
          join Status                  s (nolock) on i.StatusId = s.StatusId
         where s.StatusCode in ('QA','A')
           and osi.OutboundShipmentId = @OutboundShipmentId
        
        if @@Error <> 0
          goto error
        
        update os
           set StatusId = @StatusId
          from OutboundShipment os
          join Status            s (nolock) on os.StatusId = s.StatusId
         where s.StatusCode in ('QA','A')
           and os.OutboundShipmentId = @OutboundShipmentId
        
        if @@Error <> 0
          goto error
      end
    end
  end
  else
  begin
    while exists(select top 1 1
                   from @TableJobs)
    begin
      select top 1
             @JobId      = JobId,
             @StatusId   = StatusId,
             @StatusCode = StatusCode
        from @TableJobs
      
      delete @TableJobs
       where JobId = @JobId
      
      if @StatusCode = 'A'
      begin
        select @StatusId = dbo.ufn_StatusId('IS','RL')
        set @StatusCode = 'RL'
      end
      else if @StatusCode = 'QA'
      begin
        select @StatusId = dbo.ufn_StatusId('IS','CK')
        set @StatusCode = 'CK'
      end
      
      if @StatusId is not null
      begin
        exec @Error = p_Job_Update
         @JobId = @JobId,
         @StatusId = @StatusId
        
        if @Error <> 0
          goto error
        
--        if @IssueId is not null
--        begin
--          update il
--             set StatusId = @StatusId
--            from IssueLine              il /*lock*/
--            join Status                  s (nolock) on il.StatusId = s.StatusId
--           where s.StatusCode in ('QA','A')
--             and il.IssueId = @IssueId
--          
--          if @@Error <> 0
--            goto error
--          
--          update i
--             set StatusId = @StatusId
--            from Issue                   i /*lock*/
--            join Status                  s (nolock) on i.StatusId = s.StatusId
--           where s.StatusCode in ('QA','A')
--             and i.IssueId = @IssueId
--          
--          if @@Error <> 0
--            goto error
--        end
--        
--        if @OutboundShipmentId is not null
--        begin
--          update il
--             set StatusId = dbo.ufn_StatusId('IS','CD')
--            from OutboundShipmentIssue osi (nolock)
--            join IssueLine              il /*lock*/ on osi.IssueId = il.IssueId
--            join Status                  s (nolock) on il.StatusId = s.StatusId
--           where s.StatusCode in ('QA','A')
--             and osi.OutboundShipmentId = @OutboundShipmentId
--          
--          if @@Error <> 0
--            goto error
--          
--          update i
--             set StatusId = dbo.ufn_StatusId('IS','CD')
--            from OutboundShipmentIssue osi (nolock)
--            join Issue                   i /*lock*/ on osi.IssueId = i.IssueId
--            join Status                  s (nolock) on i.StatusId = s.StatusId
--           where s.StatusCode = 'QA'
--             and osi.OutboundShipmentId = @OutboundShipmentId
--          
--          if @@Error <> 0
--            goto error
--          
--          update os
--             set StatusId = dbo.ufn_StatusId('IS','CD')
--            from OutboundShipment os
--            join Status            s (nolock) on os.StatusId = s.StatusId
--           where s.StatusCode = 'QA'
--             and os.OutboundShipmentId = @OutboundShipmentId
--        end
        
        exec @Error = p_Status_Rollup
         @jobId = @JobId
        
        if @Error <> 0
          goto error
      end
    end
  end

--  exec @Error = p_Outbound_Release
--   @OutboundShipmentId = @outboundShipmentId,
--   @IssueId            = @issueId,
--   @StatusCode         = @StatusCode
--  
--  if @Error <> 0
--    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Authorise'
    rollback transaction
    return @Error
end
