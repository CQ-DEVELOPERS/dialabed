﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupInstructionType_Insert
  ///   Filename       : p_OperatorGroupInstructionType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:06
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroupInstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null output,
  ///   @OperatorGroupId int = null output,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupInstructionType.InstructionTypeId,
  ///   OperatorGroupInstructionType.OperatorGroupId,
  ///   OperatorGroupInstructionType.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupInstructionType_Insert
(
 @InstructionTypeId int = null output,
 @OperatorGroupId int = null output,
 @OrderBy int = null 
)
 
as
begin
	 set nocount on;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
	 declare @Error int
 
  insert OperatorGroupInstructionType
        (InstructionTypeId,
         OperatorGroupId,
         OrderBy)
  select @InstructionTypeId,
         @OperatorGroupId,
         @OrderBy 
  
  select @Error = @@Error, @OperatorGroupId = scope_identity()
  
  
  return @Error
  
end
