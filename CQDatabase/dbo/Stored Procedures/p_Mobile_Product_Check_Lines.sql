﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Lines
  ///   Filename       : p_Mobile_Product_Check_Lines.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Mar 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Lines
(
 @JobId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  update CheckingProduct
     set Checked = (select COUNT(1)
                      from CheckingProduct (nolock)
                     where JobId = @JobId
                       and CheckQuantity is not null)
   where JobId = @JobId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  update CheckingProduct
     set Total = (select COUNT(1)
                    from CheckingProduct (nolock)
                   where JobId = @JobId)
   where JobId = @JobId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  update CheckingProduct
     set Lines = CONVERT(nvarchar(10), Checked) + ' of ' + CONVERT(nvarchar(10), Total)
   where JobId = @JobId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  return
  
  error:
    raiserror 900000 'Error executing p_Mobile_Product_Check_Lines'
    return @Error
end
