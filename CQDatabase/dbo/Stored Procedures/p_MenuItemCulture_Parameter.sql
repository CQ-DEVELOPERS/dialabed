﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItemCulture_Parameter
  ///   Filename       : p_MenuItemCulture_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:34
  /// </summary>
  /// <remarks>
  ///   Selects rows from the MenuItemCulture table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   MenuItemCulture.MenuItemCultureId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItemCulture_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as MenuItemCultureId
        ,null as 'MenuItemCulture'
  union
  select
         MenuItemCulture.MenuItemCultureId
        ,MenuItemCulture.MenuItemCultureId as 'MenuItemCulture'
    from MenuItemCulture
  
end
