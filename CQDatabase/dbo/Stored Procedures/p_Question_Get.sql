﻿

create procedure [dbo].[p_Question_Get]
(
 @questionaireId		 int = null,
 @questionaireType nvarchar(255) = null,
 @sequence			      int = null,
 @warehouseId      int = null
)
as
begin
  select top 1 
         QuestionId,
         Code,
         QuestionText,
         Sequence,
         QuestionText,
         QuestionType
    from Questions (nolock)
   where Sequence > @sequence
     and Active = 1
   order by Sequence
End

