﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_List
  ///   Filename       : p_Comment_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2014 08:27:24
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Comment table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Comment.CommentId,
  ///   Comment.Comment 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as CommentId
        ,'{All}' as Comment
  union
  select
         Comment.CommentId
        ,Comment.Comment
    from Comment
  order by Comment
  
end
