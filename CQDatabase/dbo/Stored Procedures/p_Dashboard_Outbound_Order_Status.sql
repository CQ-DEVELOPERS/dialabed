﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Order_Status
  ///   Filename       : p_Dashboard_Outbound_Order_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Order_Status
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select Legend,
	          Value
	     from DashboardOutboundOrderStatus
	    where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardOutboundOrderStatus
	   
	   insert DashboardOutboundOrderStatus
	         (WarehouseId,
           Legend,
           Value)
	   select i.WarehouseId,
	          s.Status as 'Legend',
	          count(IssueId) as 'Value'
	     from Issue  i (nolock)
	     join Status s (nolock) on i.StatusId = s.StatusId
	    where s.StatusCode in ('RL','CK','CD','D','DC')
	      and i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	   group by i.WarehouseId,
	            s.Status
	 end
end
