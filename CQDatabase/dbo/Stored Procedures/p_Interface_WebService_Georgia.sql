﻿
create procedure [dbo].[p_Interface_WebService_Georgia]
(
 @xml nvarchar(max) output
)
--with encryption
as
begin

	declare @doc xml
	set @doc = convert(xml,@xml)
	DECLARE @idoc int
	Declare @Object nvarchar(20)
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	SELECT    @Object = FileType
		FROM       OPENXML (@idoc, '/root/Header',1)
            WITH (FileType varchar(50) 'FileType')
    Declare @params nchar(50)
	Declare @SqlCommand nchar(500)
	--Declare @Xml Varchar(max)

		
	Set @params =  N'@xml nvarchar(max) output'
    Select @SqlCommand = 'exec p_Interface_WebService_' + @Object + ' @xml output'
	EXEC sp_executesql @SqlCommand, @params, @xml = @xml OUTPUT
	Set @xml = @Xml 
End




