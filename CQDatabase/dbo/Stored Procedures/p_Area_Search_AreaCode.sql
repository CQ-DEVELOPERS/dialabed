﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Search_AreaCode
  ///   Filename       : p_Area_Search_AreaCode.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Search_AreaCode
(
 @AreaCode nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  select AreaId,
         Area
    from Area
   where AreaCode = @AreaCode
  order by Area
end
