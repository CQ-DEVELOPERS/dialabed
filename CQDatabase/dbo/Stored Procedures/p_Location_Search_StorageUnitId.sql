﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_StorageUnitId
  ///   Filename       : p_Location_Search_StorageUnitId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_StorageUnitId
(
 @WarehouseId   int,
 @StorageUnitId int
)
 
as
begin
	 set nocount on;
  
  select distinct l.LocationId,
         l.Location,
         l.Ailse,
         l.RelativeValue
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
   where a.WarehouseId     = @WarehouseId
     and sub.StorageUnitId = @StorageUnitId
     and a.StockOnHand     = 1
     and l.StockTakeInd    = 0
  order by l.Ailse,
           l.RelativeValue
end
