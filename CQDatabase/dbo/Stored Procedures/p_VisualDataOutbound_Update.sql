﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualDataOutbound_Update
  ///   Filename       : p_VisualDataOutbound_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualDataOutbound_Update
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   LocationId      int,
   Location        nvarchar(15),
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   IssueId         int,
   JobId           int,
   Pallet          nvarchar(30),
   InstructionId   int,
   InstructionType nvarchar(50),
   StatusCode      nvarchar(10),
   Scanned         datetime,
   DespatchDate    datetime,
   Minutes         int
  )
  
  insert @TableResult
        (LocationId,
         Location,
         JobId,
         InstructionId,
         InstructionType,
         StatusCode,
         Scanned,
         Minutes)
  select distinct
         vl.LocationId,
         vl.Location,
         j.JobId,
         isnull(i.InstructionRefId, i.InstructionId),
         it.InstructionType,
         s.StatusCode,
         case when s.StatusCode in ('CK','CD')
         then op.Despatch
         else isnull(op.Checked, op.Checking)
         end,
         datediff(mi, op.Despatch, getdate()) as 'Minutes'
    from viewLocation vl
    join Instruction   i on vl.LocationId = i.StoreLocationId
    join InstructionType it on i.INstructionTypeId = it.InstructionTypeId
    join Job           j on i.JobId = j.JobId
    join Status        s on j.StatusId = s.StatusId
    join OutboundPerformance op on j.JobId = op.JobId
   where s.StatusCode in ('CK','CD','D','DC')
     and op.Despatch > '2008-08-01'
  
  update tr
     set OutboundDocumentId = ili.OutboundDocumentId,
         IssueId            = ili.IssueId
    from @TableResult tr
    join IssueLineInstruction ili on tr.InstructionId = ili.InstructionId
                                  or tr.InstructionId = ili.InstructionId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult tr
    join OutboundDocument od on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult tr
    join ExternalCompany ec on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set DespatchDate = i.DeliveryDate
    from @TableResult tr
    join Issue i on tr.IssueId = i.IssueId
  
  truncate table VisualDataOutbound
  
  insert VisualDataOutbound
        (Location,
         OrderNumber,
         ExternalCompany,
         JobId,
         Pallet,
         InstructionType,
         StatusCode,
         Scanned,
         DespatchDate,
         Minutes)
  select distinct
         Location,
         OrderNumber,
         ExternalCompany,
         JobId,
         '1 of 3' as 'Pallet',
         InstructionType,
         StatusCode,
         Scanned,
         DespatchDate,
         Minutes
    from @TableResult
end
