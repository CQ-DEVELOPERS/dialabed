﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Load_Remove_Job
  ///   Filename       : p_Mobile_Load_Remove_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Load_Remove_Job
(
 @warehouseId     int,
 @operatorId      int,
 @intransitLoadId int,
 @barcode         nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @IntransitLoadJobId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Errormsg = 'Error executing p_Mobile_Load_Remove_Job'
  
  begin transaction
  
  select @IntransitLoadJobId = ilj.IntransitLoadJobId
    from IntransitLoadJob ilj (nolock)
    join Job                j (nolock) on ilj.JobId = j.JobId
   where IntransitLoadId = @intransitLoadId
     and (j.ReferenceNumber = @barcode
     or   convert(nvarchar(10), j.JobId) = replace(@barcode, 'J:', ''))
  
  exec @Error = p_IntransitLoadJob_Delete
   @intransitLoadJobId = @intransitLoadJobId
  
  if @Error <> 0
    goto error
  
  commit transaction
  select @intransitLoadJobId
  return @intransitLoadJobId
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    select @Error
    return @Error
end
