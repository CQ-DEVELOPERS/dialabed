﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DangerousGoods_Update
  ///   Filename       : p_DangerousGoods_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:30
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the DangerousGoods table.
  /// </remarks>
  /// <param>
  ///   @DangerousGoodsId int = null,
  ///   @DangerousGoodsCode nvarchar(20) = null,
  ///   @DangerousGoods nvarchar(100) = null,
  ///   @Class float = null,
  ///   @UNNumber nvarchar(60) = null,
  ///   @PackClass nvarchar(60) = null,
  ///   @DangerFactor int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DangerousGoods_Update
(
 @DangerousGoodsId int = null,
 @DangerousGoodsCode nvarchar(20) = null,
 @DangerousGoods nvarchar(100) = null,
 @Class float = null,
 @UNNumber nvarchar(60) = null,
 @PackClass nvarchar(60) = null,
 @DangerFactor int = null 
)
 
as
begin
	 set nocount on;
  
  if @DangerousGoodsId = '-1'
    set @DangerousGoodsId = null;
  
  if @DangerousGoodsCode = '-1'
    set @DangerousGoodsCode = null;
  
  if @DangerousGoods = '-1'
    set @DangerousGoods = null;
  
	 declare @Error int
 
  update DangerousGoods
     set DangerousGoodsCode = isnull(@DangerousGoodsCode, DangerousGoodsCode),
         DangerousGoods = isnull(@DangerousGoods, DangerousGoods),
         Class = isnull(@Class, Class),
         UNNumber = isnull(@UNNumber, UNNumber),
         PackClass = isnull(@PackClass, PackClass),
         DangerFactor = isnull(@DangerFactor, DangerFactor) 
   where DangerousGoodsId = @DangerousGoodsId
  
  select @Error = @@Error
  
  
  return @Error
  
end
