﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_DropSequence
  ///   Filename       : p_Outbound_DropSequence.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_DropSequence
(
 @OutboundShipmentId int
)
 
as
begin
	 set nocount on;
  
  declare @Drops as table
  (
   DropSequence int identity,
   IssueId      int
  )
  
  insert @Drops
        (IssueId)
  select distinct IssueId
    from OutboundShipmentIssue (nolock)
   where OutboundShipmentId = @OutboundShipmentId
  
--  insert @Drops
--        (IssueId)
--  select 1
--  union
--  select 2
  
  select null as 'DropSequence'
  union
  select DropSequence
    from @Drops
  union
  select distinct i.DropSequence
    from OutboundShipmentIssue osi (nolock)
    join Issue                   i (nolock) on osi.IssueId = i.IssueId
   where OutboundShipmentId = @OutboundShipmentId
  order by 'DropSequence'
end
