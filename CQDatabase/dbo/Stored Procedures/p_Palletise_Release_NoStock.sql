﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Palletise_Release_NoStock
  ///   Filename       : p_Palletise_Release_NoStock.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Palletise_Release_NoStock
(
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @OperatorId         int = null,
 @StorageUnitId      int = null
)
 
as
begin
  set nocount on;
  
  declare @TableNoStock as table
  (
   InstructionTypeCode    nvarchar(10),
   OutboundDocumentTypeId int,
   OutboundDocumentId     int,
   WarehouseId            int,
   StorageUnitBatchId     int,
   StorageUnitId          int,
   StoreLocationId        int,
   Quantity               float,
   IssueLineId            int,
   InstructionLine        int,
   InstructionId          int,
   OutboundShipmentId     int,
   DropSequence           int,
   PalletQuantity         float,
   UnitWeight             float,
   UnitVolume             float,
   AreaSequence           int
  )
  
  declare @Error                  int,
          @Errormsg               nvarchar(500),
          @GetDate                datetime,
          @WarehouseId            int,
          @PriorityId             int,
          @JobId                  int,
          @StatusId               int,
          @IssueLineId            int,
          @StorageUnitBatchId     int,
          @StoreLocationId        int,
          @Quantity               float,
          @ConfirmedQuantity      float,
          @DropSequence           int,
          @InstructionLine        int,
          @InstructionId          int,
          @PickLocationId         int,
          @NoStockId              int,
          @OutboundDocumentTypeId int,
          @OutboundDocumentId     int,
          @Weight                 float,
          @Volume                 float,
          @JobWeight              float,
          @JobVolume              float,
          @UnitWeight             float,
          @UnitVolume             float,
          @Count                  int,
          @AlternatePallet        bit,
          @SubstitutePallet       bit,
          @ExternalCompanyCode    nvarchar(30),
          @ToWarehouseId          int,
          @OldInstructionId       int,
          @NewInstructionId       int,
          @OrderLineSequence      bit,
          @InstructionTypeCode    nvarchar(10),
          @AreaSequence           int,
          @PreviousSequence       int,
          @NewIssueId             int,
          @Releases               int,
          @InstructionTypeId      int,
          @UpdInstructionTypeId   int,
          @OldIssueLineId         int,
          @OldStorageUnitBatchId  int
  
  select @GetDate = dbo.ufn_Getdate()
  set @ErrorMsg = 'Error executing p_Palletise_Release_NoStock'
  select @NoStockId = dbo.ufn_StatusId('I','NS')
  set @OrderLineSequence = 0
  
  if @OutboundShipmentId = -1 and @IssueId = -1
  begin
    set @Error = -1
    goto error
  end
  
  select top 1 @PriorityId = PriorityId
    from Priority (nolock)
  order by OrderBy
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  else
  begin
    if exists(select top 1 1
                from InterfaceExportSOHeader h (nolock)
                join OutboundShipmentIssue osi (nolock) on h.IssueId = osi.IssueId
               where osi.OutboundShipmentId = @OutboundShipmentId)
    begin
      begin transaction
      set @ErrorMsg = 'Error executing p_Palletise_Release_NoStock - Document already invoiced'
      goto error
    end
    
    select @WarehouseId = WarehouseId from OutboundShipment (nolock) where OutboundShipmentId = @OutboundShipmentId
    
    select @ExternalCompanyCode = ExternalCompanyCode,
           @OrderLineSequence   = ec.OrderLineSequence
      from OutboundShipmentIssue osi (nolock)
      join Issue             i (nolock) on osi.IssueId = i.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
     where osi.OutboundShipmentId = @OutboundShipmentId
    
    select @ToWarehouseId = ToWarehouseId
      from WarehouseCodeReference (nolock)
     where WarehouseId = @WarehouseId
       and ToWarehouseCode = @ExternalCompanyCode
  end
  
  if @IssueId = -1
    set @IssueId = null
  else
  begin
    if exists(select 1
                from InterfaceExportSOHeader (nolock)
               where IssueId = @IssueId)
    begin
      begin transaction
      set @ErrorMsg = 'Error executing p_Palletise_Release_NoStock - Document already invoiced'
      goto error
    end
    
    select @WarehouseId = WarehouseId from Issue (nolock) where IssueId = @IssueId
    
    select @ExternalCompanyCode = ExternalCompanyCode,
           @OrderLineSequence   = ec.OrderLineSequence 
      from Issue             i (nolock)
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
     where IssueId = @IssueId
    
    select @ToWarehouseId = ToWarehouseId
      from WarehouseCodeReference (nolock)
     where WarehouseId = @WarehouseId
       and ToWarehouseCode = @ExternalCompanyCode
  end
  
  if @OrderLineSequence is null
    set @OrderLineSequence = 0
  
  select @Weight = Weight,
         @Volume = Volume,
         @AlternatePallet = AlternatePallet,
         @SubstitutePallet = SubstitutePallet
    from OutboundShipment (nolock)
   where OutboundShipmentId = @OutboundShipmentId
  
  if @Weight is null
  begin
    select @Weight = convert(decimal(13,3), Value)
      from Configuration
     where ConfigurationId = 54
       and WarehouseId = @WarehouseId
  end
  
  if @Volume is null
  begin
    select @Volume = convert(decimal(13,3), Value)
      from Configuration
     where ConfigurationId = 55
       and WarehouseId = @WarehouseId
  end
  
  insert @TableNoStock
        (InstructionTypeCode,
         OutboundDocumentTypeId,
         OutboundDocumentId,
         WarehouseId,
         StorageUnitBatchId,
         StorageUnitId,
         StoreLocationId,
         Quantity,
         InstructionLine,
         IssueLineId,
         InstructionId,
         OutboundShipmentId,
         DropSequence)
  select distinct it.InstructionTypeCode,
         ili.OutboundDocumentTypeId,
         ili.OutboundDocumentId,
         i.WarehouseId,
         i.StorageUnitBatchId,
         sub.StorageUnitId,
         i.StoreLocationId,
         case when si.StatusCode = 'NS'
              then i.Quantity
              else i.Quantity - i.ConfirmedQuantity
              end,
         ROW_NUMBER() OVER (partition by ili.instructionId order by ili.IssueLineId),
         ili.IssueLineId,
         ili.InstructionId,
         ili.OutboundShipmentId,
         i.DropSequence
    from IssueLineInstruction ili (nolock)
    join Instruction            i /*lock*/ on ili.InstructionId    in (i.InstructionRefId, i.InstructionId)--= isnull(i.InstructionRefId, i.InstructionId)
    join InstructionType       it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Status                 s (nolock) on i.StatusId           = s.StatusId
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Issue                iss (nolock) on ili.IssueId          = iss.IssueId
    join Status                si (nolock) on iss.StatusId         = si.StatusId
    where isnull(ili.OutboundShipmentId,0) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId,0))
    and ili.IssueId            = isnull(@IssueId, ili.IssueId)
    and ((s.StatusCode          = 'F' and i.Quantity             > Isnull(i.ConfirmedQuantity,0))
    or (s.StatusCode          = 'NS'))
    --and si.StatusCode         in ('RL','CK')
    and ili.Quantity           > Isnull(ili.ConfirmedQuantity,0)
    and sub.StorageUnitId      = isnull(@StorageUnitId, sub.StorageUnitId)
  
  update ns
     set AreaSequence = a.AreaSequence
    from @TableNoStock    ns
    join StorageUnitArea sua (nolock) on ns.StorageUnitId = sua.StorageUnitId
    join Area              a (nolock) on sua.AreaId       = a.AreaId
   where a.WarehouseId = @WarehouseId
  
  update PalletiseStock
     set AreaSequence = 99
   where AreaSequence is null
  
  update i
     set StatusId = dbo.ufn_StatusId('IS','F')
    from IssueLineInstruction ili (nolock)
    join Instruction            i /*lock*/ on ili.InstructionId    = i.InstructionId
    join Status                 s (nolock) on i.StatusId           = s.StatusId
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Issue                iss (nolock) on ili.IssueId          = iss.IssueId
    join Status                si (nolock) on iss.StatusId         = si.StatusId
   where isnull(ili.OutboundShipmentId,0) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId,0))
     and ili.IssueId            = isnull(@IssueId, ili.IssueId)
     and s.StatusCode           = 'NS'
     and si.StatusCode         in ('RL','CK')
  
  if isnull(@AlternatePallet, 0) = 0 and isnull(@SubstitutePallet, 0) = 0
    update ps
       set PalletQuantity = p.Quantity
      from @TableNoStock  ps
      join Pack            p (nolock) on ps.StorageUnitId = p.StorageUnitId
      join PackType       pt (nolock) on p.PackTypeId     = pt.PackTypeId
     where OutboundSequence = 1
       and p.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
  else if @AlternatePallet = 1 -- Palletising change for Branches 2008-05-23
    update ps
       set PalletQuantity = sku.AlternatePallet
      from @TableNoStock  ps
      join StorageUnit    su (nolock) on ps.StorageUnitId = su.StorageUnitId
      join SKU           sku (nolock) on su.SKUId         = sku.SKUId
      join SKUInfo      skui (nolock) on sku.SKUId        = skui.SKUId
     where skui.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
  else if @SubstitutePallet = 1 -- Palletising change for Exports 2008-06-10
    update ps
       set PalletQuantity = sku.SubstitutePallet
      from @TableNoStock  ps
      join StorageUnit    su (nolock) on ps.StorageUnitId = su.StorageUnitId
      join SKU           sku (nolock) on su.SKUId         = sku.SKUId
      join SKUInfo      skui (nolock) on sku.SKUId        = skui.SKUId
     where skui.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
  
  -- Set the Theoretical Weight and Volume
  update @TableNoStock
     set UnitWeight = (100.000 / convert(numeric(13,3), PalletQuantity)) * Quantity,
         UnitVolume = (100.000 / convert(numeric(13,3), PalletQuantity)) * Quantity
  
  select @InstructionTypeId = InstructionTypeId
    from InstructionType (nolock)
   where InstructionTypeCode = 'PM'
  
  select @StatusId = dbo.ufn_StatusId('IS','P')
  
  select * from @TableNoStock
  
  begin transaction
  
  while exists(select 1
                 from @TableNoStock)
  begin
    if @InstructionTypeCode = 'P'
      set @PreviousSequence = -1
    
    select top 1
           @InstructionTypeCode    = InstructionTypeCode,
           @OutboundDocumentTypeId = OutboundDocumentTypeId,
           @OutboundDocumentId     = OutboundDocumentId,
           @WarehouseId            = WarehouseId,
           @OldStorageUnitBatchId  = StorageUnitBatchId,
           @StorageUnitBatchId     = StorageUnitBatchId,
           @StoreLocationId        = StoreLocationId,
           @Quantity               = Quantity,
           @IssueLineId            = IssueLineId,
           @OldIssueLineId         = IssueLineId,
           @OldInstructionId       = InstructionId,
           @InstructionLine        = InstructionLine,
           @OutboundShipmentId     = OutboundShipmentId,
           @DropSequence           = DropSequence,
           @UnitWeight             = UnitWeight,
           @UnitVolume             = UnitVolume,
           @AreaSequence           = AreaSequence
      from @TableNoStock
     order by AreaSequence, InstructionLine
    
    if @PreviousSequence is null
      set @PreviousSequence = @AreaSequence
    
    if @OrderLineSequence = 0 AND @InstructionLine = 1
    begin
        if @PreviousSequence != @AreaSequence and @InstructionTypeCode != 'P'  
        begin  
            set @PreviousSequence = @AreaSequence -- Make them equal on change  
          
            set @JobId = null  
          
            -- Create the new job  
            exec @Error = p_Job_Insert  
             @JobId           = @JobId output,  
             @PriorityId      = @PriorityId,  
             @OperatorId      = @OperatorId,  
             @StatusId        = @StatusId,  
             @WarehouseId     = @WarehouseId,  
             @IssueLineId     = @IssueLineId,
             @Pallets         = -1
            --select 'p_Job_Insert (AreaSequence)', @JobId  
            if @Error <> 0  
              goto error  
          
            select @JobWeight = 0  
            select @JobVolume = 0  
        end  
    end  
    --if @InstructionTypeCode = 'P'  
    --  set @OrderLineSequence = 0  
      
    select @JobWeight = isnull(@JobWeight,0) + @UnitWeight  
    select @JobVolume = isnull(@JobVolume,0) + @UnitVolume  
      
    -- Move to allow full pallets to be changed into mixed pallets  
    --delete @TableNoStock  
    -- where StorageUnitBatchId = @StorageUnitBatchId  
    --   and IssueLineId        = @IssueLineId  
    --   and InstructionId      = @OldInstructionId  
    
    if @InstructionLine = 1
    begin
        set @NewInstructionId = null
        
        if @OrderLineSequence = 0 and @InstructionTypeCode != 'P'
        begin  
            exec @Error = p_Palletised_Insert  
                @WarehouseId         = @WarehouseId,  
                @OperatorId          = null,  
                @InstructionTypeCode = 'PM',  
                @StorageUnitBatchId  = @StorageUnitBatchId,  
                @PickLocationId      = null,  
                @StoreLocationId     = @StoreLocationId,  
                @Quantity            = @Quantity,  
                @InstructionRefId    = @OldInstructionId,  
                @IssueLineId         = @IssueLineId,  
                @JobId               = @JobId,  
                @OutboundShipmentId  = @OutboundShipmentId,  
                @DropSequence        = null, --@DropSequence, - allow it to automatically assgin best drop sequence  
                @InstructionId       = @InstructionId output,  
                @TransactionControl  = 0  
                
              if @Error <> 0  
                goto error
                
            select @NewInstructionId = @InstructionId
        end  
        else  
        begin  
            set @InstructionId = @OldInstructionId  
            
            exec @Error = p_Instruction_Update  
                @InstructionId     = @InstructionId,  
                @StatusId          = @NoStockId,  
                @ConfirmedQuantity = 0  
            
            if @Error <> 0  
                goto error  
        end  
          
        exec @Error = p_StorageUnitBatchLocation_Deallocate  
             @InstructionId       = @instructionId,  
             @Store               = 0 -- false  
          
        if @Error <> 0  
          goto error  
          
        exec @Error = p_Instruction_Update  
             @InstructionId = @InstructionId,  
             @ConfirmedQuantity = @Quantity  
          
        if @Error <> 0  
          goto error  
      
        --exec @Error = p_Pick_Location_Get  
        -- @WarehouseId        = @WarehouseId,  
        -- @LocationId         = @PickLocationId output,  
        -- @StorageUnitBatchId = @StorageUnitBatchId output,  
        -- @Quantity           = @Quantity  
          
        --if @Error <> 0  
        --  goto error  
    end
    else
        set @InstructionId = @NewInstructionId
              
    exec @Error = p_Despatch_Manual_Palletisation_Auto_Locations  
        @InstructionId = @InstructionId,
        @StatusUpd     = 0
        
    if @Error <> 0  
      goto error  
    
    exec @Error = p_Status_Rollup_Issue
     @JobId     = @JobId
    
    if @Error <> 0
      goto error
      
    select @PickLocationId     = PickLocationId,  
           @StorageUnitBatchId = StorageUnitBatchId  
      from Instruction  
     where InstructionId = @InstructionId  
  
    --select @InstructionId      as '@InstructionId',  
    --       @PickLocationId     as '@PickLocationId',  
    --       @StorageUnitBatchId as 'StorageUnitBatchId'  
  
    if @PickLocationId is null  
    begin  
        exec @Error = p_StorageUnitBatchLocation_Deallocate  
            @InstructionId       = @instructionId  
    
        if @Error <> 0  
            goto error  
    
        set @UpdInstructionTypeId = null  
    
        if @InstructionTypeCode != 'P'  
        begin  
            delete @TableNoStock  
            where StorageUnitBatchId = @OldStorageUnitBatchId  
            and IssueLineId        = @OldIssueLineId  
            and InstructionId      = @OldInstructionId  
        end  
        else  
        begin  
            select @UpdInstructionTypeId = @InstructionTypeId  
          
            update @TableNoStock  
               set InstructionTypeCode = 'PM'  
             where StorageUnitBatchId = @OldStorageUnitBatchId  
               and IssueLineId        = @OldIssueLineId  
               and InstructionId      = @OldInstructionId  
            --set @InstructionTypeCode = 'PM'  
        end  
    
        exec @Error = p_Instruction_Update  
             @InstructionId     = @InstructionId,  
             @PickLocationId    = @PickLocationId,  
             @StoreLocationId   = @StoreLocationId,  
             @StatusId          = @NoStockId,  
             @ConfirmedQuantity = 0,  
             @InstructionTypeId = @UpdInstructionTypeId  
        
        if @Error <> 0  
          goto error  
    end  
    else  
    begin  
        delete @TableNoStock  
        where StorageUnitBatchId = @OldStorageUnitBatchId  
         and IssueLineId        = @OldIssueLineId  
         and InstructionId      = @OldInstructionId  
    
        select @JobId = JobId  
          from Instruction (nolock)  
         where InstructionId = @InstructionId  
    
        -- Release the current job  
        select @StatusId = dbo.ufn_StatusId('IS','RL')  
    
        insert JobHistory  
            (JobId,  
             StatusId,  
             CommandType,  
             InsertDate)  
        select distinct  
             j.JobId,  
             @StatusId,  
             'Update',  
             @GetDate  
        from Job               j  
        join Status            s on j.StatusId            = s.StatusId  
       where s.StatusCode != 'NS'  
         and j.JobId       = @JobId  
    
        select @Error = @@Error  
    
        if @Error <> 0  
            goto error  
    
        update j  
         set StatusId = @StatusId,  
             OperatorId = null  
        from Job               j  
        join Status            s on j.StatusId            = s.StatusId  
       where j.JobId       = @JobId  
    
        select @Error = @@Error  
    
        if @Error <> 0  
            goto error  
    
        select @StatusId = dbo.ufn_StatusId('I','W')  
    
        exec @Error = p_Instruction_Update  
           @InstructionId      = @instructionId,  
           @PickLocationId     = @PickLocationId,  
           @StorageUnitBatchId = @StorageUnitBatchId,  
           @StatusId           = @StatusId,  
           @ConfirmedQuantity  = @Quantity  
    
        if @Error <> 0  
            goto error  
    
        exec @Error = p_StorageUnitBatchLocation_Reserve  
           @InstructionId       = @instructionId,  
           @Store               = 0 -- false  
    
        if @Error <> 0  
            goto error  
                       
        if @OrderLineSequence = 0 and @InstructionTypeCode != 'P'
        begin  
            select @ConfirmedQuantity = isnull(ConfirmedQuantity,0)
                  ,@Quantity = Quantity - isnull(ConfirmedQuantity,0)
              from IssueLineInstruction  
             where IssueLineId            = @IssueLineId  
               and InstructionId          = @OldInstructionId  
          
            -- Update IssueLineInstruction to Exclued from next run  
            exec @Error = p_IssueLineInstruction_Update  
                 @IssueLineId            = @IssueLineId,  
                 @InstructionId          = @OldInstructionId,  
                 @Quantity               = @ConfirmedQuantity -- 0  
          
            if @Error <> 0  
              goto error  
        
            select @NewIssueId = IssueId  
              from IssueLine (nolock)  
             where IssueLineId = @IssueLineId  
             
            exec @Error = p_IssueLineInstruction_Insert  
                 @OutboundDocumentTypeId = @OutboundDocumentTypeId,  
                 @OutboundShipmentId     = @OutboundShipmentId,  
                 @OutboundDocumentId     = @OutboundDocumentId,  
                 @IssueId                = @NewIssueId,  
                 @IssueLineId            = @IssueLineId,  
                 @InstructionId          = @InstructionId,  
                 @DropSequence           = @DropSequence,  
                 @Quantity               = @Quantity  
          
            if @Error <> 0  
                goto error  
      
            select @Count = count(1) from @TableNoStock  
            --select @InstructionId, @OldInstructionId, @Weight as '@Weight', @JobWeight as '@JobWeight', @Volume as '@Volume', @JobVolume as '@JobVolume', @Count as '@Count'  
            if (@Weight <= @JobWeight OR @Volume <= @JobVolume) or @Count = 0  
            begin  
              -- Check to see if all lines are no-stock and then updates job too.  
                if (select count(1) from Instruction (nolock) where JobId = @JobId and StatusId != @NoStockId) = 0  
                begin  
                    exec @Error = p_Job_Update  
                         @JobId    = @JobId,  
                         @StatusId = @NoStockId  
                  
                    if @Error <> 0  
                    begin  
                        set @Error = 1  
                        goto error  
                    end  
                end  
                else  
                begin  
                    -- Release the current job  
                    select @StatusId = dbo.ufn_StatusId('IS','RL')  
              
                    insert JobHistory  
                          (JobId,  
                           StatusId,  
                           CommandType,  
                           InsertDate)  
                    select distinct  
                           j.JobId,  
                           @StatusId,  
                           'Update',  
                           @GetDate  
                      from Job               j  
                      join Status            s on j.StatusId            = s.StatusId  
                     where s.StatusCode != 'NS'  
                       and j.JobId       = @JobId  
              
                    select @Error = @@Error  
              
                    if @Error <> 0  
                      goto error  
              
                    update j  
                       set StatusId = @StatusId,  
                           OperatorId = null  
                      from Job               j  
                      join Status            s on j.StatusId            = s.StatusId  
                     where s.StatusCode != 'NS'  
                       and j.JobId       = @JobId  
                      
                    select @Error = @@Error  
                      
                    if @Error <> 0  
                      goto error  
                end  
            
                if @Count > 0  
                begin  
                    set @JobId = null  
                      
                    -- Create the new job  
                    exec @Error = p_Job_Insert  
                     @JobId           = @JobId output,  
                     @PriorityId      = @PriorityId,  
                     @OperatorId      = @OperatorId,  
                     @StatusId        = @StatusId,  
                     @WarehouseId     = @WarehouseId,  
                     @IssueLineId     = @IssueLineId,
                     @Pallets         = -1
                    --select 'p_Job_Insert (Weight Check)', @JobId  
                    if @Error <> 0  
                      goto error  
                      
                    select @JobWeight = 0  
                    select @JobVolume = 0  
                end  
            end  
        end  
    end      
  end  
    
  --if @OrderLineSequence = 0  
  --begin  
  --  exec @Error = p_Outbound_Palletise_Check  
  --   @OutboundShipmentId = @OutboundShipmentId,  
  --   @IssueId            = @IssueId  
      
  --  if @Error <> 0  
  --  begin  
  --    set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Check'  
  --    goto error  
  --  end  
  --end  
    
  select @Releases = Releases + 1  
    from Issue (nolock)
   where IssueId = @IssueId  
    
  if @Releases is null  
    set @Releases = 1  
    
  exec @Error = p_Issue_Update  
   @IssueId  = @IssueId,  
   @Releases = @Releases  
    
  if @Error <> 0  
  begin  
    set @ErrorMsg = 'p_Palletise_Release_NoStock - Error executing p_Outbound_Palletise_Check'  
    goto error  
  end  
    
  update i
     set IssueLineId = ili.IssueLineId
    from Instruction            i (nolock)
    join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
   where ili.IssueId = @IssueId
    
  if @Error <> 0  
  begin  
    set @ErrorMsg = 'p_Palletise_Release_NoStock - Error executing p_WIP_Order_Update'  
    goto error   
  end  
    
  commit transaction  
    
  --select @PriorityId = min(PriorityId)
  --  from OutboundShipmentIssue osi (nolock)
  --  join Issue                   i (nolock) on osi.IssueId = i.IssueId
  -- where OutboundShipmentId = @outboundShipmentId
  
  --if @PriorityId is not null
  --begin
  --  exec @Error = p_WIP_Order_Update
  --   @outboundShipmentId = @outboundShipmentId,
  --   @issueId            = @IssueId,
  --   @priorityId         = @PriorityId
    
  --  if @Error <> 0
  --  begin
  --    set @ErrorMsg = 'p_Palletise_Release_NoStock - Error executing p_WIP_Order_Update'
  --    goto error 
  --  end
  --end
    
  update i  
       set ShortPicks = (select isnull(sum(ins.Quantity),0) - isnull(sum(ins.ConfirmedQuantity),0)  
                           from IssueLineInstruction ili (nolock)  
                           join Instruction          ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId)  
                           join Status                 s (nolock) on ins.StatusId      = s.StatusId  
                          where i.IssueId   = ili.IssueId)  
   from Issue i  
   left  
   join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
  where i.IssueId = @IssueId  
    and isnull(osi.OutboundShipmentId, -1) = isnull(@OutboundShipmentId,-1)  
  return  
    
  error:  
    if @Error = 0  
      set @Error = -1  
      
    raiserror 900000 @ErrorMsg  
    rollback transaction  
    return @Error  
end  
