﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Search
  ///   Filename       : p_Product_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2014 14:16:33
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(510) = null,
  ///   @DangerousGoodsId int = null,
  ///   @PrincipalId int = null,
  ///   @ProductCategoryId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Product.ProductId,
  ///   Product.StatusId,
  ///   Status.Status,
  ///   Product.ProductCode,
  ///   Product.Product,
  ///   Product.Barcode,
  ///   Product.MinimumQuantity,
  ///   Product.ReorderQuantity,
  ///   Product.MaximumQuantity,
  ///   Product.CuringPeriodDays,
  ///   Product.ShelfLifeDays,
  ///   Product.QualityAssuranceIndicator,
  ///   Product.ProductType,
  ///   Product.OverReceipt,
  ///   Product.HostId,
  ///   Product.RetentionSamples,
  ///   Product.Samples,
  ///   Product.ParentProductCode,
  ///   Product.DangerousGoodsId,
  ///   DangerousGoods.DangerousGoods,
  ///   Product.Description2,
  ///   Product.Description3,
  ///   Product.PrincipalId,
  ///   Principal.Principal,
  ///   Product.AssaySamples,
  ///   Product.Category,
  ///   Product.ProductCategoryId,
  ///   DangerousGoods.DangerousGoods,
  ///   Product.ProductAlias,
  ///   Product.ProductGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Search
(
 @ProductId int = null output,
 @StatusId int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(510) = null,
 @DangerousGoodsId int = null,
 @PrincipalId int = null,
 @ProductCategoryId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ProductId = '-1'
    set @ProductId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @DangerousGoodsId = '-1'
    set @DangerousGoodsId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @ProductCategoryId = '-1'
    set @ProductCategoryId = null;
  
 
  select
         Product.ProductId
        ,Product.StatusId
         ,StatusStatusId.Status as 'Status'
        ,Product.ProductCode
        ,Product.Product
        ,Product.Barcode
        ,Product.MinimumQuantity
        ,Product.ReorderQuantity
        ,Product.MaximumQuantity
        ,Product.CuringPeriodDays
        ,Product.ShelfLifeDays
        ,Product.QualityAssuranceIndicator
        ,Product.ProductType
        ,Product.OverReceipt
        ,Product.HostId
        ,Product.RetentionSamples
        ,Product.Samples
        ,Product.ParentProductCode
        ,Product.DangerousGoodsId
         ,DangerousGoodsDangerousGoodsId.DangerousGoods as 'DangerousGoods'
        ,Product.Description2
        ,Product.Description3
        ,Product.PrincipalId
         ,PrincipalPrincipalId.Principal as 'Principal'
        ,Product.AssaySamples
        ,Product.Category
        ,Product.ProductCategoryId
         ,DangerousGoodsProductCategoryId.DangerousGoods as 'DangerousGoodsProductCategoryId'
        ,Product.ProductAlias
        ,Product.ProductGroup
    from Product
    left
    join Status StatusStatusId on StatusStatusId.StatusId = Product.StatusId
    left
    join DangerousGoods DangerousGoodsDangerousGoodsId on DangerousGoodsDangerousGoodsId.DangerousGoodsId = Product.DangerousGoodsId
    left
    join Principal PrincipalPrincipalId on PrincipalPrincipalId.PrincipalId = Product.PrincipalId
    left
    join DangerousGoods DangerousGoodsProductCategoryId on DangerousGoodsProductCategoryId.DangerousGoodsId = Product.ProductCategoryId
   where isnull(Product.ProductId,'0')  = isnull(@ProductId, isnull(Product.ProductId,'0'))
     and isnull(Product.StatusId,'0')  = isnull(@StatusId, isnull(Product.StatusId,'0'))
     and isnull(Product.ProductCode,'%')  like '%' + isnull(@ProductCode, isnull(Product.ProductCode,'%')) + '%'
     and isnull(Product.Product,'%')  like '%' + isnull(@Product, isnull(Product.Product,'%')) + '%'
     and isnull(Product.DangerousGoodsId,'0')  = isnull(@DangerousGoodsId, isnull(Product.DangerousGoodsId,'0'))
     and isnull(Product.PrincipalId,'0')  = isnull(@PrincipalId, isnull(Product.PrincipalId,'0'))
     and isnull(Product.ProductCategoryId,'0')  = isnull(@ProductCategoryId, isnull(Product.ProductCategoryId,'0'))
  order by ProductCode
  
end
