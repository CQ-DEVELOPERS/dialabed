﻿
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Movement
  ///   Filename       : p_Mobile_Movement.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   @type = 0 -- Get Pallet Info
  ///           1 -- Cancel
  ///           2 -- Product Picked
  ///           4 -- Product Binned
  ///           5 -- Loation Error
  ///           6 -- Confirm Quantity
  ///           7 -- Find location
  ///   @operatorId        int,
  ///   @palletId          int,
  ///   @pickLocation      nvarchar(15),
  ///   @confirmedQuantity float,
  ///   @storeLocation     nvarchar(15)
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter
  ///   Modified Date  : April 2018
  ///   Details        : CQPMO-2806: Allowed for alpha numeric security codes.
  /// </newpara>
*/
CREATE procedure p_Mobile_Movement
(
 @type              int,
 @operatorId        int,
 @palletId          int,
 @newPalletId       int,
 @pickLocation      nvarchar(15),
 @confirmedQuantity float,
 @storeLocation     nvarchar(15),
 @instructionId     int
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @DwellTime          int,
          @ProductCode        nvarchar(30),
          @Product            nvarchar(50),
          @SKUCode            nvarchar(50),
          @Batch              nvarchar(50),
          @PickLocationId     int,
          @PickSecurityCode   NVARCHAR(50), --CQPMO-2806: Allowed for alpha numeric security codes.
          @StoreLocationId    int,
          @StoreSecurityCode  NVARCHAR(50), --CQPMO-2806: Allowed for alpha numeric security codes.
          @StorageUnitBatchId int,
          @StatusId           int,
          @InstructionTypeId  int,
          @Quantity           float,
          @WarehouseId        int,
          @JobId              int,
          @Weight             float
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @storeLocation = replace(@storeLocation,'L:','')
  select @pickLocation = replace(@pickLocation,'L:','')
  
  set @Error = 0
  
  exec @DwellTime = p_Mobile_Dwell_Time
   @Type       = 2,
   @OperatorId = @OperatorId
  
  select @WarehouseId = WarehouseId
    from Operator (nolock)
   where OperatorId = @operatorId
  
  if @pickLocation is null
--    select @PickLocationId = l.LocationId,
--           @pickLocation   = l.Location
--      from Instruction i
--      join Pallet      p on i.PalletId = p.PalletId
--      join Location    l on i.StoreLocationId = l.LocationId
    select @PickLocationId = l.LocationId,
           @pickLocation   = l.Location
      from Pallet      p
      join Location    l on p.LocationId = l.LocationId
     where p.PalletId = @PalletId
  else
    select top 1
           @PickLocationId = LocationId
      from Location (nolock)
     where Location = @pickLocation
   
  select top 1
         @StoreLocationId = LocationId
    from Location (nolock)
   where Location = @storeLocation
  
  select top 1
         @StorageUnitBatchId = StorageUnitBatchId
    from Pallet (nolock)
   where PalletId = @palletId
  
  select @Quantity = ActualQuantity - ReservedQuantity
    from StorageUnitBatchLocation
   where StorageUnitBatchId = @StorageUnitBatchId
     and LocationId         = @PickLocationId
  
--  select top 1
--         @InstructionId = InstructionId
--    from Instruction i (nolock)
--    join Status      s (nolock) on i.StatusId = s.StatusId
--   where i.PalletId = @palletId
  
  begin transaction
  
  if @Type = 0 -- Get Pallet Info
  begin
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output,
     @Product     output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 1 -- Cancel
  begin
    goto Result
  end
  else if @Type = 2 -- Product Picked
  begin
    set @StatusId = dbo.ufn_StatusId('I','W')
    
    select @InstructionTypeId = InstructionTypeId
      from InstructionType (nolock)
     where InstructionTypeCode = 'M'
    
    if @Quantity < 0 or @Quantity is null
    begin
      set @Error = 2
      goto Result
    end
    
    if @confirmedQuantity is null
      set @confirmedQuantity = 0
    
    if @confirmedQuantity > @Quantity or @confirmedQuantity is null
    begin
      set @Error = 2
      goto Result
    end
    
    exec @Error = p_Instruction_Insert
     @InstructionId       = @InstructionId output,
     @InstructionTypeId   = @InstructionTypeId,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @WarehouseId         = @WarehouseId,
     @StatusId            = @StatusId,
     @JobId               = @JobId,
     @OperatorId          = @OperatorId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = @StoreLocationId,
     @InstructionRefId    = null,
     @Quantity            = @confirmedQuantity,
     @ConfirmedQuantity   = 0,
     @Weight              = @Weight,
     @ConfirmedWeight     = 0,
     @PalletId            = @PalletId,
     @CreateDate          = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
--    if (select dbo.ufn_Configuration(21, @WarehouseId)) = 1
--    begin
--      exec p_Location_Get
--       @WarehouseId        = @WarehouseId,
--       @LocationId         = @StoreLocationId output,
--       @StorageUnitBatchId = @StorageUnitBatchId,
--       @Quantity           = @Quantity
--      
--      exec @Error = p_Instruction_Update
--       @InstructionId      = @InstructionId,
--       @StoreLocationId    = @StoreLocationId
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
--      
--      exec @Error = p_StorageUnitBatchLocation_Reserve
--       @InstructionId       = @InstructionId,
--       @Pick                = 0 -- false
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
--      
--      select @StoreSecurityCode = SecurityCode,
--             @StoreLocation     = Location
--        from Location (nolock)
--       where LocationId = @StoreLocationId
--    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId,
     @Pick          = 1, -- true
     @Store         = 0, -- false
     @Confirmed     = 1  -- true
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId       = @InstructionId,
     @Store               = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 4 -- Product Binned
  begin
--    if @StoreLocationId = (select StoreLocationId
--                             from Instruction (nolock)
--                            where InstructionId = @InstructionId)
--    begin
--      set @Error = 2
--      goto Result
--    end
    
    exec @Error = p_Instruction_Update
     @InstructionId   = @InstructionId,
     @StoreLocationId = @StoreLocationId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId,
     @Pick          = 0, -- false
     @Store         = 1, -- true
     @Confirmed     = 1  -- true
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId = @InstructionId,
     @Pick          = 0, -- false
     @Store         = 1, -- true
     @Confirmed     = 1  -- true
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'F' 
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @EndDate       = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  else if @Type = 5 -- Loation Error
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId       = @InstructionId,
     @Pick                = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec p_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @StoreLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity
    
    exec @Error = p_Instruction_Update
     @InstructionId      = @InstructionId,
     @StoreLocationId    = @StoreLocationId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId,
     @Pick                = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @StoreSecurityCode = SecurityCode,
           @StoreLocation     = Location
      from Location (nolock)
     where LocationId = @StoreLocationId
    
--    exec @Error = p_Mobile_Exception_Stock_Take
--     @OperatorId          = @OperatorId,
--     @StorageUnitBatchId  = 1,
--     @PickLocationId      = @PickLocationId,
--     @Quantity            = @Quantity
--    
--    if @Error <> 0
--    begin
--      set @Error = 1
--      goto Result
--    end
    
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output,
     @Product     output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 6 -- Confirm Quantity
  begin
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output,
     @Product     output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    if @ConfirmedQuantity > @Quantity
    begin
      set @Error = 7
      goto Result
    end
    else if @ConfirmedQuantity = @Quantity
    begin
      set @Error = 0
      goto Result
    end
    else
    begin
      exec @Error = p_Instruction_Update
       @InstructionId       = @InstructionId,
       @ConfirmedQuantity   = @ConfirmedQuantity,
       @PalletId            = @newPalletId
        
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId       = @InstructionId,
       @Pick                = 0 -- false
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
    end
    
    set @Error = 0
    goto result
  end
  else if @type = 7 -- Find location
  begin
    exec p_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @StoreLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity
    
    exec @Error = p_Instruction_Update
     @InstructionId      = @InstructionId,
     @StoreLocationId    = @StoreLocationId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId,
     @Pick                = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @StoreSecurityCode = SecurityCode,
           @StoreLocation     = Location
      from Location (nolock)
     where LocationId = @StoreLocationId
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  
  Result:
    if @Error <> 0
      rollback transaction
    else
      commit transaction
    
    select @Error             as 'Result',
           @DwellTime         as 'DwellTime',
           @PalletId          as 'PalletId',
           @InstructionId     as 'InstructionId',
           @ProductCode       as 'ProductCode',
           @Product           as 'Product',
           @SKUCode           as 'SKUCode',
           @Batch             as 'Batch',
           @pickLocation      as 'PickLocation',
           @storeLocation     as 'StoreLocation',
           @storeSecurityCode as 'storeSecurityCode',
           @Quantity          as 'Quantity',
           @confirmedQuantity as 'ConfirmedQuantity'
end
