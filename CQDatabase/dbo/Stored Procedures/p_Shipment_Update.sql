﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shipment_Update
  ///   Filename       : p_Shipment_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:49
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Shipment table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Route nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shipment_Update
(
 @ShipmentId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(500) = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Route nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @ShipmentId = '-1'
    set @ShipmentId = null;
  
	 declare @Error int
 
  update Shipment
     set StatusId = isnull(@StatusId, StatusId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         LocationId = isnull(@LocationId, LocationId),
         ShipmentDate = isnull(@ShipmentDate, ShipmentDate),
         Remarks = isnull(@Remarks, Remarks),
         SealNumber = isnull(@SealNumber, SealNumber),
         VehicleRegistration = isnull(@VehicleRegistration, VehicleRegistration),
         Route = isnull(@Route, Route) 
   where ShipmentId = @ShipmentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
