﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_List
  ///   Filename       : p_Receipt_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 13:28:38
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Receipt table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Receipt.ReceiptId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ReceiptId
        ,null as 'Receipt'
  union
  select
         Receipt.ReceiptId
        ,Receipt.ReceiptId as 'Receipt'
    from Receipt
  
end
