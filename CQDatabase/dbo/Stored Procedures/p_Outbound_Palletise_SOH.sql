﻿
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_SOH
  ///   Filename       : p_Outbound_Palletise_SOH.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Outbound_Palletise_SOH
(
 @WarehouseId        int,
 @ToWarehouseId      int,
 @StoreLocationId    int,
 @OutboundShipmentId int,
 @IssueId            int,
 @OperatorId         int,
 @SubstitutePallet   bit = 0,
 @JobId              int = null output
) 
AS
  declare @full_DropSequence as table
  (
   DropSequence int,
   AreaSequence int
  )
  
  declare @full_stock as table
  (
   StorageUnitBatchId numeric(10)
  )
begin
  
  begin transaction
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @DropSequence        int,
          @DropSequenceCount   int,
          @AreaSequence        int,
          @IssueLineId         int,
          @StorageUnitBatchId  int,
          @ProductCount        int,
          @PackQuantity        numeric(13,6),
          @Quantity            numeric(13,6),
          @InstructionTypeCode nvarchar(10),
          @InstructionId       int,
          @ConfirmedQuantity   numeric(13,6),
          @PickLocationId      int,
          @StatusCode          nvarchar(10),
          @PriorityId          int,
          @StatusId            int,
          @OutboundDocumentTypeCode	nvarchar(10)
  
  set @StatusId = dbo.ufn_StatusId('IS','P')
  
  set @OutboundDocumentTypeCode = (select OutboundDocumentTypeCode 
									 from issue i (nolock)
									 join OutboundDocument od (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
									 join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
								    where i.issueid = @issueid)
  
  /**********************/
  /* Process each drop  */
  /**********************/
  insert @full_DropSequence
        (DropSequence,
         AreaSequence)
  select DropSequence,
         AreaSequence
    FROM PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
  group by DropSequence, AreaSequence
  
  select @DropSequenceCount = @@RowCount
  
  while @DropSequenceCount > 0
  begin
    select top 1
           @DropSequence = DropSequence,
           @AreaSequence = AreaSequence
      from @full_DropSequence
    order by DropSequence desc, AreaSequence
    
    delete @full_DropSequence
     where DropSequence = @DropSequence
       and AreaSequence = @AreaSequence
    
    select @DropSequenceCount = @DropSequenceCount - 1
          
    select @PriorityId = min(PriorityId)
      from Priority (nolock)
    
    if (select dbo.ufn_Configuration(221, @WarehouseId)) = 0
    begin
      exec @Error = p_Job_Insert
       @JobId           = @JobId output,
       @PriorityId      = @PriorityId,
       @OperatorId      = @OperatorId,
       @StatusId        = @StatusId,
       @WarehouseId     = @WarehouseId,
       @DropSequence    = @DropSequence
      
      if @Error <> 0
        goto error
    end
    
    /*******************************/
    /* Process each stock lot item */
    /*******************************/
    insert @full_stock
    select StorageUnitBatchId
      from PalletiseStock
     where DropSequence       = @DropSequence
       and AreaSequence       = @AreaSequence
       and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
    
    select @ProductCount = @@rowcount
    
    while @ProductCount > 0
    begin
      select top 1
             @StorageUnitBatchId = StorageUnitBatchId
        from @full_stock
      set rowcount 1
      delete @full_stock
       where StorageUnitBatchId = @StorageUnitBatchId
      set rowcount 0
      select @ProductCount = @ProductCount - 1
      
      /************************************/
      /* Get the default qty weight & vol */
      /* for the given stock item   */
      /************************************/
    
      if isnull(@SubstitutePallet,0) = 0 and (select dbo.ufn_Configuration(427, @WarehouseId)) = 0
        set @InstructionTypeCode = 'P'
      else
        set @InstructionTypeCode = 'FM'
      
      if isnull(@SubstitutePallet,0) = 0
      begin
        select top 1
               @PackQuantity = p.Quantity
          from StorageUnitBatch sub (nolock)
          join Pack               p (nolock) on sub.StorageUnitId = p.StorageUnitId
          join PackType          pt (nolock) on p.PackTypeId    = pt.PackTypeId
         where sub.StorageUnitBatchId = @StorageUnitBatchId
           and p.WarehouseId          = isnull(@ToWarehouseId, @WarehouseId)
           and pt.OutboundSequence    = 1
        
        if @WarehouseId != isnull(@ToWarehouseId, @WarehouseId)
          if @PackQuantity != (select top 1 p.Quantity
                                 from StorageUnitBatch sub (nolock)
                                 join Pack               p (nolock) on sub.StorageUnitId = p.StorageUnitId
                                 join PackType          pt (nolock) on p.PackTypeId    = pt.PackTypeId
                                where sub.StorageUnitBatchId = @StorageUnitBatchId
                                  and p.WarehouseId          = @WarehouseId
                                  and pt.OutboundSequence    = 1)
            set @InstructionTypeCode = 'FM'
      end
      else
      begin
        select @PackQuantity = skui.SubstitutePallet
          from StorageUnitBatch sub (nolock)
          join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
          join SKU              sku (nolock) on su.SKUId          = sku.SKUId
          join SKUInfo         skui (nolock) on sku.SKUId         = skui.SKUId
         where skui.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
           and sub.StorageUnitBatchId = @StorageUnitBatchId
      end
      
      /**********************************/
      /* Get the actual qty of stock to */
      /* be palletised                  */
      /**********************************/
      select top 1 @Quantity    = isnull(Quantity,0),
             @IssueLineId = IssueLineId
--             @doc_no = doc_no,
--             @di_no = di_no
        from PalletiseStock
       where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and IssueId            = isnull(@IssueId, -1)
         and DropSequence       = @DropSequence
         and AreaSequence       = @AreaSequence
         and StorageUnitBatchId = @StorageUnitBatchId
         and IssueLineId       != isnull(@IssueLineId, IssueLineId + 1)
      
      select top 1 @Quantity    = sum(isnull(Quantity,0))
        from PalletiseStock
       where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and IssueId            = isnull(@IssueId, -1)
         and DropSequence       = @DropSequence
         and AreaSequence       = @AreaSequence
         and StorageUnitBatchId = @StorageUnitBatchId
      
      if @PackQuantity = 0
      begin
        RAISERROR 50028 'p_full_palletise - Pack Quantity = 0!'
        rollback transaction
        return
      end
      
      if @PackQuantity >= 9999
        --set @PackQuantity = 2
        set @PackQuantity = @Quantity
      
      /*********************************/
      /* Create each pick based on SOH */
      /*********************************/
      
      select @StoreLocationId = al.LocationId
        from StorageUnitBatch sub (nolock)
        join StorageUnitArea sua (nolock) on sub.StorageUnitId = sua.StorageUnitId
        join Area              a (nolock) on sua.AreaId = a.AreaId
        join AreaLocation     al (nolock) on a.CheckingAreaId = al.AreaId
       where sub.StorageUnitBatchId = @StorageUnitBatchId
      
      print 'p_Outbound_Palletise_SOH'
      print '@StoreLocationId'
      print @StoreLocationId
      
      if @OutboundDocumentTypeCode = 'COL'
		set @StoreLocationId = (select locationid
								  from  AreaLocation al 
								  join Area a on al.AreaId = a.AreaId
								 where Area like '%coll%'
						   	       and AreaCode = 'CK')
      
      select @Error = @@Error
    
      if @Error <> 0
        goto error
      
      while floor(@Quantity/@PackQuantity) > 0 and @Quantity > 0
      begin
        if (dbo.ufn_Configuration(172, @WarehouseId)) = 1
          set @JobId = null
        
        exec @Error = p_Palletised_Insert
         @InstructionId       = @InstructionId output,
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @InstructionTypeCode = @InstructionTypeCode,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = null,
         @StoreLocationId     = @StoreLocationId,
         @Quantity            = @Quantity,
         @IssueLineId         = @IssueLineId,
         @OutboundShipmentId  = @OutboundShipmentId,
         @DropSequence        = @DropSequence,
         @JobId               = @JobId
        
        if @Error <> 0  
          goto error
        
        set @PickLocationId = null

        exec @Error = p_Despatch_Manual_Palletisation_Auto_Locations
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId,
         @IssueLineId        = @IssueLineId,
         @InstructionId      = @InstructionId
        
        if @Error <> 0
          exec @Error = p_Instruction_No_Stock
           @InstructionId = @InstructionId
        
        if @Error <> 0
        begin
          set @Errormsg = 'Error executiing p_Despatch_Manual_Palletisation_Auto_Locations'
          goto error
        end
        
        select @ConfirmedQuantity = i.ConfirmedQuantity,
               @PickLocationId    = i.PickLocationId,
               @JobId             = i.JobId,
               @StatusCode        = s.StatusCode
          from Instruction i
          join Status      s (nolock) on i.StatusId = s.StatusId 
         where i.InstructionId = @InstructionId
        
        if isnull(@ConfirmedQuantity,0) = 0
          set @Quantity = 0
        else
          set @Quantity = @Quantity - @ConfirmedQuantity
        
        --select @Quantity as '@Quantity', @ConfirmedQuantity as '@ConfirmedQuantity', @StatusCode as '@StatusCode'
        
--        if @StatusCode != 'NS'
--        begin
--          if (select AreaCode
--                from Area          a (nolock)
--                join AreaLocation al (nolock) on a.AreaId      = al.AreaId
--                join Location      l (nolock) on al.Locationid = l.LocationId
--               where l.LocationId = @PickLocationId) = 'SP'
--          begin
--            set @Quantity = 0
--            set @ConfirmedQuantity = 0
--            delete Instruction where InstructionId = @InstructionId
--            --delete Job where JobId = @JobId
--            exec sp_grants_reseed 'Instruction'
--            --exec sp_grants_reseed 'Job'
--          end
--        end
        
        if @StatusCode = 'NS'
        begin
            delete Instruction where InstructionId = @InstructionId
            --delete Job where JobId = @JobId
            exec sp_grants_reseed 'Instruction'
            --exec sp_grants_reseed 'Job'
        end
        
        update PalletiseStock
           set Quantity           = Quantity - @ConfirmedQuantity,
               OriginalQuantity   = OriginalQuantity - @ConfirmedQuantity
         where DropSequence       = @DropSequence
           and AreaSequence       = @AreaSequence
           and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and IssueId            = isnull(@IssueId, -1)
           and StorageUnitBatchId = @StorageUnitBatchId
      end
    end
    
    delete @full_stock
  end
  delete @full_DropSequence
  
  delete PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
     and Quantity           = 0
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
END
rollback transaction


