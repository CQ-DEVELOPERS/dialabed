--IF OBJECT_ID('dbo.p_Mobile_Stock_Take_Get_Product') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Mobile_Stock_Take_Get_Product
--    IF OBJECT_ID('dbo.p_Mobile_Stock_Take_Get_Product') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Mobile_Stock_Take_Get_Product >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Mobile_Stock_Take_Get_Product >>>'
--END
--go
/*
 /// <summary>
 ///   Procedure Name : p_Mobile_Stock_Take_Get_Product
 ///   Filename       : p_Mobile_Stock_Take_Get_Product.sql
 ///   Create By      : Venkat Nallamothu
 ///   Date Created   : 29 June 2020
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
*/
CREATE PROCEDURE [dbo].[p_Mobile_Stock_Take_Get_Product]
(
@operatorId         INT,
@jobId              INT,
@locationId         INT
)
 
AS
BEGIN
	 SET NOCOUNT ON;

 DECLARE @Error                 INT,
         @Errormsg              NVARCHAR(500)

SELECT TOP 1 p.ProductId,
	   p.ProductCode,
	   p.Product,
	   pr.Principal
 FROM Instruction i  
  JOIN Status			  si (NOLOCK) ON i.StatusId			  = si.StatusId  
  JOIN Job				   j (NOLOCK) ON i.JobId			  = j.JobId 
  JOIN StorageUnitBatch  sub (NOLOCK) ON i.StorageUnitBatchId = sub.StorageUnitBatchId
  JOIN StorageUnit        su (NOLOCK) ON sub.StorageUnitId	  = su.StorageUnitId
  JOIN Product             p (NOLOCK) ON su.ProductId		  = p.ProductId
  LEFT
  JOIN Principal		  pr (NOLOCK) ON p.PrincipalId		  = pr.PrincipalId
  WHERE j.JobId = @jobId 
  AND i.PickLocationId = @locationId
  AND i.ConfirmedWeight IS NULL
  AND si.StatusCode in ('W','S')
    
END
--go
--IF OBJECT_ID('dbo.p_Mobile_Stock_Take_Get_Product') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Mobile_Stock_Take_Get_Product >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Mobile_Stock_Take_Get_Product >>>'
--go

