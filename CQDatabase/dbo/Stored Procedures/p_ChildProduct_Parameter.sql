﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ChildProduct_Parameter
  ///   Filename       : p_ChildProduct_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:21
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ChildProduct table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ChildProduct.ChildProductId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ChildProduct_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as ChildProductId
        ,null as 'ChildProduct'
  union
  select
         ChildProduct.ChildProductId
        ,ChildProduct.ChildProductId as 'ChildProduct'
    from ChildProduct
  
end
