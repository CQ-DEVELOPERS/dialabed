﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LabelingCategory_Search
  ///   Filename       : p_LabelingCategory_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:56:44
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the LabelingCategory table.
  /// </remarks>
  /// <param>
  ///   @LabelingCategoryId int = null output,
  ///   @LabelingCategory nvarchar(510) = null,
  ///   @LabelingCategoryCode nvarchar(60) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   LabelingCategory.LabelingCategoryId,
  ///   LabelingCategory.LabelingCategory,
  ///   LabelingCategory.LabelingCategoryCode,
  ///   LabelingCategory.Additional1,
  ///   LabelingCategory.Additional2,
  ///   LabelingCategory.Additional3,
  ///   LabelingCategory.Additional4,
  ///   LabelingCategory.Additional5,
  ///   LabelingCategory.ManualLabel,
  ///   LabelingCategory.ScreenGaurd 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LabelingCategory_Search
(
 @LabelingCategoryId int = null output,
 @LabelingCategory nvarchar(510) = null,
 @LabelingCategoryCode nvarchar(60) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @LabelingCategoryId = '-1'
    set @LabelingCategoryId = null;
  
  if @LabelingCategory = '-1'
    set @LabelingCategory = null;
  
  if @LabelingCategoryCode = '-1'
    set @LabelingCategoryCode = null;
  
 
  select
         LabelingCategory.LabelingCategoryId
        ,LabelingCategory.LabelingCategory
        ,LabelingCategory.LabelingCategoryCode
        ,LabelingCategory.Additional1
        ,LabelingCategory.Additional2
        ,LabelingCategory.Additional3
        ,LabelingCategory.Additional4
        ,LabelingCategory.Additional5
        ,LabelingCategory.ManualLabel
        ,LabelingCategory.ScreenGaurd
    from LabelingCategory
   where isnull(LabelingCategory.LabelingCategoryId,'0')  = isnull(@LabelingCategoryId, isnull(LabelingCategory.LabelingCategoryId,'0'))
     and isnull(LabelingCategory.LabelingCategory,'%')  like '%' + isnull(@LabelingCategory, isnull(LabelingCategory.LabelingCategory,'%')) + '%'
     and isnull(LabelingCategory.LabelingCategoryCode,'%')  like '%' + isnull(@LabelingCategoryCode, isnull(LabelingCategory.LabelingCategoryCode,'%')) + '%'
  order by LabelingCategory
  
end
 
