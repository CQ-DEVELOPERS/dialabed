﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceCustomer_Insert
  ///   Filename       : p_InterfaceCustomer_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:52
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceCustomer table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @CustomerCode nvarchar(60) = null,
  ///   @CustomerName nvarchar(510) = null,
  ///   @Class nvarchar(510) = null,
  ///   @DeliveryPoint nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null,
  ///   @Address2 nvarchar(510) = null,
  ///   @Address3 nvarchar(510) = null,
  ///   @Address4 nvarchar(510) = null,
  ///   @Modified nvarchar(20) = null,
  ///   @ContactPerson nvarchar(200) = null,
  ///   @Phone nvarchar(510) = null,
  ///   @Fax nvarchar(510) = null,
  ///   @Email nvarchar(510) = null,
  ///   @DeliveryGroup nvarchar(510) = null,
  ///   @DepotCode nvarchar(510) = null,
  ///   @VisitFrequency nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceCustomer.HostId,
  ///   InterfaceCustomer.CustomerCode,
  ///   InterfaceCustomer.CustomerName,
  ///   InterfaceCustomer.Class,
  ///   InterfaceCustomer.DeliveryPoint,
  ///   InterfaceCustomer.Address1,
  ///   InterfaceCustomer.Address2,
  ///   InterfaceCustomer.Address3,
  ///   InterfaceCustomer.Address4,
  ///   InterfaceCustomer.Modified,
  ///   InterfaceCustomer.ContactPerson,
  ///   InterfaceCustomer.Phone,
  ///   InterfaceCustomer.Fax,
  ///   InterfaceCustomer.Email,
  ///   InterfaceCustomer.DeliveryGroup,
  ///   InterfaceCustomer.DepotCode,
  ///   InterfaceCustomer.VisitFrequency,
  ///   InterfaceCustomer.ProcessedDate,
  ///   InterfaceCustomer.RecordStatus,
  ///   InterfaceCustomer.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceCustomer_Insert
(
 @HostId nvarchar(60) = null,
 @CustomerCode nvarchar(60) = null,
 @CustomerName nvarchar(510) = null,
 @Class nvarchar(510) = null,
 @DeliveryPoint nvarchar(510) = null,
 @Address1 nvarchar(510) = null,
 @Address2 nvarchar(510) = null,
 @Address3 nvarchar(510) = null,
 @Address4 nvarchar(510) = null,
 @Modified nvarchar(20) = null,
 @ContactPerson nvarchar(200) = null,
 @Phone nvarchar(510) = null,
 @Fax nvarchar(510) = null,
 @Email nvarchar(510) = null,
 @DeliveryGroup nvarchar(510) = null,
 @DepotCode nvarchar(510) = null,
 @VisitFrequency nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceCustomer
        (HostId,
         CustomerCode,
         CustomerName,
         Class,
         DeliveryPoint,
         Address1,
         Address2,
         Address3,
         Address4,
         Modified,
         ContactPerson,
         Phone,
         Fax,
         Email,
         DeliveryGroup,
         DepotCode,
         VisitFrequency,
         ProcessedDate,
         RecordStatus,
         InsertDate)
  select @HostId,
         @CustomerCode,
         @CustomerName,
         @Class,
         @DeliveryPoint,
         @Address1,
         @Address2,
         @Address3,
         @Address4,
         @Modified,
         @ContactPerson,
         @Phone,
         @Fax,
         @Email,
         @DeliveryGroup,
         @DepotCode,
         @VisitFrequency,
         @ProcessedDate,
         @RecordStatus,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
