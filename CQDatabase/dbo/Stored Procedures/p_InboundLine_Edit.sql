﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Edit
  ///   Filename       : p_InboundLine_Edit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Edit
(
 @inboundLineId int,
 @quantity      int,
 @unitPrice     numeric(13,2) = null,
 @ReasonId		int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @ReceiptLineId     int,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('ID','N')
  
  if @ReasonId in (-1,0)
    set @ReasonId = null
  
  begin transaction
  
  if @StatusId != (select StatusId
                     from InboundLine
                    where InboundLineId = @InboundLineId)
  begin
    set @Errormsg = 'Line not in the correct status'
    goto error
  end
  
  exec @Error = p_InboundLine_Update
   @InboundLineId = @inboundLineId,
   @Quantity      = @quantity,
   @UnitPrice     = @unitPrice,
   @ReasonId	  = @ReasonId
  
  if @Error <> 0
    goto error
  
  select @ReceiptLineId = ReceiptLineId
    from ReceiptLine
   where InboundLineId = @inboundLineId
  
  exec @Error = p_ReceiptLine_Update
   @ReceiptLineId    = @ReceiptLineId,
   @InboundLineId    = @inboundLineId,
   @RequiredQuantity = @quantity
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_InboundLine_Edit'
    rollback transaction
    return @Error
end

