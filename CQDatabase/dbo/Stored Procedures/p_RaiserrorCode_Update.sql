﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RaiserrorCode_Update
  ///   Filename       : p_RaiserrorCode_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:35
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the RaiserrorCode table.
  /// </remarks>
  /// <param>
  ///   @RaiserrorCodeId int = null,
  ///   @RaiserrorCode nvarchar(20) = null,
  ///   @RaiserrorMessage nvarchar(510) = null,
  ///   @StoredProcedure nvarchar(256) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RaiserrorCode_Update
(
 @RaiserrorCodeId int = null,
 @RaiserrorCode nvarchar(20) = null,
 @RaiserrorMessage nvarchar(510) = null,
 @StoredProcedure nvarchar(256) = null 
)
 
as
begin
	 set nocount on;
  
  if @RaiserrorCodeId = '-1'
    set @RaiserrorCodeId = null;
  
  if @RaiserrorCode = '-1'
    set @RaiserrorCode = null;
  
	 declare @Error int
 
  update RaiserrorCode
     set RaiserrorCode = isnull(@RaiserrorCode, RaiserrorCode),
         RaiserrorMessage = isnull(@RaiserrorMessage, RaiserrorMessage),
         StoredProcedure = isnull(@StoredProcedure, StoredProcedure) 
   where RaiserrorCodeId = @RaiserrorCodeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
