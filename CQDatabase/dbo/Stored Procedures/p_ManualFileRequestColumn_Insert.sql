﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestColumn_Insert
  ///   Filename       : p_ManualFileRequestColumn_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:01
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ManualFileRequestColumn table.
  /// </remarks>
  /// <param>
  ///   @ManualFileRequestTypeId int = null,
  ///   @ColumnName nvarchar(100) = null,
  ///   @ColumnLength int = null,
  ///   @ColumnDescription nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   ManualFileRequestColumn.ManualFileRequestTypeId,
  ///   ManualFileRequestColumn.ColumnName,
  ///   ManualFileRequestColumn.ColumnLength,
  ///   ManualFileRequestColumn.ColumnDescription 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestColumn_Insert
(
 @ManualFileRequestTypeId int = null,
 @ColumnName nvarchar(100) = null,
 @ColumnLength int = null,
 @ColumnDescription nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ManualFileRequestColumn
        (ManualFileRequestTypeId,
         ColumnName,
         ColumnLength,
         ColumnDescription)
  select @ManualFileRequestTypeId,
         @ColumnName,
         @ColumnLength,
         @ColumnDescription 
  
  select @Error = @@Error
  
  
  return @Error
  
end
