﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shipment_List
  ///   Filename       : p_Shipment_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:50
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Shipment table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Shipment.ShipmentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shipment_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ShipmentId
        ,null as 'Shipment'
  union
  select
         Shipment.ShipmentId
        ,Shipment.ShipmentId as 'Shipment'
    from Shipment
  
end
