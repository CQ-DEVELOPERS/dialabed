﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_List
  ///   Filename       : p_Location_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2013 08:13:23
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Location table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Location.LocationId,
  ///   Location.Location 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as LocationId
        ,'{All}' as Location
  union
  select
         Location.LocationId
        ,Location.Location
    from Location
  order by Location
  
end
