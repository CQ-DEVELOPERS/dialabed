﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Despatch_Manual_Palletisation_Auto_Locations_DM
  ///   Filename       : p_Despatch_Manual_Palletisation_Auto_Locations_DM.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Despatch_Manual_Palletisation_Auto_Locations_DM
(
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @IssueLineId        int = null,
 @InstructionId      int = null,
 @StatusUpd          bit = 1
)
 
as
begin
  set nocount on;
  
  declare @TableInstructions as table
  (
   OutboundShipmentId  int,
   OutboundDocumentId  int,
   ExternalCompanyId   int,
   IssueId             int,
   IssueLineId         int,
   JobId               int,
   InstructionId       int,
   InstructionRefId    int,
   WarehouseId         int,
   PickLocationId      int,
   StoreLocationId     int,
   RepackLocationId    int,
   StorageUnitBatchId  int,
   StorageUnitId       int,
   Quantity            float,
   InstructionTypeCode nvarchar(10),
   LIFO                bit default 0,
   MinimumShelfLife    numeric(13,3),
   AreaType            nvarchar(10),
   DeliveryDate        datetime
  )
  
  declare @TableIssueLines as table
  (
   IssueId             int,
   IssueLineId         int
  )
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @WarehouseId         int,
          @PickLocationId      int,
          @StoreLocationId     int,
          @StorageUnitBatchId  int,
          @Quantity            float,
          @InstructionCount    int,
          @StatusId            int,
          @CurrentIssueLineId  int,
          @JobId               int,
          @NoStockId           int,
          @IsStockId           int,
          @UpdIssueId          int,
          @UpdIssueLineId      int,
          @InstructionTypeId   int,
          @InstructionTypeCode nvarchar(10),
          @LIFO                bit,
          @MinimumShelfLife    numeric(13,3),
          @StorageUnitId       int,
          @PickPartPallet      int,
          @AreaType            nvarchar(10),
          @OutboundDocumentId  int,
          @OrginalQuantity     numeric(13,6),
          @PalletQuantity      numeric(13,6),
          @AreaCode            nvarchar(10),
          @StatusCode          nvarchar(10),
          @RepackLocationId    int,
          @DeliveryDate        datetime,
          @InstructionRefId    int,
          @AllocateLoc         bit = 1
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId is null and @IssueId is null and @IssueLineId is null and @InstructionId is null
    return
  
  select @StatusId  = dbo.ufn_StatusId('I','W')
  select @NoStockId = dbo.ufn_StatusId('I','NS')
  select @IsStockId = dbo.ufn_StatusId('IS','SA')
  
  select @AllocateLoc = dbo.ufn_Configuration(339, @WarehouseId)
  
  if @OutboundShipmentId is not null
  begin
    insert @TableInstructions
          (OutboundShipmentId,
           JobId,
           InstructionId,
           InstructionRefId,
           WarehouseId,
           PickLocationId,
           StoreLocationId,
           StorageUnitBatchId,
           Quantity,
           InstructionTypeCode)
    select @OutboundShipmentId,
           i.JobId,
           i.InstructionId,
           isnull(i.InstructionRefId, i.InstructionId),
           i.WarehouseId,
           i.PickLocationId,
           i.StoreLocationId,
           i.StorageUnitBatchId,
           i.Quantity,
           it.InstructionTypeCode
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where i.OutboundShipmentId = @OutboundShipmentId
       and i.StatusId          in (@StatusId,@NoStockId)
       and i.PickLocationId    is null
    
    update i
       set OutboundDocumentId = ili.OutboundDocumentId,
           IssueId            = ili.IssueId,
           IssueLineId        = ili.IssueLineId
      from @TableInstructions i
      join IssueLineInstruction ili on i.InstructionId = ili.InstructionId
    
    if @@rowcount = 0
      update ti
         set OutboundDocumentId = i.OutboundDocumentId,
             IssueId            = i.IssueId,
             IssueLineId        = il.IssueLineId
        from @TableInstructions     ti
        join OutboundShipmentIssue osi on ti.OutboundShipmentId = osi.OutboundShipmentId
        join Issue                   i on osi.IssueId          = i.IssueId
        join IssueLine              il on i.IssueId            = il.IssueId
                                      and ti.StorageUnitBatchId = il.StorageUnitBatchId
  end
  else if @IssueId is not null
  begin
    insert @TableInstructions
          (OutboundDocumentId,
           IssueId,
           IssueLineId,
           JobId,
           InstructionId,
           InstructionRefId,
           WarehouseId,
           PickLocationId,
           StoreLocationId,
           StorageUnitBatchId,
           Quantity,
           InstructionTypeCode)
    select ili.OutboundDocumentId,
           ili.IssueId,
           ili.IssueLineId,
           i.JobId,
           i.InstructionId,
           isnull(i.InstructionRefId, i.InstructionId),
           i.WarehouseId,
           i.PickLocationId,
           i.StoreLocationId,
           i.StorageUnitBatchId,
           i.Quantity,
           it.InstructionTypeCode
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where ili.IssueId       = @IssueId
       and i.StatusId       in (@StatusId,@NoStockId)
       and i.PickLocationId is null
  end
  else 
  begin
    insert @TableInstructions
          (OutboundDocumentId,
           IssueId,
           IssueLineId,
           JobId,
           InstructionId,
           InstructionRefId,
           WarehouseId,
           PickLocationId,
           StoreLocationId,
           StorageUnitBatchId,
           Quantity,
           InstructionTypeCode)
    select ili.OutboundDocumentId,
           ili.IssueId,
           ili.IssueLineId,
           i.JobId,
           i.InstructionId,
           isnull(i.InstructionRefId, i.InstructionId),
           i.WarehouseId,
           i.PickLocationId,
           i.StoreLocationId,
           i.StorageUnitBatchId,
           i.Quantity,
           it.InstructionTypeCode
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where ili.IssueLineId   = isnull(@IssueLineId, ili.IssueLineId)
       and i.InstructionId   = isnull(@InstructionId, i.InstructionId)
       and i.StatusId       in (@StatusId,@NoStockId)
       and i.PickLocationId is null
  end
  
  select @InstructionCount = count(1) from @TableInstructions
  
  if @InstructionId is not null and @InstructionCount = 0
  begin
    select @OutboundDocumentId = OutboundDocumentId
      from Issue (nolock)
     where IssueId = @IssueId
    
    insert @TableInstructions
          (OutboundDocumentId,
           IssueId,
           IssueLineId,
           JobId,
           InstructionId,
           InstructionRefId,
           WarehouseId,
           PickLocationId,
           StoreLocationId,
           StorageUnitBatchId,
           Quantity,
           InstructionTypeCode)
    select @OutboundDocumentId,
           @IssueId,
           @IssueLineId,
           i.JobId,
           i.InstructionId,
           isnull(i.InstructionRefId, i.InstructionId),
           i.WarehouseId,
           i.PickLocationId,
           i.StoreLocationId,
           i.StorageUnitBatchId,
           i.Quantity,
           it.InstructionTypeCode
      from Instruction            i (nolock)
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where i.InstructionId   = @InstructionId
       and i.StatusId       in (@StatusId,@NoStockId)
       and i.PickLocationId is null
  end
  
  if (@AllocateLoc = 1)
  begin
    delete @TableInstructions
     where InstructionTypeCode = 'P'
  end
  
  select @InstructionCount = count(1) from @TableInstructions
  
  update ti
     set OutboundShipmentId = ili.OutboundShipmentId,
         IssueId            = ili.IssueId,
         IssueLineId        = ili.IssueLineId
    from @TableInstructions    ti
    join IssueLineInstruction ili (nolock) on ti.InstructionRefId = ili.InstructionId
   where (ti.OutboundShipmentId is null
      or  ti.IssueId            is null
      or  ti.IssueLineId        is null)
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update ti
     set DeliveryDate = i.DeliveryDate
    from @TableInstructions     ti
    join Issue                   i (nolock) on ti.IssueId = i.IssueId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  --update ti
  --   set DeliveryDate = os.ShipmentDate
  --  from @TableInstructions     ti
  --  join OutboundShipment       os (nolock) on ti.OutboundShipmentId = os.OutboundShipmentId
  
  --select @Error = @@Error
  
  --if @Error <> 0
  --  goto error
  
  if @@trancount > 0
    begin transaction
  
  if @InstructionCount = 0 and @AllocateLoc = 1
  begin
    set @Error = 0
    goto Error
  end
  
  update i
     set LIFO              = isnull(odt.LIFO, 0),
         MinimumShelfLife  = odt.MinimumShelfLife,
         AreaType          = odt.AreaType,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableInstructions     i
    join OutboundDocument      od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update i
     set MinimumShelfLife  = suec.MinimumShelfLife
    from @TableInstructions            i
    join StorageUnitBatch            sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnitExternalCompany suec (nolock) on sub.StorageUnitId    = suec.StorageUnitId
                                                 and i.ExternalCompanyId  = suec.ExternalCompanyId
   where i.InstructionTypeCode = 'P' -- Only updated the shelf life of a full pallet - mixed pallets should be done only at time of picking.
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  select @WarehouseId = WarehouseId
    from @TableInstructions
  
  if (dbo.ufn_Configuration(186, @WarehouseId) = 1)
  begin
    set @RepackLocationId = null
    
    select @RepackLocationId = l.LocationId
      from Location      l (nolock)
      join AreaLocation al (nolock) on l.LocationId = al.LocationId
      join Area          a (nolock) on al.AreaId    = a.AreaId
     where a.AreaCode = 'RP'
    
    update i
       set StorageUnitId = sub.StorageUnitId
      from @TableInstructions i
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
     where i.StorageUnitId is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update i
       set RepackLocationId = @RepackLocationId
      from @TableInstructions    i
      join PackExternalCompany pec (nolock) on i.ExternalCompanyId = pec.ExternalCompanyId
                                           and i.StorageUnitId     = pec.StorageUnitId
                                           and i.WarehouseId       = pec.WarehouseId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  if (dbo.ufn_Configuration(190, @WarehouseId) = 1)
  begin
    set @RepackLocationId = null
    
    select @RepackLocationId = l.LocationId
      from Location      l (nolock)
      join AreaLocation al (nolock) on l.LocationId = al.LocationId
      join Area          a (nolock) on al.AreaId    = a.AreaId
     where a.AreaCode = 'WT'
    
    update i
       set StorageUnitId = sub.StorageUnitId
      from @TableInstructions i
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
     where i.StorageUnitId is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update i
       set RepackLocationId = @RepackLocationId
      from @TableInstructions i
      join StorageUnit       su (nolock) on i.StorageUnitId = su.StorageUnitId
     where su.ProductCategory = 'V'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  if (dbo.ufn_Configuration(374, @WarehouseId) = 1)
  begin
    update i
       set StorageUnitId = sub.StorageUnitId
      from @TableInstructions i
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
     where i.StorageUnitId is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update i
       set RepackLocationId = al.LocationId
      from @TableInstructions i
      join StorageUnitArea  sua (nolock) on i.StorageUnitId = sua.StorageUnitId
      join Area               a (nolock) on sua.AreaId = a.AreaId
      join AreaLocation      al (nolock) on a.AreaId = al.AreaId
     where a.AreaType = 'Pack'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  update ti
     set AreaType         = i.AreaType
    from @TableInstructions ti
    join Issue              i (nolock) on ti.IssueId = i.IssueId
   where i.AreaType is not null
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  while @InstructionCount > 0
  begin
    set @InstructionCount = @InstructionCount - 1
    
    select top 1 
           @JobId               = JobId,
           @InstructionId       = InstructionId,
           @WarehouseId         = WarehouseId,
           @PickLocationId      = PickLocationId,
           @StoreLocationId     = isnull(RepackLocationId, StoreLocationId),
           @StorageUnitBatchId  = StorageUnitBatchId,
           @Quantity            = Quantity,
           @OrginalQuantity     = Quantity,
           @UpdIssueId          = IssueId,
           @UpdIssueLineId      = IssueLineId,
           @InstructionTypeCode = InstructionTypeCode,
           @LIFO                = LIFO,
           @MinimumShelfLife    = MinimumShelfLife,
           @AreaType            = AreaType,
           @DeliveryDate        = DeliveryDate
      from @TableInstructions
    order by JobId
    
    delete @TableInstructions where InstructionId = @InstructionId
    
    set @PickPartPallet = null
    
    select @StorageUnitId  = sub.StorageUnitId,
           @PickPartPallet = su.PickPartPallet
      from StorageUnitBatch sub (nolock)
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
     where StorageUnitBatchId = @StorageUnitBatchId
    
    exec @Error = p_Pick_Location_Get
     @WarehouseId         = @WarehouseId,
     @LocationId          = @PickLocationId output,
     @StorageUnitBatchId  = @StorageUnitBatchId output,
     @Quantity            = @Quantity output,
     @InstructionId       = @InstructionId,
     @LIFO                = @LIFO,
     @MinimumShelfLife    = @MinimumShelfLife,
     @AreaType            = @AreaType,
     @DeliveryDate        = @DeliveryDate,
     @InstructionTypeCode = @InstructionTypeCode
    select @PickLocationId as '@PickLocationId'
    if @Error <> 0
      goto error
    
    if @AreaType = 'MassMart' and @PickLocationId is null
    begin
      set @AreaType = ''
      
      exec @Error = p_Pick_Location_Get
       @WarehouseId         = @WarehouseId,
       @LocationId          = @PickLocationId output,
       @StorageUnitBatchId  = @StorageUnitBatchId output,
       @Quantity            = @Quantity output,
       @InstructionId       = @InstructionId,
       @LIFO                = @LIFO,
       @MinimumShelfLife    = @MinimumShelfLife,
       @AreaType            = @AreaType,
       @DeliveryDate        = @DeliveryDate,
       @InstructionTypeCode = @InstructionTypeCode
      
      set @AreaType = 'MassMart'
    end
    
    if @PickPartPallet = 1 -- Pick part pallets from racking, dont pick Full mixed pallets from pick face
    begin
      if (dbo.ufn_Configuration(142, @WarehouseId) = 1)
      begin
        if @PickLocationId is null and @InstructionTypeCode = 'P'
        begin
          exec @Error = p_Pick_Location_Get
           @WarehouseId         = @WarehouseId,
           @LocationId          = @PickLocationId output,
           @StorageUnitBatchId  = @StorageUnitBatchId output,
           @Quantity            = @Quantity output,
           @InstructionId       = @InstructionId,
           @LIFO                = @LIFO,
           @MinimumShelfLife    = @MinimumShelfLife,
           @Replenishment       = 1,
           @AreaType            = @AreaType,
           @DeliveryDate        = @DeliveryDate,
           @InstructionTypeCode = @InstructionTypeCode
          
          if @Error <> 0
            goto error
          
          if @PickLocationId is not null
          begin
            exec @Error = p_Instruction_Update
              @InstructionId     = @InstructionId,
              @Quantity          = @Quantity,
              @ConfirmedQuantity = @Quantity
            
            if @Error <> 0
              goto error
          end
        end
      end
    end
    else
    begin
      if (dbo.ufn_Configuration(136, @WarehouseId) = 1)
      or (not exists(select  top 1 1
                       from StorageUnitArea sua (nolock)
                       join Area              a (nolock) on sua.AreaId = a.AreaId
                      where a.WarehouseId     = @WarehouseId
                        and a.AreaCode       in ('BK','RK')
                        and sua.StorageUnitId = @StorageUnitId))
      begin
        if @PickLocationId is null and @InstructionTypeCode in ('FM','P')
        begin
          exec @Error = p_Pick_Location_Get
           @WarehouseId         = @WarehouseId,
           @LocationId          = @PickLocationId output,
           @StorageUnitBatchId  = @StorageUnitBatchId output,
           @Quantity            = 1,
           @InstructionId       = @InstructionId,
           @LIFO                = @LIFO,
           @MinimumShelfLife    = @MinimumShelfLife,
           @AreaType            = @AreaType,
           @DeliveryDate        = @DeliveryDate,
           @InstructionTypeCode = @InstructionTypeCode
          
          if @Error <> 0
            goto error
          
          if @PickLocationId is not null
          begin
            select @InstructionTypeId = InstructionTypeId
              from InstructionType (nolock)
             where InstructionTypeCode = 'FM'
            
            exec @Error = p_Instruction_Update
              @InstructionId     = @InstructionId,
              @InstructionTypeId = @InstructionTypeId
            
            if @Error <> 0
              goto error
          end
        end
      end
    end
    
    if @PickLocationId is null
    begin
      exec @Error = p_Instruction_Update
        @InstructionId     = @InstructionId,
        @PickLocationId    = @PickLocationId,
        @StoreLocationId   = @StoreLocationId,
        @ConfirmedQuantity = 0,
        @StatusId          = @NoStockId
      
      if @Error <> 0
        goto error
      
      select @InstructionRefId = InstructionRefId
        from Instruction (nolock)
       where InstructionId = @InstructionId
      
      exec @Error = p_Instruction_Update_ili
        @InstructionId     = @InstructionId,
        @InstructionRefId  = @InstructionRefId,
        @insConfirmed      = 0
      
      if @Error <> 0
        goto error
      
      -- Check to see if all lines are no-stock and then updates job too.
      if (select count(1) from Instruction (nolock) where JobId = @JobId and StatusId not in (@IsStockId, @NoStockId)) = 0
      begin
        exec @Error = p_Job_Update
         @JobId    = @JobId,
         @StatusId = @NoStockId
        
        if @Error <> 0
          goto error
      end
    end
    else
    begin
      if (select dbo.ufn_Configuration(155, @WarehouseId)) = 1 -- Palletise based on SOH
      begin
        if @Quantity > @OrginalQuantity
          set @Quantity = @OrginalQuantity
        
        select @AreaCode = a.AreaCode
          from Location      l (nolock)
          join AreaLocation al (nolock) on l.LocationId = al.LocationId
          join Area          a (nolock) on al.AreaId = a.AreaId
         where l.LocationId = @PickLocationId
        
        if @AreaCode = 'PK'
          select @InstructionTypeId = InstructionTypeId
            from InstructionType (nolock)
           where InstructionTypeCode = 'PM'
        
        if @AreaCode = 'SP'
        begin
          select top 1 @PalletQuantity = p.Quantity
            from Pack      p (nolock)
            join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
           where p.WarehouseId      = @WarehouseId
             and p.StorageUnitId    = @StorageUnitId
          order by OutboundSequence
          
          if @PalletQuantity > @Quantity
            select @InstructionTypeId = InstructionTypeId
              from InstructionType (nolock)
             where InstructionTypeCode = 'FM'
        end
        
        exec @Error = p_Instruction_Update
          @InstructionId     = @InstructionId,
          @Quantity          = @Quantity,
          @ConfirmedQuantity = @Quantity,
          @InstructionTypeId = @InstructionTypeId
        
        if @Error <> 0
          goto error
      end
      
      exec @Error = p_Job_Update
       @JobId    = @JobId,
       @StatusId = @IsStockId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Instruction_Update
        @InstructionId      = @InstructionId,
        @PickLocationId     = @PickLocationId,
        @StoreLocationId    = @StoreLocationId,
        @StorageUnitBatchId = @StorageUnitBatchId,
        @StatusId           = @StatusId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId      = @InstructionId
      
      if @Error <> 0
        goto error
    end
    
    
    if @StatusUpd = 1
    begin
      if @OutboundShipmentId is null
      begin
        if (select count(1)
              from Instruction i (nolock)
              join Job         j /*lock*/ on i.JobId       = j.JobId
              join Status      s (nolock) on j.StatusId    = s.StatusId
             where i.IssueLineId    = @UpdIssueLineId
               and s.StatusCode not in ('SA','NS')) = 0
        begin
          exec @Error = p_IssueLine_Update
           @IssueLineId = @UpdIssueLineId,
           @StatusId    = @IsStockId
          
          if @Error <> 0
            goto error
        
          if (select count(1)
                from IssueLine il /*lock*/
                join Status     s (nolock) on il.StatusId    = s.StatusId
               where il.IssueId        = @UpdIssueId
                 and s.StatusCode not in ('SA','NS')) = 0
          begin
            exec @Error = p_Issue_Update
             @IssueId  = @UpdIssueId,
             @StatusId = @IsStockId
            
            if @Error <> 0
              goto error
          end
        end
      end
      else
      begin
        if (select count(1)
              from Instruction            i (nolock)
              join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
              join Job                    j /*lock*/ on i.JobId         = j.JobId
              join Status                 s (nolock) on j.StatusId      = s.StatusId
             where i.InstructionId = @InstructionId
               and s.StatusCode not in ('SA','NS')) = 0
        begin
          insert @TableIssueLines
                (IssueId,
                 IssueLineId)
          select il.IssueId,
                 il.IssueLineId
            from IssueLineInstruction ili (nolock)
            join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
           where ili.InstructionId = @InstructionId
          
          while exists(select top 1 1 from @TableIssueLines)
          begin
            select top 1
                   @UpdIssueId     = IssueId,
                   @UpdIssueLineId = IssueLineId
              from @TableIssueLines
            
            delete @TableIssueLines
             where IssueId     = @UpdIssueId
               and IssueLineId = @UpdIssueLineId
            
            exec @Error = p_IssueLine_Update
             @IssueLineId = @UpdIssueLineId,
             @StatusId    = @IsStockId
            
            if @Error <> 0
              goto error
            
            if (select count(1)
                  from IssueLine il /*lock*/
                  join Status     s (nolock) on il.StatusId    = s.StatusId
                 where il.IssueId        = @UpdIssueId
                   and s.StatusCode not in ('SA','NS')) = 0
            begin
              exec @Error = p_Issue_Update
               @IssueId  = @UpdIssueId,
               @StatusId = @IsStockId
              
              if @Error <> 0
                goto error
            end
          end
        end
      end
    end
  end
  
  if @OutboundShipmentId is not null
  begin
    if exists(select top 1 1 
                from IssueLineInstruction ili (nolock)
                join Instruction            i (nolock) on ili.InstructionId =  i.InstructionId
                join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                join Job                    j (nolock) on i.JobId = j.JobId
                join Status                si (nolock) on i.StatusId = si.StatusId
                --join Status                sj (nolock) on j.StatusId = sj.StatusId
               where ili.OutboundShipmentId = @OutboundShipmentId
                 --and si.StatusCode = 'RL'
                 and si.StatusCode = 'W'
                 and (case when @AllocateLoc = 0 and it.InstructionTypeCode = 'P'
                          then isnull(i.PickLocationId,-2)
                          else isnull(i.PickLocationId,-1)
                          end) = -1)
    begin
      set @Error = -1
      goto error
    end
  end
  else if @IssueId is not null
  begin
    if exists(select top 1 1 
                from IssueLineInstruction ili (nolock)
                join Instruction            i (nolock) on ili.InstructionId =  i.InstructionId
                join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                join Job                    j (nolock) on i.JobId = j.JobId
                join Status                si (nolock) on i.StatusId = si.StatusId
               where ili.IssueId = @IssueId
                 and si.StatusCode = 'W'
                 and (case when @AllocateLoc = 0 and it.InstructionTypeCode = 'P'
                          then isnull(i.PickLocationId,-2)
                          else isnull(i.PickLocationId,-1)
                          end) = -1)
    begin
      set @Error = -1
      goto error
    end
  end
  else if @IssueLineId is not null
  begin
    if exists(select top 1 1 
                from IssueLineInstruction ili (nolock)
                join Instruction            i (nolock) on ili.InstructionId =  i.InstructionId
                join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                join Job                    j (nolock) on i.JobId = j.JobId
                join Status                si (nolock) on i.StatusId = si.StatusId
               where ili.IssueLineId = @IssueLineId
                 and si.StatusCode = 'W'
                 and (case when @AllocateLoc = 0 and it.InstructionTypeCode = 'P'
                          then isnull(i.PickLocationId,-2)
                          else isnull(i.PickLocationId,-1)
                          end) = -1)
    begin
      set @Error = -1
      goto error
    end
  end
  
  if @StatusUpd = 1
  begin
    if @IssueId is not null or @IssueLineId is not null
    begin
      select @StatusCode = convert(nvarchar(max), value)
        from Configuration (nolock)
       where ConfigurationId = 173
         and WarehouseId = @WarehouseId
      
      if @StatusCode is null
        set @StatusCode = 'SA'
      
      if (select dbo.ufn_Configuration(155, @WarehouseId)) = 0
      begin
        exec @Error = p_Planning_Complete
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId
        
        if @Error <> 0
          goto error
      end
      
      exec @Error = p_Outbound_Release
       @OutboundShipmentId = null,
       @IssueId            = @IssueId,
       @IssueLineId        = @IssueLineId,
       @StatusCode         = @StatusCode
      
      if @Error <> 0
        goto error
    end
  end
  
  if @@trancount > 0
    commit transaction
  
  return @Error
  return
  
  error:
    if @@trancount = 1
    begin
      rollback transaction
      raiserror 900000 'Error executing p_Despatch_Manual_Palletisation_Auto_Locations_DM'
    end
    else
      commit transaction
    
    return @Error
end
