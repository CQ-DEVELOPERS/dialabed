﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Division_Search
  ///   Filename       : p_Division_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:05
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Division table.
  /// </remarks>
  /// <param>
  ///   @DivisionId int = null output,
  ///   @DivisionCode nvarchar(20) = null,
  ///   @Division nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Division.DivisionId,
  ///   Division.DivisionCode,
  ///   Division.Division 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Division_Search
(
 @DivisionId int = null output,
 @DivisionCode nvarchar(20) = null,
 @Division nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @DivisionId = '-1'
    set @DivisionId = null;
  
  if @DivisionCode = '-1'
    set @DivisionCode = null;
  
  if @Division = '-1'
    set @Division = null;
  
 
  select
         Division.DivisionId
        ,Division.DivisionCode
        ,Division.Division
    from Division
   where isnull(Division.DivisionId,'0')  = isnull(@DivisionId, isnull(Division.DivisionId,'0'))
     and isnull(Division.DivisionCode,'%')  like '%' + isnull(@DivisionCode, isnull(Division.DivisionCode,'%')) + '%'
     and isnull(Division.Division,'%')  like '%' + isnull(@Division, isnull(Division.Division,'%')) + '%'
  
end
