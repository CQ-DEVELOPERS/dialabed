﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLine_Select
  ///   Filename       : p_IssueLine_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2013 08:43:18
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IssueLine table.
  /// </remarks>
  /// <param>
  ///   @IssueLineId int = null 
  /// </param>
  /// <returns>
  ///   IssueLine.IssueLineId,
  ///   IssueLine.IssueId,
  ///   IssueLine.OutboundLineId,
  ///   IssueLine.StorageUnitBatchId,
  ///   IssueLine.StatusId,
  ///   IssueLine.OperatorId,
  ///   IssueLine.Quantity,
  ///   IssueLine.ConfirmedQuatity,
  ///   IssueLine.Weight,
  ///   IssueLine.ConfirmedWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLine_Select
(
 @IssueLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         IssueLine.IssueLineId
        ,IssueLine.IssueId
        ,IssueLine.OutboundLineId
        ,IssueLine.StorageUnitBatchId
        ,IssueLine.StatusId
        ,IssueLine.OperatorId
        ,IssueLine.Quantity
        ,IssueLine.ConfirmedQuatity
        ,IssueLine.Weight
        ,IssueLine.ConfirmedWeight
    from IssueLine
   where isnull(IssueLine.IssueLineId,'0')  = isnull(@IssueLineId, isnull(IssueLine.IssueLineId,'0'))
  
end
