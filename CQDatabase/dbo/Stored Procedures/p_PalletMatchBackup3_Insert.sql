﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletMatchBackup3_Insert
  ///   Filename       : p_PalletMatchBackup3_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:51
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PalletMatchBackup3 table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null,
  ///   @InstructionId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(60) = null,
  ///   @Batch nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   PalletMatchBackup3.PalletId,
  ///   PalletMatchBackup3.InstructionId,
  ///   PalletMatchBackup3.StorageUnitBatchId,
  ///   PalletMatchBackup3.ProductCode,
  ///   PalletMatchBackup3.SKUCode,
  ///   PalletMatchBackup3.Batch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletMatchBackup3_Insert
(
 @PalletId int = null,
 @InstructionId int = null,
 @StorageUnitBatchId int = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(60) = null,
 @Batch nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PalletMatchBackup3
        (PalletId,
         InstructionId,
         StorageUnitBatchId,
         ProductCode,
         SKUCode,
         Batch)
  select @PalletId,
         @InstructionId,
         @StorageUnitBatchId,
         @ProductCode,
         @SKUCode,
         @Batch 
  
  select @Error = @@Error
  
  
  return @Error
  
end
