﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLineHistory_Insert
  ///   Filename       : p_IssueLineHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2013 08:43:15
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IssueLineHistory table.
  /// </remarks>
  /// <param>
  ///   @IssueLineId int = null,
  ///   @IssueId int = null,
  ///   @OutboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuatity float = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null 
  /// </param>
  /// <returns>
  ///   IssueLineHistory.IssueLineId,
  ///   IssueLineHistory.IssueId,
  ///   IssueLineHistory.OutboundLineId,
  ///   IssueLineHistory.StorageUnitBatchId,
  ///   IssueLineHistory.StatusId,
  ///   IssueLineHistory.OperatorId,
  ///   IssueLineHistory.Quantity,
  ///   IssueLineHistory.ConfirmedQuatity,
  ///   IssueLineHistory.CommandType,
  ///   IssueLineHistory.InsertDate,
  ///   IssueLineHistory.Weight,
  ///   IssueLineHistory.ConfirmedWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLineHistory_Insert
(
 @IssueLineId int = null,
 @IssueId int = null,
 @OutboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @Quantity float = null,
 @ConfirmedQuatity float = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @Weight float = null,
 @ConfirmedWeight float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert IssueLineHistory
        (IssueLineId,
         IssueId,
         OutboundLineId,
         StorageUnitBatchId,
         StatusId,
         OperatorId,
         Quantity,
         ConfirmedQuatity,
         CommandType,
         InsertDate,
         Weight,
         ConfirmedWeight)
  select @IssueLineId,
         @IssueId,
         @OutboundLineId,
         @StorageUnitBatchId,
         @StatusId,
         @OperatorId,
         @Quantity,
         @ConfirmedQuatity,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @Weight,
         @ConfirmedWeight 
  
  select @Error = @@Error
  
  
  return @Error
  
end
