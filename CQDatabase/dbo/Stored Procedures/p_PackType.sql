﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackType
  ///   Filename       : p_PackType.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jan 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackType
 
as
begin
	 set nocount on;
  
  select PackTypeId,
         PackType,
         PackType
    from PackType
end
