﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QAAudit_Search
  ///   Filename       : p_QAAudit_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:07
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the QAAudit table.
  /// </remarks>
  /// <param>
  ///   @QAAuditId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   QAAudit.QAAuditId,
  ///   QAAudit.JobId,
  ///   QAAudit.StorageUnitBatchId,
  ///   QAAudit.OrderedQuantity,
  ///   QAAudit.PickedQuantity,
  ///   QAAudit.CheckedQuantity,
  ///   QAAudit.WHQuantity,
  ///   QAAudit.PickedBy,
  ///   QAAudit.PickedDate,
  ///   QAAudit.CheckedBy,
  ///   QAAudit.CheckedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QAAudit_Search
(
 @QAAuditId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @QAAuditId = '-1'
    set @QAAuditId = null;
  
 
  select
         QAAudit.QAAuditId
        ,QAAudit.JobId
        ,QAAudit.StorageUnitBatchId
        ,QAAudit.OrderedQuantity
        ,QAAudit.PickedQuantity
        ,QAAudit.CheckedQuantity
        ,QAAudit.WHQuantity
        ,QAAudit.PickedBy
        ,QAAudit.PickedDate
        ,QAAudit.CheckedBy
        ,QAAudit.CheckedDate
    from QAAudit
   where isnull(QAAudit.QAAuditId,'0')  = isnull(@QAAuditId, isnull(QAAudit.QAAuditId,'0'))
  
end
