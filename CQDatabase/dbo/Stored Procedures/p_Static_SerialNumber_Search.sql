﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_SerialNumber_Search
  ///   Filename       : p_Static_SerialNumber_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Jul 2013 07:41:24
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the SerialNumber table.
  /// </remarks>
  /// <param>
  ///   @SerialNumberId int = null output,
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null,
  ///   @ReceiptId int = null,
  ///   @SerialNumber nvarchar(100) = null,
  ///   @BatchId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @ReceiptLineId int = null,
  ///   @StoreJobId int = null,
  ///   @PickJobId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   sn.SerialNumberId,
  ///   sn.StorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   sn.LocationId,
  ///   Location.Location,
  ///   sn.ReceiptId,
  ///   Receipt.ReceiptId,
  ///   sn.StoreInstructionId,
  ///   sn.PickInstructionId,
  ///   sn.SerialNumber,
  ///   sn.ReceivedDate,
  ///   sn.DespatchDate,
  ///   sn.ReferenceNumber,
  ///   sn.BatchId,
  ///   Batch.Batch,
  ///   sn.IssueId,
  ///   Issue.IssueId,
  ///   sn.IssueLineId,
  ///   IssueLine.IssueLineId,
  ///   sn.ReceiptLineId,
  ///   ReceiptLine.ReceiptLineId,
  ///   sn.StoreJobId,
  ///   Job.JobId,
  ///   sn.PickJobId,
  ///   Job.JobId,
  ///   sn.Remarks 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_SerialNumber_Search
(
 @SerialNumber nvarchar(100) = null,
 @productCode  nvarchar(30) = null,
 @product nvarchar(255) = null,
 @skuCode nvarchar(50) = null,
 @sku nvarchar(50) = null,
 @batch nvarchar(50) = null,
 @location nvarchar(15) = null
)
 
as
begin
	 set nocount on;
  
  select
         sn.SerialNumberId
        ,sn.SerialNumber
        ,sn.StorageUnitId
        ,p.Product
        ,p.ProductCode
        ,sku.SKUCode
        ,sku.SKU
        ,sn.LocationId
        ,l.Location as 'Location'
        ,id.OrderNumber as 'PurchaseOrderNumber'
        ,sn.ReceivedDate
        ,sn.ReferenceNumber
        ,sn.BatchId
        ,b.Batch as 'Batch'
        ,od.OrderNumber as 'SalesOrderNumber'
        ,sn.DespatchDate
        ,sn.Remarks
    from SerialNumber sn (nolock)
    left
    join StorageUnit su (nolock) on su.StorageUnitId = sn.StorageUnitId
    left
    join Product p (nolock) on su.ProductId = p.ProductId
    left
    join SKU sku (nolock) on su.SKUId = sku.SKUId
    left
    join Batch b (nolock) on sn.BatchId = b.BatchId
    left
    join Location l (nolock) on l.LocationId = sn.LocationId
    left
    join Receipt r (nolock) on r.ReceiptId = sn.ReceiptId
    left
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    left
    join Issue i (nolock) on i.IssueId = sn.IssueId
    left
    join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
   where sn.SerialNumber like '%' + isnull(@SerialNumber, sn.SerialNumber) + '%'
     and p.ProductCode like isnull(@ProductCode, p.ProductCode) + '%'
     and p.Product like isnull(@Product, p.Product) + '%'
     and sku.SKUCode like isnull(@SKUCode, sku.SKUCode) + '%'
     and sku.SKU like isnull(@sku, sku.SKU) + '%'
     and b.Batch like isnull(@batch, b.Batch) + '%'
     and isnull(l.Location, '') like isnull(@location, isnull(l.Location, '')) + '%'
  order by SerialNumber
  
end
