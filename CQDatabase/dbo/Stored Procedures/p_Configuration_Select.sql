﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Select
  ///   Filename       : p_Configuration_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 12:12:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Configuration table.
  /// </remarks>
  /// <param>
  ///   @ConfigurationId int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   Configuration.ConfigurationId,
  ///   Configuration.WarehouseId,
  ///   Configuration.ModuleId,
  ///   Configuration.Configuration,
  ///   Configuration.Indicator,
  ///   Configuration.IntegerValue,
  ///   Configuration.Value 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Select
(
 @ConfigurationId int = null,
 @WarehouseId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Configuration.ConfigurationId
        ,Configuration.WarehouseId
        ,Configuration.ModuleId
        ,Configuration.Configuration
        ,Configuration.Indicator
        ,Configuration.IntegerValue
        ,Configuration.Value
    from Configuration
   where isnull(Configuration.ConfigurationId,'0')  = isnull(@ConfigurationId, isnull(Configuration.ConfigurationId,'0'))
     and isnull(Configuration.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Configuration.WarehouseId,'0'))
  order by Configuration
  
end
 
