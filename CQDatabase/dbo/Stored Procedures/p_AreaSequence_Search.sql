﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaSequence_Search
  ///   Filename       : p_AreaSequence_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:02
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the AreaSequence table.
  /// </remarks>
  /// <param>
  ///   @AreaSequenceId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   AreaSequence.AreaSequenceId,
  ///   AreaSequence.PickAreaId,
  ///   AreaSequence.StoreAreaId,
  ///   AreaSequence.AdjustmentType,
  ///   AreaSequence.ReasonCode,
  ///   AreaSequence.InstructionTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaSequence_Search
(
 @AreaSequenceId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @AreaSequenceId = '-1'
    set @AreaSequenceId = null;
  
 
  select
         AreaSequence.AreaSequenceId
        ,AreaSequence.PickAreaId
        ,AreaSequence.StoreAreaId
        ,AreaSequence.AdjustmentType
        ,AreaSequence.ReasonCode
        ,AreaSequence.InstructionTypeCode
    from AreaSequence
   where isnull(AreaSequence.AreaSequenceId,'0')  = isnull(@AreaSequenceId, isnull(AreaSequence.AreaSequenceId,'0'))
  
end
