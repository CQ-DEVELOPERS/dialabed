﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipment_Select
  ///   Filename       : p_OutboundShipment_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Oct 2013 09:31:16
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundShipment table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null 
  /// </param>
  /// <returns>
  ///   OutboundShipment.OutboundShipmentId,
  ///   OutboundShipment.StatusId,
  ///   OutboundShipment.WarehouseId,
  ///   OutboundShipment.LocationId,
  ///   OutboundShipment.ShipmentDate,
  ///   OutboundShipment.Remarks,
  ///   OutboundShipment.SealNumber,
  ///   OutboundShipment.VehicleRegistration,
  ///   OutboundShipment.Route,
  ///   OutboundShipment.RouteId,
  ///   OutboundShipment.Weight,
  ///   OutboundShipment.Volume,
  ///   OutboundShipment.FP,
  ///   OutboundShipment.SS,
  ///   OutboundShipment.LP,
  ///   OutboundShipment.FM,
  ///   OutboundShipment.MP,
  ///   OutboundShipment.PP,
  ///   OutboundShipment.AlternatePallet,
  ///   OutboundShipment.SubstitutePallet,
  ///   OutboundShipment.DespatchBay,
  ///   OutboundShipment.NumberOfOrders,
  ///   OutboundShipment.ContactListId,
  ///   OutboundShipment.TotalOrders,
  ///   OutboundShipment.IDN,
  ///   OutboundShipment.WaveId,
  ///   OutboundShipment.ReferenceNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipment_Select
(
 @OutboundShipmentId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         OutboundShipment.OutboundShipmentId
        ,OutboundShipment.StatusId
        ,OutboundShipment.WarehouseId
        ,OutboundShipment.LocationId
        ,OutboundShipment.ShipmentDate
        ,OutboundShipment.Remarks
        ,OutboundShipment.SealNumber
        ,OutboundShipment.VehicleRegistration
        ,OutboundShipment.Route
        ,OutboundShipment.RouteId
        ,OutboundShipment.Weight
        ,OutboundShipment.Volume
        ,OutboundShipment.FP
        ,OutboundShipment.SS
        ,OutboundShipment.LP
        ,OutboundShipment.FM
        ,OutboundShipment.MP
        ,OutboundShipment.PP
        ,OutboundShipment.AlternatePallet
        ,OutboundShipment.SubstitutePallet
        ,OutboundShipment.DespatchBay
        ,OutboundShipment.NumberOfOrders
        ,OutboundShipment.ContactListId
        ,OutboundShipment.TotalOrders
        ,OutboundShipment.IDN
        ,OutboundShipment.WaveId
        ,OutboundShipment.ReferenceNumber
    from OutboundShipment
   where isnull(OutboundShipment.OutboundShipmentId,'0')  = isnull(@OutboundShipmentId, isnull(OutboundShipment.OutboundShipmentId,'0'))
  order by Remarks
  
end
