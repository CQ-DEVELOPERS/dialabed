﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionTypeCode
  ///   Filename       : p_InstructionTypeCode.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:54
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null 
  /// </param>
  /// <returns>
  ///   InstructionType.InstructionTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_InstructionTypeCode
(
 @InstructionTypeId int = null 
)
as
begin
	 set nocount on
	 
  select InstructionTypeCode 
    from InstructionType
   where InstructionTypeId = @InstructionTypeId
end
