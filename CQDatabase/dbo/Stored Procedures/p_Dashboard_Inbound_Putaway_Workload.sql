﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Inbound_Putaway_Workload
  ///   Filename       : p_Dashboard_Inbound_Putaway_Workload.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Inbound_Putaway_Workload
(
 @WarehouseId int = null,
 @Summarise   bit = 0,
 @Type        nvarchar(30) = null
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select Legend,
           Value,
           GroupBy,
           Jobs
      from DashboardInboundPutawayWorkload
     where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardInboundPutawayWorkload
	   
	   insert DashboardInboundPutawayWorkload
	         (WarehouseId,
	          GroupBy,
           Legend,
           Value,
           Jobs)
	   select i.WarehouseId,
	          it.InstructionType,
	          s.Status,
	          sum(i.ConfirmedQuantity),
	          COUNT(distinct(j.JobId)) as 'Jobs'
	     from Instruction      i (nolock)
	     join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
	                                     and it.InstructionTypeCode in ('S','SM','PR')
	     join Job              j (nolock) on i.JobId = j.JobId
	     join Status           s (nolock) on j.StatusId = s.StatusId
	    --where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      --and i.EndDate >= CONVERT(nvarchar(10), getdate(), 120)
	   group by i.WarehouseId,
	            it.InstructionType,
	            s.Status
	 end
end
