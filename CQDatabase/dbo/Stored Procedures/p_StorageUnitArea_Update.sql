﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitArea_Update
  ///   Filename       : p_StorageUnitArea_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012 14:20:19
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StorageUnitArea table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @AreaId int = null,
  ///   @StoreOrder int = null,
  ///   @PickOrder int = null,
  ///   @MinimumQuantity float = null,
  ///   @ReorderQuantity float = null,
  ///   @MaximumQuantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitArea_Update
(
 @StorageUnitId int = null,
 @AreaId int = null,
 @StoreOrder int = null,
 @PickOrder int = null,
 @MinimumQuantity float = null,
 @ReorderQuantity float = null,
 @MaximumQuantity float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  update StorageUnitArea
     set StoreOrder = isnull(@StoreOrder, StoreOrder),
         PickOrder = isnull(@PickOrder, PickOrder),
         MinimumQuantity = isnull(@MinimumQuantity, MinimumQuantity),
         ReorderQuantity = isnull(@ReorderQuantity, ReorderQuantity),
         MaximumQuantity = isnull(@MaximumQuantity, MaximumQuantity) 
   where StorageUnitId = @StorageUnitId
     and AreaId = @AreaId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_StorageUnitAreaHistory_Insert
         @StorageUnitId = @StorageUnitId,
         @AreaId = @AreaId,
         @StoreOrder = @StoreOrder,
         @PickOrder = @PickOrder,
         @MinimumQuantity = @MinimumQuantity,
         @ReorderQuantity = @ReorderQuantity,
         @MaximumQuantity = @MaximumQuantity,
         @CommandType = 'Update'
  
  return @Error
  
end
