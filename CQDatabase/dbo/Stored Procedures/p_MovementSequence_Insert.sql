﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MovementSequence_Insert
  ///   Filename       : p_MovementSequence_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:01
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MovementSequence table.
  /// </remarks>
  /// <param>
  ///   @MovementSequenceId int = null output,
  ///   @PickAreaId int = null,
  ///   @StoreAreaId int = null,
  ///   @StageAreaId int = null,
  ///   @OperatorGroupId int = null,
  ///   @StatusCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   MovementSequence.MovementSequenceId,
  ///   MovementSequence.PickAreaId,
  ///   MovementSequence.StoreAreaId,
  ///   MovementSequence.StageAreaId,
  ///   MovementSequence.OperatorGroupId,
  ///   MovementSequence.StatusCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MovementSequence_Insert
(
 @MovementSequenceId int = null output,
 @PickAreaId int = null,
 @StoreAreaId int = null,
 @StageAreaId int = null,
 @OperatorGroupId int = null,
 @StatusCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @MovementSequenceId = '-1'
    set @MovementSequenceId = null;
  
	 declare @Error int
 
  insert MovementSequence
        (PickAreaId,
         StoreAreaId,
         StageAreaId,
         OperatorGroupId,
         StatusCode)
  select @PickAreaId,
         @StoreAreaId,
         @StageAreaId,
         @OperatorGroupId,
         @StatusCode 
  
  select @Error = @@Error, @MovementSequenceId = scope_identity()
  
  
  return @Error
  
end
