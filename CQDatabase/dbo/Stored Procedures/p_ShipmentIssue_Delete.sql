﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ShipmentIssue_Delete
  ///   Filename       : p_ShipmentIssue_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:52
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null,
  ///   @IssueId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ShipmentIssue_Delete
(
 @ShipmentId int = null,
 @IssueId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ShipmentIssue
     where ShipmentId = @ShipmentId
       and IssueId = @IssueId
  
  select @Error = @@Error
  
  
  return @Error
  
end
