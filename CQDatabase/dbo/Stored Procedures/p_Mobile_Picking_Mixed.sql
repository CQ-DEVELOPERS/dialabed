﻿--IF OBJECT_ID('dbo.p_Mobile_Picking_Mixed') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Mobile_Picking_Mixed
--    IF OBJECT_ID('dbo.p_Mobile_Picking_Mixed') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Mobile_Picking_Mixed >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Mobile_Picking_Mixed >>>'
--END
--go
/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Picking_Mixed
    ///   Filename       : p_Mobile_Picking_Mixed.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 11 June 2007 14:15:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    ///   @OperatorId          int,
    ///   @InstructionId       int output
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_Picking_Mixed
(
 @operatorId          int,
 @instructionId       int output
)
as
begin
	 set nocount on
  
  declare @MobileLogId int
  

  insert MobileLog
        (ProcName,
         OperatorId,
         InstructionId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @OperatorId,
         @InstructionId,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @JobId              int
  
  begin transaction
  
  exec @Error = p_Mobile_Next_Job
   @OperatorId          = @OperatorId,
   @JobId               = @JobId output,
   @InstructionTypeCode = 'PM'
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Mobile_Next_Line
   @JobId         = @JobId,
   @InstructionId = @instructionId output,
   @OperatorId    = @operatorId
  
  if @Error <> 0
    goto error
  
  if exists(select top 1 1
              from Instruction i (nolock)
              join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                                              and it.InstructionTypeCode = 'M'
             where i.InstructionId = @instructionId
               and i.StoreLocationId is null
               and (i.OverrideStore = 0 or i.OverrideStore is null))
  begin
    exec @Error = p_Receiving_Manual_Palletisation_Auto_Locations
    @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_Instruction_MovementSequence
   @InstructionId = @InstructionId,
   @operatorId    = @operatorId
  
  if @Error <> 0
    goto error
  
  update MobileLog
     set EndDate       = getdate(),
         JobId         = @JobId,
         InstructionId = @InstructionId
   where MobileLogId = @MobileLogId
  
  commit transaction
  return
  
  error:
    if @Error in (-99,-88)
    begin
      commit transaction
      set @instructionId = @Error
    end
    else
    begin
      rollback transaction
      set @instructionId = -1
    end
    update Operator
       set PickAisle = null,
           StoreAisle = null
     where OperatorId = @OperatorId
    
    update MobileLog
       set EndDate = getdate(),
    
       JobId         = @JobId,
           InstructionId = @InstructionId,
           ErrorMsg = 'Error'
     where MobileLogId = @MobileLogId
    
    set @Error = -1
    return @Error
end

--go
--IF OBJECT_ID('dbo.p_Mobile_Picking_Mixed') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Mobile_Picking_Mixed >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Mobile_Picking_Mixed >>>'
--go


