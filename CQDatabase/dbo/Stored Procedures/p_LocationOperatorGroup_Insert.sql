﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationOperatorGroup_Insert
  ///   Filename       : p_LocationOperatorGroup_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jul 2013 10:58:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the LocationOperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null,
  ///   @OperatorGroupId int = null,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  ///   LocationOperatorGroup.LocationId,
  ///   LocationOperatorGroup.OperatorGroupId,
  ///   LocationOperatorGroup.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationOperatorGroup_Insert
(
 @LocationId int = null,
 @OperatorGroupId int = null,
 @OrderBy int = 0
)
 
as
begin
	 set nocount on;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
	 declare @Error int
 
  insert LocationOperatorGroup
        (LocationId,
         OperatorGroupId,
         OrderBy)
  select @LocationId,
         @OperatorGroupId,
         @OrderBy 
  
  select @Error = @@Error
  
  
  return @Error
  
end
