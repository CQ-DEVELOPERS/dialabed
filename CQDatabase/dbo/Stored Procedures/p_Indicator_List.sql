﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Indicator_List
  ///   Filename       : p_Indicator_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:25
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Indicator table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Indicator.IndicatorId,
  ///   Indicator.Indicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Indicator_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as IndicatorId
        ,'{All}' as Indicator
  union
  select
         Indicator.IndicatorId
        ,Indicator.Indicator
    from Indicator
  
end
