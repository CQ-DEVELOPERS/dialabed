﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Package_Line_Insert
  ///   Filename       : p_Package_Line_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Package_Line_Insert
(
	@ReceiptId int,
	@ReceiptLineId int = null,
	@StorageUnitBatchId int,
	@OperatorId int,
	@ReceivedQuantity float = null,
	@AcceptedQuantity float = null,
	@RejectQuantity float = null,
	@DeliveryNoteQuantity float = null
)
as
begin
	 set nocount on;
	

If (@ReceiptLineId is null)
Begin


	Insert Into ReceiptLinePackaging 
		(ReceiptId,
		ReceiptLineId,
        StorageUnitBatchId,
        StatusId,
        OperatorId,
        ReceivedQuantity,
        AcceptedQuantity,
        RejectQuantity,
        DeliveryNoteQuantity)
	Values (@ReceiptId,@ReceiptLineId,@StorageUnitBatchId,4,@OperatorId,@ReceivedQuantity,@AcceptedQuantity,@RejectQuantity,@DeliveryNoteQuantity)
print 'Insert'
END
Else
Begin
	Update ReceiptLinePackaging set ReceivedQuantity = Isnull(@ReceivedQuantity,ReceivedQuantity),
									AcceptedQuantity = Isnull(@AcceptedQuantity,AcceptedQuantity),
									RejectQuantity	= Isnull(@RejectQuantity,RejectQuantity),
									DeliveryNoteQuantity	= Isnull(@DeliveryNoteQuantity,DeliveryNoteQuantity)
	From ReceiptLinePackaging
	where ReceiptId = @ReceiptId and ReceiptLineId = @ReceiptLineId
End

END
