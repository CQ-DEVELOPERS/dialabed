﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Confirm_Outbound_QAOrder
  ///   Filename       : p_Mobile_Confirm_Outbound_QAOrder.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2014
  /// </summary>
  /// <remarks>
  ///   

  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Confirm_Outbound_QAOrder
(
 @warehouseId int,
 @orderNumber nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @IssueId         int,
          @OutboundDocumentId int,
          @OldWarehouseId    int
  
  set @Error = 
0
  
  set @OldWarehouseId = @warehouseId
  
  
  select @IssueId         = i.IssueId,
         @OutboundDocumentId = od.OutboundDocumentId
    from OutboundDocument od (nolock)
    join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join Status            s (nolock) on i.StatusId = s.StatusId
   where od.WarehouseId = isnull(@warehouseid, od.WarehouseId)
     and od.OrderNumber = @orderNumber
     --and s.statusid in (dbo.ufn_StatusId('IS','QA'))
  
  if @IssueId is null
 
 begin
    set @IssueId = -1
    goto result
  end
  else
  begin
    set @Error = -1
    goto result
  end
  
  result:
    select @IssueId as '@IssueId'
   
 return @IssueId
end
 
