﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerType_Insert
  ///   Filename       : p_ContainerType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:29
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ContainerType table.
  /// </remarks>
  /// <param>
  ///   @ContainerTypeId int = null output,
  ///   @ContainerType nvarchar(100) = null,
  ///   @ContainerTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   ContainerType.ContainerTypeId,
  ///   ContainerType.ContainerType,
  ///   ContainerType.ContainerTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerType_Insert
(
 @ContainerTypeId int = null output,
 @ContainerType nvarchar(100) = null,
 @ContainerTypeCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @ContainerType = '-1'
    set @ContainerType = null;
  
  if @ContainerTypeCode = '-1'
    set @ContainerTypeCode = null;
  
	 declare @Error int
 
  insert ContainerType
        (ContainerType,
         ContainerTypeCode)
  select @ContainerType,
         @ContainerTypeCode 
  
  select @Error = @@Error, @ContainerTypeId = scope_identity()
  
  
  return @Error
  
end
