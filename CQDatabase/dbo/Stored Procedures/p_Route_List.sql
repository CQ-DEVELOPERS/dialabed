﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Route_List
  ///   Filename       : p_Route_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:08
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Route table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Route.RouteId,
  ///   Route.Route 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Route_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as RouteId
        ,'{All}' as Route
  union
  select
         Route.RouteId
        ,Route.Route
    from Route
  
end
