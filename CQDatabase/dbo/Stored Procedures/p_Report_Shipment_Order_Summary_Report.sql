﻿/*
  /// <summary>
  ///   Procedure Name : p_Report_Shipment_Order_Summary_Report
  ///   Filename       : p_Report_Shipment_Order_Summary_Report.sql
  ///   Create By      : Karen
  ///   Date Created   : Jan 2014
  /// </summary>
  /// <remarks>
  ///  
  /// </remarks>
  /// <param>
  ///  
  /// </param>
  /// <returns>
  ///  
  /// </returns>
  /// <newpara>
  ///   Modified by    :
  ///   Modified Date  :
  ///   Details        :
  /// </newpara>
*/
CREATE procedure p_Report_Shipment_Order_Summary_Report
(
@FromDate          datetime,
@ToDate            datetime,
@PrincipalId	   int,
@ExternalCompanyId int
)
 
as
begin

  set nocount on;

 
  select @FromDate as FromDate,
		 @ToDate as ToDate,
		 @PrincipalId as PrincipalId,
		 @ExternalCompanyId as ExternalCompanyId

end
 
 
