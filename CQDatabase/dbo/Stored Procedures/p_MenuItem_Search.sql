﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Search
  ///   Filename       : p_MenuItem_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 16:06:10
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the MenuItem table.
  /// </remarks>
  /// <param>
  ///   @MenuItemId int = null output,
  ///   @MenuId int = null,
  ///   @MenuItem nvarchar(100) = null,
  ///   @ParentMenuItemId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   MenuItem.MenuItemId,
  ///   MenuItem.MenuId,
  ///   Menu.Menu,
  ///   MenuItem.MenuItem,
  ///   MenuItem.ToolTip,
  ///   MenuItem.NavigateTo,
  ///   MenuItem.ParentMenuItemId,
  ///   MenuItem.MenuItem,
  ///   MenuItem.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_Search
(
 @MenuItemId int = null output,
 @MenuId int = null,
 @MenuItem nvarchar(100) = null,
 @ParentMenuItemId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @MenuItem = '-1'
    set @MenuItem = null;
  
  if @ParentMenuItemId = '-1'
    set @ParentMenuItemId = null;
  
 
  select
         MenuItem.MenuItemId
        ,MenuItem.MenuId
         ,MenuMenuId.Menu as 'Menu'
        ,MenuItem.MenuItem
        ,MenuItem.ToolTip
        ,MenuItem.NavigateTo
        ,MenuItem.ParentMenuItemId
         ,MenuItemParentMenuItemId.MenuItem as 'MenuItemParentMenuItemId'
        ,MenuItem.OrderBy
    from MenuItem
    left
    join Menu MenuMenuId on MenuMenuId.MenuId = MenuItem.MenuId
    left
    join MenuItem MenuItemParentMenuItemId on MenuItemParentMenuItemId.MenuItemId = MenuItem.ParentMenuItemId
   where isnull(MenuItem.MenuItemId,'0')  = isnull(@MenuItemId, isnull(MenuItem.MenuItemId,'0'))
     and isnull(MenuItem.MenuId,'0')  = isnull(@MenuId, isnull(MenuItem.MenuId,'0'))
     and isnull(MenuItem.MenuItem,'%')  like '%' + isnull(@MenuItem, isnull(MenuItem.MenuItem,'%')) + '%'
     and isnull(MenuItem.ParentMenuItemId,'0')  = isnull(@ParentMenuItemId, isnull(MenuItem.ParentMenuItemId,'0'))
  order by MenuItem
  
end
