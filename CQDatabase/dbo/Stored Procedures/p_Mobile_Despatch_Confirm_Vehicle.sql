﻿--IF OBJECT_ID('dbo.p_Mobile_Despatch_Confirm_Vehicle') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Mobile_Despatch_Confirm_Vehicle
--    IF OBJECT_ID('dbo.p_Mobile_Despatch_Confirm_Vehicle') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Mobile_Despatch_Confirm_Vehicle >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Mobile_Despatch_Confirm_Vehicle >>>'
--END
--go
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Despatch_Confirm_Vehicle
  ///   Filename       : p_Mobile_Despatch_Confirm_Vehicle.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2018-06-20
  ///   Details        : Waco001 - Save the operator that scanned onto truck as well as date and time 
  /// </newpara>  
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2018-06-25
  ///   Details        : Waco002 - display the pallets to scan and total pallets on the first entry into the screen
  /// </newpara>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2018-06-25
  ///   Details        : Waco003 - if config 616 is on allow scan to vehicle from checking, otherwise only from despatch
  ///					 Don't allow order to proceed if all lines are not checked
  /// </newpara>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2018-06-25
  ///   Details        : Waco004 - Don't allow scanning of job not belonging to order
  /// </newpara>
*/
CREATE procedure [dbo].[p_Mobile_Despatch_Confirm_Vehicle]
(
 @DocumentNumber nvarchar(50) = null,
 @Vehicle        nvarchar(50) = null,
 @Barcode        nvarchar(50) = null,
 @Pallets        int = null output,
 @Total          int = null output,
 @OperatorId	    int = null  -- Waco001
)
 
as
begin
     set nocount on;
  
  declare @Error              int = 0,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @OutboundShipmentId int,
          @OutboundDocumentId int,
          @JobId              int,
          @StatusCode         nvarchar(10),
          @PalletId           int,
          @ReferenceNumber    nvarchar(50),
          @IssueId            int,
          @ShipmentId         int,
          @DocumentId         int,
          @InstructionId      int,
          @Count              int,
          @Stored             int,
          @Picked             int,
          @StorageUnitBatchId int,
          @WarehouseId		  int,
          @StatusInvalid	  int,
          @JobOrderNumber	  nvarchar(50)
          
  declare @InstructionTable table
  (
      InstructionId int,
      InstructionRefId int,
      JobId int,
      ParentInstructionId int
  )        
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Pallets = 0;
  set @Total   = 0;
  
  if @Barcode = '-1'
    set @Barcode = null
  
  begin transaction
  
  if @DocumentNumber is not null
  begin
    if @DocumentNumber is null and @Vehicle is null
    begin
      set @Error = 900001
      set @Errormsg = 'Document and Vehicle cannot both be null'
      goto error
    end
    
    --if isnumeric(@DocumentNumber) = 1
    --begin
    --  select @ShipmentId = OutboundShipmentId
    --    from OutboundShipment (nolock)
    --   where OutboundShipmentId = convert(int, @DocumentNumber)
    --end
    
    if @ShipmentId is null
    begin
      select @DocumentId = OutboundDocumentId
        from OutboundDocument (nolock)
       where OrderNumber = @DocumentNumber
    end
    
    -- Waco003 - start
    
    -- Waco003 - end
    
    if @ShipmentId is null and @DocumentId is null
    begin
      set @Error = 900002
      set @Errormsg = 'Document not valid'
      goto error
    end
  end
  
  if @barcode is not null
  begin
    if isnumeric(replace(@barcode,'J:','')) = 1
    begin
      select @JobId           = replace(@barcode,'J:',''),
             @barcode = null
      
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
            -- @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId        = ili.IssueId,
             @WarehouseId		 = i.WarehouseId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId or ili.InstructionId = i.InstructionRefId
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where j.JobId = @JobId
	   and ili.OutboundDocumentId = @DocumentId
      -- Waco004 - start
      
      select @JobOrderNumber = od.OrderNumber
        from Instruction i
        join IssueLineInstruction ili ON ili.InstructionId = i.InstructionId OR ili.InstructionId = i.InstructionRefId
        join OutboundDocument od ON ili.OutboundDocumentId = od.OutboundDocumentId
			    where i.JobId = @JobId

			
	  if @JobOrderNumber != @DocumentNumber --AND @OutboundShipmentId != @DocumentNumber
	  begin
	  	set @Error = 900011
        set @ErrorMsg = 'Pallet/Box not for this order'
        goto error
	  end
	  -- Waco004 - end
  
    end
    else if isnumeric(replace(@barcode,'P:','')) = 1
    begin
      select @PalletId        = replace(@barcode,'P:',''),
             @barcode = null
      
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
           --  @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId or ili.InstructionId = i.InstructionRefId
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where i.PalletId = @PalletId
	   and ili.OutboundDocumentId = @DocumentId
         and s.StatusCode = 'DC'
    end
    else if isnumeric(replace(@barcode,'R:','')) = 1 or isnumeric(replace(@barcode,'NB:','')) = 1
      select @ReferenceNumber = @barcode,
             @barcode         = null
    begin
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             --@OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId,
			 @JobOrderNumber	= od.OrderNumber
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId or ili.InstructionId = i.InstructionRefId
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
		join OutboundDocument od ON ili.OutboundDocumentId = od.OutboundDocumentId
       where j.ReferenceNumber = @ReferenceNumber
	   --and ili.OutboundDocumentId = @DocumentId
    end

	 if @JobOrderNumber != @DocumentNumber --AND @OutboundShipmentId != @DocumentNumber
	  begin
	  	set @Error = 900011
        set @ErrorMsg = 'Pallet/Box not for this order'
        goto error
	  end

    if @JobId is null
    begin
      set @Error = 900006
      set @ErrorMsg = 'Could not find the pallet'
      goto error
    end
    
    if @StatusCode = 'C'
    begin
      set @Error = 900005
      set @ErrorMsg = 'The pallet is already complete'
      goto error
    end
    
    if (select dbo.ufn_Configuration(616, @WarehouseId)) = 1
    begin
    if @StatusCode in ('CD','D','DC')
    begin
      -- Waco001 - start
      update Job
         set StatusId = dbo.ufn_StatusId('IS','C'),
             DespatchOperatorId = @OperatorId,
             DespatchDate = GETDATE()
       where JobId = @JobId
      -- Waco001 - end
      
      update OutboundPerformance
         set Complete = @Getdate
       where JobId = @JobId
         and Complete is null
    end
    else
    begin
      set @Error = 900003
      set @ErrorMsg = 'Incorrect Status'
      goto error
    end
    end
    else
    begin
    if @StatusCode in ('D','DC')
    begin
      update Job
         set StatusId = dbo.ufn_StatusId('IS','C'),
             DespatchOperatorId = @OperatorId,
             DespatchDate = GETDATE()
       where JobId = @JobId
      
      update OutboundPerformance
         set Complete = @Getdate
       where JobId = @JobId
         and Complete is null
    end
    else
    begin
      set @Error = 900003
      set @ErrorMsg = 'Incorrect Status'
      goto error
    end
    end
    
    if @ShipmentId <> @OutboundShipmentId
    begin
      set @Error = 900004
      set @ErrorMsg = 'Could not tie up shipment and pallet'
      goto error
    end
  end
  else
  begin
   set @OutboundShipmentId = @ShipmentId
   set @OutboundDocumentId = @DocumentId
   -- Waco002 - start
    select top 1
          @IssueId            = ili.IssueId,
             @WarehouseId		 = iss.WarehouseId
        from IssueLineInstruction ili (nolock)
        join Issue iss (nolock) on ili.IssueId = iss.IssueId
        join OutboundDocument od (nolock) on iss.OutboundDocumentId = od.OutboundDocumentId
 join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where od.OrderNumber = @DocumentNumber       
    -- Waco002 - end   
    
    -- Waco003 - start
    set @StatusInvalid = 0
    set @StatusInvalid = (select top 1 1 
						  from Issue iss (nolock) 
						  join IssueLine il (nolock) on il.IssueId = iss.IssueId 
						  join Status s (nolock) on il.StatusId = s.StatusId
						 where iss.IssueId = @IssueId
						   and s.StatusCode not in ('CD','D','DC'))
    
    if @StatusInvalid = 1
    begin
		set @Error = 900010
        set @ErrorMsg = 'Some lines have incorrect status'
        goto error
    end   
   -- Waco003 - end
  end
------  else if @Vehicle is not null
------  begin
------    select @OutboundShipmentId = OutboundShipmentId
------      from OutboundShipment
------     where VehicleRegistration = @Vehicle
------  end
  
  if @OutboundShipmentId is not null
  begin
    select @Pallets = count(distinct(ins.JobId))
      from Instruction           ins (nolock)
      join Job                     j (nolock) on ins.JobId = j.JobId
      join Status                  s (nolock) on j.StatusId = s.StatusId
     where ins.OutboundShipmentId = @OutboundShipmentId
       and s.StatusCode not in  ('C','NS')
       and ins.ConfirmedQuantity > 0
    
    select @Total = count(distinct(ins.JobId))
      from Instruction           ins (nolock)
      join Job                     j (nolock) on ins.JobId = j.JobId
      join Status                  s (nolock) on j.StatusId = s.StatusId
     where ins.OutboundShipmentId = @OutboundShipmentId
       and s.StatusCode not in  ('NS')
       and ins.ConfirmedQuantity > 0
    
    exec @Error = p_OutboundShipment_Update
     @OutboundShipmentId = @OutboundShipmentId,
     @VehicleRegistration = @Vehicle
    
    update i
       set FirstLoaded = @GetDate
      from Issue i
      join OutboundShipmentIssue osi ON i.IssueId = osi.IssueId
                                    AND osi.OutboundShipmentId = @OutboundShipmentId
    
    if @Error <> 0
    begin
      set @Error = 900007
      set @ErrorMsg = 'Error updating OutboundShipment'
      goto error
    end
  end
  else if @DocumentId is not null
  begin
    if @DocumentId <> @OutboundDocumentId
    begin
      set @Error = 900004
      set @ErrorMsg = 'Could not tie up document and pallet'
      goto error
    end
    
    select @Pallets = count(distinct(ins.JobId))
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId = ISNULL(ins.InstructionRefId, ins.InstructionId)
                        and ins.ConfirmedQuantity > 0
      join Job                     j (nolock) on ins.JobId = j.JobId
      join Status                  s (nolock) on j.StatusId = s.StatusId
     where ili.IssueId = @IssueId
       and s.StatusCode not in  ('C','NS')
    
    select @Total = count(distinct(ins.JobId))
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId = ISNULL(ins.InstructionRefId, ins.InstructionId)
                                              and ins.ConfirmedQuantity > 0
      join Job                     j (nolock) on ins.JobId = j.JobId
      join Status                  s (nolock) on j.StatusId = s.StatusId
     where ili.IssueId = @IssueId
       and s.StatusCode not in  ('NS')
    
    select @Vehicle = left(@Vehicle,10)
 
    exec @Error = p_Issue_Update
     @IssueId             = @IssueId,
     @VehicleRegistration = @Vehicle,
     @FirstLoaded         = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 900007
      set @ErrorMsg = 'Error updating Issue'
      goto error
    end
  end
  
  -- GS - 2018-04-04 Add InstructionRefId and JobId to update the original Job to the same status as the new box
  insert @InstructionTable 
        (InstructionId,
         InstructionRefId,
         JobId,
         ParentInstructionId) 
  select InstructionId,
         InstructionRefId,
         JobId,
         ParentInstructionId
    from Instruction (nolock)
   where JobId = @JobId
  
  insert @InstructionTable
        (InstructionId,
        InstructionRefId,
        JobId,
        ParentInstructionId) 
   select i.InstructionId,
          i.InstructionRefId,
          i.JobId,
          i.ParentInstructionId
     from @InstructionTable t
    join Instruction i (nolock) ON t.InstructionRefId = i.InstructionId
                               AND i.Quantity = 0
                               AND NOT EXISTS(select top 1 1 
                                                from Instruction i2 
                                               where i.JobId = i2.JobId
                                                 and i.Quantity > 0)
  
  insert @InstructionTable
        (InstructionId,
        InstructionRefId,
        JobId,
        ParentInstructionId) 
   select i.InstructionId,
          i.InstructionRefId,
          i.JobId,
          i.ParentInstructionId
     from @InstructionTable t
    join Instruction i (nolock) ON t.ParentInstructionId = i.InstructionId
                               AND i.Quantity = 0
                               AND NOT EXISTS(select top 1 1 
                                                from Instruction i2 
                                               where i.JobId = i2.JobId
                                                 and i.Quantity > 0)
  
  update j
     set StatusId = original.StatusId
    from @InstructionTable t
    JOIN Job j ON t.JobId = j.JobId
    JOIN Job original ON original.JobId = @JobId
  
  DELETE @InstructionTable WHERE JobId != @JobId
  
  exec p_Status_Rollup_Issue @JobId
  
  while exists (select top 1 1 from @InstructionTable)
     begin
    select top 1 @InstructionId = InstructionId
      from @InstructionTable
    order by InstructionId asc
    
    delete @InstructionTable
    where InstructionId = @InstructionId
    
    select @Stored = Stored,
           @Picked = Picked,
           @StorageUnitBatchId = StorageUnitBatchId
      from Instruction
     where Instructionid = @InstructionId
    
    update Instruction
       set Stored = 1
     where InstructionId = @instructionId
    
    exec	@Error = p_StorageUnitBatchLocation_Reverse
        @instructionId = @instructionId,
        @Pick  = 0,
        @Store = 1,
        @Confirmed = 1
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @instructionId = @instructionId,
     @Pick  = 0,
     @Store = 1,
     @Confirmed = 1
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Deallocate'
      goto error
    end
    end
    
     commit transaction
  select @Error
  return @Error
  
  error:
    --raiserror @Error @Errormsg
    rollback transaction
    select @Error
    return @Error
end
--go
--IF OBJECT_ID('dbo.p_Mobile_Despatch_Confirm_Vehicle') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Mobile_Despatch_Confirm_Vehicle >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Mobile_Despatch_Confirm_Vehicle >>>'
--go

