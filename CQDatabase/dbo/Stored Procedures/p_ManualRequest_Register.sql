﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualRequest_Register
  ///   Filename       : p_ManualRequest_Register.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualRequest_Register
(
 @warehouseId        int,
 @operatorId         int,
 @storageUnitBatchId int,
 @quantity           float,
 @IssueLineId      int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @OrderNumber           nvarchar(20),
          @LineNumber            int,
          @OutboundDocumentTypeId int,
          @OutboundDocumentId     int,
          @OutboundLineId         int,
          @StorageUnitId         int,
          @BatchId               int,
          @StatusId              int,
          @Weight                numeric(13,3),
          @PalletQuantity        numeric(13,3),
          @ExternalCompanyId     int,
          @PriorityId            int,
          @ActualYield           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @BatchId       = BatchId,
         @StorageUnitId = StorageUnitId
    from StorageUnitBatch
   where StorageUnitBatchId = @storageUnitBatchId
  
  begin transaction
  
  if @IssueLineId is null
  begin
    select @StatusId = dbo.ufn_StatusId('ID','W')
    
    set @OrderNumber = 'MR' + convert(nvarchar(20), @Getdate, 120)
    
    select @OutboundDocumentTypeId = OutboundDocumentTypeId,
           @PriorityId             = PriorityId
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = 'MR'
    
    if @ExternalCompanyId is null
      set @ExternalCompanyId = 2
    
    exec @Error = p_OutboundDocument_Create
      @OutboundDocumentId     = @OutboundDocumentId output,
      @OutboundDocumentTypeId = @OutboundDocumentTypeId,
      @ExternalCompanyId      = @ExternalCompanyId,
      @StatusId               = @StatusId,
      @WarehouseId            = @WarehouseId,
      @OrderNumber            = @OrderNumber,
      @DeliveryDate           = @GetDate,
      @CreateDate             = @GetDate,
      @ModifiedDate           = null
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_OutboundDocument_Create'
      goto error
    end
    
    select @LineNumber = count(1) from OutboundLine where OutboundDocumentId = @OutboundDocumentId
    
    if @LineNumber is null
      set @LineNumber = 1
    else
      select @LineNumber = @LineNumber + 1
    
    exec @Error = p_OutboundLine_Create
     @OperatorId         = @OperatorId,
     @OutboundLineId     = @OutboundLineId output,
     @OutboundDocumentId = @OutboundDocumentId,
     @StorageUnitId      = @StorageUnitId,
     @LineNumber         = @LineNumber,
     @Quantity           = @Quantity,
     @BatchId            = @BatchId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_OutboundLine_Create'
      goto error
    end
		end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
