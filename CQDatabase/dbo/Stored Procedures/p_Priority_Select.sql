﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Priority_Select
  ///   Filename       : p_Priority_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:15
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Priority table.
  /// </remarks>
  /// <param>
  ///   @PriorityId int = null 
  /// </param>
  /// <returns>
  ///   Priority.PriorityId,
  ///   Priority.Priority,
  ///   Priority.PriorityCode,
  ///   Priority.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Priority_Select
(
 @PriorityId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Priority.PriorityId
        ,Priority.Priority
        ,Priority.PriorityCode
        ,Priority.OrderBy
    from Priority
   where isnull(Priority.PriorityId,'0')  = isnull(@PriorityId, isnull(Priority.PriorityId,'0'))
  
end
