﻿CREATE procedure [dbo].[p_Interface_WebService_IV]
(
 @doc2			varchar(max) output
)
--with encryption
as
begin

	Insert into tbl_Invoice (doc) select @Doc2	

	declare @doc xml
	set @doc = convert(xml,@doc2)
	DECLARE @idoc int
	Declare @InsertDate datetime
	
	Set @InsertDate = Getdate()
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
	
		Insert Into InterfaceImportIPHeader (RecordStatus,
							InsertDate,
						  OrderNumber,
						  InvoiceDate,
						  PrimaryKey,
						  CustomerAddress1,
						  CustomerAddress2,
						  CustomerAddress3,
						  CustomerAddress4,
						  CustomerAddress5,
						  CustomerAddress6,
						  DeliveryAddress1,
						  DeliveryAddress2,
						  DeliveryAddress3,
						  DeliveryAddress4,
						  DeliveryAddress5,
						  DeliveryAddress6,
						  CustomerName,
						  CustomerCode,
						  ReferenceNumber,
						  TaxReference,
						  SalesCode,
						  TotalNettValue,
						  LogisticsDataFee,
						  NettExcLogDataFee,
						  Tax,
						  TotalDue,
						  ChargeTax,
						  Message1,
						  Message2,
						  Message3)
		SELECT    'W',@InsertDate,*
		FROM       OPENXML (@idoc, '/Root/Header',1)
            WITH (OrderNumber varchar(50) 'OrderNumber',
				  InvoiceDate varchar(50) 'invDate',
				  PrimaryKey varchar(50) 'invNumber',
				  CustomerAddress1 varchar(255) 'pAddress1',
				  CustomerAddress2 varchar(255) 'pAddress2',
				  CustomerAddress3 varchar(255) 'pAddress3',
				  CustomerAddress4 varchar(255) 'pAddress4',
				  CustomerAddress5 varchar(255) 'pAddress5',
				  CustomerAddress6 varchar(255) 'pAddress6',
				  DeliveryAddress1 varchar(255) 'Address1',
				  DeliveryAddress2 varchar(255) 'Address2',
				  DeliveryAddress3 varchar(255) 'Address3',
				  DeliveryAddress4 varchar(255) 'Address4',
				  DeliveryAddress5 varchar(255) 'Address5',
				  DeliveryAddress6 varchar(255) 'Address6',
				  CustomerName		varchar(255) 'CustomerName',
				  CustomerCode		varchar(50) 'Account',
				  ReferenceNumber  varchar(50) 'ExtOrderNum',
				  TaxReference		varchar(50) 'TaxNumber',
				  SalesCode			Varchar(50) 'SalesCode',
				  TotalNettValue		Varchar(50) 'InvTotExcl',
				  LogisticsDataFee	Varchar(50) 'InvDiscAmnt',
				  NettExcLogDataFee	Varchar(50) 'InvTotInclDex',
				  Tax				varchar(50) 'InvTotTax',
				  TotalDue			Varchar(50) 'invTotIncl',
				  ChargeTax			Varchar(50) 'ChargeTax',
				  Message1			varchar(255) 'Message1',
				  Message2			varchar(255) 'Message2',
				  Message3			varchar(255) 'Message3')
		
		
		Insert Into InterfaceImportIPDetail (ForeignKey,
											Batch,
											Quantity,
											Excluding,
											Including,
											NettValue,
											ProductCode,
											Product)
		Select *											
		FROM       OPENXML (@idoc, '/Root/Header/row',1)
            WITH (
					ForeignKey  varchar(50) 'invNumber',
					Batch		varchar(50) 'Batch',
					Quantity varchar(50) 'Qty',
					Excluding	Varchar(50) 'UnitPriceExcl',
					Including	Varchar(50) 'UnitPriceIncl',
					NettValue	Varchar(50) 'UnitPriceIncl1',
					ProductCode varchar(50) 'Code',
					Product		Varchar(255) 'Desc')	
		
		
		
		Update InterfaceImportIPHeader set RecordStatus = 'N'
		Where RecordSTatus = 'W' and InsertDate = @InsertDate
		EXEC p_Pastel_Import_IV
		Set @doc2 = ''
End
