﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DivisionHistory_Insert
  ///   Filename       : p_DivisionHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:02
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the DivisionHistory table.
  /// </remarks>
  /// <param>
  ///   @DivisionId int = null output,
  ///   @DivisionCode nvarchar(20) = null,
  ///   @Division nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   DivisionHistory.DivisionId,
  ///   DivisionHistory.DivisionCode,
  ///   DivisionHistory.Division 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DivisionHistory_Insert
(
 @DivisionId int = null output,
 @DivisionCode nvarchar(20) = null,
 @Division nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @DivisionId = '-1'
    set @DivisionId = null;
  
	 declare @Error int
 
  insert DivisionHistory
        (DivisionCode,
         Division)
  select @DivisionCode,
         @Division 
  
  select @Error = @@Error, @DivisionId = scope_identity()
  
  
  return @Error
  
end
