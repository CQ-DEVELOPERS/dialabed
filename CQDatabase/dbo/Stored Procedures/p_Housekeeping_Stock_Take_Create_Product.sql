﻿  
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Create_Product
  ///   Filename       : p_Housekeeping_Stock_Take_Create_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Edgar Blount
  ///   Modified Date  : 
  ///   Details        : Support create stock take from Recon screen
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Create_Product
(
 @warehouseId                                       int,
 @operatorId                                            int,
 @jobId                            int = null output,
 @locationId                                            int = null,
 @StorageUnitBatchId      int = null,
 @storageUnitId              int = null,
 @StockTakeReferenceId  int = null,
 @ConfirmedQuantity    numeric(13,6) = null
)
 
as
begin
     set nocount on;
      
     declare @TableLocation as table
     (
      LocationId         int,
      StorageUnitBatchId int,
      ActualQuantity     float,
      AreaId             int,
      [Ailse]            nvarchar(10),
      [Column]           nvarchar(10),
      [Level]            nvarchar(10)
     )
   
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @InstructionTypeId   int,
          @InstructionId       int,
          @StatusId            int,
          @ActualQuantity      float,
          @referenceNumber     nvarchar(30),
          @InstructionTypeCode nvarchar(10),
          @Exception           bit,
          @AreaId              int,
          @OriginalJobId       int
   
  select @GetDate = dbo.ufn_Getdate()
   
  select @StatusId = dbo.ufn_StatusId('I','W')
   
  select @Exception = 0
  
  select @OriginalJobId = @JobId
   
  begin transaction
   
  if @Error <> 0
    goto error
   
  if @StorageUnitId is null
  begin
    insert @TableLocation
          (LocationId,
           StorageUnitBatchId,
           ActualQuantity,
           AreaId,
           [Ailse],
           [Column],
           [Level])
    select l.LocationId,
           subl.StorageUnitBatchId,
           subl.ActualQuantity,
           a.AreaId,
           l.[Ailse],
           l.[Column],
           l.[Level]
      from Location                    l (nolock)
      join AreaLocation               al (nolock) on l.LocationId = al.LocationId
      join Area                        a (nolock) on al.AreaId    = a.AreaId
      left outer
      join StorageUnitBatchLocation subl (nolock) on l.LocationId = subl.LocationId
     where l.LocationId            = isnull(@locationId, l.LocationId)
       and subl.StorageUnitBatchId = isnull(@storageUnitBatchId, subl.StorageUnitBatchId)
       and l.StocktakeInd          = 0
       and a.StockOnHand           = 1
       and a.WarehouseId           = @warehouseId
     
    if @@rowcount = 0
      insert @TableLocation
            (LocationId,
             StorageUnitBatchId,
             ActualQuantity,
             AreaId,
             [Ailse],
             [Column],
             [Level])
      select l.LocationId,
             @StorageUnitBatchId,
             0,
             a.AreaId,
             l.[Ailse],
             l.[Column],
             l.[Level]
        from Location                    l (nolock)
        join AreaLocation               al (nolock) on l.LocationId = al.LocationId
        join Area                        a (nolock) on al.AreaId    = a.AreaId
       where l.LocationId            = @locationId
         and l.StocktakeInd          = 0
         and a.StockOnHand           = 1
         and a.WarehouseId           = @warehouseId
  end
  else
  begin
    insert @TableLocation
          (LocationId,
           StorageUnitBatchId,
           ActualQuantity,
           AreaId,
           l.[Ailse],
           l.[Column],
           l.[Level])
    select l.LocationId,
           subl.StorageUnitBatchId,
           subl.ActualQuantity,
           a.AreaId,
           l.[Ailse],
           l.[Column],
           l.[Level]
         from Location                    l (nolock)
         join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
         join Area                        a (nolock) on al.AreaId               = a.AreaId
         join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
         join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
        where l.LocationId            = isnull(@locationId, l.LocationId)
          and sub.StorageUnitId       = @StorageUnitId
          and l.StocktakeInd          = 0
          and a.StockOnHand           = 1
          and a.WarehouseId           = @warehouseId
     end
     
     if @jobId is null
     begin
       set @Exception = 1
        
       if @locationId is null
      select @InstructionTypeId   = InstructionTypeId,
             @InstructionTypeCode = InstructionTypeCode
        from InstructionType
       where InstructionTypeCode = 'STP'
    else
      select @InstructionTypeId   = InstructionTypeId,
             @InstructionTypeCode = InstructionTypeCode
        from InstructionType
       where InstructionTypeCode = 'STE'
     end
     else
     begin
       select @InstructionTypeId = InstructionTypeId,
              @InstructionTypeCode = InstructionTypeCode
         from InstructionType
        where InstructionTypeCode = 'STL'
     end
       
       while exists(select 1 from @TableLocation)
       begin
         select top 1
                @LocationId         = LocationId,
                @StorageUnitBatchId = StorageUnitBatchId,
                @ActualQuantity     = isnull(ActualQuantity, 0),
                @AreaId             = AreaId
           from @TableLocation
          where AreaId =  ISNULL(@AreaId, AreaId)
        
         delete @TableLocation
          where LocationId                   = @LocationId
            and isnull(StorageUnitBatchId,0) = isnull(@StorageUnitBatchId,0)
       
       select @referenceNumber = Area
         from Area (nolock)
        where AreaId = @AreaId
       
       select @referenceNumber = isnull(@referenceNumber, '') + ' / ' + Operator
         from Operator (nolock)
        where OperatorId = @operatorId
       
       SET @JobId = NULL
       
       SELECT @JobId = j.JobId
         FROM Job j (NOLOCK)
         JOIN Instruction i (NOLOCK) ON j.Jobid = i.JobId
         JOIN AreaLocation al (NOLOCK) ON i.PickLocationId = al.LocationId
         JOIN Area a (NOLOCK) ON al.AreaId = a.AreaId
                             AND a.AreaId = @AreaId
        WHERE j.OriginalJobId = @JobId
       
       exec @Error = p_Housekeeping_Stock_Take_Create_Job
        @warehouseId         = @warehouseId,
        @operatorId          = @operatorId,
        @instructionTypeCode = @instructionTypeCode,
        @jobId               = @jobId output,
        @referenceNumber     = @referenceNumber,
        @OriginalJobId       = @OriginalJobId
       
       if @Error <> 0
         goto error
         
         exec @Error = p_Location_Update
          @locationId    = @LocationId,
          @stocktakeInd  = 1
         
         if @Error <> 0
           goto error
         
         exec @Error = p_Instruction_Insert
          @InstructionId       = @InstructionId output,
          @InstructionTypeId   = @InstructionTypeId,
          @StorageUnitBatchId  = @storageUnitBatchId,
          @WarehouseId         = @warehouseId,
          @StatusId            = @StatusId,
          @JobId               = @jobId,
          @OperatorId          = @OperatorId,
          @PickLocationId      = @LocationId,
          @Quantity            = @ActualQuantity,
          @CheckQuantity       = @ActualQuantity,
          @CreateDate          = @GetDate
     
         if @Error <> 0
           goto error
   end
   
  if (select dbo.ufn_Configuration(222, @WarehouseId)) = 1
  and @Exception = 1
  begin
    exec p_Housekeeping_Stock_Take_Release @jobId=@jobId,@operatorId=null,@statusCode='RL'
  end
   
  if @ConfirmedQuantity is not null
  begin
    exec @Error = p_Housekeeping_Stock_Take_Update_Count
     @instructionId = @instructionId,
     @operatorId    = @OperatorId,
     @latestCount   = @ConfirmedQuantity
  end
   
  commit transaction
  return
   
  error:
    raiserror 900000 'Error executing p_Housekeeping_Stock_Take_Create_Product'
    rollback transaction
    return @Error
end
