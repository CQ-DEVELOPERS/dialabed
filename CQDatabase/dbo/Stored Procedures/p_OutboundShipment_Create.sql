﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipment_Create
  ///   Filename       : p_OutboundShipment_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipment_Create
(
 @WarehouseId       int,
 @ShipmentDate      datetime,
 @Remarks           nvarchar(500),
 @OutboundShipmentId int = -1 output,
 @TotalOrders        int = null,
 @RouteId            int = null,
 @ReferenceNumber    nvarchar(30) = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @FP                bit,
          @SS                bit,
          @LP                bit,
          @FM                bit,
          @MP                bit,
          @PP                bit,
          @Weight            numeric(13,3),
          @Volume            numeric(13,3)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @FP is null
    select @FP = dbo.ufn_Configuration(48, @WarehouseId)
  
  if @SS is null
    select @SS = dbo.ufn_Configuration(49, @WarehouseId)
  
  if @LP is null
    select @LP = dbo.ufn_Configuration(50, @WarehouseId)
  
  if @FM is null
    select @FM = dbo.ufn_Configuration(51, @WarehouseId)
  
  if @MP is null
    select @MP = dbo.ufn_Configuration(52, @WarehouseId)
  
  if @PP is null
    select @PP = dbo.ufn_Configuration(53, @WarehouseId)
  
  select @Weight = convert(decimal(13,3), Value)
    from Configuration
   where ConfigurationId = 54
  
  select @Volume = convert(decimal(13,3), Value)
    from Configuration
   where ConfigurationId = 55
  
  begin transaction
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'W'
     and Type       = 'IS'
  
  exec @Error = p_OutboundShipment_Insert
   @OutboundShipmentId = @OutboundShipmentId output,
   @StatusId           = @StatusId,
   @WarehouseId        = @WarehouseId,
   @ShipmentDate       = @ShipmentDate,
   @Remarks            = @Remarks,
   @Weight             = @Weight,
   @Volume             = @Volume,
   @FP                 = @FP,
   @SS                 = @SS,
   @LP                 = @LP,
   @FM                 = @FM,
   @MP                 = @MP,
   @PP                 = @PP,
   @TotalOrders        = @TotalOrders,
   @RouteId            = @RouteId,
   @ReferenceNumber    = @ReferenceNumber
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_OutboundShipment_Create'
    rollback transaction
    return @Error
end

