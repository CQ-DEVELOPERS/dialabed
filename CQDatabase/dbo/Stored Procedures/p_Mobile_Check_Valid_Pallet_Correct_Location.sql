﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Valid_Pallet_Correct_Location
  ///   Filename       : p_Mobile_Check_Valid_Pallet_Correct_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_Valid_Pallet_Correct_Location
(
 @barcode		nvarchar(30),
 @locationId	int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @bool               bit,
	         @PalletId           int,
	         @StorageUnitBatchId int,
	         @Batch              nvarchar(30),
	         @AreaId			 int,
	         @AreaCode			 nvarchar(10),
	         @StorageUnitId		 int,
	         @ProductId			 int,
	         @BatchId			 int
  
	set @bool = 1

	if @locationId = -1
		set @locationId = null

	if @barcode like 'P:%'
		if isnumeric(replace(@barcode,'P:','')) = 1
			select @PalletId = replace(@barcode,'P:',''),
				   @barcode  = null

	if @locationId is not null
	begin
		select	@AreaId		= a.AreaId,
				@AreaCode	= a.AreaCode
		  from	AreaLocation		al (nolock) 
		  join	Area				 a (nolock) on al.AreaId = a.AreaId
		 where al.LocationId = @locationId
	end  


	select	@StorageUnitId = su.StorageUnitId,
			@ProductId     = p.ProductId
	  from	StorageUnit su (nolock)
	  join	Product      p (nolock) on p.ProductId = su.ProductId
	  join  Pack		pa (nolock) on pa.StorageUnitId = su.StorageUnitId
	  join  PackType	pt (nolock) on pa.PackTypeId = pt.PackTypeId
	  join	SKU        sku (nolock) on sku.SKUId = su.SKUId
	 where	isnull(p.ProductType,'') != 'FPC'
	   --AND pt.PackTypeCode LIKE '%unit%'
	   AND pa.WarehouseId = 1
	   and (p.ProductCode	= @barcode
	    or	p.Barcode		= @barcode
	    or	pa.Barcode		= @barcode
	    or  sku.SKUCode		= @barcode)


	if @StorageUnitId is not null
	begin
		select @BatchId = b.BatchId
		  from StorageUnitBatch sub (nolock)
		  join Batch              b (nolock) on sub.BatchId = b.BatchId
		 where sub.StorageUnitId = @StorageUnitId
		   and b.Batch           = isnull(@batch,b.Batch)

		select @StorageUnitBatchId = StorageUnitBatchId
		  from StorageUnitBatch (nolock)
		 where StorageUnitId = @StorageUnitId
		   and BatchId       = @BatchId
	end


	--If @AreaCode in ('PK')
	--begin
	--	if not exists (select top 1 1 
	--					 from StorageUnitLocation sul (nolock) 
	--					where sul.StorageUnitId = @StorageUnitId
	--					  and sul.LocationId = @locationId)
	--		set @bool = 0
	--end
	--else
	--begin
		if not exists (select top 1 1 
						 from StorageUnitArea sua (nolock) 
						where sua.StorageUnitId = @StorageUnitId
						  and sua.AreaId = @AreaId)
			set @bool = 0
	--end

select @bool  
  
end
 
 
 
 
