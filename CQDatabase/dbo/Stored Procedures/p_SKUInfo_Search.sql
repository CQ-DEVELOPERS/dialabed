﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKUInfo_Search
  ///   Filename       : p_SKUInfo_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:37
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the SKUInfo table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   SKUInfo.SKUId,
  ///   SKUInfo.WarehouseId,
  ///   SKUInfo.Quantity,
  ///   SKUInfo.AlternatePallet,
  ///   SKUInfo.SubstitutePallet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKUInfo_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         SKUInfo.SKUId
        ,SKUInfo.WarehouseId
        ,SKUInfo.Quantity
        ,SKUInfo.AlternatePallet
        ,SKUInfo.SubstitutePallet
    from SKUInfo
  
end
