﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLocation_Update
  ///   Filename       : p_InterfaceLocation_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:23
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceLocation table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @Location nvarchar(60) = null,
  ///   @LocationDescription nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null,
  ///   @Address2 nvarchar(510) = null,
  ///   @Address3 nvarchar(510) = null,
  ///   @Address4 nvarchar(510) = null,
  ///   @City nvarchar(510) = null,
  ///   @State nvarchar(510) = null,
  ///   @Zip nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLocation_Update
(
 @HostId nvarchar(60) = null,
 @Location nvarchar(60) = null,
 @LocationDescription nvarchar(510) = null,
 @Address1 nvarchar(510) = null,
 @Address2 nvarchar(510) = null,
 @Address3 nvarchar(510) = null,
 @Address4 nvarchar(510) = null,
 @City nvarchar(510) = null,
 @State nvarchar(510) = null,
 @Zip nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceLocation
     set HostId = isnull(@HostId, HostId),
         Location = isnull(@Location, Location),
         LocationDescription = isnull(@LocationDescription, LocationDescription),
         Address1 = isnull(@Address1, Address1),
         Address2 = isnull(@Address2, Address2),
         Address3 = isnull(@Address3, Address3),
         Address4 = isnull(@Address4, Address4),
         City = isnull(@City, City),
         State = isnull(@State, State),
         Zip = isnull(@Zip, Zip),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         InsertDate = isnull(@InsertDate, InsertDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
