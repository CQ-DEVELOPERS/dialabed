--IF OBJECT_ID('dbo.p_Mobile_Product_Check_List_Quantity') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Mobile_Product_Check_List_Quantity
--    IF OBJECT_ID('dbo.p_Mobile_Product_Check_List_Quantity') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Mobile_Product_Check_List_Quantity >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Mobile_Product_Check_List_Quantity >>>'
--END
--go

/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_List_Quantity
  ///   Filename       : p_Mobile_Product_Check_List_Quantity.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_List_Quantity
(
 @WarehouseId        int,
 @jobId              int,
 @storageUnitBatchId int,
 @quantity           numeric(13,6),
 @storageUnitId int = null
)

 as
begin
	 set nocount on;

  declare @TableResult as table
  (
   Id int identity
  ,InstructionId int
  ,ConfirmedQuantity numeric(13,6)
  )
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Count             INT,
          @Id                INT,
          @InstructionId     INT,
          @ConfirmedQuantity NUMERIC(13,6),
          @UpdateQuantity    NUMERIC(13,6)
  
  select @GetDate = dbo.ufn_Getdate()

  begin transaction

  INSERT @TableResult
        (InstructionId
        ,ConfirmedQuantity)
  SELECT InstructionId
        ,ConfirmedQuantity
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where JobId = @jobId
     and sub.StorageUnitId = @StorageUnitId
  
  SELECT @Count = COUNT(1) FROM @TableResult
  
  WHILE @Count > 0
  BEGIN
    SET @Count = @Count - 1

    SELECT TOP 1
           @Id = Id
          ,@InstructionId = InstructionId
          ,@ConfirmedQuantity = ConfirmedQuantity
      FROM @TableResult

    DELETE @TableResult WHERE Id = @Id

    IF @quantity > @ConfirmedQuantity AND @Count > 0 -- This will ensure any remianing quantity goes against the last line.
    BEGIN
      SET @UpdateQuantity = @ConfirmedQuantity
    END
    ELSE
    BEGIN
      SET @UpdateQuantity = @quantity
    END

    SET @quantity = @quantity - @UpdateQuantity
    
    UPDATE i
       SET CheckQuantity = @UpdateQuantity
      FROM Instruction        i (nolock)
     WHERE InstructionId = @InstructionId
    
    select @Error = @@error
  
    if @Error <> 0
      goto error
  END

 
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Mobile_Product_Check_List_Quantity');
    rollback transaction
    return @Error
end


--go
--IF OBJECT_ID('dbo.p_Mobile_Product_Check_List_Quantity') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Mobile_Product_Check_List_Quantity >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Mobile_Product_Check_List_Quantity >>>'
--go


