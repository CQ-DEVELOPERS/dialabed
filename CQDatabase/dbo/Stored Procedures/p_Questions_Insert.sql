﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questions_Insert
  ///   Filename       : p_Questions_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:03
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Questions table.
  /// </remarks>
  /// <param>
  ///   @QuestionId int = null output,
  ///   @QuestionaireId int = null,
  ///   @Category nvarchar(100) = null,
  ///   @Code nvarchar(100) = null,
  ///   @Sequence int = null,
  ///   @Active bit = null,
  ///   @QuestionText nvarchar(510) = null,
  ///   @QuestionType nvarchar(510) = null,
  ///   @DateUpdated datetime = null 
  /// </param>
  /// <returns>
  ///   Questions.QuestionId,
  ///   Questions.QuestionaireId,
  ///   Questions.Category,
  ///   Questions.Code,
  ///   Questions.Sequence,
  ///   Questions.Active,
  ///   Questions.QuestionText,
  ///   Questions.QuestionType,
  ///   Questions.DateUpdated 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Questions_Insert
(
 @QuestionId int = null output,
 @QuestionaireId int = null,
 @Category nvarchar(100) = null,
 @Code nvarchar(100) = null,
 @Sequence int = null,
 @Active bit = null,
 @QuestionText nvarchar(510) = null,
 @QuestionType nvarchar(510) = null,
 @DateUpdated datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @QuestionId = '-1'
    set @QuestionId = null;
  
	 declare @Error int
 
  insert Questions
        (QuestionaireId,
         Category,
         Code,
         Sequence,
         Active,
         QuestionText,
         QuestionType,
         DateUpdated)
  select @QuestionaireId,
         @Category,
         @Code,
         @Sequence,
         @Active,
         @QuestionText,
         @QuestionType,
         @DateUpdated 
  
  select @Error = @@Error, @QuestionId = scope_identity()
  
  
  return @Error
  
end
