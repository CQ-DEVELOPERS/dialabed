﻿ 
/*
/// <summary>
///   Procedure Name : p_Pastel_Import_PO
///   Filename       : p_Pastel_Import_PO.sql
///   Create By      : Grant Schultz
///   Date Created   : 10 Feb 2009
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    : Karen
///   Modified Date  : 16 September 2010
///   Details        : Create detailed error log InterfaceDashboardBulkLog
/// </newpara>
*/
create procedure [dbo].[p_Pastel_Import_PO]
as
begin
  -- InterfaceImportPOHeader
  declare @InterfaceImportPOHeaderId    int,
          @OrderNumber                  varchar(20),
          @ExternalCompanyCode          varchar(30),
          @CreateDate                   varchar(255),
          @Status                       varchar(255),
          @FromWarehouseCode            varchar(30),
          @ToWarehouseCode              varchar(30),
          @RecordType                   Varchar(10),
          @Remarks                      varchar(255),
          @Instructions                 varchar(255),
          -- InterfaceImportPODetail
          @LineNumber                   int,
          @ProductCode                  varchar(50),
          @Quantity                     numeric(13,3),
          @PickUp                       varchar(255),
          -- InterfaceDashboardBulkLog  (Karen)
          @FileKeyId                    INT,
          @FileType                     VARCHAR(100),
          @ProcessedDate                DATETIME,
          @FileName                     VARCHAR(50) ,
          @ErrorCode                    CHAR (5),
          @ErrorDescription             VARCHAR (255),
          -- Internal variables
          @Product                      varchar(50),
          @InboundDocumentTypeId        int,
          @InboundDocumentId            int,
          @InboundLineId                int,
          @ReceiptLineId                int,
          @GetDate                      datetime,
          @StorageUnitId                int,
          @StorageUnitBatchId           int,
          @BatchId                      int,
          @Batch                        varchar(30),
          @Error                        int,
          @Error2                       int,
          @StatusId                     int,
          @Weight                       numeric(13,3),
          @PalletQuantity               numeric(13,3),
          @ErrorMsg                     varchar(100),
          @ExternalCompanyId            int ,
          @ExternalCompany              varchar(255),
          @FromWarehouseId              int,
          @InboundDocumentTypeCode      varchar(10),
          @InPriorityId                 int,
          @InterfaceMessage             varchar (255),
          @ErrorStatus                  varchar(1),
          @PrincipalCode                nvarchar(30),
          @PrincipalId                  int
  
  select @GetDate = dbo.ufn_Getdate()
         
  update d
     set InterfaceImportPOHeaderId = (select max(InterfaceImportPOHeaderId)
    from InterfaceImportPOHeader h
   where d.ForeignKey = h.PrimaryKey)
    from InterfaceImportPODetail d
   where InterfaceImportPOHeaderId is null
  
  begin transaction
  
  update h
     set ProcessedDate = @Getdate
    from InterfaceImportPOHeader h
   where exists(select 1 from InterfaceImportPODetail d where h.PrimaryKey = d.ForeignKey)
     and h.RecordStatus in ('N','U')
     and ProcessedDate is null
  
  update h
     set OrderNumber = PrimaryKey
    from InterfaceImportPOHeader h
   where h.RecordStatus in ('N','U')
     and ProcessedDate = @Getdate
     and OrderNumber = ''
  
  set @ErrorStatus = 'N'
         
         
  declare header_cursor                 cursor for
  select InterfaceImportPOHeaderId,
         OrderNumber,       -- OrderNumber
         SupplierCode,      -- SupplierCode
         Additional1,       -- CreateDate
         Additional2,       -- Status
         FromWarehouseCode, -- Location
         Remarks,           -- Remarks
         Additional3,        -- Instructions
         RecordType,
         ToWarehouseCode,
         PrincipalCode
    from InterfaceImportPOHeader
   where ProcessedDate = @Getdate
  order by PrimaryKey
  
  open header_cursor
  
  fetch header_cursor
    into @InterfaceImportPOHeaderId,
         @OrderNumber,
         @ExternalCompanyCode,
         @CreateDate,
         @Status,
         @FromWarehouseCode,
         @Remarks,
         @Instructions,
         @RecordType,
         @ToWarehouseCode,
         @PrincipalCode
         
  while (@@fetch_status = 0)
  begin
    set rowcount 1
    update InterfaceImportPOHeader
       set RecordStatus = 'C'
     where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
    set rowcount 0
    set @InterfaceMessage = 'Success'
    set @InboundDocumentTypeId = null
    set @InboundDocumentTypeCode = null
    set @FromWarehouseId = 1
    
    select @InboundDocumentTypeId    = InboundDocumentTypeId,
           @InboundDocumentTypeCode = InboundDocumentTypeCode
      from InboundDocumentType
     where InboundDocumentTypeCode = @RecordType
    
    If (@InboundDocumentTypeId is null)
    Begin
      select @InboundDocumentTypeId    = InboundDocumentTypeId,
             @InboundDocumentTypeCode = InboundDocumentTypeCode
        from InboundDocumentType
       where InboundDocumentTypeCode = 'RCP'
    End
    
    set @PrincipalId = null
    
    select @PrincipalId = PrincipalId
      from Principal (nolock)
     where PrincipalCode = @PrincipalCode
    
    exec @Error = p_Inbound_Order_Delete
    @OrderNumber = @OrderNumber,
    @PrincipalId = @PrincipalId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Inbound_Order_Delete'
             
      -- Karen - setup error message for create into InterfaceMessage
      
         Set @InterfaceMessage = @InterfaceMessage + 'Error deleting Inbound_Order, '
             
      goto next_header
    end
    
    If @InboundDocumentTypeCode = 'IBT'
    Begin
      select @ExternalCompany=Warehouse,
             @ExternalCompanyCode=WarehouseCode,
             @FromWarehouseId=ParentWarehouseId
        from Warehouse
       where HostId = @ToWarehouseCode
      
      if Len(Isnull(@ExternalCompany,''))= 0
      begin
        if @@trancount > 0
        rollback transaction
        
        update InterfaceImportPOHeader
           set RecordStatus = 'E'
         where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
           set @ErrorStatus = 'E'
           set @InterfaceMessage = 'Error'
        
        begin transaction
      end
      
      select @ExternalCompanyCode = ExternalCompanyCode,
             @ExternalCompanyId = ExternalCompanyId
        from ExternalCompany
       where ExternalCompanyCode = @ExternalCompanyCode
         and ExternalCompanyTypeId = 2
      
      if @ExternalCompanyId is null
      begin
        
        exec @Error = p_ExternalCompany_Insert
        @ExternalCompanyId         = @ExternalCompanyId output,
        @ExternalCompanyTypeId     = 2,
        @RouteId                   = null,
        @ExternalCompany           = @ExternalCompany,
        @ExternalCompanyCode       = @ExternalCompanyCode
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
                 
          -- Karen - setup error message for create into InterfaceMessage
          
             Set @InterfaceMessage = 'Error inserting External Company'
                 
          goto error_header
        end
      end
    End
    Else
    Begin
      if not exists(select 1 from Warehouse where WarehouseId = @FromWarehouseId)
      begin
        if @@trancount > 0
 rollback transaction
        
        set @InterfaceMessage = 'Error'
        set @ErrorStatus = 'E'
        
        update InterfaceImportPOHeader
           set RecordStatus = 'E'
         where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
        
        begin transaction
      end
      
      if @InboundDocumentTypeCode is not null
      begin
           set @ExternalCompanyId = null
               
        select @ExternalCompanyCode = ExternalCompanyCode,
@ExternalCompany  = ExternalCompany
          from ExternalCompany
         where HostId = @ExternalCompanyCode
        
        if @ExternalCompanyCode is not null
        begin
          select @ExternalCompanyId   = ExternalCompanyId,
                 @ExternalCompanyCode = ExternalCompanyCode,
                 @ExternalCompany     = ExternalCompany
            from ExternalCompany (nolock)
           where ExternalCompanyCode = @ExternalCompanyCode
          
          if @ExternalCompanyId is null
          begin
               set @ExternalCompany = @ExternalCompanyCode
               
               exec @Error = p_ExternalCompany_Insert
                @ExternalCompanyId         = @ExternalCompanyId output,
                @ExternalCompanyTypeId     = 2,
                @RouteId                   = null,
                --@drop
                @ExternalCompany           = @ExternalCompany,
                @ExternalCompanyCode       = @ExternalCompanyCode
                   
            if @Error != 0
            begin
            select @ExternalCompanyId, @ExternalCompanyCode, @ExternalCompany
              select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
                     
              -- Karen - setup error message for create into InterfaceMessage
              
                 Set @InterfaceMessage = 'Error inserting ExternalCompany - ExternalCompanyId is blank'
                     
              goto error_header
            end
          end
        end
      End
    End
    
    select @StatusId = dbo.ufn_StatusId('ID','W')
           
           exec @Error = p_InboundDocument_Insert
           @InboundDocumentId     = @InboundDocumentId output,
           @InboundDocumentTypeId = @InboundDocumentTypeId,
           @ExternalCompanyId     = @ExternalCompanyId,
           @StatusId              = @StatusId,
           @WarehouseId           = @FromWarehouseId,
           @OrderNumber           = @OrderNumber,
           @DeliveryDate          = @GetDate,
           @CreateDate            = @GetDate,
           @ModifiedDate          = null,
           @PrincipalId           = @PrincipalId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_InboundDocument_Insert'
      -- Karen - setup error message for create into InterfaceMessage
      Set @InterfaceMessage = 'Error inserting InboundDocument'
      goto error_header
    end
    
    
    declare detail_cursor                 cursor for
    select ForeignKey,  -- OrderNumber
           LineNumber,  -- LineNumber
           ProductCode, -- ProductCode
           Quantity,    -- Quantity
           Additional1,  -- PickUp
           Batch
      from InterfaceImportPODetail
     where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
    if (@InboundDocumentTypeCode <> 'IBT')
       set @FromWarehouseId = null
           
    open detail_cursor
    
    fetch detail_cursor
      into @OrderNumber,
           @LineNumber,
           @ProductCode,
           @Quantity,
           @PickUp,
           @Batch
           
    while (@@fetch_status = 0)
    begin
      set @StorageUnitBatchId = null
      set @StorageUnitId = null
      
      if (@FromWarehouseId is null) and (@InboundDocumentTypeCode <> 'IBT')
      begin
        set @Batch = null
        set @Product = null
        --set @Batch = convert(varchar(30), @OrderNumber)

        --select @FromWarehouseId = ParentWarehouseId,
        --       @FromWarehouseCode = WarehouseCode
        --  from Warehouse (nolock)
        -- where HostId = @PickUp
        
        --if @FromWarehouseId is null
        --begin
        --  select @ErrorMsg = 'Error executing p_Pastel_Import_PO - Could not find warehouse'
        --  -- Karen - setup error message for create into InterfaceMessage
        --  Set @InterfaceMessage = 'Could not find warehouse'
        --  close detail_cursor
        --  deallocate detail_cursor
        -- goto next_header
        --end
        
        exec p_InboundDocument_Update
         @InboundDocumentId = @InboundDocumentId,
         @WarehouseId       = @FromWarehouseId,
         @FromLocation      = @FromWarehouseCode
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_InboundDocument_Update'
          -- Karen - setup error message for create into InterfaceMessage
          Set @InterfaceMessage = 'Error updating InboundDocument'
          goto error_detail
        end
      end
      
      select @ProductCode = ProductCode,
             @Product     = Product
        from Product (nolock)
       where ProductCode = @ProductCode
      
      if @@rowcount = 0
        select @ProductCode = ProductCode,
               @Product     = Product
          from Product (nolock)
         where HostId = @ProductCode
      
      if @Product is null
      begin
        select @ErrorMsg = 'Error executing Product does not exist'
        -- Karen - setup error message for create into InterfaceMessage
        Set @InterfaceMessage = 'Error creating Product - does not exist'
        close detail_cursor
        deallocate detail_cursor
        goto next_header
      end
      
      exec @Error = p_interface_xml_Product_Insert
      @ProductCode        = @ProductCode,
      @Product            = @Product,
      @Batch              = @Batch,
      @WarehouseId        = @FromWarehouseId,
      @StorageUnitId      = @StorageUnitId output,
      @StorageUnitBatchId = @StorageUnitBatchId output,
      @BatchId            = @BatchId output
      
      if @Error != 0 or @StorageUnitBatchId is null
      begin
        select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
               
        -- Karen - setup error message for create into InterfaceMessage
        
           Set @InterfaceMessage = 'Error inserting Product - StorageUnitBatchId is blank'
               
        goto error_detail
      end
      
      if @InboundDocumentTypeCode is not null
      begin
        exec @Error = p_InboundLine_Insert
        @InboundLineId     = @InboundLineId output,
        @InboundDocumentId = @InboundDocumentId,
        @StorageUnitId     = @StorageUnitId,
        @StatusId          = @StatusId,
        @LineNumber        = @LineNumber,
        @Quantity          = @Quantity,
        @BatchId           = @BatchId
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_InboundLine_Insert'
                 
          -- Karen - setup error message for create into InterfaceMessage
          
             Set @InterfaceMessage = 'Error inserting InboundLine - InboundDocumentTypeCode is blank'
                 
          goto error_detail
        end
        
        exec @Error = p_Receiving_Create_Receipt
        @InboundDocumentId = @InboundDocumentId,
        @InboundLineId     = @InboundLineId,
        @OperatorId        = null
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Receiving_Create_Receipt'
                 
          -- Karen - setup error message for create into InterfaceMessage
          
             Set @InterfaceMessage = 'Error executing Receiving_Create_Receipt'
                 
          goto error_detail
        end
      end
      
      fetch detail_cursor
        into @OrderNumber,
             @LineNumber,
             @ProductCode,
             @Quantity,
             @PickUp,
             @Batch
    end
    
    close detail_cursor
    deallocate detail_cursor
    
    goto fetch_header
    next_header:
    
    update InterfaceImportPOHeader
       set RecordStatus = 'E'
     where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
       set @ErrorStatus = 'E'
       set @InterfaceMessage = 'Error'
           
    fetch_header:
    
    fetch header_cursor
     into @InterfaceImportPOHeaderId,
          @OrderNumber,
          @ExternalCompanyCode,
          @CreateDate,
          @Status,
          @FromWarehouseCode,
          @Remarks,
          @Instructions,
          @RecordType,
          @ToWarehouseCode,
          @PrincipalCode
           
    -- Karen - added error message create into InterfaceMessage
    INSERT INTO InterfaceMessage
          ([InterfaceMessageCode]
          ,[InterfaceMessage]
          ,[InterfaceId]
          ,[InterfaceTable]
          ,[Status]
          ,[OrderNumber]
          ,[CreateDate]
          ,[ProcessedDate])
    VALUES
           ('Processed'
           ,@InterfaceMessage
           ,@InterfaceImportPOHeaderId
           ,'InterfaceImportPOHeader'
           ,@ErrorStatus
           ,@OrderNumber
           ,@GetDate
           ,@GetDate)
           
  end
  
  close header_cursor
  deallocate header_cursor
  
  commit transaction
  return
  
  error_detail:
  
  -- Karen - added error message create into InterfaceMessage
  
  --INSERT INTO InterfaceMessage
  --      ([InterfaceMessageCode]
           --      ,[InterfaceMessage]
           --      ,[InterfaceId]
           --   ,[InterfaceTable]
           --   ,[Status]
           --   ,[OrderNumber]
           --   ,[CreateDate]
           --   ,[ProcessedDate])
  --VALUES
  --   ('Failed'
        --   ,@InterfaceMessage
        --   ,@InterfaceImportPOHeaderId
        --   ,'InterfaceImportPODetail'
        --   ,'E'
        --   ,@OrderNumber
        --   ,@GetDate
        --   ,@GetDate)
  close detail_cursor
  deallocate detail_cursor
  
  error_header:
  
  -- Karen - added error message create into InterfaceMessage
  INSERT INTO InterfaceMessage
         ([InterfaceMessageCode]
         ,[InterfaceMessage]
         ,[InterfaceId]
         ,[InterfaceTable]
         ,[Status]
         ,[OrderNumber]
         ,[CreateDate]
         ,[ProcessedDate])
  VALUES
         ('Failed'
         ,@InterfaceMessage
         ,@InterfaceImportPOHeaderId
         ,'InterfaceImportPOHeader'
         ,'E'
         ,@OrderNumber
         ,@GetDate
         ,@GetDate)
  
  close header_cursor
  deallocate header_cursor
  raiserror 900000 @ErrorMsg
  rollback transaction
  
  return
end
