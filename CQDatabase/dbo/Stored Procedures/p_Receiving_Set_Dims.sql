﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Set_Dims
  ///   Filename       : p_Receiving_Set_Dims.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Set_Dims
(
 @receiptLineId  int,
 @Length         numeric(13,2),
 @Height         numeric(13,2),
 @Width          numeric(13,2),
 @Weight         numeric(13,2),
 @Quantity       numeric(13,2),
 @PalletQuantity numeric(13,2) = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Exception		 nvarchar(255),
          @ProductCode		 nvarchar(50),
          @SKUCode			 nvarchar(50),
          @PrincipalCode	 nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
   
  if dbo.ufn_Configuration(434, 1) = 0
  begin
  
   select @ProductCode = p.ProductCode,
		 @SKUCode = sku.SKUCode,
		 @PrincipalCode = pri.PrincipalCode
    from ReceiptLine       rl (nolock)
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
    join SKU			  sku (nolock) on su.SKUId = sku.SKUId
    join Principal		  pri (nolock) on p.PrincipalId = pri.PrincipalId
    
  set @Exception = 'DIMS for Product(' + LTRIM(RTRIM(@ProductCode)) + '), Principal(' + LTRIM(RTRIM(@PrincipalCode)) + '), SKU(' + LTRIM(RTRIM(@SKUCode)) + '), is incorrect. Should be Length(' + LTRIM(RTRIM(@Length)) + '), Height('+ LTRIM(RTRIM(@Height)) 
+'), Width('+ LTRIM(RTRIM(@Width)) + '), Weight(' + LTRIM(RTRIM(@Weight)) + ')'
  
	 exec @Error = p_Exception_Insert
       @ExceptionId   = null,
       @ExceptionCode = 'DIMERR',
       @Exception     = @Exception,
       @CreateDate    = @GetDate,
       @ExceptionDate = @Getdate,
       @Quantity      = null
  end
  else
  begin
  update Pack
     set Length   = @Length,
         Width    = @Width,
         Height   = @Height,
         Weight   = @Weight,
         Quantity = @Quantity
    from ReceiptLine       rl (nolock)
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId    
    join Pack               p (nolock) on sub.StorageUnitId = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId = pt.PackTypeId
   where pt.PackTypeCode = 'Unit'
     and rl.ReceiptLineId = @receiptLineId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  update Pack
     set Quantity = @palletQuantity
    from ReceiptLine       rl (nolock)
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Pack               p (nolock) on sub.StorageUnitId = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId = pt.PackTypeId
                                      and pt.InboundSequence = 1
   where rl.ReceiptLineId = @receiptLineId
  
  select @Error = @@ERROR
  end
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Receiving_Set_Dims'
    rollback transaction
    return @Error
end
 
 
