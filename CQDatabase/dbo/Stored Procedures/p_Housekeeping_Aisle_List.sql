﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Aisle_List
  ///   Filename       : p_Housekeeping_Aisle_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 May 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Aisle_List
(
 @WarehouseId int
)
 
as
begin
	 set nocount on;
  
  select distinct Ailse as 'Aisle'
    from viewLocation
   where WarehouseId = @WarehouseId
     and Ailse is not null
  order by Ailse
end
