﻿   
/*  
  /// <summary>  
  ///   Procedure Name : p_Inbound_Order_Delete  
  ///   Filename       : p_Inbound_Order_Delete.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 24 Oct 2008  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Inbound_Order_Delete  
(  
 @OrderNumber   nvarchar(30),  
 @DeliveryDate  datetime = null,  
 @PrincipalId   int = null,
 @WarehouseCode nvarchar(255) = null
)  
   
as  
begin  
  set nocount on;  
    
  declare @Error              int,  
          @Errormsg           nvarchar(500),  
          @GetDate            datetime,  
          @Transaction       bit = 0,  
          @InboundDocumentId  int,  
          @ReceiptId          int,
          @WarehouseId        int
          
  select @WarehouseId = WarehouseId from Warehouse (nolock) where WarehouseCode = @WarehouseCode
    
  select @GetDate = dbo.ufn_Getdate()  
  set @Error = 0  
    
  if @DeliveryDate is null  
    select @InboundDocumentId = max(InboundDocumentId)  
      from InboundDocument  
     where OrderNumber = @OrderNumber  
       and isnull(PrincipalId,-1) = isnull(@PrincipalId, isnull(PrincipalId,-1))
       and WarehouseId = @WarehouseId 
  else  
    select @InboundDocumentId = max(InboundDocumentId)  
      from InboundDocument  
     where OrderNumber  = @OrderNumber  
       and DeliveryDate = @DeliveryDate  
       and isnull(PrincipalId,-1) = isnull(@PrincipalId, isnull(PrincipalId,-1))
       and WarehouseId = @WarehouseId
    
  if @@rowcount = 0  
    return;  
    
  select @ReceiptId = max(ReceiptId)  
    from Receipt  
   where InboundDocumentId = @InboundDocumentId  
    
  if @@trancount = 0  
    begin  
      begin transaction  
      set @Transaction = 1  
    end  
    
  if exists(select top 1 1  
              from Receipt r  
              join ReceiptLine rl on r.ReceiptId = rl.ReceiptId  
              join Status       s on rl.StatusId = s.StatusId  
--             where r.InboundDocumentId = @InboundDocumentId  
             where r.ReceiptId   = @ReceiptId  
               and s.StatusCode != 'W')  
    Begin  
  set @Error = -1  
  goto error  
    End  
    
--  exec @Error = p_Palletise_Reverse  
--   @ReceiptId     = @ReceiptId,  
--   @ReceiptLineId = @ReceiptLineId  
    
  if @InboundDocumentId is not null  
  begin  
    delete e  
      from ReceiptLine rl  
      join Exception    e on rl.ReceiptLineId = e.ReceiptLineId  
     where rl.ReceiptId = @ReceiptId  
      
    if @@Error <> 0  
      goto error  
      
    delete il  
      from ReceiptLine il  
      join Receipt      i on il.ReceiptId = i.ReceiptId  
     where il.ReceiptId = @ReceiptId  
      
    if @@Error <> 0  
      goto error  
      
    delete osi  
      from InboundShipmentReceipt osi  
      join Receipt                  i on osi.ReceiptId = i.ReceiptId  
     where i.ReceiptId = @ReceiptId  
      
    if @@Error <> 0  
      goto error  
      
    delete Receipt  
     where ReceiptId = @ReceiptId  
      
    if @@Error <> 0  
      goto error  
      
    delete InboundLine  
     where InboundDocumentId = @InboundDocumentId  
       and not exists(select top 1 1 from ReceiptLine where ReceiptLine.InboundLineId = InboundLine.InboundLineId)  
      
    if @@Error <> 0  
      goto error  
      
    delete InboundDocument  
     where InboundDocumentId = @InboundDocumentId  
       and not exists(select top 1 1 from Receipt where Receipt.InboundDocumentId = InboundDocument.InboundDocumentId)  
      
    if @@Error <> 0  
      goto error  
  end  
    
  result:  
      if @Transaction = 1  
        commit transaction  
      return 0  
      
    error:  
      if @Transaction = 1  
      begin  
        RAISERROR (@Errormsg,11,1)  
        rollback transaction  
      end  
      return @Error  
end 
