﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Warehouse_Select
  ///   Filename       : p_Warehouse_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 May 2014 13:32:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Warehouse table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   Warehouse.WarehouseId,
  ///   Warehouse.CompanyId,
  ///   Warehouse.Warehouse,
  ///   Warehouse.WarehouseCode,
  ///   Warehouse.HostId,
  ///   Warehouse.ParentWarehouseId,
  ///   Warehouse.AddressId,
  ///   Warehouse.DesktopMaximum,
  ///   Warehouse.DesktopWarning,
  ///   Warehouse.MobileWarning,
  ///   Warehouse.MobileMaximum,
  ///   Warehouse.UploadToHost,
  ///   Warehouse.PostalAddressId,
  ///   Warehouse.ContactListId,
  ///   Warehouse.PrincipalId,
  ///   Warehouse.AllowInbound,
  ///   Warehouse.AllowOutbound 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Warehouse_Select
(
 @WarehouseId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Warehouse.WarehouseId
        ,Warehouse.CompanyId
        ,Warehouse.Warehouse
        ,Warehouse.WarehouseCode
        ,Warehouse.HostId
        ,Warehouse.ParentWarehouseId
        ,Warehouse.AddressId
        ,Warehouse.DesktopMaximum
        ,Warehouse.DesktopWarning
        ,Warehouse.MobileWarning
        ,Warehouse.MobileMaximum
        ,Warehouse.UploadToHost
        ,Warehouse.PostalAddressId
        ,Warehouse.ContactListId
        ,Warehouse.PrincipalId
        ,Warehouse.AllowInbound
        ,Warehouse.AllowOutbound
    from Warehouse
   where isnull(Warehouse.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Warehouse.WarehouseId,'0'))
  order by Warehouse
  
end
