﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Update_Status
  ///   Filename       : p_Receipt_Update_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_Update_Status
(
 @ReceiptId int,
 @StatusId  int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @OrderNumber        nvarchar(30),
          @OutboundShipmentId int,
          @IssueId            int,
          @Transaction        bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @StatusId is null
    select @StatusId = dbo.ufn_StatusId('R','R')
  
  select @OrderNumber = id.OrderNumber
    from Receipt          r (nolock)
    join InboundDocument id on r.InboundDocumentId = id.InboundDocumentId
   where r.ReceiptId = @ReceiptId
     and r.StatusId = dbo.ufn_StatusId('R','R')
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  if (select count(distinct(StatusId)) from ReceiptLine where ReceiptId = @ReceiptId) = 1
  begin
    exec @Error = p_Receipt_Update
     @ReceiptId = @ReceiptId,
     @StatusId  = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  if @OrderNumber like 'MO-%'
  begin
    select @OrderNumber = replace(@OrderNumber, 'MO-', '')
    select @OrderNumber = substring(@OrderNumber, 1, charindex(':', @OrderNumber) - 1)
    
    select @OutboundShipmentId = osi.OutboundShipmentId,
           @IssueId            = i.IssueId
      from OutboundDocument       od (nolock)
      join Issue                   i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
      join Status                  s (nolock) on i.StatusId = s.StatusId
      left
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
     where od.OrderNumber = @OrderNumber
       and s.StatusCode = 'W'
    
    exec @Error = p_Issue_Update
     @IssueId            = @IssueId,
     @AreaType           = 'Finished'
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Outbound_Auto_Release
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @PlanningComplete   = 0,
     @AutoRelease        = 1
    
    if @Error <> 0
      goto error
  end
  
  if @Transaction = 1
    commit transaction
  
  return 0
  
  error:
  if @Transaction = 1
    begin
      raiserror 900000 'Error executing p_Receipt_Update_Status'
      rollback transaction
    end
    return @Error
end

