﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Print
  ///   Filename       : p_Instruction_Print.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Print
(
 @instructionId int = null,
 @jobId         int = null,
 @operatorId    int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @PalletId            int,
          @StorageUnitBatchId  int,
          @PickLocationId      int,
          @StoreLocationId     int,
          @Quantity            float,
          @Prints              int,
          @StatusId            int,
          @InstructionTypeCode nvarchar(10),
          @StatusCode          nvarchar(10),
          @PriorityId          int,
          @WarehouseId         int,
          @Exception           nvarchar(255),
          @Operator            nvarchar(50),
          @FromWarehouseCode   nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @Operator = Operator
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  begin transaction
  
  select @PalletId            = i.PalletId,
         @StorageUnitBatchId  = i.StorageUnitBatchId,
         @PickLocationId      = i.PickLocationId,
         @StoreLocationId     = i.StoreLocationId,
         @Quantity            = i.Quantity,
         @OperatorId          = isnull(@OperatorId, i.OperatorId),
         @JobId               = i.JobId,
         @InstructionTypeCode = it.InstructionTypeCode,
         @WarehouseId         = i.WarehouseId
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where i.InstructionId = @instructionId
  
  if @@rowcount = 0
  select @PalletId            = -1,--i.PalletId, -- not for job - will scan product code from mixed putaway
         @StorageUnitBatchId  = i.StorageUnitBatchId,
         @StoreLocationId     = i.StoreLocationId,
         @Quantity            = i.Quantity,
         @OperatorId          = isnull(@OperatorId, i.OperatorId),
         @JobId               = i.JobId,
         @InstructionTypeCode = it.InstructionTypeCode,
         @WarehouseId         = i.WarehouseId
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where i.JobId = @jobId
  
  select @StatusCode = s.StatusCode
    from Job    j (nolock)
    join Status s (nolock) on j.StatusId = s.StatusId
   where j.JobId = @JobId
  
  if @StatusCode = 'PR'
  and (dbo.ufn_Configuration(340, @WarehouseId)) = 0 and @PalletId is not null -- Pallet Id re-print
  begin
    select @Exception = 'Operator ' + @Operator + ' reprinted pallet id ' + convert(varchar(10), @PalletId)
    
    exec @Error = p_Exception_Insert
     @ExceptionId   = null,
     @ExceptionCode = 'PALREPRINT',
     @Exception     = @Exception,
     @OperatorId    = @OperatorId,
     @CreateDate    = @GetDate,
     @ExceptionDate = @Getdate,
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
    
    if (dbo.ufn_Configuration(395, @WarehouseId)) = 0
      set @PalletId = null
  end
  
  if @PalletId is null
  begin
    exec @Error = p_Pallet_Insert
     @PalletId           = @PalletId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @LocationId         = @StoreLocationId,
     @Weight             = null,
     @Tare               = null,
     @Quantity           = @Quantity,
     @Prints             = 1
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Update
     @InstructionId       = @instructionId,
     @PalletId            = @PalletId
    
    if @Error <> 0
      goto error
  end
  else if @PalletId != -1
  begin
    select @Prints = isnull(Prints,0) + 1
      from Pallet
     where PalletId = @PalletId
    
    if @Prints is null
      set @Prints = 1
    
    exec @Error = p_Pallet_Update
     @PalletId           = @PalletId,
     @Prints             = @Prints
    
    if @Error <> 0
      goto error
  end
  
  if @InstructionTypeCode in ('S','SM')
  begin
    select @StatusCode = s.StatusCode
      from Job    j (nolock)
      join Status s (nolock) on j.StatusId = s.StatusId
     where j.JobId = @JobId
    
    if @StatusCode != 'A'
    begin
      select @StatusId = dbo.ufn_StatusId('R','PR')
      
      exec @Error = p_Job_Update
       @JobId      = @JobId,
       @OperatorId = @operatorId,
       @StatusId   = @StatusId
      
      if @Error <> 0
        goto error
    end
  end
  
  if @InstructionTypeCode in ('PR')
  begin
    select @StatusCode = s.StatusCode
      from Job    j (nolock)
      join Status s (nolock) on j.StatusId = s.StatusId
     where j.JobId = @JobId
    
    if @StatusCode != 'A'
    begin
      select @StatusId = dbo.ufn_StatusId('PR','PR')
      
      exec @Error = p_Job_Update
       @JobId      = @JobId,
       @OperatorId = @operatorId,
       @StatusId   = @StatusId
      
      if @Error <> 0
        goto error
      
      if (select dbo.ufn_Configuration(319, @WarehouseId)) = 1 and @StatusCode != 'PR'
      begin
        exec @Error = p_Production_Adjustment
         @InstructionId = @InstructionId,
         @Sign          = '+'
        
        if @Error <> 0
          goto error
      end
      
      if (select dbo.ufn_Configuration(210, @WarehouseId)) = 1 and @StatusCode != 'PR'
      begin
        exec @Error = p_Production_Update_Accepted_Quantity
         @InstructionId = @InstructionId,
         @Sign          = '+'
        
        if @Error <> 0
          goto error
      end
      
      if (select dbo.ufn_Configuration(471, @WarehouseId)) = 1 and @StatusCode != 'PR'
      begin
        select @FromWarehouseCode = a.WarehouseCode
          from AreaLocation al (nolock)
          join Area a (nolock) on al.AreaId = a.AreaId
         where al.LocationId = @PickLocationId
        
        exec p_StockAdjustment_Insert
         @operatorId         = @operatorId
        ,@recordType         = 'PRD'
        ,@storageUnitBatchId = @StorageUnitBatchId
        ,@quantity           = @Quantity
        ,@fromWarehouseCode  = @FromWarehouseCode
        ,@Msg                = 'Production Adjustment'
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
      end
    end
    
    declare @OutboundShipmentId int,
            @IssueId            int,
            @OrderNumber        nvarchar(30)
    
    select @OrderNumber = id.OrderNumber
      from Instruction  i (nolock)
      join ReceiptLine rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
      join InboundLine il (nolock) on rl.InboundLineId = il.InboundLineId
      join InboundDocument id (nolock) on il.InboundDocumentId = id.InboundDocumentId
     where i.InstructionId = @InstructionId
    
    if @OrderNumber is not null
    begin
      select @OutboundShipmentId = osi.OutboundShipmentId,
             @IssueId            = i.IssueId,
             @WarehouseId        = i.WarehouseId
        from Issue  i (nolock)
        join Status s (nolock) on i.StatusId = s.StatusId
        left outer
        join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
       where i.Remarks    = @OrderNumber
         and s.StatusCode in('W','P','PS','PC','RL')
      
      if @OutboundShipmentId is not null
        set @IssueId = null
      
      if @OutboundShipmentId is not null or @IssueId is not null
      begin
        exec @Error = p_Receiving_Accept
         @JobId      = @JobId,
         @OperatorId = @OperatorId
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Receiving_Accept'
          goto error
        end
        
        exec @Error = p_Instruction_Started
         @InstructionId = @InstructionId,
         @OperatorId    = @operatorId
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Instruction_Started'
          goto error
        end
        
        waitfor delay '00:00:01'
        
        exec @Error = p_Instruction_Finished
         @InstructionId = @InstructionId,
         @OperatorId    = @operatorId
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Instruction_Finished'
          goto error
        end
        
        exec @Error = p_Outbound_Palletise
         @WarehouseId        = @WarehouseId,
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId,
         @OperatorId         = null,
         @Weight             = null,
         @Volume             = null
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Outbound_Palletise'
          goto error
        end
        
        exec @Error = p_Despatch_Manual_Palletisation_Auto_Locations
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Outbound_Palletise'
          goto error
        end
        
        exec @Error = p_Planning_Complete
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId
              
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Outbound_Release'
          goto error
        end
        
        exec @Error = p_Outbound_Release
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Outbound_Release'
          goto error
        end
        
        select @PriorityId = PriorityId
          from Priority
         where OrderBy = 2
        
        if @PriorityId is not null
        begin
		        exec @Error = p_WIP_Order_Update
		         @outboundShipmentId = @OutboundShipmentId,
		         @issueId            = @IssueId,
		         @priorityId         = @PriorityId
        		
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_Outbound_Release'
            goto error
          end
		      end
      end
    end
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Instruction_Print'
    rollback transaction
    return @Error
end
