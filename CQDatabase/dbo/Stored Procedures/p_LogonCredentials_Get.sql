﻿--drop procedure p_LogonCredentials_Get

/*
  /// <summary>
  ///   Procedure Name : p_LogonCredentials_Get
  ///   Filename       : p_LogonCredentials_Get.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_LogonCredentials_Get]
(
 @userName          nvarchar(50),
 @WarehouseId       int          output,
 @OperatorId        int          output,
 @OperatorGroupId   int          output,
 @CultureName       nvarchar(50) output,
 @Printer           nvarchar(50) output,
 @Port              nvarchar(50) output,
 @IPAddress         nvarchar(50) output,
 @OperatorGroupCode nvarchar(10) = '' output,
 @MenuId            int = null   output,
 @IsActive          bit = null   output,
 @IsDeleted         bit = 0      output
)

as
begin
	 set nocount on;
	 
	 select @OperatorId = OperatorId 
    from Operator
   where Operator = @userName
	 
  select @WarehouseId       = o.WarehouseId,
         @OperatorId        = o.OperatorId,
         @OperatorGroupId   = o.OperatorGroupId,
         @CultureName       = c.CultureName,
         @Printer           = o.Printer,
         @Port              = o.Port,
         @IPAddress         = o.IPAddress,
         @OperatorGroupCode = og.OperatorGroupCode,
         @MenuId            = isnull(o.MenuId,-1),
         @IsActive			= o.ActiveIndicator,
         @IsDeleted			= isnull(o.Deleted,0)
    from Operator       o (nolock)
    join OperatorGroup og (nolock) on o.OperatorGroupId = og.OperatorGroupId
    left outer
    join Culture        c (nolock) on o.CultureId       = c.CultureId
   where o.OperatorId = @OperatorId
end
 
 

