﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Linked_Instructions
  ///   Filename       : p_Pallet_Rebuild_Linked_Instructions.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Linked_Instructions
(
 @JobId int
)
 
as
begin
	 set nocount on;
	 
  select Distinct i.InstructionId,
         ili.IssueLineId,
         null as 'OrderNumber',
         null as 'ExternalCompany',
         s.Status,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         i.ConfirmedQuantity
    from IssueLineInstruction ili (nolock)
    join Instruction            i (nolock) on ili.InstructionId    = i.InstructionId
    join Status                 s (nolock) on i.StatusId           = s.StatusId
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch                  b (nolock) on sub.BatchId          = b.BatchId
    join StorageUnit           su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product                p (nolock) on su.ProductId         = p.ProductId
    join SKU                  sku (nolock) on su.SKUId             = sku.SKUId
   where i.JobId = @JobId
     and i.ConfirmedQuantity > 0
end
