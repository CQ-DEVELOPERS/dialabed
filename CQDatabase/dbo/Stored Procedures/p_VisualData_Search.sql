﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualData_Search
  ///   Filename       : p_VisualData_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:36:07
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the VisualData table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   VisualData.Key1,
  ///   VisualData.Sort1,
  ///   VisualData.Sort2,
  ///   VisualData.Sort3,
  ///   VisualData.Colour,
  ///   VisualData.Latitude,
  ///   VisualData.Longitude,
  ///   VisualData.Info 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualData_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         VisualData.Key1
        ,VisualData.Sort1
        ,VisualData.Sort2
        ,VisualData.Sort3
        ,VisualData.Colour
        ,VisualData.Latitude
        ,VisualData.Longitude
        ,VisualData.Info
    from VisualData
  
end
