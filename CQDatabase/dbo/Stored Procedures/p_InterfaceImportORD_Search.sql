﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportORD_Search
  ///   Filename       : p_InterfaceImportORD_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:49
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportORD table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportORD.RecordStatus,
  ///   InterfaceImportORD.InsertDate,
  ///   InterfaceImportORD.RecordType,
  ///   InterfaceImportORD.ID,
  ///   InterfaceImportORD.Ordernumber,
  ///   InterfaceImportORD.StatusId,
  ///   InterfaceImportORD.OrderDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportORD_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportORD.RecordStatus
        ,InterfaceImportORD.InsertDate
        ,InterfaceImportORD.RecordType
        ,InterfaceImportORD.ID
        ,InterfaceImportORD.Ordernumber
        ,InterfaceImportORD.StatusId
        ,InterfaceImportORD.OrderDate
    from InterfaceImportORD
  
end
