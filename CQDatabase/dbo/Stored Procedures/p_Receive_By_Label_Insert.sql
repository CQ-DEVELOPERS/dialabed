﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receive_By_Label_Insert
  ///   Filename       : p_Receive_By_Label_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Sep 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receive_By_Label_Insert
(
 @OutboundShipmentId int = null,
 @IssueId            int = null
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   OutboundShipmentId int,
   IssueId            int,
   ReceiptId          int,
   Delivery           int,
   WarehouseId        int,
   OrderNumber        nvarchar(30)
  )
  
  declare @TableSum as table
  (
   IssueId            int,
   StorageUnitId      int,
   ConfirmedQuantity  float
  )
  
  declare @TableDetail as table
  (
   InboundShipmentId  int,
   ReceiptId          int,
   Delivery           int,
   OrderNumber        nvarchar(30)
  )
  
  declare @TableJobs as table
  (
   InstructionId      int,
   PalletId           int,
   WarehouseId        int,
   ReceiptId          int,
   ReceiptLineId      int,
   OldJobId           int,
   JobId              int,
   ReferenceNumber    nvarchar(30),
   StorageUnitBatchId int,
   StorageUnitId      int,
   ConfirmedQuantity  float
  )
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @OrderNumer         nvarchar(30),
          @ReceiptId          int,
          @Delivery           int,
          @Count              int,
          @JobId              int,
          @OldJobId           int,
          @WarehouseId        int,
          @PickLocationId     int,
          @ConfirmedQuantity  float,
          @StorageUnitBatchId int,
          @ReceiptLineId      int,
          @PriorityId         int,
          @OperatorId         int,
          @StatusId           int,
          @ReferenceNumber    nvarchar(30),
          @InstructionId      int,
          @PalletId           int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Receive_By_Label_Insert'
  set @Error = 0
  
  if @OutboundShipmentId is null and @IssueId is null
  begin
--    begin transaction
--    set @Errormsg = @Errormsg + ' - Parameters null'
--    goto error
    select top 1
           @OutboundShipmentId = osi.OutboundShipmentId,
           @IssueId            = i.IssueId,
           @ReceiptId          = r.ReceiptId
      from Issue             i (nolock)
      left
      join OutboundShipmentIssue osi (nolock) on i.IssueId      = osi.IssueId
      join Status           si (nolock) on i.StatusId           = si.StatusId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join InboundDocument  id (nolock) on od.OrderNumber       = id.OrderNumber
      join Receipt           r (nolock) on id.InboundDocumentId = r.InboundDocumentId
                                       and r.Delivery           = isnull(i.Delivery,1)
      join Status           sr (nolock) on r.StatusId           = sr.StatusId
     where si.StatusCode in ('CD','D','DC','C')
       and sr.StatusCode = 'W'
       and od.CreateDate >= '2009-10-12'
       and r.Interfaced is null
  end
  
  update Receipt
     set Interfaced = 0
   where ReceiptId = @ReceiptId
     and Interfaced is null
  
  if @OutboundShipmentId is null and @IssueId is null
  begin
    return
  end
  
  if @IssueId is not null
  begin
    insert @TableHeader
          (IssueId,
           Delivery,
           OrderNumber)
    select i.IssueId,
           isnull(i.Delivery,1),
           od.OrderNumber
      from Issue                   i (nolock)
      join Status                  s (nolocK) on i.StatusId           = s.StatusId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where i.IssueId = @IssueId
       and s.StatusCode in ('CD','D','DC','C')
  end
  else if @OutboundShipmentId is not null
  begin
    insert @TableHeader
          (OutboundShipmentId,
           IssueId,
           Delivery,
           OrderNumber)
    select osi.OutboundShipmentId,
           osi.IssueId,
           isnull(i.Delivery,1),
           od.OrderNumber
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and not exists(select 1 from Status s (nolock) where i.StatusId = s.StatusId and s.StatusCode not in ('CD','D','DC','C'))
  end
  
  begin transaction
  
  update th
     set ReceiptId   = r.ReceiptId,
         WarehouseId = r.WarehouseId
    from @TableHeader     th
    join InboundDocument id (nolock) on th.OrderNumber       = id.OrderNumber
    join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
                                    and r.Delivery           = th.Delivery
  
  if @Error <> 0
    goto error
  
  insert @TableSum
        (IssueId,
         StorageUnitId,
         ConfirmedQuantity)
  select th.IssueId,
         sub.StorageUnitId,
         sum(il.ConfirmedQuatity)
    from @TableHeader      th
    join IssueLine         il (nolock) on th.IssueId = il.IssueId
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
  group by th.IssueId,
           sub.StorageUnitId
  
  update rl
     set StatusId = dbo.ufn_StatusId('R','R'),
         ReceivedQuantity     = ConfirmedQuantity,
         AcceptedQuantity     = ConfirmedQuantity,
         DeliveryNoteQuantity = ConfirmedQuantity
    from @TableHeader th
    join @TableSum         ts on th.IssueId = ts.IssueId
    join ReceiptLine       rl (nolock) on th.ReceiptId          = rl.ReceiptId
    join Status             s (nolock) on rl.StatusId           = s.StatusId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
                             and ts.StorageUnitId               = sub.StorageUnitId
   where s.StatusCode = 'W'
  
  update r
     set StatusId = dbo.ufn_StatusId('R','R')
    from @TableHeader th
    join Receipt       r (nolock) on th.ReceiptId = r.ReceiptId
    join Status        s (nolock) on r.StatusId   = s.StatusId
   where s.StatusCode = 'W'
  
  insert @TableJobs
        (InstructionId,
         PalletId,
         WarehouseId,
         ReceiptId,
         OldJobId,
         ReferenceNumber,
         StorageUnitBatchId,
         StorageUnitId,
         ConfirmedQuantity)
  select i.InstructionId,
         i.PalletId,
         th.WarehouseId,
         th.ReceiptId,
         i.JobId,
         j.ReferenceNumber,
         i.StorageUnitBatchId,
         sub.StorageUnitId,
         sum(ili.ConfirmedQuantity)
    from @TableHeader          th
    join IssueLineInstruction ili (nolock) on th.IssueId           = ili.IssueId
    join Instruction            i (nolock) on ili.InstructionId    = i.InstructionId
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Job                    j (nolock) on i.JobId              = j.JobId
   where isnull(ili.ConfirmedQuantity,0) > 0
  group by i.InstructionId,
           i.PalletId,
           th.WarehouseId,
           th.ReceiptId,
           i.JobId,
           j.ReferenceNumber,
           i.StorageUnitBatchId,
           sub.StorageUnitId
  
  update tj
     set ReceiptLineId = rl.ReceiptLineId
    from @TableJobs        tj
    join ReceiptLine       rl (nolock) on tj.ReceiptId = rl.ReceiptId
    join Status             s (nolock) on rl.StatusId  = s.StatusId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
                                      and tj.StorageUnitId      = sub.StorageUnitId
   where s.StatusCode = 'R'
  
  delete @TableJobs where ReceiptLineId is null
  
  select @count = count(1)
    from @TableJobs
  
  select @PriorityId  = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'SM'
  
  select @StatusId = StatusId
    from Status (nolock)
   where StatusCode = 'PR'
     and Type       = 'PR'
  
  while @count > 0
  begin
    select @count = @count -1
    
    select @InstructionId      = InstructionId,
           @WarehouseId        = WarehouseId,
           @ReceiptLineId      = ReceiptLineId,
           @OldJobId           = OldJobId,
           @JobId              = JobId,
           @ReferenceNumber    = 'J:' + convert(nvarchar(10), OldJobId),
           @StorageUnitBatchId = StorageUnitBatchId,
           @ConfirmedQuantity  = ConfirmedQuantity,
           @PalletId           = PalletId
      from @TableJobs
    
    delete @TableJobs
     where InstructionId = @InstructionId
    
    if @JobId is null
    begin
      exec @Error = p_Job_Insert
       @JobId           = @JobId output,
       @PriorityId      = @PriorityId,
       @OperatorId      = @OperatorId,
       @StatusId        = @StatusId,
       @WarehouseId     = @WarehouseId,
       @ReferenceNumber = @ReferenceNumber
      
      update @TableJobs
         set JobId = @JobId
       where OldJobId = @OldJobId
    end
    
    exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @JobId               = @JobId,
         @OperatorId          = null,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @PickLocationId,
         @StoreLocationId     = null,
         @Quantity            = @ConfirmedQuantity,
         @ReceiptLineId       = @ReceiptLineId,
         @PalletId            = @PalletId
    
    if @error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
