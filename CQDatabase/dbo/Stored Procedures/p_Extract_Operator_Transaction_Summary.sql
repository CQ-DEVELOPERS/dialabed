﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Extract_Operator_Transaction_Summary
  ///   Filename       : p_Extract_Operator_Transaction_Summary.sql
  ///   Create By      : Karen
  ///   Date Created   : Jul 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Extract_Operator_Transaction_Summary

 
as
begin
	 set nocount on;
	 
	Declare @TableResult as  Table 
			(OperatorId int NULL,
			InstructionTypeId int NULL,
			EndDate datetime NULL,
			Units int NULL,
			Weight numeric(13,3) NULL,
			Instructions int NULL,
			Jobs int NULL,
			ActiveTime int NULL,
			DwellTime int NULL,
			WarehouseId int NULL,
			JobId int NULL)
			 
	Declare @DwellTime2			varchar(20),	 
			@Error				int,
			@Errormsg			varchar(500),
			@GetDate			datetime,
			@FromDate			datetime,
			@ToDate				datetime,
			@CheckedFrom		date,
			@CheckedTo			date
  
  select @GetDate = dbo.ufn_Getdate()

  set @FromDate		= DATEADD(day,DATEDIFF(day,0,GETDATE())-1,0)
  set @ToDate		= DATEADD(day,DATEDIFF(day,0,GETDATE())+1,0)
  set @CheckedFrom	= GETDATE()
  set @CheckedTo	= dateadd(dd, 1, GETDATE())
  
  -- Make the Date to the hour by excluding minutes and seconds
  select @GetDate = convert(varchar(14), @GetDate, 120) + '00:00'

  begin transaction  
    
          
  insert OperatorTransactionSummary  
        (WarehouseId,  
		 OperatorId,   
         InstructionTypeId,  
         EndDate,  
         Units,  
         Weight,  
         Instructions,  
         Jobs,  
         ActiveTime,  
         DwellTime)  
  select i.WarehouseId,  
		 i.OperatorId,  
         i.InstructionTypeId,  
         convert(varchar(14), i.EndDate, 120) + '00:00',  
         sum(i.ConfirmedQuantity),  
         sum(i.ConfirmedWeight),  
         count(distinct(i.InstructionId)),  
         count(distinct(i.JobId)),  
         sum(datediff(ss, i.StartDate, i.EndDate)),  
         3600 - sum(datediff(ss, i.StartDate, i.EndDate))  
    from Instruction            i (nolock)  
   where convert(varchar(14), i.EndDate, 120) + '00:00' between @FromDate and @ToDate  
   --where  convert(varchar(14), i.EndDate, 120) + '00:00' = @GetDate  
     and not exists(select 1 from OperatorTransactionSummary op where op.EndDate = convert(varchar(14), i.EndDate, 120) + '00:00')  
  group by i.OperatorId, i.InstructionTypeId,i.WarehouseId,  
           convert(varchar(14), i.EndDate, 120) + '00:00'  
    
    insert @TableResult  
        (WarehouseId,  
		 OperatorId,   
         EndDate,  
         Units,  
         Weight,  
         Instructions,  
         Jobs,  
         JobId,  
         DwellTime)  
 select i.WarehouseId,  
		j.checkedby as OperatorId,  
         convert(varchar(14), j.checkeddate, 120) + '00:00' as enddate,  
         sum(i.ConfirmedQuantity),  
         sum(j.NettWeight),  
         count((j.checkedby)),  
         count((j.checkedby)),  
         i.JobId,  
         3600 - sum(datediff(ss, i.StartDate, i.EndDate))       
    from Instruction            i (nolock)  
    join Job j (nolock) on i.JobId = j.JobId  
    join Status s (nolock) on j.StatusId = s.StatusId  
    --where  convert(varchar(14), i.EndDate, 120) + '00:00' = @GetDate  
   --where convert(varchar(14), j.CheckedDate, 110) between @CheckedFrom and @CheckedTo  
   where j.CheckedDate between @CheckedFrom and @CheckedTo  
   and s.status = 'Checked'  
  group by  j.CheckedBy,
			j.CheckedDate, 
			i.WarehouseId, 
			i.JobId,  
           convert(varchar(14), i.EndDate, 120) + '00:00'  
 order by enddate  
 
 select * from @TableResult
    
  insert OperatorTransactionSummary  
        (WarehouseId,  
		 OperatorId,   
         InstructionTypeId,  
         EndDate,  
         Units,  
         Weight,  
         Instructions,  
         Jobs,  
         ActiveTime,  
         DwellTime)  
  select tr.WarehouseId,  
   tr.OperatorId,  
         9999,  
         EndDate,  
         sum(tr.Units),  
         sum(tr.Weight),  
         0,  
         count(distinct(tr.JobId)) as Jobs,  
         0,  
         0  
    from @TableResult tr   
 where OperatorId > 0  
  group by tr.OperatorId, tr.InstructionTypeId,tr.WarehouseId, EndDate  
             
    
  select @Error = @@Error  
    
  if @Error <> 0  
    goto error  
    
  update op  
     set Units = (select sum(ConfirmedQuantity)  
                    from Instruction i (nolock)  
                   where i.InstructionTypeId = op.InstructionTypeId  
                     and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)  
                     and i.OperatorId = op.OperatorId  
      and i.WarehouseId = op.WarehouseId)  
    from OperatorTransactionSummary op  
    where op.InstructionTypeId != 9999  
    
  update op  
     set Weight = (select sum(i.ConfirmedWeight)  
                     from Instruction i (nolock)  
                    where i.InstructionTypeId = op.InstructionTypeId  
                      and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)  
                      and i.OperatorId = op.OperatorId  
       and i.WarehouseId = op.WarehouseId)  
    from OperatorTransactionSummary op  
    where op.InstructionTypeId != 9999  
    
   
  commit transaction  
  select * from OperatorTransactionSummary  
  return  
    
  error:  
    raiserror 900000 'Error executing p_Extract_Operator_Transaction_Summary'  
    rollback transaction  
    return @Error  
  
  
end  
   		
