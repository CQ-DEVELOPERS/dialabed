﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Reset
  ///   Filename       : p_Instruction_Reset.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Aug 2007
  /// </summary>
  /// <remarks>
  ///   Reset an instuction to Waiting (if not Finnished).
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Reset
(
 @instructionId int,
 @statusId      int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @OldStatusId       int,
          @Picked            bit,
          @Stored            bit,
          @Quantity          float,
          @ConfirmedQuantity float,
          @JobId             int,
          @JobStatusCode     nvarchar(10),
          @JobStatusId       int,
          @WarehouseId       int,
          @InstructionTypeCode nvarchar(10),
          @StatusCode          nvarchar(10),
          @InstructionRefId    int
  
  set @ErrorMsg = 'Error executing p_Instruction_Reset';
  
  select @GetDate = dbo.ufn_Getdate();
  
  if @statusId is null
    set @statusId = dbo.ufn_StatusId('I','W');
  
  select @OldStatusId = i.StatusId,
         @Picked      = i.Picked,
         @Stored      = i.Stored,
         @Quantity    = i.Quantity,
         @JobId       = i.JobId,
         @WarehouseId = i.WarehouseId,
         @InstructionTypeCode = it.InstructionTypeCode,
         @StatusCode          = s.StatusCode,
         @InstructionRefId    = isnull(i.InstructionRefId, i.InstructionId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status           s (nolock) on i.StatusId          = s.StatusId
   where i.InstructionId = @instructionId
  
  if (select dbo.ufn_Configuration(31, @WarehouseId)) = 1
    set @ConfirmedQuantity = @Quantity
  else
    set @ConfirmedQuantity = 0
  
  if @InstructionTypeCode in ('M','R')
    update Job
       set OperatorId = null
     where JobId = @JobId
  
  begin transaction
  
  if (select StatusCode
        from Status (nolock)
       where StatusId = @OldStatusId) not in ('S','A','F','NS','Z','E') -- Started, Accepted
  begin
    set @Error = -1;
    set @ErrorMsg = 'Error - Instruction cannot be reset - invalid status';
    goto error;
  end
  
  select @JobStatusCode = s.StatusCode
    from Job    j (nolock)
    join Status s (nolock) on j.StatusId = s.StatusId
   where j.JobId = @JobId
  
  if @InstructionTypeCode in ('P','PM','FM','PS')
  begin
    delete CheckingProduct
     where JobId = @JobId
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
    
    if (select dbo.ufn_Configuration(362, @WarehouseId)) = 1
    begin
      if (select distinct StatusCode
            from IssueLineInstruction ili (nolock)
            join Issue                  i /*lock*/ on ili.IssueId = i.IssueId
            join Status                 s (nolock) on i.StatusId  = s.StatusId
           where ili.InstructionId = @InstructionRefId) not in ('RL','CK','S','PS','QA')
      begin
        set @Error = -1;
        set @ErrorMsg = 'Error - Instruction cannot be reset - invalid issue status';
        goto error;
      end
    end
    
    if @JobStatusCode in ('A','NS','CK','CD','QA','S')
    begin
      select @JobStatusId = dbo.ufn_StatusId('IS','RL')
      
      exec p_Job_Update
       @jobId    = @JobId,
       @statusId = @JobStatusId
    end
  end
  
  if @InstructionTypeCode in ('PR','S','SM')
  begin
    if @JobStatusCode in ('Z','A','S')
    begin
      select @JobStatusId = dbo.ufn_StatusId(@InstructionTypeCode,'PR')
      
      exec p_Job_Update
       @jobId    = @JobId,
       @statusId = @JobStatusId
    end
  end
  
  if @StatusCode = 'F'
  begin
    exec @Error = p_StorageUnitBatchLocation_Reverse
     @InstructionId = @instructionId,
     @Pick          = @Picked,
     @Store         = @Stored
    
    if @Error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @instructionId,
     @Pick          = @Picked,
     @Store         = @Stored
    
    if @Error <> 0
      goto error
  end
  
  update Instruction
     set StatusId          = @statusId,
         OperatorId        = null,
         StartDate         = null,
         EndDate           = null,
         ConfirmedQuantity = @ConfirmedQuantity,
         ConfirmedWeight   = null
   where InstructionId = @instructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  exec @Error = p_InstructionHistory_Insert
   @InstructionId     = @instructionId,
   @StatusId          = @statusId,
   @OperatorId        = null,
   @StartDate         = null,
   @EndDate           = null,
   @ConfirmedQuantity = null,
   @ConfirmedWeight   = null,
   @CommandType       = 'Update',
   @InsertDate        = @GetDate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @ErrorMsg
    rollback transaction
    return @Error
end
