﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLine_Update
  ///   Filename       : p_IssueLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2013 08:43:16
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the IssueLine table.
  /// </remarks>
  /// <param>
  ///   @IssueLineId int = null,
  ///   @IssueId int = null,
  ///   @OutboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuatity float = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLine_Update
(
 @IssueLineId int = null,
 @IssueId int = null,
 @OutboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @Quantity float = null,
 @ConfirmedQuatity float = null,
 @Weight float = null,
 @ConfirmedWeight float = null 
)
 
as
begin
	 set nocount on;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @OutboundLineId = '-1'
    set @OutboundLineId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
	 declare @Error int
 
  update IssueLine
     set IssueId = isnull(@IssueId, IssueId),
         OutboundLineId = isnull(@OutboundLineId, OutboundLineId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         StatusId = isnull(@StatusId, StatusId),
         OperatorId = isnull(@OperatorId, OperatorId),
         Quantity = isnull(@Quantity, Quantity),
         ConfirmedQuatity = isnull(@ConfirmedQuatity, ConfirmedQuatity),
         Weight = isnull(@Weight, Weight),
         ConfirmedWeight = isnull(@ConfirmedWeight, ConfirmedWeight) 
   where IssueLineId = @IssueLineId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_IssueLineHistory_Insert
         @IssueLineId = @IssueLineId,
         @IssueId = @IssueId,
         @OutboundLineId = @OutboundLineId,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @StatusId = @StatusId,
         @OperatorId = @OperatorId,
         @Quantity = @Quantity,
         @ConfirmedQuatity = @ConfirmedQuatity,
         @Weight = @Weight,
         @ConfirmedWeight = @ConfirmedWeight,
         @CommandType = 'Update'
  
  return @Error
  
end
