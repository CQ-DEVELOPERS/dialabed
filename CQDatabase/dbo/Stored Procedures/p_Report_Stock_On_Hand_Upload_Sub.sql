﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_On_Hand_Upload_Sub
  ///   Filename       : p_Report_Stock_On_Hand_Upload_Sub.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_On_Hand_Upload_Sub
(
 @WarehouseId	int,
 @PrincipalId	int
)
 
as
begin
	 set nocount on;
	 
	 
	 exec dbo.p_Housekeeping_Stock_Take_Reload @WarehouseId
	 
	if @PrincipalId = -1
		set @PrincipalId = null
		
			 
  declare	@ComparisonDate datetime,
	        @Variance       int,
			@MaxDate		datetime,
	        @MinDate		datetime
	        
	 select @MaxDate = max(ComparisonDate)
	   from StockOnHandCompare
	 
	 select @MinDate = max(ComparisonDate)
	   from StockOnHandCompare
	  where ComparisonDate < @MaxDate
	    and ComparisonDate > dateadd(dd, -7, getdate())
	             
     select @ComparisonDate = max(ComparisonDate) from HousekeepingCompare (nolock)
  
   select sohmax.ComparisonDate,
	        sohmax.StorageUnitId,
	        sohmax.BatchId,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vs.Batch,
         sohmax.Quantity as 'CurrentQuantity',
         sohmin.Quantity as 'PreviousQuantity',
         sohmax.Quantity - sohmin.Quantity as 'Variance',
         sohmax.Sent,
         sohmax.UnitPrice * sohmax.Quantity as 'CurrentValue',
         sohmax.UnitPrice * sohmin.Quantity as 'PreviousValue',
         (sohmax.UnitPrice * sohmax.Quantity) - (sohmin.UnitPrice * sohmin.Quantity) as 'PriceVariance',
         sohmax.UnitPrice
    from StockOnHandCompare sohmax (nolock)
    left
    join StockOnHandCompare sohmin (nolock) on sohmax.StorageUnitId = sohmin.StorageUnitId
                                  and sohmax.BatchId       = sohmin.BatchId
                                  and sohmax.ComparisonDate = @MaxDate
                                  and sohmin.ComparisonDate = @MinDate
    join viewStock           vs on sohmax.StorageUnitId    = vs.StorageUnitId
                               and sohmax.BatchId          = vs.BatchId
    join Product              p (nolock) on vs.ProductId   = p.ProductId
   where sohmax.WarehouseId    = @WarehouseId
     --and vs.ProductCode     like @ProductCode + '%'
     --and vs.Product         like @Product + '%'
     --and vs.Batch           like @Batch + '%'
     --and sohmax.Sent           = 0
     --and vs.SkuCode	    like @SkuCode + '%'
     --and (isnull(sohmax.WarehouseCode,'') like @WarehouseCode or isnull(sohmin.WarehouseCode,'') like @WarehouseCode)
     --and sohmax.Quantity - sohmin.Quantity != isnull(@Variance,-9999999)
     --and (isnull(sohmax.Sent,0)    <= @showSent or isnull(sohmin.Sent,0)    <= @showSent)
     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
  	         
  --select hc.WarehouseCode,
  --       hc.ComparisonDate,
  --       p.ProductCode,
  --       p.Product,
  --       sku.SKUCode,
  --       b.Batch,
  --       hc.WMSQuantity,
  --       hc.HostQuantity,
  --       hc.WMSQuantity - hc.HostQuantity as 'Variance',
  --       hc.Sent,
  --       hc.UnitPrice * hc.WMSQuantity as 'WMSPrice',
  --       hc.UnitPrice * hc.HostQuantity as 'HostPrice',
  --       (hc.UnitPrice * hc.WMSQuantity) - (hc.UnitPrice * hc.HostQuantity) as 'PriceVariance',
  --       hc.UnitPrice,
  --       Case when Isnull(h.RecordStatus,sa.RecordStatus) in ('I','Y') Then 'Successful'
  --            when Isnull(h.RecordStatus,sa.RecordStatus) = 'E' Then 'Error'
  --            when Isnull(h.RecordStatus,sa.RecordStatus) = 'N' Then 'Waiting'
  --            Else '' end as UploadStatus,
  --       CustomField1     	  
  --  from HousekeepingCompare  hc (nolock) -- HousekeepingCompare2
  --		left Join InterfaceExportHeader h (nolock) on h.InterfaceExportHeaderId = hc.InterfaceId
  --		Left join InterfaceExportStockAdjustment sa (nolock) on hc.InterfaceId = sa.InterfaceExportStockAdjustmentId
  --  join StorageUnit          su (nolock) on hc.StorageUnitId          = su.StorageUnitId
  --  join Product               p (nolock) on su.ProductId              = p.ProductId
  --  join SKU                 sku (nolock) on su.SKUId                  = sku.SKUId
  --  join Batch                 b (nolock) on hc.BatchId                = b.BatchId
		--  join Status                s (nolock) on p.StatusId                = s.StatusId
  -- where isnull(hc.WarehouseId, @WarehouseId) = @WarehouseId
  --   --and p.StatusId     = 65 -- Can't do that it may change
  --   and s.StatusCode   = 'A'
  --   and hc.ComparisonDate = @ComparisonDate
  --   and p.PrincipalId = isnull(@PrincipalId,p.PrincipalId)
end
 
 
