﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLinePackaging_Delete
  ///   Filename       : p_IssueLinePackaging_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:55
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the IssueLinePackaging table.
  /// </remarks>
  /// <param>
  ///   @PackageLineId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLinePackaging_Delete
(
 @PackageLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete IssueLinePackaging
     where PackageLineId = @PackageLineId
  
  select @Error = @@Error
  
  
  return @Error
  
end
