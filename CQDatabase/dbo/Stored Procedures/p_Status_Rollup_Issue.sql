﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Rollup_Issue
  ///   Filename       : p_Status_Rollup_Issue.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Mar 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Rollup_Issue
(
 @JobId int
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @OutboundShipmentId int,
          @IssueId            int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  create table #Status
  (
   OutboundShipmentId              int              null,
   IssueId                         int              null,
   IssueCode                       nvarchar(20)     null,
   IssueOrderBy                    int              null,
   JobCode                         nvarchar(20)     null,
   JobOrderBy                      int              null,
   JobStatusId                     int              null
  )
  
  select @OutboundShipmentId = ili.OutboundShipmentId,
         @IssueId            = ili.IssueId
    from IssueLineInstruction ili (nolock)
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
   where i.JobId = @JobId
  
  if @@rowcount = 0 -- Find using InstructionRefId
    select @OutboundShipmentId = ili.OutboundShipmentId,
           @IssueId            = ili.IssueId
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId = i.InstructionRefId
     where i.JobId = @JobId
  
  if @OutboundShipmentId is not null
  begin
    insert #Status
          (OutboundShipmentId, IssueId, IssueCode, IssueOrderBy, JobOrderBy)
    select ili.OutboundShipmentId,
           i.IssueId,
           iss.StatusCode,
           iss.OrderBy,
           min(js.OrderBy)
      from IssueLineInstruction ili (nolock)
      join Instruction          ins (nolock) on isnull(ins.InstructionRefId, ins.InstructionId) = ili.InstructionId --ili.InstructionId = ins.InstructionId or ili.InstructionId = ins.InstructionRefId
      join Issue                  i (nolock) on ili.IssueId       = i.IssueId
      join Status               iss (nolock) on i.StatusId        = iss.StatusId
      join Job                    j (nolock) on ins.JobId         = j.JobId
      join Status                js (nolock) on j.StatusId        = js.StatusId
     where ili.OutboundShipmentId = @OutboundShipmentId
    group by ili.OutboundShipmentId, i.IssueId, iss.StatusCode, iss.OrderBy
    order by iss.OrderBy
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
  end
  else if @IssueId is not null
  begin
    insert #Status
          (IssueId, IssueCode, IssueOrderBy, JobOrderBy)
    select i.IssueId,
           iss.StatusCode,
           iss.OrderBy,
           min(js.OrderBy)
      from IssueLineInstruction ili (nolock)
      join Instruction          ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId) --ili.InstructionId = ins.InstructionId or ili.InstructionId = ins.InstructionRefId
      join Issue                  i (nolock) on ili.IssueId       = i.IssueId
      join Status               iss (nolock) on i.StatusId        = iss.StatusId
      join Job                    j (nolock) on ins.JobId         = j.JobId
      join Status                js (nolock) on j.StatusId        = js.StatusId
     where i.IssueId = @IssueId
    group by ili.OutboundShipmentId, i.IssueId, iss.StatusCode, iss.OrderBy
    order by iss.OrderBy
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
  end
  
  update t
     set JobCode = s.StatusCode,
         JobStatusId = s.StatusId
    from #Status t
    join Status  s on t.JobOrderBy = s.OrderBy
                  and s.Type = 'IS'
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update t
     set JobCode = s.StatusCode,
         JobStatusId = s.StatusId
    from #Status t
    join Status  s on t.JobOrderBy = s.OrderBy
   where t.JobStatusId is null
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update i
     set StatusId = JobStatusId
    from #Status s
    join Issue   i on s.IssueId = i.IssueId
   where JobOrderBy > IssueOrderBy
     --and JobOrderBy >= 9
     --and IssueOrderBy <= 8
  
  --select @@rowcount, * from #Status
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update il
     set StatusId = i.StatusId
    from #Status s
    join Issue   i on s.IssueId = i.IssueId
    join IssueLine il on i.IssueId = il.IssueId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Status_Rollup_Issue'
    rollback transaction
    return @Error
end
