﻿/*  
  /// <summary>  
  ///   Procedure Name : p_Interface_Export_SOSend  
  ///   Filename       : p_Interface_Export_SOSend.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 14 Oct 2013  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
create procedure p_Interface_Export_SOSend  
--with encryption  
as  
begin  
  set nocount on;  
    
  declare @Error             int,  
          @Errormsg          varchar(500) = 'Error executing p_Interface_Export_SOSend',  
          @GetDate           datetime,  
          @Transaction       bit = 1  
    
  select @GetDate = dbo.ufn_Getdate()  
    
  if @@trancount = 0  
    begin  
      begin transaction  
      set @Transaction = 1  
    end  
  
 Update ies  
 Set ProcessedDate = @GetDate  
 From InterfaceExportShipment ies   
 inner join InterfaceExportShipmentDelivery iesd on ies.InterfaceExportShipmentId = iesd.InterfaceExportShipmentId   
 Where iesd.RecordStatus = 'Y'
 And ((ies.RecordStatus = 'N' And ies.ProcessedDate Is Null)
   OR (ies.RecordStatus = 'E' and DATEDIFF(DD,ies.InsertDate,getdate()) = 0))

 Declare @InterfaceTable as Table  
  (ShipmentId Int  
  ,DeliveryCount Int  
  ,SuccessCount Int)  
  
 Insert Into @InterfaceTable  
  (ShipmentId, DeliveryCount)  
 Select ies.InterfaceExportShipmentId, Count(*) As DeliveryCount  
 From InterfaceExportShipment ies   
 inner join InterfaceExportShipmentDelivery iesd on ies.InterfaceExportShipmentId = iesd.InterfaceExportShipmentId  
 where ies.ProcessedDate = @GetDate  
 and exists (select 1 from InterfaceExportShipmentDetail dl 
             where dl.InterfaceExportShipmentDeliveryId = iesd.InterfaceExportShipmentDeliveryId)
 Group By ies.InterfaceExportShipmentId  
  
 Update IT  
 Set SuccessCount = T1.SuccessCount  
 From @InterfaceTable IT  
 Inner Join  
 (  
 select ies.InterfaceExportShipmentId, COUNT(Distinct InterfaceId) As SuccessCount  
 from InterfaceMessage im  
 inner join InterfaceExportShipmentDelivery iesd on im.interfaceId = iesd.InterfaceExportShipmentDeliveryId  
 inner join InterfaceExportShipment ies on ies.InterfaceExportShipmentId = iesd.InterfaceExportShipmentId  
 inner join @InterfaceTable it on ies.InterfaceExportShipmentId = it.ShipmentId  
 where InterfaceTable = 'InterfaceExportShipmentDelivery'  
 and left(interfaceMessage, 7) = 'Success'  
 Group By ies.InterfaceExportShipmentId) As T1 ON IT.ShipmentId = T1.InterfaceExportShipmentId  
  
 Insert Into InterfaceExportHeader  
  (PrimaryKey  
  ,OrderNumber  
  ,RecordType  
  ,RecordStatus  
  ,InsertDate  
  ,PrincipalCode)  
 Select Distinct  
   IssueId As PrimaryKey  
  ,WaybillNo As OrderNumber  
  ,'FinalDespatch' As RecordType  
  ,'N' As RecordStatus  
  ,@GetDate As InsertDate  
  ,PrincipalCode  
 From InterfaceExportShipmentDelivery iesd  
 Inner Join @InterfaceTable it on iesd.InterfaceExportShipmentId = ShipmentId  
 Where DeliveryCount = SuccessCount  
  
 Update InterfaceExportShipment  
 Set RecordStatus = 'Y'
 Where ProcessedDate = @GetDate
 and InterfaceExportShipmentId in (Select ShipmentId From @InterfaceTable Where DeliveryCount = SuccessCount)
  
 Update InterfaceExportShipment  
 Set RecordStatus = 'E'
 Where ProcessedDate = @GetDate
 and InterfaceExportShipmentId in (Select ShipmentId From @InterfaceTable Where DeliveryCount <> SuccessCount)  
 
  if @Error <> 0  
    goto error  
    
  result:  
      if @Transaction = 1  
        commit transaction  
      return 0  
      
    error:  
      if @Transaction = 1  
      begin  
        RAISERROR (@Errormsg,11,1)  
        rollback transaction  
      end  
      return @Error  
end  