﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_StockTake_Fix
  ///   Filename       : p_Housekeeping_StockTake_Fix.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_StockTake_Fix
 
as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_Housekeeping_StockTake_Fix;
    
   if not exists(select top 1 1 from sys.tables where name = 'StockTakeLog')
   begin
     -- select * into StockTakeLog_Backup from StockTakeLog
     -- drop table StockTakeLog
     create table StockTakeLog
     (
      StockTakeLogId int identity
     ,ProductCode nvarchar(50)
     ,Product nvarchar(255)
     ,SKUCode nvarchar(50)
     ,Batch nvarchar(50)
     ,Quantity numeric(13,6)
     ,ConfirmedQuantity numeric(13,6)
     ,ActualQuantity numeric(13,6)
     ,AllocatedQuantity numeric(13,6)
     ,ReservedQuantity numeric(13,6)
     ,InsertDate datetime
     )
     
     alter table StockTakeLog add Location nvarchar(15) null;
     alter table StockTakeLog add Area nvarchar(50) null;
     alter table StockTakeLog add InstructionId int null;
     alter table StockTakeLog add StockTakeLogCode nvarchar(50) null;
   end
   
   create table #Instructions
   (
    LocationId int
   ,StorageUnitBatchId int
   ,InstructionId int
   )
   
   insert #Instructions
         (LocationId
         ,StorageUnitBatchId
         ,InstructionId)
   select vi.StoreLocationId
         ,vi.StorageUnitBatchId
         ,max(vi.InstructionId)
     from viewInstruction vi
     join StockTakeAuthorise sta on vi.InstructionId = sta.InstructionId
     join StorageUnitBatchLocation soh on vi.StorageUnitBatchId = soh.StorageUnitBatchId
                     and vi.StoreLocationId = soh.LocationId
    where CreateDate > '2016-06-23'
      and vi.InstructionTypeCode like 'ST%'
      and vi.Stored = 1
      and vi.ConfirmedQuantity > 0
      and soh.ActualQuantity = 0
      and vi.InstructionCode = 'F'
   group by vi.StoreLocationId
           ,vi.StorageUnitBatchId

   insert StockTakeLog
         ([StockTakeLogCode]
         ,[InstructionId]
         ,[Area]
         ,[Location]
         ,[ProductCode]
         ,[Product]
         ,[SKUCode]
         ,[Batch]
         ,[Quantity]
         ,[ConfirmedQuantity]
         ,[ActualQuantity]
         ,[AllocatedQuantity]
         ,[ReservedQuantity]
         ,[InsertDate])
   select 'Auto Run'
         ,vi.InstructionId
         ,vi.StoreArea
         ,vi.StoreLocation
         ,vi.ProductCode
         ,vi.Product
         ,vi.SKUCode
         ,vi.Batch
         ,vi.Quantity
         ,vi.ConfirmedQuantity
         ,soh.ActualQuantity
         ,soh.AllocatedQuantity
         ,soh.ReservedQuantity
         ,GETDATE() as 'InsertDate'
     from #Instructions t
     join viewInstruction vi on t.InstructionId = vi.InstructionId
     join StorageUnitBatchLocation soh on vi.StorageUnitBatchId = soh.StorageUnitBatchId
                                      and vi.StoreLocationId    = soh.LocationId
   
   if @@ROWCOUNT = 0
     goto lbexit
   
   update soh
      set ActualQuantity = vi.ConfirmedQuantity
     from #Instructions t
     join viewInstruction vi on t.InstructionId = vi.InstructionId
     join StorageUnitBatchLocation soh on vi.StorageUnitBatchId = soh.StorageUnitBatchId
                                      and vi.StoreLocationId    = soh.LocationId
	   
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Housekeeping_StockTake_Fix;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
 
 
