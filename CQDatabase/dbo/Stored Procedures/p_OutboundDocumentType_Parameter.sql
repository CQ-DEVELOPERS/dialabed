﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentType_Parameter
  ///   Filename       : p_OutboundDocumentType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jan 2014 09:16:08
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundDocumentType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OutboundDocumentType.OutboundDocumentTypeId,
  ///   OutboundDocumentType.OutboundDocumentType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentType_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as OutboundDocumentTypeId
        ,'{All}' as OutboundDocumentType
  union
  select
         OutboundDocumentType.OutboundDocumentTypeId
        ,OutboundDocumentType.OutboundDocumentType
    from OutboundDocumentType
  order by OutboundDocumentType
  
end
