﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Insert
  ///   Filename       : p_Wave_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013 14:17:29
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Wave table.
  /// </remarks>
  /// <param>
  ///   @WaveId int = null output,
  ///   @Wave nvarchar(100) = null,
  ///   @CreateDate datetime = null,
  ///   @ReleasedDate datetime = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @CompleteDate datetime = null,
  ///   @ReleasedOrders datetime = null,
  ///   @PutawayDate datetime = null 
  /// </param>
  /// <returns>
  ///   Wave.WaveId,
  ///   Wave.Wave,
  ///   Wave.CreateDate,
  ///   Wave.ReleasedDate,
  ///   Wave.StatusId,
  ///   Wave.WarehouseId,
  ///   Wave.CompleteDate,
  ///   Wave.ReleasedOrders,
  ///   Wave.PutawayDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Insert
(
 @WaveId int = null output,
 @Wave nvarchar(100) = null,
 @CreateDate datetime = null,
 @ReleasedDate datetime = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @CompleteDate datetime = null,
 @ReleasedOrders datetime = null,
 @PutawayDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
  if @Wave = '-1'
    set @Wave = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
	 declare @Error int
 
  insert Wave
        (Wave,
         CreateDate,
         ReleasedDate,
         StatusId,
         WarehouseId,
         CompleteDate,
         ReleasedOrders,
         PutawayDate)
  select @Wave,
         @CreateDate,
         @ReleasedDate,
         @StatusId,
         @WarehouseId,
         @CompleteDate,
         @ReleasedOrders,
         @PutawayDate 
  
  select @Error = @@Error, @WaveId = scope_identity()
  
  
  return @Error
  
end
