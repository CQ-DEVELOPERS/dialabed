﻿--IF OBJECT_ID('dbo.p_Generic_Import_Product') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Generic_Import_Product
--    IF OBJECT_ID('dbo.p_Generic_Import_Product') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Generic_Import_Product >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Generic_Import_Product >>>'
--END
--go
/*    
/// <summary>    
///   Procedure Name : p_Generic_Import_Product  
///   Filename       : p_Generic_Import_Product.sql    
///   Create By      : Grant Schultz    
///   Date Created   : 19 Feb 2009    
/// </summary>    
/// <remarks>    
///    
/// </remarks>    
/// <param>    
///    
/// </param>    
/// <returns>    
///    
/// </returns>    
/// <newpara>    
///   Modified by    : Gareth    
///   Modified Date  : 20 June 2019    
///   Details        : prevent adding new SKU   
/// </newpara>    
*/
CREATE PROCEDURE [dbo].[p_Generic_Import_Product]
	--with encryption    
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Error INT
		,@Errormsg NVARCHAR(500)
		,@GetDate DATETIME
		,@ProductId INT
		,@SKUId INT
		,@StorageUnitBatchId INT
		,@BatchId INT
		,@Quantity INT
		,@StatusId INT
		,@InterfaceImportProductId INT
		,@RecordStatus CHAR(1)
		,@ProcessedDate DATETIME
		,@ProductCode NVARCHAR(30)
		,@Product NVARCHAR(255)
		,@Description2 NVARCHAR(255)
		,@SKUCode NVARCHAR(50)
		,@SKU NVARCHAR(50)
		,@Size NVARCHAR(200)
		,@Colour NVARCHAR(200)
		,@Style NVARCHAR(200)
		,@PalletQuantity INT
		,@Barcode NVARCHAR(50)
		,@MinimumQuantity INT
		,@ReorderQuantity INT
		,@MaximumQuantity INT
		,@CuringPeriodDays INT
		,@ShelfLifeDays INT
		,@QualityAssuranceIndicator BIT
		,@ProductType NVARCHAR(20)
		,@ProductCategory NVARCHAR(50)
		,@OverReceipt NUMERIC(13, 3)
		,@RetentionSamples INT
		,@HostId NVARCHAR(30)
		,@HostRecordStatus CHAR(1)
		,@Samples INT
		,@PackCode NVARCHAR(10)
		,@PackDescription NVARCHAR(255)
		,@Length NUMERIC(13, 6)
		,@Width NUMERIC(13, 6)
		,@Height NUMERIC(13, 6)
		,@Volume NUMERIC(13, 6)
		,@NettWeight NUMERIC(13, 6)
		,@GrossWeight NUMERIC(13, 6)
		,@TareWeight NUMERIC(13, 6)
		,@PackingCategory CHAR(1)
		,@PickEmpty BIT
		,@StackingCategory INT
		,@MovementCategory INT
		,@ValueCategory INT
		,@StoringCategory INT
		,@PickPartPallet INT
		,@PackTypeId INT
		,@StorageUnitId INT
		,@PackId INT
		,@WarehouseId INT
		,@PrincipalCode NVARCHAR(30)
		,@PrincipalId INT
		,@UnitPrice NUMERIC(13, 2)
		,@PreviousPrice NUMERIC(13, 2)
		,@TBSPrincipalCode NVARCHAR(30)
		,@InterfaceImportProductIdTBS INT

	SELECT @GetDate = dbo.ufn_Getdate()

	SET @Error = 0

	UPDATE InterfaceImportProduct
	SET ProcessedDate = @Getdate
	WHERE RecordStatus IN (
			'N'
			,'U'
			)
		AND ProcessedDate IS NULL

	DECLARE Product_cursor CURSOR
	FOR
	SELECT InterfaceImportProductId
		,ProductCode
		,Product
		,SKUCode
		,SKU
		,Style
		,Colour
		,Size
		,isnull(PalletQuantity, 99999)
		,Barcode
		,MinimumQuantity
		,ReorderQuantity
		,MaximumQuantity
		,CuringPeriodDays
		,ShelfLifeDays
		,QualityAssuranceIndicator
		,ProductType
		,ProductCategory
		,OverReceipt
		,RetentionSamples
		,HostId
		,HostRecordStatus
		,PrincipalCode
		,convert(NUMERIC(13, 2), isnull(Additional1, 0))
	FROM InterfaceImportProduct
	WHERE RecordStatus = 'N'

	--ProcessedDate = @Getdate    
	OPEN Product_cursor

	FETCH Product_cursor
	INTO @InterfaceImportProductId
		,@ProductCode
		,@Product
		,@SKUCode
		,@SKU
		,@Style
		,@Colour
		,@Size
		,@PalletQuantity
		,@Barcode
		,@MinimumQuantity
		,@ReorderQuantity
		,@MaximumQuantity
		,@CuringPeriodDays
		,@ShelfLifeDays
		,@QualityAssuranceIndicator
		,@ProductType
		,@ProductCategory
		,@OverReceipt
		,@RetentionSamples
		,@HostId
		,@HostRecordStatus
		,@PrincipalCode
		,@UnitPrice

	WHILE (@@fetch_status = 0)
	BEGIN
		BEGIN TRANSACTION
 -- if @PrincipalCode in ('DAB')
 -- begin
  
 --   set @TBSPrincipalCode = 'TBS'
    
 --   if @TBSPrincipalCode is not null
 --   begin
	--INSERT INTO InterfaceImportProduct
 --          (RecordStatus
 --          ,ProcessedDate
 --          ,ProductCode
 --          ,Product
 --          ,SKUCode
 --          ,SKU
 --          ,PalletQuantity
 --          ,Barcode
 --          ,MinimumQuantity
 --          ,ReorderQuantity
 --     ,MaximumQuantity
 --          ,CuringPeriodDays
 --          ,ShelfLifeDays
 --          ,QualityAssuranceIndicator
 --          ,ProductType
 --          ,ProductCategory
 --          ,OverReceipt
 --          ,RetentionSamples
	--	   ,HostId
 --          ,InsertDate
 --          ,Additional1
 --          ,Additional2
 --          ,Additional3
 --          ,Additional4
 --          ,Additional5
 --          ,Additional6
 --          ,Additional7
 --          ,Additional8
 --          ,Additional9
 --          ,Additional10
	--	   ,DangerousGoodsCode
 --          ,ABCStatus
 --          ,Samples
 --          ,AssaySamples
 --          ,PrimaryKey
 --          ,Style
 --          ,Colour
 --          ,Size
 --          ,PrincipalCode
 --          ,HostRecordStatus)
 --    select
 --          'N'
 --          ,null
 --          ,ProductCode
 --          ,Product
 --          ,SKUCode
 --          ,SKU
 --          ,PalletQuantity
 --          ,Barcode
 --          ,MinimumQuantity
 --          ,ReorderQuantity
 --          ,MaximumQuantity
 --          ,CuringPeriodDays
 --          ,ShelfLifeDays
 --          ,QualityAssuranceIndicator
 --          ,ProductType
 --          ,ProductCategory
 --          ,OverReceipt
 --          ,RetentionSamples
 --          ,HostId
 --          ,InsertDate
 --          ,Additional1
 --          ,Additional2
 --          ,Additional3
 --          ,Additional4
 --          ,Additional5
 --          ,Additional6
 --          ,Additional7
 --          ,Additional8
 --          ,Additional9
 --          ,Additional10
 --          ,DangerousGoodsCode
 --          ,ABCStatus
 --          ,Samples
 --          ,AssaySamples
 --          ,PrimaryKey
 --          ,Style
 --          ,Colour
 --          ,Size
 --          ,@TBSPrincipalCode
 --          ,HostRecordStatus
 --   from InterfaceImportProduct
 --   where InterfaceImportProductId = @InterfaceImportProductId
    
 --   set @InterfaceImportProductIdTBS = scope_identity()

 --   end
 -- end
  
		IF @HostRecordStatus = 'D'
			SELECT @StatusId = dbo.ufn_StatusId('P', 'I')
		ELSE
			SELECT @StatusId = dbo.ufn_StatusId('P', 'A')

		IF @QualityAssuranceIndicator IS NULL
			SET @QualityAssuranceIndicator = 0
		SET @PrincipalId = NULL

		SELECT @PrincipalId = PrincipalId
		FROM Principal(NOLOCK)
		WHERE PrincipalCode = @PrincipalCode

		SET @ProductId = NULL

		SELECT @ProductId = ProductId
		FROM Product
		WHERE ProductCode = @ProductCode
			AND PrincipalId = @PrincipalId

		IF @ProductId IS NULL
			SELECT @ProductId = ProductId
			FROM Product
			WHERE ProductCode = @ProductCode
				AND PrincipalId IS NULL

		SET @Description2 = @Product
		SET @Product = left(@Product, 50)

		IF @ProductId IS NULL
		BEGIN
			EXEC @Error = p_Product_Insert @ProductId = @ProductId OUTPUT
				,@StatusId = @StatusId
				,@ProductCode = @ProductCode
				,@Product = @Product
				,@Description2 = @Description2
				,@Barcode = @Barcode
				,@MinimumQuantity = @MinimumQuantity
				,@ReorderQuantity = @ReorderQuantity
				,@MaximumQuantity = @MaximumQuantity
				,@CuringPeriodDays = @CuringPeriodDays
				,@ShelfLifeDays = @ShelfLifeDays
				,@QualityAssuranceIndicator = @QualityAssuranceIndicator
				,@ProductType = @ProductType
				,@OverReceipt = @OverReceipt
				,@HostId = @HostId
				,@RetentionSamples = @RetentionSamples
				,@Samples = @Samples
				,@PrincipalId = @PrincipalId
				,@Category = @ProductCategory

			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_Product_Insert'

				GOTO error
			END
		END
		ELSE
		BEGIN
			EXEC @Error = p_Product_Update @ProductId = @ProductId
				,@StatusId = @StatusId
				,@ProductCode = @ProductCode
				,@Product = @Product
				,@Description2 = @Description2
				,@Barcode = @Barcode
				,@MinimumQuantity = @MinimumQuantity
				,@ReorderQuantity = @ReorderQuantity
				,@MaximumQuantity = @MaximumQuantity
				,@CuringPeriodDays = @CuringPeriodDays
				,@ShelfLifeDays = @ShelfLifeDays
				,@QualityAssuranceIndicator = @QualityAssuranceIndicator
				,@ProductType = @ProductType
				,@OverReceipt = @OverReceipt
				,@HostId = @HostId
				,@RetentionSamples = @RetentionSamples
				,@Samples = @Samples
				,@PrincipalId = @PrincipalId
				,@Category = @ProductCategory

			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_Product_Update'

				GOTO error
			END
		END

		SET @SKUId = NULL

		IF ISNULL(@SKUCode, '') = ''
			SET @SKUCode = 'Each'

		IF ISNULL(@SKU, '') = ''
			SET @SKU = @SKUCode

		SELECT @SKUId = SKUId
			,@Quantity = Quantity
		FROM SKU(NOLOCK)
		WHERE SKUCode = @SKUCode

		IF @Quantity IS NULL
			SET @Quantity = 99999

		IF @SKUId IS NULL
		BEGIN
			EXEC @Error = p_SKU_Insert @SKUId = @SKUId OUTPUT
				,@UOMId = 1
				,@SKU = @SKU
				,@SKUCode = @SKUCode
				,@Quantity = @Quantity
				,@AlternatePallet = NULL
				,@SubstitutePallet = NULL
				,@Litres = NULL

			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_SKU_Insert'

				GOTO error
			END
		END
		ELSE
		BEGIN
			EXEC @Error = p_SKU_Update @SKUId = @SKUId
				,@UOMId = 1
				,@SKU = @SKU
				,@SKUCode = @SKUCode
				,@Quantity = @Quantity
				,@AlternatePallet = NULL
				,@SubstitutePallet = NULL
				,@Litres = NULL

			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_SKU_Update'

				GOTO error
			END
		END

		SET @StorageUnitId = NULL

		SELECT @StatusId = dbo.ufn_StatusId('P', 'A')
		
--config - dont add new SKU for single SKU customer
if (dbo.ufn_Configuration(1015, 1)) = 1
begin
		SELECT @StorageUnitId = StorageUnitId
			,@PreviousPrice = UnitPrice
		FROM StorageUnit
		WHERE ProductId = @ProductId
			--AND SKUId = @SKUId
end
--add new SKU for multiple SKU customer
Else if (dbo.ufn_Configuration(1015, 1)) = 0
begin
SELECT @StorageUnitId = StorageUnitId
			,@PreviousPrice = UnitPrice
		FROM StorageUnit
		WHERE ProductId = @ProductId
			AND SKUId = @SKUId
end
		
		IF @StorageUnitId IS NULL
		BEGIN
			EXEC @Error = p_StorageUnit_Insert @StorageUnitId = @StorageUnitId OUTPUT
				,@SKUId = @SKUId
				,@ProductId = @ProductId
				,@Style = @Style
				,@Colour = @Colour
				,@Size = @Size
				,@UnitPrice = @UnitPrice
				,@StatusId = @StatusId

			--@ProductCategory  = @ProductCategory,    
			--@PackingCategory  = @PackingCategory,    
			--@PickEmpty        = @PickEmpty,    
			--@StackingCategory = @StackingCategory,    
			--@MovementCategory = @MovementCategory,    
			--@ValueCategory    = @ValueCategory,    
			--@StoringCategory  = @StoringCategory,    
			--@PickPartPallet   = @PickPartPallet,    
			--@MinimumQuantity  = @MinimumQuantity,    
			--@ReorderQuantity  = @ReorderQuantity,    
			--@MaximumQuantity  = @MaximumQuantity    
			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_StorageUnit_Insert'

				GOTO error
			END
		END
		ELSE
		BEGIN
			EXEC @Error = p_StorageUnit_Update @StorageUnitId = @StorageUnitId
				,@SKUId = @SKUId
				,@ProductId = @ProductId
				,@Style = @Style
				,@Colour = @Colour
				,@Size = @Size
				,@UnitPrice = @UnitPrice
				,@PreviousUnitPrice = @PreviousPrice
				,@StatusId = @StatusId

			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_StorageUnit_Update'

				GOTO error
			END
		END

		SELECT @StatusId = dbo.ufn_StatusId('B', 'A')

		SET @BatchId = NULL

		SELECT @BatchId = BatchId
		FROM Batch
		WHERE Batch = 'Default'

		IF @BatchId IS NULL
		BEGIN
			EXEC @Error = p_Batch_Insert @BatchId = @BatchId OUTPUT
				,@StatusId = @StatusId
				,@WarehouseId = 1
				,@Batch = 'Default'
				,@CreateDate = @getdate
				,@ExpiryDate = '2029-12-31'
				,@IncubationDays = 0
				,@ShelfLifeDays = 365
				,@ExpectedYield = NULL
				,@ActualYield = NULL
				,@ECLNumber = NULL

			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'

				GOTO error
		END
		END

		SET @StorageUnitBatchId = NULL

		SELECT @StorageUnitBatchId = StorageUnitBatchId
		FROM StorageUnitBatch
		WHERE StorageUnitId = @StorageUnitId
			AND BatchId = @BatchId

		IF @StorageUnitBatchId IS NULL
		BEGIN
			EXEC @Error = p_StorageUnitBatch_Insert @StorageUnitBatchId = @StorageUnitBatchId OUTPUT
				,@BatchId = @BatchId
				,@StorageUnitId = @StorageUnitId

			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'

				GOTO error
			END
		END
		ELSE
		BEGIN
			EXEC @Error = p_StorageUnitBatch_Update @StorageUnitBatchId = @StorageUnitBatchId
				,@BatchId = @BatchId
				,@StorageUnitId = @StorageUnitId

			IF @Error != 0
			BEGIN
				SELECT @ErrorMsg = 'Error executing p_StorageUnitBatch_Update'

				GOTO error
			END
		END

		DECLARE Pack_cursor CURSOR
		FOR
		SELECT ISNULL(PackCode, PackDescription)
			,PackDescription
			,Quantity
			,Barcode
			,Length
			,Width
			,Height
			,Volume
			,NettWeight
			,GrossWeight
			,TareWeight
			,ProductCategory
			,PackingCategory
			,PickEmpty
			,StackingCategory
			,MovementCategory
			,ValueCategory
			,StoringCategory
			,PickPartPallet
		FROM InterfaceImportPack
		WHERE InterfaceImportProductId = @InterfaceImportProductId

		OPEN Pack_cursor

		FETCH Pack_cursor
		INTO @PackCode
			,@PackDescription
			,@Quantity
			,@Barcode
			,@Length
			,@Width
			,@Height
			,@Volume
			,@NettWeight
			,@GrossWeight
			,@TareWeight
			,@ProductCategory
			,@PackingCategory
			,@PickEmpty
			,@StackingCategory
			,@MovementCategory
			,@ValueCategory
			,@StoringCategory
			,@PickPartPallet

		WHILE (@@fetch_status = 0)
		BEGIN
		
			DECLARE Warehouse_cursor CURSOR
			FOR
			SELECT WarehouseId
			FROM Warehouse
			WHERE WarehouseId = ParentWarehouseId

			OPEN Warehouse_cursor

			FETCH Warehouse_cursor
			INTO @WarehouseId

			WHILE (@@fetch_status = 0)
			BEGIN
			
				SET @PackId = NULL

				 -- if @PrincipalCode in ('DAB')
				 -- begin
				  
					--select @TBSPrincipalCode = 'TBS'
				    
					--if @TBSPrincipalCode is not null
					--begin
				    
					--	INSERT INTO InterfaceImportPack
					--	   (InterfaceImportProductId
					--		,PackCode
					--		,PackDescription
					--		,Quantity
					--		,Barcode
					--		,[Length]
					--		,Width
					--		,Height
					--		,Volume
					--		,NettWeight
					--		,GrossWeight
					--		,TareWeight
					--		,ProductCategory
					--		,PackingCategory
					--		,PickEmpty
					--		,StackingCategory		
					--		,MovementCategory
					--		,ValueCategory
					--		,StoringCategory
					--		,PickPartPallet
					--		,HostId
					--		,ForeignKey)
					-- select
					--		@InterfaceImportProductIdTBS
					--		,PackCode
					--		,PackDescription
					--		,Quantity
					--		,Barcode
					--		,[Length]
					--		,Width
					--		,Height
					--		,Volume
					--		,NettWeight
					--		,GrossWeight
					--		,TareWeight
					--		,ProductCategory
					--		,PackingCategory
					--		,PickEmpty
					--		,StackingCategory		
					--		,MovementCategory
					--		,ValueCategory
					--		,StoringCategory
					--		,PickPartPallet
					--		,HostId
					--		,ForeignKey
					--from InterfaceImportPack
					--where InterfaceImportProductId = @InterfaceImportProductId
					--end       
				    
					--end
    
				SELECT @PackTypeId = PackTypeId
				FROM PackType				 
				WHERE PackType IN ('Pallet')

				SELECT @PackId = PackId
				FROM Pack
				WHERE StorageUnitId = @StorageUnitId
					AND PackTypeId = @PackTypeId
					AND WarehouseId = @WarehouseId

				IF @PalletQuantity IS NOT NULL
					IF @PackId IS NULL
					BEGIN
						EXEC @Error = p_Pack_Insert @PackId = @PackId OUTPUT
						,@WarehouseId = @WarehouseId
							,@StorageUnitId = @StorageUnitId
							,@PackTypeId = @PackTypeId
							,@Quantity = 99999

						IF @Error != 0
						BEGIN
							SELECT @ErrorMsg = 'Error executing p_Pack_Insert'

							GOTO error
						END
					END
					ELSE
					BEGIN
						EXEC @Error = p_Pack_Update @PackId = @PackId
							,@WarehouseId = @WarehouseId
							,@StorageUnitId = @StorageUnitId
							,@PackTypeId = @PackTypeId
							,@Quantity = @PalletQuantity

						IF @Error != 0
						BEGIN
							SELECT @ErrorMsg = 'Error executing p_PackType_Update'

							GOTO error
						END
					END

				SET @PackId = NULL

				SELECT @PackTypeId = PackTypeId
				FROM PackType
				WHERE PackType IN ('Unit')

				  
				SELECT @PackId = PackId
				FROM Pack
				WHERE StorageUnitId = @StorageUnitId
					AND PackTypeId = @PackTypeId
					AND WarehouseId = @WarehouseId

				IF @PackId IS NULL
				BEGIN
					EXEC @Error = p_Pack_Insert @PackId = @PackId OUTPUT
						,@WarehouseId = @WarehouseId
						,@StorageUnitId = @StorageUnitId
						,@PackTypeId = @PackTypeId
						,@Quantity = 1

					IF @Error != 0
					BEGIN
						SELECT @ErrorMsg = 'Error executing p_Pack_Insert'

						GOTO error
					END
				END
				ELSE
				BEGIN
					EXEC @Error = p_Pack_Update @PackId = @PackId
						,@WarehouseId = @WarehouseId
						,@StorageUnitId = @StorageUnitId
						,@PackTypeId = @PackTypeId
						,@Quantity = 1

					IF @Error != 0
					BEGIN
						SELECT @ErrorMsg = 'Error executing p_PackType_Update'

						GOTO error
					END
				END

				SET @PackTypeId = NULL

				SELECT @PackTypeId = PackTypeId
				FROM PackType
				WHERE PackType = @PackCode

				IF @PackTypeId IS NULL
				BEGIN
					EXEC @Error = p_PackType_Insert @PackTypeId = @PackTypeId OUTPUT
						,@PackType = @PackCode
						,@InboundSequence = 1
						,@OutboundSequence = 1

					IF @Error != 0
					BEGIN
						SELECT @ErrorMsg = 'Error executing p_PackType_Insert'

						GOTO error
					END
				END
				ELSE
				BEGIN
					EXEC @Error = p_PackType_Update @PackTypeId = @PackTypeId
						,@PackType = @PackCode

					IF @Error != 0
					BEGIN
						SELECT @ErrorMsg = 'Error executing p_PackType_Update'

						GOTO error
					END
				END

				SET @PackId = NULL

				SELECT @PackId = PackId
				FROM Pack
				WHERE StorageUnitId = @StorageUnitId
					AND PackTypeId = @PackTypeId
					AND Barcode = @Barcode

				FETCH Warehouse_cursor
				INTO @WarehouseId
			END

			CLOSE Warehouse_cursor

			DEALLOCATE Warehouse_cursor

			FETCH Pack_cursor
			INTO @PackCode
				,@PackDescription
				,@Quantity
				,@Barcode
				,@Length
				,@Width
				,@Height
				,@Volume
				,@NettWeight
				,@GrossWeight
				,@TareWeight
				,@ProductCategory
				,@PackingCategory
				,@PickEmpty
				,@StackingCategory
				,@MovementCategory
				,@ValueCategory
				,@StoringCategory
				,@PickPartPallet
		END

		CLOSE Pack_cursor

		DEALLOCATE Pack_cursor

		error:

		IF @Error = 0
		BEGIN
			UPDATE InterfaceImportProduct
			SET RecordStatus = 'C'
			WHERE InterfaceImportProductId = @InterfaceImportProductId

			COMMIT TRANSACTION
		END
		ELSE
		BEGIN
			IF @@trancount > 0
				ROLLBACK TRANSACTION

			UPDATE InterfaceImportProduct
			SET RecordStatus = 'E'
			WHERE InterfaceImportProductId = @InterfaceImportProductId
		END

		FETCH Product_cursor
		INTO @InterfaceImportProductId
			,@ProductCode
			,@Product
			,@SKUCode
			,@SKU
			,@Style
			,@Colour
			,@Size
			,@PalletQuantity
			,@Barcode
			,@MinimumQuantity
			,@ReorderQuantity
			,@MaximumQuantity
			,@CuringPeriodDays
			,@ShelfLifeDays
			,@QualityAssuranceIndicator
			,@ProductType
			,@ProductCategory
			,@OverReceipt
			,@RetentionSamples
			,@HostId
			,@HostRecordStatus
			,@PrincipalCode
			,@UnitPrice
	END

	CLOSE Product_cursor

	DEALLOCATE Product_cursor

	UPDATE Pack
	SET TareWeight = isnull(Weight, 0) - isnull(NettWeight, 0)
	WHERE TareWeight IS NULL
END

--go
--IF OBJECT_ID('dbo.p_Generic_Import_Product') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Generic_Import_Product >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Generic_Import_Product >>>'
--go


