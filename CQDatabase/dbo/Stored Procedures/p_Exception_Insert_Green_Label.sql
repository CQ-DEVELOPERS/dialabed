﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Insert_Green_Label
  ///   Filename       : p_Exception_Insert_Green_Label.sql
  ///   Create By      : Karen	
  ///   Date Created   : March 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Insert_Green_Label
(
	@ReceiptLineId int,
	@Quantity float,
	@OperatorId	int
)


 
as
begin
	 set nocount on;
  
 declare @Reasonid int
 
 SET @Reasonid = (SELECT ReasonId FROM Reason WHERE ReasonCode = 'LAB02')
 
 if @ReceiptLineId = -1
   set @ReceiptLineId = null
  
	Insert into Exception
		(ReceiptLineId,
		ReasonId,
		ExceptionCode,
		Exception, 
		Quantity, 
		CreateDate,
		ExceptionDate,
		OperatorId) 
    Values
		(@ReceiptLineId,
		@Reasonid,
		'LAB02',
		'Green Labels Printed',
		@Quantity,
		GETDATE(),
		GETDATE(),
		@OperatorId)
end

