﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Free_Location_Summary
  ///   Filename       : p_Report_Free_Location_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Free_Location_Summary
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int,
 @AreaId           int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  AreaId       int,
	  Area         nvarchar(50),
	  Total        int,
	  Free         int,
	  Percentage   int
	 )
  
  if @AreaId = -1
    set @AreaId = null
  
  insert @TableResult
        (AreaId,
         Area,
         Total,
         Free)
  select a.AreaId,
         a.Area,
         count(1),
         null
    from Location                    l (nolock)
    join AreaLocation               al (nolock) on l.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId    = a.AreaId
   where a.WarehouseId = @WarehouseId
     and a.AreaId      = isnull(@AreaId, a.AreaId)
     and a.StockOnHand = 1
  group by a.AreaId,
           a.Area
  
  update tr
     set Free = (select count(distinct(vl.LocationId))
                   from viewLocation vl
                  where tr.AreaId = vl.AreaId
                    and vl.ActivePicking = 1
                    and vl.ActiveBinning = 1
                    and vl.StockTakeInd  = 0
                    and vl.Used          = 0)
    from @TableResult tr
  
  select Area,
         Total,
         Free,
         convert(numeric(13,3), convert(numeric(13,3), Free) / convert(numeric(13,3), Total) * convert(numeric(13,3), 100)) as 'Percentage'
    from @TableResult
  order by Area
end
