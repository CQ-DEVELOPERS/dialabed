﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_OutboundLine_Insert
  ///   Filename       : p_BOM_OutboundLine_Insert.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 13 Jan 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_OutboundLine_Insert
(@OutboundDocumentId int, 
 @StorageUnitId      int,
 @StatusId           int,
 @LineNumber         int,
 @Quantity           float,
 @BatchId            int)     
 
as
begin
	 set nocount on;
  
	  declare @Error            int,
			  @Errormsg         varchar(500),
			  @GetDate          datetime,
			  @BOMHeaderId      INT,
			  @BOMQuantity	    float,
			  @BOMStorageUnitId INT,
			  @OutboundLineId   INT
  
	select @GetDate = dbo.ufn_Getdate()
  
	begin transaction
  
	select @BOMHeaderId = BOMHeaderId 
	      ,@LineNumber = @LineNumber * -1
	from BOMHeader where StorageUnitId = @StorageUnitId
	
	IF @@ROWCOUNT > 0
    BEGIN
    
		declare BOMProduct_cursor cursor for  
		select bp.Quantity * @Quantity As Quantity
			  ,bp.StorageUnitId
		from BOMHeader bh
		inner join BOMLine bl on bh.BOMHeaderId = bl.BOMHeaderId
		inner join BOMProduct bp on bl.BOMLineId = bp.BOMLineId
		where bh.StorageUnitId = @StorageUnitId
		  and bp.StorageUnitId <> @StorageUnitId
     
		open BOMProduct_cursor  
      
		fetch BOMProduct_cursor  
		 into @BOMQuantity
			 ,@BOMStorageUnitId
			 
		while (@@fetch_status = 0)
		begin
          
			exec p_OutboundLine_Insert  
				  @OutboundLineId     = @OutboundLineId output,  
				  @OutboundDocumentId = @OutboundDocumentId,  
				  @StorageUnitId      = @BOMStorageUnitId,  
				  @StatusId           = @StatusId,  
				  @LineNumber         = @LineNumber,  
				  @Quantity           = @BOMQuantity,  
				  --@Weight             = @di_weight * @Quatity,  
				  @BatchId            = @BatchId     
		 
			fetch BOMProduct_cursor  
			into @BOMQuantity
				,@BOMStorageUnitId
			 
		end 
		close BOMProduct_cursor  
		deallocate BOMProduct_cursor  
    END
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_BOM_OutboundLine_Insert'
    rollback transaction
    return @Error
end
