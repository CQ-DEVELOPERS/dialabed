﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportPODetail_Insert
  ///   Filename       : p_InterfaceExportPODetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:05
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExportPODetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportPOHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(100) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @ReceiptLineId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportPODetail.InterfaceExportPOHeaderId,
  ///   InterfaceExportPODetail.ForeignKey,
  ///   InterfaceExportPODetail.LineNumber,
  ///   InterfaceExportPODetail.ProductCode,
  ///   InterfaceExportPODetail.Product,
  ///   InterfaceExportPODetail.SKUCode,
  ///   InterfaceExportPODetail.Batch,
  ///   InterfaceExportPODetail.Quantity,
  ///   InterfaceExportPODetail.Weight,
  ///   InterfaceExportPODetail.Additional1,
  ///   InterfaceExportPODetail.Additional2,
  ///   InterfaceExportPODetail.Additional3,
  ///   InterfaceExportPODetail.Additional4,
  ///   InterfaceExportPODetail.Additional5,
  ///   InterfaceExportPODetail.Additional6,
  ///   InterfaceExportPODetail.Additional7,
  ///   InterfaceExportPODetail.Additional8,
  ///   InterfaceExportPODetail.Additional9,
  ///   InterfaceExportPODetail.Additional10,
  ///   InterfaceExportPODetail.ReceiptLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportPODetail_Insert
(
 @InterfaceExportPOHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(100) = null,
 @SKUCode nvarchar(20) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @ReceiptLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceExportPODetail
        (InterfaceExportPOHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         ReceiptLineId)
  select @InterfaceExportPOHeaderId,
         @ForeignKey,
         @LineNumber,
         @ProductCode,
         @Product,
         @SKUCode,
         @Batch,
         @Quantity,
         @Weight,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         @ReceiptLineId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
