﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Performance
  ///   Filename       : p_Report_Performance.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Apr 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Performance
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int,
 @FromDate         datetime,
 @ToDate           datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OperatorId         int,
   Operator           nvarchar(50),
   OperatorGroupCode  nvarchar(10),
   JobId              int,
   InstructionId      int,
   InstructionType    nvarchar(50),
   insStatusId        int,
   insStatus          nvarchar(50),
   jobStatusId        int,
   jobStatus          nvarchar(50),
   StartDate          datetime,
   EndDate            datetime,
   StorageUnitBatchId int,
   StorageUnitId      int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   BatchId            int,
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   Weight             float,
   ConfirmedWeight    float
  )

  insert @TableResult
        (OperatorId,
         JobId,
         InstructionId,
         InstructionType,
         insStatusId,
         insStatus,
         jobStatusId,
         StartDate,
         EndDate,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight)
  select i.OperatorId,
         i.JobId,
         i.InstructionId,
         it.InstructionType,
         i.StatusId,
         s.Status,
         j.StatusId,
         i.StartDate,
         i.EndDate,
         i.StorageUnitBatchId,
         i.Quantity,
         i.ConfirmedQuantity,
         i.Weight,
         i.ConfirmedWeight
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status           s (nolock) on i.StatusId          = s.StatusId
   where i.EndDate between @FromDate and @ToDate
     and it.InstructionTypeCode in ('M','R','P','S','SM','O','PM','PS','FM','PR')
     and s.StatusCode            = 'F'
	 and i.warehouseId = @WarehouseId

--select i.OperatorId,
--         i.JobId,
--         i.InstructionId,
--         it.InstructionType,
--         i.StatusId,
--         s.Status,
--         j.StatusId,
--         i.StartDate,
--         i.EndDate,
--         i.StorageUnitBatchId,
--         i.Quantity,
--         i.ConfirmedQuantity,
--         i.Weight,
--         i.ConfirmedWeight
--    from Instruction      i (nolock)
--    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
--    join Job              j (nolock) on i.JobId             = j.JobId
--    join Status           s (nolock) on i.StatusId          = s.StatusId
--   where i.EndDate between @FromDate and @ToDate
--     and it.InstructionTypeCode in ('M','R','P','S','SM','O','PM','PS','FM','PR')
--     and s.StatusCode            = 'F'
--	 and i.warehouseId = @WarehouseId

  update tr
     set Operator          = o.Operator,
         OperatorGroupCode = og.OperatorGroupCode
    from @TableResult tr
    join Operator       o (nolock) on tr.OperatorId = o.OperatorId
    join OperatorGroup og (nolock) on o.OperatorGroupId = og.OperatorGroupId

  update tr
     set jobStatus = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.jobStatusId = s.StatusId

  update tr
     set StorageUnitId = sub.StorageUnitId,
         BatchId       = sub.BatchId
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId

  update tr
     set Batch = b.Batch
    from @TableResult tr
    join Batch         b (nolock) on tr.BatchId = b.BatchId

  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product
    from @TableResult tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product       p (nolock) on su.ProductId     = p.ProductId

  update tr
     set SKUCode = sku.SKUCode
    from @TableResult tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join SKU         sku (nolock) on su.SKUId         = sku.SKUId

  update tr
     set Weight          = tr.Quantity * p.Weight,
         ConfirmedWeight = tr.ConfirmedQuantity * p.Weight
    from @TableResult tr
    join Pack          p (nolock) on tr.StorageUnitId = p.StorageUnitId
   where p.Quantity = 1

  select JobId,
         InstructionId,
         InstructionType,
         Operator,
         jobStatus,
         insStatus,
         StartDate,
         EndDate,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight
    from @TableResult
---where OperatorGroupCode = 'OP'
   where OperatorGroupCode IN ('A', 'OP')
end
