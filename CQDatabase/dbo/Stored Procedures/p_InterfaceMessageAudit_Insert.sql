﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMessageAudit_Insert
  ///   Filename       : p_InterfaceMessageAudit_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:34
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceMessageAudit table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMessageId int = null,
  ///   @InterfaceMessageCode nvarchar(100) = null,
  ///   @InterfaceMessage nvarchar(max) = null,
  ///   @InterfaceId int = null,
  ///   @InterfaceTable nvarchar(256) = null,
  ///   @Status nvarchar(40) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @CreateDate datetime = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceMessageAudit.InterfaceMessageId,
  ///   InterfaceMessageAudit.InterfaceMessageCode,
  ///   InterfaceMessageAudit.InterfaceMessage,
  ///   InterfaceMessageAudit.InterfaceId,
  ///   InterfaceMessageAudit.InterfaceTable,
  ///   InterfaceMessageAudit.Status,
  ///   InterfaceMessageAudit.OrderNumber,
  ///   InterfaceMessageAudit.CreateDate,
  ///   InterfaceMessageAudit.ProcessedDate,
  ///   InterfaceMessageAudit.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMessageAudit_Insert
(
 @InterfaceMessageId int = null,
 @InterfaceMessageCode nvarchar(100) = null,
 @InterfaceMessage nvarchar(max) = null,
 @InterfaceId int = null,
 @InterfaceTable nvarchar(256) = null,
 @Status nvarchar(40) = null,
 @OrderNumber nvarchar(60) = null,
 @CreateDate datetime = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceMessageId = '-1'
    set @InterfaceMessageId = null;
  
  if @InterfaceMessageCode = '-1'
    set @InterfaceMessageCode = null;
  
	 declare @Error int
 
  insert InterfaceMessageAudit
        (InterfaceMessageId,
         InterfaceMessageCode,
         InterfaceMessage,
         InterfaceId,
         InterfaceTable,
         Status,
         OrderNumber,
         CreateDate,
         ProcessedDate,
         InsertDate)
  select @InterfaceMessageId,
         @InterfaceMessageCode,
         @InterfaceMessage,
         @InterfaceId,
         @InterfaceTable,
         @Status,
         @OrderNumber,
         @CreateDate,
         @ProcessedDate,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
