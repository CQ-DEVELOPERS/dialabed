﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaLocation_Search
  ///   Filename       : p_AreaLocation_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:42
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the AreaLocation table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null output,
  ///   @LocationId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   AreaLocation.AreaId,
  ///   AreaLocation.LocationId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaLocation_Search
(
 @AreaId int = null output,
 @LocationId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
 
  select
         AreaLocation.AreaId
        ,AreaLocation.LocationId
    from AreaLocation
   where isnull(AreaLocation.AreaId,'0')  = isnull(@AreaId, isnull(AreaLocation.AreaId,'0'))
     and isnull(AreaLocation.LocationId,'0')  = isnull(@LocationId, isnull(AreaLocation.LocationId,'0'))
  
end
