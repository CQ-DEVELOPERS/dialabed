﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Get_By_Type
  ///   Filename       : p_SerialNumber_Get_By_Type.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Get_By_Type
(
 @keyId   int,
 @keyType nvarchar(100),
 @StorageUnitId int = null
)
 
as
begin
  set nocount on;
  
  if @storageUnitId = -1
    set @storageUnitId = null
  
  if @keyType = 'ReceiptId'
  begin
    select sn.SerialNumberId,
           sn.SerialNumber
      from SerialNumber sn (nolock)
     where sn.ReceiptId = @keyId
       and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
    return
  end
  
  if @keyType = 'ReceiptLineId'
  begin
    select sn.SerialNumberId,
           sn.SerialNumber
      from SerialNumber sn (nolock)
     where sn.ReceiptLineId = @keyId
       and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
    return
  end
  
  if @keyType = 'IssueId'
  begin
    select sn.SerialNumberId,
           sn.SerialNumber
      from SerialNumber sn (nolock)
     where sn.IssueId = @keyId
       and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
    return
  end
  
  if @keyType = 'IssueLineId'
  begin
    select sn.SerialNumberId,
           sn.SerialNumber
      from SerialNumber sn (nolock)
     where sn.IssueLineId = @keyId
       and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
    return
  end
  
  if @keyType = 'StoreInstructionId'
  begin
    select sn.SerialNumberId,
           sn.SerialNumber
      from SerialNumber sn (nolock)
     where sn.StoreInstructionId = @keyId
       and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
    return
  end
  
  if @keyType = 'PickInstructionId'
  begin
    select sn.SerialNumberId,
           sn.SerialNumber
      from SerialNumber sn (nolock)
     where sn.PickInstructionId = @keyId
       and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
    return
  end
  
  if @keyType = 'StoreJobId'
  begin
    select sn.SerialNumberId,
           sn.SerialNumber
      from SerialNumber sn (nolock)
     where sn.StoreJobId = @keyId
       and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
    return
  end
  
  if @keyType = 'PickJobId'
  begin
    select sn.SerialNumberId,
           sn.SerialNumber
      from SerialNumber sn (nolock)
     where sn.PickJobId = @keyId
       and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
    return
  end
  
  select sn.SerialNumberId,
         sn.SerialNumber
    from SerialNumber sn (nolock)
   where sn.PickInstructionId = @keyId
     and sn.StorageUnitId = isnull(@storageUnitId, sn.StorageUnitId)
end
