﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MobileLog_Delete
  ///   Filename       : p_MobileLog_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:11
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the MobileLog table.
  /// </remarks>
  /// <param>
  ///   @MobileLogId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MobileLog_Delete
(
 @MobileLogId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete MobileLog
     where MobileLogId = @MobileLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
