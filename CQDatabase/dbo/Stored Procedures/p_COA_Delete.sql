﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Delete
  ///   Filename       : p_COA_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:50
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the COA table.
  /// </remarks>
  /// <param>
  ///   @COAId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Delete
(
 @COAId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete COA
     where COAId = @COAId
  
  select @Error = @@Error
  
  
  return @Error
  
end
