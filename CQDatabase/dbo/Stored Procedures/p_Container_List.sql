﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_List
  ///   Filename       : p_Container_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 11:46:28
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Container table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Container.ContainerId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ContainerId
        ,null as 'Container'
  union
  select
         Container.ContainerId
        ,Container.ContainerId as 'Container'
    from Container
  
end
