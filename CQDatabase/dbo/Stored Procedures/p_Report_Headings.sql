﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Headings
  ///   Filename       : p_Report_Headings.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Oct 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Headings
(
 @ServerName   nvarchar(50),
 @DatabaseName nvarchar(50),
 @UserName     nvarchar(10)
)
 
as
begin
  declare @CultureId int
  
  select @CultureId = CultureId
    from Operator (nolock)
   where Operator = @UserName
  
  if @CultureId is null
    select @CultureId = min(CultureId)
      from Culture (nolock)
  
  select ReportHeadingCode,
         ReportHeading
    from ReportHeading (nolock)
   where CultureId = @CultureId
end
