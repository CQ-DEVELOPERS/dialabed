﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMLine_Search
  ///   Filename       : p_BOMLine_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:11
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the BOMLine table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   BOMLine.BOMLineId,
  ///   BOMLine.BOMHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMLine_Search
(
 @BOMLineId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @BOMLineId = '-1'
    set @BOMLineId = null;
  
 
  select
         BOMLine.BOMLineId
        ,BOMLine.BOMHeaderId
    from BOMLine
   where isnull(BOMLine.BOMLineId,'0')  = isnull(@BOMLineId, isnull(BOMLine.BOMLineId,'0'))
  
end
