﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Inbound_Line_Sample_Quantity
    ///   Filename       : p_Mobile_Inbound_Line_Sample_Quantity.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 2 March 2009 20:04:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
  
  ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_Inbound_Line_Sample_Quantity
(
 @receiptLineId       int
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @GetDate            datetime,
          @SampleQuantity     float
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @SampleQuantity = isnull(p.RetentionSamples,0) + isnull(p.Samples,0) + isnull(p.AssaySamples,0)
    from ReceiptLine       rl (nolock)
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
   where rl.ReceiptLineId = @receiptLineId
  
  if @SampleQuantity is null
    set @SampleQuantity = 0
  
  select @SampleQuantity
  return @SampleQuantity
  
  error:
    set @SampleQuantity = -1
    select @SampleQuantity
    return @SampleQuantity
end

