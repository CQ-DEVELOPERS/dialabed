﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Finished
  ///   Filename       : p_Mobile_Product_Check_Finished.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Jan 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Finished
(
 @jobId             int,
 @OperatorId        int = null
)
 
as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         JobId,
         OperatorId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @JobId,
         @OperatorId,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @StatusId            int,
          @GetDate             datetime,
          @StatusCode          nvarchar(10),
          @InstructionTypeCode nvarchar(10)
	 
	 select @GetDate = dbo.ufn_Getdate()
	 
	 begin transaction
	 
  select top 1 @InstructionTypeCode = it.InstructionTypeCode
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where i.JobId = @JobId
  
  if not exists(select top 1 1
                  from Instruction (nolock)
                 where JobId = @JobId
                   and ConfirmedQuantity != isnull(CheckQuantity, 0))
  begin
    if @InstructionTypeCode in ('S','SM','PR')
    begin
      exec @Error = p_Receiving_Accept
	      @jobId      = @jobId,
       @operatorId = @OperatorId
      
      if @Error <> 0
        goto error
    end
    else
    begin
      exec @Error = p_Job_Update
       @JobId          = @JobId,
       @CheckedBy      = @OperatorId,
       @CheckedDate    = @GetDate
      
      if @Error <> 0
        goto error
      
      select @StatusCode = s.StatusCode
        from Job    j
        join Status s on j.StatusId = s.StatusId
       where j.JobId = @jobId
      
      if @StatusCode = 'CK' -- GS 2012-01-13 only update if in checking
      begin
        exec @Error = p_Status_Rollup
         @jobId = @jobId
        
        if @Error <> 0
          goto error
      end
    end
  end
  else
  begin
    if @InstructionTypeCode in ('S','SM','PR')
    begin
      exec @Error = p_Receiving_Reject
	      @jobId      = @jobId,
       @operatorId = @OperatorId
      
      if @Error <> 0
        goto error
    end
    else
    begin
      select @StatusId = dbo.ufn_StatusId('IS','QA')
      
      exec @Error = p_Job_Update
       @JobId          = @JobId,
       @StatusId       = @StatusId,
       @CheckedBy      = @OperatorId,
       @CheckedDate    = @GetDate
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Status_Rollup
       @JobId          = @JobId
      
      if @Error <> 0
        goto error
    end
    
    set @Error = -1
  end
  
  commit transaction
  
  --insert ProductCheck
  --      (JobId,
  --       OperatorId)
  --select @JobId,
  --       @OperatorId
  
  update MobileLog
     set EndDate = @getdate
   where MobileLogId = @MobileLogId
  
  select @Error
  return @Error
  
  error:
    rollback transaction
    
    update MobileLog
       set EndDate = getdate()
     where MobileLogId = @MobileLogId
    
    select @Error
    return @Error
end
