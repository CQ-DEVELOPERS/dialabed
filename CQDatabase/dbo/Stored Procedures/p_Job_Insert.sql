﻿/*
  /// <summary>
  ///   Procedure Name : p_Job_Insert
  ///   Filename       : p_Job_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Aug 2014 12:07:49
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Job table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null output,
  ///   @PriorityId int = null,
  ///   @OperatorId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @ReceiptLineId int = null,
  ///   @IssueLineId int = null,
  ///   @ContainerTypeId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @TareWeight float = null,
  ///   @Weight float = null,
  ///   @NettWeight float = null,
  ///   @CheckedBy int = null,
  ///   @CheckedDate datetime = null,
  ///   @DropSequence int = null,
  ///   @Pallets int = null,
  ///   @BackFlush bit = null,
  ///   @Prints int = null,
  ///   @CheckingClear bit = null,
  ///   @TrackingNumber nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   Job.JobId,
  ///   Job.PriorityId,
  ///   Job.OperatorId,
  ///   Job.StatusId,
  ///   Job.WarehouseId,
  ///   Job.ReceiptLineId,
  ///   Job.IssueLineId,
  ///   Job.ContainerTypeId,
  ///   Job.ReferenceNumber,
  ///   Job.TareWeight,
  ///   Job.Weight,
  ///   Job.NettWeight,
  ///   Job.CheckedBy,
  ///   Job.CheckedDate,
  ///   Job.DropSequence,
  ///   Job.Pallets,
  ///   Job.BackFlush,
  ///   Job.Prints,
  ///   Job.CheckingClear,
  ///   Job.TrackingNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : Edgar Blount
  ///   Modified Date  : 
  ///   Details        : To support create stock stock from Recon screen
  /// </newpara>
*/
CREATE procedure p_Job_Insert
(
 @JobId int = null output,
 @PriorityId int = null,
 @OperatorId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @ReceiptLineId int = null,
 @IssueLineId int = null,
 @ContainerTypeId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @TareWeight float = null,
 @Weight float = null,
 @NettWeight float = null,
 @CheckedBy int = null,
 @CheckedDate datetime = null,
 @DropSequence int = null,
 @Pallets int = null,
 @BackFlush bit = null,
 @Prints int = null,
 @CheckingClear bit = null,
 @TrackingNumber nvarchar(60) = null,
 @OriginalJobId int = null
)
 
as
begin
	 set nocount on;
  
  if @JobId = '-1'
    set @JobId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @CheckedBy = '-1'
    set @CheckedBy = null;

	 if @OriginalJobId = '-1'
    set @OriginalJobId = null;
  
	 declare @Error int
 
  insert Job
        (PriorityId,
         OperatorId,
         StatusId,
         WarehouseId,
         ReceiptLineId,
         IssueLineId,
         ContainerTypeId,
         ReferenceNumber,
         TareWeight,
         Weight,
         NettWeight,
         CheckedBy,
         CheckedDate,
         DropSequence,
         Pallets,
         BackFlush,
         Prints,
         CheckingClear,
         TrackingNumber,
		 OriginalJobId)
  select @PriorityId,
         @OperatorId,
         @StatusId,
         @WarehouseId,
         @ReceiptLineId,
         @IssueLineId,
         @ContainerTypeId,
         @ReferenceNumber,
         @TareWeight,
         @Weight,
         @NettWeight,
         @CheckedBy,
         @CheckedDate,
         @DropSequence,
         @Pallets,
         @BackFlush,
         @Prints,
         @CheckingClear,
         @TrackingNumber,
		 @OriginalJobId 
  
  select @Error = @@Error, @JobId = scope_identity()
  
  
  return @Error
  
end
