﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnit_List
  ///   Filename       : p_StorageUnit_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:02:12
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnit table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnit.StorageUnitId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnit_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as StorageUnitId
        ,null as 'StorageUnit'
  union
  select
         StorageUnit.StorageUnitId
        ,StorageUnit.StorageUnitId as 'StorageUnit'
    from StorageUnit
  
end
