﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Intergration_Get_File_Fields
  ///   Filename       : p_Intergration_Get_File_Fields.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Mar 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Intergration_Get_File_Fields
(
 @interfaceMappingFileId int
)
 
as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   InterfaceFieldId int,
   InterfaceFieldCode   nvarchar(100),
   InterfaceField       nvarchar(100),
   Datatype             nvarchar(50),
   Mandatory            bit
  )
  
  insert @TableResult
        (InterfaceFieldId,
         InterfaceField,
         InterfaceFieldCode,
         Datatype,
         Mandatory)
  select ifd.InterfaceFieldId,
         ifd.InterfaceField,
         ifd.InterfaceFieldCode,
         ifd.Datatype,
         isnull(ifd.Mandatory,0)
    from InterfaceMappingFile  imf (nolock)
    join InterfaceFileType     ift (nolock) on imf.InterfaceFileTypeId = ift.InterfaceFileTypeId
    join InterfaceDocumentType idt (nolock) on imf.InterfaceDocumentTypeId = idt.InterfaceDocumentTypeId
    join InterfaceField        ifd (nolock) on idt.InterfaceDocumentTypeId = ifd.InterfaceDocumentTypeId
   where ift.InterfaceFileTypeCode = 'TXT'
     and imf.InterfaceMappingFileId = @interfaceMappingFileId
     and ifd.InterfaceFieldCode = 'Place Holder'
  union
  select ifd.InterfaceFieldId,
         ifd.InterfaceField,
         REPLACE(REPLACE(REPLACE(ifd.InterfaceFieldCode, 'Interface',''), 'Import',''), 'Export',''),
         ifd.Datatype,
         isnull(ifd.Mandatory,0)
    from InterfaceMappingFile  imf (nolock)
    join InterfaceFileType     ift (nolock) on imf.InterfaceFileTypeId = ift.InterfaceFileTypeId
    join InterfaceDocumentType idt (nolock) on imf.InterfaceDocumentTypeId = idt.InterfaceDocumentTypeId
    join InterfaceField        ifd (nolock) on idt.InterfaceDocumentTypeId = ifd.InterfaceDocumentTypeId
    left
    join InterfaceMappingColumn imc (nolock) on ifd.InterfaceFieldId = imc.InterfaceFieldId
                                            and idt.InterfaceDocumentTypeId = imc.InterfaceDocumentTypeId
                                            and imf.InterfaceMappingFileId = imc.InterfaceMappingFileId
   where imf.InterfaceMappingFileId = @interfaceMappingFileId
     and imc.InterfaceMappingColumnId is null
     and ifd.InterfaceFieldCode != 'Place Holder'
  
  if (select COUNT(distinct(InterfaceFieldCode)) from @TableResult where InterfaceFieldCode != 'Place Holder') > 1
    update @TableResult
       set InterfaceField = InterfaceFieldCode + ' - ' + InterfaceField
  
  --select distinct InterfaceFieldCode from @TableResult
  
  select InterfaceFieldId,
         InterfaceField,
         InterfaceFieldCode,
         Datatype,
         Mandatory
    from @TableResult
  order by InterfaceFieldCode desc, InterfaceFieldId
end
