﻿
/*
 /// <summary>
 ///   Procedure Name : p_Reserve_Batches_Delete
 ///   Filename       : p_Reserve_Batches_Delete.sql
 ///   Create By      : Grant Schultz
 ///   Date Created   : 15 Mar 2010
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
 /// <newpara>
 ///   Modified by    : 
 ///   Modified Date  : 
 ///   Details        : 
 /// </newpara>
*/
CREATE procedure p_Reserve_Batches_Delete
(
 @OutboundShipmentId int = null,
 @IssueId            int = null
)
 
as
begin
	 set nocount on;
  
  declare @TableInstructions as table
  (
   InstructionId int
  )
  
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @InstructionId     int

  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is not null
    insert @TableInstructions
          (InstructionId)
    select InstructionId
      from Instruction i (nolock) 
      join Status      s (nolock) on i.StatusId = s.StatusId
     where s.StatusCode in ('W','S')
       and i.JobId in (select ReservedId from Issue (nolock) where IssueId = @IssueId)
  else if @IssueId is not null
    insert @TableInstructions
          (InstructionId)
    select InstructionId
      from Instruction i (nolock) 
      join Status      s (nolock) on i.StatusId = s.StatusId
     where s.StatusCode in ('W','S')
       and i.JobId in (select ReservedId from Issue (nolock) where IssueId = @IssueId)
  
  begin transaction
  
  while exists(select top 1 1
                 from @TableInstructions)
  begin
    select @InstructionId = InstructionId
      from @TableInstructions
    
    delete @TableInstructions
     where InstructionId = @InstructionId
    
    exec @Error = p_Housekeeping_Instruction_Delete
     @InstructionId       = @InstructionId
    
    if @Error <> 0
      goto error
  end
  
  if @OutboundShipmentId is not null
    update i
       set ReservedId = null
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
  else if @IssueId is not null
    update Issue
       set ReservedId = null
     where IssueId = @IssueId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return

  error:
    raiserror 900000 'Error executing p_Reserve_Batches_Delete'
    rollback transaction
    return @Error
end
