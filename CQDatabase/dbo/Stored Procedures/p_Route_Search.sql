﻿/*
  /// <summary>
  ///   Procedure Name : p_Route_Search
  ///   Filename       : p_Route_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:07
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Route table.
  /// </remarks>
  /// <param>
  ///   @RouteId int = null output,
  ///   @Route nvarchar(100) = null,
  ///   @RouteCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Route.RouteId,
  ///   Route.Route,
  ///   Route.RouteCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : Gareth
  ///   Modified Date  : 2020-12-08
  ///   Details        : add checking lane
  /// </newpara>
*/
CREATE PROCEDURE [dbo].[p_Route_Search]
(
 @RouteId int = null output,
 @Route nvarchar(100) = null,
 @RouteCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @Route = '-1'
    set @Route = null;
  
  if @RouteCode = '-1'
    set @RouteCode = null;
  
 
  select
         Route.RouteId,
         Route.CheckingLaneId
        ,Route.Route
        ,Route.RouteCode
    from Route
   where isnull(Route.RouteId,'0')  = isnull(@RouteId, isnull(Route.RouteId,'0'))
     and isnull(Route.Route,'%')  like '%' + isnull(@Route, isnull(Route.Route,'%')) + '%'
     and isnull(Route.RouteCode,'%')  like '%' + isnull(@RouteCode, isnull(Route.RouteCode,'%')) + '%'
  
end
