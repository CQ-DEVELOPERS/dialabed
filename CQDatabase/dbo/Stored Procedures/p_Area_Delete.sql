﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Delete
  ///   Filename       : p_Area_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2014 09:46:58
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Area table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Delete
(
 @AreaId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Area
     where AreaId = @AreaId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_AreaHistory_Insert
         @AreaId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
