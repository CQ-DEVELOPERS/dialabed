﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Update
  ///   Filename       : p_Batch_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Sep 2014 15:23:46
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Batch table.
  /// </remarks>
  /// <param>
  ///   @BatchId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @Batch nvarchar(100) = null,
  ///   @CreateDate datetime = null,
  ///   @ExpiryDate datetime = null,
  ///   @IncubationDays int = null,
  ///   @ShelfLifeDays int = null,
  ///   @ExpectedYield int = null,
  ///   @ActualYield int = null,
  ///   @ECLNumber nvarchar(20) = null,
  ///   @BatchReferenceNumber nvarchar(100) = null,
  ///   @ProductionDateTime datetime = null,
  ///   @FilledYield int = null,
  ///   @COACertificate bit = null,
  ///   @StatusModifiedBy int = null,
  ///   @StatusModifiedDate datetime = null,
  ///   @CreatedBy int = null,
  ///   @ModifiedBy int = null,
  ///   @ModifiedDate datetime = null,
  ///   @Prints int = null,
  ///   @ParentProductCode nvarchar(60) = null,
  ///   @RI float = null,
  ///   @Density float = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @BillOfEntry nvarchar(100) = null,
  ///   @ReferenceNumber nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Update
(
 @BatchId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @Batch nvarchar(100) = null,
 @CreateDate datetime = null,
 @ExpiryDate datetime = null,
 @IncubationDays int = null,
 @ShelfLifeDays int = null,
 @ExpectedYield int = null,
 @ActualYield int = null,
 @ECLNumber nvarchar(20) = null,
 @BatchReferenceNumber nvarchar(100) = null,
 @ProductionDateTime datetime = null,
 @FilledYield int = null,
 @COACertificate bit = null,
 @StatusModifiedBy int = null,
 @StatusModifiedDate datetime = null,
 @CreatedBy int = null,
 @ModifiedBy int = null,
 @ModifiedDate datetime = null,
 @Prints int = null,
 @ParentProductCode nvarchar(60) = null,
 @RI float = null,
 @Density float = null,
 @BOELineNumber nvarchar(100) = null,
 @BillOfEntry nvarchar(100) = null,
 @ReferenceNumber nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Batch = '-1'
    set @Batch = null;
  
  if @StatusModifiedBy = '-1'
    set @StatusModifiedBy = null;
  
  if @CreatedBy = '-1'
    set @CreatedBy = null;
  
  if @ModifiedBy = '-1'
    set @ModifiedBy = null;
  
	 declare @Error int
 
  update Batch
     set StatusId = isnull(@StatusId, StatusId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         Batch = isnull(@Batch, Batch),
         CreateDate = isnull(@CreateDate, CreateDate),
         ExpiryDate = isnull(@ExpiryDate, ExpiryDate),
         IncubationDays = isnull(@IncubationDays, IncubationDays),
         ShelfLifeDays = isnull(@ShelfLifeDays, ShelfLifeDays),
         ExpectedYield = isnull(@ExpectedYield, ExpectedYield),
         ActualYield = isnull(@ActualYield, ActualYield),
         ECLNumber = isnull(@ECLNumber, ECLNumber),
         BatchReferenceNumber = isnull(@BatchReferenceNumber, BatchReferenceNumber),
         ProductionDateTime = isnull(@ProductionDateTime, ProductionDateTime),
         FilledYield = isnull(@FilledYield, FilledYield),
         COACertificate = isnull(@COACertificate, COACertificate),
         StatusModifiedBy = isnull(@StatusModifiedBy, StatusModifiedBy),
         StatusModifiedDate = isnull(@StatusModifiedDate, StatusModifiedDate),
         CreatedBy = isnull(@CreatedBy, CreatedBy),
         ModifiedBy = isnull(@ModifiedBy, ModifiedBy),
         ModifiedDate = isnull(@ModifiedDate, ModifiedDate),
         Prints = isnull(@Prints, Prints),
         ParentProductCode = isnull(@ParentProductCode, ParentProductCode),
         RI = isnull(@RI, RI),
         Density = isnull(@Density, Density),
         BOELineNumber = isnull(@BOELineNumber, BOELineNumber),
         BillOfEntry = isnull(@BillOfEntry, BillOfEntry),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber) 
   where BatchId = @BatchId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_BatchHistory_Insert
         @BatchId = @BatchId,
         @StatusId = @StatusId,
         @WarehouseId = @WarehouseId,
         @Batch = @Batch,
         @CreateDate = @CreateDate,
         @ExpiryDate = @ExpiryDate,
         @IncubationDays = @IncubationDays,
         @ShelfLifeDays = @ShelfLifeDays,
         @ExpectedYield = @ExpectedYield,
         @ActualYield = @ActualYield,
         @ECLNumber = @ECLNumber,
         @BatchReferenceNumber = @BatchReferenceNumber,
         @ProductionDateTime = @ProductionDateTime,
         @FilledYield = @FilledYield,
         @COACertificate = @COACertificate,
         @StatusModifiedBy = @StatusModifiedBy,
         @StatusModifiedDate = @StatusModifiedDate,
         @CreatedBy = @CreatedBy,
         @ModifiedBy = @ModifiedBy,
         @ModifiedDate = @ModifiedDate,
         @Prints = @Prints,
         @ParentProductCode = @ParentProductCode,
         @RI = @RI,
         @Density = @Density,
         @BOELineNumber = @BOELineNumber,
         @BillOfEntry = @BillOfEntry,
         @ReferenceNumber = @ReferenceNumber,
         @CommandType = 'Update'
  
  return @Error
  
end
