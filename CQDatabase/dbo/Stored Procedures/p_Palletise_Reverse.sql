﻿ 
 
/*
  /// <summary>
  ///   Procedure Name : p_Palletise_Reverse
  ///   Filename       : p_Palletise_Reverse.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Palletise_Reverse
(
 @ReceiptId          int = null,
 @ReceiptLineId      int = null,
 @IssueId            int = null,
 @IssueLineId        int = null,
 @OutboundShipmentId int = null
)
 
as
begin
	 set nocount on;
	 
  declare @TableInstructions as table
  (
   InstructionId int,
   StatusCode    nvarchar(10)
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Count             int,
          @InstructionId     int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @ReceiptId          is null and
     @ReceiptLineId      is null and
     @IssueId            is null and
     @IssueLineId        is null and
     @OutboundShipmentId is null
    return;
  
  if @ReceiptLineId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i
      join Status      s (nolock) on i.StatusId = s.StatusId
     where ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId)
  end
  else if @ReceiptId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction  i
      join Status       s (nolock) on i.StatusId      = s.StatusId
      join ReceiptLine rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
     where ReceiptId     = isnull(@ReceiptId, ReceiptId)
  end
  
  if @OutboundShipmentId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i
      join Status      s (nolock) on i.StatusId    = s.StatusId
     where i.OutboundShipmentId = @OutboundShipmentId
    union
    select InstructionId,
           StatusCode
      from Instruction i
      join IssueLine  il on i.IssueLineId = il.IssueLineId
      join OutboundShipmentIssue osi on il.IssueId = osi.IssueId
      join Status      s (nolock) on i.StatusId    = s.StatusId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  else if @IssueId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i
      join Status      s (nolock) on i.StatusId    = s.StatusId
      join IssueLine  il (nolock) on i.IssueLineId = il.IssueLineId
     where IssueId = isnull(@IssueId, IssueId)
  end
  else if @IssueLineId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i
      join Status      s (nolock) on i.StatusId = s.StatusId
     where IssueLineId = isnull(@IssueLineId, IssueLineId)
  end
  
  set @Count = (select count(1) from @TableInstructions)
  
  begin transaction
  
  if not exists(select 1
                  from @TableInstructions
                 where StatusCode not in ('W','NS','Z')) and @Count > 0
  begin
    while @Count > 0
    begin
      set @Count = @Count - 1
      
      select top 1 @InstructionId = InstructionId
        from @TableInstructions
      
      delete @TableInstructions where InstructionId = @InstructionId
      
      exec @Error = p_StorageUnitBatchLocation_Deallocate
       @InstructionId = @InstructionId,
       @Confirmed     = 1
      
      if @Error <> 0
        goto error
      
      delete IssueLineInstruction
       where InstructionId = @InstructionId
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      delete Exception
       where InstructionId = @InstructionId
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      exec @Error = p_instruction_Delete @InstructionId
      
      if @Error <> 0
        goto error
    end
  end
  else if @Count > 0
  begin
    set @Error = -1
    set @Errormsg = 'This instructions have been started or finished, and cannot be re-palletised (p_Palletise_Reverse).'
    goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
