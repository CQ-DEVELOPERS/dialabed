﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_StockTake_KPI
  ///   Filename       : p_Report_StockTake_KPI.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 11 Nov 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Junaid Desai	
  ///   Modified Date  : 13 Oct 2008
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_StockTake_KPI
(
		@FromDate			datetime,
		@ToDate				datetime,
		@OperatorGroupId		int
)
 
as
begin
	 set nocount on;
	

Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int

Set @DateHoursDiff = DateDiff(hh,@FromDate,@ToDate)
Set @DateDaysDiff = DateDiff(D,@FromDate,@ToDate)
Set @DateMonthsDiff = DateDiff(M,@FromDate,@ToDate) 

Declare @StockTakesTempTable as Table 
(
	StockTakes					int,
	EndDate						DateTime,
	ExceptionTarget				Decimal(10,3)
)


Insert Into @StockTakesTempTable (StockTakes,EndDate,ExceptionTarget)
(
SELECT     Count(I.InstructionId) As  NumberOfExceptionStockTakes,convert(nvarchar(14), Enddate, 120) + '00:00' --, I.OperatorId,  I.CreateDate, I.StartDate, I.EndDate, IT.InstructionType
,Sum(Distinct(KT.KPITargetValue))
FROM         Instruction AS I INNER JOIN
                      InstructionType AS IT ON I.InstructionTypeId = IT.InstructionTypeId INNER JOIN
							  Operator O ON I.OperatorId = O.OperatorId
							   join KPITarget KT on O.OperatorGroupId = KT.OperatorGroupID	
WHERE     (IT.InstructionTypeId = 1)
And I.EndDate Between @FromDate And @ToDate
And O.OperatorGroupId = @OperatorGroupId
and KT.KPITargetCode = 'E' 
Group By EndDate )

if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
							Select SUM(StockTakes) as StockTakes,							
							Convert(nvarchar,EndDate,108) as EndDate, 
							Convert(nvarchar,EndDate,108) as ordscale,
							SUM(Distinct(ExceptionTarget)) as ExceptionTarget
							From @StockTakesTempTable
							Group By Convert(nvarchar,EndDate,108)
							Order By ordscale

			-- Select data by this Scale
					End
				Else
					Begin
					--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
							Select SUM(StockTakes) as StockTakes,							
							Convert(nvarchar,EndDate,101) as EndDate, 
							Convert(nvarchar,EndDate,101) as ordscale,
							8 * SUM(Distinct(ExceptionTarget)) as ExceptionTarget 
							From @StockTakesTempTable
							Group By Convert(nvarchar,EndDate,101)
							Order By ordscale
					End
			End
		Else
				Begin
			
							Select SUM(StockTakes) as StockTakes,							
							DatePart(wk,EndDate) as EndDate, 
									DatePart(wk,EndDate) as ordscale,
							5 * 8 * SUM(Distinct(ExceptionTarget)) as ExceptionTarget
							From @StockTakesTempTable
							Group By DatePart(wk,EndDate) 
							Order By ordscale
			
			End
	End
Else
	Begin
	
							Select SUM(StockTakes) as StockTakes,							
							DateName(M,EndDate) as EndDate, 
									DatePart(M,EndDate) as ordscale,
							22 * 8 * SUM(Distinct(ExceptionTarget)) as ExceptionTarget
							From @StockTakesTempTable
							Group By DateName(M,EndDate),DatePart(M,EndDate)
							Order By ordscale

	End




--Select @startDate,@enddate

end
