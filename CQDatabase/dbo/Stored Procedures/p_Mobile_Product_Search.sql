﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Search
  ///   Filename       : p_Mobile_Product_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Search
(
 @Product     nvarchar(50) = null,
 @ProductCode nvarchar(30) = null,
 @SKU         nvarchar(50) = null,
 @SKUCode     nvarchar(50) = null,
 @Batch       nvarchar(50) = null,
 @ECLNumber   nvarchar(10) = null,
 @Barcode     nvarchar(50) = null
)
 
as
begin
	 set nocount on;
  
  declare @Error    int,
          @Rowcount int
  
  declare @TableResult as table
  (
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50)
  )
  
  if isnull(@Product,'')       = '' and
     isnull(@ProductCode,'')   = '' and
     isnull(@SKU,'')           = '' and
     isnull(@SKUCode,'')       = '' and
     isnull(@Barcode,'')       = ''
    goto Result;
  
  insert @TableResult
        (StorageUnitBatchId,
         ProductCode,
         Product,
         SKUCode,
         Batch)
  select distinct sub.StorageUnitBatchId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Batch              b (nolock) on sub.BatchId       = b.BatchId
    join Product            p (nolock) on su.ProductId      = p.ProductId
    join SKU              sku (nolock) on su.SKUId          = sku.SKUId
    left join Pack         pk (nolock) on su.StorageUnitId  = pk.StorageUnitId
   where isnull(p.ProductType,'') != 'FPC'
     and p.Product     like '%' + isnull(@Product,'') + '%'
     and p.ProductCode like '%' + isnull(@ProductCode,'') + '%'
     and sku.SKUCode   like '%' + isnull(@SKUCode,'') + '%'
     and sku.SKU       like '%' + isnull(@SKU,'') + '%'
     and b.Batch       like '%' + isnull(@Batch,'') + '%'
     and isnull(b.ECLNumber,'')   like '%' + isnull(@ECLNumber,'') + '%'
     and (isnull(p.Barcode,'')    like '%' + isnull(@Barcode,'') + '%'
     or   isnull(pk.Barcode,'')   like '%' + isnull(@Barcode,'') + '%')
  
  select @Error = @@Error,
         @Rowcount = @@Rowcount
  
  if @Error <> 0
  begin
    set @Error = 1;
    goto Result;
  end
  
--  if @Rowcount = 0
--  begin
--    set @Error = 2
--    
--    insert @TableResult
--          (ProductCode,
--           SKUCode,
--           Batch)
--    select null,
--           null,
--           null
--    
--    goto Result;
--  end
  
  Result:
  select @Error as 'Result',
         StorageUnitBatchId,
         ProductCode,
         Product,
         SKUCode,
         Batch
    from @TableResult
end
