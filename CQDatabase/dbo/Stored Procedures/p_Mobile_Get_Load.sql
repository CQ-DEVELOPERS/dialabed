﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Get_Load
  ///   Filename       : p_Mobile_Get_Load.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Get_Load
(
 @WarehouseId int,
 @OrderNumber nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @IntransitLoadId   int
  
  set @Errormsg = 'Error executing p_Mobile_Get_Load'
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @IntransitLoadId = IntransitLoadId
    from IntransitLoad (nolock)
   where convert(nvarchar(30), IntransitLoadId) = @OrderNumber
  
  if @IntransitLoadId is null
  begin
    set @IntransitLoadId = -1
    set @Error = -1
    goto result
  end
  
  result:
    select @IntransitLoadId as '@IntransitLoadId'
    return @IntransitLoadId
  
  error:
    raiserror 900000 @Errormsg
    select @Error as '@Error'
    return @Error
end
