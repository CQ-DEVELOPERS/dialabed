﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Pickface_Without_Stock
  ///   Filename       : p_Report_Pickface_Without_Stock.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 10 June 2110
  ///   Details        : Excluded inactive products from list
  /// </newpara>
*/
CREATE procedure p_Report_Pickface_Without_Stock
(
 @WarehouseId int
)
 
as
begin
  set nocount on;
  
  declare @SOH as table
  (
   StorageUnitId   int, 
   WarehouseId   int,
   SOH         float
  )
  
  declare @Products as table
  (
   StorageUnitId int,
   ProductId  int, 
   AreaId   int,
   WarehouseId  int,
   Quantity3  float
   )
  
  insert @Products
        (StorageUnitId,
         ProductId,
         WarehouseId)
  select distinct su.StorageUnitId,
         su.ProductId,
         a.WarehouseId
    from StorageUnit      su (nolock)
    join StorageUnitArea sua (nolock) on su.StorageUnitId = sua.StorageUnitId
    join Area              a (nolock) on sua.AreaId       = a.AreaId
   where a.WarehouseId = isnull(@WarehouseId, WarehouseId)
     and a.StockOnHand = 1
   
   insert @SOH
       (StorageUnitId,
         WarehouseId,
         SOH) 
  select sub.StorageUnitId,
         a.WarehouseId,
         sum (subl.ActualQuantity)
   from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch         sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Location                   l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation              al (nolock) on l.LocationId            = al.LocationId
    join Area                       a (nolock) on al.AreaId               = a.AreaId 
    where a.StockOnHand = 1
      and a.WarehouseId = @WarehouseId
    group by sub.StorageUnitId, 
              a.Warehouseid
             
  select distinct p.ProductCode,
         p.Product,
         sku.SKUCode,
         soh.SOH
    from StorageUnitBatch    sub (nolock)
    join StorageUnit          su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product               p (nolock) on su.ProductId      = p.ProductId
    join SKU                 sku (nolock) on su.SKUId          =  sku.SKUId
    join Batch                 b (nolock) on b.BatchId         = sub.BatchId
    join Status                s (nolock) on p.StatusId        = s.StatusId
    left
    join @SOH                soh          on soh.StorageUnitId = su.StorageUnitId
    left
    join StorageUnitLocation sul (nolock) on sub.StorageUnitId = sul.StorageUnitId
   where sul.LocationId is null
     and b.WarehouseId   = @WarehouseId 
     and s.Status       <> 'Inactive'
  order by soh.SOH desc,
           p.ProductCode,
           sku.SKUCode
end
