﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Inbound_Order_Select
  ///   Filename       : p_Mobile_Inbound_Order_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  
/// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Inbound_Order_Select

(
 @ReceiptId int
)
 
as
begin
	 set nocount on;
  
  select r.ReceiptId,
         id.OrderNumber,
         r.DeliveryDate,
         r.VehicleRegistration,
         r.DeliveryNoteNumber,
         r.SealNumber
    from InboundDocument id
   
 join Receipt          r on id.InboundDocumentId = r.InboundDocumentId
   where ReceiptId = @ReceiptId
end

 
