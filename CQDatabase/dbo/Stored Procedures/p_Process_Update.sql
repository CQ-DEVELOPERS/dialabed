﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Process_Update
  ///   Filename       : p_Process_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Nov 2013 12:48:38
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Process table.
  /// </remarks>
  /// <param>
  ///   @ProcessId int = null,
  ///   @ProcessCode nvarchar(60) = null,
  ///   @Process nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Process_Update
(
 @ProcessId int = null,
 @ProcessCode nvarchar(60) = null,
 @Process nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @ProcessId = '-1'
    set @ProcessId = null;
  
  if @ProcessCode = '-1'
    set @ProcessCode = null;
  
  if @Process = '-1'
    set @Process = null;
  
	 declare @Error int
 
  update Process
     set ProcessCode = isnull(@ProcessCode, ProcessCode),
         Process = isnull(@Process, Process) 
   where ProcessId = @ProcessId
  
  select @Error = @@Error
  
  
  return @Error
  
end
