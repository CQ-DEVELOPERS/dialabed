﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSKU_Update
  ///   Filename       : p_InterfaceImportSKU_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:05
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportSKU table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSKUId int = null,
  ///   @RecordStatus char(1) = null,
  ///   @SKUCode varchar(10) = null,
  ///   @SKU varchar(50) = null,
  ///   @UnitOfMeasure varchar(50) = null,
  ///   @Quantity float = null,
  ///   @AlternatePallet int = null,
  ///   @SubstitutePallet int = null,
  ///   @Litres numeric(13,3) = null,
  ///   @HostId varchar(30) = null,
  ///   @ProcessedDate datetime = null,
  ///   @Insertdate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSKU_Update
(
 @InterfaceImportSKUId int = null,
 @RecordStatus char(1) = null,
 @SKUCode varchar(10) = null,
 @SKU varchar(50) = null,
 @UnitOfMeasure varchar(50) = null,
 @Quantity float = null,
 @AlternatePallet int = null,
 @SubstitutePallet int = null,
 @Litres numeric(13,3) = null,
 @HostId varchar(30) = null,
 @ProcessedDate datetime = null,
 @Insertdate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportSKUId = '-1'
    set @InterfaceImportSKUId = null;
  
	 declare @Error int
 
  update InterfaceImportSKU
     set RecordStatus = isnull(@RecordStatus, RecordStatus),
         SKUCode = isnull(@SKUCode, SKUCode),
         SKU = isnull(@SKU, SKU),
         UnitOfMeasure = isnull(@UnitOfMeasure, UnitOfMeasure),
         Quantity = isnull(@Quantity, Quantity),
         AlternatePallet = isnull(@AlternatePallet, AlternatePallet),
         SubstitutePallet = isnull(@SubstitutePallet, SubstitutePallet),
         Litres = isnull(@Litres, Litres),
         HostId = isnull(@HostId, HostId),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         Insertdate = isnull(@Insertdate, Insertdate) 
   where InterfaceImportSKUId = @InterfaceImportSKUId
  
  select @Error = @@Error
  
  
  return @Error
  
end
