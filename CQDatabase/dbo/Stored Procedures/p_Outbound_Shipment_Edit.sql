﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Shipment_Edit
  ///   Filename       : p_Outbound_Shipment_Edit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Shipment_Edit
(
 @outboundShipmentId	 int,
 @locationId										int,
 @routeId				         int,
 @shipmentDate								datetime,
 @SealNumber										varchar(30) = null,
 @VehicleRegistration	varchar(10) = null,
 @Remarks				         nvarchar(500) = null,
 @DespatchBay			      int = null,
 @ContactListId			    int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Route             nvarchar(50),
          @IssueId           int
  
  declare @Issue as table
  (
   IssueId int
  )
  
  if @LocationId = -1
    set @LocationId = null
    
  if @DespatchBay = -1
    set @DespatchBay = null
    
  if @ContactListId = -1
    set @ContactListId = null
  
  if @RouteId = -1
    set @RouteId = null
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @routeId is null
  begin
    select top 1
           @RouteId = i.RouteId,
           @Route   = r.Route
      from OutboundShipmentIssue osi
      join Issue                   i on osi.IssueId = i.IssueId
      join Route                   r on i.RouteId   = r.RouteId
     where OutboundShipmentId = @outboundShipmentId
  end
  else
  begin
    select @Route = Route
      from Route (nolock)
     where RouteId = @routeId
  end
  
  begin transaction
  
  exec @Error = p_OutboundShipment_Update
   @outboundShipmentId  = @outboundShipmentId,
   @locationId          = @locationId,
   @shipmentDate        = @shipmentDate,
   @Route               = @Route,
   @RouteId             = @RouteId,
   @SealNumber		        = @SealNumber,
   @VehicleRegistration = @VehicleRegistration,
   @Remarks				         = @Remarks,
   @DespatchBay			      = @DespatchBay,
   @ContactListId		     = @ContactListId
  
  if @Error <> 0
    goto error
  
  insert @Issue
        (IssueId)
  select IssueId
    from OutboundShipmentIssue
   where OutboundShipmentId = @outboundShipmentId
  
  while exists(select top 1 1 from @Issue)
  begin
    select top 1 @IssueId = IssueId
      from @Issue
    
    delete @Issue where IssueId = @IssueId
    
    exec @Error = p_Issue_Update
     @IssueId				         = @IssueId,
     @LocationId			  				 = @LocationId,
     @RouteId				    				 = @RouteId,
     @SealNumber			       = @SealNumber,
     @VehicleRegistration	= @VehicleRegistration,
     @Remarks				         = @Remarks,
     @DespatchBay	  						= @DespatchBay,
     @ContactListId							= @ContactListId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Shipment_Edit'
    rollback transaction
    return @Error
end
