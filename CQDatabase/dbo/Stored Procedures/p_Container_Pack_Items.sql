﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Pack_Items
  ///   Filename       : p_Container_Pack_Items.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Pack_Items
(
 @warehouseId       int,
 @operatorId        int,
 @containerHeaderId int,
 @pickLocation      nvarchar(50),
 @barcode           nvarchar(50) = null,
 @batch             nvarchar(50) = null,
 @quantity          float = null
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           varchar(500) = 'Error executing p_Container_Pack_Items',
          @GetDate            datetime,
          @PickLocationId     int,
          @StoreLocationId    int,
          @StorageUnitId      int,
          @StorageUnitBatchId int,
          @ContainerDetailId  int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @batch = ''
    set @batch = null
  
  select @PickLocationId = PickLocationId
    from ContainerHeader (nolock)
   where ContainerHeaderId = @containerHeaderId
  
  select @pickLocationId = l.LocationId
    from Area a
    join AreaLocation al on a.AreaId      = al.AreaId
    join Location      l on al.LocationId = l.LocationId
   where a.WarehouseId = @warehouseId
     and l.Location    = replace(@pickLocation,'L:','')
  
  if @barcode is not null
    select @StorageUnitId = su.StorageUnitId
      from StorageUnit su (nolock)
      join Product      p (nolock) on su.ProductId = p.ProductId
                                  and (p.ProductCode = @Barcode
                                   or  p.Barcode     = @Barcode)
      left
      join Pack        pk (nolock) on su.StorageUnitId = pk.StorageUnitId
                                  and pk.WarehouseId = @WarehouseId
                                  and pk.Barcode    = @Barcode
  
  begin transaction
  
  if @pickLocationId is null or (@pickLocationId = @StoreLocationId)
  begin
    select @StoreLocationId as '@StoreLocationId', @PickLocationId as '@PickLocationId'
    set @Errormsg = 'Location Error'
    set @Error = -1
    goto error
  end
  
  if @StorageUnitId is null
  begin
    select @StorageUnitId as '@StorageUnitId'
    set @Errormsg = 'Product Error'
    set @Error = -1
    goto error
  end
  
  select @StorageUnitBatchId = sub.StorageUnitBatchId
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
    join SKU              sku (nolock) on su.SKUId = sku.SKUId
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where su.StorageUnitId = isnull(@StorageUnitId, su.StorageUnitId)
    and b.Batch           = isnull(@batch, b.Batch)
  
  exec @Error = p_ContainerHeader_Update
   @containerHeaderId = @containerHeaderId,
   @PickLocationId    = @PickLocationId,
   @StoreLocationId   = @StoreLocationId
  
  if @Error <> 0
    goto error
  
  select @ContainerDetailId = ContainerDetailId
    from ContainerDetail (nolock)
   where ContainerHeaderId = @ContainerHeaderId
     and StorageUnitBatchId = @StorageUnitBatchId
  
  if @ContainerDetailId is null
  begin
    exec @Error = p_ContainerDetail_Insert
     @ContainerHeaderId  = @ContainerHeaderId,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity
    
    if @Error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_ContainerDetail_Update
     @ContainerDetailId  = @ContainerDetailId,
     @Quantity           = @Quantity
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @errormsg
    rollback transaction
    return @Error
end
