﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SelectDatePeriod_byID
  ///   Filename       : p_SelectDatePeriod_byID.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 25 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SelectDatePeriod_byID
(
 @DatePeriodID int
)
 
as
begin
	 set nocount on;
	 
	 Declare @ReportSubscriptionDatePeriods As Table(

datePeriodID int,
DatePeriodDesc nvarchar(50),
DateFrom datetime,
DateTo datetime
)

Declare @DateHourNow Datetime
	Declare @DateHourAfter Datetime	

	Set @DateHourNow = getdate();
	Set @DateHourAfter = DateAdd(hour,-8,getdate())

	Insert into @ReportSubscriptionDatePeriods (datePeriodID,DatePeriodDesc,DateFrom,DateTo) values (1,'Last 8 Hours',@DateHourAfter,@DateHourNow)
	
	
	Declare @DateNow Datetime
	Declare @DateWeek Datetime	

	Set @DateNow = getdate();
	Set @DateWeek = DateAdd(day,-7,getdate())

	Insert into @ReportSubscriptionDatePeriods (datePeriodID,DatePeriodDesc,DateFrom,DateTo) values (2,'Last 7 Days',@DateWeek,@DateNow)
	
	Declare @DateMonth Datetime
	Set @DateMonth = DateAdd(day,-30,getdate())

	Insert into @ReportSubscriptionDatePeriods (datePeriodID,DatePeriodDesc,DateFrom,DateTo) values (3,'Last 30 Days',@DateMonth,@DateNow)

	Declare @DateFortnight Datetime
	Set @DateFortnight = DateAdd(day,-14,getdate())

	Insert into @ReportSubscriptionDatePeriods (datePeriodID,DatePeriodDesc,DateFrom,DateTo) values (4,'Last 14 Days',@DateFortnight,@DateNow)
	
	DECLARE @DateLastWeek datetime
	SET @DateLastWeek = DATEADD(dd,-(DATEPART(dw, getdate()) - 1),getdate()) --//First day of this week
	Set @DateLastWeek = DateAdd(day,-7,@dateLastWeek)							--//First day of last week
	
	Declare @DateLastWeek2 datetime
	Set @DateLastWeek2 = DateAdd(day,6,@dateLastWeek)

	Insert into @ReportSubscriptionDatePeriods (datePeriodID,DatePeriodDesc,DateFrom,DateTo) values (5,'Last Week',@DateLastWeek,@DateLastWeek2)
	
	Declare @lastMonth datetime
	set @lastMonth = DateAdd(Month,-1,getdate())
	Set @lastMonth = DateAdd(day,- Cast(day(getdate()) as integer),@lastMonth)
	Set @lastMonth = DateAdd(day,1,@lastMonth)
--	set @lastMonth = Cast('01' +  month(@lastMonth) + Year(@lastmonth)   as Datetime)

	Declare @DateLastMonth Datetime
	Set @DateLastMonth = DateAdd(day,- Cast(day(getdate()) as integer),getdate())

	Insert into @ReportSubscriptionDatePeriods (datePeriodID,DatePeriodDesc,DateFrom,DateTo) values (6,'Last Calendar Month',@lastMonth,@DateLastMonth)
	 
  if (isnull(@DatePeriodID,0) = 0 Or (@DatePeriodID = 0))
  Begin
    
    Select * from @ReportSubscriptionDatePeriods  
    
  end
  else
  Begin
  
  SELECT 
      DatePeriodDesc,
      DateFrom,
      DateTo
  FROM @ReportSubscriptionDatePeriods
   where DatePeriodID = @DatePeriodID
   
   end
end
