﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentHistory_Insert
  ///   Filename       : p_OutboundShipmentHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Oct 2013 09:31:07
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundShipmentHistory table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Route nvarchar(80) = null,
  ///   @RouteId int = null,
  ///   @Weight float = null,
  ///   @Volume decimal(18,0) = null,
  ///   @FP bit = null,
  ///   @SS bit = null,
  ///   @LP bit = null,
  ///   @FM bit = null,
  ///   @MP bit = null,
  ///   @PP bit = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @AlternatePallet int = null,
  ///   @SubstitutePallet int = null,
  ///   @DespatchBay int = null,
  ///   @NumberOfOrders int = null,
  ///   @ContactListId int = null,
  ///   @TotalOrders int = null,
  ///   @IDN bit = null,
  ///   @WaveId int = null,
  ///   @ReferenceNumber nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   OutboundShipmentHistory.OutboundShipmentId,
  ///   OutboundShipmentHistory.StatusId,
  ///   OutboundShipmentHistory.WarehouseId,
  ///   OutboundShipmentHistory.LocationId,
  ///   OutboundShipmentHistory.ShipmentDate,
  ///   OutboundShipmentHistory.Remarks,
  ///   OutboundShipmentHistory.SealNumber,
  ///   OutboundShipmentHistory.VehicleRegistration,
  ///   OutboundShipmentHistory.Route,
  ///   OutboundShipmentHistory.RouteId,
  ///   OutboundShipmentHistory.Weight,
  ///   OutboundShipmentHistory.Volume,
  ///   OutboundShipmentHistory.FP,
  ///   OutboundShipmentHistory.SS,
  ///   OutboundShipmentHistory.LP,
  ///   OutboundShipmentHistory.FM,
  ///   OutboundShipmentHistory.MP,
  ///   OutboundShipmentHistory.PP,
  ///   OutboundShipmentHistory.CommandType,
  ///   OutboundShipmentHistory.InsertDate,
  ///   OutboundShipmentHistory.AlternatePallet,
  ///   OutboundShipmentHistory.SubstitutePallet,
  ///   OutboundShipmentHistory.DespatchBay,
  ///   OutboundShipmentHistory.NumberOfOrders,
  ///   OutboundShipmentHistory.ContactListId,
  ///   OutboundShipmentHistory.TotalOrders,
  ///   OutboundShipmentHistory.IDN,
  ///   OutboundShipmentHistory.WaveId,
  ///   OutboundShipmentHistory.ReferenceNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentHistory_Insert
(
 @OutboundShipmentId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(500) = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Route nvarchar(80) = null,
 @RouteId int = null,
 @Weight float = null,
 @Volume decimal(18,0) = null,
 @FP bit = null,
 @SS bit = null,
 @LP bit = null,
 @FM bit = null,
 @MP bit = null,
 @PP bit = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @AlternatePallet int = null,
 @SubstitutePallet int = null,
 @DespatchBay int = null,
 @NumberOfOrders int = null,
 @ContactListId int = null,
 @TotalOrders int = null,
 @IDN bit = null,
 @WaveId int = null,
 @ReferenceNumber nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OutboundShipmentHistory
        (OutboundShipmentId,
         StatusId,
         WarehouseId,
         LocationId,
         ShipmentDate,
         Remarks,
         SealNumber,
         VehicleRegistration,
         Route,
         RouteId,
         Weight,
         Volume,
         FP,
         SS,
         LP,
         FM,
         MP,
         PP,
         CommandType,
         InsertDate,
         AlternatePallet,
         SubstitutePallet,
         DespatchBay,
         NumberOfOrders,
         ContactListId,
         TotalOrders,
         IDN,
         WaveId,
         ReferenceNumber)
  select @OutboundShipmentId,
         @StatusId,
         @WarehouseId,
         @LocationId,
         @ShipmentDate,
         @Remarks,
         @SealNumber,
         @VehicleRegistration,
         @Route,
         @RouteId,
         @Weight,
         @Volume,
         @FP,
         @SS,
         @LP,
         @FM,
         @MP,
         @PP,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @AlternatePallet,
         @SubstitutePallet,
         @DespatchBay,
         @NumberOfOrders,
         @ContactListId,
         @TotalOrders,
         @IDN,
         @WaveId,
         @ReferenceNumber 
  
  select @Error = @@Error
  
  
  return @Error
  
end
