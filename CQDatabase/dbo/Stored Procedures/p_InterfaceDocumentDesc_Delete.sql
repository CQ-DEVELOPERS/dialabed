﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentDesc_Delete
  ///   Filename       : p_InterfaceDocumentDesc_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:34:59
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceDocumentDesc table.
  /// </remarks>
  /// <param>
  ///   @DocId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentDesc_Delete
(
 @DocId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceDocumentDesc
     where DocId = @DocId
  
  select @Error = @@Error
  
  
  return @Error
  
end
