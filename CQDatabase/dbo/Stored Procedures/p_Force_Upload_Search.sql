﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Force_Upload_Search
  ///   Filename       : p_Force_Upload_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Force_Upload_Search
(
 @WarehouseId int = 1,
 @OrderNumber varchar(30) = null
)
 
as
begin
	 set nocount on;
	 
	 if @OrderNumber is null
	   set @OrderNumber = ''
  
  select InterfaceExportHeaderId,
         RecordStatus,
         RecordType,
         OrderNumber,
         CompanyCode,
         Company,
         FromWarehouseCode,
         ToWarehouseCode,
         InsertDate
    from InterfaceExportHeader (nolock)
   where RecordStatus = 'N'
     and RecordType in ('BOT','SIS')
     and InsertDate > dateadd(hh, -6, getdate())
     and OrderNumber like '%' + @OrderNumber + '%'
end
