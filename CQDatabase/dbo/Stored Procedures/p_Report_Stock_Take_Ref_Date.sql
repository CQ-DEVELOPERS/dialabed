﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_Ref_Date
  ///   Filename       : p_Report_Stock_Take_Ref_Date.sql
  ///   Create By      : Karen
  ///   Date Created   : 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Report_Stock_Take_Ref_Date]
(
 @StockTakeReferenceId		int,
 @WarehouseId				int,
 @FromDate					datetime,
 @ToDate					datetime,
 @AreaId					Int
)
as
begin
	 set nocount on; 

if (@AreaId = 0) or (@AreaId = -1)
Begin
 set @AreaId = null
end
	
Declare @StockTakeSummary as  Table 
		(StockTakeReferenceId	int,
		 InstructionType			Varchar(50),
		 InstructionTypeCode		Varchar(25),
		 Area						Varchar(50),
		 AreaId						int,
	 	 Date						Datetime,
		 LocationCount				Int,
		 StockTakes					Int,
		 Cancelled					Int,		
		 Waiting					int,
		 Busy						int,
		 Complete					float,		 
		 ActualStockTakes			int,
		 StockTakesPerRef			int,
		 StockTakeComplete			float)

If @AreaId = -1 Set @AreaId = null

If @StockTakeReferenceId = -1 Set @StockTakeReferenceId = null

Declare @AreaLocationCount as Table  
	(StockTakeReferenceId	int,
	 InstructionTypeCode	varchar(10) NOT NULL,
	 AreaId					int NOT NULL,
	 LocationCount			int NULL) 

Insert into @AreaLocationCount 
		   (InstructionTypeCode,
			AreaId,
			LocationCount)
Select		'STL',
			al.AreaId,
			Count(LocationId)  as LocationCount
from AreaLocation al
	Join Area a on a.AreaId = al.AreaId
	--join view_Instruction_Stock_Ref isr on isr.Areaid = al.AreaId and isr.InstructionTypeCode = 'STL'
	where Isnull(@AreaId,al.AreaId) = al.AreaId
		and a.WarehouseId = @WarehouseId
		--and isr.StockTakeReferenceId = ISNULL(@StockTakeReferenceId, isr.StockTakeReferenceId)
		--and isr.InstructionTypeCode = 'STL'
Group by al.AreaId



Insert into @AreaLocationCount 
		   (InstructionTypeCode,
			AreaId,
			LocationCount)

Select		'STE',
			al.AreaId,
			Count(LocationId)  as LocationCount
from AreaLocation al
Join Area a on a.AreaId = al.AreaId
--join view_Instruction_Stock_Ref isr on isr.Areaid = al.AreaId
where Isnull(@AreaId,al.AreaId) = al.AreaId
	and a.WarehouseId = @WarehouseId
	--and isr.StockTakeReferenceId = ISNULL(@StockTakeReferenceId, isr.StockTakeReferenceId)
	--and isr.InstructionTypeCode = 'STE'
Group by al.AreaId
		 

Declare @TempDate Datetime

Set @TempDate = @FromDate

While (@TempDate <= @ToDate)
Begin

Insert into @StockTakeSummary

Select Distinct 
		isnull(@StockTakeReferenceId,0),
		i.InstructionType,
		i.InstructionTypeCode,
		Area,
		c.AreaId,
		@TempDate,
		LocationCount,
		0 as StockTakes,
		0 as Cancelled,
		0 as Waiting,
		0 as Busy,
		0 as Complete,
		0 as ActualStockTakes,
		0 as StockTakesPerRef,
		0 as StockTakeComplete
From 	@AreaLocationCount c
		Join InstructionType i on i.InstructionTypeCode = c.InstructionTypeCode		
		Join Area a on a.Areaid = c.AreaId
	--where c.StockTakeReferenceId = ISNULL(@StockTakeReferenceId, c.StockTakeReferenceId)
	
	Set  @TempDate = Dateadd(dd,1,@TempDate)
End

Select  vs.InstructionType,
		vs.InstructionTypeCode,
		vs.Area,
		vs.Areaid,
		vs.Status,
		vs.CreateDate,
		Count(PickLocationId) as Cnt,
		vs.StockTakeReferenceId
Into #Instruction_Stock
from view_Instruction_Stock_Ref vs
		jOIN @StockTakeSummary s 
				on s.Date = vs.CreateDate 
				and s.InstructionType = vs.InstructionType 
				and s.Area= vs.Area 
				and vs.StockTakeReferenceId = isnull(s.StockTakeReferenceId, vs.StockTakeReferenceId)
		--join StockTakeReferenceJob strj (nolock) on strj.StockTakeReferenceId = @StockTakeReferenceId
where vs.StockTakeReferenceId = isnull(@StockTakeReferenceId, vs.StockTakeReferenceId)
Group by vs.InstructionType,
		vs.InstructionTypeCode,
		vs.Area,
		vs.Areaid,
		vs.Status,
		vs.CreateDate,
		vs.StockTakeReferenceId

Update @StockTakeSummary Set StockTakes = Cnt 
From @StockTakeSummary s
	Join #Instruction_Stock vs
		on vs.InstructionType = s.InstructionType and
			vs.AreaId = s.AreaId and
			vs.CreateDate = s.Date and
			vs.InstructionTypeCode = s.InstructionTypeCode
Where vs.Status = 'Finished'


Update @StockTakeSummary Set Waiting = Cnt 
From @StockTakeSummary s
	Join #Instruction_Stock vs
		on vs.InstructionType = s.InstructionType and
			vs.AreaId = s.AreaId and
			vs.CreateDate = s.Date and
			vs.InstructionTypeCode = s.InstructionTypeCode
Where vs.Status = 'Waiting'

Update @StockTakeSummary Set Cancelled = Cnt 
From @StockTakeSummary s
	Join #Instruction_Stock vs
		on vs.InstructionType = s.InstructionType and
			vs.AreaId = s.AreaId and
			vs.CreateDate = s.Date and
			vs.InstructionTypeCode = s.InstructionTypeCode
Where vs.Status = 'Cancelled'

Update @StockTakeSummary Set Busy = Cnt 
From @StockTakeSummary s
	Join #Instruction_Stock vs
		on vs.InstructionType = s.InstructionType and
			vs.AreaId = s.AreaId and
			vs.CreateDate = s.Date and
			vs.InstructionTypeCode = s.InstructionTypeCode
Where vs.Status = 'Started'

Update @StockTakeSummary 
set ActualStockTakes = (select COUNT(distinct vs.picklocationid)
						from view_Instruction_Stock_Ref vs
						where vs.StockTakeReferenceId = @StockTakeReferenceId
						and vs.Areaid = s.AreaId
						and status = 'Finished'
						and StockTakes > 0
						and s.Date = vs.CreateDate
						and s.InstructionType = vs.InstructionType)
from @StockTakeSummary s 

Update @StockTakeSummary
Set StockTakesPerRef = (select COUNT(distinct vs.picklocationid)
						from view_Instruction_Stock_Ref vs
						where vs.StockTakeReferenceId = @StockTakeReferenceId
						and vs.Areaid = s.AreaId
						and status = 'Finished'
						and StockTakes > 0
						and s.InstructionType = vs.InstructionType)
from @StockTakeSummary s

SET ARITHABORT OFF
SET ANSI_WARNINGS OFF

Update @StockTakeSummary set Complete = Round(Convert(Float,ActualStockTakes) * 100 / Convert(Float,LocationCount),2)
Update @StockTakeSummary set StockTakeComplete = Round(Convert(Float,StockTakesPerRef) * 100 / Convert(Float,LocationCount),2)

Select * 
from @StockTakeSummary
where StockTakes > 0

end
