﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_List
  ///   Filename       : p_Status_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:55
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Status table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Status.StatusId,
  ///   Status.Status 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as StatusId
        ,'{All}' as Status
  union
  select
         Status.StatusId
        ,Status.Status
    from Status
  
end
