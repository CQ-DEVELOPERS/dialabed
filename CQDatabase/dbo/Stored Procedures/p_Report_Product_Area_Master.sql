﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Area_Master
  ///   Filename       : p_Report_Product_Area_Master.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter
  ///   Modified Date  : 2018/03/19
  ///   Details        : CQPMO-2252 Product Area Master report not working.
  /// </newpara>
*/
CREATE procedure p_Report_Product_Area_Master
(
 @PrincipalId int,
 @AreaId      int,
 @Linked      nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  if @PrincipalId = -1
    set @PrincipalId = null
  
  if @AreaId = -1
    set @AreaId = null
  
  if lower(@Linked) = 'true'
    select pn.PrincipalCode,
           p.ProductCode, 
           sku.SKUCode,
           a.Area,
           case when sua.AreaId is null
                then 'False'
                else 'True'
                end as 'Linked',
           sua.StoreOrder,
           sua.PickOrder	,
           su.MinimumQuantity,
           su.ReorderQuantity,
           su.MaximumQuantity
      from StorageUnit su (nolock)
      join Product      p (nolock) on su.ProductId  = p.ProductId
      LEFT JOIN Principal   pn (nolock) on p.PrincipalId = pn.PrincipalId --CQPMO-2252 Added Left join for non principal enabled customers
                                  and isnull(pn.PrincipalId,-1) = isnull(@PrincipalId, isnull(pn.PrincipalId,-1))
      join SKU        sku (nolock) on su.SKUId      = sku.SKUId
      join StorageUnitArea sua (nolock) on sua.StorageUnitId = su.StorageUnitId
      join Area              a (nolock) on sua.AreaId        = a.AreaId
	  WHERE a.AreaId = @AreaId --CQPMO-2252 Was not making use of the area selected at all.
  else
    select pn.PrincipalCode,
           p.ProductCode, 
           sku.SKUCode,
           a.Area,
           case when sua.AreaId is null
                then 'False'
                else 'True'
                end as 'Linked',
           1,
           1,
           su.MinimumQuantity,
           su.ReorderQuantity,
           su.MaximumQuantity
      from Area              a (nolock)
      join StorageUnit su (nolock) on 1= 1
      join Product      p (nolock) on su.ProductId  = p.ProductId
      LEFT JOIN Principal   pn (nolock) on p.PrincipalId = pn.PrincipalId --CQPMO-2252 Added Left join for non principal enabled customers
                                  and isnull(pn.PrincipalId,-1) = isnull(@PrincipalId, isnull(pn.PrincipalId,-1))
      join SKU        sku (nolock) on su.SKUId      = sku.SKUId
      left
      join StorageUnitArea sua (nolock) on sua.StorageUnitId = su.StorageUnitId
                                       and sua.AreaId = a.AreaId
     where sua.AreaId is NULL
	       AND a.AreaId = @AreaId --CQPMO-2252 Was not making use of the area selected at all.
END
