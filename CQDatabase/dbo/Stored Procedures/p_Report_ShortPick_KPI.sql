﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_ShortPick_KPI
  ///   Filename       : p_Report_ShortPick_KPI.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 12 Nov 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Junaid Desai	
  ///   Modified Date  : 13 Oct 2008
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_ShortPick_KPI
(
		@FromDate			datetime,
		@ToDate				datetime,
		@OperatorGroupId		int
)
 
as
begin
	 set nocount on;
	

Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int

Set @DateHoursDiff = DateDiff(hh,@FromDate,@ToDate)
Set @DateDaysDiff = DateDiff(D,@FromDate,@ToDate)
Set @DateMonthsDiff = DateDiff(M,@FromDate,@ToDate) 


if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime

							SELECT     OSP.OperatorId,
							O.Operator,
							O.OperatorGroupId,
							Convert(nvarchar,OSP.EndDate,108) as EndDate, 
							Convert(nvarchar,OSP.EndDate,108) as ordscale,
							OSP.InstructionTypeId,
							Sum(OP.Units) as TotalUnits,
							Sum(OSP.Units) as UnitsPicked, 
							Sum(OSP.UnitsShort) as ShortUnits,
							Sum(OP.Weight) as TotalWeight,
							Sum(OSP.Weight) as WeightPicked, 
							SUM(isnull(OSP.WeightShort,0)) as WeightShort, 
							SUM(OP.Instructions) as Instructions,  
							SUM(OSP.Instructions) as ShortInstructions, 
							SUM(OP.Orders) as Orders,	
							SUM(OSP.Orders) as ShortOrders, 
							SUM(OP.OrderLines) as OrderLines,	
							SUM(OSP.OrderLines) AS ShortOrderLines, 
							SUM(OP.Jobs) as Jobs,
							SUM(OSP.Jobs) AS ShortJobs,
							(SUM(Distinct(KT.KPITargetValue))) as ShortTarget
							FROM         OperatorShortPickPerformance AS OSP INNER JOIN
												  OperatorPerformance AS OP ON OSP.OperatorId = OP.OperatorId And OSp.EndDate = OP.EndDate and OSP.InstructionTypeId = Op.InstructionTypeId
										 join Operator O on O.OperatorId = OSP.OperatorId
											 join KPITarget KT on O.OperatorGroupId = KT.OperatorGroupID	
							where O.OperatorGroupId = @OperatorGroupId
							and KT.KPITargetCode = 'SP'	
							--and OSP.OperatorId = 19
							and OSP.Enddate between @FromDate and @ToDate
							Group By OSP.OperatorId,O.Operator,O.OperatorGroupId,Convert(nvarchar,OSP.EndDate,108),OSP.InstructionTypeId
							ORDER BY ordscale 

			-- Select data by this Scale
					End
				Else
					Begin
					--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
							SELECT     OSP.OperatorId,
							O.Operator,
							O.OperatorGroupId,
							Convert(nvarchar,OSP.EndDate,101) as EndDate, 
							Convert(nvarchar,OSP.EndDate,101) as ordscale,
							OSP.InstructionTypeId,
							Sum(OP.Units) as TotalUnits,
							Sum(OSP.Units) as UnitsPicked, 
							Sum(OSP.UnitsShort) as ShortUnits,
							Sum(OP.Weight) as TotalWeight,
							Sum(OSP.Weight) as WeightPicked, 
							SUM(isnull(OSP.WeightShort,0)) as WeightShort, 
							SUM(OP.Instructions) as Instructions,  
							SUM(OSP.Instructions) as ShortInstructions, 
							SUM(OP.Orders) as Orders,	
							SUM(OSP.Orders) as ShortOrders, 
							SUM(OP.OrderLines) as OrderLines,	
							SUM(OSP.OrderLines) AS ShortOrderLines, 
							SUM(OP.Jobs) as Jobs,
							SUM(OSP.Jobs) AS ShortJobs,
							8 * (SUM(Distinct(KT.KPITargetValue))) as ShortTarget
							FROM         OperatorShortPickPerformance AS OSP INNER JOIN
												  OperatorPerformance AS OP ON OSP.OperatorId = OP.OperatorId And OSp.EndDate = OP.EndDate and OSP.InstructionTypeId = Op.InstructionTypeId
										 join Operator O on O.OperatorId = OSP.OperatorId
										 join KPITarget KT on O.OperatorGroupId = KT.OperatorGroupID	
							where O.OperatorGroupId = @OperatorGroupId
							and KT.KPITargetCode = 'SP'
							--and OSP.OperatorId = 19
							and OSP.Enddate between @FromDate and @ToDate
							Group By OSP.OperatorId,O.Operator,O.OperatorGroupId,Convert(nvarchar,OSP.EndDate,101),OSP.InstructionTypeId
							ORDER BY ordscale 

					End
			End
		Else
				Begin
							SELECT     OSP.OperatorId,
							O.Operator,
							O.OperatorGroupId,
							DatePart(wk,OSP.EndDate) as EndDate, 
							DatePart(wk,OSP.EndDate) as ordscale,
							OSP.InstructionTypeId,
							Sum(OP.Units) as TotalUnits,
							Sum(OSP.Units) as UnitsPicked, 
							Sum(OSP.UnitsShort) as ShortUnits,
							Sum(OP.Weight) as TotalWeight,
							Sum(OSP.Weight) as WeightPicked, 
							SUM(isnull(OSP.WeightShort,0)) as WeightShort,
							SUM(OP.Instructions) as Instructions,  
							SUM(OSP.Instructions) as ShortInstructions, 
							SUM(OP.Orders) as Orders,	
							SUM(OSP.Orders) as ShortOrders, 
							SUM(OP.OrderLines) as OrderLines,	
							SUM(OSP.OrderLines) AS ShortOrderLines, 
							SUM(OP.Jobs) as Jobs,
							SUM(OSP.Jobs) AS ShortJobs,
							5 * 8 * (SUM(Distinct(KT.KPITargetValue))) as ShortTarget

							FROM         OperatorShortPickPerformance AS OSP INNER JOIN
												  OperatorPerformance AS OP ON OSP.OperatorId = OP.OperatorId And OSp.EndDate = OP.EndDate and OSP.InstructionTypeId = Op.InstructionTypeId
											join Operator O					on O.OperatorId = OSP.OperatorId	
											join KPITarget KT				on O.OperatorGroupId = KT.OperatorGroupID	
							where O.OperatorGroupId = @OperatorGroupId
							and KT.KPITargetCode = 'SP'
							--and OSP.OperatorId = 19
							and OSP.Enddate between @FromDate and @ToDate
							Group By OSP.OperatorId,O.Operator,O.OperatorGroupId,DatePart(wk,OSP.EndDate),OSP.InstructionTypeId
							ORDER BY ordscale 

			
			End
	End
Else
	Begin

							SELECT     OSP.OperatorId,
							O.Operator,
							O.OperatorGroupId,
							DateName(M,OSP.EndDate) as EndDate, 
							DatePart(M,OSP.EndDate) as ordscale,
							OSP.InstructionTypeId,
							Sum(OP.Units) as TotalUnits,
							Sum(OSP.Units) as UnitsPicked, 
							Sum(OSP.UnitsShort) as ShortUnits,
							Sum(OP.Weight) as TotalWeight,
							Sum(OSP.Weight) as WeightPicked, 
							SUM(isnull(OSP.WeightShort,0)) as WeightShort, 
							SUM(OP.Instructions) as Instructions,  
							SUM(OSP.Instructions) as ShortInstructions, 
							SUM(OP.Orders) as Orders,	
							SUM(OSP.Orders) as ShortOrders, 
							SUM(OP.OrderLines) as OrderLines,	
							SUM(OSP.OrderLines) AS ShortOrderLines, 
							SUM(OP.Jobs) as Jobs,
							SUM(OSP.Jobs) AS ShortJobs,
							22 * 8 * (SUM(Distinct(KT.KPITargetValue))) as ShortTarget
							FROM         OperatorShortPickPerformance AS OSP INNER JOIN
												  OperatorPerformance AS OP ON OSP.OperatorId = OP.OperatorId And OSp.EndDate = OP.EndDate and OSP.InstructionTypeId = Op.InstructionTypeId
										 join Operator O on O.OperatorId = OSP.OperatorId
										join KPITarget KT				on O.OperatorGroupId = KT.OperatorGroupID	
							where O.OperatorGroupId = @OperatorGroupId
							and KT.KPITargetCode = 'SP'
							--and OSP.OperatorId = 19
							and OSP.Enddate between @FromDate and @ToDate
							Group By OSP.OperatorId,O.Operator,O.OperatorGroupId,DateName(M,OSP.EndDate),DatePart(M,OSP.EndDate),OSP.InstructionTypeId
							ORDER BY ordscale 
				
	End

--Select @startDate,@enddate
end
