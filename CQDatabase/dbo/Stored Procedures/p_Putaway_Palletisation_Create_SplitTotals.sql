﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Putaway_Palletisation_Create_SplitTotals
  ///   Filename       : p_Putaway_Palletisation_Create_SplitTotals.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Putaway_Palletisation_Create_SplitTotals
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
  
  declare @SplitQuantity float,
          @OrderQuantity float,
          @Difference    float;
  
  select @SplitQuantity = sum(i.Quantity)
    from Instruction         i   (nolock)
    join Status              s   (nolock) on i.StatusId = s.StatusId
   where i.InstructionRefId = @InstructionId
     and s.Type             = 'I'
     and s.StatusCode      in ('W','A','S');
  
  select @OrderQuantity = sum(i.Quantity)
    from Instruction         i   (nolock)
    join Status              s   (nolock) on i.StatusId = s.StatusId
   where i.InstructionId    = @InstructionId
     and s.Type             = 'I'
     and s.StatusCode       = 'D';
  
  set @Difference = @OrderQuantity - @SplitQuantity
  
  select @OrderQuantity as 'OrderQuantity',
         @SplitQuantity as 'SplitQuantity',
         @Difference    as 'Difference'
end
