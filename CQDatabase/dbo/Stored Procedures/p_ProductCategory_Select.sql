﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCategory_Select
  ///   Filename       : p_ProductCategory_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:00
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ProductCategory table.
  /// </remarks>
  /// <param>
  ///   @ProductCategoryId int = null 
  /// </param>
  /// <returns>
  ///   ProductCategory.ProductCategoryId,
  ///   ProductCategory.ProductCategoryType,
  ///   ProductCategory.ProductType,
  ///   ProductCategory.ProductCategory,
  ///   ProductCategory.PackingCategory,
  ///   ProductCategory.StackingCategory,
  ///   ProductCategory.MovementCategory,
  ///   ProductCategory.ValueCategory,
  ///   ProductCategory.StoringCategory 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCategory_Select
(
 @ProductCategoryId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ProductCategory.ProductCategoryId
        ,ProductCategory.ProductCategoryType
        ,ProductCategory.ProductType
        ,ProductCategory.ProductCategory
        ,ProductCategory.PackingCategory
        ,ProductCategory.StackingCategory
        ,ProductCategory.MovementCategory
        ,ProductCategory.ValueCategory
        ,ProductCategory.StoringCategory
    from ProductCategory
   where isnull(ProductCategory.ProductCategoryId,'0')  = isnull(@ProductCategoryId, isnull(ProductCategory.ProductCategoryId,'0'))
  
end
