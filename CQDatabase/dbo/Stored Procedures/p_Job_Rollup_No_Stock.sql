﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Rollup_No_Stock
  ///   Filename       : p_Job_Rollup_No_Stock.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Rollup_No_Stock
(
 @jobId int = null
)
 
as
begin
	 set nocount on;
  
  declare @TableJobs as table
  (
   JobId int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @Count             int
  
  if @JobId is null
  begin
    insert @TableJobs
          (JobId)
    select distinct j.JobId
      from IssueLineInstruction ili (nolock)
      join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
      join Status              inss (nolock) on ins.StatusId      = inss.StatusId
      join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
      join Status               ils (nolock) on il.StatusId     = ils.StatusId
      join Job                    j (nolock) on ins.JobId       = j.JobId
      join Status                js (nolock) on j.StatusId      = js.StatusId
     where js.StatusCode in ('NS','S')
       and ils.StatusCode = 'RL'
       and not exists(select 1 from Instruction ins2 join Status s on ins2.StatusId = s.StatusId and s.StatusCode in ('W','S') where ins.JobId = ins2.JobId)
    
    select @Count = @@rowcount
    
    if @Count = 0
      return
  end
  
  begin transaction
  
  while @Count > 0
  begin
    select @Count = @Count - 1
    
    select @JobId = min(JobId) from @TableJobs
    
    delete @TableJobs where JobId = @JobId
    
    update Job set StatusId = dbo.ufn_StatusId('IS','CK') where JobId = @JobId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    exec @Error = p_status_rollup @JobId
    
    if @Error <> 0
      goto error
    
    update Job set StatusId = dbo.ufn_StatusId('I','NS') where JobId = @JobId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    select j.JobId, s.Status
      from Job    j
      join Status s on j.StatusId = s.StatusId
     where j.JobId = @JobId
    
    if @Error <> 0
      goto error
  end
  
  update j
     set StatusId = dbo.ufn_StatusId('IS','CK')
    from Instruction i
    join InstructionType it on i.InstructionTypeId= it.InstructionTypeId
                           and it.InstructionTypeCode in ('P','PM','PS','FM')
    join Job         j on i.JobId = j.JobId
    join Status      s on j.StatusId = s.StatusId
   where s.StatusCode in ('NS','S')
    and exists(select 1 from Instruction i2 join Status s2 on i2.StatusId = s2.StatusId  and s2.StatusCode = 'F' where i.JobId = i2.JobId and i2.ConfirmedQuantity > 0)
    and not exists(select 1 from Instruction i2 join Status s2 on i2.StatusId = s2.StatusId  and s2.StatusCode in ('S','W') where i.JobId = i2.JobId)
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Job_Rollup_No_Stock'
    rollback transaction
    return @Error
end
