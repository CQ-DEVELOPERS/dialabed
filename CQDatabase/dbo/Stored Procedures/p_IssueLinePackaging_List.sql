﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLinePackaging_List
  ///   Filename       : p_IssueLinePackaging_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:55
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IssueLinePackaging table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   IssueLinePackaging.PackageLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLinePackaging_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as PackageLineId
        ,null as 'IssueLinePackaging'
  union
  select
         IssueLinePackaging.PackageLineId
        ,IssueLinePackaging.PackageLineId as 'IssueLinePackaging'
    from IssueLinePackaging
  
end
