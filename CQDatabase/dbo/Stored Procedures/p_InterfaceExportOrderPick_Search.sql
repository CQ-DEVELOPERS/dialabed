﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportOrderPick_Search
  ///   Filename       : p_InterfaceExportOrderPick_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:04
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceExportOrderPick table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportOrderPickId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceExportOrderPick.InterfaceExportOrderPickId,
  ///   InterfaceExportOrderPick.IssueId,
  ///   InterfaceExportOrderPick.OrderNumber,
  ///   InterfaceExportOrderPick.RecordStatus,
  ///   InterfaceExportOrderPick.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportOrderPick_Search
(
 @InterfaceExportOrderPickId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceExportOrderPickId = '-1'
    set @InterfaceExportOrderPickId = null;
  
 
  select
         InterfaceExportOrderPick.InterfaceExportOrderPickId
        ,InterfaceExportOrderPick.IssueId
        ,InterfaceExportOrderPick.OrderNumber
        ,InterfaceExportOrderPick.RecordStatus
        ,InterfaceExportOrderPick.ProcessedDate
    from InterfaceExportOrderPick
   where isnull(InterfaceExportOrderPick.InterfaceExportOrderPickId,'0')  = isnull(@InterfaceExportOrderPickId, isnull(InterfaceExportOrderPick.InterfaceExportOrderPickId,'0'))
  
end
