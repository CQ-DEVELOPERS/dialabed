﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOM_Select
  ///   Filename       : p_InterfaceBOM_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:47
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceBOM table.
  /// </remarks>
  /// <param>
  ///   @InterfaceBOMId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceBOM.InterfaceBOMId,
  ///   InterfaceBOM.ParentProductCode,
  ///   InterfaceBOM.OrderDescription,
  ///   InterfaceBOM.CreateDate,
  ///   InterfaceBOM.PlannedDate,
  ///   InterfaceBOM.Revision,
  ///   InterfaceBOM.BuildQuantity,
  ///   InterfaceBOM.Units,
  ///   InterfaceBOM.Location,
  ///   InterfaceBOM.ProcessedDate,
  ///   InterfaceBOM.RecordStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOM_Select
(
 @InterfaceBOMId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceBOM.InterfaceBOMId
        ,InterfaceBOM.ParentProductCode
        ,InterfaceBOM.OrderDescription
        ,InterfaceBOM.CreateDate
        ,InterfaceBOM.PlannedDate
        ,InterfaceBOM.Revision
        ,InterfaceBOM.BuildQuantity
        ,InterfaceBOM.Units
        ,InterfaceBOM.Location
        ,InterfaceBOM.ProcessedDate
        ,InterfaceBOM.RecordStatus
    from InterfaceBOM
   where isnull(InterfaceBOM.InterfaceBOMId,'0')  = isnull(@InterfaceBOMId, isnull(InterfaceBOM.InterfaceBOMId,'0'))
  
end
