﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnit_Select
  ///   Filename       : p_StorageUnit_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:02:13
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnit table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null 
  /// </param>
  /// <returns>
  ///   StorageUnit.StorageUnitId,
  ///   StorageUnit.SKUId,
  ///   StorageUnit.ProductId,
  ///   StorageUnit.ProductCategory,
  ///   StorageUnit.PackingCategory,
  ///   StorageUnit.PickEmpty,
  ///   StorageUnit.StackingCategory,
  ///   StorageUnit.MovementCategory,
  ///   StorageUnit.ValueCategory,
  ///   StorageUnit.StoringCategory,
  ///   StorageUnit.PickPartPallet,
  ///   StorageUnit.MinimumQuantity,
  ///   StorageUnit.ReorderQuantity,
  ///   StorageUnit.MaximumQuantity,
  ///   StorageUnit.DefaultQC,
  ///   StorageUnit.UnitPrice,
  ///   StorageUnit.Size,
  ///   StorageUnit.Colour,
  ///   StorageUnit.Style,
  ///   StorageUnit.PreviousUnitPrice,
  ///   StorageUnit.StockTakeCounts,
  ///   StorageUnit.SerialTracked,
  ///   StorageUnit.SizeCode,
  ///   StorageUnit.ColourCode,
  ///   StorageUnit.StyleCode,
  ///   StorageUnit.StatusId,
  ///   StorageUnit.PutawayRule,
  ///   StorageUnit.AllocationRule,
  ///   StorageUnit.BillingRule,
  ///   StorageUnit.CycleCountRule,
  ///   StorageUnit.LotAttributeRule,
  ///   StorageUnit.StorageCondition,
  ///   StorageUnit.ManualCost,
  ///   StorageUnit.OffSet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnit_Select
(
 @StorageUnitId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         StorageUnit.StorageUnitId
        ,StorageUnit.SKUId
        ,StorageUnit.ProductId
        ,StorageUnit.ProductCategory
        ,StorageUnit.PackingCategory
        ,StorageUnit.PickEmpty
        ,StorageUnit.StackingCategory
        ,StorageUnit.MovementCategory
        ,StorageUnit.ValueCategory
        ,StorageUnit.StoringCategory
        ,StorageUnit.PickPartPallet
        ,StorageUnit.MinimumQuantity
        ,StorageUnit.ReorderQuantity
        ,StorageUnit.MaximumQuantity
        ,StorageUnit.DefaultQC
        ,StorageUnit.UnitPrice
        ,StorageUnit.Size
        ,StorageUnit.Colour
        ,StorageUnit.Style
        ,StorageUnit.PreviousUnitPrice
        ,StorageUnit.StockTakeCounts
        ,StorageUnit.SerialTracked
        ,StorageUnit.SizeCode
        ,StorageUnit.ColourCode
        ,StorageUnit.StyleCode
        ,StorageUnit.StatusId
        ,StorageUnit.PutawayRule
        ,StorageUnit.AllocationRule
        ,StorageUnit.BillingRule
        ,StorageUnit.CycleCountRule
        ,StorageUnit.LotAttributeRule
        ,StorageUnit.StorageCondition
        ,StorageUnit.ManualCost
        ,StorageUnit.OffSet
    from StorageUnit
   where isnull(StorageUnit.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(StorageUnit.StorageUnitId,'0'))
  order by ProductCategory
  
end
