﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SOH_Allocated_Reserved_Quantity_Fix
  ///   Filename       : p_SOH_Allocated_Reserved_Quantity_Fix.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Jun 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SOH_Allocated_Reserved_Quantity_Fix
(
 @StartDate datetime = null
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_SOH_Allocated_Reserved_Quantity_Fix',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @Id                int,
          @Command           nvarchar(max)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @StartDate is null
    set @StartDate = DATEADD(m, -1, @getdate)
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  -- Save Current StorageUnitBatchLocation
  /*
  -- drop table StorageUnitBatchLocationDropme

  select *
    into StorageUnitBatchLocationDropme
    from StorageUnitBatchLocation
  */
  
  declare @TableResult as table
  (
   Id      int identity,
   Command nvarchar(max)
  )
  
  -- Deallocate Replenishments which have been deleted
  insert @TableResult
        (Command)
  select 'exec p_StorageUnitBatchLocation_Deallocate @InstructionId=' + convert(varchar(10), InstructionId) 
    from viewInstruction
   where InstructionCode = 'Z'
     and (isnull(Stored,-1) = 0 or isnull(Picked,-1) = 0)

  -- Deallocate Picks without IssueLineInstruction
  insert @TableResult
        (Command)
  select 'exec p_StorageUnitBatchLocation_Deallocate @InstructionId=' + convert(varchar(10), InstructionId)
    from viewInstruction vi
   where (isnull(Stored,-1) = 0 or isnull(Picked,-1) = 0)
     and InstructionCode in ('W','S') and InstructionTypeCode in ('P','PM','PS','FM')
     and not exists(select 1 from IssueLineInstruction ili where ili.InstructionId = isnull(vi.InstructionRefId, vi.InstructionId))

  -- Deallocate invalid Stock Takes
  insert @TableResult
        (Command)
  select 'exec p_StorageUnitBatchLocation_Deallocate @InstructionId=' + convert(varchar(10), InstructionId)
    from viewInstruction vi
   where (isnull(Stored,-1) = 0 or isnull(Picked,-1) = 0)
     and InstructionTypeCode like 'ST%'
     and InstructionCode in ('W','S')
     and JobCode = 'F'
  
  select *
    from @TableResult
  
  while exists(select top 1 1 from @TableResult)
  begin
    select @Id      = Id,
           @Command = Command
      from @TableResult
    
    exec (@Command)
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
    
    delete @TableResult where Id = @Id
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
  end
  
  -- Remove Minuses
  -- select * from StorageUnitBatchLocation where AllocatedQuantity < 0 or ReservedQuantity < 0
  update StorageUnitBatchLocation set AllocatedQuantity = 0 where AllocatedQuantity < 0
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  update StorageUnitBatchLocation set ReservedQuantity = 0 where ReservedQuantity < 0
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error

  -- Update invalid Stock Take to Complete
  update i
     set StatusId = dbo.ufn_StatusId('I','F')
    from viewInstruction vi
    join Instruction      i (nolock) on vi.InstructionId = i.InstructionId
   where (isnull(vi.Stored,0) = 0 or isnull(vi.Picked,0) = 0)
     and vi.InstructionTypeCode like 'ST%'
     and vi.InstructionCode in ('W','S')
     and vi.JobCode = 'F'
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error

  -- Update ReservedQuantity
  update subl
     set ReservedQuantity = isnull((select sum(isnull(i.ConfirmedQuantity,0))
                               from Instruction i (nolock)
                               join Status      s (nolock) on i.StatusId = s.StatusId
                              where s.StatusCode        in ('W','S')
                                and i.Picked             = 0
                                and i.CreateDate         > @StartDate
                                and i.StorageUnitBatchId = subl.StorageUnitBatchId
                                and i.PickLocationId     = subl.LocationId),0)
    from StorageUnitBatchLocation subl
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error

  -- Update AllocatedQuantity
  update subl
     set AllocatedQuantity = isnull((select sum(isnull(i.ConfirmedQuantity,0))
                               from Instruction i (nolock)
                               join Status      s (nolock) on i.StatusId = s.StatusId
                              where s.StatusCode        in ('W','S')
                                and i.Stored             = 0
                                and i.CreateDate         > @StartDate
                                and i.StorageUnitBatchId = subl.StorageUnitBatchId
                                and i.StoreLocationId    = subl.LocationId),0)
    from StorageUnitBatchLocation subl
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  -- Delete StorageUnitBatchLocation where All zero
  -- select * from viewSOH where ActualQuantity = 0 and AllocatedQuantity = 0 and ReservedQuantity = 0
  delete StorageUnitBatchLocation where ActualQuantity = 0 and AllocatedQuantity = 0 and ReservedQuantity = 0
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  -- Insert Missing StoreLocation
  insert StorageUnitBatchLocation
        (StorageUnitBatchId,
          LocationId,
          ActualQuantity,
          AllocatedQuantity,
          ReservedQuantity)
  select vi.StorageUnitBatchId,
         vi.StoreLocationId,
         0,
         sum(vi.ConfirmedQuantity),
         0
    from viewInstruction vi
   where Stored = 0
     and InstructionCode in ('W','S')
     and not exists(select 1 from StorageUnitBatchLocation soh where vi.StorageUnitBatchId = soh.StorageUnitBatchId and vi.StoreLocationId = soh.LocationId)
     and vi.ConfirmedQuantity is not null
     and vi.StoreLocationId is not null
     and vi.CreateDate         > @StartDate
  group by vi.StorageUnitBatchId,
           vi.StoreLocationId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  -- Insert Missing PickLocation
  insert StorageUnitBatchLocation
        (StorageUnitBatchId,
          LocationId,
          ActualQuantity,
          AllocatedQuantity,
          ReservedQuantity)
  select vi.StorageUnitBatchId,
         vi.PickLocationId,
         0,
         sum(vi.ConfirmedQuantity),
         0
    from viewInstruction vi
   where Picked = 0
     and InstructionCode in ('W','S')
     and not exists(select 1 from StorageUnitBatchLocation soh where vi.StorageUnitBatchId = soh.StorageUnitBatchId and vi.PickLocationId = soh.LocationId)
     and vi.ConfirmedQuantity is not null
     and vi.PickLocationId is not null
     and vi.CreateDate         > @StartDate
  group by vi.StorageUnitBatchId,
           vi.PickLocationId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  /*
  -- Compare Dropme to New
  -- Everything
  select vl.Area,
         vl.Location,
         subl.StorageUnitBatchId,
         subl.LocationId,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         dm.AllocatedQuantity as 'AllocatedDropme',
         subl.ReservedQuantity,
         dm.ReservedQuantity as 'ReservedDropme'
    from StorageUnitBatchLocation subl
    join StorageUnitBatchLocationDropme dm on subl.StorageUnitBatchId = dm.StorageUnitBatchId
                                          and subl.LocationId         = dm.LocationId
    join viewLocation vl on subl.LocationId = vl.LocationId
   where(subl.ReservedQuantity <> dm.ReservedQuantity
      or subl.AllocatedQuantity <> dm.AllocatedQuantity)
     and vl.Area != 'Despatch'

  -- Stock on hand
  select soh.Area,
         soh.Location,
         soh.ProductCode,
         soh.Product,
         soh.Batch,
         subl.StorageUnitBatchId,
         subl.LocationId,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         dm.AllocatedQuantity as 'AllocatedDropme',
         subl.ReservedQuantity,
         dm.ReservedQuantity as 'ReservedDropme'
    from StorageUnitBatchLocation subl
    join StorageUnitBatchLocationDropme dm on subl.StorageUnitBatchId = dm.StorageUnitBatchId
                                          and subl.LocationId         = dm.LocationId
    join viewSOH                       soh on subl.LocationId         = soh.LocationId
                                          and subl.StorageUnitBatchId = soh.StorageUnitBatchId
   where(subl.ReservedQuantity <> dm.ReservedQuantity
      or subl.AllocatedQuantity <> dm.AllocatedQuantity)
     and soh.Area != 'Despatch'
  
  select InstructionType, JobId, JobCode, InstructionCode, PickLocation, Picked, StoreLocation, Stored, ProductCode, Product, Batch, Quantity, ConfirmedQuantity, CreateDate, StartDate, EndDate
    from viewInstruction vi
   where Stored = 0 and InstructionCode in ('W','S')
     and not exists(select 1 from StorageUnitBatchLocation soh where vi.StorageUnitBatchId = soh.StorageUnitBatchId and vi.StoreLocationId = soh.LocationId)

  select InstructionType, JobId, JobCode, InstructionCode, PickLocation, Picked, StoreLocation, Stored, ProductCode, Product, Batch, Quantity, ConfirmedQuantity, CreateDate, StartDate, EndDate
    from viewInstruction vi
   where Picked = 0 and InstructionCode in ('W','S')
     and not exists(select 1 from StorageUnitBatchLocation soh where vi.StorageUnitBatchId = soh.StorageUnitBatchId and vi.PickLocationId = soh.LocationId)
  */
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
