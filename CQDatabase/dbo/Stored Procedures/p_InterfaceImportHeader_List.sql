﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportHeader_List
  ///   Filename       : p_InterfaceImportHeader_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:28
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportHeader.InterfaceImportHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportHeader_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceImportHeaderId
        ,null as 'InterfaceImportHeader'
  union
  select
         InterfaceImportHeader.InterfaceImportHeaderId
        ,InterfaceImportHeader.InterfaceImportHeaderId as 'InterfaceImportHeader'
    from InterfaceImportHeader
  
end
