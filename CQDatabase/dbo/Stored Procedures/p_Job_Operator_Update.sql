﻿
/*
  /// <summary>
  ///   Procedure Name : p_Job_Operator_Update
  ///   Filename       : p_Job_Operator_Update.sql
  ///   Create By      : 
  ///   Date Created   : 14 Dec 2020
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <RETURNs>
  ///   
  /// </RETURNs>
  /// <newpara>
  ///   ModIFied by    : 
  ///   ModIFied Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Job_Operator_Update
(
 @InstructionId     INT,
 @ConfirmedQuantity FLOAT,
 @operatorId        INT = NULL,
 @palletId			INT = NULL,
 @PalletIdStatusId	INT = NULL
)
 
AS
BEGIN
	 SET NOCOUNT ON;
  
  DECLARE @Error             INT,
          @Errormsg          NVARCHAR(500),
          @GetDate           DATETIME,
          @StatusId          INT,
          @JobId		     INT
  
  SELECT @GetDate = dbo.ufn_Getdate()
  
  IF @palletId IS NOT NULL
  BEGIN
  EXEC @Error = p_Pallet_Update
   @palletId	=	@palletId,
   @StatusId    =	@PalletIdStatusId
  
  IF @Error <> 0
  BEGIN
  PRINT @Error
    GOTO error
  END  
  END
  
  IF @ConfirmedQuantity > (SELECT Quantity FROM Instruction WHERE InstructionId = @InstructionId)
    RETURN
  
  SELECT @StatusId = dbo.ufn_StatusId('W','S')
  
  BEGIN TRANSACTION
  
  
  EXEC @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId = @InstructionId,
   @Confirmed     = 0
  
  IF @Error <> 0
  	BEGIN
    GOTO error
  END  
  
  EXEC @Error = p_Instruction_Update
   @InstructionId     = @InstructionId,
   @StatusId          = @StatusId,
   @StartDate         = @GetDate,
   @ConfirmedQuantity = @ConfirmedQuantity,
   @operatorId		  = @operatorId
    
  IF @Error <> 0
    GOTO error
  
  SELECT @JobId = JobId FROM Instruction WHERE InstructionId = @InstructionId
  
  IF @operatorId = '-1' OR @operatorId = ''
  SET @operatorId = NULL
  
  UPDATE Job
  SET OperatorId = @OperatorId
  WHERE JobId = @JobId

  EXEC @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId = @InstructionId,
   @Confirmed     = 1
  
  IF @Error <> 0
    GOTO error
  
  COMMIT TRANSACTION
  RETURN
  
  ERROR:
    RAISERROR ('Error ExeCuting p_Job_Operator_Update', 16, 1)
    ROLLBACK TRANSACTION
    RETURN @Error
END
