﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UOM_Insert
  ///   Filename       : p_UOM_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:59
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the UOM table.
  /// </remarks>
  /// <param>
  ///   @UOMId int = null output,
  ///   @UOM nvarchar(100) = null,
  ///   @UOMCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   UOM.UOMId,
  ///   UOM.UOM,
  ///   UOM.UOMCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_UOM_Insert
(
 @UOMId int = null output,
 @UOM nvarchar(100) = null,
 @UOMCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @UOMId = '-1'
    set @UOMId = null;
  
  if @UOM = '-1'
    set @UOM = null;
  
  if @UOMCode = '-1'
    set @UOMCode = null;
  
	 declare @Error int
 
  insert UOM
        (UOM,
         UOMCode)
  select @UOM,
         @UOMCode 
  
  select @Error = @@Error, @UOMId = scope_identity()
  
  
  return @Error
  
end
