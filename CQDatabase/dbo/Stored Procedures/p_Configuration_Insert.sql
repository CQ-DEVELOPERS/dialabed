﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Insert
  ///   Filename       : p_Configuration_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 12:11:59
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Configuration table.
  /// </remarks>
  /// <param>
  ///   @ConfigurationId int = null output,
  ///   @WarehouseId int = null output,
  ///   @ModuleId int = null,
  ///   @Configuration nvarchar(510) = null,
  ///   @Indicator bit = null,
  ///   @IntegerValue int = null,
  ///   @Value sql_variant = null 
  /// </param>
  /// <returns>
  ///   Configuration.ConfigurationId,
  ///   Configuration.WarehouseId,
  ///   Configuration.ModuleId,
  ///   Configuration.Configuration,
  ///   Configuration.Indicator,
  ///   Configuration.IntegerValue,
  ///   Configuration.Value 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Insert
(
 @ConfigurationId int = null output,
 @WarehouseId int = null output,
 @ModuleId int = null,
 @Configuration nvarchar(510) = null,
 @Indicator bit = null,
 @IntegerValue int = null,
 @Value sql_variant = null 
)
 
as
begin
	 set nocount on;
  
  if @ConfigurationId = '-1'
    set @ConfigurationId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ModuleId = '-1'
    set @ModuleId = null;
  
  if @Configuration = '-1'
    set @Configuration = null;
  
	 declare @Error int
 
  insert Configuration
        (ConfigurationId,
         WarehouseId,
         ModuleId,
         Configuration,
         Indicator,
         IntegerValue,
         Value)
  select @ConfigurationId,
         @WarehouseId,
         @ModuleId,
         @Configuration,
         @Indicator,
         @IntegerValue,
         @Value 
  
  select @Error = @@Error, @WarehouseId = scope_identity()
  
  
  return @Error
  
end
 
