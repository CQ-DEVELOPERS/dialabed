﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportHeading_Update
  ///   Filename       : p_ReportHeading_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:25
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ReportHeading table.
  /// </remarks>
  /// <param>
  ///   @ReportHeadingId int = null,
  ///   @CultureId int = null,
  ///   @ReportHeadingCode nvarchar(200) = null,
  ///   @ReportHeading nvarchar(200) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportHeading_Update
(
 @ReportHeadingId int = null,
 @CultureId int = null,
 @ReportHeadingCode nvarchar(200) = null,
 @ReportHeading nvarchar(200) = null 
)
 
as
begin
	 set nocount on;
  
  if @ReportHeadingId = '-1'
    set @ReportHeadingId = null;
  
  if @ReportHeadingCode = '-1'
    set @ReportHeadingCode = null;
  
  if @ReportHeading = '-1'
    set @ReportHeading = null;
  
	 declare @Error int
 
  update ReportHeading
     set CultureId = isnull(@CultureId, CultureId),
         ReportHeadingCode = isnull(@ReportHeadingCode, ReportHeadingCode),
         ReportHeading = isnull(@ReportHeading, ReportHeading) 
   where ReportHeadingId = @ReportHeadingId
  
  select @Error = @@Error
  
  
  return @Error
  
end
