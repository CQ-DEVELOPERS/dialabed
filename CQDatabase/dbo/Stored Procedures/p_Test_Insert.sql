﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Test_Insert
  ///   Filename       : p_Test_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:59
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Test table.
  /// </remarks>
  /// <param>
  ///   @TestId int = null output,
  ///   @TestCode nvarchar(20) = null,
  ///   @Test nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   Test.TestId,
  ///   Test.TestCode,
  ///   Test.Test 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Test_Insert
(
 @TestId int = null output,
 @TestCode nvarchar(20) = null,
 @Test nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
  if @TestId = '-1'
    set @TestId = null;
  
  if @TestCode = '-1'
    set @TestCode = null;
  
  if @Test = '-1'
    set @Test = null;
  
	 declare @Error int
 
  insert Test
        (TestCode,
         Test)
  select @TestCode,
         @Test 
  
  select @Error = @@Error, @TestId = scope_identity()
  
  
  return @Error
  
end
