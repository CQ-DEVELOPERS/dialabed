﻿
CREATE PROCEDURE [dbo].[p_Interface_WebService_LOCK] (@XMLBody NVARCHAR(MAX) OUTPUT)
AS
BEGIN
	CREATE TABLE #ExportIds (InterfaceExportHeaderId INT PRIMARY KEY)

	DECLARE @GETDATE DATETIME

	SELECT @GETDATE = dbo.ufn_GetDate()

	UPDATE InterfaceExportHeader
	SET ProcessedDate = @GETDATE
	OUTPUT INSERTED.InterfaceExportHeaderId
	INTO #ExportIds
	WHERE RecordStatus = 'N'
		AND RecordType = 'Allocated'
		AND ProcessedDate IS NULL

	SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' + (
			SELECT (
					SELECT h.OrderNumber AS 'OrderNumber'
					FROM InterfaceExportHeader h
					INNER JOIN #ExportIds ei ON h.InterfaceExportHeaderId = ei.InterfaceExportHeaderId
					)
			FOR XML PATH('OrderPick'),
				TYPE
			)
	FOR XML PATH('root')

	UPDATE InterfaceExportHeader
	SET RecordStatus = 'Y'
	WHERE InterfaceExportHeaderId IN (
			SELECT ei.InterfaceExportHeaderId
			FROM #ExportIds ei
			)
END
