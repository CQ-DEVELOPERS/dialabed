﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingColumn_Delete
  ///   Filename       : p_InterfaceMappingColumn_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 10:53:58
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceMappingColumn table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingColumnId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingColumn_Delete
(
 @InterfaceMappingColumnId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceMappingColumn
     where InterfaceMappingColumnId = @InterfaceMappingColumnId
  
  select @Error = @@Error
  
  
  return @Error
  
end
