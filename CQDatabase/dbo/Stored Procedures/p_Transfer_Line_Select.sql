﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Transfer_Line_Select
  ///   Filename       : p_Transfer_Line_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Transfer_Line_Select
(
 @OutboundShipmentId int = null,
 @IssueId int = null
)
 
as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   IssueLineId                     int,
   IssueId                         int,
   OrderNumber                     nvarchar(30),
   StorageUnitBatchId              int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   BatchId                         int,
   Batch                           nvarchar(50),
   SKUCode                         nvarchar(50),
   RequiredQuantity                float default 0,
   ConfirmedQuatity                float default 0,
   AllocatedQuantity               float default 0,
   ManufacturingQuantity           float default 0,
   RawQuantity                     float default 0,
   ReplenishmentQuantity           float default 0,
   AvailableQuantity               float default 0,
   AvailabilityIndicator           nvarchar(20),
   AvailablePercentage             numeric(13,2) default 0,
   AreaType                        nvarchar(10),
   WarehouseId                     int,
   DropSequence                    int
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null;
  
  if @IssueId = -1
    set @IssueId = null;
  
  if @IssueId is not null
  begin
    insert @TableResult
          (IssueLineId,
           IssueId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           RequiredQuantity,
           ConfirmedQuatity,
           AreaType,
           WarehouseId,
           DropSequence)
    select il.IssueLineId,
           il.IssueId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           il.ConfirmedQuatity,
           isnull(i.AreaType,''),
           i.WarehouseId,
           i.DropSequence
      from IssueLine         il
      join Issue              i (nolock) on il.IssueId            = i.IssueId
      join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument  od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status             s (nolock) on il.StatusId           = s.StatusId
     where il.IssueId = @IssueId
  end
  else if @OutboundShipmentId is not null
  begin
    insert @TableResult
          (IssueLineId,
           IssueId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           RequiredQuantity,
           ConfirmedQuatity,
           AreaType,
           WarehouseId,
           DropSequence)
    select il.IssueLineId,
           il.IssueId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           il.ConfirmedQuatity,
           isnull(i.AreaType,''),
           i.WarehouseId,
           i.DropSequence
      from IssueLine         il
      join Issue              i (nolock) on il.IssueId            = i.IssueId
      join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument  od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status             s (nolock) on il.StatusId           = s.StatusId
      join OutboundShipmentIssue osi (nolock) on il.IssueId       = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         Batch       = b.Batch,
         SKUCode     = sku.SKUCode
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch            b   (nolock) on tr.BatchId           = b.BatchId
  
  update tr
     set AllocatedQuantity = a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ManufacturingQuantity = a.ActualQuantity - a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set RAWQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set AvailableQuantity = ManufacturingQuantity + RAWQuantity
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), ManufacturingQuantity) / convert(numeric(13,3), RequiredQuantity)) * 100
   where AllocatedQuantity + AvailableQuantity != 0
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity   > ManufacturingQuantity
     and RequiredQuantity   > ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity  <= ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where ManufacturingQuantity >= RequiredQuantity
  
  select IssueLineId,
         ProductCode,
         Product,
         Batch,
         SKUCode,
         RequiredQuantity,
         AllocatedQuantity,
         ManufacturingQuantity,
         RawQuantity,
         ReplenishmentQuantity,
         AvailableQuantity,
         AvailabilityIndicator,
         AvailablePercentage
    from @TableResult
  order by ProductCode,
           Product,
           Batch,
           SKUCode
end
