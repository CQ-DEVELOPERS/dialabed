﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_List
  ///   Filename       : p_ExternalCompany_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:44:48
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ExternalCompany table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ExternalCompany.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompany 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ExternalCompanyId
        ,'{All}' as ExternalCompany
  union
  select
         ExternalCompany.ExternalCompanyId
        ,ExternalCompany.ExternalCompany
    from ExternalCompany
  order by ExternalCompany
  
end
 
