﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitLocation_Search
  ///   Filename       : p_StorageUnitLocation_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:08
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StorageUnitLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null output,
  ///   @LocationId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StorageUnitLocation.StorageUnitId,
  ///   StorageUnitLocation.LocationId,
  ///   StorageUnitLocation.MinimumQuantity,
  ///   StorageUnitLocation.HandlingQuantity,
  ///   StorageUnitLocation.MaximumQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitLocation_Search
(
 @StorageUnitId int = null output,
 @LocationId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
 
  select
         StorageUnitLocation.StorageUnitId
        ,StorageUnitLocation.LocationId
        ,StorageUnitLocation.MinimumQuantity
        ,StorageUnitLocation.HandlingQuantity
        ,StorageUnitLocation.MaximumQuantity
    from StorageUnitLocation
   where isnull(StorageUnitLocation.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(StorageUnitLocation.StorageUnitId,'0'))
     and isnull(StorageUnitLocation.LocationId,'0')  = isnull(@LocationId, isnull(StorageUnitLocation.LocationId,'0'))
  
end
