﻿create procedure [dbo].[p_Pastel_Import_RTS]  
as  
begin  
          -- InterfaceImportSOHeader  
  declare @InterfaceImportHeaderId    int,  
          @OrderNumber                  varchar(30),  
          @PrimaryKey                   varchar(30),
          @ExternalCompanyCode          varchar(30),  
          @DeliveryDate                 datetime,  
          @JobWindowStart               varchar(255),  
          @JobWindowEnd                 varchar(255),  
          @JobDuration                  varchar(255),  
          @HoldStatus                   varchar(255),  
          @Remarks                      varchar(255),  
          @Instructions                 varchar(255),  
          @NumberOfLines                int,  
          -- InterfaceImportSODetail  
          @LineNumber                   int,  
          @ProductCode                  varchar(50),  
          @Quantity                     numeric(13,3),  
          @WhsCode                      varchar(255),  
          @WhsID                      varchar(255),  
          @PickUp                       varchar(255),  
          -- Internal variables  
          @ExternalCompanyId            int,  
          @ExternalCompany              varchar(255),  
          @DropSequence                 int,  
          @Weight                       numeric(13,3),  
          @OutboundDocumentTypeCode     varchar(10),  
          @PriorityId                   int,  
          @ErrorMsg                     varchar(100),  
          @Error                        int,  
          @OutboundDocumentTypeId       int,  
          @OutboundDocumentId           int,  
          @StatusId                     int,  
          @GetDate                      datetime,  
          @ProductId                    int,  
          @StorageUnitId                int,  
          @StorageUnitBatchId           int,  
          @OutboundLineId               int,  
          @Exception                    varchar(255),  
          @Delete                       bit,  
          @OutboundShipmentId           int,  
          @IssueId                      int,  
          @WarehouseId                  int,  
          @RecordType                   varchar(30),  
          @Product                      varchar(255),  
          @AreaType                     varchar(10)  ,
          @FromWarehouseCode			varchar(50),
          @FromWarehouseId				int,
          @ToWarehouseId				int,
          @ToWarehouseCode				varchar(50),
          @InterfaceMessage				varchar(255),
          @ErrorStatus					varchar(1)
    
  select @GetDate = dbo.ufn_Getdate()  
  
  -- Reprocess Order which did not process properly  
  update InterfaceImportHeader  
     set ProcessedDate = null  
   where RecordStatus     = 'N'  
     and ProcessedDate   <= dateadd(mi, -10, @Getdate)  
  
  set  @InterfaceMessage = 'Success'
  set  @ErrorStatus = 'N'
  Select * from OutboundDocumentType

--  update OutboundDocumentType set OutboundDocumentTypeCode = 'RTS' WHERE OutboundDocumentTypeCode = 'ret'

  update h
     set ProcessedDate = @Getdate,
         RecordType    = isnull(odt.OutboundDocumentTypeCode, h.RecordType)
    from InterfaceImportHeader h
        left join OutboundDocumentType odt (nolock) on h.RecordType = odt.OutboundDocumentType
   where exists(select 1 from InterfaceImportDetail d where h.InterfaceImportHeaderId = d.InterfaceImportHeaderId)
     and RecordType = 'RTS'
     and RecordStatus in ('N','U')
     and ProcessedDate is null
  Select top 10 * from InterfaceImportHeader
  
  declare header_cursor cursor for  
   select InterfaceImportHeaderId,  
          OrderNumber ,
          PrimaryKey,
          CompanyCode, -- CompanyCode  
          convert(datetime, isnull(DeliveryDate, @getdate), 120), -- DeliveryDate  
          Remarks,      -- Remarks  
          RecordType,    
           Convert(int,FromWarehouseCode),
		  Convert(int,ToWarehouseCode)
     from InterfaceImportHeader h  
    where ProcessedDate = @Getdate   
   order by OrderNumber
  
  open header_cursor  
    
  fetch header_cursor  
   into @InterfaceImportHeaderId,  
        @OrderNumber,  
        @PrimaryKey,
        @ExternalCompanyCode,  
        @DeliveryDate,  
        @Remarks,  
        @RecordType  ,
        @FromWarehouseId,
        @ToWarehouseId
    
  while (@@fetch_status = 0)  
  begin  
    begin transaction xml_import  
      
    if (select NumberOfLines  
          from InterfaceImportHeader  
         where InterfaceImportHeaderId = @InterfaceImportHeaderId)  
       !=  
       (select count(1)  
         from InterfaceImportDetail  
        where InterfaceImportHeaderId = @InterfaceImportHeaderId)  
    begin  
      select @ErrorMsg = 'Error executing Number of line in header and detail are different'  
      
             -- Karen - set error message for create into InterfaceMessage
     
			set @InterfaceMessage = 'Error executing Number of line in header and detail are different'
			
      goto error_header  
    end  
      
    set rowcount 1  
    update InterfaceImportHeader  
       set RecordStatus = 'C'  
     where InterfaceImportHeaderId = @InterfaceImportHeaderId  
    set rowcount 0  
      
  set  @InterfaceMessage = 'Success'
  set  @ErrorStatus = 'N'
      If @OrderNumber is null
        Set @OrderNumber = @PrimaryKey
      Else
      Set @OrderNumber = @OrderNumber + ' (' + @PrimaryKey + ')'   
      
    exec @Error = p_Interface_Outbound_Order_Delete  
     @OrderNumber = @OrderNumber  
      
    if @Error != 0  
    begin  
      select @ErrorMsg = 'Error executing p_Interface_Outbound_Order_Delete'  
            -- Karen - set error message for create into InterfaceMessage
			set @InterfaceMessage = 'Error deleting InterfaceOutboundOrder'
      goto error_header
    end  
      
    if @ExternalCompanyCode is null  
      set @ExternalCompanyCode = 'None'  
      
    set @ExternalCompanyId = null  
      
    select @ExternalCompanyId = ExternalCompanyId  
      from ExternalCompany  
     where HostId = @ExternalCompanyCode  
    
    if @ExternalCompanyId is null  
    begin  
      select @ExternalCompanyId   = ExternalCompanyId,  
             @ExternalCompanyCode = ExternalCompanyCode,  
             @ExternalCompany     = ExternalCompany  
        from ExternalCompany (nolock)  
       where ExternalCompanyCode = @ExternalCompanyCode  
        
        if @ExternalCompanyId is null  
        begin  
          set @ExternalCompany = @ExternalCompanyCode  
            
          exec @Error = p_ExternalCompany_Insert  
           @ExternalCompanyId         = @ExternalCompanyId output,  
           @ExternalCompanyTypeId     = 2,  
           @ExternalCompany           = @ExternalCompanyCode,  
           @ExternalCompanyCode       = @ExternalCompany  
            
          if @Error != 0  
          begin  
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'  
            
                                    -- Karen - set error message for create into InterfaceMessage
                     
					set @InterfaceMessage = 'Error inserting to ExternalCompany - ExternalCompanyId is blank'
					
            goto error_header  
          end  
        end  
    end  
      
    set @OutboundDocumentTypeCode = null  
--    update OutboundDocumentType set AreaType = 'RTS' WHERE OutboundDocumentTypeId = 4
    
    select @OutboundDocumentTypeCode = OutboundDocumentTypeCode,  
           @AreaType                 = AreaType  
      from OutboundDocumentType (nolock)  
     where OutboundDocumentTypeCode = @RecordType  
      
    if @OutboundDocumentTypeCode is null  
      set @OutboundDocumentTypeCode = 'SAL'  
      
    select @OutboundDocumentTypeId = OutboundDocumentTypeId,  
           @PriorityId             = PriorityId  
      from OutboundDocumentType (nolock)  
     where OutboundDocumentTypeCode = @OutboundDocumentTypeCode  
      
    select @StatusId = dbo.ufn_StatusId('OD','I')  
      
    if not exists (select 1  
                     from OutboundDocument  
                    where OrderNumber = @OrderNumber  
                      and OutboundDocumentTypeId = @OutboundDocumentTypeId)  
    begin  
      set @WarehouseId = null  
        
      select @WarehouseId = ParentWarehouseId  
        from Warehouse (nolock)  
       where HostId = @WhsCode  
        
      if @WarehouseId is null  
        select @WarehouseId = WarehouseId  
          from WarehouseCodeReference (nolock)  
         where WarehouseCode = @WhsCode  
           and DownloadType = 'RTS'  
        
  	
      if @WarehouseId is null  
        set @WarehouseId = 1  
        
      exec @Error = p_OutboundDocument_Insert  
       @OutboundDocumentId     = @OutboundDocumentId output,  
       @OrderNumber            = @OrderNumber,  
       @OutboundDocumentTypeId = @OutboundDocumentTypeId,  
       @ExternalCompanyId      = @ExternalCompanyId,
--       @PriorityId             = @PriorityId,
       @WarehouseId            = @WarehouseId,
       @StatusId               = @StatusId,
       @DeliveryDate           = @DeliveryDate,
       @CreateDate             = @GetDate,
       @ModifiedDate           = null  
      
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'  
        
                -- Karen - set error message for create into InterfaceMessage
             
        
        set @InterfaceMessage = 'Error inserting to OutboundDocument'
        
        goto error_header  
      end
    end  
    
    declare detail_cursor cursor for  
     select d.LineNumber,  -- LineNumber  
            d.ProductCode, -- ProductCode  
            d.Additional1, -- WArehouseId  
            d.Quantity,    -- Quantity  
            d.Additional2  -- WarehouseCode
       from InterfaceImportDetail d
      where d.InterfaceImportHeaderId = @InterfaceImportHeaderId

     
    open detail_cursor  
      
    fetch detail_cursor  
     into @LineNumber,  
          @ProductCode,  
          @WhsID,  
          @Quantity,  
          @WhsCode  
    
    if @@fetch_status != 0
    begin
      close detail_cursor
      deallocate detail_cursor
      select @ErrorMsg = 'Error executing no lines to import'
      
              -- Karen - set error message for create into InterfaceMessage
        
        set @InterfaceMessage = 'Error executing no lines to import'
        
      goto error_detail
    end
    
    while (@@fetch_status = 0)  
    begin  
      set @StorageUnitBatchId = null  
      set @StorageUnitId = null  
      set @Product = null  
        
      select @ProductCode = ProductCode,  
             @Product     = Product  
        from Product  
       where ProductCode = @ProductCode  
        
      if @Product is null  
      begin  
        select @ErrorMsg = 'Error executing Product does not exist'  
        
                -- Karen - set error message for create into InterfaceMessage
        
        set @InterfaceMessage = 'Error executing Product does not exist'
        
        goto error_detail  
      end  
        
      exec @Error = p_interface_xml_Product_Insert  
       @ProductCode        = @ProductCode,  
       @Product            = @Product,  
       @WarehouseId        = @WarehouseId,  
       @StorageUnitId      = @StorageUnitId output,  
       @StorageUnitBatchId = @StorageUnitBatchId output  
        
      if @Error != 0 or @StorageUnitBatchId is null  
      begin  
        select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'  
                -- Karen - set error message for create into InterfaceMessage
        set @InterfaceMessage = 'Error inserting interface_xml_Product -StorageUnitBatchId is blank'
        goto error_detail  
      end  
        
      if exists (select 1  
                   from OutboundLine  
                  where OutboundDocumentId = @OutboundDocumentId  
                    and LineNumber = @LineNumber)  
      begin  
        select @Exception = 'Attempt to insert duplicate line '  
                          + @ProductCode  
                          + '  Order No:'  
                          + @OrderNumber  
        exec @Error = p_Exception_Insert  
         @Exception     = @Exception,  
         @ExceptionCode = 'INTERROR02',  
         @CreateDate    = @GetDate  
          
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_Exception_Insert'  
                    -- Karen - set error message for create into InterfaceMessage
          set @InterfaceMessage = 'Error inserting Exception'
          
          goto error_detail  
        end  
      end  
    else  
      begin  
        exec @Error = p_OutboundLine_Insert  
         @OutboundLineId     = @OutboundLineId output,  
         @OutboundDocumentId = @OutboundDocumentId,  
         @StorageUnitId      = @StorageUnitId,  
         @StatusId           = @StatusId,  
         @LineNumber         = @LineNumber,  
         @Quantity           = @Quantity,  
         --@Weight             = @di_weight * @Quatity,  
         @BatchId            = null  
          
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_OutboundLine_Insert'  
                    -- Karen - set error message for create into InterfaceMessage
          set @InterfaceMessage = 'Error executing p_OutboundLine_Insert'
          goto error_detail  
        end  
      end  
        
      fetch detail_cursor  
       into @LineNumber,  
            @ProductCode,  
            @WhsCode,  
            @Quantity,  
            @PickUp  
    end
      
    close detail_cursor
    deallocate detail_cursor
    
    if @RecordType <> 'IBT'
    Begin
		select @WarehouseId = ParentWarehouseId
		  from Warehouse (nolock)
		 where HostId = @WhsID
	 
		exec p_OutboundDocument_Update
		 @OutboundDocumentId = @OutboundDocumentId,
		 @WarehouseId        = @WarehouseId
    End
      
    if @Error != 0  
    begin  
      select @ErrorMsg = 'Error executing p_OutboundDocument_Update'  
      
            -- Karen - set error message for create into InterfaceMessage
      
      set @InterfaceMessage = 'Error updating OutboundDocument'
      
      goto error_detail  
    end  
      
    exec @Error = p_Despatch_Create_Issue  
     @OutboundDocumentId = @OutboundDocumentId,  
     @OperatorId         = null,  
     @Remarks            = @Remarks  
      
    if @Error != 0  
    begin  
      select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'  
      
            -- Karen - set error message for create into InterfaceMessage
      
      set @InterfaceMessage = 'Error executing p_Despatch_Create_Issue'
      
      goto error_detail  
    end  
      

    select @IssueId     = IssueId,  
           @WarehouseId = WarehouseId  
      from Issue (nolock)  
     where OutboundDocumentId = @OutboundDocumentId  
      
   
	
   	update Issue  
       set AreaType = @AreaType  
     where IssueId = @IssueId  

    select @Error = @@Error  
      
    if @Error != 0  
    begin  
      select @ErrorMsg = 'Error updating Issue'  
      
           
      -- Karen - set error message for create into InterfaceMessage
      
      set @InterfaceMessage = 'Error updating Issue'
      
      goto error_detail  
    end  
      
    if @HoldStatus = 'True'  
    begin  
      select @StatusId = dbo.ufn_StatusId('IS','OH')  
        
      exec @Error = p_Issue_Update  
       @IssueId = @IssueId,  
       @StatusId = @StatusId  
      
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'  
        
                -- Karen - set error message for create into InterfaceMessage
        
        set @InterfaceMessage = 'Error executing p_Outbound_Auto_Load'
        
        goto error_detail  
      end  
    end  
    
    exec @Error = p_Outbound_Auto_Load  
     @OutboundShipmentId = @OutboundShipmentId output,  
     @IssueId            = @IssueId  
      
    if @Error != 0  
    begin  
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'  
      
              -- Karen - set error message for create into InterfaceMessage
        
        set @InterfaceMessage = 'Error executing p_Outbound_Auto_Load'
        
      goto error_detail  
    end  
    
    exec @Error = p_Outbound_Auto_Release  
     @OutboundShipmentId = @OutboundShipmentId,  
     @IssueId            = @IssueId  
      
    if @Error != 0  
    begin  
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load' 
      
              -- Karen - set error message for create into InterfaceMessage
        
        set @InterfaceMessage = 'Error executing p_Outbound_Auto_Load'
         
      goto error_detail  
    end  
    
    error_detail:
    
        
     -- Karen - added error message create into InterfaceMessage
					 --INSERT INTO InterfaceMessage
					--		    ([InterfaceMessageCode]
					--		    ,[InterfaceMessage]
					--		    ,[InterfaceId]
					--			,[InterfaceTable]
					--			,[Status]
					--			,[OrderNumber]
					--			,[CreateDate]
					--			,[ProcessedDate])
					--VALUES
					--			('Failed'
					--			,@InterfaceMessage
					--			,@InterfaceImportSOHeaderId
					--			,'InterfaceImportSODetail'
					--			,'E'
					--			,@OrderNumber
					--			,@GetDate
					--			,@GetDate)
								
    error_header:  
    
        
         -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,@InterfaceMessage
								,@InterfaceImportHeaderId
								,'InterfaceImportHeader'
								,'E'
								,@OrderNumber
								,@GetDate
								,@GetDate)
      
    if @Error = 0  
    begin  
      --select @OrderNumber as 'OrderNumber', 'Commit 1', @@trancount as '@@trancount'  
      if @@trancount > 0  
        commit transaction xml_import  
         -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Processed'
								,@InterfaceMessage
								,@InterfaceImportHeaderId
								,'InterfaceImportHeader'
								,@ErrorStatus
								,@OrderNumber
								,@GetDate
								,@GetDate)
								
      --select @OrderNumber as 'OrderNumber', 'Commit 2', @@trancount as '@@trancount'  
    end  
    else  
    begin  
      --select @OrderNumber as 'OrderNumber', 'Error 1', @@trancount as '@@trancount'  
      if @@trancount > 0  
        rollback transaction xml_import  
      --select @OrderNumber as 'OrderNumber', 'Error 2', @@trancount as '@@trancount'  
      update InterfaceImportHeader  
         set RecordStatus = 'E',  
             RecordType = isnull(@OutboundDocumentTypeCode, RecordType)
             --Additional9 = @ErrorMsg
       where InterfaceImportHeaderId = @InterfaceImportHeaderId  
         
  set  @InterfaceMessage = 'Error'
  set  @ErrorStatus = 'E'
  
    -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,@InterfaceMessage
								,@InterfaceImportHeaderId
								,'InterfaceImportHeader'
								,'E'
								,@OrderNumber
								,@GetDate
								,@GetDate)
    end  
      
    fetch header_cursor  
   into @InterfaceImportHeaderId,  
        @OrderNumber,  
        @PrimaryKey,
        @ExternalCompanyCode,  
        @DeliveryDate,  
        @Remarks,  
        @RecordType  ,
        @FromWarehouseId,
        @ToWarehouseId
end  
    
  close header_cursor  
  deallocate header_cursor  
    
  update InterfaceImportHeader  
     set ProcessedDate = null  
   where RecordStatus = 'N'  
     and ProcessedDate = @GetDate  
  
  --exec p_Pastel_Import_ORD
  
  return  
end

