﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Insert
  ///   Filename       : p_SerialNumber_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2013 14:12:39
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the SerialNumber table.
  /// </remarks>
  /// <param>
  ///   @SerialNumberId int = null output,
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null,
  ///   @ReceiptId int = null,
  ///   @StoreInstructionId int = null,
  ///   @PickInstructionId int = null,
  ///   @SerialNumber varchar(50) = null,
  ///   @ReceivedDate datetime = null,
  ///   @DespatchDate datetime = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @BatchId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @ReceiptLineId int = null,
  ///   @StoreJobId int = null,
  ///   @PickJobId int = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @StatusId int = null 
  /// </param>
  /// <returns>
  ///   SerialNumber.SerialNumberId,
  ///   SerialNumber.StorageUnitId,
  ///   SerialNumber.LocationId,
  ///   SerialNumber.ReceiptId,
  ///   SerialNumber.StoreInstructionId,
  ///   SerialNumber.PickInstructionId,
  ///   SerialNumber.SerialNumber,
  ///   SerialNumber.ReceivedDate,
  ///   SerialNumber.DespatchDate,
  ///   SerialNumber.ReferenceNumber,
  ///   SerialNumber.BatchId,
  ///   SerialNumber.IssueId,
  ///   SerialNumber.IssueLineId,
  ///   SerialNumber.ReceiptLineId,
  ///   SerialNumber.StoreJobId,
  ///   SerialNumber.PickJobId,
  ///   SerialNumber.Remarks,
  ///   SerialNumber.StatusId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Insert
(
 @SerialNumberId int = null output,
 @StorageUnitId int = null,
 @LocationId int = null,
 @ReceiptId int = null,
 @StoreInstructionId int = null,
 @PickInstructionId int = null,
 @SerialNumber varchar(50) = null,
 @ReceivedDate datetime = null,
 @DespatchDate datetime = null,
 @ReferenceNumber nvarchar(60) = null,
 @BatchId int = null,
 @IssueId int = null,
 @IssueLineId int = null,
 @ReceiptLineId int = null,
 @StoreJobId int = null,
 @PickJobId int = null,
 @Remarks nvarchar(510) = null,
 @StatusId int = null 
)
 
as
begin
	 set nocount on;
  
  if @SerialNumberId = '-1'
    set @SerialNumberId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @SerialNumber = '-1'
    set @SerialNumber = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @StoreJobId = '-1'
    set @StoreJobId = null;
  
  if @PickJobId = '-1'
    set @PickJobId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
	 declare @Error int
 
  insert SerialNumber
        (StorageUnitId,
         LocationId,
         ReceiptId,
         StoreInstructionId,
         PickInstructionId,
         SerialNumber,
         ReceivedDate,
         DespatchDate,
         ReferenceNumber,
         BatchId,
         IssueId,
         IssueLineId,
         ReceiptLineId,
         StoreJobId,
         PickJobId,
         Remarks,
         StatusId)
  select @StorageUnitId,
         @LocationId,
         @ReceiptId,
         @StoreInstructionId,
         @PickInstructionId,
         @SerialNumber,
         @ReceivedDate,
         @DespatchDate,
         @ReferenceNumber,
         @BatchId,
         @IssueId,
         @IssueLineId,
         @ReceiptLineId,
         @StoreJobId,
         @PickJobId,
         @Remarks,
         @StatusId 
  
  select @Error = @@Error, @SerialNumberId = scope_identity()
  
  
  return @Error
  
end
