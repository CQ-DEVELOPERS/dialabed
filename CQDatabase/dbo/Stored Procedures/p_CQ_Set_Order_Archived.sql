﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CQ_Set_Order_Archived
  ///   Filename       : p_CQ_Set_Order_Archived.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Apr 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CQ_Set_Order_Archived
(
 @OrderNum varchar(20)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500)
  
  begin transaction
  
  update invNum
     set DocState = 4
   where OrderNum = @OrderNum
     and DocState = 1
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_CQ_Set_Order_Archived'
    rollback transaction
    return @Error
end
