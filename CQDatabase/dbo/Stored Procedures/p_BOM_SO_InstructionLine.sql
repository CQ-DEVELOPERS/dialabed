﻿ 
/*
/// <summary>
///   Procedure Name : p_BOM_SO_InstructionLine
///   Filename       : p_BOM_SO_InstructionLine.sql
///   Create By      : Ruan groenewald
///   Date Created   : 05 Sept 2011
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
/// </param>
/// <returns>
 
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
create procedure p_BOM_SO_InstructionLine
(
 @warehouseId        int,
 @bOMInstructionId   int,
 @outboundShipmentId int,
 @issueId            int
)
--with encryption
as
begin
     set nocount on;
         
  declare @TableResult TABLE
  (OrderNumber           nvarchar(30)
  ,LineNumber            Int
  ,Quantity              float
  ,ProductCode           nvarchar(30)
  ,Product               nvarchar(50)
  ,BOMInstructionId      Int
  ,BOMLineId             Int
  ,StorageUnitId         int
  ,RequiredQuantity      float default 0
  ,ConfirmedQuatity      float default 0
  ,AllocatedQuantity     float default 0
  ,ManufacturingQuantity float default 0
  ,RawQuantity           float default 0
  ,ReplenishmentQuantity float default 0
  ,ReceivingQuantity     float default 0
  ,AvailableQuantity     float default 0
  ,AvailabilityIndicator nvarchar(20)
  ,AvailablePercentage   float default 0
  ,AreaType              nvarchar(10) default ''
  ,WarehouseId           int
  ,DropSequence          int
  ,StockOnOrder          float
   )
  
  if @BOMInstructionId in (-1,0)
    set @BOMInstructionId = null
  
  if @outboundShipmentId in (-1,0)
    set @outboundShipmentId = null
  
  if @outboundShipmentId is not null
    set @issueId = null
  
  if @issueId = 0
    set @issueId = null
  
  if @BOMInstructionId is null
  and @outboundShipmentId is null
  and @issueId is null
    goto result
  
  if @BOMInstructionId is not null
  begin
    INSERT @TableResult
           (WarehouseId
           ,OrderNumber
           ,LineNumber
           ,RequiredQuantity
           ,ProductCode
           ,Product
           ,BOMInstructionId
           ,BOMLineId
           ,StorageUnitId)
    SELECT @WarehouseId,
           bi.OrderNumber
           ,ROW_NUMBER() OVER (Order By bil.BOMLineId, bil.LineNumber) AS LineNumber
           ,bil.Quantity*bp.Quantity As Quantity
           ,P.ProductCode
           ,P.Product
           ,bi.BOMInstructionId
           ,bil.BOMLineId
           ,bp.StorageUnitId
      FROM BOMInstruction bi (nolock)
      Inner join BOMInstructionLine bil (nolock) on bi.BOMInstructionId = bil.BOMInstructionId
      Inner join BOMProduct bp (nolock) on bil.BOMLineId = bp.BOMLineId
                              and bil.LineNumber = bp.LineNumber
      Inner join StorageUnit su (nolock) on bp.StorageUnitId = su.StorageUnitId
      Inner join Product P (nolock) ON SU.ProductId = P.ProductId
     where bi.BOMInstructionId = @BOMInstructionId
  end
  else
  begin
    INSERT @TableResult
           (WarehouseId
           ,AreaType
           ,OrderNumber
           ,LineNumber
           ,RequiredQuantity
           ,ProductCode
           ,Product
           ,BOMInstructionId
           ,BOMLineId
           ,StorageUnitId)
    SELECT @WarehouseId
           ,i.AreaType
           ,case when @issueId is null
                 then convert(varchar(10), osi.OutboundShipmentId)
                 else od.OrderNumber
                 end
           ,ol.LineNumber
           ,sum(il.Quantity*bp.Quantity)
           ,P.ProductCode
           ,P.Product
           ,null as BOMInstructionId
           ,-1 as BOMLineId
           ,bp.StorageUnitId
      FROM	OutboundDocument        od (nolock)
      join OutboundLine            ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId
      join Issue                    i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
      left
      join OutboundShipmentIssue  osi (nolock) on i.IssueId = osi.IssueId
                                              and isnull(osi.OutboundShipmentId,-1) = isnull(@outboundShipmentId,-1)
      join IssueLine               il (nolock) on i.IssueId = il.IssueId
      join StorageUnitBatch       sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join BOMheader               bh (nolock) on sub.StorageUnitId = bh.StorageUnitId
      join BOMLine                 bl (nolock) on bh.BOMHeaderId = bl.BOMHeaderId
      join BOMProduct              bp (nolock) on bl.BOMLineId = bp.BOMLineId
      join StorageUnit             su (nolock) ON bp.StorageUnitId = su.StorageUnitId 
      join Product                  p (nolock) ON su.ProductId = p.ProductId
      join Status                   s (nolock) ON p.StatusId = s.StatusId
     where i.IssueId = isnull(@issueId, i.IssueId)
    group by i.AreaType
            ,case when @issueId is null
                  then convert(varchar(10), osi.OutboundShipmentId)
                  else od.OrderNumber
                  end
            ,ol.LineNumber
            ,P.ProductCode
            ,P.Product
            ,bp.StorageUnitId
  end
  
  if @bOMInstructionId is not null or @outboundShipmentId is null or @issueId is null
  begin
  update tr
     set AllocatedQuantity = a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ManufacturingQuantity = a.ActualQuantity - a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set RAWQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ReceivingQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailableArea a on tr.WarehouseId   = a.WarehouseId
                            and a.AreaType       = 'backup'
                            and tr.StorageUnitId = a.StorageUnitId
                            and a.AreaCode = 'R'
  
  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set AvailableQuantity = ManufacturingQuantity + RAWQuantity
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), ManufacturingQuantity) / convert(numeric(13,3), RequiredQuantity)) * 100
   where AllocatedQuantity + AvailableQuantity != 0
     and convert(numeric(13,3), RequiredQuantity) > 0
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity   > ManufacturingQuantity
     and RequiredQuantity   > ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity  <= ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where ManufacturingQuantity >= RequiredQuantity
  
  update tr
     set StockOnOrder = (select sum(RequiredQuantity)
                           from ReceiptLine rl (nolock)
                          where rl.StorageUnitBatchId = sub.StorageUnitBatchId
                            and rl.StatusId = dbo.ufn_StatusId('R','W'))
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitId = sub.StorageUnitId
  end
  
  result:
  SELECT
       OrderNumber
       ,LineNumber
       ,Quantity
       ,ProductCode
       ,Product
       ,BOMInstructionId
       ,BOMLineId
       ,RequiredQuantity
       ,AllocatedQuantity
       ,AvailableQuantity
       ,ManufacturingQuantity
       ,RawQuantity
       ,ReceivingQuantity
       ,ReplenishmentQuantity
       ,AvailablePercentage
       ,AvailabilityIndicator
       ,StockOnOrder
  FROM @TableResult
       
       end
