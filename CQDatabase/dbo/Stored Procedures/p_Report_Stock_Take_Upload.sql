﻿--IF OBJECT_ID('dbo.p_Report_Stock_Take_Upload') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Report_Stock_Take_Upload
--    IF OBJECT_ID('dbo.p_Report_Stock_Take_Upload') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Report_Stock_Take_Upload >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Report_Stock_Take_Upload >>>'
--END
--go
 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_Upload
  ///   Filename       : p_Report_Stock_Take_Upload.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter 
  ///   Modified Date  : 2020/09/23
  ///   Details        : Reports were often returning blank with no data despite there being data on the screen.
  ///	Details		   : Added the warehouseid to the @ComparisonDate select statement.
  /// </newpara>
*/
CREATE procedure [dbo].[p_Report_Stock_Take_Upload]
(
 @WarehouseId	int,
 @PrincipalId	int = null
)
 
as
begin
	 set nocount on;
	 
	if @PrincipalId = -1
		set @PrincipalId = null
		
			 
  declare @ComparisonDate datetime,
	         @Variance       int
	         
  select @ComparisonDate = max(ComparisonDate) from HousekeepingCompare (nolock) where warehouseid = @WarehouseId
  	         
  select hc.WarehouseCode,
         hc.ComparisonDate,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         hc.WMSQuantity,
         hc.HostQuantity,
         hc.WMSQuantity - hc.HostQuantity as 'Variance',
         hc.Sent,
         hc.UnitPrice * hc.WMSQuantity as 'WMSPrice',
         hc.UnitPrice * hc.HostQuantity as 'HostPrice',
         (hc.UnitPrice * hc.WMSQuantity) - (hc.UnitPrice * hc.HostQuantity) as 'PriceVariance',
         hc.UnitPrice,
         p.Category,
         hc.Sentdate AS 'DateSent',
         Case when Isnull(h.RecordStatus,sa.RecordStatus) in ('I','Y') Then 'Successful'
              when Isnull(h.RecordStatus,sa.RecordStatus) = 'E' Then 'Error'
              when Isnull(h.RecordStatus,sa.RecordStatus) = 'N' Then 'Waiting'
              Else '' end as UploadStatus,
         CustomField1,
         Pr.Principal  
    from HousekeepingCompare  hc (nolock) -- HousekeepingCompare2
  		left Join InterfaceExportHeader h (nolock) on h.InterfaceExportHeaderId = hc.InterfaceId
  		Left join InterfaceExportStockAdjustment sa (nolock) on hc.InterfaceId = sa.InterfaceExportStockAdjustmentId
    join StorageUnit          su (nolock) on hc.StorageUnitId          = su.StorageUnitId
    join Product               p (nolock) on su.ProductId              = p.ProductId
    join SKU                 sku (nolock) on su.SKUId                  = sku.SKUId
    join Batch                 b (nolock) on hc.BatchId                = b.BatchId
	join Status                s (nolock) on p.StatusId                = s.StatusId
	left 
	join Principal			  pr (nolock) on hc.PrincipalId = pr.PrincipalId
   where isnull(hc.WarehouseId, @WarehouseId) = @WarehouseId
     --and p.StatusId     = 65 -- Can't do that it may change
     --and s.StatusCode   = 'A'
     and hc.ComparisonDate = @ComparisonDate
     --and p.PrincipalId = isnull(@PrincipalId,p.PrincipalId)
end
 
 
--go
--IF OBJECT_ID('dbo.p_Report_Stock_Take_Upload') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Report_Stock_Take_Upload >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Report_Stock_Take_Upload >>>'
--go


