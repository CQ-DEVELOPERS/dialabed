﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_ABC_Pick_Bin
  ///   Filename       : p_Report_ABC_Pick_Bin.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_ABC_Replenishment
(
 @WarehouseId int,
 @FromDate    datetime,
 @ToDate      datetime,
 @A           numeric(10) = 80,
 @B           numeric(10) = 95,
 @PrincipalId	   int
)
 
as
begin
	 set nocount on;
  
  declare @Ranking       int,
          @StorageUnitId int,
          @LocationId    int
  
  declare @TableResult as table
  (
   Warehouse       nvarchar(50),
   Ranking         int,
   StorageUnitId   int,
   ProductCode     nvarchar(30),
   Product         nvarchar(255),
   SKUCode         nvarchar(50),
   Visits          numeric(13,3),
   TotalMonthly    numeric(13,3),
   Cumulative      numeric(13,3),
   CumulativeItems numeric(13,3),
   Category        char(1),
   LocationId      int,
   Location        nvarchar(15),
   Lines           numeric(13,3),
   PrincipalId		  int,
	  PrincipalCode		  nvarchar(50)
  )
  if @PrincipalId = -1
		set @PrincipalId = null
  insert @TableResult
        (StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         LocationId,
         Location,
         PrincipalId)
  select distinct
         vs.StorageUnitId,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vl.LocationId,
         vl.Location,
           p.PrincipalId
    from viewStock vs
    join StorageUnitBatchLocation subl on vs.StorageUnitBatchId = subl.StorageUnitBatchId
    join viewLocation               vl on subl.LocationId       = vl.LocationId
       join Principal p on vs.PrincipalId = p.PrincipalId
      where isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
  AND vl.AreaCode = 'PK'
     and vl.WarehouseId = @WarehouseId
  order by vl.Location
  
  update tr
     set Visits = (select count(1)
                     from Instruction        i (nolock)
                     join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                     join Status             s (nolock) on i.StatusId          = s.StatusId
                     join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
                    where it.InstructionTypeCode in ('P','PM','PS','FM')
                      and s.StatusCode            = 'F'
                      and i.ConfirmedQuantity     > 0
                      and i.PickLocationId        = tr.LocationId
                      and sub.StorageUnitId       = tr.StorageUnitId
                      and i.EndDate         between @FromDate and @ToDate)
    from @TableResult tr
  
  update @TableResult
     set TotalMonthly = (Visits / (select sum(Visits) from @TableResult)) * 100.000
   where Visits > 0
  
  set @Ranking = 0
  set rowcount 1
  
  while exists (select top 1 1 from @TableResult where Ranking is null)
  begin
    select @Ranking = @Ranking + 1
    
    select top 1
           @StorageUnitId = StorageUnitId,
           @LocationId    = LocationId
      from @TableResult
     where Ranking is null
    order by Visits desc
    
    update @TableResult
       set Ranking = @Ranking,
           Cumulative = TotalMonthly + isnull((select max(Cumulative) from @TableResult),0)
     where StorageUnitId = @StorageUnitId
       and LocationId    = @LocationId
  end
  
  set rowcount 0
  
--  set rowcount 1
--  
--  while exists (select top 1 1 from @TableResult where Cumulative is null)
--  begin
--    update @TableResult
--       set Cumulative = isnull(TotalMonthly,0) + isnull((select max(Cumulative) from @TableResult),0)
--     where Cumulative is null
--  end
--  
--  set rowcount 0
  
  update @TableResult
     set CumulativeItems = (convert(numeric(13,3), Ranking) / (select convert(numeric(13,3), max(Ranking)) from @TableResult)) * 100.000
  
  update @TableResult
     set Category = 'A'
   where Cumulative < @A
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'A')
   where Lines is null
     and Category = 'A'
  set rowcount 0
  
  update @TableResult
     set Category = 'B'
   where Cumulative between @A and @B
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'B') - (select max(CumulativeItems) from @TableResult where Category = 'A')
   where Lines is null
     and Category = 'B'
  set rowcount 0
  
  update @TableResult
     set Category = 'C'
   where Cumulative > @B
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'C') - (select max(CumulativeItems) from @TableResult where Category = 'B')
   where Lines is null
     and Category = 'C'
  set rowcount 0
  
  update @TableResult
     set TotalMonthly = 100
   where TotalMonthly > 100
  
  update @TableResult
     set Cumulative = 100
   where Cumulative > 100
  
  update @TableResult
     set CumulativeItems = 100
   where CumulativeItems > 100
  
  update @TableResult
     set Warehouse = w.Warehouse
    from Warehouse w
   where w.WarehouseId = @WarehouseId
   
     update @TableResult
     set PrincipalCode = p.PrincipalCode
     from @TableResult tr
    join Principal p on tr.PrincipalId = p.PrincipalId
  
  select Warehouse,
         Ranking,
         ProductCode,
         Product,
         SKUCode,
         Visits,
         TotalMonthly,
         convert(numeric(13,2), Cumulative) as 'Cumulative',
         convert(numeric(13,2), CumulativeItems) as 'CumulativeItems',
         Category,
         Location,
         Lines,
          PrincipalCode
    from @TableResult
  order by Ranking
end
