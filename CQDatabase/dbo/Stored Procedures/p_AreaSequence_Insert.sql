﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaSequence_Insert
  ///   Filename       : p_AreaSequence_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:01
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaSequence table.
  /// </remarks>
  /// <param>
  ///   @AreaSequenceId int = null output,
  ///   @PickAreaId int = null,
  ///   @StoreAreaId int = null,
  ///   @AdjustmentType nvarchar(20) = null,
  ///   @ReasonCode varchar(10) = null,
  ///   @InstructionTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   AreaSequence.AreaSequenceId,
  ///   AreaSequence.PickAreaId,
  ///   AreaSequence.StoreAreaId,
  ///   AreaSequence.AdjustmentType,
  ///   AreaSequence.ReasonCode,
  ///   AreaSequence.InstructionTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaSequence_Insert
(
 @AreaSequenceId int = null output,
 @PickAreaId int = null,
 @StoreAreaId int = null,
 @AdjustmentType nvarchar(20) = null,
 @ReasonCode varchar(10) = null,
 @InstructionTypeCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @AreaSequenceId = '-1'
    set @AreaSequenceId = null;
  
	 declare @Error int
 
  insert AreaSequence
        (PickAreaId,
         StoreAreaId,
         AdjustmentType,
         ReasonCode,
         InstructionTypeCode)
  select @PickAreaId,
         @StoreAreaId,
         @AdjustmentType,
         @ReasonCode,
         @InstructionTypeCode 
  
  select @Error = @@Error, @AreaSequenceId = scope_identity()
  
  
  return @Error
  
end
