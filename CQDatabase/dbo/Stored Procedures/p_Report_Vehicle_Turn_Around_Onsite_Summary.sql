﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Vehicle_Turn_Around_Onsite_Summary
  ///   Filename       : p_Report_Vehicle_Turn_Around_Onsite_Summary.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Vehicle_Turn_Around_Onsite_Summary
(
 @ExternalCompanyId int,
 @LocationId		int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
  set nocount on;
 
  declare @TableResult as table
  (
   ExternalCompany			nvarchar(255),
   ExternalCompanyCode		nvarchar(30),
   OrderNumber				nvarchar(30),
   PlannedStart				datetime,
   PlannedEnd				datetime,
   YardIn					datetime,
   YardOut					datetime,
   YardStartDiff			int,
   YardEndDiff				int,
   DockOn					datetime,
   DockOff					datetime,
   DockStartDiff			int,
   DockEndDiff				int,
   TimeOnDock				int,
   TimeInYard				int,
   InYardNotOnDock			int,
   TotalDeliveries			int,
   TotalLateDeliveries		int,
   ChartDate				date
   )
   
    
 --Declare @ActualPlannedStart		datetime,
	--	 @ActualPlannedEnd			datetime,
	--	 @ActualYardIn				datetime,
	--	 @ActualYardOut				datetime,
	--	 @ActualDockOn				datetime,
	--	 @ActualDockOff				datetime
  
  if @ExternalCompanyId = -1
    set @ExternalCompanyId = null
  
   insert @TableResult
        (ExternalCompany,
         ExternalCompanyCode,
         OrderNumber,
         PlannedStart,
         PlannedEnd,
         YardIn,
         YardOut,
         DockOn,
         DockOff,
         ChartDate
         )
  select ec.ExternalCompany,
         ec.ExternalCompanyCode,
         id.OrderNumber,
         pln.PlannedStart,
         pln.PlannedEnd,
         yard.PlannedStart,
         yard.PlannedEnd,
         dock.PlannedStart,
         dock.PlannedEnd,
         pln.PlannedStart       
    from DockSchedule   pln (nolock)
    left
    join DockSchedule   yard (nolock) on pln.ReceiptId = yard.ReceiptId
                                    and yard.Version = 1
    join DockSchedule   dock (nolock) on pln.ReceiptId = dock.ReceiptId
                                    and dock.Version = 2
    join Receipt          r (nolock) on pln.ReceiptId = r.ReceiptId
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join ExternalCompany ec (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
   where (id.ExternalCompanyId = @ExternalCompanyId or @ExternalCompanyId is null)
     and pln.PlannedStart between @FromDate and @ToDate
     and pln.Version = 0
     
   update tr
     set YardStartDiff = DATEDIFF(MINUTE,PlannedStart,YardIn),
		 YardEndDiff = DATEDIFF(MINUTE,PlannedEnd,YardOut),
		 DockStartDiff = DATEDIFF(MINUTE,PlannedStart,DockOn),
		 DockEndDiff = DATEDIFF(MINUTE,PlannedEnd,DockOff)
    from @TableResult tr
    
  update tr
     set TimeOnDock = DATEDIFF(MINUTE,DockOn,DockOff),
		 TimeInYard = DATEDIFF(MINUTE,YardIn,YardOut)
    from @TableResult tr
    
  update tr
     set InYardNotOnDock = TimeInYard - TimeOnDock
    from @TableResult tr
    
  update tr
     set TotalDeliveries = (select COUNT(distinct OrderNumber) 
							  from @TableResult tr2
							 where tr2.ChartDate = cast(tr.PlannedStart as DATE))
    from @TableResult tr
    
  update tr
     set TotalLateDeliveries = (select COUNT(distinct OrderNumber) 
								  from @TableResult  tr2
								 where YardStartDiff > 0
								   and tr2.ChartDate = cast(tr.PlannedStart as DATE))
    from @TableResult tr 
  
    
    select 
			*, 
			@Fromdate as FromDate, 
			@ToDate as ToDate 
	  from	@TableResult
	  order by chartdate
end
 
 
