﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockOnHandCompare_Search
  ///   Filename       : p_StockOnHandCompare_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:44
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StockOnHandCompare table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StockOnHandCompare.WarehouseId,
  ///   StockOnHandCompare.ComparisonDate,
  ///   StockOnHandCompare.StorageUnitId,
  ///   StockOnHandCompare.BatchId,
  ///   StockOnHandCompare.Quantity,
  ///   StockOnHandCompare.Sent,
  ///   StockOnHandCompare.UnitPrice,
  ///   StockOnHandCompare.WarehouseCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockOnHandCompare_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         StockOnHandCompare.WarehouseId
        ,StockOnHandCompare.ComparisonDate
        ,StockOnHandCompare.StorageUnitId
        ,StockOnHandCompare.BatchId
        ,StockOnHandCompare.Quantity
        ,StockOnHandCompare.Sent
        ,StockOnHandCompare.UnitPrice
        ,StockOnHandCompare.WarehouseCode
    from StockOnHandCompare
  
end
