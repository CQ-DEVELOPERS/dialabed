﻿create procedure dbo.p_Interface_WebService_Logon
(
	@doc nvarchar(max) output
)
--with encryption
as
begin
	 declare @UserName            nvarchar(50),
	         @idoc                int,
	         @xml                 nvarchar(max)
	 
	 EXEC sp_xml_preparedocument @idoc OUTPUT, @doc

	 SELECT @UserName = UserName
		 FROM  OPENXML (@idoc, '/Root/Header',1)
            WITH (UserName varchar(50) 'UserName')
  
  if exists(select top 1 1
              from Operator       o (nolock)
              join OperatorGroup og (nolock) on o.OperatorGroupId = og.OperatorGroupId
             where o.Operator = @UserName
               and og.OperatorGroupCode in ('A','S'))
    set @doc = 'admin'
  else if exists(select 1 from Operator (nolock) where Operator = @UserName)
    set @doc = 'true'
  else
    set @doc = 'false'
end
