﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PerformanceLog_Insert
  ///   Filename       : p_PerformanceLog_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:56
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PerformanceLog table.
  /// </remarks>
  /// <param>
  ///   @PerformanceLogId int = null output,
  ///   @ProcedureName nvarchar(256) = null,
  ///   @Remarks nvarchar(max) = null,
  ///   @StartDate datetime = null,
  ///   @LogDate datetime = null 
  /// </param>
  /// <returns>
  ///   PerformanceLog.PerformanceLogId,
  ///   PerformanceLog.ProcedureName,
  ///   PerformanceLog.Remarks,
  ///   PerformanceLog.StartDate,
  ///   PerformanceLog.LogDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PerformanceLog_Insert
(
 @PerformanceLogId int = null output,
 @ProcedureName nvarchar(256) = null,
 @Remarks nvarchar(max) = null,
 @StartDate datetime = null,
 @LogDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @PerformanceLogId = '-1'
    set @PerformanceLogId = null;
  
	 declare @Error int
 
  insert PerformanceLog
        (ProcedureName,
         Remarks,
         StartDate,
         LogDate)
  select @ProcedureName,
         @Remarks,
         @StartDate,
         @LogDate 
  
  select @Error = @@Error, @PerformanceLogId = scope_identity()
  
  
  return @Error
  
end
