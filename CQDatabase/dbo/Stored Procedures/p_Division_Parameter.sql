﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Division_Parameter
  ///   Filename       : p_Division_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:05
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Division table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Division.DivisionId,
  ///   Division.Division 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Division_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as DivisionId
        ,'{All}' as Division
  union
  select
         Division.DivisionId
        ,Division.Division
    from Division
  
end
