﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PickChecking_Default_CheckQuantity
  ///   Filename       : p_PickChecking_Default_CheckQuantity.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PickChecking_Default_CheckQuantity
(
 @outboundShipmentId int,
 @issueId            int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @outboundShipmentId = -1 or @outboundShipmentId = 0
    set @outboundShipmentId = null
  
  if @issueId = -1 or @issueId = 0
    set @issueId = null
  
  begin transaction
  
  if @outboundShipmentId is not null
  begin
--    update i
--       set CheckQuantity = i.ConfirmedQuantity
--      from OutboundShipmentIssue osi (nolock)
--      join IssueLine              il (nolock) on osi.IssueId    = il.IssueId
--      join Instruction             i /*LOCK*/ on il.IssueLineId = i.IssueLineId
--     where osi.OutboundShipmentId = @outboundShipmentId
--       and i.CheckQuantity is null
    update Instruction
       set CheckQuantity = ConfirmedQuantity
     where OutboundShipmentId = @outboundShipmentId
       and CheckQuantity is null
  end
  else if @issueId is not null
  begin
    update i
       set CheckQuantity = i.ConfirmedQuantity
      from IssueLine  il (nolock)
      join Instruction i on il.IssueLineId = i.IssueLineId
     where il.IssueId = @IssueId
       and i.CheckQuantity is null
  end
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_PickChecking_Default_CheckQuantity'
    rollback transaction
    return @Error
end
