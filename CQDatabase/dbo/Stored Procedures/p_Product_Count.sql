﻿
/*
  /// <summary>
  ///   Procedure Name : p_Product_Count
  ///   Filename       : p_Product_Count.sql
  ///   Create By      : Mdletshe Thulasizwe
  ///   Date Created   : 05 June 2020
  /// </summary>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create PROCEDURE [dbo].[p_Product_Count]
(
 @ProductCode           nvarchar(30),
 @Product	            nvarchar(50),
 @SKUCode               nvarchar(10),
 @SKU		            nvarchar(50),
 @Barcode	            nvarchar(50),
 @PrincipalId			int = null
)
 
as
begin
	 set nocount on;

if @PrincipalId in (0,-1)
  set @PrincipalId = null
declare @count int

SELECT @count = COUNT(distinct P.ProductId) 
  FROM Product      P (nolock)
  left
  join StorageUnit su (nolock) on p.ProductId = su.ProductId
  left
  join SKU        sku (nolock) on su.SKUId = sku.SKUId
  JOIN Status       S ON P.StatusId = S.StatusId
  LEFT
  JOIN DangerousGoods AS dg ON dg.DangerousGoodsId = p.DangerousGoodsId
  left join Principal pri on pri.PrincipalId = p.PrincipalId
	where (p.ProductCode like @ProductCode + '%' or @ProductCode is null)
	and (p.Product like @Product + '%' or @Product is null)
	and (sku.SKUCode like '%' + @SKUCode + '%' or @SKUCode is null)
	and (sku.SKU  like '%' + @SKU + '%' or @SKU  is null)
	And (isnull(p.Barcode,'')  like @Barcode + '%'
	or   @Barcode is null)  
   and (p.ProductType != 'FPC' or p.ProductType is null)
   and (p.PrincipalId = @PrincipalId or @PrincipalId is null)
   select @count as Count 
end
