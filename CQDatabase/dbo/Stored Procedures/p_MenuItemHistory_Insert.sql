﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItemHistory_Insert
  ///   Filename       : p_MenuItemHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:55
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MenuItemHistory table.
  /// </remarks>
  /// <param>
  ///   @MenuItemId int = null,
  ///   @MenuId int = null,
  ///   @MenuItemText nvarchar(100) = null,
  ///   @ToolTip nvarchar(510) = null,
  ///   @NavigateTo nvarchar(510) = null,
  ///   @ParentMenuItemId int = null,
  ///   @OrderBy smallint = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   MenuItemHistory.MenuItemId,
  ///   MenuItemHistory.MenuId,
  ///   MenuItemHistory.MenuItemText,
  ///   MenuItemHistory.ToolTip,
  ///   MenuItemHistory.NavigateTo,
  ///   MenuItemHistory.ParentMenuItemId,
  ///   MenuItemHistory.OrderBy,
  ///   MenuItemHistory.CommandType,
  ///   MenuItemHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItemHistory_Insert
(
 @MenuItemId int = null,
 @MenuId int = null,
 @MenuItemText nvarchar(100) = null,
 @ToolTip nvarchar(510) = null,
 @NavigateTo nvarchar(510) = null,
 @ParentMenuItemId int = null,
 @OrderBy smallint = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert MenuItemHistory
        (MenuItemId,
         MenuId,
         MenuItemText,
         ToolTip,
         NavigateTo,
         ParentMenuItemId,
         OrderBy,
         CommandType,
         InsertDate)
  select @MenuItemId,
         @MenuId,
         @MenuItemText,
         @ToolTip,
         @NavigateTo,
         @ParentMenuItemId,
         @OrderBy,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
