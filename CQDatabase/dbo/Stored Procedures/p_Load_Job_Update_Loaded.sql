﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Load_Job_Update_Loaded
  ///   Filename       : p_Load_Job_Update_Loaded.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Load_Job_Update_Loaded
(
 @jobId  int,
 @status nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @OldStatusCode     nvarchar(10)
  
  set @Error = 0;
  set @Errormsg = 'Error executing p_Load_Job_Update_Loaded';
  set @GetDate = dbo.ufn_Getdate();
  
  begin transaction
  
  select @OldStatusCode = s.StatusCode
    from Job    j (nolock)
    join Status s (nolock) on j.StatusId = s.StatusId
   where j.JobId = @jobId
  
  if @OldStatusCode not in ('CD','DC','D','C')
  begin
     set @Error = -1
     set @Errormsg = 'Error executing p_Load_Job_Update_Loaded - Job in incorrect status';
     goto error
  end
  
  if @status = 'Load'
  begin
    set @StatusId = dbo.ufn_StatusId('IS','C')
  end
  else
  begin
    set @StatusId = dbo.ufn_StatusId('IS','DC')
  end
  
  exec @Error = p_Job_Update
   @JobId    = @jobId,
   @StatusId = @StatusId
  
  if @Error <> 0
    goto error
  
  commit transaction
  
  update i
     set Loaded = (select count(distinct (j.JobId))
                     from IssueLineInstruction ili (nolock)
                     join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
                     join Job                    j (nolock) on ins.JobId         = j.JobId
                     join Status                 s (nolock) on j.StatusId        = s.StatusId
                    where i.IssueId   = ili.IssueId
                      and s.StatusCode in ('C'))
    from Issue i
   where i.IssueId in (select distinct ili.IssueId
                         from IssueLIneInstruction ili (nolock)
                         join Instruction          ins (nolock) on ili.InstructionId= ins.InstructionId
                        where ins.JobId = @JobId)
  
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
