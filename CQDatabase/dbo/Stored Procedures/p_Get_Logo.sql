﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Get_Logo
  ///   Filename       : p_Get_Logo.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Get_Logo
(
 @ReportName	nvarchar(255)
)

 
as
begin
	 set nocount on;
  
select	Logo1,
		Logo2,
		Logo3,
		Logo4,
		Logo5
from Logo 
where ReportName = @ReportName
          

  end
