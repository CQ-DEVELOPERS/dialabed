﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactList_Parameter
  ///   Filename       : p_ContactList_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:18:15
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ContactList table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ContactList.ContactListId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContactList_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as ContactListId
        ,null as 'ContactList'
  union
  select
         ContactList.ContactListId
        ,ContactList.ContactListId as 'ContactList'
    from ContactList
  
end
