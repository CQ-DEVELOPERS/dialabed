﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Plan_Order_Update
  ///   Filename       : p_Production_Plan_Order_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Plan_Order_Update
(
 @issueId            int,
 @Rank               int,
 @dropSequence       int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @Increment int
	 
	 declare @TableResult as table
  (
   Id                 int identity,
   DropSequence       int,
   OutboundShipmentId int,
   IssueId            int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @rowcount          int,
          @OldIssueId        int,
          @WarehouseId       int
  
  begin transaction
  
  select @WarehouseId = WarehouseId  from Issue (nolock) where IssueId = @issueId
  
  --if isnull(@dropSequence,0) > @Rank
  --  set @dropSequence = @Rank - 1
  --else if isnull(@dropSequence,0) = @Rank
  --  set @dropSequence = @Rank + 1
  --else if isnull(@dropSequence,0) < @Rank
  --  set @dropSequence = @Rank + 1
  
  --select @IssueId '@IssueId', @dropSequence '@dropSequence', @Rank '@Rank'
  
  --exec @Error = p_Issue_Update
  -- @IssueId      = @issueId,
  -- @DropSequence = @Rank
  
  --if @Error <> 0
  --  goto error
  
  --select @OldIssueId = IssueId from Issue (nolock) where DropSequence in (@Rank,@dropSequence) and IssueId != @issueId
  --select @OldIssueId '@OldIssueId', @dropSequence '@dropSequence', @Rank '@Rank'
  --exec @Error = p_Issue_Update
  -- @IssueId      = @OldIssueId,
  -- @DropSequence = @Rank
  
  --if @Error <> 0
  --  goto error
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId)
  select osi.OutboundShipmentId,
         i.IssueId
   from OutboundDocument        od (nolock)
   join OutboundDocumentType   odt (nolock) on od.OutboundDocumentTypeId     = odt.OutboundDocumentTypeId
                                           and odt.OutboundDocumentTypeCode in ('MO','KIT')
   join Issue                    i (nolock) on od.OutboundDocumentId         = i.OutboundDocumentId
   join Status                   s (nolock) on i.StatusId                    = s.StatusId
   left
   join OutboundShipmentIssue  osi (nolock) on i.IssueId                     = osi.IssueId
  where s.Type                    = 'IS'
    and s.StatusCode             in ('W')--,'P','SA','M','RL','PC','PS','CK','A','WC','QA')
    and od.WarehouseId            = @WarehouseId
  order by i.DropSequence
  
  update @TableResult set DropSequence = Id
  
  select @dropSequence = Id from @TableResult where IssueId = @issueId
  
  if @Rank > @dropSequence
    set @Increment = -1
  else
    set @Increment = 1
  
  -- Update Issue with same rank as the new one
  update @TableResult set DropSequence = DropSequence + @Increment where DropSequence = @Rank
  
  -- Update Issue's new rank
  update @TableResult set DropSequence = @Rank where IssueId = @issueId
  
  update i
     set DropSequence = tr.DropSequence
    from @TableResult tr
    join Issue i on tr.IssueId = i.IssueId
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Production_Plan_Order_Update'
    rollback transaction
    return @Error
end
