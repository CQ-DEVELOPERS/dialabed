﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Delete
  ///   Filename       : p_InboundShipment_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:20
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InboundShipment table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Delete
(
 @InboundShipmentId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InboundShipment
     where InboundShipmentId = @InboundShipmentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
