﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Delete
  ///   Filename       : p_Location_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2013 08:13:22
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Location table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Delete
(
 @LocationId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Location
     where LocationId = @LocationId
  
  select @Error = @@Error
  
  
  return @Error
  
end
