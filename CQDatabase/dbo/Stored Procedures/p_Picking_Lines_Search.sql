﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Picking_Lines_Search
  ///   Filename       : p_Picking_Lines_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Picking_Lines_Search
(
 @OutboundShipmentId  int,
 @IssueId             int,
 @InstructionTypeCode nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   DocumentNumber nvarchar(30),
   OrderNumber    nvarchar(30),
   IssueLineId    int
  )
  
  declare @TableResult as table
  (
   DocumentNumber      nvarchar(30),
   OrderNumber         nvarchar(30),
   JobId               int,
   InstructionId       int,
   InstructionType     nvarchar(30),
   StorageUnitBatchId  int,
   ProductCode         nvarchar(30),
   Product             nvarchar(50),
   SKUCode             nvarchar(50),
   SKU                 nvarchar(50),
   Area                nvarchar(50),
   PickLocationId      int,
   PickLocation        nvarchar(15),
   Quantity            float,
   ConfirmedQuantity   float,
   PalletId            int,
   StatusId            int,
   Status              nvarchar(50),
   OperatorId          int,
   Operator            nvarchar(50)
  );
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is not null
    set @IssueId = null
  
--  if @IssueId is not null
--  begin
--    insert @TableHeader
--          (IssueLineId,
--           OrderNumber)
--    select il.IssueLineId,
--           od.OrderNumber
--      from IssueLine              il (nolock)
--      join Issue                   i (nolock) on il.IssueId  = i.IssueId
--      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
--     where il.IssueId = @IssueId
--    
--    insert @TableResult
--          (DocumentNumber,
--           OrderNumber,
--           JobId,
--           InstructionId,
--           InstructionType,
--           StorageUnitBatchId,
--           PickLocationId,
--           Quantity,
--           ConfirmedQuantity,
--           PalletId,
--           StatusId,
--           OperatorId)
--    select th.DocumentNumber,
--           th.OrderNumber,
--           i.JobId,
--           i.InstructionId,
--           it.InstructionType,
--           i.StorageUnitBatchId,
--           i.PickLocationId,
--           i.Quantity,
--           i.ConfirmedQuantity,
--           i.PalletId,
--           j.StatusId,
--           i.OperatorId
--      from @TableHeader th
--      join Instruction      i (nolock) on th.IssueLineId      = i.IssueLineId
--      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
--      join Job              j (nolock) on i.JobId             = j.JobId
--     where it.InstructionTypeCode = @InstructionTypeCode
--  end
--  else if @OutboundShipmentId is not null
--  begin
--    insert @TableHeader
--          (IssueLineId,
--           DocumentNumber,
--           OrderNumber)
--    select il.IssueLineId,
--           convert(nvarchar(30), osi.OutboundShipmentId),
--           od.OrderNumber
--      from OutboundShipmentIssue osi (nolock)
--      join IssueLine              il (nolock) on osi.IssueId = il.IssueId
--      join Issue                   i (nolock) on il.IssueId  = i.IssueId
--      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
--     where osi.OutboundShipmentId = @OutboundShipmentId
--    
--    insert @TableResult
--          (DocumentNumber,
--           OrderNumber,
--           JobId,
--           InstructionId,
--           InstructionType,
--           StorageUnitBatchId,
--           PickLocationId,
--           Quantity,
--           ConfirmedQuantity,
--           PalletId,
--           StatusId,
--           OperatorId)
--    select th.DocumentNumber,
--           th.OrderNumber,
--           i.JobId,
--           i.InstructionId,
--           it.InstructionType,
--           i.StorageUnitBatchId,
--           i.PickLocationId,
--           i.Quantity,
--           i.ConfirmedQuantity,
--           i.PalletId,
--           j.StatusId,
--           i.OperatorId
--      from @TableHeader th
--      join IssueLineInstruction ili (nolock) on th.IssueLineId      = ili.IssueLineId
--      join Instruction            i (nolock) on ili.InstructionId   = i.InstructionId
--      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
--      join Job                    j (nolock) on i.JobId             = j.JobId
--     where it.InstructionTypeCode = @InstructionTypeCode
--  end
  
  insert @TableResult
        (JobId,
         InstructionId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         Quantity,
         ConfirmedQuantity,
         PalletId,
         StatusId,
         OperatorId)
  select distinct i.JobId,
         i.InstructionId,
         it.InstructionType,
         i.StorageUnitBatchId,
         i.PickLocationId,
         i.Quantity,
         i.ConfirmedQuantity,
         i.PalletId,
         j.StatusId,
         i.OperatorId
    from IssueLineInstruction ili (nolock)
    join Instruction            i (nolock) on ili.InstructionId   = i.InstructionId
    join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job                    j (nolock) on i.JobId             = j.JobId
   where it.InstructionTypeCode = @InstructionTypeCode
     and isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, -1)
     and isnull(ili.IssueId, -1)            = isnull(@IssueId, ili.IssueId)
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SkuCode,
         SKU         = sku.SKU
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set PickLocation = l.Location,
         Area         = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId      = al.LocationId
    join Area          a (nolock) on al.AreaId         = a.AreaId
  
  select JobId,
         InstructionId,
         InstructionType,
         DocumentNumber,
         OrderNumber,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Quantity,
         ConfirmedQuantity,
         PalletId,
         Status,
         isnull(OperatorId,-1) as 'OperatorId',
         Operator,
         Area,
         PickLocation
    from @TableResult
end
