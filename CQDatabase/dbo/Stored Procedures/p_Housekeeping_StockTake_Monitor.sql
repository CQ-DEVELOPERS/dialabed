﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_StockTake_Monitor
  ///   Filename       : p_Housekeeping_StockTake_Monitor.sql
  ///   Create By      : Karen
  ///   Date Created   : April 2016
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_StockTake_Monitor
 
as
begin
  set nocount on;
  
  declare @trancount		int
         ,@ErrorId			int
         ,@ErrorSeverity	int
         ,@ErrorMessage		varchar(4000)
         ,@ErrorState		int
         ,@FromDate			datetime
  
  set @trancount = @@trancount;
  
  set @FromDate = DATEADD(minute,-6,getdate())
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_Housekeeping_StockTake_Monitor;
    
   if not exists(select top 1 1 from sys.tables where name = 'StockTakeLog')
   begin
     -- select * into StockTakeLog_Backup from StockTakeLog
     -- drop table StockTakeLog
     create table StockTakeLog
     (
      StockTakeLogId int identity
     ,ProductCode nvarchar(50)
     ,Product nvarchar(255)
     ,SKUCode nvarchar(50)
     ,Batch nvarchar(50)
     ,Quantity numeric(13,6)
     ,ConfirmedQuantity numeric(13,6)
     ,ActualQuantity numeric(13,6)
     ,AllocatedQuantity numeric(13,6)
     ,ReservedQuantity numeric(13,6)
     ,InsertDate datetime
     )
     
     alter table StockTakeLog add Location nvarchar(15) null;
     alter table StockTakeLog add Area nvarchar(50) null;
     alter table StockTakeLog add InstructionId int null;
     alter table StockTakeLog add StockTakeLogCode nvarchar(50) null;
   end
   
   create table #Instructions
   (
    LocationId int
   ,StorageUnitBatchId int
   ,InstructionId int
   )
   
   insert #Instructions
         (LocationId
         ,StorageUnitBatchId
         ,InstructionId)
   select vi.StoreLocationId
         ,vi.StorageUnitBatchId
         ,max(vi.InstructionId)
     from viewInstruction vi
     join StockTakeAuthorise sta on vi.InstructionId = sta.InstructionId
     join StorageUnitBatchLocation soh on vi.StorageUnitBatchId = soh.StorageUnitBatchId
                     and vi.StoreLocationId = soh.LocationId
    where CreateDate >= cast(@FromDate as DATE)
      and vi.InstructionTypeCode like 'ST%'
      and vi.Stored = 1
      and vi.ConfirmedQuantity > 0
      and soh.ActualQuantity = 0
      and vi.InstructionCode = 'F'
      --and Not exists (select top 1 1 
					 --   from viewInstruction vi2 
					 --  where vi2.InstructionId > vi.InstructionId
						-- and vi2.InstructionTypeCode not like 'ST%')
   group by vi.StoreLocationId
           ,vi.StorageUnitBatchId


   insert StockTakeLog
         ([StockTakeLogCode]
         ,[InstructionId]
         ,[Area]
         ,[Location]
         ,[ProductCode]
         ,[Product]
         ,[SKUCode]
         ,[Batch]
         ,[Quantity]
         ,[ConfirmedQuantity]
         ,[ActualQuantity]
         ,[AllocatedQuantity]
         ,[ReservedQuantity]
         ,[InsertDate])
   select 'Check Error'
         ,vi.InstructionId
         ,vi.StoreArea
         ,vi.StoreLocation
         ,vi.ProductCode
         ,vi.Product
         ,vi.SKUCode
         ,vi.Batch
         ,vi.Quantity
         ,vi.ConfirmedQuantity
         ,soh.ActualQuantity
         ,soh.AllocatedQuantity
         ,soh.ReservedQuantity
         ,GETDATE() as 'InsertDate'
     from #Instructions t
     join viewInstruction vi on t.InstructionId = vi.InstructionId
     join StorageUnitBatchLocation soh on vi.StorageUnitBatchId = soh.StorageUnitBatchId
                                      and vi.StoreLocationId    = soh.LocationId
     where not exists (select top 1 1 
						 from StockTakeLog stl (nolock) 
						where stl.InstructionId = vi.InstructionId
						  and stl.StockTakeLogCode = 'Check Error')
   
   if @@ROWCOUNT = 0
     goto lbexit
   
	   
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId = ERROR_NUMBER()
			  ,@ErrorSeverity = ERROR_SEVERITY()
			  ,@ErrorMessage  = ERROR_MESSAGE()
			  ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Housekeeping_StockTake_Monitor;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
 
 
 
