﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Culture_List
  ///   Filename       : p_Culture_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:37
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Culture table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Culture.CultureId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Culture_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as CultureId
        ,null as 'Culture'
  union
  select
         Culture.CultureId
        ,Culture.CultureId as 'Culture'
    from Culture
  
end
