﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Dangerous_Goods_Summary
  ///   Filename       : p_Report_Dangerous_Goods_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Dec 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Dangerous_Goods_Summary
(
 @OutboundShipmentId int = null,
 @IssueId            int = null
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   StorageUnitId                   int,
   DangerousGoodsCode              nvarchar(20),
   DangerousGoods                  nvarchar(100),
   Class                           float,
   UNNumber                        nvarchar(60),
   PackClass                       nvarchar(60),
   DangerFactor                    int,
   WeightPicked                    float,
   ConfirmedQuantity               float                          
  )
  
  if @OutboundShipmentId = -1
	set @OutboundShipmentId = null
	
  if @OutboundShipmentId is not null
    insert @TableResult
          (StorageUnitId,
           DangerousGoodsCode,
           DangerousGoods,
           Class,
           UNNumber,
           PackClass,
           DangerFactor,
           ConfirmedQuantity)
    select su.StorageUnitId,
           dg.DangerousGoodsCode,
           dg.DangerousGoods,
           dg.Class,
           dg.UNNumber,
           dg.PackClass,
           dg.DangerFactor,
           il.ConfirmedQuatity
      from OutboundShipmentIssue osi (nolock)
      join IssueLine              il (nolock) on osi.IssueId           = il.IssueId
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                 p (nolock) on su.ProductId          = p.ProductId
      join DangerousGoods         dg (nolock) on isnull(p.DangerousGoodsId,dg.DangerousGoodsId)    = dg.DangerousGoodsId
     where osi.OutboundShipmentId = isnull(@OutboundShipmentId, osi.OutboundShipmentId)
       and il.ConfirmedQuatity > 0
       and p.DangerousGoodsId > 0
  else
    insert @TableResult
          (StorageUnitId,
           DangerousGoodsCode,
           DangerousGoods,
           Class,
           UNNumber,
           PackClass,
           DangerFactor,
           ConfirmedQuantity)
    select su.StorageUnitId,
           dg.DangerousGoodsCode,
           dg.DangerousGoods,
           dg.Class,
           dg.UNNumber,
           dg.PackClass,
           dg.DangerFactor,
           il.ConfirmedQuatity
      from IssueLine              il (nolock)
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                 p (nolock) on su.ProductId          = p.ProductId
      join DangerousGoods         dg (nolock) on isnull(p.DangerousGoodsId,dg.DangerousGoodsId)    = dg.DangerousGoodsId
   where il.IssueId = isnull(@IssueId, il.IssueId)
     and il.ConfirmedQuatity > 0
     and p.DangerousGoodsId > 0
  
  update tr
     set WeightPicked = tr.ConfirmedQuantity * p.Weight
    from @TableResult      tr
    join Pack               p (nolock) on tr.StorageUnitId = p.StorageUnitId
   where p.Quantity    = 1
     --and p.WarehouseId = @WarehouseId
  
  select DangerousGoodsCode,
         DangerousGoods,
         Class,
         UNNumber,
         PackClass,
         sum(WeightPicked) as 'TotalWeight',
         sum(WeightPicked * DangerFactor) as 'Weight*Factor'
    from @TableResult
   group by DangerousGoodsCode,
            DangerousGoods,
            Class,
            UNNumber,
            PackClass
end
