﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber_Search_Job
  ///   Filename       : p_Register_SerialNumber_Search_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber_Search_Job
(
 @JobId int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        JobId                     int,
        InstructionId             int,
        OrderNumber               nvarchar(30),
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        StorageUnitBatchId        int,
        PalletId                  int
       );
  
  declare @StatusId int;
  
  select @StatusId = StatusId
    from Status
   where Type = 'I'
     and StatusCode = 'D';
  
  insert @TableResult
        (JobId,
         InstructionId,
         OrderNumber,
         SupplierCode,
         Supplier,
         StorageUnitBatchId,
         PalletId)
  select i.JobId,
         i.InstructionId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         i.StorageUnitBatchId,
         i.PalletId
    from Receipt r
    join ReceiptLine     rl on r.ReceiptId = rl.ReceiptId
    join InboundDocument id on r.InboundDocumentId = id.InboundDocumentId
    join ExternalCompany ec on id.ExternalCompanyId = ec.ExternalCompanyId
    join Instruction     i  on rl.ReceiptLineId     = i.ReceiptLineId
   where i.JobId     = @JobId
     and i.StatusId != @StatusId
  union
  select i.JobId,
         i.InstructionId,
         od.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         i.StorageUnitBatchId,
         i.PalletId
    from Issue           iss (nolock)
    join IssueLine        il (nolock) on iss.IssueId            = il.IssueId
    join OutboundDocument od (nolock) on iss.OutboundDocumentId = od.OutboundDocumentId
    join ExternalCompany  ec (nolock) on od.ExternalCompanyId   = ec.ExternalCompanyId
    join Instruction       i (nolock) on il.IssueLineId         = i.IssueLineId
   where i.JobId     = @JobId
     and i.StatusId != @StatusId
  
  select t.JobId,
         t.InstructionId,
         su.StorageUnitId,
         t.OrderNumber,
         t.SupplierCode,
         t.Supplier,
         t.PalletId,
         p.ProductCode,
         p.Product
    from @TableResult t
    join StorageUnitBatch sub on t.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId    = su.StorageUnitId
    join Product          p   on su.ProductId         = p.ProductId
end
