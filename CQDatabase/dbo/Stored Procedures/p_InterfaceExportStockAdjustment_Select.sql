﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportStockAdjustment_Select
  ///   Filename       : p_InterfaceExportStockAdjustment_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:16
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportStockAdjustment table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportStockAdjustmentId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportStockAdjustment.InterfaceExportStockAdjustmentId,
  ///   InterfaceExportStockAdjustment.RecordType,
  ///   InterfaceExportStockAdjustment.RecordStatus,
  ///   InterfaceExportStockAdjustment.ProductCode,
  ///   InterfaceExportStockAdjustment.SKUCode,
  ///   InterfaceExportStockAdjustment.Batch,
  ///   InterfaceExportStockAdjustment.Quantity,
  ///   InterfaceExportStockAdjustment.Weight,
  ///   InterfaceExportStockAdjustment.Additional1,
  ///   InterfaceExportStockAdjustment.Additional2,
  ///   InterfaceExportStockAdjustment.Additional3,
  ///   InterfaceExportStockAdjustment.Additional4,
  ///   InterfaceExportStockAdjustment.Additional5,
  ///   InterfaceExportStockAdjustment.ProcessedDate,
  ///   InterfaceExportStockAdjustment.InsertDate,
  ///   InterfaceExportStockAdjustment.Product 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportStockAdjustment_Select
(
 @InterfaceExportStockAdjustmentId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceExportStockAdjustment.InterfaceExportStockAdjustmentId
        ,InterfaceExportStockAdjustment.RecordType
        ,InterfaceExportStockAdjustment.RecordStatus
        ,InterfaceExportStockAdjustment.ProductCode
        ,InterfaceExportStockAdjustment.SKUCode
        ,InterfaceExportStockAdjustment.Batch
        ,InterfaceExportStockAdjustment.Quantity
        ,InterfaceExportStockAdjustment.Weight
        ,InterfaceExportStockAdjustment.Additional1
        ,InterfaceExportStockAdjustment.Additional2
        ,InterfaceExportStockAdjustment.Additional3
        ,InterfaceExportStockAdjustment.Additional4
        ,InterfaceExportStockAdjustment.Additional5
        ,InterfaceExportStockAdjustment.ProcessedDate
        ,InterfaceExportStockAdjustment.InsertDate
        ,InterfaceExportStockAdjustment.Product
    from InterfaceExportStockAdjustment
   where isnull(InterfaceExportStockAdjustment.InterfaceExportStockAdjustmentId,'0')  = isnull(@InterfaceExportStockAdjustmentId, isnull(InterfaceExportStockAdjustment.InterfaceExportStockAdjustmentId,'0'))
  
end
