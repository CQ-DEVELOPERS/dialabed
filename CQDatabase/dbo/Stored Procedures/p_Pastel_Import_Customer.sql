﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pastel_Import_Customer
  ///   Filename       : p_Pastel_Import_Customer.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 17 September 2010
  ///   Details        : Create detailed error log InterfaceDashboardBulkLog
  /// </newpara>
*/
CREATE procedure p_Pastel_Import_Customer
 
as
begin
	 set nocount on;
  
  declare @Error               int,
          @Errormsg            varchar(500),
          @GetDate             datetime,
          @CustomerCode        varchar(30),
          @CustomerName        varchar(255),
          @Class               varchar(255),
          @DeliveryPoint       varchar(255),
          @Address1            varchar(255),
          @Address2            varchar(255),
          @Address3            varchar(255),
          @Address4            varchar(255),
          @Modified            varchar(10) ,
          @ContactPerson       varchar(100),
          @Phone               varchar(255),
          @Fax                 varchar(255),
          @Email               varchar(255),
          @DeliveryGroup       varchar(255),
          @DepotCode           varchar(255),
          @VisitFrequency      varchar(255),
          @FloCustomerNumber   varchar(255),
          @ExternalCompanyId   int,
          @AddressId           int,
          @ContactListId       int,
          @HostId              varchar(30),
          -- InterfaceDashboardBulkLog  (Karen)
          @FileKeyId                    INT,
		  @FileType                     VARCHAR(100),
	      @ProcessedDate                DATETIME,
	      @FileName                     VARCHAR(50) ,
	      @ErrorCode                    CHAR (5),
	      @ErrorDescription             VARCHAR (255),
       @PrincipalCode                nvarchar(30),
       @PrincipalId                  int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  update InterfaceCustomer
     set ProcessedDate = @Getdate
   where RecordStatus in ('N','U')
     and ProcessedDate is null

  declare Customer_cursor cursor for
   select HostId,
          CustomerCode,
          CustomerName,
          Class,
          DeliveryPoint,
          Address1,
          Address2,
          Address3,
          Address4,
          Modified,
          ContactPerson,
          Phone,
          Fax,
          Email,
          DeliveryGroup,
          DepotCode,
          VisitFrequency,
          PrincipalCode
     from InterfaceCustomer
    where ProcessedDate = @Getdate
   order by CustomerCode

  open Customer_cursor

  fetch Customer_cursor into @HostId,
                             @CustomerCode,
                             @CustomerName,
                             @Class,
                             @DeliveryPoint,
                             @Address1,
                             @Address2,
                             @Address3,
                             @Address4,
                             @Modified,
                             @ContactPerson,
                             @Phone,
                             @Fax,
                             @Email,
                             @DeliveryGroup,
                             @DepotCode,
                             @VisitFrequency,
                             @PrincipalCode

  while (@@fetch_status = 0)
  begin
    begin transaction
    
    set @PrincipalId = null
           
    select @PrincipalId = PrincipalId
      from Principal (nolock)
     where PrincipalCode = @PrincipalCode
    
    set @ExternalCompanyId = null
    
    select @ExternalCompanyId = ExternalCompanyId
      from ExternalCompany (nolock)
     where ExternalCompanyCode = @CustomerCode
       and ExternalCompanyTypeId = 1
       and PrincipalId = @PrincipalId
    
    if @ExternalCompanyId is null
      select @ExternalCompanyId = ExternalCompanyId
        from ExternalCompany (nolock)
       where ExternalCompanyCode = @CustomerCode
         and ExternalCompanyTypeId = 1
         and PrincipalId is null
     
    if @ExternalCompanyId is null
    begin
      exec @Error = p_ExternalCompany_Insert
       @ExternalCompanyId         = @ExternalCompanyId output,
       @ExternalCompanyTypeId     = 1,
       @ExternalCompany           = @CustomerName,
       @ExternalCompanyCode       = @CustomerCode,
       @PrincipalId               = @PrincipalId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
        
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error inserting ExternalCompany - ExternalCompanyId is blank'
								,@ExternalCompanyId
								,'ExternalCompany'
								,'E'
								,NULL
								,@GetDate
								,@GetDate) 
        
        goto error
      end
    end
    else
    begin
      exec @Error = p_ExternalCompany_Update
       @ExternalCompanyId         = @ExternalCompanyId,
       @ExternalCompanyTypeId     = 1,
       @ExternalCompany           = @CustomerName,
       @ExternalCompanyCode       = @CustomerCode,
       @PrincipalId               = @PrincipalId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
        
       
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error updating ExternalCompany'
								,@ExternalCompanyId
								,'ExternalCompany'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)  
        
        goto error
      end
    end
    
    update ExternalCompany
       set HostId = @HostId
     where ExternalCompanyId = @ExternalCompanyId
    
    set @AddressId = null
    
    select @AddressId = AddressId
      from Address
     where ExternalCompanyId = @ExternalCompanyId
    
    if @AddressId is null
    begin
      exec @Error = p_Address_Insert
       @AddressId         = @AddressId output,
       @ExternalCompanyId = @ExternalCompanyId,
       @Street            = @Address1,
       @Suburb            = @Address2,
       @Town              = @Address3,
       @Country           = @Address4
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
        
         
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error updating ExternalCompany'
								,@ExternalCompanyId
								,'ExternalCompany'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
        
        goto error
      end
    end
    else
    begin
      exec @Error = p_Address_Update
       @AddressId         = @AddressId,
       @ExternalCompanyId = @ExternalCompanyId,
       @Street            = @Address1,
       @Suburb            = @Address2,
       @Town              = @Address3,
       @Country           = @Address4
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
        
       
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'rror updating ExternalCompany'
								,@ExternalCompanyId
								,'ExternalCompany'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
        
        goto error
      end
    end
    
    select @ContactListId = ContactListId
      from ContactList
     where ExternalCompanyId = @ExternalCompanyId
       and ContactPerson = @ContactPerson
    
    if @ContactListId is null
    begin
      exec @Error = p_ContactList_Insert
       @ContactListId     = @ContactListId output,
       @ExternalCompanyId = @ExternalCompanyId,
       @OperatorId        = null,
       @ContactPerson     = @ContactPerson,
       @Telephone         = @Phone,
       @Fax               = @Fax,
       @EMail             = @Email
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ContactList_Insert'
        
        
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error inserting ContactList - ContactListId is blank'
								,@ExternalCompanyId
								,'ExternalCompany'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
        
        goto error
      end
    end
    else
    begin
      exec @Error = p_ContactList_Update
       @ContactListId     = @ContactListId,
       @ExternalCompanyId = @ExternalCompanyId,
       @OperatorId        = null,
       @ContactPerson     = @ContactPerson,
       @Telephone         = @Phone,
       @Fax               = @Fax,
       @EMail             = @Email
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ContactList_Update'
        
					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Failed'
								,'Error updating ContactList'
								,@ContactListId
								,'ExternalCompany'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
        
        goto error
      end
    end
    
--    if exists(select top 1 1
--                from FloCustomerImport
--               where CustomerNumber = @CustomerCode)
--    begin
--      update FloCustomerImport
--         set Name           = convert(varchar(50), @CustomerName),
--             Class          = @Class,
--             DeliveryPoint  = @DeliveryPoint,
--             Address1       = convert(varchar(50), @Address1),
--             Address2       = convert(varchar(50), @Address2),
--             Address3       = convert(varchar(50), @Address3),
--             Address4       = convert(varchar(50), @Address4),
--             Contact        = convert(varchar(30), @ContactPerson),
--             Tel            = convert(varchar(20), @Phone),
--             Fax            = convert(varchar(20), @Fax),
--             DeliveryGroup  = @DeliveryGroup,
--             DepotCode      = @DepotCode,
--             VisitFrequency = @VisitFrequency
--       where CustomerNumber = @CustomerCode
--    end
--    else
--    begin
--      insert FloCustomerImport
--            (CustomerNumber,
--             Name,
--             Class,
--             DeliveryPoint,
--             Address1,
--             Address2,
--             Address3,
--             Address4,
--             Contact,
--             Tel,
--             Fax,
--             DeliveryGroup,
--             DepotCode,
--             VisitFrequency,
--             FloCustomerNumber)
--      select @CustomerCode,
--             convert(varchar(50), @CustomerName),
--             @Class,
--             @DeliveryPoint,
--             convert(varchar(50), @Address1),
--             convert(varchar(50), @Address2),
--             convert(varchar(50), @Address3),
--             convert(varchar(50), @Address4),
--             convert(varchar(30), @ContactPerson),
--             convert(varchar(20), @Phone),
--             convert(varchar(20), @Fax),
--             @DeliveryGroup,
--             @DepotCode,
--             @VisitFrequency,
--             @FloCustomerNumber
--    end
    
    error:
    if @Error = 0
    begin
      update InterfaceCustomer
         set RecordStatus = 'Y'
       where CustomerCode = @CustomerCode
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
      
      commit transaction
      
      					 -- Karen - added error message create into InterfaceMessage
					 INSERT INTO InterfaceMessage
							    ([InterfaceMessageCode]
							    ,[InterfaceMessage]
							    ,[InterfaceId]
								,[InterfaceTable]
								,[Status]
								,[OrderNumber]
								,[CreateDate]
								,[ProcessedDate])
					VALUES
								('Processed'
								,'Success'
								,@ContactListId
								,'ExternalCompany'
								,'E'
								,NULL
								,@GetDate
								,@GetDate)
								
    end
    else
    begin
      if @@trancount > 0
        rollback transaction
      
      update InterfaceCustomer
         set RecordStatus = 'E'
       where CustomerCode = @CustomerCode
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
    end
    
    fetch Customer_cursor into @HostId,
                               @CustomerCode,
                               @CustomerName,
                               @Class,
                               @DeliveryPoint,
                               @Address1,
                               @Address2,
                               @Address3,
                               @Address4,
                               @Modified,
                               @ContactPerson,
                               @Phone,
                               @Fax,
                               @Email,
                               @DeliveryGroup,
                               @DepotCode,
                               @VisitFrequency,
                               @PrincipalCode
  end

  close Customer_cursor
  deallocate Customer_cursor
end
 
