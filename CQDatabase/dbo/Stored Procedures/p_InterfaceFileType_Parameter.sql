﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceFileType_Parameter
  ///   Filename       : p_InterfaceFileType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:47:29
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceFileType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceFileType.InterfaceFileTypeId,
  ///   InterfaceFileType.InterfaceFileType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceFileType_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as InterfaceFileTypeId
        ,'{All}' as InterfaceFileType
  union
  select
         InterfaceFileType.InterfaceFileTypeId
        ,InterfaceFileType.InterfaceFileType
    from InterfaceFileType
  order by InterfaceFileType
  
end
