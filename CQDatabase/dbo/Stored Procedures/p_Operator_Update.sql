﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Update
  ///   Filename       : p_Operator_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 13:19:57
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Operator table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @OperatorGroupId int = null,
  ///   @WarehouseId int = null,
  ///   @Operator nvarchar(100) = null,
  ///   @OperatorCode nvarchar(100) = null,
  ///   @Password nvarchar(100) = null,
  ///   @NewPassword nvarchar(100) = null,
  ///   @ActiveIndicator bit = null,
  ///   @ExpiryDate datetime = null,
  ///   @LastInstruction datetime = null,
  ///   @Printer nvarchar(100) = null,
  ///   @Port nvarchar(100) = null,
  ///   @IPAddress nvarchar(100) = null,
  ///   @CultureId int = null,
  ///   @LoginTime datetime = null,
  ///   @LogoutTime datetime = null,
  ///   @LastActivity datetime = null,
  ///   @SiteType nvarchar(40) = null,
  ///   @Name nvarchar(200) = null,
  ///   @Surname nvarchar(200) = null,
  ///   @PrincipalId int = null,
  ///   @PickAisle nvarchar(20) = null,
  ///   @StoreAisle nvarchar(20) = null,
  ///   @MenuId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @Upload bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Update
(
 @OperatorId int = null,
 @OperatorGroupId int = null,
 @WarehouseId int = null,
 @Operator nvarchar(100) = null,
 @OperatorCode nvarchar(100) = null,
 @Password nvarchar(100) = null,
 @NewPassword nvarchar(100) = null,
 @ActiveIndicator bit = null,
 @ExpiryDate datetime = null,
 @LastInstruction datetime = null,
 @Printer nvarchar(100) = null,
 @Port nvarchar(100) = null,
 @IPAddress nvarchar(100) = null,
 @CultureId int = null,
 @LoginTime datetime = null,
 @LogoutTime datetime = null,
 @LastActivity datetime = null,
 @SiteType nvarchar(40) = null,
 @Name nvarchar(200) = null,
 @Surname nvarchar(200) = null,
 @PrincipalId int = null,
 @PickAisle nvarchar(20) = null,
 @StoreAisle nvarchar(20) = null,
 @MenuId int = null,
 @ExternalCompanyId int = null,
 @Upload bit = null 
)
 
as
begin
	 set nocount on;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Operator = '-1'
    set @Operator = null;
  
  if @OperatorCode = '-1'
    set @OperatorCode = null;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
	 declare @Error int
 
  update Operator
     set OperatorGroupId = isnull(@OperatorGroupId, OperatorGroupId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         Operator = isnull(@Operator, Operator),
         OperatorCode = isnull(@OperatorCode, OperatorCode),
         Password = isnull(@Password, Password),
         NewPassword = isnull(@NewPassword, NewPassword),
         ActiveIndicator = isnull(@ActiveIndicator, ActiveIndicator),
         ExpiryDate = isnull(@ExpiryDate, ExpiryDate),
         LastInstruction = isnull(@LastInstruction, LastInstruction),
         Printer = isnull(@Printer, Printer),
         Port = isnull(@Port, Port),
         IPAddress = isnull(@IPAddress, IPAddress),
         CultureId = isnull(@CultureId, CultureId),
         LoginTime = isnull(@LoginTime, LoginTime),
         LogoutTime = isnull(@LogoutTime, LogoutTime),
         LastActivity = isnull(@LastActivity, LastActivity),
         SiteType = isnull(@SiteType, SiteType),
         Name = isnull(@Name, Name),
         Surname = isnull(@Surname, Surname),
         PrincipalId = isnull(@PrincipalId, PrincipalId),
         PickAisle = isnull(@PickAisle, PickAisle),
         StoreAisle = isnull(@StoreAisle, StoreAisle),
         MenuId = isnull(@MenuId, MenuId),
         ExternalCompanyId = isnull(@ExternalCompanyId, ExternalCompanyId),
         Upload = isnull(@Upload, Upload) 
   where OperatorId = @OperatorId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OperatorHistory_Insert
         @OperatorId = @OperatorId,
         @OperatorGroupId = @OperatorGroupId,
         @WarehouseId = @WarehouseId,
         @Operator = @Operator,
         @OperatorCode = @OperatorCode,
         @Password = @Password,
         @NewPassword = @NewPassword,
         @ActiveIndicator = @ActiveIndicator,
         @ExpiryDate = @ExpiryDate,
         @LastInstruction = @LastInstruction,
         @Printer = @Printer,
         @Port = @Port,
         @IPAddress = @IPAddress,
         @CultureId = @CultureId,
         @LoginTime = @LoginTime,
         @LogoutTime = @LogoutTime,
         @LastActivity = @LastActivity,
         @SiteType = @SiteType,
         @Name = @Name,
         @Surname = @Surname,
         @PrincipalId = @PrincipalId,
         @PickAisle = @PickAisle,
         @StoreAisle = @StoreAisle,
         @MenuId = @MenuId,
         @ExternalCompanyId = @ExternalCompanyId,
         @Upload = @Upload,
         @CommandType = 'Update'
  
  return @Error
  
end
