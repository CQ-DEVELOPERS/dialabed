--IF OBJECT_ID('dbo.p_Planning_All_Stock_Available') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Planning_All_Stock_Available
--    IF OBJECT_ID('dbo.p_Planning_All_Stock_Available') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Planning_All_Stock_Available >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Planning_All_Stock_Available >>>'
--END
--go
 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_All_Stock_Available
  ///   Filename       : p_Planning_All_Stock_Available.sql
  ///   Create By      : Karen
  ///   Date Created   : May 2021
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Planning_All_Stock_Available]
(
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @AvailabilityIndicator nvarchar(20) output
)
as
begin
	 set nocount on;

 declare @TableResult as table
  (
   WarehouseId                     int,
   IssueLineId                     int,
   IssueId                         int,
   StorageUnitId                   int,
   OrderQuantity                   float,
   AllocatedQuantity               float,
   AvailableQuantity               float,
   AvailabilityIndicator           nvarchar(20),
   AreaType                        nvarchar(10),
   AvailabilityIndicatorSeq			int
  )
  
   declare @WarehouseId int
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null;
  else
    select @WarehouseId = WarehouseId from OutboundShipment (nolock) where OutboundShipmentId = @OutboundShipmentId
  
  if @IssueId = -1
    set @IssueId = null;
  else
    select @WarehouseId = WarehouseId from Issue (nolock) where IssueId = @IssueId
 
   if @IssueId is not null
  begin
 insert @TableResult
          (WarehouseId,
           IssueLineId,
           IssueId,
           StorageUnitId,
           OrderQuantity,
           AreaType,
           AvailabilityIndicator,
           AvailableQuantity)
    select i.WarehouseId,
           il.IssueLineId,
           il.IssueId,
           sub.StorageUnitId,
           ol.Quantity,
           isnull(i.AreaType,''),
           'Red',0
      from IssueLine        il
      join Issue            i   (nolock) on il.IssueId            = i.IssueId
      join OutboundLine     ol  (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
     where il.IssueId = @IssueId
	 end
	  else if @OutboundShipmentId is not null
  begin
    insert @TableResult
          (WarehouseId,
           IssueLineId,
           IssueId,
           StorageUnitId,
           OrderQuantity,
           AreaType,
           AvailabilityIndicator,
           AvailableQuantity)
    select i.WarehouseId,
           il.IssueLineId,
           il.IssueId,
           sub.StorageUnitId,
           ol.Quantity,
           isnull(i.AreaType,''),
           'Red',
           0
      from IssueLine        il
      join Issue            i   (nolock) on il.IssueId            = i.IssueId
      join OutboundLine     ol  (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument od  (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status           s   (nolock) on il.StatusId           = s.StatusId
      join OutboundShipmentIssue osi (nolock) on il.IssueId = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end


  update tr
     set AvailableQuantity = (select sum(isnull(subl.ActualQuantity,0) - isnull(subl.ReservedQuantity,0))
                                from StorageUnitBatchLocation subl (nolock)
                                join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                                join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
                                join Area                        a (nolock) on al.AreaId               = a.AreaId
   where a.StockOnHand     = 1
                                 and sub.StorageUnitId = tr.StorageUnitId
                                 and a.WarehouseId     = 1
                                 and a.AreaCode in ('RK','PK','R','SP','BK')
                                 and isnull(a.AreaType,'') = tr.AreaType)
    from @TableResult tr

	 update @TableResult
     set AvailabilityIndicator = 'Red',
		 AvailabilityIndicatorSeq = 1
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow',
		 AvailabilityIndicatorSeq = 2
   where AvailableQuantity > 0
  
  update @TableResult
     set AvailabilityIndicator = 'Green',
		 AvailabilityIndicatorSeq = 3
   where (AvailableQuantity >= OrderQuantity
      or OrderQuantity = AllocatedQuantity)

	set @AvailabilityIndicator = (select top 1 AvailabilityIndicator from @TableResult order by AvailabilityIndicatorSeq)
	

end
--go
--IF OBJECT_ID('dbo.p_Planning_All_Stock_Available') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Planning_All_Stock_Available >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Planning_All_Stock_Available >>>'
--go


