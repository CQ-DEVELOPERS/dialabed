﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportSOHeader_Select
  ///   Filename       : p_InterfaceExportSOHeader_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:14
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportSOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportSOHeaderId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportSOHeader.InterfaceExportSOHeaderId,
  ///   InterfaceExportSOHeader.IssueId,
  ///   InterfaceExportSOHeader.PrimaryKey,
  ///   InterfaceExportSOHeader.OrderNumber,
  ///   InterfaceExportSOHeader.RecordType,
  ///   InterfaceExportSOHeader.RecordStatus,
  ///   InterfaceExportSOHeader.CustomerCode,
  ///   InterfaceExportSOHeader.Customer,
  ///   InterfaceExportSOHeader.Address,
  ///   InterfaceExportSOHeader.FromWarehouseCode,
  ///   InterfaceExportSOHeader.ToWarehouseCode,
  ///   InterfaceExportSOHeader.Route,
  ///   InterfaceExportSOHeader.DeliveryDate,
  ///   InterfaceExportSOHeader.Remarks,
  ///   InterfaceExportSOHeader.NumberOfLines,
  ///   InterfaceExportSOHeader.Additional1,
  ///   InterfaceExportSOHeader.Additional2,
  ///   InterfaceExportSOHeader.Additional3,
  ///   InterfaceExportSOHeader.Additional4,
  ///   InterfaceExportSOHeader.Additional5,
  ///   InterfaceExportSOHeader.Additional6,
  ///   InterfaceExportSOHeader.Additional7,
  ///   InterfaceExportSOHeader.Additional8,
  ///   InterfaceExportSOHeader.Additional9,
  ///   InterfaceExportSOHeader.Additional10,
  ///   InterfaceExportSOHeader.ProcessedDate,
  ///   InterfaceExportSOHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportSOHeader_Select
(
 @InterfaceExportSOHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceExportSOHeader.InterfaceExportSOHeaderId
        ,InterfaceExportSOHeader.IssueId
        ,InterfaceExportSOHeader.PrimaryKey
        ,InterfaceExportSOHeader.OrderNumber
        ,InterfaceExportSOHeader.RecordType
        ,InterfaceExportSOHeader.RecordStatus
        ,InterfaceExportSOHeader.CustomerCode
        ,InterfaceExportSOHeader.Customer
        ,InterfaceExportSOHeader.Address
        ,InterfaceExportSOHeader.FromWarehouseCode
        ,InterfaceExportSOHeader.ToWarehouseCode
        ,InterfaceExportSOHeader.Route
        ,InterfaceExportSOHeader.DeliveryDate
        ,InterfaceExportSOHeader.Remarks
        ,InterfaceExportSOHeader.NumberOfLines
        ,InterfaceExportSOHeader.Additional1
        ,InterfaceExportSOHeader.Additional2
        ,InterfaceExportSOHeader.Additional3
        ,InterfaceExportSOHeader.Additional4
        ,InterfaceExportSOHeader.Additional5
        ,InterfaceExportSOHeader.Additional6
        ,InterfaceExportSOHeader.Additional7
        ,InterfaceExportSOHeader.Additional8
        ,InterfaceExportSOHeader.Additional9
        ,InterfaceExportSOHeader.Additional10
        ,InterfaceExportSOHeader.ProcessedDate
        ,InterfaceExportSOHeader.InsertDate
    from InterfaceExportSOHeader
   where isnull(InterfaceExportSOHeader.InterfaceExportSOHeaderId,'0')  = isnull(@InterfaceExportSOHeaderId, isnull(InterfaceExportSOHeader.InterfaceExportSOHeaderId,'0'))
  
end
