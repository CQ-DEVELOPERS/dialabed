﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackType_Delete
  ///   Filename       : p_PackType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:40:54
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the PackType table.
  /// </remarks>
  /// <param>
  ///   @PackTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackType_Delete
(
 @PackTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete PackType
     where PackTypeId = @PackTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
