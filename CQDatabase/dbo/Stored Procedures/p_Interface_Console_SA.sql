﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Console_SA
  ///   Filename       : p_Interface_Console_SA.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Apr 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Interface_Console_SA
(
 @WarehouseId int = null,
 @productCode nvarchar(50),
 @product     nvarchar(255),
 @batch       nvarchar(50),
 @additional1 nvarchar(max),
 @additional2 nvarchar(max),
 @additional3 nvarchar(max),
 @status      nvarchar(50),
 @message     nvarchar(max),
 @recordType  nvarchar(50),
 @fromDate    datetime,
 @toDate      datetime
)
 
as
begin
	 set nocount on;
  
  select im.InterfaceMessageId,
         im.InterfaceMessageCode,
         im.InterfaceMessage,
         im.Status,
         isnull(im.InterfaceMessageCode, 'No Response') as 'InterfaceMessageCode',
         im.CreateDate as 'MessageDate',

         (select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceExportStockAdjustment') as 'InterfaceTableId',
         h.InterfaceExportStockAdjustmentId as 'InterfaceId',
         h.RecordType,
         h.RecordStatus,
         h.ProcessedDate,

         h.InsertDate,
         h.ProductCode,
         h.Product,
         h.Batch,
         h.Quantity, h.Additional1, h.Additional2, h.Additional3, h.Additional4, h.Additional5
  from InterfaceExportStockAdjustment h (nolock)
  left

  join InterfaceMessage       im (nolock) on h.InterfaceExportStockAdjustmentId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceExportStockAdjustment'
                                         
  where h.InsertDate between @fromDate and @toDate
    and isnull(h.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(h.ProductCode,'%')) + '%'
    and isnull(h.Product,'%')     like '%' + isnull(@product, isnull(h.Product,'%')) + '%'
    and isnull(h.Batch,'%')     like '%' + isnull(@Batch, isnull(h.Batch,'%')) + '%'
    and isnull(h.Additional1,'%')     like '%' + isnull(@additional1, isnull(h.Additional1,'%')) + '%'
    and isnull(h.Additional2,'%')     like '%' + isnull(@additional2, isnull(h.Additional2,'%')) + '%'
    and isnull(h.Additional3,'%')     like '%' + isnull(@additional3, isnull(h.Additional3,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
end
 
