﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingLog_Parameter
  ///   Filename       : p_CheckingLog_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:16
  /// </summary>
  /// <remarks>
  ///   Selects rows from the CheckingLog table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   CheckingLog.CheckingLogId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingLog_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as CheckingLogId
        ,null as 'CheckingLog'
  union
  select
         CheckingLog.CheckingLogId
        ,CheckingLog.CheckingLogId as 'CheckingLog'
    from CheckingLog
  
end
