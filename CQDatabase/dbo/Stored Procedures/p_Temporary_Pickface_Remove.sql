﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Temporary_Pickface_Remove
  ///   Filename       : p_Temporary_Pickface_Remove.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Temporary_Pickface_Remove
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @WarehouseId       int,
          @PickLocationId    int,
          @ActualQuantity    float,
          @StoreLocationId   int,
          @InstructionTypeId int,
          @JobId             int,
          @StatusId          int,
          @PriorityId        int,
          @StorageUnitBatchId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @PickLocationId = PickLocationId,
         @WarehouseId    = WarehouseId,
         @StorageUnitBatchId = StorageUnitBatchId
    from Instruction
   where InstructionId = @InstructionId
  
  if (select a.AreaCode
        from AreaLocation al
        join Area          a on al.AreaId = a.AreaId
       where al.LocationId = @PickLocationId) != 'TP'
    return 0
  
  begin transaction
  
  if @PickLocationId is not null
  begin
    select @ActualQuantity = ActualQuantity
      from StorageUnitBatchLocation
     where StorageUnitBatchId = @StorageUnitBatchId
       and LocationId         = @PickLocationId
    
    if @ActualQuantity <= 0 or @ActualQuantity is null
    begin
      set @Error = 0
      goto Result
    end
    
    exec @Error = p_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @StoreLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     --@FullPalletInd      = 1,
     @Quantity           = @ActualQuantity
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
    
    select @InstructionTypeId = InstructionTypeId,
           @PriorityId        = PriorityId
      from InstructionType (nolock)
     where InstructionTypeCode = 'M'
    
    set @StatusId = dbo.ufn_StatusId('IS','RL')
    
    exec @Error = p_Job_Insert
     @JobId       = @JobId output,
     @PriorityId  = @PriorityId,
     @OperatorId  = null,
     @StatusId    = @StatusId,
     @WarehouseId = @WarehouseId
    
    if @Error <> 0 or @JobId is null
    begin
      set @InstructionId = -1
      goto Result
    end
    
    set @StatusId = dbo.ufn_StatusId('I','W')
    
    exec @Error = p_Instruction_Insert
     @InstructionId       = @InstructionId output,
     @InstructionTypeId   = @InstructionTypeId,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @WarehouseId         = @WarehouseId,
     @StatusId            = @StatusId,
     @JobId               = @JobId,
     @OperatorId          = null,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = @StoreLocationId,
     @InstructionRefId    = null,
     @Quantity            = @ActualQuantity,
     @ConfirmedQuantity   = @ActualQuantity,
     @Weight              = null,
     @ConfirmedWeight     = 0,
     @CreateDate          = @GetDate
    
    if @Error <> 0 or @InstructionId is null
    begin
      set @InstructionId = -1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto Result
    
    if @StoreLocationId is null
      exec @error = p_Instruction_no_stock
       @InstructionId = @InstructionId
  end
  
  Result:
    if @InstructionId is null
      set @InstructionId = -1
    
    if @Error <> 0 or @InstructionId = -1
      rollback transaction
    else
      commit transaction
    
    return @Error
end
