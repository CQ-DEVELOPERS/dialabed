﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestType_Delete
  ///   Filename       : p_ManualFileRequestType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:04
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ManualFileRequestType table.
  /// </remarks>
  /// <param>
  ///   @ManualFileRequestTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestType_Delete
(
 @ManualFileRequestTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ManualFileRequestType
     where ManualFileRequestTypeId = @ManualFileRequestTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
