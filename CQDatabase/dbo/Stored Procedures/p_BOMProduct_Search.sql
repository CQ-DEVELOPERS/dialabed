﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMProduct_Search
  ///   Filename       : p_BOMProduct_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:14
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the BOMProduct table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null output,
  ///   @LineNumber int = null output,
  ///   @StorageUnitId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   BOMProduct.BOMLineId,
  ///   BOMProduct.LineNumber,
  ///   BOMProduct.StorageUnitId,
  ///   BOMProduct.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMProduct_Search
(
 @BOMLineId int = null output,
 @LineNumber int = null output,
 @StorageUnitId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @BOMLineId = '-1'
    set @BOMLineId = null;
  
  if @LineNumber = '-1'
    set @LineNumber = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
 
  select
         BOMProduct.BOMLineId
        ,BOMProduct.LineNumber
        ,BOMProduct.StorageUnitId
        ,BOMProduct.Quantity
    from BOMProduct
   where isnull(BOMProduct.BOMLineId,'0')  = isnull(@BOMLineId, isnull(BOMProduct.BOMLineId,'0'))
     and isnull(BOMProduct.LineNumber,'0')  = isnull(@LineNumber, isnull(BOMProduct.LineNumber,'0'))
     and isnull(BOMProduct.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(BOMProduct.StorageUnitId,'0'))
  
end
