﻿--IF OBJECT_ID('dbo.p_Interface_WebService_SOH') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Interface_WebService_SOH
--    IF OBJECT_ID('dbo.p_Interface_WebService_SOH') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Interface_WebService_SOH >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Interface_WebService_SOH >>>'
--END
--go
CREATE procedure [dbo].[p_Interface_WebService_SOH]
(
 @doc2			varchar(max) output,
 @PrincipalCode NVARCHAR(30) = NULL
)

as
begin
  declare @doc xml,
	@DCCode nvarchar(50)
	

  set @doc = convert(xml,@doc2)

  DECLARE @idoc int,
          @InsertDate datetime

  Set @InsertDate = cast(Getdate() as date)

  EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
   
	
	SELECT @DCCode = DCCode
	  FROM  OPENXML (@idoc, '/root/Body/Item',1)
      WITH (DCCode varchar(50) 'DCCode')

	  SELECT @PrincipalCode = PrincipalCode
	  FROM  OPENXML (@idoc, '/root/Body/Item',1)
      WITH (PrincipalCode varchar(50) 'PrincipalCode')


		Insert InterfaceImportSOH
		      (RecordStatus,
			   InsertDate,
			   Location,
			   ProductCode,
			   SKUCode,
			   Batch,
			   Quantity,
			   UnitPrice,
			   PrincipalCode,
			   StdCostPrice,
			   SupplierCostPrice,
			   DCCode)
		SELECT 'W',
			    @InsertDate,
			    Location,
			    ProductCode,
			    SKUCode,
			    Batch,
			    Replace(Quantity,',','.'),
			    UnitPrice,
			    @PrincipalCode,
				StdCostPrice,
				SupplierCostPrice,
				@DCCode
		  from OPENXML (@idoc, '/root/Body/Item/ItemLine',1)
            WITH (Location     nvarchar(50) 'Location',
				  ProductCode  nvarchar(50) 'ProductCode',
				  SKUCode	   nvarchar(50) 'SKUCode',
                  Batch        nvarchar(50) 'Batch',
				  Quantity     nvarchar(50) 'Quantity',
				  UnitPrice    nvarchar(50) 'UnitPrice',
				  StdCostPrice    nvarchar(50) 'StdCostPrice',
				  SupplierCostPrice    nvarchar(50) 'SupplierCostPrice')
		



		update InterfaceImportSOH
		   set RecordStatus = 'N'
		 where RecordSTatus = 'W'
		   and InsertDate = @InsertDate
		
		set @doc2  = '<root><status>Success</status></root>'
End
--go
--IF OBJECT_ID('dbo.p_Interface_WebService_SOH') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Interface_WebService_SOH >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Interface_WebService_SOH >>>'
--go


