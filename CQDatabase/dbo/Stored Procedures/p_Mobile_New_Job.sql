﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_New_Job
    ///   Filename       : p_Mobile_New_Job.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 11 June 2007 14:15:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    ///   @operatorId          int,
    ///   @referenceNumber     nvarchar(30),
    ///   @instructionId       int output
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_New_Job
(
 @operatorId      int,
 @referenceNumber nvarchar(30) = null,
 @containerTypeId int = null,
 @instructionId   int,
 @jobId           int output
)
as
begin
	 set nocount on
	 
	 declare @TableInstruction as table
	 (
	  InstructionId int,
   StatusCode    nvarchar(10)
	 )
  
  declare @Error           int,
          @Errormsg        nvarchar(500),
          @GetDate         datetime,
          @OldJobId        int,
          @NewJobId        int,
          @StatusId        int,
          @PriorityId      int,
          @WarehouseId     int,
          @ReceiptLineId   int,
          @IssueLineId     int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @referenceNumber = ''
    set @referenceNumber = null
  
  if @jobId is null
    select @OldJobId = JobId
      from Instruction (nolock)
     where InstructionId = @instructionId
  else
    set @OldJobId = @jobId
  
  insert @TableInstruction
        (InstructionId,
         StatusCode)
  select i.InstructionId,
         s.StatusCode
    from Instruction i
    join Status      s on i.StatusId = s.StatusId
   where i.JobId = @OldJobId
  
  if not exists(select top 1 1 from @TableInstruction where StatusCode = 'F')
  begin
    set @jobId = -1
    return @jobId
  end
  
  select @PriorityId      = PriorityId,
         @WarehouseId     = WarehouseId,
         @ReceiptLineId   = ReceiptLineId,
         @IssueLineId     = IssueLineId
    from Job (nolock)
   where JobId = @OldJobId
  
  select @StatusId = dbo.ufn_StatusId('I','S')
  
  begin transaction
  
  exec @Error = p_Job_Insert
   @JobId           = @NewJobId output,
   @PriorityId      = @PriorityId,
   @OperatorId      = @OperatorId,
   @StatusId        = @StatusId,
   @WarehouseId     = @WarehouseId,
   @ReceiptLineId   = @ReceiptLineId,
   @IssueLineId     = @IssueLineId,
   @ReferenceNumber = @ReferenceNumber,
   @ContainerTypeId = @ContainerTypeId
  
  if @Error <> 0
    goto error
  
  set @jobId = @NewJobId
  
  update i
     set JobId = @NewJobId
    from @TableInstruction t
    join Instruction       i on t.InstructionId = i.InstructionId
   where StatusCode in ('W','S')
  
  if @@Error <> 0
    goto error
  
  exec @Error = p_Status_Rollup
   @JobId = @OldJobId
  
  if @Error <> 0
    goto error
  
  commit transaction
  
  return
  
  error:
    if @jobId is null
      set @jobId = -1
    
    rollback transaction
    return @Error
end
