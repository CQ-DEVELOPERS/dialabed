﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QuestionAnswers_Update
  ///   Filename       : p_QuestionAnswers_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:28
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the QuestionAnswers table.
  /// </remarks>
  /// <param>
  ///   @QuestionId int = null,
  ///   @Answer nvarchar(100) = null,
  ///   @JobId int = null,
  ///   @ReceiptId int = null,
  ///   @OrderNumber int = null,
  ///   @PalletId int = null,
  ///   @DateAsked datetime = null,
  ///   @QuestionAnswersId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QuestionAnswers_Update
(
 @QuestionId int = null,
 @Answer nvarchar(100) = null,
 @JobId int = null,
 @ReceiptId int = null,
 @OrderNumber int = null,
 @PalletId int = null,
 @DateAsked datetime = null,
 @QuestionAnswersId int = null 
)
 
as
begin
	 set nocount on;
  
  if @QuestionAnswersId = '-1'
    set @QuestionAnswersId = null;
  
	 declare @Error int
 
  update QuestionAnswers
     set QuestionId = isnull(@QuestionId, QuestionId),
         Answer = isnull(@Answer, Answer),
         JobId = isnull(@JobId, JobId),
         ReceiptId = isnull(@ReceiptId, ReceiptId),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         PalletId = isnull(@PalletId, PalletId),
         DateAsked = isnull(@DateAsked, DateAsked)
   where QuestionAnswersId = @QuestionAnswersId
  
  select @Error = @@Error
  
  
  return @Error
  
end
