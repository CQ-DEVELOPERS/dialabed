﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_OrderLine_Details
  ///   Filename       : p_Outbound_OrderLine_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_OrderLine_Details
(
 @OutboundShipmentId int
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   OutboundShipmentId  int,
   IssueId             int,
   OutboundDocumentId  int,
   OrderNumber         nvarchar(30),
   ExternalCompanyId   int,
   ExternalCompanyCode nvarchar(30),
   ExternalCompany     nvarchar(255),
   RouteId             int,
   Route               nvarchar(50),
   DropSequence        int,
   OrderVolume         numeric(13,3),
   OrderWeight         numeric(13,3)
  )
  
  declare @TableDetail as table
  (
   OutboundDocumentId int,
   LineNumber         int,
   Quantity           float,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   LineVolume         float,
   LineWeight         float
  )
  
  insert @TableHeader
        (OutboundShipmentId,
         IssueId,
         OutboundDocumentId,
         OrderNumber,
         ExternalCompanyId,
         RouteId,
         DropSequence)
  select isi.OutboundShipmentId,
         i.IssueId,
         od.OutboundDocumentId,
         od.OrderNumber,
         od.ExternalCompanyId,
         i.RouteId,
         i.DropSequence
    from OutboundShipmentIssue isi (nolock)
    join Issue                   i (nolock) on isi.IssueId = i.IssueId
    join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
   where isi.OutboundShipmentId = @OutboundShipmentId
  
  update th
     set ExternalCompanyCode = ec.ExternalCompanyCode,
         ExternalCompany     = ec.ExternalCompany
    from @TableHeader    th
    join ExternalCompany ec (nolock) on th.ExternalCompanyId = ec.ExternalCompanyId
  
  update th
     set Route     = r.Route
    from @TableHeader th
    join Route         r (nolock) on th.RouteId = r.RouteId
  
  insert @TableDetail
        (OutboundDocumentId,
         LineNumber,
         Quantity,
         StorageUnitBatchId,
         LineVolume,
         LineWeight)
  select th.OutboundDocumentId,
         ol.LineNumber,
         sum(il.Quantity),
         il.StorageUnitBatchId,
         sum(il.Quantity) * sum(p.Quantity),
         sum(il.Quantity) * sum(p.Weight)
    from @TableHeader th
    join OutboundLine ol (nolock) on th.OutboundDocumentId = ol.OutboundDocumentId
    join IssueLine    il (nolock) on ol.OutboundLineId     = il.OutboundLineId
    join Pack          p (nolock) on ol.StorageUnitId      = p.StorageUnitId
   where p.Quantity = 1
  group by th.OutboundDocumentId,
           ol.LineNumber,
           il.StorageUnitBatchId
  
  update td
     set ProductCode = p.ProductCode,
         Product     = p.Product
    from @TableDetail td
    join StorageUnitBatch sub (nolock) on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
  
  select th.OutboundShipmentId
        ,th.IssueId
        ,td.LineNumber
        ,th.OrderNumber
        ,td.ProductCode
        ,td.Product
        ,td.Quantity
        ,td.LineVolume
        ,td.LineWeight
    from @TableHeader th
    join @TableDetail td on th.OutboundDocumentId = td.OutboundDocumentId
end
