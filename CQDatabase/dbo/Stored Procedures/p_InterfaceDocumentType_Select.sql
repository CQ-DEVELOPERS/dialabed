﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentType_Select
  ///   Filename       : p_InterfaceDocumentType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 16:26:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceDocumentTypeId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceDocumentType.InterfaceDocumentTypeId,
  ///   InterfaceDocumentType.InterfaceDocumentTypeCode,
  ///   InterfaceDocumentType.InterfaceDocumentType,
  ///   InterfaceDocumentType.RecordType,
  ///   InterfaceDocumentType.InterfaceType,
  ///   InterfaceDocumentType.InDirectory,
  ///   InterfaceDocumentType.OutDirectory,
  ///   InterfaceDocumentType.SubDirectory,
  ///   InterfaceDocumentType.FilePrefix 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentType_Select
(
 @InterfaceDocumentTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceDocumentType.InterfaceDocumentTypeId
        ,InterfaceDocumentType.InterfaceDocumentTypeCode
        ,InterfaceDocumentType.InterfaceDocumentType
        ,InterfaceDocumentType.RecordType
        ,InterfaceDocumentType.InterfaceType
        ,InterfaceDocumentType.InDirectory
        ,InterfaceDocumentType.OutDirectory
        ,InterfaceDocumentType.SubDirectory
        ,InterfaceDocumentType.FilePrefix
    from InterfaceDocumentType
   where isnull(InterfaceDocumentType.InterfaceDocumentTypeId,'0')  = isnull(@InterfaceDocumentTypeId, isnull(InterfaceDocumentType.InterfaceDocumentTypeId,'0'))
  order by InterfaceDocumentTypeCode
  
end
