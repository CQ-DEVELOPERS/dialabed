﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLine_Parameter
  ///   Filename       : p_IssueLine_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2013 08:43:18
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IssueLine table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   IssueLine.IssueLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLine_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as IssueLineId
        ,null as 'IssueLine'
  union
  select
         IssueLine.IssueLineId
        ,IssueLine.IssueLineId as 'IssueLine'
    from IssueLine
  
end
