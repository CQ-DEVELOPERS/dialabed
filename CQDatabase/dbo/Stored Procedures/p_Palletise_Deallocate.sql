﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Palletise_Deallocate
  ///   Filename       : p_Palletise_Deallocate.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Palletise_Deallocate
(
 @ReceiptId          int = null,
 @ReceiptLineId      int = null,
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @IssueLineId        int = null
)
 
as
begin
	 set nocount on;
	 
  declare @TableInstructions as table
  (
   InstructionId int,
   StatusCode    nvarchar(10),
   JobId         int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Count             int,
          @InstructionId     int,
          @StatusCode        nvarchar(10),
          @StatusId          int,
          @JobId             int,
          @WaitingId         int,
          @WarehouseId       int
  
  select @GetDate = dbo.ufn_Getdate()
  select @StatusId = dbo.ufn_StatusId('IS','P')
  select @WaitingId = dbo.ufn_StatusId('I','W')
  
  if @OutboundShipmentId = -1
     set @OutboundShipmentId = null
  
  if @IssueId = -1
     set @IssueId = null
  
  if @ReceiptId          is null and
     @ReceiptLineId      is null and
     @OutboundShipmentId is null and
     @IssueId            is null and
     @IssueLineId        is null
    return;
  
  if @ReceiptId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction  i
      join Status       s (nolock) on i.StatusId      = s.StatusId
      join ReceiptLine rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
     where ReceiptId     = isnull(@ReceiptId, ReceiptId)
  end
  else if @ReceiptLineId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction i
      join Status      s (nolock) on i.StatusId = s.StatusId
     where ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId)
  end
  
  if @OutboundShipmentId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction i
      join Status      s (nolock) on i.StatusId    = s.StatusId
     where OutboundShipmentId = @OutboundShipmentId
  end
  if @IssueId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction i
      join Status      s (nolock) on i.StatusId    = s.StatusId
      join IssueLine  il (nolock) on i.IssueLineId = il.IssueLineId
     where IssueId = isnull(@IssueId, IssueId)
  end
  else if @IssueLineId is not null
  begin
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction i
      join Status      s (nolock) on i.StatusId = s.StatusId
     where IssueLineId = isnull(@IssueLineId, IssueLineId)
  end
  
  set @Count = (select count(1) from @TableInstructions)
  
  begin transaction
  
  if @Count = 0 and (@OutboundShipmentId is not null or @IssueId is not null)
  begin
    if @OutboundShipmentId is not null
      select @WarehouseId = WarehouseId
        from OutboundShipment
       where OutboundShipmentId = @OutboundShipmentId
    
    if @IssueId is not null
      select @WarehouseId = WarehouseId
        from Issue
       where IssueId = @IssueId
    
    --if (select dbo.ufn_Configuration(155, @WarehouseId)) = 0 -- Palletise based on SOH
    --begin
    --  exec @Error = p_Outbound_Palletise
    --   @WarehouseId        = @WarehouseId,
    --   @OutboundShipmentId = @OutboundShipmentId,
    --   @IssueId            = @IssueId,
    --   @OperatorId         = null,
    --   @Weight             = null,
    --   @Volume             = null
      
    --  if @Error <> 0
    --  begin
    --    set @Errormsg = 'Error executing p_Palletise_Deallocate, p_Outbound_Palletise'
    --    goto error
    --  end
    --end
    --else
    --begin
      exec @Error = p_Palletise_Reverse
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId
      
      if @Error <> 0
      begin
        set @Errormsg = 'Error executing p_Palletise_Reverse, p_Outbound_Palletise'
        goto error
      end
      
      exec @Error = p_Outbound_Release
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId,
       @IssueLineId        = @IssueLineId,
       @StatusCode         = 'P'
      
      if @Error <> 0
      begin
        set @Errormsg = 'Error executing p_Outbound_Release, p_Outbound_Palletise'
        goto error
      end
    --end
  end
  
  if not exists(select 1
                  from @TableInstructions
                 where StatusCode not in ('W','NS')) and @Count > 0
  begin
    while @Count > 0
    begin
      set @Count = @Count - 1
      
      select top 1 @InstructionId = InstructionId,
                   @StatusCode    = StatusCode,
                   @JobId         = JobId
        from @TableInstructions
      
      delete @TableInstructions where InstructionId = @InstructionId
      
      if @StatusCode = 'W'
      begin
        exec @Error = p_StorageUnitBatchLocation_Deallocate
         @InstructionId = @InstructionId
        
        if @Error <> 0
          goto error
        
        update Instruction
           set PickLocationId = null,
               StatusId       = @WaitingId
         where InstructionId = @InstructionId
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
        
        if @OutboundShipmentId is null or 
           @IssueId            is null or
           @IssueLineId        is null
        begin
          exec @Error = p_Job_Update
           @JobId    = @JobId,
           @StatusId = @StatusId
          
          if @Error <> 0
            goto error
        end
        
        exec p_InstructionHistory_Insert
         @InstructionId  = @InstructionId,
         @PickLocationId = null,
         @CommandType    = 'Update',
         @InsertDate     = @GetDate
        
        delete IssueLineInstruction
         where InstructionId = @InstructionId
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
      end
    end
    
    exec @Error = p_Outbound_Release
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @IssueLineId        = @IssueLineId,
     @StatusCode         = 'P'
    
    if @Error <> 0
      goto error
  end
  else if @Count > 0
  begin
    set @Error = -1
    set @Errormsg = 'This instructions have been started or finished, and cannot be deallocated (p_Palletise_Deallocate).'
    goto error
  end
  
  if @OutboundShipmentId is not null
  begin
    update bi
       set InboundDocumentId = null,
           OutboundDocumentId = null,
           IssueId = null
      from BOMInstruction         bi (nolock)
      join OutboundDocument       od (nolock) on bi.OutboundDocumentId = od.OutboundDocumentId
      join Issue                   i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
      join OutboundShipmentIssue osi (nolock) on i.IssueId             = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
   end
   else if @IssueId is not null
   begin
    update bi
       set InboundDocumentId = null,
           OutboundDocumentId = null,
           IssueId = null
      from BOMInstruction         bi (nolock)
      join OutboundDocument       od (nolock) on bi.OutboundDocumentId = od.OutboundDocumentId
      join Issue                   i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
     where i.IssueId = @IssueId
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
