﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Insert
  ///   Filename       : p_Receipt_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 13:28:36
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Receipt table.
  /// </remarks>
  /// <param>
  ///   @ReceiptId int = null output,
  ///   @InboundDocumentId int = null,
  ///   @PriorityId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @CreditAdviceIndicator bit = null,
  ///   @Delivery int = null,
  ///   @AllowPalletise bit = null,
  ///   @Interfaced bit = null,
  ///   @StagingLocationId int = null,
  ///   @NumberOfLines int = null,
  ///   @ReceivingCompleteDate datetime = null,
  ///   @ParentReceiptId int = null,
  ///   @GRN nvarchar(60) = null,
  ///   @PlannedDeliveryDate datetime = null,
  ///   @ShippingAgentId int = null,
  ///   @ContainerNumber nvarchar(100) = null,
  ///   @AdditionalText1 nvarchar(510) = null,
  ///   @AdditionalText2 nvarchar(510) = null,
  ///   @BOE nvarchar(510) = null,
  ///   @Incoterms nvarchar(100) = null,
  ///   @ReceiptConfirmed datetime = null,
  ///   @ReceivingStarted datetime = null,
  ///   @VehicleId int = null,
  ///   @ParentIssueId int = null,
  ///   @VehicleTypeId int = null 
  /// </param>
  /// <returns>
  ///   Receipt.ReceiptId,
  ///   Receipt.InboundDocumentId,
  ///   Receipt.PriorityId,
  ///   Receipt.WarehouseId,
  ///   Receipt.LocationId,
  ///   Receipt.StatusId,
  ///   Receipt.OperatorId,
  ///   Receipt.DeliveryNoteNumber,
  ///   Receipt.DeliveryDate,
  ///   Receipt.SealNumber,
  ///   Receipt.VehicleRegistration,
  ///   Receipt.Remarks,
  ///   Receipt.CreditAdviceIndicator,
  ///   Receipt.Delivery,
  ///   Receipt.AllowPalletise,
  ///   Receipt.Interfaced,
  ///   Receipt.StagingLocationId,
  ///   Receipt.NumberOfLines,
  ///   Receipt.ReceivingCompleteDate,
  ///   Receipt.ParentReceiptId,
  ///   Receipt.GRN,
  ///   Receipt.PlannedDeliveryDate,
  ///   Receipt.ShippingAgentId,
  ///   Receipt.ContainerNumber,
  ///   Receipt.AdditionalText1,
  ///   Receipt.AdditionalText2,
  ///   Receipt.BOE,
  ///   Receipt.Incoterms,
  ///   Receipt.ReceiptConfirmed,
  ///   Receipt.ReceivingStarted,
  ///   Receipt.VehicleId,
  ///   Receipt.ParentIssueId,
  ///   Receipt.VehicleTypeId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_Insert
(
 @ReceiptId int = null output,
 @InboundDocumentId int = null,
 @PriorityId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Remarks nvarchar(500) = null,
 @CreditAdviceIndicator bit = null,
 @Delivery int = null,
 @AllowPalletise bit = null,
 @Interfaced bit = null,
 @StagingLocationId int = null,
 @NumberOfLines int = null,
 @ReceivingCompleteDate datetime = null,
 @ParentReceiptId int = null,
 @GRN nvarchar(60) = null,
 @PlannedDeliveryDate datetime = null,
 @ShippingAgentId int = null,
 @ContainerNumber nvarchar(100) = null,
 @AdditionalText1 nvarchar(510) = null,
 @AdditionalText2 nvarchar(510) = null,
 @BOE nvarchar(510) = null,
 @Incoterms nvarchar(100) = null,
 @ReceiptConfirmed datetime = null,
 @ReceivingStarted datetime = null,
 @VehicleId int = null,
 @ParentIssueId int = null,
 @VehicleTypeId int = null 
)
 
as
begin
	 set nocount on;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ShippingAgentId = '-1'
    set @ShippingAgentId = null;
  
  if @VehicleId = '-1'
    set @VehicleId = null;
  
  if @VehicleTypeId = '-1'
    set @VehicleTypeId = null;
  
	 declare @Error int
 
  insert Receipt
        (InboundDocumentId,
         PriorityId,
         WarehouseId,
         LocationId,
         StatusId,
         OperatorId,
         DeliveryNoteNumber,
         DeliveryDate,
         SealNumber,
         VehicleRegistration,
         Remarks,
         CreditAdviceIndicator,
         Delivery,
         AllowPalletise,
         Interfaced,
         StagingLocationId,
         NumberOfLines,
         ReceivingCompleteDate,
         ParentReceiptId,
         GRN,
         PlannedDeliveryDate,
         ShippingAgentId,
         ContainerNumber,
         AdditionalText1,
         AdditionalText2,
         BOE,
         Incoterms,
         ReceiptConfirmed,
         ReceivingStarted,
         VehicleId,
         ParentIssueId,
         VehicleTypeId)
  select @InboundDocumentId,
         @PriorityId,
         @WarehouseId,
         @LocationId,
         @StatusId,
         @OperatorId,
         @DeliveryNoteNumber,
         @DeliveryDate,
         @SealNumber,
         @VehicleRegistration,
         @Remarks,
         @CreditAdviceIndicator,
         @Delivery,
         @AllowPalletise,
         @Interfaced,
         @StagingLocationId,
         @NumberOfLines,
         @ReceivingCompleteDate,
         @ParentReceiptId,
         @GRN,
         @PlannedDeliveryDate,
         @ShippingAgentId,
         @ContainerNumber,
         @AdditionalText1,
         @AdditionalText2,
         @BOE,
         @Incoterms,
         @ReceiptConfirmed,
         @ReceivingStarted,
         @VehicleId,
         @ParentIssueId,
         @VehicleTypeId 
  
  select @Error = @@Error, @ReceiptId = scope_identity()
  
  
  return @Error
  
end
