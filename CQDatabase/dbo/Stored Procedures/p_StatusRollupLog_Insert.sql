﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StatusRollupLog_Insert
  ///   Filename       : p_StatusRollupLog_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:38
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StatusRollupLog table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @JobCode varchar(10) = null,
  ///   @CreateDate datetime = null 
  /// </param>
  /// <returns>
  ///   StatusRollupLog.JobId,
  ///   StatusRollupLog.JobCode,
  ///   StatusRollupLog.CreateDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StatusRollupLog_Insert
(
 @JobId int = null,
 @JobCode varchar(10) = null,
 @CreateDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StatusRollupLog
        (JobId,
         JobCode,
         CreateDate)
  select @JobId,
         @JobCode,
         @CreateDate 
  
  select @Error = @@Error
  
  
  return @Error
  
end
