﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerDetail_Delete
  ///   Filename       : p_ContainerDetail_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:56
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ContainerDetail table.
  /// </remarks>
  /// <param>
  ///   @ContainerDetailId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerDetail_Delete
(
 @ContainerDetailId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ContainerDetail
     where ContainerDetailId = @ContainerDetailId
  
  select @Error = @@Error
  
  
  return @Error
  
end
