﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Weight_Select
  ///   Filename       : p_Pallet_Weight_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Weight_Select
(
 @barcode nvarchar(30) = null,
 @jobId int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @PalletId           int
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId   = replace(@barcode,'J:',''),
           @barcode = null
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
    select @JobId = JobId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where PalletId = @PalletId
      and it.InstructionTypeCode in ('PR','S','SM')
  
  if @barcode is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @barcode
  end
  
  --select JobId,
  --       ReferenceNumber,
  --       TareWeight,
  --       NettWeight,
  --       Weight     as 'GrossWeight'
  --  from Job (nolock)
  -- where JobId = @JobId
  declare @TableResult as table
  (
   InstructionId      int,
   StorageUnitBatchId int,
   ProductCode	       nvarchar(30) null,
   Product	           nvarchar(50) null,
   SKUCode            nvarchar(50) null,
   Quantity           float null,
   JobId              int null,
   StatusId           int null,
   Status             nvarchar(50) null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   StoreLocationId    int null,
   StoreLocation      nvarchar(15) null,
   PalletId           int null,
   CreateDate         datetime null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   InstructionType	   nvarchar(30),
   OperatorId         int null,
   Operator           nvarchar(50) null,
   Dwell              nvarchar(50) null,
   StockLevel         nvarchar(50) null,
   ReferenceNumber    nvarchar(30),
   TareWeight         float,
   NettWeight         float,
   GrossWeight        float
  )

  insert @TableResult
        (InstructionId,
         StorageUnitBatchId,
         Quantity,
         JobId,
         StatusId,
         PalletId,
         CreateDate,
         ReferenceNumber,
         TareWeight,
         NettWeight,
         GrossWeight)
  select i.InstructionId,
         i.StorageUnitBatchId,
         i.Quantity,
         i.JobId,
         i.StatusId,
         i.PalletId,
         i.CreateDate,
         j.ReferenceNumber,
         j.TareWeight,
         j.NettWeight,
         j.Weight
         
    from Instruction         i   (nolock)
    join Job                 j   (nolock) on i.JobId             = j.JobId
    join InstructionType     it  (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status              s   (nolock) on j.StatusId          = s.StatusId
   where i.JobId = @JobId
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ECLNumber     = b.ECLNumber
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId

  update tr
     set Status = s.Status
    from @TableResult tr
    join Status       s  (nolock) on tr.StatusId = s.StatusId
  
  select JobId,
         ReferenceNumber,
         ProductCode,
         Product,
         SKUCode,
         Quantity,
         Status,
         CreateDate,
         Batch,
         TareWeight,
         NettWeight,
         GrossWeight
    from @TableResult tr 
end
