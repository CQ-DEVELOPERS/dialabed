﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Track_Product
  ///   Filename       : p_Report_Track_Product.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    
  ///   Modified Date 
  ///   Details       
  /// </newpara>
*/
CREATE procedure p_Report_Track_Product
(
 @StorageUnitId int,
 @StartDate		datetime,    
 @EndDate		datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   LineNumber         int,
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   JobId              int,
   InstructionId      int,
   StorageUnitBatchId int,
   StorageUnitId	  int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Description	      nvarchar(255),
   Statusid			  int,
   Status			  nvarchar(50),
   LineStatusid		  int,
   LineStatus		  nvarchar(50),
   InsertDate		  datetime,
   ProcessDate		  datetime,
   WarehouseCode      nvarchar(10),
   Quantity			  int,
   SortDate			  datetime,
   InterfaceMessage	  nvarchar(max)  	 
  )
  
  declare @ProductCode nvarchar(30)
  
  set @productcode = (select productcode 
						from storageunit su 
						join product p on su.productid = p.productid 
						where su.storageunitid = @storageunitid)
 
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate,
         InterfaceMessage)
  select h.OrderNumber,
		 'Import PO into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate),
		 InterfaceMessage
  From InterfaceImportPOHeader		h (nolock)
  left join InterfaceImportPODetail d (nolock) on d.InterfaceImportPOHeaderId = h.InterfaceImportPOHeaderId
  join RecordStatus				   rs (nolock) on rs.StatusCode = h.RecordStatus
  left join InterfaceMessage       im (nolock) on h.InterfaceImportPOHeaderId = im.InterfaceMessageId
  where d.ProductCode = @ProductCode
  and h.InsertDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate,
         InterfaceMessage)
  select h.OrderNumber,
		 'Import SO into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate),
		 im.InterfaceMessage
  From InterfaceImportSOHeader      h (nolock)
  left join InterfaceImportSODetail d (nolock) on d.InterfaceImportSOHeaderId = h.InterfaceImportSOHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  left join InterfaceMessage       im (nolock) on h.InterfaceImportSOHeaderId = im.InterfaceMessageId
  where d.ProductCode = @ProductCode
  and h.InsertDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate,
         InterfaceMessage)
  select h.OrderNumber,
		 'Import PO into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate),
		 InterfaceMessage
  From InterfaceImportHeader		h (nolock)
  left join InterfaceImportDetail   d (nolock) on d.InterfaceImportHeaderId = h.InterfaceImportHeaderId
  join RecordStatus				   rs (nolock) on rs.StatusCode = h.RecordStatus
  left join InterfaceMessage       im (nolock) on h.InterfaceImportHeaderId = im.InterfaceMessageId
  where d.ProductCode = @ProductCode
  and h.InsertDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  --insert @TableResult
  --      (OrderNumber,
  --       Description,
  --       Status,
  --       InsertDate,
  --       ProcessDate,
  --       StorageUnitId,
  --       LineNumber,
  --       WarehouseCode,
  --       Quantity,
  --       LineStatus,
  --       SortDate)
  --select OrderNumber,
		-- 'Inbound Document',
		-- s1.Status,
		-- id.CreateDate,
		-- null,
		-- il.StorageUnitId,
		-- il.LineNumber,
		-- id.WarehouseId,
		-- il.Quantity,
		-- s2.Status,
		-- isnull(id.CreateDate, getdate()) as SortDate
  --From InboundDocument id
  --left join InboundLine il on id.InboundDocumentId = il.InboundDocumentId
  --join StorageUnit su on su.StorageUnitId = il.StorageUnitId
  --join Product p on  su.ProductId = p.ProductId
  --join Status s1 on s1.StatusId = id.StatusId
  --join Status s2 on s2.StatusId = il.StatusId
  --where p.ProductCode = @ProductCode
  --and id.CreateDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         StorageUnitBatchId,
         LineNumber,
         WarehouseCode,
         Quantity,
         LineStatus,
         SortDate)
  select OrderNumber,
		 idt.InboundDocumentType as Description,
		 s1.Status,
		 r.DeliveryDate,
		 null,
		 rl.StorageUnitBatchId,
		 rl.ReceiptLineId,
		 r.WarehouseId,
		 rl.AcceptedQuantity,
		 s2.Status,
		 isnull(r.DeliveryDate, id.CreateDate) as SortDate
  From InboundDocument           id (nolock)
  left join Receipt               r (nolock) on id.InboundDocumentId = r.InboundDocumentId
  left join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId 
  left join ReceiptLine          rl (nolock) on r.ReceiptId = rl.ReceiptId 
  join StorageUnitbatch         sub (nolock) on sub.StorageUnitbatchId = rl.StorageUnitBatchId
  join StorageUnit               su (nolock) on sub.StorageUnitId = su.StorageUnitId
  join Product                    p (nolock) on  su.ProductId = p.ProductId
  join Status                    s1 (nolock) on s1.StatusId = r.StatusId
  join Status                    s2 (nolock) on s2.StatusId = rl.StatusId
  where p.ProductCode = @ProductCode
  and id.CreateDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  --insert @TableResult
  --      (OrderNumber,
  --       Description,
  --       Status,
  --       InsertDate,
  --       ProcessDate,
  --       StorageUnitId,
  --       LineNumber,
  --       WarehouseCode,
  --       Quantity,
  --       LineStatus,
  --       SortDate)
  --select OrderNumber,
		-- 'Outbound Document',
		-- s1.Status,
		-- od.CreateDate,
		-- null,
		-- ol.StorageUnitId,
		-- ol.LineNumber,
		-- od.WarehouseId,
		-- ol.Quantity,
		-- s2.Status,
		-- isnull(od.CreateDate, getdate()) as SortDate
  --From OutboundDocument od
  --left join OutboundLine ol on od.OutboundDocumentId = ol.OutboundDocumentId
  --join StorageUnit su on su.StorageUnitId = ol.StorageUnitId
  --join Product p on  su.ProductId = p.ProductId
  --join Status s1 on s1.StatusId = od.StatusId
  --join Status s2 on s2.StatusId = ol.StatusId
  --where p.ProductCode = @ProductCode
  --and od.CreateDate between @StartDate and DATEADD(dd,1,@EndDate)
  
    insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         StorageUnitBatchId,
         LineNumber,
         WarehouseCode,
         Quantity,
         LineStatus,
         SortDate)
  select OrderNumber,
		 odt.OutboundDocumentType,
		 s1.Status,
		 r.DeliveryDate,
		 null,
		 rl.StorageUnitBatchId,
		 rl.IssueLineId,
		 r.WarehouseId,
		 rl.ConfirmedQuatity,
		 s2.Status,
		 isnull(r.DeliveryDate, id.CreateDate) as SortDate
  From OutboundDocument           id (nolock)
  left join Issue                  r (nolock) on id.OutboundDocumentid = r.OutboundDocumentid
  left join OutboundDocumentType odt (nolock) on id.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  left join IssueLine             rl (nolock) on r.IssueId = rl.IssueId 
  join StorageUnitbatch          sub (nolock) on sub.StorageUnitbatchId = rl.StorageUnitBatchId
  join StorageUnit                su (nolock) on sub.StorageUnitId = su.StorageUnitId
  join Product                     p (nolock) on  su.ProductId = p.ProductId
  join Status                     s1 (nolock) on s1.StatusId = r.StatusId
  join Status                     s2 (nolock) on s2.StatusId = rl.StatusId
  where p.ProductCode = @ProductCode
  and id.CreateDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select OrderNumber,
		 'Export PO from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceExportPOHeader      h (nolock)
  left join InterfaceExportPODetail d (nolock) on d.InterfaceExportPOHeaderId = h.InterfaceExportPOHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where ProductCode = @ProductCode
  and h.InsertDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select OrderNumber,
		 'Export SO from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceExportSOHeader      h (nolock)
  left join InterfaceExportSODetail d (nolock) on d.InterfaceExportSOHeaderId = h.InterfaceExportSOHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where ProductCode = @ProductCode
  and h.InsertDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select OrderNumber,
		 'Export PO from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceExportHeader      h (nolock)
  left join InterfaceExportDetail d (nolock) on d.InterfaceExportHeaderId = h.InterfaceExportHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where ProductCode = @ProductCode
  and h.InsertDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  --insert @TableResult
  --      (OrderNumber,
  --       Description,
  --       Status,
  --       InsertDate,
  --       ProcessDate,
  --       ProductCode,
  --       Product,
  --       LineNumber,
  --       WarehouseCode,
  --       Quantity,
  --       SortDate)
  --select OrderNumber,
		-- 'Export PO from Cquential',
		-- StatusDesc,
		-- h.InsertDate,
		-- h.ProcessedDate,
		-- d.ProductCode,
		-- d.Product,
		-- d.LineNumber,
		-- d.Additional1,
		-- d.Quantity,
		-- isnull(h.ProcessedDate, h.InsertDate)
  --From InterfaceExportPOHeader      h (nolock)
  --left join InterfaceExportPODetail d (nolock) on d.InterfaceExportPOHeaderId = h.InterfaceExportPOHeaderId
  --join RecordStatus                rs(nolock)  on rs.StatusCode = h.RecordStatus
  --where ProductCode = @ProductCode
  --and h.InsertDate between @StartDate and DATEADD(dd,1,@EndDate)
  
  update tr
     set ProductCode = p.ProductCode,
		 Product = p.Product
    from @TableResult tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product       p (nolock) on p.ProductId = su.ProductId
   where tr.StorageUnitId is not null
   and   tr.ProductCode is null
   
   update tr
     set ProductCode = p.ProductCode,
		 Product = p.Product
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on p.ProductId = su.ProductId
   where tr.StorageUnitBatchId is not null
   and   tr.StorageUnitId is null
   and   tr.ProductCode is null
   
     update tr
     set ProductCode = p.ProductCode,
		 Product = p.Product
    from @TableResult tr
    join Product       p (nolock) on p.HostId = tr.ProductCode
   where tr.Product is null
   
  select OrderNumber,
		 Description,
         ProductCode,
		 Product,
	     Status,
	     LineNumber,
         LineStatus,
	     InsertDate,
		 ProcessDate,
		 WarehouseCode,
		 Quantity	 
    from @TableResult
  order by OrderNumber,
			SortDate
end
