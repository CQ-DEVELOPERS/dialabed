﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentType_Delete
  ///   Filename       : p_OutboundDocumentType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jan 2014 09:16:07
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OutboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentType_Delete
(
 @OutboundDocumentTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete OutboundDocumentType
     where OutboundDocumentTypeId = @OutboundDocumentTypeId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OutboundDocumentTypeHistory_Insert
         @OutboundDocumentTypeId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
