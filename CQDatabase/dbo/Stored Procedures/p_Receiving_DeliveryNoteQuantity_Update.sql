﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_DeliveryNoteQuantity_Update
  ///   Filename       : p_Receiving_DeliveryNoteQuantity_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_DeliveryNoteQuantity_Update
(
 @InboundLineId int,
 @ReceiptId int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @DeliveryNoteQuantity numeric(13,6),
          @TempReceiptLineId    int,
          @AcceptedQuantity     numeric(13,6)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select @DeliveryNoteQuantity = sum(DeliveryNoteQuantity)
    from ReceiptLine (nolock)
   where InboundLineId = @InboundLineId
     and ReceiptId = @ReceiptId
  
  declare @TableReceiptLine as table
  (
   ReceiptLineId    int,
   AcceptedQuantity float
  )
  
  insert @TableReceiptLine
        (ReceiptLineId,
         AcceptedQuantity)
  select ReceiptLineId,
         AcceptedQuantity
    from ReceiptLine (nolock)
   where InboundLineId = @InboundLineId
     and ReceiptId = @ReceiptId
  
  while exists(select top 1 1 from @TableReceiptLine)
  begin
    select top 1
           @TempReceiptLineId = ReceiptLineId,
           @AcceptedQuantity  = AcceptedQuantity
      from @TableReceiptLine
    order by ReceiptLineId desc
    
    delete @TableReceiptLine where ReceiptLineId = @TempReceiptLineId
    
    -- Added if statemant 2014-04-02 for UTI
    if @DeliveryNoteQuantity = 0
      set @DeliveryNoteQuantity = @AcceptedQuantity
    
    if @AcceptedQuantity > @DeliveryNoteQuantity
    begin
      exec p_ReceiptLine_Update
       @ReceiptLineId = @TempReceiptLineId,
       @DeliveryNoteQuantity = @DeliveryNoteQuantity
      
      set @DeliveryNoteQuantity = null
      
      delete @TableReceiptLine
    end
    else
    begin
      exec p_ReceiptLine_Update
       @ReceiptLineId = @TempReceiptLineId,
       @DeliveryNoteQuantity = @AcceptedQuantity
      --select @TempReceiptLineId as '@TempReceiptLineId', @DeliveryNoteQuantity as '@DeliveryNoteQuantity', @AcceptedQuantity as '@AcceptedQuantity'
      set @DeliveryNoteQuantity = @DeliveryNoteQuantity - @AcceptedQuantity
    end
  end
  
  if @DeliveryNoteQuantity is not null
  begin
    set @DeliveryNoteQuantity = @DeliveryNoteQuantity + @AcceptedQuantity
    exec p_ReceiptLine_Update
     @ReceiptLineId = @TempReceiptLineId,
     @DeliveryNoteQuantity = @DeliveryNoteQuantity
  end
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Receiving_DeliveryNoteQuantity_Update'
    rollback transaction
    return @Error
end
