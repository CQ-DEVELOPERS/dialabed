﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItemBackup_Insert
  ///   Filename       : p_OperatorGroupMenuItemBackup_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:21
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroupMenuItemBackup table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @MenuItemId int = null,
  ///   @Access bit = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupMenuItemBackup.OperatorGroupId,
  ///   OperatorGroupMenuItemBackup.MenuItemId,
  ///   OperatorGroupMenuItemBackup.Access 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItemBackup_Insert
(
 @OperatorGroupId int = null,
 @MenuItemId int = null,
 @Access bit = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorGroupMenuItemBackup
        (OperatorGroupId,
         MenuItemId,
         Access)
  select @OperatorGroupId,
         @MenuItemId,
         @Access 
  
  select @Error = @@Error
  
  
  return @Error
  
end
