﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Shipment_Search
  ///   Filename       : p_Outbound_Shipment_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Shipment_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId int,
 @FromDate	              datetime,
 @ToDate	                datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId    int,
   TotalOrders           int,
   NumberOfOrders        int,
   ShipmentDate         datetime,
   StatusId             int,
   Status               nvarchar(50),
   LocationId           int,
   Location             nvarchar(15),
   RouteId              int,
   Route                nvarchar(50),
   CreateDate           datetime,
   SealNumber			nvarchar(30),
   VehicleRegistration	nvarchar(10),
   Remarks				nvarchar(250),
   DespatchBay			int,
   DespatchBayName      nvarchar(15),
   ContactListId		int,
   ContactPerson		nvarchar(50)
  );
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (OutboundShipmentId,
           ShipmentDate,
           StatusId,
           Status,
           LocationId,
           RouteId,
           Route,
           TotalOrders,
           NumberOfOrders,
           SealNumber,
           VehicleRegistration,
           Remarks,
           DespatchBay,
           ContactListId)
    select os.OutboundShipmentId,
           os.ShipmentDate,
           os.StatusId,
           s.Status,
           isnull(os.LocationId, -1),
           isnull(os.RouteId,-1),
           Route,
           os.TotalOrders,
           os.NumberOfOrders,
           os.SealNumber,
           os.VehicleRegistration,
           os.Remarks,
           isnull(os.DespatchBay, -1),
           isnull(os.ContactListId, -1)
      from OutboundShipment os (nolock)
      join Status            s (nolock) on os.StatusId = s.StatusId
     where os.WarehouseId        = @WarehouseId
       and os.ShipmentDate between @FromDate and @ToDate
       and s.Type                = 'IS'
       and s.StatusCode         in ('W','P')

  end
  else
  begin
    insert @TableResult
          (OutboundShipmentId,
           ShipmentDate,
           StatusId,
           Status,
           LocationId,
           RouteId,
           Route,
           TotalOrders,
           NumberOfOrders,
           SealNumber,
           VehicleRegistration,
           Remarks,
           DespatchBay,
           ContactListId)
    select os.OutboundShipmentId,
           os.ShipmentDate,
           os.StatusId,
           s.Status,
           isnull(os.LocationId, -1),
           isnull(os.RouteId,-1),
           Route,
           os.TotalOrders,
           os.NumberOfOrders,
           os.SealNumber,
           os.VehicleRegistration,
           os.Remarks,
           isnull(os.DespatchBay, -1),
           isnull(os.ContactListId, -1)
      from OutboundShipment os (nolock)
      join Status            s (nolock) on os.StatusId = s.StatusId
     where os.WarehouseId        = @WarehouseId
       and os.OutboundShipmentId = isnull(@OutboundShipmentId, os.OutboundShipmentId)
       and s.Type                = 'IS'
       and s.StatusCode         in ('W','P')

  end
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.LocationId = l.LocationId
    
  update tr
     set DespatchBayName = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.DespatchBay = l.LocationId
    
  update tr
     set ContactPerson = cl.ContactPerson
    from @TableResult tr
    join ContactList     cl (nolock) on tr.ContactListId = cl.ContactListId
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
   where tr.Route is null
  
  select OutboundShipmentId,
         TotalOrders,
         NumberOfOrders,
         ShipmentDate,
         StatusId,
         Status,
         LocationId,
         Location,
         RouteId,
         Route,
         SealNumber,
         VehicleRegistration,
         Remarks,
         DespatchBay,
         DespatchBayName,
         ContactListId,
         ContactPerson
    from @TableResult
   where OutboundShipmentId is not null
end
