﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Set_Items
  ///   Filename       : p_Container_Set_Items.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Set_Items
(
 @containerHeaderId int,
 @containerDetailId int,
 @operatorId int,
 @quantity int
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Container_Set_Items',
          @GetDate           datetime,
          @Transaction       bit = 1
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  exec @Error = p_ContainerDetail_Update
   @containerDetailId = @containerDetailId,
   @quantity          = @quantity
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
