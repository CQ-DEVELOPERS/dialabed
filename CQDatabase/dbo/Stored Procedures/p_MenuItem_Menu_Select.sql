﻿/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Menu_Select
  ///   Filename       : p_MenuItem_Menu_Select.sql
  ///   Create By      : William
  ///   Date Created   : 10 July 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// 
  /// <returns>
  ///   mi.MenuItemId,
  ///   mic.MenuItemText,
  ///   mic.ToolTip,
  ///   mi.NavigateTo,
  ///   mi.ParentMenuItemId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure dbo.p_MenuItem_Menu_Select
as
begin
	 set nocount on

Select m.MenuId,
		     m.Menu
  from Menu  m (nolock)
order by m.Menu
end
