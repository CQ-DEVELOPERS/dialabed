﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Show_Warning
  ///   Filename       : p_Batch_Show_Warning.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Show_Warning
(
 @storageUnitBatchId int
)
 
as
begin
	 set nocount on;
  
  declare @Error         int,
          @Errormsg      nvarchar(500),
          @GetDate       datetime,
          @Batch         nvarchar(30),
          @CreateDate    datetime,
          @ExpiryDate    datetime,
          @StorageUnitId int,
          @prevBatch     nvarchar(30),
          @ShelfLifeDays int,
          @Percentage    numeric(13,3),
          @WarehouseId   int,
          @Exception     nvarchar(255)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @Batch         = b.Batch,
         @CreateDate    = b.CreateDate,
         @ExpiryDate    = isnull(b.ExpiryDate, dateadd(dd, p.ShelfLifeDays, b.CreateDate)),
         @StorageUnitId = sub.StorageUnitId,
         @ShelfLifeDays = p.ShelfLifeDays,
         @WarehouseId   = b.WarehouseId
    from StorageUnitBatch sub (nolock)
    join Batch              b (nolock) on sub.BatchId = b.BatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
   where sub.StorageUnitBatchId = @storageUnitBatchId
  
  declare @TableResult as table
  (
   WaringMessage ntext
  )
  
  select top 1 @prevBatch = b.Batch
    from StorageUnitBatch sub
    join Batch              b on sub.BatchId = b.BatchId
   where sub.StorageUnitId = @StorageUnitId
     and b.CreateDate      < @CreateDate
     and b.ExpiryDate      > @ExpiryDate
     and b.Batch          != 'Default'
  order by b.CreateDate desc
  
  if @prevBatch is not null
  begin
    select @Exception = 'Batch ' + isnull(@Batch, '') + ' is older than previous batch ' + isnull(@prevBatch, '') + ' received.'
    
    insert @TableResult
    select @Exception
    
    exec @Error = p_Exception_Insert
     @ExceptionId   = null,
     @ExceptionCode = 'BATOLDER',
     @Exception     = @Exception,
     @CreateDate    = @GetDate,
     @ExceptionDate = @Getdate
  end
  
  select @Percentage = dbo.ufn_Configuration_Value(128, @WarehouseId)
  
  if isnull(@ShelfLifeDays,0) > 0
    if (datediff(dd, @CreateDate, @ExpiryDate) / @ShelfLifeDays * 100) < @Percentage
    begin
      select @Exception = 'This batch less than ' + convert(nvarchar(10), isnull(@Percentage,'0')) + '% of it''s shelf life.'
      
      insert @TableResult
      select @Exception
      
      exec @Error = p_Exception_Insert
       @ExceptionId   = null,
       @ExceptionCode = 'BATSL',
       @Exception     = @Exception,
       @CreateDate    = @GetDate,
       @ExceptionDate = @Getdate
    end
  
  select WaringMessage
    from @TableResult
end
