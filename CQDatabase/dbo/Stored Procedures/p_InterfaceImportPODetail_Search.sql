﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPODetail_Search
  ///   Filename       : p_InterfaceImportPODetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:56
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportPODetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportPODetail.InterfaceImportPOHeaderId,
  ///   InterfaceImportPODetail.ForeignKey,
  ///   InterfaceImportPODetail.LineNumber,
  ///   InterfaceImportPODetail.ProductCode,
  ///   InterfaceImportPODetail.Product,
  ///   InterfaceImportPODetail.SKUCode,
  ///   InterfaceImportPODetail.Batch,
  ///   InterfaceImportPODetail.Quantity,
  ///   InterfaceImportPODetail.Weight,
  ///   InterfaceImportPODetail.Additional1,
  ///   InterfaceImportPODetail.Additional2,
  ///   InterfaceImportPODetail.Additional3,
  ///   InterfaceImportPODetail.Additional4,
  ///   InterfaceImportPODetail.Additional5,
  ///   InterfaceImportPODetail.Additional6,
  ///   InterfaceImportPODetail.Additional7,
  ///   InterfaceImportPODetail.Additional8,
  ///   InterfaceImportPODetail.Additional9,
  ///   InterfaceImportPODetail.Additional10 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPODetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportPODetail.InterfaceImportPOHeaderId
        ,InterfaceImportPODetail.ForeignKey
        ,InterfaceImportPODetail.LineNumber
        ,InterfaceImportPODetail.ProductCode
        ,InterfaceImportPODetail.Product
        ,InterfaceImportPODetail.SKUCode
        ,InterfaceImportPODetail.Batch
        ,InterfaceImportPODetail.Quantity
        ,InterfaceImportPODetail.Weight
        ,InterfaceImportPODetail.Additional1
        ,InterfaceImportPODetail.Additional2
        ,InterfaceImportPODetail.Additional3
        ,InterfaceImportPODetail.Additional4
        ,InterfaceImportPODetail.Additional5
        ,InterfaceImportPODetail.Additional6
        ,InterfaceImportPODetail.Additional7
        ,InterfaceImportPODetail.Additional8
        ,InterfaceImportPODetail.Additional9
        ,InterfaceImportPODetail.Additional10
    from InterfaceImportPODetail
  
end
