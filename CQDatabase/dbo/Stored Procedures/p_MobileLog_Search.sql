﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MobileLog_Search
  ///   Filename       : p_MobileLog_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:11
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the MobileLog table.
  /// </remarks>
  /// <param>
  ///   @MobileLogId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   MobileLog.MobileLogId,
  ///   MobileLog.ProcName,
  ///   MobileLog.WarehouseId,
  ///   MobileLog.InstructionId,
  ///   MobileLog.ReferenceNumber,
  ///   MobileLog.JobId,
  ///   MobileLog.PalletId,
  ///   MobileLog.Barcode,
  ///   MobileLog.Pick,
  ///   MobileLog.Store,
  ///   MobileLog.StorageUnitId,
  ///   MobileLog.StorageUnitBatchId,
  ///   MobileLog.PickLocationId,
  ///   MobileLog.StoreLocationId,
  ///   MobileLog.Batch,
  ///   MobileLog.Quantity,
  ///   MobileLog.OperatorId,
  ///   MobileLog.StartDate,
  ///   MobileLog.EndDate,
  ///   MobileLog.ErrorMsg,
  ///   MobileLog.StatusCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MobileLog_Search
(
 @MobileLogId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @MobileLogId = '-1'
    set @MobileLogId = null;
  
 
  select
         MobileLog.MobileLogId
        ,MobileLog.ProcName
        ,MobileLog.WarehouseId
        ,MobileLog.InstructionId
        ,MobileLog.ReferenceNumber
        ,MobileLog.JobId
        ,MobileLog.PalletId
        ,MobileLog.Barcode
        ,MobileLog.Pick
        ,MobileLog.Store
        ,MobileLog.StorageUnitId
        ,MobileLog.StorageUnitBatchId
        ,MobileLog.PickLocationId
        ,MobileLog.StoreLocationId
        ,MobileLog.Batch
        ,MobileLog.Quantity
        ,MobileLog.OperatorId
        ,MobileLog.StartDate
        ,MobileLog.EndDate
        ,MobileLog.ErrorMsg
        ,MobileLog.StatusCode
    from MobileLog
   where isnull(MobileLog.MobileLogId,'0')  = isnull(@MobileLogId, isnull(MobileLog.MobileLogId,'0'))
  
end
