﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Select
  ///   Filename       : p_Product_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2014 14:16:34
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null 
  /// </param>
  /// <returns>
  ///   Product.ProductId,
  ///   Product.StatusId,
  ///   Product.ProductCode,
  ///   Product.Product,
  ///   Product.Barcode,
  ///   Product.MinimumQuantity,
  ///   Product.ReorderQuantity,
  ///   Product.MaximumQuantity,
  ///   Product.CuringPeriodDays,
  ///   Product.ShelfLifeDays,
  ///   Product.QualityAssuranceIndicator,
  ///   Product.ProductType,
  ///   Product.OverReceipt,
  ///   Product.HostId,
  ///   Product.RetentionSamples,
  ///   Product.Samples,
  ///   Product.ParentProductCode,
  ///   Product.DangerousGoodsId,
  ///   Product.Description2,
  ///   Product.Description3,
  ///   Product.PrincipalId,
  ///   Product.AssaySamples,
  ///   Product.Category,
  ///   Product.ProductCategoryId,
  ///   Product.ProductAlias,
  ///   Product.ProductGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Select
(
 @ProductId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Product.ProductId
        ,Product.StatusId
        ,Product.ProductCode
        ,Product.Product
        ,Product.Barcode
        ,Product.MinimumQuantity
        ,Product.ReorderQuantity
        ,Product.MaximumQuantity
        ,Product.CuringPeriodDays
        ,Product.ShelfLifeDays
        ,Product.QualityAssuranceIndicator
        ,Product.ProductType
        ,Product.OverReceipt
        ,Product.HostId
        ,Product.RetentionSamples
        ,Product.Samples
        ,Product.ParentProductCode
        ,Product.DangerousGoodsId
        ,Product.Description2
        ,Product.Description3
        ,Product.PrincipalId
        ,Product.AssaySamples
        ,Product.Category
        ,Product.ProductCategoryId
        ,Product.ProductAlias
        ,Product.ProductGroup
    from Product
   where isnull(Product.ProductId,'0')  = isnull(@ProductId, isnull(Product.ProductId,'0'))
  order by ProductCode
  
end
