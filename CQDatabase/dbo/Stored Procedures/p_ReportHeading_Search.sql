﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportHeading_Search
  ///   Filename       : p_ReportHeading_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:25
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ReportHeading table.
  /// </remarks>
  /// <param>
  ///   @ReportHeadingId int = null output,
  ///   @ReportHeadingCode nvarchar(200) = null,
  ///   @ReportHeading nvarchar(200) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ReportHeading.ReportHeadingId,
  ///   ReportHeading.CultureId,
  ///   ReportHeading.ReportHeadingCode,
  ///   ReportHeading.ReportHeading 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportHeading_Search
(
 @ReportHeadingId int = null output,
 @ReportHeadingCode nvarchar(200) = null,
 @ReportHeading nvarchar(200) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ReportHeadingId = '-1'
    set @ReportHeadingId = null;
  
  if @ReportHeadingCode = '-1'
    set @ReportHeadingCode = null;
  
  if @ReportHeading = '-1'
    set @ReportHeading = null;
  
 
  select
         ReportHeading.ReportHeadingId
        ,ReportHeading.CultureId
        ,ReportHeading.ReportHeadingCode
        ,ReportHeading.ReportHeading
    from ReportHeading
   where isnull(ReportHeading.ReportHeadingId,'0')  = isnull(@ReportHeadingId, isnull(ReportHeading.ReportHeadingId,'0'))
     and isnull(ReportHeading.ReportHeadingCode,'%')  like '%' + isnull(@ReportHeadingCode, isnull(ReportHeading.ReportHeadingCode,'%')) + '%'
     and isnull(ReportHeading.ReportHeading,'%')  like '%' + isnull(@ReportHeading, isnull(ReportHeading.ReportHeading,'%')) + '%'
  
end
