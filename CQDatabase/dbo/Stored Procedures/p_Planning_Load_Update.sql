﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Load_Update
  ///   Filename       : p_Planning_Load_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Load_Update
(
 @outboundShipmentId int,
 @locationId         int,
 @priorityId         int,
 @routeId            int,
 @deliveryDate       datetime,
 @remarks            nvarchar(250),
 @areaType           nvarchar(10)
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
  (
   OutboundShipmentId              int,
   IssueId                         int,
   IssueLineId                     int,
   InstructionId                   int,
   JobId                           int
  )
  
  declare @TableIssue as table
  (
   IssueId int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @IssueId           int,
          @InstructionId     int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @locationId = -1
    set @locationId = null
  
  if @priorityId = -1
    set @priorityId = null
  
  if @routeId = -1
    set @routeId = null
  
  begin transaction
  
  exec @Error = p_OutboundShipment_Update
   @OutboundShipmentId = @outboundShipmentId,
   @ShipmentDate       = @deliveryDate,
   @LocationId         = @locationId,
   @DespatchBay        = @locationId,
   @RouteId            = @routeId,
   @Remarks            = @remarks
  
  if @Error <> 0
    goto error
  
  insert @TableIssue
        (IssueId)
  select IssueId
    from OutboundShipmentIssue
   where OutboundShipmentId = @OutboundShipmentId
  
  while exists(select top 1 1 from @TableIssue)
  begin
    select top 1 @IssueId = IssueId
      from @TableIssue
    
    delete @TableIssue where IssueId = @IssueId
    
    exec @Error = p_Issue_Update
     @IssueId      = @issueId,
     @LocationId   = @locationId,
     @DespatchBay  = @locationId,
     @PriorityId   = @priorityId,
     @RouteId      = @routeId
     --@DeliveryDate = @deliveryDate
    
    if @Error <> 0
      goto error
    
    update Issue
       set AreaType = @areaType
     where IssueId = @issueId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  if @outboundShipmentId is not null
    insert @TableResult
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           InstructionId,
           JobId)
    select ili.OutboundShipmentId,
           ili.IssueId,
           ili.IssueLineId,
           ili.InstructionId,
           i.JobId
      from IssueLineInstruction ili
      join Instruction            i on ili.InstructionId = i.InstructionId
     where ili.OutboundShipmentId = isnull(@outboundShipmentId, -1)
  else
    insert @TableResult
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           InstructionId,
           JobId)
    select ili.OutboundShipmentId,
           ili.IssueId,
           ili.IssueLineId,
           ili.InstructionId,
           i.JobId
      from IssueLineInstruction ili
      join Instruction            i on ili.InstructionId = i.InstructionId
     where ili.IssueId = isnull(@issueId, -1)
  
  update u
     set PriorityId = @priorityId
    from @TableResult tr
    join Issue         u (nolock) on tr.IssueId = u.IssueId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update u
     set PriorityId = @priorityId
    from @TableResult tr
    join Job           u (nolock) on tr.JobId = u.JobId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  set @InstructionId = -1
  
  while @InstructionId is not null
  begin
    select @InstructionId = min(InstructionId)
      from @TableResult
    
    delete @TableResult where InstructionId = @InstructionId
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate 
     @InstructionId = @InstructionId,
     @Pick          = 0,
     @Store         = 1,
     @Confirmed     = 0
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Update
     @InstructionId   = @InstructionId,
     @StoreLocationId = @locationId
    
    if @Error <> 0
      goto error

    exec @Error = p_StorageUnitBatchLocation_Allocate 
     @InstructionId = @InstructionId,
     @Pick          = 0,
     @Store         = 1,
     @Confirmed     = 0
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Planning_Load_Update'
    rollback transaction
    return @Error
end
