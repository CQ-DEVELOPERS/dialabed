﻿ 
/*
/// <summary>
///   Procedure Name : p_Report_Purchase_Order_Detail
///   Filename       : p_Report_Purchase_Order_Detail.sql
///   Create By      : Karen
///   Date Created   : 01 Nov 2010
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    : Karen
///   Modified Date  : 2018-05-21
///   Details        : PL001 - Plumblink - add warehouse as a parameter
/// </newpara>
*/
CREATE procedure p_Report_Purchase_Order_Detail
(
@WarehouseId	   int = null,
@OrderNumber       nvarchar(30),
--@ProductCode       nvarchar(30),
@StorageUnitId	   int,
@Batch             nvarchar(50),
@FromDate          datetime,
@ToDate            datetime
)
 
as
begin
  set nocount on;
  
  declare @TableSOH as table
  (
   WarehouseId int,
   StorageUnitBatchId  int,
   StockOnHand int
  )
  
  insert @TableSOH
        (WarehouseId,
         StorageUnitBatchId,
         StockOnHand)
  select a.WarehouseId,
         subl.StorageUnitBatchId,
         sum(subl.ActualQuantity)
    from StorageUnitBatchLocation subl (nolock)
    join AreaLocation               al (nolock) on subl.LocationId        = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
  group by a.WarehouseId,
           subl.StorageUnitBatchId

  
  declare @TableDetails as Table
  (
  OrderNumber          nvarchar(30),
  ExternalOrderNumber  nvarchar(30),
  OrderDate            datetime,
  ExpectedDeliveryDate datetime,
  ProductCode          nvarchar(30),
  ProductDesc          nvarchar(255),
  StorageUnitId		   int,
  SKUCode              nvarchar(50),
  SKU                  nvarchar(50),
  Batch                nvarchar(50),
  OrderQty             numeric(13,6),
  ReceivedQty          numeric(13,6),
  OutstandingBal       numeric(13,6),
  StockOnHand          numeric(21,6)
  )
  
  if @StorageUnitId = '-1'
     set @StorageUnitId = null
         
  if @OrderNumber = '-1'
     set @OrderNumber = null
         
  if @Batch = '-1'
     set @Batch = null
         
  insert @TableDetails
        (OrderNumber,
         OrderDate,
         ExpectedDeliveryDate,
         ProductCode,
         ProductDesc,
         StorageUnitId,
         SKUCode,
         SKU,
         Batch,
         OrderQty,
         ReceivedQty,
         OutstandingBal,
         StockOnHand
         )
  select id.OrderNumber,
         id.CreateDate,
         id.DeliveryDate,
         p.ProductCode,
         p.Product,
         su.StorageUnitId,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         sum(rl.RequiredQuantity),
         sum(rl.ReceivedQuantity),
         sum(rl.RequiredQuantity - isnull(rl.ReceivedQuantity,0)),
         sum(StockOnHand)         
    from InboundDocument     id
     join InboundLine        il on id.InboundDocumentId = il.InboundDocumentId
     join ReceiptLine        rl on il.InboundLineId = rl.InboundLineId
     join StorageUnitBatch  sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
     join Batch               b on sub.BatchId = b.BatchId
     join StorageUnit        su on sub.StorageUnitId = su.StorageUnitId
     join Product             p on su.ProductId = p.ProductId
     join SKU               sku on su.SKUId = sku.SKUId
     left
     join @TableSOH         soh on rl.StorageUnitBatchId = soh.StorageUnitBatchId
                               and id.WarehouseId = soh.WarehouseId
   where id.CreateDate between @FromDate and @ToDate
   and id.WarehouseId = isnull(@WarehouseId,id.WarehouseId)
  group by id.OrderNumber,
           id.CreateDate,
           id.DeliveryDate,
           p.ProductCode,
           p.Product,
           su.StorageUnitId,
           sku.SKUCode,
           sku.SKU,
           b.Batch
  
  select OrderNumber,
         ExternalOrderNumber,
         OrderDate,
         ExpectedDeliveryDate,
         ProductCode,
         ProductDesc,
         SKUCode,
         SKU,
         Batch,
         OrderQty,
         ReceivedQty,
         OutstandingBal,
         StockOnHand
    from @TableDetails
   where OrderNumber = isnull(@OrderNumber, OrderNumber)
	 and StorageUnitId = ISNULL(@StorageUnitId, StorageUnitId)
     --and ProductCode = isnull(@ProductCode, ProductCode)
     and Batch = isnull(@Batch, Batch)
  order by ProductCode
end
 
