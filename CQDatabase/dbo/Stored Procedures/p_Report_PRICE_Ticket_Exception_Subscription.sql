﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_PRICE_Ticket_Exception_Subscription
  ///   Filename       : p_Report_PRICE_Ticket_Exception_Subscription.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_PRICE_Ticket_Exception_Subscription
 
as
begin
	 set nocount on;
	 
	 declare @ReceiptId				int,
			 @ReportTypeId			int
	 
 
	select	@ReportTypeId = ReportTypeId
	  from	ReportType rt
	 where	ReportTypeCode = 'PREX'
    
 
	--select @StorageUnitId = rp.StorageUnitId
	--  from ReportPrinted rp (nolock) 
	-- where rp.PrintedCopies = 0
 --  	   and rp.ReportTypeId = @ReportTypeId
   	   
	 
	select (select Value 
			  from Configuration 
			 where ConfigurationId = 200 
			   and WarehouseId = WarehouseId)    as 'ServerName',
			db_name()							 as 'DatabaseName',
			'Auto Email'						 as 'UserName',
			rp.StorageUnitId					 as 'StorageUnitId',
			cl.EMail							 as 'Email',
			cl.EMail							 as 'TO',
			'Price Exception'					 as 'Subject'
	  from ReportPrinted         rp (nolock)
	  join ReportType            rt (nolock) on rp.ReportTypeId = rt.ReportTypeId
	  join ContactList           cl (nolock) on cl.ReportTypeId = @ReportTypeId
	 where rt.ReportTypeCode = 'PREX'
	 and PrintedCopies = 0
	   --and rl.StorageUnitBatchId = rp.StorageUnitBatchId
  
	update ReportPrinted
	   set PrintedCopies = 1,
		   EmailDate = GETDATE()
	 where PrintedCopies = 0
	   and ReportTypeId = @ReportTypeId
    
end 
 
