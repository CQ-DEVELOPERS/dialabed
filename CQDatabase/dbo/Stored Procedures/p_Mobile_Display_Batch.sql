﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Display_Batch
  ///   Filename       : p_Mobile_Display_Batch.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Display_Batch
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
  
  select b.Batch
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch              b (nolock) on sub.BatchId = b.BatchId
    join AreaLocation      al (nolock) on i.PickLocationId = al.LocationId
    join Area               a (nolock) on al.AreaId = a.AreaId
   where i.InstructionId = @InstructionId
     and a.BatchTracked = 0
end
