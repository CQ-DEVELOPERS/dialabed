﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Replenishment_Request
  ///   Filename       : p_Mobile_Replenishment_Request.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Jul 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Replenishment_Request
(
 @operatorId         int,
 @location           nvarchar(15) = null,
 @barcode            nvarchar(50)= null,
 @StoreLocationId    int = null,
 @StorageUnitBatchId int = null,
 @ShowMsg            bit = 1,
 @ManualReq          bit = 1,
 @AnyLocation        bit = 0,
 @AreaType           nvarchar(10) = null,
 @RequestedQuantity  float = null output,
 @InstructionId      int = null output
)
 
as
begin
  /*ERROR CODES*************************************************************
    1  = 'General Error'
    2  = 'Invalid Location'
    3  = 'Incorrect barcode'
    4  = 'Multiple Products in Pick Bin, please scan Product or PalletId'
    5  = 'Invalid barcode / Location combination'
    6  = 'No stock'
    8  = 'Quantity mismatch, please check product setup'
    9  = 'Replenishment already created'
    10 = 'No Products linked to Pick Bin, cannot create replenishment'
    11 = 'No Areas defined for Product!'
	 *************************************************************************/
	 
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @InstructionTypeId  int,
          @WarehouseId        int,
          @StatusId           int,
          @JobId              int,
          @PickLocationId     int,
          @Quantity           float,
          @Weight             float,
          @PalletId           int,
          @ProductId          int,
          @StorageUnitId      int,
          @Count              int,
          @PriorityId         int,
          @ConfirmedQuantity  float,
          @PFSOH              float,
          @BoxQuantity        float,
          @SOH                float,
          @ReferenceNumber    nvarchar(50),
          @PickAisle          nvarchar(10),
          @PickArea           nvarchar(50),
          @AutoComplete       bit,
          @InstructionCount   int,
          @PalletQuantity     int,
          @MaxLocationQuantity int,
          @SOHQuantity		  int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @location = replace(@location,'L:','')
  select @location = replace(@location,'L:','')
  
  if @StoreLocationId is null
  begin
    select @WarehouseId = WarehouseId
      from Operator (nolock)
     where OperatorId = @OperatorId
    
    select @StoreLocationId = l.LocationId,
           @AreaType        = case when @AreaType is null then a.AreaType else @AreaType end,
           @PalletQuantity  = isnull(l.PalletQuantity,1)
      from Location      l (nolock)
      join AreaLocation al (nolock) on l.LocationId = al.LocationId
      join Area          a (nolock) on al.AreaId = a.AreaId
     where l.Location    = @location
       and a.WarehouseId = @WarehouseId
       and a.AreaCode   in ('PK','PP','SP')
       and a.Area not like '%Mezz%'
       and a.Replenish   = 1
    
    if @StoreLocationId is null
    begin
      set @Error = 2
      set @Errormsg = 'Invalid Location'
      goto result
    end
  end
  else
  begin
    if @AnyLocation = 1
    begin
      select @WarehouseId = a.WarehouseId,
             @AreaType    = case when @AreaType is null then a.AreaType else @AreaType end,
             @PalletQuantity   = isnull(l.PalletQuantity,1)
        from Location      l (nolock)
        join AreaLocation al (nolock) on l.LocationId = al.LocationId
        join Area          a (nolock) on al.AreaId = a.AreaId
       where l.LocationId = @StoreLocationId
         and a.Replenish = 1
      
      if @WarehouseId is null
      begin
        set @Error = 2
        set @Errormsg = 'Invalid Location'
        goto result
      end
    end
    else
    begin
      select @WarehouseId = a.WarehouseId,
             @AreaType    = case when @AreaType is null then a.AreaType else @AreaType end,
             @PalletQuantity   = isnull(l.PalletQuantity,1)
        from Location      l (nolock)
        join AreaLocation al (nolock) on l.LocationId = al.LocationId
        join Area          a (nolock) on al.AreaId = a.AreaId
       where l.LocationId = @StoreLocationId
         and a.AreaCode   in ('PK','PP','SP')
         and a.Area not like '%Mezz%'
         and a.Replenish = 1
      
      if @WarehouseId is null
      begin
        set @Error = 2
        set @Errormsg = 'Invalid Location'
        goto result
      end
    end
  end
  
  if @AnyLocation = 1
    set @Count = 1
  else
    select @Count = count(distinct(sub.StorageUnitId))
      from StorageUnitBatchLocation subl (nolock)
      join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
     where subl.LocationId = @StoreLocationId
  
  if @Count = 0
  begin
    select @Count = count(1)
      from StorageUnitLocation (nolock)
     where LocationId = @StoreLocationId
    
    if @Count = 0
    begin
      set @Error = 10
      set @Errormsg = 'No Products linked to Pick Bin, cannot create replenishment'
      goto result
    end
  end
  
  select top 1
         @StorageUnitId      = sub.StorageUnitId
    from StorageUnitBatch sub (nolock)
   where sub.StorageUnitBatchId = @StorageUnitBatchId
  
  if @Count = 1 and @StorageUnitId is null
  begin
    select top 1
           @StorageUnitBatchId = sub.StorageUnitBatchId,
           @StorageUnitId      = sub.StorageUnitId
      from StorageUnitBatchLocation subl (nolock)
      join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
     where LocationId = @StoreLocationId
    order by ActualQuantity desc
    
    if @@rowcount = 0
    begin
      select @StorageUnitBatchId = sub.StorageUnitBatchId,
             @StorageUnitId      = sub.StorageUnitId
        from StorageUnitLocation       sul (nolock)
        join StorageUnitBatch          sub (nolock) on sul.StorageUnitId = sub.StorageUnitId
        join Batch                       b (nolock) on sub.BatchId       = b.BatchId
       where sul.LocationId = @StoreLocationId
         and b.Batch         = 'Default'
    end
  end
  else if @Count > 1 and (@barcode = '' and @StorageUnitBatchId is null)
  begin
    select @Count = count(1)
      from StorageUnitBatchLocation subl (nolock)
      join StorageUnitBatch sub1         (nolock) on subl.StorageUnitBatchId = sub1.StorageUnitBatchId
      join StorageUnitBatch sub2         (nolock) on subl.StorageUnitBatchId = sub2.StorageUnitBatchId
     where subl.LocationId    = @StoreLocationId
       and sub1.StorageUnitId = sub2.StorageUnitId
       and sub1.BatchId      != sub2.BatchId
    
    if @Count = 1
    begin
      select top 1
             @StorageUnitBatchId = sub.StorageUnitBatchId,
             @StorageUnitId      = sub.StorageUnitId
        from StorageUnitBatchLocation subl (nolock)
        join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
       where LocationId = @StoreLocationId
      order by ActualQuantity desc
      
      if @@rowcount = 0
      begin
        select @StorageUnitBatchId = sub.StorageUnitBatchId,
               @StorageUnitId      = sub.StorageUnitId
          from StorageUnitLocation       sul (nolock)
          join StorageUnitBatch          sub (nolock) on sul.StorageUnitId = sub.StorageUnitId
          join Batch                       b (nolock) on sub.BatchId       = b.BatchId
         where sul.LocationId = @StoreLocationId
           and b.Batch         = 'Default'
      end
    end
    else
    begin
      set @Error = 4
      set @Errormsg = 'Multiple Products in Pick Bin, please scan Product or PalletId'
      goto result
    end
  end
  
--  if @barcode = '' and @StorageUnitBatchId is null
--  begin
--    set @Error = 10
--    set @Errormsg = 'Multiple Product in Pick Bin, please scan Product or PalletId'
--    goto result
--  end
  
  if (@barcode is not null and @barcode != '') and @StorageUnitBatchId is null
  begin
    select @ProductId = ProductId
      from StorageUnit (nolock)
     where StorageUnitId = @StorageUnitId
    
    select @ProductId = ProductId
      from Product (nolock)
     where Barcode = @barcode or ProductCode = @barcode
    
    if @ProductId is null
    begin
      select @StorageUnitId = StorageUnitId
        from Pack
       where Barcode = @barcode
      
      if @StorageUnitId is null
      begin
        set @Error = 3
        set @Errormsg = 'Incorrect barcode'
        goto result
      end
      else
      begin
        select @StorageUnitBatchId = sub.StorageUnitBatchId
          from StorageUnitBatchLocation subl (nolock)
          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
         where subl.LocationId  = @StoreLocationId
           and su.StorageUnitId = @StorageUnitId
      end
    end
    else
    begin
      select @StorageUnitBatchId = sub.StorageUnitBatchId,
             @StorageUnitId      = su.StorageUnitId
        from StorageUnitBatchLocation subl (nolock)
        join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
        join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
        join Product                     p (nolock) on su.ProductId            = p.ProductId
       where subl.LocationId = @StoreLocationId
         and p.ProductId     = @ProductId
    end
  end
  else
  begin
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch
     where StorageUnitBatchId = @StorageUnitBatchId
  end
  
  if @StorageUnitBatchId is null
  begin
    set @Error = 5
    set @Errormsg = 'Invalid barcode / Location combination'
    goto result
  end
  
  if (dbo.ufn_Configuration(470, @WarehouseId) = 1) -- Stop replenishment if pick face has SOH
  begin
    select @PFSOH = sum(subl.ActualQuantity)
      from StorageUnitBatchLocation subl (nolock)
      join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
      join AreaLocation               al (nolock) on subl.LocationId = al.LocationId
      join Area                        a (nolock) on al.AreaId = a.AreaId
     where sub.StorageUnitId = @StorageUnitId
       and a.AreaCode = 'PK'
       and a.WarehouseId = @WarehouseId
       and subl.LocationId = @StoreLocationId
    
    if @PFSOH > 0
    begin
      set @Error = 9
      set @Errormsg = 'Replenishment not needed'
      goto result
    end
  end
  
  select @InstructionTypeId = InstructionTypeId,
         @PriorityId        = PriorityId
    from InstructionType
   where InstructionTypeCode = 'R'
  
  select @InstructionCount = COUNT(distinct(i.InstructionId))
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Status             s (nolock) on i.StatusId           = s.StatusId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where s.StatusCode in ('W','S')
     and i.InstructionTypeId    = @InstructionTypeId -- Replenishment
     and i.WarehouseId          = @WarehouseId       -- Same Warehouse (not really necessary)
     and i.StoreLocationId      = @StoreLocationId   -- Same to Location
     and sub.StorageUnitId      = @StorageUnitId     -- Same product
     and it.InstructionTypeCode = 'R'                -- Replenishment
  
  if isnull(@InstructionCount, 0) >= ISNULL(@PalletQuantity, 1)
  begin
    set @Error = 9
    set @Errormsg = 'Replenishment already created'
    goto result
  end
  
  set @InstructionId = null
  
  select top 1 @Quantity = Quantity
    from Pack      p (nolock)
    join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
   where StorageUnitId = @StorageUnitId
     and WarehouseId = @WarehouseId
  order by pt.OutboundSequence
  
  if @Quantity is null
  begin
    set @Error = 8
    set @Errormsg = 'Quantity mismatch, please check product setup'
    goto result
  end
  
  set @PickLocationId = @StoreLocationId
  
  exec @Error = p_Pick_Location_Get
   @WarehouseId        = @WarehouseId,
   @locationId         = @PickLocationId output,
   @StorageUnitBatchId = @StorageUnitBatchId output,
   @Quantity           = @Quantity output,
   @Replenishment      = 1,
   @AreaType           = @AreaType
  
  if @StorageUnitBatchId = -1
  begin
    set @Error = 11
    set @Errormsg = 'No Areas defined for Product!'
    goto result
  end
  
  if @PickLocationId is null
  begin
    set @Error = 6
    set @Errormsg = 'No stock'
    goto result
  end
  
  if @PickLocationId = @StoreLocationId
  begin
    set @Error = 6
    set @Errormsg = 'No stock found'
    goto result
  end
  
--  select @StorageUnitBatchId = subl.StorageUnitBatchId
--    from StorageUnitBatchLocation subl
--    join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
--   where sub.StorageUnitId = @StorageUnitId
--     and subl.LocationId   = @PickLocationId
  
  set @StatusId = dbo.ufn_StatusId('I','W')
  
  begin transaction
  
  select @StatusId = dbo.ufn_StatusId('IS','RL')
  
  if dbo.ufn_Configuration(140, @WarehouseId) = 1
  begin
    set @PFSOH = null
    
    select @PFSOH = sum(subl.ActualQuantity)
      from StorageUnitBatchLocation subl (nolock)
      join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
      join AreaLocation               al (nolock) on subl.LocationId = al.LocationId
      join Area                        a (nolock) on al.AreaId = a.AreaId
     where sub.StorageUnitId = @StorageUnitId
       and a.AreaCode = 'PK'
       and a.WarehouseId = @WarehouseId
    
    if @PFSOH is null
       set @PFSOH = 0
    
    if @PFSOH <= 0
      select top 1 @PriorityId = PriorityId
        from Priority
      order by OrderBy
    else
      select top 1 @PriorityId = PriorityId
        from Priority
      order by OrderBy desc
  end
  
  if dbo.ufn_Configuration(472, @WarehouseId) = 1 -- Replenishments - Group jobs by Aisles
  begin
    select @PickAisle = l.Ailse,
           @PickArea = a.Area
      from Location l (nolock)
      join AreaLocation al (nolock) on l.LocationId = al.LocationId
      join Area a (nolock) on al.AreaId = a.AreaId
     where l.LocationId = @PickLocationId
    
    select @ReferenceNumber = 'Replenish - ' + ISNULL(@PickArea, '') + ' ' + isnull(@PickAisle,'Undefind');
    
    select @JobId = max(JobId)
      from Job
     where StatusId = @StatusId
       and ReferenceNumber = @ReferenceNumber
    
    set @AutoComplete = 1
    
    if (select COUNT(1) from Instruction (nolock) where JobId = @JobId) >= 10
      set @JobId = null
  end
  
  if @JobId is null
  begin
    exec @Error = p_Job_Insert
     @JobId            = @JobId output,
     @PriorityId       = @PriorityId,
     @OperatorId       = null,
     @StatusId         = @StatusId,
     @WarehouseId      = @WarehouseId,
     @ReferenceNumber  = @ReferenceNumber
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Error
    end
  end
  select @StatusId = dbo.ufn_StatusId('I','W')
  
  
  if @RequestedQuantity is not null
  begin
    --select @Quantity as '@Quantity', @RequestedQuantity as '@RequestedQuantity', * from viewSOH where StorageUnitBatchId = @StorageUnitBatchId and LocationId = @PickLocationId
    
    select @SOH = ActualQuantity - ReservedQuantity
      from StorageUnitBatchLocation
     where StorageUnitBatchId = @StorageUnitBatchId
       and LocationId = @PickLocationId
    
    if @SOH > @RequestedQuantity
      if dbo.ufn_Configuration(400, @WarehouseId) = 1
      begin
        set @Quantity = @SOH
        set @RequestedQuantity = @SOH
      end
      else
        set @Quantity = @RequestedQuantity
    else
    begin
      set @Quantity = @SOH
      set @RequestedQuantity = @Quantity
    end
  end
  else if dbo.ufn_Configuration(244, @WarehouseId) = 1
  begin
    select top 1 @BoxQuantity = Quantity
      from Pack      p (nolock)
      join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
     where StorageUnitId = @StorageUnitId
       and WarehouseId = @WarehouseId
       and pt.PackTypeCode = 'Box'
    
    if @BoxQuantity is not null
      select @Quantity = @BoxQuantity
    else
      select @Quantity = 1
  end
  
  if dbo.ufn_Configuration(31, @WarehouseId) = 0
    set @ConfirmedQuantity = 0
  else
    set @ConfirmedQuantity = @Quantity
  
  select @MaxLocationQuantity = sul.MaximumQuantity
    from StorageUnitLocation sul (nolock)  
   where sul.StorageUnitId = @StorageUnitId  
     and sul.LocationId    = @StoreLocationId
     
  select @SOHQuantity = sum(subl.ActualQuantity) + sum(subl.AllocatedQuantity)  
	from StorageUnitBatchLocation subl (nolock)
	join StorageUnitBatch		   sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
	join StorageUnit				su (nolock) on sub.StorageUnitId  = su.StorageUnitId
   where su.StorageUnitId = @StorageUnitId  
	 and subl.LocationId    = @StoreLocationId  
  
  if @SOHQuantity is null
	select @SOHQuantity = 0
  
  if (@Quantity + @SOHQuantity > @MaxLocationQuantity)
  begin
	 set @Quantity = @MaxLocationQuantity - @SOHQuantity
	 set @ConfirmedQuantity = @Quantity
  end
  
  exec @Error = p_Instruction_Insert
   @InstructionId       = @InstructionId output,
   @InstructionTypeId   = @InstructionTypeId,
   @StorageUnitBatchId  = @StorageUnitBatchId,
   @WarehouseId         = @WarehouseId,
   @StatusId            = @StatusId,
   @JobId               = @JobId,
   @OperatorId          = @OperatorId,
   @PickLocationId      = @PickLocationId,
   @StoreLocationId     = @StoreLocationId,
   @InstructionRefId    = null,
   @Quantity            = @Quantity,
   @ConfirmedQuantity   = @ConfirmedQuantity,
   @Weight              = @Weight,
   @CheckQuantity       = @ManualReq,
   @ConfirmedWeight     = 0,
   @PalletId            = @PalletId,
   @CreateDate          = @GetDate,
   @AutoComplete        = @AutoComplete
  
  if @Error <> 0
  begin
    set @Error = 1
    goto Error
  end
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @InstructionId
  
  if @Error <> 0
  begin
    set @Error = 1
    goto Error
  end
  
  set @Errormsg = 'Replenishment successfully created'
  
  commit transaction
  
  if isnull(@InstructionCount, 0) + 1 < ISNULL(@PalletQuantity, 1) -- Replenishment was create but call proc again if required to replenishs multiple pallets
  begin
    exec @Error = p_Mobile_Replenishment_Request
     @storeLocationId    = @StoreLocationId,
     @storageUnitBatchId = @StorageUnitBatchId,
     @operatorId         = @OperatorId,
     @ShowMsg            = @ShowMsg,
     @ManualReq          = 0
  end
  
  set @Error = 0
  goto result
  
  error:
    rollback transaction
  
  result:
  if @ShowMsg = 1
    select @Error    as 'Result',
           @Errormsg as 'Message'
  else
    return @Error
end
