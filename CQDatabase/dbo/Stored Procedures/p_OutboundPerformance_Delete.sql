﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundPerformance_Delete
  ///   Filename       : p_OutboundPerformance_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:35
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OutboundPerformance table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundPerformance_Delete
(
 @JobId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete OutboundPerformance
     where JobId = @JobId
  
  select @Error = @@Error
  
  
  return @Error
  
end
