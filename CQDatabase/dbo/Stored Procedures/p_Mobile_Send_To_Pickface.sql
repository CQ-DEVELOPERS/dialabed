﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Send_To_Pickface
  ///   Filename       : p_Mobile_Send_To_Pickface.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Send_To_Pickface
(
 @instructionId   int
)
 
as
begin
	 set nocount on;
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @InstructionTypeCode nvarchar(10),
          @WarehouseId         int,
          @LocationId          int,
          @StorageUnitBatchId  int,
          @Quantity            float,
          @PickLocationId      int,
          @OldPickLocationId   int,
          @StoreLocationId     int,
          @OperatorId          int,
          @NoStockId           int,
          @JobId               int,
          @Replenishment       bit,
          @StorageUnitId       int,
          @LIFO                bit,
          @MinimumShelfLife    numeric(13,3),
          @Temporary           bit,
          @PickAreaCode        nvarchar(10),
          @InstructionTypeId   int
  
  select @GetDate = dbo.ufn_Getdate()
  select @NoStockId = dbo.ufn_StatusId('I','NS')
  set @Replenishment = 0
  
  set @Error = 0
  
  select @InstructionTypeCode = it.InstructionTypeCode,
         @WarehouseId         = i.WarehouseId,
         @Quantity            = i.Quantity,
         @StorageUnitBatchId  = i.StorageUnitBatchId,
         @OldPickLocationId   = i.PickLocationId,
         @StoreLocationId     = i.StoreLocationId,
         @OperatorId          = i.OperatorId,
         @JobId               = i.JobId
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status           s (nolock) on i.StatusId          = s.StatusId
   where i.InstructionId = @instructionId
     and s.StatusCode in ('W','S')
  
  if @@Rowcount = 0
  begin
    set @Error = 0
    goto result
  end
  
  exec @Error = p_Instruction_Reset
   @InstructionId       = @instructionId
  
  if @Error <> 0
  begin
    set @Error = 1
    goto result
  end
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId       = @instructionId
  
  if @Error <> 0
  begin
    set @Error = 1
    goto result
  end
  
  if @InstructionTypeCode = 'P'
  begin
    select @InstructionTypeId = InstructionTypeId,
           @InstructionTypeCode = InstructionTypeCode
      from InstructionType (nolock)
     where InstructionTypeCode = 'FM'
  end
  
--  select @PickAreaCode = AreaCode
--    from AreaLocation al (nolock)
--    join Area          a (nolock) on al.AreaId = a.AreaId
--   where LocationId = @OldPickLocationId
--  
--  if @createStockTake = 1
--    if @PickAreaCode in ('PK','SP','TP')
--      set @createStockTake = 0
--  
--  if @createStockTake = 1
--  begin
--    if @PickAreaCode = 'BK'
--    begin
--      exec @Error = p_Housekeeping_Stock_Take_Create_Product
--       @warehouseId        = @WarehouseId,
--       @operatorId         = @OperatorId,
--       @locationId         = @OldPickLocationId,
--       @storageUnitBatchId = @StorageUnitBatchId
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
--    end
--    else
--    begin
--      exec @Error = p_Housekeeping_Stock_Take_Create_Product
--       @warehouseId        = @WarehouseId,
--       @operatorId         = @OperatorId,
--       @locationId         = @OldPickLocationId
--       --@storageUnitBatchId = @StorageUnitBatchId
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
--    end
--  end
  
  set @PickLocationId = @OldPickLocationId
  
  if @InstructionTypeCode in ('R','PM','FM','PS')
  begin
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch (nolock)
     where StorageUnitBatchId = @StorageUnitBatchId
    
    if @InstructionTypeCode = 'R'
    begin
      set @Replenishment = 1
    
      select top 1 @Quantity = Quantity
        from Pack      p (nolock)
        join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
       where StorageUnitId = @StorageUnitId
         and WarehouseId   = @WarehouseId
      order by pt.OutboundSequence
    end
    else
    begin
      select top 1 @Quantity = Quantity
        from Pack      p (nolock)
        join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
       where StorageUnitId = @StorageUnitId
         and WarehouseId   = @WarehouseId
      order by pt.OutboundSequence desc
    end
  end
  
  select @LIFO             = odt.LIFO,
         @MinimumShelfLife = odt.MinimumShelfLife
    from IssueLineInstruction ili (nolock)
    join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where ili.InstructionId = @instructionId
  
  select @LIFO             = odt.LIFO,
         @MinimumShelfLife = odt.MinimumShelfLife
    from IssueLineInstruction ili (nolock)
    join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where ili.InstructionId = (select InstructionRefId from Instruction (nolock) where InstructionId = @instructionId)
  
  if @LIFO is null
    set @LIFO = 0
  
  if @Temporary is null
    set @Temporary = 0
  
  if dbo.ufn_Configuration(133, @WarehouseId) = 1
    if @MinimumShelfLife is not null or @LIFO = 1
       set @Temporary = 1
     else
       set @Temporary = 0
  
  exec @Error = p_Pick_Location_Get
   @WarehouseId        = @WarehouseId,
   @LocationId         = @PickLocationId output,
   @StorageUnitBatchId = @StorageUnitBatchId output,
   @Quantity           = @Quantity output,
   @InstructionId      = @InstructionId,
   @Replenishment      = @Replenishment,
   @LIFO               = @LIFO,
   @MinimumShelfLife   = @MinimumShelfLife,
   @Temporary          = @Temporary
  
  if @Error <> 0
  begin
    set @Error = 1
    goto result
  end
  
  if @InstructionTypeCode in ('M','P','R') or (@InstructionTypeCode in ('PM','PS','FM') and @Temporary = 1)
  begin
    if @PickLocationId is null or (@Temporary = 0 and @PickLocationId = @OldPickLocationId)
    begin
      exec @Error = p_Instruction_Update
       @InstructionId   = @InstructionId,
       @PickLocationId  = @PickLocationId,
       @StoreLocationId = @StoreLocationId,
       @StatusId        = @NoStockId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto result
      end
      
      -- Check to see if all lines are no-stock and then updates job too.
      if (select count(1) from Instruction (nolock) where JobId = @JobId and StatusId != @NoStockId) = 0
      begin
        exec @Error = p_Job_Update
         @JobId    = @JobId,
         @StatusId = @NoStockId
        
        if @Error <> 0
        begin
          set @Error = 1
          goto result
        end
      end
      
      goto result
    end
    
    if @InstructionTypeCode in ('M','P','R')
    begin
      update Job
         set OperatorId = null
       where JobId = @JobId
      
      select @Error = @@Error
      
      if @Error <> 0
      begin
        set @Error = 1
        goto result
      end
    end
  end
  
  if @InstructionTypeCode = 'R'
  begin
    exec @Error = p_Instruction_Update
     @InstructionId      = @instructionId,
     @PickLocationId     = @PickLocationId,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity,
     @ConfirmedQuantity  = @Quantity
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
  end
  else
  begin
    exec @Error = p_Instruction_Update
     @InstructionId      = @instructionId,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @PickLocationId     = @PickLocationId,
     @InstructionTypeId  = @InstructionTypeId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
    
    update Job set OperatorId = null where JobId = @JobId
    
    select @Error = @@Error
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
  end
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @instructionId
  
  if @Error <> 0
  begin
    set @Error = 1
    goto result
  end
  
  result:
    select @Error
    return @Error
end
