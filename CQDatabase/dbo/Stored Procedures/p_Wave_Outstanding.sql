﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Outstanding
  ///   Filename       : p_Wave_Outstanding.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Outstanding
(
 @WarehouseId int
)
 
as
begin
	 set nocount on;
	 
	 select -1 as 'WaveId',
	        '{All}' as Wave
  union
  select distinct w.WaveId,
         w.Wave
    from Wave  w (nolock)
    join Issue i (nolock) on w.WaveId = i.WaveId
    join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
    join Status s (nolock) on i.StatusId = s.StatusId
                         and s.StatusCode in ('M','RL','PC','PS','CK','A','WC','QA','S')
   where w.WarehouseId = @WarehouseId
     and OrderNumber like 'WAV%'
  order by WaveId
end
