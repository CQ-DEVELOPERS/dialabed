﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMasterFile_List
  ///   Filename       : p_InterfaceMasterFile_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:29:21
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceMasterFile table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceMasterFile.MasterFileId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMasterFile_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as MasterFileId
        ,null as 'InterfaceMasterFile'
  union
  select
         InterfaceMasterFile.MasterFileId
        ,InterfaceMasterFile.MasterFileId as 'InterfaceMasterFile'
    from InterfaceMasterFile
  
end
