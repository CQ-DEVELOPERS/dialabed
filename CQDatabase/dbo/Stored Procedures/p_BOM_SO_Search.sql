﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_SO_Search
  ///   Filename       : p_BOM_SO_Search.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@ProductCode> 
  ///       <@Product>	 
  ///       <@Barcode>	 
  /// </param>
  /// <returns>
  ///        <ProductId>
  ///        <StatusId> 
  ///        <Status> 
  ///        <ProductCode> 
  ///        <Product> 
  ///        <Barcode> 
  ///        <MinimumQuantity> 
  ///        <ReorderQuantity> 
  ///        <MaximumQuantity> 
  ///        <CuringPeriodDays>        
  ///        <ShelfLifeDays> 
  ///        <QualityAssuranceIndicator>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_SO_Search
(
 @OrderNumber         nvarchar(30) = null,
 @ProductCode         nvarchar(30),
 @Product	            nvarchar(50),
 @Barcode	            nvarchar(50),
 @FromDate            datetime = '1900-01-01',
 @ToDate              datetime = '2069-01-01'
)
 
as

  begin
	 set nocount on;

  declare @TableHeader as table
  (
   WarehouseId           int              null,
   BOMOrderNumber        nvarchar(46)     null,
   OutboundShipmentId    int              null,
   OutboundDocumentId    int              null,
   IssueId               int              null,
   OrderNumber           nvarchar(60)     null,
   DeliveryDate          datetime         null,
   SOH                   float            null,
   Quantity              float            null,
   Total                 float            null,
   ShipmentTotal         float            null,
   ProductId             int              null,
   StatusId              int              null,
   Status                nvarchar(100)    null,
   StorageUnitId         int              null,
   ProductCode           nvarchar(60)     null,
   Product               nvarchar(100)    null,
   Barcode               nvarchar(100)    null,
   MaximumQuantity       float            null,
   BOMHeaderId           int              null,
   BOMInstructionId      int              null,
   Description           nvarchar(100)    null,
   Remarks               nvarchar(100)    null,
   Instruction           text             null,
   BOMType               nvarchar(100)    null,
   ExternalCompanyId     int,
   ExternalCompany       nvarchar(255)
  )
	 
  declare @TableResult as table
  (OrderNumber           nvarchar(30)
  ,LineNumber            Int
  ,Quantity              float
  ,ProductCode           nvarchar(30)
  ,Product               nvarchar(50)
  ,BOMHeaderId           Int
  ,BOMLineId             Int
  ,StorageUnitId         int
  ,RequiredQuantity      float default 0
  ,ConfirmedQuatity      float default 0
  ,AllocatedQuantity     float default 0
  ,ManufacturingQuantity float default 0
  ,RawQuantity           float default 0
  ,ReplenishmentQuantity float default 0
  ,AvailableQuantity     float default 0
  ,AvailabilityIndicator nvarchar(20)
  ,AvailablePercentage   float default 0
  ,AreaType              nvarchar(10) default ''
  ,WarehouseId           int
  ,DropSequence          int
  ,MaximumQuantity       float
  )
  
  declare @TableDetail as table
  (
   OutboundShipmentId int,
   IssueId int,
   StorageUnitId int,
   Quantity float
  )
	 
  insert @TableHeader
        (WarehouseId,
         BOMOrderNumber,
         OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         OrderNumber,
         DeliveryDate,
         Quantity,
         ProductId,
         StatusId,
         Status,
         StorageUnitId,
         ProductCode,
         Product,
         BOMHeaderId,
         Description,
         Remarks,
         Instruction,
         BOMType,
         ExternalCompanyId)
  select i.WarehouseId,
         'MO-' + isnull(convert(nvarchar(20), OutboundShipmentId), od.OrderNumber) as 'BOMOrderNumber',
         isnull(osi.OutboundShipmentId, -1) as 'OutboundShipmentId',
         od.OutboundDocumentId,
         i.IssueId,
         od.OrderNumber,
         isnull(i.DeliveryDate, od.CreateDate),
         il.Quantity - isnull(il.ConfirmedQuatity,0),
         p.ProductId, 
         p.StatusId, 
         s.Status,
         su.StorageUnitId, 
         p.ProductCode, 
         p.Product, 
         bh.BOMHeaderId,
         bh.Description,
         bh.Remarks,
         bh.Instruction,
         bh.BOMType,
         od.ExternalCompanyId
    from OutboundDocument        od (nolock)
    join OutboundDocumentType   odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join Issue                    i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    Inner join Status             s (nolock) on i.StatusId = s.StatusId
    left join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
    Inner join IssueLine         il (nolock) on i.IssueId = il.IssueId
    Inner join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
    inner join BOMHeader         bh (nolock) on sub.StorageUnitId = bh.StorageUnitId
    Inner join StorageUnit       su (nolock) ON bh.StorageUnitId = su.StorageUnitId 
    Inner join Product            p (nolock) ON su.ProductId = p.ProductId
   where od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
     and p.ProductCode          like isnull(@ProductCode,p.ProductCode) + '%'
			  And p.Product              like isnull(@Product,p.Product) + '%'
			  And isnull(p.Barcode,'')   like isnull(@Barcode,isnull(p.Barcode,'')) + '%'
			  and s.StatusCode              = 'W'
			  and isnull(i.DeliveryDate, od.CreateDate) between @FromDate and @ToDate
			  and odt.OutboundDocumentTypeCode = 'PRQ'
  
  INSERT @TableResult
           (WarehouseId
           ,AreaType
           ,OrderNumber
           ,LineNumber
           ,RequiredQuantity
           ,ProductCode
           ,Product
           ,BOMHeaderId
           ,BOMLineId
           ,StorageUnitId)
  SELECT od.WarehouseId
           --,i.AreaType
           ,null
           --,case when osi.OutboundShipmentId is not null
           --      then convert(varchar(10), osi.OutboundShipmentId)
           --      else od.OrderNumber
           --      end
           ,null
           --,ol.LineNumber
           ,null
           ,sum(il.Quantity*bp.Quantity)
           ,P.ProductCode
           ,P.Product
           --,null as BOMInstructionId
           ,bh.BOMHeaderId
           ,-1 as BOMLineId
           ,bp.StorageUnitId
      FROM	@TableHeader            th
      join OutboundDocument        od (nolock) on th.OutboundDocumentId = od.OutboundDocumentId
      join OutboundLine            ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId
      join Issue                    i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
                                              and i.IssueId = th.IssueId
      left
      join OutboundShipmentIssue  osi (nolock) on i.IssueId = osi.IssueId
      join IssueLine               il (nolock) on i.IssueId = il.IssueId
      join StorageUnitBatch       sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join BOMheader               bh (nolock) on sub.StorageUnitId = bh.StorageUnitId
      join BOMLine                 bl (nolock) on bh.BOMHeaderId = bl.BOMHeaderId
      join BOMProduct              bp (nolock) on bl.BOMLineId = bp.BOMLineId
      join StorageUnit             su (nolock) on bp.StorageUnitId = su.StorageUnitId 
      join Product                  p (nolock) on su.ProductId = p.ProductId
      join Status                   s (nolock) on p.StatusId = s.StatusId
     where bh.BOMHeaderId in (select distinct BOMHeaderId from @TableHeader)
    group by od.WarehouseId
            --,i.AreaType
            --,case when osi.OutboundShipmentId is not null
            --     then convert(varchar(10), osi.OutboundShipmentId)
            --     else od.OrderNumber
            --     end
            --,ol.LineNumber
            ,P.ProductCode
            ,P.Product
            ,bh.BOMHeaderId
            ,bp.StorageUnitId
            ,th.ExternalCompanyId
  
  /* Start
     keep this same as in p_BOM_SO_InstructionLine */
  update tr
     set AllocatedQuantity = a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ManufacturingQuantity = a.ActualQuantity - a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set RAWQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set AvailableQuantity = ManufacturingQuantity + RAWQuantity
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), AvailableQuantity) / convert(numeric(13,3), RequiredQuantity)) * 100
   where AllocatedQuantity + AvailableQuantity != 0
     and RequiredQuantity > 0
     and convert(numeric(13,3), RequiredQuantity) > 0
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity   > ManufacturingQuantity
     and RequiredQuantity   > ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity  <= ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where ManufacturingQuantity >= RequiredQuantity
  
  /* End
     keep this same as in p_BOM_SO_InstructionLine */
  
  update @TableResult set MaximumQuantity = convert(numeric(13,6), RequiredQuantity) * (convert(numeric(13,6), AvailablePercentage) / 100.000000)
  --select * from @TableResult
  ----update @TableResult set MaximumQuantity = convert(numeric(13,6), RequiredQuantity) / (convert(numeric(13,6), MaximumQuantity)) * 100 where MaximumQuantity > 0
  update @TableResult set MaximumQuantity = convert(numeric(13,6), MaximumQuantity) / (convert(numeric(13,6), RequiredQuantity)) * 100 where MaximumQuantity > 0
  --select 6 * .8
  --select * from @TableResult
  update tr
     set MaximumQuantity = (select min(MaximumQuantity)
                              from @TableResult mx
                             where tr.BOMHeaderId = mx.BOMHeaderId)
    from @TableResult tr
  
  update th
     set BOMInstructionId = bi.BOMInstructionId
    from @TableHeader th
    join BOMInstruction bi on th.BOMHeaderId = bi.BOMHeaderId
  
  --update th
  --   set MaximumQuantity = convert(numeric(13,6), tr.RequiredQuantity) * (convert(numeric(13,6), tr.MaximumQuantity) / 100.000000)
  --  from @TableHeader th
  --  join @TableResult tr on th.BOMHeaderId = tr.BOMHeaderId
  
  update th
     set MaximumQuantity = tr.MaximumQuantity
    from @TableHeader th
    join @TableResult tr on th.BOMHeaderId = tr.BOMHeaderId
  
  insert @TableDetail
        (OutboundShipmentId,
         IssueId,
         StorageUnitId,
         Quantity)
  select OutboundShipmentId,
         max(IssueId),
         StorageUnitId,
         sum(Quantity)
    from @TableHeader
   where isnull(OutboundShipmentId,-1) != -1
  group by OutboundShipmentId,
           StorageUnitId
  
  update tr
     set Total = td.Quantity
    from @TableHeader tr
    join @TableDetail td on tr.OutboundShipmentId = td.OutboundShipmentId
                        and tr.IssueId = td.IssueId
                        and tr.StorageUnitId = tr.StorageUnitId
  
  update @TableHeader
     set Total = Quantity,
         ShipmentTotal = Quantity
    from @TableHeader tr
   where isnull(tr.OutboundShipmentId,-1) = -1
  
  update tr
     set ShipmentTotal = td.Quantity
    from @TableHeader tr
    join @TableDetail td on tr.OutboundShipmentId = td.OutboundShipmentId
                        and tr.StorageUnitId = tr.StorageUnitId
  
  update tr
     set SOH = a.ActualQuantity
    from @TableHeader tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and tr.StorageUnitId = a.StorageUnitId
                        and a.AreaType       = 'Finished'
  
  update th
     set Status = s.Status
    from @TableHeader     th
    join OutboundDocument od (nolock) on od.OrderNumber        = left(th.OrderNumber,charindex(':', th.OrderNumber)-1)
    join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join Status            s (nolock) on i.StatusId            = s.StatusId
   where charindex(':', th.OrderNumber) > 0
  
  update th
     set ExternalCompany = ec.ExternalCompany
    from @TableHeader    th
    join ExternalCompany ec (nolock) on th.ExternalCompanyId = ec.ExternalCompanyId
  
  select BOMOrderNumber,
         OutboundShipmentId,
         IssueId,
         OrderNumber,
         DeliveryDate,
         SOH,
         Quantity,
         convert(numeric(13,3), (convert(numeric(13,6), MaximumQuantity) / 100.000000) * convert(numeric(13,6), Quantity)) as 'MaximumQuantity' ,
         Total,--'Total: ' + convert(nvarchar(30), Total) as 'Total',
         ShipmentTotal,
         case when Total is null
              then 0
              else 1
              end as 'ViewSOH',
         ProductCode,
         Product,
         Barcode,
         BOMHeaderId,
         Description,
         Remarks,
         Instruction,
         BOMType,
         Status,
         ExternalCompany
    from @TableHeader
  order by ProductCode,
           OutboundShipmentId,
           OrderNumber
end
