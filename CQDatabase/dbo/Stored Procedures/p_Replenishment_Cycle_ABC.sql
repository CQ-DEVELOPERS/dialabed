﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Replenishment_Cycle_ABC
  ///   Filename       : p_Replenishment_Cycle_ABC.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Replenishment_Cycle_ABC
(
 @WarehouseId int,
 @FromDate    datetime = null,
 @ToDate      datetime = null,
 @A           numeric(10) = 80,
 @B           numeric(10) = 95
)
 
as
begin
	 set nocount on;
  
  declare @Ranking       int,
          @StorageUnitId int,
          @LocationId    int
  
  declare @TableResult as table
  (
   Warehouse       nvarchar(50),
   Ranking         int,
   StorageUnitId   int,
   ProductCode     nvarchar(30),
   Product         nvarchar(50),
   SKUCode         nvarchar(50),
   Visits          numeric(13,3),
   TotalMonthly    numeric(13,3),
   Cumulative      numeric(13,3),
   CumulativeItems numeric(13,3),
   Category        char(1),
   LocationId      int,
   Location        nvarchar(15),
   Lines           numeric(13,3)
  )
  
  if @FromDate is null
    set @FromDate = dateadd(dd, -7, getdate())
  
  if @ToDate is null
    set @ToDate = getdate()
  
  insert @TableResult
        (StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         LocationId,
         Location)
  select distinct
         vs.StorageUnitId,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vl.LocationId,
         vl.Location
    from viewProduct vs
    join StorageUnitLocation sul on vs.StorageUnitId = sul.StorageUnitId
    join viewLocation         vl on sul.LocationId   = vl.LocationId
   where vl.AreaCode = 'PK'
     and vl.WarehouseId = @WarehouseId
  order by vl.Location
  
  update tr
     set Visits = (select count(1)
                     from Instruction i
                     join InstructionType it on i.InstructionTypeId = it.InstructionTypeId
                     join Status           s on i.StatusId          = s.StatusId
                    where it.InstructionTypeCode = 'R'
                      and s.StatusCode = 'F'
                      and i.ConfirmedQuantity > 0
                      and i.StoreLocationId = tr.LocationId
                      and i.EndDate between @FromDate and @ToDate)
    from @TableResult tr
  
  update @TableResult
     set TotalMonthly = (Visits / (select sum(Visits) from @TableResult)) * 100.000
   where Visits > 0
  
  set @Ranking = 0
  set rowcount 1
  
  while exists (select top 1 1 from @TableResult where Ranking is null)
  begin
    select @Ranking = @Ranking + 1
    
    select top 1
           @StorageUnitId = StorageUnitId,
           @LocationId    = LocationId
      from @TableResult
     where Ranking is null
    order by Visits desc
    
    update @TableResult
       set Ranking = @Ranking,
           Cumulative = TotalMonthly + isnull((select max(Cumulative) from @TableResult),0)
     where StorageUnitId = @StorageUnitId
       and LocationId    = @LocationId
  end
  
  set rowcount 0
  
--  set rowcount 1
--  
--  while exists (select top 1 1 from @TableResult where Cumulative is null)
--  begin
--    update @TableResult
--       set Cumulative = isnull(TotalMonthly,0) + isnull((select max(Cumulative) from @TableResult),0)
--     where Cumulative is null
--  end
--  
--  set rowcount 0
  
  update @TableResult
     set CumulativeItems = (convert(numeric(13,3), Ranking) / (select convert(numeric(13,3), max(Ranking)) from @TableResult)) * 100.000
  
  update @TableResult
     set Category = 'A'
   where Cumulative < @A
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'A')
   where Lines is null
     and Category = 'A'
  set rowcount 0
  
  update @TableResult
     set Category = 'B'
   where Cumulative between @A and @B
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'B') - (select max(CumulativeItems) from @TableResult where Category = 'A')
   where Lines is null
     and Category = 'B'
  set rowcount 0
  
  update @TableResult
     set Category = 'C'
   where isnull(Cumulative,@B + 1) > @B
  
  set rowcount 1
  update @TableResult
     set Lines = (select max(CumulativeItems) from @TableResult where Category = 'C') - (select max(CumulativeItems) from @TableResult where Category = 'B')
   where Lines is null
     and Category = 'C'
  set rowcount 0
  
  update @TableResult
     set TotalMonthly = 100
   where TotalMonthly > 100
  
  update @TableResult
     set Cumulative = 100
   where Cumulative > 100
  
  update @TableResult
     set CumulativeItems = 100
   where CumulativeItems > 100
  
  update @TableResult
     set Warehouse = w.Warehouse
    from Warehouse w
   where w.WarehouseId = @WarehouseId
  
  update su
     set ProductCategory = tr.Category
    from @TableResult tr
    join StorageUnit  su on tr.StorageUnitId = su.StorageUnitId
  
  update StorageUnit
     set ProductCategory = 'D'
   where ProductCategory is null
  
  select Warehouse,
         Ranking,
         ProductCode,
         Product,
         SKUCode,
         Visits,
         TotalMonthly,
         convert(numeric(13,2), Cumulative) as 'Cumulative',
         convert(numeric(13,2), CumulativeItems) as 'CumulativeItems',
         Category,
         Location,
         Lines
    from @TableResult
  order by Ranking
end
