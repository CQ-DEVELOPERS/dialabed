﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Intergration_Get_FileExtension
  ///   Filename       : p_Intergration_Get_FileExtension.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Mar 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Intergration_Get_FileExtension
(
 @interfaceMappingFileId int,
 @fileExtension  nvarchar(50) output
)
 
as
begin
  set nocount on;
  
  select @fileExtension = isnull(imf.FileExtension, 'txt')
    from InterfaceMappingFile   imf (nolock)
   where imf.InterfaceMappingFileId = @interfaceMappingFileId
end
