﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_List
  ///   Filename       : p_Instruction_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Aug 2013 13:13:51
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Instruction table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Instruction.InstructionId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InstructionId
        ,null as 'Instruction'
  union
  select
         Instruction.InstructionId
        ,Instruction.InstructionId as 'Instruction'
    from Instruction
  
end
