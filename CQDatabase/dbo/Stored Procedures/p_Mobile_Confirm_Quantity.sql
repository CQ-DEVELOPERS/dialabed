﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Confirm_Quantity
  ///   Filename       : p_Mobile_Confirm_Quantity.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Mobile_Confirm_Quantity]  
(  
 @instructionId     int output,  
 @confirmedQuantity float
)  
   
as  
begin  
  set nocount on;  
    
  declare @MobileLogId int  
    
  insert MobileLog  
        (ProcName,  
         InstructionId,  
         Quantity,  
         StartDate)  
  select OBJECT_NAME(@@PROCID),  
         @InstructionId,  
         @confirmedQuantity,  
         getdate()  
    
  select @MobileLogId = scope_identity()  
    
  declare @Error              int,  
          @GetDate            datetime,  
          @InstructionTypeId  int,  
          @StorageUnitBatchId int,  
          @WarehouseId        int,  
          @StatusId           int,  
          @JobId              int,  
          @OperatorId         int,  
          @PickLocationId     int,  
          @StoreLocationId    int,  
          @Quantity           float,  
          @Weight             float,  
          @ConfirmedWeight    float,  
          @PalletId           int,  
          @ReceiptLineId      int,  
          @IssueLineId        int,  
          @Picked             bit,  
          @InstructionTypeCode nvarchar(10),
          @SOH                 float
    
  select @GetDate = dbo.ufn_Getdate()  
    
  set @Error = 0  
    
  select @InstructionTypeId   = InstructionTypeId,  
         @StorageUnitBatchId  = StorageUnitBatchId,  
         @WarehouseId         = WarehouseId,  
         @StatusId            = StatusId,  
         @JobId               = JobId,  
         @OperatorId          = OperatorId,  
         @PickLocationId      = PickLocationId,  
         @StoreLocationId     = StoreLocationId,  
         @Quantity            = Quantity,  
         @PalletId            = PalletId,  
         @ReceiptLineId       = ReceiptLineId,  
         @IssueLineId         = IssueLineId,  
         @Picked              = Picked  
    from Instruction (nolock)  
   where InstructionId = @instructionId  
    
  select @InstructionTypeCode = InstructionTypeCode  
    from InstructionType (nolock)  
   where InstructionTypeId = @InstructionTypeId  
    
  if @StatusId not in (select StatusId  
                         from Status (nolock)  
                        where StatusCode in ('W','S'))  
  begin  
    set @Error = 5 -- Invliad Status  
    goto Result  
  end  
  
  if @confirmedQuantity < 0 
    begin  
      set @Error = 7  
      goto Result  
    end

    
  if @InstructionTypeCode != 'R' and (select dbo.ufn_Configuration(21, @WarehouseId)) = 0  
    if @confirmedQuantity > @Quantity  
    begin  
      set @Error = 7  
      goto Result  
    end  
  
  if @InstructionTypeCode in ('P','PM','FM','PS') and (select dbo.ufn_Configuration(417, @WarehouseId)) = 1 -- Mixed Pick - Dont allow > 0 Default Batch
    if @confirmedQuantity > 0
    begin
      if (select Batch 
           from StorageUnitBatch sub
           join Batch              b on sub.BatchId = b.BatchId
          where StorageUnitBatchId = @StorageUnitBatchId) = 'Default'
      begin
        set @Error = 9
        goto Result
      end
    end  
    
  --if @InstructionTypeCode in ('PR','S','SM')  
  --and @confirmedQuantity < @Quantity  
  --begin  
  --  set @Error = 7  
  --  goto Result  
  --end
  
  select @SOH = ActualQuantity
    from Instruction                 i (nolock)
    join StorageUnitBatchLocation subl (nolock) on i.StorageUnitBatchId = subl.StorageUnitBatchId
                                               and i.PickLocationId = subl.LocationId
   where i.InstructionId = @instructionId
  
  if @SOH is null
     set @SOH = 0
  
  if (@InstructionTypeCode in ('PM','PS','FM'))  
  begin  
  
    if not exists(select 1  
                    from Location      l (nolock)  
                    join AreaLocation al (nolock) on l.LocationId = al.LocationId  
                    join Area          a (nolock) on al.AreaId    = a.AreaId  
                   where l.LocationId = @PickLocationId  
                     and (a.Area like '%Mezzanine%' or a.Area in ('Freezer','Fridge'))) -- Added Mezzanine 2009-09-16  
    begin
      if (dbo.ufn_Configuration(451, @WarehouseId) = 1) -- Mixed Picking - Only create stock take - GS 2014-06-05
      begin
        if(select ActualQuantity from StorageUnitBatchLocation (nolock) where StorageUnitBatchId = @StorageUnitBatchId and LocationId = @PickLocationId) < @confirmedQuantity
        --if @Quantity <> @confirmedQuantity
        begin
          exec @Error = p_Instruction_Update  
              @InstructionId       = @instructionId,  
              @ConfirmedQuantity   = @confirmedQuantity,
              @SOH                 = @SOH
              
          set @Error = 8
          goto Result  
        end  
        
        --if(@confirmedQuantity < @Quantity)  
        --and (select ActualQuantity from StorageUnitBatchLocation (nolock) where StorageUnitBatchId = @StorageUnitBatchId and LocationId = @PickLocationId) > @confirmedQuantity  
        --begin  
        --  exec @Error = p_Instruction_Update  
        --      @InstructionId       = @instructionId,  
        --      @ConfirmedQuantity   = @confirmedQuantity,
        --      @SOH                 = @SOH
          
        --  set @Error = 9  
        --  goto Result  
        --end
      end
    end  
  end  
  
  if @confirmedQuantity = @Quantity  
  begin  
    exec @Error = p_StorageUnitBatchLocation_Deallocate  
     @InstructionId       = @instructionId  
      
    if @Error <> 0  
    begin  
      set @Error = 1  
      goto Result  
    end  
  
    exec @Error = p_Instruction_Update  
     @InstructionId       = @instructionId,  
     @ConfirmedQuantity   = @confirmedQuantity,
     @SOH                 = @SOH
      
    if @Error <> 0  
    begin  
      set @Error = 1  
      goto Result  
    end  
      
    exec @Error = p_StorageUnitBatchLocation_Reserve  
     @InstructionId       = @instructionId  
      
    if @Error <> 0  
    begin  
      set @Error = 1  
      goto Result  
    end  
      
    set @Error = 0  
    goto Result  
  end  
  else  
  begin  
    exec @Error = p_StorageUnitBatchLocation_Deallocate  
     @InstructionId       = @instructionId  
      
    if @Error <> 0  
    begin  
      set @Error = 1  
      goto Result  
    end  
    
    exec @Error = p_Instruction_Update  
     @InstructionId       = @instructionId,  
     @ConfirmedQuantity   = @confirmedQuantity,
     @SOH                 = @SOH
    
    if @Error <> 0  
    begin  
      set @Error = 1  
      goto Result  
    end  
      
    exec @Error = p_StorageUnitBatchLocation_Reserve  
     @InstructionId       = @instructionId  
      
    if @Error <> 0  
    begin  
      set @Error = 1  
      goto Result  
    end  
      
    if @InstructionTypeCode in ('S','SM')  
    begin  
      exec @Error = p_Instruction_Update  
       @InstructionId       = @instructionId,  
       @Quantity            = @confirmedQuantity,  
       @ConfirmedQuantity   = @confirmedQuantity  
        
      if @Error <> 0  
      begin  
        set @Error = 1  
        goto Result  
      end  
        
      set @StatusId = dbo.ufn_StatusId('I','W')  
        
      set @Quantity = @Quantity - @confirmedQuantity  
        
      exec @Error = p_Instruction_Insert  
       @InstructionId       = @instructionId output,  
       @InstructionTypeId   = @InstructionTypeId,  
       @StorageUnitBatchId  = @StorageUnitBatchId,  
       @WarehouseId         = @WarehouseId,  
       @StatusId            = @StatusId,  
       @JobId               = @JobId,  
       @OperatorId          = @OperatorId,  
       @PickLocationId      = @PickLocationId,  
       @StoreLocationId     = @StoreLocationId,  
       @ReceiptLineId       = @ReceiptLineId,  
       @IssueLineId         = @IssueLineId,  
       @InstructionRefId    = @instructionId,  
       @Quantity            = @Quantity,  
       @ConfirmedQuantity   = 0,  
       @Weight              = @Weight,  
       @ConfirmedWeight     = 0,  
       @PalletId            = @PalletId,  
       @CreateDate          = @GetDate,  
       @Picked              = @Picked  
        
      if @Error <> 0  
      begin  
        set @Error = 1  
        goto Result  
      end  
        
      if (select dbo.ufn_Configuration(21, @WarehouseId)) = 1  
      begin  
        exec p_Location_Get  
         @WarehouseId        = @WarehouseId,  
         @LocationId         = @StoreLocationId output,  
         @StorageUnitBatchId = @StorageUnitBatchId,  
         @Quantity           = @Quantity  
          
        exec @Error = p_Instruction_Update  
         @InstructionId      = @instructionId,  
         @StoreLocationId    = @StoreLocationId  
          
        if @Error <> 0  
        begin  
          set @Error = 1  
          goto Result  
        end  
          
        exec @Error = p_StorageUnitBatchLocation_Reserve  
         @InstructionId       = @instructionId,  
         @Pick                = 0 -- false  
          
        if @Error <> 0  
        begin  
          set @Error = 1  
          goto Result  
        end  
      end  
    end  
--    else  
--    begin  
--      if (select dbo.ufn_Configuration(21, @WarehouseId)) = 1  
--      begin  
--        exec p_Pick_Location_Get  
--         @WarehouseId        = @WarehouseId,  
--         @LocationId         = @PickLocationId output,  
--         @StorageUnitBatchId = @StorageUnitBatchId,  
--         @Quantity           = @Quantity  
--          
--        exec @Error = p_Instruction_Update  
--         @InstructionId      = @instructionId,  
--         @PickLocationId     = @PickLocationId  
--          
--        if @Error <> 0  
--        begin  
--          set @Error = 1  
--          goto Result  
--        end  
--          
--        exec @Error = p_StorageUnitBatchLocation_Reserve  
--         @InstructionId       = @instructionId  
--          
--        if @Error <> 0  
--        begin  
--          set @Error = 1  
--          goto Result  
--        end  
--      end  
--    end  
  end  
    
  result:  
    update MobileLog  
       set EndDate = getdate()  
     where MobileLogId = @MobileLogId  
      
    select @Error  
    return @Error  
end
 
