﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Method_Update
  ///   Filename       : p_Method_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:04
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Method table.
  /// </remarks>
  /// <param>
  ///   @MethodId int = null,
  ///   @MethodCode nvarchar(20) = null,
  ///   @Method nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Method_Update
(
 @MethodId int = null,
 @MethodCode nvarchar(20) = null,
 @Method nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
  if @MethodId = '-1'
    set @MethodId = null;
  
  if @MethodCode = '-1'
    set @MethodCode = null;
  
  if @Method = '-1'
    set @Method = null;
  
	 declare @Error int
 
  update Method
     set MethodCode = isnull(@MethodCode, MethodCode),
         Method = isnull(@Method, Method) 
   where MethodId = @MethodId
  
  select @Error = @@Error
  
  
  return @Error
  
end
