﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Product_Insert
  ///   Filename       : p_Housekeeping_Stock_Take_Product_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Product_Insert
(
 @operatorId         int,
 @instructionId      int output,
 @storageUnitBatchId int,
 @locationId         int = null,
 @quantity           float
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId           int,
          @NewInstructionId   int,
          @InstructionTypeId	 int,
          @WarehouseId	       int,
          @JobId	             int,
          @OldStorageUnitBatchId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @JobId                 = JobId,
         @WarehouseId           = WarehouseId,
         @InstructionTypeId     = InstructionTypeId,
         @OldStorageUnitBatchId = StorageUnitBatchId,
         @locationId            = PickLocationId
    from Instruction
   where InstructionId = @instructionId
  
  begin transaction
  
  select @StatusId = dbo.ufn_StatusId('I','S')
  
  exec @Error = p_Instruction_Insert
   @InstructionId	     = @NewInstructionId output,
   @InstructionTypeId	 = @InstructionTypeId,
   @StorageUnitBatchId	= @storageUnitBatchId,
   @WarehouseId	       = @WarehouseId,
   @StatusId           = @StatusId,
   @JobId	             = @JobId,
   @OperatorId	        = null,
   @PickLocationId	    = @locationId,
   @InstructionRefId	  = @instructionId,
   @Quantity	          = 0,
   @ConfirmedQuantity  = @quantity,
   @CreateDate	        = @GetDate
  
  if @Error <> 0
    goto error
  
  if @OldStorageUnitBatchId is null
  begin
    exec @Error = p_Instruction_Update
     @InstructionId	     = @instructionId,
     @Stored             = 1,
     @Picked             = 1
        
    if @Error <> 0
      goto error
  end
  
  set @instructionId = @NewInstructionId
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Housekeeping_Stock_Take_Product_Insert'
    rollback transaction
    return @Error
end
