﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSODetail_Insert
  ///   Filename       : p_InterfaceImportSODetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:07
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportSODetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(100) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportSODetail.InterfaceImportSOHeaderId,
  ///   InterfaceImportSODetail.ForeignKey,
  ///   InterfaceImportSODetail.LineNumber,
  ///   InterfaceImportSODetail.ProductCode,
  ///   InterfaceImportSODetail.Product,
  ///   InterfaceImportSODetail.SKUCode,
  ///   InterfaceImportSODetail.Batch,
  ///   InterfaceImportSODetail.Quantity,
  ///   InterfaceImportSODetail.Weight,
  ///   InterfaceImportSODetail.Additional1,
  ///   InterfaceImportSODetail.Additional2,
  ///   InterfaceImportSODetail.Additional3,
  ///   InterfaceImportSODetail.Additional4,
  ///   InterfaceImportSODetail.Additional5 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSODetail_Insert
(
 @InterfaceImportSOHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(100) = null,
 @SKUCode nvarchar(20) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportSODetail
        (InterfaceImportSOHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5)
  select @InterfaceImportSOHeaderId,
         @ForeignKey,
         @LineNumber,
         @ProductCode,
         @Product,
         @SKUCode,
         @Batch,
         @Quantity,
         @Weight,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5 
  
  select @Error = @@Error
  
  
  return @Error
  
end
