﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_Create_Reference_Split
  ///   Filename       : p_Outbound_Palletise_Create_Reference_Split.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Palletise_Create_Reference_Split
(
 @OutboundShipmentId int,
 @IssueId            int
)
 
as
begin
	 set nocount on;
	 
	 declare @TableInstructions as table
	 (
   IssueLineId       int,
   JobId             int,
   InstructionId     int,
   StorageUnitId     int,
   DropSequence      int,
   Quantity          numeric(13,6),
   ConfirmedQuantity numeric(13,6),
   Weight            numeric(13,6),
   ConfirmedWeight   numeric(13,6),
   InstructionRefId  int
	 )
  
  declare @TableIssueLines as table
  (
   OutboundDocumentTypeId int,
   OutboundShipmentId int,
   OutboundDocumentId int,
   IssueId            int,
   IssueLineId        int,
   StorageUnitId      int,
   DropSequence       int,
   Quantity           numeric(13,6),
   AllocatedQuantity  numeric(13,6) default 0
  )
  
  declare @TableSplit as table
  (
   StorageUnitId      int,
   OrderedQuantity    numeric(13,6),
   PickedQuantity     numeric(13,6),
   Percentage         numeric(13,6)
  )
  
  declare @Error                  int,
          @Errormsg               nvarchar(500),
          @GetDate                datetime,
          @IssueLineId            int,
          @InstructionId          int,
          @StorageUnitId          int,
          @DropSequence           int,
          @insQuantity            numeric(13,6),
          @ilQuantity             numeric(13,6),
          @MinusQuantity          numeric(13,6),
          @OutboundDocumentId     int,
          @OutboundDocumentTypeId int,
          @OrderLineSequence      bit,
          @OneProductPerPallet    bit,
          @InstructionRefId       int,
          @ConfirmedQuantity      numeric(13,6)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @OutboundShipmentId is not null
  begin
    select @OrderLineSequence   = ec.OrderLineSequence,
           @OneProductPerPallet = ec.OneProductPerPallet
      from OutboundShipmentIssue osi (nolock)
      join Issue             i (nolock) on osi.IssueId = i.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
     where osi.OutboundShipmentId = @OutboundShipmentId
    
    insert @TableInstructions
          (IssueLineId,
           JobId,
           InstructionId,
           StorageUnitId,
           DropSequence,
           Quantity,
           ConfirmedQuantity,
           Weight,
           ConfirmedWeight,
           InstructionRefId)
    select i.IssueLineId,
           i.JobId,
           i.InstructionId,
           su.StorageUnitId,
           i.DropSequence,
           i.Quantity,
           i.ConfirmedQuantity,
           i.Weight,
           i.ConfirmedWeight,
           i.InstructionRefId
      from Instruction        i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
     where OutboundShipmentId = @OutboundShipmentId
    
    insert @TableIssueLines
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           IssueLineId,
           StorageUnitId,
           DropSequence,
           Quantity)
    select osi.OutboundShipmentId,
           i.OutboundDocumentId,
           i.IssueId,
           il.IssueLineId,
           su.StorageUnitId,
           --case when @OrderLineSequence = 1
           --     then isnull(i.DropSequence,1)
           --     else isnull(osi.DropSequence,1)
           --     end,
           case when @OneProductPerPallet = 0
                then case when @OrderLineSequence = 1
                          then isnull(i.DropSequence,1)
                          else isnull(osi.DropSequence,1)
                          end
                else il.OutboundLineId
                end,
           Quantity
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId           = i.IssueId
      join IssueLine              il (nolock) on i.IssueId             = il.IssueId
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  else if @IssueId is not null
  begin
    insert @TableInstructions
          (IssueLineId,
           JobId,
           InstructionId,
           StorageUnitId,
           DropSequence,
           Quantity,
           ConfirmedQuantity,
           Weight,
           ConfirmedWeight,
           InstructionRefId)
    select il.IssueLineId,
           i.JobId,
           i.InstructionId,
           su.StorageUnitId,
           i.DropSequence,
           i.Quantity,
           i.ConfirmedQuantity,
           i.Weight,
           i.ConfirmedWeight,
           i.InstructionRefId
      from Instruction        i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join IssueLine         il (nolock) on i.IssueLineId        = il.IssueLineId
     where il.IssueId = @IssueId
    
    insert @TableIssueLines
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           IssueLineId,
           StorageUnitId,
           DropSequence,
           Quantity)
    select null,
           i.OutboundDocumentId,
           i.IssueId,
           il.IssueLineId,
           su.StorageUnitId,
           isnull(i.DropSequence,1),
           il.Quantity
      from Issue                   i (nolock)
      join IssueLine              il (nolock) on i.IssueId             = il.IssueId
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
     where i.IssueId = @IssueId
  end
  
  insert @TableSplit
        (StorageUnitId,
         OrderedQuantity)
  select StorageUnitId,
         sum(Quantity)
    from @TableIssueLines
  group by StorageUnitId
  
  update ts
     set PickedQuantity = (select SUM(ConfirmedQuantity)
                             from @TableInstructions ti
                            where ts.StorageUnitId = ti.StorageUnitId)
    from @TableSplit ts
  
  update @TableSplit
     set Percentage = (PickedQuantity / OrderedQuantity) * 100
  
  update il
     set Quantity = Quantity * (ts.Percentage / 100.000000)
    from @TableIssueLines il
    join @TableSplit      ts on il.StorageUnitId = ts.StorageUnitId
  
  update til
     set OutboundDocumentTypeId = od.OutboundDocumentTypeId
    from @TableIssueLines til
    join OutboundDocument  od (nolock) on til.OutboundDocumentId = od.OutboundDocumentId
  
  delete ili
    from @TableInstructions   i
    join IssueLineInstruction ili on i.InstructionId = ili.InstructionId
  
  while exists(select top 1 1
                 from @TableInstructions
                where Quantity > 0)
  begin
    select top 1
           @InstructionId      = InstructionId,
           @StorageUnitId      = StorageUnitId,
           @DropSequence       = DropSequence,
           @insQuantity        = Quantity
      from @TableInstructions
     where Quantity > 0
    order by Dropsequence, JobId
    
    select top 1
           @OutboundDocumentTypeId = OutboundDocumentTypeId,
           @OutboundShipmentId = OutboundShipmentId,
           @OutboundDocumentId = OutboundDocumentId,
           @IssueId            = IssueId,
           @IssueLineId        = IssueLineId,
           @ilQuantity         = Quantity - AllocatedQuantity
      from @TableIssueLines
     where DropSequence  = @DropSequence
       and StorageUnitId = @StorageUnitId
       and Quantity      > AllocatedQuantity
    order by DropSequence, Quantity, AllocatedQuantity
    
    if @@rowcount = 0
      select top 1
             @OutboundDocumentTypeId = OutboundDocumentTypeId,
             @OutboundShipmentId = OutboundShipmentId,
             @OutboundDocumentId = OutboundDocumentId,
             @IssueId            = IssueId,
             @IssueLineId        = IssueLineId,
             @ilQuantity         = Quantity - AllocatedQuantity
        from @TableIssueLines
       where StorageUnitId = @StorageUnitId
         and Quantity      > AllocatedQuantity
      order by DropSequence, Quantity, AllocatedQuantity
    
    if @IssueLineId is not null
    begin
      set @MinusQuantity = null
      
      if @ilQuantity > @insQuantity
        set @MinusQuantity = @insQuantity
      else
        set @MinusQuantity = @ilQuantity
      
      update @TableInstructions
         set Quantity = Quantity - @MinusQuantity
       where InstructionId = @InstructionId
      
      update @TableIssueLines
         set AllocatedQuantity = AllocatedQuantity + @MinusQuantity
       where IssueLineId = @IssueLineId
      
      if exists(select top 1 1
                  from IssueLineInstruction
                 where IssueLineId       = @IssueLineId
                   and InstructionId     = @InstructionId)
      begin
        exec @Error = p_IssueLineInstruction_Update
         @OutboundDocumentTypeId = @OutboundDocumentTypeId,
         @OutboundShipmentId = @OutboundShipmentId,
         @OutboundDocumentId = @OutboundDocumentId,
         @IssueId            = @IssueId,
         @IssueLineId        = @IssueLineId,
         @InstructionId      = @InstructionId,
         @DropSequence       = @DropSequence,
         @Quantity           = @MinusQuantity 
        
        if @Error <> 0
          goto error
      end
      else
      begin
        exec @Error = p_IssueLineInstruction_Insert
         @OutboundDocumentTypeId = @OutboundDocumentTypeId,
         @OutboundShipmentId = @OutboundShipmentId,
         @OutboundDocumentId = @OutboundDocumentId,
         @IssueId            = @IssueId,
         @IssueLineId        = @IssueLineId,
         @InstructionId      = @InstructionId,
         @DropSequence       = @DropSequence,
         @Quantity           = @MinusQuantity
        
        if @Error <> 0
          goto error
      end
    end
  end
  
  while exists(select top 1 1 from @TableInstructions)
  begin
    set @InstructionId = null
    set @InstructionRefId = null
    set @ConfirmedQuantity = null
    
    select @InstructionId     = InstructionId,
           @InstructionRefId  = InstructionRefId,
           @ConfirmedQuantity = ConfirmedQuantity
      from @TableInstructions
    
    delete @TableInstructions
     where InstructionId = @InstructionId
    
    exec @Error = p_Instruction_Update_ili
     @InstructionId    = @InstructionId,
     @InstructionRefId = @InstructionRefId,
     @insConfirmed     = @ConfirmedQuantity
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Palletise_Create_Reference_Split'
    rollback transaction
    return @Error
end
