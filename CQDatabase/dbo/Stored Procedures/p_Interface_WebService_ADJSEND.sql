﻿--IF OBJECT_ID('dbo.p_Interface_WebService_ADJSEND') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Interface_WebService_ADJSEND
--    IF OBJECT_ID('dbo.p_Interface_WebService_ADJSEND') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Interface_WebService_ADJSEND >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Interface_WebService_ADJSEND >>>'
--END
--go

/*                
  /// <summary>                
  ///   Procedure Name : p_Interface_WebService_ADJSEND                
  ///   Filename       : p_Interface_WebService_ADJSEND.sql                
  ///   Create By      : Johan J v Rensburg                
  ///   Date Created   : 12 Jun 2013                
  /// </summary>                
*/
CREATE PROCEDURE [dbo].[p_Interface_WebService_ADJSEND] (
	@XMLBody NVARCHAR(MAX) OUTPUT
	,@PrincipalCode NVARCHAR(30) = NULL
	--,@warehouseCode NVARCHAR(30) = NULL
	)
               
AS
BEGIN
	DECLARE @GETDATE DATETIME,
			@DCCode  NVARCHAR(30),
			@iDoc	 INT

	SELECT @GETDATE = dbo.ufn_GetDate()

	EXEC sp_xml_preparedocument @idoc OUTPUT, @XMLBody
	
	SELECT @DCCode = DCCode
	  FROM  OPENXML (@idoc, '/root/Body/Item',1)
      WITH (DCCode varchar(50) 'DCCode')

	  SELECT @PrincipalCode = PrincipalCode
	  FROM  OPENXML (@idoc, '/root/Body/Item',1)
      WITH (PrincipalCode varchar(50) 'PrincipalCode')

	if  @DCCode = ''
		set @DCCode = null

	if  @PrincipalCode = ''
		set @PrincipalCode = null
		
	UPDATE sa
	SET ProcessedDate = @GETDATE
	from InterfaceExportStockAdjustment sa
	join Warehouse w (nolock) on sa.Additional1 = w.WarehouseCode
	join Warehouse w2 (nolock) on w.WarehouseId = w2.ParentWarehouseId
	WHERE RecordStatus = 'N'
		AND ProcessedDate IS NULL
		AND (
			PrincipalCode = @PrincipalCode
			OR @PrincipalCode IS NULL
			)
		AND w2.WarehouseCode = @DCCode


	SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' + (
			SELECT (
					SELECT 'Cquential' AS 'Source'
						,'' AS 'Target'
						,GETDATE() AS 'CreateDate'
						,'StockAdjustment' AS 'FileType'
					FOR XML PATH('Header')
						,TYPE
					)
				,(
					SELECT ''
						,(
							SELECT H1.RecordType AS 'ItemType'
								,'N' AS 'ItemStatus'
								,(
									SELECT 'InterfaceExportStockAdjustment' AS 'InterfaceTable'
										,H.InterfaceExportStockAdjustmentId AS 'InterfaceTableId'
										,CASE
											WHEN H1.RecordType <> 'XFR'
												THEN 'LOC1'
											ELSE H.Additional1 
											END AS 'FromWarehouseCode' --H.Additional1             
										,CASE 
											WHEN H1.RecordType <> 'XFR'
												THEN NULL
											ELSE H.Additional2
											END AS 'ToWarehouseCode'
										,isnull(h.Additional4,'LOC1') AS 'FromLocation'
										,CASE
											WHEN H1.RecordType <> 'XFR'
												THEN NULL
											ELSE H.Additional5 
											END as 'ToLocation'
										,H.ProductCode AS 'ProductCode'
										,H.Batch AS 'Batch'
										,'' AS 'SerialNumber'
										,H.SKUCode AS 'SKUCode'
										,convert(NVARCHAR(50), H.Quantity) AS 'Quantity'
										,h.Additional4 AS 'Comments'
										,H.Additional3 AS 'ReasonCode'
										,ISNULL(H.InsertDate, dbo.ufn_Getdate()) AS 'TransactionDate'
									FROM InterfaceExportStockAdjustment H
									WHERE H.RecordType = H1.RecordType
										AND H.ProcessedDate = @GETDATE
										AND H.RecordStatus = 'N'
										AND (
											H.PrincipalCode = @PrincipalCode
											OR @PrincipalCode IS NULL
											)
										--AND H.Additional1 = @warehouseCode
									ORDER BY H.Additional1
										,H.ProductCode
									FOR XML PATH('ItemLine')
										,TYPE
									)
							FROM InterfaceExportStockAdjustment H1
							WHERE H1.RecordType IN (
									'SOH'
									,'ADJ'
									,'XFR'
									,'WHO'
									)
								AND H1.ProcessedDate = @GETDATE
								AND H1.RecordStatus = 'N'
								AND (
									H1.PrincipalCode = @PrincipalCode
									OR @PrincipalCode IS NULL
									)
								--AND H1.Additional1 = @warehouseCode
							GROUP BY H1.RecordType
							FOR XML PATH('Item')
								,TYPE
							)
					FOR XML PATH('Body')
						,TYPE
					)
			FOR XML PATH('root')
			)

	UPDATE InterfaceExportStockAdjustment
	SET RecordStatus = 'Y'
	WHERE RecordStatus = 'N'
		AND RecordType IN (
			'SOH'
			,'ADJ'
			,'XFR'
			,'WHO'
			)
		AND ProcessedDate = @GETDATE
		AND (
			PrincipalCode = @PrincipalCode
			OR @PrincipalCode IS NULL
			)
		--AND Additional1 = @warehouseCode
END
--go
--IF OBJECT_ID('dbo.p_Interface_WebService_ADJSEND') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Interface_WebService_ADJSEND >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Interface_WebService_ADJSEND >>>'
--go


