﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKU_Insert
  ///   Filename       : p_SKU_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:05
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the SKU table.
  /// </remarks>
  /// <param>
  ///   @SKUId int = null output,
  ///   @UOMId int = null,
  ///   @SKU nvarchar(100) = null,
  ///   @SKUCode nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @AlternatePallet int = null,
  ///   @SubstitutePallet int = null,
  ///   @Litres numeric(13,3) = null,
  ///   @ParentSKUCode nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   SKU.SKUId,
  ///   SKU.UOMId,
  ///   SKU.SKU,
  ///   SKU.SKUCode,
  ///   SKU.Quantity,
  ///   SKU.AlternatePallet,
  ///   SKU.SubstitutePallet,
  ///   SKU.Litres,
  ///   SKU.ParentSKUCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKU_Insert
(
 @SKUId int = null output,
 @UOMId int = null,
 @SKU nvarchar(100) = null,
 @SKUCode nvarchar(100) = null,
 @Quantity float = null,
 @AlternatePallet int = null,
 @SubstitutePallet int = null,
 @Litres numeric(13,3) = null,
 @ParentSKUCode nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @SKUId = '-1'
    set @SKUId = null;
  
  if @UOMId = '-1'
    set @UOMId = null;
  
  if @SKU = '-1'
    set @SKU = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
	 declare @Error int
 
  insert SKU
        (UOMId,
         SKU,
         SKUCode,
         Quantity,
         AlternatePallet,
         SubstitutePallet,
         Litres,
         ParentSKUCode)
  select @UOMId,
         @SKU,
         @SKUCode,
         @Quantity,
         @AlternatePallet,
         @SubstitutePallet,
         @Litres,
         @ParentSKUCode 
  
  select @Error = @@Error, @SKUId = scope_identity()
  
  
  return @Error
  
end
