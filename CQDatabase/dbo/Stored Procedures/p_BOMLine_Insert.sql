﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMLine_Insert
  ///   Filename       : p_BOMLine_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:10
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the BOMLine table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null output,
  ///   @BOMHeaderId int = null 
  /// </param>
  /// <returns>
  ///   BOMLine.BOMLineId,
  ///   BOMLine.BOMHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMLine_Insert
(
 @BOMLineId int = null output,
 @BOMHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
  if @BOMLineId = '-1'
    set @BOMLineId = null;
  
	 declare @Error int
 
  insert BOMLine
        (BOMHeaderId)
  select @BOMHeaderId 
  
  select @Error = @@Error, @BOMLineId = scope_identity()
  
  
  return @Error
  
end
