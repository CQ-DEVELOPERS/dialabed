﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DangerousGoods_Select
  ///   Filename       : p_DangerousGoods_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:31
  /// </summary>
  /// <remarks>
  ///   Selects rows from the DangerousGoods table.
  /// </remarks>
  /// <param>
  ///   @DangerousGoodsId int = null 
  /// </param>
  /// <returns>
  ///   DangerousGoods.DangerousGoodsId,
  ///   DangerousGoods.DangerousGoodsCode,
  ///   DangerousGoods.DangerousGoods,
  ///   DangerousGoods.Class,
  ///   DangerousGoods.UNNumber,
  ///   DangerousGoods.PackClass,
  ///   DangerousGoods.DangerFactor 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DangerousGoods_Select
(
 @DangerousGoodsId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         DangerousGoods.DangerousGoodsId
        ,DangerousGoods.DangerousGoodsCode
        ,DangerousGoods.DangerousGoods
        ,DangerousGoods.Class
        ,DangerousGoods.UNNumber
        ,DangerousGoods.PackClass
        ,DangerousGoods.DangerFactor
    from DangerousGoods
   where isnull(DangerousGoods.DangerousGoodsId,'0')  = isnull(@DangerousGoodsId, isnull(DangerousGoods.DangerousGoodsId,'0'))
  
end
