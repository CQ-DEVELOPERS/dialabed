﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoadJob_List
  ///   Filename       : p_IntransitLoadJob_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:35
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IntransitLoadJob table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   IntransitLoadJob.IntransitLoadJobId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoadJob_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as IntransitLoadJobId
        ,null as 'IntransitLoadJob'
  union
  select
         IntransitLoadJob.IntransitLoadJobId
        ,IntransitLoadJob.IntransitLoadJobId as 'IntransitLoadJob'
    from IntransitLoadJob
  
end
