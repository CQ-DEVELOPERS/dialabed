﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Insert
  ///   Filename       : p_InboundShipment_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:19
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundShipment table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null output,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   InboundShipment.InboundShipmentId,
  ///   InboundShipment.StatusId,
  ///   InboundShipment.WarehouseId,
  ///   InboundShipment.ShipmentDate,
  ///   InboundShipment.Remarks 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Insert
(
 @InboundShipmentId int = null output,
 @StatusId int = null,
 @WarehouseId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
  if @InboundShipmentId = '-1'
    set @InboundShipmentId = null;
  
	 declare @Error int
 
  insert InboundShipment
        (StatusId,
         WarehouseId,
         ShipmentDate,
         Remarks)
  select @StatusId,
         @WarehouseId,
         @ShipmentDate,
         @Remarks 
  
  select @Error = @@Error, @InboundShipmentId = scope_identity()
  
  
  return @Error
  
end
