﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Putaway_Palletisation_Select
  ///   Filename       : p_Putaway_Palletisation_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Putaway_Palletisation_Select
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
  
  select i.JobId,
         i.InstructionId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         i.Quantity
    from Instruction        i
    join Status             s (nolock) on i.StatusId           = s.StatusId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
   where i.InstructionId = @InstructionId
     and s.Statuscode   in ('W','S')
end
