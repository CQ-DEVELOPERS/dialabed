﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Detail_Insert
  ///   Filename       : p_COA_Detail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Detail_Insert
(
 @COAId       int,
 @COADetailId int,
 @NoteId      int,
 @Note        nvarchar(max)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @Note is not null or @Note != ''
    exec p_Note_Insert
     @NoteId = @NoteId output,
     @Note   = @Note
  
  exec p_COADetail_Insert
   @COADetailId = @COADetailId,
   @COAId       = @COAId,
   @NoteId      = @NoteId,
   @Note        = @Note
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_COA_Detail_Insert'
    rollback transaction
    return @Error
end
