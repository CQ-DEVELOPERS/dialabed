﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualData_Insert_Racking
  ///   Filename       : p_VisualData_Insert_Racking.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualData_Insert_Racking
 
as
begin
	 set nocount on;
	 
	 declare @Location         nvarchar(15),
          @Latitude         decimal(16,13),
          @Longitude        decimal(16,13),
          @LatitudeAdd      decimal(16,13),
          @LongitudeAdd     decimal(16,13),
          @CurrentLatitude  decimal(16,13),
          @CurrentLongitude decimal(16,13),
          @Direction        nvarchar(10),
          @Height           int,
          @Length           int,
          @HeightCount      int,
          @LengthCount      int,
          @PreviousLocation nvarchar(15),
          @Rowcount         int,
          @Ident            int
  
  declare @TableResult as table
  (
   Ident              int primary key identity,
   LocationId         int,
   Location           nvarchar(15),
   StorageUnitBatchId int,
   Latitude           decimal(16,13),
   Longitude          decimal(16,13),
   Height             int,
   [Length]           int,
   Updated            bit default 0,
   Row                int,
   Ailse              nvarchar(10)      null    ,
   [Column]           nvarchar(10)      null    ,
   [Level]            nvarchar(10)      null    ,
   Direction          nvarchar(10)      null,
   Colour             int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50)
  )
  
  -- Get the rest of the details
  insert @TableResult
        (LocationId,
         StorageUnitBatchId,
         Location,
         Latitude,
         Longitude,
         Direction,
         Height,
         [Length],
         Ailse,
         [Column],
         [Level])
  select distinct
         vl.LocationId,
         -1,
         vl.Location,
         vl.Latitude,
         vl.Longitude,
         vl.Direction,
         vl.Height,
         vl.[Length],
         vl.Ailse,
         vl.[Column],
         vl.[Level]
    from viewLocation vl
   where vl.AreaCode in ('RK','PK')
  
  update @TableResult
     set Colour = 25
   where Ailse in ('c','e','g','i','k','m','o','q','s','u','w','y')
  
  update @TableResult
     set Colour = 55
   where Ailse in ('d','f','h','j','l','n','p','r','t','v','x','z')
  
  update tr
     set ProductCode = vs.ProductCode,
         Product = vs.Product,
         SKUCode = vs.SKUCode,
         Batch = vs.Batch
     from @TableResult tr
     join StorageUnitBatchLocation subl on tr.LocationId = subl.LocationId
     join viewStock vs on subl.StorageUnitBatchId = vs.StorageUnitBatchId
  
  declare visual_cursor cursor for
   select Ident,
          Location,
          Latitude,
          Longitude,
          Direction,
          Height,
          [Length]
  	  from	@TableResult tr
	  order by	Location
  
  open visual_cursor
  
  set @HeightCount = 1
  set @LengthCount = 1
  set @PreviousLocation = '-1'
  
  fetch visual_cursor into @Ident,
                           @Location,
                           @Latitude,
                           @Longitude,
                           @Direction,
                           @Height,
                           @Length
  
  while (@@fetch_status = 0)
  begin
    if @Location != isnull(@PreviousLocation, @Location)
    begin
      set @PreviousLocation = @Location
      set @CurrentLatitude  = @Latitude
      set @CurrentLongitude = @Longitude
    end
    
    if @Direction = 'North'
    begin
      set @LatitudeAdd = 0.00003
      set @LongitudeAdd = 0
    end
    if @Direction = 'East'
    begin
      set @LatitudeAdd = 0
      set @LongitudeAdd = 0.00003
    end
    if @Direction = 'South'
    begin
      set @LatitudeAdd = -0.00003
      set @LongitudeAdd = 0
    end
    if @Direction = 'West'
    begin
      set @LatitudeAdd = 0
      set @LongitudeAdd = -0.00003
    end
    
    update @TableResult
       set Latitude  = @CurrentLatitude,
           Longitude = @CurrentLongitude,
           Updated   = 1,
           Row       = isnull(@Rowcount, 0),
           [Length]  = @LengthCount,
           Height    = @HeightCount,
           Location  = @Location
     where Ident = @Ident
    
    if @HeightCount < @Height
    begin
      set @HeightCount = @HeightCount + 1
    end
    else if @LengthCount < @Length
    begin
      set @HeightCount = 1
      set @LengthCount = @LengthCount + 1
      select @CurrentLatitude  = @CurrentLatitude + @LatitudeAdd
      select @CurrentLongitude = @CurrentLongitude + @LongitudeAdd
    end
    else
    begin
      set @Rowcount = isnull(@Rowcount,0) + 1
      select @CurrentLatitude  = @Latitude + 0.00003 * @Rowcount
      select @CurrentLongitude = @Longitude
      set @HeightCount = 1
      set @LengthCount = 1
    end
    
    fetch visual_cursor into @Ident,
                             @Location,
                             @Latitude,
                             @Longitude,
                             @Direction,
                             @Height,
                             @Length
  end

  close visual_cursor
  deallocate visual_cursor
  
  delete VisualData where Key1 = 'Racking'
  
  insert VisualData
        (Key1,
         Sort1,
         Sort2,
         Sort3,
         Colour,
         Latitude,
         Longitude,
         Info)
  select 'Racking',
         Ailse,
         Location,
         [Level],
         Colour,
         Latitude,
         Longitude,
         isnull(
         '_______________________' + 
         '<h3>' + convert(nvarchar(10), ProductCode) + '</h3>' + 
         '<b>Product Code: </b>' + convert(nvarchar(10), Product) + 
         '<br><b>Sku Code: </b>' + convert(nvarchar(10), SKUCode) + 
         '<br><b>Batch: </b>' + convert(nvarchar(10), Batch),'Empty')
    from @TableResult
  where Row is not null
end
