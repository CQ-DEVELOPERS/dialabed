﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Indicator_Update
  ///   Filename       : p_Report_Indicator_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Indicator_Update
(
 @IndicatorId int = -1
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @IndicatorValue    int,
          @IndicatorString   nvarchar(500),
          @IndicatorNumeric  numeric(16,3)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  print 'Production'
  
  -- Production Dwell Time -----------------------------------------------------------------
  -- Pallets
  if @IndicatorId in (-1,1)
  begin
  select @IndicatorValue = count(i.PalletId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId    = j.JobId
    join Status           s (nolock) on j.StatusId = s.StatusId
   where it.InstructionTypeCode = 'PR'
     and s.StatusCode in ('PR')
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 1,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorValue,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,2)
  begin
  -- Average Dwell Time
  select @IndicatorValue = sum(datediff(ss, CreateDate, @Getdate)) / count(i.PalletId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId    = j.JobId
    join Status           s (nolock) on j.StatusId = s.StatusId
   where it.InstructionTypeCode = 'PR'
     and s.StatusCode in ('PR')
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 2,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Putaway'
  
  -- Putaway Dwell Time --------------------------------------------------------------------
  -- Pallets
  if @IndicatorId in (-1,3)
  begin
  select @IndicatorValue = count(i.PalletId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId    = j.JobId
    join Status           s (nolock) on j.StatusId = s.StatusId
   where it.InstructionTypeCode in ('PR','S','SM')
     and s.StatusCode in ('A')
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 3,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorValue,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,4)
  begin
  -- Average Dwell Time
  select @IndicatorValue = sum(datediff(ss, CreateDate, @Getdate)) / count(i.PalletId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId    = j.JobId
    join Status           s (nolock) on j.StatusId = s.StatusId
   where it.InstructionTypeCode in ('PR','S','SM')
     and s.StatusCode in ('A')
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 4,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Picking Weight'
  
  -- Picking -------------------------------------------------------------------------------
  -- Weight
  if @IndicatorId in (-1,5)
  begin
  select @IndicatorNumeric = sum(ins.Weight)
    from Instruction          ins (nolock)
    join Job                    j (nolock) on ins.JobId = j.JobId
    join Status                 s (nolock) on j.StatusId = s.StatusId
    join IssueLineInstruction ili (nolock) on ins.InstructionId = ili.InstructionId
    join Issue                  i (nolock) on ili.IssueId = i.IssueId
   where s.StatusCode in ('RL','S')
     and datediff(hh, @GetDate, i.DeliveryDate) <= 24
     --and i.DeliveryDate <= '2008-08-26 12:00:00.000'
  
  if @IndicatorNumeric is null
    set @IndicatorNumeric = 0
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 5,
   @IndicatorValue   = @IndicatorNumeric,
   @IndicatorDisplay = @IndicatorNumeric,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,6)
  begin
  -- Average Dwell Time
  select @IndicatorNumeric = @IndicatorNumeric / convert(numeric(13,3), sum(ins.Weight)) -- sum(datediff(ss, Release, Checking)) / count(j.JobId)
    from Instruction          ins (nolock)
    join Job                    j (nolock) on ins.JobId = j.JobId
    join OutboundPerformance   op (nolock) on j.JobId = op.JobId
   where datediff(mi, Checking, @GetDate) <= 60
     and op.Checked > dateadd(hh, -24, @Getdate)
  
  select @IndicatorNumeric = @IndicatorNumeric * 60 -- mins
  select @IndicatorNumeric = @IndicatorNumeric * 60 -- secs
  select @IndicatorValue = @IndicatorNumeric
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 6,
   @IndicatorValue   = @IndicatorNumeric,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Picking Weight > 24 hrs'
  
  -- Picking > 24 hrs -----------------------------------------------------------------------
  -- Weight
  if @IndicatorId in (-1,15)
  begin
  select @IndicatorNumeric = sum(ins.Weight)
    from Instruction          ins (nolock)
    join Job                    j (nolock) on ins.JobId = j.JobId
    join Status                 s (nolock) on j.StatusId = s.StatusId
    join IssueLineInstruction ili (nolock) on ins.InstructionId = ili.InstructionId
    join Issue                  i (nolock) on ili.IssueId = i.IssueId
   where s.StatusCode in ('RL','S')
     and datediff(hh, @GetDate, i.DeliveryDate) > 24
  
  if @IndicatorNumeric is null
    set @IndicatorNumeric = 0
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 15,
   @IndicatorValue   = @IndicatorNumeric,
   @IndicatorDisplay = @IndicatorNumeric,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,16)
  begin
  -- Average Dwell Time
  select @IndicatorNumeric = @IndicatorNumeric / convert(numeric(13,3), sum(ins.Weight)) -- sum(datediff(ss, Release, Checking)) / count(j.JobId)
    from Instruction          ins (nolock)
    join Job                    j (nolock) on ins.JobId = j.JobId
    join OutboundPerformance   op (nolock) on j.JobId = op.JobId
   where datediff(mi, Checking, @GetDate) <= 60
     and op.Checked > dateadd(hh, -24, @Getdate)
  
  select @IndicatorNumeric = @IndicatorNumeric * 60 -- mins
  select @IndicatorNumeric = @IndicatorNumeric * 60 -- secs
  select @IndicatorValue = @IndicatorNumeric
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 16,
   @IndicatorValue   = @IndicatorNumeric,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Picking Units/Cases'
  
  -- Picking -------------------------------------------------------------------------------
  -- Weight
  if @IndicatorId in (-1,17)
  begin
  select @IndicatorNumeric = sum(ins.Quantity)
    from Instruction          ins (nolock)
    join Job                    j (nolock) on ins.JobId = j.JobId
    join Status                 s (nolock) on j.StatusId = s.StatusId
    join IssueLineInstruction ili (nolock) on ins.InstructionId = ili.InstructionId
    join Issue                  i (nolock) on ili.IssueId = i.IssueId
   where s.StatusCode in ('RL','S')
     and datediff(hh, @GetDate, i.DeliveryDate) <= 24
     --and i.DeliveryDate <= '2008-08-26 12:00:00.000'
  
  if @IndicatorNumeric is null
    set @IndicatorNumeric = 0
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 17,
   @IndicatorValue   = @IndicatorNumeric,
   @IndicatorDisplay = @IndicatorNumeric,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,18)
  begin
  -- Average Dwell Time
  select @IndicatorNumeric = @IndicatorNumeric / convert(numeric(13,3), sum(ins.Quantity)) -- sum(datediff(ss, Release, Checking)) / count(j.JobId)
    from Instruction          ins (nolock)
    join Job                    j (nolock) on ins.JobId = j.JobId
    join OutboundPerformance   op (nolock) on j.JobId = op.JobId
   where datediff(mi, Checking, @GetDate) <= 60
     and op.Checked > dateadd(hh, -24, @Getdate)
  
  select @IndicatorNumeric = @IndicatorNumeric * 60 -- mins
  select @IndicatorNumeric = @IndicatorNumeric * 60 -- secs
  select @IndicatorValue = @IndicatorNumeric
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 18,
   @IndicatorValue   = @IndicatorNumeric,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Picking Units/Cases > 24 hrs'
  
  -- Picking > 24 hrs -----------------------------------------------------------------------
  -- Weight
  if @IndicatorId in (-1,19)
  begin
  select @IndicatorNumeric = sum(ins.Quantity)
    from Instruction          ins (nolock)
    join Job                    j (nolock) on ins.JobId = j.JobId
    join Status                 s (nolock) on j.StatusId = s.StatusId
    join IssueLineInstruction ili (nolock) on ins.InstructionId = ili.InstructionId
    join Issue                  i (nolock) on ili.IssueId = i.IssueId
   where s.StatusCode in ('RL','S')
     and datediff(hh, @GetDate, i.DeliveryDate) > 24
  
  if @IndicatorNumeric is null
    set @IndicatorNumeric = 0
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 19,
   @IndicatorValue   = @IndicatorNumeric,
   @IndicatorDisplay = @IndicatorNumeric,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,20)
  begin
  -- Average Dwell Time
  select @IndicatorNumeric = @IndicatorNumeric / convert(numeric(13,3), sum(ins.Quantity)) -- sum(datediff(ss, Release, Checking)) / count(j.JobId)
    from Instruction          ins (nolock)
    join Job                    j (nolock) on ins.JobId = j.JobId
    join OutboundPerformance   op (nolock) on j.JobId = op.JobId
   where datediff(mi, Checking, @GetDate) <= 60
     and op.Checked > dateadd(hh, -24, @Getdate)
  
  select @IndicatorNumeric = @IndicatorNumeric * 60 -- mins
  select @IndicatorNumeric = @IndicatorNumeric * 60 -- secs
  select @IndicatorValue = @IndicatorNumeric
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 20,
   @IndicatorValue   = @IndicatorNumeric,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Dispatch'
  
  -- Dispatch ------------------------------------------------------------------------------
  -- Pallets
  if @IndicatorId in (-1,11)
  begin
  select @IndicatorValue = count(JobId)
    from Job    j (nolock)
    join Status s (nolock) on j.StatusId = s.StatusId
   where s.StatusCode in ('CD','D','DC')
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 11,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorValue,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,12)
  begin
  -- Average Dwell Time
  select @IndicatorValue = sum(convert(numeric(18), datediff(ss, Checked, @Getdate))) / count(j.JobId)
    from Job                  j (nolock)
    join Status               s (nolock) on j.StatusId = s.StatusId
    join OutboundPerformance op (nolock) on j.JobId = op.JobId
   where s.StatusCode in ('CD','D','DC')
     and op.Checked > dateadd(hh, -24, @Getdate)
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 12,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Stretch Wrap'
  
  -- Stretch Wrap --------------------------------------------------------------------------
  -- Pallets
  if @IndicatorId in (-1,7)
  begin
  select @IndicatorValue = count(JobId)
    from Job    j (nolock)
    join Status s (nolock) on j.StatusId = s.StatusId
   where s.StatusCode = 'CK'
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 7,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorValue,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,8)
  begin
  -- Average Dwell Time
  select @IndicatorValue = sum(isnull(datediff(ss, Checking, Checked),0)) / count(j.JobId)
    from Job                  j (nolock)
    join Status               s (nolock) on j.StatusId = s.StatusId
    join OutboundPerformance op (nolock) on j.JobId = op.JobId
   where s.StatusCode = 'D'
     and op.Checked > dateadd(hh, -24, @Getdate)
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 8,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Replenishment'
  
  -- Replenishment Dwell Time --------------------------------------------------------------
  -- Pallets
  if @IndicatorId in (-1,9)
  begin
  select @IndicatorValue = count(j.JobId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId = j.JobId
    join Status           s (nolock) on i.StatusId = s.StatusId
   where s.StatusCode not in ('F','NS','Z')
     and it.InstructionTypeCode = 'R'
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 9,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorValue,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  if @IndicatorId in (-1,10)
  begin
  -- Average Dwell Time
  select @IndicatorValue = sum(datediff(ss, CreateDate, @Getdate)) / count(j.JobId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId = j.JobId
    join Status           s (nolock) on i.StatusId = s.StatusId
   where s.StatusCode not in ('F','NS','Z')
     and it.InstructionTypeCode = 'R'
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 10,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Interface'
  
  -- Interface Activity HH:mm:ss --------------------------------------------------------------
  -- Download
  if @IndicatorId in (-1,13)
  begin
  select @IndicatorValue = datediff(ss, max(CreateDate), @GetDate)
    from InboundDocument (nolock)
  
  select @IndicatorValue = 
         case when datediff(ss, max(CreateDate), @GetDate) < @IndicatorValue
              then datediff(ss, max(CreateDate), @GetDate)
              else @IndicatorValue
              end
    from OutboundDocument (nolock)
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 13,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  -- Upload
  if @IndicatorId in (-1,14)
  begin
  select @IndicatorValue = datediff(ss, max(Checked), @GetDate)
    from OutboundPerformance
  
  select @IndicatorString = convert(nvarchar(10), @IndicatorValue / 3600) + 
                            ':' + convert(nvarchar(2), (@IndicatorValue % 3600) / 60) + 
                            ':' + convert(nvarchar(10), @IndicatorValue % 60)
  
  exec @Error = p_Indicator_Update
   @IndicatorId      = 14,
   @IndicatorValue   = @IndicatorValue,
   @IndicatorDisplay = @IndicatorString,
   @ModifiedDate     = @GetDate
  
  if @Error <> 0
    goto error
  end
  
  print 'Batches'
  
  -- Batches Inactive (with stock on hand)
  -- 0 to 7 days
  if @IndicatorId in (-1,17)
  begin
    select @IndicatorValue = count(distinct(Batch))
      from StorageUnitBatch          sub (nolock)
      join Batch                       b (nolock) on sub.BatchId            = b.BatchId
      join Status                      s (nolock) on b.StatusId             = s.StatusId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join Location                    l (nolock) on subl.LocationId        = l.LocationId
      join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
      join Area                        a (nolock) on al.AreaId              = a.AreaId
     where a.StockOnHand = 1
       and s.StatusCode != 'A'
       and b.CreateDate >= dateadd(dd, -7, getdate())
       and a.WarehouseId  = 5
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 17,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  -- 8 to 14 days
  if @IndicatorId in (-1,18)
  begin
    select @IndicatorValue = count(distinct(Batch))
      from StorageUnitBatch          sub (nolock)
      join Batch                       b (nolock) on sub.BatchId            = b.BatchId
      join Status                      s (nolock) on b.StatusId             = s.StatusId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join Location                    l (nolock) on subl.LocationId        = l.LocationId
      join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
      join Area                        a (nolock) on al.AreaId              = a.AreaId
     where a.StockOnHand = 1
       and s.StatusCode != 'A'
       and b.CreateDate between dateadd(dd, -8, getdate()) and dateadd(dd, -14, getdate())
       and a.WarehouseId  = 5
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 18,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  -- 15 TO 21 days
  if @IndicatorId in (-1,19)
  begin
    select @IndicatorValue = count(distinct(Batch))
      from StorageUnitBatch          sub (nolock)
      join Batch                       b (nolock) on sub.BatchId            = b.BatchId
      join Status                      s (nolock) on b.StatusId             = s.StatusId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join Location                    l (nolock) on subl.LocationId        = l.LocationId
      join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
      join Area                        a (nolock) on al.AreaId              = a.AreaId
     where a.StockOnHand = 1
       and s.StatusCode != 'A'
       and b.CreateDate between dateadd(dd, -15, getdate()) and dateadd(dd, -21, getdate())
       and a.WarehouseId  = 5
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 19,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  -- > 21 days
  if @IndicatorId in (-1,20)
  begin
    select @IndicatorValue = count(distinct(Batch))
      from StorageUnitBatch          sub (nolock)
      join Batch                       b (nolock) on sub.BatchId            = b.BatchId
      join Status                      s (nolock) on b.StatusId             = s.StatusId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join Location                    l (nolock) on subl.LocationId        = l.LocationId
      join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
      join Area                        a (nolock) on al.AreaId              = a.AreaId
     where a.StockOnHand = 1
       and s.StatusCode != 'A'
       and b.CreateDate  < dateadd(dd, -21, getdate())
       and a.WarehouseId  = 5
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 20,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  -- Batches Active (with stock on hand)
  -- Expired
  if @IndicatorId in (-1,21)
  begin
    select @IndicatorValue = count(distinct(Batch))
      from StorageUnitBatch          sub (nolock)
      join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
      join Product                     p (nolock) on su.ProductId           = p.ProductId
      join Batch                       b (nolock) on sub.BatchId            = b.BatchId
      join Status                      s (nolock) on b.StatusId             = s.StatusId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join Location                    l (nolock) on subl.LocationId        = l.LocationId
      join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
      join Area                        a (nolock) on al.AreaId              = a.AreaId
     where a.StockOnHand = 1
       and s.StatusCode  = 'A'
       and dateadd(dd, isnull(p.ShelfLifeDays,1), b.CreateDate) < getdate()
       and a.WarehouseId  = 5
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 21,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  -- < 30 days
  if @IndicatorId in (-1,22)
  begin
    select @IndicatorValue = count(distinct(Batch))
      from StorageUnitBatch          sub (nolock)
      join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
      join Product                     p (nolock) on su.ProductId           = p.ProductId
      join Batch                       b (nolock) on sub.BatchId            = b.BatchId
      join Status                      s (nolock) on b.StatusId             = s.StatusId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join Location                    l (nolock) on subl.LocationId        = l.LocationId
      join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
      join Area                        a (nolock) on al.AreaId              = a.AreaId
     where a.StockOnHand = 1
       and s.StatusCode  = 'A'
       and dateadd(dd, isnull(p.ShelfLifeDays,1), b.CreateDate) between getdate() and dateadd(dd, -30, getdate())
       and a.WarehouseId  = 5
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 22,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  -- 31 to 120 days
  if @IndicatorId in (-1,23)
  begin
    select @IndicatorValue = count(distinct(Batch))
      from StorageUnitBatch          sub (nolock)
      join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
      join Product                     p (nolock) on su.ProductId           = p.ProductId
      join Batch                       b (nolock) on sub.BatchId            = b.BatchId
      join Status                      s (nolock) on b.StatusId             = s.StatusId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join Location                    l (nolock) on subl.LocationId        = l.LocationId
      join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
      join Area                        a (nolock) on al.AreaId              = a.AreaId
     where a.StockOnHand = 1
       and s.StatusCode  = 'A'
       and dateadd(dd, isnull(p.ShelfLifeDays,1), b.CreateDate) between dateadd(dd, -31, getdate()) and dateadd(dd, -120, getdate())
       and a.WarehouseId  = 5
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 23,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  -- > 120 days
  if @IndicatorId in (-1,24)
  begin
    select @IndicatorValue = count(distinct(Batch))
      from StorageUnitBatch          sub (nolock)
      join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
      join Product                     p (nolock) on su.ProductId           = p.ProductId
      join Batch                       b (nolock) on sub.BatchId            = b.BatchId
      join Status                      s (nolock) on b.StatusId             = s.StatusId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join Location                    l (nolock) on subl.LocationId        = l.LocationId
      join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
      join Area                        a (nolock) on al.AreaId              = a.AreaId
     where a.StockOnHand = 1
       and s.StatusCode  = 'A'
       and dateadd(dd, isnull(p.ShelfLifeDays,1), b.CreateDate) > dateadd(dd, -120, getdate())
       and dateadd(dd, isnull(p.ShelfLifeDays,1), b.CreateDate) >= getdate()
       and a.WarehouseId  = 5
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 24,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  
  -- Interface Activity HH:mm:ss --------------------------------------------------------------
  -- Download
  if @IndicatorId in (-1,29)
  begin
    select @IndicatorValue = count(distinct(OrderNumber))
      from InterfaceImportOrderNumbers
     where RecordStatus = 'N'
       and InboundDocument is null
       and OrderNumber like 'SO%'
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 29,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  
  -- Upload
  if @IndicatorId in (-1,30)
  begin
    select @IndicatorValue = count(distinct(OrderNumber))
      from InterfaceImportOrderNumbers
     where RecordStatus = 'N'
       and InboundDocument is null
       and OrderNumber like 'PO%'
    
    exec @Error = p_Indicator_Update
     @IndicatorId      = 30,
     @IndicatorValue   = @IndicatorValue,
     @IndicatorDisplay = @IndicatorValue,
     @ModifiedDate     = @GetDate
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    set @Error = -1
    raiserror 900000 'Error executing p_Report_Indicator_Update'
    rollback transaction
    return @Error
end
