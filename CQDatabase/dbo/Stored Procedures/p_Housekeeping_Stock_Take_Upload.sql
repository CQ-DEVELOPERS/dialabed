﻿--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Upload') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload
--    IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Upload') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload >>>'
--END
--go
 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Upload
  ///   Filename       : p_Housekeeping_Stock_Take_Upload.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Oct 2015a
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Housekeeping_Stock_Take_Upload]
(
 @WarehouseId      int,
 @ProductCode      nvarchar(30),
 @Product          nvarchar(255),
 @Batch            nvarchar(50),
 @SkuCode		   nvarchar(50) = null,
 @WarehouseCode    nvarchar(10) = null,
 @showZeroVariance bit = 1,
 @showSent         bit = 1,
 @principalId      int = null,
 @Category		   nvarchar(50) = null,
 @CategoryExactMatch bit = 0
)
 
as
begin
	 set nocount on;
	 
	 declare @ComparisonDate datetime,
	         @HostDate       datetime,
	         @Variance       int,
             @OldWarehouseId int,
             @SearchChar nvarchar(10)
  
  set @SearchChar = '%'
  set @OldWarehouseId = @WarehouseId
  
	 
	 if @ProductCode is null
	   set @ProductCode = ''
	 
	 if @Product is null
	   set @Product = ''
	 
	 if @Batch is null
	   set @Batch = ''

     if @SkuCode is null
	   set @SkuCode = ''
	 
	 if @WarehouseCode is null
	   set @WarehouseCode = '%'
	 
	 if @PrincipalId = -1
       set @PrincipalId = null
       
     if @Category is null
       set @Category = ''
   
     if @CategoryExactMatch = 1
       set @SearchChar = ''
	 
	 if (select dbo.ufn_Configuration(266, @OldWarehouseId)) = 1 -- ST Upload - Combine warehouses
	 begin
	   set @WarehouseId = null
	   select @ComparisonDate = max(ComparisonDate) from HousekeepingCompare (nolock)
	   select @HostDate       = max(HostDate)       from HousekeepingCompare (nolock) where ComparisonDate = @ComparisonDate
	 end
	 else
	 begin
	   select @ComparisonDate = max(ComparisonDate) from HousekeepingCompare (nolock) where WarehouseId = @WarehouseId
	   select @HostDate       = max(HostDate)       from HousekeepingCompare (nolock) where WarehouseId = @WarehouseId and ComparisonDate = @ComparisonDate
	   --select @ComparisonDate = convert(varchar(10), @ComparisonDate, 120)
	 end
	 
	 if (select dbo.ufn_Configuration(234, @OldWarehouseId)) = 1 -- ST Upload - InterfaceExportHeader on|off
	 begin
	   update hc
	      set RecordStatus = left(im.Status, 1)
	     from HousekeepingCompare  hc
	     join InterfaceExportHeader h (nolock) on hc.InterfaceId = h.InterfaceExportHeaderId
	     left Join InterfaceMessage          im (nolock) on h.InterfaceExportHeaderId = im.InterfaceId
														                                       and im.InterfaceTable = 'InterfaceExportHeader'
     where isnull(hc.WarehouseId, -1) = isnull(@WarehouseId, -1)
       and hc.ComparisonDate = @ComparisonDate
       --and isnull(hc.RecordStatus,'') <> h.RecordStatus
	 end
	 else
	 begin
	   update hc
	      set RecordStatus = left(im.Status, 1)
	     from HousekeepingCompare            hc
	     join InterfaceExportStockAdjustment sa (nolock) on hc.InterfaceId = sa.InterfaceExportStockAdjustmentId
	     left Join InterfaceMessage          im (nolock) on sa.InterfaceExportStockAdjustmentId = im.InterfaceId
														                                       and im.InterfaceTable = 'InterfaceExportStockAdjustment'
		   where isnull(hc.WarehouseId, -1) = isnull(@WarehouseId, -1)
       and hc.ComparisonDate = @ComparisonDate
	 end
	 
	 if @showZeroVariance = 1
	   set @showZeroVariance = null
	 
	 select @Variance = convert(int, @showZeroVariance)
	 
	 select top 1000 hc.WarehouseCode,
	        hc.ComparisonDate,
	        hc.HostDate,
	        hc.StorageUnitId,
	        hc.BatchId,
			p.ProductCode,
			p.Product,
			sku.SKUCode,
			b.Batch,
			pr.Principal,
			p.Category,
			hc.WMSQuantity,
			hc.HostQuantity,
         --hc.WMSQuantity - hc.HostQuantity as 'Adjustment',
         --hc.WMSQuantity - (hc.HostQuantity + (hc.WMSQuantity - hc.HostQuantity)) as 'Variance',
			hc.WMSQuantity - hc.HostQuantity as 'Variance',
			hc.Sent,
         convert(bit, case when isnull(hc.Sent,0) = 1
              then 0
              else 1
              end) as 'Enabled',
         hc.Sentdate,
         hc.CustomField1,
         cast(isnull(hc.StdCostPrice,0) as decimal(38,2)) as 'StdCostPrice',
         CAST(ISNULL(hc.UnitPrice * hc.WMSQuantity, ISNULL(su.UnitPrice * hc.WMSQuantity, 0)) as decimal(38,2)) as 'WMSPrice',
         CAST(ISNULL(hc.UnitPrice * hc.HostQuantity, ISNULL(su.UnitPrice * hc.WMSQuantity, 0)) as decimal(38,2)) as 'HostPrice',
         CAST(ISNULL(hc.UnitPrice * hc.WMSQuantity, su.UnitPrice * hc.WMSQuantity) - ISNULL(hc.UnitPrice * hc.HostQuantity,su.UnitPrice * hc.WMSQuantity) as decimal(38,2)) as 'PriceVariance',
         hc.UnitPrice,
         Case when Isnull(h.RecordStatus,sa.RecordStatus) in ('I','Y') Then 'Successful'
              when Isnull(h.RecordStatus,sa.RecordStatus) = 'E' Then 'Error'
              when Isnull(h.RecordStatus,sa.RecordStatus) = 'N' Then 'Waiting'
              Else '' end as UploadStatus
    from HousekeepingCompare hc (nolock)
		left Join InterfaceExportHeader h (nolock) on h.InterfaceExportHeaderId = hc.InterfaceId
		Left join InterfaceExportStockAdjustment sa (nolock) on hc.InterfaceId = sa.InterfaceExportStockAdjustmentId
		  join StorageUnit         su (nolock) on hc.StorageUnitId = su.StorageUnitId
		  join Product              p (nolock) on su.ProductId     = p.ProductId
		  join Status               s (nolock) on p.StatusId       = s.StatusId
		  join SKU                sku (nolock) on su.SKUId         = sku.SKUId
		  join Batch                b (nolock) on hc.BatchId       = b.BatchId
		  left
		  join Principal		   pr (nolock) on hc.PrincipalId = pr.PrincipalId
   where isnull(hc.WarehouseId, -1)      = isnull(@WarehouseId, -1)
	    and s.StatusCode        = 'A'
     and p.ProductCode    like @ProductCode + '%'
     and p.Product        like @Product + '%'
     and b.Batch          like @Batch + '%'
     and isnull(p.Category,'') like @SearchChar + @Category + @SearchChar
	    and sku.SkuCode	  like @SkuCode + '%'
     and isnull(hc.WarehouseCode,'') like @WarehouseCode
     and (ComparisonDate       = @ComparisonDate)
     and hc.WMSQuantity - hc.HostQuantity != isnull(@Variance,-9999999)
     and isnull(hc.Sent,0)    <= @showSent
     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
end
--go
--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Upload') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload >>>'
--go


