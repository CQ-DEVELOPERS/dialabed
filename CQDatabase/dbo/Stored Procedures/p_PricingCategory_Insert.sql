﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PricingCategory_Insert
  ///   Filename       : p_PricingCategory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:51:22
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PricingCategory table.
  /// </remarks>
  /// <param>
  ///   @PricingCategoryId int = null output,
  ///   @PricingCategory nvarchar(510) = null,
  ///   @PricingCategoryCode nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   PricingCategory.PricingCategoryId,
  ///   PricingCategory.PricingCategory,
  ///   PricingCategory.PricingCategoryCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PricingCategory_Insert
(
 @PricingCategoryId int = null output,
 @PricingCategory nvarchar(510) = null,
 @PricingCategoryCode nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
  if @PricingCategoryId = '-1'
    set @PricingCategoryId = null;
  
  if @PricingCategory = '-1'
    set @PricingCategory = null;
  
  if @PricingCategoryCode = '-1'
    set @PricingCategoryCode = null;
  
	 declare @Error int
 
  insert PricingCategory
        (PricingCategory,
         PricingCategoryCode)
  select @PricingCategory,
         @PricingCategoryCode 
  
  select @Error = @@Error, @PricingCategoryId = scope_identity()
  
  
  return @Error
  
end
 
