﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Search
  ///   Filename       : p_COA_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:50
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the COA table.
  /// </remarks>
  /// <param>
  ///   @COAId int = null output,
  ///   @COACode nvarchar(510) = null,
  ///   @COA nvarchar(max) = null,
  ///   @StorageUnitBatchId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   COA.COAId,
  ///   COA.COACode,
  ///   COA.COA,
  ///   COA.StorageUnitBatchId 
  ///   StorageUnitBatch.StorageUnitBatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Search
(
 @COAId int = null output,
 @COACode nvarchar(510) = null,
 @COA nvarchar(max) = null,
 @StorageUnitBatchId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @COACode = '-1'
    set @COACode = null;
  
  if @COA = '-1'
    set @COA = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
 
  select
         COA.COAId
        ,COA.COACode
        ,COA.COA
        ,COA.StorageUnitBatchId
        ,StorageUnitBatchStorageUnitBatchId.StorageUnitBatchId as 'StorageUnitBatch'
    from COA
    left
    join StorageUnitBatch StorageUnitBatchStorageUnitBatchId on StorageUnitBatchStorageUnitBatchId.StorageUnitBatchId = COA.StorageUnitBatchId
   where isnull(COA.COAId,'0')  = isnull(@COAId, isnull(COA.COAId,'0'))
     and isnull(COA.COACode,'%')  like '%' + isnull(@COACode, isnull(COA.COACode,'%')) + '%'
     and isnull(COA.COA,'%')  like '%' + isnull(@COA, isnull(COA.COA,'%')) + '%'
     and isnull(COA.StorageUnitBatchId,'0')  = isnull(@StorageUnitBatchId, isnull(COA.StorageUnitBatchId,'0'))
  order by COACode
  
end
