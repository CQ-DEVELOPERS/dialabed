﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_On_Hand_By_Area
  ///   Filename       : p_Report_Stock_On_Hand_By_Area.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_On_Hand_By_Area
(
 @WarehouseId       int,
 @PrincipalId		int
)
 
as
begin
	 set nocount on;
	 
	if @PrincipalId = -1
		set @PrincipalId = null
		
  declare @TableResult as table 
  (
   LocationId                      int        ,
   RelativeValue                   numeric(13,3),
   StorageUnitId				               int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(255),
   SKUCode                         nvarchar(50),
   Batch                           nvarchar(50),
   Area                            nvarchar(50),
   Location                        nvarchar(15),
   ActualQuantity                  float        ,
   AllocatedQuantity               float        ,
   ReservedQuantity                float,
   NettWeight					   numeric(13,6),
   ExpiryDate					   datetime        
  )
  
  insert @TableResult
        (LocationId,
         RelativeValue,
         StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Area,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         ExpiryDate)
  select l.LocationId,
         l.RelativeValue,
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         a.Area,
         l.Location,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         b.ExpiryDate
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
   where a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     and isnull(p.PrincipalId,-1) = isnull(@PrincipalId,isnull(p.PrincipalId,-1))
  
  insert @TableResult
        (LocationId,
         RelativeValue,
         Location,
         Area,
         StorageUnitId,
         ProductCode,
         Product,
         SKUCode)
  select l.LocationId,
         l.RelativeValue,
         l.Location,
         a.Area,
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode
    from StorageUnitLocation sul (nolock)
    join StorageUnit          su (nolock) on sul.StorageUnitId       = su.StorageUnitId
    join Product               p (nolock) on su.ProductId            = p.ProductId
    join SKU                 sku (nolock) on su.SKUId                = sku.SKUId
    join Location              l (nolock) on sul.LocationId = l.LocationId
    join AreaLocation         al (nolock) on l.LocationId = al.LocationId
    join Area                  a (nolock) on al.AreaId    = a.AreaId
    left 
    join @TableResult         tr on tr.LocationId    = sul.LocationId
                                and tr.StorageUnitId = sul.StorageUnitId
   where tr.LocationId is null
     and a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     and isnull(p.PrincipalId,-1) = isnull(@PrincipalId,isnull(p.PrincipalId,-1))
  
  insert @TableResult
        (LocationId,
         l.RelativeValue,
         Location,
         Area)
  select l.LocationId,
         l.RelativeValue,
         l.Location,
         a.Area
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId    = a.AreaId
    left 
    join @TableResult tr on tr.LocationId    = l.LocationId
   where tr.LocationId is null
     and a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     
  
  declare @InboundSequence smallint,
		  @PackTypeId	int,
	      @PackType   	nvarchar(30)
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence
  
  --update tr
  --   set NettWeight = Sub.NettWeight * tr.ActualQuantity
  --  from @TableResult tr
	 --  join (select MAX(NettWeight) NettWeight, su.StorageUnitId
		--          from Pack p
		--          inner join StorageUnit su on p.StorageUnitId = su.StorageUnitId
		--          where p.PackTypeId = @PackTypeId
		--           and isnull(su.ProductCategory,'') != 'V'
		--           and exists (select StorageUnitId
		--	                        from @TableResult tr
		--	                        where tr.StorageUnitId = p.StorageUnitId)
  --                       Group By su.StorageUnitId) sub on sub.StorageUnitId = tr.StorageUnitId
  
  select ProductCode,
         Product,
         SKUCode,
         Batch + ' - ' + CONVERT(varchar(12), isnull(ExpiryDate,0), 103) as Batch,
         Area,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         NettWeight
    from @TableResult
  order by RelativeValue,
           Area,
           Location,
           Product,
           SKUCode
end
 
