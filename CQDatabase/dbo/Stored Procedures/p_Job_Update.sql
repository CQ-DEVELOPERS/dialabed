﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Update
  ///   Filename       : p_Job_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Aug 2014 12:07:50
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Job table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @PriorityId int = null,
  ///   @OperatorId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @ReceiptLineId int = null,
  ///   @IssueLineId int = null,
  ///   @ContainerTypeId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @TareWeight float = null,
  ///   @Weight float = null,
  ///   @NettWeight float = null,
  ///   @CheckedBy int = null,
  ///   @CheckedDate datetime = null,
  ///   @DropSequence int = null,
  ///   @Pallets int = null,
  ///   @BackFlush bit = null,
  ///   @Prints int = null,
  ///   @CheckingClear bit = null,
  ///   @TrackingNumber nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Update
(
 @JobId int = null,
 @PriorityId int = null,
 @OperatorId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @ReceiptLineId int = null,
 @IssueLineId int = null,
 @ContainerTypeId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @TareWeight float = null,
 @Weight float = null,
 @NettWeight float = null,
 @CheckedBy int = null,
 @CheckedDate datetime = null,
 @DropSequence int = null,
 @Pallets int = null,
 @BackFlush bit = null,
 @Prints int = null,
 @CheckingClear bit = null,
 @TrackingNumber nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
  if @JobId = '-1'
    set @JobId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @CheckedBy = '-1'
    set @CheckedBy = null;
  
	 declare @Error int
 
  update Job
     set PriorityId = isnull(@PriorityId, PriorityId),
         OperatorId = isnull(@OperatorId, OperatorId),
         StatusId = isnull(@StatusId, StatusId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId),
         IssueLineId = isnull(@IssueLineId, IssueLineId),
         ContainerTypeId = isnull(@ContainerTypeId, ContainerTypeId),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         TareWeight = isnull(@TareWeight, TareWeight),
         Weight = isnull(@Weight, Weight),
         NettWeight = isnull(@NettWeight, NettWeight),
         CheckedBy = isnull(@CheckedBy, CheckedBy),
         CheckedDate = isnull(@CheckedDate, CheckedDate),
         DropSequence = isnull(@DropSequence, DropSequence),
         Pallets = isnull(@Pallets, Pallets),
         BackFlush = isnull(@BackFlush, BackFlush),
         Prints = isnull(@Prints, Prints),
         CheckingClear = isnull(@CheckingClear, CheckingClear),
         TrackingNumber = isnull(@TrackingNumber, TrackingNumber) 
   where JobId = @JobId
  
  select @Error = @@Error
  
  
  return @Error
  
end
