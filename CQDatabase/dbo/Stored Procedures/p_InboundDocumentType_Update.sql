﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentType_Update
  ///   Filename       : p_InboundDocumentType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 10:28:16
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentTypeId int = null,
  ///   @InboundDocumentTypeCode nvarchar(20) = null,
  ///   @InboundDocumentType nvarchar(60) = null,
  ///   @OverReceiveIndicator bit = null,
  ///   @AllowManualCreate bit = null,
  ///   @QuantityToFollowIndicator bit = null,
  ///   @PriorityId int = null,
  ///   @AutoSendup bit = null,
  ///   @BatchButton bit = null,
  ///   @BatchSelect bit = null,
  ///   @DeliveryNoteNumber bit = null,
  ///   @VehicleRegistration bit = null,
  ///   @AreaType nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentType_Update
(
 @InboundDocumentTypeId int = null,
 @InboundDocumentTypeCode nvarchar(20) = null,
 @InboundDocumentType nvarchar(60) = null,
 @OverReceiveIndicator bit = null,
 @AllowManualCreate bit = null,
 @QuantityToFollowIndicator bit = null,
 @PriorityId int = null,
 @AutoSendup bit = null,
 @BatchButton bit = null,
 @BatchSelect bit = null,
 @DeliveryNoteNumber bit = null,
 @VehicleRegistration bit = null,
 @AreaType nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @InboundDocumentTypeCode = '-1'
    set @InboundDocumentTypeCode = null;
  
  if @InboundDocumentType = '-1'
    set @InboundDocumentType = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
	 declare @Error int
 
  update InboundDocumentType
     set InboundDocumentTypeCode = isnull(@InboundDocumentTypeCode, InboundDocumentTypeCode),
         InboundDocumentType = isnull(@InboundDocumentType, InboundDocumentType),
         OverReceiveIndicator = isnull(@OverReceiveIndicator, OverReceiveIndicator),
         AllowManualCreate = isnull(@AllowManualCreate, AllowManualCreate),
         QuantityToFollowIndicator = isnull(@QuantityToFollowIndicator, QuantityToFollowIndicator),
         PriorityId = isnull(@PriorityId, PriorityId),
         AutoSendup = isnull(@AutoSendup, AutoSendup),
         BatchButton = isnull(@BatchButton, BatchButton),
         BatchSelect = isnull(@BatchSelect, BatchSelect),
         DeliveryNoteNumber = isnull(@DeliveryNoteNumber, DeliveryNoteNumber),
         VehicleRegistration = isnull(@VehicleRegistration, VehicleRegistration),
         AreaType = isnull(@AreaType, AreaType) 
   where InboundDocumentTypeId = @InboundDocumentTypeId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_InboundDocumentTypeHistory_Insert
         @InboundDocumentTypeId = @InboundDocumentTypeId,
         @InboundDocumentTypeCode = @InboundDocumentTypeCode,
         @InboundDocumentType = @InboundDocumentType,
         @OverReceiveIndicator = @OverReceiveIndicator,
         @AllowManualCreate = @AllowManualCreate,
         @QuantityToFollowIndicator = @QuantityToFollowIndicator,
         @PriorityId = @PriorityId,
         @AutoSendup = @AutoSendup,
         @BatchButton = @BatchButton,
         @BatchSelect = @BatchSelect,
         @DeliveryNoteNumber = @DeliveryNoteNumber,
         @VehicleRegistration = @VehicleRegistration,
         @AreaType = @AreaType,
         @CommandType = 'Update'
  
  return @Error
  
end
