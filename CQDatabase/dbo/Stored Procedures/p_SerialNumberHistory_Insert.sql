﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumberHistory_Insert
  ///   Filename       : p_SerialNumberHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2013 14:12:38
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the SerialNumberHistory table.
  /// </remarks>
  /// <param>
  ///   @SerialNumberId int = null,
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null,
  ///   @ReceiptId int = null,
  ///   @StoreInstructionId int = null,
  ///   @PickInstructionId int = null,
  ///   @SerialNumber varchar(50) = null,
  ///   @ReceivedDate datetime = null,
  ///   @DespatchDate datetime = null,
  ///   @CommandType varchar(10) = null,
  ///   @InsertDate datetime = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @BatchId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @ReceiptLineId int = null,
  ///   @StoreJobId int = null,
  ///   @PickJobId int = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @StatusId int = null 
  /// </param>
  /// <returns>
  ///   SerialNumberHistory.SerialNumberId,
  ///   SerialNumberHistory.StorageUnitId,
  ///   SerialNumberHistory.LocationId,
  ///   SerialNumberHistory.ReceiptId,
  ///   SerialNumberHistory.StoreInstructionId,
  ///   SerialNumberHistory.PickInstructionId,
  ///   SerialNumberHistory.SerialNumber,
  ///   SerialNumberHistory.ReceivedDate,
  ///   SerialNumberHistory.DespatchDate,
  ///   SerialNumberHistory.CommandType,
  ///   SerialNumberHistory.InsertDate,
  ///   SerialNumberHistory.ReferenceNumber,
  ///   SerialNumberHistory.BatchId,
  ///   SerialNumberHistory.IssueId,
  ///   SerialNumberHistory.IssueLineId,
  ///   SerialNumberHistory.ReceiptLineId,
  ///   SerialNumberHistory.StoreJobId,
  ///   SerialNumberHistory.PickJobId,
  ///   SerialNumberHistory.Remarks,
  ///   SerialNumberHistory.StatusId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumberHistory_Insert
(
 @SerialNumberId int = null,
 @StorageUnitId int = null,
 @LocationId int = null,
 @ReceiptId int = null,
 @StoreInstructionId int = null,
 @PickInstructionId int = null,
 @SerialNumber varchar(50) = null,
 @ReceivedDate datetime = null,
 @DespatchDate datetime = null,
 @CommandType varchar(10) = null,
 @InsertDate datetime = null,
 @ReferenceNumber nvarchar(60) = null,
 @BatchId int = null,
 @IssueId int = null,
 @IssueLineId int = null,
 @ReceiptLineId int = null,
 @StoreJobId int = null,
 @PickJobId int = null,
 @Remarks nvarchar(510) = null,
 @StatusId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert SerialNumberHistory
        (SerialNumberId,
         StorageUnitId,
         LocationId,
         ReceiptId,
         StoreInstructionId,
         PickInstructionId,
         SerialNumber,
         ReceivedDate,
         DespatchDate,
         CommandType,
         InsertDate,
         ReferenceNumber,
         BatchId,
         IssueId,
         IssueLineId,
         ReceiptLineId,
         StoreJobId,
         PickJobId,
         Remarks,
         StatusId)
  select @SerialNumberId,
         @StorageUnitId,
         @LocationId,
         @ReceiptId,
         @StoreInstructionId,
         @PickInstructionId,
         @SerialNumber,
         @ReceivedDate,
         @DespatchDate,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @ReferenceNumber,
         @BatchId,
         @IssueId,
         @IssueLineId,
         @ReceiptLineId,
         @StoreJobId,
         @PickJobId,
         @Remarks,
         @StatusId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
