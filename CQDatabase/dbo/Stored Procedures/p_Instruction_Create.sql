﻿/*
      <summary>
        Procedure Name : p_Instruction_Create
        Filename       : p_Instruction_Create.sql
        Create By      : Grant Schultz
        Date Created   : 23 May 2007 13:00:00
      </summary>
      <remarks>
        Inserts palletised lines into the instruction table
      </remarks>
      <param>
        @WarehouseId         int,
        @OperatorId          int,
        @InstructionTypeCode nvarchar(10),
        @StorageUnitBatchId  int,
        @PickLocationId      int,
        @StoreLocationId     int,
        @Quantity            float,
        @Weight              float = null,
        @PriorityId          int = null
      </param>
      <returns>
        @error
      </returns>
      <newpara>
        Modified by    : 
        Modified Date  : 
        Details        : 
      </newpara>
*/
create procedure p_Instruction_Create
(
 @WarehouseId         int,
 @OperatorId          int,
 @InstructionTypeCode nvarchar(10),
 @StorageUnitBatchId  int,
 @PickLocationId      int,
 @StoreLocationId     int,
 @Quantity            float,
 @Weight              float = null,
 @PriorityId          int = null
)
as
begin
	 set nocount on
  
  declare @error             int,
          @errormsg          nvarchar(500),
          @CreateDate        datetime,
          @JobId             int,
          @StatusId          int,
          @InstructionTypeId int,
          @InstructionId     int
  
  select @StatusId = StatusId
    from Status
   where Type = @InstructionTypeCode
  
  select @InstructionTypeId = InstructionTypeId
    from InstructionType
   where InstructionTypeCode = @InstructionTypeCode
  
  if @PriorityId is null
    select @PriorityId  = PriorityId
      from InstructionType
     where InstructionTypeCode = @InstructionTypeCode
  
  if @Weight is null
    select @Weight = @Quantity * isnull(p.Weight,1)
      from StorageUnitBatch sub (nolock)
      join StorageUnit      su  (nolock) on sub.StorageUnitId = su.StorageUnitId
      join Pack             p   (nolock) on su.StorageUnitId  = p.StorageUnitId
      join PackType         pt  (nolock) on p.PackTypeId      = pt.PackTypeId
     where pt.PackType = 'Unit'
  
  begin transaction
  
  -- Pick Location cannot = store location 
  if @PickLocationId = @StoreLocationId
  begin
    set @error = -1
    goto error
  end
  
  exec @error = p_Job_Insert
   @JobId         = @JobId output,
   @PriorityId    = @PriorityId,
   @OperatorId    = @OperatorId,
   @StatusId      = @StatusId,
   @WarehouseId   = @WarehouseId,
   @ReceiptLineId = null,
   @IssueLineId   = null
  
  if @error <> 0
    goto error
  
  select @CreateDate = dbo.ufn_Getdate()
  
  exec @error = p_Instruction_Insert
   @InstructionId      = @InstructionId,
   @ReasonId           = null,
   @InstructionTypeId  = @InstructionTypeId,
   @StorageUnitBatchId = @StorageUnitBatchId,
   @WarehouseId        = @WarehouseId,
   @StatusId           = @StatusId,
   @JobId              = @JobId,
   @OperatorId         = @OperatorId,
   @PickLocationId     = @PickLocationId,
   @StoreLocationId    = @StoreLocationId,
   @InstructionRefId   = null,
   @Quantity           = @Quantity,
   @ConfirmedQuantity  = 0,
   @Weight             = @Weight,
   @ConfirmedWeight    = 0,
   @PalletId           = null,
   @CreateDate         = @CreateDate,
   @StartDate          = null,
   @EndDate            = null
  
  if @error <> 0
    goto error
  
  exec @error = p_StorageUnitBatchLocation_Reserve
   @OperatorId          = @OperatorId,
   @StorageUnitBatchId  = @StorageUnitBatchId,
   @PickLocationId      = @PickLocationId,
   @StoreLocationId     = @StoreLocationId,
   @Quantity            = @Quantity
  
  if @error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Nope'
    rollback transaction
    return @error
end
