﻿Create procedure [dbo].[p_Interface_WebService_ProductMaster]
(
 @XMLBody nvarchar(max) output
)
--with encryption
as
begin
	
	DECLARE @doc xml
	DECLARE @idoc int
	DECLARE @InsertDate datetime
	DECLARE @ERROR AS VARCHAR(255)
	
	SELECT @doc = convert(xml,@XMLBody)
	     , @InsertDate = Getdate()
	     
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
	BEGIN TRY
		                
	    INSERT INTO [InterfaceImportProduct]
               ([RecordStatus]
               ,[ProcessedDate]
               ,[ProductCode]
               ,[Product]
               ,[SKUCode]
               ,[SKU]
               ,[PalletQuantity]
               ,[Barcode]
               ,[MinimumQuantity]
               ,[ReorderQuantity]
               ,[MaximumQuantity]
               ,[CuringPeriodDays]
               ,[ShelfLifeDays]
               ,[QualityAssuranceIndicator]
               ,[ProductType]
               ,[ProductCategory]
               ,[OverReceipt]
               ,[RetentionSamples]
               ,[HostId])               
	    SELECT  'W'
	           ,@InsertDate
	           ,nodes.entity.value('ProductCode[1]',        'varchar(30)')  'ProductCode'
	           ,nodes.entity.value('Product[1]',            'varchar(255)') 'Product'
               ,nodes.entity.value('SKUCode[1]',            'varchar(10)')  'SKUCode'
               ,nodes.entity.value('SKU[1]',                'varchar(10)')  'SKU'
               ,nodes.entity.value('PalletQuantity[1]',     'int')          'PalletQuantity'
               ,nodes.entity.value('Barcode[1]',            'varchar(50)')  'Barcode'
               ,nodes.entity.value('MinimumQuantity[1]',    'int')          'MinimumQuantity'
               ,nodes.entity.value('ReorderQuantity[1]',    'int')          'ReorderQuantity'
               ,nodes.entity.value('MaximumQuantity[1]',    'int')          'MaximumQuantity'
               ,nodes.entity.value('CuringPeriodDays[1]',   'int')          'CuringPeriodDays'
               ,nodes.entity.value('ShelfLifeDays[1]',      'int')          'ShelfLifeDays'
               ,nodes.entity.value('QualityAssuranceIndicator[1]', 'bit')   'QualityAssuranceIndicator'
               ,nodes.entity.value('ProductType[1]',        'varchar(20)')  'ProductType'
               ,nodes.entity.value('ProductCategory[1]',    'varchar(20)')  'ProductCategory'
               ,nodes.entity.value('OverReceipt[1]',        'numeric(13,3)') 'OverReceipt'
               ,nodes.entity.value('RetentionSamples[1]',   'int')          'RetentionSamples'
               ,nodes.entity.value('HostId[1]',             'varchar(30)')  'HostId'
               
               --,CAST(ISNULL(nodes.entity.value('VatPercentage[1]', 'varchar(50)'),'0.00') AS decimal(13,3)) 'VatPercentage'
               
               --,nodes.entity.value('ProcessedDate[1]',      'datetime
               --,nodes.entity.value('InsertDate[1]',         'datetime		       
	    FROM
	            @doc.nodes('/root/Body/Item') AS nodes(entity)
    	
    	
	    INSERT INTO [InterfaceImportPack]
           (InterfaceImportProductId
           ,[PackCode]
           ,[PackDescription]
           ,[Quantity]
           ,[Barcode]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[Volume]
           ,[NettWeight]
           ,[GrossWeight]
           ,[TareWeight]
           ,[ProductCategory]
           ,[PackingCategory]
           ,[PickEmpty]
           ,[StackingCategory]
           ,[MovementCategory]
           ,[ValueCategory]
           ,[StoringCategory]
           ,[PickPartPallet])
     	    SELECT
     	        p.InterfaceImportProductId
	           ,nodes.entity.value('PackCode[1]',           'varchar(30)')  'PackCode'
               ,nodes.entity.value('PackDescription[1]',    'varchar(255)') 'PackDescription'
               ,nodes.entity.value('Quantity[1]',           'int')          'Quantity'
               ,nodes.entity.value('Barcode[1]',            'varchar(50)')  'Barcode'
               ,nodes.entity.value('Length[1]',             'numeric(13,6)') 'Length'
               ,nodes.entity.value('Width[1]',              'numeric(13,6)') 'Width'
               ,nodes.entity.value('Height[1]',              'numeric(13,6)') 'Height'
               ,nodes.entity.value('Volume[1]',             'numeric(13,6)') 'Volume'
               ,nodes.entity.value('NettWeight[1]',         'numeric(13,6)') 'NettWeight'
               ,nodes.entity.value('GrossWeight[1]',        'numeric(13,6)') 'GrossWeight'
               ,nodes.entity.value('TareWeight[1]',         'numeric(13,6)') 'TareWeight'
               ,nodes.entity.value('ProductCategory[1]',    'char(1)')      'ProductCategory'
               ,nodes.entity.value('PackingCategory[1]',    'char(1)')      'PackingCategory'
               ,nodes.entity.value('PickEmpty[1]',          'bit')          'PickEmpty'
               ,nodes.entity.value('StackingCategory[1]',   'int')          'StackingCategory'
               ,nodes.entity.value('MovementCategory[1]',   'int')          'MovementCategory'
               ,nodes.entity.value('ValueCategory[1]',      'int')          'ValueCategory'
               ,nodes.entity.value('StoringCategory[1]',    'int')          'StoringCategory'
               ,nodes.entity.value('PickPartPallet[1]',     'int')          'PickPartPallet'
        FROM
	            @doc.nodes('/root/Body/Item/ItemLine') AS nodes(entity)
	            inner join InterfaceImportProduct p on p.ProductCode = nodes.entity.value('ForeignKey[1]', 'varchar(30)')
	    WHERE 
	            p.RecordStatus = 'W'
    				    	
	    Update prd 
	    set RecordStatus = 'N'
	    From InterfaceImportProduct prd
	    inner join InterfaceImportPack pck on prd.InterfaceImportProductId = pck.InterfaceImportProductId 
	    Where RecordSTatus = 'W' 
	    --and InsertDate = @InsertDate    	
	
	END TRY
	BEGIN CATCH
	    
	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	   
	   PRINT @ERROR
	END CATCH
	
	--exec p_Pastel_Import_Product
	
	SET @XMLBody = '<root><status>' 
	    + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR
	           ELSE 'Product(s) successfully imported'
	      END
	    + '</status></root>'
End