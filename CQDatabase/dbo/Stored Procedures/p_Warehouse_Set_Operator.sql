﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Warehouse_Set_Operator
  ///   Filename       : p_Warehouse_Set_Operator.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Jun 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Warehouse_Set_Operator
(
 @WarehouseId int,
 @OperatorId  int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_Operator_Update
   @OperatorId = @OperatorId,
   @WarehouseId = @WarehouseId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Warehouse_Set_Operator'
    rollback transaction
    return @Error
end
