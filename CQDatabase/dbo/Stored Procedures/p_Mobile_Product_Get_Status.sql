﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Get_Status
  ///   Filename       : p_Mobile_Product_Get_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Get_Status
(
 @WarehouseId     int,
 @JobId			  int,
 @StorageUnitId	  int
)
 
as
begin
	 set nocount on;
	 
	declare @Checked int,
			@JobCode varchar(10)
	set @Checked = '0'
  select @JobCode = JobCode
    from viewInstruction (nolock)
   where JobId = @jobId
   and   StorageUnitId = @StorageUnitId
   
   if @JobCode = 'CD'
	set @Checked = 1
	

end

	select @Checked 
	return @Checked 
