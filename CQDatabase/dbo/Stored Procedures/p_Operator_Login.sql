﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Login
  ///   Filename       : p_Operator_Login.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Login
(
 @OperatorId        int,
 @SiteType          nvarchar(20)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Exception         nvarchar(255),
          @Operator          nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 begin transaction
  
  exec @Error = p_Operator_Update
   @OperatorId   = @OperatorId,
   @SiteType     = @SiteType,
   @LoginTime    = @GetDate,
   @LastActivity = @GetDate
  
  if @Error <> 0
    goto error
  
  select @Operator = Operator
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  select @Exception = 'Operator ' + @Operator + ' logged in.'
  
  exec @Error = p_Exception_Insert
   @ExceptionId   = null,
   @ExceptionCode = 'LOGIN',
   @Exception     = @Exception,
   @OperatorId    = @OperatorId,
   @CreateDate    = @GetDate,
   @ExceptionDate = @Getdate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Operator_Login'
    rollback transaction
    return @Error
end
