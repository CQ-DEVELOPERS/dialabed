﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyType_Update
  ///   Filename       : p_ExternalCompanyType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:38
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ExternalCompanyType table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyTypeId int = null,
  ///   @ExternalCompanyType nvarchar(100) = null,
  ///   @ExternalCompanyTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyType_Update
(
 @ExternalCompanyTypeId int = null,
 @ExternalCompanyType nvarchar(100) = null,
 @ExternalCompanyTypeCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @ExternalCompanyTypeId = '-1'
    set @ExternalCompanyTypeId = null;
  
  if @ExternalCompanyType = '-1'
    set @ExternalCompanyType = null;
  
  if @ExternalCompanyTypeCode = '-1'
    set @ExternalCompanyTypeCode = null;
  
	 declare @Error int
 
  update ExternalCompanyType
     set ExternalCompanyType = isnull(@ExternalCompanyType, ExternalCompanyType),
         ExternalCompanyTypeCode = isnull(@ExternalCompanyTypeCode, ExternalCompanyTypeCode) 
   where ExternalCompanyTypeId = @ExternalCompanyTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
