﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Unread_Communication
  ///   Filename       : p_Dashboard_Unread_Communication.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Unread_Communication
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 select ch.CallOffNumber,
	        c.Comment
	   from CallOffHeader         ch (nolock)
	   join CommentCallOffHeader cch (nolock) on ch.CallOffHeaderId = cch.CallOffHeaderId
	   join Comment                c (nolock) on cch.CommentId = c.CommentId
end
