﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RecordStatus_Search
  ///   Filename       : p_RecordStatus_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:23
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the RecordStatus table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   RecordStatus.StatusId,
  ///   RecordStatus.StatusCode,
  ///   RecordStatus.StatusDesc 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RecordStatus_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         RecordStatus.StatusId
        ,RecordStatus.StatusCode
        ,RecordStatus.StatusDesc
    from RecordStatus
  
end
