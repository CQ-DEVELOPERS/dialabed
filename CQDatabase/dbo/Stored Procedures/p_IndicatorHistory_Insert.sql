﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IndicatorHistory_Insert
  ///   Filename       : p_IndicatorHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:23
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IndicatorHistory table.
  /// </remarks>
  /// <param>
  ///   @IndicatorId int = null,
  ///   @Header nvarchar(100) = null,
  ///   @Indicator nvarchar(100) = null,
  ///   @IndicatorValue sql_variant = null,
  ///   @IndicatorDisplay sql_variant = null,
  ///   @ThresholdGreen int = null,
  ///   @ThresholdYellow int = null,
  ///   @ThresholdRed int = null,
  ///   @ModifiedDate datetime = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   IndicatorHistory.IndicatorId,
  ///   IndicatorHistory.Header,
  ///   IndicatorHistory.Indicator,
  ///   IndicatorHistory.IndicatorValue,
  ///   IndicatorHistory.IndicatorDisplay,
  ///   IndicatorHistory.ThresholdGreen,
  ///   IndicatorHistory.ThresholdYellow,
  ///   IndicatorHistory.ThresholdRed,
  ///   IndicatorHistory.ModifiedDate,
  ///   IndicatorHistory.CommandType,
  ///   IndicatorHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IndicatorHistory_Insert
(
 @IndicatorId int = null,
 @Header nvarchar(100) = null,
 @Indicator nvarchar(100) = null,
 @IndicatorValue sql_variant = null,
 @IndicatorDisplay sql_variant = null,
 @ThresholdGreen int = null,
 @ThresholdYellow int = null,
 @ThresholdRed int = null,
 @ModifiedDate datetime = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @ThresholdGreen = '-1'
    set @ThresholdGreen = null;
  
	 declare @Error int
 
  insert IndicatorHistory
        (IndicatorId,
         Header,
         Indicator,
         IndicatorValue,
         IndicatorDisplay,
         ThresholdGreen,
         ThresholdYellow,
         ThresholdRed,
         ModifiedDate,
         CommandType,
         InsertDate)
  select @IndicatorId,
         @Header,
         @Indicator,
         @IndicatorValue,
         @IndicatorDisplay,
         @ThresholdGreen,
         @ThresholdYellow,
         @ThresholdRed,
         @ModifiedDate,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
