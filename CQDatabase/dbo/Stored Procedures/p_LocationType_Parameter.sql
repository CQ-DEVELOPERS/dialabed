﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationType_Parameter
  ///   Filename       : p_LocationType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:00
  /// </summary>
  /// <remarks>
  ///   Selects rows from the LocationType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   LocationType.LocationTypeId,
  ///   LocationType.LocationType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationType_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as LocationTypeId
        ,'{All}' as LocationType
  union
  select
         LocationType.LocationTypeId
        ,LocationType.LocationType
    from LocationType
  
end
