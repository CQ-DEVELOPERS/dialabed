﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_Exception
  ///   Filename       : p_Report_Stock_Take_Exception.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 21 July 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_Take_Exception
(
 @WarehouseId		int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
  
  declare @GetDate datetime
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 declare @TableResult as table
	 (
	  JobId              int,
   InstructionType    nvarchar(50),
   StorageUnitBatchId int,
   Quantity           float,
   ConfirmedQuantity  float,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   CreateDate         datetime,
   StartDate          datetime,
   EndDate            datetime
	 )
	 
	 
	 insert @TableResult
        (JobId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
         Quantity,
         ConfirmedQuantity,
         CreateDate,
         StartDate)
  SELECT    i.JobId, 
            it.InstructionType, 
            i.StorageUnitBatchId, 
            i.PickLocationId, 
            i.StoreLocationId, 
            i.Quantity,
            i.ConfirmedQuantity, 
            i.CreateDate, 
            i.StartDate
FROM        Instruction AS i WITH (nolock) INNER JOIN
                      InstructionType AS it WITH (nolock) ON i.InstructionTypeId = it.InstructionTypeId
WHERE     (it.InstructionTypeCode = 'STE')
     and i.CreateDate   between @FromDate and @ToDate
	 and i.WarehouseId = Isnull(@WarehouseId,i.WarehouseId)
  
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
  
  
  select tr.Jobid,
         tr.InstructionType,
         tr.PickLocation,
         tr.StoreLocation,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         tr.Quantity,
         tr.ConfirmedQuantity,
         tr.CreateDate,
         tr.StartDate
    from @TableResult    tr
    join viewStock       vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
  order by tr.CreateDate
end
