﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentHistory_Insert
  ///   Filename       : p_InboundDocumentHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2013 14:47:28
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundDocumentHistory table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null,
  ///   @InboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @ReasonId int = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @FromLocation nvarchar(20) = null,
  ///   @ToLocation nvarchar(20) = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @DivisionId int = null,
  ///   @PrincipalId int = null 
  /// </param>
  /// <returns>
  ///   InboundDocumentHistory.InboundDocumentId,
  ///   InboundDocumentHistory.InboundDocumentTypeId,
  ///   InboundDocumentHistory.ExternalCompanyId,
  ///   InboundDocumentHistory.StatusId,
  ///   InboundDocumentHistory.WarehouseId,
  ///   InboundDocumentHistory.OrderNumber,
  ///   InboundDocumentHistory.DeliveryDate,
  ///   InboundDocumentHistory.CreateDate,
  ///   InboundDocumentHistory.ModifiedDate,
  ///   InboundDocumentHistory.CommandType,
  ///   InboundDocumentHistory.InsertDate,
  ///   InboundDocumentHistory.ReasonId,
  ///   InboundDocumentHistory.Remarks,
  ///   InboundDocumentHistory.ReferenceNumber,
  ///   InboundDocumentHistory.FromLocation,
  ///   InboundDocumentHistory.ToLocation,
  ///   InboundDocumentHistory.StorageUnitId,
  ///   InboundDocumentHistory.BatchId,
  ///   InboundDocumentHistory.DivisionId,
  ///   InboundDocumentHistory.PrincipalId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentHistory_Insert
(
 @InboundDocumentId int = null,
 @InboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @ReasonId int = null,
 @Remarks nvarchar(510) = null,
 @ReferenceNumber nvarchar(60) = null,
 @FromLocation nvarchar(20) = null,
 @ToLocation nvarchar(20) = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @DivisionId int = null,
 @PrincipalId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InboundDocumentHistory
        (InboundDocumentId,
         InboundDocumentTypeId,
         ExternalCompanyId,
         StatusId,
         WarehouseId,
         OrderNumber,
         DeliveryDate,
         CreateDate,
         ModifiedDate,
         CommandType,
         InsertDate,
         ReasonId,
         Remarks,
         ReferenceNumber,
         FromLocation,
         ToLocation,
         StorageUnitId,
         BatchId,
         DivisionId,
         PrincipalId)
  select @InboundDocumentId,
         @InboundDocumentTypeId,
         @ExternalCompanyId,
         @StatusId,
         @WarehouseId,
         @OrderNumber,
         @DeliveryDate,
         @CreateDate,
         @ModifiedDate,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @ReasonId,
         @Remarks,
         @ReferenceNumber,
         @FromLocation,
         @ToLocation,
         @StorageUnitId,
         @BatchId,
         @DivisionId,
         @PrincipalId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
