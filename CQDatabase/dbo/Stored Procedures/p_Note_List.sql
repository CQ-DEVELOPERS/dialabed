﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Note_List
  ///   Filename       : p_Note_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:47
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Note table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Note.NoteId,
  ///   Note.Note 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Note_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as NoteId
        ,'{All}' as Note
  union
  select
         Note.NoteId
        ,Note.Note
    from Note
  order by Note
  
end
