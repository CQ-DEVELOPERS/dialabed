﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Supplier_Return_Complete
  ///   Filename       : p_Supplier_Return_Complete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Supplier_Return_Complete
(
 @OutboundDocumentId int,
 @OperatorId        int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @OrderNumber       nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  insert InterfaceExportPOHeader
        (PrimaryKey,
         Additional1,
         RecordType,
         RecordStatus,
         SupplierCode)
  select od.OrderNumber,
         od.ReferenceNumber,
         'SR',
         'N',
         ec.ExternalCompanyCode
    from OutboundDocument od
    join ExternalCompany ec on od.ExternalCompanyId = ec.ExternalCompanyId
   where od.OutboundDocumentId = @OutboundDocumentId
  
  if @@rowcount > 0
    insert InterfaceExportPODetail
          (ForeignKey,
           LineNumber,
           ProductCode,
           Batch,
           Quantity,
           Additional1)
    select od.OrderNumber,
           ol.LineNumber,
           vs.ProductCode,
           vs.Batch,
           il.Quantity,
           w.WarehouseCode
      from OutboundDocument od
      join OutboundLine     ol on od.OutboundDocumentId  = ol.OutboundDocumentId
      join IssueLine        il on ol.OutboundLineId      = il.OutboundLineId
      join viewStock        vs on il.StorageUnitBatchId = vs.StorageUnitBatchId
      join Warehouse         w on od.WarehouseId        = w.WarehouseId
     where od.OutboundDocumentId = @OutboundDocumentId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Supplier_Return_Complete'
    rollback transaction
    return @Error
end
