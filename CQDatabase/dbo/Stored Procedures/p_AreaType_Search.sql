﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaType_Search
  ///   Filename       : p_AreaType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Mar 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaType_Search
(
 @WarehouseId int
)
 
as
begin
	 set nocount on;
  
  select distinct AreaType
    from Area
   where WarehouseId = @WarehouseId
end
