﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Putaway_Palletisation_Create
  ///   Filename       : p_Putaway_Palletisation_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Putaway_Palletisation_Create
(
 @InstructionId    int,
 @NumberOfLines    int
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @LineQuantity       float,
          @WarehouseId        int,
          @PickLocationId     int,
          @StorageUnitBatchId int,
          @AcceptedQuantity   float,
          @PalletQuantity     float,
          @PalletCount        float,
          @RemainingQuantity  float,
          @StatusId           int,
          @JobId              int,
          @ReceiptLineId      int,
          @IssueLineId        int,
          @Count              int,
          @InstructionRefId   int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Putaway_Palletisation_Create'
  
--  select @StatusId = StatusId
--    from Status
--   where StatusCode = 'R'
--     and Type       = 'R';
  
  select @WarehouseId        = WarehouseId,
         @PickLocationId     = PickLocationId,
         @StorageUnitBatchId = StorageUnitBatchId,
         @AcceptedQuantity   = Quantity,
         @JobId              = JobId,
         @ReceiptLineId      = ReceiptLineId,
         @IssueLineId        = IssueLineId,
         @InstructionRefId   = InstructionId
    from Instruction (nolock)
   where InstructionId = @InstructionId
     --and rl.StatusId   = @StatusId
  
  if @StorageUnitBatchId is null
    return -1
  
  if exists(select top 1 1 from Instruction (nolock) where JobId = @JobId and InstructionRefId = @InstructionRefId)
  begin
    declare @TableInstructions as table
    (
     InstructionId int,
     StatusCode    nvarchar(10)
    )
    
    insert @TableInstructions
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i (nolock)
      join Status      s (nolock) on i.StatusId = s.StatusId
     where i.JobId            = @JobId
       and i.InstructionRefId = @InstructionRefId
    
    set @Count = @@rowcount
    
    begin transaction
    
    if not exists(select 1
                    from @TableInstructions
                   where StatusCode != 'W') and @Count > 0
    begin
      while @Count > 0
      begin
        set @Count = @Count - 1
        
        select top 1 @InstructionId = InstructionId
          from @TableInstructions
        
        delete @TableInstructions where InstructionId = @InstructionId
        
        exec @Error = p_StorageUnitBatchLocation_Deallocate
         @InstructionId = @InstructionId
        
        exec @Error = p_instruction_Delete @InstructionId
        
        if @error <> 0
          goto error
      end
      
      commit transaction
    end
    else
    begin
      if @Count = 0
        commit transaction
      begin
        set @Error = -1
        set @Errormsg = 'This Job has already been worked on! (p_Putaway_Palletisation_Create).'
        goto error
      end
    end
  end
  
  select top 1 @PalletQuantity = p.Quantity
    from StorageUnitBatch sub (nolock)
    join Pack             p   (nolock) on sub.StorageUnitId = p.StorageUnitId
    join PackType         pt  (nolock) on p.PackTypeId      = pt.PackTypeId
   where StorageUnitBatchId = @StorageUnitBatchId
  order by InboundSequence
  
  begin transaction
  
  if (@AcceptedQuantity / @PalletQuantity) > @NumberOfLines
  begin
    set @Error = 100
    set @Errormsg = 'p_Putaway_Palletisation_Create - Cannot put more than a full pallet quantity onto single line'
    goto error
  end
  
  if @AcceptedQuantity < @NumberOfLines 
  begin
    set @Error = 200
    set @Errormsg = 'p_Putaway_Palletisation_Create - Cannot put a Quantity of ' + convert(nvarchar(10), @AcceptedQuantity) + ' on ' + convert(nvarchar(10), @NumberOfLines) + ' lines'
    goto error
  end
  
  if @AcceptedQuantity > @PalletQuantity
  begin
    set @PalletCount = floor(@AcceptedQuantity / @PalletQuantity)
    set @RemainingQuantity = @AcceptedQuantity - (@PalletQuantity * @PalletCount)
    
    if @PalletCount > 0
    begin
      while @PalletCount > 0
      begin
        set @PalletCount = @PalletCount - 1
        set @NumberOfLines = @NumberOfLines - 1
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @PickLocationId,
         @StoreLocationId     = null,
         @Quantity            = @PalletQuantity,
         @JobId               = @JobId,
         @ReceiptLineId       = @ReceiptLineId,
         @IssueLineId         = @IssueLineId,
         @InstructionRefId    = @InstructionRefId
        
        if @error <> 0
          goto error
      end
    end
  end
  
  if @RemainingQuantity is null
  begin
    set @LineQuantity      = floor(@AcceptedQuantity / @NumberOfLines)
    set @RemainingQuantity = @AcceptedQuantity - (floor(@AcceptedQuantity / @NumberOfLines) * @NumberOfLines)
  end
  else
  begin
    set @LineQuantity = @RemainingQuantity / @NumberOfLines
    set @NumberOfLines = @NumberOfLines - 1
  end
  
  while @NumberOfLines > 0
  begin
    set @NumberOfLines = @NumberOfLines -1
    
    if @NumberOfLines = 0 and @RemainingQuantity > 0
    begin
      set @LineQuantity = @LineQuantity + @RemainingQuantity
      set @RemainingQuantity = 0
    end
    
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = null,
     @Quantity            = @LineQuantity,
     @JobId               = @JobId,
     @ReceiptLineId       = @ReceiptLineId,
     @IssueLineId         = @IssueLineId,
     @InstructionRefId    = @InstructionRefId

    if @error <> 0
      goto error
  end
  
  if @RemainingQuantity > 0
  begin
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = null,
     @Quantity            = @RemainingQuantity,
     @JobId               = @JobId,
     @ReceiptLineId       = @ReceiptLineId,
     @IssueLineId         = @IssueLineId,
     @InstructionRefId    = @InstructionRefId

    if @error <> 0
      goto error
  end
  
  select @StatusId = StatusId
    from Status (nolock)
   where StatusCode = 'D' -- Palletised
     and Type       = 'I'
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @StatusId      = @StatusId
  
  if @error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @ErrorMsg
    rollback transaction
    return @Error
end
