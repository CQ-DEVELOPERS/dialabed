﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Weight_Update
  ///   Filename       : p_Pallet_Weight_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Weight_Update
(
 @jobId       int,
 @palletId    int,
 @tareWeight  numeric(13,6) = null,
 @nettWeight  numeric(13,6) = null,
 @grossWeight numeric(13,6) = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @tareWeight < 0
    set @tareWeight = null
  
  if @grossWeight < 0
    set @grossWeight = null
  
  if @jobId = -1
    set @jobId = null
  
  if @palletId = -1
    set @palletId = null
  
  begin transaction
  
  if @jobId is not null
  begin
    exec @Error = p_Job_Update
     @JobId      = @jobId,
     @TareWeight = @tareWeight,
     @NettWeight = @nettWeight,
     @Weight     = @grossWeight
    
    if @Error <> 0
      goto error
  end
  
  if @palletId is not null
  begin
    exec @Error = p_Pallet_Update
     @palletId   = @palletId,
     @Tare       = @tareWeight,
     @Nett       = @nettWeight,
     @Weight     = @grossWeight
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Pallet_Weight_Update'
    rollback transaction
    return @Error
end
