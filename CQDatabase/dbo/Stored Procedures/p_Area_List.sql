﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_List
  ///   Filename       : p_Area_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2014 09:46:59
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Area table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Area.AreaId,
  ///   Area.Area 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as AreaId
        ,'{All}' as Area
  union
  select
         Area.AreaId
        ,Area.Area
    from Area
  order by Area
  
end
