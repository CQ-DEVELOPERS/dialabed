﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Exception
  ///   Filename       : p_Report_Batch_Exception.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Report_Batch_Exception
(
	
 @WarehouseId       int,
 @DaysRemaining		int,
 @PercentRemaining		nvarchar(50)
)

as
begin
	 set nocount on;
	 
	 Set @PercentRemaining = Replace(@PercentRemaining,'%','')

  
	if @DaysRemaining = '-1' or len(@DaysRemaining) = 0  set @DaysRemaining = null
	if @PercentRemaining = '-1' set @PercentRemaining = null

Select b.Batch,
	Datediff(dd,Getdate(),b.ExpiryDate) as DaysRemaining,
	 case when Datediff(dd,ProductionDateTime,b.ExpiryDate) = 0 then 0 else 
	Round(convert(float,Datediff(dd,Getdate(),b.ExpiryDate)) / convert(float,Datediff(dd,ProductionDateTime,b.ExpiryDate)) * 100,0) end as PercentRemaining,
		@DaysRemaining as '2',
		p.ProductCode,
		p.Product,
		sku.SKUCode,
		b.BatchReferenceNumber,
		1 as ProductionLine,
		ProductionDateTime,
		b.ExpiryDate,
		b.ShelfLifeDays,
		s.Status as BatchStatus,
		o.OPerator as StatusModifiedBy,
		StatusModifiedDate,
		ExpectedYield,
		ActualYield,
		FilledYield,
		CreateDate,
		getdate() as IrradiationDate,
		p.ProductCode as ProductCode2,
		p.Product as Product2,
		'200' as Qty_Filled

From Batch b
		Join StorageUnitBatch sub on sub.BatchId = b.BatchId
		JOin StorageUnit su on su.StorageUnitId = sub.StorageUnitId
		Join Product p on p.ProductId = su.ProductId
		Join Status s on s.StatusId = b.StatusId
		Join Operator o on o.OperatorId = b.StatusModifiedBy
		Join SKU sku on sku.SkuId = su.SkuId
where Datediff(dd,Getdate(),b.ExpiryDate) <= Isnull(@DaysRemaining, Datediff(dd,Getdate(),b.ExpiryDate))
		and b.ExpiryDate > Getdate() and
 case when Datediff(dd,ProductionDateTime,b.ExpiryDate) = 0 then 0 else 
	Round(convert(float,Datediff(dd,Getdate(),b.ExpiryDate)) / convert(float,Datediff(dd,ProductionDateTime,b.ExpiryDate)) * 100,0) end 
		<= Isnull(@PercentRemaining,case when Datediff(dd,ProductionDateTime,b.ExpiryDate) = 0 then 0 else 
	Round(convert(float,Datediff(dd,Getdate(),b.ExpiryDate)) / convert(float,Datediff(dd,ProductionDateTime,b.ExpiryDate)) * 100,0) end)

	

	
	
end
