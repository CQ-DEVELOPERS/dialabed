﻿create procedure [dbo].[p_Interface_WebService_PM]
(
 @doc2			Nvarchar(max) output
)
--with encryption
as
begin
	
	declare @doc xml
	set @doc = convert(xml,@doc2)
	DECLARE @idoc int
	Declare @InsertDate datetime
	
	Set @InsertDate = Getdate()
--	Select top 10 * from InterfaceImportProduct
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
		Insert Into 
		InterfaceImportProduct(RecordStatus,
								InsertDate,
								HostId,
								ProductCode,
								Product,
								PalletQuantity,
								SKUCode,
								SKU,
								BarCode,
								Additional1,
								Additional2) --,
								--OuterCartonBarcode,
								--CaseQuantity)	
		SELECT    'W',
					@InsertDate,
					HostId,
					ProductCode,
					Product,
					PalletQuantity,
					'0',
					'0',
					Barcode,
					Description1,
					Description2
		FROM       OPENXML (@idoc, '/root/Product',1)
            WITH (HostId varchar(50) 'StockLink',
				  ProductCode  nvarchar(50) 'Code',
                  Product nvarchar(50) 'Description_1',
				  PalletQuantity varchar(50) 'PalletQuantity',
				  BarCode nvarchar(50) 'BarCode',
				  Description1 nvarchar(50) 'Description_2',
				  Description2 nvarchar(50) 'Description_3') --,
				 -- OuterCartonBarcode nvarchar(50) 'OuterCartonBarcode',
				  -- CaseQuantity varchar(50) 'CaseQuantity')
				 
		Insert Into InterFaceImportPack(HostId,
										 PackCode,
										 PackDescription,
										 Quantity)
		Select *											
		FROM       OPENXML (@idoc, '/root/Product/PackInfo',1)
            WITH (
					HostId  varchar(50) 'idPckTbl',
					PackCode nvarchar(50) 'Code',
					PackDescription nvarchar(50) 'Description',
					Quantity varchar(50) 'PackSize')	

		Update InterfaceImportProduct set RecordStatus = 'N'
		Where RecordSTatus = 'W' and InsertDate = @InsertDate
		Set @doc2 = ''
		exec p_Enterprise_Import_Product
End


