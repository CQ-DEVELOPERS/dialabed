﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Process_Insert
  ///   Filename       : p_Process_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Nov 2013 12:48:37
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Process table.
  /// </remarks>
  /// <param>
  ///   @ProcessId int = null output,
  ///   @ProcessCode nvarchar(60) = null,
  ///   @Process nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   Process.ProcessId,
  ///   Process.ProcessCode,
  ///   Process.Process 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Process_Insert
(
 @ProcessId int = null output,
 @ProcessCode nvarchar(60) = null,
 @Process nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @ProcessId = '-1'
    set @ProcessId = null;
  
  if @ProcessCode = '-1'
    set @ProcessCode = null;
  
  if @Process = '-1'
    set @Process = null;
  
	 declare @Error int
 
  insert Process
        (ProcessCode,
         Process)
  select @ProcessCode,
         @Process 
  
  select @Error = @@Error, @ProcessId = scope_identity()
  
  
  return @Error
  
end
