﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Parameter
  ///   Filename       : p_OutboundDocument_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 07:31:32
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundDocument table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OutboundDocument.OutboundDocumentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as OutboundDocumentId
        ,null as 'OutboundDocument'
  union
  select
         OutboundDocument.OutboundDocumentId
        ,OutboundDocument.OutboundDocumentId as 'OutboundDocument'
    from OutboundDocument
  
end
