﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLineInstruction_Insert
  ///   Filename       : p_IssueLineInstruction_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:52
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IssueLineInstruction table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentTypeId int = null,
  ///   @OutboundShipmentId int = null,
  ///   @OutboundDocumentId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null output,
  ///   @InstructionId int = null output,
  ///   @DropSequence int = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null 
  /// </param>
  /// <returns>
  ///   IssueLineInstruction.OutboundDocumentTypeId,
  ///   IssueLineInstruction.OutboundShipmentId,
  ///   IssueLineInstruction.OutboundDocumentId,
  ///   IssueLineInstruction.IssueId,
  ///   IssueLineInstruction.IssueLineId,
  ///   IssueLineInstruction.InstructionId,
  ///   IssueLineInstruction.DropSequence,
  ///   IssueLineInstruction.Quantity,
  ///   IssueLineInstruction.ConfirmedQuantity,
  ///   IssueLineInstruction.Weight,
  ///   IssueLineInstruction.ConfirmedWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLineInstruction_Insert
(
 @OutboundDocumentTypeId int = null,
 @OutboundShipmentId int = null,
 @OutboundDocumentId int = null,
 @IssueId int = null,
 @IssueLineId int = null output,
 @InstructionId int = null output,
 @DropSequence int = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null,
 @Weight float = null,
 @ConfirmedWeight float = null 
)
 
as
begin
	 set nocount on;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @InstructionId = '-1'
    set @InstructionId = null;
  
	 declare @Error int
 
  insert IssueLineInstruction
        (OutboundDocumentTypeId,
         OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         InstructionId,
         DropSequence,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight)
  select @OutboundDocumentTypeId,
         @OutboundShipmentId,
         @OutboundDocumentId,
         @IssueId,
         @IssueLineId,
         @InstructionId,
         @DropSequence,
         @Quantity,
         @ConfirmedQuantity,
         @Weight,
         @ConfirmedWeight 
  
  select @Error = @@Error, @InstructionId = scope_identity()
  
  
  return @Error
  
end
