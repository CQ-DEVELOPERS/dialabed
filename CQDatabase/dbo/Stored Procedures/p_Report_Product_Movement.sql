﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Movement
  ///   Filename       : p_Report_Product_Movement.sql
  ///   Create By      : Karen
  ///   Date Created   : 14 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_Movement
(
 @FromDate          datetime,
 @ToDate            datetime,
 @ProductCode		varchar(30),
 @WarehouseId		int
)
 
as
begin
	 set nocount on;
	 
	 declare @Weeks int,
	         @Count int,
	         @Date  datetime
	 

  
  declare @TableResult as table
  (StartDate		date,
   EndDate			date,
   MovementDate		date,
   ProductCode		nvarchar(50),
   Product			nvarchar(255),
   ContainerNumber  nvarchar(50),
   Opening			int,
   Received			int,
   Issued			int,
   Balance			int,
   Variance			int,
   Percentage		int,
   StockOnHand		int,
   WarehouseId		int,
   StorageUnitId	int,
   SKUId			int
  )
  
  declare @TableResult2 as table
  (
   StartDate		date,
   EndDate			date,
   MovementDate		date,
   ProductCode		varchar(30),
   Product			varchar(50),
   ContainerNumber  nvarchar(50),
   Opening			int,
   Received			int,
   Issued			int,
   Balance			int,
   Variance			int,
   Percentage		int,
   StockOnHand		int,
   WarehouseId		int,
   StorageUnitId	int,
   SKU  			varchar(50)
  )
  
	declare @TableContainer as table
		(
		MovementDate		date,
		StorageUnitBatchId	int,
		StorageUnitId		int,
		WarehouseId			int,
		ContainerNumber		nvarchar(50),
		Received			float
		)
	  
	if @ProductCode = '-1' 
	 set @ProductCode = null
	 
	 
	insert	@TableContainer
			(MovementDate,
			StorageUnitBatchId,
			StorageUnitId,
			WarehouseId,
			ContainerNumber,
			Received)
	select	r.DeliveryDate,
			rl.StorageUnitBatchId,
			sub.StorageUnitId,
			r.WarehouseId,
			r.ContainerNumber,
			sum(rl.AcceptedQuantity)		   
	from ReceiptLine        rl (nolock)  
	join Receipt             r (nolock) on rl.ReceiptId = r.ReceiptId  
	join status              s (nolock) on rl.statusid = s.statusid  
	join StorageUnitBatch  sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId  
	join StorageUnit        su (nolock) on sub.StorageUnitId = su.StorageUnitId
	join Product			 p (nolock) on su.ProductId = p.ProductId
	where s.statuscode = 'RC'  
	and CONVERT(datetime, CONVERT(varchar,DeliveryDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
	and p.ProductCode = isnull(@ProductCode, p.ProductCode)  
	and @WarehouseId   = r.WarehouseId
	group by
			r.DeliveryDate,
			rl.StorageUnitBatchId,
			sub.StorageUnitId,
			r.WarehouseId,
			r.ContainerNumber
	--select * from @TableContainer
    --select @Date = dateadd(wk, @Count, @FromDate)

    --select @Count = @Count + 1
    
    insert @TableResult
          (StartDate,
           EndDate,
           MovementDate,
           ProductCode,
           WarehouseId,
           StorageUnitId,
           SKUId)
    select @FromDate,
		   @ToDate,
           MovementDate,
           ProductCode,
           WarehouseId,
           StorageUnitId,
           SKUId
      from ProductMovement pm (nolock)
     where pm.MovementDate between @FromDate and @ToDate
       and pm.ProductCode = isnull(@ProductCode,pm.ProductCode)
       and pm.WarehouseId = @WarehouseId
       and (pm.ReceivedQuantity > 0 
       or pm.IssuedQuantity > 0)

	update tr
       set Opening = (select sum(OpeningBalance) 
					  from ProductMovement pm 
					 where pm.WarehouseId = @WarehouseId 
					   and pm.productcode = tr.productcode 
					   and pm.MovementDate = tr.StartDate)
					   --and pm.MovementDate between tr.StartDate and tr.EndDate)
      from @TableResult tr  
    
   
	update tr
       set Received = tc.Received
	  from @TableResult tr
	  join @TableContainer tc on tc.StorageUnitId = tr.StorageUnitId
	  where tr.WarehouseId = tc.WarehouseId
	    and tc.MovementDate = tr.MovementDate

         
	--update tr  
 --      set Received2 = (select sum(i.ConfirmedQuantity)  
 --                         from Instruction        i (nolock)  
 --                         join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId  
 --                        where it.InstructionTypeCode in ('S','SM','PR') -- Store, Store mixed, Production receipt  
	--					   and CONVERT(datetime, CONVERT(varchar,i.EndDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
	--					   and tr.StorageUnitBatchId = i.StorageUnitBatchId
	--					   and tr.WarehouseId   = i.WarehouseId)  
 --     from @TableResult tr  
         
   
      
	update tr
	   set Issued = (select sum(isnull(IssuedQuantity,0)) 
					   from ProductMovement pm (nolock) 
					  where pm.WarehouseId = @WarehouseId 
					    and pm.productcode = tr.productcode 
					    and pm.MovementDate = tr.MovementDate) 
	  from @TableResult tr
	 where tr.ProductCode = isnull(@ProductCode, tr.ProductCode)
	   and tr.WarehouseId = @WarehouseId
 

	insert	@TableResult2
			(StartDate,
			EndDate,
			ProductCode,
			Product,
			ContainerNumber,
			Opening,
			Received,
			Issued,
			WarehouseId,
			SKU)
	 select tr.StartDate,
			tr.EndDate,
			tr.ProductCode,
			p.Product,
			ContainerNumber,
			min(tr.Opening),
			tr.Received,
			tr.Issued,
			tr.WarehouseId,
			s.SKU
	  from @TableResult tr
	  join Product p on tr.ProductCode = p.ProductCode
	  left join Sku     s on tr.SKUId = s.SKUId
	  group by
			tr.StartDate,
			tr.EndDate,
			tr.ProductCode,
			p.Product,
			ContainerNumber,
			tr.Received,
			tr.Issued,
			tr.WarehouseId,
			p.Product,
			s.SKU
 
      
 update tr
     set Balance = ((isnull(Opening,0) + isnull(Received,0)) - isnull(Issued,0)) 
    from @TableResult2 tr
    
 --update tr
 --    set Variance = (isnull(Balance,0) - isnull(Issued,0)) 
 --   from @TableResult2 tr
    
  
SET ARITHABORT OFF
SET ANSI_WARNINGS OFF   

 update tr
     set Percentage = isnull((isnull(Variance,0) / isnull(Issued,0)*100),0)
    from @TableResult2 tr
    
   update tr
     set StockOnHand = tr.Opening + isnull(Received,0) - isnull(Issued,0)
    from @TableResult2 tr 
    
  --update tr
  --   set StockOnHand = isnull(((isnull(Received,0) + isnull(Opening,0)) / isnull(Issued,0)*100),0)
  --  from @TableResult2 tr   
    
      update tr
     set StockOnHand = 100
    from @TableResult2 tr 
    where isnull(tr.Received,0) = 0 and isnull(tr.Issued,0) = 0
    
    update tr
     set Variance = (isnull(StockOnHand,0) - isnull(Opening,0)) 
    from @TableResult2 tr
    

	select 
          tr.StartDate,
          tr.EndDate,
		  tr.Opening,
		  tr.Received,
		  tr.Issued,
		  tr.Balance,
		  tr.Variance,
		  tr.Percentage,
		  tr.StockOnHand,
		  tr.WarehouseId,
		  tr.Product,
		  tr.ProductCode,
		  tr.SKU,
		  ContainerNumber
		  
    from @TableResult2 tr
    order by tr.ProductCode

--select * from @TableResult
--select * from @TableResult2

end
 
