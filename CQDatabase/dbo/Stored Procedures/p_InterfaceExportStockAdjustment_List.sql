﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportStockAdjustment_List
  ///   Filename       : p_InterfaceExportStockAdjustment_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:15
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportStockAdjustment table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceExportStockAdjustment.InterfaceExportStockAdjustmentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportStockAdjustment_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceExportStockAdjustmentId
        ,null as 'InterfaceExportStockAdjustment'
  union
  select
         InterfaceExportStockAdjustment.InterfaceExportStockAdjustmentId
        ,InterfaceExportStockAdjustment.InterfaceExportStockAdjustmentId as 'InterfaceExportStockAdjustment'
    from InterfaceExportStockAdjustment
  
end
