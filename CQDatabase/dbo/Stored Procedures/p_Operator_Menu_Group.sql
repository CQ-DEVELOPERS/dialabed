﻿/*
  /// <summary>
  ///   Procedure Name : p_Operator_Menu_Group
  ///   Filename       : p_Operator_Menu_Group.sql
  ///   Create By      : William
  ///   Date Created   : 10 July 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// 
  /// <returns>
  ///   mi.MenuItemId,
  ///   mic.MenuItemText,
  ///   mic.ToolTip,
  ///   mi.NavigateTo,
  ///   mi.ParentMenuItemId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Operator_Menu_Group](
						@MenuItemId int
						)
as
begin
	 set nocount on

Select i.menuItemId as ParentItemId,
       i.MenuItem as ParentItemText,
       isnull(c.MenuItemid,'') as 'MenuItemid',
       isnull(c.MenuItem,'') as MenuItem,
       p.OperatorGroupId ,
       p.OperatorGroup,
       '~/App_Themes/Default/' + str(isnull(c.access,0),1) + '.png' as [c.access]
  from vw_MenuItem_OperatorGroup p
  Left join Menuitem i on i.MenuitemId = p.MenuItemId
  Left join Menu     m on i.MenuId = m.MenuId
  Left join vw_MenuItem_OperatorGroup c on	c.ParentMenuItemId	= p.MenuItemId 
	                                      and c.operatorGRoupid	 = p.OperatorGroupId
 where p.parentMenuItemId is null and i.MenuItemid = @MenuItemId
order by i.menuitemid,
         c.menuItemId,
         c.MenuItem,
         p.OperatorGroupId
	

for xml AUTO,ROOT('Menu_Items')

end

