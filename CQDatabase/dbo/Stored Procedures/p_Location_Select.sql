﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Select
  ///   Filename       : p_Location_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2013 08:13:23
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Location table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  ///   Location.LocationId,
  ///   Location.LocationTypeId,
  ///   Location.Location,
  ///   Location.Ailse,
  ///   Location.[Column],
  ///   Location.[Level],
  ///   Location.PalletQuantity,
  ///   Location.SecurityCode,
  ///   Location.RelativeValue,
  ///   Location.NumberOfSUB,
  ///   Location.StocktakeInd,
  ///   Location.ActiveBinning,
  ///   Location.ActivePicking,
  ///   Location.Used,
  ///   Location.Latitude,
  ///   Location.Longitude,
  ///   Location.Direction,
  ///   Location.Height,
  ///   Location.Length,
  ///   Location.Width,
  ///   Location.Volume 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Select
(
 @LocationId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Location.LocationId
        ,Location.LocationTypeId
        ,Location.Location
        ,Location.Ailse
        ,Location.[Column]
        ,Location.Level
        ,Location.PalletQuantity
        ,Location.SecurityCode
        ,Location.RelativeValue
        ,Location.NumberOfSUB
        ,Location.StocktakeInd
        ,Location.ActiveBinning
        ,Location.ActivePicking
        ,Location.Used
        ,Location.Latitude
        ,Location.Longitude
        ,Location.Direction
        ,Location.Height
        ,Location.Length
        ,Location.Width
        ,Location.Volume
    from Location
   where isnull(Location.LocationId,'0')  = isnull(@LocationId, isnull(Location.LocationId,'0'))
  order by Location
  
end
