﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorAverage_Insert
  ///   Filename       : p_OperatorAverage_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:14
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorAverage table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @InstructionTypeId int = null,
  ///   @EndDate datetime = null,
  ///   @Units int = null,
  ///   @Weight float = null,
  ///   @Instructions int = null,
  ///   @Orders int = null,
  ///   @OrderLines int = null,
  ///   @Jobs int = null,
  ///   @ActiveTime int = null,
  ///   @DwellTime int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   OperatorAverage.OperatorGroupId,
  ///   OperatorAverage.InstructionTypeId,
  ///   OperatorAverage.EndDate,
  ///   OperatorAverage.Units,
  ///   OperatorAverage.Weight,
  ///   OperatorAverage.Instructions,
  ///   OperatorAverage.Orders,
  ///   OperatorAverage.OrderLines,
  ///   OperatorAverage.Jobs,
  ///   OperatorAverage.ActiveTime,
  ///   OperatorAverage.DwellTime,
  ///   OperatorAverage.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorAverage_Insert
(
 @OperatorGroupId int = null,
 @InstructionTypeId int = null,
 @EndDate datetime = null,
 @Units int = null,
 @Weight float = null,
 @Instructions int = null,
 @Orders int = null,
 @OrderLines int = null,
 @Jobs int = null,
 @ActiveTime int = null,
 @DwellTime int = null,
 @WarehouseId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorAverage
        (OperatorGroupId,
         InstructionTypeId,
         EndDate,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         ActiveTime,
         DwellTime,
         WarehouseId)
  select @OperatorGroupId,
         @InstructionTypeId,
         @EndDate,
         @Units,
         @Weight,
         @Instructions,
         @Orders,
         @OrderLines,
         @Jobs,
         @ActiveTime,
         @DwellTime,
         @WarehouseId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
