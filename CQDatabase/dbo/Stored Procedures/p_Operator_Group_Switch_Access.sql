﻿/*
  /// <summary>
  ///   Procedure Name : p_Operator_Group_Switch_Access
  ///   Filename       : p_Operator_Group_Switch_Access.sql
  ///   Create By      : William
  ///   Date Created   : 30 Jun 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// 
  /// <returns>
  ///   mi.MenuItemId,
  ///   mic.MenuItem,
  ///   mic.ToolTip,
  ///   mi.NavigateTo,
  ///   mi.ParentMenuItemId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure dbo.p_Operator_Group_Switch_Access(@OperatorGroupId int,
													@MenuItemId int,
													@Access nvarchar(40)= null output)
as
begin
	 set nocount on
	 Declare @NewAccess bit
	 Declare @Parent int
	 
     Set @Parent = (Select ParentMenuItemId from MenuItem where MenuItemId = @MenuItemId)
	 Set @NewAccess = (Select (case access when 0 then 1 else 0 end) from OperatorGroupMenuItem
	                   where OperatorGroupId = @OperatorGroupId and MenuItemId = @MenuItemId )
	                   
	update OperatorGroupMenuItem set access = @NewAccess
	where OperatorGroupId = @OperatorGroupId and MenuItemId = @MenuItemId
	
	-- Below does the Children
	update OperatorGroupMenuItem set access = @NewAccess
	From OperatorGroupMenuItem o 
	       Join MenuItem m on m.MenuItemId =  o .MenuItemId
	where o.OperatorGroupId = @OperatorGroupId and m.ParentMenuItemId = @MenuItemId
	-- Do the PArent
	If (@NewAccess = 1)
	Begin
		update OperatorGroupMenuItem set access = 1
		From OperatorGroupMenuItem o 
		where o.MenuItemId = @Parent and o.OperatorGroupId = @OperatorGroupId
	End



	Select @Access = '~/App_Themes/Default/' + str(access,1) + '.png' 
	from OperatorGroupMenuItem
			where OperatorGroupId = @OperatorGroupId and MenuItemId = @MenuItemId
		
end
