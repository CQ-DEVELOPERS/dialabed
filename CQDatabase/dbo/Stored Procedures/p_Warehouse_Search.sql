﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Warehouse_Search
  ///   Filename       : p_Warehouse_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 May 2014 13:32:02
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Warehouse table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null output,
  ///   @CompanyId int = null,
  ///   @Warehouse nvarchar(100) = null,
  ///   @WarehouseCode nvarchar(60) = null,
  ///   @ParentWarehouseId int = null,
  ///   @AddressId int = null,
  ///   @PostalAddressId int = null,
  ///   @ContactListId int = null,
  ///   @PrincipalId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Warehouse.WarehouseId,
  ///   Warehouse.CompanyId,
  ///   Company.Company,
  ///   Warehouse.Warehouse,
  ///   Warehouse.WarehouseCode,
  ///   Warehouse.HostId,
  ///   Warehouse.ParentWarehouseId,
  ///   Warehouse.Warehouse,
  ///   Warehouse.AddressId,
  ///   Address.AddressId,
  ///   Warehouse.DesktopMaximum,
  ///   Warehouse.DesktopWarning,
  ///   Warehouse.MobileWarning,
  ///   Warehouse.MobileMaximum,
  ///   Warehouse.UploadToHost,
  ///   Warehouse.PostalAddressId,
  ///   Address.AddressId,
  ///   Warehouse.ContactListId,
  ///   ContactList.ContactListId,
  ///   Warehouse.PrincipalId,
  ///   Principal.Principal,
  ///   Warehouse.AllowInbound,
  ///   Warehouse.AllowOutbound 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Warehouse_Search
(
 @WarehouseId int = null output,
 @CompanyId int = null,
 @Warehouse nvarchar(100) = null,
 @WarehouseCode nvarchar(60) = null,
 @ParentWarehouseId int = null,
 @AddressId int = null,
 @PostalAddressId int = null,
 @ContactListId int = null,
 @PrincipalId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @CompanyId = '-1'
    set @CompanyId = null;
  
  if @Warehouse = '-1'
    set @Warehouse = null;
  
  if @WarehouseCode = '-1'
    set @WarehouseCode = null;
  
  if @ParentWarehouseId = '-1'
    set @ParentWarehouseId = null;
  
  if @AddressId = '-1'
    set @AddressId = null;
  
  if @PostalAddressId = '-1'
    set @PostalAddressId = null;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
 
  select
         Warehouse.WarehouseId
        ,Warehouse.CompanyId
         ,CompanyCompanyId.Company as 'Company'
        ,Warehouse.Warehouse
        ,Warehouse.WarehouseCode
        ,Warehouse.HostId
        ,Warehouse.ParentWarehouseId
         ,WarehouseParentWarehouseId.Warehouse as 'WarehouseParentWarehouseId'
        ,Warehouse.AddressId
        ,Warehouse.DesktopMaximum
        ,Warehouse.DesktopWarning
        ,Warehouse.MobileWarning
        ,Warehouse.MobileMaximum
        ,Warehouse.UploadToHost
        ,Warehouse.PostalAddressId
        ,Warehouse.ContactListId
        ,Warehouse.PrincipalId
         ,PrincipalPrincipalId.Principal as 'Principal'
        ,Warehouse.AllowInbound
        ,Warehouse.AllowOutbound
    from Warehouse
    left
    join Company CompanyCompanyId on CompanyCompanyId.CompanyId = Warehouse.CompanyId
    left
    join Warehouse WarehouseParentWarehouseId on WarehouseParentWarehouseId.WarehouseId = Warehouse.ParentWarehouseId
    left
    join Address AddressAddressId on AddressAddressId.AddressId = Warehouse.AddressId
    left
    join Address AddressPostalAddressId on AddressPostalAddressId.AddressId = Warehouse.PostalAddressId
    left
    join ContactList ContactListContactListId on ContactListContactListId.ContactListId = Warehouse.ContactListId
    left
    join Principal PrincipalPrincipalId on PrincipalPrincipalId.PrincipalId = Warehouse.PrincipalId
   where isnull(Warehouse.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Warehouse.WarehouseId,'0'))
     and isnull(Warehouse.CompanyId,'0')  = isnull(@CompanyId, isnull(Warehouse.CompanyId,'0'))
     and isnull(Warehouse.Warehouse,'%')  like '%' + isnull(@Warehouse, isnull(Warehouse.Warehouse,'%')) + '%'
     and isnull(Warehouse.WarehouseCode,'%')  like '%' + isnull(@WarehouseCode, isnull(Warehouse.WarehouseCode,'%')) + '%'
     and isnull(Warehouse.ParentWarehouseId,'0')  = isnull(@ParentWarehouseId, isnull(Warehouse.ParentWarehouseId,'0'))
     and isnull(Warehouse.AddressId,'0')  = isnull(@AddressId, isnull(Warehouse.AddressId,'0'))
     and isnull(Warehouse.PostalAddressId,'0')  = isnull(@PostalAddressId, isnull(Warehouse.PostalAddressId,'0'))
     and isnull(Warehouse.ContactListId,'0')  = isnull(@ContactListId, isnull(Warehouse.ContactListId,'0'))
     and isnull(Warehouse.PrincipalId,'0')  = isnull(@PrincipalId, isnull(Warehouse.PrincipalId,'0'))
  order by Warehouse
  
end
