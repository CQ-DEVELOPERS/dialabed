﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Station_Sheet_Receiving_Line_Search
  ///   Filename       : p_Station_Sheet_Receiving_Line_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Station_Sheet_Receiving_Line_Search
(
 @InboundShipmentId int = null,
 @ReceiptId int = null
)
 
as
begin
	 set nocount on;
  
  declare @WarehouseId int
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @InboundShipmentId in ('-1','0')
    set @InboundShipmentId = null
  
  if @InboundShipmentId is not null
    set @ReceiptId = null
  
  if @InboundShipmentId is not null
    select @WarehouseId = WarehouseId
      from InboundShipment (nolock)
     where InboundShipmentId = @InboundShipmentId
  
  if @ReceiptId is not null
    select @WarehouseId = WarehouseId
      from Receipt (nolock)
     where ReceiptId = @ReceiptId
  
  declare @TableResult as table
  (
   ReceiptLineId           int,
   ReceiptId               int,
   OrderNumber             nvarchar(30),
   LineNumber              int,
   ProductCode             nvarchar(30),
   Product                 nvarchar(50),
   BatchId                 int,
   Batch                   nvarchar(50),
   SKUCode                 nvarchar(50),
   Quantity                float,
   AcceptedQuantity        float,
   AcceptedWeight          float,
   DeliveryNoteQuantity    float,
   RejectQuantity          float,
   SampleQuantity          float,
   Status                  nvarchar(50),
   StorageUnitId           int,
   StorageUnitBatchId      int,
   CreditAdviceIndicator   bit default 0,
   ProductCategory         char(1),
   BatchButton             bit default 1,
   BatchSelect             bit default 0,
   ReasonId				   int,
   Reason				   nvarchar(50),
   AdjustmentQuantity	   float,
   Comments				   nvarchar(255),
   RequiredQuantity		   float,
   Remarks				   nvarchar(255))
  
  if @InboundShipmentId is null
  insert @TableResult
        (ReceiptLineId,
         ReceiptId,
         OrderNumber,
         Quantity,
         DeliveryNoteQuantity,
         AcceptedQuantity,
         AcceptedWeight,
         RejectQuantity,
         SampleQuantity,
         Status,
         StorageUnitBatchId,
         CreditAdviceIndicator,
         LineNumber,
         RequiredQuantity,
         Remarks)	
  select rl.ReceiptLineId,
         rl.ReceiptId,
         id.OrderNumber,
         rl.RequiredQuantity,
         rl.DeliveryNoteQuantity,
         rl.AcceptedQuantity,
         rl.AcceptedWeight,
         rl.RejectQuantity,
         rl.SampleQuantity,
         s.Status,
         rl.StorageUnitBatchId,
         r.CreditAdviceIndicator,
         il.LineNumber,
         rl.RequiredQuantity,
         id.Remarks
    from ReceiptLine rl
    join Receipt                  r (nolock) on rl.ReceiptId        = r.ReceiptId
    join InboundLine             il (nolock) on rl.InboundLineId    = il.InboundLineId
    join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join Status                   s (nolock) on rl.StatusId         = s.StatusId
   where rl.ReceiptId          = isnull(@ReceiptId, isnull(rl.ReceiptId,'0'))
     and s.Type = 'R'
     and s.StatusCode in ('W','R','P','S','LA','RC')
else
  insert @TableResult
        (ReceiptLineId,
         ReceiptId,
         OrderNumber,
         Quantity,
         DeliveryNoteQuantity,
         AcceptedQuantity,
         AcceptedWeight,
         RejectQuantity,
         SampleQuantity,
         Status,
         StorageUnitBatchId,
         CreditAdviceIndicator,
         LineNumber,
         RequiredQuantity,
         Remarks)
  select rl.ReceiptLineId,
         rl.ReceiptId,
         id.OrderNumber,
         rl.RequiredQuantity,
         rl.DeliveryNoteQuantity,
         rl.AcceptedQuantity,
         rl.AcceptedWeight,
         rl.RejectQuantity,
         rl.SampleQuantity,
         s.Status,
         rl.StorageUnitBatchId,
         r.CreditAdviceIndicator,
         il.LineNumber,
         rl.RequiredQuantity,
         id.Remarks

    from ReceiptLine rl
    join Receipt                  r (nolock) on rl.ReceiptId        = r.ReceiptId
    join InboundLine             il (nolock) on rl.InboundLineId    = il.InboundLineId
    join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    left outer
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId         = isr.ReceiptId
    join Status                   s (nolock) on rl.StatusId         = s.StatusId
   where isr.InboundShipmentId = @InboundShipmentId
     and s.Type = 'R'
     and s.StatusCode in ('W','R','P','S','LA','RC')
  
  update tr
     set BatchId       = sub.BatchId,
         StorageUnitId = sub.StorageUnitId
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
  
  update tr
     set ProductCode     = p.ProductCode,
         Product         = p.Product,
         Batch           = b.Batch,
         SKUCode         = sku.SKUCode,
         ProductCategory = su.ProductCategory
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId     = p.ProductId
    join SKU              sku (nolock) on su.SKUId         = sku.SKUId
    join Batch            b   (nolock) on tr.BatchId      = b.BatchId
  
  update tr
     set CreditAdviceIndicator = 1
    from @TableResult tr
    join Exception     e (nolock) on tr.ReceiptLineId = e.ReceiptLineId
   where ExceptionCode = 'CAVUPD'
   
  update tr
     set ReasonId = e.ReasonId, 
         AdjustmentQuantity = e.Quantity, 
         Comments = e.Exception,
         Reason = re.Reason
    from @TableResult tr
    join Exception     e (nolock) on tr.ReceiptLineId = e.ReceiptLineId
    join Reason       re (nolock) on e.ReasonId = re.ReasonId

  
  update @TableResult
     set BatchButton = dbo.ufn_Configuration(161, @WarehouseId)
  
  update @TableResult
     set BatchSelect = dbo.ufn_Configuration(162, @WarehouseId)
  
  select ReceiptLineId,
         ReceiptId,
         OrderNumber + ' LN:'+ convert(nvarchar(10), LineNumber) as 'OrderNumber',
         ProductCode,
         Product,
         BatchId,
         Batch,
         SKUCode,
         Quantity,
         DeliveryNoteQuantity as 'AcceptedQuantity',
         AcceptedWeight,
         DeliveryNoteQuantity,
         RejectQuantity,
         SampleQuantity,
         Status,
         StorageUnitId,
         StorageUnitBatchId,
         convert(bit, 0) as 'AlternateBatch',
         CreditAdviceIndicator,
         convert(bit, case when ProductCategory = 'P'
              then 0
              else BatchButton
              end) as 'BatchButton',
         BatchSelect,
         ReasonId,
		 Reason,
		 AdjustmentQuantity,
		 Comments,
		 RequiredQuantity,
		 Remarks
    from @TableResult
  order by OrderNumber,
           ProductCode,
           Product,
           SKUCode,
           Quantity
end
