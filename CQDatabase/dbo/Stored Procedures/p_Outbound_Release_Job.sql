﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Release_Job
  ///   Filename       : p_Outbound_Release_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Release_Job
(
 @JobId int,
 @OperatorId int = null,
 @StatusCode nvarchar(10) = 'RL'
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Outbound_Release_Job',
          @GetDate           datetime,
          @Transaction       bit = 1,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OperatorId = -1
    set @OperatorId = null
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  select @StatusId = StatusId
    from Status (nolock)
   where Type = 'IS'
     and StatusCode = @StatusCode
  
  if exists(select top 1 1
              from Job    j (nolock)
              join Status s (nolock) on j.StatusId = s.StatusId
             where JobId = @JobId
               and s.StatusCode in ('W','RL','PS'))
  begin
    exec p_Job_Update
     @JobId    = @JobId,
     @StatusId = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
