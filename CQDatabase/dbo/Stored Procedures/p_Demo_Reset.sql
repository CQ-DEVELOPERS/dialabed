﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Demo_Reset
  ///   Filename       : p_Demo_Reset.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Jun 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Demo_Reset
 
as
begin
 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if db_name() != 'Georgia'
  begin
    raiserror 900000 'Wrong database!'
    return
  end
  
  delete Address
  delete ContactList
  delete StorageUnitArea
  delete StorageUnitBatchLocation
  delete AreaOperatorGroup
  delete LocationOperatorGroup
  delete AreaLocation
  delete Area

  delete from InterfaceExportDetail
  delete from InterfaceExportHeader

  delete from InterfaceImportDetail
  delete from InterfaceImportHeader

  delete from InterfaceExportSODetail
  delete from InterfaceExportSOHeader

  delete from InterfaceImportSODetail
  delete from InterfaceImportSOHeader

  delete from InterfaceExportPODetail
  delete from InterfaceExportPOHeader

  delete from InterfaceImportPODetail
  delete from InterfaceImportPOHeader

  delete from InterfaceExportStockAdjustment

  delete from InterfaceMessage

  delete from InterfaceCustomer
  delete from InterfaceSupplier
  delete from InterfaceLog
  delete HousekeepingCompare
  delete OutboundPerformance
  delete Exception
  delete IssueLineInstruction
  delete Instruction
  delete OperatorQA
  delete SKUCheck
  delete ProductCheck
  delete Job
  delete ReceiptLine
  delete InboundShipmentReceipt
  delete InboundShipment
  delete SerialNumber
  delete Receipt
  delete InboundLine
  delete InboundDocument
  delete IssueLine
  delete OutboundShipmentIssue
  delete OutboundShipment
  delete Issue
  delete OutboundLine
  delete OutboundDocument
  delete OperatorShortPickPerformance
  delete OperatorPerformance
  delete Pallet
  delete StorageUnitBatchLocation
  delete StorageUnitBatch
  delete Batch
  delete StorageUnitLocation
  delete StorageUnitArea
  delete Pack
  delete PackType
  delete StorageUnitExternalCompany
  delete StorageUnit
  delete Product
  delete SKU
  --delete Operator
  delete ExternalCompany
  delete Location

  update Configuration set Value  = '0' where Configuration like '%Ref%'

  -- select 'truncate table ' + name from sysobjects where xtype = 'U' and name like '%History'

  truncate table ContactListHistory
  truncate table CultureHistory
  truncate table ExternalCompanyTypeHistory
  truncate table InboundDocumentTypeHistory
  truncate table InboundShipmentHistory
  truncate table InboundShipmentReceiptHistory
  truncate table OutboundDocumentTypeHistory
  truncate table InstructionTypeHistory
  truncate table IssueLineHistory
  truncate table LocationHistory
  truncate table IssueHistory
  truncate table LocationTypeHistory
  truncate table MenuHistory
  truncate table MenuItemHistory
  truncate table MenuItemCultureHistory
  truncate table ModuleHistory
  truncate table MovementSequenceHistory
  truncate table OperatorHistory
  truncate table OperatorGroupHistory
  truncate table OperatorGroupInstructionTypeHistory
  truncate table OperatorGroupMenuItemHistory
  truncate table OutboundDocumentHistory
  truncate table OutboundLineHistory
  truncate table PackTypeHistory
  truncate table PalletHistory
  truncate table PriorityHistory
  truncate table ProductHistory
  truncate table RaiserrorCodeHistory
  truncate table ReasonHistory
  truncate table ExternalCompanyHistory
  truncate table RouteHistory
  truncate table SerialNumberHistory
  truncate table ShipmentHistory
  truncate table ShipmentIssueHistory
  truncate table SKUHistory
  truncate table StatusHistory
  truncate table StorageUnitHistory
  truncate table StorageUnitAreaHistory
  truncate table StorageUnitBatchHistory
  truncate table StorageUnitBatchLocationHistory
  truncate table PackHistory
  truncate table StorageUnitLocationHistory
  truncate table UOMHistory
  truncate table WarehouseHistory
  truncate table InboundDocumentHistory
  truncate table InboundLineHistory
  truncate table ExceptionHistory
  truncate table OutboundShipmentHistory
  truncate table OutboundShipmentIssueHistory
  truncate table ReceiptHistory
  truncate table IndicatorHistory
  truncate table InstructionHistory
  truncate table ContainerTypeHistory
  truncate table JobHistory
  truncate table AddressHistory
  truncate table ApplicationExceptionHistory
  truncate table AreaHistory
  truncate table ReceiptLineHistory
  truncate table AreaLocationHistory
  truncate table AreaOperatorHistory
  truncate table AreaOperatorGroupHistory
  truncate table BatchHistory
  truncate table CompanyHistory
  truncate table ConfigurationHistory


  exec sp_grants_reseed
  exec sp_grants_reindex
  exec sp_grants_shrink
  
  begin transaction
  
  --drop table #Location
  create table #Location
  (
   LocationType     nvarchar(max),
   Location         nvarchar(max),
   Ailse            nvarchar(max),
   [Column]         nvarchar(max),
   [Level]          nvarchar(max),
   PalletQty        nvarchar(max),
   SecurityCode     nvarchar(max),
   RelativeValue    nvarchar(max),
   NumberOfSUB      nvarchar(max),
   Stocktake        nvarchar(max),
   ActiveBinning    nvarchar(max),
   ActivePicking    nvarchar(max),
   Used             nvarchar(max),
   Area             nvarchar(max)
  )

  insert #Location select 'Bulk','W01BL01','BL','01','01','1','79','100.01','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W01BL02','BL','02','02','1','6','100.02','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W01BL03','BL','03','03','1','56','100.03','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W01BL04','BL','04','04','1','93','100.04','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W01BL05','BL','05','05','1','43','100.05','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W02BL01','BL','01','01','1','53','101.01','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W02BL02','BL','02','02','1','91','101.02','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W02BL03','BL','03','03','1','22','101.03','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W02BL04','BL','04','04','1','89','101.04','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Bulk','W02BL05','BL','05','05','1','41','101.05','1','FALSE','TRUE','TRUE','TRUE','Bulk Lane'
  insert #Location select 'Racking','W02A01B','A','01','B','1','93','20101.201','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A01C','A','01','C','1','11','20101.301','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B01B','B','01','B','1','7','20201.201','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B01C','B','01','C','1','59','20201.301','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A02B','A','02','B','1','16','20302.202','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A02C','A','02','C','1','98','20302.302','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B02B','B','02','B','1','38','20402.202','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B02C','B','02','C','1','44','20402.302','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A03B','A','03','B','1','39','20503.203','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A03C','A','03','C','1','32','20503.303','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B03B','B','03','B','1','28','20603.203','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B03C','B','03','C','1','13','20603.303','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A04B','A','04','B','1','75','20704.204','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A04C','A','04','C','1','99','20704.304','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B04B','B','04','B','1','45','20804.204','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B04C','B','04','C','1','47','20804.304','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A05B','A','05','B','1','68','20905.205','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A05C','A','05','C','1','89','20905.305','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B05B','B','05','B','1','56','21005.205','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B05C','B','05','C','1','6','21005.305','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A06B','A','06','B','1','19','21106.206','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A06C','A','06','C','1','57','21106.306','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B06B','B','06','B','1','69','21206.206','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B06C','B','06','C','1','91','21206.306','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A07B','A','07','B','1','86','21307.207','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A07C','A','07','C','1','85','21307.307','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B07B','B','07','B','1','12','21407.207','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B07C','B','07','C','1','77','21407.307','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A08B','A','08','B','1','1','21508.208','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A08C','A','08','C','1','4','21508.308','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B08B','B','08','B','1','19','21608.208','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B08C','B','08','C','1','31','21608.308','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A09B','A','09','B','1','21','21709.209','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A09C','A','09','C','1','50','21709.309','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B09B','B','09','B','1','97','21809.209','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B09C','B','09','C','1','46','21809.309','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A10B','A','10','B','1','60','21910.210','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02A10C','A','10','C','1','14','21910.310','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B10B','B','10','B','1','24','22010.210','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02B10C','B','10','C','1','55','22010.310','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C01B','C','01','B','1','2','22101.201','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C01C','C','01','C','1','94','22101.301','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D01B','D','01','B','1','86','22201.201','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D01C','D','01','C','1','17','22201.301','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C02C','C','02','C','1','38','22302.302','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D02B','D','02','B','1','3','22402.202','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D02C','D','02','C','1','44','22402.302','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C03B','C','03','B','1','35','22503.203','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C03C','C','03','C','1','23','22503.303','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D03B','D','03','B','1','59','22603.203','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D03C','D','03','C','1','84','22603.303','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C04B','C','04','B','1','81','22704.204','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C04C','C','04','C','1','71','22704.304','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D04B','D','04','B','1','72','22804.204','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D04C','D','04','C','1','4','22804.304','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C05B','C','05','B','1','78','22905.205','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C05C','C','05','C','1','42','22905.305','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D05B','D','05','B','1','13','23005.205','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D05C','D','05','C','1','87','23005.305','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C06B','C','06','B','1','12','23106.206','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C06C','C','06','C','1','90','23106.306','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D06B','D','06','B','1','30','23206.206','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D06C','D','06','C','1','52','23206.306','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C02B','C','02','B','1','36','23302.202','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C07B','C','07','B','1','99','23307.207','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C07C','C','07','C','1','98','23307.307','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D07B','D','07','B','1','65','23407.207','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D07C','D','07','C','1','9','23407.307','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C08B','C','08','B','1','80','23508.208','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C08C','C','08','C','1','92','23508.308','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D08B','D','08','B','1','83','23608.208','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D08C','D','08','C','1','54','23608.308','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C09B','C','09','B','1','66','23709.209','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C09C','C','09','C','1','26','23709.309','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D09B','D','09','B','1','5','23809.209','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D09C','D','09','C','1','73','23809.309','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C10B','C','10','B','1','82','23910.210','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02C10C','C','10','C','1','57','23910.310','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D10B','D','10','B','1','58','24010.210','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Racking','W02D10C','D','10','C','1','29','24010.310','1','FALSE','TRUE','TRUE','TRUE','Racking'
  insert #Location select 'Pick-face','W02A01A','A','01','A','1','67','20101.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A02A','A','02','A','1','18','20101.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A03A','A','03','A','1','88','20302.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A04A','A','04','A','1','74','20302.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A05A','A','05','A','1','16','20502.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A06A','A','06','A','1','45','20502.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A07A','A','07','A','1','37','20702.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A08A','A','08','A','1','34','20702.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A09A','A','09','A','1','28','20902.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02A10A','A','10','A','1','75','20902.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B01A','B','01','A','1','96','20201.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B02A','B','02','A','1','70','20201.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B03A','B','03','A','1','11','20402.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B04A','B','04','A','1','8','20402.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B05A','B','05','A','1','77','20602.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B06A','B','06','A','1','25','20602.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B07A','B','07','A','1','61','20802.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B08A','B','08','A','1','10','20802.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B09A','B','09','A','1','7','21002.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02B10A','B','10','A','1','33','21002.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C01A','C','01','A','1','51','21101.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C02A','C','02','A','1','47','21101.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C03A','C','03','A','1','68','21302.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C04A','C','04','A','1','15','21302.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C05A','C','05','A','1','85','21502.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C06A','C','06','A','1','39','21502.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C07A','C','07','A','1','69','21702.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C08A','C','08','A','1','49','21702.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C09A','C','09','A','1','95','21902.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02C10A','C','10','A','1','62','21902.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D01A','D','01','A','1','79','21201.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D02A','D','02','A','1','6','21201.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D03A','D','03','A','1','56','21402.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D04A','D','04','A','1','93','21402.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D05A','D','05','A','1','43','21602.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D06A','D','06','A','1','53','21602.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D07A','D','07','A','1','91','21802.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D08A','D','08','A','1','22','21802.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D09A','D','09','A','1','89','22002.101','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W02D10A','D','10','A','1','41','22002.102','1','FALSE','TRUE','TRUE','TRUE','Pick-face'
  insert #Location select 'Pick-face','W04A01A','A','01','A','1','70','40101.101','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A02A','A','02','A','1','11','40101.102','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A03A','A','03','A','1','8','40402.101','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A04A','A','04','A','1','77','40402.102','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A05A','A','05','A','1','25','40502.101','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A06A','A','06','A','1','61','40502.102','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A07A','A','07','A','1','10','40702.101','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A08A','A','08','A','1','7','40702.102','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A09A','A','09','A','1','33','40902.101','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Pick-face','W04A10A','A','10','A','1','51','41902.102','1','FALSE','TRUE','TRUE','TRUE','High-Value'
  insert #Location select 'Receiving','W01RC01','RC','01','01','1','47','10101.101','1','FALSE','TRUE','TRUE','TRUE','Receiving'
  insert #Location select 'Receiving','W01RC02','RC','02','02','1','68','10101.102','2','FALSE','TRUE','TRUE','TRUE','Receiving'
  insert #Location select 'Returns','W01RT01','RT','01','01','1','93','10701.101','1','FALSE','TRUE','TRUE','TRUE','Returns'
  insert #Location select 'Damages','W01DM01','DM','01','01','1','11','10801.101','1','FALSE','TRUE','TRUE','TRUE','Damages'
  insert #Location select 'Checking','W01CH01','CH','01','01','1','7','10901.101','1','FALSE','TRUE','TRUE','TRUE','Checking'
  insert #Location select 'Checking','W01CH02','CH','02','02','1','59','10901.102','2','FALSE','TRUE','TRUE','TRUE','Checking'
  insert #Location select 'Checking','W01CH03','CH','03','03','1','16','10901.103','3','FALSE','TRUE','TRUE','TRUE','Checking'
  insert #Location select 'Dispatch','W05DS01','DS','01','01','1','98','50101.101','1','FALSE','TRUE','TRUE','TRUE','Dispatch'
  insert #Location select 'Dispatch','W05DS02','DS','02','02','1','38','50101.102','2','FALSE','TRUE','TRUE','TRUE','Dispatch'
  insert #Location select 'Dispatch','W05DS03','DS','03','03','1','44','50101.103','3','FALSE','TRUE','TRUE','TRUE','Dispatch'

  insert Area
        (WarehouseId,
         Area,
         AreaCode,
         StockOnHand,
         AreaSequence,
         AreaType,
         WarehouseCode,
         Replenish)
  select distinct 1,
         Area,
         case when Area = 'Pick-face' then 'PK' 
              when Area = 'Racking'   then 'RK'
              when Area = 'Bulk Lane' then 'BK'
              when Area = 'High-Value' then 'SP'
              when Area = 'Receiving' then 'R'
              when Area = 'Returns' then 'J'
              when Area = 'Dispatch' then 'D'
              when Area = 'Damages' then 'JJ'
              when Area = 'Checking' then 'SW'
              else 'SP'
              end,
         1,
         1,
         '',
         null,
         case when Area = 'Pick-face' then '1'
              else 0
              end 
    from #Location

  insert Location
        (LocationTypeId,
         Location,
         Ailse,
         [Column],
         [Level],
         PalletQuantity,
         SecurityCode,
         RelativeValue,
         NumberOfSUB,
         StocktakeInd,
         ActiveBinning,
         ActivePicking,
         Used)
  select 1,
         Location,
         Ailse,
         [Column],
         [Level],
         PalletQty,
         SecurityCode,
         RelativeValue,
         NumberOfSUB,
         Stocktake,
         ActiveBinning,
         ActivePicking,
         Used
    from #Location

  insert AreaLocation
        (AreaId,
         LocationId)
  select max(a.AreaId),
         max(l.LocationId)
    from #Location t
    join Area      a on t.Area = a.Area collate Latin1_General_CI_AS
    join Location  l on t.Location = l.Location collate Latin1_General_CI_AS
  group by t.Area, t.Location

  --drop table #Product
  create table #Product
  (
   ProductCode nvarchar(max),
   Product     nvarchar(max),
   SKUCode     nvarchar(max),
   PackType    nvarchar(max),
   Quantity    nvarchar(max)
  )
  insert #Product select 'PRDA001','Product A - Perishable Goods 001','EACH','Single','1'
  insert #Product select 'PRDA001','Product A - Perishable Goods 001','EACH','6 Pack','6'
  insert #Product select 'PRDA001','Product A - Perishable Goods 001','EACH','Case','24'
  insert #Product select 'PRDA001','Product A - Perishable Goods 001','EACH','Pallet','120'
  insert #Product select 'PRDA002','Product A - Perishable Goods 002','EACH','Single','1'
  insert #Product select 'PRDA002','Product A - Perishable Goods 002','EACH','6 Pack','6'
  insert #Product select 'PRDA002','Product A - Perishable Goods 002','EACH','Case','24'
  insert #Product select 'PRDA002','Product A - Perishable Goods 002','EACH','Pallet','120'
  insert #Product select 'PRDA003','Product A - Perishable Goods 003','EACH','Single','1'
  insert #Product select 'PRDA003','Product A - Perishable Goods 003','EACH','6 Pack','6'
  insert #Product select 'PRDA003','Product A - Perishable Goods 003','EACH','Case','24'
  insert #Product select 'PRDA003','Product A - Perishable Goods 003','EACH','Pallet','120'
  insert #Product select 'PRDA004','Product A - Perishable Goods 004','EACH','Single','1'
  insert #Product select 'PRDA004','Product A - Perishable Goods 004','EACH','6 Pack','6'
  insert #Product select 'PRDA004','Product A - Perishable Goods 004','EACH','Case','24'
  insert #Product select 'PRDA004','Product A - Perishable Goods 004','EACH','Pallet','120'
  insert #Product select 'PRDA005','Product A - Perishable Goods 005','EACH','Single','1'
  insert #Product select 'PRDA005','Product A - Perishable Goods 005','EACH','6 Pack','6'
  insert #Product select 'PRDA005','Product A - Perishable Goods 005','EACH','Case','24'
  insert #Product select 'PRDA005','Product A - Perishable Goods 005','EACH','Pallet','120'
  insert #Product select 'PRDB001','Product B - Non-Perishable Goods 001','EACH','Single','1'
  insert #Product select 'PRDB001','Product B - Non-Perishable Goods 001','EACH','6 Pack','6'
  insert #Product select 'PRDB001','Product B - Non-Perishable Goods 001','EACH','Case','24'
  insert #Product select 'PRDB001','Product B - Non-Perishable Goods 001','EACH','Pallet','600'
  insert #Product select 'PRDB002','Product B - Non-Perishable Goods 002','EACH','Single','1'
  insert #Product select 'PRDB002','Product B - Non-Perishable Goods 002','EACH','6 Pack','6'
  insert #Product select 'PRDB002','Product B - Non-Perishable Goods 002','EACH','Case','24'
  insert #Product select 'PRDB002','Product B - Non-Perishable Goods 002','EACH','Pallet','600'
  insert #Product select 'PRDB003','Product B - Non-Perishable Goods 003','EACH','Single','1'
  insert #Product select 'PRDB003','Product B - Non-Perishable Goods 003','EACH','6 Pack','6'
  insert #Product select 'PRDB003','Product B - Non-Perishable Goods 003','EACH','Case','24'
  insert #Product select 'PRDB003','Product B - Non-Perishable Goods 003','EACH','Pallet','600'
  insert #Product select 'PRDB004','Product B - Non-Perishable Goods 004','EACH','Single','1'
  insert #Product select 'PRDB004','Product B - Non-Perishable Goods 004','EACH','6 Pack','6'
  insert #Product select 'PRDB004','Product B - Non-Perishable Goods 004','EACH','Case','24'
  insert #Product select 'PRDB004','Product B - Non-Perishable Goods 004','EACH','Pallet','600'
  insert #Product select 'PRDB005','Product B - Non-Perishable Goods 005','EACH','Single','1'
  insert #Product select 'PRDB005','Product B - Non-Perishable Goods 005','EACH','6 Pack','6'
  insert #Product select 'PRDB005','Product B - Non-Perishable Goods 005','EACH','Case','24'
  insert #Product select 'PRDB005','Product B - Non-Perishable Goods 005','EACH','Pallet','600'
  insert #Product select 'PRDC001','Product C - Serialised Goods 001','EACH','Single','1'
  insert #Product select 'PRDC001','Product C - Serialised Goods 001','EACH','Pallet','25'
  insert #Product select 'PRDC002','Product C - Serialised Goods 002','EACH','Single','1'
  insert #Product select 'PRDC002','Product C - Serialised Goods 002','EACH','Pallet','25'
  insert #Product select 'PRDC003','Product C - Serialised Goods 003','EACH','Single','1'
  insert #Product select 'PRDC003','Product C - Serialised Goods 003','EACH','Pallet','50'
  insert #Product select 'PRDC004','Product C - Serialised Goods 004','EACH','Single','1'
  insert #Product select 'PRDC004','Product C - Serialised Goods 004','EACH','Pallet','50'
  insert #Product select 'PRDC005','Product C - Serialised Goods 005','EACH','Single','1'
  insert #Product select 'PRDC005','Product C - Serialised Goods 005','EACH','Pallet','50'
  insert #Product select 'PRDD001','Product D - Non-Serialised Goods 001','EACH','Single','1'
  insert #Product select 'PRDD001','Product D - Non-Serialised Goods 001','EACH','Case','25'
  insert #Product select 'PRDD001','Product D - Non-Serialised Goods 001','EACH','Pallet','400'
  insert #Product select 'PRDD002','Product D - Non-Serialised Goods 002','EACH','Single','1'
  insert #Product select 'PRDD002','Product D - Non-Serialised Goods 002','EACH','Case','25'
  insert #Product select 'PRDD002','Product D - Non-Serialised Goods 002','EACH','Pallet','400'
  insert #Product select 'PRDD003','Product D - Non-Serialised Goods 003','EACH','Single','1'
  insert #Product select 'PRDD003','Product D - Non-Serialised Goods 003','EACH','Case','25'
  insert #Product select 'PRDD003','Product D - Non-Serialised Goods 003','EACH','Pallet','400'
  insert #Product select 'PRDD004','Product D - Non-Serialised Goods 004','EACH','Single','1'
  insert #Product select 'PRDD004','Product D - Non-Serialised Goods 004','EACH','Case','25'
  insert #Product select 'PRDD004','Product D - Non-Serialised Goods 004','EACH','Pallet','400'
  insert #Product select 'PRDD005','Product D - Non-Serialised Goods 005','EACH','Single','1'
  insert #Product select 'PRDD005','Product D - Non-Serialised Goods 005','EACH','Case','25'
  insert #Product select 'PRDD005','Product D - Non-Serialised Goods 005','EACH','Pallet','400'

  delete InterfaceImportPack
  delete InterfaceImportProduct
  exec sp_grants_reseed 'InterfaceImportProduct'
  exec sp_grants_reseed 'InterfaceImportPack'
  
  insert InterfaceImportProduct
        (RecordStatus,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         PalletQuantity)
  select 'N',
         ProductCode,
         Product,
         SKUCode,
         SKUCode,
         max(Quantity)
    from #Product
  group by ProductCode,
           Product,
           SKUCode

  insert InterfaceImportPack
        (InterfaceImportProductId,
         PackCode,
         PackDescription,
         Quantity)
  select ip.InterfaceImportProductId,
         t.PackType,
         t.PackType,
         t.Quantity
    from #Product t
    join InterfaceImportProduct ip on t.ProductCode = ip.ProductCode and t.SKUCode = ip.SKUCOde
  
  exec p_Enterprise_Import_Product

  update PackType set InboundSequence = 4, OutboundSequence = 4 where PackType = 'Single'
  update PackType set InboundSequence = 3, OutboundSequence = 3 where PackType = '6 Pack'
  update PackType set InboundSequence = 2, OutboundSequence = 2 where PackType = 'Case'
  update PackType set InboundSequence = 1, OutboundSequence = 1 where PackType = 'Pallet'

  truncate table InterfaceSupplier

  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPA001','Supplier A - Perishable Goods 001'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPA002','Supplier A - Perishable Goods 002'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPA003','Supplier A - Perishable Goods 003'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPA004','Supplier A - Perishable Goods 004'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPA005','Supplier A - Perishable Goods 005'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPB001','Supplier B - Non-Perishable Goods 001'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPB002','Supplier B - Non-Perishable Goods 002'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPB003','Supplier B - Non-Perishable Goods 003'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPB004','Supplier B - Non-Perishable Goods 004'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPB005','Supplier B - Non-Perishable Goods 005'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPC001','Supplier C - Serialised Goods 001'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPC002','Supplier C - Serialised Goods 002'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPC003','Supplier C - Serialised Goods 003'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPC004','Supplier C - Serialised Goods 004'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPC005','Supplier C - Serialised Goods 005'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPD001','Supplier D - Non-Serialised Goods 001'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPD002','Supplier D - Non-Serialised Goods 002'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPD003','Supplier D - Non-Serialised Goods 003'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPD004','Supplier D - Non-Serialised Goods 004'
  insert InterfaceSupplier (SupplierCode, SupplierName) select 'SUPD005','Supplier D - Non-Serialised Goods 005'

  exec p_Pastel_Import_Supplier

  --select * from ExternalCompany where ExternalCompanyTypeId = 2

  truncate table InterfaceCustomer

  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTA001','Customer A - Local Delivery 001','Route 02'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTA002','Customer A - Local Delivery 002','Route 01'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTA003','Customer A - Local Delivery 003','Route 03'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTA004','Customer A - Local Delivery 004','Route 02'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTA005','Customer A - Local Delivery 005','Route 01'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTB001','Customer B - Courier Delivery 001','HUB 04'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTB002','Customer B - Courier Delivery 002','HUB 02'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTB003','Customer B - Courier Delivery 003','HUB 01'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTB004','Customer B - Courier Delivery 004','HUB 01'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTB005','Customer B - Courier Delivery 005','HUB 03'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTC001','Customer C - Regionall Distribution 001','Region 03'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTC002','Customer C - Regionall Distribution 002','Region 01'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTC003','Customer C - Regionall Distribution 003','Region 02'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTC004','Customer C - Regionall Distribution 004','Region 03'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTC005','Customer C - Regionall Distribution 005','Region 04'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTD001','Customer D - Export 001','Country 01'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTD002','Customer D - Export 002','Country 02'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTD003','Customer D - Export 003','Country 03'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTD004','Customer D - Export 004','Country 04'
  insert InterfaceCustomer (CustomerCode, CustomerName, DeliveryPoint) select 'CSTD005','Customer D - Export 005','Country 05'

  exec p_Pastel_Import_Customer

  --select * from ExternalCompany where ExternalCompanyTypeId = 1

  --drop table #PurchaseOrders
  create table #PurchaseOrders
  (
   OrderNumber         nvarchar(max),
   SupplierCode        nvarchar(max),
   Remarks             nvarchar(max),
   NumberOfLines       nvarchar(max),
   OrderDate           nvarchar(max),
   PlannedDeliveryDate nvarchar(max),
   LineNumber          nvarchar(max),
   ProducCode          nvarchar(max),
   Product             nvarchar(max),
   SKUCode             nvarchar(max),
   Quantity            nvarchar(max)
  )

  insert #PurchaseOrders select 'PO001','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','1','PRDA001','Product A - Perishable Goods 001','EACH','50'
  insert #PurchaseOrders select 'PO001','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','2','PRDA002','Product A - Perishable Goods 002','EACH','200'
  insert #PurchaseOrders select 'PO001','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','3','PRDA003','Product A - Perishable Goods 003','EACH','400'
  insert #PurchaseOrders select 'PO002','SUPB001','Supplier B - Non-Perishable Goods 001','5','01/11/2011','05/11/2011','1','PRDB001','Product B - Non-Perishable Goods 001','EACH','250'
  insert #PurchaseOrders select 'PO002','SUPB001','Supplier B - Non-Perishable Goods 001','5','01/11/2011','05/11/2011','2','PRDB002','Product B - Non-Perishable Goods 002','EACH','1000'
  insert #PurchaseOrders select 'PO002','SUPB001','Supplier B - Non-Perishable Goods 001','5','01/11/2011','05/11/2011','3','PRDB003','Product B - Non-Perishable Goods 003','EACH','1000'
  insert #PurchaseOrders select 'PO002','SUPB001','Supplier B - Non-Perishable Goods 001','5','01/11/2011','05/11/2011','4','PRDB004','Product B - Non-Perishable Goods 004','EACH','1500'
  insert #PurchaseOrders select 'PO002','SUPB001','Supplier B - Non-Perishable Goods 001','5','01/11/2011','05/11/2011','5','PRDB005','Product B - Non-Perishable Goods 005','EACH','2000'
  insert #PurchaseOrders select 'PO003','SUPC001','Supplier C - Serialised Goods 001','3','02/11/2011','06/11/2011','1','PRDC001','Product C - Serialised Goods 001','EACH','75'
  insert #PurchaseOrders select 'PO003','SUPC001','Supplier C - Serialised Goods 001','3','02/11/2011','06/11/2011','2','PRDC002','Product C - Serialised Goods 002','EACH','100'
  insert #PurchaseOrders select 'PO003','SUPC001','Supplier C - Serialised Goods 001','3','02/11/2011','06/11/2011','3','PRDC003','Product C - Serialised Goods 003','EACH','150'
  insert #PurchaseOrders select 'PO004','SUPD001','Supplier D - Non-Serialised Goods 001','5','02/11/2011','06/11/2011','1','PRDD001','Product D - Non-Serialised Goods 001','EACH','800'
  insert #PurchaseOrders select 'PO004','SUPD001','Supplier D - Non-Serialised Goods 001','5','02/11/2011','06/11/2011','2','PRDD002','Product D - Non-Serialised Goods 002','EACH','800'
  insert #PurchaseOrders select 'PO004','SUPD001','Supplier D - Non-Serialised Goods 001','5','02/11/2011','06/11/2011','3','PRDD003','Product D - Non-Serialised Goods 003','EACH','400'
  insert #PurchaseOrders select 'PO004','SUPD001','Supplier D - Non-Serialised Goods 001','5','02/11/2011','06/11/2011','4','PRDD004','Product D - Non-Serialised Goods 004','EACH','1200'
  insert #PurchaseOrders select 'PO004','SUPD001','Supplier D - Non-Serialised Goods 001','5','02/11/2011','06/11/2011','5','PRDD005','Product D - Non-Serialised Goods 005','EACH','1600'
  insert #PurchaseOrders select 'PO005','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','1','PRDA001','Product A - Perishable Goods 001','EACH','50'
  insert #PurchaseOrders select 'PO005','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','2','PRDA002','Product A - Perishable Goods 002','EACH','200'
  insert #PurchaseOrders select 'PO005','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','3','PRDA003','Product A - Perishable Goods 003','EACH','400'
  insert #PurchaseOrders select 'PO006','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','1','PRDA001','Product A - Perishable Goods 001','EACH','50'
  insert #PurchaseOrders select 'PO006','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','2','PRDA002','Product A - Perishable Goods 002','EACH','200'
  insert #PurchaseOrders select 'PO006','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','3','PRDA003','Product A - Perishable Goods 003','EACH','400'
  insert #PurchaseOrders select 'PO007','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','1','PRDA001','Product A - Perishable Goods 001','EACH','50'
  insert #PurchaseOrders select 'PO007','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','2','PRDA002','Product A - Perishable Goods 002','EACH','200'
  insert #PurchaseOrders select 'PO007','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','3','PRDA003','Product A - Perishable Goods 003','EACH','400'
  insert #PurchaseOrders select 'PO008','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','1','PRDA001','Product A - Perishable Goods 001','EACH','50'
  insert #PurchaseOrders select 'PO008','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','2','PRDA002','Product A - Perishable Goods 002','EACH','200'
  insert #PurchaseOrders select 'PO008','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','3','PRDA003','Product A - Perishable Goods 003','EACH','400'
  insert #PurchaseOrders select 'PO009','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','1','PRDA001','Product A - Perishable Goods 001','EACH','50'
  insert #PurchaseOrders select 'PO009','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','2','PRDA002','Product A - Perishable Goods 002','EACH','200'
  insert #PurchaseOrders select 'PO009','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','3','PRDA003','Product A - Perishable Goods 003','EACH','400'
  insert #PurchaseOrders select 'PO010','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','1','PRDA001','Product A - Perishable Goods 001','EACH','50'
  insert #PurchaseOrders select 'PO010','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','2','PRDA002','Product A - Perishable Goods 002','EACH','200'
  insert #PurchaseOrders select 'PO010','SUPA001','Supplier A - Perishable Goods 001','3','01/11/2011','05/11/2011','3','PRDA003','Product A - Perishable Goods 003','EACH','400'

  truncate table InterfaceImportPODetail
  delete InterfaceImportPOHeader
  exec sp_grants_reseed 'InterfaceImportPOHeader'

  insert InterfaceImportPOHeader
        (PrimaryKey,
         OrderNumber,
         RecordType,
         RecordStatus,
         SupplierCode,
         Remarks,
         NumberOfLines,
         DeliveryDate)
  select distinct OrderNumber,
         OrderNumber,
         'PUR',
         'N',
         SupplierCode,
         Remarks,
         NumberOfLines,
         PlannedDeliveryDate
    from #PurchaseOrders

  insert InterfaceImportPODetail
        (ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Quantity)
  select OrderNumber,
         LineNumber,
         ProducCode,
         Product,
         SKUCode,
         Quantity
    from #PurchaseOrders

  exec p_Pastel_Import_PO
  --select * from InterfaceMessage
  --select * from viewReceipt

  update Configuration set Value = (select min(LocationId) from viewLocation where AreaCode = 'D') where ConfigurationId in (43,72)

  update Product set HostId = ProductCode

  truncate table InterfaceImportSODetail
  delete InterfaceImportSOHeader
  exec sp_grants_reseed 'InterfaceImportSOHeader'

  --drop table #Shipment
  create table #Shipment
  (
   Shipment             nvarchar(max),
   OrderNumber          nvarchar(max),
   CustomerCode         nvarchar(max),
   Remakrs              nvarchar(max),
   NumberOfLines        nvarchar(max),
   CreateDate           nvarchar(max),
   PlannedDeliveryDate  datetime,
   Route                nvarchar(max),
   DocumentType         nvarchar(max),
   Priority             nvarchar(max)
  )

  insert #Shipment select 'S001','SA001','CSTC001','Customer C - Regionall Distribution 001','3','2011/06/01','2011/06/10','Region 03','02 - Regional Sales Order','Low'
  insert #Shipment select 'S002','SA002','CSTD002','Customer D - Export 002','2','01/10/2011 08:20AM','2011/06/17','Country 02','04 - Export Sales Order','Low'
  insert #Shipment select 'S003','SA003','CSTD001','Customer D - Export 001','2','01/10/2011 09:15AM','2011/06/17','Country 01','04 - Export Sales Order','Low'
  insert #Shipment select 'S004','SA004','CSTA001','Customer A - Local Delivery 001','4','01/10/2011 14:30PM','2011/06/02','Route 02','01- Local Sales Order','Medium'
  insert #Shipment select 'S005','SA005','CSTB002','Customer B - Courier Delivery 002','3','01/10/2011 14:50PM','2011/06/02','HUB 02','03 - Emergency Order','High'
  insert #Shipment select 'S006','SA006','CSTA005','Customer A - Local Delivery 005','4','01/10/2011 15:15PM','2011/06/02','Route 01','01- Local Sales Order','Medium'
  insert #Shipment select 'S007','SA007','CSTC005','Customer C - Regionall Distribution 005','5','01/10/2011 16:15PM','2011/06/10','Region 04','02 - Regional Sales Order','Low'
  insert #Shipment select 'S008','SA008','CSTD003','Customer D - Export 003','2','01/10/2011 16:15PM','2011/06/17','Country 03','04 - Export Sales Order','Low'
  insert #Shipment select 'S009','SA009','CSTB003','Customer B - Courier Delivery 003','3','02/10/2011 08:20AM','2011/06/03','HUB 01','03 - Emergency Order','High'
  insert #Shipment select 'S006','SA010','CSTA002','Customer A - Local Delivery 002','1','02/10/2011 09:15AM','2011/06/03','Route 01','01- Local Sales Order','Medium'
  insert #Shipment select 'S009','SA011','CSTB004','Customer B - Courier Delivery 004','3','02/10/2011 09:15AM','2011/06/03','HUB 01','03 - Emergency Order','High'
  insert #Shipment select 'S010','SA012','CSTC002','Customer C - Regionall Distribution 002','3','02/10/2011 09:15AM','2011/06/10','Region 01','02 - Regional Sales Order','Low'
  insert #Shipment select 'S011','SA013','CSTC003','Customer C - Regionall Distribution 003','3','02/10/2011 09:15AM','2011/06/10','Region 02','02 - Regional Sales Order','Low'
  insert #Shipment select 'S012','SA014','CSTA004','Customer A - Local Delivery 004','3','02/10/2011 15:00PM','2011/06/03','Route 02','01- Local Sales Order','Medium'
  insert #Shipment select 'S013','SA015','CSTA003','Customer A - Local Delivery 003','5','02/10/2011 11:00AM','2011/06/03','Route 03','01- Local Sales Order','Medium'
  insert #Shipment select 'S014','SA016','CSTD004','Customer D - Export 004','4','02/10/2011 14:30PM','2011/06/17','Country 04','04 - Export Sales Order','Low'
  insert #Shipment select 'S015','SA017','CSTB001','Customer B - Courier Delivery 001','3','02/10/2011 16:15PM','2011/06/03','HUB 04','03 - Emergency Order','High'
  insert #Shipment select 'S016','SA018','CSTB005','Customer B - Courier Delivery 005','4','03/10/2011 14:30PM','2011/06/06','HUB 03','03 - Emergency Order','High'
  insert #Shipment select 'S001','SA019','CSTC004','Customer C - Regionall Distribution 004','3','03/10/2011 14:30PM','2011/06/10','Region 03','02 - Regional Sales Order','Low'
  insert #Shipment select 'S017','SA020','CSTD005','Customer D - Export 005','2','03/10/2011 15:15PM','2011/06/17','Country 05','04 - Export Sales Order','Low'

  --drop table #SalesOrders
  create table #SalesOrders
  (
   Shipment            nvarchar(max),
   OrderNumber         nvarchar(max),
   CustomerCode        nvarchar(max),
   Remarks             nvarchar(max),
   LineNumber          nvarchar(max),
   ProducCode          nvarchar(max),
   Product             nvarchar(max),
   SKUCode             nvarchar(max),
   Quantity            nvarchar(max)
  )

  insert #SalesOrders select 'S001','SA001','CSTC001','Customer C - Regionall Distribution 001','1','PRDA001','Product A - Perishable Goods 001','EACH','60'
  insert #SalesOrders select 'S001','SA001','CSTC001','Customer C - Regionall Distribution 001','2','PRDA004','Product A - Perishable Goods 004','EACH','60'
  insert #SalesOrders select 'S001','SA001','CSTC001','Customer C - Regionall Distribution 001','3','PRDA005','Product A - Perishable Goods 005','EACH','120'
  insert #SalesOrders select 'S001','SA019','CSTC004','Customer C - Regionall Distribution 004','1','PRDA002','Product A - Perishable Goods 002','EACH','6'
  insert #SalesOrders select 'S001','SA019','CSTC004','Customer C - Regionall Distribution 004','2','PRDA003','Product A - Perishable Goods 003','EACH','240'
  insert #SalesOrders select 'S001','SA019','CSTC004','Customer C - Regionall Distribution 004','3','PRDA005','Product A - Perishable Goods 005','EACH','240'
  insert #SalesOrders select 'S002','SA002','CSTD002','Customer D - Export 002','1','PRDA002','Product A - Perishable Goods 002','EACH','200'
  insert #SalesOrders select 'S002','SA002','CSTD002','Customer D - Export 002','2','PRDA003','Product A - Perishable Goods 003','EACH','100'
  insert #SalesOrders select 'S003','SA003','CSTD001','Customer D - Export 001','1','PRDA001','Product A - Perishable Goods 001','EACH','100'
  insert #SalesOrders select 'S003','SA003','CSTD001','Customer D - Export 001','2','PRDA005','Product A - Perishable Goods 005','EACH','100'
  insert #SalesOrders select 'S004','SA004','CSTA001','Customer A - Local Delivery 001','1','PRDD002','Product D - Non-Serialised Goods 002','EACH','50'
  insert #SalesOrders select 'S004','SA004','CSTA001','Customer A - Local Delivery 001','2','PRDD003','Product D - Non-Serialised Goods 003','EACH','5'
  insert #SalesOrders select 'S004','SA004','CSTA001','Customer A - Local Delivery 001','3','PRDD004','Product D - Non-Serialised Goods 004','EACH','10'
  insert #SalesOrders select 'S004','SA004','CSTA001','Customer A - Local Delivery 001','4','PRDD005','Product D - Non-Serialised Goods 005','EACH','25'
  insert #SalesOrders select 'S005','SA005','CSTB002','Customer B - Courier Delivery 002','1','PRDB001','Product B - Non-Perishable Goods 001','EACH','500'
  insert #SalesOrders select 'S005','SA005','CSTB002','Customer B - Courier Delivery 002','2','PRDB003','Product B - Non-Perishable Goods 003','EACH','20'
  insert #SalesOrders select 'S005','SA005','CSTB002','Customer B - Courier Delivery 002','3','PRDB004','Product B - Non-Perishable Goods 004','EACH','10'
  insert #SalesOrders select 'S006','SA006','CSTA005','Customer A - Local Delivery 005','1','PRDB002','Product B - Non-Perishable Goods 002','EACH','24'
  insert #SalesOrders select 'S006','SA006','CSTA005','Customer A - Local Delivery 005','2','PRDB003','Product B - Non-Perishable Goods 003','EACH','24'
  insert #SalesOrders select 'S006','SA006','CSTA005','Customer A - Local Delivery 005','3','PRDB004','Product B - Non-Perishable Goods 004','EACH','6'
  insert #SalesOrders select 'S006','SA006','CSTA005','Customer A - Local Delivery 005','4','PRDB005','Product B - Non-Perishable Goods 005','EACH','48'
  insert #SalesOrders select 'S006','SA010','CSTA002','Customer A - Local Delivery 002','1','PRDB002','Product B - Non-Perishable Goods 002','EACH','12'
  insert #SalesOrders select 'S007','SA007','CSTC005','Customer C - Regionall Distribution 005','1','PRDA001','Product A - Perishable Goods 001','EACH','120'
  insert #SalesOrders select 'S007','SA007','CSTC005','Customer C - Regionall Distribution 005','2','PRDA002','Product A - Perishable Goods 002','EACH','120'
  insert #SalesOrders select 'S007','SA007','CSTC005','Customer C - Regionall Distribution 005','3','PRDA003','Product A - Perishable Goods 003','EACH','240'
  insert #SalesOrders select 'S007','SA007','CSTC005','Customer C - Regionall Distribution 005','4','PRDA004','Product A - Perishable Goods 004','EACH','240'
  insert #SalesOrders select 'S007','SA007','CSTC005','Customer C - Regionall Distribution 005','5','PRDA005','Product A - Perishable Goods 005','EACH','120'
  insert #SalesOrders select 'S008','SA008','CSTD003','Customer D - Export 003','1','PRDA001','Product A - Perishable Goods 001','EACH','200'
  insert #SalesOrders select 'S008','SA008','CSTD003','Customer D - Export 003','2','PRDA005','Product A - Perishable Goods 005','EACH','400'
  insert #SalesOrders select 'S009','SA009','CSTB003','Customer B - Courier Delivery 003','1','PRDD001','Product D - Non-Serialised Goods 001','EACH','25'
  insert #SalesOrders select 'S009','SA009','CSTB003','Customer B - Courier Delivery 003','2','PRDD002','Product D - Non-Serialised Goods 002','EACH','12'
  insert #SalesOrders select 'S009','SA009','CSTB003','Customer B - Courier Delivery 003','3','PRDD003','Product D - Non-Serialised Goods 003','EACH','5'
  insert #SalesOrders select 'S009','SA011','CSTB004','Customer B - Courier Delivery 004','1','PRDB002','Product B - Non-Perishable Goods 002','EACH','10'
  insert #SalesOrders select 'S009','SA011','CSTB004','Customer B - Courier Delivery 004','2','PRDB003','Product B - Non-Perishable Goods 003','EACH','100'
  insert #SalesOrders select 'S009','SA011','CSTB004','Customer B - Courier Delivery 004','3','PRDB004','Product B - Non-Perishable Goods 004','EACH','45'
  insert #SalesOrders select 'S010','SA012','CSTC002','Customer C - Regionall Distribution 002','1','PRDB002','Product B - Non-Perishable Goods 002','EACH','48'
  insert #SalesOrders select 'S010','SA012','CSTC002','Customer C - Regionall Distribution 002','2','PRDB003','Product B - Non-Perishable Goods 003','EACH','600'
  insert #SalesOrders select 'S010','SA012','CSTC002','Customer C - Regionall Distribution 002','3','PRDB004','Product B - Non-Perishable Goods 004','EACH','1200'
  insert #SalesOrders select 'S011','SA013','CSTC003','Customer C - Regionall Distribution 003','1','PRDD001','Product D - Non-Serialised Goods 001','EACH','120'
  insert #SalesOrders select 'S011','SA013','CSTC003','Customer C - Regionall Distribution 003','2','PRDD002','Product D - Non-Serialised Goods 002','EACH','600'
  insert #SalesOrders select 'S011','SA013','CSTC003','Customer C - Regionall Distribution 003','3','PRDD003','Product D - Non-Serialised Goods 003','EACH','480'
  insert #SalesOrders select 'S012','SA014','CSTA004','Customer A - Local Delivery 004','1','PRDD002','Product D - Non-Serialised Goods 002','EACH','5'
  insert #SalesOrders select 'S012','SA014','CSTA004','Customer A - Local Delivery 004','2','PRDD004','Product D - Non-Serialised Goods 004','EACH','15'
  insert #SalesOrders select 'S012','SA014','CSTA004','Customer A - Local Delivery 004','3','PRDD005','Product D - Non-Serialised Goods 005','EACH','5'
  insert #SalesOrders select 'S013','SA015','CSTA003','Customer A - Local Delivery 003','1','PRDC001','Product C - Serialised Goods 001','EACH','2'
  insert #SalesOrders select 'S013','SA015','CSTA003','Customer A - Local Delivery 003','2','PRDC002','Product C - Serialised Goods 002','EACH','1'
  insert #SalesOrders select 'S013','SA015','CSTA003','Customer A - Local Delivery 003','3','PRDC003','Product C - Serialised Goods 003','EACH','4'
  insert #SalesOrders select 'S013','SA015','CSTA003','Customer A - Local Delivery 003','4','PRDC004','Product C - Serialised Goods 004','EACH','5'
  insert #SalesOrders select 'S013','SA015','CSTA003','Customer A - Local Delivery 003','5','PRDC005','Product C - Serialised Goods 005','EACH','5'
  insert #SalesOrders select 'S014','SA016','CSTD004','Customer D - Export 004','1','PRDB002','Product B - Non-Perishable Goods 002','EACH','500'
  insert #SalesOrders select 'S014','SA016','CSTD004','Customer D - Export 004','2','PRDB003','Product B - Non-Perishable Goods 003','EACH','500'
  insert #SalesOrders select 'S014','SA016','CSTD004','Customer D - Export 004','3','PRDB001','Product B - Non-Perishable Goods 001','EACH','500'
  insert #SalesOrders select 'S014','SA016','CSTD004','Customer D - Export 004','4','PRDB004','Product B - Non-Perishable Goods 004','EACH','500'
  insert #SalesOrders select 'S015','SA017','CSTB001','Customer B - Courier Delivery 001','1','PRDB002','Product B - Non-Perishable Goods 002','EACH','312'
  insert #SalesOrders select 'S015','SA017','CSTB001','Customer B - Courier Delivery 001','2','PRDB003','Product B - Non-Perishable Goods 003','EACH','96'
  insert #SalesOrders select 'S015','SA017','CSTB001','Customer B - Courier Delivery 001','3','PRDB005','Product B - Non-Perishable Goods 005','EACH','48'
  insert #SalesOrders select 'S016','SA018','CSTB005','Customer B - Courier Delivery 005','1','PRDD001','Product D - Non-Serialised Goods 001','EACH','25'
  insert #SalesOrders select 'S016','SA018','CSTB005','Customer B - Courier Delivery 005','2','PRDD003','Product D - Non-Serialised Goods 003','EACH','25'
  insert #SalesOrders select 'S016','SA018','CSTB005','Customer B - Courier Delivery 005','3','PRDD004','Product D - Non-Serialised Goods 004','EACH','5'
  insert #SalesOrders select 'S016','SA018','CSTB005','Customer B - Courier Delivery 005','4','PRDD005','Product D - Non-Serialised Goods 005','EACH','10'
  insert #SalesOrders select 'S017','SA020','CSTD005','Customer D - Export 005','1','PRDB001','Product B - Non-Perishable Goods 001','EACH','1000'
  insert #SalesOrders select 'S017','SA020','CSTD005','Customer D - Export 005','2','PRDB003','Product B - Non-Perishable Goods 003','EACH','1000'

  insert InterfaceImportSOHeader
        (PrimaryKey,
         OrderNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Route,
         Remarks,
         NumberOfLines,
         DeliveryDate,
         Additional1,
         Additional2,
         Additional3)
  select distinct so.OrderNumber,
         so.OrderNumber,
         'SAL',--sm.DocumentType,
         'N',
         sm.CustomerCode,
         sm.Route,
         so.Remarks,
         sm.NumberOfLines,
         convert(datetime, sm.PlannedDeliveryDate),
         sm.Shipment,
         sm.Remakrs,
         sm.Priority
    from #SalesOrders so
    join #Shipment    sm on so.OrderNumber = sm.OrderNumber

  insert InterfaceImportSODetail
        (ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Quantity)
  select OrderNumber,
         LineNumber,
         ProducCode,
         Product,
         SKUCode,
         Quantity
    from #SalesOrders

  exec p_Pastel_Import_SO
  --select * from InterfaceMessage
  --select * from viewIssue
  SET IDENTITY_INSERT Georgia.dbo.OutboundShipment on
  insert OutboundShipment
        (OutboundShipmentId,
         StatusId,
         WarehouseId,
         LocationId,
         ShipmentDate,
         Remarks,
         SealNumber,
         VehicleRegistration,
         Route,
         RouteId,
         Weight,
         Volume,
         FP,
         SS,
         LP,
         FM,
         MP,
         PP,
         AlternatePallet,
         SubstitutePallet,
         DespatchBay)
  select distinct replace(Shipment, 'S',''),
         dbo.ufn_StatusId('IS','W'),
         1,
         (select min(LocationId) from viewLocation where AreaCode = 'CK'),
         min(PlannedDeliveryDate),
         min(Remakrs),
         null,
         null,
         min(Route),
         null,
         600,
         750000,
         1,
         1,
         0,
         1,
         1,
         1,
         0,
         0,
         (select min(LocationId) from viewLocation where AreaCode = 'D')
    from #Shipment
  group by replace(Shipment, 'S','')
  SET IDENTITY_INSERT Georgia.dbo.OutboundShipment off

  insert OutboundShipmentIssue
        (OutboundShipmentId,
         IssueId,
         DropSequence)
  select distinct replace(Shipment, 'S',''),
         vi.IssueId,
         vi.IssueId
    from #SalesOrders so
    join viewIssue   vi on so.OrderNumber = vi.OrderNumber


  select distinct Shipment from #Shipment

  delete LocationOperatorGroup

  insert LocationOperatorGroup
        (LocationId,
         OperatorGroupId,
         OrderBy)
  select l.LocationId,
         og.OperatorGroupId,
         0
    from Location       l,
         OperatorGroup og
   where not exists(select 1 from LocationOperatorGroup lg where l.LocationId = lg.LocationId and og.OperatorGroupId = lg.OperatorGroupId)

  delete AreaOperatorGroup

  insert AreaOperatorGroup
        (AreaId,
         OperatorGroupId)
  select l.AreaId,
         og.OperatorGroupId
    from Area       l,
         OperatorGroup og
   where not exists(select 1 from AreaOperatorGroup lg where l.AreaId = lg.AreaId and og.OperatorGroupId = lg.OperatorGroupId)

  delete OperatorGroupInstructionType

  insert OperatorGroupInstructionType
        (InstructionTypeId,
         OperatorGroupId,
         OrderBy)
  select it.InstructionTypeId,
         og.OperatorGroupId,
         0
    from InstructionType it,
         OperatorGroup og
   where not exists(select 1 from OperatorGroupInstructionType ogi where it.InstructionTypeId = ogi.InstructionTypeId and og.OperatorGroupId = ogi.OperatorGroupId)

  insert GroupPerformance
        (OperatorGroupId,
         PerformanceType,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         QA)
  select og.OperatorGroupId,
         gp.PerformanceType,
         gp.Units,
         gp.Weight,
         gp.Instructions,
         gp.Orders,
         gp.OrderLines,
         gp.Jobs,
         gp.QA
    from GroupPerformance gp,
         OperatorGroup    og
   where gp.OperatorGroupId = 5
     and not exists(select top 1 1 from GroupPerformance gp2 where og.OperatorGroupId = gp2.OperatorGroupId and gp.PerformanceType = gp2.PerformanceType)
  
  if @Error <> 0
    goto error
  
  create table #Batches
  (
   ProductCode     nvarchar(max),
   Product         nvarchar(max),
   Batch           nvarchar(max),
   CreateDate      nvarchar(max),
   ExpiryDate      nvarchar(max),
   ProductionDate  nvarchar(max),
   ShelfLifeDays   nvarchar(max),
   IncubationDays  nvarchar(max),
   Status          nvarchar(max)
  )
  
  insert #Batches select 'PRDA001','Product A - Perishable Goods 001','A0120110101','2011/01/01','2011/02/01','2010/12/20','0','5','Active'
  insert #Batches select 'PRDA001','Product A - Perishable Goods 001','A0120110102','2011/01/02','2011/02/02','2010/12/21','0','5','Active'
  insert #Batches select 'PRDA001','Product A - Perishable Goods 001','A0120110103','2011/01/03','2011/02/03','2010/12/22','0','5','Active'
  insert #Batches select 'PRDA001','Product A - Perishable Goods 001','A0120110104','2011/01/04','2011/02/04','2010/12/23','0','5','Active'
  insert #Batches select 'PRDA002','Product A - Perishable Goods 002','A0220110101','2011/01/01','2011/02/01','2010/12/20','0','5','Active'
  insert #Batches select 'PRDA002','Product A - Perishable Goods 002','A0220110102','2011/01/02','2011/02/02','2010/12/21','0','5','Active'
  insert #Batches select 'PRDA002','Product A - Perishable Goods 002','A0220110103','2011/01/03','2011/02/03','2010/12/22','0','5','Active'
  insert #Batches select 'PRDA002','Product A - Perishable Goods 002','A0220110104','2011/01/04','2011/02/04','2010/12/23','0','5','Active'
  insert #Batches select 'PRDA003','Product A - Perishable Goods 003','A0320110101','2011/01/01','2011/02/01','2010/12/20','0','0','Active'
  insert #Batches select 'PRDA003','Product A - Perishable Goods 003','A0320110102','2011/01/02','2011/02/02','2010/12/21','0','0','Active'
  insert #Batches select 'PRDA003','Product A - Perishable Goods 003','A0320110103','2011/01/03','2011/02/03','2010/12/22','0','0','Active'
  insert #Batches select 'PRDA003','Product A - Perishable Goods 003','A0320110104','2011/01/04','2011/02/04','2010/12/23','0','0','Active'
  insert #Batches select 'PRDA004','Product A - Perishable Goods 004','A0420110101','2011/01/01','2011/03/01','2010/12/20','0','0','Active'
  insert #Batches select 'PRDA004','Product A - Perishable Goods 004','A0420110102','2011/01/02','2011/03/02','2010/12/21','0','0','Active'
  insert #Batches select 'PRDA004','Product A - Perishable Goods 004','A0420110103','2011/01/03','2011/03/03','2010/12/22','0','0','Active'
  insert #Batches select 'PRDA004','Product A - Perishable Goods 004','A0420110104','2011/01/04','2011/03/04','2010/12/23','0','0','Active'
  insert #Batches select 'PRDA005','Product A - Perishable Goods 005','A0520110101','2011/01/01','2011/03/01','2010/12/20','0','0','Active'
  insert #Batches select 'PRDA005','Product A - Perishable Goods 005','A0520110102','2011/01/02','2011/03/02','2010/12/21','0','0','Active'
  insert #Batches select 'PRDA005','Product A - Perishable Goods 005','A0520110103','2011/01/03','2011/03/03','2010/12/22','0','0','Active'
  insert #Batches select 'PRDA005','Product A - Perishable Goods 005','A0520110104','2011/01/04','2011/03/04','2010/12/23','0','0','Active'
  insert #Batches select 'PRDA005','Product A - Perishable Goods 005','A0520110105','2011/01/05','2011/03/05','2010/12/24','0','0','Active'
  insert #Batches select 'PRDB001','Product B - Non-Perishable Goods 001','20110101B01','2011/01/01','0','','620','0','Active'
  insert #Batches select 'PRDB001','Product B - Non-Perishable Goods 001','20110102B01','2011/01/02','0','','620','0','Active'
  insert #Batches select 'PRDB001','Product B - Non-Perishable Goods 001','20110103B01','2011/01/03','0','','620','0','Active'
  insert #Batches select 'PRDB001','Product B - Non-Perishable Goods 001','20110104B01','2011/01/04','0','','620','0','Active'
  insert #Batches select 'PRDB002','Product B - Non-Perishable Goods 002','20110101B02','2011/01/01','0','','620','0','Active'
  insert #Batches select 'PRDB002','Product B - Non-Perishable Goods 002','20110102B02','2011/01/02','0','','620','0','Active'
  insert #Batches select 'PRDB002','Product B - Non-Perishable Goods 002','20110103B02','2011/01/03','0','','620','0','Active'
  insert #Batches select 'PRDB002','Product B - Non-Perishable Goods 002','20110104B02','2011/01/04','0','','620','0','Active'
  insert #Batches select 'PRDB003','Product B - Non-Perishable Goods 003','20110101B03','2011/01/01','0','','620','0','Active'
  insert #Batches select 'PRDB003','Product B - Non-Perishable Goods 003','20110102B03','2011/01/02','0','','620','0','Active'
  insert #Batches select 'PRDB003','Product B - Non-Perishable Goods 003','20110103B03','2011/01/03','0','','620','0','Active'
  insert #Batches select 'PRDB003','Product B - Non-Perishable Goods 003','20110104B03','2011/01/04','0','','620','0','Active'
  insert #Batches select 'PRDB004','Product B - Non-Perishable Goods 004','20110101B04','2011/01/01','0','','620','0','Active'
  insert #Batches select 'PRDB004','Product B - Non-Perishable Goods 004','20110102B04','2011/01/02','0','','620','0','Active'
  insert #Batches select 'PRDB004','Product B - Non-Perishable Goods 004','20110103B04','2011/01/03','0','','620','0','Active'
  insert #Batches select 'PRDB004','Product B - Non-Perishable Goods 004','20110104B04','2011/01/04','0','','620','0','Active'
  insert #Batches select 'PRDB005','Product B - Non-Perishable Goods 005','20110101B05','2011/01/01','0','','620','0','Active'
  insert #Batches select 'PRDB005','Product B - Non-Perishable Goods 005','20110102B05','2011/01/02','0','','620','0','Active'
  insert #Batches select 'PRDB005','Product B - Non-Perishable Goods 005','20110103B05','2011/01/03','0','','620','0','Active'
  insert #Batches select 'PRDB005','Product B - Non-Perishable Goods 005','20110104B05','2011/01/04','0','','620','0','Active'
  insert #Batches select 'PRDB005','Product B - Non-Perishable Goods 005','20110105B05','2011/01/05','0','','620','0','Active'
  insert #Batches select 'PRDC001','Product C - Serialised Goods 001','SNC0120110101S001','2011/01/01','0','2010/12/20','','0','Active'
  insert #Batches select 'PRDC001','Product C - Serialised Goods 001','SNC0120110102S002','2011/01/02','0','2010/12/21','','0','Active'
  insert #Batches select 'PRDC002','Product C - Serialised Goods 002','SNC0220110101S001','2011/01/03','0','2010/12/22','','0','Active'
  insert #Batches select 'PRDC002','Product C - Serialised Goods 002','SNC0220110102S002','2011/01/04','0','2010/12/23','','0','Active'
  insert #Batches select 'PRDC003','Product C - Serialised Goods 003','SNC0320110101S001','2011/01/01','0','2010/12/20','','0','Active'
  insert #Batches select 'PRDC003','Product C - Serialised Goods 003','SNC0320110102S002','2011/01/02','0','2010/12/21','','0','Active'
  insert #Batches select 'PRDC004','Product C - Serialised Goods 004','SNC0420110101S001','2011/01/03','0','2010/12/22','','0','Active'
  insert #Batches select 'PRDC004','Product C - Serialised Goods 004','SNC0420110102S002','2011/01/04','0','2010/12/23','','0','Active'
  insert #Batches select 'PRDC005','Product C - Serialised Goods 005','SNC0520110101S001','2011/01/01','0','2010/12/20','','0','Active'
  insert #Batches select 'PRDC005','Product C - Serialised Goods 005','SNC0520110102S002','2011/01/02','0','2010/12/21','','0','Active'
  insert #Batches select 'PRDD001','Product D - Non-Serialised Goods 001','20110103D01','2011/01/03','0','2010/12/22','','0','Active'
  insert #Batches select 'PRDD001','Product D - Non-Serialised Goods 001','20110104D01','2011/01/04','0','2010/12/23','','0','Active'
  insert #Batches select 'PRDD002','Product D - Non-Serialised Goods 002','20110101D02','2011/01/01','0','2010/12/20','','0','Active'
  insert #Batches select 'PRDD002','Product D - Non-Serialised Goods 002','20110102D02','2011/01/02','0','2010/12/21','','0','Active'
  insert #Batches select 'PRDD003','Product D - Non-Serialised Goods 003','20110101D03','2011/01/03','0','2010/12/22','','0','Active'
  insert #Batches select 'PRDD003','Product D - Non-Serialised Goods 003','20110102D03','2011/01/04','0','2010/12/23','','0','Active'
  insert #Batches select 'PRDD004','Product D - Non-Serialised Goods 004','20110101D04','2011/01/01','0','2010/12/20','','0','Active'
  insert #Batches select 'PRDD004','Product D - Non-Serialised Goods 004','20110102D04','2011/01/02','0','2010/12/21','','0','Active'
  insert #Batches select 'PRDD005','Product D - Non-Serialised Goods 005','20110103D05','2011/01/03','0','2010/12/22','','0','Active'
  insert #Batches select 'PRDD005','Product D - Non-Serialised Goods 005','20110104D05','2011/01/04','0','2010/12/23','','0','Active'
  
  update #Batches set CreateDate = null where isdate(CreateDate) = 0
  update #Batches set ExpiryDate = null where isdate(ExpiryDate) = 0
  update #Batches set ProductionDate = null where isdate(ProductionDate) = 0
  
  insert Batch
        (StatusId,
         WarehouseId,
         Batch,
         CreateDate,
         ExpiryDate,
         IncubationDays,
         ShelfLifeDays,
         ProductionDateTime)
  select dbo.ufn_StatusId('B','A'),
         1,
         Batch,
         CreateDate,
         ExpiryDate,
         IncubationDays,
         ShelfLifeDays,
         ProductionDate
    from #Batches
  
  insert StorageUnitBatch
        (BatchId,
         StorageUnitId)
  select b.BatchId,
         su.StorageUnitId
    from #Batches t
    join Batch    b on t.Batch = b.Batch
    join viewProduct su on t.ProductCode = su.ProductCode
  
  create table #SOH
  (
   LocationType    nvarchar(max),
   Location        nvarchar(max),
   ProductCode     nvarchar(max),
   Product         nvarchar(max),
   Batch           nvarchar(max),
   ActualQuantity  nvarchar(max)
  )
  
  insert #SOH select 'Bulk','W01BL01','','','',''
  insert #SOH select 'Bulk','W01BL02','','','',''
  insert #SOH select 'Bulk','W01BL03','','','',''
  insert #SOH select 'Bulk','W01BL04','PRDA001','Product A - Perishable Goods 001','A0120110103','120'
  insert #SOH select 'Bulk','W01BL05','PRDA001','Product A - Perishable Goods 001','A0120110104','120'
  insert #SOH select 'Bulk','W02BL01','PRDA002','Product A - Perishable Goods 002','A0220110101','120'
  insert #SOH select 'Bulk','W02BL02','PRDA002','Product A - Perishable Goods 002','A0220110102','120'
  insert #SOH select 'Bulk','W02BL03','PRDA002','Product A - Perishable Goods 002','A0220110103','120'
  insert #SOH select 'Bulk','W02BL04','PRDA002','Product A - Perishable Goods 002','A0220110104','120'
  insert #SOH select 'Bulk','W02BL05','','','',''
  insert #SOH select 'Racking','W02A01B','PRDA003','Product A - Perishable Goods 003','A0320110104','120'
  insert #SOH select 'Racking','W02A01C','PRDA004','Product A - Perishable Goods 004','A0420110101','120'
  insert #SOH select 'Racking','W02B01B','PRDA004','Product A - Perishable Goods 004','A0420110102','120'
  insert #SOH select 'Racking','W02B01C','PRDA004','Product A - Perishable Goods 004','A0420110103','120'
  insert #SOH select 'Racking','W02A02B','PRDA004','Product A - Perishable Goods 004','A0420110104','120'
  insert #SOH select 'Racking','W02A02C','PRDA005','Product A - Perishable Goods 005','A0520110101','120'
  insert #SOH select 'Racking','W02B02B','PRDA005','Product A - Perishable Goods 005','A0520110102','120'
  insert #SOH select 'Racking','W02B02C','PRDA005','Product A - Perishable Goods 005','A0520110103','120'
  insert #SOH select 'Racking','W02A03B','PRDA005','Product A - Perishable Goods 005','A0520110104','120'
  insert #SOH select 'Racking','W02A03C','PRDA005','Product A - Perishable Goods 005','A0520110105','120'
  insert #SOH select 'Racking','W02B03B','PRDB001','Product B - Non-Perishable Goods 001','20110101B01','600'
  insert #SOH select 'Racking','W02B03C','PRDB001','Product B - Non-Perishable Goods 001','20110102B01','600'
  insert #SOH select 'Racking','W02A04B','PRDB001','Product B - Non-Perishable Goods 001','20110103B01','600'
  insert #SOH select 'Racking','W02A04C','PRDB001','Product B - Non-Perishable Goods 001','20110104B01','600'
  insert #SOH select 'Racking','W02B04B','PRDB002','Product B - Non-Perishable Goods 002','20110101B02','600'
  insert #SOH select 'Racking','W02B04C','PRDB002','Product B - Non-Perishable Goods 002','20110102B02','600'
  insert #SOH select 'Racking','W02A05B','PRDB002','Product B - Non-Perishable Goods 002','20110103B02','600'
  insert #SOH select 'Racking','W02A05C','PRDB002','Product B - Non-Perishable Goods 002','20110104B02','600'
  insert #SOH select 'Racking','W02B05B','PRDB003','Product B - Non-Perishable Goods 003','20110101B03','600'
  insert #SOH select 'Racking','W02B05C','PRDB003','Product B - Non-Perishable Goods 003','20110102B03','600'
  insert #SOH select 'Racking','W02A06B','PRDB003','Product B - Non-Perishable Goods 003','20110103B03','600'
  insert #SOH select 'Racking','W02A06C','PRDB003','Product B - Non-Perishable Goods 003','20110104B03','600'
  insert #SOH select 'Racking','W02B06B','PRDB004','Product B - Non-Perishable Goods 004','20110101B04','600'
  insert #SOH select 'Racking','W02B06C','PRDB004','Product B - Non-Perishable Goods 004','20110102B04','600'
  insert #SOH select 'Racking','W02A07B','PRDB004','Product B - Non-Perishable Goods 004','20110103B04','600'
  insert #SOH select 'Racking','W02A07C','PRDB004','Product B - Non-Perishable Goods 004','20110104B04','600'
  insert #SOH select 'Racking','W02B07B','PRDB005','Product B - Non-Perishable Goods 005','20110101B05','600'
  insert #SOH select 'Racking','W02B07C','PRDB005','Product B - Non-Perishable Goods 005','20110102B05','600'
  insert #SOH select 'Racking','W02A08B','PRDB005','Product B - Non-Perishable Goods 005','20110103B05','600'
  insert #SOH select 'Racking','W02A08C','PRDB005','Product B - Non-Perishable Goods 005','20110104B05','600'
  insert #SOH select 'Racking','W02B08B','PRDB005','Product B - Non-Perishable Goods 005','','600'
  insert #SOH select 'Racking','W02B08C','PRDC001','Product C - Serialised Goods 001','SNC0120110101S001','25'
  insert #SOH select 'Racking','W02A09B','PRDC001','Product C - Serialised Goods 001','SNC0120110102S002','25'
  insert #SOH select 'Racking','W02A09C','PRDC002','Product C - Serialised Goods 002','SNC0220110101S001','25'
  insert #SOH select 'Racking','W02B09B','PRDC002','Product C - Serialised Goods 002','SNC0220110102S002','25'
  insert #SOH select 'Racking','W02B09C','PRDC003','Product C - Serialised Goods 003','SNC0320110101S001','50'
  insert #SOH select 'Racking','W02A10B','PRDC003','Product C - Serialised Goods 003','SNC0320110102S002','50'
  insert #SOH select 'Racking','W02A10C','PRDC004','Product C - Serialised Goods 004','SNC0420110101S001','50'
  insert #SOH select 'Racking','W02B10B','PRDC004','Product C - Serialised Goods 004','SNC0420110102S002','50'
  insert #SOH select 'Racking','W02B10C','PRDC005','Product C - Serialised Goods 005','SNC0520110101S001','50'
  insert #SOH select 'Racking','W02C01B','PRDC005','Product C - Serialised Goods 005','SNC0520110102S002','50'
  insert #SOH select 'Racking','W02C01C','','','',''
  insert #SOH select 'Racking','W02D01B','PRDD001','Product D - Non-Serialised Goods 001','20110103D01','400'
  insert #SOH select 'Racking','W02D01C','PRDD001','Product D - Non-Serialised Goods 001','20110104D01','400'
  insert #SOH select 'Racking','W02C02C','','','',''
  insert #SOH select 'Racking','W02D02B','PRDD002','Product D - Non-Serialised Goods 002','20110101D02','400'
  insert #SOH select 'Racking','W02D02C','PRDD002','Product D - Non-Serialised Goods 002','20110102D02','400'
  insert #SOH select 'Racking','W02C03B','','','',''
  insert #SOH select 'Racking','W02C03C','PRDD003','Product D - Non-Serialised Goods 003','20110101D03','400'
  insert #SOH select 'Racking','W02D03B','PRDD003','Product D - Non-Serialised Goods 003','20110102D03','400'
  insert #SOH select 'Racking','W02D03C','','','',''
  insert #SOH select 'Racking','W02C04B','PRDD004','Product D - Non-Serialised Goods 004','20110101D04','400'
  insert #SOH select 'Racking','W02C04C','PRDD004','Product D - Non-Serialised Goods 004','20110102D04','400'
  insert #SOH select 'Racking','W02D04B','','','',''
  insert #SOH select 'Racking','W02D04C','PRDD005','Product D - Non-Serialised Goods 005','20110103D05','400'
  insert #SOH select 'Racking','W02C05B','PRDD005','Product D - Non-Serialised Goods 005','20110104D05','400'
  insert #SOH select 'Racking','W02C05C','','','',''
  insert #SOH select 'Racking','W02D05B','','','',''
  insert #SOH select 'Racking','W02D05C','','','',''
  insert #SOH select 'Racking','W02C06B','PRDA003','Product A - Perishable Goods 003','A0320110101','120'
  insert #SOH select 'Racking','W02C06C','PRDA003','Product A - Perishable Goods 003','A0320110102','120'
  insert #SOH select 'Racking','W02D06B','PRDA003','Product A - Perishable Goods 003','A0320110103','120'
  insert #SOH select 'Racking','W02D06C','','','',''
  insert #SOH select 'Racking','W02C02B','','','',''
  insert #SOH select 'Racking','W02C07B','PRDA001','Product A - Perishable Goods 001','A0120110101','120'
  insert #SOH select 'Racking','W02C07C','PRDA001','Product A - Perishable Goods 001','A0120110102','120'
  insert #SOH select 'Racking','W02D07B','','','',''
  insert #SOH select 'Racking','W02D07C','','','',''
  insert #SOH select 'Racking','W02C08B','','','',''
  insert #SOH select 'Racking','W02C08C','','','',''
  insert #SOH select 'Racking','W02D08B','','','',''
  insert #SOH select 'Racking','W02D08C','','','',''
  insert #SOH select 'Racking','W02C09B','','','',''
  insert #SOH select 'Racking','W02C09C','','','',''
  insert #SOH select 'Racking','W02D09B','','','',''
  insert #SOH select 'Racking','W02D09C','','','',''
  insert #SOH select 'Racking','W02C10B','','','',''
  insert #SOH select 'Racking','W02C10C','','','',''
  insert #SOH select 'Racking','W02D10B','','','',''
  insert #SOH select 'Racking','W02D10C','','','',''
  insert #SOH select 'Pick-face','W02A01A','PRDA001','Product A - Perishable Goods 001','A0120110101','10'
  insert #SOH select 'Pick-face','W02A02A','PRDA002','Product A - Perishable Goods 002','A0220110101','100'
  insert #SOH select 'Pick-face','W02A03A','PRDA003','Product A - Perishable Goods 003','A0320110101','120'
  insert #SOH select 'Pick-face','W02A04A','','','',''
  insert #SOH select 'Pick-face','W02A05A','','','',''
  insert #SOH select 'Pick-face','W02A06A','','','',''
  insert #SOH select 'Pick-face','W02A07A','','','',''
  insert #SOH select 'Pick-face','W02A08A','','','',''
  insert #SOH select 'Pick-face','W02A09A','','','',''
  insert #SOH select 'Pick-face','W02A10A','','','',''
  insert #SOH select 'Pick-face','W02B01A','PRDA004','Product A - Perishable Goods 004','A0420110102','10'
  insert #SOH select 'Pick-face','W02B02A','PRDA004','Product A - Perishable Goods 004','A0420110103','120'
  insert #SOH select 'Pick-face','W02B03A','PRDA005','Product A - Perishable Goods 005','A0520110101','10'
  insert #SOH select 'Pick-face','W02B04A','PRDA005','Product A - Perishable Goods 005','A0520110102','120'
  insert #SOH select 'Pick-face','W02B05A','','','',''
  insert #SOH select 'Pick-face','W02B06A','','','',''
  insert #SOH select 'Pick-face','W02B07A','','','',''
  insert #SOH select 'Pick-face','W02B08A','','','',''
  insert #SOH select 'Pick-face','W02B09A','','','',''
  insert #SOH select 'Pick-face','W02B10A','','','',''
  insert #SOH select 'Pick-face','W02C01A','PRDB001','Product B - Non-Perishable Goods 001','20110101B01','100'
  insert #SOH select 'Pick-face','W02C02A','PRDB002','Product B - Non-Perishable Goods 002','20110101B02','300'
  insert #SOH select 'Pick-face','W02C03A','PRDB003','Product B - Non-Perishable Goods 003','20110101B03','600'
  insert #SOH select 'Pick-face','W02C04A','','','',''
  insert #SOH select 'Pick-face','W02C05A','','','',''
  insert #SOH select 'Pick-face','W02C06A','','','',''
  insert #SOH select 'Pick-face','W02C07A','','','',''
  insert #SOH select 'Pick-face','W02C08A','','','',''
  insert #SOH select 'Pick-face','W02C09A','','','',''
  insert #SOH select 'Pick-face','W02C10A','','','',''
  insert #SOH select 'Pick-face','W02D01A','PRDB004','Product B - Non-Perishable Goods 004','20110101B04','100'
  insert #SOH select 'Pick-face','W02D02A','PRDB004','Product B - Non-Perishable Goods 004','20110102B04','600'
  insert #SOH select 'Pick-face','W02D03A','PRDB005','Product B - Non-Perishable Goods 005','20110101B05','300'
  insert #SOH select 'Pick-face','W02D04A','PRDB005','Product B - Non-Perishable Goods 005','20110102B05','300'
  insert #SOH select 'Pick-face','W02D05A','','','',''
  insert #SOH select 'Pick-face','W02D06A','','','',''
  insert #SOH select 'Pick-face','W02D07A','','','',''
  insert #SOH select 'Pick-face','W02D08A','','','',''
  insert #SOH select 'Pick-face','W02D09A','','','',''
  insert #SOH select 'Pick-face','W02D10A','','','',''
  insert #SOH select 'Pick-face','W04A01A','PRDC001','Product C - Serialised Goods 001','SNC0120110101S001','1'
  insert #SOH select 'Pick-face','W04A01A','PRDC001','Product C - Serialised Goods 001','SNC0120110102S002','1'
  insert #SOH select 'Pick-face','W04A02A','PRDC002','Product C - Serialised Goods 002','SNC0220110101S001','1'
  insert #SOH select 'Pick-face','W04A02A','PRDC002','Product C - Serialised Goods 002','SNC0220110102S002','1'
  insert #SOH select 'Pick-face','W04A03A','PRDC003','Product C - Serialised Goods 003','SNC0320110101S001','1'
  insert #SOH select 'Pick-face','W04A03A','PRDC003','Product C - Serialised Goods 003','SNC0320110102S002','1'
  insert #SOH select 'Pick-face','W04A04A','PRDC004','Product C - Serialised Goods 004','SNC0420110101S001','1'
  insert #SOH select 'Pick-face','W04A04A','PRDC004','Product C - Serialised Goods 004','SNC0420110102S002','1'
  insert #SOH select 'Pick-face','W04A05A','PRDC005','Product C - Serialised Goods 005','SNC0520110101S001','1'
  insert #SOH select 'Pick-face','W04A05A','PRDC005','Product C - Serialised Goods 005','SNC0520110102S002','1'
  insert #SOH select 'Pick-face','W04A06A','PRDD001','Product D - Non-Serialised Goods 001','20110103D01','100'
  insert #SOH select 'Pick-face','W04A07A','PRDD002','Product D - Non-Serialised Goods 002','20110101D02','400'
  insert #SOH select 'Pick-face','W04A08A','PRDD003','Product D - Non-Serialised Goods 003','20110101D03','200'
  insert #SOH select 'Pick-face','W04A09A','PRDD004','Product D - Non-Serialised Goods 004','20110101D04','100'
  insert #SOH select 'Pick-face','W04A10A','PRDD005','Product D - Non-Serialised Goods 005','20110103D05','300'
  insert #SOH select 'Receiving','W01RC01','','','',''
  insert #SOH select 'Receiving','W01RC02','','','',''
  insert #SOH select 'Returns','W01RT01','','','',''
  insert #SOH select 'Damages','W01DM01','','','',''
  insert #SOH select 'Checking','W01CH01','','','',''
  insert #SOH select 'Checking','W01CH02','','','',''
  insert #SOH select 'Checking','W01CH03','','','',''
  insert #SOH select 'Dispatch','W05DS01','','','',''
  insert #SOH select 'Dispatch','W05DS02','','','',''
  insert #SOH select 'Dispatch','W05DS03','','','',''
  
  insert StorageUnitBatchLocation
        (StorageUnitBatchId,
         LocationId,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity)
  select StorageUnitBatchId,
         LocationId,
         ActualQuantity,
         0,
         0
    from #SOH soh
    join viewLocation vl on soh.Location    = vl.Location
    join viewStock    vs on soh.ProductCode = vs.ProductCode
                        and soh.Batch       = vs.Batch
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Demo_Reset'
    rollback transaction
    return @Error
end
