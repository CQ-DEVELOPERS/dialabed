﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackExternalCompany_Delete
  ///   Filename       : p_PackExternalCompany_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 21:17:04
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the PackExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StorageUnitId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackExternalCompany_Delete
(
 @WarehouseId int = null,
 @ExternalCompanyId int = null,
 @StorageUnitId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete PackExternalCompany
     where WarehouseId = @WarehouseId
       and ExternalCompanyId = @ExternalCompanyId
       and StorageUnitId = @StorageUnitId
  
  select @Error = @@Error
  
  
  return @Error
  
end
