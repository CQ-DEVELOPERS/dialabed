﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Station_Sheet_Receiving_Document_Search
  ///   Filename       : p_Station_Sheet_Receiving_Document_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Station_Sheet_Receiving_Document_Search
(
 @WarehouseId           int,
 @InboundDocumentTypeId	int,
 @InboundShipmentId     int,
 @ExternalCompanyCode		 varchar(30),
 @ExternalCompany	     	varchar(255),
 @OrderNumber	          varchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime,
 @ProductCode											varchar(30),
 @Batch			      								varchar(50)
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   InboundDocumentId         int,
   ReceiptId                 int,
   OrderNumber               varchar(30),
   ReferenceNumber           varchar(30),
   InboundShipmentId         int,
   SupplierCode              varchar(30),
   Supplier                  varchar(255),
   NumberOfLines             int,
   DeliveryDate              datetime,
   PlannedDeliveryDate       datetime,
   CreateDate                datetime,
   StatusId                  int,
   Status                    varchar(50),
   InboundDocumentType       varchar(30),
   InboundDocumentTypeCode   varchar(10),
   LocationId                int,
   Location                  varchar(15),
   Rating                    int,
   Delivery                  int,
   DeliveryNoteNumber		      varchar(30),
   SealNumber				            varchar(30),
   PriorityID				            int,
   Priority				              varchar(50),
   VehicleRegistration							varchar(10),
   Remarks					              varchar(250),
   AllowPalletise			         bit,
   ProductCode				           varchar(30),
   Product					              varchar(50),
   Batch					                varchar(50),
   RequiredQuantity		        float,
   Dwelltime				             int,
   AvailabilityIndicator     varchar(20),
   InterfaceStatus			        varchar(20)
  );
  
  if @InboundDocumentTypeId = -1
    set @InboundDocumentTypeId = null
  
  if @ExternalCompanyCode is null
    set @ExternalCompanyCode = ''
   
  if @ExternalCompany is null
    set @ExternalCompany = ''
  
  insert @TableResult
        (InboundDocumentId,
         ReceiptId,
         LocationId,
         OrderNumber,
         ReferenceNumber,
         SupplierCode,
         Supplier,
         StatusId,
         Status,
         DeliveryDate,
         PlannedDeliveryDate,
         CreateDate,
         InboundDocumentType,
         InboundDocumentTypeCode,
         Rating,
         Delivery,
         DeliveryNoteNumber,	
         SealNumber,			
         PriorityID,						
         VehicleRegistration,	
         Remarks,
         AllowPalletise,
         ProductCode,
         Product,
         Batch,
         InterfaceStatus)
  select id.InboundDocumentId,
         r.ReceiptId,
         r.LocationId,
         id.OrderNumber,
         id.ReferenceNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         r.StatusId,
         s.Status,
         r.DeliveryDate,
         id.DeliveryDate,
         id.CreateDate,
         idt.InboundDocumentType,
         idt.InboundDocumentTypeCode,
         ec.Rating,
         r.Delivery,
         r.DeliveryNoteNumber,	
         r.SealNumber,			
         r.PriorityID,						
         r.VehicleRegistration,	
         r.Remarks,
         r.AllowPalletise,
         p.ProductCode,
         p.Product,
         ba.Batch,
         Case when r.Interfaced = 1 then 'Posted' else 'Waiting' end
    from InboundDocument     id  (nolock)
    left
    join StorageUnit         su  (nolock) on id.StorageUnitId	        = su.StorageUnitId
    left
    join Product             p   (nolock) on su.ProductId		           = p.ProductId
                                         and p.ProductCode         like isnull(@ProductCode  + '%', p.ProductCode)
    join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
    join Status              s   (nolock) on r.StatusId               = s.StatusId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    left outer
    join ExternalCompany     ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
    left
    join Batch               ba  (nolock) on id.BatchId				  = ba.BatchId
   where id.WarehouseId                       = @WarehouseId
     and id.InboundDocumentTypeId              = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and isnull(ec.ExternalCompanyCode,1)   like isnull(@ExternalCompanyCode + '%', isnull(ec.ExternalCompanyCode,1))
     and isnull(ec.ExternalCompany,1)       like isnull(@ExternalCompany + '%', isnull(ec.ExternalCompany,1))
     and id.OrderNumber                     like isnull(@OrderNumber  + '%', id.OrderNumber)
     and s.Type                                = 'R'
     and s.StatusCode                         in ('W','C','D','S','P','R','LA','OH') -- Waiting, Confirmed, Delivered, Started, Palletised, Received, Locations Allocated, On Hold
     and idt.InboundDocumentTypeCode          in ('PRV','MO','KIT')
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
  
  update r
     set InboundShipmentId = isr.InboundShipmentId
    from @TableResult             r
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from ReceiptLine rl
                           where t.ReceiptId = rl.ReceiptId)
    from @TableResult t
    
  update r
     set Location = l.Location
    from @TableResult r
    join Location     l (nolock) on r.LocationId = l.LocationId
	
  update r
     set Priority = P.Priority
    from @TableResult r
    join Priority    P (nolock) on r.PriorityId = P.PriorityID  
    
  update tr
     set AvailabilityIndicator = 'Red'
    from @TableResult tr
    
  update tr
     set AvailabilityIndicator = 'Red'
    from @TableResult tr
   where (datediff(hh, GETDATE(), PlannedDeliveryDate)) > -24
  
  update tr
     set AvailabilityIndicator = 'Yellow'
    from @TableResult tr
   where datediff(hh, GETDATE(), PlannedDeliveryDate) between -24 and 12
  
  update tr
     set AvailabilityIndicator = 'Green'
    from @TableResult tr
   where datediff(HH, GETDATE(), PlannedDeliveryDate) > 12
  
  select isnull(InboundShipmentId,-1) as 'InboundShipmentId',
         ReceiptId,
         OrderNumber,
         SupplierCode,
         Supplier,
         NumberOfLines,
         DeliveryDate,
         PlannedDeliveryDate,
         CreateDate,
         Status,
         InboundDocumentType,
         isnull(LocationId,-1) as LocationId,	
         Location,
         Rating,
         Delivery,
         DeliveryNoteNumber,	
         SealNumber,			
         PriorityID,
         Priority,						
         VehicleRegistration,	
         Remarks,
         isnull(AllowPalletise,1) as 'AllowPalletise',
         OrderNumber as 'ProductCode',
         Product,
         Batch,
         RequiredQuantity,
         Dwelltime,
         AvailabilityIndicator,
         InterfaceStatus
    from @TableResult
end
