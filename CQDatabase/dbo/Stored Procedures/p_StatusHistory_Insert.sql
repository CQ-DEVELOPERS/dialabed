﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StatusHistory_Insert
  ///   Filename       : p_StatusHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:55
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StatusHistory table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null,
  ///   @Status nvarchar(100) = null,
  ///   @StatusCode nvarchar(20) = null,
  ///   @Type char(2) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  ///   StatusHistory.StatusId,
  ///   StatusHistory.Status,
  ///   StatusHistory.StatusCode,
  ///   StatusHistory.Type,
  ///   StatusHistory.CommandType,
  ///   StatusHistory.InsertDate,
  ///   StatusHistory.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StatusHistory_Insert
(
 @StatusId int = null,
 @Status nvarchar(100) = null,
 @StatusCode nvarchar(20) = null,
 @Type char(2) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @OrderBy int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StatusHistory
        (StatusId,
         Status,
         StatusCode,
         Type,
         CommandType,
         InsertDate,
         OrderBy)
  select @StatusId,
         @Status,
         @StatusCode,
         @Type,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @OrderBy 
  
  select @Error = @@Error
  
  
  return @Error
  
end
