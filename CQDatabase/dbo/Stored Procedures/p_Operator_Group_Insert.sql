﻿/*
  /// <summary>
  ///   Procedure Name : p_Operator_Group_Insert
  ///   Filename       : p_Operator_Group_Insert.sql
  ///   Create By      : William
  ///   Date Created   : 24 July 2008
  /// </summary>
  /// <remarks>
  ///   Inserts New Groups and Initialises Menu
  /// </remarks>
  /// 
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Description
  ///   Default2
  ///   
  /// </newpara>
*/
create procedure dbo.p_Operator_Group_Insert(@OperatorGroupDesc nvarchar(40) = null,
													@SourceOperatorGroupId int = null)
as
begin
	
	declare @RestrictedAreaIndicator	bit,
			@OperatorGroupId			int,
			@Error						int,
			@Errormsg           nvarchar(500)
	
	begin transaction
	set @Error = 0
	If len(Isnull(@OperatorGroupDesc,'')) = 0 
	Begin
		set @Error = 900040
		set @Errormsg = 'No Description Passed'
		Goto error
	End
	
	if Exists(Select top 1 1 from OperatorGroup where OperatorGroup = @OperatorGroupDesc)
	Begin
		set @Error = 900041
		set @Errormsg = 'Already Exists'
		Goto error
	End


	------------------------------------------
	--	User has Passed a Valid Description 
	--  That Doesnt exist already
	------------------------------------------
	if exists(Select top 1 1 from OperatorGroup where OperatorGroupId = @SourceOperatorGroupId)
			Begin
				-------------------------------------------------
				--	User has Passed a Valid Source Operator Group
				-------------------------------------------------
				Select @RestrictedAreaIndicator=RestrictedAreaIndicator from OperatorGroup where OperatorGroupId = @SourceOperatorGroupId
				Insert into OperatorGroup(OperatorGroup,RestrictedAreaIndicator) Values (@OperatorGroupDesc,@RestrictedAreaIndicator)

				Select @OperatorGroupId = OperatorGroupId
					from OperatorGroup
				where OperatorGroup = @OperatorGroupDesc

				Insert into OperatorGroupMenuItem (OperatorGroupId,MenuItemId,Access)
					Select @OperatorGroupId,MenuItemId,Access
						From OperatorGroupMenuItem
					where OperatorGroupId = @SourceOperatorGroupId
	
			End
	Else
			Begin
				----------------------------------------------------------------
				-- Invalid or No Source Operator Group - Create with no Access
				----------------------------------------------------------------
				Insert into OperatorGroup(OperatorGroup,RestrictedAreaIndicator) Values (@OperatorGroupDesc,0)
				Select @OperatorGroupId = OperatorGroupId
					from OperatorGroup
				where OperatorGroup = @OperatorGroupDesc

				Insert into OperatorGroupMenuItem (OperatorGroupId,MenuItemId,Access)
				Select distinct @OperatorGroupId,MenuItemId,0 From MenuItem
				
			End

  commit transaction
  select @Error
  return @Error
  
  error:
    rollback transaction
    select @Error as Error,
			@Errormsg as ErrorMsg
    return @Error

end
