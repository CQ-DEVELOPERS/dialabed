﻿CREATE Procedure dbo.p_Pastel_Import_BOM
as
Begin
    Declare -- Header
           @InterfaceImportBOMId   int,
           @ProductCode             varchar(255),
           @Product                 varchar(255),
           @PrimaryKey              varchar(255),
           @HostId                  Int,
           @HeaderFetchStatus       int,
           @DetailFetchStatus       int,
           @Quantity                Float,
           -- Detail
           @Compindex               Int,
           @CompProductCode         Varchar(255),
           @CompProduct             Varchar(255),
           @CompQuantity            Float,
           @ProductId               Int,
           @StorageUnitId           Int,
           @BOMHeaderId             Int,
           @BOMLineId               Int,
           @ErrorMsg                varchar(255)

    Declare Header_Cursor Cursor for 
    Select InterfaceImportBOMId,
           ProductCode,
           Product,
           PrimaryKey,
           HostId ,
           Quantity
    from InterfaceImportBOM
    Where RecordStatus = 'N'
      and ProcessedDate is null
    
    Open Header_Cursor
    Fetch Header_Cursor 
    into @InterfaceImportBOMId,
         @ProductCode,
         @Product,
         @PrimaryKey,
         @HostId,
         @Quantity
    Set @HeaderFetchStatus = @@fetch_status
    
    While @HeaderFetchStatus = 0
    Begin
        set @StorageUnitId = null
        set @ProductId = null
        
        If Exists (Select top 1 1 from BOMHeader (NoLock) where HostId = @HostId)
        Begin
            -- Update
            Select @BOMHeaderId = BOMHeaderid from BOMHeader where HostId = @HostId
            Select @ProductId = ProductId from Product where ProductCode = @ProductCode
            Select @StorageUnitId = StorageUnitId from StorageUnit where ProductId = @ProductId
            update BOMHeader set StorageUnitId = @StorageUnitId, Quantity = @Quantity where HostId = @HostId
            
            --clear removed lines
            
            delete bp
            from BOMProduct bp
            Inner Join BOMLine bl on bp.BOMLineId = bl.BOMLineId
            Inner Join viewProduct vp on bp.StorageUnitId = vp.StorageUnitId
            Where bl.BOMHeaderId = @BOMHeaderId
            And not exists(select 1 From InterfaceImportBOMDetail bd 
                            Where bd.InterfaceImportBOMId = @InterfaceImportBOMId
                            And bd.CompProductCode = vp.ProductCode)
            
            delete 
            from BOMLine
            Where BOMHeaderId = @BOMHeaderId
            And not exists (select 1 from BOMProduct bp
                             where bp.BOMLineId = BOMLineId)
                             
            --Update changes
            Update bp
            Set Quantity = CompQuantity
            From BOMProduct bp
            Inner Join BOMLine bl on bp.BOMLineId = bl.BOMLineId
                                    and bp.LineNumber = 1
            Inner Join viewProduct p on bp.StorageUnitId = p.StorageUnitId
            Inner Join InterfaceImportBOMDetail bd on p.ProductCode = bd.CompProductCode
            Where bd.InterfaceImportBOMId = @InterfaceImportBOMId
            and bl.BOMHeaderId = @BOMHeaderId
            and bp.Quantity <> bd.CompQuantity
                
        End
        Else
        Begin
            --Insert
            Select @ProductId = ProductId from Product where ProductCode = @ProductCode
            
            If @ProductId is null 
            Begin
                Set @ErrorMsg = 'Product not Found'
                Goto ErrorHeader
            End
            --Declare StorageUnit_Cursor cursor for Select StorageUnitId from StorageUnit where ProductId = @ProductId
            --Open StorageUnit_Cursor
            --Fetc
            Select @StorageUnitId = StorageUnitId from StorageUnit where ProductId = @ProductId
            If @StorageUnitId is null
            Begin
                Set @ErrorMsg = 'StorageUnit not Found'
                Goto ErrorHeader
            End
            
            Insert into BOMHeader(StorageUnitId,Remarks,HostId,Quantity) Select @StorageUnitId,'Inserted by Interface',@HostId,@Quantity 
            Set @BOMHeaderId = @@Identity
        ENd
        
        Insert BOMSpecialInstruction
              (BOMHeaderId,
               ProcedureLine1,
               ProcedureLine2,
               ProcedureLine3,
               ProcedureLine4,
               ProcedureLine5,
               ProcedureLine6,
               ProcedureLine7,
               ProcedureLine8,
               ProcedureLine9,
               ProcedureLine10,
               ProcedureLine11,
               ProcedureLine12,
               SpecialInstructionsLine1,
               SpecialInstructionsLine2,
               SpecialInstructionsLine3,
               SpecialInstructionsLine4,
               SpecialInstructionsLine5,
               SpecialInstructionsLine6,
               SpecialInstructionsLine7,
               SpecialInstructionsLine8,
               SpecialInstructionsLine9,
               SpecialInstructionsLine10,
               SpecialInstructionsLine11,
               SpecialInstructionsLine12)
        Select @BOMHeaderId,
               ProcedureLine1,
               ProcedureLine2,
               ProcedureLine3,
               ProcedureLine4,
               ProcedureLine5,
               ProcedureLine6,
               ProcedureLine7,
               ProcedureLine8,
               ProcedureLine9,
               ProcedureLine10,
               ProcedureLine11,
               ProcedureLine12,
               SpecialInstructionsLine1,
               SpecialInstructionsLine2,
               SpecialInstructionsLine3,
               SpecialInstructionsLine4,
               SpecialInstructionsLine5,
               SpecialInstructionsLine6,
               SpecialInstructionsLine7,
               SpecialInstructionsLine8,
               SpecialInstructionsLine9,
               SpecialInstructionsLine10,
               SpecialInstructionsLine11,
               SpecialInstructionsLine12
          from InterfaceImportBOMSpecialInstruction
         where InterfaceImportBOMId = @InterfaceImportBOMId
        
        
        Declare Detail_Cursor_Insert Cursor for
        Select Compindex,
                CompProductCode,
                CompProduct,
                CompQuantity
           From InterfaceImportBOMDetail i
          where InterfaceImportBOMId = @InterfaceImportBOMId
            and CompProductCode != '001MAN'
            and not exists (select top 1 1
                              From BOMLine     bl (nolock)
                              join BOMProduct  bp (nolock) on bl.BOMLineId = bp.BOMLineId
                              join StorageUnit su (nolock) on bp.StorageUnitId = su.StorageUnitId
                              join Product      p (nolock) on su.ProductId = p.ProductId
                             Where bl.BOMHeaderId = @BOMHeaderId
                               and isnull(bp.LineNumber, 1) = 1
                               and bp.Quantity = i.compQuantity
                               and p.ProductCode = i.CompProductCode)
        --Select Compindex,
        --        CompProductCode,
        --        CompProduct,
        --        CompQuantity
        --   From InterfaceImportBOMDetail i
        --  where InterfaceImportBOMId = @InterfaceImportBOMId
        --    and CompProductCode != '001MAN'
        --    and not exists (select top 1 1
        --                      From BOMProduct  bp
        --                      Join BOMLine     bl (nolock) on bp.BOMLineId = bl.BOMLineId
        --                                                  and isnull(bp.LineNumber, 1) = 1
        --                      join BOMHeader   bh (nolock) on bh.BOMHeaderId = bl.BOMHeaderId
        --                      join StorageUnit su (nolock) on bp.StorageUnitId = su.StorageUnitId
        --                     Where bh.BOMHeaderId = @BOMHeaderId
        --                       and bp.Quantity = i.compQuantity)
        
        open Detail_Cursor_Insert
        
        Fetch Detail_Cursor_Insert 
        Into @CompIndex,
             @CompProductCode,
             @CompProduct,
             @CompQuantity
        Set @DetailFetchStatus = @@fetch_status
        While @DetailFetchStatus = 0
        Begin
            Insert into BOMLine (BOMHeaderId) Select @BOMHeaderId
            Set @BOMLineId = @@identity
            set @ProductId = null
            set @StorageUnitId = null
            
            Select @ProductId = ProductId from Product where ProductCode = @CompProductCode
            
            If @ProductId is null 
            Begin
      Set @ErrorMsg = 'Product not Found ' + @CompProductCode
                Goto ErrorDetail
            End
            
            Select @StorageUnitId = StorageUnitId from StorageUnit where ProductId = @ProductId
            
            If @StorageUnitId is null
            Begin
                Set @ErrorMsg = 'StorageUnit not Found ' + @CompProductCode
                Goto ErrorDetail
            End
            
            Insert into BOMProduct (BOMLineId,LineNumber,Quantity,StorageUnitId)
            Select @BOMLineId,1,@CompQuantity,@StorageUnitId
            
            Print 'Detail ' + @CompProduct
            Fetch Detail_Cursor_Insert 
            Into @CompIndex,
                 @CompProductCode,
                 @CompProduct,
                 @CompQuantity
            Set @DetailFetchStatus = @@fetch_status
        End
    ErrorDetail:
        Print @ErrorMsg
            
        Close Detail_Cursor_Insert
        Deallocate Detail_Cursor_Insert
        
    ErrorHeader:
        Print @ErrorMsg
        
        update InterfaceImportBOM
          set RecordStatus = 'Y'
         where InterfaceImportBOMId = @InterfaceImportBOMId
        
        Fetch Header_Cursor 
        into @InterfaceImportBOMId,
             @ProductCode,
             @Product,
             @PrimaryKey,
             @HostId,
             @Quantity
        Set @HeaderFetchStatus = @@fetch_status
    End
    Close Header_Cursor
    Deallocate Header_Cursor
return    
End

