﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DockSchedule_CssClass_Select
  ///   Filename       : p_DockSchedule_CssClass_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Sep 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DockSchedule_CssClass_Select
 
as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   CssClassId int primary key identity,
   CssClass   nvarchar(50) null
  )
  
  insert @TableResult (CssClass) select 'rsCategoryBlue'
  insert @TableResult (CssClass) select 'rsCategoryGreen'
  insert @TableResult (CssClass) select 'rsCategoryGray'
  insert @TableResult (CssClass) select 'rsCategoryRed'
  insert @TableResult (CssClass) select 'rsCategoryLime'
  
  select CssClassId,
         CssClass
    from @TableResult
end
 
