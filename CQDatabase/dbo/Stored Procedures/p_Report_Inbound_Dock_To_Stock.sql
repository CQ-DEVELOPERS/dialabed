﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Inbound_Dock_To_Stock
  ///   Filename       : p_Report_Inbound_Dock_To_Stock.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Inbound_Dock_To_Stock
(
 @WarehouseId int,
 @Production  bit = null
)
 
as
begin
	 set nocount on;
  
  select 'Target (minutes)' as 'Type',
         18 as 'Minutes'
  union
  select 'Achieved (minutes)' as 'Type',
         sum(datediff(mi, j.CheckedDate, i.EndDate)) / count(i.InstructionId) as 'Minutes'
    from Instruction      i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on j.StatusId           = s.StatusId
   where i.WarehouseId = @WarehouseId
     and it.InstructionTypeCode in ('PR','S','SM')
     and isnull(j.CheckedDate, i.StartDate) > convert(varchar(10), getdate(), 120)
     and j.CheckedDate < i.EndDate
  order by 'Type' desc
end
