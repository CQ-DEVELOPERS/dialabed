﻿
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Export_Main
  ///   Filename       : p_Plascon_Export_Main.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Plascon_Export_Main]
(
 @RecordType varchar(30) = null
)
as
begin
	 set nocount on;
  
    if ISNULL(@RecordType, 'BOT') = 'BOT' exec p_Plascon_Export_BOT 
    if ISNULL(@RecordType, 'PRV') = 'PRV' exec p_Plascon_Export_PRV 
    if ISNULL(@RecordType, 'SIS') = 'SIS' exec p_Plascon_Export_SIS 
    if ISNULL(@RecordType, 'TCR') = 'TCR' exec p_Plascon_Export_TCR 
    if ISNULL(@RecordType, 'MOW') = 'MOW' exec p_Plascon_Export_MOW 
    
    select COUNT(*) from InterfaceExtract where RecordStatus = 'N' and ProcessedDate Is Null 
end

