﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOH_Delete
  ///   Filename       : p_InterfaceImportSOH_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:11
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceImportSOH table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOH_Delete
(
 @InterfaceImportSOHId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceImportSOH
     where InterfaceImportSOHId = @InterfaceImportSOHId
  
  select @Error = @@Error
  
  
  return @Error
  
end
