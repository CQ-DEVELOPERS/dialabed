﻿/*  
  /// <summary>  
  ///   Procedure Name : p_Report_Product_Master  
  ///   Filename       : p_Report_Product_Master.sql  
  ///   Create By      : Kevin Wilson  
  ///   Date Created   : 15 September 2018  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/
CREATE PROCEDURE [dbo].[p_Report_Product_Master]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT p.ProductCode
		,REPLACE(p.Product, ',', '') AS 'Product'
		,su.SKUCode
		,su.SKU
		,pk.PalletQuantity
		,p.Barcode AS 'ProductBarcode'
		,s.MinimumQuantity
		,s.ReorderQuantity
		,s.MaximumQuantity
		,s.StorageUnitId
		,p.CuringPeriodDays
		,p.ShelfLifeDays
		,p.QualityAssuranceIndicator
		,p.ProductType
		,s.ProductCategory
		,OverReceipt
		,RetentionSamples
		,HostId
		,pk.PackTypeCode
		,pk.PackType
		,pk.Quantity
		,pk.Barcode AS 'PackBarcode'
		,pk.[Length]
		,pk.Width
		,pk.Height
		,pk.Volume
		,pk.NettWeight
		,pk.[Weight] AS 'GrossWeight'
		,pk.TareWeight
		,s.ProductCategory AS 'PackCategory'
		,s.PackingCategory
		,s.PickEmpty
		,s.StackingCategory
		,s.MovementCategory
		,s.ValueCategory
		,s.StoringCategory
		,s.PickPartPallet
		,pn.PrincipalCode
		,s.UnitPrice
		,s.StyleCode
		,s.Style
		,s.ColourCode
		,s.Colour
		,s.SizeCode
		,s.Size
		,p.Description2
		,p.Description3
		,p.ProductAlias
		,s.PutawayRule
		,s.AllocationRule
		,s.BillingRule
		,s.CycleCountRule
		,s.LotAttributeRule
		,s.StorageCondition
		,s.SerialTracked
	FROM Product p(NOLOCK)
	INNER JOIN StorageUnit s(NOLOCK) ON p.ProductId = s.ProductId
	INNER JOIN SKU su(NOLOCK) ON s.SKUId = su.SKUId
	LEFT OUTER JOIN Principal pn(NOLOCK) ON p.PrincipalId = pn.PrincipalId
	OUTER APPLY (
		SELECT DISTINCT pt.PackType
			,pt.PackTypeCode
			,p.Quantity
			,p.Barcode
			,p.[Length]
			,p.Width
			,p.Height
			,p.Volume
			,p.NettWeight
			,p.[Weight]
			,p.TareWeight
			,MAX(p.Quantity) AS PalletQuantity
		FROM Pack p
		INNER JOIN PackType pt ON p.PackTypeId = pt.PackTypeId
		WHERE StorageUnitId = s.StorageUnitId
		GROUP BY pt.PackType
			,pt.PackTypeCode
			,p.Quantity
			,p.Barcode
			,p.[Length]
			,p.Width
			,p.Height
			,p.Volume
			,p.NettWeight
			,p.[Weight]
			,p.TareWeight
		) AS pk
END