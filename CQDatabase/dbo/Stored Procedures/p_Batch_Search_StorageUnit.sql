﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Search_StorageUnit
  ///   Filename       : p_Batch_Search_StorageUnit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Search_StorageUnit
(
 @StorageUnitId int,
 @Batch         nvarchar(50) = null,
 @ECLNumber     nvarchar(10) = null
)
 
as
begin
	 set nocount on;
	 
  select sub.StorageUnitBatchId,
         b.Batch,
         b.BatchId,
         b.ECLNumber,
         b.CreateDate
    
from StorageUnitBatch sub (nolock)
    join Batch            b   (nolock) on sub.BatchId = b.BatchId
    where sub.StorageUnitId          = @StorageUnitId
--   where sub.StorageUnitId          = isnull(@StorageUnitId, sub.StorageUnitId)
     and isnull(b.Batch,'%')     like '%' + isnull(@Batch, isnull(b.Batch,'%')) + '%'
     and isnull(b.ECLNumber,'%') like '%' + isnull(@ECLNumber, isnull(b.ECLNumber,'%')) + '%'
  union
  select null,
        'None',
        1,
        '',
        getdate()
end

 
