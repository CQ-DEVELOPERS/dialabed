﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Packaging_Check_Sheet
  ///   Filename       : p_Report_Packaging_Check_Sheet.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 25 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/


CREATE procedure p_Report_Packaging_Check_Sheet
(
 @InboundShipmentId int = null,
 @ReceiptId         int = null,
 @ReceiptLineId     int = null
)
 
as
begin

	 set nocount on;
  
  declare @TableHeader as Table
  (
   InboundShipmentId  int,
   ReceiptId          int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   Supplier           nvarchar(255),
   SupplierCode       nvarchar(30)
  );
  
  declare @TableDetails as Table
  (
   ReceiptId          int,
   ReceiptLineId      int,
   LineNumber         int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   SKUCode            nvarchar(50),
   RequiredQuantity   numeric(13,6),
   ReceivedQuantity   numeric(13,6),
   AcceptedQuantity   numeric(13,6),
   RejectQuantity     numeric(13,6),
   DeliveryNoteQuantity numeric(13,6)
  );
  
  declare @StatusId    int,
          @WarehouseId int
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
 
  if @ReceiptLineId = -1
    set @ReceiptLineId = null
  
  if @InboundShipmentId is not null
  begin
    insert @TableHeader
          (InboundShipmentId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId)
    select isr.InboundShipmentId,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId
      from InboundShipmentReceipt isr (nolock)
      join Receipt                  r (nolock) on isr.ReceiptId = r.ReceiptId
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
     where isr.InboundShipmentId = isnull(@InboundShipmentId, isr.InboundShipmentId)
  
    select @WarehouseId = WarehouseId
      from InboundShipment (nolock)
     where InboundShipmentId = @InboundShipmentId
  end
  else if @ReceiptId is not null
  begin
    insert @TableHeader
          (InboundShipmentId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId)
    select null,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId
      from Receipt                  r (nolock)
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
     where r.ReceiptId = isnull(@ReceiptId, r.ReceiptId)
  
    select @WarehouseId = WarehouseId
      from Receipt (nolock)
     where ReceiptId = @ReceiptId
  end
  
  update th
     set Supplier     = ec.ExternalCompany,
         SupplierCode = ec.ExternalCompanyCode
    from @TableHeader th
    join ExternalCompany ec on th.ExternalCompanyId = ec.ExternalCompanyId
  
  insert @TableDetails
        (ReceiptId,
         ReceiptLineId,
         LineNumber,
         StorageUnitBatchId)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         il.LineNumber,
         rl.StorageUnitBatchId
    from @TableHeader th
    join ReceiptLine  rl on th.ReceiptId     = rl.ReceiptId
    join InboundLine  il on rl.InboundLineId = il.InboundLineId
   where rl.ReceiptLineId = isnull(@ReceiptLineId, rl.ReceiptLineId)
  
  --if dbo.ufn_Configuration(122, @WarehouseId) = 0
  --begin
  --  update @TableDetails
  --     set ExpectedQuantity = 0
  --end
  
  update td
     set ProductCode   = p.ProductCode,
         Product       = p.Product + ' - ' + sku.SKUCode,
         SKUCode       = sku.SKUCode,
         StorageUnitId = su.StorageUnitId
    from @TableDetails    td
    join StorageUnitBatch sub on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId     = su.StorageUnitId
    join Product          p   on su.ProductId          = p.ProductId
    join SKU              sku on su.SKUId              = sku.SKUId
  
  update td
  set RequiredQuantity      = rlp.RequiredQuantity,
      ReceivedQuantity      = rlp.ReceivedQuantity,
      AcceptedQuantity      = rlp.AcceptedQuantity,
      RejectQuantity        = rlp.RejectQuantity,
      DeliveryNoteQuantity  =  rlp.DeliveryNoteQuantity
  from @TableDetails    td
  join StorageUnitBatch     sub on td.StorageUnitBatchId = sub.StorageUnitBatchId
  join ReceiptLinePackaging rlp on rlp.StorageUnitBatchId = sub.StorageUnitBatchId
  join StorageUnit           su on sub.StorageUnitId      = su.StorageUnitId
  join Product                p on su.ProductId           = p.ProductId
  join SKU                  sku on su.SKUId               = sku.SKUId
     
  
  select th.InboundShipmentId,
         th.OrderNumber,
         th.Supplier,
         th.SupplierCode,
         td.ProductCode,
         td.Product,
         td.RequiredQuantity,
         td.ReceivedQuantity,
         td.AcceptedQuantity,
         td.RejectQuantity,
         td.DeliveryNoteQuantity
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
end
