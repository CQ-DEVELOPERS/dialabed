﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Parameter_OperatorExternalCompany
  ///   Filename       : p_Parameter_OperatorExternalCompany.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Parameter_OperatorExternalCompany
(
 @OperatorId int
)
 
as
begin
	 set nocount on;
  
  if exists(select top 1 1 from OperatorExternalCompany where OperatorId = @OperatorId)
    select -1      as 'ExternalCompanyId',
           '{All}' as 'ExternalCompanyCode',
           ' (All}' as 'ExternalCompany'
    union
    select ec.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany
      from ExternalCompany          ec (nolock)
      join OperatorExternalCompany oec (nolock) on ec.ExternalCompanyId = oec.ExternalCompanyId
     where oec.OperatorId = @OperatorId
    order by 'ExternalCompany'
  else
    select -1      as 'ExternalCompanyId',
           '{All}' as 'ExternalCompanyCode',
           ' (All}' as 'ExternalCompany'
    union
    select ExternalCompanyId,
           ExternalCompanyCode,
           ExternalCompany
      from ExternalCompany (nolock)
    order by 'ExternalCompany'
end
