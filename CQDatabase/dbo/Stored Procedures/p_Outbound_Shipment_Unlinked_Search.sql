﻿USE [DialaBed]
GO
/****** Object:  StoredProcedure [dbo].[p_Outbound_Shipment_Unlinked_Search]    Script Date: 6/25/2021 10:34:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Shipment_Unlinked_Search
  ///   Filename       : p_Outbound_Shipment_Unlinked_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/


ALTER procedure [dbo].[p_Outbound_Shipment_Unlinked_Search]
(
 @WarehouseId            int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime,
 @PrincipalId            int = null,
 @Route                  nvarchar(50) = null,
 @OnlyWaitingStatus		int = null,
 @RouteId				int = -1
)
 
as
begin
	 set nocount on;
  
  
  if @RouteId in (-1, 0)
	set @Route = null
  else
    set @Route = (Select Route FROM Route WHERE RouteId = @RouteId)

  declare @TableHeader as table
  (
   WarehouseId           int,
   IssueId               int,
   OutboundDocumentId    int,
   OrderNumber           nvarchar(30),
   ExternalCompanyCode   nvarchar(30),
   ExternalCompany       nvarchar(255),
   RouteId               int,
   Route                 nvarchar(50),
   NumberOfLines         int,
   DeliveryDate          datetime,
   StatusId              int,
   Status                nvarchar(50),
   OutboundDocumentType  nvarchar(50),
   LocationId            int,
   Location              nvarchar(15),
   CreateDate            datetime,
   Rating                int,
   AvailabilityIndicator nvarchar(20),
   Availability          nvarchar(20),
   OrderVolume           float,
   OrderWeight           float
  );
  
  declare @TableDetail as table
  (
   WarehouseId        int,
   OutboundDocumentId int,
   OrderVolume        numeric(18,3),
   OrderWeight        numeric(18,3),
   NumberOfLines      int
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
	 
	 if @ExternalCompanyCode is null
	   set @ExternalCompanyCode = ''
	 
	 if @ExternalCompany is null
	   set @ExternalCompany = ''
	 
	 if @PrincipalId = -1
    set @PrincipalId = null
  
  insert @TableHeader
        (WarehouseId,
         IssueId,
         OutboundDocumentId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         RouteId,
         DeliveryDate,
         StatusId,
         Status,
         OutboundDocumentType,
         LocationId,
         CreateDate,
         Rating,
         NumberOfLines,
         OrderVolume,
         OrderWeight,
         Availability)
  select i.WarehouseId,
         i.IssueId,
         od.OutboundDocumentId,
         od.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         i.RouteId,
         i.DeliveryDate,
         i.StatusId,
         s.Status,
         odt.OutboundDocumentType,
         i.LocationId,
         od.CreateDate,
         ec.Rating,
         i.NumberOfLines,
         i.Weight,
         null,
         i.Availability
    from OutboundDocument     od  (nolock)
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
    join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join Status               s   (nolock) on i.StatusId               = s.StatusId
    left
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
   where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
     --and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     --and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     --and od.OrderNumber           like isnull(@OrderNumber  + '%', od.OrderNumber)
     and i.DeliveryDate        between @FromDate and @ToDate
     and od.WarehouseId              = @WarehouseId
     and s.Type                      = 'IS'
     and s.StatusCode           not in ('C')
     and osi.OutboundShipmentId     is null
     and odt.OutboundDocumentTypeCode != 'PRQ'
     and isnull(od.PrincipalId, -1) = isnull(@PrincipalId, isnull(od.PrincipalId, -1))
  
  update tr
     set Location = l.Location
    from @TableHeader tr
    join Location     l  (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Route = r.Route
    from @TableHeader  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
--  insert @TableDetail
--        (OutboundDocumentId,
--         OrderVolume,
--         OrderWeight,
--         NumberOfLines)
--  select th.OutboundDocumentId,
--         round(sum(il.Quantity) * sum(p.Quantity),0),
--         round(sum(il.Quantity) * sum(p.Weight),0),
--         count(distinct(ol.OutboundLineId))
--    from @TableHeader th
--    join OutboundLine ol (nolock) on th.OutboundDocumentId = ol.OutboundDocumentId
--    join IssueLine    il (nolock) on ol.OutboundLineId     = il.OutboundLineId
--    join Pack          p (nolock) on ol.StorageUnitId      = p.StorageUnitId
--   where p.PackTypeId = 2 --p.Quantity = 1 -- Performance Issue
--     and p.WarehouseId = @WarehouseId
--  group by th.OutboundDocumentId
  
  --declare @TableTemp as table
  --(
  -- WarehouseId        int,
  -- OutboundDocumentId int,
  -- StorageUnitId      int,
  -- Quantity           float,
  -- Weight             numeric(13,3),
  -- Volume             numeric(13,3)
  --);
  
  --insert @TableTemp
  --      (WarehouseId,
  --       OutboundDocumentId,
  --       StorageUnitId,
  --       Quantity)
  --select th.WarehouseId,
  --       th.OutboundDocumentId,
  --       ol.StorageUnitId,
  --       sum(il.Quantity)
  --  from @TableHeader th
  --  join OutboundLine ol (nolock) on th.OutboundDocumentId = ol.OutboundDocumentId
  --  join IssueLine    il (nolock) on ol.OutboundLineId     = il.OutboundLineId
  --group by th.WarehouseId,
  --         th.OutboundDocumentId,
  --         ol.StorageUnitId
  
  --update tt
  --   set Weight = p.Weight,
  --       Volume = isnull(p.Volume, p.Quantity)
  --  from @TableTemp tt
  --  join Pack        p (nolock) on tt.StorageUnitId = p.StorageUnitId
  --                             and tt.Warehouseid   = p.WarehouseId
  --  join PackType   pt (nolock) on p.PackTypeId     = pt.PackTypeId
  -- where pt.InboundSequence in (select max(InboundSequence) from PackType (nolock))
  --   and p.WarehouseId      = @WarehouseId
  
  --insert @TableDetail
  --      (OutboundDocumentId,
  --       OrderVolume,
  --       OrderWeight,
  --       NumberOfLines)
  --select OutboundDocumentId,
  --       Quantity * Volume,
  --       Quantity * Weight,
  --       null
  --  from @TableTemp
  
  update @TableHeader
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @WarehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableHeader
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableHeader
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableHeader
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  IF(@OnlyWaitingStatus = 1)
  BEGIN
  select IssueId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         Route,
         NumberOfLines,
         DeliveryDate,
         Status,
         OutboundDocumentType,
         Location,
         CreateDate,
         Rating,
         AvailabilityIndicator,
         Availability,
         OrderVolume,
         OrderWeight
    from @TableHeader
   where ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ExternalCompanyCode)
     and ExternalCompany     like isnull(@ExternalCompany + '%', ExternalCompany)
     and OrderNumber         like isnull(@OrderNumber  + '%', OrderNumber)
     and isnull(Route,'')    like ISNULL(@Route + '%', isnull(Route,''))
     and Status				 like 'Waiting'
  END
  ELSE
  BEGIN
  select IssueId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         Route,
         NumberOfLines,
         DeliveryDate,
         Status,
         OutboundDocumentType,
         Location,
         CreateDate,
         Rating,
         AvailabilityIndicator,
         Availability,
         OrderVolume,
         OrderWeight
    from @TableHeader
   where ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ExternalCompanyCode)
     and ExternalCompany     like isnull(@ExternalCompany + '%', ExternalCompany)
     and OrderNumber         like isnull(@OrderNumber  + '%', OrderNumber)
     and isnull(Route,'')    like ISNULL(@Route + '%', isnull(Route,''))
  END
end
