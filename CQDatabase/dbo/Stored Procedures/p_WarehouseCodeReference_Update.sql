﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WarehouseCodeReference_Update
  ///   Filename       : p_WarehouseCodeReference_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 09:52:56
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the WarehouseCodeReference table.
  /// </remarks>
  /// <param>
  ///   @WarehouseCodeReferenceId int = null,
  ///   @WarehouseCode nvarchar(20) = null,
  ///   @DownloadType nvarchar(20) = null,
  ///   @WarehouseId int = null,
  ///   @InboundDocumentTypeId int = null,
  ///   @OutboundDocumentTypeId int = null,
  ///   @ToWarehouseId int = null,
  ///   @ToWarehouseCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WarehouseCodeReference_Update
(
 @WarehouseCodeReferenceId int = null,
 @WarehouseCode nvarchar(20) = null,
 @DownloadType nvarchar(20) = null,
 @WarehouseId int = null,
 @InboundDocumentTypeId int = null,
 @OutboundDocumentTypeId int = null,
 @ToWarehouseId int = null,
 @ToWarehouseCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @WarehouseCodeReferenceId = '-1'
    set @WarehouseCodeReferenceId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @OutboundDocumentTypeId = '-1'
    set @OutboundDocumentTypeId = null;
  
  if @ToWarehouseId = '-1'
    set @ToWarehouseId = null;
  
	 declare @Error int
 
  update WarehouseCodeReference
     set WarehouseCode = isnull(@WarehouseCode, WarehouseCode),
         DownloadType = isnull(@DownloadType, DownloadType),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         InboundDocumentTypeId = isnull(@InboundDocumentTypeId, InboundDocumentTypeId),
         OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, OutboundDocumentTypeId),
         ToWarehouseId = isnull(@ToWarehouseId, ToWarehouseId),
         ToWarehouseCode = isnull(@ToWarehouseCode, ToWarehouseCode) 
   where WarehouseCodeReferenceId = @WarehouseCodeReferenceId
  
  select @Error = @@Error
  
  
  return @Error
  
end
