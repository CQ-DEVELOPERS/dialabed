﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_PickFace
  ///   Filename       : p_Report_Product_PickFace.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Report_Product_PickFace
(
 @WarehouseId        int,
 @LocationId         int = Null,
 @StorageUnitId      int = Null,
 @ProductDescription nvarchar(50) = Null  
)
as
Begin
  if @StorageUnitId = -1
    set @StorageUnitId = null
  
  if @LocationId = -1
    set @LocationId = null
  
  If @ProductDescription is null 
  Begin
    Select l.Location,
           ProductCode,
           Product as Description,
           sk.SKUCode As Units,
           RelativeValue as 'Pick Sequence',
           PackingCategory,
           ProductCategory as 'ABC - Movement'
      from StorageUnitLocation s (nolock)
      join AreaLocation       al (nolock) on s.Locationid = al.LocationId
      join Area                a (nolock) on al.AreaId = a.AreaId
      join Location            l (nolock) on l.locationId = s.LocationId
      join StorageUnit         u (nolock) on u.StorageUnitId = s.StorageUnitId
      join Product             p (nolock) on p.ProductID = u.ProductId
      join SKU                sk (nolock) on sk.SkuID = u.SkuID
      join UOM                 m (nolock) on m.UOMid = sk.UOMid
     where a.WarehouseId = @WarehouseId
       and u.StorageUnitId = Isnull(@StorageUnitId,u.StorageUnitId)
       and l.LocationId    = Isnull(@LocationId,l.LocationId)
    order by l.RelativeValue
  end
  else
  begin
    select l.Location,
           ProductCode,
           Product as Description,
           sk.SKUCode As Units,
           RelativeValue as 'Pick Sequence',
           PackingCategory,
           ProductCategory as 'ABC - Movement',
           u.StorageUnitId
      from storageUnitLocation s (nolock)
      join AreaLocation       al (nolock) on s.Locationid = al.LocationId
      join Area                a (nolock) on al.AreaId = a.AreaId
      join Location            l (nolock) on l.locationId = s.LocationId
      join StorageUnit         u (nolock) on u.StorageUnitId = s.StorageUnitId
      join Product             p (nolock) on p.ProductID = u.ProductId
      join SKU                sk (nolock) on sk.SkuID = u.SkuID
      join UOM                 m (nolock) on m.UOMid = sk.UOMid
     where a.WarehouseId = @WarehouseId
       and u.StorageUnitID = Isnull(@StorageUnitId,u.StorageUnitId)
       and l.LocationId    = Isnull(@LocationId,l.LocationId)
       and p.Product    like '%' + @ProductDescription + '%'
    order by l.RelativeValue
  End
End
