﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_All_Status_By_Type
  ///   Filename       : p_All_Status_By_Type.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_All_Status_By_Type
(
 @Type       char(2)
)
 
as
begin
	 set nocount on;
  
  begin
    select '-1'    as 'StatusId',
           '{All}' as 'Status',
           '{All}' as 'StatusCode'
    union
    select distinct StatusId,
           Status,
           StatusCode
    from Status 
    where Type = 'IS'
    order by Status 
          
--from  Status s on j.StatusId = s.StatusId
--join Instruction i on i.JobId = j.JobId
--join ReceiptLine          rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
--		   join Receipt               r (nolock) on rl.ReceiptId = r.ReceiptId
--		   join InboundDocument      id (nolock) on r.InboundDocumentId = id.InboundDocumentId
--		   join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
--		   where idt.InboundDocumentTypeCode = 'PRV'
    --order by Status
  end
end
