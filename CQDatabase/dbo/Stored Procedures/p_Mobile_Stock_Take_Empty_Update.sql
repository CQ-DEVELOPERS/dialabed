﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Stock_Take_Empty_Update
  ///   Filename       : p_Mobile_Stock_Take_Empty_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Stock_Take_Empty_Update
(
 @WarehouseId int,
 @OperatorId  int,
 @LocationId  int,
 @CreateOnly  bit = 0
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @CreateOnly = 0
  begin
    -- If there is already a Stock Take ignore it
    exec @Error = p_Location_Update
     @locationId   = @locationId,
     @StockTakeInd = 0
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_Housekeeping_Stock_Take_Create_Product
   @warehouseId        = @WarehouseId,
   @operatorId         = @operatorId,
   @locationId         = @locationId,
   @jobId              = @JobId output
   --@storageUnitBatchId = @StorageUnitBatchId
  
  if @Error <> 0
    goto error
  
  if @CreateOnly = 0
  begin
    if @JobId is not null
    begin
    select 'p_Mobile_Stock_Take_Count'
      exec @Error = p_Mobile_Stock_Take_Count
       @type = 2,
       @operatorId = @operatorId,
       @jobId = @jobId,
       @locationId = @locationId
      
      if @Error <> 0
        goto error
    end
  end
  
  commit transaction
  return 1
  
  error:
    raiserror 900000 'Error executing p_Mobile_Stock_Take_Empty_Update'
    rollback transaction
    return 0
end
