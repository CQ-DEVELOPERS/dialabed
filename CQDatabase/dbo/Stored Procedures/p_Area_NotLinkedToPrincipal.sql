﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_NotLinkedToPrincipal
  ///   Filename       : p_Area_NotLinkedToPrincipal.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_NotLinkedToPrincipal
( 
@PrincipalId	int
)

 
as
begin
	 set nocount on;
  
  select a.AreaId,
		 a.Area,
		 a.AreaCode
    from Area a
    where not exists(select 1 from AreaPrincipal ap where a.AreaId = ap.AreaId
														and ap.PrincipalId = @PrincipalId)
	order by Area
end
