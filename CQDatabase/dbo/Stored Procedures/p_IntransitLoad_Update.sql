﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoad_Update
  ///   Filename       : p_IntransitLoad_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012 12:08:06
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the IntransitLoad table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadId int = null,
  ///   @WarehouseId int = null,
  ///   @StatusId int = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(100) = null,
  ///   @VehicleRegistration nvarchar(100) = null,
  ///   @Route nvarchar(100) = null,
  ///   @DriverId int = null,
  ///   @Remarks nvarchar(max) = null,
  ///   @CreatedById int = null,
  ///   @ReceivedById int = null,
  ///   @CreateDate datetime = null,
  ///   @LoadClosed datetime = null,
  ///   @ReceivedDate datetime = null,
  ///   @Offloaded datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoad_Update
(
 @IntransitLoadId int = null,
 @WarehouseId int = null,
 @StatusId int = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @SealNumber nvarchar(100) = null,
 @VehicleRegistration nvarchar(100) = null,
 @Route nvarchar(100) = null,
 @DriverId int = null,
 @Remarks nvarchar(max) = null,
 @CreatedById int = null,
 @ReceivedById int = null,
 @CreateDate datetime = null,
 @LoadClosed datetime = null,
 @ReceivedDate datetime = null,
 @Offloaded datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  update IntransitLoad
     set WarehouseId = isnull(@WarehouseId, WarehouseId),
         StatusId = isnull(@StatusId, StatusId),
         DeliveryNoteNumber = isnull(@DeliveryNoteNumber, DeliveryNoteNumber),
         DeliveryDate = isnull(@DeliveryDate, DeliveryDate),
         SealNumber = isnull(@SealNumber, SealNumber),
         VehicleRegistration = isnull(@VehicleRegistration, VehicleRegistration),
         Route = isnull(@Route, Route),
         DriverId = isnull(@DriverId, DriverId),
         Remarks = isnull(@Remarks, Remarks),
         CreatedById = isnull(@CreatedById, CreatedById),
         ReceivedById = isnull(@ReceivedById, ReceivedById),
         CreateDate = isnull(@CreateDate, CreateDate),
         LoadClosed = isnull(@LoadClosed, LoadClosed),
         ReceivedDate = isnull(@ReceivedDate, ReceivedDate),
         Offloaded = isnull(@Offloaded, Offloaded) 
   where IntransitLoadId = @IntransitLoadId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_IntransitLoadHistory_Insert
         @IntransitLoadId = @IntransitLoadId,
         @WarehouseId = @WarehouseId,
         @StatusId = @StatusId,
         @DeliveryNoteNumber = @DeliveryNoteNumber,
         @DeliveryDate = @DeliveryDate,
         @SealNumber = @SealNumber,
         @VehicleRegistration = @VehicleRegistration,
         @Route = @Route,
         @DriverId = @DriverId,
         @Remarks = @Remarks,
         @CreatedById = @CreatedById,
         @ReceivedById = @ReceivedById,
         @CreateDate = @CreateDate,
         @LoadClosed = @LoadClosed,
         @ReceivedDate = @ReceivedDate,
         @Offloaded = @Offloaded,
         @CommandType = 'Update'
  
  return @Error
  
end
