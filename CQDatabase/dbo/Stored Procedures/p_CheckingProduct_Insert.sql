﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingProduct_Insert
  ///   Filename       : p_CheckingProduct_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:17
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the CheckingProduct table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber varchar(max) = null,
  ///   @JobId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StorageUnitId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @ProductBarcode nvarchar(100) = null,
  ///   @PackBarcode nvarchar(100) = null,
  ///   @Product nvarchar(510) = null,
  ///   @SKUCode nvarchar(100) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @ExpiryDate datetime = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null,
  ///   @CheckQuantity float = null,
  ///   @Checked int = null,
  ///   @Total int = null,
  ///   @Lines nvarchar(510) = null,
  ///   @InstructionRefId int = null,
  ///   @SkipUpdate bit = null,
  ///   @InsertDate datetime = null,
  ///   @ErrorMsg nvarchar(max) = null 
  /// </param>
  /// <returns>
  ///   CheckingProduct.InstructionId,
  ///   CheckingProduct.WarehouseId,
  ///   CheckingProduct.OrderNumber,
  ///   CheckingProduct.JobId,
  ///   CheckingProduct.ReferenceNumber,
  ///   CheckingProduct.StorageUnitBatchId,
  ///   CheckingProduct.StorageUnitId,
  ///   CheckingProduct.ProductCode,
  ///   CheckingProduct.ProductBarcode,
  ///   CheckingProduct.PackBarcode,
  ///   CheckingProduct.Product,
  ///   CheckingProduct.SKUCode,
  ///   CheckingProduct.Batch,
  ///   CheckingProduct.ExpiryDate,
  ///   CheckingProduct.Quantity,
  ///   CheckingProduct.ConfirmedQuantity,
  ///   CheckingProduct.CheckQuantity,
  ///   CheckingProduct.Checked,
  ///   CheckingProduct.Total,
  ///   CheckingProduct.Lines,
  ///   CheckingProduct.InstructionRefId,
  ///   CheckingProduct.SkipUpdate,
  ///   CheckingProduct.InsertDate,
  ///   CheckingProduct.ErrorMsg 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingProduct_Insert
(
 @InstructionId int = null,
 @WarehouseId int = null,
 @OrderNumber varchar(max) = null,
 @JobId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @StorageUnitBatchId int = null,
 @StorageUnitId int = null,
 @ProductCode nvarchar(60) = null,
 @ProductBarcode nvarchar(100) = null,
 @PackBarcode nvarchar(100) = null,
 @Product nvarchar(510) = null,
 @SKUCode nvarchar(100) = null,
 @Batch nvarchar(100) = null,
 @ExpiryDate datetime = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null,
 @CheckQuantity float = null,
 @Checked int = null,
 @Total int = null,
 @Lines nvarchar(510) = null,
 @InstructionRefId int = null,
 @SkipUpdate bit = null,
 @InsertDate datetime = null,
 @ErrorMsg nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert CheckingProduct
        (InstructionId,
         WarehouseId,
         OrderNumber,
         JobId,
         ReferenceNumber,
         StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         ProductBarcode,
         PackBarcode,
         Product,
         SKUCode,
         Batch,
         ExpiryDate,
         Quantity,
         ConfirmedQuantity,
         CheckQuantity,
         Checked,
         Total,
         Lines,
         InstructionRefId,
         SkipUpdate,
         InsertDate,
         ErrorMsg)
  select @InstructionId,
         @WarehouseId,
         @OrderNumber,
         @JobId,
         @ReferenceNumber,
         @StorageUnitBatchId,
         @StorageUnitId,
         @ProductCode,
         @ProductBarcode,
         @PackBarcode,
         @Product,
         @SKUCode,
         @Batch,
         @ExpiryDate,
         @Quantity,
         @ConfirmedQuantity,
         @CheckQuantity,
         @Checked,
         @Total,
         @Lines,
         @InstructionRefId,
         @SkipUpdate,
         isnull(@InsertDate, getdate()),
         @ErrorMsg 
  
  select @Error = @@Error
  
  
  return @Error
  
end
