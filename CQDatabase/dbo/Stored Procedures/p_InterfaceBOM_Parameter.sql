﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOM_Parameter
  ///   Filename       : p_InterfaceBOM_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:46
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceBOM table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceBOM.InterfaceBOMId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOM_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceBOMId
        ,null as 'InterfaceBOM'
  union
  select
         InterfaceBOM.InterfaceBOMId
        ,InterfaceBOM.InterfaceBOMId as 'InterfaceBOM'
    from InterfaceBOM
  
end
