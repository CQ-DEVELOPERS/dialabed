﻿/*          
  /// <summary>          
  ///   Procedure Name : p_UserOperator_Insert_Xml          
  ///   Filename       : p_UserOperator_Insert_Xml.sql          
  ///   Create By      : Grant Schultz          
  ///   Date Created   : 25 Mar 2015          
  /// </summary>          
  /// <remarks>          
  ///             
  /// </remarks>          
  /// <param>          
  ///             
  /// </param>          
  /// <returns>          
  ///             
  /// </returns>          
  /// <newpara>          
  ///   Modified by    :           
  ///   Modified Date  :           
  ///   Details        :           
  /// </newpara>          
*/          
CREATE procedure [dbo].[p_UserOperator_Insert_Xml]          
(          
 @UserName      nvarchar(512),          
 @Password      nvarchar(128),          
 @IpAddress     nvarchar(50) = null,          
 @Result        nvarchar(max) = null Output,          
 @ServerName    nvarchar(256) = null output,          
 @DatabaseName  nvarchar(256) = null output,          
 @XmlType       nvarchar(50),          
 @Xml           nvarchar(max)          
)          
as             
begin         
        
 Declare @WareHOuseCode nvarchar(100)        
 SEt @WareHOuseCode = ( Select WarehouseCode from Operator O,Warehouse w where Operator = @UserName and o.WarehouseId  = w.WarehouseId)         
        
        
   EXEC CQCommon.dbo.[p_UserOperator_Insert_Xml]          
  @UserName      = @UserName          
 ,@Password      = @Password          
 ,@IpAddress     = @IpAddress          
 ,@Result        = @Result OUTPUT          
 ,@ServerName    = @ServerName OUTPUT          
 ,@DatabaseName  = @DatabaseName OUTPUT          
 ,@XmlType       = @XmlType          
 ,@Xml           = @Xml        
 ,@WareHOuseCode = @WareHOuseCode        
   
   
  
         
end 