﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Inbound_BOE_Line
  ///   Filename       : p_Mobile_Inbound_BOE_Line.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Inbound_BOE_Line
(
 @ReceiptLineId int = null
)
 
as
begin
  set nocount on;
  
  declare @BOEL bit = 0
  
  if @ReceiptLineId is not null
    select @BOEL = 1
      from ReceiptLine rl (nolock)
      join Receipt      r (nolock) on rl.ReceiptId = r.ReceiptId
      join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
      join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      join Batch b (nolock) on sub.BatchId = b.BatchId
                           and b.BillOfEntry + b.BOELineNumber = b.Batch
     where rl.ReceiptLineId = @ReceiptLineId
       and idt.InboundDocumentTypeCode = 'BOND'
  
  if @BOEL is null
    set @BOEL = 0
  
  select @BOEL
  return @BOEL
end
