﻿IF OBJECT_ID('dbo.p_Planning_Order_Search') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.p_Planning_Order_Search
    IF OBJECT_ID('dbo.p_Planning_Order_Search') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Planning_Order_Search >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.p_Planning_Order_Search >>>'
END
go

/*
  /// <summary>
  ///   Procedure Name : p_Planning_Order_Search
  ///   Filename       : p_Planning_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>      
  ///   Modified by    :  Pragna Parikh    
  ///   Modified Date  :  10 Aug 2021
  ///   Details        :  Task : DAB-96
  ///				   :  Mapping changed as below:
  ///   			   :  VatSummary(InterfaceImportHeader) = AdditionalText1(OutboundDocument) = Display as Balance column in UI
  ///   				  Total(InterfaceImportHeader) = AdditionalText2(OutboundDocument) = Display as Amount column in UI
  ///					  If (balance * -1) < Amount then it indicates that the account is not fully paid, CQ 
  ///					  must highlight the text in the amount column as red.
  /// </newpara>
*/

CREATE procedure [dbo].[p_Planning_Order_Search]
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime,
 @PrincipalId            int = null,
 @PageSize                bigint = null,
 @PageNum                 bigint = null,
 @RouteId				  int = -1
)
 
as
begin
	 set nocount on;
 set @PageSize = 200
 --set @PageNum = 10
  declare @TableResult as
  table(
		ResultNum				INT,
        OutboundDocumentId        int,
        OutboundDocumentType      nvarchar(30),
        IssueId                   int,
        OrderNumber               nvarchar(30),
        OutboundShipmentId        int,
        CustomerCode              nvarchar(30),
        Customer                  nvarchar(255),
        RouteId                   int,
        Route                     nvarchar(50),
        NumberOfLines             int,
        DeliveryDate              datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        PriorityId                int,
        Priority                  nvarchar(50),
        LocationId                int,
        Location                  nvarchar(15),
        Rating                    int,
        AvailabilityIndicator     nvarchar(20),
        Availability              nvarchar(20),
        Remarks                   nvarchar(255),
        AreaType                  nvarchar(10),
        PrincipalId               int,
        PrincipalCode             nvarchar(30),
        AddressLine1              nvarchar(255),
        AddressLine2              nvarchar(255),
        AddressLine3              nvarchar(255),
        AddressLine4              nvarchar(255),
        AddressLine5              nvarchar(255),
        Id                        int,
		ContactPerson			  nvarchar(100),  
		AdditionalText1			  DECIMAL(13,3),  
		AdditionalText2			  DECIMAL(13,3),  
		IsFullyPaidOrder		  BIT  
       );
  
  declare @TableIssues as table
		(
		IssueId					INT
		)
			
  declare @GetDate					datetime,
          @PlannedOnly				nvarchar(30) = '0',
		  @AvailabilityIndicator	nvarchar(20),
		  @IssueId					int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null

  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null

	if @RouteId in (-1, 0)
		set @RouteId = null
	 
	 if @ExternalCompanyCode is null
	   set @ExternalCompanyCode = ''
	 
	 if @ExternalCompany is null
	   set @ExternalCompany = ''
	 
	 if @PrincipalId in (-1,0)
    set @PrincipalId = null
  print '@PrincipalId'
  print @PrincipalId
  if exists(select top 1 1 from InterfaceImportOrderNumbers (Nolock))
    set @PlannedOnly = '1'
  
  if @OutboundShipmentId is null
  begin
		With SQLPaging As   ( 
		Select Top(@PageSize * @PageNum) ROW_NUMBER() OVER (ORDER BY i.DeliveryDate DESC) as ResultNum,
		  od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           odt.OutboundDocumentType,
           ec.Rating,
           i.Remarks,
           i.AreaType,
           i.NumberOfLines,
           od.PrincipalId,
           Availability,
           i.AddressLine1,
           i.AddressLine2,
    i.AddressLine3,
           i.AddressLine4,
           i.AddressLine5,
           o.InterfaceImportOrderNumbersId,
		   ContactPerson,
		   ISNULL(od.AdditionalText1, 0) AS AdditionalText1,  
		   ISNULL(od.AdditionalText2, 0) AS AdditionalText2,  
		   CASE WHEN (AdditionalText1 * CAST(-1 AS DECIMAL(13,3))) < AdditionalText2 THEN '0' ELSE '1' END AS IsFullyPaidOrder  
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId    = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      left
      join InterfaceImportOrderNumbers o (nolock) on od.OrderNumber = o.OrderNumber
     where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode like isnull('%' + @ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany     like isnull('%' + @ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber         like isnull('%' + @OrderNumber  + '%', od.OrderNumber)
	   and i.RouteId = isnull(@RouteId,i.RouteId)
       and isnull(i.DeliveryDate, od.CreateDate)      between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('I','W','P','SA') -- Interfaced, Waiting, Palletised, Stock Allocated
       and od.WarehouseId            = @WarehouseId
       and odt.OutboundDocumentTypeCode != 'PRQ'
	   and od.PrincipalId = isnull(@PrincipalId, od.PrincipalId)
       --and isnull(od.PrincipalId, -1) = isnull(@PrincipalId, isnull(od.PrincipalId, -1))
       and (isnull(o.OrderNumber, '0') = @PlannedOnly or od.OrderNumber = o.OrderNumber)
	   group by 		  od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           odt.OutboundDocumentType,
           ec.Rating,
           i.Remarks,
           i.AreaType,
           i.NumberOfLines,
           od.PrincipalId,
           Availability,
           i.AddressLine1,
           i.AddressLine2,
           i.AddressLine3,
           i.AddressLine4,
           i.AddressLine5,
           o.InterfaceImportOrderNumbersId,
           ContactPerson,
		   od.AdditionalText1,
		   od.AdditionalText2)

    insert @TableResult
          (ResultNum,
		  OutboundDocumentId,
           IssueId,
           LocationId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           OutboundDocumentType,
           Rating,
           Remarks,
           AreaType,
           NumberOfLines,
           PrincipalId,
           Availability,
           AddressLine1,
           AddressLine2,
           AddressLine3,
           AddressLine4,
           AddressLine5,
           Id,
           ContactPerson,  
		   AdditionalText1,  
		   AdditionalText2,
		   IsFullyPaidOrder)
    SELECT * FROM SQLPaging WITH (NOLOCK) WHERE ResultNum > ((@PageNum - 1) * @PageSize)
    
    update tr
       set OutboundShipmentId = si.OutboundShipmentId,
           DeliveryDate       = os.ShipmentDate
      from @TableResult  tr
      join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
      join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  end
  else
  BEGIN
    With SQLPaging As   ( 
    Select Top(@PageSize * @PageNum) ROW_NUMBER() OVER (ORDER BY i.DeliveryDate DESC) as ResultNum,
          osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           odt.OutboundDocumentType,
           ec.Rating,
 i.Remarks,
           i.AreaType,
           NumberOfLines,
           od.PrincipalId,
           i.Availability,
           i.AddressLine1,
           i.AddressLine2,
           i.AddressLine3,
           i.AddressLine4,
           i.AddressLine5,
o.InterfaceImportOrderNumbersId,
		   ContactPerson,
		   ISNULL(od.AdditionalText1, 0) AS AdditionalText1,  
		   ISNULL(od.AdditionalText2, 0) AS  AdditionalText2,  
		   CASE WHEN (AdditionalText1 * CAST(-1 AS DECIMAL(13,3))) < AdditionalText2 THEN '0' ELSE '1' END AS IsFullyPaidOrder  
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
      left
      join InterfaceImportOrderNumbers o (nolock) on od.OrderNumber = o.OrderNumber
     where osi.OutboundShipmentId = @OutboundShipmentId
       and odt.OutboundDocumentTypeCode != 'PRQ'
	   and od.PrincipalId = isnull(@PrincipalId, od.PrincipalId)
       --and isnull(od.PrincipalId, -1) = isnull(@PrincipalId, isnull(od.PrincipalId, -1))
       and (isnull(o.OrderNumber, '0') = @PlannedOnly or od.OrderNumber = o.OrderNumber)
	group by           osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           odt.OutboundDocumentType,
           ec.Rating,
           i.Remarks,
           i.AreaType,
           NumberOfLines,
           od.PrincipalId,
           i.Availability,
           i.AddressLine1,
           i.AddressLine2,
           i.AddressLine3,
           i.AddressLine4,
           i.AddressLine5,
           o.InterfaceImportOrderNumbersId,
           ContactPerson,
		   od.AdditionalText1,
		   od.AdditionalText2)
    insert @TableResult
          (ResultNum,
		  OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           LocationId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           OutboundDocumentType,
           Rating,
           Remarks,
           AreaType,
           NumberOfLines,
           PrincipalId,
           Availability,
           AddressLine1,
           AddressLine2,
           AddressLine3,
           AddressLine4,
           AddressLine5,
           Id,
           ContactPerson,
		   AdditionalText1,  
		   AdditionalText2,
		   IsFullyPaidOrder)
	SELECT * FROM SQLPaging WITH (NOLOCK) WHERE ResultNum > ((@PageNum - 1) * @PageSize)
END


 insert @TableIssues
          (IssueId)
	SELECT distinct issueid 
	FROM @TableResult

  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal     p (nolock) on tr.PrincipalId = p.PrincipalId
  
  update @TableResult
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @warehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set Availability = 'Standard'
where Availability is null
  
    while exists (select top 1 1 from @TableIssues)
    begin
		select top 1 @IssueId = IssueId 
		  from @TableIssues

		exec	p_Planning_All_Stock_Available
				@OutboundShipmentId  = null,
				@IssueId  = @IssueId,
				@AvailabilityIndicator = @AvailabilityIndicator output

		update tr
		   set  Availability = @AvailabilityIndicator
		  from @TableResult tr
		 where tr.IssueId = @IssueId

		delete from @TableIssues
		 where IssueId = @IssueId
    end
    
  select IssueId,
         isnull(OutboundShipmentId, '-1')as 'OutboundShipmentId',
         OrderNumber,
         CustomerCode,
         Customer,
         isnull(RouteId,-1) as 'RouteId',
         Route,
         NumberOfLines,
         DeliveryDate,
         CreateDate,
         Status,
         PriorityId,
         Priority,
         OutboundDocumentType,
         isnull(LocationId,-1) as 'LocationId',
         Location,
         Rating,
         AvailabilityIndicator,
         Availability,
         Remarks,
         AreaType,
         PrincipalCode,
         AddressLine1,
         AddressLine2,
         AddressLine3,
         AddressLine4,
         AddressLine5,
           ContactPerson,
		   AdditionalText1,  
		   AdditionalText2,  
		   IsFullyPaidOrder
    from @TableResult
  order by Id, DeliveryDate, OutboundShipmentId, OrderNumber
end
go
IF OBJECT_ID('dbo.p_Planning_Order_Search') IS NOT NULL
    PRINT '<<< CREATED PROCEDURE dbo.p_Planning_Order_Search >>>'
ELSE
    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Planning_Order_Search >>>'
go


