﻿/*
      <summary>
        Procedure Name : p_Housekeeping_Palletise_Manual_Select
        Filename       : p_Housekeeping_Palletise_Manual_Select.sql
        Create By      : Grant Schultz
        Date Created   : 23 May 2007 13:00:00
      </summary>
      <remarks>
        Inserts palletised lines into the instruction table
      </remarks>
      <param>
        @WarehouseId         int,
        @OperatorId          int,
        @InstructionTypeCode nvarchar(10),
        @StorageUnitBatchId  int,
        @PickLocationId      int,
        @StoreLocationId     int,
        @Quantity            float,
        @Weight              float = null,
        @PriorityId          int = null
      </param>
      <returns>
        @error
      </returns>
      <newpara>
        Modified by    : 
        Modified Date  : 
        Details        : 
      </newpara>
*/
create procedure p_Housekeeping_Palletise_Manual_Select
(
 @StorageUnitBatchId  int = null,
 @PickLocationId      int = null
)
as
begin
	 set nocount on
  
  select StorageUnitBatchId,
         LocationId as 'PickLocationId',
         null as 'StoreLocationId',
         ActualQuantity as 'Quantity'
    from StorageUnitBatchLocation subl (nolock)
--   where StorageUnitBatchId = @StorageUnitBatchId
--     and LocationId = @PickLocationId
end
