﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Area_Search
  ///   Filename       : p_Housekeeping_Stock_Take_Area_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Area_Search
(
 @WarehouseId int
)
 
as
begin
	 set nocount on;
  
  select '-1' as 'AreaId',
         '{All}' as 'Area'
  union
  select distinct a.AreaId,
                  a.Area
    from Area             a (nolock)
    join AreaLocation    al (nolock) on a.AreaId            = al.AreaId
    join Instruction      i (nolock) on al.LocationId       = i.PickLocationId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status           s (nolock) on i.StatusId          = s.StatusId
   where it.InstructionTypeCode in ('STE','STL','STA','STP')
     and s.Type        = 'I'
     and s.StatusCode  = 'F'
     --and j.JobId       = isnull(@jobId, j.JobId)
     and i.WarehouseId = @WarehouseId
     and i.StoreLocationId is null
     and isnull(i.Stored,0) != 1 -- Stock is Finished (New count inserted)
     and isnull(i.Picked,0) != 1 -- Stock is Finished (New count inserted)
  order by 'Area'
end
