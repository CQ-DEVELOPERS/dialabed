﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Principal_Search
  ///   Filename       : p_Principal_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 08:15:18
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Principal table.
  /// </remarks>
  /// <param>
  ///   @PrincipalId int = null output,
  ///   @PrincipalCode nvarchar(60) = null,
  ///   @Principal nvarchar(510) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Principal.PrincipalId,
  ///   Principal.PrincipalCode,
  ///   Principal.Principal,
  ///   Principal.Email,
  ///   Principal.FirstName,
  ///   Principal.LastName,
  ///   Principal.Address1,
  ///   Principal.Address2,
  ///   Principal.Address3,
  ///   Principal.Address4,
  ///   Principal.Address5,
  ///   Principal.Address6 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Principal_Search
(
 @PrincipalId int = null output,
 @PrincipalCode nvarchar(60) = null,
 @Principal nvarchar(510) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @PrincipalCode = '-1'
    set @PrincipalCode = null;
  
  if @Principal = '-1'
    set @Principal = null;
  
 
  select
         Principal.PrincipalId
        ,Principal.PrincipalCode
        ,Principal.Principal
        ,Principal.Email
        ,Principal.FirstName
        ,Principal.LastName
        ,Principal.Address1
        ,Principal.Address2
        ,Principal.Address3
        ,Principal.Address4
        ,Principal.Address5
        ,Principal.Address6
    from Principal
   where isnull(Principal.PrincipalId,'0')  = isnull(@PrincipalId, isnull(Principal.PrincipalId,'0'))
     and isnull(Principal.PrincipalCode,'%')  like '%' + isnull(@PrincipalCode, isnull(Principal.PrincipalCode,'%')) + '%'
     and isnull(Principal.Principal,'%')  like '%' + isnull(@Principal, isnull(Principal.Principal,'%')) + '%'
  order by PrincipalCode
  
end
