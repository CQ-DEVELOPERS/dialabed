﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_Packaging_Delete
  ///   Filename       : p_BOM_Packaging_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Dec 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_Packaging_Delete
(
 @bomPackagingId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  delete BOMPackaging
   where BOMPackagingId = @bomPackagingId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_BOM_Packaging_Delete'
    rollback transaction
    return @Error
end
