﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportPOHeader_Delete
  ///   Filename       : p_InterfaceExportPOHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:08
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceExportPOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportPOHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportPOHeader_Delete
(
 @InterfaceExportPOHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceExportPOHeader
     where InterfaceExportPOHeaderId = @InterfaceExportPOHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
