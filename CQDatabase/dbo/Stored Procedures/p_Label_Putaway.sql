﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Label_Putaway
  ///   Filename       : p_Label_Putaway.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Label_Putaway
(
 @palletId      int = null,
 @instructionId int = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare @TableResult as table
  (
   PalletId           varchar(15),
   StorageUnitBatchId int,
   BatchId            int,
   Batch              varchar(50),
   SKUId              int,
   SKUCode            varchar(20),
   ProductId          int,
   ProductCode        varchar(30),
   Product            varchar(50),
   LocationId         int,
   Location           varchar(15),
   Date               varchar(10),
   Prints             varchar(10)
  )
  
  if @PalletId is not null
  begin
    insert @TableResult
          (PalletId,
           StorageUnitBatchId,
           LocationId,
           Date,
           Prints)
    select p.PalletId,
           i.StorageUnitBatchId,
           i.StoreLocationId,
           convert(varchar(10), @GetDate, 101),
           case when Prints > 1
                then 'Copy ' + convert(varchar(10), Prints)
                else 'Original'
                end
      from Instruction i (nolock)
      join Pallet      p (nolock) on i.PalletId = p.PalletId 
     where p.PalletId = @palletId
  end
  else if @instructionId is not null
  begin
    insert @TableResult
          (PalletId,
           StorageUnitBatchId,
           LocationId,
           Date,
           Prints)
    select p.PalletId,
           i.StorageUnitBatchId,
           i.StoreLocationId,
           convert(varchar(10), @GetDate, 101),
           case when Prints > 1
                then 'Copy ' + convert(varchar(10), Prints)
                else 'Original'
                end
      from Instruction i (nolock)
      join Pallet      p (nolock) on i.PalletId = p.PalletId 
     where i.InstructionId = @instructionId
  end
  
  update tr
     set BatchId   = sub.BatchId,
         SKUId     = su.SKUId,
         ProductId = su.ProductId
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product
    from @TableResult tr
    join Product       p (nolock) on tr.ProductId = p.ProductId
  
  update tr
     set SKUCode = sku.SKUCode
    from @TableResult tr
    join SKU         sku (nolock) on tr.SKUId = sku.SKUId
  
  update tr
     set Batch = b.Batch
    from @TableResult tr
    join Batch         b (nolock) on tr.BatchId = b.BatchId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.LocationId = l.LocationId
  
  select PalletId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Location,
         Date,
         Prints
    from @TableResult
end
