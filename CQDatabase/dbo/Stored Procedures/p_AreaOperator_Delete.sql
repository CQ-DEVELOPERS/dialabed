﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperator_Delete
  ///   Filename       : p_AreaOperator_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:46
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the AreaOperator table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @OperatorId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperator_Delete
(
 @AreaId int = null,
 @OperatorId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete AreaOperator
     where AreaId = @AreaId
       and OperatorId = @OperatorId
  
  select @Error = @@Error
  
  
  return @Error
  
end
