﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingColumn_List
  ///   Filename       : p_InterfaceMappingColumn_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 10:53:58
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceMappingColumn table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceMappingColumn.InterfaceMappingColumnId,
  ///   InterfaceMappingColumn.InterfaceMappingColumn 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingColumn_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceMappingColumnId
        ,'{All}' as InterfaceMappingColumn
  union
  select
         InterfaceMappingColumn.InterfaceMappingColumnId
        ,InterfaceMappingColumn.InterfaceMappingColumn
    from InterfaceMappingColumn
  order by InterfaceMappingColumn
  
end
