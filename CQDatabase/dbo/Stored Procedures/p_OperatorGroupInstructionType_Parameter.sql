﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupInstructionType_Parameter
  ///   Filename       : p_OperatorGroupInstructionType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroupInstructionType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OperatorGroupInstructionType.InstructionTypeId,
  ///   OperatorGroupInstructionType.OperatorGroupId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupInstructionType_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InstructionTypeId
        ,null as 'OperatorGroupInstructionType'
        ,null as OperatorGroupId
        ,null as 'OperatorGroupInstructionType'
  union
  select
         OperatorGroupInstructionType.InstructionTypeId
        ,OperatorGroupInstructionType.InstructionTypeId as 'OperatorGroupInstructionType'
        ,OperatorGroupInstructionType.OperatorGroupId
        ,OperatorGroupInstructionType.OperatorGroupId as 'OperatorGroupInstructionType'
    from OperatorGroupInstructionType
  
end
