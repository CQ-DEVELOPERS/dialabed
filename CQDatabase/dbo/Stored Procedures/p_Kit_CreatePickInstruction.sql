﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Kit_CreatePickInstruction
  ///   Filename       : p_Kit_CreatePickInstruction.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@BOMInstructionId>
  ///       <@OrderNumber>
  ///       <@StorageUnitId>
  ///       <@WarehouseId>
  ///       <@Quantity>>	 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Kit_CreatePickInstruction
(
     @BOMInstructionId  int
    ,@OrderNumber       varchar(50)
    ,@StorageUnitId     int
    ,@WarehouseId       int
    ,@Quantity          float
)
As
Begin 
    Declare        
         @ExternalCompanyId         int
        ,@IssueId                   int
        ,@OutboundDocumentId        int
        ,@OutboundDocumentTypeId    int
        ,@OutboundLineId            int
        ,@OutboundShipmentId        int
        ,@StorageUnitBatchId        int   
        ,@StatusId                  int
        ,@LineNumber                int
        ,@DeliveryDate              datetime
        ,@GetDate                   datetime
        ,@Error                     int
        ,@ErrorMsg                  varchar(100)
        ,@ProductCode               varchar(30)
        ,@Product                   varchar(255)
        ,@BatchId                   int
        ,@Batch                     nvarchar(50)
        ,@BOMHeaderId               int
        ,@Remarks                   nvarchar(max)
     
    select
         @OutboundDocumentTypeId = 8                --Pick better doc type
        ,@WarehouseId = 1                           --Double check this
        ,@StatusId = dbo.ufn_StatusId('OD','I')     
        ,@DeliveryDate = dbo.ufn_Getdate()
        ,@GetDate = dbo.ufn_Getdate()

    begin transaction KitPick
    
    select @ExternalCompanyId   = ExternalCompanyId
      from ExternalCompany (nolock)  
     where ExternalCompanyCode = 'KIT'
     
    if @ExternalCompanyId is null  
    begin  
      exec @Error = p_ExternalCompany_Insert  
       @ExternalCompanyId         = @ExternalCompanyId output,  
       @ExternalCompanyTypeId     = 1,  
       @RouteId                   = null,  
       --@drop  
       @ExternalCompany           = 'Internal kitting area',  
       @ExternalCompanyCode       = 'KIT'  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'  
        goto error_header  
      end  
    end
    
    select @StorageUnitId = bh.StorageUnitId,
           @Quantity      = bi.Quantity,
           @BOMHeaderId   = bh.BOMHeaderId
      from BOMInstruction bi (nolock)
      join BOMHeader      bh (nolock) on bi.BOMHeaderId = bh.BOMHeaderId
     where bi.BOMInstructionId = @BOMInstructionId
    
    select @Batch = replace(replace(convert(nvarchar(19), @Getdate, 127),'-',''),':','')
    
    exec @Error = p_Static_Batch_Insert
     @BatchId              = @BatchId output
    ,@StorageUnitId        = @StorageUnitId
    ,@OperatorId           = null
    ,@StatusId             = @StatusId
    ,@WarehouseId          = @WarehouseId
    ,@Batch                = @Batch
    
    exec @Error = p_OutboundDocument_Insert
       @OutboundDocumentId      = @OutboundDocumentId output
      ,@OutboundDocumentTypeId  = @OutboundDocumentTypeId
      ,@ExternalCompanyId       = @ExternalCompanyId
      ,@StatusId                = @StatusId
      ,@WarehouseId             = @WarehouseId
      ,@OrderNumber             = @OrderNumber
      ,@DeliveryDate            = @DeliveryDate
      ,@CreateDate              = @GetDate
      ,@ModifiedDate            = null
      ,@ReferenceNumber         = null
      ,@FromLocation            = null
      ,@ToLocation              = 'KBay1'
      ,@StorageUnitId           = @StorageUnitId
      ,@BatchId                 = @BatchId
      ,@Quantity                = @Quantity
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'
      goto error_header
    end
    
    set @StorageUnitId = null
    
-- Get Kit product
    
    declare kit_cursor cursor for
    select p.ProductCode
          ,p.Product
          ,1
          ,bp.Quantity * bil.Quantity
          ,ROW_NUMBER() OVER (order by bil.BOMLineId, bil.LineNumber) as 'LineNumber'
    from BOMInstruction            bi
    inner join BOMInstructionLine bil (nolock) on bi.BOMInstructionId = bil.BOMInstructionId
    inner join BOMProduct          bp (nolock) on bil.BOMLineId       = bp.BOMLineId
                                              and bil.LineNumber      = bp.LineNumber
    inner join StorageUnit         su (nolock) on su.StorageUnitId    = bp.StorageUnitId
    inner join Product              p (nolock) on su.ProductId        = p.ProductId
    where bi.BOMInstructionId = @BOMInstructionId
    union
    select p.ProductCode
          ,p.Product
          ,1
          ,bpk.Quantity
          ,0
    from BOMPackaging             bpk (nolock)
    inner join StorageUnit         su (nolock) on su.StorageUnitId    = bpk.StorageUnitId
    inner join Product              p (nolock) on su.ProductId        = p.ProductId
    where bpk.BOMInstructionId = @BOMInstructionId
    order by 'LineNumber'
    
    open kit_cursor
    
    fetch kit_cursor into @ProductCode
                         ,@Product
                         ,@WarehouseId
                         ,@Quantity
                         ,@LineNumber

    while (@@fetch_status = 0)
    begin
        
        exec @Error = p_interface_xml_Product_Insert
             @ProductCode        = @ProductCode
            ,@Product            = @Product
            ,@WarehouseId        = @WarehouseId
            ,@StorageUnitId      = @StorageUnitId output
            ,@StorageUnitBatchId = @StorageUnitBatchId output
           
        if @Error != 0 or @StorageUnitBatchId is null
        begin
            select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
            goto error_detail
        end
           
        exec @Error = p_OutboundLine_Insert
             @OutboundLineId     = @OutboundLineId output,
             @OutboundDocumentId = @OutboundDocumentId,
             @StorageUnitId      = @StorageUnitId,
             @StatusId           = @StatusId,
             @LineNumber         = @LineNumber,
             @Quantity           = @Quantity,
             --@Weight             = @di_weight * @Quatity,
             @BatchId            = null
             
        if @Error != 0
        begin
            select @ErrorMsg = 'Error executing p_OutboundLine_Insert'
            goto error_detail
        end
        
        fetch kit_cursor
         into  
             @ProductCode
            ,@Product
            ,@WarehouseId
            ,@Quantity
            ,@LineNumber
        
    end   

    close kit_cursor
    deallocate kit_cursor        
    
    if @Batch is null
      set @Batch = 'Pick instruction for kit'
    
    exec @Error = p_Despatch_Create_Issue
     @OutboundDocumentId = @OutboundDocumentId,
     @OperatorId         = null
     
    update BOMInstruction
    Set OutboundDocumentId = @OutboundDocumentId
    where BOMInstructionId = @BOMInstructionId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
      goto error_detail
    end
    
    select @IssueId = max(IssueId)
      from Issue (nolock)
     where OutboundDocumentId = @OutboundDocumentId
    
    select @Remarks = Remarks
      from BOMHeader (nolock)
     where BOMHeaderId = @BOMHeaderId
    
    exec @Error = p_Issue_Update
     @IssueId = @IssueId,
     @Remarks = @Remarks
    
    exec @Error = p_Outbound_Auto_Load
     @OutboundShipmentId = @OutboundShipmentId output,
     @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
      goto error_detail
    end
    
    exec @Error = p_Outbound_Auto_Release
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
      goto error_detail
    end
        
    error_detail:
        
        if (CURSOR_STATUS ('local','kit_cursor') = 1)
        begin
            print 'Closing failed cursor'
            close kit_cursor
            deallocate kit_cursor 
        end
    error_header:
    
    if @Error = 0
    begin     
      if @@trancount > 0
        commit transaction KitPick
      
    end
    else
    begin      
      if @@trancount > 0
        rollback transaction KitPick
      
    end
end
