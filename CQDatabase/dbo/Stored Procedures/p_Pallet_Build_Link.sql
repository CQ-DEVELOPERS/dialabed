﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Build_Link
  ///   Filename       : p_Pallet_Build_Link.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Build_Link
(
 @warehouseId int,
 @operatorId int,
 @barcode nvarchar(50),
 @referenceNumber nvarchar(50)
)
 
as
begin
  set nocount on;
  
  declare @Error              int = -1,
          @Errormsg           varchar(500) = 'Error executing p_Pallet_Build_Link',
          @GetDate            datetime,
          @Transaction        bit = 0,
          @JobId              int,
          @PriorityId         int,
          @StatusId           int,
          @StorageUnitBatchId int,
          @PickLocationId     int,
          @AcceptedQuantity   int,
          @ReceiptLineId      int,
          @PalletId           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(replace(@barcode,'P:','')) = 1 and @barcode like 'P:%'
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  select top 1
         @ReceiptLineId    = ReceiptLineId,
         @AcceptedQuantity = AcceptedQuantity,
         @PickLocationId   = r.LocationId,
         @StorageUnitBatchId = rl.StorageUnitBatchId
    from ReceiptLine       rl (nolock)
    join Receipt            r (nolock) on rl.ReceiptId = r.ReceiptId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where b.Batch = @referenceNumber
     and rl.AcceptedQuantity > 0
     and not exists(select top 1 1
                      from Instruction      i (nolock)
                      join Status           s (nolock) on i.StatusId = s.StatusId
                      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                     where i.ReceiptLineId = rl.ReceiptLineId
                       and i.StorageUnitBatchId = rl.StorageUnitBatchId
                      and it.InstructionTypeCode in ('S','SM','PR')
                      and s.StatusCode != 'Z')
  order by ReceiptLineId desc
  
  if @ReceiptLineId is null
    goto error
  
  if exists(select top 1 1
              from Instruction i
             where i.PalletId = @PalletId
               and i.ReceiptLineId != @ReceiptLineId)
  begin
    goto error
  end
  
  select @PriorityId = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'S'
  
  set @StatusId = dbo.ufn_StatusId('R','PR')
  
  select @JobId = j.JobId
    from Job         j (nolock)
    join Status      s (nolock) on j.StatusId = s.StatusId
                               and s.StatusCode in ('PR','R','A')
    join Instruction i (nolock) on j.JobId = i.JobId
   where i.PalletId = @PalletId
  
  if @JobId is null
  begin
    exec @Error = p_Job_Insert
     @JobId           = @JobId output,
     @PriorityId      = @PriorityId,
     @StatusId        = @StatusId,
     @WarehouseId     = @WarehouseId
    
    if @error <> 0
      goto error
  end
  
  exec @Error = p_Palletised_Insert
   @WarehouseId         = @WarehouseId,
   @OperatorId          = null,
   @InstructionTypeCode = 'S',
   @StorageUnitBatchId  = @StorageUnitBatchId,
   @PickLocationId      = @PickLocationId,
   @StoreLocationId     = null,
   @Quantity            = @AcceptedQuantity,
   @ReceiptLineId       = @ReceiptLineId,
   @PalletId            = @PalletId,
   @JobId               = @JobId,
   @AutoComplete        = 1
  
  if @Error <> 0
    goto error
   
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
