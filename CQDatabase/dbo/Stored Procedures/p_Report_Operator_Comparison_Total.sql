﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_Comparison_Total
  ///   Filename       : p_Report_Operator_Comparison_Total.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 08 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Operator_Comparison_Total
(
			@StartDate DateTime,
			@EndDate DateTime,
			@UOM	nvarchar(10) = 'Units',
			@OperatorGroupId int = null
)
 
as
begin
	 set nocount on;
	
Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int

Select 
 @DateHoursDiff = DateDiff(hh,@StartDate,@EndDate)
,@DateDaysDiff = DateDiff(D,@StartDate,@EndDate)
,@DateMonthsDiff = DateDiff(M,@StartDate,@EndDate) 

Declare @GroupPerformance As Table
(
	[OperatorGroupId]   int,
	[PerformanceType]	int,
	[Units]				float,
	[Weight]			float,
	[Instructions]		float,
	[Orders]			float,
	[OrderLines]		float,
	[Jobs]				int
)

Insert Into @GroupPerformance
(
	[OperatorGroupId]
	,[PerformanceType]
	 ,[Units]
	 ,[Weight]
	 ,[Instructions]
	 ,[Orders]
	 ,[OrderLines]
	 ,[Jobs]
)

(
Select		 O.OperatorGroupId,
			 GP.PerformanceType,
			Count(O.OperatorId) *  Sum(Distinct(GP.Units)) AS Units,
			Count(O.OperatorId) *  Sum(Distinct(GP.Weight)) As Weight,
			Count(O.OperatorId) *  Sum(Distinct(GP.Instructions)) As Instructions,
			Count(O.OperatorId) *  Sum(Distinct(GP.Orders)) As Orders,
			Count(O.OperatorId) *  Sum(Distinct(GP.OrderLines)) As OrderLines,
			Count(O.OperatorId) *  Sum(Distinct(GP.Jobs)) As Jobs	
FROM        OperatorPerformance Op INNER JOIN
                      Operator O ON Op.OperatorId = O.OperatorId INNER JOIN
					(SELECT 
						[OperatorGroupId]
					  ,[PerformanceType]
					  ,[Units]
					  ,[Weight]
					  ,[Instructions]
					  ,[Orders]
					  ,[OrderLines]
					  ,[Jobs]
							FROM [dbo].[GroupPerformance]
									Where OperatorGroupID = @OperatorGroupId
									And [PerformanceType] = 1
				Union
					SELECT [OperatorGroupId]
					  ,[PerformanceType]
					  ,[Units]
					  ,[Weight]
					  ,[Instructions]
					  ,[Orders]
					  ,[OrderLines]
					  ,[Jobs]
							FROM [dbo].[GroupPerformance]
									Where OperatorGroupID = @OperatorGroupId
									And [PerformanceType] = 2
					) AS GP ON O.OperatorGroupId = GP.OperatorGroupId 
Where Op.EndDate Between @StartDate And @EndDate
And O.OperatorGroupId = @OperatorGroupId
Group By O.OperatorGroupId,GP.[PerformanceType]
)


Declare @TempTable As Table
(
	OperatorGroupId			int,
	OperatorId				int,
	EndDate					dateTime,
	SiteTarget				int,
	IndustryTarget			int,
	Actual					int,
	UOM						nvarchar(10)
)

if (@UOM = 'Units')
Begin

Insert Into @TempTable (OperatorGroupId,OperatorId,EndDate,SiteTarget,IndustryTarget,Actual,UOM)
(
Select O.OperatorGroupID,OP.OperatorId,Op.EndDate,GPSite.Units,GPIndustry.Units,OP.Units,'Pieces'
	 FROM         OperatorPerformance OP
	Inner Join Operator O				on  O.OperatorId		= OP.OperatorId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From @GroupPerformance Where PerformanceType = 1) GPSite	on  GPSite.OperatorGroupId	= O.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From @GroupPerformance Where PerformanceType = 2) GPIndustry	on  GPIndustry.OperatorGroupId	= O.OperatorGroupId
	Where Op.EndDate Between @StartDate And  @EndDate
	And O.OperatorGroupID = isnull(@OperatorGroupId,O.OperatorGroupID)
	--Group BY OP.OperatorId,EndDate
						
)
End

if (@UOM = 'Weight')
Begin

Insert Into @TempTable (OperatorGroupId,OperatorId,EndDate,SiteTarget,IndustryTarget,Actual,UOM)
(
Select O.OperatorGroupID,OP.OperatorId,EndDate,GPSite.Weight,GPIndustry.Weight,OP.Weight,'KG'
	 FROM         OperatorPerformance OP
	Inner Join Operator O				on  O.OperatorId		= OP.OperatorId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From @GroupPerformance Where PerformanceType = 1) GPSite	on  GPSite.OperatorGroupId	= O.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From @GroupPerformance Where PerformanceType = 2) GPIndustry	on  GPIndustry.OperatorGroupId	= O.OperatorGroupId
	Where Op.EndDate Between @StartDate And  @EndDate	
	And O.OperatorGroupID = isnull(@OperatorGroupId,O.OperatorGroupID)
--Group BY OP.OperatorId,EndDate
						
)
End

if (@UOM = 'Jobs')
Begin

Insert Into @TempTable (OperatorGroupId,OperatorId,EndDate,SiteTarget,IndustryTarget,Actual,UOM)
(
Select O.OperatorGroupID,OP.OperatorId,EndDate,GPSite.Jobs,GPIndustry.Jobs,OP.Jobs,'Jobs'
	 FROM         OperatorPerformance OP
	Inner Join Operator O				on  O.OperatorId		= OP.OperatorId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From @GroupPerformance Where PerformanceType = 1) GPSite	on  GPSite.OperatorGroupId	= O.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From @GroupPerformance Where PerformanceType = 2) GPIndustry	on  GPIndustry.OperatorGroupId	= O.OperatorGroupId
	Where Op.EndDate Between @StartDate And  @EndDate
	And O.OperatorGroupID = isnull(@OperatorGroupId,O.OperatorGroupID)
	--Group BY OP.OperatorId,EndDate
						
)
End



if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
 					SELECT		--OperatorId, 
								--InstructionTypeId,
								Convert(nvarchar,EndDate,108) as EndDate, 
								Convert(nvarchar,EndDate,108) as ordscale, 
								OperatorGroupId,
								Convert(bigInt,SUM(Distinct(SiteTarget))) As SiteTarget,
								Convert(bigInt,SUM(Distinct(IndustryTarget))) As IndustryTarget,
								Convert(bigInt,SUM(Actual)) as Actual ,UOM 
							From @TempTable
							Group by OperatorGroupId,Convert(nvarchar,EndDate,108),Uom
							ORDER BY ordscale
						
						-- Select data by this Scale
					End
				Else
					Begin
						--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
 					SELECT		--OperatorId, 
					--InstructionTypeId,
							Convert(nvarchar,EndDate,101) as EndDate,
							Convert(nvarchar,EndDate,101) as ordscale, 
							OperatorGroupId,
								 8 *	Convert(bigInt,SUM(Distinct(SiteTarget))) As SiteTarget,
								 8 *	Convert(bigInt,SUM(Distinct(IndustryTarget))) As IndustryTarget,
										Convert(bigInt,SUM(Actual)) as Actual ,UOM 
							From @TempTable
							Group by OperatorGroupId,Convert(nvarchar,EndDate,101),Uom
							ORDER BY ordscale						
						-- Select data by this Scale
					End
			End
			Else
				Begin
					--Select '1 Week< x < 3 months' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
 					SELECT		--OperatorId, 
					--InstructionTypeId,
					DatePart(wk,EndDate) as EndDate,
					DatePart(wk,EndDate) as ordscale, 
					OperatorGroupId,
								5 * (8 *	Convert(bigInt,SUM(Distinct(SiteTarget)))) As SiteTarget,
								5 * (8 *	Convert(bigInt,SUM(Distinct(IndustryTarget)))) As IndustryTarget,
											Convert(bigInt,SUM(Actual)) as Actual ,UOM 
							From @TempTable
							Group by OperatorGroupId,DatePart(wk,EndDate),Uom
							ORDER BY ordscale		
				
					-- Select data by this Scale
				End
	End
Else
	Begin
			--Select  '3 months < x' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
				
			SELECT		--OperatorId, 
			--InstructionTypeId,
								DateName(M,EndDate) as EndDate,
								DatePart(M,EndDate) as ordscale,
								OperatorGroupId,
								22 * (8 *			Convert(bigInt,SUM(Distinct(SiteTarget)))) As SiteTarget,
								22 * (8 *			Convert(bigInt,SUM(Distinct(IndustryTarget)))) As IndustryTarget,
													Convert(bigInt,SUM(Actual)) as Actual ,UOM 
								UOM 
							From @TempTable
							Group by OperatorGroupId,DatePart(M,EndDate),DateName(M,EndDate),Uom
							ORDER BY ordscale
	End

  
end
