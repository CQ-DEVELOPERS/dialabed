﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentType_Parameter
  ///   Filename       : p_InboundDocumentType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 10:28:18
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundDocumentType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InboundDocumentType.InboundDocumentTypeId,
  ///   InboundDocumentType.InboundDocumentType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentType_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as InboundDocumentTypeId
        ,'{All}' as InboundDocumentType
  union
  select
         InboundDocumentType.InboundDocumentTypeId
        ,InboundDocumentType.InboundDocumentType
    from InboundDocumentType
  order by InboundDocumentType
  
end
