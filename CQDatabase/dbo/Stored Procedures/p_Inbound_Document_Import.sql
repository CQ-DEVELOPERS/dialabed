﻿--IF OBJECT_ID('dbo.p_Inbound_Document_Import') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Inbound_Document_Import
--    IF OBJECT_ID('dbo.p_Inbound_Document_Import') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Inbound_Document_Import >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Inbound_Document_Import >>>'
--END
--go
 
/*
  /// <summary>
  ///   Procedure Name : p_Inbound_Document_Import
  ///   Filename       : p_Inbound_Document_Import.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Sep 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Inbound_Document_Import
(
 @xmlstring nvarchar(max)
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Inbound_Document_Import',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @DocHandle         int,
          @Xmldoc            xml,
          @Warehouse               nvarchar(50),
          @GRVNumber               nvarchar(50),
          @Principal               nvarchar(50),
          @ExpectedReceiptDate     nvarchar(50),
          @GRVType                 nvarchar(50),
          @CustomerReferenceNumber nvarchar(50),
          @Carrier                 nvarchar(50),
          @CarrierReferenceNumber  nvarchar(50),
          @ExpectedQuantity        nvarchar(50),
          @ExpectedUOM             nvarchar(50),
          @GRVLineNumber           nvarchar(50),
          @InterfaceImportHeaderId int,
          @SealNumber              nvarchar(50),
          @ContainerNumber         nvarchar(50),
          @BillOfEntry             nvarchar(50),
          @Reference1              nvarchar(50),
          @Reference2              nvarchar(50),
          @Incoterms               nvarchar(50),
          @ProductCode             nvarchar(50),
          @Product                 nvarchar(255),
          @SkuCode                 nvarchar(50),
          @SKU                     nvarchar(255),
          @Batch                   nvarchar(50),
          @BOELineNumber           nvarchar(50),
          @AlternativeSKU          nvarchar(50),
          @CountryofOrigin         nvarchar(50),
          @Height                  nvarchar(50),
          @Width                   nvarchar(50),
          @Length                  nvarchar(50),
          @TariffCode              nvarchar(50),
          @LineReference1          nvarchar(50),
          @LineReference2          nvarchar(50),
          @ExpiryDate              nvarchar(50),
          @UnitPrice               nvarchar(50),
          @Weight                  nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
   
  select @GetDate = dbo.ufn_Getdate()
  
  set @Xmldoc = convert(xml,@xmlstring)
   
  EXEC sp_xml_preparedocument @DocHandle OUTPUT, @Xmldoc
   
  select @Warehouse               = Warehouse,
         @GRVNumber               = GRVNumber,
         @Principal               = Principal,
         @ExpectedReceiptDate     = case when isdate(ExpectedReceiptDate) = 1
                                         then ExpectedReceiptDate
                                         else null
                                         end,
         @GRVType                 = GRVType,
         @CustomerReferenceNumber = CustomerReferenceNumber,
         @Carrier                 = Carrier,
         @CarrierReferenceNumber  = CarrierReferenceNumber,
         
         @SealNumber              = SealNumber,
         @ContainerNumber         = ContainerNumber,
         @BillOfEntry             = BillOfEntry,
         @Reference1              = Reference1,
         @Reference2              = Reference2,
         @Incoterms               = Incoterms,
         
         @ProductCode             = ProductCode,
         @Product                 = Product,
         @SkuCode                 = SkuCode,
         @Sku          = Sku,
         @Batch                   = Batch,
         @ExpectedQuantity        = ExpectedQuantity,
         @ExpectedUOM             = ExpectedUOM,
         @GRVLineNumber           = GRVLineNumber,
         
         @BOELineNumber           = BOELineNumber,
         @AlternativeSKU          = AlternativeSKU,
         @CountryofOrigin         = CountryofOrigin,
         @Weight                  = case when isnumeric(Height) = 1
                                         then [Weight]
                                         end,
         @Height                  = case when isnumeric(Height) = 1
                                         then Height
                                         end,
         @Width                   = case when isnumeric(Width) = 1
                                         then Width
                                         end,
         @Length                  = case when isnumeric(Length) = 1
                                         then Length
                                         end,
         @TariffCode              = TariffCode,
         @LineReference1          = LineReference1,
         @LineReference2          = LineReference2,
         @ExpiryDate              = ExpiryDate,
         @UnitPrice               = case when isnumeric(UnitPrice) = 1
                                         then UnitPrice
                                         end
  
  from OPENXML (@Dochandle, '/root/Line',1)
              WITH (Warehouse nvarchar(50) 'Warehouse',
              GRVNumber nvarchar(50) 'GRVNumber',
              Principal nvarchar(50) 'Principal',
              ExpectedReceiptDate nvarchar(50) 'ExpectedReceiptDate',
              GRVType nvarchar(50) 'GRVType',
              CustomerReferenceNumber nvarchar(50) 'CustomerReferenceNumber',
              Carrier nvarchar(50) 'Carrier',
              CarrierReferenceNumber nvarchar(50) 'CarrierReferenceNumber',
              
              SealNumber nvarchar(50) 'SealNumber',
              ContainerNumber nvarchar(50) 'ContainerNumber',
              BillOfEntry nvarchar(50) 'BillOfEntry',
              Reference1 nvarchar(50) 'Reference1',
              Reference2 nvarchar(50) 'Reference2',
              Incoterms nvarchar(50) 'Incoterms',
              
              
              ProductCode nvarchar(50) 'ProductCode',
              Product nvarchar(50) 'Product',
              SkuCode nvarchar(50) 'SkuCode',
              SKU nvarchar(50) 'SKU',
              
              Batch nvarchar(50) 'Batch',
              ExpectedQuantity nvarchar(50) 'ExpectedQuantity',
              ExpectedUOM nvarchar(50) 'ExpectedUOM',
              GRVLineNumber nvarchar(50) 'GRVLineNumber',
              
              BOELineNumber    nvarchar(50) 'BOELineNumber',
              AlternativeSKU   nvarchar(50) 'AlternativeSKU',
              CountryofOrigin  nvarchar(50) 'CountryofOrigin',
              Weight           nvarchar(50) 'Weight',
              Height           nvarchar(50) 'Height',
              Width            nvarchar(50) 'Width',
              Length           nvarchar(50) 'Length',
              TariffCode       nvarchar(50) 'TariffCode',
              LineReference1   nvarchar(50) 'LineReference1',
              LineReference2   nvarchar(50) 'LineReference2',
              ExpiryDate       nvarchar(50) 'ExpiryDate',
              UnitPrice        nvarchar(50) 'UnitPrice'
              )
  
select @Warehouse               'Warehouse',
       @GRVNumber               'GRVNumber',
       @Principal               'Principal',
       @ExpectedReceiptDate     'ExpectedReceiptDate',
       @GRVType                 'GRVType',
       @CustomerReferenceNumber 'CustomerReferenceNumber',
       @Carrier                 'Carrier',
       @CarrierReferenceNumber  'CarrierReferenceNumber',
       @ProductCode             'ProductCode',
       @Product      'Product',
  @SkuCode                 'SkuCode',
       @Sku                     'Sku',
       @ExpectedQuantity        'ExpectedQuantity',
       @ExpectedUOM             'ExpectedUOM',
       @GRVLineNumber           'GRVLineNumber',
       @BOELineNumber           'BOELineNumber',
       @AlternativeSKU          'AlternativeSKU',
       @CountryofOrigin         'CountryofOrigin',
       @Height                  'Height',
       @Width                   'Width',
       @Length                  'Length',
       @TariffCode              'TariffCode',
       @LineReference1          'LineReference1',
       @LineReference2          'LineReference2',
       @ExpiryDate              'ExpiryDate',
       @UnitPrice               'UnitPrice'
  
  select @InterfaceImportHeaderId = max(InterfaceImportHeaderId)
    from InterfaceImportHeader
   where OrderNumber = @GRVNumber
     and PrincipalCode   = @Principal
     and InsertDate  > dateadd(mi, -1, @getdate)
     and RecordType in ('RCP',@GRVType)
     and isnull(Module, 'Inbound') = 'Inbound'
  
  if @InterfaceImportHeaderId is null
  begin
    insert InterfaceImportHeader
          (ToWarehouseCode,
           OrderNumber,
           PrincipalCode,
           DeliveryDate,
           RecordType,
           DeliveryNoteNumber,
           CompanyCode,
           Additional3,
           
           SealNumber,
           ContainerNumber,
           BillOfEntry,
           Reference1,
           Reference2,
           Incoterms,
           Module)
    select @Warehouse,
           @GRVNumber,
           @Principal,
           @ExpectedReceiptDate,
           @GRVType,
           @CustomerReferenceNumber,
           @Carrier,
           @CarrierReferenceNumber,
           
           @SealNumber,
           @ContainerNumber,
           @BillOfEntry,
           @Reference1,
           @Reference2,
           @Incoterms,
           'Inbound'
    
    select @Error = @@Error, @InterfaceImportHeaderId = scope_identity()
    
    if @Error <> 0
      goto error
  end
  
  insert InterfaceImportDetail
        (InterfaceImportHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         Quantity,
         Weight,
         RetailPrice,
         NetPrice,
         LineTotal,
         Volume,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         BOELineNumber,
         AlternativeSKU,
         CountryofOrigin,
         Height,
         Width,
         Length,
         TariffCode,
         Reference1,
         Reference2,
         ExpiryDate,
         UnitPrice)
  select @InterfaceImportHeaderId,
         @GRVNumber,
         @GRVLineNumber,
         @ProductCode,
         @Product,
         @SkuCode,
         @SKU,
         @Batch,
         @ExpectedQuantity,
         @Weight,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         @BOELineNumber,
         @AlternativeSKU,
         @CountryofOrigin,
         @Height,
         @Width,
         @Length,
         @TariffCode,
         @LineReference1,
         @LineReference2,
         @ExpiryDate,
         @UnitPrice
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
--go
--IF OBJECT_ID('dbo.p_Inbound_Document_Import') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Inbound_Document_Import >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Inbound_Document_Import >>>'
--go


