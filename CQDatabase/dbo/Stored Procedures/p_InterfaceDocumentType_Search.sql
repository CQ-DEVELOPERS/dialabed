﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentType_Search
  ///   Filename       : p_InterfaceDocumentType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 16:26:01
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceDocumentTypeId int = null output,
  ///   @InterfaceDocumentTypeCode nvarchar(60) = null,
  ///   @InterfaceDocumentType nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceDocumentType.InterfaceDocumentTypeId,
  ///   InterfaceDocumentType.InterfaceDocumentTypeCode,
  ///   InterfaceDocumentType.InterfaceDocumentType,
  ///   InterfaceDocumentType.RecordType,
  ///   InterfaceDocumentType.InterfaceType,
  ///   InterfaceDocumentType.InDirectory,
  ///   InterfaceDocumentType.OutDirectory,
  ///   InterfaceDocumentType.SubDirectory,
  ///   InterfaceDocumentType.FilePrefix 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentType_Search
(
 @InterfaceDocumentTypeId int = null output,
 @InterfaceDocumentTypeCode nvarchar(60) = null,
 @InterfaceDocumentType nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
  if @InterfaceDocumentTypeCode = '-1'
    set @InterfaceDocumentTypeCode = null;
  
  if @InterfaceDocumentType = '-1'
    set @InterfaceDocumentType = null;
  
 
  select
         InterfaceDocumentType.InterfaceDocumentTypeId
        ,InterfaceDocumentType.InterfaceDocumentTypeCode
        ,InterfaceDocumentType.InterfaceDocumentType
        ,InterfaceDocumentType.RecordType
        ,InterfaceDocumentType.InterfaceType
        ,InterfaceDocumentType.InDirectory
        ,InterfaceDocumentType.OutDirectory
        ,InterfaceDocumentType.SubDirectory
        ,InterfaceDocumentType.FilePrefix
    from InterfaceDocumentType
   where isnull(InterfaceDocumentType.InterfaceDocumentTypeId,'0')  = isnull(@InterfaceDocumentTypeId, isnull(InterfaceDocumentType.InterfaceDocumentTypeId,'0'))
     and isnull(InterfaceDocumentType.InterfaceDocumentTypeCode,'%')  like '%' + isnull(@InterfaceDocumentTypeCode, isnull(InterfaceDocumentType.InterfaceDocumentTypeCode,'%')) + '%'
     and isnull(InterfaceDocumentType.InterfaceDocumentType,'%')  like '%' + isnull(@InterfaceDocumentType, isnull(InterfaceDocumentType.InterfaceDocumentType,'%')) + '%'
  order by InterfaceDocumentTypeCode
  
end
