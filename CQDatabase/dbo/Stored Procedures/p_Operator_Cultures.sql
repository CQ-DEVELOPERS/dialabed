﻿/*
  /// <summary>
  ///   Procedure Name : p_Operator_Cultures
  ///   Filename       : p_Operator_Cultures.sql
  ///   Create By      : William
  ///   Date Created   : August 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Culture table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Operator.GroupId,
  ///   Operator.OperatorGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Cultures
 
as
begin
	 set nocount on;
  
	 declare @Error int
  select CultureId,
         CultureName
    from Culture
  order by CultureId
end
