﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Credit_Note_Search_Update
  ///   Filename       : p_Credit_Note_Search_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Credit_Note_Search_Update
(
 @WarehouseId           int = null,
 @InboundDocumentId     int = null,
 @InboundDocumentTypeId	int = null,
 @ExternalCompanyCode	  nvarchar(30) = null,
 @ExternalCompany	      nvarchar(255) = null,
 @OrderNumber	          nvarchar(30) = null,
 @FromDate	             datetime = null,
 @ToDate	               datetime = null
)
 
as
begin
	 set nocount on;
  
	 declare @StatusId int
  
  declare @TableResult as
  table(
        InboundDocumentId         int,
        OrderNumber               nvarchar(30),
        ExternalCompanyId         int,
        ExternalCompanyCode       nvarchar(30),
        ExternalCompany           nvarchar(255),
        NumberOfLines             int,
        DeliveryDate              datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        InboundDocumentTypeId     int,
        InboundDocumentType       nvarchar(30),
        InboundDocumentTypeCode   nvarchar(10),
        ReferenceNumber           nvarchar(30),
        Remarks                   nvarchar(255)
       );
  
  select @StatusId = dbo.ufn_StatusId('ID','N');
  
  if @InboundDocumentTypeId = -1
    set @InboundDocumentTypeId = null
  
  if @InboundDocumentId = -1
    set @InboundDocumentId = null
  
  if @InboundDocumentId is null
  begin
    insert @TableResult
          (InboundDocumentId,
           OrderNumber,
           ExternalCompanyId,
           ExternalCompanyCode,
           ExternalCompany,
           Status,
           InboundDocumentType,
           InboundDocumentTypeCode,
           DeliveryDate,
           CreateDate,
           InboundDocumentTypeId,
           ReferenceNumber)
    select id.InboundDocumentId,
           id.OrderNumber,
           id.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           s.Status,
           idt.InboundDocumentType,
           idt.InboundDocumentTypeCode,
           id.DeliveryDate,
           id.CreateDate,
           id.InboundDocumentTypeId,
           id.ReferenceNumber
      from InboundDocument     id  (nolock)
      join ExternalCompany     ec  (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
      join Status              s   (nolock) on id.StatusId          = s.StatusId
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where id.InboundDocumentId        = isnull(@InboundDocumentId, id.InboundDocumentId)
       and id.InboundDocumentTypeId    = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
       and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
       and id.DeliveryDate       between isnull(@FromDate, id.DeliveryDate) and isnull(@ToDate, id.DeliveryDate)
       and s.StatusId                  = @StatusId
       and id.WarehouseId              = @WarehouseId
       and id.WarehouseId               = isnull(@WarehouseId, id.WarehouseId)
  end
  else
  begin
    insert @TableResult
          (InboundDocumentId,
           OrderNumber,
           ExternalCompanyId,
           ExternalCompanyCode,
           ExternalCompany,
           Status,
           InboundDocumentType,
           InboundDocumentTypeCode,
           DeliveryDate,
           CreateDate,
           InboundDocumentTypeId,
           ReferenceNumber)
    select id.InboundDocumentId,
           id.OrderNumber,
           id.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           s.Status,
           idt.InboundDocumentType,
           idt.InboundDocumentTypeCode,
           id.DeliveryDate,
           id.CreateDate,
           id.InboundDocumentTypeId,
           id.ReferenceNumber
      from InboundDocument     id  (nolock)
      join ExternalCompany     ec  (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
      join Status              s   (nolock) on id.StatusId          = s.StatusId
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where id.InboundDocumentId        = @InboundDocumentId
  end
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from InboundLine il
                           where t.InboundDocumentId = il.InboundDocumentId)
    from @TableResult t
   where InboundDocumentTypeCode = 'RET'
  
  update @TableResult
     set Remarks = '<h3 style="color:Red"> * Please insert reference number</h3>'
   where InboundDocumentTypeCode = 'RET'
     and ReferenceNumber is null
  
  update tr
     set Remarks = '<h6 style="color:LimeGreen"> * Valid</h6>'
    from @TableResult tr
   where InboundDocumentTypeCode = 'RET'
     and ReferenceNumber is not null
     and exists(select top 1 1 from InterfaceInvoice ii where ii.InvoiceNumber = tr.ReferenceNumber)
  
  update tr
     set Remarks = '<h3 style="color:Red"> * Does not exist on WMS</h3>'
    from @TableResult tr
   where InboundDocumentTypeCode = 'RET'
     and ReferenceNumber is not null
     and Remarks is null
  
  select InboundDocumentId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         Status,
         InboundDocumentType,
         InboundDocumentTypeId,
         ExternalCompanyId,
         ReferenceNumber,
         Remarks
    from @TableResult
end
