﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportOrderPick_Delete
  ///   Filename       : p_InterfaceExportOrderPick_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:04
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceExportOrderPick table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportOrderPickId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportOrderPick_Delete
(
 @InterfaceExportOrderPickId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceExportOrderPick
     where InterfaceExportOrderPickId = @InterfaceExportOrderPickId
  
  select @Error = @@Error
  
  
  return @Error
  
end
