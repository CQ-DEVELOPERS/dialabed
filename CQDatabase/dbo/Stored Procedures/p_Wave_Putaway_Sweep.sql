﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Putaway_Sweep
  ///   Filename       : p_Wave_Putaway_Sweep.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Sep 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Putaway_Sweep
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Wave_Putaway_Sweep',
          @GetDate           datetime,
          @Transaction       bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  --if @@trancount = 0
  --  begin
  --    begin transaction
  --    set @Transaction = 1
  --  end
  
  -- Update ReservedQuantity
  update subl
     set ReservedQuantity = isnull((select sum(isnull(i.ConfirmedQuantity,0))
                               from Instruction i (nolock)
                               join Status      s (nolock) on i.StatusId = s.StatusId
                              where s.StatusCode        in ('W','S')
                                and i.Picked             = 0
                                and i.CreateDate         > '2013-06-01'
                                and i.StorageUnitBatchId = subl.StorageUnitBatchId
                                and i.PickLocationId     = subl.LocationId),0)
    from StorageUnitBatchLocation subl

  -- Update AllocatedQuantity
  update subl
     set AllocatedQuantity = isnull((select sum(isnull(i.ConfirmedQuantity,0))
                               from Instruction i (nolock)
                               join Status      s (nolock) on i.StatusId = s.StatusId
                              where s.StatusCode        in ('W','S')
                                and i.Stored             = 0
                                and i.CreateDate         > '2013-06-01'
                                and i.StorageUnitBatchId = subl.StorageUnitBatchId
                                and i.StoreLocationId    = subl.LocationId),0)
    from StorageUnitBatchLocation subl
  
  delete StorageUnitBatchLocation where ActualQuantity = 0 and AllocatedQuantity = 0 and ReservedQuantity = 0

  --drop table #LOC

  select distinct LocationId
    into #LOC
    from viewSOH soh1
   where Area = 'WavePick'
     and not exists(select top 1 1 from viewSOH soh2 where soh1.LocationId = soh2.LocationId and soh2.ReservedQuantity > 0 and soh2.ActualQuantity > 0)
     and exists(select top 1 1 from viewSOH soh3 where soh1.LocationId = soh3.LocationId and soh3.ActualQuantity > 0)
     and not exists(select top 1 1 from viewInstruction vi where vi.InstructionTypeCode in ('PM','PS','FM') and soh1.StorageUnitId = vi.StorageUnitId and InstructionCode in ('W','S'))
     and StockTakeInd = 0
  order by LocationId

  delete l
    from #LOC l
    join viewSOH soh on l.LocationId = soh.LocationId
    where exists(select top 1 1 from viewInstruction vi where vi.StorageUnitId = soh.StorageUnitId and vi.InstructionTypeCode in ('PM','PS','FM') and InstructionCode in ('W','S'))
      and soh.ActualQuantity > 0
  
  select Location, SKUCode, ActualQuantity, AllocatedQuantity, ReservedQuantity from viewSOH where LocationId in (select LocationId from #LOC) order by Location

  declare @LocationId int 

  while exists(select top 1 1 from #LOC)
  begin
    select top 1 @LocationId = LocationId
      from #LOC
    delete #LOC where LocationId = @LocationId
      
    select LocationId, StorageUnitBatchId, SKUCode, ActualQuantity, AllocatedQuantity, ReservedQuantity
      into #SOH
      from viewSOH where LocationId = @LocationId

    select InstructionType, JobId, InstructionId, InstructionRefId, JobCode, InstructionCode, PalletId, SKUCode, ConfirmedQuantity, PickLocation, StoreLocation, MostRecent, StoreLocationId, StorageUnitBatchId
      into #INS
      from viewInstruction vi where vi.JobId in (select max(JobId) from viewINstruction where StoreLocationId = @LocationId and (InstructionTypeCode = 'M' or InstructionTypeCode like 'ST%') and InstructionCode not in ('W','S'))
    
    delete soh
      from #SOH soh
      join #INS ins on soh.LocationId = ins.StoreLocationId
                   and soh.StorageUnitBatchId = ins.StorageUnitBatchId

    if not exists(select top 1 1 from #SOH)
    begin
      --select * from #INS
      exec p_Wave_Planning_Move_Items_Putaway @LocationId = @LocationId
      select 'WAVE!!!'
    end
    else
    begin
      select * from #SOH
      select * from #INS
      
      exec p_Housekeeping_Stock_Take_Create_Product
       @warehouseId        = 1,
       @operatorId         = 2,
       @locationId         = @LocationId
    end
    
    drop table #SOH
    drop table #INS
  end

  --select *
  --  from viewInstruction vi 
  --  left
  --  join viewSOH        soh on vi.StorageLocationId = soh.LocationId
  -- where vi.JobId in (select max(JobId) from viewINstruction where StoreLocationId = 15803 and InstructionTypeCode = 'M')

  --return

  --select count(distinct(LocationId))
  --  from viewSOH soh1
  -- where Area = 'WavePick'
  --   and not exists(select top 1 1 from viewSOH soh2 where soh1.LocationId = soh2.LocationId and soh2.ReservedQuantity > 0)
  --   and LocationId != 15830
  --   and StockTakeInd = 0

  --select distinct(LocationId)
  --  from viewSOH soh1
  -- where Area = 'WavePick'
  --   and not exists(select top 1 1 from viewSOH soh2 where soh1.LocationId = soh2.LocationId and soh2.ReservedQuantity > 0)
  --   and LocationId != 15830
  --   and StockTakeInd = 0
  --order by LocationId
  
  if @Error <> 0
    goto error
  
  result:
      --if @Transaction = 1
      --  commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        --rollback transaction
      end
      return @Error
end
