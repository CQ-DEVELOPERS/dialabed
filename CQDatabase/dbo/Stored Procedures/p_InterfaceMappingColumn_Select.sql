﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingColumn_Select
  ///   Filename       : p_InterfaceMappingColumn_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 10:53:59
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceMappingColumn table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingColumnId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceMappingColumn.InterfaceMappingColumnId,
  ///   InterfaceMappingColumn.InterfaceMappingColumnCode,
  ///   InterfaceMappingColumn.InterfaceMappingColumn,
  ///   InterfaceMappingColumn.InterfaceDocumentTypeId,
  ///   InterfaceMappingColumn.InterfaceFieldId,
  ///   InterfaceMappingColumn.ColumnNumber,
  ///   InterfaceMappingColumn.StartPosition,
  ///   InterfaceMappingColumn.EndPostion,
  ///   InterfaceMappingColumn.Format,
  ///   InterfaceMappingColumn.InterfaceMappingFileId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingColumn_Select
(
 @InterfaceMappingColumnId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceMappingColumn.InterfaceMappingColumnId
        ,InterfaceMappingColumn.InterfaceMappingColumnCode
        ,InterfaceMappingColumn.InterfaceMappingColumn
        ,InterfaceMappingColumn.InterfaceDocumentTypeId
        ,InterfaceMappingColumn.InterfaceFieldId
        ,InterfaceMappingColumn.ColumnNumber
        ,InterfaceMappingColumn.StartPosition
        ,InterfaceMappingColumn.EndPostion
        ,InterfaceMappingColumn.Format
        ,InterfaceMappingColumn.InterfaceMappingFileId
    from InterfaceMappingColumn
   where isnull(InterfaceMappingColumn.InterfaceMappingColumnId,'0')  = isnull(@InterfaceMappingColumnId, isnull(InterfaceMappingColumn.InterfaceMappingColumnId,'0'))
  order by InterfaceMappingColumnCode
  
end
