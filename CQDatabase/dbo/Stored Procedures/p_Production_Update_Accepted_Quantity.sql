﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Update_Accepted_Quantity
  ///   Filename       : p_Production_Update_Accepted_Quantity.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Mar 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Update_Accepted_Quantity
(
 @InstructionId int,
 @Sign          char(2)
)
 
as
begin
	 set nocount on;
  
  declare @Error                   int,
          @Errormsg                nvarchar(500),
          @GetDate                 datetime,
          @WarehouseId             int,
          @ReceiptLineId           int,
          @OrderNumber             nvarchar(30),
          @InboundDocumentTypeCode nvarchar(10),
          @DeliveryNoteNumber      nvarchar(30),
          @DeliveryDate            datetime,
          @InterfaceExportPOHeaderId int,
          @StorageUnitBatchId      int,
          @StorageUnitId		   int,
          @ConfirmedQuantity       float,
          @transaction             bit,
          @OldReceiptLineId        int,
          @ProductionQuantity      float,
          @PackTypeId              int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  if @@TRANCOUNT > 0
    set @transaction = 1
  else
    set @transaction = 0
  
  if @transaction = 0
    begin transaction
  
  select @WarehouseId        = WarehouseId,
         @StorageUnitBatchId = StorageUnitBatchId,
         @ConfirmedQuantity  = ConfirmedQuantity,
         @ReceiptLineId      = ReceiptLineId
         --@OldReceiptLineId   = ReceiptLineId
    from Instruction (nolock) 
   where InstructionId = @InstructionId
  
  set @ProductionQuantity = 1
  
  Select @StorageUnitId      = s.StorageUnitId,
		       @ProductionQuantity = Isnull(p.ProductionQuantity,1)
  From StorageUnitBatch s (nolock) 
		Join Pack             p (nolock) on p.StorageUnitId = s.StorageUnitId	
	where s.StorageUnitBatchId = @StorageUnitBatchId
			and p.PackTypeId = 2
  
  Set @ConfirmedQuantity = Floor(@ConfirmedQuantity * @ProductionQuantity)
  
  /*
  select @ReceiptLineId = rl.ReceiptLineId
    from ReceiptLine rl (nolock)
    join Receipt      r (nolock) on rl.ReceiptId = r.ReceiptId
    join Status       s (nolock) on rl.StatusId = s.StatusId
   where r.WarehouseId         = @WarehouseId
     and rl.ReceiptLineId     != @OldReceiptLineId
     and rl.StorageUnitBatchId = @StorageUnitBatchId
     and s.StatusCode         in ('W','R')
  
  if @ReceiptLineId is null
  begin
    set @Error = -1
    goto Error
  end
  */
  select top 1 @PackTypeId = PackTypeId -- try get unit value
    from PackType (nolock)
  order by InboundSequence desc
  
  update subl
     set ActualQuantity = case when subl.ActualQuantity - ((isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0))/ISNULL(pc.ProductionQuantity,1)) < 0
                               then 0
                               else subl.ActualQuantity - ((isnull(rl.AcceptedQuantity,0)  - isnull(rl.SampleQuantity,0))/ISNULL(pc.ProductionQuantity,1))
                               end
    from ReceiptLine                rl
 	  Join StorageUnitBatch          sub on sub.StorageUnitBatchId  = rl.StorageUnitBatchId
    left join Pack                  pc on pc.StorageUnitId        = sub.StorageUnitId
                                      and pc.PackTypeId           = @PackTypeId
    join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
								                              and subl.LocationId         = r.LocationId
   where rl.ReceiptLineId = @ReceiptLineId
  
  if @Sign = '-'
  begin
    update ReceiptLine
       set AcceptedQuantity = ISNULL(AcceptedQuantity,0) - @ConfirmedQuantity
     where ReceiptLineId = @ReceiptLineId
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
    end
  end
  
  if @Sign = '+'
  begin
    update ReceiptLine
       set AcceptedQuantity = ISNULL(AcceptedQuantity,0) + @ConfirmedQuantity
     where ReceiptLineId = @ReceiptLineId
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
  end
  
  update subl
     set ActualQuantity = subl.ActualQuantity + ((isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0))/ISNULL(pc.ProductionQuantity,1))
    from ReceiptLine                rl
    join StorageUnitBatch          sub on sub.StorageUnitBatchId   = rl.StorageUnitBatchId
    left join Pack                  pc on pc.StorageUnitId         = sub.StorageUnitId
                                      and pc.PackTypeId            = @PackTypeId
    join Receipt                     r on rl.ReceiptId             = r.ReceiptId
    join StorageUnitBatchLocation subl on subl.StorageUnitBatchId  = rl.StorageUnitBatchId
				                                  and subl.LocationId          = r.LocationId
   where rl.ReceiptLineId = @ReceiptLineId
  
  if @@rowcount = 0
  begin
    insert StorageUnitBatchLocation
          (StorageUnitBatchId,
           LocationId,
           ActualQuantity,
           AllocatedQuantity,
           ReservedQuantity)
    select rl.StorageUnitBatchId,
           r.LocationId,
           (rl.AcceptedQuantity  - isnull(rl.SampleQuantity,0))/ISNULL(pc.ProductionQuantity,1),
           0,
           0
      from ReceiptLine                rl
      join Receipt                     r on rl.ReceiptId            = r.ReceiptId
      Join StorageUnitBatch          sub on sub.StorageUnitBatchId  = rl.StorageUnitBatchId
      left join Pack                  pc on pc.StorageUnitId        = sub.StorageUnitId
                                        and pc.PackTypeId           = @PackTypeId
     where rl.ReceiptLineId = @ReceiptLineId
       and r.LocationId is not null
       and rl.AcceptedQuantity > 0
  
  if @transaction = 0
    commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Production_Update_Accepted_Quantity'
    if @transaction = 0
      rollback transaction
    return @Error
end
