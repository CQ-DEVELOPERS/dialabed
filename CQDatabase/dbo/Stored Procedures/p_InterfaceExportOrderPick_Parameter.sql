﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportOrderPick_Parameter
  ///   Filename       : p_InterfaceExportOrderPick_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportOrderPick table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceExportOrderPick.InterfaceExportOrderPickId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportOrderPick_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceExportOrderPickId
        ,null as 'InterfaceExportOrderPick'
  union
  select
         InterfaceExportOrderPick.InterfaceExportOrderPickId
        ,InterfaceExportOrderPick.InterfaceExportOrderPickId as 'InterfaceExportOrderPick'
    from InterfaceExportOrderPick
  
end
