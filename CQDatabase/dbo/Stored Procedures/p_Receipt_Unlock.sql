﻿CREATE PROCEDURE p_Receipt_Unlock (@ReceiptId INT)
AS
BEGIN
	SET NOCOUNT ON;

	IF @ReceiptId IS NULL
	BEGIN
		RETURN
	END
	ELSE
	BEGIN
		UPDATE	Receipt
		SET		Locked = 0
		WHERE	ReceiptId = @ReceiptId
	END
END
