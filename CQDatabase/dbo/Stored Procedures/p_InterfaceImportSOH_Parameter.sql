﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOH_Parameter
  ///   Filename       : p_InterfaceImportSOH_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:12
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportSOH table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportSOH.InterfaceImportSOHId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOH_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceImportSOHId
        ,null as 'InterfaceImportSOH'
  union
  select
         InterfaceImportSOH.InterfaceImportSOHId
        ,InterfaceImportSOH.InterfaceImportSOHId as 'InterfaceImportSOH'
    from InterfaceImportSOH
  
end
