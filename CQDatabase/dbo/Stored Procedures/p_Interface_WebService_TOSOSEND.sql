﻿create procedure [dbo].[p_Interface_WebService_TOSOSEND]
(
 @doc varchar(max) = null output
)
as
begin
  set nocount on
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @PrimaryKey           varchar(30),
          @OrderNumber          varchar(30),
          @Remarks              varchar(255),
          @Intransit            varchar(255),
          @Status               varchar(255),
          @DeliveryNoteNumber   varchar(255),
          @DateIssued           varchar(20),
          @DateReceived         varchar(20),
          @ProcessedDate        varchar(20),
          @FromWarehouseCode    varchar(10),
          @ToWarehouseCode      varchar(10),
          @LineNumber           varchar(10),
          @ProductCode          varchar(30),
          @Batch                varchar(50),
          @ExpiryDate			Varchar(20),
          @Quantity             float,
          @fQtyReceived         varchar(255),
          @fQtyDamaged          varchar(255),
          @fQtyVariance         varchar(255),
          -- Internal
          @fetch_status_header  int,
          @fetch_status_detail  int,
          @print_string         varchar(255),
          @InterfaceExportSOHeaderId int,
          @IssueId                   int,
          @WarehouseId               int,
          @HostId                    int,
          @iWhseIDVariance           varchar(30),
          @iWhseIDDamaged            varchar(30),
          @CreateOnly				varchar(10),
          @Detail					int
  set @Detail = 0
  select @GetDate = dbo.ufn_Getdate()
 -- update d set Additional5 = '1' 
	--from
	--InterfaceMessage m
	--	 Join InterfaceExportSODetail d on d.InterfaceExportSOHeaderId = m.InterfaceId and d.LineNumber = InterfaceMessage
	--where OrderNumber like  'IBT%' and ISNUMERIC(m.InterfaceMessage) = 1 AND M.InterfaceMessageCode = 'Processed'
  
  declare extract_header_cursor cursor for
  select top 1
         InterfaceExportSOHeaderId,
         PrimaryKey,
         OrderNumber,
         isnull(Remarks,''),
         isnull(FromWarehouseCode,''),
         isnull(ToWarehouseCode,''),
         isnull(Additional4,''),
         isnull(Additional10,''),
         isnull(Additional5,''),
         isnull(Additional6,''),
         isnull(Additional7,''),
         convert(varchar(10), isnull(DeliveryDate, @Getdate), 111),
         isnull(Additional3,''),
         IssueId
    from InterfaceExportSOHeader
   where RecordStatus = 'N' and
		 RecordType = 'IBT'
  order by PrimaryKey
  open extract_header_cursor
  fetch extract_header_cursor into @InterfaceExportSOHeaderId,
                                   @PrimaryKey,
                                   @OrderNumber,
                                   @Remarks,
                                   @FromWarehouseCode,
                                   @ToWarehouseCode,
                                   @Intransit,
                                   @Status,
                                   @DeliveryNoteNumber,
                                   @iWhseIDVariance,
                                   @iWhseIDDamaged,
                                   @DateIssued,
                                   @DateReceived,
                                   @IssueId
	set @doc = '<?xml version="1.0" encoding="Windows-1252" standalone="yes" ?>'
    set @doc = @doc + '<root>'                                   
  select @fetch_status_header = @@fetch_status
  while (@fetch_status_header = 0)
  begin
    update InterfaceExportSOHeader
       set RecordStatus = 'Y',
           ProcessedDate = @GetDate
     where InterfaceExportSOHeaderId  = @InterfaceExportSOHeaderId
    select @WarehouseId = WarehouseId from Issue (nolock) where IssueId = @IssueId
    select @HostId = HostId from Warehouse where WarehouseId = @WarehouseId
    set @Detail = 1
    set @doc = @doc + '<TransferOrder>'
    set @doc = @doc + '<IDWhseIBT>' + @PrimaryKey + '</IDWhseIBT>'
    set @doc = @doc + '<InterfaceTable>InterfaceExportSOHeader</InterfaceTable>'
    set @doc = @doc + '<InterfaceID>' + Convert(char(20),@InterfaceExportSOHeaderId) + '</InterfaceID>'
    set @doc = @doc + '<cIBTNumber>' + @OrderNumber + '</cIBTNumber>'
    set @doc = @doc + '<cIBTDescription>' + @Remarks + '</cIBTDescription>'
    set @doc = @doc + '<iWhseIDFrom>' + @FromWarehouseCode + '</iWhseIDFrom>'
    set @doc = @doc + '<iWhseIDTo>' + @ToWarehouseCode + '</iWhseIDTo>'
    set @doc = @doc + '<iWhseIDIntransit>' + @Intransit + '</iWhseIDIntransit>'
    set @doc = @doc + '<iWhseIDVariance>' + @iWhseIDVariance + '</iWhseIDVariance>'
    set @doc = @doc + '<iWhseIDDamaged>' + @iWhseIDDamaged + '</iWhseIDDamaged>'
    set @doc = @doc + '<iIBTStatus>1</iIBTStatus>'
    set @doc = @doc + '<cDelNoteNumber>' + @DeliveryNoteNumber + '</cDelNoteNumber>'
    set @doc = @doc + '<dDateIssued>' + @DateIssued + '</dDateIssued>'
    set @doc = @doc + '<dDateReceived>' + @DateReceived + '</dDateReceived>'
    declare extract_detail_cursor cursor for
    select convert(varchar(10), LineNumber),
           isnull(ProductCode, ''),
           isnull(Batch,''),
           sum(Quantity),
           isnull(Additional1,'0'),
           isnull(Additional2,'0'),
           isnull(Additional3,'0'),
           isnull(Additional4,''),
           isnull(Additional5,'0')
      from InterfaceExportSODetail
     where InterfaceExportSOHeaderId = @InterfaceExportSOHeaderId
     Group by convert(varchar(10), LineNumber),
           isnull(ProductCode, ''),
           isnull(Batch,''),
           isnull(Additional1,'0'),
           isnull(Additional2,'0'),
           isnull(Additional3,'0'),
           isnull(Additional4,''),
           isnull(Additional5,'0')
     order by convert(varchar(10), LineNumber)
    open extract_detail_cursor
    fetch extract_detail_cursor into @LineNumber,
                                     @ProductCode,
                                     @Batch,
                                     @Quantity,
                                     @fQtyReceived,
                                     @fQtyDamaged,
                                     @fQtyVariance,
                                     @ExpiryDate,
                                     @CreateOnly
    
    select @fetch_status_detail = @@fetch_status
    while (@fetch_status_detail = 0)
    begin
      set @doc = @doc + '  <TransferOrderLine>'
      set @doc = @doc + '    <IDWhseIBTLines>' + @LineNumber + '</IDWhseIBTLines>'
      set @doc = @doc + '		  <iWhseIBTID>' + @PrimaryKey + '</iWhseIBTID>'
      set @doc = @doc + '    <iStockID>' + @ProductCode + '</iStockID>'
      set @doc = @doc + '    <cLotCode>' + @Batch + '</cLotCode>'
      set @doc = @doc + '    <ExpiryDate>' + @ExpiryDate + '</ExpiryDate>'
      set @doc = @doc + '    <CreateOnly>' + @CreateOnly + '</CreateOnly>'
      set @doc = @doc + '    <fQtyIssued>' + convert(char(12),@Quantity) + '</fQtyIssued>'
      set @doc = @doc + '    <fQtyReceived>' + @fQtyReceived + '</fQtyReceived>'
      set @doc = @doc + '    <fQtyDamaged>' + @fQtyDamaged + '</fQtyDamaged>'
      set @doc = @doc + '    <fQtyVariance>' + @fQtyVariance + '</fQtyVariance>'
      set @doc = @doc + '  </TransferOrderLine>'
      fetch extract_detail_cursor into @LineNumber,
                                       @ProductCode,
                                       @Batch,
                                       @Quantity,
                                       @fQtyReceived,
                                       @fQtyDamaged,
                                       @fQtyVariance,
									   @ExpiryDate,
                                       @CreateOnly
      select @fetch_status_detail = @@fetch_status
    end
    close extract_detail_cursor
    deallocate extract_detail_cursor
    set @doc = @doc + '</TransferOrder>'
    fetch extract_header_cursor into @InterfaceExportSOHeaderId,
                                     @PrimaryKey,
                                     @OrderNumber,
                                     @Remarks,
                                     @FromWarehouseCode,
                                     @ToWarehouseCode,
                                     @Intransit,
                                     @Status,
                                     @DeliveryNoteNumber,
                                     @iWhseIDVariance,
                                     @iWhseIDDamaged,
                                     @DateIssued,
                                     @DateReceived,
                                     @IssueId
    
    select @fetch_status_header = @@fetch_status
  end
  close extract_header_cursor
  deallocate extract_header_cursor
  set @doc = @doc + '</root>'
  if @Detail = 0
	Set @doc = ''
end




