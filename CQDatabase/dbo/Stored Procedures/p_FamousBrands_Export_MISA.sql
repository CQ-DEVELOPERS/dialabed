﻿/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_MISA
  ///   Filename       : p_FamousBrands_Export_MISA.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_FamousBrands_Export_MISA
(
 @FileName varchar(30) = null output
)
as
begin
  set nocount on
  
  declare @TableResult as table
  (
   Data varchar(500)
  )
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @ProcessedDate        varchar(20),
          @FromWarehouseCode    varchar(10),
          @ProductCode          varchar(30),
          @Quantity             varchar(10),
          @ReasonCode           char(1),
          @Batch                varchar(50),
          -- Internal
          @fetch_status_header  int,
          @fetch_status_detail  int,
          @print_string         varchar(255)
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare extract_header_cursor cursor for
  select isnull(Additional1, 'G200'),
         ProductCode,
         convert(varchar(10), case when Quantity >= 0
                                   then Quantity
                                   else Quantity * -1
                                   end),
         case when Quantity >= 0
              then '1'
              else '2'
              end,
         convert(varchar(20), isnull(ProcessedDate, @Getdate), 120),
         Batch
    from InterfaceExportStockAdjustment
   where RecordType = 'MISA'
     and RecordStatus = 'N'
  order by Additional1, ProductCode
  
  open extract_header_cursor
  
  fetch extract_header_cursor into @FromWarehouseCode,
                                   @ProductCode,
                                   @Quantity,
                                   @ReasonCode,
                                   @ProcessedDate,
                                   @Batch
  
  select @fetch_status_header = @@fetch_status
  
  if @fetch_status_header < 0
    goto end_proc
  
  set @FileName = @FromWarehouseCode + replace(replace(replace(convert(varchar(23), @Getdate,121),'-',''),':',''),'.','')
  
  insert @TableResult (Data) select '<?xml version="1.0" encoding="Windows-1252" standalone="yes" ?>'
  insert @TableResult (Data) select '<root>'
  insert @TableResult (Data) select '<StockAdjustment>'
  
  while (@fetch_status_header = 0)
  begin
    update InterfaceExportStockAdjustment
       set RecordStatus = 'Y',
           ProcessedDate = @GetDate
     where isnull(Additional1, 'G200')    = @FromWarehouseCode
       and ProductCode                    = @ProductCode
       and convert(varchar(10), case when Quantity >= 0
                                     then Quantity
                                     else Quantity * -1
                                     end) = @Quantity
       and RecordStatus = 'N'
       and RecordType = 'MISA'
    
    insert @TableResult (Data) select '		<StockAdjustmentLine>'
    insert @TableResult (Data) select '	  <Location>' + @FromWarehouseCode + '</Location>'
    insert @TableResult (Data) select '			<ProductCode>' + @ProductCode + '</ProductCode>'
    insert @TableResult (Data) select '			<Batch>' + @Batch + '</Batch>'
    insert @TableResult (Data) select '			<CreateDate>' + @ProcessedDate + '</CreateDate>'
    insert @TableResult (Data) select '			<Quantity>' + @Quantity + '</Quantity>'
    insert @TableResult (Data) select '			<ReasonCode>' + @ReasonCode + '</ReasonCode>'
    insert @TableResult (Data) select '		</StockAdjustmentLine>'
    
    fetch extract_header_cursor into @FromWarehouseCode,
                                     @ProductCode,
                                     @Quantity,
                                     @ReasonCode,
                                     @ProcessedDate,
                                     @Batch
    
    select @fetch_status_header = @@fetch_status
  end
  
  insert @TableResult (Data) select '</StockAdjustment>'
  insert @TableResult (Data) select '</root>'
  
  end_proc:
  
  close extract_header_cursor
  deallocate extract_header_cursor
  select Data
    from @TableResult
end
