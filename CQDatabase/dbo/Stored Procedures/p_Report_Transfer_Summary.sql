﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Transfer_Summary
  ///   Filename       : p_Report_Transfer_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Transfer_Summary
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   StorageUnitBatchId int,
   ConfirmedQuantity  float,
   Status             nvarchar(50),
   CreateDate         datetime,
   StartDate          datetime,
   Litres             numeric(13,3)
  )
	 
	 insert @TableResult
        (StorageUnitBatchId,
         ConfirmedQuantity,
         Status,
         CreateDate,
         StartDate)
  select i.StorageUnitBatchId,
         i.ConfirmedQuantity,
         s.Status,
         i.CreateDate,
         i.StartDate
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
    join Status             s (nolock) on i.StatusId            = s.StatusId
   where i.CreateDate between @FromDate and @ToDate
     and s.StatusCode in ('W','S','F')
	 and i.warehouseId = @WarehouseId
  select vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         convert(nvarchar(10), tr.CreateDate, 120) as 'CreateDate',
         sum(tr.ConfirmedQuantity)
    from @TableResult    tr
    join viewStock       vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
  group by vs.ProductCode,
           vs.Product,
           vs.SKUCode,
           convert(nvarchar(10), tr.CreateDate, 120)
  order by ProductCode, convert(nvarchar(10), tr.CreateDate, 120)
end
