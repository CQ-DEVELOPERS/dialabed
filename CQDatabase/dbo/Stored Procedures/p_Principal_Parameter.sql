﻿/*  
  /// <summary>  
  ///   Procedure Name : p_Principal_Parameter  
  ///   Filename       : p_Principal_Parameter.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 23 Jan 2013 13:21:18  
  /// </summary>  
  /// <remarks>  
  ///   Selects rows from the Principal table.  
  /// </remarks>  
  /// <param>  
  /// </param>  
  /// <returns>  
  ///   Principal.PrincipalId,  
  ///   Principal.Principal   
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Principal_Parameter  
   
as  
begin  
  set nocount on;  
    
  declare @Error int  
   
  select  
        -1 as PrincipalId  
        ,'{All}' as Principal  
  union  
  select  
         Principal.PrincipalId  
        ,Principal.Principal  
    from Principal  
  order by Principal  
    
end  
