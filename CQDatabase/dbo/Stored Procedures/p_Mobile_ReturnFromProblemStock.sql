﻿ 
/*
/// <summary>
///   Procedure Name : p_Mobile_ReturnFromProblemStock
///   Filename       : p_Mobile_ReturnFromProblemStock.sql
///   Create By      : Karen
///   Date Created   : November 2014
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
create procedure p_Mobile_ReturnFromProblemStock
(
@WarehouseId   int,
@OrderNumber   nvarchar(30),
@Location    nvarchar(30)
)
--with encryption
as
begin
     set nocount on;
         
  declare @TableResult                  as table
  (
  Id     int identity,
  operatorId            int,
  storageUnitBatchId    int,
  pickLocation   nvarchar(30),
  pickLocationId     int,
  storeLocation   nvarchar(30),
  storeLocationId  int,
  confirmedQuantity     int,
  instructionTypeCode   nvarchar(30),
  JobId     int
  )
  
  declare @TableResult2                 as table
  (
  Id     int identity,
  IssueId    int,
  IssueLineId      int,
  JobId     int
  )
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int,
          @Id                           int,
          @operatorId                   int,
          @storageUnitBatchId           int,
          @pickLocation                 nvarchar(30),
          @pickLocationId               int,
          @storeLocation                nvarchar(30),
          @storeLocationId              int,
          @confirmedQuantity            int,
          @instructionTypeCode          nvarchar(30),
          @JobId                        int,
          @LocationId                   int,
          @IssueId                      int,
          @IssueLineId                  int,
          @Branded                      bit,
          @CheckingCount                int,
          @StatusId                     int,
          @IssueStatusId                int,
          @AreaCode                     nvarchar(50),
          @Downsizing                   bit,
          @OutboundDocumentId           int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_Wizard_Receipt_Insert;
    
		  -- Do the actual work here
  select @AreaCode   = a.AreaCode,
         @LocationId = l.LocationId
    from Location  l (nolock)
         join AreaLocation al (nolock) on l.locationid = al.locationid
         join Area   a (nolock) on a.areaid = al.areaid
   where (l.Location = @Location or l.SecurityCode = @Location)
     and a.Warehouseid = @WarehouseId
  
     set @AreaCode = (select AreaCode
                        from Area    a (nolock)
                             join AreaLocation al (nolock) on a.AreaId = al.AreaId
                       where al.Locationid = @LocationId)
  
  if @AreaCode = 'WSP'
  begin
    RAISERROR 900000 'Incorrect Area'
  end
  
  set @IssueId = (select IssueId
                    from OutboundDocument od (nolock)
                         join Issue             i (nolock)  on od.OutboundDocumentId = i.OutboundDocumentId
                   where od.OrderNumber = @OrderNumber)

  set @IssueStatusId = (select i.StatusId
                          from OutboundDocument od (nolock)
                               join Issue             i (nolock)  on od.OutboundDocumentId = i.OutboundDocumentId
                         where od.OrderNumber = @OrderNumber)

  set @CheckingCount = (select CheckingCount
                          from Issue i (nolock)
                         where i.IssueId = @IssueId)

  set @OutboundDocumentId = (select OutboundDocumentId
                               from OutboundDocument od (nolock)
                              where od.OrderNumber = @OrderNumber)
  
  insert @TableResult2
    (IssueId,
         IssueLineId,
         JobId)
  select ili.IssueId,
         ili.IssueLineId,
         ili.JobId
    from viewili ili
   where ili.ordernumber = @OrderNumber
  
  exec @ErrorId = p_Issue_Update
  @IssueId  = @IssueId,
  @DespatchBay = @LocationId
  
  exec @ErrorId = p_OutboundDocument_Update
  @OutboundDocumentId = @OutboundDocumentId,
  @ToLocation   = @Location
  
  insert @TableResult
         (operatorId,
         storageUnitBatchId,
         pickLocation,
         pickLocationId,
         storeLocation,
         storeLocationId,
         confirmedQuantity,
         instructionTypeCode,
         ili.JobId)
  select ili.operatorid,
         ili.StorageUnitBatchId,
         ili.PickLocation,
         ili.PickLocationId,
         ili.StoreLocation,
         ili.StoreLocationId,
         ili.confirmedQuantity,
         'M',
         JobId
    from viewili ili
   where ili.ordernumber = @OrderNumber
     and JobCode = 'QA'
  
  if @@ROWCOUNT = 0
  begin
     RAISERROR 900000 'No rows'
  end
  
  while exists(select top 1 1 from @TableResult)
  begin
    select @Id       = Id,
           @operatorId     = operatorId,
           @storageUnitBatchId   = storageUnitBatchId,
           @pickLocation    = StoreLocation,
           @pickLocationId    = StoreLocationId,
           @storeLocation    = @Location,
           @confirmedQuantity   = confirmedQuantity,
           @instructionTypeCode  = instructionTypeCode,
           @JobId      = JobId
      from @TableResult tr
           
           delete @TableResult where Id = @Id
           
   
    if @confirmedQuantity > 0
    exec @ErrorId = p_Mobile_Auto_Movement 
   @operatorId   =@operatorId,
   @storageUnitBatchId =@storageUnitBatchId,
   @pickLocation   =@pickLocation,
   @storeLocation   =@storeLocation,
   @confirmedQuantity = @confirmedQuantity,
   @instructionTypeCode =@instructionTypeCode
 
  
   update instruction 
      set storelocationId = @LocationId
    where jobid = @jobid  
  
 end 
	   
	   
    lbexit:
    if @trancount = 0
		    commit;
    
  end try
  begin catch
    error:
		    select @ErrorId       = ERROR_NUMBER()
            ,@ErrorSeverity = ERROR_SEVERITY()
            ,@ErrorMessage  = ERROR_MESSAGE()
            ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Wizard_Receipt_Insert;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
