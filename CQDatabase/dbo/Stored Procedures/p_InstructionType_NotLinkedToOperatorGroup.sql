﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_NotLinkedToOperatorGroup
  ///   Filename       : p_InstructionType_NotLinkedToOperatorGroup.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_NotLinkedToOperatorGroup
( 
@OperatorGroupId	int
)

 
as
begin
	 set nocount on;
  
  select it.InstructionType,
		 it.InstructionTypeCode,
		 it.InstructionTypeId
    from InstructionType it
    where not exists(select 1 from OperatorGroupInstructionType ogi where it.InstructionTypeId = ogi.InstructionTypeId
														and ogi.OperatorGroupId = @OperatorGroupId)
	order by InstructionType
end
