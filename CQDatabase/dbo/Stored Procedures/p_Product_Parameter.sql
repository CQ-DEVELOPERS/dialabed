﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Parameter
  ///   Filename       : p_Product_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2014 14:16:34
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Product table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Product.ProductId,
  ///   Product.Product 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as ProductId
        ,'{All}' as Product
  union
  select
         Product.ProductId
        ,Product.Product
    from Product
  order by Product
  
end
