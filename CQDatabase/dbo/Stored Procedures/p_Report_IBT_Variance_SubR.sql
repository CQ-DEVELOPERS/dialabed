﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_IBT_Variance_SubR
  ///   Filename       : p_Report_IBT_Variance_SubR.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_IBT_Variance_SubR
(
 @ReceiptLineId		int,
 @IssueLineId		int
)

 
as
begin
	 set nocount on;
  
 
  declare @TableHeader as Table
  (
   JobId				int,
   OperatorId			int,
   StatusId				int,
   WarehouseId			int,
   ReferenceNumber		nvarchar(30),
   CheckedBy			int,
   CheckedDate			datetime   
  );
  
  declare @TableDetails as Table
  (
   JobId				int,
   ConfirmedQuantity	float,
   PalletId				int,
   CheckQuantity		float,
   OperatorId			int
  );
  
   
  insert @TableHeader
        (JobId,
         OperatorId,
         StatusId,
         WarehouseId,
         ReferenceNumber,
         CheckedBy,
         CheckedDate)
  select j.JobId,
         j.OperatorId,
         j.StatusId,
         j.WarehouseId,
         j.ReferenceNumber,
         j.CheckedBy,
         j.CheckedDate
  from   Job					j
  where	 j.ReceiptLineId = @ReceiptLineId
  or     j.IssueLineId = @IssueLineId
      
  insert @TableDetails 
        (JobId,
         ConfirmedQuantity,
         PalletId,
         CheckQuantity,
         OperatorId)
  select th.JobId,
         i.ConfirmedQuantity,
         i.PalletId,
         i.CheckQuantity,
         i.OperatorId
  from	 @TableHeader th
  join Instruction			i  (nolock) on i.JobId = th.JobId
   
 
  select	th.JobId,
			th.OperatorId as JobOperator,
			th.StatusId,
			th.WarehouseId,
			th.ReferenceNumber,
			th.CheckedBy,
			th.CheckedDate,
			td.ConfirmedQuantity,
			td.PalletId,
			td.CheckQuantity,
			td.OperatorId as InstructionOperator
    from @TableHeader  th
    join @TableDetails td on th.JobId = td.JobId
 
end
