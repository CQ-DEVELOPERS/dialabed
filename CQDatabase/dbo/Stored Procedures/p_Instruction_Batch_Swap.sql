﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Batch_Swap
  ///   Filename       : p_Instruction_Batch_Swap.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Batch_Swap
(
 @LocationId         int,
 @StorageUnitId      int
)
 
as
begin
	 set nocount on;
	 
	 declare @TableInstructions as table
	 (
	  InstructionId      int,
	  StorageUnitBatchId int,
	  LocationId         int
	 )
	 
	 declare @TableLocations as table
	 (
	  StorageUnitBatchId int,
	  LocationId         int
	 )
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @StorageUnitBatchId int,
          @UpdInstructionId   int,
          @AllocatedQuantity  float,
          @ReservedQuantity   float
  
  if (select AreaCode
        from AreaLocation al
        join Area          a on al.AreaId = a.AreaId
       where al.LocationId = @LocationId) = 'RK'
    return;
	 
	 select top 1
	        @StorageUnitBatchId = subl.StorageUnitBatchId
    from StorageUnitBatchLocation subl
    join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
   where subl.LocationId = @LocationId
     and sub.StorageUnitId = @StorageUnitId
  order by ActualQuantity desc
  
  select @GetDate = dbo.ufn_Getdate()
  
  insert @TableInstructions
        (InstructionId,
         StorageUnitBatchId,
         LocationId)
  select i.InstructionId,
         i.StorageUnitBatchId,
         @LocationId
    from Instruction                 i
    join StorageUnitBatch          sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Status                      s (nolock) on i.StatusId              = s.StatusId
   where s.StatusCode         != 'F'
     and i.PickLocationId      = @LocationId
     and i.StorageUnitBatchId != @StorageUnitBatchId
     and sub.StorageUnitId     = @StorageUnitId
     and i.Picked = 0
     and i.Stored = 0
     --and subl.ActualQuantity     <= 0
  
  insert @TableLocations
        (StorageUnitBatchId,
         LocationId)
  select distinct
         StorageUnitBatchId,
         LocationId
    from @TableInstructions
  
  while exists(select top 1 1 from @TableInstructions)
  begin
    select @UpdInstructionId = InstructionId
      from @TableInstructions
    
    delete @TableInstructions
     where InstructionId = @UpdInstructionId
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @UpdInstructionId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Update
     @InstructionId      = @UpdInstructionId,
     @StorageUnitBatchId = @StorageUnitBatchId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @UpdInstructionId
    
    if @Error <> 0
      goto error
  end
  
  select @AllocatedQuantity = sum(AllocatedQuantity),
         @ReservedQuantity  = sum(ReservedQuantity)
    from @TableLocations             t
    join StorageUnitBatchLocation subl on t.LocationId = subl.LocationId
   where subl.StorageUnitBatchId = @StorageUnitBatchId
     and subl.LocationId         = @LocationId
  
  if @AllocatedQuantity is null
    set @AllocatedQuantity = 0
  
  if @ReservedQuantity is null
    set @ReservedQuantity = 0
  
  update subl
     set AllocatedQuantity = @AllocatedQuantity,
         ReservedQuantity  = @ReservedQuantity
    from @TableLocations             t
    join StorageUnitBatchLocation subl on t.LocationId = subl.LocationId
   where subl.StorageUnitBatchId = @StorageUnitBatchId
     and subl.LocationId         = @LocationId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update subl
     set AllocatedQuantity = 0,
         ReservedQuantity  = 0
    from @TableLocations             t
    join StorageUnitBatchLocation subl on t.StorageUnitBatchId = subl.StorageUnitBatchId
                                      and t.LocationId         = subl.LocationId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  return
  
  error:
    raiserror 900000 'Error executing p_Instruction_Batch_Swap'
    return @Error
end
