﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Insert
  ///   Filename       : p_COA_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:48
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the COA table.
  /// </remarks>
  /// <param>
  ///   @COAId int = null output,
  ///   @COACode nvarchar(510) = null,
  ///   @COA nvarchar(max) = null,
  ///   @StorageUnitBatchId int = null 
  /// </param>
  /// <returns>
  ///   COA.COAId,
  ///   COA.COACode,
  ///   COA.COA,
  ///   COA.StorageUnitBatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Insert
(
 @COAId int = null output,
 @COACode nvarchar(510) = null,
 @COA nvarchar(max) = null,
 @StorageUnitBatchId int = null 
)
 
as
begin
	 set nocount on;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @COACode = '-1'
    set @COACode = null;
  
  if @COA = '-1'
    set @COA = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
	 declare @Error int
 
  insert COA
        (COACode,
         COA,
         StorageUnitBatchId)
  select @COACode,
         @COA,
         @StorageUnitBatchId 
  
  select @Error = @@Error, @COAId = scope_identity()
  
  
  return @Error
  
end
