﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPOHeader_Update
  ///   Filename       : p_InterfaceImportPOHeader_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:58
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportPOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportPOHeaderId int = null,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordType char(3) = null,
  ///   @RecordStatus char(1) = null,
  ///   @SupplierCode nvarchar(60) = null,
  ///   @Supplier nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @ContainerNumber nvarchar(60) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPOHeader_Update
(
 @InterfaceImportPOHeaderId int = null,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @RecordType char(3) = null,
 @RecordStatus char(1) = null,
 @SupplierCode nvarchar(60) = null,
 @Supplier nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @ContainerNumber nvarchar(60) = null,
 @SealNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportPOHeaderId = '-1'
    set @InterfaceImportPOHeaderId = null;
  
	 declare @Error int
 
  update InterfaceImportPOHeader
     set PrimaryKey = isnull(@PrimaryKey, PrimaryKey),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         RecordType = isnull(@RecordType, RecordType),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         SupplierCode = isnull(@SupplierCode, SupplierCode),
         Supplier = isnull(@Supplier, Supplier),
         Address = isnull(@Address, Address),
         FromWarehouseCode = isnull(@FromWarehouseCode, FromWarehouseCode),
         ToWarehouseCode = isnull(@ToWarehouseCode, ToWarehouseCode),
         DeliveryNoteNumber = isnull(@DeliveryNoteNumber, DeliveryNoteNumber),
         ContainerNumber = isnull(@ContainerNumber, ContainerNumber),
         SealNumber = isnull(@SealNumber, SealNumber),
         DeliveryDate = isnull(@DeliveryDate, DeliveryDate),
         Remarks = isnull(@Remarks, Remarks),
         NumberOfLines = isnull(@NumberOfLines, NumberOfLines),
         Additional1 = isnull(@Additional1, Additional1),
         Additional2 = isnull(@Additional2, Additional2),
         Additional3 = isnull(@Additional3, Additional3),
         Additional4 = isnull(@Additional4, Additional4),
         Additional5 = isnull(@Additional5, Additional5),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         Additional6 = isnull(@Additional6, Additional6),
         Additional7 = isnull(@Additional7, Additional7),
         Additional8 = isnull(@Additional8, Additional8),
         Additional9 = isnull(@Additional9, Additional9),
         Additional10 = isnull(@Additional10, Additional10),
         InsertDate = isnull(@InsertDate, InsertDate) 
   where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
