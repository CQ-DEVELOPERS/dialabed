﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstructionLine_Insert
  ///   Filename       : p_BOMInstructionLine_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:08
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the BOMInstructionLine table.
  /// </remarks>
  /// <param>
  ///   @BOMInstructionId int = null,
  ///   @BOMLineId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   BOMInstructionLine.BOMInstructionId,
  ///   BOMInstructionLine.BOMLineId,
  ///   BOMInstructionLine.LineNumber,
  ///   BOMInstructionLine.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMInstructionLine_Insert
(
 @BOMInstructionId int = null,
 @BOMLineId int = null,
 @LineNumber int = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert BOMInstructionLine
        (BOMInstructionId,
         BOMLineId,
         LineNumber,
         Quantity)
  select @BOMInstructionId,
         @BOMLineId,
         @LineNumber,
         @Quantity 
  
  select @Error = @@Error
  
  
  return @Error
  
end
