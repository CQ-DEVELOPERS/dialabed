﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentDesc_Update
  ///   Filename       : p_InterfaceDocumentDesc_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:34:59
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceDocumentDesc table.
  /// </remarks>
  /// <param>
  ///   @DocId int = null,
  ///   @Doc nvarchar(20) = null,
  ///   @DocDesc nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentDesc_Update
(
 @DocId int = null,
 @Doc nvarchar(20) = null,
 @DocDesc nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
  if @DocId = '-1'
    set @DocId = null;
  
	 declare @Error int
 
  update InterfaceDocumentDesc
     set Doc = isnull(@Doc, Doc),
         DocDesc = isnull(@DocDesc, DocDesc) 
   where DocId = @DocId
  
  select @Error = @@Error
  
  
  return @Error
  
end
