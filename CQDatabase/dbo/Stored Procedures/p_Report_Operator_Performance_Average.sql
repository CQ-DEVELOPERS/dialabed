﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_Performance_Average
  ///   Filename       : p_Report_Operator_Performance_Average.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 06 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Operator_Performance_Average
(
	@StartDate				Datetime,
	@OperatorGroupId		int,
	@UOM                   nvarchar(10) = 'Units'
)
 
as
begin
	 set nocount on;
	

Declare @GroupPerformance As Table
(
	[OperatorGroupId]   int,
	[PerformanceType]	int,
	[Units]				float,
	[Weight]			float,
	[Instructions]		float,
	[Orders]			float,
	[OrderLines]		float,
	[Jobs]				int
)

Declare @OperatorCount int
	Select @OperatorCount = Count(OperatorId) From OperatorPerformance
	WHERE convert(nvarchar,EndDate,111) = convert(nvarchar,@StartDate,111)
Insert Into @GroupPerformance
(
	[OperatorGroupId]
	,[PerformanceType]
	 ,[Units]
	 ,[Weight]
	 ,[Instructions]
	 ,[Orders]
	 ,[OrderLines]
	 ,[Jobs]
)

(
Select		 O.OperatorGroupId,
			 GP.PerformanceType,
			Count(O.OperatorId) *  SUM(Distinct(GP.Units)) AS Units,
			Count(O.OperatorId) *  SUM(Distinct(GP.Weight)) As Weight,
			Count(O.OperatorId) *  SUM(Distinct(GP.Instructions)) As Instructions,
			Count(O.OperatorId) *  SUM(Distinct(GP.Orders)) As Orders,
			Count(O.OperatorId) *  SUM(Distinct(GP.OrderLines)) As OrderLines,
			Count(O.OperatorId) *  SUM(Distinct(GP.Jobs)) As Jobs	
FROM        OperatorPerformance Op INNER JOIN
                      Operator O ON Op.OperatorId = O.OperatorId INNER JOIN
					(SELECT 
						[OperatorGroupId]
					  ,[PerformanceType]
					  ,[Units]
					  ,[Weight]
					  ,[Instructions]
					  ,[Orders]
					  ,[OrderLines]
					  ,[Jobs]
							FROM [dbo].[GroupPerformance]
									Where OperatorGroupID = @OperatorGroupId
									And [PerformanceType] = 1
				Union
					SELECT [OperatorGroupId]
					  ,[PerformanceType]
					  ,[Units]
					  ,[Weight]
					  ,[Instructions]
					  ,[Orders]
					  ,[OrderLines]
					  ,[Jobs]
							FROM [dbo].[GroupPerformance]
									Where OperatorGroupID = @OperatorGroupId
									And [PerformanceType] = 2
					) AS GP ON O.OperatorGroupId = GP.OperatorGroupId 
Where convert(nvarchar,OP.EndDate,111) = convert(nvarchar,@StartDate,111)
And O.OperatorGroupId = @OperatorGroupId
Group By O.OperatorGroupId,GP.[PerformanceType]
)



	Declare @TempTable As Table
(
	OperatorGroupId int,
	Actual			int,
	SiteTar			int,
	Industry		int,
	Measure			nvarchar(10)	
)


if @UOM = 'Units'
  begin

					Insert Into @TempTable(Industry,SiteTar,OperatorGroupId)
					(
					Select X.Units/@OperatorCount,Y.Units/@OperatorCount,X.OperatorGroupId As IndustryJobs from 
					(Select Units,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = @OperatorGroupId
					And GP.PerformanceType = 2) X Inner join
					(Select Units,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = @OperatorGroupId
					And GP.PerformanceType = 1) Y
					On X.OperatorGroupId = Y.OperatorGroupId
					)

					Update @TempTable Set Actual = 
					(
									Select Avg(Units) From (
						SELECT      
								Sum(OperatorPerformance.Units) as Units, Convert(nvarchar,EndDate,108) As EndDate
					FROM         OperatorPerformance INNER JOIN
										  Operator ON OperatorPerformance.OperatorId = Operator.OperatorId
					INNER JOIN
										  OperatorGroup ON Operator.OperatorGroupId = OperatorGroup.OperatorGroupId 
					WHERE convert(nvarchar,OperatorPerformance.EndDate,111) = convert(nvarchar,@StartDate,111)
					And Operator.OperatorGroupId = @OperatorGroupId
					Group BY  Convert(nvarchar,EndDate,108)
					) X
					),Measure = 'Units'
					Where OperatorGroupId = @OperatorGroupId
    
  end
   else if @UOM = 'Weight'
  
	begin

					Insert Into @TempTable(Industry,SiteTar,OperatorGroupId)
					(
					Select X.Weight/@OperatorCount ,Y.Weight/@OperatorCount,X.OperatorGroupId As IndustryJobs from 
					(Select Weight,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = @OperatorGroupId
					And GP.PerformanceType = 2) X Inner join
					 
					(Select Weight,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = @OperatorGroupId
					And GP.PerformanceType = 1) Y

					On X.OperatorGroupId = Y.OperatorGroupId

					)

					Update @TempTable Set Actual = 
						(
				Select Avg(Weight) From (
						SELECT      
								Sum(OperatorPerformance.Weight) as Weight, Convert(nvarchar,EndDate,108) As EndDate
					FROM         OperatorPerformance INNER JOIN
										  Operator ON OperatorPerformance.OperatorId = Operator.OperatorId
					INNER JOIN
										  OperatorGroup ON Operator.OperatorGroupId = OperatorGroup.OperatorGroupId 
					WHERE convert(nvarchar,OperatorPerformance.EndDate,111) = convert(nvarchar,@StartDate,111)
					And Operator.OperatorGroupId = @OperatorGroupId
					Group BY  Convert(nvarchar,EndDate,108)
					) X
						),Measure = 'KG'
						Where OperatorGroupId = @OperatorGroupId
      

	end
 else if @UOM = 'Jobs'
  
	begin

					Insert Into @TempTable(Industry,SiteTar,OperatorGroupId)
					(
					Select X.Jobs/@OperatorCount,Y.Jobs/@OperatorCount,X.OperatorGroupId As IndustryJobs from 
					(Select Jobs,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = @OperatorGroupId
					And GP.PerformanceType = 2) X Inner join
					 
					(Select Jobs,OperatorGroupId From @GroupPerformance GP
					Where GP.OperatorGroupId = @OperatorGroupId
					And GP.PerformanceType = 1) Y

					On X.OperatorGroupId = Y.OperatorGroupId

					)

					Update @TempTable Set Actual = 
						(
						Select Avg(Jobs) From (
						SELECT      
								Sum(OperatorPerformance.Jobs) as Jobs, Convert(nvarchar,EndDate,108) As EndDate
					FROM         OperatorPerformance INNER JOIN
										  Operator ON OperatorPerformance.OperatorId = Operator.OperatorId
					INNER JOIN
										  OperatorGroup ON Operator.OperatorGroupId = OperatorGroup.OperatorGroupId 
					WHERE convert(nvarchar,OperatorPerformance.EndDate,111) = convert(nvarchar,@StartDate,111)
					And Operator.OperatorGroupId = @OperatorGroupId
					Group BY  Convert(nvarchar,EndDate,108)
					) X
						),Measure = 'Jobs'
						Where OperatorGroupId = @OperatorGroupId
      

	end
Select OperatorGroupId,  
	   Actual,			
	   SiteTar,			
       Industry,Measure
		 From @TempTable


end	   		
