﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DockSchedule_Maintenance_Update
  ///   Filename       : p_DockSchedule_Maintenance_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jul 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DockSchedule_Maintenance_Update
(
 @DockScheduleId                  int,
 @OperatorId                      int,
 @Subject                         nvarchar(1024),
 @PlannedStart                    datetime,
 @PlannedEnd                      datetime,
 @LocationId                      int,
 @RecurrenceRule                  nvarchar(1024),
 @RecurrenceParentID              int,
 @Description                     nvarchar(max),
 @CssClass                        nvarchar(50)
)
 
as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
         ,@GetDate            datetime
         ,@OutboundShipmentId int
         ,@IssueId            int
         ,@InboundShipmentId  int
         ,@ReceiptId          int
         ,@ExternalCompanyId  int
         ,@EditExternalCompanyId int
  
  set @trancount = @@trancount;
  
 
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction DockSchedule;
			   
    select @GetDate = dbo.ufn_Getdate()
	 
	    if (DATEDIFF(HH, @GetDate, @PlannedStart) < 24)
	    begin
			raiserror 900000 'Editing not allowed within 24 hours prior to delivery'
	    end
  
    select @ExternalCompanyId = ExternalCompanyId
      from Operator (nolock)
     where OperatorId = @OperatorId
    
    select @OutboundShipmentId = OutboundShipmentId
          ,@IssueId            = IssueId
          ,@InboundShipmentId  = InboundShipmentId
          ,@ReceiptId          = ReceiptId
      from DockSchedule (nolock)
     where DockScheduleId = @DockScheduleId
    
    if @OutboundShipmentId is not null
    begin
      select @EditExternalCompanyId = od.ExternalCompanyId
        from OutboundShipmentIssue osi (nolock)
        join Issue                   i (nolock) on osi.IssueId = i.IssueId
        join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
       where osi.OutboundShipmentId = @OutboundShipmentId
    end
    else if @IssueId is not null
    begin
      select @EditExternalCompanyId = od.ExternalCompanyId
        from Issue                   i (nolock)
        join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
       where i.IssueId = @IssueId
    end
    else if @InboundShipmentId is not null
    begin
      select @EditExternalCompanyId = id.ExternalCompanyId
        from InboundShipmentReceipt isr (nolock)
        join Receipt                  r (nolock) on isr.ReceiptId = r.ReceiptId
        join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
       where isr.InboundShipmentId = @InboundShipmentId
    end
    else if @ReceiptId is not null
    begin
      select @EditExternalCompanyId = id.ExternalCompanyId
        from Receipt          r (nolock)
        join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
       where r.ReceiptId = @ReceiptId
    end
    
    if @EditExternalCompanyId != @ExternalCompanyId
      raiserror 900000 'Company not the same!'
    
    
    select @Subject = isnull(@Subject + ' (' + convert(nvarchar(10), datediff(mi, @PlannedStart, @PlannedEnd)) + ' minutes)',@Subject)
    select @Subject = isnull(substring(@Subject, 1, charindex('(',@Subject)-2),@Subject)
    
   
    UPDATE [DockSchedule]
       SET [Subject] = @Subject,
           [PlannedStart] = @PlannedStart,
           [PlannedEnd] = @PlannedEnd,
           [RecurrenceRule] = @RecurrenceRule,
           [RecurrenceParentID] = @RecurrenceParentID,
           [Description] = @Description,
           [LocationId] = @LocationId
     WHERE [DockScheduleId] = @DockScheduleId
	   
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction DockSchedule;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
  
  error:
   
  
end
 
 
