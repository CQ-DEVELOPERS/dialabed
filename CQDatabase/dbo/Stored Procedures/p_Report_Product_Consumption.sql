﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Consumption
  ///   Filename       : p_Report_Product_Consumption.sql
  ///   Create By      : Karen
  ///   Date Created   : 7 April 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_Consumption
(
 @ProductCode		nvarchar(30),
 @WarehouseId		int,
 @Sort				char
 
)
 
as
begin
	 set nocount on;
	 
	 declare @Weeks			int,
	         @Count			int,
	         @Date			datetime,
	         @Months		int,
	         @FromDate3		datetime,
	         @FromDate6		datetime,
	         @ProductId		int,
	         @DaysInMonth	int,
	         @ToDate        datetime,
	         @FromDate      datetime,
	         @Nbr			numeric,
	         @Days1			int,
	         @Days2			int
	 
  
  declare @TableResult as table
  (
   Rank				int,
   ProductCode		nvarchar(30),
   Product			nvarchar(50),
   Month6			int,
   Month3			int,
   MinimumQuantity	float,
   ReorderQuantity	float,
   MaximumQuantity	float,
   StockOnHand		float,
   CalcDaysSOH		numeric(12,1),
   StartDate		datetime,
   EndDate			datetime,
   StorageUnitId	int,
   WarehouseId		int,
   ProductId		int,
   DaysInMonth		int,
   Days1			int,
   Days2			int,
   Replenishments	int)
   
   select @ProductId = ProductId from Product
   where @ProductCode = ProductCode
  
  declare @Products as table
  (
   StorageUnitId	int,
   ProductId		int,	
   AreaId			int,
   WarehouseId		int,
   Quantity3		float
  )
  
  declare @Products3 as table
  (
   ProductId			int,	
   WarehouseId			int,
   OutboundDocumentId	int,
   LineNumber			int,
   Quantity3			float
  )
  
  declare @Products6 as table
  (
   ProductId			int,	
   WarehouseId			int,
   OutboundDocumentId	int,
   LineNumber			int,
   Quantity6			float
  )
  declare @SOH as table
  (
   ProductId			int,	
   WarehouseId			int,
   SOH      			float
  )
  
  if @WarehouseId = -1
    set @WarehouseId = null
    
  select @ToDate = CURRENT_TIMESTAMP
   
  select @Months = round((datediff(MM, @FromDate, @ToDate)), 0)
  
  SELECT @Days1 = (CONVERT(INT, Value) * -1)
  from Configuration where ConfigurationId = 257
  
  SELECT @Days2 = (CONVERT(INT, Value) * -1)
  from Configuration where ConfigurationId = 258
     
  select @Fromdate3 = DATEADD (MM , -3 , @ToDate )
  
  select @Fromdate6 = DATEADD (MM , -6 , @ToDate )

  
SELECT @DaysInMonth = CONVERT(INT, Value) 
  from Configuration where ConfigurationId = 215
   
  insert @Products
        (StorageUnitId,
         ProductId,
         WarehouseId)
  select distinct su.StorageUnitId,
		 su.ProductId,
		 a.WarehouseId
    from StorageUnit su
    join StorageUnitArea sua on su.StorageUnitId = sua.StorageUnitId
    join Area a			on sua.AreaId = a.AreaId
    where ProductId = isnull(@ProductId, ProductId)
    and   WarehouseId = isnull(@WarehouseId, WarehouseId)
    and   a.StockOnHand = 1
    
  insert @Products3
        (ProductId,
         WarehouseId,
         OutboundDocumentId,
         LineNumber,
         Quantity3)
  select distinct su.ProductId,
		 od.WarehouseId,
		 od.OutboundDocumentId,
		 ol.LineNumber,
		 max (ol.Quantity)
  from StorageUnit su 
    join StorageUnitArea sua on su.StorageUnitId = sua.StorageUnitId
    join Area a			on sua.AreaId = a.AreaId
    join OutboundLine ol (nolock) on su.StorageUnitId = ol.StorageUnitId
	join OutboundDocument od (nolock) on ol.OutboundDocumentId = od.OutboundDocumentId
	where od.DeliveryDate between @FromDate3 and @ToDate
    and a.StockOnHand = 1
    group by su.productid, 
             od.warehouseid,
             od.OutboundDocumentId,
			 ol.LineNumber
    order by su.productid,
             od.warehouseid,
             od.OutboundDocumentId,
			 ol.LineNumber
    
    
  insert @Products6
        (ProductId,
         WarehouseId,
         OutboundDocumentId,
         LineNumber,
         Quantity6)
  select distinct su.ProductId,
		 od.WarehouseId,
		 od.OutboundDocumentId,
		 ol.LineNumber,
		 max (ol.Quantity)
  from StorageUnit su 
    join StorageUnitArea sua on su.StorageUnitId = sua.StorageUnitId
    join Area a			on sua.AreaId = a.AreaId
    join OutboundLine ol (nolock) on su.StorageUnitId = ol.StorageUnitId
	join OutboundDocument od (nolock) on ol.OutboundDocumentId = od.OutboundDocumentId
	where od.DeliveryDate between @FromDate6 and @ToDate
    and a.StockOnHand = 1
    group by su.productid, 
             od.warehouseid,
             od.OutboundDocumentId,
			 ol.LineNumber
    order by su.productid,
             od.warehouseid,
             od.OutboundDocumentId,
			 ol.LineNumber  

            
  insert @SOH
       (ProductId,
         WarehouseId,
         SOH) 
  select su.ProductId,
		 a.WarehouseId,
		 sum (subl.ActualQuantity)
   from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId 
    where a.StockOnHand = 1
    group by su.productid, 
              a.warehouseid
    order by su.productid,
              a.warehouseid 

  set @Count = 0
  select @Weeks = round((datediff(dd, @FromDate, @ToDate) / 7.000) + .49, 0)
  
  if @Weeks = 0
    set @Weeks = 1
  
    select @Date = dateadd(wk, @Count, @FromDate)
    
    set  @Count = 0
    set  @Days1 = (@Days1 * -1)
    set  @Days2 = (@Days2 * -1)
        
    insert @TableResult
          (Rank,
		  ProductCode,
		  Product,
		  MinimumQuantity,
		  ReorderQuantity,
		  MaximumQuantity,
		  StartDate,
		  EndDate,
		  StorageUnitId,
		  WarehouseId,
		  ProductId,
	      DaysInMonth,
	      Days1,
	      Days2)
    select @Count,
           pr.ProductCode,
		   pr.Product,
		   pr.MinimumQuantity,
		   pr.ReorderQuantity,
		   pr.MaximumQuantity,
		   @FromDate,
		   @ToDate,
		   p.StorageUnitId,
		   WarehouseId,
		   p.ProductId,
	       @DaysInMonth,
	       @Days1,
	       @Days2
      from @Products p
      join Product pr on pr.ProductId = p.ProductId
      join StorageUnit su on p.StorageUnitId = su.StorageUnitId

 
  update tr
    set StockOnHand = SOH
  From @TableResult tr
  join @SOH so on tr.productid = so.productid
				 and tr.warehouseid = so.warehouseid
                      
  --update tr
  --  set Month6 = Quantity6
  --From @TableResult tr
  --join @products6 p6 on tr.productid = p6.productid
		--		 and tr.warehouseid = p6.warehouseid
				 
 update tr
    set Month6 = (select sum(Quantity6)
		from @products6 p6 where tr.productid = p6.productid)
    From @TableResult tr
    
 update tr
    set Month3 = (select sum(Quantity3)
		from @products3 p3 where tr.productid = p3.productid)
    From @TableResult tr
                  
  --update tr
  --  set Month3 = Quantity3
  --From @TableResult tr
  --join @products3 p3 on tr.productid = p3.productid
		--		 and tr.warehouseid = p3.warehouseid                      

 update tr
   set CalcDaysSOH = (tr.Month3/(3 * @DaysInMonth))
   from @TableResult tr
                      
SET ARITHABORT OFF
SET ANSI_WARNINGS OFF

 update tr
    set CalcDaysSOH = isnull(tr.StockOnHand / tr.CalcDaysSOH, 0)
    from @TableResult tr
    
                       
if @Sort = 'L'  
select 
	( select count(*) + 1
           from @TableResult
          where StorageUnitId < t.StorageUnitId ) as rank,

		ProductCode,
		Product,
		Month6,
		Month3,
		MinimumQuantity,
		ReorderQuantity,
		MaximumQuantity,
		StockOnHand,
		CalcDaysSOH,
		StartDate,
		EndDate,
		StorageUnitId,
		WarehouseId,
		ProductId,
		DaysInMonth,
		Days1,
		Days2,
		Replenishments	
from @TableResult t
group by
		ProductCode,
		Product,
		Month6,
		Month3,
		MinimumQuantity,
		ReorderQuantity,
		MaximumQuantity,
		StockOnHand,
		CalcDaysSOH,
		StartDate,
		EndDate,
		StorageUnitId,
		WarehouseId,
		ProductId,
		DaysInMonth,
		Days1,
		Days2,
		Replenishments
order by CalcDaysSOH 
	
	
if @Sort = 'H'
	
select 
	( select count(*) + 1
           from @TableResult
          where StorageUnitId < t.StorageUnitId ) as rank,
		ProductCode,
		Product,
		Month6,
		Month3,
		MinimumQuantity,
		ReorderQuantity,
		MaximumQuantity,
		StockOnHand,
		CalcDaysSOH,
		StartDate,
		EndDate,
		StorageUnitId,
		WarehouseId,
		ProductId,
		DaysInMonth,
		Days1,
		Days2,
		Replenishments
from	@TableResult t
group by
		ProductCode,
		Product,
		Month6,
		Month3,
		MinimumQuantity,
		ReorderQuantity,
		MaximumQuantity,
		StockOnHand,
		CalcDaysSOH,
		StartDate,
		EndDate,
		StorageUnitId,
		WarehouseId,
		ProductId,
		DaysInMonth,
		Days1,
		Days2,
		Replenishments
order by Month3 DESC

end
