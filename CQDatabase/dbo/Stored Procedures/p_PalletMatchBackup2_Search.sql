﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletMatchBackup2_Search
  ///   Filename       : p_PalletMatchBackup2_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:50
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the PalletMatchBackup2 table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   PalletMatchBackup2.PalletId,
  ///   PalletMatchBackup2.InstructionId,
  ///   PalletMatchBackup2.StorageUnitBatchId,
  ///   PalletMatchBackup2.ProductCode,
  ///   PalletMatchBackup2.SKUCode,
  ///   PalletMatchBackup2.Batch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletMatchBackup2_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         PalletMatchBackup2.PalletId
        ,PalletMatchBackup2.InstructionId
        ,PalletMatchBackup2.StorageUnitBatchId
        ,PalletMatchBackup2.ProductCode
        ,PalletMatchBackup2.SKUCode
        ,PalletMatchBackup2.Batch
    from PalletMatchBackup2
  
end
