﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_User_Print
  ///   Filename       : p_Operator_User_Print.sql
  ///   Create By      : Willis
  ///   Date Created   : 26 August 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Operator table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.Operator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_User_Print
(
	@warehouseId int,
	@StartOperator nvarchar(50),
	@EndOperator	nvarchar(50)
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  Declare Operator_Cursor Cursor for
	Select dbo.ConvertTo128(Operator) as Operator,
			dbo.ConvertTo128(Password) as Password
	From Operator 
	where Operator >= isnull(@StartOperator,'') and Operator <= Isnull(@EndOperator,Char(255))
			and warehouseId = @warehouseId
		order by operator

Open Operator_Cursor

Declare @User nvarchar(50),
		@Password nvarchar(50),
		@row int
		
		
Create Table #Users(	row int,
						User1 nvarchar(50),
						Pass1 nvarchar(50),
						User2 nvarchar(50),
						Pass2 nvarchar(50),
						User3 nvarchar(50),
						Pass3 nvarchar(50),
						User4 nvarchar(50),
						Pass4 nvarchar(50))

Fetch next From Operator_Cursor into @User,@Password
set @row = 1
While @@Fetch_Status = 0 
Begin
	
	Insert into #Users (Row,User1,Pass1)
	Select @Row,	@User,		@Password
	Fetch next From Operator_Cursor into @User,@Password

	If @@Fetch_Status = 0 
	Begin
		update #Users set User2=@User ,Pass2 = @Password 
		where row = @row
		Fetch next From Operator_Cursor into @User,@Password
	End
	If @@Fetch_Status = 0 
	Begin
		update #Users set User3=@User ,Pass3 = @Password 
		where row = @row
		Fetch next From Operator_Cursor into @User,@Password
	End
	If @@Fetch_Status = 0 
	Begin
		update #Users set User4=@User ,Pass4 = @Password 
		where row = @row
		Fetch next From Operator_Cursor into @User,@Password
	End
	set @row = @row + 1
End

Close Operator_Cursor
Deallocate Operator_Cursor
Select * from #Users
Drop table #Users

end
