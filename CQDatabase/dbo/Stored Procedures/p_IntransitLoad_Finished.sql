﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoad_Finished
  ///   Filename       : p_IntransitLoad_Finished.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jan 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoad_Finished
(
 @IntransitLoadId int
)
 
as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
         ,@InterfaceImportHeaderId int
         ,@InterfaceExportHeaderId int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_IntransitLoad_Finished;
    
		  -- Insert IBT here:
    insert InterfaceImportHeader
          (PrimaryKey,
           OrderNumber,
           RecordType,
           RecordStatus,
           CompanyCode,
           Company,
           FromWarehouseCode,
           ToWarehouseCode,
           DeliveryDate)
    select 'IBT-IL-' + CONVERT(nvarchar(10), il.IntransitLoadId),
           'IBT-IL-' + CONVERT(nvarchar(10), il.IntransitLoadId),
           'TRC',
           'N',
           'GIT',
           'Goods in-transit',
           'GIT',
           'G200',
           dbo.ufn_Getdate()
      from IntransitLoad il (nolock)
     where il.IntransitLoadId = @IntransitLoadId
    
    select @InterfaceImportHeaderId = SCOPE_IDENTITY();
    
    insert InterfaceImportDetail
          (InterfaceImportHeaderId,
           ForeignKey,
           LineNumber,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           Quantity,
           Weight,
           Additional3)
    select @InterfaceImportHeaderId,
           'IBT-IL-' + CONVERT(nvarchar(10), ilj.IntransitLoadId),
           1,
           p.ProductCode,
           p.Product,
           sk.SKUCode,
           b.Batch,
           sum(i.Quantity),
           sum(i.Weight),
           '01'
      from IntransitLoadJob ilj (nolock)
      join Instruction i (nolock) on ilj.JobId = i.JobId
      left
      --join ExternalCompany   ec (nolock) on i.ExternalCompanyId = ec.ExternalCompanyId
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      join Sku               sk (nolock) on su.SkuId = sk.SkuId
      join Product            p (nolock) on su.ProductId = p.ProductId
      join Batch              b (nolock) on sub.BatchId = b.BatchId
     where ilj.IntransitLoadId = @IntransitLoadId
	   group by 'IBT-IL-' + CONVERT(nvarchar(10), ilj.IntransitLoadId),
             p.ProductCode,
             p.Product,
             sk.SKUCode,
             b.Batch
             
             
              -- Insert EXPORT IBT here:
    insert InterfaceExportHeader
          (PrimaryKey,
           OrderNumber,
           RecordType,
           RecordStatus,
           CompanyCode,
           Company,
           FromWarehouseCode,
           ToWarehouseCode,
           DeliveryDate)
    select 'IBT-IL-' + CONVERT(nvarchar(10), il.IntransitLoadId),
           'IBT-IL-' + CONVERT(nvarchar(10), il.IntransitLoadId),
           'IBT',
           'N',
           'GIT',
           'Goods in-transit',
           'S101',
           'GIT',
           dbo.ufn_Getdate()
      from IntransitLoad il (nolock)
     where il.IntransitLoadId = @IntransitLoadId
    
    select @InterfaceExportHeaderId = SCOPE_IDENTITY();
    
    insert InterfaceExportDetail
          (InterfaceExportHeaderId,
           ForeignKey,
           LineNumber,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           Quantity,
           Weight,
           Additional3)
    select @InterfaceExportHeaderId,
           'IBT-IL-' + CONVERT(nvarchar(10), ilj.IntransitLoadId),
           1,
           p.ProductCode,
           p.Product,
           sk.SKUCode,
           b.Batch,
           sum(i.Quantity),
           sum(i.Weight),
           '01'
      from IntransitLoadJob ilj (nolock)
      join Instruction i (nolock) on ilj.JobId = i.JobId
      left
      --join ExternalCompany   ec (nolock) on i.ExternalCompanyId = ec.ExternalCompanyId
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      join Sku               sk (nolock) on su.SkuId = sk.SkuId
      join Product            p (nolock) on su.ProductId = p.ProductId
      join Batch              b (nolock) on sub.BatchId = b.BatchId
     where ilj.IntransitLoadId = @IntransitLoadId
	   group by 'IBT-IL-' + CONVERT(nvarchar(10), ilj.IntransitLoadId),
             p.ProductCode,
             p.Product,
             sk.SKUCode,
             b.Batch
             
             
             
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_IntransitLoad_Finished;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
 
 
 
