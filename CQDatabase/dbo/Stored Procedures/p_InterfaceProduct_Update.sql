﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceProduct_Update
  ///   Filename       : p_InterfaceProduct_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:39
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceProduct table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @ProductDescription nvarchar(510) = null,
  ///   @Barcode nvarchar(510) = null,
  ///   @OuterCartonBarcode nvarchar(510) = null,
  ///   @PalletQuantity float = null,
  ///   @Class nvarchar(100) = null,
  ///   @Modified nvarchar(60) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @CaseQuantity float = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceProduct_Update
(
 @HostId nvarchar(60) = null,
 @ProductCode nvarchar(60) = null,
 @ProductDescription nvarchar(510) = null,
 @Barcode nvarchar(510) = null,
 @OuterCartonBarcode nvarchar(510) = null,
 @PalletQuantity float = null,
 @Class nvarchar(100) = null,
 @Modified nvarchar(60) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @CaseQuantity float = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceProduct
     set HostId = isnull(@HostId, HostId),
         ProductCode = isnull(@ProductCode, ProductCode),
         ProductDescription = isnull(@ProductDescription, ProductDescription),
         Barcode = isnull(@Barcode, Barcode),
         OuterCartonBarcode = isnull(@OuterCartonBarcode, OuterCartonBarcode),
         PalletQuantity = isnull(@PalletQuantity, PalletQuantity),
         Class = isnull(@Class, Class),
         Modified = isnull(@Modified, Modified),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         CaseQuantity = isnull(@CaseQuantity, CaseQuantity),
         InsertDate = isnull(@InsertDate, InsertDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
