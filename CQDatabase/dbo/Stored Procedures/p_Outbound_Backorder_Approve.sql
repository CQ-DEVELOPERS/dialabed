﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Backorder_Approve
  ///   Filename       : p_Outbound_Backorder_Approve.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Backorder_Approve
(
 @IssueId            int = null,
 @OperatorId         int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableIssueLine as table
	 (
	  IssueLineId int
	 )
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @OutboundShipmentId    int,
          @StatusId              int,
          @IssueLineId           int,
          @Count                 int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if not exists(select top 1 1
                  from Issue  i (nolock)
                  join Status s (nolock) on i.StatusId = s.StatusId
                 where IssueId = @IssueId
                   and s.StatusCode = 'BP')
    return 0
  
  begin transaction
  
  select @StatusId = dbo.ufn_StatusId('IS', 'W')
  
  exec @Error = p_Issue_Update
   @IssueId             = @IssueId,
   @StatusId            = @StatusId
  
  if @Error <> 0
    goto error
  
  insert @TableIssueLine
        (IssueLineId)
  select IssueLineId
    from IssueLine (nolock)
   where IssueId = @IssueId
  
  set @Count = @@rowcount
  
  if isnull(@Count,0) < 1
  begin
    rollback transaction
    return 0
  end
  
  while @Count > 0
  begin
    set @Count = @Count - 1
    
    select top 1 @IssueLineId       = IssueLineId
      from @TableIssueLine
    
    delete @TableIssueLine
     where IssueLineId = @IssueLineId
    
    exec @Error = p_IssueLine_Update
     @IssueLineId          = @IssueLineId,
     @StatusId             = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_Outbound_Auto_Load
   @OutboundShipmentId = @OutboundShipmentId output,
   @IssueId            = @IssueId
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
    goto error
  end
  
  exec @Error = p_Outbound_Auto_Release
   @OutboundShipmentId = @OutboundShipmentId,
   @IssueId            = @IssueId
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
    goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Backorder_Approve'
    rollback transaction
    return @Error
end
