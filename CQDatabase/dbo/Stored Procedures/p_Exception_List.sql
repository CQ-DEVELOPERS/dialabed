﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_List
  ///   Filename       : p_Exception_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Exception table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Exception.ExceptionId,
  ///   Exception.Exception 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ExceptionId
        ,'{All}' as Exception
  union
  select
         Exception.ExceptionId
        ,Exception.Exception
    from Exception
  
end
