﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_List_OperatorGroups
  ///   Filename       : p_List_OperatorGroups.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:43
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroup table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OperatorGroup.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_List_OperatorGroups
as
begin
	 set nocount on
	 declare @Error int
  
 
  select
         OperatorGroup.OperatorGroupId,
         OperatorGroup.OperatorGroup 
    from OperatorGroup
  order by OperatorGroup 
  
end
