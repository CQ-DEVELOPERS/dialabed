﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Select
  ///   Filename       : p_Job_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Aug 2014 12:07:53
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Job table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null 
  /// </param>
  /// <returns>
  ///   Job.JobId,
  ///   Job.PriorityId,
  ///   Job.OperatorId,
  ///   Job.StatusId,
  ///   Job.WarehouseId,
  ///   Job.ReceiptLineId,
  ///   Job.IssueLineId,
  ///   Job.ContainerTypeId,
  ///   Job.ReferenceNumber,
  ///   Job.TareWeight,
  ///   Job.Weight,
  ///   Job.NettWeight,
  ///   Job.CheckedBy,
  ///   Job.CheckedDate,
  ///   Job.DropSequence,
  ///   Job.Pallets,
  ///   Job.BackFlush,
  ///   Job.Prints,
  ///   Job.CheckingClear,
  ///   Job.TrackingNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Select
(
 @JobId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Job.JobId
        ,Job.PriorityId
        ,Job.OperatorId
        ,Job.StatusId
        ,Job.WarehouseId
        ,Job.ReceiptLineId
        ,Job.IssueLineId
        ,Job.ContainerTypeId
        ,Job.ReferenceNumber
        ,Job.TareWeight
        ,Job.Weight
        ,Job.NettWeight
        ,Job.CheckedBy
        ,Job.CheckedDate
        ,Job.DropSequence
        ,Job.Pallets
        ,Job.BackFlush
        ,Job.Prints
        ,Job.CheckingClear
        ,Job.TrackingNumber
    from Job
   where isnull(Job.JobId,'0')  = isnull(@JobId, isnull(Job.JobId,'0'))
  order by ReferenceNumber
  
end
