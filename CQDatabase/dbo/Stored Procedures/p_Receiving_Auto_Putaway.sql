﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Auto_Putaway
  ///   Filename       : p_Receiving_Auto_Putaway.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Auto_Putaway
(
 @ReceiptId int
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = object_name(@@procid) + 'Error executing p_Receiving_Auto_Putaway',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @WarehouseId       int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @WarehouseId = WarehouseId
    from Receipt (nolock)
   where ReceiptId = @ReceiptId
  
  if (dbo.ufn_Configuration(456, @WarehouseId) = 0)
    return 0
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  update subl
     set ActualQuantity = case when subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)) < 0
                               then 0
                               else subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0)  - isnull(rl.SampleQuantity,0))
                               end
    from ReceiptLine                rl (nolock)
    join Receipt                     r (nolock) on rl.ReceiptId            = r.ReceiptId
    join StorageUnitBatchLocation subl (nolock) on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                               and subl.LocationId         = r.LocationId
   where rl.ReceiptId = @ReceiptId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update subl
     set ActualQuantity = subl.ActualQuantity + isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)
    from ReceiptLine                rl
    join StorageUnitBatch          sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnitLocation       sul (nolock) on sub.StorageUnitId = sul.StorageUnitId
    join StorageUnitBatchLocation subl (nolock) on sul.LocationId = subl.LocationId
                                               and sub.StorageUnitBatchId  = subl.StorageUnitBatchId
   where rl.ReceiptId = @ReceiptId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  insert StorageUnitBatchLocation
        (StorageUnitBatchId,
         LocationId,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity)
  select rl.StorageUnitBatchId,
         sul.LocationId,
         sum(rl.AcceptedQuantity  - isnull(rl.SampleQuantity,0)),
         0,
         0
    from ReceiptLine                rl (nolock)
    join StorageUnitBatch          sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnitLocation       sul (nolock) on sub.StorageUnitId = sul.StorageUnitId
    left
    join StorageUnitBatchLocation subl (nolock) on sul.LocationId = subl.LocationId
                                               and sub.StorageUnitBatchId  = subl.StorageUnitBatchId
   where rl.ReceiptId = @ReceiptId
     and rl.AcceptedQuantity > 0
     and subl.LocationId is null
  group by rl.StorageUnitBatchId,
           sul.LocationId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
 
