﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallets_Insert
  ///   Filename       : p_Pallets_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:53
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Pallets table.
  /// </remarks>
  /// <param>
  ///   @pallet_id int = null,
  ///   @stock_no nvarchar(60) = null,
  ///   @sku_code nvarchar(30) = null,
  ///   @lot_code nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   Pallets.pallet_id,
  ///   Pallets.stock_no,
  ///   Pallets.sku_code,
  ///   Pallets.lot_code 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallets_Insert
(
 @pallet_id int = null,
 @stock_no nvarchar(60) = null,
 @sku_code nvarchar(30) = null,
 @lot_code nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert Pallets
        (pallet_id,
         stock_no,
         sku_code,
         lot_code)
  select @pallet_id,
         @stock_no,
         @sku_code,
         @lot_code 
  
  select @Error = @@Error
  
  
  return @Error
  
end
