﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Shipment_Create_Search
  ///   Filename       : p_Outbound_Shipment_Create_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Shipment_Create_Search
(
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	  nvarchar(30),
 @ExternalCompany	      nvarchar(255),
 @OrderNumber	          nvarchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
 (
  IssueId            int,
  OutboundDocumentId    int,
  OutboundShipmentId    int,
  OrderNumber          nvarchar(30),
  ExternalCompanyCode  nvarchar(30),
  ExternalCompany      nvarchar(255),
  NumberOfLines        int,
  DeliveryDate         datetime,
  StatusId             int,
  Status               nvarchar(50),
  OutboundDocumentType  nvarchar(50),
  LocationId           int,
  Location             nvarchar(15),
  CreateDate           datetime,
  Rating               int
   );
  
  insert @TableResult
        (IssueId,
         id.OutboundDocumentId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         DeliveryDate,
         StatusId,
         Status,
         OutboundDocumentType,
         LocationId,
         Location,
         CreateDate,
         Rating)
  select i.IssueId,
         id.OutboundDocumentId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         id.DeliveryDate,
         i.StatusId,
         s.Status,
         idt.OutboundDocumentType,
         l.LocationId,
         l.Location,
         id.CreateDate,
         ec.Rating
    from OutboundDocument     id  (nolock)
    join OutboundDocumentType idt (nolock) on id.OutboundDocumentTypeId = idt.OutboundDocumentTypeId
    join ExternalCompany      ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
    join Issue                i   (nolock) on id.OutboundDocumentId     = i.OutboundDocumentId
    join Status               s   (nolock) on i.StatusId               = s.StatusId
    join Location             l   (nolock) on i.LocationId             = l.LocationId
   where id.OutboundDocumentTypeId  = isnull(@OutboundDocumentTypeId, id.OutboundDocumentTypeId)
     and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and id.OrderNumber         like isnull(@OrderNumber  + '%', id.OrderNumber)
     and i.DeliveryDate      between @FromDate and @ToDate
     and s.Type                    = 'IS'
     and s.StatusCode             in ('W','C','D') -- Waiting, Confirmed, Delivered
  
  update @TableResult
     set OutboundShipmentId = isi.OutboundShipmentId
    from @TableResult t
    join OutboundShipmentIssue isi on t.IssueId = isi.IssueId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from OutboundLine il
                           where t.OutboundDocumentId = il.OutboundDocumentId)
    from @TableResult t
  
  select OutboundShipmentId,
         IssueId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         StatusId,
         Status,
         OutboundDocumentType,
         LocationId,
         Location,
         CreateDate,
         Rating
    from @TableResult
   where OutboundShipmentId is not null
end
