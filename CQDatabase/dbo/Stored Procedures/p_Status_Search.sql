﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Search
  ///   Filename       : p_Status_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:55
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Status table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null output,
  ///   @Status nvarchar(100) = null,
  ///   @StatusCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Status.StatusId,
  ///   Status.Status,
  ///   Status.StatusCode,
  ///   Status.Type,
  ///   Status.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Search
(
 @StatusId int = null output,
 @Status nvarchar(100) = null,
 @StatusCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @Status = '-1'
    set @Status = null;
  
  if @StatusCode = '-1'
    set @StatusCode = null;
  
 
  select
         Status.StatusId
        ,Status.Status
        ,Status.StatusCode
        ,Status.Type
        ,Status.OrderBy
    from Status
   where isnull(Status.StatusId,'0')  = isnull(@StatusId, isnull(Status.StatusId,'0'))
     and isnull(Status.Status,'%')  like '%' + isnull(@Status, isnull(Status.Status,'%')) + '%'
     and isnull(Status.StatusCode,'%')  like '%' + isnull(@StatusCode, isnull(Status.StatusCode,'%')) + '%'
  
end
