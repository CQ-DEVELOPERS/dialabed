﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Build_Unlink
  ///   Filename       : p_Pallet_Build_Unlink.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Build_Unlink
(
 @warehouseId int,
 @operatorId int,
 @barcode nvarchar(50),
 @referenceNumber nvarchar(50)
)
 
as
begin
  set nocount on;
  
  declare @Error              int,
          @Errormsg           varchar(500) = 'Error executing p_Pallet_Build_Unlink',
          @GetDate            datetime,
          @Transaction        bit = 0,
          @JobId              int,
          @PalletId           int,
          @InstructionId      int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(replace(@barcode,'P:','')) = 1 and @barcode like 'P:%'
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  select @InstructionId = i.InstructionId,
         @JobId = i.JobId
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                                      and it.InstructionTypeCode = 'S'
    join Job                j (nolock) on i.JobId = j.JobId
    join Status             s (nolock) on j.StatusId = s.StatusId
                                      and s.StatusCode = 'PR'
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where b.Batch = @referenceNumber
     and i.PalletId = @PalletId
  
  exec @Error = p_Housekeeping_Instruction_Delete
   @InstructionId = @InstructionId,
   @JobId         = @JobId,
   @operatorId    = @operatorId
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
