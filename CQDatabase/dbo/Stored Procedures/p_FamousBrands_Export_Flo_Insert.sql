﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_Flo_Insert
  ///   Filename       : p_FamousBrands_Export_Flo_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Export_Flo_Insert
 
as
begin
	 set nocount on;
	 
  declare @ProcessedDate datetime
  
  select @ProcessedDate = dbo.ufn_Getdate()
  
  insert FLOInvoiceMasterExport
        (UniqueNumber,
         InvoiceNumber,
         Detail,
         CustomerNumber,
         DeliveryDateFrom,
         DeliveryDateTo,
         Preschedule,
         AmountOfInvoice,
         COD,
         PickUpCustomer,
         LoadPriority,
         JobWindowStart,
         JobWindowEnd,
         JobDuration,
         HoldStatus,
         SpecialInstructions,
         RecordStatus,
         ProcessedDate)
  select distinct t.UniqueNumber,
         t.InvoiceNumber,
         t.Detail,
         t.CustomerNumber,
         t.DeliveryDateFrom,
         t.DeliveryDateTo,
         t.Preschedule,
         t.AmountOfInvoice,
         t.COD,
         t.PickUpCustomer,
         t.LoadPriority,
         t.JobWindowStart,
         t.JobWindowEnd,
         t.JobDuration,
         t.HoldStatus,
         t.SpecialInstructions,
         'N',
         @ProcessedDate
    from FloInvoiceMasterImport t
   where not exists(select top 1 1 from FloInvoiceMasterExport x where t.UniqueNumber = x.UniqueNumber)
     and exists(select top 1 1
                  from OutboundDocument od
                  join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
                 where i.RoutingSystem = 0
                   and t.UniqueNumber = od.OrderNumber)
  
  update i
     set RoutingSystem = 1
    from OutboundDocument      od
    join Issue                  i on od.OutboundDocumentId = i.OutboundDocumentId
    join FloInvoiceMasterImport t on od.OrderNumber = t.UniqueNumber
   where i.RoutingSystem = 0
  
  insert FLOInvoiceDetailExport
        (ID,
         UniqueNumber,
         ProductCode,
         Units,
         Quantity,
         PickUp,
         ShippedQuantity,
         RecordStatus,
         ProcessedDate)
  select distinct t.ID,
         t.UniqueNumber,
         t.ProductCode,
         t.Units,
         t.Quantity,
         t.PickUp,
         t.ShippedQuantity,
         'N',
         m.ProcessedDate
    from FloInvoiceMasterExport m
    join FLOInvoiceDetailImport t on m.UniqueNumber = t.UniqueNumber
   where m.ProcessedDate = @ProcessedDate
  
  insert FloDeliverypointExport
        (DeliveryPoint,
         RecordStatus,
         ProcessedDate)
  select t.DeliveryPoint,
         'N',
         m.ProcessedDate
    from FloInvoiceMasterExport m
    join FloCustomerImport      t on m.CustomerNumber  = t.CustomerNumber
   where m.ProcessedDate = @ProcessedDate
  
  insert FloCustomerExport
        (CustomerNumber,
         Name,
         Class,
         DeliveryPoint,
         Address1,
         Address2,
         Address3,
         Address4,
         Contact,
         Tel,
         Fax,
         DeliveryGroup,
         DepotCode,
         VisitFrequency,
         FloCustomerNumber,
         RecordStatus,
         ProcessedDate)
  select t.CustomerNumber,
         t.Name,
         t.Class,
         t.DeliveryPoint,
         t.Address1,
         t.Address2,
         t.Address3,
         t.Address4,
         t.Contact,
         t.Tel,
         t.Fax,
         t.DeliveryGroup,
         t.DepotCode,
         t.VisitFrequency,
         t.FloCustomerNumber,
         'N',
         m.ProcessedDate
    from FloInvoiceMasterExport m
    join FloCustomerImport      t on m.CustomerNumber  = t.CustomerNumber
   where m.ProcessedDate = @ProcessedDate
  
  insert FloProductMasterExport
        (Code,
         Name,
         Class,
         RecordStatus,
         ProcessedDate)
  select t.Code,
         t.Name,
         t.Class,
         'N',
         m.ProcessedDate
    from FloInvoiceMasterExport m
    join FLOInvoiceDetailImport d on m.UniqueNumber = d.UniqueNumber
    join FloProductMasterImport t on d.ProductCode  = t.Code
   where m.ProcessedDate = @ProcessedDate
  
  insert FloProductDetailExport
        (ID,
         Code,
         Units,
         Quantity,
         Unitsize,
         Weight,
         Length,
         Width,
         Height,
         RecordStatus,
         ProcessedDate)
  select distinct t.ID,
         t.Code,
         t.Units,
         t.Quantity,
         t.Unitsize,
         t.Weight,
         t.Length,
         t.Width,
         t.Height,
         'N',
         m.ProcessedDate
    from FloInvoiceMasterExport m
    join FLOInvoiceDetailImport d on m.UniqueNumber = d.UniqueNumber
    join FloProductDetailImport t on d.ProductCode  = t.Code
   where m.ProcessedDate = @ProcessedDate
end
