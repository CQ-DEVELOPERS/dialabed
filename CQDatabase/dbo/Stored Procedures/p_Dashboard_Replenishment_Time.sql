﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Replenishment_Time
  ///   Filename       : p_Dashboard_Replenishment_Time.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Replenishment_Time
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select [Type],
	          Area,
	          Value
	     from DashboardReplenishmentTime
	    where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardReplenishmentTime
	   
	   insert DashboardReplenishmentTime
	         (WarehouseId,
	          [Type],
	          AreaId,
	          Area,
	          Value)
	   select a.WarehouseId,
	          'Actual',
	          a.AreaId,
	          a.Area,
	          sum(datediff(ss, i.StartDate, i.EndDate) / 60) / count(1) as 'Time'
	     from Instruction      i (nolock)
	     join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
	     join Status           s (nolock) on i.StatusId          = s.StatusId
	     join AreaLocation    al (nolock) on i.PickLocationId    = al.LocationId 
	     join Area             a (nolock) on al.AreaId           = a.AreaId
	    where i.WarehouseId           = isnull(@WarehouseId, i.WarehouseId)
	      and it.InstructionTypeCode in ('R')
	      and s.StatusCode           in ('F','NS')
	      and datediff(hh, i.StartDate, i.EndDate) > 0
	   group by a.WarehouseId,
	            a.AreaId,
	            a.Area
  	 
	   insert DashboardReplenishmentTime
	         (WarehouseId,
	          [Type],
	          AreaId,
	          Area,
	          Value)
	   select a.WarehouseId,
	          'KPI',
	          a.AreaId,
	          a.Area,
	          3.350 as 'KPI'
	     from Area          a
	     join DashboardReplenishmentTime tr on a.AreaId = tr.AreaId
	 end
end
