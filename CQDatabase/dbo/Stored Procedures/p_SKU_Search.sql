﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKU_Search
  ///   Filename       : p_SKU_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:06
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the SKU table.
  /// </remarks>
  /// <param>
  ///   @SKUId int = null output,
  ///   @UOMId int = null,
  ///   @SKU nvarchar(100) = null,
  ///   @SKUCode nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   SKU.SKUId,
  ///   SKU.UOMId,
  ///   UOM.UOM,
  ///   SKU.SKU,
  ///   SKU.SKUCode,
  ///   SKU.Quantity,
  ///   SKU.AlternatePallet,
  ///   SKU.SubstitutePallet,
  ///   SKU.Litres,
  ///   SKU.ParentSKUCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKU_Search
(
 @SKUId int = null output,
 @UOMId int = null,
 @SKU nvarchar(100) = null,
 @SKUCode nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @SKUId = '-1'
    set @SKUId = null;
  
  if @UOMId = '-1'
    set @UOMId = null;
  
  if @SKU = '-1'
    set @SKU = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
 
  select
         SKU.SKUId
        ,SKU.UOMId
         ,UOMUOMId.UOM as 'UOM'
        ,SKU.SKU
        ,SKU.SKUCode
        ,SKU.Quantity
        ,SKU.AlternatePallet
        ,SKU.SubstitutePallet
        ,SKU.Litres
        ,SKU.ParentSKUCode
    from SKU
    left
    join UOM UOMUOMId on UOMUOMId.UOMId = SKU.UOMId
   where isnull(SKU.SKUId,'0')  = isnull(@SKUId, isnull(SKU.SKUId,'0'))
     and isnull(SKU.UOMId,'0')  = isnull(@UOMId, isnull(SKU.UOMId,'0'))
     and isnull(SKU.SKU,'%')  like '%' + isnull(@SKU, isnull(SKU.SKU,'%')) + '%'
     and isnull(SKU.SKUCode,'%')  like '%' + isnull(@SKUCode, isnull(SKU.SKUCode,'%')) + '%'
  
end
