﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestType_Select
  ///   Filename       : p_ManualFileRequestType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:05
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ManualFileRequestType table.
  /// </remarks>
  /// <param>
  ///   @ManualFileRequestTypeId int = null 
  /// </param>
  /// <returns>
  ///   ManualFileRequestType.ManualFileRequestTypeId,
  ///   ManualFileRequestType.Description,
  ///   ManualFileRequestType.DocumentTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestType_Select
(
 @ManualFileRequestTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ManualFileRequestType.ManualFileRequestTypeId
        ,ManualFileRequestType.Description
        ,ManualFileRequestType.DocumentTypeCode
    from ManualFileRequestType
   where isnull(ManualFileRequestType.ManualFileRequestTypeId,'0')  = isnull(@ManualFileRequestTypeId, isnull(ManualFileRequestType.ManualFileRequestTypeId,'0'))
  
end
