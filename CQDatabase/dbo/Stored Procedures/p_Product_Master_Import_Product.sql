﻿   
/*  
  /// <summary>  
  ///   Procedure Name : p_Product_Master_Import_Product  
  ///   Filename       : p_Product_Master_Import_Product.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 19 Feb 2009  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    : Karen  
  ///   Modified Date  : 21 September 2010  
  ///   Details        : Create detailed error log InterfaceDashboardBulkLog  
  /// </newpara>  
*/  
CREATE procedure p_Product_Master_Import_Product
   
as  
begin  
  set nocount on;  
    
  declare @Error                           int,  
          @Errormsg                        varchar(500),  
          @GetDate                         datetime,  
          @ProductId                       int,  
          @SKUId                           int,  
          @StorageUnitBatchId              int,  
          @BatchId                         int,  
          @Quantity                        float,  
          @StatusId                        int,  
          @InterfaceImportProductId        int,  
          @RecordStatus                    char(1),  
          @ProcessedDate                   datetime,  
          @ProductCode                     varchar(255),
          @Product                         varchar(255),  
          @SKUCode                         varchar(10),  
          @SKU                             varchar(10),  
          @PalletQuantity                  float,  
          @Barcode                         varchar(50),  
          @MinimumQuantity                 float,  
          @ReorderQuantity                 float,  
          @MaximumQuantity                 float,  
          @CuringPeriodDays                int,  
          @ShelfLifeDays                   int,  
          @QualityAssuranceIndicator       bit,  
          @ProductType                     varchar(20),  
          @ProductCategory                 varchar(50),  
          @OverReceipt                     numeric(13,3),  
          @HostId                          varchar(30),  
          @Samples                         float,  
          @RetentionSamples                float,
          @AssaySamples                    float,
          @PackCode                        varchar(10),  
          @PackDescription                 varchar(255),  
          @Length                          numeric(13,6),  
          @Width                           numeric(13,6),  
          @Height                          numeric(13,6),  
          @Volume                          float,
          @NettWeight                      float,
          @GrossWeight                     float,
          @TareWeight                      float,
          @PackingCategory                 char(1),  
          @PickEmpty                       bit,  
          @StackingCategory                int,  
          @MovementCategory                int,  
          @ValueCategory                   int,  
          @StoringCategory                 int,  
          @PickPartPallet                  int,  
          @PackTypeId                      int,  
          @StorageUnitId                   int,  
          @PackId                          int,  
          @WarehouseId                     int,  
          @ParentProductCode               nvarchar(255),
          @Description2                    nvarchar(255),
          @Description3                    nvarchar(255),
          @PrincipalCode                   nvarchar(30),
          @PrincipalId                     int,
          @SizeCode                        nvarchar(50),
          @Size                            nvarchar(255),
          @ColourCode                      nvarchar(50),
          @Colour                          nvarchar(255),
          @StyleCode                       nvarchar(50),
          @Style                           nvarchar(255),
          @UnitPrice					                  numeric(13,2),
          @PreviousPrice																			numeric(13,2),
          @ProductAlias                    nvarchar(30),
          @PutawayRule                     nvarchar(100),
          @AllocationRule                  nvarchar(100),
          @BillingRule                     nvarchar(100),
          @CycleCountRule                 	nvarchar(100),
          @LotAttributeRule                nvarchar(100),
          @StorageCondition                nvarchar(100),
          @SerialTracked                   bit,
          @Weight                          nvarchar(20),
          @ProductGroup                    nvarchar(255),
          @ManualCost                      nvarchar(30),
          @OffSet                          nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()  
  set @Error = 0

  select @WarehouseId = min(WarehouseId)
    from Warehouse
   where WarehouseId = ParentWarehouseId

   if @WarehouseId is null
     select @WarehouseId = min(WarehouseId)
       from Warehouse
  
  update InterfaceImportProduct  
     set ProcessedDate = @Getdate  
   where RecordStatus in ('N','U')  
     and ProcessedDate is null
     
  update p
     set Additional1 = (select top 1 Additional1
					                    from InterfaceImportProduct psub (nolock)
					                   where psub.ProductCode = p.ProductCode
					                     and ProcessedDate = @GetDate
     					                and Additional1 <> '')
    from InterfaceImportProduct p
   where ProcessedDate = @GetDate
     and Additional1 = ''
  
  declare Product_cursor cursor for  
   select InterfaceImportProductId,  
          left(ProductCode,30),
          Product,
          SKUCode,  
          SKU,  
          PalletQuantity,  
          Barcode,  
          MinimumQuantity,  
          ReorderQuantity,  
          MaximumQuantity,  
          CuringPeriodDays,  
          ShelfLifeDays,  
          QualityAssuranceIndicator,  
          ProductType,  
          ProductCategory,  
          OverReceipt,  
          Samples,  
          RetentionSamples,  
          AssaySamples,  
          HostId,  
          isnull(ParentProductCode, Additional1),
          isnull(Description2, Additional2),
          isnull(Description3, Additional3),
          PrincipalCode,
          UnitPrice,
          SizeCode,
          Size,
          ColourCode,
          Colour,
          StyleCode,
          Style,
          ProductAlias,
          PutawayRule,
          AllocationRule,
          BillingRule,
          CycleCountRule,
          LotAttributeRule,
          StorageCondition,
          SerialTracked,
          convert(nvarchar(20), [Weight]),
          ProductGroup,
          ManualCost,
          OffSet
     from InterfaceImportProduct  
    where ProcessedDate = @Getdate  
    
  open Product_cursor  
    
  fetch Product_cursor into @InterfaceImportProductId,  
                            @ProductCode,  
                            @Product,  
                            @SKUCode,  
                            @SKU,  
                            @PalletQuantity,  
                            @Barcode,  
                            @MinimumQuantity,  
                            @ReorderQuantity,  
                            @MaximumQuantity,  
                            @CuringPeriodDays,  
                            @ShelfLifeDays,  
                            @QualityAssuranceIndicator,  
                            @ProductType,  
                            @ProductCategory,  
                            @OverReceipt,  
                            @Samples,  
                            @RetentionSamples,  
                            @AssaySamples,  
                            @HostId,  
                            @ParentProductCode,
                            @Description2,
                            @Description3,
                            @PrincipalCode,
                            @UnitPrice,
                            @SizeCode,
                            @Size,
                            @ColourCode,
                            @Colour,
                            @StyleCode,
                            @Style,
                            @ProductAlias,
                            @PutawayRule,
                            @AllocationRule,
                            @BillingRule,
                            @CycleCountRule,
                            @LotAttributeRule,
                            @StorageCondition,
                            @SerialTracked,
                            @Weight,
                            @ProductGroup,
                            @ManualCost,
                            @OffSet
    
  while (@@fetch_status = 0)  
  begin  
    begin transaction
    
    if @SizeCode = ''
      set @SizeCode = null
    
    if @ColourCode = ''
      set @ColourCode = null
    
    set @Weight = replace(@Weight,' KG','')
    
    if isnumeric(@Weight) = '0'
      set @Weight = null
    
    select @StatusId = dbo.ufn_StatusId('P','A')  
      
    if @QualityAssuranceIndicator is null  
      set @QualityAssuranceIndicator = 0
    
    set @PrincipalId = null
    
    select @PrincipalId = PrincipalId
      from Principal (nolock)
     where PrincipalCode = @PrincipalCode
    
    if (select dbo.ufn_Configuration(457, @WarehouseId)) = 1
      if @PrincipalId is null
      begin
        set @Error = -1
        select @ErrorMsg = 'Error importing product - principal does not exist'
        goto error  
      end
    else
      if @PrincipalId is null
      begin
        exec @Error = p_Principal_Insert
         @PrincipalId   = @PrincipalId out,
         @PrincipalCode = @PrincipalCode,
         @Principal     = @PrincipalCode
        
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_Principal_Insert'  
          goto error  
        end
      end
    
    set @ProductId = null
    
    if @HostId = ''
      set @HostId = null
    
    select @ProductId = p.ProductId
      from Product p
      join StorageUnit su on p.ProductId = su.ProductId
     where ((ProductCode = @ProductCode and isnull(@HostId,'') = '') or (HostId = @HostId))
       and PrincipalId = @PrincipalId
       and (su.SizeCode = @SizeCode or @SizeCode is null)
       and (su.ColourCode = @ColourCode or @ColourCode is null)
    
    if @ProductId is null and isnull(@HostId,'') != ''
      select @ProductId = p.ProductId
        from Product p
        join StorageUnit su on p.ProductId = su.ProductId
       where (ProductCode = @ProductCode or HostId = @HostId)
         and p.PrincipalId is null
         and (su.SizeCode = @SizeCode or @SizeCode is null)
         and (su.ColourCode = @ColourCode or @ColourCode is null)
    
    if @ProductId is null and isnull(@HostId,'') = ''
      select @ProductId = p.ProductId
        from Product      p
        join StorageUnit su on p.ProductId = su.ProductId
        join SKU        sku on su.SKUId = sku.SKUId
       where p.ProductCode = @ProductCode
         and sku.SKUCode = @SKUCode
         and p.PrincipalId is null
         and (su.SizeCode = @SizeCode or @SizeCode is null)
         and (su.ColourCode = @ColourCode or @ColourCode is null)
    
    if @ParentProductCode is null or @ParentProductCode = ''  
      set @ParentProductCode = @ProductCode  
    
    set @Product = replace(@Product,'&amp;','&')  
    
    if @Barcode = ''
		set @Barcode = null
    
    if @ProductId is null  
    begin  
      exec @Error = p_Product_Insert  
       @ProductId                 = @ProductId output,  
       @StatusId                  = @StatusId,  
       @ProductCode               = @ProductCode,  
       @Product                   = @Product,  
       @Barcode                   = @Barcode,  
       @MinimumQuantity           = @MinimumQuantity,  
       @ReorderQuantity           = @ReorderQuantity,  
       @MaximumQuantity           = @MaximumQuantity,  
       @CuringPeriodDays          = @CuringPeriodDays,  
       @ShelfLifeDays             = @ShelfLifeDays,  
       @QualityAssuranceIndicator = @QualityAssuranceIndicator,  
       @ProductType               = @ProductType,  
       @OverReceipt               = @OverReceipt,  
       @HostId                    = @HostId,  
       @Samples                   = @Samples,
       @RetentionSamples          = @RetentionSamples,
       @AssaySamples              = @AssaySamples,
       @ParentProductCode         = @ParentProductCode,
       @Description2              = @Description2,
       @Description3              = @Description3,
       @PrincipalId               = @PrincipalId,
       @ProductAlias              = @ProductAlias,
       @ProductGroup              = @ProductGroup
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_Product_Insert'  
        goto error  
      end  
    end  
    else  
    begin  
      exec @Error = p_Product_Update  
       @ProductId                 = @ProductId,  
       @StatusId                  = @StatusId,  
       @ProductCode               = @ProductCode,  
       @Product                   = @Product,  
       @Barcode                   = @Barcode,  
       @MinimumQuantity           = @MinimumQuantity,  
       @ReorderQuantity           = @ReorderQuantity,  
       @MaximumQuantity           = @MaximumQuantity,  
       @CuringPeriodDays          = @CuringPeriodDays,  
       @ShelfLifeDays             = @ShelfLifeDays,  
       @QualityAssuranceIndicator = @QualityAssuranceIndicator,  
       @ProductType               = @ProductType,  
       @OverReceipt               = @OverReceipt,  
       @HostId                    = @HostId,  
       @Samples                   = @Samples,
       @RetentionSamples          = @RetentionSamples,
       @AssaySamples              = @AssaySamples,
       @ParentProductCode         = @ParentProductCode,
       @Description2              = @Description2,
       @Description3              = @Description3,
       @PrincipalId               = @PrincipalId,
       @ProductAlias              = @ProductAlias,
       @ProductGroup              = @ProductGroup
      
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_Product_Update'  
        goto error  
      end  
    end  
      
    set @SKUId = null  
      
    select @SKUId = SKUId,  
           @Quantity = Quantity  
      from SKU (nolock)  
     where SKUCode = @SKUCode
      
    if @SKU is null  
      set @SKU = @SKUCode  
      
    if @Quantity is null  
      set @Quantity = 99999  
      
    if @SKUId is null  
    begin  
      exec @Error = p_SKU_Insert  
       @SKUId            = @SKUId output,  
       @UOMId            = 1,  
       @SKU              = @SKU,  
       @SKUCode          = @SKUCode,  
       @Quantity         = @Quantity,  
       @AlternatePallet  = null,  
       @SubstitutePallet = null,  
       @Litres           = null  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_SKU_Insert'  
        goto error  
      end  
    end  
    else  
    begin  
      exec @Error = p_SKU_Update  
       @SKUId            = @SKUId,  
       @UOMId            = 1,  
       @SKU              = @SKU,  
       @SKUCode          = @SKUCode,  
       @Quantity         = @Quantity,  
       @AlternatePallet  = null,  
       @SubstitutePallet = null,  
       @Litres           = null  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_SKU_Update'  
        goto error  
      end  
    end  
      
    set @StorageUnitId = null  
    
    select @StorageUnitId = StorageUnitId  
      from StorageUnit  
     where ProductId  = @ProductId  
       and SKUId      = @SKUId
       and (SizeCode   = @SizeCode or @SizeCode is null)
       and (ColourCode = @ColourCode or @ColourCode is null) 
    
    if @StorageUnitId is null
      select @StorageUnitId = StorageUnitId
        from StorageUnit
       where ProductId  = @ProductId  
         and SKUId      = @SKUId
    
    if @StorageUnitId is null  
    begin  
      exec @Error = p_StorageUnit_Insert  
       @StorageUnitId    = @StorageUnitId output,  
       @SKUId            = @SKUId,  
       @ProductId        = @ProductId,
       @StyleCode        = @StyleCode,
       @Style            = @Style,
       @ColourCode       = @ColourCode,
       @Colour           = @Colour,
       @SizeCode         = @SizeCode,
       @Size             = @Size,
       @UnitPrice		      = @UnitPrice,
       @PutawayRule      = @PutawayRule,
       @AllocationRule   = @AllocationRule,
       @BillingRule      = @BillingRule,
       @CycleCountRule   = @CycleCountRule,
       @LotAttributeRule = @LotAttributeRule,
       @StorageCondition = @StorageCondition,
       @SerialTracked    = @SerialTracked,
       @ManualCost       = @ManualCost,
       @OffSet           = @OffSet
       --@ProductCategory  = @ProductCategory,  
       --@PackingCategory  = @PackingCategory,  
       --@PickEmpty        = @PickEmpty,  
       --@StackingCategory = @StackingCategory,  
       --@MovementCategory = @MovementCategory,  
       --@ValueCategory    = @ValueCategory,  
       --@StoringCategory  = @StoringCategory,  
       --@PickPartPallet   = @PickPartPallet,  
       --@MinimumQuantity  = @MinimumQuantity,  
       --@ReorderQuantity  = @ReorderQuantity,  
       --@MaximumQuantity  = @MaximumQuantity  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnit_Insert'  
        goto error  
      end  
    end  
    else  
    begin  
      exec @Error = p_StorageUnit_Update  
       @StorageUnitId    = @StorageUnitId,  
       @SKUId            = @SKUId,  
       @ProductId        = @ProductId,
       @StyleCode        = @StyleCode,
       @Style            = @Style,
       @ColourCode       = @ColourCode,
       @Colour           = @Colour,
       @SizeCode         = @SizeCode,
       @Size             = @Size,
       @UnitPrice		      = @UnitPrice,
       @PutawayRule      = @PutawayRule,
       @AllocationRule   = @AllocationRule,
       @BillingRule      = @BillingRule,
       @CycleCountRule   = @CycleCountRule,
       @LotAttributeRule = @LotAttributeRule,
       @StorageCondition = @StorageCondition,
       @SerialTracked    = @SerialTracked,
       @ManualCost       = @ManualCost,
       @OffSet           = @OffSet
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnit_Update'  
        goto error  
      end
    end  
      
    select @StatusId = dbo.ufn_StatusId('B','A')  
      
    set @BatchId = null  
      
    select @BatchId = BatchId  
      from Batch  
     where Batch = 'Default'  
      
    if @BatchId is null  
    begin  
      exec @Error = p_Batch_Insert  
         @BatchId        = @BatchId output,  
         @StatusId       = @StatusId,  
         @WarehouseId    = @WarehouseId,  
         @Batch          = 'Default',  
         @CreateDate     = @getdate,  
         @ExpiryDate     = '2029-12-31',  
         @IncubationDays = 0,  
         @ShelfLifeDays  = 365,  
         @ExpectedYield  = null,  
         @ActualYield    = null,  
         @ECLNumber      = null  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_Batch_Insert'  
        goto error  
      end  
    end  
      
    set @StorageUnitBatchId = null  
      
    select @StorageUnitBatchId = StorageUnitBatchId  
      from StorageUnitBatch  
     where StorageUnitId = @StorageUnitId  
       and BatchId       = @BatchId  
    
    if @StorageUnitBatchId is null  
    begin  
      exec @Error = p_StorageUnitBatch_Insert  
       @StorageUnitBatchId = @StorageUnitBatchId output,  
       @BatchId            = @BatchId,  
       @StorageUnitId      = @StorageUnitId  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'  
        goto error  
      end  
    end  
    else  
    begin  
      exec @Error = p_StorageUnitBatch_Update  
       @StorageUnitBatchId = @StorageUnitBatchId,  
       @BatchId            = @BatchId,  
       @StorageUnitId      = @StorageUnitId  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnitBatch_Update'  
        goto error  
      end  
    end  
      
    declare Pack_cursor cursor for  
     select PackCode,  
            PackDescription,  
            isnull(Quantity, 1),  
            ISNULL(Barcode, @Barcode),  
            Length,  
            Width,  
            Height,  
            Volume,  
            NettWeight,  
            GrossWeight,  
            TareWeight,  
            ProductCategory,  
            PackingCategory,  
            PickEmpty,  
            StackingCategory,  
            MovementCategory,  
            ValueCategory,  
            StoringCategory,  
            PickPartPallet  
       from InterfaceImportPack
      where InterfaceImportProductId = @InterfaceImportProductId
     --union
     --select pt.PackTypeCode,
     --       pt.PackType,  
     --       p.PalletQuantity,  
     --       isnull(pk.Barcode, @Barcode),  
     --       pk.Length * p.PalletQuantity,
     --       pk.Width * p.PalletQuantity,
     --       pk.Height * p.PalletQuantity,
     --       pk.Volume * p.PalletQuantity,
     --       pk.NettWeight * p.PalletQuantity,
     --       pk.GrossWeight * p.PalletQuantity,
     --       pk.TareWeight * p.PalletQuantity,
     --       pk.ProductCategory,
     --       pk.PackingCategory,
     --       pk.PickEmpty,
     --       pk.StackingCategory,
     --       pk.MovementCategory,
     --       pk.ValueCategory,
     --       pk.StoringCategory,
     --       pk.PickPartPallet
     --  from PackType               pt
     --  join InterfaceImportProduct  p on p.InterfaceImportProductId = @InterfaceImportProductId
     --  left
     --  join InterfaceImportPack    pk on p.InterfaceImportProductId = pk.InterfaceImportProductId
     -- where pt.Pallet = 1
     union
     select PackTypeCode,  
            PackType,  
            case when Unit = 1 then 1
                 when Pallet = 1 then 99999
                 end,  
            null,  
            null,  
            null,  
            null,  
            null,  
            null,  
            convert(numeric(13,6), @Weight),
            null,  
            null,  
            null,  
            null,  
            null,  
            null,  
            null,  
            null,  
            null
       from PackType def
      where (Unit = 1 or Pallet = 1)
        and not exists(select top 1 1
                         from InterfaceImportPack pk
                         join PackType            pt on (pk.PackCode = pt.PackTypeCode
                                                    and pt.Pallet = def.Pallet)
                                                    or (pk.PackCode = pt.PackTypeCode
                                                    and  pt.Unit = def.Unit)
                        where pk.InterfaceImportProductId = @InterfaceImportProductId)

    open Pack_cursor  
      
    fetch Pack_cursor into @PackCode,  
                           @PackDescription,  
                           @Quantity,  
                           @Barcode,  
                           @Length,  
                           @Width,  
                           @Height,  
                           @Volume,  
                           @NettWeight,  
                           @GrossWeight,  
                           @TareWeight,  
                           @ProductCategory,  
                           @PackingCategory,  
                           @PickEmpty,  
                           @StackingCategory,  
                           @MovementCategory,  
                           @ValueCategory,  
                           @StoringCategory,  
                           @PickPartPallet  
      
    while (@@fetch_status = 0)  
    begin
      if @GrossWeight is null
        set @GrossWeight = 1
      
      set @PackTypeId = null  
      
      if @PackCode is not null
        select @PackTypeId = PackTypeId  
          from PackType (nolock)
         where PackTypeCode = @PackCode
      else if @PackDescription is not null
        select @PackTypeId = PackTypeId  
          from PackType (nolock)
         where PackType = @PackDescription
      
      if @PackDescription is null
        set @PackDescription = @PackCode
      
      if @PackTypeId is null  
      begin
        exec @Error = p_PackType_Insert
         @PackTypeId       = @PackTypeId output,
         @PackTypeCode     = @PackCode,
         @PackType         = @PackDescription,
         @InboundSequence  = 1,
         @OutboundSequence = 1
          
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_PackType_Insert'  
          goto error  
        end  
      end  
      else  
      begin  
        exec @Error = p_PackType_Update  
         @PackTypeId       = @PackTypeId,
         @PackTypeCode     = @PackCode,
         @PackType         = @PackDescription
        
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_PackType_Update'  
          goto error  
        end  
      end  
      
      set @PackId = null  
      
      if @Barcode = ''
        set @Barcode = null;
      
      select @PackId = PackId  
        from Pack (nolock)
       where StorageUnitId = @StorageUnitId  
         and PackTypeId    = @PackTypeId  
         and WarehouseId   = @WarehouseId
         and (Barcode      = @Barcode or @Barcode is null or Barcode = '')
      
      if @Quantity is null
        set @Quantity = 1
      
      if @PackId is null  
      begin  
        exec @Error = p_Pack_Insert  
         @PackId        = @PackId output,  
         @WarehouseId   = @WarehouseId,  
         @StorageUnitId = @StorageUnitId,  
         @PackTypeId    = @PackTypeId,  
         @Quantity      = @Quantity,  
         @Barcode       = @Barcode,  
         @Length        = @Length,  
         @Width         = @Width,  
         @Height        = @Height,  
         @Volume        = @Volume,  
         @Weight        = @GrossWeight,  
         @NettWeight    = @NettWeight,  
         @TareWeight    = @TareWeight  
          
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_PackType_Insert'  
          goto error  
        end  
      end  
      else  
      begin  
        exec @Error = p_Pack_Update  
         @PackId        = @PackId,  
         @WarehouseId   = @WarehouseId,  
         @StorageUnitId = @StorageUnitId,  
         @PackTypeId    = @PackTypeId,  
         @Quantity      = @Quantity,  
         @Barcode       = @Barcode,  
         @Length        = @Length,  
         @Width         = @Width,  
         @Height        = @Height,  
         @Volume        = @Volume,  
         @Weight        = @GrossWeight,  
         @NettWeight    = @NettWeight,  
         @TareWeight    = @TareWeight  
        
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_PackType_Update'  
          goto error  
        end  
      end  
      
      fetch Pack_cursor into @PackCode,  
                             @PackDescription,  
                             @Quantity,  
                             @Barcode,  
                             @Length,  
                             @Width,  
                             @Height,  
                             @Volume,  
                             @NettWeight,  
                             @GrossWeight,  
                             @TareWeight,  
                             @ProductCategory,  
                             @PackingCategory,  
                             @PickEmpty,  
                             @StackingCategory,  
                             @MovementCategory,  
                             @ValueCategory,  
                             @StoringCategory,  
                             @PickPartPallet  
    end  
      
    close Pack_cursor  
    deallocate Pack_cursor  
      
    error:  
    if @Error = 0  
    begin  
      update InterfaceImportProduct  
         set RecordStatus = 'C'  
       where InterfaceImportProductId = @InterfaceImportProductId  
        
      commit transaction  
      
      insert InterfaceMessage
            ([InterfaceMessageCode],
             [InterfaceMessage],
             [InterfaceId],
             [InterfaceTable],
             [Status],
             [OrderNumber],
             [CreateDate],
             [ProcessedDate])
      select 'Processed',
             'Success',
             @InterfaceImportProductId,
             'InterfaceImportProduct',
             'N',
             @ProductCode,
             @GetDate,
             @GetDate
    end  
    else  
    begin  
      if @@trancount > 0  
        rollback transaction  
        
      update InterfaceImportProduct  
         set RecordStatus = 'E'
       where InterfaceImportProductId = @InterfaceImportProductId  
      
      insert InterfaceMessage
            ([InterfaceMessageCode],
             [InterfaceMessage],
             [InterfaceId],
             [InterfaceTable],
             [Status],
             [OrderNumber],
             [CreateDate],
             [ProcessedDate])
      select 'Failed',
             @ErrorMsg,
             @InterfaceImportProductId,
             'InterfaceImportProduct',
             'E',
             @ProductCode,
             @GetDate,
             @GetDate
    end
    
    fetch Product_cursor into @InterfaceImportProductId,  
                              @ProductCode,  
                              @Product,  
                              @SKUCode,  
                              @SKU,  
                              @PalletQuantity,  
                              @Barcode,  
                              @MinimumQuantity,  
                              @ReorderQuantity,  
                              @MaximumQuantity,  
                              @CuringPeriodDays,  
                              @ShelfLifeDays,  
                              @QualityAssuranceIndicator,  
                              @ProductType,  
                              @ProductCategory,  
                              @OverReceipt,  
                              @Samples,  
                              @RetentionSamples,  
                              @AssaySamples,   
                              @HostId,  
                              @ParentProductCode,
                              @Description2,
                              @Description3,
                              @PrincipalCode,
                              @UnitPrice,
                              @SizeCode,
                              @Size,
                              @ColourCode,
                              @Colour,
                              @StyleCode,
                              @Style,
                              @ProductAlias,
                              @PutawayRule,
                              @AllocationRule,
                              @BillingRule,
                              @CycleCountRule,
                              @LotAttributeRule,
                              @StorageCondition,
                              @SerialTracked,
                              @Weight,
                              @ProductGroup,
                              @ManualCost,
                              @OffSet
  end  
    
  close Product_cursor  
  deallocate Product_cursor  
  
  update Pack  
     set TareWeight = isnull(Weight,0) - isnull(NettWeight,0)  
   where TareWeight is null  
  
  update StorageUnit set LotAttributeRule = 'None' where LotAttributeRule is null
  --return
  --insert Pack
  --      (StorageUnitId,
  --       PackTypeId,
  --       Quantity,
  --       WarehouseId)
  --select su.StorageUnitId,
  --       pt.PackTypeId,
  --       1,
  --       1
  --  from StorageUnit su
  --  join PackType    pt on pt.Unit = 1
  --  left
  --  join Pack       pk2 on su.StorageUnitId = pk2.StorageUnitId
  --                     and pk2.WarehouseId = 1
  --  join PackType   pt2 on pk2.PackTypeId = pt2.PackTypeId
  --                     and pt2.Unit = 1
  -- where pk2.PackId is null
  
  --insert Pack
  --      (StorageUnitId,
  --       PackTypeId,
  --       Quantity,
  --       WarehouseId)
  --select su.StorageUnitId,
  --       pt.PackTypeId,
  --       1,
  --       1
  --  from StorageUnit su,
  --       PackType    pt
  -- where pt.Unit = 1
  --   and not exists(select 1 from Pack pk2 where su.StorageUnitId = pk2.StorageUnitId and pk2.WarehouseId = 1 and pt.PackTypeId = pk2.PackTypeId)
  
  --insert Pack
  --      (StorageUnitId,
  --       PackTypeId,
  --       Quantity,
  --       WarehouseId)
  --select su.StorageUnitId,
  --       pt.PackTypeId,
  --       99999,
  --       1
  --  from StorageUnit su,
  --       PackType    pt
  -- where pt.Pallet = 1
  --   and not exists(select 1 from Pack pk2 where su.StorageUnitId = pk2.StorageUnitId and pk2.WarehouseId = 1 and pt.PackTypeId = pk2.PackTypeId)
  
  --insert Pack
  --      (StorageUnitId,
  --       PackTypeId,
  --       Quantity,
  --       WarehouseId)
  --select su.StorageUnitId,
  --       pt.PackTypeId,
  --       1,
  --       1
  --  from StorageUnit su
  --  join PackType    pt on pt.Pallet = 1
  --  left
  --  join Pack       pk2 on su.StorageUnitId = pk2.StorageUnitId
  --                     and pk2.WarehouseId = 1
  --  join PackType   pt2 on pk2.PackTypeId = pt2.PackTypeId
  --                     and pt2.Pallet = 1
  -- where pk2.PackId is null
  
  insert Pack
        (StorageUnitId,
         PackTypeId,
         Quantity,
         Barcode,
         Length,
         Width,
         Height,
         Volume,
         Weight,
         WarehouseId,
         NettWeight,
         TareWeight,
         ProductionQuantity,
         StatusId)
  select pk.StorageUnitId,
         pk.PackTypeId,
         pk.Quantity,
         pk.Barcode,
         pk.Length,
         pk.Width,
         pk.Height,
         pk.Volume,
         pk.Weight,
         w.WarehouseId,
         pk.NettWeight,
         pk.TareWeight,
         pk.ProductionQuantity,
         pk.StatusId
    from Pack        pk,
         Warehouse    w
   where not exists(select 1 from Pack pk2 where pk.StorageUnitId = pk2.StorageUnitId and pk2.WarehouseId = w.WarehouseId and pk.PackTypeId = pk2.PackTypeId)
     and w.warehouseid = w.ParentWarehouseId
  
  insert StorageUnitArea
        (StorageUnitId,
         AreaId,
         StoreOrder,
         PickOrder)
  select distinct su.StorageUnitId,
         a.AreaId,
         1,
         1
    from Area a,
         StorageUnit su
   where not exists(select top 1 1 from StorageUnitArea sua where su.StorageUnitId = sua.StorageUnitId and a.AreaId = sua.AreaId)
     and a.AreaCode in ('BK','RK','SP')
     and a.AutoLink = 1
  
  update PackType
     set InboundSequence = 3, OutboundSequence = 3
   where Unit = 1
  
  update PackType
     set InboundSequence = 1, OutboundSequence = 1
   where Pallet = 1
  
  update PackType
     set InboundSequence = 2, OutboundSequence = 2
   where isnull(Pallet, 0) = 0
     and isnull(Unit, 0) = 0
  
  --update Pack set Quantity = 99999 where PackTypeId = 4 and Quantity = 1
  
  --insert StorageUnitLocation
  --      (StorageUnitId,
  --       LocationId)
  --select distinct su.StorageUnitId,
  --       al.LocationId
  --  from AreaProductGroup apg
  --  join Product            p on apg.ProductGroup = p.ProductGroup
  --  join StorageUnit       su on p.ProductId = su.StorageUnitId
  --  join Area               a on apg.Area = a.Area
  --  join AreaLocation      al on a.AreaId = al.AreaId
  --  left
  --  join StorageUnitLocation sul on sul.LocationId = al.LocationId
  --                              and sul.StorageUnitId = su.StorageUnitId
  --where sul.LocationId is null
  
  if DB_NAME() like '%Wizard%'
    update su
       set ColourCode = p.ParentProductCode
          ,SizeCode = p.Description2
          ,Colour = p.ParentProductCode
          ,Size = p.Description2
      from Product p 
      join StorageUnit su on p.ProductId = su.ProductId
end  
