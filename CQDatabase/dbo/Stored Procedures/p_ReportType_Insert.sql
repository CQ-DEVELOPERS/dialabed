﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportType_Insert
  ///   Filename       : p_ReportType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 15:24:48
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ReportType table.
  /// </remarks>
  /// <param>
  ///   @ReportTypeId int = null output,
  ///   @ReportType nvarchar(100) = null,
  ///   @ReportTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   ReportType.ReportTypeId,
  ///   ReportType.ReportType,
  ///   ReportType.ReportTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportType_Insert
(
 @ReportTypeId int = null output,
 @ReportType nvarchar(100) = null,
 @ReportTypeCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @ReportTypeId = '-1'
    set @ReportTypeId = null;
  
  if @ReportType = '-1'
    set @ReportType = null;
  
  if @ReportTypeCode = '-1'
    set @ReportTypeCode = null;
  
	 declare @Error int
 
  insert ReportType
        (ReportType,
         ReportTypeCode)
  select @ReportType,
         @ReportTypeCode 
  
  select @Error = @@Error, @ReportTypeId = scope_identity()
  
  
  return @Error
  
end
 
