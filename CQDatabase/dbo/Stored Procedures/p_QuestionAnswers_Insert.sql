﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QuestionAnswers_Insert
  ///   Filename       : p_QuestionAnswers_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:27
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the QuestionAnswers table.
  /// </remarks>
  /// <param>
  ///   @QuestionId int = null,
  ///   @Answer nvarchar(100) = null,
  ///   @JobId int = null,
  ///   @ReceiptId int = null,
  ///   @OrderNumber int = null,
  ///   @PalletId int = null,
  ///   @DateAsked datetime = null,
  ///   @QuestionAnswersId int = null output 
  /// </param>
  /// <returns>
  ///   QuestionAnswers.QuestionId,
  ///   QuestionAnswers.Answer,
  ///   QuestionAnswers.JobId,
  ///   QuestionAnswers.ReceiptId,
  ///   QuestionAnswers.OrderNumber,
  ///   QuestionAnswers.PalletId,
  ///   QuestionAnswers.DateAsked,
  ///   QuestionAnswers.QuestionAnswersId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QuestionAnswers_Insert
(
 @QuestionId int = null,
 @Answer nvarchar(100) = null,
 @JobId int = null,
 @ReceiptId int = null,
 @OrderNumber int = null,
 @PalletId int = null,
 @DateAsked datetime = null,
 @QuestionAnswersId int = null output 
)
 
as
begin
	 set nocount on;
  
  if @QuestionAnswersId = '-1'
    set @QuestionAnswersId = null;
  
	 declare @Error int
 
  insert QuestionAnswers
        (QuestionId,
         Answer,
         JobId,
         ReceiptId,
         OrderNumber,
         PalletId,
         DateAsked,
         QuestionAnswersId)
  select @QuestionId,
         @Answer,
         @JobId,
         @ReceiptId,
         @OrderNumber,
         @PalletId,
         @DateAsked,
         @QuestionAnswersId 
  
  select @Error = @@Error, @QuestionAnswersId = scope_identity()
  
  
  return @Error
  
end
