﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_On_Hand
  ///   Filename       : p_Housekeeping_Stock_On_Hand.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_On_Hand
(
 @WarehouseId int,
 @ProductCode nvarchar(30),
 @Product     nvarchar(255),
 @Batch       nvarchar(50)
)
 
as
begin
	 set nocount on;
	 
	 declare @MaxDate datetime,
	         @MinDate datetime
	 
	 if @ProductCode is null
	   set @ProductCode = ''
	 
	 if @Product is null
	   set @Product = ''
	 
	 if @Batch is null
	   set @Batch = ''
	 
	 select @MaxDate = max(ComparisonDate)
	   from StockOnHandCompare
	 
	 select @MinDate = max(ComparisonDate)
	   from StockOnHandCompare
	  where ComparisonDate < @MaxDate
	    and ComparisonDate > dateadd(dd, -7, getdate())
	 
	 select sohmax.ComparisonDate,
	        sohmax.StorageUnitId,
	        sohmax.BatchId,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vs.Batch,
         sohmax.Quantity as 'CurrentQuantity',
         sohmin.Quantity as 'PreviousQuantity',
         sohmax.Quantity - sohmin.Quantity as 'Variance',
         sohmax.Sent,
         sohmax.UnitPrice * sohmax.Quantity as 'CurrentValue',
         sohmax.UnitPrice * sohmin.Quantity as 'PreviousValue',
         (sohmax.UnitPrice * sohmax.Quantity) - (sohmin.UnitPrice * sohmin.Quantity) as 'PriceVariance',
         sohmax.UnitPrice
    from StockOnHandCompare sohmax
    left
    join StockOnHandCompare sohmin on sohmax.StorageUnitId = sohmin.StorageUnitId
                                  and sohmax.BatchId       = sohmin.BatchId
                                  and sohmax.ComparisonDate = @MaxDate
                                  and sohmin.ComparisonDate = @MinDate
    join viewStock           vs on sohmax.StorageUnitId    = vs.StorageUnitId
                               and sohmax.BatchId          = vs.BatchId
   where sohmax.WarehouseId    = @WarehouseId
     and vs.ProductCode     like @ProductCode + '%'
     and vs.Product         like @Product + '%'
     and vs.Batch           like @Batch + '%'
     and sohmax.Sent           = 0
end
