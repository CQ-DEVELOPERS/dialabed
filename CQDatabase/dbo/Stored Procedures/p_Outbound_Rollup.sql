﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Rollup
  ///   Filename       : p_Outbound_Rollup.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Rollup
(
 @OutboundShipmentId int,
 @IssueId            int,
 @StatusCode         nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Outbound_Rollup'
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  begin transaction
  
  -- Need all the jobs.
  -- Must be in the right status.
  -- Don't worry about the InstructionRefId becuase it will be the same job.

  declare @TableJobs as table
  (
   JobId      int,
   StatusCode nvarchar(10)
  )
  
  if @OutboundShipmentId is not null
  begin
    insert @TableJobs
          (JobId)
    select distinct i.JobId
      from IssueLineInstruction ili
      join Instruction            i on ili.InstructionId = i.InstructionId
     where ili.OutboundShipmentId = @OutboundShipmentId
  end
  else
  begin
    insert @TableJobs
          (JobId)
    select distinct i.JobId
      from IssueLineInstruction ili
      join Instruction            i on ili.InstructionId = i.InstructionId
     where ili.IssueId = @IssueId
  end
  
  update t
     set StatusCode = s.StatusCode
    from @TableJobs t
    join Job        j (nolock) on t.JobId    = j.JobId
    join Status     s (nolock) on j.StatusId = s.StatusId
  
  while exists(select top 1 1
                 from @TableJobs
                where StatusCode = @StatusCode)
  begin
    select top 1 @JobId = JobId
      from @TableJobs
     where StatusCode = @StatusCode
    
    delete @TableJobs
     where JobId = @JobId
    
    exec @Error = p_Status_Rollup
     @jobId = @JobId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
