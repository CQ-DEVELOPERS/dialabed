﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCheck_Insert
  ///   Filename       : p_ProductCheck_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:01
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ProductCheck table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @OperatorId int = null 
  /// </param>
  /// <returns>
  ///   ProductCheck.JobId,
  ///   ProductCheck.OperatorId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCheck_Insert
(
 @JobId int = null,
 @OperatorId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ProductCheck
        (JobId,
         OperatorId)
  select @JobId,
         @OperatorId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
