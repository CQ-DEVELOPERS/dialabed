﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLog_Search
  ///   Filename       : p_InterfaceLog_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceLog table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceLog.InterfaceLogId,
  ///   InterfaceLog.Data,
  ///   InterfaceLog.RecordStatus,
  ///   InterfaceLog.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLog_Search
(
 @InterfaceLogId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceLogId = '-1'
    set @InterfaceLogId = null;
  
 
  select
         InterfaceLog.InterfaceLogId
        ,InterfaceLog.Data
        ,InterfaceLog.RecordStatus
        ,InterfaceLog.ProcessedDate
    from InterfaceLog
   where isnull(InterfaceLog.InterfaceLogId,'0')  = isnull(@InterfaceLogId, isnull(InterfaceLog.InterfaceLogId,'0'))
  
end
