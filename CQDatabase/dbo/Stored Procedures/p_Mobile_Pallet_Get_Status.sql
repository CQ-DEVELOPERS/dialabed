﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Pallet_Get_Status
  ///   Filename       : p_Mobile_Pallet_Get_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Pallet_Get_Status
(
 @barcode nvarchar(50)
)
 
as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
         ,@PalletId int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_Mobile_Pallet_Get_Status;
    
    if @barcode like 'P:%'
    begin
      if isnumeric(replace(@barcode,'P:','')) = 1
      begin
        select @PalletId = replace(@barcode,'P:',''),
               @barcode  = null
        
		      -- Do the actual work here
        if (select top 1 s.StatusCode
              from Pallet p (nolock)
              join StorageUnitBatch sub (nolock) on p.StorageUnitBatchId = sub.StorageUnitBatchId
              join Batch b (nolock) on sub.BatchId = b.BatchId
              join Status s (nolock) on b.StatusId = s.StatusId
             where p.PalletId = @PalletId) not in ('A','QCA')
        begin
          raiserror 900000 'Invlid batch status'
	       end
      end
    end
    
    raiserror 900000 'Invliad pallet scanned'

    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Mobile_Pallet_Get_Status;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
