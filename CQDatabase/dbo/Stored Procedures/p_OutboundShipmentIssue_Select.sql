﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssue_Select
  ///   Filename       : p_OutboundShipmentIssue_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jun 2013 12:36:36
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null 
  /// </param>
  /// <returns>
  ///   OutboundShipmentIssue.OutboundShipmentId,
  ///   OutboundShipmentIssue.IssueId,
  ///   OutboundShipmentIssue.DropSequence 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssue_Select
(
 @OutboundShipmentId int = null,
 @IssueId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         OutboundShipmentIssue.OutboundShipmentId
        ,OutboundShipmentIssue.IssueId
        ,OutboundShipmentIssue.DropSequence
    from OutboundShipmentIssue
   where isnull(OutboundShipmentIssue.OutboundShipmentId,'0')  = isnull(@OutboundShipmentId, isnull(OutboundShipmentIssue.OutboundShipmentId,'0'))
     and isnull(OutboundShipmentIssue.IssueId,'0')  = isnull(@IssueId, isnull(OutboundShipmentIssue.IssueId,'0'))
  
end
