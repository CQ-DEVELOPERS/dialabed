﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Search
  ///   Filename       : p_SerialNumber_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2013 14:12:41
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the SerialNumber table.
  /// </remarks>
  /// <param>
  ///   @SerialNumberId int = null output,
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null,
  ///   @ReceiptId int = null,
  ///   @SerialNumber varchar(50) = null,
  ///   @BatchId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @ReceiptLineId int = null,
  ///   @StoreJobId int = null,
  ///   @PickJobId int = null,
  ///   @StatusId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   SerialNumber.SerialNumberId,
  ///   SerialNumber.StorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   SerialNumber.LocationId,
  ///   Location.Location,
  ///   SerialNumber.ReceiptId,
  ///   Receipt.ReceiptId,
  ///   SerialNumber.StoreInstructionId,
  ///   SerialNumber.PickInstructionId,
  ///   SerialNumber.SerialNumber,
  ///   SerialNumber.ReceivedDate,
  ///   SerialNumber.DespatchDate,
  ///   SerialNumber.ReferenceNumber,
  ///   SerialNumber.BatchId,
  ///   Batch.Batch,
  ///   SerialNumber.IssueId,
  ///   Issue.IssueId,
  ///   SerialNumber.IssueLineId,
  ///   IssueLine.IssueLineId,
  ///   SerialNumber.ReceiptLineId,
  ///   ReceiptLine.ReceiptLineId,
  ///   SerialNumber.StoreJobId,
  ///   Job.JobId,
  ///   SerialNumber.PickJobId,
  ///   Job.JobId,
  ///   SerialNumber.Remarks,
  ///   SerialNumber.StatusId 
  ///   Status.Status 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Search
(
 @SerialNumberId int = null output,
 @StorageUnitId int = null,
 @LocationId int = null,
 @ReceiptId int = null,
 @SerialNumber varchar(50) = null,
 @BatchId int = null,
 @IssueId int = null,
 @IssueLineId int = null,
 @ReceiptLineId int = null,
 @StoreJobId int = null,
 @PickJobId int = null,
 @StatusId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @SerialNumberId = '-1'
    set @SerialNumberId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @SerialNumber = '-1'
    set @SerialNumber = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @StoreJobId = '-1'
    set @StoreJobId = null;
  
  if @PickJobId = '-1'
    set @PickJobId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
 
  select
         SerialNumber.SerialNumberId
        ,SerialNumber.StorageUnitId
        ,SerialNumber.LocationId
         ,LocationLocationId.Location as 'Location'
        ,SerialNumber.ReceiptId
        ,SerialNumber.StoreInstructionId
        ,SerialNumber.PickInstructionId
        ,SerialNumber.SerialNumber
        ,SerialNumber.ReceivedDate
        ,SerialNumber.DespatchDate
        ,SerialNumber.ReferenceNumber
        ,SerialNumber.BatchId
         ,BatchBatchId.Batch as 'Batch'
        ,SerialNumber.IssueId
        ,SerialNumber.IssueLineId
        ,SerialNumber.ReceiptLineId
        ,SerialNumber.StoreJobId
        ,SerialNumber.PickJobId
        ,SerialNumber.Remarks
        ,SerialNumber.StatusId
         ,StatusStatusId.Status as 'Status'
    from SerialNumber
    left
    join StorageUnit StorageUnitStorageUnitId on StorageUnitStorageUnitId.StorageUnitId = SerialNumber.StorageUnitId
    left
    join Location LocationLocationId on LocationLocationId.LocationId = SerialNumber.LocationId
    left
    join Receipt ReceiptReceiptId on ReceiptReceiptId.ReceiptId = SerialNumber.ReceiptId
    left
    join Batch BatchBatchId on BatchBatchId.BatchId = SerialNumber.BatchId
    left
    join Issue IssueIssueId on IssueIssueId.IssueId = SerialNumber.IssueId
    left
    join IssueLine IssueLineIssueLineId on IssueLineIssueLineId.IssueLineId = SerialNumber.IssueLineId
    left
    join ReceiptLine ReceiptLineReceiptLineId on ReceiptLineReceiptLineId.ReceiptLineId = SerialNumber.ReceiptLineId
    left
    join Job JobStoreJobId on JobStoreJobId.JobId = SerialNumber.StoreJobId
    left
    join Job JobPickJobId on JobPickJobId.JobId = SerialNumber.PickJobId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = SerialNumber.StatusId
   where isnull(SerialNumber.SerialNumberId,'0')  = isnull(@SerialNumberId, isnull(SerialNumber.SerialNumberId,'0'))
     and isnull(SerialNumber.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(SerialNumber.StorageUnitId,'0'))
     and isnull(SerialNumber.LocationId,'0')  = isnull(@LocationId, isnull(SerialNumber.LocationId,'0'))
     and isnull(SerialNumber.ReceiptId,'0')  = isnull(@ReceiptId, isnull(SerialNumber.ReceiptId,'0'))
     and isnull(SerialNumber.SerialNumber,'%')  like '%' + isnull(@SerialNumber, isnull(SerialNumber.SerialNumber,'%')) + '%'
     and isnull(SerialNumber.BatchId,'0')  = isnull(@BatchId, isnull(SerialNumber.BatchId,'0'))
     and isnull(SerialNumber.IssueId,'0')  = isnull(@IssueId, isnull(SerialNumber.IssueId,'0'))
     and isnull(SerialNumber.IssueLineId,'0')  = isnull(@IssueLineId, isnull(SerialNumber.IssueLineId,'0'))
     and isnull(SerialNumber.ReceiptLineId,'0')  = isnull(@ReceiptLineId, isnull(SerialNumber.ReceiptLineId,'0'))
     and isnull(SerialNumber.StoreJobId,'0')  = isnull(@StoreJobId, isnull(SerialNumber.StoreJobId,'0'))
     and isnull(SerialNumber.PickJobId,'0')  = isnull(@PickJobId, isnull(SerialNumber.PickJobId,'0'))
     and isnull(SerialNumber.StatusId,'0')  = isnull(@StatusId, isnull(SerialNumber.StatusId,'0'))
  order by SerialNumber
  
end
