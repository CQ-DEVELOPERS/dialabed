﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperatorGroupHistory_Insert
  ///   Filename       : p_AreaOperatorGroupHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:42
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaOperatorGroupHistory table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @OperatorGroupId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   AreaOperatorGroupHistory.AreaId,
  ///   AreaOperatorGroupHistory.OperatorGroupId,
  ///   AreaOperatorGroupHistory.CommandType,
  ///   AreaOperatorGroupHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperatorGroupHistory_Insert
(
 @AreaId int = null,
 @OperatorGroupId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert AreaOperatorGroupHistory
        (AreaId,
         OperatorGroupId,
         CommandType,
         InsertDate)
  select @AreaId,
         @OperatorGroupId,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
