﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Inbound_Exceptions
  ///   Filename       : p_Report_Inbound_Exceptions.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Inbound_Exceptions
(
 @WarehouseId int,
 @Production  bit = null
)
 
as
begin
	 set nocount on;
  
  --select 'Within 4 Hours' as 'Type',
  --       count(distinct(j.JobId)) as 'Pallets',
  --       sum(ConfirmedQuantity) as 'Litres'
  --  from Instruction      i (nolock)
  --  join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
  --  join Job                j (nolock) on i.JobId              = j.JobId
  --  join Status             s (nolock) on j.StatusId           = s.StatusId
  -- where i.WarehouseId = @WarehouseId
  --   and it.InstructionTypeCode in ('PR','S','SM')
  --   and s.StatusCode in ('A','S')
  --   and isnull(j.CheckedDate, i.StartDate) > dateadd(hh, -4, getdate())
  --union
  select 'Exceeding 4 Hours' as 'Type',
         count(distinct(j.JobId)) as 'Pallets',
         sum(ConfirmedQuantity) as 'Litres'
    from Instruction      i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on j.StatusId           = s.StatusId
   where i.WarehouseId = @WarehouseId
     and it.InstructionTypeCode in ('PR','S','SM')
     and s.StatusCode in ('A','S')
     and isnull(j.CheckedDate, i.StartDate) between dateadd(hh, -4, getdate()) and dateadd(hh, -8, getdate())
  union
  select 'Exceeding 8 Hours',
         count(distinct(j.JobId)) as 'Pallets',
         sum(ConfirmedQuantity) as 'Litres'
    from Instruction      i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on j.StatusId           = s.StatusId
   where i.WarehouseId = @WarehouseId
     and it.InstructionTypeCode in ('PR','S','SM')
     and s.StatusCode in ('A','S')
     and isnull(j.CheckedDate, i.StartDate) < dateadd(hh, -8, getdate())
end
