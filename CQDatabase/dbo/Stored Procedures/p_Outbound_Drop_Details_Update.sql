﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Drop_Details_Update
  ///   Filename       : p_Outbound_Drop_Details_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Drop_Details_Update
(
 @IssueId      int,
 @DropSequence int
)
 
as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @OutboundShipmentId int,
          @ExternalCompanyId  int
  
  declare @TableResult as table
  (
   IssueId int
  )
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @DropSequence = 0
    return
  
  begin transaction
  
  select @OutboundShipmentId = OutboundShipmentId
    from OutboundShipmentIssue
   where IssueId = @IssueId
  
  select @ExternalCompanyId = ExternalCompanyId
    from Issue             i (nolock)
    join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
   where i.IssueId = @IssueId
  
  insert @TableResult
        (IssueId)
  select i.IssueId
    from OutboundShipmentIssue osi (nolock)
    join Issue                   i (nolock) on osi.IssueId = i.IssueId
    join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
   where od.ExternalCompanyId = @ExternalCompanyId
     and osi.OutboundShipmentId = @OutboundShipmentId
  
  set @IssueId = 0
  
  while exists(select top 1 1 from @TableResult where IssueId > @IssueId)
  begin
    select @IssueId = IssueId
      from @TableResult tr
     where IssueId > @IssueId
    order by IssueId desc
    
    exec @Error = p_Issue_Update
     @IssueId      = @IssueId,
     @DropSequence = @DropSequence
    
    if @Error <> 0
      goto error
    
    exec @Error = p_OutboundShipmentIssue_Update
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @DropSequence       = @DropSequence
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Drop_Details_Update'
    rollback transaction
    return @Error
end

