﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_StorageUnitBatch_Search
  ///   Filename       : p_Static_StorageUnitBatch_Search.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 19 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@ProductId> 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_StorageUnitBatch_Search
(
 @StorageUnitId       int
)
 
as
begin
	 set nocount on;

SELECT     sub.StorageUnitBatchId, sub.StorageUnitId, b.BatchId, b.Batch
FROM         StorageUnitBatch AS sub INNER JOIN
                      Batch AS b ON sub.BatchId = b.BatchId
                      
                      Where sub.StorageUnitId = @StorageUnitId
 
end
