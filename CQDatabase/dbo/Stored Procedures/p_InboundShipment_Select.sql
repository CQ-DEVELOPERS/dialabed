﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Select
  ///   Filename       : p_InboundShipment_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:21
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundShipment table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null 
  /// </param>
  /// <returns>
  ///   InboundShipment.InboundShipmentId,
  ///   InboundShipment.StatusId,
  ///   InboundShipment.WarehouseId,
  ///   InboundShipment.ShipmentDate,
  ///   InboundShipment.Remarks 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Select
(
 @InboundShipmentId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InboundShipment.InboundShipmentId
        ,InboundShipment.StatusId
        ,InboundShipment.WarehouseId
        ,InboundShipment.ShipmentDate
        ,InboundShipment.Remarks
    from InboundShipment
   where isnull(InboundShipment.InboundShipmentId,'0')  = isnull(@InboundShipmentId, isnull(InboundShipment.InboundShipmentId,'0'))
  
end
