﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
/*
  /// <summary>
  ///   Procedure Name : p_Checking_Rollup_Retry
  ///   Filename       : p_Checking_Rollup_Retry.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Oct 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Checking_Rollup_Retry
 
as
begin
	 set nocount on;
  
  select distinct 'exec p_Status_rollup ' + convert(varchar(10), j.JobId) as 'String'
    from CheckingLog cl (nolock)
    join Job          j (nolock) on cl.JobId = j.JobId
    join Status       s (nolock) on j.StatusId = s.StatusId
   where cl.StartDate > '2011-10-20 00:00:00.000'
     and cl.StartDate < dateadd(mi, -5, getdate())
     and s.StatusCode = 'CK'
     and cl.OperatorId is not null
     and cl.EndDate is null
  
  declare @String varchar(500)

  while exists(select 1 from #temp)
  begin
    select top 1 @String = String
      from #temp
    
    execute (@String)
    select @String
    delete #temp where String = @String
  end
end

