﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COADetail_Delete
  ///   Filename       : p_COADetail_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:22
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the COADetail table.
  /// </remarks>
  /// <param>
  ///   @COADetailId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COADetail_Delete
(
 @COADetailId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete COADetail
     where COADetailId = @COADetailId
  
  select @Error = @@Error
  
  
  return @Error
  
end
