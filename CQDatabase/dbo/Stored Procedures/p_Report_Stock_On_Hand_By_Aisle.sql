﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_On_Hand_By_Aisle
  ///   Filename       : p_Report_Stock_On_Hand_By_Aisle.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Mar 2008
  /// </summary>
  /// <remarks>
  ///  

 
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_On_Hand_By_Aisle
(
 @WarehouseId       int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table 
  (
   LocationId                      int        ,
   RelativeValue                   numeric(13,3),
   Aisle                           nvarchar(50),
   [Column]                        nvarchar(10),
   [Level]                         nvarchar(10),
   StorageUnitId                   int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(255),
   SKUCode                         nvarchar(50),
   Batch                           nvarchar(50),
   Area                            nvarchar(50),
   AreaCode                        nvarchar(10),
   Location                        nvarchar(15),
   ActualQuantity                  float        ,
   AllocatedQuantity               float        ,
   ReservedQuantity                float,
   NettWeight					   numeric(13,6)        
  )
  
  insert @TableResult
        (LocationId,
         RelativeValue,
         Aisle,
         [Level],
         [Column],
         StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Area,
         AreaCode,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity)
  select l.LocationId,
         l.RelativeValue,
         l.Ailse,
         l.[Level],
         l.[Column],
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         a.Area,    		 a.AreaCode,
         l.Location,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
   where a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     and a.AreaCode in ('PK','RK','BK','SP')
  
  insert @TableResult
        (LocationId,
         RelativeValue,
         Aisle,
         [Level],
         [Column],
         Location,
         Area,
         AreaCode,
         StorageUnitId,
         ProductCode,
         Product,
         SKUCode)
  select l.LocationId,
         l.RelativeValue,
         l.Ailse,
         l.[Level],
         l.[Column],
         l.Location,
         a.Area,
         a.AreaCode,
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode
    

from StorageUnitLocation sul (nolock)
    join StorageUnit          su (nolock) on sul.StorageUnitId       = su.StorageUnitId
    join Product               p (nolock) on su.ProductId            = p.ProductId
    join SKU                 sku (nolock) on su.SKUId                = sku.SKUId
    join Location              l (nolock) on sul.LocationId = l.LocationId
    join AreaLocation         al (nolock) on l.LocationId = al.LocationId
    join Area                  a (nolock) on al.AreaId    = a.AreaId
    left 
    join @TableResult         tr on tr.LocationId    = sul.LocationId
                                and tr.StorageUnitId = sul.StorageUnitId
   where tr.LocationId is null
     and a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     and a.AreaCode in ('PK','RK','BK','SP')
  
  insert @TableResult
        (LocationId,
         RelativeValue,
         Aisle,
         [Level],
         [Column],
         Location,
         Area,
         AreaCode)
  select l.LocationId,
         l.RelativeValue,
         l.Ailse,
         l.[Level],
         l.[Column],
         l.Location,
         a.Area,
         a.AreaCode
    from Location      l
    join AreaLocation al on l.LocationId = al.LocationId
    join Area          a on al.AreaId    = a.AreaId
  

 where not exists(select top 1 1 from @TableResult tr where tr.LocationId = l.LocationId)
     and a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     and a.AreaCode in ('PK','RK','BK','SP')
  
  update @TableResult
     set [Column] = Replicate('0', 10 - datalength([Column])) + [Column] from @TableResult
   where [Column] is not null
   
     declare @InboundSequence smallint,
		  @PackTypeId	int,
	      @PackType   	nvarchar(30),
		  @ShowWeight	bit = null
		  
  select @ShowWeight = Indicator


  from Configuration where ConfigurationId = 206
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence

  if  @ShowWeight = 1
  

    update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   where pk.PackTypeId = @PackTypeId
                       and p.ProductCode = tr.ProductCode
       

                and isnull(su.ProductCategory,'') != 'V')* tr.ActualQuantity)
    
    from @TableResult tr
  
  select ProductCode,
         Product,
         SKUCode,
         Batch,
         Area,
         Location,
         isnull(Aisle,Area) as 'Aisle',
         case when AreaCode = 'BK'
              then ''
              else isnull([Level],'')
              end as 'Level',
         [Column],
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         NettWeight
    from @TableResult
  order by Aisle,
           [Level],
           [Column],
           Location,
           Product,
           SKUCode,
           NettWeight
end

 
 
