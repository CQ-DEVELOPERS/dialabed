﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Exception_Stock_Take
  ///   Filename       : p_Dashboard_Exception_Stock_Take.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Exception_Stock_Take
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select [Type],
	          [Area],
	          [Count]
	     from DashboardExceptionStockTake
	    where WarehouseId = @WarehouseId
	 end
	 else
	 begin
    truncate table DashboardExceptionStockTake
  	 
    insert DashboardExceptionStockTake
          (WarehouseId,
           Type,
           AreaId,
           Area,
           Count)
    select a.WarehouseId,
           'Counts Required',
           a.AreaId,
           a.Area,
           count(distinct(i.PickLocationId))
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Status           s (nolock) on i.StatusId          = s.StatusId
      join AreaLocation    al (nolock) on i.PickLocationId    = al.LocationId 
      join Area             a (nolock) on al.AreaId           = a.AreaId
     where i.WarehouseId           = isnull(@WarehouseId, i.WarehouseId)
       and it.InstructionTypeCode in ('STE','STL','STA','STP')
       and s.StatusCode           in ('W','S')
    group by a.WarehouseId,
             a.AreaId,
             a.Area
  	 
    insert DashboardExceptionStockTake
          (WarehouseId,
           [Type],
           [AreaId],
           [Area],
           [Count])
    select a.WarehouseId,
           'KPI',
           a.AreaId,
           a.Area,
           isnull(a.STEKPI,0)
      from Area          a (nolock)
      join DashboardExceptionStockTake tr on a.AreaId = tr.AreaId
	 end
end
