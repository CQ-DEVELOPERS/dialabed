﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shifts_List
  ///   Filename       : p_Shifts_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:48
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Shifts table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Shifts.ShiftId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shifts_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ShiftId
        ,null as 'Shifts'
  union
  select
         Shifts.ShiftId
        ,Shifts.ShiftId as 'Shifts'
    from Shifts
  
end
