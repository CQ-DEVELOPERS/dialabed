﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceSupplier_Insert
  ///   Filename       : p_InterfaceSupplier_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:45
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceSupplier table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @SupplierCode nvarchar(60) = null,
  ///   @SupplierName nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null,
  ///   @Address2 nvarchar(510) = null,
  ///   @Address3 nvarchar(510) = null,
  ///   @Address4 nvarchar(510) = null,
  ///   @Modified nvarchar(20) = null,
  ///   @ContactPerson nvarchar(200) = null,
  ///   @Phone nvarchar(510) = null,
  ///   @Fax nvarchar(510) = null,
  ///   @Email nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceSupplier.HostId,
  ///   InterfaceSupplier.SupplierCode,
  ///   InterfaceSupplier.SupplierName,
  ///   InterfaceSupplier.Address1,
  ///   InterfaceSupplier.Address2,
  ///   InterfaceSupplier.Address3,
  ///   InterfaceSupplier.Address4,
  ///   InterfaceSupplier.Modified,
  ///   InterfaceSupplier.ContactPerson,
  ///   InterfaceSupplier.Phone,
  ///   InterfaceSupplier.Fax,
  ///   InterfaceSupplier.Email,
  ///   InterfaceSupplier.ProcessedDate,
  ///   InterfaceSupplier.RecordStatus,
  ///   InterfaceSupplier.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceSupplier_Insert
(
 @HostId nvarchar(60) = null,
 @SupplierCode nvarchar(60) = null,
 @SupplierName nvarchar(510) = null,
 @Address1 nvarchar(510) = null,
 @Address2 nvarchar(510) = null,
 @Address3 nvarchar(510) = null,
 @Address4 nvarchar(510) = null,
 @Modified nvarchar(20) = null,
 @ContactPerson nvarchar(200) = null,
 @Phone nvarchar(510) = null,
 @Fax nvarchar(510) = null,
 @Email nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceSupplier
        (HostId,
         SupplierCode,
         SupplierName,
         Address1,
         Address2,
         Address3,
         Address4,
         Modified,
         ContactPerson,
         Phone,
         Fax,
         Email,
         ProcessedDate,
         RecordStatus,
         InsertDate)
  select @HostId,
         @SupplierCode,
         @SupplierName,
         @Address1,
         @Address2,
         @Address3,
         @Address4,
         @Modified,
         @ContactPerson,
         @Phone,
         @Fax,
         @Email,
         @ProcessedDate,
         @RecordStatus,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
