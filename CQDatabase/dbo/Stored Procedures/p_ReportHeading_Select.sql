﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportHeading_Select
  ///   Filename       : p_ReportHeading_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:26
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ReportHeading table.
  /// </remarks>
  /// <param>
  ///   @ReportHeadingId int = null 
  /// </param>
  /// <returns>
  ///   ReportHeading.ReportHeadingId,
  ///   ReportHeading.CultureId,
  ///   ReportHeading.ReportHeadingCode,
  ///   ReportHeading.ReportHeading 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportHeading_Select
(
 @ReportHeadingId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ReportHeading.ReportHeadingId
        ,ReportHeading.CultureId
        ,ReportHeading.ReportHeadingCode
        ,ReportHeading.ReportHeading
    from ReportHeading
   where isnull(ReportHeading.ReportHeadingId,'0')  = isnull(@ReportHeadingId, isnull(ReportHeading.ReportHeadingId,'0'))
  
end
