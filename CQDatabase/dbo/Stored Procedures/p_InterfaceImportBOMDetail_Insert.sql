﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportBOMDetail_Insert
  ///   Filename       : p_InterfaceImportBOMDetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:21
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportBOMDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportBOMId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @CompIndex int = null,
  ///   @CompProductCode nvarchar(60) = null,
  ///   @CompProduct nvarchar(510) = null,
  ///   @CompQuantity float = null,
  ///   @HostId nvarchar(60) = null,
  ///   @InsertDate datetime = null,
  ///   @Additional1 varchar(255) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportBOMDetail.InterfaceImportBOMId,
  ///   InterfaceImportBOMDetail.ForeignKey,
  ///   InterfaceImportBOMDetail.CompIndex,
  ///   InterfaceImportBOMDetail.CompProductCode,
  ///   InterfaceImportBOMDetail.CompProduct,
  ///   InterfaceImportBOMDetail.CompQuantity,
  ///   InterfaceImportBOMDetail.HostId,
  ///   InterfaceImportBOMDetail.InsertDate,
  ///   InterfaceImportBOMDetail.Additional1,
  ///   InterfaceImportBOMDetail.Additional2,
  ///   InterfaceImportBOMDetail.Additional3,
  ///   InterfaceImportBOMDetail.Additional4,
  ///   InterfaceImportBOMDetail.Additional5,
  ///   InterfaceImportBOMDetail.Additional6,
  ///   InterfaceImportBOMDetail.Additional7,
  ///   InterfaceImportBOMDetail.Additional8,
  ///   InterfaceImportBOMDetail.Additional9 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportBOMDetail_Insert
(
 @InterfaceImportBOMId int = null,
 @ForeignKey nvarchar(60) = null,
 @CompIndex int = null,
 @CompProductCode nvarchar(60) = null,
 @CompProduct nvarchar(510) = null,
 @CompQuantity float = null,
 @HostId nvarchar(60) = null,
 @InsertDate datetime = null,
 @Additional1 varchar(255) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportBOMDetail
        (InterfaceImportBOMId,
         ForeignKey,
         CompIndex,
         CompProductCode,
         CompProduct,
         CompQuantity,
         HostId,
         InsertDate,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9)
  select @InterfaceImportBOMId,
         @ForeignKey,
         @CompIndex,
         @CompProductCode,
         @CompProduct,
         @CompQuantity,
         @HostId,
         isnull(@InsertDate, getdate()),
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9 
  
  select @Error = @@Error
  
  
  return @Error
  
end
