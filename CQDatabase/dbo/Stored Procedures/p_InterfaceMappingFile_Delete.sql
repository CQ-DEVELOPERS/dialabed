﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingFile_Delete
  ///   Filename       : p_InterfaceMappingFile_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 09:03:34
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceMappingFile table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingFileId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingFile_Delete
(
 @InterfaceMappingFileId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceMappingFile
     where InterfaceMappingFileId = @InterfaceMappingFileId
  
  select @Error = @@Error
  
  
  return @Error
  
end
