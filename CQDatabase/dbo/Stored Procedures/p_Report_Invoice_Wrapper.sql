﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Invoice_Wrapper
  ///   Filename       : p_Report_Invoice_Wrapper.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Apr 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Invoice_Wrapper
 
as
begin
	 set nocount on;
  
  select isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',
         isnull(IssueId,-1) as 'IssueId'
    from InterfaceImportIPHeader
end
