﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Stock_Take_Check
  ///   Filename       : p_Mobile_Stock_Take_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Stock_Take_Check
(
 @instructionId int
)
 
as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         InstructionId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @PreviousQuantity  float,
          @ConfirmedQuantity float,
          @ActualQuantity    float,
          @Picked            bit,
          @bool              bit,
          @WarehouseId       int,
          @StockTake         bit = 0,
          @StocktakeInd      bit = 0
  
  select @GetDate = dbo.ufn_Getdate(),
         @Errormsg = 'Error executing ' + OBJECT_NAME(@@PROCID)
  
  select @WarehouseId       = i.WarehouseId,
         @Picked            = i.Picked,
         @PreviousQuantity  = isnull(i.PreviousQuantity, i.Quantity),
         @ConfirmedQuantity = i.ConfirmedQuantity,
                              --case when i.Picked  = 1
                              --     then i.ConfirmedQuantity - subl.ActualQuantity
                              --     else subl.ActualQuantity
                              --     end,
         @ActualQuantity    = isnull(i.SOH, isnull(subl.ActualQuantity, 0)),
         @WarehouseId       = i.WarehouseId,
         @StockTake         = a.StockTake,
         @StockTakeInd      = l.StocktakeInd
    from Instruction                 i (nolock)
    join Location                    l (nolock) on i.PickLocationId = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId = a.AreaId
    left
    join StorageUnitBatchLocation subl (nolock) on i.PickLocationId = subl.LocationId
                                               and i.StorageUnitBatchId = subl.StorageUnitBatchId
   where i.InstructionId = @instructionId
  
  set @Errormsg = '@StockTake = ' + CONVERT(nvarchar(10), @StockTake) + ' Config451 ' + CONVERT(nvarchar(10), dbo.ufn_Configuration(451, @warehouseId)) + 'CQty ' + CONVERT(nvarchar(10), @ConfirmedQuantity)
  
  if (dbo.ufn_Configuration(451, @warehouseId)) = 1 and @StockTake = 1
  begin
    if @ConfirmedQuantity != @PreviousQuantity
    if @ConfirmedQuantity = 0 or (dbo.ufn_Configuration(474, @warehouseId) = 1 and @StocktakeInd = 0)
    begin
      if (dbo.ufn_Configuration(139, @warehouseId) = 0)
      begin
        exec @Error = p_Mobile_Location_Check
         @InstructionId = @InstructionId,
         @confirmedQuantity = @ConfirmedQuantity,
         @SelectValue       = 0
        
        --if @Error != 0
        --  goto Error
      end
      else
      begin
        exec @Error = p_Mobile_Location_Error
         @InstructionId   = @InstructionId,
         @PickError       = 1,
         @StoreError      = 0,
         @CreateStockTake = 1,
         @ingorePick      = 0
        
        if @Error != 0
          goto Error
      end
    end
     
    goto successful
  end
  
  if (dbo.ufn_Configuration(301, @warehouseId)) = 0
    goto successful
  
  if @PreviousQuantity = 0 and @StockTake = 1
     goto Error
  
  --if @Picked = 1
  --  set @ActualQuantity = @ActualQuantity + @ConfirmedQuantity
  
  --select @Picked as '@Picked',
  --       @StockTake as '@StockTake',
  --       @ConfirmedQuantity as '@ConfirmedQuantity',
  --       @ActualQuantity as '@ActualQuantity',
  --       @PreviousQuantity as '@PreviousQuantity',
  --       @ConfirmedQuantity as '@ConfirmedQuantity'
  
  if @StockTake = 1
  begin
    if @ConfirmedQuantity > 0 and dbo.ufn_Configuration(399, @WarehouseId) = 1 -- Mixed pick - Keep inserting remainder unless picked 0
    begin
      goto successful
    end
    else
    if @ConfirmedQuantity >= @ActualQuantity or @PreviousQuantity > @ConfirmedQuantity
      goto Error
  end
  else
  begin
    if @ConfirmedQuantity > @ActualQuantity or @PreviousQuantity > @ConfirmedQuantity
      goto Error
  end
  
  successful:
    update MobileLog
       set EndDate = getdate(),
           InstructionId = @InstructionId,
           ErrorMsg = isnull(@Errormsg, '') + ' Successful'
     where MobileLogId = @MobileLogId
  
  set @bool = 1
  select @bool as '@bool'
  return @bool
  
  error:
    update MobileLog
       set EndDate = getdate(),
           InstructionId = @InstructionId,
           ErrorMsg = isnull(@Errormsg, '') + ' Error'
     where MobileLogId = @MobileLogId
    
    set @bool = 0
    select @bool as '@bool'
    return @Error
end

 
