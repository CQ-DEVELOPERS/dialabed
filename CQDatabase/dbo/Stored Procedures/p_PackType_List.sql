﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackType_List
  ///   Filename       : p_PackType_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:40:55
  /// </summary>
  /// <remarks>
  ///   Selects rows from the PackType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   PackType.PackTypeId,
  ///   PackType.PackType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackType_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as PackTypeId
        ,'{All}' as PackType
  union
  select
         PackType.PackTypeId
        ,PackType.PackType
    from PackType
  order by PackType
  
end
