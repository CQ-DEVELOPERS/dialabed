﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Delete
  ///   Filename       : p_Status_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:55
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Status table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Delete
(
 @StatusId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Status
     where StatusId = @StatusId
  
  select @Error = @@Error
  
  
  return @Error
  
end
