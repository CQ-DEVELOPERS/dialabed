﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Issue_Select
  ///   Filename       : p_Issue_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2015 09:30:36
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Issue table.
  /// </remarks>
  /// <param>
  ///   @IssueId int = null 
  /// </param>
  /// <returns>
  ///   Issue.IssueId,
  ///   Issue.OutboundDocumentId,
  ///   Issue.PriorityId,
  ///   Issue.WarehouseId,
  ///   Issue.LocationId,
  ///   Issue.StatusId,
  ///   Issue.OperatorId,
  ///   Issue.RouteId,
  ///   Issue.DeliveryNoteNumber,
  ///   Issue.DeliveryDate,
  ///   Issue.SealNumber,
  ///   Issue.VehicleRegistration,
  ///   Issue.Remarks,
  ///   Issue.DropSequence,
  ///   Issue.LoadIndicator,
  ///   Issue.DespatchBay,
  ///   Issue.RoutingSystem,
  ///   Issue.Delivery,
  ///   Issue.NumberOfLines,
  ///   Issue.Total,
  ///   Issue.Complete,
  ///   Issue.AreaType,
  ///   Issue.Units,
  ///   Issue.ShortPicks,
  ///   Issue.Releases,
  ///   Issue.Weight,
  ///   Issue.ConfirmedWeight,
  ///   Issue.FirstLoaded,
  ///   Issue.Interfaced,
  ///   Issue.Loaded,
  ///   Issue.Pallets,
  ///   Issue.Volume,
  ///   Issue.ContactListId,
  ///   Issue.ManufacturingId,
  ///   Issue.ReservedId,
  ///   Issue.ParentIssueId,
  ///   Issue.ASN,
  ///   Issue.WaveId,
  ///   Issue.ReserveBatch,
  ///   Issue.InvoiceNumber,
  ///   Issue.TotalDue,
  ///   Issue.AddressLine1,
  ///   Issue.AddressLine2,
  ///   Issue.AddressLine3,
  ///   Issue.AddressLine4,
  ///   Issue.AddressLine5,
  ///   Issue.PlanningComplete,
  ///   Issue.PlannedBy,
  ///   Issue.Release,
  ///   Issue.ReleasedBy,
  ///   Issue.Checking,
  ///   Issue.Checked,
  ///   Issue.Despatch,
  ///   Issue.DespatchChecked,
  ///   Issue.CompleteDate,
  ///   Issue.Availability,
  ///   Issue.CheckingCount,
  ///   Issue.Branding,
  ///   Issue.Downsizing,
  ///   Issue.CheckSheetPrinted,
  ///   Issue.CreateDate,
  ///   Issue.VehicleId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Issue_Select
(
 @IssueId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Issue.IssueId
        ,Issue.OutboundDocumentId
        ,Issue.PriorityId
        ,Issue.WarehouseId
        ,Issue.LocationId
        ,Issue.StatusId
        ,Issue.OperatorId
        ,Issue.RouteId
        ,Issue.DeliveryNoteNumber
        ,Issue.DeliveryDate
        ,Issue.SealNumber
        ,Issue.VehicleRegistration
        ,Issue.Remarks
        ,Issue.DropSequence
        ,Issue.LoadIndicator
        ,Issue.DespatchBay
        ,Issue.RoutingSystem
        ,Issue.Delivery
        ,Issue.NumberOfLines
        ,Issue.Total
        ,Issue.Complete
        ,Issue.AreaType
        ,Issue.Units
        ,Issue.ShortPicks
        ,Issue.Releases
        ,Issue.Weight
        ,Issue.ConfirmedWeight
        ,Issue.FirstLoaded
        ,Issue.Interfaced
        ,Issue.Loaded
        ,Issue.Pallets
        ,Issue.Volume
        ,Issue.ContactListId
        ,Issue.ManufacturingId
        ,Issue.ReservedId
        ,Issue.ParentIssueId
        ,Issue.ASN
        ,Issue.WaveId
        ,Issue.ReserveBatch
        ,Issue.InvoiceNumber
        ,Issue.TotalDue
        ,Issue.AddressLine1
        ,Issue.AddressLine2
        ,Issue.AddressLine3
        ,Issue.AddressLine4
        ,Issue.AddressLine5
        ,Issue.PlanningComplete
        ,Issue.PlannedBy
        ,Issue.Release
        ,Issue.ReleasedBy
        ,Issue.Checking
        ,Issue.Checked
        ,Issue.Despatch
        ,Issue.DespatchChecked
        ,Issue.CompleteDate
        ,Issue.Availability
        ,Issue.CheckingCount
        ,Issue.Branding
        ,Issue.Downsizing
        ,Issue.CheckSheetPrinted
        ,Issue.CreateDate
        ,Issue.VehicleId
    from Issue
   where isnull(Issue.IssueId,'0')  = isnull(@IssueId, isnull(Issue.IssueId,'0'))
  order by DeliveryNoteNumber
  
end
