﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIPHeader_Update
  ///   Filename       : p_InterfaceImportIPHeader_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:32
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportIPHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIPHeaderId int = null,
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null,
  ///   @InvoiceDate datetime = null,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @CustomerAddress1 nvarchar(510) = null,
  ///   @CustomerAddress2 nvarchar(510) = null,
  ///   @CustomerAddress3 nvarchar(510) = null,
  ///   @CustomerAddress4 nvarchar(510) = null,
  ///   @CustomerAddress5 nvarchar(510) = null,
  ///   @CustomerAddress6 nvarchar(510) = null,
  ///   @DeliveryAddress1 nvarchar(510) = null,
  ///   @DeliveryAddress2 nvarchar(510) = null,
  ///   @DeliveryAddress3 nvarchar(510) = null,
  ///   @DeliveryAddress4 nvarchar(510) = null,
  ///   @DeliveryAddress5 nvarchar(510) = null,
  ///   @DeliveryAddress6 nvarchar(510) = null,
  ///   @CustomerName nvarchar(100) = null,
  ///   @CustomerCode nvarchar(100) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @ReferenceNumber nvarchar(100) = null,
  ///   @ChargeTax nvarchar(20) = null,
  ///   @TaxReference nvarchar(100) = null,
  ///   @SalesCode nvarchar(100) = null,
  ///   @TotalNettValue numeric(13,2) = null,
  ///   @LogisticsDataFee numeric(13,2) = null,
  ///   @NettExcLogDataFee numeric(13,2) = null,
  ///   @Tax numeric(13,2) = null,
  ///   @TotalDue numeric(13,2) = null,
  ///   @Message1 nvarchar(510) = null,
  ///   @Message2 nvarchar(510) = null,
  ///   @Message3 nvarchar(510) = null,
  ///   @Printed int = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIPHeader_Update
(
 @InterfaceImportIPHeaderId int = null,
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @InvoiceDate datetime = null,
 @PrimaryKey nvarchar(60) = null,
 @CustomerAddress1 nvarchar(510) = null,
 @CustomerAddress2 nvarchar(510) = null,
 @CustomerAddress3 nvarchar(510) = null,
 @CustomerAddress4 nvarchar(510) = null,
 @CustomerAddress5 nvarchar(510) = null,
 @CustomerAddress6 nvarchar(510) = null,
 @DeliveryAddress1 nvarchar(510) = null,
 @DeliveryAddress2 nvarchar(510) = null,
 @DeliveryAddress3 nvarchar(510) = null,
 @DeliveryAddress4 nvarchar(510) = null,
 @DeliveryAddress5 nvarchar(510) = null,
 @DeliveryAddress6 nvarchar(510) = null,
 @CustomerName nvarchar(100) = null,
 @CustomerCode nvarchar(100) = null,
 @OrderNumber nvarchar(60) = null,
 @ReferenceNumber nvarchar(100) = null,
 @ChargeTax nvarchar(20) = null,
 @TaxReference nvarchar(100) = null,
 @SalesCode nvarchar(100) = null,
 @TotalNettValue numeric(13,2) = null,
 @LogisticsDataFee numeric(13,2) = null,
 @NettExcLogDataFee numeric(13,2) = null,
 @Tax numeric(13,2) = null,
 @TotalDue numeric(13,2) = null,
 @Message1 nvarchar(510) = null,
 @Message2 nvarchar(510) = null,
 @Message3 nvarchar(510) = null,
 @Printed int = null,
 @ProcessedDate datetime = null,
 @RecordStatus nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportIPHeaderId = '-1'
    set @InterfaceImportIPHeaderId = null;
  
	 declare @Error int
 
  update InterfaceImportIPHeader
     set OutboundShipmentId = isnull(@OutboundShipmentId, OutboundShipmentId),
         IssueId = isnull(@IssueId, IssueId),
         InvoiceDate = isnull(@InvoiceDate, InvoiceDate),
         PrimaryKey = isnull(@PrimaryKey, PrimaryKey),
         CustomerAddress1 = isnull(@CustomerAddress1, CustomerAddress1),
         CustomerAddress2 = isnull(@CustomerAddress2, CustomerAddress2),
         CustomerAddress3 = isnull(@CustomerAddress3, CustomerAddress3),
         CustomerAddress4 = isnull(@CustomerAddress4, CustomerAddress4),
         CustomerAddress5 = isnull(@CustomerAddress5, CustomerAddress5),
         CustomerAddress6 = isnull(@CustomerAddress6, CustomerAddress6),
         DeliveryAddress1 = isnull(@DeliveryAddress1, DeliveryAddress1),
         DeliveryAddress2 = isnull(@DeliveryAddress2, DeliveryAddress2),
         DeliveryAddress3 = isnull(@DeliveryAddress3, DeliveryAddress3),
         DeliveryAddress4 = isnull(@DeliveryAddress4, DeliveryAddress4),
         DeliveryAddress5 = isnull(@DeliveryAddress5, DeliveryAddress5),
         DeliveryAddress6 = isnull(@DeliveryAddress6, DeliveryAddress6),
         CustomerName = isnull(@CustomerName, CustomerName),
         CustomerCode = isnull(@CustomerCode, CustomerCode),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         ChargeTax = isnull(@ChargeTax, ChargeTax),
         TaxReference = isnull(@TaxReference, TaxReference),
         SalesCode = isnull(@SalesCode, SalesCode),
         TotalNettValue = isnull(@TotalNettValue, TotalNettValue),
         LogisticsDataFee = isnull(@LogisticsDataFee, LogisticsDataFee),
         NettExcLogDataFee = isnull(@NettExcLogDataFee, NettExcLogDataFee),
         Tax = isnull(@Tax, Tax),
         TotalDue = isnull(@TotalDue, TotalDue),
         Message1 = isnull(@Message1, Message1),
         Message2 = isnull(@Message2, Message2),
         Message3 = isnull(@Message3, Message3),
         Printed = isnull(@Printed, Printed),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         InsertDate = isnull(@InsertDate, InsertDate) 
   where InterfaceImportIPHeaderId = @InterfaceImportIPHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
