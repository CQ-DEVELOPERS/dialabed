﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssue_Delete
  ///   Filename       : p_OutboundShipmentIssue_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jun 2013 12:36:34
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OutboundShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssue_Delete
(
 @OutboundShipmentId int = null,
 @IssueId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete OutboundShipmentIssue
     where OutboundShipmentId = @OutboundShipmentId
       and IssueId = @IssueId
  
  select @Error = @@Error
  
  
  return @Error
  
end
