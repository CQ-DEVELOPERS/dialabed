﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnit_Insert
  ///   Filename       : p_StorageUnit_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:02:11
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnit table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null output,
  ///   @SKUId int = null,
  ///   @ProductId int = null,
  ///   @ProductCategory char(1) = null,
  ///   @PackingCategory char(1) = null,
  ///   @PickEmpty bit = null,
  ///   @StackingCategory int = null,
  ///   @MovementCategory int = null,
  ///   @ValueCategory int = null,
  ///   @StoringCategory int = null,
  ///   @PickPartPallet int = null,
  ///   @MinimumQuantity int = null,
  ///   @ReorderQuantity int = null,
  ///   @MaximumQuantity int = null,
  ///   @DefaultQC bit = null,
  ///   @UnitPrice numeric(13,2) = null,
  ///   @Size nvarchar(200) = null,
  ///   @Colour nvarchar(200) = null,
  ///   @Style nvarchar(200) = null,
  ///   @PreviousUnitPrice numeric(13,2) = null,
  ///   @StockTakeCounts int = null,
  ///   @SerialTracked bit = null,
  ///   @SizeCode nvarchar(200) = null,
  ///   @ColourCode nvarchar(200) = null,
  ///   @StyleCode nvarchar(200) = null,
  ///   @StatusId int = null,
  ///   @PutawayRule nvarchar(400) = null,
  ///   @AllocationRule nvarchar(400) = null,
  ///   @BillingRule nvarchar(400) = null,
  ///   @CycleCountRule nvarchar(400) = null,
  ///   @LotAttributeRule nvarchar(400) = null,
  ///   @StorageCondition nvarchar(400) = null,
  ///   @ManualCost nvarchar(60) = null,
  ///   @OffSet nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   StorageUnit.StorageUnitId,
  ///   StorageUnit.SKUId,
  ///   StorageUnit.ProductId,
  ///   StorageUnit.ProductCategory,
  ///   StorageUnit.PackingCategory,
  ///   StorageUnit.PickEmpty,
  ///   StorageUnit.StackingCategory,
  ///   StorageUnit.MovementCategory,
  ///   StorageUnit.ValueCategory,
  ///   StorageUnit.StoringCategory,
  ///   StorageUnit.PickPartPallet,
  ///   StorageUnit.MinimumQuantity,
  ///   StorageUnit.ReorderQuantity,
  ///   StorageUnit.MaximumQuantity,
  ///   StorageUnit.DefaultQC,
  ///   StorageUnit.UnitPrice,
  ///   StorageUnit.Size,
  ///   StorageUnit.Colour,
  ///   StorageUnit.Style,
  ///   StorageUnit.PreviousUnitPrice,
  ///   StorageUnit.StockTakeCounts,
  ///   StorageUnit.SerialTracked,
  ///   StorageUnit.SizeCode,
  ///   StorageUnit.ColourCode,
  ///   StorageUnit.StyleCode,
  ///   StorageUnit.StatusId,
  ///   StorageUnit.PutawayRule,
  ///   StorageUnit.AllocationRule,
  ///   StorageUnit.BillingRule,
  ///   StorageUnit.CycleCountRule,
  ///   StorageUnit.LotAttributeRule,
  ///   StorageUnit.StorageCondition,
  ///   StorageUnit.ManualCost,
  ///   StorageUnit.OffSet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnit_Insert
(
 @StorageUnitId int = null output,
 @SKUId int = null,
 @ProductId int = null,
 @ProductCategory char(1) = null,
 @PackingCategory char(1) = null,
 @PickEmpty bit = null,
 @StackingCategory int = null,
 @MovementCategory int = null,
 @ValueCategory int = null,
 @StoringCategory int = null,
 @PickPartPallet int = null,
 @MinimumQuantity int = null,
 @ReorderQuantity int = null,
 @MaximumQuantity int = null,
 @DefaultQC bit = null,
 @UnitPrice numeric(13,2) = null,
 @Size nvarchar(200) = null,
 @Colour nvarchar(200) = null,
 @Style nvarchar(200) = null,
 @PreviousUnitPrice numeric(13,2) = null,
 @StockTakeCounts int = null,
 @SerialTracked bit = null,
 @SizeCode nvarchar(200) = null,
 @ColourCode nvarchar(200) = null,
 @StyleCode nvarchar(200) = null,
 @StatusId int = null,
 @PutawayRule nvarchar(400) = null,
 @AllocationRule nvarchar(400) = null,
 @BillingRule nvarchar(400) = null,
 @CycleCountRule nvarchar(400) = null,
 @LotAttributeRule nvarchar(400) = null,
 @StorageCondition nvarchar(400) = null,
 @ManualCost nvarchar(60) = null,
 @OffSet nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @SKUId = '-1'
    set @SKUId = null;
  
  if @ProductId = '-1'
    set @ProductId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
	 declare @Error int
 
  insert StorageUnit
        (SKUId,
         ProductId,
         ProductCategory,
         PackingCategory,
         PickEmpty,
         StackingCategory,
         MovementCategory,
         ValueCategory,
         StoringCategory,
         PickPartPallet,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity,
         DefaultQC,
         UnitPrice,
         Size,
         Colour,
         Style,
         PreviousUnitPrice,
         StockTakeCounts,
         SerialTracked,
         SizeCode,
         ColourCode,
         StyleCode,
         StatusId,
         PutawayRule,
         AllocationRule,
         BillingRule,
         CycleCountRule,
         LotAttributeRule,
         StorageCondition,
         ManualCost,
         OffSet)
  select @SKUId,
         @ProductId,
         @ProductCategory,
         @PackingCategory,
         @PickEmpty,
         @StackingCategory,
         @MovementCategory,
         @ValueCategory,
         @StoringCategory,
         @PickPartPallet,
         @MinimumQuantity,
         @ReorderQuantity,
         @MaximumQuantity,
         @DefaultQC,
         @UnitPrice,
         @Size,
         @Colour,
         @Style,
         @PreviousUnitPrice,
         @StockTakeCounts,
         @SerialTracked,
         @SizeCode,
         @ColourCode,
         @StyleCode,
         @StatusId,
         @PutawayRule,
         @AllocationRule,
         @BillingRule,
         @CycleCountRule,
         @LotAttributeRule,
         @StorageCondition,
         @ManualCost,
         @OffSet 
  
  select @Error = @@Error, @StorageUnitId = scope_identity()
  
  if @Error = 0
    exec @Error = p_StorageUnitHistory_Insert
         @StorageUnitId = @StorageUnitId,
         @SKUId = @SKUId,
         @ProductId = @ProductId,
         @ProductCategory = @ProductCategory,
         @PackingCategory = @PackingCategory,
         @PickEmpty = @PickEmpty,
         @StackingCategory = @StackingCategory,
         @MovementCategory = @MovementCategory,
         @ValueCategory = @ValueCategory,
         @StoringCategory = @StoringCategory,
         @PickPartPallet = @PickPartPallet,
         @MinimumQuantity = @MinimumQuantity,
         @ReorderQuantity = @ReorderQuantity,
         @MaximumQuantity = @MaximumQuantity,
         @DefaultQC = @DefaultQC,
         @UnitPrice = @UnitPrice,
         @Size = @Size,
         @Colour = @Colour,
         @Style = @Style,
         @PreviousUnitPrice = @PreviousUnitPrice,
         @StockTakeCounts = @StockTakeCounts,
         @SerialTracked = @SerialTracked,
         @SizeCode = @SizeCode,
         @ColourCode = @ColourCode,
         @StyleCode = @StyleCode,
         @StatusId = @StatusId,
         @PutawayRule = @PutawayRule,
         @AllocationRule = @AllocationRule,
         @BillingRule = @BillingRule,
         @CycleCountRule = @CycleCountRule,
         @LotAttributeRule = @LotAttributeRule,
         @StorageCondition = @StorageCondition,
         @ManualCost = @ManualCost,
         @OffSet = @OffSet,
         @CommandType = 'Insert'
  
  return @Error
  
end
