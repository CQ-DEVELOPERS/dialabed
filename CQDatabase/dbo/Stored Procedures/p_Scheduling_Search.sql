﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Scheduling_Search
  ///   Filename       : p_Scheduling_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Scheduling_Search
(
 @WarehouseId           int,
 @InboundDocumentTypeId	int,
 @ExternalCompanyCode	  nvarchar(30),
 @ExternalCompany	      nvarchar(255),
 @OrderNumber	          nvarchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        InboundDocumentId         int,
        ReceiptId                 int,
        OrderNumber               nvarchar(30),
        InboundShipmentId         int,
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        NumberOfLines             int,
        PlannedDeliveryDate       datetime,
        ActualDeliveryDate        datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        InboundDocumentType       nvarchar(30),
        LocationId                int,
        Location                  nvarchar(15),
        Rating                    int
       );
  
  if @InboundDocumentTypeId = -1
    set @InboundDocumentTypeId = null
  
  insert @TableResult
        (InboundDocumentId,
         ReceiptId,
         LocationId,
         OrderNumber,
         SupplierCode,
         Supplier,
         Status,
         PlannedDeliveryDate,
         ActualDeliveryDate,
         InboundDocumentType,
         Rating)
  select id.InboundDocumentId,
         r.ReceiptId,
         r.LocationId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         s.Status,
         id.DeliveryDate,
         r.DeliveryDate,
         idt.InboundDocumentType,
         ec.Rating
    from InboundDocument     id  (nolock)
    join ExternalCompany     ec  (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
    join Receipt             r   (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join Status              s   (nolock) on r.StatusId          = s.StatusId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
   where id.InboundDocumentTypeId = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
     and s.Type                   = 'R'
     and s.StatusCode            in ('W','C','D')
  
  update r
     set InboundShipmentId = isr.InboundShipmentId,
         PlannedDeliveryDate = ship.ShipmentDate
    from @TableResult             r
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
    join InboundShipment       ship (nolock) on isr.InboundShipmentId = ship.InboundShipmentId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from InboundLine il
                           where t.InboundDocumentId = il.InboundDocumentId)
    from @TableResult t
  
  update r
     set Location = l.Location
    from @TableResult r
    join Location     l (nolock) on r.LocationId = l.LocationId
  
  select InboundShipmentId,
         ReceiptId,
         OrderNumber,
         SupplierCode,
         Supplier,
         NumberOfLines,
         PlannedDeliveryDate,
         ActualDeliveryDate,
         Status,
         InboundDocumentType,
         Location,
         Rating
    from @TableResult
end
