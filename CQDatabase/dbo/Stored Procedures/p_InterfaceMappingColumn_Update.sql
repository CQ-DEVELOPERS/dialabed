﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingColumn_Update
  ///   Filename       : p_InterfaceMappingColumn_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 10:53:57
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceMappingColumn table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingColumnId int = null,
  ///   @InterfaceMappingColumnCode nvarchar(60) = null,
  ///   @InterfaceMappingColumn nvarchar(100) = null,
  ///   @InterfaceDocumentTypeId int = null,
  ///   @InterfaceFieldId int = null,
  ///   @ColumnNumber int = null,
  ///   @StartPosition int = null,
  ///   @EndPostion int = null,
  ///   @Format nvarchar(60) = null,
  ///   @InterfaceMappingFileId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingColumn_Update
(
 @InterfaceMappingColumnId int = null,
 @InterfaceMappingColumnCode nvarchar(60) = null,
 @InterfaceMappingColumn nvarchar(100) = null,
 @InterfaceDocumentTypeId int = null,
 @InterfaceFieldId int = null,
 @ColumnNumber int = null,
 @StartPosition int = null,
 @EndPostion int = null,
 @Format nvarchar(60) = null,
 @InterfaceMappingFileId int = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceMappingColumnId = '-1'
    set @InterfaceMappingColumnId = null;
  
  if @InterfaceMappingColumnCode = '-1'
    set @InterfaceMappingColumnCode = null;
  
  if @InterfaceMappingColumn = '-1'
    set @InterfaceMappingColumn = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
  if @InterfaceFieldId = '-1'
    set @InterfaceFieldId = null;
  
  if @InterfaceMappingFileId = '-1'
    set @InterfaceMappingFileId = null;
  
	 declare @Error int
 
  update InterfaceMappingColumn
     set InterfaceMappingColumnCode = isnull(@InterfaceMappingColumnCode, InterfaceMappingColumnCode),
         InterfaceMappingColumn = isnull(@InterfaceMappingColumn, InterfaceMappingColumn),
         InterfaceDocumentTypeId = isnull(@InterfaceDocumentTypeId, InterfaceDocumentTypeId),
         InterfaceFieldId = isnull(@InterfaceFieldId, InterfaceFieldId),
         ColumnNumber = isnull(@ColumnNumber, ColumnNumber),
         StartPosition = isnull(@StartPosition, StartPosition),
         EndPostion = isnull(@EndPostion, EndPostion),
         Format = isnull(@Format, Format),
         InterfaceMappingFileId = isnull(@InterfaceMappingFileId, InterfaceMappingFileId) 
   where InterfaceMappingColumnId = @InterfaceMappingColumnId
  
  select @Error = @@Error
  
  
  return @Error
  
end
