﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Scheduling
  ///   Filename       : p_Status_Scheduling.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Scheduling
 
as
begin
	 set nocount on;
  
  select StatusId,
         Status
    from Status (nolock)
   where Type = 'R'
     and StatusCode in ('C','D','W')
  order by Status
end
