﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Update
  ///   Filename       : p_Area_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2014 09:46:58
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Area table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @WarehouseId int = null,
  ///   @Area nvarchar(100) = null,
  ///   @AreaCode nvarchar(20) = null,
  ///   @StockOnHand bit = null,
  ///   @AreaSequence int = null,
  ///   @AreaType nvarchar(20) = null,
  ///   @WarehouseCode nvarchar(60) = null,
  ///   @Replenish bit = null,
  ///   @MinimumShelfLife float = null,
  ///   @Undefined bit = null,
  ///   @STEKPI int = null,
  ///   @BatchTracked bit = null,
  ///   @StockTake bit = null,
  ///   @NarrowAisle bit = null,
  ///   @Interleaving bit = null,
  ///   @PandD bit = null,
  ///   @SmallPick bit = null,
  ///   @AutoLink bit = null,
  ///   @WarehouseCode2 nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Update
(
 @AreaId int = null,
 @WarehouseId int = null,
 @Area nvarchar(100) = null,
 @AreaCode nvarchar(20) = null,
 @StockOnHand bit = null,
 @AreaSequence int = null,
 @AreaType nvarchar(20) = null,
 @WarehouseCode nvarchar(60) = null,
 @Replenish bit = null,
 @MinimumShelfLife float = null,
 @Undefined bit = null,
 @STEKPI int = null,
 @BatchTracked bit = null,
 @StockTake bit = null,
 @NarrowAisle bit = null,
 @Interleaving bit = null,
 @PandD bit = null,
 @SmallPick bit = null,
 @AutoLink bit = null,
 @WarehouseCode2 nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Area = '-1'
    set @Area = null;
  
  if @AreaCode = '-1'
    set @AreaCode = null;
  
	 declare @Error int
 
  update Area
     set WarehouseId = isnull(@WarehouseId, WarehouseId),
         Area = isnull(@Area, Area),
         AreaCode = isnull(@AreaCode, AreaCode),
         StockOnHand = isnull(@StockOnHand, StockOnHand),
         AreaSequence = isnull(@AreaSequence, AreaSequence),
         AreaType = isnull(@AreaType, AreaType),
         WarehouseCode = isnull(@WarehouseCode, WarehouseCode),
         Replenish = isnull(@Replenish, Replenish),
         MinimumShelfLife = isnull(@MinimumShelfLife, MinimumShelfLife),
         Undefined = isnull(@Undefined, Undefined),
         STEKPI = isnull(@STEKPI, STEKPI),
         BatchTracked = isnull(@BatchTracked, BatchTracked),
         StockTake = isnull(@StockTake, StockTake),
         NarrowAisle = isnull(@NarrowAisle, NarrowAisle),
         Interleaving = isnull(@Interleaving, Interleaving),
         PandD = isnull(@PandD, PandD),
         SmallPick = isnull(@SmallPick, SmallPick),
         AutoLink = isnull(@AutoLink, AutoLink),
         WarehouseCode2 = isnull(@WarehouseCode2, WarehouseCode2) 
   where AreaId = @AreaId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_AreaHistory_Insert
         @AreaId = @AreaId,
         @WarehouseId = @WarehouseId,
         @Area = @Area,
         @AreaCode = @AreaCode,
         @StockOnHand = @StockOnHand,
         @AreaSequence = @AreaSequence,
         @AreaType = @AreaType,
         @WarehouseCode = @WarehouseCode,
         @Replenish = @Replenish,
         @MinimumShelfLife = @MinimumShelfLife,
         @Undefined = @Undefined,
         @STEKPI = @STEKPI,
         @BatchTracked = @BatchTracked,
         @StockTake = @StockTake,
         @NarrowAisle = @NarrowAisle,
         @Interleaving = @Interleaving,
         @PandD = @PandD,
         @SmallPick = @SmallPick,
         @AutoLink = @AutoLink,
         @WarehouseCode2 = @WarehouseCode2,
         @CommandType = 'Update'
  
  return @Error
  
end
