﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Insert
  ///   Filename       : p_MenuItem_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 16:06:09
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MenuItem table.
  /// </remarks>
  /// <param>
  ///   @MenuItemId int = null output,
  ///   @MenuId int = null,
  ///   @MenuItem nvarchar(100) = null,
  ///   @ToolTip nvarchar(510) = null,
  ///   @NavigateTo nvarchar(510) = null,
  ///   @ParentMenuItemId int = null,
  ///   @OrderBy smallint = null 
  /// </param>
  /// <returns>
  ///   MenuItem.MenuItemId,
  ///   MenuItem.MenuId,
  ///   MenuItem.MenuItem,
  ///   MenuItem.ToolTip,
  ///   MenuItem.NavigateTo,
  ///   MenuItem.ParentMenuItemId,
  ///   MenuItem.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_Insert
(
 @MenuItemId int = null output,
 @MenuId int = null,
 @MenuItem nvarchar(100) = null,
 @ToolTip nvarchar(510) = null,
 @NavigateTo nvarchar(510) = null,
 @ParentMenuItemId int = null,
 @OrderBy smallint = null 
)
 
as
begin
	 set nocount on;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @MenuItem = '-1'
    set @MenuItem = null;
  
  if @ParentMenuItemId = '-1'
    set @ParentMenuItemId = null;
  
	 declare @Error int
 
  insert MenuItem
        (MenuId,
         MenuItem,
         ToolTip,
         NavigateTo,
         ParentMenuItemId,
         OrderBy)
  select @MenuId,
         @MenuItem,
         @ToolTip,
         @NavigateTo,
         @ParentMenuItemId,
         @OrderBy 
  
  select @Error = @@Error, @MenuItemId = scope_identity()
  
  
  return @Error
  
end
