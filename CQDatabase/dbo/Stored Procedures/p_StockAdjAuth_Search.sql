﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockAdjAuth_Search
  ///   Filename       : p_StockAdjAuth_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockAdjAuth_Search
(
 @WarehouseId int
,@FromDate    datetime = null
,@ToDate      datetime = null
)
 
as
begin
  set nocount on;
  
  select InterfaceExportStockAdjustmentId
        ,RecordType
        ,PrincipalCode
        ,RecordStatus
        ,ProductCode
        ,Product
        ,SKUCode
        ,Batch
        ,Quantity
        ,Weight
        ,Additional1
        ,Additional2
        ,Additional3
        ,Additional4
        ,Additional5
        ,o.Operator as 'CreatedBy'
        ,sa.InsertDate as 'InsertDate'
        ,im.InterfaceMessage
    from InterfaceExportStockAdjustment sa
    left
    join InterfaceMessage im on sa.InterfaceExportStockAdjustmentId = im.InterfaceId
                            and im.InterfaceTable = 'InterfaceExportStockAdjustment'
    left
    join Operator o on sa.CreatedBy = o.OperatorId
   where sa.InsertDate between @FromDate and @ToDate
end
