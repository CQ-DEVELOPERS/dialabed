﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Back_Flush
  ///   Filename       : p_Production_Back_Flush.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Back_Flush
(
 @JobId       int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare @Instructions as table
  (
   InstructionId int
  )
  
  declare @OutboundShipmentId int,
          @OutboundDocumentId int,
          @Display nvarchar(50),
          @OutboundDocumentTypeCode nvarchar(10)
          
  select @OutboundShipmentId = ili.OutboundShipmentId,
         @OutboundDocumentId = ili.OutboundDocumentId
    from Instruction            i (nolock)
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
   where i.JobId = @JobId
  
  --select @OutboundDocumentTypeCode = OutboundDocumentTypeCode
  --  from OutboundDocument od (nolock)
  --  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  -- where od.OutboundDocumentId = @OutboundDocumentId
  
  if isnull(@OutboundDocumentTypeCode, '') != 'MO'
    return
  
  if @OutboundShipmentId is null
    select @Display = ' Order: ' + OrderNumber
      from OutboundDocument (nolock)
     where OutboundDocumentId = @OutboundDocumentId
  else
    set @Display = 'Shipment: ' + CONVERT(nvarchar(10), @OutboundShipmentId)
  
  begin transaction
  
  insert InterfaceExportStockAdjustment
        (RecordType,
         RecordStatus,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional1,
         Additional3,
         Additional5) -- Only used for scripts
  select 'ADJ',
         'N',
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         i.ConfirmedQuantity * -1,
         null,
         a.WarehouseCode,
         @Display,
         'CQ Flush Id: ' + convert(varchar(10), i.InstructionId)
    from Job                j (nolock)
    join Status             s (nolock) on j.StatusId = s.StatusId
    join Instruction        i (nolock) on j.JobId = i.JobId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
    join SKU              sku (nolock) on su.SKUId = sku.SKUId
    join Batch              b (nolock) on sub.BatchId = b.BatchId
    join AreaLocation      al (nolock) on i.StoreLocationId = al.Locationid
    join Area               a (nolock) on al.AreaId = a.AreaId
   where i.JobId = @JobId
     and i.ConfirmedQuantity > 0
     and s.StatusCode = 'CD'
     and isnull(j.BackFlush, 0) = 0
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Job_Update
   @JobId     = @JobId,
   @BackFlush = 1
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Production_Back_Flush'
    rollback transaction
    return @Error
end
