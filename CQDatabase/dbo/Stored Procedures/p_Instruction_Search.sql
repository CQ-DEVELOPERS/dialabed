﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Search
  ///   Filename       : p_Instruction_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Aug 2013 13:13:51
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Instruction table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null output,
  ///   @ReasonId int = null,
  ///   @InstructionTypeId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @WarehouseId int = null,
  ///   @StatusId int = null,
  ///   @JobId int = null,
  ///   @OperatorId int = null,
  ///   @PalletId int = null,
  ///   @AuthorisedBy int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Instruction.InstructionId,
  ///   Instruction.ReasonId,
  ///   Reason.Reason,
  ///   Instruction.InstructionTypeId,
  ///   InstructionType.InstructionType,
  ///   Instruction.StorageUnitBatchId,
  ///   StorageUnitBatch.StorageUnitBatchId,
  ///   Instruction.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Instruction.StatusId,
  ///   Status.Status,
  ///   Instruction.JobId,
  ///   Job.JobId,
  ///   Instruction.OperatorId,
  ///   Operator.Operator,
  ///   Instruction.PickLocationId,
  ///   Instruction.StoreLocationId,
  ///   Instruction.ReceiptLineId,
  ///   Instruction.IssueLineId,
  ///   Instruction.InstructionRefId,
  ///   Instruction.Quantity,
  ///   Instruction.ConfirmedQuantity,
  ///   Instruction.Weight,
  ///   Instruction.ConfirmedWeight,
  ///   Instruction.PalletId,
  ///   Pallet.PalletId,
  ///   Instruction.CreateDate,
  ///   Instruction.StartDate,
  ///   Instruction.EndDate,
  ///   Instruction.Picked,
  ///   Instruction.Stored,
  ///   Instruction.CheckQuantity,
  ///   Instruction.CheckWeight,
  ///   Instruction.OutboundShipmentId,
  ///   Instruction.DropSequence,
  ///   Instruction.PackagingWeight,
  ///   Instruction.PackagingQuantity,
  ///   Instruction.NettWeight,
  ///   Instruction.PreviousQuantity,
  ///   Instruction.AuthorisedBy,
  ///   Operator.Operator,
  ///   Instruction.AuthorisedStatus,
  ///   Instruction.SOH,
  ///   Instruction.AutoComplete 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Search
(
 @InstructionId int = null output,
 @ReasonId int = null,
 @InstructionTypeId int = null,
 @StorageUnitBatchId int = null,
 @WarehouseId int = null,
 @StatusId int = null,
 @JobId int = null,
 @OperatorId int = null,
 @PalletId int = null,
 @AuthorisedBy int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InstructionId = '-1'
    set @InstructionId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @JobId = '-1'
    set @JobId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @PalletId = '-1'
    set @PalletId = null;
  
  if @AuthorisedBy = '-1'
    set @AuthorisedBy = null;
  
 
  select
         Instruction.InstructionId
        ,Instruction.ReasonId
         ,ReasonReasonId.Reason as 'Reason'
        ,Instruction.InstructionTypeId
         ,InstructionTypeInstructionTypeId.InstructionType as 'InstructionType'
        ,Instruction.StorageUnitBatchId
        ,Instruction.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Instruction.StatusId
         ,StatusStatusId.Status as 'Status'
        ,Instruction.JobId
        ,Instruction.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,Instruction.PickLocationId
        ,Instruction.StoreLocationId
        ,Instruction.ReceiptLineId
        ,Instruction.IssueLineId
        ,Instruction.InstructionRefId
        ,Instruction.Quantity
        ,Instruction.ConfirmedQuantity
        ,Instruction.Weight
        ,Instruction.ConfirmedWeight
        ,Instruction.PalletId
        ,Instruction.CreateDate
        ,Instruction.StartDate
        ,Instruction.EndDate
        ,Instruction.Picked
        ,Instruction.Stored
        ,Instruction.CheckQuantity
        ,Instruction.CheckWeight
        ,Instruction.OutboundShipmentId
        ,Instruction.DropSequence
        ,Instruction.PackagingWeight
        ,Instruction.PackagingQuantity
        ,Instruction.NettWeight
        ,Instruction.PreviousQuantity
        ,Instruction.AuthorisedBy
         ,OperatorAuthorisedBy.Operator as 'OperatorAuthorisedBy'
        ,Instruction.AuthorisedStatus
        ,Instruction.SOH
        ,Instruction.AutoComplete
    from Instruction
    left
    join Reason ReasonReasonId on ReasonReasonId.ReasonId = Instruction.ReasonId
    left
    join InstructionType InstructionTypeInstructionTypeId on InstructionTypeInstructionTypeId.InstructionTypeId = Instruction.InstructionTypeId
    left
    join StorageUnitBatch StorageUnitBatchStorageUnitBatchId on StorageUnitBatchStorageUnitBatchId.StorageUnitBatchId = Instruction.StorageUnitBatchId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Instruction.WarehouseId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = Instruction.StatusId
    left
    join Job JobJobId on JobJobId.JobId = Instruction.JobId
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = Instruction.OperatorId
    left
    join Pallet PalletPalletId on PalletPalletId.PalletId = Instruction.PalletId
    left
    join Operator OperatorAuthorisedBy on OperatorAuthorisedBy.OperatorId = Instruction.AuthorisedBy
   where isnull(Instruction.InstructionId,'0')  = isnull(@InstructionId, isnull(Instruction.InstructionId,'0'))
     and isnull(Instruction.ReasonId,'0')  = isnull(@ReasonId, isnull(Instruction.ReasonId,'0'))
     and isnull(Instruction.InstructionTypeId,'0')  = isnull(@InstructionTypeId, isnull(Instruction.InstructionTypeId,'0'))
     and isnull(Instruction.StorageUnitBatchId,'0')  = isnull(@StorageUnitBatchId, isnull(Instruction.StorageUnitBatchId,'0'))
     and isnull(Instruction.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Instruction.WarehouseId,'0'))
     and isnull(Instruction.StatusId,'0')  = isnull(@StatusId, isnull(Instruction.StatusId,'0'))
     and isnull(Instruction.JobId,'0')  = isnull(@JobId, isnull(Instruction.JobId,'0'))
     and isnull(Instruction.OperatorId,'0')  = isnull(@OperatorId, isnull(Instruction.OperatorId,'0'))
     and isnull(Instruction.PalletId,'0')  = isnull(@PalletId, isnull(Instruction.PalletId,'0'))
     and isnull(Instruction.AuthorisedBy,'0')  = isnull(@AuthorisedBy, isnull(Instruction.AuthorisedBy,'0'))
  order by AuthorisedStatus
  
end
