﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Inbound_Get_Batch_of_Order
  ///   Filename       : p_Mobile_Inbound_Get_Batch_of_Order.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Inbound_Get_Batch_of_Order
(
	@receiptLineId int
)
 
as
begin
  set nocount on;
  
declare @ReceiptId int,
		@Batch nvarchar(50),
		@CurrentStorageUnitId int

select @ReceiptId = rl.ReceiptId
  from ReceiptLine		rl 
 where rl.ReceiptLineId = @receiptLineId
 
select @CurrentStorageUnitId = su.StorageUnitId
  from ReceiptLine		rl 
  join Status			 s(nolock) on rl.StatusId = s.StatusId
  join InboundLine		il(nolock) on rl.InboundLineId = il.InboundLineId
  join StorageUnit		su(nolock) on il.StorageUnitId = su.StorageUnitId
 where rl.ReceiptLineId = @receiptLineId
 
select @Batch = b.Batch
  from ReceiptLine			rl 
  join Status				 s(nolock) on rl.StatusId = s.StatusId
  join InboundLine			il(nolock) on rl.InboundLineId = il.InboundLineId
  join StorageUnit			su(nolock) on il.StorageUnitId = su.StorageUnitId
  join StorageUnitBatch	   sub(nolock) on su.StorageUnitId = sub.StorageUnitId
  join Batch				 b(nolock) on sub.BatchId = b.BatchId
 where rl.ReceiptId = @ReceiptId
   and su.StorageUnitId = @CurrentStorageUnitId
   and rl.StorageUnitBatchId = sub.StorageUnitBatchId
   and s.StatusCode in ('R')

select isnull(@Batch,'')
end
