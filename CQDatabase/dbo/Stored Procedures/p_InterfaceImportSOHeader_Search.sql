﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOHeader_Search
  ///   Filename       : p_InterfaceImportSOHeader_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:17
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportSOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHeaderId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportSOHeader.InterfaceImportSOHeaderId,
  ///   InterfaceImportSOHeader.PrimaryKey,
  ///   InterfaceImportSOHeader.OrderNumber,
  ///   InterfaceImportSOHeader.RecordType,
  ///   InterfaceImportSOHeader.RecordStatus,
  ///   InterfaceImportSOHeader.CustomerCode,
  ///   InterfaceImportSOHeader.Customer,
  ///   InterfaceImportSOHeader.Address,
  ///   InterfaceImportSOHeader.FromWarehouseCode,
  ///   InterfaceImportSOHeader.ToWarehouseCode,
  ///   InterfaceImportSOHeader.Route,
  ///   InterfaceImportSOHeader.DeliveryDate,
  ///   InterfaceImportSOHeader.Remarks,
  ///   InterfaceImportSOHeader.NumberOfLines,
  ///   InterfaceImportSOHeader.Additional1,
  ///   InterfaceImportSOHeader.Additional2,
  ///   InterfaceImportSOHeader.Additional3,
  ///   InterfaceImportSOHeader.Additional4,
  ///   InterfaceImportSOHeader.Additional5,
  ///   InterfaceImportSOHeader.Additional6,
  ///   InterfaceImportSOHeader.Additional7,
  ///   InterfaceImportSOHeader.Additional8,
  ///   InterfaceImportSOHeader.Additional9,
  ///   InterfaceImportSOHeader.Additional10,
  ///   InterfaceImportSOHeader.ProcessedDate,
  ///   InterfaceImportSOHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOHeader_Search
(
 @InterfaceImportSOHeaderId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceImportSOHeaderId = '-1'
    set @InterfaceImportSOHeaderId = null;
  
 
  select
         InterfaceImportSOHeader.InterfaceImportSOHeaderId
        ,InterfaceImportSOHeader.PrimaryKey
        ,InterfaceImportSOHeader.OrderNumber
        ,InterfaceImportSOHeader.RecordType
        ,InterfaceImportSOHeader.RecordStatus
        ,InterfaceImportSOHeader.CustomerCode
        ,InterfaceImportSOHeader.Customer
        ,InterfaceImportSOHeader.Address
        ,InterfaceImportSOHeader.FromWarehouseCode
        ,InterfaceImportSOHeader.ToWarehouseCode
        ,InterfaceImportSOHeader.Route
        ,InterfaceImportSOHeader.DeliveryDate
        ,InterfaceImportSOHeader.Remarks
        ,InterfaceImportSOHeader.NumberOfLines
        ,InterfaceImportSOHeader.Additional1
        ,InterfaceImportSOHeader.Additional2
        ,InterfaceImportSOHeader.Additional3
        ,InterfaceImportSOHeader.Additional4
        ,InterfaceImportSOHeader.Additional5
        ,InterfaceImportSOHeader.Additional6
        ,InterfaceImportSOHeader.Additional7
        ,InterfaceImportSOHeader.Additional8
        ,InterfaceImportSOHeader.Additional9
        ,InterfaceImportSOHeader.Additional10
        ,InterfaceImportSOHeader.ProcessedDate
        ,InterfaceImportSOHeader.InsertDate
    from InterfaceImportSOHeader
   where isnull(InterfaceImportSOHeader.InterfaceImportSOHeaderId,'0')  = isnull(@InterfaceImportSOHeaderId, isnull(InterfaceImportSOHeader.InterfaceImportSOHeaderId,'0'))
  
end
