﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportLotBack_Search
  ///   Filename       : p_InterfaceImportLotBack_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:44
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportLotBack table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportLotBack.InterfaceImportLotId,
  ///   InterfaceImportLotBack.RecordStatus,
  ///   InterfaceImportLotBack.InsertDate,
  ///   InterfaceImportLotBack.ProcessedDate,
  ///   InterfaceImportLotBack.LotId,
  ///   InterfaceImportLotBack.Code,
  ///   InterfaceImportLotBack.WhseId,
  ///   InterfaceImportLotBack.Whse,
  ///   InterfaceImportLotBack.QtyOnHand,
  ///   InterfaceImportLotBack.QtyFree,
  ///   InterfaceImportLotBack.LotStatusId,
  ///   InterfaceImportLotBack.LotStatus,
  ///   InterfaceImportLotBack.ExpiryDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportLotBack_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportLotBack.InterfaceImportLotId
        ,InterfaceImportLotBack.RecordStatus
        ,InterfaceImportLotBack.InsertDate
        ,InterfaceImportLotBack.ProcessedDate
        ,InterfaceImportLotBack.LotId
        ,InterfaceImportLotBack.Code
        ,InterfaceImportLotBack.WhseId
        ,InterfaceImportLotBack.Whse
        ,InterfaceImportLotBack.QtyOnHand
        ,InterfaceImportLotBack.QtyFree
        ,InterfaceImportLotBack.LotStatusId
        ,InterfaceImportLotBack.LotStatus
        ,InterfaceImportLotBack.ExpiryDate
    from InterfaceImportLotBack
  
end
