﻿/*
  /// <summary>
  ///   Procedure Name : p_Report_Receiving_Check_Sheet_SerialNo_Capture
  ///   Filename       : p_Report_Receiving_Check_Sheet_SerialNo_Capture.sql
  ///   Create By      : Karen
  ///   Date Created   : Jan 2014
  /// </summary>
  /// <remarks>
  ///  
  /// </remarks>
  /// <param>
  ///  
  /// </param>
  /// <returns>
  ///  
  /// </returns>
  /// <newpara>
  ///   Modified by    :
  ///   Modified Date  :
  ///   Details        :
  /// </newpara>
*/
CREATE procedure p_Report_Receiving_Check_Sheet_SerialNo_Capture
(
@ReceiptId		   int = null
)
 
as
begin

  set nocount on;
  
  declare @OrderNumber nvarchar(30),
		  @PrintSerial bit
  
  set @PrintSerial = 0
  
  select @PrintSerial = 1
    from  Receipt r 
	join ReceiptLine rl (nolock) on r.ReceiptId = rl.ReceiptId
	join StorageUnitBatch sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
	join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
   where su.SerialTracked = 1
 
	select	@OrderNumber = id.OrderNumber
	from Receipt r 
	join InboundDocument id on r.InboundDocumentId = id.InboundDocumentId

select	@OrderNumber as OrderNumber,
		@PrintSerial as PrintSerial

end 
 
