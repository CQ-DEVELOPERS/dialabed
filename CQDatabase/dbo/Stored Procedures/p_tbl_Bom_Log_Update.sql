﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_tbl_Bom_Log_Update
  ///   Filename       : p_tbl_Bom_Log_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:59
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the tbl_Bom_Log table.
  /// </remarks>
  /// <param>
  ///   @Insertdate datetime = null,
  ///   @xmlstring varchar(max) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_tbl_Bom_Log_Update
(
 @Insertdate datetime = null,
 @xmlstring varchar(max) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update tbl_Bom_Log
     set Insertdate = isnull(@Insertdate, Insertdate),
         xmlstring = isnull(@xmlstring, xmlstring) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
