﻿--IF OBJECT_ID('dbo.p_Mobile_Next_Job') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Mobile_Next_Job
--    IF OBJECT_ID('dbo.p_Mobile_Next_Job') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Mobile_Next_Job >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Mobile_Next_Job >>>'
--END
--go
/*
      <summary>
        Procedure Name : p_Mobile_Next_Job
        Filename       : p_Mobile_Next_Job.sql
        Create By      : Grant Schultz
        Date Created   : 11 June 2007 14:15:00
      </summary>
      <remarks>
        Inserts palletised lines into the instruction table
      </remarks>
      <param>
        @OperatorId          int,
        @JobId               int = null output
      </param>
      <returns>
        @error
      </returns>
      <newpara>
        Modified by    : 
        Modified Date  : 
        Details        : 
      </newpara>
*/
create procedure p_Mobile_Next_Job
(
 @OperatorId          int,
 @JobId               int = null output,
 @InstructionTypeCode nvarchar(10) = null,
 @ReferenceNumber     nvarchar(30) = null
)
as
begin
	 set nocount on
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         OperatorId,
         JobId,
         ErrorMsg,
         ReferenceNumber,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @OperatorId,
         @JobId,
         'InstructionTypeCode = ' + @InstructionTypeCode,
         @ReferenceNumber,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @TableResult as table
  (
   JobId          int,
   InstructionId  int,
   CreateDate     datetime,
   PickLocationId int,
   DTPriority     int,
   OGPriority     int,
   JobPriority    int,
   LGPriority     int default 0,
   Type           char(2)
  )
  
  declare @error             int,
          @errormsg          nvarchar(500),
          @OperatorGroupId   int,
          @WarehouseId       int,
          @rowcount          int,
          @StatusId          int
  
  select @StatusId = dbo.ufn_StatusId('IS','S')
  
  if @JobId = -1
    set @JobId = null
  
  select @OperatorGroupId = OperatorGroupId,
         @WarehouseId     = WarehouseId
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  if @InstructionTypeCode in ('P','PM','PM','M','R')
  begin
    if @jobId is null
    begin
      insert @TableResult
            (JobId,
             JobPriority,
             InstructionId,
             OGPriority,
             Type,
             LGPriority,
             CreateDate)
      select distinct j.JobId,
             j.PriorityId,
             i.InstructionId,
             ogit.OrderBy,
             sj.Type,
             isnull(lg.OrderBy,999999),
             i.CreateDate
        from OperatorGroupInstructionType ogit (nolock)
        join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
        join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
        join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
        join Job                          j    (nolock) on i.JobId                = j.JobId
        left
        join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
        left
        join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
        join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                       and ogit.OperatorGroupId   = aog.OperatorGroupId
        join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
        join Status                       si   (nolock) on i.StatusId             = si.StatusId
        -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
        join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                       and ogit.OperatorGroupId   = lg.OperatorGroupId
       where ogit.OperatorGroupId             = @OperatorGroupId
         and j.WarehouseId   = @WarehouseId
         and isnull(j.OperatorId,@OperatorId) = @OperatorId
         and sj.StatusCode                    in ('RL','S')
         and si.StatusCode                    in ('W','S')
         and it.InstructionTypeCode           in ('P','PM','PM','M','R')
      
      set @rowcount = @@rowcount
    end
    else
    begin
      insert @TableResult
            (JobId,
             JobPriority,
             InstructionId,
             OGPriority,
             Type,
             LGPriority,
             CreateDate)
      select distinct j.JobId,
             j.PriorityId,
             i.InstructionId,
             ogit.OrderBy,
             sj.Type,
             isnull(lg.OrderBy,999999),
             i.CreateDate
        from OperatorGroupInstructionType ogit (nolock)
        join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
        join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
        join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
        join Job                          j    (nolock) on i.JobId                = j.JobId
        --left
        --join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
        --left
        join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
        join AreaOperatorGroup             aog (nolock) on al.AreaId              = aog.AreaId  -- isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                       and ogit.OperatorGroupId   = aog.OperatorGroupId
        join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
        join Status                       si   (nolock) on i.StatusId             = si.StatusId
        -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
        join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                       and ogit.OperatorGroupId   = lg.OperatorGroupId
       where ogit.OperatorGroupId             = @OperatorGroupId
         and j.WarehouseId                    = @WarehouseId
         and isnull(j.OperatorId,@OperatorId) = @OperatorId
         and sj.StatusCode                   in ('RL','S')
         and si.StatusCode                   in ('W','S')
         and it.InstructionTypeCode          in ('P','PM','PM','M','R')
         and j.JobId                          = @jobId
      
      set @rowcount = @@rowcount
    end
  end
  else
  begin
    if @JobId is null
    begin
      insert @TableResult
            (JobId,
             JobPriority,
             InstructionId,
             OGPriority,
             Type,
             CreateDate)
      select distinct j.JobId,
             j.PriorityId,
             i.InstructionId,
             ogit.OrderBy,
             sj.Type,
             i.CreateDate
        from OperatorGroupInstructionType ogit (nolock)
        join Instruction                     i (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
        join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
        join Job                             j (nolock) on i.JobId                = j.JobId
        join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
        join AreaOperatorGroup             aog (nolock) on al.AreaId              = aog.AreaId
                                                       and ogit.OperatorGroupId   = aog.OperatorGroupId
        join Priority                        p (nolock) on j.PriorityId           = p.PriorityId
        join Status                         sj (nolock) on j.StatusId             = sj.StatusId
        join Status                         si (nolock) on i.StatusId       = si.StatusId
   where ogit.OperatorGroupId             = @OperatorGroupId
         and j.WarehouseId                    = @WarehouseId
         and isnull(j.OperatorId,@OperatorId) = @OperatorId
         and sj.StatusCode                    in ('RL','S')
         and si.StatusCode                    in ('W','S')
         and j.ReferenceNumber               is null
         and it.InstructionTypeCode          in ('PM','PS','FM','O')
      
      set @rowcount = @@rowcount
    end
    else
    begin
      insert @TableResult
            (JobId,
             JobPriority,
             InstructionId,
             OGPriority,
             Type,
             CreateDate)
      select distinct j.JobId,
             j.PriorityId,
             i.InstructionId,
             ogit.OrderBy,
             sj.Type,
             i.CreateDate
        from OperatorGroupInstructionType ogit (nolock)
        join Instruction                     i (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
        join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
        join Job                             j (nolock) on i.JobId                = j.JobId
        join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
--        join AreaOperatorGroup             aog (nolock) on al.AreaId              = aog.AreaId
--                                                       and ogit.OperatorGroupId   = aog.OperatorGroupId
        join Priority                        p (nolock) on j.PriorityId           = p.PriorityId
        join Status                         sj (nolock) on j.StatusId             = sj.StatusId
        join Status                         si (nolock) on i.StatusId             = si.StatusId
       where ogit.OperatorGroupId             = @OperatorGroupId
         and j.WarehouseId                    = @WarehouseId
         and isnull(j.OperatorId,@OperatorId) = @OperatorId
         and sj.StatusCode                   in ('RL','S')
         and si.StatusCode                   in ('W','S')
         and it.InstructionTypeCode          in ('PM','PS','FM','O')
         and j.JobId                          = @JobId
      
      set @rowcount = @@rowcount
    end
  end
  
  select @rowcount = count(1) from @TableResult
  
  if @rowcount = 0
  begin
    set @JobId = null
    set @Error = -1
    goto error
  end
  
  update tr
     set JobPriority = p.OrderBy
    from @TableResult tr
    join Priority      p (nolock) on tr.JobPriority = p.PriorityId
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  update tr
     set DTPriority = p.OrderBy,
         CreateDate = isnull(i.DeliveryDate, tr.CreateDate)
    from @TableResult tr
    join IssueLineInstruction     ili (nolock) on tr.InstructionId = ili.InstructionId
    join OperatorGroupDocumentType dt (nolock) on ili.OutboundDocumentTypeId = dt.OutboundDocumentTypeId
    join Priority                   p (nolock) on dt.PriorityId              = p.PriorityId
    join Issue                      i (nolock) on ili.IssueId                = i.IssueId
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  update @TableResult
     set DTPriority = 2
   where DTPriority is null
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  select top 1 @JobId = tr.JobId
    from @TableResult tr
  order by DTPriority, OGPriority, JobPriority, LGPriority, CreateDate
  
  update Job
     set OperatorId      = @OperatorId,
         StatusId        = @StatusId,
         ReferenceNumber = @ReferenceNumber
   where JobId = @JobId
     and (OperatorId is null
     or   OperatorId = @OperatorId) -- Should stop duplicates, if another users get this one first
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0 or @rowcount = 0
 begin
    set @JobId = null
    set @error = -1
    goto error
  end
  
  if @InstructionTypeCode = 'PM'
  begin
    exec @Error = p_Instruction_DropSequence
     @JobId = @JobId
    
    if @error <> 0
    goto error
  end
  
  update MobileLog
     set EndDate = getdate(),
         JobId = @JobId
   where MobileLogId = @MobileLogId
  
  return
  
  error:
    update MobileLog
       set EndDate = getdate(),
           JobId = @JobId,
           StatusCode = convert(varchar(10), @Error)
     where MobileLogId = @MobileLogId

    return @error
end
--go
--IF OBJECT_ID('dbo.p_Mobile_Next_Job') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Mobile_Next_Job >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Mobile_Next_Job >>>'
--go
