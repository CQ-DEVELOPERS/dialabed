﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactList_Insert
  ///   Filename       : p_ContactList_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:18:12
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ContactList table.
  /// </remarks>
  /// <param>
  ///   @ContactListId int = null output,
  ///   @ExternalCompanyId int = null,
  ///   @OperatorId int = null,
  ///   @ContactPerson nvarchar(510) = null,
  ///   @Telephone nvarchar(510) = null,
  ///   @Fax nvarchar(510) = null,
  ///   @EMail nvarchar(510) = null,
  ///   @Alias varchar(50) = null,
  ///   @Type nvarchar(40) = null,
  ///   @ReportTypeId int = null 
  /// </param>
  /// <returns>
  ///   ContactList.ContactListId,
  ///   ContactList.ExternalCompanyId,
  ///   ContactList.OperatorId,
  ///   ContactList.ContactPerson,
  ///   ContactList.Telephone,
  ///   ContactList.Fax,
  ///   ContactList.EMail,
  ///   ContactList.Alias,
  ///   ContactList.Type,
  ///   ContactList.ReportTypeId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContactList_Insert
(
 @ContactListId int = null output,
 @ExternalCompanyId int = null,
 @OperatorId int = null,
 @ContactPerson nvarchar(510) = null,
 @Telephone nvarchar(510) = null,
 @Fax nvarchar(510) = null,
 @EMail nvarchar(510) = null,
 @Alias varchar(50) = null,
 @Type nvarchar(40) = null,
 @ReportTypeId int = null 
)
 
as
begin
	 set nocount on;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ReportTypeId = '-1'
    set @ReportTypeId = null;
  
	 declare @Error int
 
  insert ContactList
        (ExternalCompanyId,
         OperatorId,
         ContactPerson,
         Telephone,
         Fax,
         EMail,
         Alias,
         Type,
         ReportTypeId)
  select @ExternalCompanyId,
         @OperatorId,
         @ContactPerson,
         @Telephone,
         @Fax,
         @EMail,
         @Alias,
         @Type,
         @ReportTypeId 
  
  select @Error = @@Error, @ContactListId = scope_identity()
  
  
  return @Error
  
end
