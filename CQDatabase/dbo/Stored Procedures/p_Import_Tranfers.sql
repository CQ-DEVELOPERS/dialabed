﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Import_Tranfers
  ///   Filename       : p_Import_Tranfers.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Apr 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Import_Tranfers
(
 @xmlstring nvarchar(max)
)
 
as
begin
	 set nocount on;
  
  declare @DesktopLogId int
  
  --insert DesktopLog
  --      (ProcName,
  --       StartDate)
  --select OBJECT_NAME(@@PROCID),
  --       getdate()
  
  --select @DesktopLogId = scope_identity()
  
  declare @TransferHeader as table
  (
   Id                int identity,
   OrderNumber       nvarchar(30),
   FromWarehouseCode nvarchar(50),
   ToWarehouseCode   nvarchar(50),
   InsertDate        datetime default getdate()
  )
  
  declare @TransferDetail as table
  (
   Id                int identity,
   OrderNumber       nvarchar(30),
   FromWarehouseCode nvarchar(50),
   ProductCode       nvarchar(50),
   ToWarehouseCode   nvarchar(50),
   Quantity          nvarchar(50),
   InsertDate        datetime default getdate()
  )
  
  declare @Error             int = 0,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @command           varchar(max),
          @InterfaceImportHeaderId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  begin try
  
  delete TransferMaster
  declare @doc xml

  set @doc = convert(xml,@xmlstring)

  DECLARE @idoc int,
          @InsertDate datetime

  Set @InsertDate = Getdate()

  EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
   
		Insert TransferMaster
								(FromWarehouseCode,
								 ProductCode,
								 ToWarehouseCode,
								 Quantity) 
		 SELECT  FromWarehouseCode,
				 ProductCode,
				 ToWarehouseCode,
				 Quantity
		  from OPENXML (@idoc, '/root/Line',1)
            WITH (FromWarehouseCode nvarchar(50) 'FromWarehouseCode',
				  ProductCode		nvarchar(50) 'ProductCode',
                  ToWarehouseCode   nvarchar(50) 'ToWarehouseCode',
				  Quantity			nvarchar(50) 'Quantity')
  
  delete TransferMaster where FromWarehouseCode = 'FromWarehouse'
  
  
  insert @TransferHeader
         (FromWarehouseCode,
          ToWarehouseCode)
  select distinct FromWarehouseCode,
         ToWarehouseCode
    from TransferMaster t
		Join Warehouse w on w.WarehouseCode = t.FromWarehouseCode
  
  update @TransferHeader set OrderNumber = CONVERT(varchar(30), InsertDate, 121)
  update @TransferHeader set OrderNumber = replace(OrderNumber, ':','')
  update @TransferHeader set OrderNumber = replace(OrderNumber, '.','')
  update @TransferHeader set OrderNumber = replace(OrderNumber, '-','')
  update @TransferHeader set OrderNumber = replace(OrderNumber, ' ','')
  update @TransferHeader set OrderNumber = OrderNumber + '(' + convert(varchar(30), Id) + ')'
  
  insert @TransferDetail
         (OrderNumber,
          FromWarehouseCode,
          ToWarehouseCode,
          ProductCode,
          Quantity)
  select distinct
         th.OrderNumber,
         th.FromWarehouseCode,
         th.ToWarehouseCode,
         tm.ProductCode,
         tm.Quantity
    from TransferMaster tm
    join @TransferHeader th on tm.FromWarehouseCode = th.FromWarehouseCode
                           and tm.ToWarehouseCode = th.ToWarehouseCode
  
  if 1=1
  begin
    insert InterfaceImportHeader
          (OrderNumber,
           PrimaryKey,
           RecordType,
           RecordStatus,
           FromWarehouseCode,
           ToWarehouseCode,
           DeliveryDate,
           Remarks,
           Additional1,
           Additional2)
    select distinct OrderNumber,
           OrderNumber,
           '67',
           'N',
           FromWarehouseCode,
           ToWarehouseCode,
           @GetDate,
           '',
           '67',
           'Manual Transfer'
      from @TransferHeader
    
    insert InterfaceImportDetail
          (InterfaceImportHeaderId,
           ForeignKey,
           LineNumber,
           ProductCode,
           Product,
           Quantity,
           Additional1)
    select h.InterfaceImportHeaderId,
           h.PrimaryKey,
           td.Id,
           td.ProductCode,
           td.ProductCode,
           td.Quantity,
           td.FromWarehouseCode
      from InterfaceImportHeader h
      join @TransferDetail      td on h.OrderNumber = td.OrderNumber
      join Product               p on td.ProductCode = p.ProductCode -- Stop invliad codes coming in
     where td.Quantity > 0 -- Stop zero qty being imported
  end
  end try
  begin catch
    select @Error = @@ERROR
    goto error
  end catch
  
  --select *
  --  from TransferMaster
  
  if @Error <> 0
    goto error
  
  commit transaction
  
  --update DesktopLog
  --   set Errormsg = 'Successful',
  --       EndDate  = getdate()
  -- where DesktopLogId = @DesktopLogId
  
  return 0
  
  error:
    raiserror 900000 'Error executing p_Import_Tranfers'
    rollback transaction
    
    update DesktopLog
       set Errormsg = isnull(@Errormsg, 'Error executing ' + OBJECT_NAME(@@PROCID)),
           EndDate  = getdate()
     where DesktopLogId = @DesktopLogId
    
    return @Error
end

