﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExtract_Insert
  ///   Filename       : p_InterfaceExtract_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:16
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExtract table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExtractId int = null,
  ///   @RecordType varchar(20) = null,
  ///   @SequenceNumber char(9) = null,
  ///   @OrderNumber varchar(20) = null,
  ///   @LineNumber int = null,
  ///   @RecordStatus char(1) = null,
  ///   @ExtractCounter int = null,
  ///   @ExtractedDate datetime = null,
  ///   @ProcessedDate datetime = null,
  ///   @Data varchar(500) = null 
  /// </param>
  /// <returns>
  ///   InterfaceExtract.InterfaceExtractId,
  ///   InterfaceExtract.RecordType,
  ///   InterfaceExtract.SequenceNumber,
  ///   InterfaceExtract.OrderNumber,
  ///   InterfaceExtract.LineNumber,
  ///   InterfaceExtract.RecordStatus,
  ///   InterfaceExtract.ExtractCounter,
  ///   InterfaceExtract.ExtractedDate,
  ///   InterfaceExtract.ProcessedDate,
  ///   InterfaceExtract.Data 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExtract_Insert
(
 @InterfaceExtractId int = null,
 @RecordType varchar(20) = null,
 @SequenceNumber char(9) = null,
 @OrderNumber varchar(20) = null,
 @LineNumber int = null,
 @RecordStatus char(1) = null,
 @ExtractCounter int = null,
 @ExtractedDate datetime = null,
 @ProcessedDate datetime = null,
 @Data varchar(500) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceExtract
        (InterfaceExtractId,
         RecordType,
         SequenceNumber,
         OrderNumber,
         LineNumber,
         RecordStatus,
         ExtractCounter,
         ExtractedDate,
         ProcessedDate,
         Data)
  select @InterfaceExtractId,
         @RecordType,
         @SequenceNumber,
         @OrderNumber,
         @LineNumber,
         @RecordStatus,
         @ExtractCounter,
         @ExtractedDate,
         @ProcessedDate,
         @Data 
  
  select @Error = @@Error
  
  
  return @Error
  
end
