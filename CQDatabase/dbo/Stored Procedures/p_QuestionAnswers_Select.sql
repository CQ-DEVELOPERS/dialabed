﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QuestionAnswers_Select
  ///   Filename       : p_QuestionAnswers_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:29
  /// </summary>
  /// <remarks>
  ///   Selects rows from the QuestionAnswers table.
  /// </remarks>
  /// <param>
  ///   @QuestionAnswersId int = null 
  /// </param>
  /// <returns>
  ///   QuestionAnswers.QuestionId,
  ///   QuestionAnswers.Answer,
  ///   QuestionAnswers.JobId,
  ///   QuestionAnswers.ReceiptId,
  ///   QuestionAnswers.OrderNumber,
  ///   QuestionAnswers.PalletId,
  ///   QuestionAnswers.DateAsked,
  ///   QuestionAnswers.QuestionAnswersId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QuestionAnswers_Select
(
 @QuestionAnswersId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         QuestionAnswers.QuestionId
        ,QuestionAnswers.Answer
        ,QuestionAnswers.JobId
        ,QuestionAnswers.ReceiptId
        ,QuestionAnswers.OrderNumber
        ,QuestionAnswers.PalletId
        ,QuestionAnswers.DateAsked
        ,QuestionAnswers.QuestionAnswersId
    from QuestionAnswers
   where isnull(QuestionAnswers.QuestionAnswersId,'0')  = isnull(@QuestionAnswersId, isnull(QuestionAnswers.QuestionAnswersId,'0'))
  order by Answer
  
end
