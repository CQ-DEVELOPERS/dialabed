﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Pick_Transaction
  ///   Filename       : p_Mobile_Pick_Transaction.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Pick_Transaction
(
 @instructionId int
)
 
as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         InstructionId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId = @instructionId
  
  if @Error <> 0
  begin
    set @Error = 1
    goto Result
  end
  
  exec @Error = p_StorageUnitBatchLocation_Allocate
   @InstructionId = @instructionId,
   @Store         = 0
  
  if @Error <> 0
  begin
    set @Error = 1
    goto Result
  end
  
  result:
    update MobileLog
       set EndDate = getdate()
     where MobileLogId = @MobileLogId
    
    select @Error
    return @Error
end
