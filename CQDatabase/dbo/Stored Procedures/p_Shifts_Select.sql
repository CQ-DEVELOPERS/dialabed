﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shifts_Select
  ///   Filename       : p_Shifts_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:48
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Shifts table.
  /// </remarks>
  /// <param>
  ///   @ShiftId int = null 
  /// </param>
  /// <returns>
  ///   Shifts.ShiftId,
  ///   Shifts.Starttime,
  ///   Shifts.EndTime 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shifts_Select
(
 @ShiftId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Shifts.ShiftId
        ,Shifts.Starttime
        ,Shifts.EndTime
    from Shifts
   where isnull(Shifts.ShiftId,'0')  = isnull(@ShiftId, isnull(Shifts.ShiftId,'0'))
  
end
