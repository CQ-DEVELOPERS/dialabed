﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_StorageUnit_Select
  ///   Filename       : p_Static_StorageUnit_Select.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 19 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@ProductId> 
  /// </param>
  /// <returns>
  ///        <StorageUnitId>
  ///        <SKUId>
  ///        <ProductId>  
  ///        <SKUCode> 
  ///        <SKU> 
  ///        <ProductCode> 
  ///        <Product>    
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_StorageUnit_Select
(
 @ProductId	            int
)
 
as
begin
	 set nocount on;

SELECT      su.StorageUnitId,
            sku.SKUId,
            p.ProductId,  
            sku.SKUCode, 
            sku.SKU, 
            p.ProductCode, 
            p.Product,
            su.ProductCategory,
            su.PackingCategory,
            isnull(su.PickEmpty,0) as 'PickEmpty',
            su.StackingCategory,
            su.MovementCategory,
            su.ValueCategory,
            su.StoringCategory,
            su.PickPartPallet,
            su.MinimumQuantity,
            su.ReorderQuantity,
            su.MaximumQuantity,
            su.UnitPrice,
            su.Size,
            su.Colour,
            su.Style,
            su.PreviousUnitPrice,
            su.StockTakeCounts,
            CASE WHEN LEN(ISNULL(su.LotAttributeRule, '')) <= 1
				THEN 'None'
				ELSE su.LotAttributeRule
			END as 'LotAttributeRule',
            isnull(su.DefaultQC, 0) as 'DefaultQC',
            isnull(su.SerialTracked, 0) as 'SerialTracked'
FROM        StorageUnit AS su INNER JOIN
                      SKU AS sku ON su.SKUId = sku.SKUId INNER JOIN
                      Product AS p ON su.ProductId = p.ProductId
                      Where p.ProductId = @ProductId
 

end
 
 
