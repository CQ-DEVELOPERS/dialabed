﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Select_By_Module
  ///   Filename       : p_Configuration_Select_By_Module.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Select_By_Module
(
 @WarehouseId int,
 @ModuleId    int
)
 
as
begin
	 set nocount on;
  
  select c.ConfigurationId,
         c.WarehouseId,
         c.ModuleId,
         m.Module,
         c.Configuration,
         c.Indicator,
         c.Value
    from Configuration c
    join Module        m on c.ModuleId = m.ModuleId
   where c.WarehouseId = @WarehouseId
     and c.ModuleId = @ModuleId
end
