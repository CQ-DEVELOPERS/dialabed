﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentDesc_List
  ///   Filename       : p_InterfaceDocumentDesc_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:34:59
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceDocumentDesc table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceDocumentDesc.DocId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentDesc_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as DocId
        ,null as 'InterfaceDocumentDesc'
  union
  select
         InterfaceDocumentDesc.DocId
        ,InterfaceDocumentDesc.DocId as 'InterfaceDocumentDesc'
    from InterfaceDocumentDesc
  
end
