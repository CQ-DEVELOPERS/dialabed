﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceSupplier_Update
  ///   Filename       : p_InterfaceSupplier_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:45
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceSupplier table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @SupplierCode nvarchar(60) = null,
  ///   @SupplierName nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null,
  ///   @Address2 nvarchar(510) = null,
  ///   @Address3 nvarchar(510) = null,
  ///   @Address4 nvarchar(510) = null,
  ///   @Modified nvarchar(20) = null,
  ///   @ContactPerson nvarchar(200) = null,
  ///   @Phone nvarchar(510) = null,
  ///   @Fax nvarchar(510) = null,
  ///   @Email nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceSupplier_Update
(
 @HostId nvarchar(60) = null,
 @SupplierCode nvarchar(60) = null,
 @SupplierName nvarchar(510) = null,
 @Address1 nvarchar(510) = null,
 @Address2 nvarchar(510) = null,
 @Address3 nvarchar(510) = null,
 @Address4 nvarchar(510) = null,
 @Modified nvarchar(20) = null,
 @ContactPerson nvarchar(200) = null,
 @Phone nvarchar(510) = null,
 @Fax nvarchar(510) = null,
 @Email nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceSupplier
     set HostId = isnull(@HostId, HostId),
         SupplierCode = isnull(@SupplierCode, SupplierCode),
         SupplierName = isnull(@SupplierName, SupplierName),
         Address1 = isnull(@Address1, Address1),
         Address2 = isnull(@Address2, Address2),
         Address3 = isnull(@Address3, Address3),
         Address4 = isnull(@Address4, Address4),
         Modified = isnull(@Modified, Modified),
         ContactPerson = isnull(@ContactPerson, ContactPerson),
         Phone = isnull(@Phone, Phone),
         Fax = isnull(@Fax, Fax),
         Email = isnull(@Email, Email),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         InsertDate = isnull(@InsertDate, InsertDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
