﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestColumn_Search
  ///   Filename       : p_ManualFileRequestColumn_Search.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 06 Feb 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestColumn_Search
    @ManualFileRequestTypeId int
 
as
begin

	select 
          ColumnName
         ,ColumnLength
         ,ColumnDescription
    from ManualFileRequestColumn
    where ManualFileRequestTypeId = @ManualFileRequestTypeId
  
end
 
 
