﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Search
  ///   Filename       : p_Configuration_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 12:12:01
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Configuration table.
  /// </remarks>
  /// <param>
  ///   @ConfigurationId int = null output,
  ///   @WarehouseId int = null output,
  ///   @ModuleId int = null,
  ///   @Configuration nvarchar(510) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Configuration.ConfigurationId,
  ///   Configuration.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Configuration.ModuleId,
  ///   Module.Module,
  ///   Configuration.Configuration,
  ///   Configuration.Indicator,
  ///   Configuration.IntegerValue,
  ///   Configuration.Value 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Search
(
 @ConfigurationId int = null output,
 @WarehouseId int = null output,
 @ModuleId int = null,
 @Configuration nvarchar(510) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ConfigurationId = '-1'
    set @ConfigurationId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ModuleId = '-1'
    set @ModuleId = null;
  
  if @Configuration = '-1'
    set @Configuration = null;
  
 
  select
         Configuration.ConfigurationId
        ,Configuration.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Configuration.ModuleId
         ,ModuleModuleId.Module as 'Module'
        ,Configuration.Configuration
        ,Configuration.Indicator
        ,Configuration.IntegerValue
        ,Configuration.Value
    from Configuration
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Configuration.WarehouseId
    left
    join Module ModuleModuleId on ModuleModuleId.ModuleId = Configuration.ModuleId
   where isnull(Configuration.ConfigurationId,'0')  = isnull(@ConfigurationId, isnull(Configuration.ConfigurationId,'0'))
     and isnull(Configuration.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Configuration.WarehouseId,'0'))
     and isnull(Configuration.ModuleId,'0')  = isnull(@ModuleId, isnull(Configuration.ModuleId,'0'))
     and isnull(Configuration.Configuration,'%')  like '%' + isnull(@Configuration, isnull(Configuration.Configuration,'%')) + '%'
  order by Configuration
  
end
 
