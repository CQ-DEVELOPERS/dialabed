﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_Flo_DP
  ///   Filename       : p_FamousBrands_Export_Flo_DP.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Export_Flo_DP
(
 @FileName varchar(30) = null output
)
 
as
begin
	 set nocount on;
  --select @FileName = 'Deliverypoint.txt'
  select @FileName = ''
  
  select distinct DeliveryPoint
    from FloDeliverypointExport
   where RecordStatus = 'N'
  order by DeliveryPoint
  
  update FloDeliverypointExport
     set RecordStatus = 'Y'
   where RecordStatus = 'N'
end
