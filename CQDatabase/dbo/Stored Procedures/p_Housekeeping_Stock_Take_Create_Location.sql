﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Create_Location
  ///   Filename       : p_Housekeeping_Stock_Take_Create_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Create_Location
(
 @warehouseId int,
 @operatorId  int,
 @jobId       int,
 @locationId  int,
 @StockTakeReferenceId	int
)
 
as
begin
	 set nocount on;
	 
	 declare @TableLocation as table
	 (
	  LocationId         int,
	  StorageUnitBatchId int,
	  ActualQuantity     float
	 )
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @InstructionTypeId  int,
          @InstructionId      int,
          @StatusId           int,
          @StorageUnitBatchId int,
          @ActualQuantity     float
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('I','W')
  
  select @InstructionTypeId = InstructionTypeId
    from InstructionType
   where InstructionTypeCode = 'STL'
  
  begin transaction
  
  if @Error <> 0
    goto error
  
  insert @TableLocation
        (LocationId,
	        StorageUnitBatchId,
	        ActualQuantity)
  select l.LocationId,
	        subl.StorageUnitBatchId,
	        subl.ActualQuantity
	   from Location                    l (nolock)
	   join AreaLocation               al (nolock) on l.LocationId = al.LocationId
	   join Area                        a (nolock) on al.AreaId    = a.AreaId
	   left outer
	   join StorageUnitBatchLocation subl (nolock) on l.LocationId = subl.LocationId
	  where l.LocationId   = @locationId
	    and l.StocktakeInd = 0
	    and a.StockOnHand  = 1
	 
	 while exists(select 1 from @TableLocation)
	 begin
	   select top 1
	          @LocationId         = LocationId,
	          @StorageUnitBatchId = StorageUnitBatchId,
	          @ActualQuantity     = isnull(ActualQuantity, 0)
	     from @TableLocation
	   
	   delete @TableLocation
	    where LocationId                   = @LocationId
	      and isnull(StorageUnitBatchId,0) = isnull(@StorageUnitBatchId,0)
    
    exec @Error = p_Location_Update
     @locationId    = @LocationId,
     @stocktakeInd  = 1
	   
    if @Error <> 0
      goto error
	   
    exec @Error = p_Instruction_Insert
     @InstructionId       = @InstructionId output,
     @InstructionTypeId   = @InstructionTypeId,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @WarehouseId         = @warehouseId,
     @StatusId            = @StatusId,
     @JobId               = @jobId,
     @OperatorId          = @OperatorId,
     @PickLocationId      = @LocationId,
     @Quantity            = @ActualQuantity,
     @CheckQuantity       = @ActualQuantity,
     @CreateDate          = @GetDate
	   
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_StockTakeReferenceJob_Insert
       @StockTakeReferenceId	= @StockTakeReferenceId,
       @JobId					= @JobId 
     
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Housekeeping_Stock_Take_Create_Location'
    rollback transaction
    return @Error
end
