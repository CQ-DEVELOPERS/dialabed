﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Search
  ///   Filename       : p_Area_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2014 09:46:59
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Area table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null output,
  ///   @WarehouseId int = null,
  ///   @Area nvarchar(100) = null,
  ///   @AreaCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Area.AreaId,
  ///   Area.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Area.Area,
  ///   Area.AreaCode,
  ///   Area.StockOnHand,
  ///   Area.AreaSequence,
  ///   Area.AreaType,
  ///   Area.WarehouseCode,
  ///   Area.Replenish,
  ///   Area.MinimumShelfLife,
  ///   Area.Undefined,
  ///   Area.STEKPI,
  ///   Area.BatchTracked,
  ///   Area.StockTake,
  ///   Area.NarrowAisle,
  ///   Area.Interleaving,
  ///   Area.PandD,
  ///   Area.SmallPick,
  ///   Area.AutoLink,
  ///   Area.WarehouseCode2 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Search
(
 @AreaId int = null output,
 @WarehouseId int = null,
 @Area nvarchar(100) = null,
 @AreaCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Area = '-1'
    set @Area = null;
  
  if @AreaCode = '-1'
    set @AreaCode = null;
  
 
  select
         Area.AreaId
        ,Area.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Area.Area
        ,Area.AreaCode
        ,Area.StockOnHand
        ,Area.AreaSequence
        ,Area.AreaType
        ,Area.WarehouseCode
        ,Area.Replenish
        ,Area.MinimumShelfLife
        ,Area.Undefined
        ,Area.STEKPI
        ,Area.BatchTracked
        ,Area.StockTake
        ,Area.NarrowAisle
        ,Area.Interleaving
        ,Area.PandD
        ,Area.SmallPick
        ,Area.AutoLink
        ,Area.WarehouseCode2
    from Area
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Area.WarehouseId
   where isnull(Area.AreaId,'0')  = isnull(@AreaId, isnull(Area.AreaId,'0'))
     and isnull(Area.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Area.WarehouseId,'0'))
     and isnull(Area.Area,'%')  like '%' + isnull(@Area, isnull(Area.Area,'%')) + '%'
     and isnull(Area.AreaCode,'%')  like '%' + isnull(@AreaCode, isnull(Area.AreaCode,'%')) + '%'
  order by Area
  
end
