﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_LinkedToOperatorGroup
  ///   Filename       : p_Area_LinkedToOperatorGroup.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_LinkedToOperatorGroup
( 
@OperatorGroupId	int
)

 
as
begin
	 set nocount on;
  
  select a.AreaId,
		 a.Area,
		 a.AreaCode
    from Area a
    join AreaOperatorGroup aop (nolock) on aop.AreaId = a.AreaId
    where aop.OperatorGroupId = @OperatorGroupId
	order by Area
end
