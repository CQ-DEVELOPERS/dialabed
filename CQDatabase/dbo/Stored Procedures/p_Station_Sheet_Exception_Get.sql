﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Station_Sheet_Exception_Get
  ///   Filename       : p_Station_Sheet_Exception_Get.sql
  ///   Create By      : Karen
  ///   Date Created   : 16 March 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Station_Sheet_Exception_Get
(
 @ReceiptLineId	        nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as
  table(ExceptionId int,
		ReceiptLineId int,
		ReasonId int,
		Reason nvarchar(50),
		Exception nvarchar(255),
		Quantity float
       );
  
 
  insert @TableResult
        (ExceptionId,
         ReceiptLineId,
		 ReasonId,
		 Reason,
		 Exception,
		 Quantity)
  select ex.ExceptionId,
		 ex.ReceiptLineId,
		 ex.ReasonId,
		 r.Reason,
		 ex.Exception,
		 ex.Quantity
    from Exception     ex  (nolock)
    left join Reason         r  (nolock) on ex.ReasonId	      = r.ReasonId
   where ex.ReceiptLineId    = @ReceiptLineId


  select ExceptionId,
		 ReceiptLineId,
		 ReasonId,
		 Reason,
		 Exception,
		 Quantity
    from @TableResult
end
