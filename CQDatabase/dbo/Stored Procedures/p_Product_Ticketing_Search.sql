﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Ticketing_Search
  ///   Filename       : p_Product_Ticketing_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 May 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Ticketing_Search
(
 @WarehouseId  int,
 @ProductCode  nvarchar(30) = null,
 @Product      nvarchar(50) = null,
 @SKUCode      nvarchar(50) = null,
 @SKU          nvarchar(50) = null,
 @PackType     nvarchar(50) = null
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
  (
   StorageUnitId                   int              ,
   ProductCode                     nvarchar(60)     ,
   Product                         nvarchar(510)    ,
   SKUCode                         nvarchar(100)    ,
   SKU                             nvarchar(100)    ,
   PackType                        nvarchar(60)     ,
   Barcode                         nvarchar(100)    ,
   Quantity                        float            ,
   UnitPrice                       numeric(13,2)     
  )
  
  if @ProductCode in ('-1','')
    set @ProductCode = null;
  
  if @Product in ('-1','')
    set @Product = null;
  
  if @SKUCode in ('-1','')
    set @SKUCode = null;
  
  if @SKU in ('-1','')
    set @SKU = null;
  
  insert @TableResult
        (StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         PackType,
         Barcode,
         Quantity,
         UnitPrice)
  select su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         pt.PackType,
         pk.Barcode,
         pk.Quantity,
         su.UnitPrice
    from StorageUnit          su (nolock)
    join Product               p (nolock) on su.ProductId     = p.ProductId
                                         and isnull(ProductCode,'%') like '%' + isnull(@ProductCode, ProductCode) + '%'
                                         and isnull(Product,'%')     like '%' + isnull(@Product, Product) + '%'
    join SKU                 sku (nolock) on su.SKUId         = sku.SKUId
                                         and isnull(SKUCode,'%')   like '%' + isnull(@SKUCode, SKUCode) + '%'
                                         and isnull(SKU,'%')       like '%' + isnull(@SKU, SKU) + '%'
    left
    join Pack                 pk (nolock) on su.StorageUnitId = pk.StorageUnitId
                                         and pk.WarehouseId   = @WarehouseId
    left
    join PackType             pt (nolock) on pk.PackTypeId    = pt.PackTypeId
                                         and isnull(pt.PackType,'%') like '%' + isnull(@PackType, pt.PackType) + '%'
                                         and pt.OutboundSequence = 1
  
  insert @TableResult
        (StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         PackType,
         Barcode,
         Quantity,
         UnitPrice)
  select StorageUnitId*-1,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         PackType,
         Barcode,
         Quantity,
         null
     from @TableResult
   
   select StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         PackType,
         Barcode,
         Quantity,
         UnitPrice
     from @TableResult
   order by ProductCode, SKUCode, PackType
end
