﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Pack_Select
  ///   Filename       : p_Static_Pack_Select
  ///   Create By      : Junaid Desai
  ///   Date Created   : 19 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@ProductId> 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Pack_Select
(
 @StorageUnitId           int,
 @WareHouseId			  int
)
 
as
begin
	 set nocount on;

SELECT     p.PackId, 
            pt.PackTypeId, 
            pt.PackType, 
            p.Quantity, 
            p.Barcode, 
            p.Length, 
            p.Width, 
            p.Height, 
            p.Volume, 
            p.Weight,
            p.NettWeight,
			p.TareWeight
FROM         Pack AS p INNER JOIN
                      PackType AS pt ON p.PackTypeId = pt.PackTypeId
                      Where p.StorageUnitId = @StorageUnitId
						And p.WarehouseId = @WareHouseId
 
end
