﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Get_Expiry
  ///   Filename       : p_Mobile_Get_Expiry.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Feb 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Get_Expiry
(
 @ReceiptLineId int
)
 
as
begin
	 set nocount on;
	 
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  select DATEADD(dd, isnull(p.ShelflifeDays,365), @Getdate)
    from ReceiptLine       rl (nolock)
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
   where rl.ReceiptLineId = @ReceiptLineId
end
