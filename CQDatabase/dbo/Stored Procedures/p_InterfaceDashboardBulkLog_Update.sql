﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDashboardBulkLog_Update
  ///   Filename       : p_InterfaceDashboardBulkLog_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:54
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceDashboardBulkLog table.
  /// </remarks>
  /// <param>
  ///   @FileKeyId int = null,
  ///   @FileType nvarchar(200) = null,
  ///   @ProcessedDate datetime = null,
  ///   @FileName nvarchar(100) = null,
  ///   @ErrorCode char(5) = null,
  ///   @ErrorDescription nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDashboardBulkLog_Update
(
 @FileKeyId int = null,
 @FileType nvarchar(200) = null,
 @ProcessedDate datetime = null,
 @FileName nvarchar(100) = null,
 @ErrorCode char(5) = null,
 @ErrorDescription nvarchar(510) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceDashboardBulkLog
     set FileKeyId = isnull(@FileKeyId, FileKeyId),
         FileType = isnull(@FileType, FileType),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         FileName = isnull(@FileName, FileName),
         ErrorCode = isnull(@ErrorCode, ErrorCode),
         ErrorDescription = isnull(@ErrorDescription, ErrorDescription) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
