﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Create
  ///   Filename       : p_Operator_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2013 11:51:27
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Operator table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null output,
  ///   @OperatorGroupId int = null,
  ///   @WarehouseId int = null,
  ///   @Operator nvarchar(100) = null,
  ///   @OperatorCode nvarchar(20) = null,
  ///   @Password nvarchar(20) = null,
  ///   @NewPassword nvarchar(100) = null,
  ///   @ActiveIndicator bit = null,
  ///   @ExpiryDate datetime = null,
  ///   @LastInstruction datetime = null,
  ///   @Printer nvarchar(100) = null,
  ///   @Port nvarchar(100) = null,
  ///   @IPAddress nvarchar(100) = null,
  ///   @CultureId int = null,
  ///   @LoginTime datetime = null,
  ///   @LogoutTime datetime = null,
  ///   @LastActivity datetime = null,
  ///   @SiteType nvarchar(40) = null,
  ///   @Name nvarchar(200) = null,
  ///   @Surname nvarchar(200) = null,
  ///   @PrincipalId int = null 
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.OperatorGroupId,
  ///   Operator.WarehouseId,
  ///   Operator.Operator,
  ///   Operator.OperatorCode,
  ///   Operator.Password,
  ///   Operator.NewPassword,
  ///   Operator.ActiveIndicator,
  ///   Operator.ExpiryDate,
  ///   Operator.LastInstruction,
  ///   Operator.Printer,
  ///   Operator.Port,
  ///   Operator.IPAddress,
  ///   Operator.CultureId,
  ///   Operator.LoginTime,
  ///   Operator.LogoutTime,
  ///   Operator.LastActivity,
  ///   Operator.SiteType,
  ///   Operator.Name,
  ///   Operator.Surname,
  ///   Operator.PrincipalId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Create
(
 @OperatorId int = null output,
 @OperatorGroupId int = null,
 @WarehouseId int = null,
 @Operator nvarchar(100) = null,
 @OperatorCode nvarchar(100) = null,
 @Password nvarchar(20) = null,
 @NewPassword nvarchar(100) = null,
 @ActiveIndicator bit = null,
 @ExpiryDate datetime = null,
 @LastInstruction datetime = null,
 @Printer nvarchar(100) = null,
 @Port nvarchar(100) = null,
 @IPAddress nvarchar(100) = null,
 @CultureId int = null,
 @LoginTime datetime = null,
 @LogoutTime datetime = null,
 @LastActivity datetime = null,
 @SiteType nvarchar(40) = null,
 @Name nvarchar(200) = null,
 @Surname nvarchar(200) = null,
 @PrincipalId int = null 
)
 
as
begin
	 set nocount on;
	 
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Operator = '-1'
    set @Operator = null;
  
  if @OperatorCode = '-1'
    set @OperatorCode = null;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
	 declare @Error int = 0
	 
	 if @OperatorId is null
	   select @OperatorId = OperatorId
	     from Operator
	    where Operator = @Operator
  
  if @OperatorId is null
  begin
    insert Operator
          (OperatorGroupId,
           WarehouseId,
           Operator,
           OperatorCode,
           [Password],
           NewPassword,
           ActiveIndicator,
           ExpiryDate,
           LastInstruction,
           Printer,
           Port,
           IPAddress,
           CultureId,
           LoginTime,
           LogoutTime,
           LastActivity,
           SiteType,
           Name,
           Surname,
           PrincipalId)
    select @OperatorGroupId,
           @WarehouseId,
           @Operator,
           @OperatorCode,
           @Password,
           @NewPassword,
           @ActiveIndicator,
           @ExpiryDate,
           @LastInstruction,
           @Printer,
           @Port,
           @IPAddress,
           @CultureId,
           @LoginTime,
           @LogoutTime,
           @LastActivity,
           @SiteType,
           @Name,
           @Surname,
           @PrincipalId 
    
    select @Error = @@Error, @OperatorId = scope_identity()
  end
  
  exec p_UserOperatorInsert @userName=@Operator
  
  return @Error
  
end
