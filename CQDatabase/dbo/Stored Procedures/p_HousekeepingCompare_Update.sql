﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_HousekeepingCompare_Update
  ///   Filename       : p_HousekeepingCompare_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:41
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the HousekeepingCompare table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null,
  ///   @WarehouseCode nvarchar(20) = null,
  ///   @ComparisonDate datetime = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @WMSQuantity float = null,
  ///   @HostQuantity float = null,
  ///   @Sent bit = null,
  ///   @UnitPrice numeric(13,3) = null,
  ///   @HostDate datetime = null,
  ///   @SentDate datetime = null,
  ///   @InterfaceId int = null,
  ///   @HousekeepingCompareId int = null,
  ///   @RecordStatus char(1) = null,
  ///   @CustomField1 nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_HousekeepingCompare_Update
(
 @WarehouseId int = null,
 @WarehouseCode nvarchar(20) = null,
 @ComparisonDate datetime = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @WMSQuantity float = null,
 @HostQuantity float = null,
 @Sent bit = null,
 @UnitPrice numeric(13,3) = null,
 @HostDate datetime = null,
 @SentDate datetime = null,
 @InterfaceId int = null,
 @HousekeepingCompareId int = null,
 @RecordStatus char(1) = null,
 @CustomField1 nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
  if @HousekeepingCompareId = '-1'
    set @HousekeepingCompareId = null;
  
	 declare @Error int
 
  update HousekeepingCompare
     set WarehouseId = isnull(@WarehouseId, WarehouseId),
         WarehouseCode = isnull(@WarehouseCode, WarehouseCode),
         ComparisonDate = isnull(@ComparisonDate, ComparisonDate),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         BatchId = isnull(@BatchId, BatchId),
         WMSQuantity = isnull(@WMSQuantity, WMSQuantity),
         HostQuantity = isnull(@HostQuantity, HostQuantity),
         Sent = isnull(@Sent, Sent),
         UnitPrice = isnull(@UnitPrice, UnitPrice),
         HostDate = isnull(@HostDate, HostDate),
         SentDate = isnull(@SentDate, SentDate),
         InterfaceId = isnull(@InterfaceId, InterfaceId),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         CustomField1 = isnull(@CustomField1, CustomField1) 
   where HousekeepingCompareId = @HousekeepingCompareId
  
  select @Error = @@Error
  
  
  return @Error
  
end
