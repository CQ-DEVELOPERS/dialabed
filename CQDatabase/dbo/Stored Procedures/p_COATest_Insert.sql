﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COATest_Insert
  ///   Filename       : p_COATest_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:07
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the COATest table.
  /// </remarks>
  /// <param>
  ///   @COATestId int = null output,
  ///   @COAId int = null,
  ///   @TestId int = null,
  ///   @ResultId int = null,
  ///   @MethodId int = null,
  ///   @ResultCode nvarchar(20) = null,
  ///   @Result nvarchar(510) = null,
  ///   @Pass bit = null,
  ///   @StartRange float = null,
  ///   @EndRange float = null,
  ///   @Value sql_variant = null 
  /// </param>
  /// <returns>
  ///   COATest.COATestId,
  ///   COATest.COAId,
  ///   COATest.TestId,
  ///   COATest.ResultId,
  ///   COATest.MethodId,
  ///   COATest.ResultCode,
  ///   COATest.Result,
  ///   COATest.Pass,
  ///   COATest.StartRange,
  ///   COATest.EndRange,
  ///   COATest.Value 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COATest_Insert
(
 @COATestId int = null output,
 @COAId int = null,
 @TestId int = null,
 @ResultId int = null,
 @MethodId int = null,
 @ResultCode nvarchar(20) = null,
 @Result nvarchar(510) = null,
 @Pass bit = null,
 @StartRange float = null,
 @EndRange float = null,
 @Value sql_variant = null 
)
 
as
begin
	 set nocount on;
  
  if @COATestId = '-1'
    set @COATestId = null;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @TestId = '-1'
    set @TestId = null;
  
  if @ResultId = '-1'
    set @ResultId = null;
  
  if @MethodId = '-1'
    set @MethodId = null;
  
	 declare @Error int
 
  insert COATest
        (COAId,
         TestId,
         ResultId,
         MethodId,
         ResultCode,
         Result,
         Pass,
         StartRange,
         EndRange,
         Value)
  select @COAId,
         @TestId,
         @ResultId,
         @MethodId,
         @ResultCode,
         @Result,
         @Pass,
         @StartRange,
         @EndRange,
         @Value 
  
  select @Error = @@Error, @COATestId = scope_identity()
  
  
  return @Error
  
end
