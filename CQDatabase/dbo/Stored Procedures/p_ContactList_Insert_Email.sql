﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactList_Insert_Email
  ///   Filename       : p_ContactList_Insert_Email.sql
  ///   Create By      : Karen
  ///   Date Created   : May 2013
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ContactList table.
  /// </remarks>
  /// <param>
  ///   @ContactListId int = 0 output,
  ///   @ExternalCompanyId int = null,
  ///   @OperatorId int = null,
  ///   @ContactPerson varchar(50) = null,
  ///   @Telephone varchar(20) = null,
  ///   @Fax varchar(20) = null,
  ///   @EMail varchar(30) = null,
  ///   @RecordTypeId int = null  
  /// </param>
  /// <returns>
  ///   ContactList.ContactListId,
  ///   ContactList.ExternalCompanyId,
  ///   ContactList.ContactPerson,
  ///   ContactList.Telephone,
  ///   ContactList.Fax,
  ///   ContactList.EMail 
  ///   ContactList.RecordTypeId
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_ContactList_Insert_Email
(
 @ContactListId int = 0 output,
 @ExternalCompanyId int = null,
 @ContactPerson varchar(50) = null,
 @Telephone varchar(20) = null,
 @Fax varchar(20) = null,
 @EMail varchar(255) = null,
 @ReportTypeId int = null 
)
                               
as
begin
	 set nocount on
	 declare @Error int
  
  insert ContactList
        (ExternalCompanyId,
         ContactPerson,
         Telephone,
         Fax,
         EMail,
         ReportTypeId)
  select @ExternalCompanyId,
         @ContactPerson,
         @Telephone,
         @Fax,
         @EMail,
         @ReportTypeId 
  
  select @Error = @@Error, @ContactListId = scope_identity()
  
  --if @Error = 0
  --  exec @Error = p_ContactListHistory_Insert
  --       @ContactListId,
  --       @ExternalCompanyId,
  --       @ContactPerson,
  --       @Telephone,
  --       @Fax,
  --       @EMail,
  --       @ReportTypeId, 
  --       @CommandType = 'Insert'
  
  return @Error
  
end
