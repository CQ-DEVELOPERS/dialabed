﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_IV
  ///   Filename       : p_FamousBrands_Import_IV.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Aug 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Import_IV
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update InterfaceInvoice
     set RecordStatus  = 'Y',
         ProcessedDate = @GetDate
   where RecordStatus = 'N'
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_FamousBrands_Import_IV'
    rollback transaction
    return @Error
end
