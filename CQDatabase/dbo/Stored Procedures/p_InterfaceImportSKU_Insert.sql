﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSKU_Insert
  ///   Filename       : p_InterfaceImportSKU_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:05
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportSKU table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSKUId int = null output,
  ///   @RecordStatus char(1) = null,
  ///   @SKUCode varchar(10) = null,
  ///   @SKU varchar(50) = null,
  ///   @UnitOfMeasure varchar(50) = null,
  ///   @Quantity float = null,
  ///   @AlternatePallet int = null,
  ///   @SubstitutePallet int = null,
  ///   @Litres numeric(13,3) = null,
  ///   @HostId varchar(30) = null,
  ///   @ProcessedDate datetime = null,
  ///   @Insertdate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportSKU.InterfaceImportSKUId,
  ///   InterfaceImportSKU.RecordStatus,
  ///   InterfaceImportSKU.SKUCode,
  ///   InterfaceImportSKU.SKU,
  ///   InterfaceImportSKU.UnitOfMeasure,
  ///   InterfaceImportSKU.Quantity,
  ///   InterfaceImportSKU.AlternatePallet,
  ///   InterfaceImportSKU.SubstitutePallet,
  ///   InterfaceImportSKU.Litres,
  ///   InterfaceImportSKU.HostId,
  ///   InterfaceImportSKU.ProcessedDate,
  ///   InterfaceImportSKU.Insertdate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSKU_Insert
(
 @InterfaceImportSKUId int = null output,
 @RecordStatus char(1) = null,
 @SKUCode varchar(10) = null,
 @SKU varchar(50) = null,
 @UnitOfMeasure varchar(50) = null,
 @Quantity float = null,
 @AlternatePallet int = null,
 @SubstitutePallet int = null,
 @Litres numeric(13,3) = null,
 @HostId varchar(30) = null,
 @ProcessedDate datetime = null,
 @Insertdate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportSKUId = '-1'
    set @InterfaceImportSKUId = null;
  
	 declare @Error int
 
  insert InterfaceImportSKU
        (RecordStatus,
         SKUCode,
         SKU,
         UnitOfMeasure,
         Quantity,
         AlternatePallet,
         SubstitutePallet,
         Litres,
         HostId,
         ProcessedDate,
         Insertdate)
  select @RecordStatus,
         @SKUCode,
         @SKU,
         @UnitOfMeasure,
         @Quantity,
         @AlternatePallet,
         @SubstitutePallet,
         @Litres,
         @HostId,
         @ProcessedDate,
         isnull(@Insertdate, getdate()) 
  
  select @Error = @@Error, @InterfaceImportSKUId = scope_identity()
  
  
  return @Error
  
end
