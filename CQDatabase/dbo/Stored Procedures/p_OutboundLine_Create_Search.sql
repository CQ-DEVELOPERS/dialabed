﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_Create_Search
  ///   Filename       : p_OutboundLine_Create_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   OutboundLineId,
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_Create_Search
(
 @OutboundDocumentId int = null output
)
 
as
begin
	 set nocount on;
  
	 declare @Error    int
    
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
    
  select ol.OutboundLineId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         b.ECLNumber,
         s.Status,
         ol.Quantity
    from OutboundLine        ol  (nolock)
    join StorageUnit         su  (nolock) on ol.StorageUnitId = su.StorageUnitId
    join Status              s   (nolock) on ol.StatusId      = s.StatusId
    join Product             p   (nolock) on su.ProductId     = p.ProductId
    join SKU                 sku (nolock) on su.SKUId         = sku.SKUId
    left outer
    join Batch               b   (nolock) on ol.BatchId       = b.BatchId
  where ol.OutboundDocumentId = @OutboundDocumentId
end
