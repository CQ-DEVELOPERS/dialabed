﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Inbound_Shipment_Get
  ///   Filename       : p_Inbound_Shipment_Get.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Inbound_Shipment_Get
(
 @InboundShipmentId	    int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   ReceiptId            int,
   InboundDocumentId    int,
   InboundShipmentId    int,
   OrderNumber          nvarchar(30),
   ExternalCompanyCode  nvarchar(30),
   ExternalCompany      nvarchar(255),
   NumberOfLines        int,
   DeliveryDate         datetime,
   StatusId             int,
   Status               nvarchar(50),
   InboundDocumentType  nvarchar(50),
   LocationId           int,
   Location             nvarchar(15),
   CreateDate           datetime,
   Rating               int
  );
  
  insert @TableResult
        (InboundShipmentId,
         DeliveryDate,
         OrderNumber)
  select InboundShipmentId,
         ins.ShipmentDate,
         'No Orders Linked'
    from InboundShipment ins
  where ins.InboundShipmentId = isnull(@InboundShipmentId, ins.InboundShipmentId)
    
  select InboundShipmentId,
         ReceiptId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         StatusId,
         Status,
         InboundDocumentType,
         LocationId,
         Location,
         CreateDate,
         Rating
    from @TableResult
end
