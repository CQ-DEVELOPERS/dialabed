﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstruction_List
  ///   Filename       : p_BOMInstruction_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMInstruction table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   BOMInstruction.BOMInstructionId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMInstruction_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as BOMInstructionId
        ,null as 'BOMInstruction'
  union
  select
         BOMInstruction.BOMInstructionId
        ,BOMInstruction.BOMInstructionId as 'BOMInstruction'
    from BOMInstruction
  
end
