﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_IssueLine
  ///   Filename       : p_Location_Search_IssueLine.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_IssueLine
(
 @IssueLineId int,
 @AreaId      int
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   LocationId        int,
   Location          nvarchar(15),
   StorageUnitId     int,
   BatchId           int,
   ProductCode       nvarchar(30),
   Product           nvarchar(50),
   Batch	            nvarchar(50),
   CreateDate        datetime,
   ActualQuantity    float,
   AllocatedQuantity float,
   ReservedQuantity  float
  );
  
  declare @StorageUnitId int;
  
  select @StorageUnitId  = sub.StorageUnitId
    from IssueLine         il (nolock)
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
   where il.IssueLineId = @IssueLineId
  
  insert @TableResult
        (LocationId,
         Location,
         StorageUnitId,
         BatchId,
         ActualQuantity,
         AllocatedQuantity,
         subl.ReservedQuantity)
  select l.LocationId,
         l.Location,
         sub.StorageUnitId,
         sub.BatchId,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity
    from AreaLocation               al (nolock)
    join Location                    l (nolock) on al.LocationId          = l.LocationId
    join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
    join StorageUnitBatchLocation subl (nolock) on l.LocationId           = subl.LocationId
    join StorageUnitBatch          sub (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
   where sub.StorageUnitId = @StorageUnitId
     and al.AreaId         = @AreaId
  
  insert @TableResult
        (LocationId,
         Location)
  select l.LocationId,
         l.Location
    from StorageUnitLocation sul (nolock)
    join Location              l (nolock) on sul.LocationId = l.LocationId
    join AreaLocation         al (nolock) on l.LocationId   = al.LocationId
    join Area                  a (nolock) on al.AreaId      = a.AreaId
   where sul.StorageUnitId = @StorageUnitId
     and a.AreaId          = @AreaId
     and not exists(select 1 from @TableResult tr where l.LocationId = tr.LocationId)
  
  update tr
     set ProductCode       = p.ProductCode,
         Product           = p.Product
    from @TableResult         tr
    join StorageUnit su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product      p (nolock) on su.ProductId     = p.ProductId
  
  update tr
     set Batch      = b.Batch,
         CreateDate = b.CreateDate
    from @TableResult tr
    join Batch b (nolock) on tr.BatchId = b.BatchId
  
  select LocationId,
         Location,
         ProductCode,
         Product,
         Batch,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         ActualQuantity - ReservedQuantity as 'AvailableQuantity'
    from @TableResult
   order by CreateDate, 'AvailableQuantity', Location
end
