﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Upload_Reject
  ///   Filename       : p_Housekeeping_Stock_Take_Upload_Reject.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Upload_Reject
(
 @warehouseId    int,
 @comparisonDate datetime,
 @storageUnitId  int,
 @batchId        int,
 @warehouseCode  nvarchar(50)
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @OldWarehouseId        int
  
  set @OldWarehouseId = @WarehouseId
  
  if (select dbo.ufn_Configuration(266, @OldWarehouseId)) = 1 -- ST Upload - Combine warehouses
	   set @WarehouseId = null
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update HousekeepingCompare
     set Sent = null
   where isnull(WarehouseId, -1) = isnull(@warehouseId, -1)
     and ComparisonDate = @comparisonDate
     and StorageUnitId  = @storageUnitId
     and BatchId        = @batchId
     and isnull(WarehouseCode,'') = @warehouseCode
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Housekeeping_Stock_Take_Upload_Reject'
    rollback transaction
    return @Error
end
