﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Intergration_Mapping_Delete
  ///   Filename       : p_Intergration_Mapping_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Mar 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Intergration_Mapping_Delete
(
 @interfaceMappingColumnId int
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Intergration_Mapping_Delete',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @InterfaceDocumentTypeId int,
          @InterfaceFieldCode      nvarchar(30),
          @InterfaceField          nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @interfaceMappingColumnId = -1
    set @interfaceMappingColumnId = null
       
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  delete imc
    from InterfaceMappingColumn imc
    join InterfaceField ifd on imc.InterfaceFieldId = ifd.InterfaceFieldId
   where imc.InterfaceMappingColumnId = @InterfaceMappingColumnId
     and isnull(ifd.Mandatory,0) = 0
  
  if @Error <> 0 or @@ROWCOUNT = 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
