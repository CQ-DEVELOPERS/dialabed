﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Cancelled_Orders
  ///   Filename       : p_Report_Cancelled_Orders.sql
  ///   Create By      :Esther Mathebula
  ///   Date Created   : 24 March 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Cancelled_Orders
(
 @WarehouseId            int,
 @FromDate	             datetime,
 @ToDate	             datetime
)
 
as
begin
	 set nocount on;
  
 
    select od.OrderNumber as SalesOrderNo, 
           ec.ExternalCompanyCode as CustomerCode,
           ec.ExternalCompany as CustomerDescription,
           odl.LineNumber,
           s.Status,
           od.CreateDate         
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundLine odl (nolock) on odl.OutboundDocumentId = od.OutboundDocumentId
      where  s.Status = 'Cancelled'
       and od.CreateDate      between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('CD','D','DC','CA')
       and od.WarehouseId            = @WarehouseId
    
end
 
 
