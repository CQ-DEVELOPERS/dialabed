﻿/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Search_StorageUnitId
  ///   Filename       : p_StorageUnitBatch_Search_StorageUnitId.sql
  ///   Create By      : Edgar Blount
  ///   Date Created   : 25 Jul 2018
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@@StorageUnitId> 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_StorageUnitBatch_Search_StorageUnitId]
(
 @StorageUnitId       int,
 @StorageUnitBatchId int output
)
 
as
begin
	 set nocount on;


SELECT     @StorageUnitBatchId = sub.StorageUnitBatchId
FROM         StorageUnitBatch AS sub INNER JOIN
                      Batch AS b ON sub.BatchId = b.BatchId
                      
                      Where sub.StorageUnitId = @StorageUnitId


 select @StorageUnitBatchId
end