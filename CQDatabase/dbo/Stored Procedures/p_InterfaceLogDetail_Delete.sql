﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLogDetail_Delete
  ///   Filename       : p_InterfaceLogDetail_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:28
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceLogDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogDetailId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLogDetail_Delete
(
 @InterfaceLogDetailId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceLogDetail
     where InterfaceLogDetailId = @InterfaceLogDetailId
  
  select @Error = @@Error
  
  
  return @Error
  
end
