﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Parameter
  ///   Filename       : p_COA_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:50
  /// </summary>
  /// <remarks>
  ///   Selects rows from the COA table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   COA.COAId,
  ///   COA.COA 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as COAId
        ,'{All}' as COA
  union
  select
         COA.COAId
        ,COA.COA
    from COA
  order by COA
  
end
