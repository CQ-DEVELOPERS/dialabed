﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualData_Insert_Picking
  ///   Filename       : p_VisualData_Insert_Picking.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualData_Insert_Picking
 
as
begin
	 set nocount on;
	 
	 declare @Location         nvarchar(15),
          @Latitude         decimal(16,13),
          @Longitude        decimal(16,13),
          @LatitudeAdd      decimal(16,13),
          @LongitudeAdd     decimal(16,13),
          @CurrentLatitude  decimal(16,13),
          @CurrentLongitude decimal(16,13),
          @Direction        nvarchar(10),
          @Height           int,
          @Length           int,
          @HeightCount      int,
          @LengthCount      int,
          @PreviousLocation nvarchar(15),
          @Rowcount         int,
          @Ident            int
  
  declare @TableResult as table
  (
   Ident              int primary key identity,
   LocationId         int,
   Location           nvarchar(15),
   OutboundShipmentId int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   IssueId            int,
   JobId              int,
   Pallet             nvarchar(30),
   InstructionType    nvarchar(50),
   StatusCode         nvarchar(10),
   Scanned            datetime,
   DespatchDate       datetime,
   Latitude           decimal(16,13),
   Longitude          decimal(16,13),
   Height             int,
   [Length]           int,
   Updated            bit default 0,
   Row                int
  )
  
  declare @TableJobs as table
  (
   JobId      int,
   StatusCode nvarchar(10),
   Scanned            datetime
  )
  
  -- Get jobs first - better for performance
  insert @TableJobs
        (JobId,
         StatusCode,
         Scanned)
  select j.JobId,
         s.StatusCode,
         op.Release
    from Job                  j (nolock)
    join Status               s (nolock) on j.StatusId = s.StatusId
    join OutboundPerformance op (nolock) on j.JobId = op.JobId
   where s.StatusCode = 'RL'
  
  -- Get the rest of the details
  insert @TableResult
        (LocationId,
         JobId,
         InstructionType,
         StatusCode,
         Scanned,
         OutboundShipmentId,
         OutboundDocumentId,
         IssueId)
  select distinct
         i.PickLocationId,
         j.JobId,
         it.InstructionType,
         j.StatusCode,
         j.Scanned,
         ili.OutboundShipmentId,
         ili.OutboundDocumentId,
         ili.IssueId
    from Instruction          i (nolock)
    join InstructionType     it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join @TableJobs           j          on i.JobId = j.JobId
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
                                           or i.InstructionRefId = ili.InstructionId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set DespatchDate = i.DeliveryDate
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
  
  declare visual_cursor cursor for
   select tr.Ident,
          vl.Location,
          vl.Latitude,
          vl.Longitude,
          vl.Direction,
          vl.Height,
          vl.[Length]
  	  from	@TableResult tr
  	  join viewLocation vl on tr.LocationId = vl.LocationId
--  	 where vl.Location != 'DESPATCH'
--  	   and vl.Location = 'COLL'
	  order by	vl.Location
  
  open visual_cursor
  
  set @HeightCount = 1
  set @LengthCount = 1
  set @PreviousLocation = '-1'
  
  fetch visual_cursor into @Ident,
                           @Location,
                           @Latitude,
                           @Longitude,
                           @Direction,
                           @Height,
                           @Length
  
  while (@@fetch_status = 0)
  begin
    if @Location != isnull(@PreviousLocation, @Location)
    begin
      set @PreviousLocation = @Location
      set @CurrentLatitude  = @Latitude
      set @CurrentLongitude = @Longitude
    end
    
    if @Direction = 'North'
    begin
      set @LatitudeAdd = 0.00003
      set @LongitudeAdd = 0
    end
    if @Direction = 'East'
    begin
      set @LatitudeAdd = 0
      set @LongitudeAdd = 0.00003
    end
    if @Direction = 'South'
    begin
      set @LatitudeAdd = -0.00003
      set @LongitudeAdd = 0
    end
    if @Direction = 'West'
    begin
      set @LatitudeAdd = 0
      set @LongitudeAdd = -0.00003
    end
    
    update @TableResult
       set Latitude  = @CurrentLatitude,
           Longitude = @CurrentLongitude,
           Updated   = 1,
           Row       = isnull(@Rowcount, 0),
           [Length]  = @LengthCount,
           Height    = @HeightCount,
           Location  = @Location
     where Ident = @Ident
    
    if @HeightCount < @Height
    begin
      set @HeightCount = @HeightCount + 1
    end
    else if @LengthCount < @Length
    begin
      set @HeightCount = 1
      set @LengthCount = @LengthCount + 1
      select @CurrentLatitude  = @CurrentLatitude + @LatitudeAdd
      select @CurrentLongitude = @CurrentLongitude + @LongitudeAdd
    end
    else
    begin
      set @Rowcount = isnull(@Rowcount,0) + 1
      select @CurrentLatitude  = @Latitude + 0.00003 * @Rowcount
      select @CurrentLongitude = @Longitude
      set @HeightCount = 1
      set @LengthCount = 1
    end
    
    fetch visual_cursor into @Ident,
                             @Location,
                             @Latitude,
                             @Longitude,
                             @Direction,
                             @Height,
                             @Length
  end

  close visual_cursor
  deallocate visual_cursor
  
  delete VisualData where Key1 = 'Picking'
  
  insert VisualData
        (Key1,
         Sort1,
         Sort2,
         Sort3,
         Colour,
         Latitude,
         Longitude,
         Info)
  select 'Picking',
         Row + 1,
         Location,
         Height,
         1,
         Latitude,
         Longitude,
         '<h3>' + ExternalCompany + '</h3>' +
         '<b>Job: </b>' + convert(nvarchar(10), JobId) + 
         '<br>' + 
         '<br>' + 
         '<b>Pallet: </b>1 of 3' +
         '<br>' + 
         '<br>' + 
         '<b>Pallet Type: </b>' + InstructionType +
         '<br>' + 
         '<br>' + 
         '<b>Scanned Date: </b>' +
         convert(nvarchar(20), Scanned, 120) +
         '<br>' + 
         '<br>' +
         '<b>Despatch Date: </b>' +
         convert(nvarchar(20), DespatchDate, 120)
    from @TableResult
  where Row is not null
end
