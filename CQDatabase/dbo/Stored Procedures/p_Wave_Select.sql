﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Select
  ///   Filename       : p_Wave_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013 14:17:32
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Wave table.
  /// </remarks>
  /// <param>
  ///   @WaveId int = null 
  /// </param>
  /// <returns>
  ///   Wave.WaveId,
  ///   Wave.Wave,
  ///   Wave.CreateDate,
  ///   Wave.ReleasedDate,
  ///   Wave.StatusId,
  ///   Wave.WarehouseId,
  ///   Wave.CompleteDate,
  ///   Wave.ReleasedOrders,
  ///   Wave.PutawayDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Select
(
 @WaveId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Wave.WaveId
        ,Wave.Wave
        ,Wave.CreateDate
        ,Wave.ReleasedDate
        ,Wave.StatusId
        ,Wave.WarehouseId
        ,Wave.CompleteDate
        ,Wave.ReleasedOrders
        ,Wave.PutawayDate
    from Wave
   where isnull(Wave.WaveId,'0')  = isnull(@WaveId, isnull(Wave.WaveId,'0'))
  order by Wave
  
end
