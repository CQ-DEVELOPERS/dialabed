﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceField_List
  ///   Filename       : p_InterfaceField_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:46:48
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceField table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceField.InterfaceFieldId,
  ///   InterfaceField.InterfaceField 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceField_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceFieldId
        ,'{All}' as InterfaceField
  union
  select
         InterfaceField.InterfaceFieldId
        ,InterfaceField.InterfaceField
    from InterfaceField
  order by InterfaceField
  
end
