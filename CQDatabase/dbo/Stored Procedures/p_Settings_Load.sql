﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Settings_Load
  ///   Filename       : p_Settings_Load.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Settings_Load
(
 @operatorId  int,
 @settingCode nvarchar(100)
)
 
as
begin
  set nocount on;
  
  select Setting
    from Setting (nolock)
   where SettingCode = @settingCode
     and OperatorId  = @operatorId
end
