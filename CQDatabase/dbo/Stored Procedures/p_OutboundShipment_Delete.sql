﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipment_Delete
  ///   Filename       : p_OutboundShipment_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Oct 2013 09:31:12
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OutboundShipment table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipment_Delete
(
 @OutboundShipmentId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete OutboundShipment
     where OutboundShipmentId = @OutboundShipmentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
