﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationType_Delete
  ///   Filename       : p_LocationType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:00
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the LocationType table.
  /// </remarks>
  /// <param>
  ///   @LocationTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationType_Delete
(
 @LocationTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete LocationType
     where LocationTypeId = @LocationTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
