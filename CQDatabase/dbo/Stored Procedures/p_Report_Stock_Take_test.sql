﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_test
  ///   Filename       : p_Report_Stock_Take_test.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 21 July 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Report_Stock_Take_test]
(
 @WarehouseId		int,
 @FromDate          datetime,
 @ToDate            datetime,
 @AreaId			Int = null
)
as
begin
	 set nocount on; 

if (@AreaId = 0) or (@AreaId = -1)
Begin
 set @AreaId = null
end
	
Declare @StockTakeSummary as  Table 
		(InstructionType nvarchar(50),
		InstructionTypeCode nvarchar(25),
		 Area				nvarchar(50),
		 AreaId				int,
	 	 Date				Datetime,
		 LocationCount		Int,
		 StockTakes			Int,
		 Cancelled			Int,		
		 Waiting			int,
		 Busy				int,
		 Complete			float)

If @AreaId = -1 Set @AreaId = null

Declare @AreaLocationCount as Table  (InstructionTypeCode nvarchar(10) NOT NULL,
								AreaId int NOT NULL,
								LocationCount int NULL) 

Insert into @AreaLocationCount (InstructionTypeCode,
			AreaId,
			LocationCount)
Select 'STL',
			al.AreaId,
			Count(LocationId)  as LocationCount
from AreaLocation al
	Join Area a on a.AreaId = al.AreaId
	where Isnull(@AreaId,al.AreaId) = al.AreaId
		and a.WarehouseId = @WarehouseId
Group by al.AreaId

Insert into @AreaLocationCount (InstructionTypeCode,
			AreaId,
			LocationCount)
Select 'STE',
			al.AreaId,
			Count(LocationId)  as LocationCount
from AreaLocation al
Join Area a on a.AreaId = al.AreaId
where Isnull(@AreaId,al.AreaId) = al.AreaId
	and a.WarehouseId = @WarehouseId
Group by al.AreaId

--select * from @AreaLocationCount


Declare @TempDate Datetime

Set @TempDate = @FromDate

While (@TempDate <= @ToDate)
Begin

Insert into @StockTakeSummary

Select Distinct i.InstructionType,
		i.InstructionTypeCode,
		Area,
		c.AreaId,
		@TempDate,
		LocationCount,
		0 as StockTakes,
		0 as Cancelled,
		0 as Waiting,
		0 as Busy,
		0 as Complete
	From 	@AreaLocationCount c
			Join InstructionType i on i.InstructionTypeCode = c.InstructionTypeCode		
			Join Area a on a.Areaid = c.AreaId

	Set  @TempDate = Dateadd(dd,1,@TempDate)
End



Select vs.InstructionType,
		vs.InstructionTypeCode,
		vs.Area,
		vs.Areaid,
		vs.Status,
		vs.CreateDate,
		Count(PickLocationId) as Cnt
Into #Instruction_Stock
from view_Instruction_Stock vs
		jOIN @StockTakeSummary s 
				on s.Date = vs.CreateDate and s.InstructionType = vs.InstructionType and s.Area= vs.Area
Group by vs.InstructionType,
		vs.InstructionTypeCode,
		vs.Area,
		vs.Areaid,
		vs.Status,
		vs.CreateDate
		
Update @StockTakeSummary Set StockTakes = Cnt 
From @StockTakeSummary s
	Join #Instruction_Stock vs
		on vs.InstructionType = s.InstructionType and
			vs.AreaId = s.AreaId and
			vs.CreateDate = s.Date and
			vs.InstructionTypeCode = s.InstructionTypeCode
Where vs.Status = 'Finished'

Update @StockTakeSummary Set Waiting = Cnt 
From @StockTakeSummary s
	Join #Instruction_Stock vs
		on vs.InstructionType = s.InstructionType and
			vs.AreaId = s.AreaId and
			vs.CreateDate = s.Date and
			vs.InstructionTypeCode = s.InstructionTypeCode
Where vs.Status = 'Waiting'

Update @StockTakeSummary Set Cancelled = Cnt 
From @StockTakeSummary s
	Join #Instruction_Stock vs
		on vs.InstructionType = s.InstructionType and
			vs.AreaId = s.AreaId and
			vs.CreateDate = s.Date and
			vs.InstructionTypeCode = s.InstructionTypeCode
Where vs.Status = 'Cancelled'

Update @StockTakeSummary Set Busy = Cnt 
From @StockTakeSummary s
	Join #Instruction_Stock vs
		on vs.InstructionType = s.InstructionType and
			vs.AreaId = s.AreaId and
			vs.CreateDate = s.Date and
			vs.InstructionTypeCode = s.InstructionTypeCode
Where vs.Status = 'Started'


Update @StockTakeSummary set Complete = Round(Convert(Float,StockTakes) * 100 / Convert(Float,LocationCount),2)

Select * 
from @StockTakeSummary

end
