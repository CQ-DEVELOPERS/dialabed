﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Line_Create_InboundLine
  ///   Filename       : p_Production_Line_Create_InboundLine.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Jul 2007 19:59:21
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundLine table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null output,
  ///   @InboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null 
  /// </param>
  /// <returns>
  ///   InboundLine.InboundLineId,
  ///   InboundLine.InboundDocumentId,
  ///   InboundLine.StorageUnitId,
  ///   InboundLine.StatusId,
  ///   InboundLine.LineNumber,
  ///   InboundLine.Quantity,
  ///   InboundLine.BatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Line_Create_InboundLine
(
 @OperatorId        int,
 @InboundLineId     int = null output,
 @InboundDocumentId int = null,
 @StorageUnitId     int = null,
 @LineNumber        int = null,
 @Quantity          float = null,
 @BatchId           int = null 
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @StatusId          int
  
  if @batchId = -1
    set @batchId = null
  
  if @StatusId is null
    select @StatusId = StatusId
      from Status (nolock)
     where StatusCode = 'N'
       and Type       = 'ID'
  
  if @LineNumber is null
    set @LineNumber = isnull((select count(1) from InboundLine (nolock) where InboundDocumentId = @InboundDocumentId),1) + 1
  
  begin transaction
  
  if exists(select 1
              from InboundLine (nolock)
             where InboundDocumentId    = @InboundDocumentId
               and StorageUnitId        = @StorageUnitId
               and isnull(BatchId,'-1') = isnull(@BatchId,'-1'))
  begin
    set @Error = -1
    set @Errormsg = 'Product and batch combination already exists'
    goto error
  end
  
  exec @Error = p_InboundLine_Insert
   @InboundLineId     = @InboundLineId output,
   @InboundDocumentId = @InboundDocumentId,
   @StorageUnitId     = @StorageUnitId,
   @StatusId          = @StatusId,
   @LineNumber        = @LineNumber,
   @Quantity          = @Quantity,
   @BatchId           = @BatchId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Receiving_Create_Receipt
   @InboundDocumentId = @InboundDocumentId,
   @InboundLineId     = @InboundLineId,
   @OperatorId        = @OperatorId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
