﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackExternalCompany_List
  ///   Filename       : p_PackExternalCompany_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 21:17:05
  /// </summary>
  /// <remarks>
  ///   Selects rows from the PackExternalCompany table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   PackExternalCompany.WarehouseId,
  ///   PackExternalCompany.ExternalCompanyId,
  ///   PackExternalCompany.StorageUnitId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackExternalCompany_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as WarehouseId
        ,null as 'PackExternalCompany'
        ,'-1' as ExternalCompanyId
        ,null as 'PackExternalCompany'
        ,'-1' as StorageUnitId
        ,null as 'PackExternalCompany'
  union
  select
         PackExternalCompany.WarehouseId
        ,PackExternalCompany.WarehouseId as 'PackExternalCompany'
        ,PackExternalCompany.ExternalCompanyId
        ,PackExternalCompany.ExternalCompanyId as 'PackExternalCompany'
        ,PackExternalCompany.StorageUnitId
        ,PackExternalCompany.StorageUnitId as 'PackExternalCompany'
    from PackExternalCompany
  
end
