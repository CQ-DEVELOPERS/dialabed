﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Import_Confirmation
  ///   Filename       : p_Plascon_Import_Confirmation.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 22 Dec 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Plascon_Import_Confirmation]
(
    @SequenceNo     varchar(9) = null,
    @RecordType     varchar(3),
    @HostReference  varchar(8),
    @HostError      varchar(255)
)
--with encryption
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @InterfaceId       int,
          @InterfaceTable    varchar(128),
          @ParmDefinition    nvarchar(500),
          @Status            varchar(20),
          @SQLString         nvarchar(MAX)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  If ISNULL(@HostReference,'') = ''
  SELECT @HostReference = OrderNumber
  FROM InterfaceExtract
  WHERE replicate('0',9  - len(convert(varchar(9),SequenceNumber))) + convert(varchar(9),SequenceNumber) = @SequenceNo
  
  Select
     @InterfaceTable = 
        CASE @RecordType
            WHEN 'UCS' THEN 'InterfaceExportHeader'
            WHEN 'UCB' THEN 'InterfaceExportHeader'
            WHEN 'UCP' THEN 'InterfaceExportHeader'
            --WHEN 'UCF' THEN 'InterfaceExport'
            WHEN 'UCO' THEN 'InterfaceExportStockAdjustment'
        END
    ,@SQLString =
        CASE @RecordType
            WHEN 'UCS' THEN N'SELECT @InterfaceIdOUT = max(InterfaceExportHeaderId) 
                              FROM InterfaceExportHeader
                              WHERE PrimaryKey = @reference'
            WHEN 'UCB' THEN N'SELECT @InterfaceIdOUT = max(InterfaceExportHeaderId) 
                              FROM InterfaceExportHeader
                              WHERE PrimaryKey = @reference'
			WHEN 'UCP' THEN N'SELECT @InterfaceIdOUT = max(InterfaceExportHeaderId) 
                              FROM InterfaceExportHeader
                              WHERE PrimaryKey = @reference'
            --WHEN 'UCF' THEN 'InterfaceExport'
            WHEN 'UCO' THEN N'SELECT @InterfaceIdOUT = max(InterfaceExportStockAdjustmentId) 
                              FROM InterfaceExportStockAdjustment
                              WHERE ProductCode = @reference'
                              
        END
    ,@Status = 
        CASE WHEN ISNULL(@HostError,'') = '' THEN 'N' ELSE 'E' END
       
    SET @ParmDefinition = N'@Reference varchar(50), @InterfaceIdOut INT OUTPUT';

    EXECUTE sp_executesql @SQLString, @ParmDefinition, @reference = @HostReference, @InterfaceIdOUT=@InterfaceId OUTPUT;
  
   INSERT INTO InterfaceMessage
        ([InterfaceMessageCode]
        ,[InterfaceMessage]
        ,[InterfaceId]
        ,[InterfaceTable]
        ,[Status]
        ,[OrderNumber]
        ,[CreateDate]
        ,[ProcessedDate])
     VALUES
        (CASE WHEN @Status = 'N' THEN 'Processed' ELSE 'Failed' END
        ,@HostError
        ,@InterfaceId
        ,@InterfaceTable
        ,@Status
        ,@HostReference
        ,@GetDate
        ,@GetDate)
  
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Plascon_Import_Confirmation'
    rollback transaction
    return @Error
end
