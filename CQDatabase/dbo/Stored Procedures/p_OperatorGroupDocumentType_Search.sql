﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupDocumentType_Search
  ///   Filename       : p_OperatorGroupDocumentType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:20
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorGroupDocumentType table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorGroupDocumentType.OperatorGroupId,
  ///   OperatorGroupDocumentType.OutboundDocumentTypeId,
  ///   OperatorGroupDocumentType.PriorityId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupDocumentType_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OperatorGroupDocumentType.OperatorGroupId
        ,OperatorGroupDocumentType.OutboundDocumentTypeId
        ,OperatorGroupDocumentType.PriorityId
    from OperatorGroupDocumentType
  
end
