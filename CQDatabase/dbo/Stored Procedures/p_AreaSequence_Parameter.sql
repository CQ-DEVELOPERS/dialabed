﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaSequence_Parameter
  ///   Filename       : p_AreaSequence_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:03
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaSequence table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   AreaSequence.AreaSequenceId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaSequence_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as AreaSequenceId
        ,null as 'AreaSequence'
  union
  select
         AreaSequence.AreaSequenceId
        ,AreaSequence.AreaSequenceId as 'AreaSequence'
    from AreaSequence
  
end
