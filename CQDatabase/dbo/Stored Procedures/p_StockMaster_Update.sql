﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockMaster_Update
  ///   Filename       : p_StockMaster_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:41
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StockMaster table.
  /// </remarks>
  /// <param>
  ///   @SKUCode nvarchar(max) = null,
  ///   @ProductCode nvarchar(max) = null,
  ///   @ParentSKUCode nvarchar(max) = null,
  ///   @ParentProductCode nvarchar(max) = null,
  ///   @Product nvarchar(max) = null,
  ///   @Barcode nvarchar(max) = null,
  ///   @WarehouseCode nvarchar(max) = null,
  ///   @PackTypeId nvarchar(max) = null,
  ///   @PackType nvarchar(max) = null,
  ///   @OutboundSequence nvarchar(max) = null,
  ///   @Quantity nvarchar(max) = null,
  ///   @Weight nvarchar(max) = null,
  ///   @Volume nvarchar(max) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockMaster_Update
(
 @SKUCode nvarchar(max) = null,
 @ProductCode nvarchar(max) = null,
 @ParentSKUCode nvarchar(max) = null,
 @ParentProductCode nvarchar(max) = null,
 @Product nvarchar(max) = null,
 @Barcode nvarchar(max) = null,
 @WarehouseCode nvarchar(max) = null,
 @PackTypeId nvarchar(max) = null,
 @PackType nvarchar(max) = null,
 @OutboundSequence nvarchar(max) = null,
 @Quantity nvarchar(max) = null,
 @Weight nvarchar(max) = null,
 @Volume nvarchar(max) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  update StockMaster
     set SKUCode = isnull(@SKUCode, SKUCode),
         ProductCode = isnull(@ProductCode, ProductCode),
         ParentSKUCode = isnull(@ParentSKUCode, ParentSKUCode),
         ParentProductCode = isnull(@ParentProductCode, ParentProductCode),
         Product = isnull(@Product, Product),
         Barcode = isnull(@Barcode, Barcode),
         WarehouseCode = isnull(@WarehouseCode, WarehouseCode),
         PackTypeId = isnull(@PackTypeId, PackTypeId),
         PackType = isnull(@PackType, PackType),
         OutboundSequence = isnull(@OutboundSequence, OutboundSequence),
         Quantity = isnull(@Quantity, Quantity),
         Weight = isnull(@Weight, Weight),
         Volume = isnull(@Volume, Volume) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
