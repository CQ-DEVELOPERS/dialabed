﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMLine_Select
  ///   Filename       : p_BOMLine_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:12
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMLine table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null 
  /// </param>
  /// <returns>
  ///   BOMLine.BOMLineId,
  ///   BOMLine.BOMHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMLine_Select
(
 @BOMLineId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         BOMLine.BOMLineId
        ,BOMLine.BOMHeaderId
    from BOMLine
   where isnull(BOMLine.BOMLineId,'0')  = isnull(@BOMLineId, isnull(BOMLine.BOMLineId,'0'))
  
end
