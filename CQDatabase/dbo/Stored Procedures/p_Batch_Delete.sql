﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Delete
  ///   Filename       : p_Batch_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Sep 2014 15:23:47
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Batch table.
  /// </remarks>
  /// <param>
  ///   @BatchId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Delete
(
 @BatchId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Batch
     where BatchId = @BatchId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_BatchHistory_Insert
         @BatchId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
