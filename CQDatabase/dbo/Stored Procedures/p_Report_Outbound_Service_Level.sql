﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Outbound_Service_Level
  ///   Filename       : p_Report_Outbound_Service_Level.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Outbound_Service_Level
(
 @WarehouseId       int,
 @OrderNumber       nvarchar(30),
 @RouteId           int,
 @ExternalCompanyId int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
	 
declare @TableResult as table
  (
    OutboundShipmentId		int,
    IssueId					int,
    Delivery				int,
    OrderNumber				nvarchar(30),
    CustomerCode			nvarchar(10),
    CustomerName			nvarchar(255),
    OutboundDocumentType	nvarchar(30),
    Status					nvarchar(50),
    StatusCode				nvarchar(10),
    CreateDate				datetime,
    DeliveryDate			datetime,
    LineNumber				int,
    ProductCode				nvarchar(30),
    Product					nvarchar(50),
    SKUCode					nvarchar(50),
    QuantityOrdered			float,
    QuantityPicked			float,
    Difference				float,
    WeightPicked			float
  )
	 
	 if @RouteId = -1
	   set @RouteId = null
	 
	 if @ExternalCompanyId = -1
	   set @ExternalCompanyId = null
	 
	 if @OrderNumber is null
	   set @OrderNumber = '%'
	   
	 if @OrderNumber = ''
	   set @OrderNumber = '%'
	   
	insert @TableResult
			(OutboundShipmentId,
			IssueId,
			Delivery,
			OrderNumber,
			CustomerCode,
			CustomerName,
			OutboundDocumentType,
			Status,
			StatusCode,
			CreateDate,
			DeliveryDate,
			LineNumber,
			ProductCode,
			Product,
			SKUCode,
			QuantityOrdered)
    select osi.OutboundShipmentId,
           i.IssueId,
           i.Delivery,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           odt.OutboundDocumentType,
		   sil.Status,
           sil.StatusCode,
           od.CreateDate,
           i.DeliveryDate,
           ol.LineNumber,
           vs.ProductCode,
           vs.Product,
           vs.SKUCode,
           il.Quantity  
    from OutboundDocument      od (nolock)
    join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
                                          and ec.ExternalCompanyId      = isnull(@ExternalCompanyId, ec.ExternalCompanyId)
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join IssueLine             il (nolock) on i.IssueId                 = il.IssueId
    join Status               sil (nolock) on il.StatusId               = sil.StatusId
    join OutboundLine          ol (nolock) on od.OutboundDocumentId     = ol.OutboundDocumentId
                                 and il.OutboundLineId         = ol.OutboundLineId
    join viewStock             vs on il.StorageUnitBatchId     = vs.StorageUnitBatchId
    left outer
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
    left outer
    join Route                   r (nolock) on ec.RouteId = r.RouteId
                                           and r.RouteId  = isnull(@RouteId, r.RouteId)
    where od.OrderNumber like isnull(@OrderNumber,'%')
     and i.DeliveryDate between @FromDate and @ToDate
     
     
 --update tr
 --   set QuantityPicked = (select max(il.ConfirmedQuatity)
	--	from OutboundDocument      od (nolock)
	--	join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
	--	join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
	--	join IssueLine             il (nolock) on i.IssueId                 = il.IssueId
	--	join viewStock             vs          on il.StorageUnitBatchId     = vs.StorageUnitBatchId

 --  where od.OrderNumber = tr.OrderNumber
	--and vs.ProductCode = tr.ProductCode  
	--and i.statusid = 45)
 --from @TableResult tr  

 update tr
    set WeightPicked = ConfirmedWeight
       ,QuantityPicked = ConfirmedQuatity
 from @TableResult tr  
	inner join (select MAX(il.ConfirmedWeight) ConfirmedWeight
		 , MAX(il.ConfirmedQuatity) ConfirmedQuatity
		 , OrderNumber
		 , LineNumber
		 , ProductCode
	 from    OutboundDocument      od (nolock) 
		join OutboundLine		   ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId
		join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
		join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
		join IssueLine             il (nolock) on i.IssueId                 = il.IssueId
											  and ol.OutboundLineId = il.OutboundLineId
		join viewStock             vs          on il.StorageUnitBatchId     = vs.StorageUnitBatchId
	 where  i.statusid = 45
		and exists
			(select 1
			 from @TableResult t
			 where t.OrderNumber = od.OrderNumber
			   and t.LineNumber = ol.LineNumber)
	 group by OrderNumber
		 , LineNumber
		 , ProductCode) sub on tr.OrderNumber = sub.OrderNumber
								and tr.LineNumber = sub.LineNumber
								and tr.ProductCode = sub.ProductCode
     
update tr
    set Difference = (tr.QuantityOrdered - tr.QuantityPicked)
From @TableResult tr

end

SELECT distinct
		--OutboundShipmentId,
		--IssueId,
		Delivery,
		OrderNumber,
		CustomerCode,
		CustomerName,
		OutboundDocumentType,
		Status,
		StatusCode,
		CreateDate,
		DeliveryDate,
		LineNumber,
		ProductCode,
		Product,
		SKUCode,
		QuantityOrdered,
		QuantityPicked,
		Difference,
		WeightPicked
FROM @TableResult
order by ordernumber

