﻿create procedure [dbo].p_Interface_WebService_CO
(
 @doc2			varchar(max) output
)
--with encryption
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Interface_WebService_CO',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @doc               xml,
	         @idoc              int,
	         @InsertDate        datetime = dbo.ufn_Getdate()
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @doc = convert(xml,@doc2)
  
  EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	 
		Insert InterfaceImportCompany
		      (RecordStatus,
         InsertDate,
         ExternalCompany,
         ExternalCompanyCode,
         RecordType,
         PricingCategoryCode,
         LabelingCategoryCode,
         ContainerTypeCode,
         RouteCode,
         HostId,
         Rating,
         Backorder,
         RedeliveryIndicator,
         QualityAssuranceIndicator,
         OrderLineSequence,
         PrincipalCode,
         Address1,
         Address2,
         Address3,
         Address4)
		SELECT 'W',
		       @InsertDate,
		       *
		  FROM OPENXML (@idoc, '/root/Body/Item',1)
            WITH (
                  ExternalCompany           nvarchar(max) 'Company',
                  ExternalCompanyCode       nvarchar(max) 'CompanyCode',
                  ExternalCompanyTypeCode   nvarchar(max) 'CompanyTypeCode',
                  PricingCategoryCode       nvarchar(max) 'PricingCategoryCode',
                  LabelingCategoryCode      nvarchar(max) 'LabelingCategoryCode',
                  ContainerTypeCode         nvarchar(max) 'ContainerTypeCode',
                  RouteCode                 nvarchar(max) 'RouteCode',
                  HostId                    nvarchar(max) 'HostId',
                  Rating                    nvarchar(max) 'Rating',
                  Backorder                 nvarchar(max) 'Backorder',
                  RedeliveryIndicator       nvarchar(max) 'RedeliveryIndicator',
                  QualityAssuranceIndicator nvarchar(max) 'QualityAssuranceIndicator',
                  OrderLineSequence         nvarchar(max) 'OrderLineSequence',
                  PrincipalCode             nvarchar(max) 'PrincipalCode',
                  Address1                  nvarchar(max) 'Address1',
                  Address2                  nvarchar(max) 'Address2',
                  Address3                  nvarchar(max) 'Address3',
                  Address4                  nvarchar(max) 'Address4'
                 )
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
		
		update tr
     set ExternalCompanyTypeId = a.ExternalCompanyTypeId
    from InterfaceImportCompany tr
    join ExternalCompanyType a (nolock) on tr.RecordType = a.ExternalCompanyTypeCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating InterfaceImportCompany'
    goto error
  end
  
  update tr
     set PricingCategoryId = a.PricingCategoryId
    from InterfaceImportCompany tr
    join PricingCategory a (nolock) on tr.PricingCategoryCode = a.PricingCategoryCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating InterfaceImportCompany'
    goto error
  end
  
  update tr
     set LabelingCategoryId = a.LabelingCategoryId
    from InterfaceImportCompany tr
    join LabelingCategory a (nolock) on tr.LabelingCategoryCode = a.LabelingCategoryCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating InterfaceImportCompany'
    goto error
  end
  
  update tr
     set ContainerTypeId = a.ContainerTypeId
    from InterfaceImportCompany tr
    join ContainerType a (nolock) on tr.ContainerTypeCode = a.ContainerTypeCode
  
  select @Error =
 @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating InterfaceImportCompany'
    goto error
  end
  
  update tr
     set RouteId = a.RouteId
    from InterfaceImportCompany tr
    join Route a (nolock) on tr.RouteCode = a.RouteCode
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating InterfaceImportCompany'
    goto error
  end
  
  update ec
     set ExternalCompany = tr.ExternalCompany,
         ExternalCompanyTypeId = tr.ExternalCompanyTypeId,
         PricingCategoryId = tr.PricingCategoryId,
         LabelingCategoryId = tr.LabelingCategoryId,
         ContainerTypeId = tr.ContainerTypeId,
         RouteId = tr.RouteId,
         HostId = tr.HostId,
         Rating = tr.Rating,
         Backorder = tr.Backorder,
         RedeliveryIndicator = tr.RedeliveryIndicator,
         QualityAssuranceIndicator = tr.QualityAssuranceIndicator,
         OrderLineSequence = tr.OrderLineSequence
     from InterfaceImportCompany tr
     join ExternalCompany ec on tr.ExternalCompanyCode = ec.ExternalCompanyCode
                            and tr.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating ExternalCompany'
    goto error
  end
  
  insert ExternalCompany
        (ExternalCompany,
         ExternalCompanyCode,
         ExternalCompanyTypeId,
         PricingCategoryId,
         LabelingCategoryId,
         ContainerTypeId,
         RouteId,
         HostId,
         Rating,
         Backorder,
         RedeliveryIndicator,
         QualityAssuranceIndicator,
         OrderLineSequence)
  select distinct tr.ExternalCompany,
         tr.ExternalCompanyCode,
         tr.ExternalCompanyTypeId,
         tr.PricingCategoryId,
         tr.LabelingCategoryId,
         tr.ContainerTypeId,
         tr.RouteId,
         tr.HostId,
         tr.Rating,
         tr.Backorder,
         tr.RedeliveryIndicator,
         tr.QualityAssuranceIndicator,
         tr.OrderLineSequence
    from InterfaceImportCompany tr
    left
    join ExternalCompany ec on tr.ExternalCompanyCode = ec.ExternalCompanyCode
                           and tr.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
   where tr.InsertDate = @InsertDate
     and ec.ExternalCompanyId is null
     and tr.ExternalCompanyTypeId is not null
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error inserting into ExternalCompany'
    goto error
  end
		
		update InterfaceImportCompany
		   set RecordStatus = 'Y',
		       ProcessedDate = dbo.ufn_Getdate()
		 where RecordSTatus = 'W'
		   and InsertDate = @InsertDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_ExternalCompany_Master_Import - Error updating into ExternalCompany'
    goto error
  end
  
  result:
      set @doc2 = ''
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
End
