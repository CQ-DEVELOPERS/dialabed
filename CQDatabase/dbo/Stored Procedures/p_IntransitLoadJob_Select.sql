﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoadJob_Select
  ///   Filename       : p_IntransitLoadJob_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:36
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IntransitLoadJob table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadJobId int = null 
  /// </param>
  /// <returns>
  ///   IntransitLoadJob.IntransitLoadJobId,
  ///   IntransitLoadJob.IntransitLoadId,
  ///   IntransitLoadJob.StatusId,
  ///   IntransitLoadJob.JobId,
  ///   IntransitLoadJob.LoadedById,
  ///   IntransitLoadJob.UnLoadedById,
  ///   IntransitLoadJob.CreateDate,
  ///   IntransitLoadJob.ReceivedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoadJob_Select
(
 @IntransitLoadJobId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         IntransitLoadJob.IntransitLoadJobId
        ,IntransitLoadJob.IntransitLoadId
        ,IntransitLoadJob.StatusId
        ,IntransitLoadJob.JobId
        ,IntransitLoadJob.LoadedById
        ,IntransitLoadJob.UnLoadedById
        ,IntransitLoadJob.CreateDate
        ,IntransitLoadJob.ReceivedDate
    from IntransitLoadJob
   where isnull(IntransitLoadJob.IntransitLoadJobId,'0')  = isnull(@IntransitLoadJobId, isnull(IntransitLoadJob.IntransitLoadJobId,'0'))
  
end
