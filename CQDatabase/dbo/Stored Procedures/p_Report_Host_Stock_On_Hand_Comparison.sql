﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Host_Stock_On_Hand_Comparison
  ///   Filename       : p_Report_Host_Stock_On_Hand_Comparison.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Host_Stock_On_Hand_Comparison
(
 @WarehouseId int
)
 
as
begin
	 set nocount on;
	 
	 select hc.ComparisonDate,
	        hc.StorageUnitId,
	        hc.BatchId,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vs.Batch,
         hc.WMSQuantity,
         hc.HostQuantity,
         hc.WMSQuantity - hc.HostQuantity as 'Variance',
         convert(numeric(13,2), hc.UnitPrice * hc.WMSQuantity) as 'WMSPrice',
         convert(numeric(13,2), hc.UnitPrice * hc.HostQuantity) as 'HostPrice',
         convert(numeric(13,2), (hc.UnitPrice * hc.WMSQuantity) - (hc.UnitPrice * hc.HostQuantity)) as 'PriceVariance',
         convert(numeric(13,2), hc.UnitPrice) as 'UnitPrice'
    from HousekeepingCompare hc
    join viewStock           vs on hc.StorageUnitId = vs.StorageUnitId
                               and hc.BatchId       = vs.BatchId
   where hc.WarehouseId    = @WarehouseId
     and hc.Sent           = 0
  order by hc.WMSQuantity - hc.HostQuantity
end
