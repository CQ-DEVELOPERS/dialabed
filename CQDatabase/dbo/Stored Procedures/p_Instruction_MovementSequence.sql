﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_MovementSequence
  ///   Filename       : p_Instruction_MovementSequence.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Nov 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_MovementSequence
(
 @InstructionId int,
 @operatorId    int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @NewInstructionId   int,
          @StatusId           int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @NewStoreLoactionId int,
          @PickAreaId         int,
          @StoreAreaId        int,
          @OperatorGroupId    int,
          @WarehouseId        int,
          @StorageUnitBatchId int,
          @PalletId           int,
          @JobId              int,
          @Quantity           float,
          @Weight             float,
          @StatusCode         nvarchar(10),
          @NearLocationId     int,
          @RelativeValue      numeric(13,3),
          @NewJobId           int,
          @AutoComplete       bit,
          @PickNarrowAilse    bit = 0,
          @StoreNarrowAilse   bit = 0,
          @PriorityId         int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select @InstructionId      = i.InstructionId,
         @JobId              = i.JobId,
         @StorageUnitBatchId = i.StorageUnitBatchId,
         @PalletId           = i.PalletId,
         @Quantity           = i.Quantity,
         @Weight             = i.Weight,
         @PickLocationId     = i.PickLocationId,
         @StoreLocationId    = i.StoreLocationId,
         @WarehouseId        = i.WarehouseId,
         @AutoComplete       = i.AutoComplete,
         @PriorityId         = j.PriorityId
    from Instruction i (nolock)
    join Job         j (nolock) on i.JobId = j.JobId
   where i.InstructionId = @instructionId
  
  select @PickAreaId      = al.AreaId,
         @PickNarrowAilse = a.NarrowAisle
    from AreaLocation al (nolock)
    join Area          a (nolock) on al.AreaId = a.AreaId
   where al.LocationId = @PickLocationId
  
  select @StoreAreaId      = al.AreaId,
         @StoreNarrowAilse = a.NarrowAisle
    from AreaLocation al (nolock)
    join Area          a (nolock) on al.AreaId = a.AreaId
   where LocationId = @StoreLocationId
  
  set @OperatorGroupId = dbo.ufn_OperatorGroupId(@OperatorId)
  
  if @PickNarrowAilse = 1
  begin
    select top 1 @NearLocationId = l.LocationId,
                 @RelativeValue  = l.RelativeValue
      from Location              l (nolock)
     where l.LocationId = @PickLocationId
  end
  else if @StoreNarrowAilse = 1
  begin
    select top 1 @NearLocationId = l.LocationId,
                 @RelativeValue  = l.RelativeValue
      from Location              l (nolock)
     where l.LocationId = @StoreLocationId
  end
  else
  begin
    select top 1 @NearLocationId = l.LocationId,
                 @RelativeValue  = l.RelativeValue
      from Location              l (nolock)
     where l.LocationId = @StoreLocationId
  end
  
  if @RelativeValue is null
    set @RelativeValue = 0
  
  select top 1
         @NewStoreLoactionId = l.LocationId,
         @StatusCode         = ms.StatusCode
    from MovementSequence ms (nolock)
    join AreaLocation     al (nolock) on ms.StageAreaId = al.AreaId
    join Location          l (nolock) on al.LocationId  = l.LocationId
   where ms.PickAreaId      = @PickAreaId
     and ms.StoreAreaId     = @StoreAreaId
     and ms.OperatorGroupId = @OperatorGroupId
     and l.ActiveBinning = 1
     and l.ActivePicking = 1
   order by l.Used,
            case when @RelativeValue > isnull(RelativeValue,0)
                           then @RelativeValue - isnull(RelativeValue,0)
                           else isnull(RelativeValue,0) - @RelativeValue
                           end
  
  if @NewStoreLoactionId is not null
  begin
    set @StatusId = dbo.ufn_StatusId('J',ISNULL(@StatusCode, 'W'))
    
    if @StatusId is null
       select @StatusId = StatusId
         from Status
        where StatusCode = @StatusCode
    
    while @InstructionId is not null
    begin
      
      exec @Error = p_Palletised_Insert
       @WarehouseId         = @WarehouseId,
       @OperatorId          = null,
       @InstructionTypeCode = 'M',
       @StorageUnitBatchId  = @StorageUnitBatchId,
       @PickLocationId      = null,
       @StoreLocationId     = null,
       @PalletId            = @PalletId,
       @Quantity            = @Quantity,
       @Weight              = @Weight,
       @StatusId            = @StatusId,
       @InstructionId       = @NewInstructionId out,
       @InstructionRefId    = @InstructionId,
       @JobId               = @NewJobId output,
       @AutoComplete        = @AutoComplete,
       @PriorityId          = @PriorityId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto error
      end
      
      exec @Error = p_StorageUnitBatchLocation_Deallocate
       @InstructionId = @InstructionId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto error
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId = @InstructionId,
       @StoreLocationId = @NewStoreLoactionId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto error
      end
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId = @InstructionId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto error
      end
      
      exec @Error = p_StorageUnitBatchLocation_Deallocate
       @InstructionId = @NewInstructionId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto error
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId   = @NewInstructionId,
       @PickLocationId  = @NewStoreLoactionId,
       @StoreLocationId = @StoreLocationId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto error
      end
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId = @NewInstructionId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto error
      end
      
      set @InstructionId = null
      
      -- Get Same Job, Same Pallet and also create movement
      select @InstructionId      = i.InstructionId,
             @StorageUnitBatchId = i.StorageUnitBatchId,
             @Quantity           = i.Quantity,
             @Weight             = i.Weight,
             @PickLocationId     = i.PickLocationId
        from Instruction i (nolock)
        left
        join Instruction ref (nolock) on i.InstructionId = ref.InstructionRefId
       where i.JobId    = @JobId
         and (i.PalletId = @PalletId or i.AutoComplete = 1)
         and ref.InstructionId is null
    end
  end
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Instruction_MovementSequence'
    rollback transaction
    return @Error
end
