﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_ReceiptLine_Default_Delivery
  ///   Filename       : p_Receiving_ReceiptLine_Default_Delivery.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_ReceiptLine_Default_Delivery
(
 @inboundShipmentId int,
 @receiptId         int
)
 
as
begin
	 --set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @BatchId           int,
          @StatusId          int,
          @WarehouseId       int,
          @Batch             nvarchar(50)
  
  if @inboundShipmentId in (-1,0)
    set @inboundShipmentId = null
  
  if @receiptId in (-1,0)
    set @receiptId = null
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select @StatusId = dbo.ufn_StatusId('B','A')
  
  select @Batch = replace(replace(replace(convert(varchar(16), getdate(), 120),' ',''),'-',''),':','')
  
  if @inboundShipmentId is not null
    select @WarehouseId = WarehouseId
      from InboundShipment
     where InboundShipmentId = @inboundShipmentId
  else if @receiptId is not null
    select @WarehouseId = WarehouseId
      from Receipt
     where ReceiptId = @receiptId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Batch_Insert
   @BatchId      = @BatchId output,
   @StatusId     = @StatusId,
   @WarehouseId  = @WarehouseId,
   @Batch        = @Batch,
   @CreateDate   = @Getdate
  
  if @Error <> 0
    goto error
  
  if @inboundShipmentId is not null
    insert StorageUnitBatch
          (BatchId,
           StorageUnitId)
    select distinct @BatchId,
           sub.StorageUnitId
      from InboundShipmentReceipt isr (nolock)
      join ReceiptLine             rl (nolock) on isr.ReceiptId         = rl.ReceiptId
      join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      join Batch                    b (nolock) on sub.BatchId           = b.BatchId
     where isr.InboundShipmentId = @inboundShipmentId
       and b.Batch = 'Default'
       and rl.DeliveryNoteQuantity = 0
       
  else if @receiptId is not null
    insert StorageUnitBatch
          (BatchId,
           StorageUnitId)
    select distinct @BatchId,
           sub.StorageUnitId
      from ReceiptLine       rl (nolock)
      join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      join Batch              b (nolock) on sub.BatchId           = b.BatchId
     where rl.ReceiptId = @receiptId
       and b.Batch = 'Default'
       and rl.DeliveryNoteQuantity = 0
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  if @inboundShipmentId is not null
  begin
    update rl
       set DeliveryNoteQuantity = rl.RequiredQuantity,
           StorageUnitBatchId   = isnull(new.StorageUnitBatchId, rl.StorageUnitBatchId)
      from InboundShipmentReceipt isr (nolock)
      join ReceiptLine             rl (nolock) on isr.ReceiptId         = rl.ReceiptId
      join Status                   s (nolock) on rl.StatusId           = s.StatusId
      join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      left
      join StorageUnitBatch       new (nolock) on sub.StorageUnitBatchId != new.StorageUnitBatchId
                                              and sub.StorageUnitId       = new.StorageUnitId
                                              and new.BatchId             = @BatchId
     where isr.InboundShipmentId = @inboundShipmentId
       and StatusCode       not in ('R','P','F')
       and Type                  = 'R'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  else
  begin
    update rl
       set DeliveryNoteQuantity = rl.RequiredQuantity,
           StorageUnitBatchId   = isnull(new.StorageUnitBatchId, rl.StorageUnitBatchId)
      from ReceiptLine             rl
      join Status                   s (nolock) on rl.StatusId           = s.StatusId
      join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      left
      join StorageUnitBatch       new (nolock) on sub.StorageUnitBatchId != new.StorageUnitBatchId
                                              and sub.StorageUnitId       = new.StorageUnitId
                                              and new.BatchId             = @BatchId
     where rl.ReceiptId    = @receiptId
       and StatusCode not in ('R','P','F')
       and Type            = 'R'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Receiving_ReceiptLine_Default_Delivery'
    rollback transaction
    return @Error
end
