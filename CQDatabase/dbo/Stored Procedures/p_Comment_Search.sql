﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Search
  ///   Filename       : p_Comment_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2014 08:27:23
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Comment table.
  /// </remarks>
  /// <param>
  ///   @CommentId int = null output,
  ///   @Comment nvarchar(max) = null,
  ///   @OperatorId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Comment.CommentId,
  ///   Comment.Comment,
  ///   Comment.OperatorId,
  ///   Operator.Operator,
  ///   Comment.CreateDate,
  ///   Comment.ModifiedDate,
  ///   Comment.ReadDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Search
(
 @CommentId int = null output,
 @Comment nvarchar(max) = null,
 @OperatorId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @CommentId = '-1'
    set @CommentId = null;
  
  if @Comment = '-1'
    set @Comment = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
 
  select
         Comment.CommentId
        ,Comment.Comment
        ,Comment.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,Comment.CreateDate
        ,Comment.ModifiedDate
        ,Comment.ReadDate
    from Comment
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = Comment.OperatorId
   where isnull(Comment.CommentId,'0')  = isnull(@CommentId, isnull(Comment.CommentId,'0'))
     and isnull(Comment.Comment,'%')  like '%' + isnull(@Comment, isnull(Comment.Comment,'%')) + '%'
     and isnull(Comment.OperatorId,'0')  = isnull(@OperatorId, isnull(Comment.OperatorId,'0'))
  order by Comment
  
end
