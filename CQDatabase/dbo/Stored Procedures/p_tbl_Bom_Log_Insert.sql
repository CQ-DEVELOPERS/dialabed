﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_tbl_Bom_Log_Insert
  ///   Filename       : p_tbl_Bom_Log_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the tbl_Bom_Log table.
  /// </remarks>
  /// <param>
  ///   @Insertdate datetime = null,
  ///   @xmlstring varchar(max) = null 
  /// </param>
  /// <returns>
  ///   tbl_Bom_Log.Insertdate,
  ///   tbl_Bom_Log.xmlstring 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_tbl_Bom_Log_Insert
(
 @Insertdate datetime = null,
 @xmlstring varchar(max) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert tbl_Bom_Log
        (Insertdate,
         xmlstring)
  select @Insertdate,
         @xmlstring 
  
  select @Error = @@Error
  
  
  return @Error
  
end
