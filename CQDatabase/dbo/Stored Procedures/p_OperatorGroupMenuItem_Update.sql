﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItem_Update
  ///   Filename       : p_OperatorGroupMenuItem_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:24
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorGroupMenuItem table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @MenuItemId int = null,
  ///   @Access bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItem_Update
(
 @OperatorGroupId int = null,
 @MenuItemId int = null,
 @Access bit = null 
)
 
as
begin
	 set nocount on;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
	 declare @Error int
 
  update OperatorGroupMenuItem
     set Access = isnull(@Access, Access) 
   where OperatorGroupId = @OperatorGroupId
     and MenuItemId = @MenuItemId
  
  select @Error = @@Error
  
  
  return @Error
  
end
