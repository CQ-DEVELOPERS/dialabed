﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerDetail_Select
  ///   Filename       : p_ContainerDetail_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:57
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ContainerDetail table.
  /// </remarks>
  /// <param>
  ///   @ContainerDetailId int = null 
  /// </param>
  /// <returns>
  ///   ContainerDetail.ContainerDetailId,
  ///   ContainerDetail.ContainerHeaderId,
  ///   ContainerDetail.StorageUnitBatchId,
  ///   ContainerDetail.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerDetail_Select
(
 @ContainerDetailId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ContainerDetail.ContainerDetailId
        ,ContainerDetail.ContainerHeaderId
        ,ContainerDetail.StorageUnitBatchId
        ,ContainerDetail.Quantity
    from ContainerDetail
   where isnull(ContainerDetail.ContainerDetailId,'0')  = isnull(@ContainerDetailId, isnull(ContainerDetail.ContainerDetailId,'0'))
  
end
