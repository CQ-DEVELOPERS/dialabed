﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportType_Select
  ///   Filename       : p_ReportType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 15:24:51
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ReportType table.
  /// </remarks>
  /// <param>
  ///   @ReportTypeId int = null 
  /// </param>
  /// <returns>
  ///   ReportType.ReportTypeId,
  ///   ReportType.ReportType,
  ///   ReportType.ReportTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportType_Select
(
 @ReportTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ReportType.ReportTypeId
        ,ReportType.ReportType
        ,ReportType.ReportTypeCode
    from ReportType
   where isnull(ReportType.ReportTypeId,'0')  = isnull(@ReportTypeId, isnull(ReportType.ReportTypeId,'0'))
  order by ReportType
  
end
 
