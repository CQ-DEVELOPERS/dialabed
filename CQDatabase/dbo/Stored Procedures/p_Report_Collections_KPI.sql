﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Collections_KPI
  ///   Filename       : p_Report_Collections_KPI.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Apr 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Collections_KPI
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
	 
	 declare @TableDetail as table
	 (
	  InstructionId int,
	  CreateDate    datetime,
	  EndDate       datetime,
	  Orders        int,
	  Lines         int,
	  Pieces        int,
   Seconds       int
  )
	 
	 declare @TableResult as table
	 (
	  CreateDate    datetime,
	  Orders        int,
	  Lines         int,
	  Pieces        int,
	  Weight        numeric(13,3),
   TotalSeconds  int,
   Hours         nvarchar(6),
   Minutes       nvarchar(2),
   Seconds       nvarchar(2)
  )
  
  insert @TableDetail
        (InstructionId,
         CreateDate,
         EndDate,
         Orders,
         Lines,
         Pieces,
         Seconds)
  select i.InstructionId,
         i.CreateDate,
         i.EndDate,
         ili.IssueId,
         ili.IssueLineId,
         ili.ConfirmedQuantity,
         DateDiff(ss, CreateDate, EndDate)
    from IssueLineInstruction ili
    join Instruction            i on ili.InstructionId = i.InstructionId
    join OutboundDocumentType odt on ili.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where OutboundDocumentTypeCode = 'CNC'
     and i.WarehouseId            = @WarehouseId
     and i.CreateDate between @FromDate and @ToDate
  
  insert @TableResult
        (CreateDate,
         Orders,
         Lines,
         Pieces,
         TotalSeconds)
  select convert(nvarchar(10), CreateDate, 120),
	        count(distinct(Orders)),
	        count(distinct(Lines)),
	        sum(Pieces),
         sum(Seconds) / count(distinct(InstructionId))
	   from @TableDetail
	 group by convert(nvarchar(10), CreateDate, 120)
  
  update @TableResult
     set Hours   =  TotalSeconds / 3600,
         Minutes = (TotalSeconds % 3600) / 60,
         Seconds =  TotalSeconds % 60
  
  update @TableResult
     set Minutes = '0' + Minutes
   where datalength(Minutes) = 1
  
  update @TableResult
     set Seconds = '0' + Seconds
   where datalength(Seconds) = 1
  
  select CreateDate,
         Orders,
         Lines,
         Pieces,
         Weight,
         Hours + ':' + Minutes + ':' + Seconds as 'AverageTime'
    from @TableResult
end
