﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactList_Delete
  ///   Filename       : p_ContactList_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:18:14
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ContactList table.
  /// </remarks>
  /// <param>
  ///   @ContactListId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContactList_Delete
(
 @ContactListId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ContactList
     where ContactListId = @ContactListId
  
  select @Error = @@Error
  
  
  return @Error
  
end
