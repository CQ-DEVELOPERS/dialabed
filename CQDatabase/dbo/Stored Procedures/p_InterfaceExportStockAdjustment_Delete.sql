﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportStockAdjustment_Delete
  ///   Filename       : p_InterfaceExportStockAdjustment_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:15
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceExportStockAdjustment table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportStockAdjustmentId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportStockAdjustment_Delete
(
 @InterfaceExportStockAdjustmentId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceExportStockAdjustment
     where InterfaceExportStockAdjustmentId = @InterfaceExportStockAdjustmentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
