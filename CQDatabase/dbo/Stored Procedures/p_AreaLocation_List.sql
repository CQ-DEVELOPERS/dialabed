﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaLocation_List
  ///   Filename       : p_AreaLocation_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:42
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaLocation table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   AreaLocation.AreaId,
  ///   AreaLocation.LocationId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaLocation_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as AreaId
        ,null as 'AreaLocation'
        ,'-1' as LocationId
        ,null as 'AreaLocation'
  union
  select
         AreaLocation.AreaId
        ,AreaLocation.AreaId as 'AreaLocation'
        ,AreaLocation.LocationId
        ,AreaLocation.LocationId as 'AreaLocation'
    from AreaLocation
  
end
