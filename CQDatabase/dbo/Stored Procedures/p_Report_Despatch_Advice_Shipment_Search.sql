﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Advice_Shipment_Search
  ///   Filename       : p_Report_Despatch_Advice_Shipment_Search.sql
  ///   Create By      : Karen
  ///   Date Created   : March 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Advice_Shipment_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	 int,
 @OutboundDocumentTypeId int,
 @FromDate	             datetime,
 @ToDate	             datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   RouteId                   int,
   Route                     nvarchar(50),
   NumberOfLines             int,
   DeliveryDate              datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   LoadIndicator             bit,
   Releases					 int
  );
  declare @TableResult2 as table
  (
   OutboundShipmentId        int,
   Orders					 int,
   TotalJobs				 int,
   JobsStarted				 int,
   JobsChecking				 int,
   JobsChecked				 int,
   JobsDespatch				 int,
   JobsComplete				 int,
   JobsDespatchChecked		 int,
   JobsQualityAssurance		 int,
   JobsNoStock			     int,
   JobsRelease				 int,
   JobsPalletised	         int,
   JobsPause				 int,
   JobsPlanComplete			 int,
   JobsWaiting				 int,
   Releases					 int,
   DespatchLocationId		 int,
   DespatchLocation			 nvarchar(15)
  );

  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           DeliveryDate,
           LoadIndicator,
           NumberOfLines,
           Releases)
    select osi.OutboundShipmentId,
		   od.OutboundDocumentId,
           i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.DeliveryDate,
           i.LoadIndicator,
           i.NumberOfLines,
           i.Releases
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and od.CreateDate       between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('PC','RL','CK','CD','DC','C')
       and od.WarehouseId            = @WarehouseId
       and osi.OutboundShipmentId > 0
  end
  else
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           DeliveryDate,
           LoadIndicator,
           NumberOfLines,
           Releases)
    select osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.DeliveryDate,
           i.LoadIndicator,
           i.NumberOfLines,
           i.Releases
      from OutboundDocument      od  (nolock)
      join ExternalCompany       ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                 i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
     where osi.OutboundShipmentId = isnull(@OutboundShipmentId, osi.OutboundShipmentId)
       and s.Type                 = 'IS'
       and s.StatusCode           in ('PC','RL','CK','CD','DC','C')
  
      
    insert @TableResult2
          (OutboundShipmentId)
    select distinct tr.OutboundShipmentId
      from @TableResult tr  
      
    update tr2
     set orders = (select COUNT(tr.ordernumber) from @TableResult tr 
					where tr.OutboundShipmentId = tr2.OutboundShipmentId)
    from @TableResult2  tr2
    
    update tr2
     set TotalJobs = (select COUNT(distinct vi.JobId) 
						from  viewInstruction  vi
						where vi.OutboundShipmentId = tr2.OutboundShipmentId)
    from @TableResult2  tr2  
    
    update tr2
     set JobsStarted = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Started')
   from @TableResult2  tr2
   
   update tr2
     set JobsChecking = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Checking')
   from @TableResult2  tr2
   
   update tr2
     set JobsChecked = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Checked')
   from @TableResult2  tr2
   
  update tr2
     set JobsDespatch = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Despatch')
   from @TableResult2  tr2
   
   
  update tr2
     set JobsComplete = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Complete')
   from @TableResult2  tr2
   
  update tr2
     set JobsDespatchChecked = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Despatch Checked')
   from @TableResult2  tr2
   
  update tr2
     set JobsQualityAssurance = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Quality Assurance')
   from @TableResult2  tr2
 
   update tr2
     set JobsNoStock = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'No-Stock')
   from @TableResult2  tr2
   
   update tr2
     set JobsRelease = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Release')
   from @TableResult2  tr2
   
   update tr2
     set JobsPalletised = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Palletised')
   from @TableResult2  tr2
   
  update tr2
     set JobsPause = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Pause')
   from @TableResult2  tr2
   
   update tr2
     set JobsPlanComplete = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Planning Complete')
   from @TableResult2  tr2
   
   update tr2
     set JobsWaiting = (select COUNT(distinct vi.JobId)
						from viewInstruction  vi 
						where vi.OutboundShipmentId = tr2.OutboundShipmentId
						and vi.JobStatus = 'Waiting')
   from @TableResult2  tr2
   
   update tr2
     set Releases = (select sum(tr1.releases)
						from @TableResult  tr1 
						where tr1.OutboundShipmentId = tr2.OutboundShipmentId)
   from @TableResult2  tr2
   
  select *
    from @TableResult2

end
