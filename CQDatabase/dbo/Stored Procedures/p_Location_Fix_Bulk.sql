﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Fix_Bulk
  ///   Filename       : p_Location_Fix_Bulk.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Fix_Bulk
(
 @InstructionId      int,
 @StorageUnitBatchId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @StorageUnitId     int,
          @DefaultId         int,
          @BatchId           int,
          @DefaultSUB        int
  
  select @StorageUnitId = StorageUnitId,
         @BatchId       = BatchId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select @DefaultId = BatchId
    from Batch (nolock)
   where Batch = 'Default'
  
  if @DefaultId = @BatchId
    return 0;
  
  select @DefaultSUB = StorageUnitBatchId
    from StorageUnitBatch
   where StorageUnitId = @StorageUnitId
     and BatchId       = @DefaultId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reverse
   @InstructionId = @InstructionId,
   @Pick          = 0,
   @Store         = 1,
   @Confirmed     = 1
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId = @InstructionId,
   @Pick          = 0,
   @Store         = 1,
   @Confirmed     = 1
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId      = @InstructionId,
   @StorageUnitBatchId = @DefaultSUB
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId = @InstructionId,
   @Pick          = 0,
   @Store         = 1,
   @Confirmed     = 1
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Allocate
   @InstructionId = @instructionId,
   @Pick          = 0,
   @Store         = 1,
   @Confirmed     = 1
  
  if @Error <> 0
    goto error
  
  return
  
  error:
    raiserror 900000 'Error executing p_Location_Fix_Bulk'
    return @Error
end
