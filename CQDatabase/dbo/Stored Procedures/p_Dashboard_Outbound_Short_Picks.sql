﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Short_Picks
  ///   Filename       : p_Dashboard_Outbound_Short_Picks.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Short_Picks
(
 @WarehouseId int = null,
 @Summarise   bit = 0,
 @OperatorId  int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @ExternalCompanyId int,
	         @PrincipalId int
	 
	 select @ExternalCompanyId = ExternalCompanyId,
	        @PrincipalId = PrincipalId
	   from Operator (nolock)
	  where OperatorId = @OperatorId
	 
	 if @Summarise = 0
	 begin
	   select top 10
	          WarehouseId,
           ProductCode,
           Product,
           SKUCode,
           ShortQuantity,
           Orders
      from DashboardOutboundShortPicks
     where WarehouseId = @WarehouseId
	      and (ExternalCompanyId = @ExternalCompanyId or @ExternalCompanyId is null)
	      and (PrincipalId = @PrincipalId or @PrincipalId is null)
    order by ShortQuantity desc
	 end
	 else
	 begin
	   truncate table DashboardOutboundShortPicks
	   
	   insert DashboardOutboundShortPicks
	         (WarehouseId,
	          ExternalCompanyId,
	          PrincipalId,
           ProductCode,
           Product,
           SKUCode,
           ShortQuantity,
           Orders)
    select ins.WarehouseId,
           od.ExternalCompanyId,
           od.PrincipalId,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           sum(ili.Quantity - ili.ConfirmedQuantity) as 'ShortQuantity',
           count(distinct(ili.IssueId)) as 'Orders'
      from IssueLineInstruction ili
      join OutboundDocument  od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
      join Instruction      ins (nolock) on ili.InstructionId = ins.InstructionId
      join StorageUnitBatch sub (nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      join Product            p (nolock) on su.ProductId = p.ProductId
      join SKU              sku (nolock) on su.SKUId = sku.SKUId
     where ins.WarehouseId = isnull(@WarehouseId, ins.WarehouseId)
       and ins.EndDate > dateadd(dd, -5, getdate())
       and ili.Quantity > ili.ConfirmedQuantity
    group by ins.WarehouseId,
	            od.ExternalCompanyId,
	            od.PrincipalId,
             p.ProductCode,
             p.Product,
             sku.SKUCode
    order by 'ShortQuantity' desc
	 end
end
