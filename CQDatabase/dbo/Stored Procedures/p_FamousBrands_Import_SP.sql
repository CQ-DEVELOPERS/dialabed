﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_SP
  ///   Filename       : p_FamousBrands_Import_SP.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 May 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Import_SP
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  -- Still to do
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_FamousBrands_Import_SP'
    rollback transaction
    return @Error
end
