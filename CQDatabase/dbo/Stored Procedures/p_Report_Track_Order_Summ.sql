﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Track_Order_Summ
  ///   Filename       : p_Report_Track_Order_Summ.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    
  ///   Modified Date 
  ///   Details       
  /// </newpara>
*/
CREATE procedure p_Report_Track_Order_Summ
(
 @OrderNumber	nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   JobId              int,
   InstructionId      int,
   Description	      nvarchar(255),
   Statusid			  int,
   Status			  nvarchar(50),
   InsertDate		  datetime,
   ProcessDate		  datetime,
   SortDate			  datetime,
   InterfaceMessage	  nvarchar(max),
   PrimaryKey         nvarchar(50)			  	 
  )
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         SortDate,
         InterfaceMessage,
         PrimaryKey)
  select isnull(h.OrderNumber, h.primarykey) as OrderNumber,
		 'Import PO into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 isnull(h.InsertDate, h.ProcessedDate),
		 InterfaceMessage,
		 h.PrimaryKey
  From InterfaceImportPOHeader h (nolock)
  join RecordStatus           rs (nolock) on rs.StatusCode = h.RecordStatus
  left join InterfaceMessage  im (nolock) on h.InterfaceImportPOHeaderId = im.InterfaceMessageId
  where @OrderNumber =  isnull(h.OrderNumber, h.PrimaryKey)
   
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         SortDate,
         InterfaceMessage,
         PrimaryKey)
  select isnull(h.OrderNumber, h.primarykey) as OrderNumber,
		 'Import SO into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 isnull(h.InsertDate, h.ProcessedDate) as SortDate,
		 InterfaceMessage,
		 h.PrimaryKey
  From InterfaceImportSOHeader h (nolock)
  join RecordStatus           rs (nolock) on rs.StatusCode = h.RecordStatus
  left join InterfaceMessage  im (nolock) on h.InterfaceImportSOHeaderId = im.InterfaceMessageId
  where @OrderNumber  =  isnull(h.OrderNumber, h.PrimaryKey)
  
    insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         SortDate,
         InterfaceMessage,
         PrimaryKey)
  select isnull(h.OrderNumber, h.primarykey) as OrderNumber,
		 'Import into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 isnull(h.ProcessedDate, h.InsertDate) as SortDate,
		 InterfaceMessage,
		 h.PrimaryKey
  From InterfaceImportHeader h (nolock)
  join RecordStatus           rs (nolock) on rs.StatusCode = h.RecordStatus
  left join InterfaceMessage  im (nolock) on h.InterfaceImportHeaderId = im.InterfaceMessageId
  where @OrderNumber Like  Isnull(h.primarykey, isnull(h.OrderNumber,0)) + '%' 
  
  --insert @TableResult
  --      (OrderNumber,
  --       Description,
  --       Status,
  --       InsertDate,
  --       ProcessDate,
  --       SortDate)
  --select OrderNumber,
		-- 'Inbound Document',
		-- s1.Status,
		-- id.CreateDate,
		-- null,
		-- isnull(id.CreateDate, getdate()) as SortDate
  --From InboundDocument id
  --join Status s1 on s1.StatusId = id.StatusId
  --where OrderNumber = @OrderNumber
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         SortDate)
  select OrderNumber,
		 idt.InboundDocumentType as Description,
		 s1.Status,
		 id.ModifiedDate,
		 null,
		 isnull(r.DeliveryDate, id.CreateDate) as SortDate
  From InboundDocument           id (nolock)
  left join Receipt               r (nolock) on id.InboundDocumentId = r.InboundDocumentId
  left join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId 
  join Status                    s1 (nolock) on s1.StatusId = r.StatusId
  where OrderNumber = @OrderNumber
  
  --insert @TableResult
  --      (OrderNumber,
  --       Description,
  --       Status,
  --       InsertDate,
  --       ProcessDate,
  --       SortDate)
  --select OrderNumber,
		-- 'Outbound Document',
		-- s1.Status,
		-- od.CreateDate,
		-- null,
		-- isnull(od.CreateDate, getdate()) as SortDate
  --From OutboundDocument od
  --join Status s1 on s1.StatusId = od.StatusId
  --where OrderNumber = @OrderNumber
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         SortDate)
  select OrderNumber,
		 odt.OutboundDocumentType as Description,
		 s1.Status,
		 od.CreateDate,
		 od.ModifiedDate,
		 isnull(i.DeliveryDate, od.createdate) as SortDate
  From OutboundDocument           od (nolock)
  left join issue                  i (nolock) on od.OutboundDocumentid = i.OutboundDocumentid
  left join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  join Status                     s1 (nolock) on s1.StatusId = od.StatusId
  where OrderNumber = @OrderNumber
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         SortDate,
         PrimaryKey)
  select isnull(h.OrderNumber, h.primarykey) as OrderNumber,
		 'Export PO from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 isnull(h.ProcessedDate, h.InsertDate) as SortDate,
		 h.PrimaryKey
  From InterfaceExportPOHeader h (nolock)
  join RecordStatus           rs (nolock) on rs.StatusCode = h.RecordStatus
  where @OrderNumber =  isnull(h.OrderNumber, h.PrimaryKey)
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         SortDate,
         PrimaryKey)
  select isnull(h.OrderNumber, h.primarykey) as OrderNumber,
		 'Export SO from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 isnull(h.ProcessedDate, h.InsertDate) as SortDate,
		 PrimaryKey
  From InterfaceExportSOHeader h (nolock)
  join RecordStatus           rs (nolock) on rs.StatusCode = h.RecordStatus
  where @OrderNumber =  isnull(h.OrderNumber, h.PrimaryKey)
  
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         SortDate,
         PrimaryKey)
  select isnull(h.OrderNumber, h.primarykey) as OrderNumber,
		 'Export from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 isnull(h.ProcessedDate, h.InsertDate) as SortDate,
		 PrimaryKey
  From InterfaceExportHeader h (nolock)
  join RecordStatus           rs (nolock) on rs.StatusCode = h.RecordStatus
  where @OrderNumber = h.primarykey
   
  select distinct OrderNumber,
		 Description,
	     Status,
	     InsertDate,
		 ProcessDate,
		 InterfaceMessage,
		 SortDate,
		 PrimaryKey
    from @TableResult
  order by SortDate
end
