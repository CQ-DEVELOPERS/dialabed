﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DangerousGoods_Delete
  ///   Filename       : p_DangerousGoods_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:30
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the DangerousGoods table.
  /// </remarks>
  /// <param>
  ///   @DangerousGoodsId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DangerousGoods_Delete
(
 @DangerousGoodsId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete DangerousGoods
     where DangerousGoodsId = @DangerousGoodsId
  
  select @Error = @@Error
  
  
  return @Error
  
end
