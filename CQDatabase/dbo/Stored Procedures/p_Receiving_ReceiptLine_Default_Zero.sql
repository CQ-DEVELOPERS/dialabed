﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_ReceiptLine_Default_Zero
  ///   Filename       : p_Receiving_ReceiptLine_Default_Zero.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_ReceiptLine_Default_Zero
(
 @inboundShipmentId int,
 @receiptId         int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500)
  
  if @inboundShipmentId in (-1,0)
    set @inboundShipmentId = null
  
  if @receiptId in (-1,0)
    set @receiptId = null
  
  begin transaction
  
  if @inboundShipmentId is not null
  begin
    update rl
       set DeliveryNoteQuantity = 0,
           AcceptedQuantity     = 0,
           StatusId             = dbo.ufn_StatusId('R','R')
      from InboundShipmentReceipt isr (nolock)
      join ReceiptLine             rl          on isr.ReceiptId = rl.ReceiptId
      join Status                   s (nolock) on rl.StatusId         = s.StatusId
     where isr.InboundShipmentId = @inboundShipmentId
       and StatusCode       not in ('R','P','F')
       and Type                  = 'R'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  else
  begin
    update rl
       set DeliveryNoteQuantity = 0,
           AcceptedQuantity     = 0,
           StatusId             = dbo.ufn_StatusId('R','R')
      from ReceiptLine rl
      join Status       s (nolock) on rl.StatusId         = s.StatusId
     where rl.ReceiptId    = @receiptId
       and StatusCode not in ('R','P','F')
       and Type            = 'R'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Receiving_ReceiptLine_Default_Zero'
    rollback transaction
    return @Error
end
