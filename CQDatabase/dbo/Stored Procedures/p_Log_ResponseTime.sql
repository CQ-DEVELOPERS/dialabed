﻿
create PROCEDURE [dbo].[p_Log_ResponseTime]
(
	@ResponseTimeId INT = NULL OUTPUT,
	@MethodName NVARCHAR(100) = NULL,
	@ResponseTime NVARCHAR(100) = NULL,
	@KeyName NVARCHAR(100) = NULL,
	@KeyId INT = NULL,
	@OperatorId INT = NULL
)
AS
BEGIN
SET NOCOUNT ON;

	DECLARE @Error INT,
			@GetDate DATETIME;

	SELECT @GetDate = dbo.ufn_Getdate()

	INSERT ResponseTime
		(MethodName,
		ResponseTime,
		CreateDate,
		KeyName,
		KeyId,
		OperatorId)
	SELECT @MethodName,
		@ResponseTime,
		@GetDate,
		@KeyName,
		@KeyId,
		@OperatorId

	SELECT @Error = @@Error, @ResponseTimeId = scope_identity()
	RETURN @Error
END
