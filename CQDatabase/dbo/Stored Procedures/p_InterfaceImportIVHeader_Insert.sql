﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIVHeader_Insert
  ///   Filename       : p_InterfaceImportIVHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:37
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportIVHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIVHeaderId int = null output,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @InvoiceNumber nvarchar(60) = null,
  ///   @RecordType nvarchar(60) = null,
  ///   @RecordStatus char(1) = null,
  ///   @CustomerCode nvarchar(60) = null,
  ///   @Customer nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @Route nvarchar(100) = null,
  ///   @DeliveryDate nvarchar(40) = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @VatPercentage decimal(13,3) = null,
  ///   @VatSummary decimal(13,3) = null,
  ///   @Total decimal(13,3) = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportIVHeader.InterfaceImportIVHeaderId,
  ///   InterfaceImportIVHeader.PrimaryKey,
  ///   InterfaceImportIVHeader.OrderNumber,
  ///   InterfaceImportIVHeader.InvoiceNumber,
  ///   InterfaceImportIVHeader.RecordType,
  ///   InterfaceImportIVHeader.RecordStatus,
  ///   InterfaceImportIVHeader.CustomerCode,
  ///   InterfaceImportIVHeader.Customer,
  ///   InterfaceImportIVHeader.Address,
  ///   InterfaceImportIVHeader.FromWarehouseCode,
  ///   InterfaceImportIVHeader.ToWarehouseCode,
  ///   InterfaceImportIVHeader.Route,
  ///   InterfaceImportIVHeader.DeliveryDate,
  ///   InterfaceImportIVHeader.Remarks,
  ///   InterfaceImportIVHeader.NumberOfLines,
  ///   InterfaceImportIVHeader.VatPercentage,
  ///   InterfaceImportIVHeader.VatSummary,
  ///   InterfaceImportIVHeader.Total,
  ///   InterfaceImportIVHeader.Additional1,
  ///   InterfaceImportIVHeader.Additional2,
  ///   InterfaceImportIVHeader.Additional3,
  ///   InterfaceImportIVHeader.Additional4,
  ///   InterfaceImportIVHeader.Additional5,
  ///   InterfaceImportIVHeader.Additional6,
  ///   InterfaceImportIVHeader.Additional7,
  ///   InterfaceImportIVHeader.Additional8,
  ///   InterfaceImportIVHeader.Additional9,
  ///   InterfaceImportIVHeader.Additional10,
  ///   InterfaceImportIVHeader.ProcessedDate,
  ///   InterfaceImportIVHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIVHeader_Insert
(
 @InterfaceImportIVHeaderId int = null output,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @InvoiceNumber nvarchar(60) = null,
 @RecordType nvarchar(60) = null,
 @RecordStatus char(1) = null,
 @CustomerCode nvarchar(60) = null,
 @Customer nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @Route nvarchar(100) = null,
 @DeliveryDate nvarchar(40) = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @VatPercentage decimal(13,3) = null,
 @VatSummary decimal(13,3) = null,
 @Total decimal(13,3) = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportIVHeaderId = '-1'
    set @InterfaceImportIVHeaderId = null;
  
	 declare @Error int
 
  insert InterfaceImportIVHeader
        (PrimaryKey,
         OrderNumber,
         InvoiceNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Customer,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         Route,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         VatPercentage,
         VatSummary,
         Total,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         ProcessedDate,
         InsertDate)
  select @PrimaryKey,
         @OrderNumber,
         @InvoiceNumber,
         @RecordType,
         @RecordStatus,
         @CustomerCode,
         @Customer,
         @Address,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @Route,
         @DeliveryDate,
         @Remarks,
         @NumberOfLines,
         @VatPercentage,
         @VatSummary,
         @Total,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         @ProcessedDate,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error, @InterfaceImportIVHeaderId = scope_identity()
  
  
  return @Error
  
end
