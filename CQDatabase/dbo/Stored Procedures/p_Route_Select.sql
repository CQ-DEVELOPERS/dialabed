﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Route_Select
  ///   Filename       : p_Route_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:08
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Route table.
  /// </remarks>
  /// <param>
  ///   @RouteId int = null 
  /// </param>
  /// <returns>
  ///   Route.RouteId,
  ///   Route.Route,
  ///   Route.RouteCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Route_Select
(
 @RouteId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Route.RouteId
        ,Route.Route
        ,Route.RouteCode
    from Route
   where isnull(Route.RouteId,'0')  = isnull(@RouteId, isnull(Route.RouteId,'0'))
  
end
