﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Space_Utilisation
  ///   Filename       : p_Dashboard_Space_Utilisation.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Space_Utilisation
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
    select AreaId,
           Area,
           Legend,
           Value
      from DashboardSpaceUtilisation
     where WarehouseId = @WarehouseId
    order by [Area]
	 end
	 else
	 begin
	   truncate table DashboardSpaceUtilisation
	   
	   insert DashboardSpaceUtilisation
          (WarehouseId,
           [AreaId],
           Legend,
           [Value])
    select WarehouseId,
           max(a.AreaId),
           convert(nvarchar(50), l.Used),
           count(1)
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId = al.AreaId
      join Location      l (nolock) on al.LocationId = l.LocationId
      where a.WarehouseId = isnull(@WarehouseId, a.WarehouseId)
        and a.StockOnHand = 1
    group by WarehouseId,
             a.AreaCode,
             convert(nvarchar(50), l.Used)
    having count(1) > 1
    
    update tr
       set Area = a.Area
      from DashboardSpaceUtilisation tr
      join Area          a (nolock) on tr.AreaId = a.AreaId
    
    insert DashboardSpaceUtilisation
          ([AreaId],
           [Area],
           Legend,
           [Value])
    select tr1.AreaId,
           tr1.Area,
           case when tr1.Legend = '0' then '1' else '0' end,
           0
      from DashboardSpaceUtilisation tr1
      left
      join DashboardSpaceUtilisation tr2 on tr1.AreaId = tr2.AreaId
                           and tr1.Legend != tr2.Legend
     where tr2.Legend is null
    
    update DashboardSpaceUtilisation
       set Legend = case when Legend = '0'
                         then 'Free Space'
                         else 'Used'
                         end
  end
end
