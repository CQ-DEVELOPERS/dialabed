﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Search_ExternalCompany
  ///   Filename       : p_Search_ExternalCompany.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Dec 2007 11:23:58
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId int = null output,
  ///   @ExternalCompanyTypeId int = null,
  ///   @RouteId int = null,
  ///   @ExternalCompany varchar(50) = null,
  ///   @ExternalCompanyCode varchar(10) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ExternalCompany.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompanyTypeId,
  ///   ExternalCompanyType.ExternalCompanyType,
  ///   ExternalCompany.RouteId,
  ///   Route.Route,
  ///   ExternalCompany.ExternalCompany,
  ///   ExternalCompany.ExternalCompanyCode,
  ///   ExternalCompany.Rating,
  ///   ExternalCompany.RedeliveryIndicator,
  ///   ExternalCompany.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Search_ExternalCompany
(
 @ExternalCompany varchar(50) = null,
 @ExternalCompanyCode varchar(10) = null
)
 
as
begin
	 set nocount on;
  
  if @ExternalCompany = '-1'
    set @ExternalCompany = null;
  
  if @ExternalCompanyCode = '-1'
    set @ExternalCompanyCode = null;
  
  select ExternalCompanyId,
         ExternalCompany,
         ExternalCompanyCode 
    from ExternalCompany
   where isnull(ExternalCompany,'%')  like '%' + isnull(@ExternalCompany, isnull(ExternalCompany,'%')) + '%'
     and isnull(ExternalCompanyCode,'%')  like '%' + isnull(@ExternalCompanyCode, isnull(ExternalCompanyCode,'%')) + '%'
     order by ExternalCompanyid
end
