﻿create procedure [dbo].[p_Interface_WebService_SA]
(
	@doc varchar(max) output
)
--with encryption
as
begin
	Declare @doc2 xml
	set @doc2 = convert(xml,@doc)
	DECLARE @idoc int
	Declare @InsertDate datetime
	
	Set @InsertDate = Getdate()
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc2

	Insert Into InterfaceImportSOH (RecordStatus,
									InsertDate,
									Location,
									ProductCode,
									Batch,
									Quantity,
									UnitPrice)
		SELECT    'W',@InsertDate,*
		FROM       OPENXML (@idoc, '/root/StockOnHand',1)
            WITH (Location varchar(50) 'WHWhseID',
				  ProductCode  varchar(50) 'WHStockLink',
                  Batch varchar(50) 'WHLotCode',
				  Quantity varchar(50) 'WHQtyOnHand',
				  UnitPrice varchar(50) 'UnitPrice'
				  )
		update InterfaceImportSOH set Batch = null where len(batch) = 0
		Update InterfaceImportSOH set RecordStatus = 'N'
		Where RecordSTatus = 'W' and InsertDate = @InsertDate
		--exec p_Pastel_Import_APSH
		set @doc = ''
		Set @doc2 = ''
End






