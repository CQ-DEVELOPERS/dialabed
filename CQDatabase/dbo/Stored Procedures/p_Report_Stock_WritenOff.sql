﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_WritenOff
  ///   Filename       : p_Report_Stock_WritenOff.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 22 July 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_WritenOff
(
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
  
  declare @GetDate datetime
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 declare @TableResult as table
	 (
   JobId              int,
   InstructionType    nvarchar(50),
   StorageUnitBatchId int,
   Quantity           float,
   ConfirmedQuantity  float,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   CreateDate         datetime,
   StartDate          datetime,
   EndDate            datetime,
   Reason             nvarchar(50)
	 )
	 
	 
	 insert @TableResult
        (JobId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
         Quantity,
         ConfirmedQuantity,
         CreateDate,
         StartDate,
         Reason)
  SELECT     i.JobId, 
            it.InstructionType, 
            i.StorageUnitBatchId, 
            i.PickLocationId, 
            i.StoreLocationId, 
            i.Quantity, 
            i.ConfirmedQuantity, 
            i.CreateDate, 
            i.StartDate, 
            r.Reason
FROM         Instruction AS i WITH (nolock) INNER JOIN
                      InstructionType AS it WITH (nolock) ON i.InstructionTypeId = it.InstructionTypeId LEFT OUTER JOIN
                      Reason AS r ON i.ReasonId = r.ReasonId
WHERE     (it.InstructionTypeCode = 'O')
     and i.CreateDate   between @FromDate and @ToDate
  
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
  
  
  select tr.Jobid,
         tr.InstructionType,
         tr.PickLocation,
         tr.StoreLocation,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         tr.Quantity,
         tr.ConfirmedQuantity,
         tr.CreateDate,
         tr.StartDate,
         tr.Reason
    from @TableResult    tr
    join viewStock       vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
  order by tr.CreateDate
end
