﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Method_Select
  ///   Filename       : p_Method_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:05
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Method table.
  /// </remarks>
  /// <param>
  ///   @MethodId int = null 
  /// </param>
  /// <returns>
  ///   Method.MethodId,
  ///   Method.MethodCode,
  ///   Method.Method 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Method_Select
(
 @MethodId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Method.MethodId
        ,Method.MethodCode
        ,Method.Method
    from Method
   where isnull(Method.MethodId,'0')  = isnull(@MethodId, isnull(Method.MethodId,'0'))
  order by MethodCode
  
end
