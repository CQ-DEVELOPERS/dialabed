﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Parameter
  ///   Filename       : p_Wave_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013 14:17:32
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Wave table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Wave.WaveId,
  ///   Wave.Wave 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as WaveId
        ,'{All}' as Wave
  union
  select
         Wave.WaveId
        ,Wave.Wave
    from Wave
  order by Wave
  
end
