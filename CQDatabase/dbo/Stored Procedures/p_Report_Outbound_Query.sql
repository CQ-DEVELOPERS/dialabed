﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Outbound_Query
  ///   Filename       : p_Report_Outbound_Query.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Outbound_Query
(
 @OutboundShipmentId int,
 @IssueId            int,
 @Barcode            nvarchar(30),
 @FromDate           datetime,
 @ToDate             datetime,
 @Summary            varchar(10) = 'false'
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   DespatchDate       datetime,
   LocationId         int,
   Location           nvarchar(15),
   JobId              int,
   StatusId           int,
   Status             nvarchar(50),
   JobStatusId        int,
   JobStatusCode      nvarchar(10),
   InstructionTypeId  int,
   InstructionTypeCode nvarchar(10),
   InstructionType    nvarchar(50),
   InstructionId      int,
   Pallet             nvarchar(10),
   StorageUnitBatchId int,
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   DropSequence       int,
   RouteId            int,
   Route              nvarchar(50),
   Weight             float,
   Loaded             char(1),
   ReferenceNumber	  nvarchar(30),
   NettWeight	  	  float,
   GrossWeight		  float,
   HeaderStatusId     int,
   HeaderStatus       nvarchar(50),
   JobStatus          nvarchar(50)
  )
  
  declare @JobId int
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
	 
	 if @OutboundShipmentId is not null
	   set @IssueId = null
  
  if @OutboundShipmentId is not null or @IssueId is not null
  begin
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           JobId,
           StatusId,
           JobStatusId,
           InstructionTypeId,
           InstructionId,
           StorageUnitBatchId,
           Quantity,
           ConfirmedQuantity,
           ShortQuantity,
           ReferenceNumber,
           Weight,
           Pallet,
           DropSequence)
    select ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ins.JobId,
           ins.StatusId,
           j.StatusId,
           ins.InstructionTypeId,
           ins.InstructionId,
           ins.StorageUnitBatchId,
           ins.Quantity,
           isnull(ili.ConfirmedQuantity, 0),
           ili.Quantity - isnull(ili.ConfirmedQuantity, 0),
           isnull(j.ReferenceNumber, case when it.InstructionTypeCode = 'P'
                                          then convert(nvarchar(30), ins.PalletId)
                                          else 'J:' + convert(nvarchar(30), j.JobId)
                                          end),
           isnull(ins.ConfirmedWeight,0),
           convert(nvarchar(10), j.DropSequence) + ' of ' + convert(nvarchar(10), j.Pallets),
           j.DropSequence
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId     = ins.InstructionId
      join InstructionType        it (nolock) on ins.InstructionTypeId = it.InstructionTypeId
      join Job                     j (nolock) on ins.JobId             = j.JobId
     where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))
       and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
  end
  else if @Barcode != '-1'
  begin
    if @Barcode like 'J:%'
    begin
      select @Barcode = replace(@Barcode, 'J:', '')
      
      select @JobId = JobId
        from Job (nolock)
       where convert(varchar(10), JobId) = @Barcode
    end
    else if @Barcode like 'R:%'
    begin
      select @JobId = JobId
        from Job (nolock)
       where ReferenceNumber = @Barcode
    end
    else if @Barcode like 'P:%'
    begin
      select @Barcode = replace(@Barcode, 'P:', '')
      
      select @JobId = JobId
        from Instruction i (nolock)
        join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
       where convert(varchar(10), PalletId) = @Barcode
    end
    
    select @OutboundShipmentId = ili.OutboundShipmentId,
           @IssueId            = ili.IssueId
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
     where i.JobId = @JobId
    
    if @OutboundShipmentId is not null
      set @IssueId = null
    
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           JobId,
           StatusId,
           JobStatusId,
           InstructionTypeId,
           InstructionId,
           StorageUnitBatchId,
           Quantity,
           ConfirmedQuantity,
           ShortQuantity,
           ReferenceNumber,
           Weight,
           Pallet,
           DropSequence)
    select ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ins.JobId,
           ins.StatusId,
           j.StatusId,
           ins.InstructionTypeId,
           ins.InstructionId,
           ins.StorageUnitBatchId,
           ins.Quantity,
           isnull(ili.ConfirmedQuantity, 0),
           ili.Quantity - isnull(ili.ConfirmedQuantity, 0),
           isnull(j.ReferenceNumber, case when it.InstructionTypeCode = 'P'
                                          then convert(nvarchar(30), ins.PalletId)
                                          else 'J:' + convert(nvarchar(30), j.JobId)
                                          end),
           isnull(ins.ConfirmedWeight,0),
           convert(nvarchar(10), j.DropSequence) + ' of ' + convert(nvarchar(10), j.Pallets),
           j.DropSequence
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId     = ins.InstructionId
      join InstructionType        it (nolock) on ins.InstructionTypeId = it.InstructionTypeId
      join Job                     j (nolock) on ins.JobId             = j.JobId
     where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))
       and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
    --select * from @TableResult
  end
  else
  begin
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           JobId,
           StatusId,
           JobStatusId,
           InstructionTypeId,
           InstructionId,
           StorageUnitBatchId,
           Quantity,
           ConfirmedQuantity,
           ShortQuantity,
           ReferenceNumber,
           Weight,
           Pallet,
           DropSequence)
    select ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ins.JobId,
           ins.StatusId,
           j.StatusId,
           ins.InstructionTypeId,
           ins.InstructionId,
           ins.StorageUnitBatchId,
           ins.Quantity,
           isnull(ili.ConfirmedQuantity, 0),
           ili.Quantity - isnull(ili.ConfirmedQuantity, 0),
           isnull(j.ReferenceNumber, case when it.InstructionTypeCode = 'P'
                                          then convert(nvarchar(30), ins.PalletId)
                                          else 'J:' + convert(nvarchar(30), j.JobId)
                                          end),
           isnull(ins.ConfirmedWeight,0),
           convert(nvarchar(10), j.DropSequence) + ' of ' + convert(nvarchar(10), j.Pallets),
           j.DropSequence
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId     = ins.InstructionId
      join InstructionType        it (nolock) on ins.InstructionTypeId = it.InstructionTypeId
      join Job                     j (nolock) on ins.JobId             = j.JobId
     where ili.IssueId in (select IssueId from Issue i (nolock) where i.DeliveryDate between @FromDate and @ToDate)
  end
  
  update tr
     set DespatchDate   = os.ShipmentDate,
         RouteId        = os.RouteId,
         --DropSequence   = osi.DropSequence,
         LocationId     = os.LocationId,
         HeaderStatusId = os.StatusId
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
   where tr.OutboundShipmentId is not null
  
  update tr
     set DespatchDate   = i.DeliveryDate,
         RouteId        = i.RouteId,
         --DropSequence   = i.DropSequence,
         LocationId     = i.LocationId,
         HeaderStatusId = isnull(HeaderStatusId, i.StatusId)
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
   where tr.OutboundShipmentId is null
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Location = l.Location
    from @TableResult    tr
    join Location         l (nolock) on tr.LocationId = l.LocationId
    
  --declare @InboundSequence smallint,
		--        @PackTypeId	 int,
		--        @PackType   	nvarchar(30),
		--        @ShowWeight	 bit = null
		 
  --select @InboundSequence = max(InboundSequence)
  --  from PackType (nolock) 
  
  --select @PackTypeId = pt.PackTypeId
  --  from PackType pt  where pt.InboundSequence = @InboundSequence
  
  --update tr
  --   set tr.NettWeight = ((select max(isnull(i.NettWeight,(i.ConfirmedWeight)))
  --                           from StorageUnitBatch       sub (nolock) 
		--			                        join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
		--			                        --join Product                  p (nolock) on su.ProductId         = p.ProductId
		--			                        join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
		--			                        join Instruction				 i (nolock) on i.InstructionId      = tr.InstructionId
		--			                       where pk.PackTypeId = @PackTypeId
  --                            and sub.StorageUnitBatchId = tr.StorageUnitBatchId
  --                            and isnull(su.ProductCategory,'') = 'V'))                                             
  --  from @TableResult tr
  
  --update tr
  --   set tr.GrossWeight = ((select max(isnull(i.ConfirmedWeight,0))
  --                            from StorageUnitBatch       sub (nolock)
		--	                           join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
		--	                           --join Product                  p (nolock) on su.ProductId         = p.ProductId
		--	                           join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
		--	                           join Instruction				 i (nolock) on i.InstructionId      = tr.InstructionId
		--                           where pk.PackTypeId = @PackTypeId
  --                             and sub.StorageUnitBatchId = tr.StorageUnitBatchId
  --                             and isnull(su.ProductCategory,'') = 'V'))
  --  from @TableResult tr
  
  update tr
     set Weight = tr.ConfirmedQuantity * p.Weight
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
   where pt.OutboundSequence in (select MAX(OutboundSequence) from PackType)
     and tr.ConfirmedQuantity > 0
     and p.Weight > 0
  
  update tr
     set InstructionType     = 'Full',
         InstructionTypeCode = it.InstructionTypeCode
    from @TableResult tr
    join InstructionType it on tr.InstructionTypeId = it.InstructionTypeId
   where it.InstructionTypeCode = 'P'
  
  update @TableResult
     set InstructionType     = 'Mixed'
   where InstructionType is null
  
  update tr
     set HeaderStatus = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.HeaderStatusId = s.StatusId
  
  update tr
     set JobStatusCode = s.StatusCode,
         JobStatus     = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.JobStatusId = s.StatusId
    
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update @TableResult
     set Loaded = 'a'
   where JobStatusCode = 'C'
  
  update @TableResult
     set Loaded = 'r'
         --Pallet = null
   where JobStatusCode = 'NS'
  
  if (select count(1) from @TableResult) < 300
  update tr
     set Loaded = 'r'
         --Status = (select Status from Status where StatusCode = 'NS'),
         --Pallet = null
    from @TableResult tr
    left
    join @TableResult tr2 on tr.JobId = tr2.JobId and tr2.ConfirmedQuantity > 0
   where tr2.JobId is null
   --where not exists(select 1 from @TableResult tr2 where tr.JobId = tr2.JobId and ConfirmedQuantity > 0)
  
  update @TableResult
     set Loaded = 'c'
   where Loaded is null
  
  if @Summary = 'false'
    select OutboundShipmentId,
           DropSequence,
           Route,
           DespatchDate,
           ExternalCompany,
           Location,
           min(InstructionType) as 'InstructionType',
           JobId,
           OrderNumber,
           Pallet,
           sum(Weight) as 'Weight',
           Loaded,
           ReferenceNumber,
           sum(NettWeight) as NettWeight,
           HeaderStatus,
           JobStatus
      from @TableResult
    group by OutboundShipmentId,
           DropSequence,
           Route,
           DespatchDate,
           ExternalCompany,
           Location,
           JobId,
           OrderNumber,
           Pallet,
           Loaded,
           ReferenceNumber,
           HeaderStatus,
           JobStatus
    order by OutboundShipmentId,
             DropSequence,
             JobId
  if @Summary = 'true'
    select OutboundShipmentId,
           DropSequence,
           Route,
           DespatchDate,
           null as ExternalCompany,
           Location,
           min(InstructionType) as 'InstructionType',
           JobId,
           null as 'OrderNumber',
           Pallet,
           sum(Weight) as 'Weight',
           Loaded,
           ReferenceNumber,
           sum(NettWeight) as NettWeight,
           HeaderStatus,
           JobStatus
      from @TableResult
    group by OutboundShipmentId,
           DropSequence,
           Route,
           DespatchDate,
           Location,
           JobId,
           Pallet,
           Loaded,
           ReferenceNumber,
           HeaderStatus,
           JobStatus
    order by OutboundShipmentId,
             DropSequence,
             JobId
             
    if @Summary = 'order'
    select distinct OutboundShipmentId,
           null as 'DropSequence',
           Route,
           DespatchDate,
           ExternalCompanyId,
           ExternalCompany as ExternalCompany,
           Location,
           min(InstructionType) as 'InstructionType',
           null as 'JobId',
           OrderNumber as 'OrderNumber',
           count(Pallet) as 'Pallet',
           sum(Weight) as 'Weight',
           null as 'Loaded',
           ReferenceNumber,
           sum(NettWeight) as NettWeight,
           null as 'HeaderStatus',
           Status
      from @TableResult
    group by OutboundShipmentId,
           OrderNumber,
           Route,
           DespatchDate,
           ExternalCompanyId,
           ExternalCompany,
           Location,
           JobId,
           Loaded,
           ReferenceNumber,
           HeaderStatus,
           Status,
           DropSequence
    order by OutboundShipmentId,
             OrderNumber
             
end
