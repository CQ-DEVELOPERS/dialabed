﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Stretch_Wrap_Accept
  ///   Filename       : p_Stretch_Wrap_Accept.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
 
 /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Stretch_Wrap_Accept
(
 @jobId      int,
 @operatorId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @InstructionId     int,
          @StatusId          int,        
  @InstructionTypeCode nvarchar(10),
          @WarehouseId         int
  
  set @Errormsg = 'Error executing p_Stretch_Wrap_Accept'
  select @GetDate = dbo.ufn_Getdate()
  
  select top 1 @InstructionId       = i.InstructionId,
               @InstructionTypeCode = it.InstructionTypeCode,
               @StatusId            = j.StatusId,
               @WarehouseId         = i.WarehouseId
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
   where i.JobId = @JobId
  order by i.InstructionId
  
  begin transaction
  
  if dbo.ufn_Configuration(85, @WarehouseId) = 1
  if @InstructionTypeCode in ('P','PM','PS','FM','PR')

  begin
  
  
      
    if (@StatusId not in (dbo.ufn_StatusId('IS','CD'),dbo.ufn_StatusId('IS','CK'),dbo.ufn_StatusId('IS','D')))
    begin
      set @Error = -1
      set @Errormsg = 'Error executing p_Stretch_Wrap_Accept - Status incorrect'
      goto error
    end
    
    if dbo.ufn_Configuration(171, @WarehouseId) = 1
    begin
      update Instruction
         set CheckQuantity = ConfirmedQuantity
       where JobId = @JobId
    end
    
    exec @Error = p_Job_Update
     @JobId          = @JobId,
     @CheckedBy      = @operatorId,
     @CheckedDate    = @GetDate

      
    exec @Error = p_Status_Rollup
     @JobId          = @JobId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end

 
