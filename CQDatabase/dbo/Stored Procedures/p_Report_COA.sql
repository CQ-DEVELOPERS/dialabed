﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_COA
  ///   Filename       : p_Report_COA.sql
  ///   Create By      : Karen
  ///   Date Created   : October 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_COA
(
  @ProductCode	nvarchar(30),
  @Batch		nvarchar(50)
)
 
as
begin
	 set nocount on;
	 
	if @ProductCode = '-1'
		set @ProductCode = null
		
	if @Batch = '-1'
		set @Batch = null

  select	COAId,
  Productcode
    from	COA coa
    join	storageunitbatch sub (nolock) on coa.StorageUnitBatchId = sub.StorageUnitBatchId
    join	batch			   b (nolock) on b.BatchId = sub.BatchId
    join	storageunit		  su (nolock) on su.StorageUnitId = sub.StorageUnitId
    join	product			   p (nolock) on p.productid = su.productid
   where	p.ProductCode = isnull(@ProductCode,p.ProductCode)
   and		b.Batch	= isnull(@Batch,b.Batch)

end
