﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPack_Search
  ///   Filename       : p_InterfaceImportPack_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:54
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportPack table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportPack.InterfaceImportProductId,
  ///   InterfaceImportPack.PackCode,
  ///   InterfaceImportPack.PackDescription,
  ///   InterfaceImportPack.Quantity,
  ///   InterfaceImportPack.Barcode,
  ///   InterfaceImportPack.Length,
  ///   InterfaceImportPack.Width,
  ///   InterfaceImportPack.Height,
  ///   InterfaceImportPack.Volume,
  ///   InterfaceImportPack.NettWeight,
  ///   InterfaceImportPack.GrossWeight,
  ///   InterfaceImportPack.TareWeight,
  ///   InterfaceImportPack.ProductCategory,
  ///   InterfaceImportPack.PackingCategory,
  ///   InterfaceImportPack.PickEmpty,
  ///   InterfaceImportPack.StackingCategory,
  ///   InterfaceImportPack.MovementCategory,
  ///   InterfaceImportPack.ValueCategory,
  ///   InterfaceImportPack.StoringCategory,
  ///   InterfaceImportPack.PickPartPallet,
  ///   InterfaceImportPack.HostId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPack_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportPack.InterfaceImportProductId
        ,InterfaceImportPack.PackCode
        ,InterfaceImportPack.PackDescription
        ,InterfaceImportPack.Quantity
        ,InterfaceImportPack.Barcode
        ,InterfaceImportPack.Length
        ,InterfaceImportPack.Width
        ,InterfaceImportPack.Height
        ,InterfaceImportPack.Volume
        ,InterfaceImportPack.NettWeight
        ,InterfaceImportPack.GrossWeight
        ,InterfaceImportPack.TareWeight
        ,InterfaceImportPack.ProductCategory
        ,InterfaceImportPack.PackingCategory
        ,InterfaceImportPack.PickEmpty
        ,InterfaceImportPack.StackingCategory
        ,InterfaceImportPack.MovementCategory
        ,InterfaceImportPack.ValueCategory
        ,InterfaceImportPack.StoringCategory
        ,InterfaceImportPack.PickPartPallet
        ,InterfaceImportPack.HostId
    from InterfaceImportPack
  
end
