﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuHistory_Insert
  ///   Filename       : p_MenuHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:50
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MenuHistory table.
  /// </remarks>
  /// <param>
  ///   @MenuId int = null,
  ///   @MenuText nvarchar(100) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   MenuHistory.MenuId,
  ///   MenuHistory.MenuText,
  ///   MenuHistory.CommandType,
  ///   MenuHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuHistory_Insert
(
 @MenuId int = null,
 @MenuText nvarchar(100) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert MenuHistory
        (MenuId,
         MenuText,
         CommandType,
         InsertDate)
  select @MenuId,
         @MenuText,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
