﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Order_Release
  ///   Filename       : p_Outbound_Order_Release.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Order_Release
(
 @operatorId int,
 @issueId    int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @StatusCode        nvarchar(10)
  
  set @Error = -1
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select @StatusCode = StatusCode
    from Issue i
    join Status s on i.StatusId = s.StatusId
   where i.IssueId = @issueId
  
  if @StatusCode in ('OH','Z')
  begin
    select @StatusId = dbo.ufn_StatusId('IS','W')
    
    exec @Error = p_Issue_Update
     @IssueId = @issueId,
     @StatusId = @StatusId
    
    if @Error <> 0
      goto error
  
    update IssueLine
       set StatusId = @StatusId
     where IssueId  = @issueId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Order_Release'
    rollback transaction
    return @Error
end
