﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Test_Parameter
  ///   Filename       : p_Test_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Test table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Test.TestId,
  ///   Test.Test 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Test_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as TestId
        ,'{All}' as Test
  union
  select
         Test.TestId
        ,Test.Test
    from Test
  order by Test
  
end
