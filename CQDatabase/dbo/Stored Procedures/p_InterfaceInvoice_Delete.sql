﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceInvoice_Delete
  ///   Filename       : p_InterfaceInvoice_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:21
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceInvoice table.
  /// </remarks>
  /// <param>
  ///   @InterfaceInvoiceId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceInvoice_Delete
(
 @InterfaceInvoiceId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceInvoice
     where InterfaceInvoiceId = @InterfaceInvoiceId
  
  select @Error = @@Error
  
  
  return @Error
  
end
