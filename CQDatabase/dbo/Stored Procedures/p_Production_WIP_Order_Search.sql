﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_WIP_Order_Search
  ///   Filename       : p_Production_WIP_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_WIP_Order_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @LocationId             int,
 @OrderNumber	           nvarchar(30),
 @ProductCode            nvarchar(50),
 @Product                nvarchar(255),
 @Batch                  nvarchar(50),
 @FromDate	              datetime,
 @ToDate	                datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   Id                        int primary key identity,
   WarehouseId               int,
   OutboundDocumentId        int,
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   NumberOfLines             int,
   DeliveryDate              datetime,
   CreateDate                datetime,
   StatusId                  int,
   StatusCode                nvarchar(10),
   Status                    nvarchar(50),
   PriorityId                int,
   Priority                  nvarchar(50),
   LocationId                int,
   Location                  nvarchar(15),
   DespatchBay               int,
   DespatchBayDesc           nvarchar(15),
   AvailabilityIndicator     nvarchar(20),
   Remarks                   nvarchar(255),
   Total                     numeric(13,3),
   Complete                  numeric(13,3),
   PercentageComplete        numeric(13,3),
   StockAvailable            numeric(13,3),
   DropSequence              int,
   StorageUnitId             int,
   BatchId                   int,
   ProductCode               nvarchar(30),
   Product                   nvarchar(255),
   Batch                     nvarchar(50),
   PlannedYield              float,
   AreaType                  nvarchar(10)
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @LocationId = -1
    set @LocationId = null
  
  if @ProductCode = '-1'
    set @ProductCode = null
  
  if @Product = '-1'
    set @Product = null
  
  if @Batch is null
    set @Batch = '%'
	 
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (WarehouseId,
           OutboundDocumentId,
           IssueId,
           LocationId,
           DespatchBay,
           OrderNumber,
           StatusId,
           StatusCode,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           Remarks,
           NumberOfLines,
           Total,
           Complete,
           DropSequence,
           StorageUnitId,
           BatchId,
           PlannedYield,
           AreaType)
    select i.WarehouseId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           i.DespatchBay,
           od.OrderNumber,
           i.StatusId,
           s.StatusCode,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           i.Remarks,
           i.NumberOfLines,
           i.Total,
           i.Complete,
           i.DropSequence,
           od.StorageUnitId,
           od.BatchId,
           od.Quantity,
           i.AreaType
      from OutboundDocument      od (nolock)
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                            and odt.OutboundDocumentTypeCode in ('MO','KIT')
      join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                 s (nolock) on i.StatusId                = s.StatusId
     where od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
       and od.CreateDate      between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('W','SA','M','RL','PC','PS','A','WC','QA','CK') -- GS 2012-10-15 Removed 'P' - sent back to MO planning screen
       and od.WarehouseId            = @WarehouseId
    order by i.DropSequence
    
    update tr
       set OutboundShipmentId = si.OutboundShipmentId,
           DeliveryDate = os.ShipmentDate,
           LocationId   = os.LocationId,
           DespatchBay  = os.DespatchBay,
           StatusId     = os.StatusId
      from @TableResult  tr
      join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
      join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  end
  else
    insert @TableResult
          (WarehouseId,
           OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           LocationId,
           DespatchBay,
           OrderNumber,
           StatusId,
           StatusCode,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           Remarks,
           NumberOfLines,
           Total,
           Complete,
           DropSequence,
           StorageUnitId,
           BatchId,
           PlannedYield,
           AreaType)
    select i.WarehouseId,
           osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           i.DespatchBay,
           od.OrderNumber,
           i.StatusId,
           s.StatusCode,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           i.Remarks,
           i.NumberOfLines,
           i.Total,
           i.Complete,
           i.DropSequence,
           od.StorageUnitId,
           od.BatchId,
           od.Quantity,
           i.AreaType
      from OutboundDocument       od (nolock)
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                            and odt.OutboundDocumentTypeCode in ('MO','KIT')
      join Issue                   i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                  s (nolock) on i.StatusId                = s.StatusId
      join OutboundShipmentIssue osi (nolock) on i.IssueId                 = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and s.StatusCode             in ('W','SA','M','RL','PC','PS','A','WC','QA','CK') -- GS 2012-10-15 Removed 'P' - sent back to MO planning screen
  order by i.DropSequence
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product
    from @TableResult tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product       p (nolock) on su.ProductId     = p.ProductId
  
  update tr
     set Batch = b.Batch
    from @TableResult tr
    join Batch         b (nolock) on tr.BatchId = b.BatchId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update th
     set Location = l.Location
   from @TableResult th
   join Location      l (nolock) on th.LocationId = l.LocationId
  
  update th
     set DespatchBayDesc = l.Location
   from @TableResult th
   join Location      l (nolock) on th.DespatchBay = l.LocationId
  
  update @TableResult
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @WarehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  select IssueId,
         isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
         OrderNumber,
         NumberOfLines,
         convert(nvarchar(10), DeliveryDate, 120) as 'DeliveryDate',
         CreateDate,
         ProductCode,
         Product,
         Batch,
         PlannedYield,
         Status,
         PriorityId,
         Priority,
         isnull(LocationId,-1) as 'LocationId',
         Location,
         isnull(DespatchBay,-1) as 'PotId',
         DespatchBayDesc        as 'Pot',
         AvailabilityIndicator,
         Remarks,
         Complete,
         Total,
         convert(int, round(PercentageComplete,0)) as 'PercentageComplete',
         DropSequence,
         --ROW_NUMBER() 
         --OVER (ORDER BY Id) AS 'DropSequence',
         ROW_NUMBER() 
         OVER (ORDER BY Id) AS 'Rank'
    from @TableResult
   where isnull(ProductCode,'%') like isnull(@ProductCode + '%', isnull(ProductCode,'%'))
     and isnull(Product    ,'%') like isnull(@Product     + '%', isnull(Product    ,'%'))
     and isnull(Batch      ,'%') like isnull(@Batch       + '%', isnull(Batch      ,'%'))
  order by DropSequence
end
