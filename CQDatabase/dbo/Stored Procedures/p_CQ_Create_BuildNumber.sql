﻿/*
  /// <summary>
  ///   Procedure Name : p_CQ_Create_BuildNumber
  ///   Filename       : p_CQ_Create_BuildNumber.sql
  ///   Create By      : Venkat
  ///   Date Created   : 17 Jan 2017
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_CQ_Create_BuildNumber
(
 @MajorVersion bit = 0,
 @MinorVersion bit = 0,
 @BuildNumber  bit = 0,
 @Revision     bit = 0,
 @CreatedBy    nvarchar(255),
 @Module       nvarchar(255),
 @QaVersion    nvarchar(255) = null
)

as
begin
	 set nocount on;
 
create table #LastInsertedBuild
(
 MajorVersion  int,
 MinorVersion  int,
 BuildNumber   int,
 Revision      int
) 

insert #LastInsertedBuild
	(MajorVersion,
	MinorVersion,
	BuildNumber,
	Revision)
select top 1
	MajorVersion,
	MinorVersion,
	BuildNumber,
	Revision 
from CQBuild order by CQBuildId DESC
  
  if (@QaVersion is not null)
  begin
	insert CQBuild (MajorVersion,MinorVersion,BuildNumber,Revision,CreatedBy,QaVersion) 
	select Name1, Name2, Name3, Name4, @CreatedBy, @QaVersion
	from
	(
	  select value, Name
	  from dbo.SplitString(@QaVersion,'.')
	) d
	pivot
	(
	  max(value)
	  FOR Name In(Name1, Name2, Name3, Name4)
	) piv;
  end
  else
  begin
  
  if(@MajorVersion = 1)
  begin
	insert CQBuild (MajorVersion,MinorVersion,BuildNumber,Revision,CreatedBy) 
	select MajorVersion+1,MinorVersion,BuildNumber,Revision+1,@CreatedBy from #LastInsertedBuild 
	goto result
  end
  
  if(@MinorVersion = 1)
  begin
	insert CQBuild (MajorVersion,MinorVersion,BuildNumber,Revision,CreatedBy) 
	select MajorVersion,MinorVersion+1,BuildNumber,Revision+1,@CreatedBy from #LastInsertedBuild 
	goto result
  end
  
  if(@Module = 'Dev')
  begin
	insert CQBuild (MajorVersion,MinorVersion,BuildNumber,Revision,CreatedBy) 
	select MajorVersion,MinorVersion,BuildNumber,Revision+1,@CreatedBy from #LastInsertedBuild 
	goto result
  end
  
  if(@Module = 'Qa')
  begin
	insert CQBuild (MajorVersion,MinorVersion,BuildNumber,Revision,CreatedBy) 
	select MajorVersion,MinorVersion,BuildNumber+1,Revision+1,@CreatedBy from #LastInsertedBuild
	goto result 
  end

end
result:
	begin
		select top 1 Convert(nvarchar(5),MajorVersion) + '.' + Convert(nvarchar(5),MinorVersion) + '.' +  Convert(nvarchar(5),BuildNumber) + '.' + Convert(nvarchar(5),Revision) as Version from CQBuild order by CQBuildId DESC
	end
end

