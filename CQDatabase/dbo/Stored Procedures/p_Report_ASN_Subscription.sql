﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_ASN_Subscription
  ///   Filename       : p_Report_ASN_Subscription.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 May 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_ASN_Subscription
 
as
begin
	set nocount on;

	declare @IssueId		int,
			@ReportTypeId	int
	
	select @ReportTypeId = rt.reporttypeid
	from   ReportType    rt (nolock)
	where  rt.reporttypecode = 'ASN'
	
	insert into ReportPrinted 
			   (ReportTypeId, 
				IssueId, 
				PrintedCopies,
				CreateDate)
	select      @ReportTypeId, 
				IssueId, 
				0,
				GETDATE() 
	  from Issue               i (nolock)
	 where i.DeliveryDate > '2013-06-03'
	   and i.statusid in (dbo.ufn_StatusId('IS','CD'),dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'))
	   and not exists (select top 1 IssueId, ReportTypeId 
						 from ReportPrinted rp 
						where rp.IssueId = i.IssueId
						and rp.ReportTypeId = @ReportTypeId)
	
	select @IssueId = i.IssueId
	from Issue       i (nolock)
	where i.ASN = 0
	and i.statusid in (dbo.ufn_StatusId('IS','CD'),dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'))
	
	select (select Value from Configuration where ConfigurationId = 200 and WarehouseId = od.WarehouseId) as 'ServerName',
			db_name()    as 'DatabaseName',
			'Auto Email' as 'UserName',
			-1           as OutboundShipmentId,
			i.IssueId,
			cl.EMail     as 'Email',
			cl.EMail     as 'TO',
			'Automatic Shipping Notice'   
						 as 'Subject'
	from  Issue                 i (nolock) 
	join  OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
	join  ContactList            cl (nolock) on od.ExternalCompanyId = cl.ExternalCompanyId
	join  ReportType             rt (nolock) on cl.ReportTypeId = rt.ReportTypeId
	join  ReportPrinted          rp (nolock) on i.IssueId = rp.IssueId
											and rp.PrintedCopies = 0
	where rp.ReportTypeId = @ReportTypeId
 

	update  ReportPrinted
	   set  PrintedCopies = 1,
	        EmailDate = getdate()
	 where  PrintedCopies = 0
	 and    ReportTypeId = @ReportTypeId
	 
   end 
