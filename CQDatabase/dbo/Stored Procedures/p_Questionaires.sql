﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questionaires
  ///   Filename       : p_Questionaires.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Questionaires
(
	@WarehouseId      int
)

as
begin
	 set nocount on;
  
  select a.QuestionAireId,
		a.QuestionaireType,
		a.QuestionaireDesc
	from QuestionAire a
		
	where warehouseid = @WarehouseId
	Order by QuestionAireId
			
      
  
end
