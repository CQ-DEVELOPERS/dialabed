﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Query_Picking_Job_Update
  ///   Filename       : p_Query_Picking_Job_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Apr 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Query_Picking_Job_Update
(
 @JobId      int,
 @OperatorId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Query_Picking_Job_Update'
  
  if @OperatorId = -1
    set @OperatorId = null
  
  begin transaction
  
  if @OperatorId is null
  begin
    update Job
      set OperatorId = @OperatorId
     where JobId = @JobId
    
    if @@error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_Job_Update
     @JobId      = @JobId,
     @OperatorId = @OperatorId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
