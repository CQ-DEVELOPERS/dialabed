﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Outbound_Summary_Report
  ///   Filename       : p_Report_Outbound_Summary_Report.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Outbound_Summary_Report
(
@OrderNumber       nvarchar(30),
@StorageUnitId	   int = null,
@Batch             nvarchar(50),
@FromDate          datetime,
@ToDate            datetime,
@StatusId		   int = null,
@PrincipalId	   int = null,
@OperatorId        int = null,
@WarehouseId       int = null
)
as
begin
  set nocount on;
  
  insert into AccessLog (AccessLog) 
  VALUES (CONVERT(nvarchar(50),isnull(@WarehouseId, '12')))
  
  declare @ExternalCompanyId int,
          @PrincipalId2      int
          
  declare @TableSOH as table
  (
   WarehouseId int,
   StorageUnitBatchId  int,
   StockOnHand int
  )
    
  insert @TableSOH
        (WarehouseId,
         StorageUnitBatchId,
         StockOnHand)
  select a.WarehouseId,
         subl.StorageUnitBatchId,
         sum(subl.ActualQuantity)
    from StorageUnitBatchLocation subl (nolock)
    join AreaLocation               al (nolock) on subl.LocationId        = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
    and a.WarehouseId = isnull(@WarehouseId, a.WarehouseId)
  group by a.WarehouseId,
           subl.StorageUnitBatchId

  
  declare @TableDetails as Table
  (
  OrderNumber          nvarchar(30),
  OrderDate            datetime,
  ExpectedDeliveryDate datetime,
  ProductCode          nvarchar(30),
  ProductDesc          nvarchar(255),
  StorageUnitId		   int,
  SKUCode              nvarchar(50),
  SKU                  nvarchar(50),
  Batch                nvarchar(50),
  OrderQty             numeric(13,6),
  IssuedQty            numeric(13,6),
  OutstandingBal       numeric(13,6),
  StockOnHand          numeric(13,6),
  DeliveryNote		   nvarchar(30),
  ExternalCompanyId	   int,
  ExternalCompanyCode  nvarchar(30),
  ExternalCompany	   nvarchar(255),
  DeliveryDate		   datetime,
  IssueId			   int,
  IssueLineId		   int,
  Status			   nvarchar(50),
  PrincipalId		   int,
  PrincipalCode		   nvarchar(30),
  OutboundDocumentTypeId	int,
  OutboundDocumentType nvarchar(30),
  DeliveryAdd1         nvarchar(255),
  DeliveryAdd2		   nvarchar(255),
  DeliveryAdd3		   nvarchar(255),
  DeliveryAdd4		   nvarchar(255),
  Reference1		   nvarchar(255),
  Reference2		   nvarchar(255),
  Weight			   int,
  Height			   int,
  Length			   int,
  Width				   int,
  ExpiryDate		   datetime,
  WarehouseId		   int
  )
  
  if @StorageUnitId = '-1'
     set @StorageUnitId = null
         
  if @OrderNumber = '-1'
     set @OrderNumber = null
         
  if @Batch = '-1'
     set @Batch = null
     
  if @StatusId = -1
	 set @StatusId = null
		
  if @PrincipalId = -1
	 set @PrincipalId = null
	 
  if @OperatorId = -1
    set @OperatorId = null     
  
  select @ExternalCompanyId = o.ExternalCompanyId,
         @PrincipalId2       = PrincipalId
    from Operator o (nolock)
   where o.OperatorId = @OperatorId
   
   if @PrincipalId2 is not null
	set @PrincipalId = @PrincipalId2
      
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 IssuedQty,
			 OutstandingBal,
			 StockOnHand,
			 ExternalCompanyId,
			 DeliveryNote,
			 DeliveryDate,
			 IssueId,
			 IssueLineId,
			 Status,
			 PrincipalId,
			 OutboundDocumentTypeId,
			 ExpiryDate,
			 WarehouseId,
			 Reference1,
			 Reference2
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 sum(il.Quantity),
			 sum(il.ConfirmedQuatity),
			 sum(il.Quantity - isnull(il.ConfirmedQuatity,0)),
			 sum(StockOnHand) ,
			 id.ExternalCompanyId,
			 i.DeliveryNoteNumber ,
			 i.DeliveryDate,
			 i.IssueId,
			 il.IssueLineId,
			 s.Status,
			 p.PrincipalId,
			 id.OutboundDocumentTypeId,
			 b.ExpiryDate ,
			 soh.WarehouseId,
			 id.AdditionalText1,
			 id.AdditionalText2      
		from  OutboundDocument    id (nolock)
		 join OutboundLine        ol (nolock) on id.OutboundDocumentId = ol.OutboundDocumentId
		 join IssueLine           il (nolock) on ol.OutboundLineId = il.OutboundLineId
		 join Issue			       i (nolock) on il.IssueId = i.IssueId
		 join status			   s (nolock) on i.StatusId = s.StatusId
		 join StorageUnitBatch   sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch                b (nolock) on sub.BatchId = b.BatchId
		 join StorageUnit         su (nolock) on sub.StorageUnitId = su.StorageUnitId
		 join Product              p (nolock) on su.ProductId = p.ProductId
		 join SKU                sku (nolock) on su.SKUId = sku.SKUId
		 left
		 join @TableSOH         soh on il.StorageUnitBatchId = soh.StorageUnitBatchId
								   and id.WarehouseId = soh.WarehouseId
	   where id.CreateDate between @FromDate and @ToDate
	     and s.StatusId = isnull(@StatusId,s.StatusId)
	     --and isnull(id.PrincipalId,-1) = isnull(@PrincipalId,isnull(id.PrincipalId,-1))
	     and (id.PrincipalId = @PrincipalId or @PrincipalId is null)
	     and id.ExternalCompanyId = isnull(@ExternalCompanyId, id.ExternalCompanyId)
	     and id.WarehouseId = isnull(@WarehouseId, id.WarehouseId)
	  group by id.OrderNumber,
			   id.CreateDate,
			   id.DeliveryDate,
			   p.ProductCode,
			   p.Product,
			   su.StorageUnitId,
			   sku.SKUCode,
			   sku.SKU,
			   b.Batch,
			   id.ExternalCompanyId,
			   i.DeliveryNoteNumber,
			   i.DeliveryDate,
			   i.IssueId,
			   il.IssueLineId,
			   s.Status,
			   p.PrincipalId,
			   id.OutboundDocumentTypeId,
			   b.ExpiryDate,
			   soh.WarehouseId,
			   id.AdditionalText1,
			   id.AdditionalText2  
			   
   update  td
      set td.Batch = b.Batch
     from @TableDetails		td
     join Instruction	   ins (nolock) on ins.IssueLineId = td.IssueLineId
     join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId = ins.StorageUnitBatchId
     join Batch				 b (nolock) on sub.BatchId = b.BatchId
     
   update  td
      set td.ExternalCompanyCode = ec.ExternalCompanyCode,
		  td.ExternalCompany = ec.ExternalCompany
     from @TableDetails td
     join ExternalCompany ec (nolock) on ec.ExternalCompanyId = td.ExternalCompanyId
     
     
     update tr
       set     tr.DeliveryAdd1 = a.Street,
		       tr.DeliveryAdd2 = a.Suburb,
		       tr.DeliveryAdd3 = a.Town,
		       tr.DeliveryAdd4 = a.Code
      from @TableDetails             tr
      join Address a  (nolock) on tr.ExternalCompanyId   = a.ExternalCompanyId
     
   update  td
      set td.PrincipalCode = p.PrincipalCode
     from @TableDetails td
     join Principal p  (nolock) on p.PrincipalId = td.PrincipalId
     
  update  td
      set td.OutboundDocumentType = odt.OutboundDocumentType
     from @TableDetails td
     join OutboundDocumentType odt  (nolock) on td.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     
  update tr
     set Height = p.Height,
         Length = p.Length,
         Width = p.Width,
         Weight = p.Weight 
    from @TableDetails tr
    join Pack p on tr.StorageUnitId = p.StorageUnitId
    where p.PackTypeId = 8 -- unit
     
  select OrderNumber,
         DeliveryNote,
         OrderDate,
         ExpectedDeliveryDate,
         ProductCode,
         ProductDesc,
         SKUCode,
         SKU,
         Batch,
         sum(OrderQty) as OrderQty,
         sum(IssuedQty) as IssuedQty,
         sum(OutstandingBal) as OutstandingBal,
         StockOnHand,
         ExternalCompanyCode,
         ExternalCompany,
		 deliverydate,
		 Status,
		 PrincipalCode,
		 OutboundDocumentType,
		 LTRIM(RTRIM(DeliveryAdd1))+','+LTRIM(RTRIM(DeliveryAdd2))+','+LTRIM(RTRIM(DeliveryAdd3))+','+LTRIM(RTRIM(DeliveryAdd4)) as DeliveryAddress,
		 Reference1,
		 Reference2,
		 Weight,
		 Height,
		 Length,
		 Width,
		 ExpiryDate,
         (select Indicator from Configuration c where ConfigurationId = 426 and c.WarehouseId = td.WarehouseId) as 'PrtDIMS'
    from @TableDetails td
   where OrderNumber = isnull(@OrderNumber, OrderNumber)
	 and StorageUnitId = ISNULL(@StorageUnitId, StorageUnitId)
     and Batch = isnull(@Batch, Batch)
    group by OrderNumber,
         DeliveryNote,
         OrderDate,
         ExpectedDeliveryDate,
         ProductCode,
         ProductDesc,
         SKUCode,
         SKU,
         Batch,        
         StockOnHand,
         ExternalCompanyCode,
         ExternalCompany,
		 deliverydate,
		 Status,
		 PrincipalCode,
		 OutboundDocumentType,
		 DeliveryAdd1,
		 DeliveryAdd2,
		 DeliveryAdd3,
		 DeliveryAdd4,
		 --LTRIM(RTRIM(DeliveryAdd1))+','+LTRIM(RTRIM(DeliveryAdd2))+','+LTRIM(RTRIM(DeliveryAdd3))+','+LTRIM(RTRIM(DeliveryAdd4)) as DeliveryAddress,
		 Reference1,
		 Reference2,
		 Weight,
		 Height,
		 Length,
		 Width,
		 ExpiryDate,
		 WarehouseId
         --(select Indicator from Configuration c where ConfigurationId = 426 and c.WarehouseId = td.WarehouseId) as 'PrtDIMS'
   order by ProductCode
end
 
 
