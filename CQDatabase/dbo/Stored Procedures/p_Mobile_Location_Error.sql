﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Location_Error
  ///   Filename       : p_Mobile_Location_Error.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Location_Error
(
 @instructionId   int,
 @pickError       bit = 0,
 @storeError      bit = 0,
 @createStockTake bit = 1,
 @ingorePick      bit = 1
)
 
as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         InstructionId,
         Pick,
         Store,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         @pickError,
         @storeError,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @InstructionTypeCode nvarchar(10),
          @WarehouseId         int,
          @LocationId          int,
          @StorageUnitBatchId  int,
          @Quantity            numeric(13,6),
          @ConfirmedQuantity   numeric(13,6),
          @PickLocationId      int,
          @OldPickLocationId   int,
          @StoreLocationId     int,
          @OperatorId          int,
          @NoStockId           int,
          @JobId               int,
          @Replenishment       bit,
          @StorageUnitId       int,
          @LIFO                bit,
          @MinimumShelfLife    numeric(13,3),
          @Temporary           bit,
          @PickAreaCode        nvarchar(10),
          @PickStockTake       bit = 0,
          @ActualQuantity      numeric(13,6),
          @AreaType            nvarchar(10),
          @ExternalCompanyId   int,
          @DeliveryDate        datetime,
          @OutboundShipmentId  int,
          @IssueId             int,
          @InstructionTypeId   int,
          @PriorityId          int,
          @InstructionRefId    int,
          @OutboundDocumentTypeCode nvarchar(10),
          @MoveId                   int,
          @Exception                nvarchar(255),
          @Operator                 nvarchar(50),
          @WaveId                   int,
          @Result                   int
  
  select @GetDate = dbo.ufn_Getdate()
  select @NoStockId = dbo.ufn_StatusId('I','NS')
  set @Replenishment = 0
  
  set @Error = 0
  
  select @InstructionTypeCode = it.InstructionTypeCode,
         @WarehouseId         = i.WarehouseId,
         @Quantity            = i.Quantity,
         @ConfirmedQuantity   = i.ConfirmedQuantity,
         @StorageUnitBatchId  = i.StorageUnitBatchId,
         @OldPickLocationId   = i.PickLocationId,
         @StoreLocationId     = i.StoreLocationId,
         @OperatorId          = i.OperatorId,
         @Operator            = o.Operator,
         @JobId               = i.JobId
    from Instruction      i (nolock)
    left
    join Operator         o (nolock) on i.OperatorId = o.OperatorId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status           s (nolock) on i.StatusId          = s.StatusId
   where i.InstructionId = @instructionId
     and ((s.StatusCode in ('W','S') and @ingorePick = 1)
     or   (@ingorePick = 0))
  
  if @@Rowcount = 0
  begin
    set @Error = 0
    goto result
  end
  
  if @ingorePick = 1
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId       = @instructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
  end
  
  update MobileLog
     set StoreLocationId = @StoreLocationId
   where MobileLogId = @MobileLogId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto result
  
  if @storeError = 1
  begin
    if @createStockTake = 0
    and exists(select top 1 1
                 from AreaLocation al (nolock)
                 join Area          a (nolock) on al.AreaId = a.AreaId
                where LocationId = @StoreLocationId
                 and a.Area      = 'WavePick')
    begin
      exec @Error = p_Housekeeping_Stock_Take_Create_Product
       @warehouseId        = @WarehouseId,
       @operatorId         = @OperatorId,
       @locationId         = @StoreLocationId,
       @storageUnitBatchId = @StorageUnitBatchId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
    end
    
    if @createStockTake = 1
    begin
      exec @Error = p_Housekeeping_Stock_Take_Create_Product
       @warehouseId        = @WarehouseId,
       @operatorId         = @OperatorId,
       @locationId         = @StoreLocationId,
       @storageUnitBatchId = @StorageUnitBatchId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      select @Exception = 'Operator ' + @Operator + ' requested alternate location.'
      
      exec @Error = p_Exception_Insert
       @ExceptionId   = null,
       @ExceptionCode = 'STKREQ',
       @Exception     = @Exception,
       @OperatorId    = @OperatorId,
       @CreateDate    = @GetDate,
       @ExceptionDate = @Getdate,
       @InstructionId = @InstructionId,
       @JobId         = @JobId,
       @Quantity      = @Quantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
    end
    else
    begin
       update Instruction
          set OverrideStore = 1
        where InstructionId = @instructionId
    end
    
    if (dbo.ufn_Configuration(290, @WarehouseId) = 1 and @InstructionTypeCode in ('PR','S','SM') and @createStockTake = 0)
    or (dbo.ufn_Configuration(331, @WarehouseId) = 1 and @InstructionTypeCode in ('R','M','P') and @createStockTake = 0)
    begin
      update Instruction
         set StoreLocationId = null
       where InstructionId = @instructionId
      
      select @Error = @@Error
      
      if @Error <> 0
      begin
        set @Error = 1
        goto result
      end
    end
    else
    begin
      exec @Error = p_Location_Get
       @WarehouseId        = @WarehouseId,
       @LocationId         = @StoreLocationId output,
       @StorageUnitBatchId = @StorageUnitBatchId,
       @Quantity           = @Quantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto result
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId      = @instructionId,
       @StoreLocationId    = @StoreLocationId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto result
      end
    end
  end
  
  if @pickError = 1
  begin
    select @OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode,
           @LIFO               = odt.LIFO,
           @MinimumShelfLife   = odt.MinimumShelfLife,
           @AreaType           = isnull(i.AreaType,''),
           @ExternalCompanyId  = od.ExternalCompanyId,
           @OutboundShipmentId = ili.OutboundShipmentId,
           @IssueId            = ili.IssueId,
           @WaveId             = i.WaveId
      from IssueLineInstruction ili (nolock)
      join OutboundDocument      od (nolock) on ili.OutboundDocumentId    = od.OutboundDocumentId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join Issue                  i (nolock) on ili.IssueId               = i.IssueId
      --join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
     where ili.InstructionId = @instructionId
    
    if @@ROWCOUNT = 0
      select @OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode,
             @LIFO               = odt.LIFO,
             @MinimumShelfLife   = odt.MinimumShelfLife,
             @AreaType           = isnull(i.AreaType,''),
             @ExternalCompanyId  = od.ExternalCompanyId,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId,
             @WaveId             = i.WaveId
        from IssueLineInstruction ili (nolock)
        join OutboundDocument      od (nolock) on ili.OutboundDocumentId    = od.OutboundDocumentId
        join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
        join Issue                  i (nolock) on ili.IssueId               = i.IssueId
       where ili.InstructionId = (select InstructionRefId from Instruction (nolock) where InstructionId = @instructionId)
    
    select @PickAreaCode = AreaCode,
           @PickStockTake = StockTake
      from AreaLocation al (nolock)
      join Area          a (nolock) on al.AreaId = a.AreaId
     where LocationId = @OldPickLocationId
    
    if @createStockTake = 1
    begin
      select @Exception = 'Operator ' + @Operator + ' requested alternate location.'
      
      exec @Error = p_Exception_Insert
       @ExceptionId   = null,
       @ExceptionCode = 'STKREQ',
       @Exception     = @Exception,
       @OperatorId    = @OperatorId,
       @CreateDate    = @GetDate,
       @ExceptionDate = @Getdate,
       @InstructionId = @InstructionId,
       @JobId         = @JobId,
       @Quantity      = @Quantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      if @PickAreaCode = 'BK'
      begin
        if dbo.ufn_Configuration(303, @WarehouseId) = 1
        begin
          exec @Error = p_Housekeeping_Stock_Take_Create_Product
           @warehouseId        = @WarehouseId,
           @operatorId         = @OperatorId,
           @locationId         = @OldPickLocationId,
           @storageUnitBatchId = @StorageUnitBatchId,
          @ConfirmedQuantity   = @ConfirmedQuantity
        end
        else
        begin
          exec @Error = p_Housekeeping_Stock_Take_Create_Product
           @warehouseId        = @WarehouseId,
           @operatorId         = @OperatorId,
           @locationId         = @OldPickLocationId 
        end
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
      end
      else if (@PickAreaCode = 'SP' and @PickStockTake = 1) -- GS 2014-01-16 Added for UTI
           or ((dbo.ufn_Configuration(451, @warehouseId)) = 1 and @PickAreaCode = 'PK' and @PickStockTake = 1)
      begin
        if dbo.ufn_Configuration(240, @WarehouseId) = 1
        begin
          exec @Error = p_Housekeeping_Stock_Take_Create_Product
           @warehouseId        = @WarehouseId,
           @operatorId         = @OperatorId,
           @locationId         = @OldPickLocationId,
           @storageUnitBatchId = @StorageUnitBatchId,
           @ConfirmedQuantity  = @ConfirmedQuantity
        end
        else
        begin
         exec @Error = p_Housekeeping_Stock_Take_Create_Product
          @warehouseId        = @WarehouseId,
          @operatorId         = @OperatorId,
          @locationId         = @OldPickLocationId
        end
      end
      else if @PickAreaCode not in ('PK','SP','TP')
      begin
        if dbo.ufn_Configuration(240, @WarehouseId) = 1
        begin
          exec @Error = p_Housekeeping_Stock_Take_Create_Product
           @warehouseId        = @WarehouseId,
           @operatorId         = @OperatorId,
           @locationId         = @OldPickLocationId,
           @storageUnitBatchId = @StorageUnitBatchId
        end
        else
        begin
         exec @Error = p_Housekeeping_Stock_Take_Create_Product
          @warehouseId        = @WarehouseId,
          @operatorId         = @OperatorId,
          @locationId         = @OldPickLocationId
        end
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
      end
    end
    
    if @ingorePick = 1
    begin
    set @PickLocationId = @OldPickLocationId
  
    update MobileLog
       set PickLocationId = @PickLocationId
     where MobileLogId = @MobileLogId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto result
    
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch (nolock)
     where StorageUnitBatchId = @StorageUnitBatchId
    
    if @InstructionTypeCode in ('R','PM','FM','PS')
    begin
      if @InstructionTypeCode = 'R'
      begin
        set @Replenishment = 1
      
        select top 1 @Quantity = Quantity
          from Pack      p (nolock)
          join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
         where StorageUnitId = @StorageUnitId
           and WarehouseId   = @WarehouseId
        order by pt.OutboundSequence
      end
      else
      begin
        select top 1 @Quantity = Quantity
          from Pack      p (nolock)
          join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
         where StorageUnitId = @StorageUnitId
           and WarehouseId   = @WarehouseId
        order by pt.OutboundSequence desc
      end
    end
    
    -- Moved up
    --select @OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode,
    --       @LIFO               = odt.LIFO,
    --       @MinimumShelfLife   = odt.MinimumShelfLife,
    --       @AreaType           = isnull(i.AreaType,''),
    --       @ExternalCompanyId  = od.ExternalCompanyId,
    --       @OutboundShipmentId = ili.OutboundShipmentId,
    --       @IssueId            = ili.IssueId
    --  from IssueLineInstruction ili (nolock)
    --  join OutboundDocument      od (nolock) on ili.OutboundDocumentId    = od.OutboundDocumentId
    --  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    --  join Issue                  i (nolock) on ili.IssueId               = i.IssueId
    --  --join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
    -- where ili.InstructionId = @instructionId
    
    --if @@ROWCOUNT = 0
    --  select @OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode,
    --         @LIFO               = odt.LIFO,
    --         @MinimumShelfLife   = odt.MinimumShelfLife,
    --         @AreaType           = isnull(i.AreaType,''),
    --         @ExternalCompanyId  = od.ExternalCompanyId,
    --         @OutboundShipmentId = ili.OutboundShipmentId,
    --         @IssueId            = ili.IssueId
    --    from IssueLineInstruction ili (nolock)
    --    join OutboundDocument      od (nolock) on ili.OutboundDocumentId    = od.OutboundDocumentId
    --    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    --    join Issue                  i (nolock) on ili.IssueId               = i.IssueId
    --   where ili.InstructionId = (select InstructionRefId from Instruction (nolock) where InstructionId = @instructionId)
    
    
    --if dbo.ufn_Configuration(380, @WarehouseId) = 1 -- If kit and mixing tank true - check stock in mix area
    --begin
    --  if @OutboundDocumentTypeCode = 'KIT'
    --  if (select sum(subl.ActualQuantity - subl.ReservedQuantity)
    --        from StorageUnitBatch          sub (nolock)
    --        join Batch                       b (nolock) on sub.BatchId            = b.BatchId
    --        join Status                      s (nolock) on b.StatusId             = s.StatusId
    --        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    --        join Location                    l (nolock) on subl.LocationId        = l.LocationId
    --        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    --        join Area                        a (nolock) on al.AreaId              = a.AreaId
    --       where a.StockOnHand     = 1
    --         and s.StatusCode      = 'A'
    --         and l.ActivePicking   =  1
    --         and a.AreaCode        = 'POT'
    --         and sub.StorageUnitId = @StorageUnitId)
    --   > @Quantity
    --  begin
    --    set @PickLocationId = null
    --    set @createStockTake = 1
    --    goto nostock
    --  end
    --end
    
    select @DeliveryDate = DeliveryDate
      from Issue (nolock)
     where IssueId = @IssueId
    
    --select @DeliveryDate = ShipmentDate
    --  from OutboundShipment (nolock)
    -- where OutboundShipmentId = @OutboundShipmentId
    
    if @MinimumShelfLife is null
      select @MinimumShelfLife = MinimumShelfLife
        from StorageUnitExternalCompany (nolock)
       where StorageUnitId     = @StorageUnitId
         and ExternalCompanyId = @ExternalCompanyId
    --select @LIFO as '@LIFO', @MinimumShelfLife as '@MinimumShelfLife', @AreaType as '@AreaType', @ExternalCompanyId as '@ExternalCompanyId', @DeliveryDate as '@DeliveryDate'
    if @LIFO is null                             
      set @LIFO = 0                              
    
    if @Temporary is null
      set @Temporary = 0
    
    if @AreaType is null
      set @AreaType = ''
    
    if dbo.ufn_Configuration(133, @WarehouseId) = 1
      if @MinimumShelfLife is not null or @LIFO = 1
         set @Temporary = 1
       else
         set @Temporary = 0
    
    if @CreateStockTake = 0
      set @PickLocationId = null
    
    exec @Error = p_Pick_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @PickLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId output,
     @Quantity           = @Quantity output,
     @InstructionId      = @InstructionId,
     @Replenishment      = @Replenishment,
     @LIFO               = @LIFO,
     @MinimumShelfLife   = @MinimumShelfLife,
     @Temporary          = @Temporary,
     @AreaType           = @AreaType,
     @DeliveryDate       = @DeliveryDate,
     @InstructionTypeCode = @InstructionTypeCode,
     @IssueId             = @IssueId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
    
    if (@createStockTake = 1 and @PickLocationId = @OldPickLocationId) or @OutboundDocumentTypeCode = 'WAV'
    begin
      select @PickLocationId = null
    end
    
    if @OutboundDocumentTypeCode != 'WAV'
    begin
      if @AreaType = 'MassMart' and @PickLocationId is null
      begin
        set @AreaType = ''
        
        exec @Error = p_Pick_Location_Get
         @WarehouseId        = @WarehouseId,
         @LocationId         = @PickLocationId output,
         @StorageUnitBatchId = @StorageUnitBatchId output,
         @Quantity           = @Quantity output,
         @InstructionId      = @InstructionId,
         @Replenishment      = @Replenishment,
         @LIFO               = @LIFO,
         @MinimumShelfLife   = @MinimumShelfLife,
         @Temporary          = @Temporary,
         @AreaType           = @AreaType,
         @DeliveryDate       = @DeliveryDate,
         @InstructionTypeCode = @InstructionTypeCode,
         @IssueId             = @IssueId
        
        if @Error <> 0
        begin
          set @Error = 1
          goto result
        end
      end
      
      --select @WarehouseId          as '@WarehouseId          ',
      --       @PickLocationId       as '@PickLocationId       ',
      --       @OldPickLocationId    as '@OldPickLocationId    ',
      --       @StorageUnitBatchId   as '@StorageUnitBatchId   ',
      --       @Quantity             as '@Quantity             ',
      --       @InstructionId        as '@InstructionId        ',
      --       @Replenishment        as '@Replenishment        ',
      --       @LIFO                 as '@LIFO                 ',
      --       @MinimumShelfLife     as '@MinimumShelfLife     ',
      --       @Temporary            as '@Temporary            ',
      --       @AreaType             as '@AreaType'
      
      -- Check if replenishment is waiting
      if @PickLocationId is null and @InstructionTypeCode in ('PM','PS','FM')
      begin
        if (select isnull(SUM(ActualQuantity - ReservedQuantity),0)
                        from viewAvailableArea
                       where AreaCode = 'POT'
                         and StorageUnitId = @StorageUnitId) < @Quantity
        begin
          if @WaveId is null
          begin
            exec p_Mobile_Request_Replenishment
             @instructionId = @instructionId,
             @ShowMsg       = 0
            
            if exists(select top 1 1
                        from Instruction        i (nolock)
                        join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
                        join Status             s (nolock) on i.StatusId           = s.StatusId
                        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
                       where s.StatusCode in ('W','S')
                         and i.WarehouseId          = @WarehouseId       -- Same Warehouse (not really necessary)
                         and i.StoreLocationId      = @OldPickLocationId -- Same to Location
                         and sub.StorageUnitId      = @StorageUnitId     -- Same product
                         and it.InstructionTypeCode = 'R')               -- Replenishment
            begin
              select @PickLocationId = @OldPickLocationId
              select @OldPickLocationId = -1
            end
          end
          else
          begin
            exec @Error = p_Instruction_Reference
             @Instructionid = @InstructionId
          end
        end
      end
    end
--    if dbo.ufn_Configuration(164, @WarehouseId) = 1
--    begin
--      if @InstructionTypeCode in ('PM','PS','FM')
--      begin
--      select @ActualQuantity = ActualQuantity
--        from StorageUnitBatchLocation
--       where StorageUnitBatchId = @StorageUnitBatchId
--         and LocationId = @PickLocationId
--      
--      if @ActualQuantity is null
--        set @ActualQuantity = 0
--      
--        if (select Batch
--              from StorageUnitBatch sub (nolock)
--              join Batch              b (nolock) on sub.BatchId = b.BatchId
--             where sub.StorageUnitBatchId = @StorageUnitBatchId) = 'Default'
--        or @ActualQuantity <= 0
--        begin
--          exec @Error = p_Instruction_Update
--           @InstructionId      = @instructionId,
--           @PickLocationId     = @PickLocationId,
--           @StorageUnitBatchId = @StorageUnitBatchId,
--           @ConfirmedQuantity  = 0
--          
--          if @Error <> 0
--          begin
--            set @Error = 1
--            goto result
--          end
--          
--          exec @Error = p_Mobile_Store_Transaction
--           @InstructionId      = @instructionId
--          
--          if @Error <> 0
--          begin
--            set @Error = 1
--            goto result
--          end
--        end
--      end
--    end
--    else
    if @InstructionTypeCode in ('M','P','R') or (@InstructionTypeCode in ('PM','PS','FM') and @Temporary = 1) or (@InstructionTypeCode in ('PM','FM','PS') and @createStockTake = 1)
    begin
      if @PickLocationId is null
      or (@Temporary = 0 and @PickLocationId = @OldPickLocationId and @createStockTake = 1) -- GS 2012-09-19 Add - if @createStockTake = 0 then just getting "Best" location not alternate
      begin
        nostock:
        
        exec @Error = p_Instruction_Update
         @InstructionId     = @InstructionId,
         @PickLocationId    = @PickLocationId,
         @StoreLocationId   = @StoreLocationId,
         @StatusId          = @NoStockId,
         @ConfirmedQuantity = 0
        
        if @Error <> 0
        begin
          set @Error = 1
          goto result
        end
        
        select @MoveId = InstructionId
          from Instruction      i (nolock)
          join Job              j (nolock) on i.JobId = j.JobId
          join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                                          and it.InstructionTypeCode = 'M'
          join Status           s (nolock) on j.StatusId = s.StatusId
                                          and s.StatusCode = 'PS'
         where i.InstructionRefId = @InstructionId
        
        if @MoveId is not null
        begin
          exec p_Housekeeping_Instruction_Delete @InstructionId = @MoveId
          
          if @Error <> 0
          begin
            set @Error = 1
            goto result
          end
        end
        
        if @InstructionTypeCode in ('M','P','R') 
        begin
          exec p_Instruction_Auto_Complete
           @InstructionId = @InstructionId,
           @NoStock       = 1
          
          if @Error <> 0
          begin
            set @Error = 1
            goto result
          end
          
          exec p_Instruction_Auto_Complete
           @InstructionId = @MoveId,
           @NoStock       = 1
          
          if @Error <> 0
          begin
            set @Error = 1
            goto result
          end
        end
        
        if @InstructionTypeCode in ('PM','PS','FM')
        begin
          exec p_Instruction_Reference @instructionId OUTPUT
          
          set @InstructionRefId = @instructionId
        
          --select @InstructionRefId = InstructionRefId
          --  from Instruction (nolock)
          -- where InstructionId = @InstructionId
        
          --exec @Error = p_Instruction_Update_ili
          --  @InstructionId     = @InstructionId,
          --  @InstructionRefId  = @InstructionRefId,
          --  @insConfirmed      = 0
          
          if @Error <> 0
          begin
            set @Error = 1
            goto result
          end
        end
        
        if @InstructionTypeCode in ('P')
        begin
          exec p_Wave_Planning_Release
           @WaveId           = @WaveId,
           @Replenish        = 1,
           @RepStorageUnitId = @StorageUnitId,
           @Result           = @Result output
          
          -- Don't check for an error
        end
        
        -- Check to see if all lines are no-stock and then updates job too.
        if (select count(1) from Instruction (nolock) where JobId = @JobId and StatusId != @NoStockId) = 0
        begin
          exec @Error = p_Job_Update
           @JobId    = @JobId,
           @StatusId = @NoStockId
          
          if @Error <> 0
          begin
            set @Error = 1
            goto result
          end
        end
        goto result
      end
	     
	     if @InstructionTypeCode in ('M','P','R') and @createStockTake = 1 -- GS 2012-09-19 Add - if @createStockTake = 0 then just getting "Best" location not alternate
      begin
        update Job
           set OperatorId = null
         where JobId = @JobId
        
        select @Error = @@Error
        
        if @Error <> 0
        begin
          set @Error = 1
          goto result
        end
      end
    end
    
    if @InstructionTypeCode = 'R'
    begin
      exec @Error = p_Instruction_Update
       @InstructionId      = @instructionId,
       @PickLocationId     = @PickLocationId,
       @StorageUnitBatchId = @StorageUnitBatchId,
       @Quantity           = @Quantity,
       @ConfirmedQuantity  = @Quantity 
      
      if @Error <> 0
      begin
        set @Error = 1
        goto result
      end
    end
    else
    begin
      exec @Error = p_Instruction_Update
       @InstructionId      = @instructionId,
       @StorageUnitBatchId = @StorageUnitBatchId,
       @PickLocationId     = @PickLocationId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto result
      end
    end
    end
    else
    begin
      exec @Error = p_Instruction_Reference
       @Instructionid = @InstructionId
    end
  end
  
  if @ingorePick = 1
  begin
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @instructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
  end
  
  result:
    update MobileLog
       set EndDate = getdate()
     where MobileLogId = @MobileLogId
  
    select @Error = @@Error
    
    if @ingorePick = 1
      select @Error
    return @Error
end

 
