﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Picking_Achieved_Summary
  ///   Filename       : p_Dashboard_Outbound_Picking_Achieved_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Picking_Achieved_Summary
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select convert(nvarchar(2), d.CreateDate, 108) + 'H00' as 'Legend',
           sum(d.Value) as 'Value'
      from DashboardOutboundPickingAchieved d
      join Operator o on d.OperatorId = o.OperatorId
     where d.WarehouseId = @WarehouseId
    group by convert(nvarchar(2), d.CreateDate, 108) + 'H00'
	 end
	 else
	 begin
	   truncate table DashboardOutboundPickingAchieved
	   
	   insert DashboardOutboundPickingAchieved
	         (WarehouseId,
           OperatorId,
           CreateDate,
           Legend,
           Value,
           KPI)
	   select i.WarehouseId,
           i.OperatorId,
           i.EndDate,
           null,
           SUM(i.ConfirmedQuantity),
           4400
	     from Instruction i (nolock)
	     join Job         j (nolock) on i.JobId = j.JobId
	     join Status      s (nolock) on j.StatusId = s.StatusId
	    where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and s.StatusCode in ('CK','CD','D','DC','C')
	      --and j.CheckedDate >= CONVERT(nvarchar(10), getdate(), 120)
	   group by i.WarehouseId,
             i.OperatorId,
             i.EndDate
	 end
end
