﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Consolidation_Slip_test
  ///   Filename       : p_Report_Consolidation_Slip_test.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Consolidation_Slip_test 
(  
 @OutboundShipmentId int,  
 @IssueId            int  
)  
   
as  
begin  
  set nocount on;  
    
  declare @TableResult as table  
  (
   OutboundShipmentId              int              null,
   IssueId                         int              null,
   OutboundDocumentId              int              null,
   OrderNumber                     nvarchar(60)     null,
   ExternalCompanyId               int              null,
   ExternalCompany                 nvarchar(510)    null,
   DespatchDate                    datetime         null,
   LocationId                      int              null,
   Location                        nvarchar(30)     null,
   JobId                           int              null,
   StatusId                        int              null,
   Status                          nvarchar(100)    null,
   InstructionTypeId               int              null,
   InstructionTypeCode             nvarchar(20)     null,
   InstructionType                 nvarchar(100)    null,
   InstructionId                   int              null,
   Pallet                          nvarchar(20)     null,
   StorageUnitBatchId              int              null,
   Quantity                        float            null,
   ConfirmedQuantity               float            null,
   ShortQuantity                   float            null,
   QuantityOnHand                  float            null,
   DropSequence                    int              null,
   RouteId                         int              null,
   Route                           nvarchar(100)    null,
   Weight                          float            null,
   ReferenceNumber                 nvarchar(60)     null,
   LineLocationId                  int              null,
   LineLocation                    nvarchar(30)     null,
   PickerId                        int              null,
   Picker                          nvarchar(100)    null,
   CheckerId                       int              null,
   Checker                         nvarchar(100)    null,
   Comment                         nvarchar(100)    null,
   StartDate                       datetime         null,
   CompleteDate                    datetime         null,
   Jobs                            int              null,
   OutboundDocumentType            nvarchar(60)     null,
   JobConfirmedQty                 int              null,
   ManifestNumber				   int				null
  )
    
  declare @startdate  datetime,  
          @enddate    datetime,
          @JobId      int
  
  if @OutboundShipmentId = -1  
    set @OutboundShipmentId = null  
    
  if @IssueId = -1  
    set @IssueId = null  
    
  --if @OutboundShipmentId is not null  
  --  set @IssueId = null  
    
  insert @TableResult  
        (OutboundShipmentId,  
         OutboundDocumentId,  
         JobId,  
         StatusId,  
         InstructionTypeId,  
         InstructionId,  
         StorageUnitBatchId,  
         Quantity,  
         ConfirmedQuantity,  
         ShortQuantity,  
         LineLocationId,  
         PickerId,  
         StartDate,  
         CompleteDate,  
         RouteId,
         Jobs,
         DropSequence,
         ReferenceNumber,
         CheckerId,
         Weight,
         ManifestNumber)
  select distinct  i.OutboundShipmentId,  
         ili.OutboundDocumentId,  
         i.JobId,  
         i.StatusId,  
         i.InstructionTypeId,  
         i.InstructionId,  
         i.StorageUnitBatchId,  
         i.Quantity,  
         isnull(ili.ConfirmedQuantity, 0),
         ili.Quantity - isnull(ili.ConfirmedQuantity, 0),
         i.StoreLocationId,
         i.OperatorId,
         i.StartDate,
         i.EndDate,
         iss.RouteId,
         j.Pallets,
         j.DropSequence,
         j.ReferenceNumber,  
         j.CheckedBy,
         j.Weight,
         iss.WaveId
    from Instruction            i (nolock)
    join Job                    j (nolock) on i.JobId = j.JobId
    left
    join Location           store (nolock) on i.StoreLocationId = store.LocationId
    left
    join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
    join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
    join Issue                iss (nolock) on ili.IssueId = iss.IssueId 
   where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))  
     and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
     and i.ConfirmedQuantity > 0
  
  select @JobId = MIN(JobId)
    from @TableResult
  
  exec p_Job_Renumber @JobId
  
  update tr  
     set DespatchDate = os.ShipmentDate,  
         --RouteId      = os.RouteId,  
         LocationId   = os.LocationId  
    from @TableResult     tr  
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId  
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId  
   where tr.OutboundShipmentId is not null
   
   update tr  
      set Picker = o.Operator  
     from @TableResult     tr  
     join Operator o (nolock) on tr.PickerId = o.OperatorId  
   
   update tr  
      set Checker = o.Operator  
     from @TableResult     tr  
     join Operator o (nolock) on tr.CheckerId = o.OperatorId  
   
   update tr  
      set LineLocation = l.Location  
     from @TableResult     tr  
     join Location l (nolock) on tr.LineLocationId = l.LocationId
   
   update tr  
      set LineLocation = tr2.LineLocation
     from @TableResult     tr
     join @TableResult     tr2 on tr.JobId = tr2.JobId
    where tr.LineLocation is null
      and tr2.LineLocation is not null
    
  update tr  
     set DespatchDate = i.DeliveryDate,  
         RouteId      = i.RouteId,  
         LocationId   = i.LocationId  
    from @TableResult tr  
    join Issue         i (nolock) on tr.IssueId = i.IssueId  
   where tr.OutboundShipmentId is null  
    
  update tr  
     set Route = r.Route  
    from @TableResult tr  
    join Route         r (nolock) on tr.RouteId = r.RouteId  
    
  update tr  
     set OrderNumber          = od.OrderNumber,  
         ExternalCompanyId    = od.ExternalCompanyId,  
         OutboundDocumentType = odt.OutboundDocumentType  
    from @TableResult               tr  
    join OutboundDocument           od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId  
    left join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId  
  
  update tr  
     set ExternalCompany = ec.ExternalCompany  
    from @TableResult    tr  
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId  
    
  update tr  
     set Location = l.Location  
    from @TableResult    tr  
    join Location         l (nolock) on tr.LocationId = l.LocationId
      
  update tr  
     set Weight = tr.Quantity * p.Weight  
    from @TableResult      tr  
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId  
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId  
   where p.Quantity = 1
     and tr.Weight is null
  
  update tr  
     set InstructionType     = 'Full',  
         InstructionTypeCode = it.InstructionTypeCode  
    from @TableResult tr  
    join InstructionType it on tr.InstructionTypeId = it.InstructionTypeId  
   where it.InstructionTypeCode = 'P'  
  
  update @TableResult  
     set InstructionType     = 'Mixed'  
   where InstructionType is null
  
  update @TableResult
     set Pallet = convert(nvarchar(10), DropSequence)  
               + ' of '  
               + convert(nvarchar(10), Jobs)
  
  set @startdate = (select MIN(StartDate) from @TableResult)  
  set @enddate   = (select MAX(CompleteDate) from @TableResult)
  
  update tr
     set JobConfirmedQty = (select sum(ConfirmedQuantity)
                      from @TableResult tr2
                     where tr.jobid = tr2.jobid)
    from @TableResult tr
  
  update tr  
     set Status = s.Status
    from @TableResult tr  
    join Status s on s.StatusId = tr.StatusId  
    
  update tr  
     set Status = 'No Stock'  
    from @TableResult tr  
   where JobConfirmedQty = 0
  
  select OutboundShipmentId,  
         DropSequence,  
         Route,  
         Weight,
         DespatchDate,  
         ExternalCompany,  
         Location,  
         min(InstructionType) as 'InstructionType',  
         JobId,  
         Status,  
         OrderNumber,  
         Pallet,  
         ReferenceNumber,  
         LineLocation,  
         Picker,  
         Checker,  
         @startdate as StartDate,  
         @enddate as CompleteDate,  
         Jobs,  
         OutboundDocumentType,
         ManifestNumber  
    from @TableResult
  group by OutboundShipmentId,  
         DropSequence,  
         Route,  
         Weight,
         DespatchDate,  
         ExternalCompany,  
         Location,  
         JobId,  
         Status,  
         OrderNumber,  
         Pallet,  
         ReferenceNumber,  
         LineLocation,  
         Picker,  
         Checker,  
         Jobs,  
         OutboundDocumentType,
         ManifestNumber  
  order by OutboundShipmentId,  
           DropSequence,  
           JobId  
end  
 
 
 
