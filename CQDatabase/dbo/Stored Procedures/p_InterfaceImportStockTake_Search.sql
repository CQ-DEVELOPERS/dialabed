﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportStockTake_Search
  ///   Filename       : p_InterfaceImportStockTake_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:19
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportStockTake table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportStockTake.InterfaceImportStockTakeId,
  ///   InterfaceImportStockTake.Location,
  ///   InterfaceImportStockTake.ProductCode,
  ///   InterfaceImportStockTake.Description,
  ///   InterfaceImportStockTake.Batch,
  ///   InterfaceImportStockTake.CQQty,
  ///   InterfaceImportStockTake.Count1,
  ///   InterfaceImportStockTake.Count2,
  ///   InterfaceImportStockTake.Count3,
  ///   InterfaceImportStockTake.NewItemCode,
  ///   InterfaceImportStockTake.NewDescription,
  ///   InterfaceImportStockTake.CurrentCount,
  ///   InterfaceImportStockTake.SKUCode,
  ///   InterfaceImportStockTake.Area,
  ///   InterfaceImportStockTake.RecordStatus,
  ///   InterfaceImportStockTake.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportStockTake_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportStockTake.InterfaceImportStockTakeId
        ,InterfaceImportStockTake.Location
        ,InterfaceImportStockTake.ProductCode
        ,InterfaceImportStockTake.Description
        ,InterfaceImportStockTake.Batch
        ,InterfaceImportStockTake.CQQty
        ,InterfaceImportStockTake.Count1
        ,InterfaceImportStockTake.Count2
        ,InterfaceImportStockTake.Count3
        ,InterfaceImportStockTake.NewItemCode
        ,InterfaceImportStockTake.NewDescription
        ,InterfaceImportStockTake.CurrentCount
        ,InterfaceImportStockTake.SKUCode
        ,InterfaceImportStockTake.Area
        ,InterfaceImportStockTake.RecordStatus
        ,InterfaceImportStockTake.ProcessedDate
    from InterfaceImportStockTake
  
end
