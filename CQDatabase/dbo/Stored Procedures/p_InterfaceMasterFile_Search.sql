﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMasterFile_Search
  ///   Filename       : p_InterfaceMasterFile_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:29:21
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceMasterFile table.
  /// </remarks>
  /// <param>
  ///   @MasterFileId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceMasterFile.MasterFileId,
  ///   InterfaceMasterFile.MasterFile,
  ///   InterfaceMasterFile.MasterFileDescription,
  ///   InterfaceMasterFile.LastDate,
  ///   InterfaceMasterFile.SendFlag 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMasterFile_Search
(
 @MasterFileId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @MasterFileId = '-1'
    set @MasterFileId = null;
  
 
  select
         InterfaceMasterFile.MasterFileId
        ,InterfaceMasterFile.MasterFile
        ,InterfaceMasterFile.MasterFileDescription
        ,InterfaceMasterFile.LastDate
        ,InterfaceMasterFile.SendFlag
    from InterfaceMasterFile
   where isnull(InterfaceMasterFile.MasterFileId,'0')  = isnull(@MasterFileId, isnull(InterfaceMasterFile.MasterFileId,'0'))
  
end
