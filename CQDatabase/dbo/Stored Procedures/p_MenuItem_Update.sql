﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Update
  ///   Filename       : p_MenuItem_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 16:06:09
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the MenuItem table.
  /// </remarks>
  /// <param>
  ///   @MenuItemId int = null,
  ///   @MenuId int = null,
  ///   @MenuItem nvarchar(100) = null,
  ///   @ToolTip nvarchar(510) = null,
  ///   @NavigateTo nvarchar(510) = null,
  ///   @ParentMenuItemId int = null,
  ///   @OrderBy smallint = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_Update
(
 @MenuItemId int = null,
 @MenuId int = null,
 @MenuItem nvarchar(100) = null,
 @ToolTip nvarchar(510) = null,
 @NavigateTo nvarchar(510) = null,
 @ParentMenuItemId int = null,
 @OrderBy smallint = null 
)
 
as
begin
	 set nocount on;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @MenuItem = '-1'
    set @MenuItem = null;
  
  if @ParentMenuItemId = '-1'
    set @ParentMenuItemId = null;
  
	 declare @Error int
 
  update MenuItem
     set MenuId = isnull(@MenuId, MenuId),
         MenuItem = isnull(@MenuItem, MenuItem),
         ToolTip = isnull(@ToolTip, ToolTip),
         NavigateTo = isnull(@NavigateTo, NavigateTo),
         ParentMenuItemId = isnull(@ParentMenuItemId, ParentMenuItemId),
         OrderBy = isnull(@OrderBy, OrderBy) 
   where MenuItemId = @MenuItemId
  
  select @Error = @@Error
  
  
  return @Error
  
end
