﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Document_Import
  ///   Filename       : p_Outbound_Document_Import.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Sep 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Document_Import
(
 @xmlstring nvarchar(max)
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Outbound_Document_Import',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @DocHandle         int,
          @Xmldoc            xml,
          @Facility                nvarchar(255),
          @ShipmentOrderNumber     nvarchar(255),
          @OrderType               nvarchar(255),
          @Owner                   nvarchar(255),
          @ScheduledShipDate       nvarchar(255),
          @Consignee               nvarchar(255),
          @ShipTo                  nvarchar(255),
          @CustomerOrderReference  nvarchar(255),
          @OrderLineNumber         nvarchar(255),
          @RequestedQuantity       nvarchar(255),
          @RequestedUOM            nvarchar(255),
          @RequestedBatch          nvarchar(255),
          @InterfaceImportHeaderId int,
          @DeliveryAddress1        nvarchar(255),
          @DeliveryAddress2        nvarchar(255),
          @DeliveryAddress3        nvarchar(255),
          @DeliveryAddress4        nvarchar(255),
          @ContactPerson           nvarchar(100),
          @ContactNumber           nvarchar(50),
          @Reference1              nvarchar(50),
          @Reference2              nvarchar(50),
          @Incoterms               nvarchar(50),
          @ProductCode             nvarchar(50),
          @Product                 nvarchar(255),
          @SkuCode                 nvarchar(50),
          @SKU                     nvarchar(255),
          @Batch                   nvarchar(50),
          @BillOfEntry             nvarchar(50),
          @BOELineNumber           nvarchar(50),
          @AlternativeSKU          nvarchar(50),
          @TariffCode              nvarchar(50),
          @ExpiryDate              nvarchar(50),
          @UnitPrice               nvarchar(50),
          @rowcount                int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
   
  select @GetDate = dbo.ufn_Getdate()
  
  set @Xmldoc = convert(xml,@xmlstring)
   
  EXEC sp_xml_preparedocument @DocHandle OUTPUT, @Xmldoc
   
  select @Facility                 = Facility               ,
         @ShipmentOrderNumber      = ShipmentOrderNumber    ,
         @OrderType                = OrderType              ,
         @Owner                    = Owner                  ,
         @ScheduledShipDate        = case when isdate(ScheduledShipDate) = 1
                                         then ScheduledShipDate
                                         else null
                                         end,
         @Consignee                = Consignee              ,
         @ShipTo                   = ShipTo                 ,
         @CustomerOrderReference   = CustomerOrderReference ,
         @BillOfEntry              = BillOfEntry,
         @OrderLineNumber          = OrderLineNumber        ,
         @ProductCode             = ProductCode,
         @Product                 = Product,
         @SkuCode                 = SkuCode,
         @Sku                     = Sku,
         @RequestedQuantity        = RequestedQuantity      ,
         @RequestedUOM             = RequestedUOM           ,
         @RequestedBatch           = RequestedBatch         ,
         
         @DeliveryAddress1         = DeliveryAddress1,
         @DeliveryAddress2         = DeliveryAddress2,
         @DeliveryAddress3         = DeliveryAddress3,
         @DeliveryAddress4         = DeliveryAddress4,
         @ContactPerson            = ContactPerson,
         @ContactNumber            = ContactNumber,
         @Reference1               = Reference1,
         @Reference2               = Reference2,
         @Incoterms                = Incoterms,
         
         @BOELineNumber            = BOELineNumber,
         @AlternativeSKU           = AlternativeSKU,
         @UnitPrice               = case when isnumeric(UnitPrice) = 1
                                         then UnitPrice
                                         end,
         @TariffCode               = TariffCode,
         @ExpiryDate               = ExpiryDate
  
  from OPENXML (@Dochandle, '/root/Line',1)
              WITH (Facility                 nvarchar(255) 'Facility'               ,
                    ShipmentOrderNumber      nvarchar(255) 'ShipmentOrderNumber'    ,
                    OrderType                nvarchar(255) 'OrderType'              ,
                    Owner                    nvarchar(255) 'Owner'                  ,
                    ScheduledShipDate        nvarchar(255) 'ScheduledShipDate'      ,
                    Consignee                nvarchar(255) 'Consignee'              ,
                    ShipTo                   nvarchar(255) 'ShipTo'                 ,
                    CustomerOrderReference   nvarchar(255) 'CustomerOrderReference' ,
                    BillOfEntry              nvarchar(255) 'BillOfEntry'            ,
                    OrderLineNumber          nvarchar(255) 'OrderLineNumber'        ,
                    ProductCode nvarchar(50) 'ProductCode',
                    Product nvarchar(50) 'Product',
                    SkuCode nvarchar(50) 'SkuCode',
                    SKU nvarchar(50) 'SKU',
                    RequestedQuantity        nvarchar(255) 'RequestedQuantity'      ,
                    RequestedUOM             nvarchar(255) 'RequestedUOM'           ,
                    RequestedBatch           nvarchar(255) 'RequestedBatch'         ,
                    
                    
                    DeliveryAddress1         nvarchar(255) 'DeliveryAddress1',
                    DeliveryAddress2         nvarchar(255) 'DeliveryAddress2',
                    DeliveryAddress3         nvarchar(255) 'DeliveryAddress3',
                    DeliveryAddress4         nvarchar(255) 'DeliveryAddress4',
                    ContactPerson            nvarchar(100) 'ContactPerson',
                    ContactNumber            nvarchar(50)  'ContactNumber',
                    Reference1               nvarchar(50)  'Reference1',
                    Reference2               nvarchar(50)  'Reference2',
                    Incoterms                nvarchar(50)  'Incoterms',
              
                    BOELineNumber            nvarchar(50) 'BOELineNumber',
                    AlternativeSKU           nvarchar(50) 'AlternativeSKU',
                    UnitPrice                nvarchar(50) 'UnitPrice',
                    TariffCode               nvarchar(50) 'TariffCode',
                    ExpiryDate               nvarchar(50) 'ExpiryDate'
                    )
  
  --select @Facility,
  --       @ShipmentOrderNumber,
  --       @OrderType,
  --       @Owner,
  --       @ScheduledShipDate,
  --       @Consignee,
  --       @ShipTo,
  --       @CustomerOrderReference,
  --       @OrderLineNumber,
  --       @SkuCode,
  --       @RequestedQuantity,
  --       @RequestedUOM,
  --       @RequestedBatch
  
  select @InterfaceImportHeaderId = max(InterfaceImportHeaderId)
    from InterfaceImportHeader
   where OrderNumber = @ShipmentOrderNumber
     and PrincipalCode   = @Owner
     and InsertDate  > dateadd(mi, -1, @getdate)
     and RecordType in ('SAL',@OrderType)
     and isnull(Module, 'Outbound') = 'Outbound'
  
  if @InterfaceImportHeaderId is null
  begin
    insert InterfaceImportHeader
          (FromWarehouseCode,
           OrderNumber,
           PrimaryKey,
           RecordType,
           PrincipalCode,
           DeliveryDate,
           CompanyCode,
           Address,
           Remarks,
           Module,
           BillOfEntry)
    select @Facility,
           @ShipmentOrderNumber,
           @ShipmentOrderNumber,
           @OrderType,
           @Owner,
           @ScheduledShipDate,
           @Consignee,
           @ShipTo,
           @CustomerOrderReference,
           'Outbound',
           @BillOfEntry
    
    select @Error = @@Error, @InterfaceImportHeaderId = scope_identity()
    
    if @Error <> 0
      goto error
  end
  
  insert InterfaceImportDetail
        (InterfaceImportHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         Quantity,
         BOELineNumber,
         AlternativeSKU,
         TariffCode,
         ExpiryDate,
         UnitPrice)
  select @InterfaceImportHeaderId,
         @ShipmentOrderNumber,
         @OrderLineNumber,
         @ProductCode,
         @Product,
         @SkuCode,
         @SKU,
         @RequestedBatch,
         @RequestedQuantity,
         @BOELineNumber,
         @AlternativeSKU,
         @TariffCode,
         @ExpiryDate,
         @UnitPrice
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  if @OrderType = 'BOND'
  begin
    set @InterfaceImportHeaderId = null
    
    set @ShipmentOrderNumber = @ShipmentOrderNumber + '-REC'
    
    select @InterfaceImportHeaderId = max(InterfaceImportHeaderId)
      from InterfaceImportHeader
     where OrderNumber   = @ShipmentOrderNumber
       and PrincipalCode = @Owner
       and InsertDate    > dateadd(mi, -1, @getdate)
       and RecordType    = 'Standard' -- Create Standard Document
       and Module        = 'Inbound' -- Create Inbound Document
    
    if @InterfaceImportHeaderId is null
    begin
      insert InterfaceImportHeader
            (FromWarehouseCode,
             OrderNumber,
             PrimaryKey,
             RecordType,
             PrincipalCode,
             DeliveryDate,
             CompanyCode,
             Address,
             Remarks,
             Module)
      select @Facility,
             @ShipmentOrderNumber,
             @ShipmentOrderNumber,
             'Standard',
             @Owner,
             @ScheduledShipDate,
             @Consignee,
             @ShipTo,
             @CustomerOrderReference,
             'Inbound'
      
      select @Error = @@Error, @InterfaceImportHeaderId = scope_identity()
      
      if @Error <> 0
        goto error
    end
    
    update InterfaceImportDetail
       set Quantity = Quantity + @RequestedQuantity
     where InterfaceImportHeaderId = @InterfaceImportHeaderId
       and ProductCode = @ProductCode
       and SkuCode = @SkuCode
       and Batch = @RequestedBatch
      
      select @Error = @@Error, @rowcount = @@ROWCOUNT
      
      if @Error <> 0
        goto error
    
    if @rowcount = 0
    begin
      insert InterfaceImportDetail
            (InterfaceImportHeaderId,
             ForeignKey,
             LineNumber,
             ProductCode,
             Product,
             SKUCode,
             SKU,
             Batch,
             Quantity,
             BOELineNumber,
             AlternativeSKU,
             TariffCode,
             ExpiryDate,
             UnitPrice)
      select @InterfaceImportHeaderId,
             @ShipmentOrderNumber,
             @OrderLineNumber,
             @ProductCode,
             @Product,
             @SkuCode,
             @SKU,
             @RequestedBatch,
             @RequestedQuantity,
             null,
             @AlternativeSKU,
             @TariffCode,
             @ExpiryDate,
             @UnitPrice
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
    end
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
