﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Description_Value_String
  ///   Filename       : p_Configuration_Description_Value_String.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Jan 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Description_Value_String
(
 @Configuration nvarchar(50),
 @WarehouseId int = 1,
 @Value varchar(255) output
)
 
as
begin
	 set nocount on;
  

  
  select @Value = Convert(varchar(255),Value)
    from Configuration (nolock)
   where Configuration = @Configuration
     and WarehouseId = @WarehouseId
  
  if @Value is null
    set @Value = ''
  
  select @Value as 'Value'
end

