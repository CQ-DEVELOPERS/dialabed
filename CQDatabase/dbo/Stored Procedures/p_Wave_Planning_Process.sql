﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Process
  ///   Filename       : p_Wave_Planning_Process.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Process
 
as
begin
  set nocount on;
  
  declare @TableReleaseOrders as table
  (
   WaveId int
  )
  
  declare @TableReturnToStock as table
  (
   WaveId int
  )
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Wave_Planning_Process',
          @GetDate           datetime,
          @Transaction       bit = 1,
          @WaveId            int
  
  select @GetDate = dbo.ufn_Getdate()
  
  --if @@trancount = 0
  --  begin
  --    begin transaction
  --    set @Transaction = 1
  --  end
  
  insert @TableReleaseOrders
        (WaveId)
  select WaveId
    from Wave w
   where DATEDIFF(dd, -1, @getdate) > CompleteDate
     and ReleasedOrders is null
  
  while exists(select top 1 1 
                 from @TableReleaseOrders)
  begin
    set @WaveId = null
    
    select top 1 @WaveId = WaveId
      from @TableReleaseOrders
    
    delete @TableReleaseOrders
     where WaveId = @WaveId
    
    --exec @Error = p_Wave_Planning_Release_Linked
    -- @WaveId = @WaveId
    
    --if @Error <> 0  
    --  goto error
    
    update j
       set StatusId = dbo.ufn_StatusId('IS','RL')
      from IssueLineInstruction ili (nolock)
      join Issue                iss (nolock) on ili.IssueId = iss.IssueId
      join OutboundDocument      od (nolock) on iss.OutboundDocumentId = od.OutboundDocumentId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                            and odt.OutboundDocumentTypeCode != 'WAV'
      join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
      join Job                    j (nolock) on i.JobId = j.JobId
      join Status                 s (nolocK) on j.StatusId = s.StatusId
                                            and s.StatusCode = 'PS'
      join AreaLocation          al (nolock) on i.PickLocationId = al.LocationId
      join Area                   a (nolock) on al.AreaId = a.AreaId
                                            and a.Area like '%Wave%'
     where iss.WaveId = @WaveId
    
    exec @Error = p_Wave_Update
     @WaveId         = @WaveId,
     @ReleasedOrders = @GetDate
    
    if @Error <> 0  
      goto error
  end
  
  insert @TableReturnToStock
        (WaveId)
  select WaveId
    from Wave w
   where DATEDIFF(dd, -1, @getdate) > ReleasedOrders
     and PutawayDate is null
  
  delete rts
    from @TableReturnToStock  rts
    join Issue                  i (nolock) on rts.WaveId                    = i.WaveId
    join Status                 s (nolock) on i.StatusId                    = s.StatusId
                                          and s.StatusCode                 in ('W','PC','RL','NS','CK')
    join OutboundDocument      od (nolock) on i.OutboundDocumentId          = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId     = odt.OutboundDocumentTypeId
                                          and odt.OutboundDocumentTypeCode != 'WAV'
  
  while exists(select top 1 1 
                 from @TableReturnToStock)
  begin
    set @WaveId = null
    
    select top 1 @WaveId = WaveId
      from @TableReturnToStock
    
    delete @TableReturnToStock
     where WaveId = @WaveId
    
    exec @Error = p_Wave_Planning_Move_Items_Putaway
     @WaveId = @WaveId
    
    if @Error <> 0  
      goto error
    
    exec @Error = p_Wave_Update
     @WaveId         = @WaveId,
     @PutawayDate    = @GetDate
    
    if @Error <> 0  
      goto error
  end
  
  if @Error <> 0
    goto error
  
  result:
      --if @Transaction = 1
      --  commit transaction
      return 0
    
    error:
      --if @Transaction = 1
      --begin
        RAISERROR (@Errormsg,11,1)
      --  rollback transaction
      --end
      return @Error
end
