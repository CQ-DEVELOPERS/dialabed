﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLinePackaging_Insert
  ///   Filename       : p_IssueLinePackaging_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:54
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IssueLinePackaging table.
  /// </remarks>
  /// <param>
  ///   @PackageLineId int = null output,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @JobId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   IssueLinePackaging.PackageLineId,
  ///   IssueLinePackaging.IssueId,
  ///   IssueLinePackaging.IssueLineId,
  ///   IssueLinePackaging.JobId,
  ///   IssueLinePackaging.StorageUnitBatchId,
  ///   IssueLinePackaging.StorageUnitId,
  ///   IssueLinePackaging.StatusId,
  ///   IssueLinePackaging.OperatorId,
  ///   IssueLinePackaging.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLinePackaging_Insert
(
 @PackageLineId int = null output,
 @IssueId int = null,
 @IssueLineId int = null,
 @JobId int = null,
 @StorageUnitBatchId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
  if @PackageLineId = '-1'
    set @PackageLineId = null;
  
	 declare @Error int
 
  insert IssueLinePackaging
        (IssueId,
         IssueLineId,
         JobId,
         StorageUnitBatchId,
         StorageUnitId,
         StatusId,
         OperatorId,
         Quantity)
  select @IssueId,
         @IssueLineId,
         @JobId,
         @StorageUnitBatchId,
         @StorageUnitId,
         @StatusId,
         @OperatorId,
         @Quantity 
  
  select @Error = @@Error, @PackageLineId = scope_identity()
  
  
  return @Error
  
end
