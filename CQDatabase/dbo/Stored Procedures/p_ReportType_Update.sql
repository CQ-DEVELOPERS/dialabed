﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportType_Update
  ///   Filename       : p_ReportType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 15:24:49
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ReportType table.
  /// </remarks>
  /// <param>
  ///   @ReportTypeId int = null,
  ///   @ReportType nvarchar(100) = null,
  ///   @ReportTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportType_Update
(
 @ReportTypeId int = null,
 @ReportType nvarchar(100) = null,
 @ReportTypeCode nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @ReportTypeId = '-1'
    set @ReportTypeId = null;
  
  if @ReportType = '-1'
    set @ReportType = null;
  
  if @ReportTypeCode = '-1'
    set @ReportTypeCode = null;
  
	 declare @Error int
 
  update ReportType
     set ReportType = isnull(@ReportType, ReportType),
         ReportTypeCode = isnull(@ReportTypeCode, ReportTypeCode) 
   where ReportTypeId = @ReportTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
