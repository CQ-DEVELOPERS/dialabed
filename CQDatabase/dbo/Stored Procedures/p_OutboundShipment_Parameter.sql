﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipment_Parameter
  ///   Filename       : p_OutboundShipment_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Oct 2013 09:31:15
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundShipment table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OutboundShipment.OutboundShipmentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipment_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as OutboundShipmentId
        ,null as 'OutboundShipment'
  union
  select
         OutboundShipment.OutboundShipmentId
        ,OutboundShipment.OutboundShipmentId as 'OutboundShipment'
    from OutboundShipment
  
end
