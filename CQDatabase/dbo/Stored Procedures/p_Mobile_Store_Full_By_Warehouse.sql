﻿/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Store_Full_By_Warehouse
  ///   Filename       : p_Mobile_Store_Full_By_Warehouse.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Mobile_Store_Full_By_Warehouse (
	@operatorId INT
	,@palletId INT
	,@warehouseId INT
	,@instructionId INT OUTPUT
	)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @MobileLogId INT

	INSERT MobileLog (
		ProcName
		,InstructionId
		,PalletId
		,OperatorId
		,StartDate
		)
	SELECT OBJECT_NAME(@@PROCID)
		,@InstructionId
		,@PalletId
		,@OperatorId
		,getdate()

	SELECT @MobileLogId = scope_identity()

	DECLARE @Error INT
		,@Errormsg NVARCHAR(500)
		,@GetDate DATETIME
		,@Rowcount INT
		,@StatusId INT
		,@JobId INT
		,@PickLocationId INT
		,@StoreLocationId INT
		,@OldInstructionId INT
		,@InstructionTypeCode NVARCHAR(10)
		,@Exception NVARCHAR(255)
		,@ReceiptLineId INT
		,@OverrideStore BIT
		,@PalletStatusId INT

	SELECT @GetDate = dbo.ufn_Getdate()

	SELECT @PalletStatusId = p.StatusId
	FROM Pallet p
	INNER JOIN Instruction ins ON p.PalletId = ins.PalletId
	WHERE p.PalletId = @palletId AND ins.WarehouseId = @warehouseId

	IF @PalletStatusId = dbo.ufn_StatusId('P', 'NA')
	BEGIN
		BEGIN TRANSACTION

		SET @InstructionId = - 4

		GOTO error
	END

	SELECT TOP 1 @InstructionId = i.InstructionId
		,@JobId = i.JobId
		,@PalletId = i.PalletId
		,@PickLocationId = i.PickLocationId
		,@StoreLocationId = i.StoreLocationId
		,@ReceiptLineId = i.ReceiptLineId
		,@OverrideStore = i.OverrideStore
	FROM Instruction i(NOLOCK)
	JOIN InstructionType it(NOLOCK) ON i.InstructionTypeId = it.InstructionTypeId
	JOIN Job j /*lock*/ ON i.JobId = j.JobId
	JOIN STATUS sj(NOLOCK) ON j.StatusId = sj.StatusId
	JOIN STATUS si(NOLOCK) ON i.StatusId = si.StatusId
	WHERE i.PalletId = @PalletId AND i.WarehouseId = @warehouseId AND ((it.InstructionTypeCode = 'PR' AND sj.StatusCode IN ('A', 'S', 'R', 'PR')) OR (it.InstructionTypeCode IN ('S', 'SM', 'M') AND sj.StatusCode IN ('PR', 'A', 'S', 'R')) OR (it.InstructionTypeCode = 'M' AND sj.StatusCode IN ('W', 'S'))) AND si.StatusCode IN ('W', 'S')
	ORDER BY CASE WHEN i.StoreLocationId IS NOT NULL THEN 0 ELSE 1 END
		,i.InstructionId

	SELECT @Rowcount = @@rowcount

	IF @Rowcount = 0
	BEGIN
		SELECT TOP 1 @InstructionId = i.InstructionId
			,@JobId = i.JobId
			,@PalletId = i.PalletId
			,@PickLocationId = i.PickLocationId
			,@StoreLocationId = i.StoreLocationId
			,@ReceiptLineId = i.ReceiptLineId
			,@OverrideStore = i.OverrideStore
		FROM Instruction i(NOLOCK)
		JOIN InstructionType it(NOLOCK) ON i.InstructionTypeId = it.InstructionTypeId
		JOIN Job j /*lock*/ ON i.JobId = j.JobId
		JOIN STATUS sj(NOLOCK) ON j.StatusId = sj.StatusId
		JOIN STATUS si(NOLOCK) ON i.StatusId = si.StatusId
		WHERE (i.PalletId = @palletId OR j.ReferenceNumber = 'J:' + convert(NVARCHAR(10), @PalletId)) AND i.WarehouseId = @warehouseId AND it.InstructionTypeCode IN ('S', 'PR', 'SM', 'M') AND sj.StatusCode IN ('PR', 'A', 'S') AND si.StatusCode IN ('W', 'S')
		ORDER BY i.InstructionId

		SELECT @Rowcount = @@rowcount
	END

	IF @Rowcount = 0
	BEGIN
		BEGIN TRANSACTION

		SET @InstructionId = - 1
		SET @Error = 2

		GOTO error
	END

	UPDATE ReceiptLine
	SET PutawayStarted = @GetDate
	WHERE ReceiptLineId = @ReceiptLineId

	IF (
			SELECT dbo.ufn_Configuration(387, @WarehouseId)
			) = 1
	BEGIN
		UPDATE Instruction
		SET AutoComplete = 1
		WHERE JobId = @JobId
	END

	--if dbo.ufn_Configuration(238, @WarehouseId) = 1 -- moved lower down
	BEGIN
		SELECT @OldInstructionId = i.InstructionId
			,@JobId = i.JobId
		FROM Instruction i(NOLOCK)
		JOIN InstructionType it(NOLOCK) ON i.InstructionTypeId = it.InstructionTypeId
		JOIN Job j /*lock*/ ON i.JobId = j.JobId
		JOIN STATUS sj(NOLOCK) ON j.StatusId = sj.StatusId
		JOIN STATUS si(NOLOCK) ON i.StatusId = si.StatusId
		WHERE j.OperatorId = @operatorId AND j.JobId != @JobId AND it.InstructionTypeCode IN ('S', 'PR', 'SM', 'M') AND sj.StatusCode IN ('S') --('PR','A','S')
			AND si.StatusCode IN ('W', 'S')

		IF @OldInstructionId IS NOT NULL
		BEGIN
			SET @StatusId = dbo.ufn_StatusId('PR', 'A')

			BEGIN TRANSACTION

			IF dbo.ufn_Configuration(238, @WarehouseId) = 0
			BEGIN
				EXEC @Error = p_Job_Update @jobId = @jobId
					,@StatusId = @StatusId

				IF @Error <> 0
					GOTO error
			END

			SELECT @Exception = 'Operator tried to start another instruction (Old ins = ' + convert(VARCHAR(10), isnull(@OldInstructionId, ''))

			EXEC @Error = p_Exception_Insert @ExceptionId = NULL
				,@ExceptionCode = 'STARTED'
				,@Exception = @Exception
				,@InstructionId = @InstructionId
				,@OperatorId = @OperatorId
				,@JobId = @JobId
				,@CreateDate = @GetDate
				,@ExceptionDate = @Getdate

			IF @Error <> 0
				GOTO error

			COMMIT TRANSACTION

			BEGIN TRANSACTION

			SET @InstructionId = - 3 --@OldInstructionId
			SET @Error = 3

			GOTO error
		END
	END

	BEGIN TRANSACTION

	IF dbo.ufn_Configuration(429, @WarehouseId) = 0 AND isnull(@OverrideStore, 0) = 0
	BEGIN
		IF @StoreLocationId IS NULL
		BEGIN
			EXEC @Error = p_Receiving_Manual_Palletisation_Auto_Locations @InstructionId = @InstructionId

			IF @Error <> 0
				GOTO error
		END

		SELECT @StoreLocationId = StoreLocationId
		FROM Instruction(NOLOCK)
		WHERE InstructionId = @InstructionId

		IF @StoreLocationId IS NULL
		BEGIN
			SET @InstructionId = - 2
			SET @Error = 2

			GOTO error
		END
	END

	SELECT @StatusId = dbo.ufn_StatusId('R', 'S')

	EXEC @Error = p_Job_Update @JobId = @JobId
		,@StatusId = @StatusId
		,@OperatorId = @operatorId

	IF @Error <> 0
		GOTO error

	EXEC @Error = p_Instruction_Started @InstructionId = @InstructionId
		,@OperatorId = @OperatorId

	IF @Error <> 0
		GOTO error

	IF dbo.ufn_Configuration(119, @WarehouseId) = 1
	BEGIN
		EXEC @Error = p_Instruction_CrossDocking @InstructionId = @InstructionId
			,@OperatorId = @OperatorId
			,@PalletId = @PalletId

		IF @Error <> 0
			GOTO error
	END

	EXEC @Error = p_Instruction_MovementSequence @InstructionId = @InstructionId
		,@operatorId = @operatorId

	IF @Error <> 0
		GOTO error

	UPDATE MobileLog
	SET EndDate = getdate()
	WHERE MobileLogId = @MobileLogId

	COMMIT TRANSACTION

	RETURN

	error:

	ROLLBACK TRANSACTION

	UPDATE MobileLog
	SET EndDate = getdate()
	WHERE MobileLogId = @MobileLogId

	RETURN @Error
END
