﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_StockTake_Monitor
  ///   Filename       : p_Report_StockTake_Monitor.sql
  ///   Create By      : Karen
  ///   Date Created   : April 2016
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_StockTake_Monitor
(
@InstructionId	int = null
)
 
as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
  
	select * 
	  from StockTakeLog stl
	 where stl.insertdate > DATEADD(mm,-6,getdate())
	   and stl.StockTakeLogCode   = 'Check Error'
	   and stl.InstructionId = @InstructionId
  

end
 
 
 
