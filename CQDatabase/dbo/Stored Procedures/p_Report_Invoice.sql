﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Invoice
  ///   Filename       : p_Report_Invoice.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Invoice
(
 @OutboundShipmentId int = null,
 @IssueId int = null
)
 
as
begin
	 set nocount on;
	 
  select h.InvoiceNumber,
         h.CustomerCode,
         h.Customer,
         h.Address as 'BillingAddress',
         h.Address as 'ShippingAddress',
         h.FromWarehouseCode,
         '11 Dan Jacobs Street Alrode 1449' as 'Address',
         '+27 11 389 4600' as 'Telephone',
         '+27 11 864 1570' as 'Fax',
         '4080165576' as 'VATNo',
         '1997/006763/07' as 'CoRegNo',
         'Alrode Branch' as 'Warehouse',
         'ABSA' as 'BankName',
         '631142' as 'BankBranch',
         '210 171 363' as 'BankAccount',
         h.DeliveryDate,
         h.Remarks,
         h.VatPercentage,
         h.VatSummary,
         h.Total,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight,
         d.RetailPrice,
         d.NetPrice,
         d.LineTotal,
         d.Volume
    from InterfaceImportIVHeader h
    join InterfaceImportIVDetail d on h.InterfaceImportIVHeaderId = d.InterfaceImportIVHeaderId
   where InvoiceNumber = '1600543030'
end
