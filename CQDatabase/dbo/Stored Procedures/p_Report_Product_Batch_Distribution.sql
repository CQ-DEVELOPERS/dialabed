﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Batch_Distribution
  ///   Filename       : p_Report_Product_Batch_Distribution.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_Batch_Distribution
(
 @WarehouseId		int,
 --@ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @ExternalCompanyId int,
 @StorageUnitId     int,
 @BatchId           int,
 @FromDate          datetime,
 @ToDate            datetime
)
 
as
begin
	 set nocount on;
  declare @TableResult as table
  (
   OrderNumber         nvarchar(30),
   ExternalCompanyId   int,
   ExternalCompanyCode nvarchar(30),
   ExternalCompany     nvarchar(255),
   StorageUnitBatchId  int,
   ProductCode         nvarchar(30),
   Product             nvarchar(50),
   SKUCode             nvarchar(50),
   SKU                 nvarchar(50),
   Batch               nvarchar(50),
   ConfirmedQuantity   float,
   DeliveryDate        datetime
  )
  
  if @ExternalCompanyId = -1
    set @ExternalCompanyId = null
  
  if @StorageUnitId = -1
    set @StorageUnitId = null
  
  if @BatchId = -1
    set @BatchId = null
  
  insert @TableResult
        (OrderNumber,
         ExternalCompanyId,
         DeliveryDate,
         StorageUnitBatchId,
         ins.ConfirmedQuantity)
  select od.OrderNumber,
         od.ExternalCompanyId,
         i.DeliveryDate,
         ins.StorageUnitBatchId,
         ins.ConfirmedQuantity
    from IssueLineInstruction ili (nolock)
    join Instruction          ins (nolock) on ili.InstructionId      = ins.InstructionId
    join Status                 s (nolock) on ins.StatusId           = s.StatusId
    join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
    join Issue                  i (nolock) on ili.IssueId            = i.IssueId
   where s.StatusCode         = 'F'
     and od.ExternalCompanyId = isnull(@ExternalCompanyId, od.ExternalCompanyId)
     and i.DeliveryDate between @FromDate and @ToDate
	    and i.Warehouseid = Isnull(@WarehouseId,i.WarehouseId)
  
  insert @TableResult
        (OrderNumber,
         ExternalCompanyId,
         DeliveryDate,
         StorageUnitBatchId,
         ins.ConfirmedQuantity)
  select od.OrderNumber,
         od.ExternalCompanyId,
         i.DeliveryDate,
         ins.StorageUnitBatchId,
         ins.ConfirmedQuantity
    from IssueLineInstruction ili (nolock)
    join Instruction          ins (nolock) on ili.InstructionId      = ins.InstructionRefId -- RefID!!!!
    join Status                 s (nolock) on ins.StatusId           = s.StatusId
    join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
    join Issue                  i (nolock) on ili.IssueId            = i.IssueId
   where s.StatusCode         = 'F'
     and od.ExternalCompanyId = isnull(@ExternalCompanyId, od.ExternalCompanyId)
     and i.DeliveryDate between @FromDate and @ToDate
	    and i.Warehouseid = Isnull(@WarehouseId,i.WarehouseId)
  
  update tr
     set ExternalCompanyCode = ec.ExternalCompanyCode,
         ExternalCompany     = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         SKU         = sku.SKU,
         Batch       = b.Batch
    from @TableResult    tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
   where su.StorageUnitId = isnull(@StorageUnitId, su.StorageUnitId)
     and b.BatchId        = isnull(@BatchId, b.BatchId)
  
  select ExternalCompanyCode,
         ExternalCompany,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ConfirmedQuantity,
         DeliveryDate,
         OrderNumber
    from @TableResult
   where ProductCode is not null
     and Batch       is not null
end
