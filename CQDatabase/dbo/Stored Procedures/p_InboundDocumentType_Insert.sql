﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentType_Insert
  ///   Filename       : p_InboundDocumentType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 10:28:15
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentTypeId int = null output,
  ///   @InboundDocumentTypeCode nvarchar(20) = null,
  ///   @InboundDocumentType nvarchar(60) = null,
  ///   @OverReceiveIndicator bit = null,
  ///   @AllowManualCreate bit = null,
  ///   @QuantityToFollowIndicator bit = null,
  ///   @PriorityId int = null,
  ///   @AutoSendup bit = null,
  ///   @BatchButton bit = null,
  ///   @BatchSelect bit = null,
  ///   @DeliveryNoteNumber bit = null,
  ///   @VehicleRegistration bit = null,
  ///   @AreaType nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   InboundDocumentType.InboundDocumentTypeId,
  ///   InboundDocumentType.InboundDocumentTypeCode,
  ///   InboundDocumentType.InboundDocumentType,
  ///   InboundDocumentType.OverReceiveIndicator,
  ///   InboundDocumentType.AllowManualCreate,
  ///   InboundDocumentType.QuantityToFollowIndicator,
  ///   InboundDocumentType.PriorityId,
  ///   InboundDocumentType.AutoSendup,
  ///   InboundDocumentType.BatchButton,
  ///   InboundDocumentType.BatchSelect,
  ///   InboundDocumentType.DeliveryNoteNumber,
  ///   InboundDocumentType.VehicleRegistration,
  ///   InboundDocumentType.AreaType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentType_Insert
(
 @InboundDocumentTypeId int = null output,
 @InboundDocumentTypeCode nvarchar(20) = null,
 @InboundDocumentType nvarchar(60) = null,
 @OverReceiveIndicator bit = null,
 @AllowManualCreate bit = null,
 @QuantityToFollowIndicator bit = null,
 @PriorityId int = null,
 @AutoSendup bit = null,
 @BatchButton bit = null,
 @BatchSelect bit = null,
 @DeliveryNoteNumber bit = null,
 @VehicleRegistration bit = null,
 @AreaType nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @InboundDocumentTypeCode = '-1'
    set @InboundDocumentTypeCode = null;
  
  if @InboundDocumentType = '-1'
    set @InboundDocumentType = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
	 declare @Error int
 
  insert InboundDocumentType
        (InboundDocumentTypeCode,
         InboundDocumentType,
         OverReceiveIndicator,
         AllowManualCreate,
         QuantityToFollowIndicator,
         PriorityId,
         AutoSendup,
         BatchButton,
         BatchSelect,
         DeliveryNoteNumber,
         VehicleRegistration,
         AreaType)
  select @InboundDocumentTypeCode,
         @InboundDocumentType,
         @OverReceiveIndicator,
         @AllowManualCreate,
         @QuantityToFollowIndicator,
         @PriorityId,
         @AutoSendup,
         @BatchButton,
         @BatchSelect,
         @DeliveryNoteNumber,
         @VehicleRegistration,
         @AreaType 
  
  select @Error = @@Error, @InboundDocumentTypeId = scope_identity()
  
  if @Error = 0
    exec @Error = p_InboundDocumentTypeHistory_Insert
         @InboundDocumentTypeId = @InboundDocumentTypeId,
         @InboundDocumentTypeCode = @InboundDocumentTypeCode,
         @InboundDocumentType = @InboundDocumentType,
         @OverReceiveIndicator = @OverReceiveIndicator,
         @AllowManualCreate = @AllowManualCreate,
         @QuantityToFollowIndicator = @QuantityToFollowIndicator,
         @PriorityId = @PriorityId,
         @AutoSendup = @AutoSendup,
         @BatchButton = @BatchButton,
         @BatchSelect = @BatchSelect,
         @DeliveryNoteNumber = @DeliveryNoteNumber,
         @VehicleRegistration = @VehicleRegistration,
         @AreaType = @AreaType,
         @CommandType = 'Insert'
  
  return @Error
  
end
