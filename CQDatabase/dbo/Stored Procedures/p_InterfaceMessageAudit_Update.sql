﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMessageAudit_Update
  ///   Filename       : p_InterfaceMessageAudit_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:34
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceMessageAudit table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMessageId int = null,
  ///   @InterfaceMessageCode nvarchar(100) = null,
  ///   @InterfaceMessage nvarchar(max) = null,
  ///   @InterfaceId int = null,
  ///   @InterfaceTable nvarchar(256) = null,
  ///   @Status nvarchar(40) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @CreateDate datetime = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMessageAudit_Update
(
 @InterfaceMessageId int = null,
 @InterfaceMessageCode nvarchar(100) = null,
 @InterfaceMessage nvarchar(max) = null,
 @InterfaceId int = null,
 @InterfaceTable nvarchar(256) = null,
 @Status nvarchar(40) = null,
 @OrderNumber nvarchar(60) = null,
 @CreateDate datetime = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceMessageId = '-1'
    set @InterfaceMessageId = null;
  
  if @InterfaceMessageCode = '-1'
    set @InterfaceMessageCode = null;
  
	 declare @Error int
 
  update InterfaceMessageAudit
     set InterfaceMessageId = isnull(@InterfaceMessageId, InterfaceMessageId),
         InterfaceMessageCode = isnull(@InterfaceMessageCode, InterfaceMessageCode),
         InterfaceMessage = isnull(@InterfaceMessage, InterfaceMessage),
         InterfaceId = isnull(@InterfaceId, InterfaceId),
         InterfaceTable = isnull(@InterfaceTable, InterfaceTable),
         Status = isnull(@Status, Status),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         CreateDate = isnull(@CreateDate, CreateDate),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         InsertDate = isnull(@InsertDate, InsertDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
