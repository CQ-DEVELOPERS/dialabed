﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocationHistory_Insert
  ///   Filename       : p_StorageUnitBatchLocationHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:02
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitBatchLocationHistory table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null,
  ///   @LocationId int = null,
  ///   @ActualQuantity float = null,
  ///   @AllocatedQuantity float = null,
  ///   @ReservedQuantity float = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @NettWeight float = null 
  /// </param>
  /// <returns>
  ///   StorageUnitBatchLocationHistory.StorageUnitBatchId,
  ///   StorageUnitBatchLocationHistory.LocationId,
  ///   StorageUnitBatchLocationHistory.ActualQuantity,
  ///   StorageUnitBatchLocationHistory.AllocatedQuantity,
  ///   StorageUnitBatchLocationHistory.ReservedQuantity,
  ///   StorageUnitBatchLocationHistory.CommandType,
  ///   StorageUnitBatchLocationHistory.InsertDate,
  ///   StorageUnitBatchLocationHistory.NettWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocationHistory_Insert
(
 @StorageUnitBatchId int = null,
 @LocationId int = null,
 @ActualQuantity float = null,
 @AllocatedQuantity float = null,
 @ReservedQuantity float = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @NettWeight float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StorageUnitBatchLocationHistory
        (StorageUnitBatchId,
         LocationId,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         CommandType,
         InsertDate,
         NettWeight)
  select @StorageUnitBatchId,
         @LocationId,
         @ActualQuantity,
         @AllocatedQuantity,
         @ReservedQuantity,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @NettWeight 
  
  select @Error = @@Error
  
  
  return @Error
  
end
