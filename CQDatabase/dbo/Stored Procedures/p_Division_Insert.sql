﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Division_Insert
  ///   Filename       : p_Division_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Oct 2011 17:20:57
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Division table.
  /// </remarks>
  /// <param>
  ///   @DivisionId int = null output,
  ///   @DivisionCode nvarchar(20) = null,
  ///   @Division nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   Division.DivisionId,
  ///   Division.DivisionCode,
  ///   Division.Division 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Division_Insert
(
 @DivisionId int = null output,
 @DivisionCode nvarchar(20) = null,
 @Division nvarchar(100) = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
  insert Division
        (DivisionCode,
         Division)
  select @DivisionCode,
         @Division 
  
  select @Error = @@Error, @DivisionId = scope_identity()
  
  if @Error = 0
    exec @Error = p_DivisionHistory_Insert
         @DivisionId = @DivisionId,
         @DivisionCode = @DivisionCode,
         @Division = @Division,
         @CommandType = 'Insert'
  
  return @Error
  
end
