﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationOperatorGroup_Search
  ///   Filename       : p_LocationOperatorGroup_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jul 2013 10:59:01
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the LocationOperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null,
  ///   @OperatorGroupId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   LocationOperatorGroup.LocationId,
  ///   Location.Location,
  ///   LocationOperatorGroup.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup,
  ///   LocationOperatorGroup.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationOperatorGroup_Search
(
 @LocationId int = null,
 @OperatorGroupId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
 
  select
         LocationOperatorGroup.LocationId
         ,LocationLocationId.Location as 'Location'
        ,LocationOperatorGroup.OperatorGroupId
         ,OperatorGroupOperatorGroupId.OperatorGroup as 'OperatorGroup'
        ,LocationOperatorGroup.OrderBy
    from LocationOperatorGroup
    left
    join Location LocationLocationId on LocationLocationId.LocationId = LocationOperatorGroup.LocationId
    left
    join OperatorGroup OperatorGroupOperatorGroupId on OperatorGroupOperatorGroupId.OperatorGroupId = LocationOperatorGroup.OperatorGroupId
   where isnull(LocationOperatorGroup.LocationId,'0')  = isnull(@LocationId, isnull(LocationOperatorGroup.LocationId,'0'))
     and isnull(LocationOperatorGroup.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(LocationOperatorGroup.OperatorGroupId,'0'))
  
end
