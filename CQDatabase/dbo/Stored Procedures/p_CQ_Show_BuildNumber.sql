﻿/*
  /// <summary>
  ///   Procedure Name : p_CQ_Show_BuildNumber
  ///   Filename       : p_CQ_Show_BuildNumber.sql
  ///   Create By      : Venkat
  ///   Date Created   : 17 Jan 2017
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_CQ_Show_BuildNumber
(
 @Version  nvarchar(50) output
)
as
begin
	 set nocount on;
 
	select top 1 @Version = ISNULL(QaVersion, Convert(nvarchar(5),MajorVersion) + '.' + Convert(nvarchar(5),MinorVersion) + '.' +  Convert(nvarchar(5),BuildNumber) + '.' + Convert(nvarchar(5),Revision)) from CQBuild order by CQBuildId DESC
end

