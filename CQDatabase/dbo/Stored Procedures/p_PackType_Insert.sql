﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackType_Insert
  ///   Filename       : p_PackType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:40:52
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PackType table.
  /// </remarks>
  /// <param>
  ///   @PackTypeId int = null output,
  ///   @PackType varchar(30) = null,
  ///   @InboundSequence smallint = null,
  ///   @OutboundSequence smallint = null,
  ///   @PackTypeCode varchar(10) = null,
  ///   @Unit bit = null,
  ///   @Pallet bit = null 
  /// </param>
  /// <returns>
  ///   PackType.PackTypeId,
  ///   PackType.PackType,
  ///   PackType.InboundSequence,
  ///   PackType.OutboundSequence,
  ///   PackType.PackTypeCode,
  ///   PackType.Unit,
  ///   PackType.Pallet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackType_Insert
(
 @PackTypeId int = null output,
 @PackType varchar(30) = null,
 @InboundSequence smallint = null,
 @OutboundSequence smallint = null,
 @PackTypeCode varchar(10) = null,
 @Unit bit = null,
 @Pallet bit = null 
)
 
as
begin
	 set nocount on;
  
  if @PackTypeId = '-1'
    set @PackTypeId = null;
  
  if @PackType = '-1'
    set @PackType = null;
  
  if @PackTypeCode = '-1'
    set @PackTypeCode = null;
  
	 declare @Error int
 
  insert PackType
        (PackType,
         InboundSequence,
         OutboundSequence,
         PackTypeCode,
         Unit,
         Pallet)
  select @PackType,
         @InboundSequence,
         @OutboundSequence,
         @PackTypeCode,
         @Unit,
         @Pallet 
  
  select @Error = @@Error, @PackTypeId = scope_identity()
  
  
  return @Error
  
end
