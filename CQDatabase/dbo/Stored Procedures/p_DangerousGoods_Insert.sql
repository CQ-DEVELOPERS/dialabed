﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DangerousGoods_Insert
  ///   Filename       : p_DangerousGoods_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:30
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the DangerousGoods table.
  /// </remarks>
  /// <param>
  ///   @DangerousGoodsId int = null output,
  ///   @DangerousGoodsCode nvarchar(20) = null,
  ///   @DangerousGoods nvarchar(100) = null,
  ///   @Class float = null,
  ///   @UNNumber nvarchar(60) = null,
  ///   @PackClass nvarchar(60) = null,
  ///   @DangerFactor int = null 
  /// </param>
  /// <returns>
  ///   DangerousGoods.DangerousGoodsId,
  ///   DangerousGoods.DangerousGoodsCode,
  ///   DangerousGoods.DangerousGoods,
  ///   DangerousGoods.Class,
  ///   DangerousGoods.UNNumber,
  ///   DangerousGoods.PackClass,
  ///   DangerousGoods.DangerFactor 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DangerousGoods_Insert
(
 @DangerousGoodsId int = null output,
 @DangerousGoodsCode nvarchar(20) = null,
 @DangerousGoods nvarchar(100) = null,
 @Class float = null,
 @UNNumber nvarchar(60) = null,
 @PackClass nvarchar(60) = null,
 @DangerFactor int = null 
)
 
as
begin
	 set nocount on;
  
  if @DangerousGoodsId = '-1'
    set @DangerousGoodsId = null;
  
  if @DangerousGoodsCode = '-1'
    set @DangerousGoodsCode = null;
  
  if @DangerousGoods = '-1'
    set @DangerousGoods = null;
  
	 declare @Error int
 
  insert DangerousGoods
        (DangerousGoodsCode,
         DangerousGoods,
         Class,
         UNNumber,
         PackClass,
         DangerFactor)
  select @DangerousGoodsCode,
         @DangerousGoods,
         @Class,
         @UNNumber,
         @PackClass,
         @DangerFactor 
  
  select @Error = @@Error, @DangerousGoodsId = scope_identity()
  
  
  return @Error
  
end
