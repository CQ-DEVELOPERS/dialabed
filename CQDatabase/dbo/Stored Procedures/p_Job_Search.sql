﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Search
  ///   Filename       : p_Job_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Aug 2014 12:07:52
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Job table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null output,
  ///   @PriorityId int = null,
  ///   @OperatorId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @ReceiptLineId int = null,
  ///   @IssueLineId int = null,
  ///   @ContainerTypeId int = null,
  ///   @CheckedBy int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Job.JobId,
  ///   Job.PriorityId,
  ///   Priority.Priority,
  ///   Job.OperatorId,
  ///   Operator.Operator,
  ///   Job.StatusId,
  ///   Status.Status,
  ///   Job.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Job.ReceiptLineId,
  ///   ReceiptLine.ReceiptLineId,
  ///   Job.IssueLineId,
  ///   IssueLine.IssueLineId,
  ///   Job.ContainerTypeId,
  ///   ContainerType.ContainerType,
  ///   Job.ReferenceNumber,
  ///   Job.TareWeight,
  ///   Job.Weight,
  ///   Job.NettWeight,
  ///   Job.CheckedBy,
  ///   Operator.Operator,
  ///   Job.CheckedDate,
  ///   Job.DropSequence,
  ///   Job.Pallets,
  ///   Job.BackFlush,
  ///   Job.Prints,
  ///   Job.CheckingClear,
  ///   Job.TrackingNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Search
(
 @JobId int = null output,
 @PriorityId int = null,
 @OperatorId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @ReceiptLineId int = null,
 @IssueLineId int = null,
 @ContainerTypeId int = null,
 @CheckedBy int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @JobId = '-1'
    set @JobId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @CheckedBy = '-1'
    set @CheckedBy = null;
  
 
  select
         Job.JobId
        ,Job.PriorityId
         ,PriorityPriorityId.Priority as 'Priority'
        ,Job.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,Job.StatusId
         ,StatusStatusId.Status as 'Status'
        ,Job.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Job.ReceiptLineId
        ,Job.IssueLineId
        ,Job.ContainerTypeId
         ,ContainerTypeContainerTypeId.ContainerType as 'ContainerType'
        ,Job.ReferenceNumber
        ,Job.TareWeight
        ,Job.Weight
        ,Job.NettWeight
        ,Job.CheckedBy
         ,OperatorCheckedBy.Operator as 'OperatorCheckedBy'
        ,Job.CheckedDate
        ,Job.DropSequence
        ,Job.Pallets
        ,Job.BackFlush
        ,Job.Prints
        ,Job.CheckingClear
        ,Job.TrackingNumber
    from Job
    left
    join Priority PriorityPriorityId on PriorityPriorityId.PriorityId = Job.PriorityId
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = Job.OperatorId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = Job.StatusId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Job.WarehouseId
    left
    join ReceiptLine ReceiptLineReceiptLineId on ReceiptLineReceiptLineId.ReceiptLineId = Job.ReceiptLineId
    left
    join IssueLine IssueLineIssueLineId on IssueLineIssueLineId.IssueLineId = Job.IssueLineId
    left
    join ContainerType ContainerTypeContainerTypeId on ContainerTypeContainerTypeId.ContainerTypeId = Job.ContainerTypeId
    left
    join Operator OperatorCheckedBy on OperatorCheckedBy.OperatorId = Job.CheckedBy
   where isnull(Job.JobId,'0')  = isnull(@JobId, isnull(Job.JobId,'0'))
     and isnull(Job.PriorityId,'0')  = isnull(@PriorityId, isnull(Job.PriorityId,'0'))
     and isnull(Job.OperatorId,'0')  = isnull(@OperatorId, isnull(Job.OperatorId,'0'))
     and isnull(Job.StatusId,'0')  = isnull(@StatusId, isnull(Job.StatusId,'0'))
     and isnull(Job.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Job.WarehouseId,'0'))
     and isnull(Job.ReceiptLineId,'0')  = isnull(@ReceiptLineId, isnull(Job.ReceiptLineId,'0'))
     and isnull(Job.IssueLineId,'0')  = isnull(@IssueLineId, isnull(Job.IssueLineId,'0'))
     and isnull(Job.ContainerTypeId,'0')  = isnull(@ContainerTypeId, isnull(Job.ContainerTypeId,'0'))
     and isnull(Job.CheckedBy,'0')  = isnull(@CheckedBy, isnull(Job.CheckedBy,'0'))
  order by ReferenceNumber
  
end
