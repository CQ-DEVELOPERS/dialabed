﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitArea_Insert
  ///   Filename       : p_StorageUnitArea_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012 14:20:19
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitArea table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null output,
  ///   @AreaId int = null output,
  ///   @StoreOrder int = null,
  ///   @PickOrder int = null,
  ///   @MinimumQuantity float = null,
  ///   @ReorderQuantity float = null,
  ///   @MaximumQuantity float = null 
  /// </param>
  /// <returns>
  ///   StorageUnitArea.StorageUnitId,
  ///   StorageUnitArea.AreaId,
  ///   StorageUnitArea.StoreOrder,
  ///   StorageUnitArea.PickOrder,
  ///   StorageUnitArea.MinimumQuantity,
  ///   StorageUnitArea.ReorderQuantity,
  ///   StorageUnitArea.MaximumQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
	CREATE procedure p_StorageUnitArea_Insert
	(
	 @StorageUnitId int = null output,
	 @AreaId int = null output,
	 @StoreOrder int = null,
	 @PickOrder int = null,
	 @MinimumQuantity float = null,
	 @ReorderQuantity float = null,
	 @MaximumQuantity float = null 
	)
	 
	as
	begin
		 set nocount on;

	declare @Principals as table
	  (
	   PrincipalId     int
	  )
    
	declare @Error				bit,
			@PrincipalIdProd	int,
			@PrincipalIdZone	INT
		
	select @PrincipalIdProd = PrincipalId
	  from StorageUnit  su
	  join Product       p (nolock) on su.ProductId = p.ProductId
	 where su.StorageUnitId = @StorageUnitId
	 
	 
	insert @Principals
		  (PrincipalId)
	select PrincipalId
	  from AreaPrincipal  ap
	 where ap.areaid = @AreaId
  
	set @PrincipalIdZone = null
  
	 select @PrincipalIdZone = PrincipalId
	  from @Principals
	 where PrincipalId = @PrincipalIdProd
	 

	select @PrincipalIdProd, @PrincipalIdZone
	 if @PrincipalIdProd = @PrincipalIdZone
	 begin

	  insert StorageUnitArea
		       (StorageUnitId,
		        AreaId,
		        StoreOrder,
		        PickOrder,
		        MinimumQuantity,
		        ReorderQuantity,
		        MaximumQuantity)
	  select @StorageUnitId,
			       @AreaId,
			       @StoreOrder,
			       @PickOrder,
			       @MinimumQuantity,
			       @ReorderQuantity,
			       @MaximumQuantity 
  
  select @Error = @@Error, @AreaId = scope_identity()


  if @Error = 0 or @AreaId is null
	  exec @Error = p_StorageUnitAreaHistory_Insert
		   @StorageUnitId = @StorageUnitId,
		   @AreaId = @AreaId,
		   @StoreOrder = @StoreOrder,
		   @PickOrder = @PickOrder,
		   @MinimumQuantity = @MinimumQuantity,
		   @ReorderQuantity = @ReorderQuantity,
		   @MaximumQuantity = @MaximumQuantity,
		   @CommandType = 'Insert'

	end
	else
	begin

	  insert StorageUnitArea
		       (StorageUnitId,
		        AreaId,
		        StoreOrder,
		        PickOrder,
		        MinimumQuantity,
		        ReorderQuantity,
		        MaximumQuantity)
	  select @StorageUnitId,
			       @AreaId,
			       @StoreOrder,
			       @PickOrder,
			       @MinimumQuantity,
			       @ReorderQuantity,
			       @MaximumQuantity
			
	  exec @Error = p_StorageUnitAreaHistory_Insert

		   @StorageUnitId = @StorageUnitId,
		   @AreaId = @AreaId,
		   @StoreOrder = @StoreOrder,
		   @PickOrder = @PickOrder,
		   @MinimumQuantity = @MinimumQuantity,
		   @ReorderQuantity = @ReorderQuantity,
		   @MaximumQuantity = @MaximumQuantity,
		   @CommandType = 'Insert'
	end
	
	return @Error

end
 
