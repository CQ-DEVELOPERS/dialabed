﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LabelingCategory_Select
  ///   Filename       : p_LabelingCategory_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:56:45
  /// </summary>
  /// <remarks>
  ///   Selects rows from the LabelingCategory table.
  /// </remarks>
  /// <param>
  ///   @LabelingCategoryId int = null 
  /// </param>
  /// <returns>
  ///   LabelingCategory.LabelingCategoryId,
  ///   LabelingCategory.LabelingCategory,
  ///   LabelingCategory.LabelingCategoryCode,
  ///   LabelingCategory.Additional1,
  ///   LabelingCategory.Additional2,
  ///   LabelingCategory.Additional3,
  ///   LabelingCategory.Additional4,
  ///   LabelingCategory.Additional5,
  ///   LabelingCategory.ManualLabel,
  ///   LabelingCategory.ScreenGaurd 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LabelingCategory_Select
(
 @LabelingCategoryId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         LabelingCategory.LabelingCategoryId
        ,LabelingCategory.LabelingCategory
        ,LabelingCategory.LabelingCategoryCode
        ,LabelingCategory.Additional1
        ,LabelingCategory.Additional2
        ,LabelingCategory.Additional3
        ,LabelingCategory.Additional4
        ,LabelingCategory.Additional5
        ,LabelingCategory.ManualLabel
        ,LabelingCategory.ScreenGaurd
    from LabelingCategory
   where isnull(LabelingCategory.LabelingCategoryId,'0')  = isnull(@LabelingCategoryId, isnull(LabelingCategory.LabelingCategoryId,'0'))
  order by LabelingCategory
  
end
 
