﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pack_Select
  ///   Filename       : p_Pack_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Nov 2012 14:41:26
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Pack table.
  /// </remarks>
  /// <param>
  ///   @PackId int = null 
  /// </param>
  /// <returns>
  ///   Pack.PackId,
  ///   Pack.StorageUnitId,
  ///   Pack.PackTypeId,
  ///   Pack.Quantity,
  ///   Pack.Barcode,
  ///   Pack.Length,
  ///   Pack.Width,
  ///   Pack.Height,
  ///   Pack.Volume,
  ///   Pack.Weight,
  ///   Pack.WarehouseId,
  ///   Pack.NettWeight,
  ///   Pack.TareWeight,
  ///   Pack.ProductionQuantity,
  ///   Pack.StatusId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pack_Select
(
 @PackId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Pack.PackId
        ,Pack.StorageUnitId
        ,Pack.PackTypeId
        ,Pack.Quantity
        ,Pack.Barcode
        ,Pack.Length
        ,Pack.Width
        ,Pack.Height
        ,Pack.Volume
        ,Pack.Weight
        ,Pack.WarehouseId
        ,Pack.NettWeight
        ,Pack.TareWeight
        ,Pack.ProductionQuantity
        ,Pack.StatusId
    from Pack
   where isnull(Pack.PackId,'0')  = isnull(@PackId, isnull(Pack.PackId,'0'))
  order by Barcode
  
end
