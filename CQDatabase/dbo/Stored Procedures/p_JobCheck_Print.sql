﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_JobCheck_Print
  ///   Filename       : p_JobCheck_Print.sql
  ///   Create By      : Louise	
  ///   Date Created   : 11 Mar 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_JobCheck_Print
 
as
begin
  set nocount on;
  
  select JobId
    from JobCheckPrinted
   where Printed = 0
  
  update JobCheckPrinted
   set Printed = 1
   where Printed = 0
  
  return
end
