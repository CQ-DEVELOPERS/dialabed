﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Parameter
  ///   Filename       : p_Job_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Aug 2014 12:07:52
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Job table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Job.JobId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as JobId
        ,null as 'Job'
  union
  select
         Job.JobId
        ,Job.JobId as 'Job'
    from Job
  
end
