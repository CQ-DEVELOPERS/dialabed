﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questions_Search
  ///   Filename       : p_Questions_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:06
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Questions table.
  /// </remarks>
  /// <param>
  ///   @QuestionId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Questions.QuestionId,
  ///   Questions.QuestionaireId,
  ///   Questions.Category,
  ///   Questions.Code,
  ///   Questions.Sequence,
  ///   Questions.Active,
  ///   Questions.QuestionText,
  ///   Questions.QuestionType,
  ///   Questions.DateUpdated 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Questions_Search
(
 @QuestionId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @QuestionId = '-1'
    set @QuestionId = null;
  
 
  select
         Questions.QuestionId
        ,Questions.QuestionaireId
        ,Questions.Category
        ,Questions.Code
        ,Questions.Sequence
        ,Questions.Active
        ,Questions.QuestionText
        ,Questions.QuestionType
        ,Questions.DateUpdated
    from Questions
   where isnull(Questions.QuestionId,'0')  = isnull(@QuestionId, isnull(Questions.QuestionId,'0'))
  order by Category
  
end
