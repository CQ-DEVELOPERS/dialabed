﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Tare_Weight
  ///   Filename       : p_Mobile_Tare_Weight.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Tare_Weight
(
 @instructionId int,
 @tareWeight    float
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @JobId             int
  
  select @JobId = JobId
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  exec p_Job_Update
   @JobId      = @JobId,
   @TareWeight = @TareWeight
  
  if @Error <> 0
    goto result
  
  result:
    select @Error
    return @Error
end
