﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Edit
  ///   Filename       : p_Location_Edit.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2011
  /// </summary>
  /// <remarks>
  /// </remarks>
  /// <param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : Daniel Schotter
  ///   Modified Date  : April 2018
  ///   Details        : CQPMO-2806: Allowed for alpha numeric security codes.
  /// </newpara>
*/
CREATE procedure p_Location_Edit
(
 @LocationId int = null,
 @LocationTypeId int = null,
 @Location varchar(15) = null,
 @Ailse varchar(10) = null,
 @Column varchar(10) = null,
 @Level varchar(10) = null,
 @PalletQuantity float = null,
 @SecurityCode NVARCHAR(50) = null, --CQPMO-2806: Allowed for alpha numeric security codes.
 @RelativeValue numeric(13,3) = null,
 @NumberOfSUB int = null,
 @StocktakeInd bit = null,
 @ActiveBinning bit = null,
 @ActivePicking bit = null,
 @Used bit = null,
 @AreaId int = null 
)
as
begin
	 set nocount on
	 declare @Error int
  
  update Location
     set LocationTypeId = @LocationTypeId,
         Location = @Location,
         Ailse = @Ailse,
         [Column] = @Column,
         [Level] = @Level,
         PalletQuantity = @PalletQuantity,
         SecurityCode = @SecurityCode,
         RelativeValue = @RelativeValue,
         NumberOfSUB = @NumberOfSUB,
         StocktakeInd = @StocktakeInd,
         ActiveBinning = @ActiveBinning,
         ActivePicking = @ActivePicking,
         Used = @Used 
   where LocationId = @LocationId
   
   --Delete From AreaLocation Where LocationId = @LocationId
   
   --Insert into AreaLocation(AreaId,LocationId) Values(@AreaId,@LocationId)
   
  exec @error = p_AreaLocation_Update
		@AreaId,
		@LocationId
		
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_LocationHistory_Insert
         @LocationId,
         @LocationTypeId,
         @Location,
         @Ailse,
         @Column,
         @Level,
         @PalletQuantity,
         @SecurityCode,
         @RelativeValue,
         @NumberOfSUB,
         @StocktakeInd,
         @ActiveBinning,
         @ActivePicking,
         @Used 
        ,@CommandType = 'Update'
  
  return @Error
  
end
