﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExceptionHistory_Insert
  ///   Filename       : p_ExceptionHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:04
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ExceptionHistory table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null,
  ///   @ReceiptLineId int = null,
  ///   @InstructionId int = null,
  ///   @IssueLineId int = null,
  ///   @ReasonId int = null,
  ///   @Exception nvarchar(510) = null,
  ///   @ExceptionCode nvarchar(20) = null,
  ///   @CreateDate datetime = null,
  ///   @OperatorId int = null,
  ///   @ExceptionDate datetime = null,
  ///   @JobId int = null,
  ///   @Detail sql_variant = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   ExceptionHistory.ExceptionId,
  ///   ExceptionHistory.ReceiptLineId,
  ///   ExceptionHistory.InstructionId,
  ///   ExceptionHistory.IssueLineId,
  ///   ExceptionHistory.ReasonId,
  ///   ExceptionHistory.Exception,
  ///   ExceptionHistory.ExceptionCode,
  ///   ExceptionHistory.CreateDate,
  ///   ExceptionHistory.OperatorId,
  ///   ExceptionHistory.ExceptionDate,
  ///   ExceptionHistory.JobId,
  ///   ExceptionHistory.Detail,
  ///   ExceptionHistory.CommandType,
  ///   ExceptionHistory.InsertDate,
  ///   ExceptionHistory.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExceptionHistory_Insert
(
 @ExceptionId int = null,
 @ReceiptLineId int = null,
 @InstructionId int = null,
 @IssueLineId int = null,
 @ReasonId int = null,
 @Exception nvarchar(510) = null,
 @ExceptionCode nvarchar(20) = null,
 @CreateDate datetime = null,
 @OperatorId int = null,
 @ExceptionDate datetime = null,
 @JobId int = null,
 @Detail sql_variant = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ExceptionHistory
        (ExceptionId,
         ReceiptLineId,
         InstructionId,
         IssueLineId,
         ReasonId,
         Exception,
         ExceptionCode,
         CreateDate,
         OperatorId,
         ExceptionDate,
         JobId,
         Detail,
         CommandType,
         InsertDate,
         Quantity)
  select @ExceptionId,
         @ReceiptLineId,
         @InstructionId,
         @IssueLineId,
         @ReasonId,
         @Exception,
         @ExceptionCode,
         @CreateDate,
         @OperatorId,
         @ExceptionDate,
         @JobId,
         @Detail,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @Quantity 
  
  select @Error = @@Error
  
  
  return @Error
  
end
