﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Order_Status_Picking
  ///   Filename       : p_Dashboard_Outbound_Order_Status_Picking.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>  
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Order_Status_Picking
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select Legend,
	          Value,
	          Units
	     from DashboardOutboundOrderStatus
	    where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardOutboundOrderStatus
	   
	   insert DashboardOutboundOrderStatus
	         (WarehouseId,
           Legend,
           Value,
           Units)
	   select i.WarehouseId,
	          case when StatusCode in ('CK','CD','D','DC','C')
	               then 'Picked'
	               else s.Status
	               end as 'Legend',
	          count(distinct(i.IssueId)) as 'Value',
	          SUM(il.Quantity)
	     from Issue                  i (nolock)
	     join OutboundDocument      od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
	     join OutboundDocumentType odt (nolock) on odt.OutboundDocumentTypeId = od.OutboundDocumentTypeId
	     join IssueLine il (nolock) on i.IssueId = il.IssueId
	     join Status s (nolock) on i.StatusId = s.StatusId
	    where odt.OutboundDocumentTypeCode != 'WAV'
	      and s.StatusCode in ('CK','CD','D','DC','C')
	      and i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and i.IssueId in (select IssueId from viewili where MostRecent >= CONVERT(nvarchar(10), getdate(), 120))
	   group by i.WarehouseId,
	            case when StatusCode in ('CK','CD','D','DC','C')
	               then 'Picked'
	               else s.Status
	               end
	   
	   insert DashboardOutboundOrderStatus
	         (WarehouseId,
           Legend,
           Value,
           Units)
	   select i.WarehouseId,
	          case when StatusCode in ('CK','CD','D','DC','C')
	               then 'Picked'
	       
        else s.Status
	               end as 'Legend',
	          count(distinct(i.IssueId)) as 'Value',
	          SUM(il.Quantity)
	     from Issue                  i (nolock)
	     join OutboundDocument      od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
	     join OutboundDocumentType odt (nolock) on odt.OutboundDocumentTypeId = od.OutboundDocumentTypeId
	     join IssueLine il (nolock) on i.IssueId = il.IssueId
	     join Status s (nolock) on i.StatusId = s.StatusId
	    where odt.OutboundDocumentTypeCode != 'WAV'
	      and s.StatusCode in ('RL')
	      and i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and i.IssueId in (select IssueId from viewili where MostRecent >= CONVERT(nvarchar(10), dateadd(dd, -3, getdate()), 120))
	   group by i.WarehouseId,
	            case when StatusCode in ('CK','CD','D','DC','C')
	               then 'Picked'
	               else s.Status
	               end
	 end
end
