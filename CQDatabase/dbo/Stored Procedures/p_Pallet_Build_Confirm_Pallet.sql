﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Build_Confirm_Pallet
  ///   Filename       : p_Pallet_Build_Confirm_Pallet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Build_Confirm_Pallet
(
 @warehouseId int,
 @operatorId int,
 @barcode nvarchar(50)
)
 
as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Pallet_Build_Confirm_Pallet',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @PalletId          int,
          @JobId             int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(replace(@barcode,'P:','')) = 1 and @barcode like 'P:%'
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if isnumeric(replace(@barcode,'J:','')) = 1 and @barcode like 'J:%'
    select @JobId = replace(@barcode,'J:',''),
           @barcode  = null
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  if @PalletId is not null
  if exists(select top 1 0
              from Pallet
             where PalletId = @PalletId
               and StorageUnitBatchId is null)
  begin
    goto result
  end
  else
  begin
    set @Error = -1
    goto error
  end
  
  -- if @JobId is not null -- not sure this will be needed
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
