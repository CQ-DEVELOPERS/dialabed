﻿--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode') IS NOT NULL
--BEGIN
--    DROP PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode
--    IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode') IS NOT NULL
--        PRINT '<<< FAILED DROPPING PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode >>>'
--    ELSE
--        PRINT '<<< DROPPED PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode >>>'
--END
--go
CREATE PROCEDURE p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode (
	@warehouseId INT
	,@comparisonDate DATETIME
	,@storageUnitId INT
	,@batchId INT
	,@warehouseCode NVARCHAR(50) = NULL
	,@reasonCode NVARCHAR(50) = NULL
	)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Error INT
		,@Errormsg NVARCHAR(500)
		,@GetDate DATETIME
		,@Rowcount INT
		,@InterfaceId INT
		,@HousekeepingCompareId INT
		,@RecordType VARCHAR(10)
		,@OldWarehouseId INT
		,@isStockOnHand BIT
		,@Gain NVARCHAR(50)
		,@Loss NVARCHAR(50)
		,@PrincipalCode NVARCHAR(30)
		,@TBSDefaultDC	nvarchar(50)
		,@DABDefaultDC	nvarchar(50)

	SET @OldWarehouseId = @WarehouseId
	set @TBSDefaultDC = '9020'
	set @DABDefaultDC = '8101'


	IF (
			SELECT dbo.ufn_Configuration(266, @OldWarehouseId)
			) = 1 -- ST Upload - Combine warehouses  
		SET @WarehouseId = NULL

	IF (
			SELECT dbo.ufn_Configuration(272, @OldWarehouseId)
			) = 1 -- ST Upload - Send Stock on hand quantity  
		SET @isStockOnHand = 1

	SELECT @GetDate = dbo.ufn_Getdate()

	SELECT @RecordType = convert(VARCHAR(10), Value)
	FROM Configuration(NOLOCK)
	WHERE WarehouseId = @OldWarehouseId AND ConfigurationId = 264

	IF (@reasonCode IS NULL)
	BEGIN
		SELECT @ReasonCode = convert(VARCHAR(10), Value)
		FROM Configuration(NOLOCK)
		WHERE WarehouseId = @OldWarehouseId AND ConfigurationId = 265
	END
	
	IF (@reasonCode IS NULL)
	BEGIN
		GOTO error
	END

	SELECT @Gain = convert(NVARCHAR(50), Value)
	FROM Configuration(NOLOCK)
	WHERE WarehouseId = @OldWarehouseId AND ConfigurationId = 334

	SELECT @Loss = convert(NVARCHAR(50), Value)
	FROM Configuration(NOLOCK)
	WHERE WarehouseId = @OldWarehouseId AND ConfigurationId = 335

	IF @Gain = ''
		SET @Gain = NULL

	IF @Loss = ''
		SET @Loss = NULL

	IF @Gain IS NOT NULL AND @Loss IS NOT NULL
		SET @RecordType = NULL

	IF @RecordType IS NULL
		SET @RecordType = 'SOH'

	BEGIN TRANSACTION

	IF @storageUnitId = - 1 AND @batchId = - 1
	BEGIN
		UPDATE HousekeepingCompare
		SET Sent = 1
			,SentDate = @GetDate
		WHERE ComparisonDate = @comparisonDate AND isnull(Sent, 0) = 0

		SELECT @Error = @@Error
			,@Rowcount = @@Rowcount

		IF @Error <> 0
			GOTO error

		IF @Rowcount > 0
		BEGIN
			IF (
					SELECT dbo.ufn_Configuration(234, @OldWarehouseId)
					) = 1 -- ST Upload - InterfaceExportHeader on|off  
			BEGIN
				INSERT InterfaceExportHeader (
					RecordType
					,RecordStatus
					,FromWarehouseCode
					,Additional1
					,Additional2
					,PrincipalCode
					)
				SELECT '60'
					,'N'
					,hc.WarehouseCode
					,p.ProductCode
					,CASE @isStockOnHand WHEN 1 THEN hc.WMSQuantity ELSE hc.WMSQuantity - hc.HostQuantity END
					,pn.PrincipalCode
				FROM HousekeepingCompare hc
				JOIN StorageUnit su(NOLOCK) ON hc.StorageUnitId = su.StorageUnitId
				JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
				LEFT JOIN Principal pn(NOLOCK) ON p.PrincipalId = pn.PrincipalId
				WHERE isnull(hc.WarehouseId, - 1) = isnull(@warehouseId, - 1) AND hc.ComparisonDate = @comparisonDate AND isnull(Sent, 0) = 1 AND hc.HostQuantity <> hc.WMSQuantity

				SELECT @Error = @@Error
					,@Rowcount = @@Rowcount

				IF @Error <> 0
					GOTO error
			END
			ELSE
			BEGIN
				INSERT InterfaceExportStockAdjustment (
					RecordType
					,RecordStatus
					,Additional1
					,ProductCode
					,SKUCode
					,Batch
					,Quantity
					,Additional3
					,PrincipalCode
					)
				SELECT CASE WHEN @RecordType IS NOT NULL THEN @RecordType ELSE CASE WHEN hc.WMSQuantity - hc.HostQuantity > 0 THEN @Gain ELSE @Loss END END
					,'N'
					,hc.DCCode
					,p.ProductCode
					,sku.SKUCode
					,b.Batch
					,CASE @isStockOnHand WHEN 1 THEN hc.WMSQuantity ELSE hc.WMSQuantity - hc.HostQuantity END
					,@ReasonCode
					,pn.PrincipalCode
				FROM HousekeepingCompare hc
				JOIN StorageUnit su(NOLOCK) ON hc.StorageUnitId = su.StorageUnitId
				JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
				JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
				JOIN Batch b(NOLOCK) ON hc.BatchId = b.BatchId
				join Warehouse		  w (nolock) on hc.WarehouseId = w.WarehouseId
				LEFT 
				JOIN Principal pn(NOLOCK) ON hc.PrincipalId = pn.PrincipalId
				WHERE isnull(hc.WarehouseId, - 1) = isnull(@warehouseId, - 1) AND hc.ComparisonDate = @comparisonDate AND isnull(Sent, 0) = 1 AND hc.HostQuantity <> hc.WMSQuantity

				SELECT @Error = @@Error
					,@Rowcount = @@Rowcount

				IF @Error <> 0
					GOTO error
			END
		END
	END
	ELSE
	BEGIN
		SELECT @HousekeepingCompareId = HousekeepingCompareId
		FROM HousekeepingCompare
		WHERE isnull(WarehouseId, - 1) = isnull(@warehouseId, - 1) AND ComparisonDate = @comparisonDate AND StorageUnitId = @storageUnitId AND BatchId = @batchId AND isnull(Sent, 0) = 0 AND isnull(WarehouseCode, '') = isnull(@warehouseCode, '')

		SELECT @Error = @@Error
			,@Rowcount = @@Rowcount

		IF @Error <> 0
			GOTO error

		IF @Rowcount > 0
		BEGIN
			IF (
					SELECT dbo.ufn_Configuration(234, @OldWarehouseId)
					) = 1 -- ST Upload - InterfaceExportHeader on|off  
			BEGIN
				INSERT InterfaceExportHeader (
					RecordType
					,RecordStatus
					,FromWarehouseCode
					,Additional1
					,Additional2
					,PrincipalCode
					)
				SELECT '60'
					,'N'
					,hc.WarehouseCode
					,p.ProductCode
					,CASE @isStockOnHand WHEN 1 THEN hc.WMSQuantity ELSE hc.WMSQuantity - hc.HostQuantity END
					,pn.PrincipalCode
				FROM HousekeepingCompare hc
				JOIN StorageUnit su(NOLOCK) ON hc.StorageUnitId = su.StorageUnitId
				JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
				LEFT JOIN Principal pn(NOLOCK) ON p.PrincipalId = pn.PrincipalId
				WHERE hc.HousekeepingCompareId = @HousekeepingCompareId

				SELECT @Error = @@Error
					,@Rowcount = @@Rowcount
					,@InterfaceId = @@Identity

				IF @Error <> 0
					GOTO error
			END
			ELSE
			BEGIN
				INSERT InterfaceExportStockAdjustment (
					RecordType
					,RecordStatus
					,Additional1
					,ProductCode
					,SKUCode
					,Batch
					,Quantity
					,Additional3
					,PrincipalCode
					)
				SELECT CASE WHEN @RecordType IS NOT NULL THEN @RecordType ELSE CASE WHEN hc.WMSQuantity - hc.HostQuantity > 0 THEN @Gain ELSE @Loss END END
					,'N'
					,hc.DCCode
					,p.ProductCode
					,sku.SKUCode
					,b.Batch
					,CASE @isStockOnHand WHEN 1 THEN hc.WMSQuantity ELSE hc.WMSQuantity - hc.HostQuantity END
					,@ReasonCode
					,pn.PrincipalCode
				FROM HousekeepingCompare hc
				JOIN StorageUnit su(NOLOCK) ON hc.StorageUnitId = su.StorageUnitId
				JOIN Product p(NOLOCK) ON su.ProductId = p.ProductId
				JOIN SKU sku(NOLOCK) ON su.SKUId = sku.SKUId
				JOIN Batch b(NOLOCK) ON hc.BatchId = b.BatchId
				join Warehouse		  w (nolock) on hc.WarehouseId = w.WarehouseId
				LEFT JOIN Principal pn(NOLOCK) ON hc.PrincipalId = pn.PrincipalId
				WHERE hc.HousekeepingCompareId = @HousekeepingCompareId

				--where hc.WarehouseId    = @warehouseId  
				--  and hc.ComparisonDate = @comparisonDate  
				--  and hc.StorageUnitId  = @storageUnitId  
				--  and hc.BatchId        = @batchId  
				--  and isnull(WarehouseCode,'') = isnull(@warehouseCode,'')  
				--  and hc.HostQuantity <> hc.WMSQuantity  
				SELECT @Error = @@Error
					,@Rowcount = @@Rowcount
					,@InterfaceId = @@Identity

				IF @Error <> 0
					GOTO error
			END

			UPDATE HousekeepingCompare
			SET Sent = 1
				,SentDate = @GetDate
				,InterfaceId = @InterfaceId
				,RecordStatus = 'N'
			WHERE HousekeepingCompareId = @HousekeepingCompareId

			SELECT @Error = @@Error
				,@Rowcount = @@Rowcount

			IF @Error <> 0
				GOTO error
		END
	END

	update InterfaceExportStockAdjustment
	set additional1 = @DABDefaultDC
	where PrincipalCode = 'DAB'
	  and Additional1 is null

	update InterfaceExportStockAdjustment
	set additional1 = @TBSDefaultDC
	where PrincipalCode = 'TBS'
	  and Additional1 is null

	COMMIT TRANSACTION

	RETURN

	error:

	RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode'); 

	ROLLBACK TRANSACTION

	RETURN @Error
END
--go
--IF OBJECT_ID('dbo.p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode') IS NOT NULL
--    PRINT '<<< CREATED PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode >>>'
--ELSE
--    PRINT '<<< FAILED CREATING PROCEDURE dbo.p_Housekeeping_Stock_Take_Upload_Accept_With_ReasonCode >>>'
--go


