﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_tbl_Bom_Log_Search
  ///   Filename       : p_tbl_Bom_Log_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:59
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the tbl_Bom_Log table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   tbl_Bom_Log.Insertdate,
  ///   tbl_Bom_Log.xmlstring 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_tbl_Bom_Log_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         tbl_Bom_Log.Insertdate
        ,tbl_Bom_Log.xmlstring
    from tbl_Bom_Log
  
end
