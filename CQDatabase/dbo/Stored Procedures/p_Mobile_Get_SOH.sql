﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Get_SOH
  ///   Filename       : p_Mobile_Get_SOH.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2016
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Get_SOH
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
	 
	 declare @InstructionRefId   int,
	         @OutboundDocumentTypeId int,
	         @SOHQuantity			int,
	         @LocationId			int,
	         @StorageUnitBatchId	int	
  
  select @LocationId = i.PickLocationId,
		 @StorageUnitBatchId = i.StorageUnitBatchId
    from Instruction            i (nolock)
   where i.InstructionId = @InstructionId
	 
	 select @SOHQuantity = subl.ActualQuantity
    from StorageUnitBatchLocation subl (nolock)
   where subl.StorageUnitBatchId = @StorageUnitBatchId
     and subl.LocationId = @LocationId
  	 
 
  select @SOHQuantity as 'sohQuantity'
  return @SOHQuantity
end
 
 
 
