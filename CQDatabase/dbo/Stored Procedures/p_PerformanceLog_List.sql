﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PerformanceLog_List
  ///   Filename       : p_PerformanceLog_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:57
  /// </summary>
  /// <remarks>
  ///   Selects rows from the PerformanceLog table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   PerformanceLog.PerformanceLogId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PerformanceLog_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as PerformanceLogId
        ,null as 'PerformanceLog'
  union
  select
         PerformanceLog.PerformanceLogId
        ,PerformanceLog.PerformanceLogId as 'PerformanceLog'
    from PerformanceLog
  
end
