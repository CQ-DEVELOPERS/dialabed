﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DesktopLog_List
  ///   Filename       : p_DesktopLog_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:35
  /// </summary>
  /// <remarks>
  ///   Selects rows from the DesktopLog table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   DesktopLog.DesktopLogId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DesktopLog_List
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as DesktopLogId
        ,null as 'DesktopLog'
  union
  select
         DesktopLog.DesktopLogId
        ,DesktopLog.DesktopLogId as 'DesktopLog'
    from DesktopLog
  
end
