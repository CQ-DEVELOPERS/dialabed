﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequest_Extract
  ///   Filename       : p_ManualFileRequest_Extract.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Feb 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequest_Extract
    @XMLBody nvarchar(max) output
 
as
begin
	 set nocount on;
  
    DECLARE @doc            xml
	      , @idoc           int
	      , @InsertDate     datetime
	      , @ERROR          VARCHAR(255)
	      , @DocTypeId      Int
	      , @record_type    varchar(3)
	      , @depot          varchar(3)
	      , @stock_no       varchar(9)
	      , @lot_code       varchar(6)
	      , @sku_code       varchar(4)
	      , @doc_no         varchar(8)
	
	SELECT @doc = convert(xml,@XMLBody)	     
	     , @InsertDate = Getdate()
	     
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
	SELECT 
	     @DocTypeId =  docTypeId
	    ,@depot = depot
	    ,@stock_no = stock_no
	    ,@lot_code = lot_code
	    ,@sku_code = sku_code
	    ,@doc_no = doc_no
	FROM OPENXML (@idoc, '/root',1)
    WITH (docTypeId int 'docTypeId'
         ,depot varchar(3) 'depot'
         ,stock_no varchar(9) 'stock_no'
         ,lot_code varchar(6) 'lot_code'
         ,sku_code varchar(4) 'sku_code'
         ,doc_no varchar(8) 'doc_no')
    select @record_type = DocumentTypeCode
    from ManualFileRequestType
    where ManualFileRequestTypeId = @DocTypeId
    
    insert InterfaceExtract
    (
        RecordType,
        SequenceNumber,
        OrderNumber,
        LineNumber,
        RecordStatus,
        ExtractedDate,
        Data
    )
    select
        @record_type
       ,''
       ,'Manaul'
       ,1
       ,'N'
       ,GETDATE()
       ,replicate(' ',9) + 
        @record_type + 
        CASE @record_type 
            WHEN 'MLT' THEN
                replicate('0',3  - datalength(convert(varchar(3),@depot))) + convert(varchar(3),@depot) +
                replicate(' ',9 - datalength(convert(varchar(9),ltrim(@stock_no)))) + convert(varchar(9),ltrim(@stock_no))     +  
                convert(varchar(6),ltrim(@lot_code)) + replicate(' ',6 - datalength(convert(varchar(6),ltrim(@lot_code))))
            WHEN 'MPR' THEN
                replicate(' ',9 - datalength(convert(varchar(9),ltrim(@stock_no)))) + convert(varchar(9),ltrim(@stock_no))     +  
                replicate('0',4  - datalength(convert(varchar(4),@sku_code))) + convert(varchar(4),@sku_code)
            WHEN 'MSK' THEN 
                replicate('0',4  - datalength(convert(varchar(4),@sku_code))) + convert(varchar(4),@sku_code)
            ELSE
                replicate('0',3  - datalength(convert(varchar(3),@depot))) + convert(varchar(3),@depot) + 
                convert(varchar(8),ltrim(@doc_no)) + replicate(' ',8 - datalength(convert(varchar(8),ltrim(@doc_no))))
        END
end
