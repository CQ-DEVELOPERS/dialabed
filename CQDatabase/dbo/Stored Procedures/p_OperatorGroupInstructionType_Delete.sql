﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupInstructionType_Delete
  ///   Filename       : p_OperatorGroupInstructionType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:07
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OperatorGroupInstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null,
  ///   @OperatorGroupId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupInstructionType_Delete
(
 @InstructionTypeId int = null,
 @OperatorGroupId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete OperatorGroupInstructionType
     where InstructionTypeId = @InstructionTypeId
       and OperatorGroupId = @OperatorGroupId
  
  select @Error = @@Error
  
  
  return @Error
  
end
