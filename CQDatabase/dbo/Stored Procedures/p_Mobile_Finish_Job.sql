﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Finish_Job
  ///   Filename       : p_Mobile_Finish_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Oct 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Finish_Job
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int,
          @StartedId         int,
          @NoStockId         int,
          @Complete         int,
          @Count             int
  
  declare @TableInstruction as table
  (
   InstructionId int
  )
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @JobId = JobId
    from Instruction (nolock)
   where InstructionId = @InstructionId
  
  select @NoStockId = dbo.ufn_StatusId('I','NS')
  select @StartedId = dbo.ufn_StatusId('I','S')
  select @Complete = dbo.ufn_StatusId('R','C')
  
  begin transaction
  
  insert @TableInstruction
        (InstructionId)
  select InstructionId
    from Instruction i
    join Status      s on i.StatusId = s.StatusId
   where i.JobId       = @JobId
     and s.StatusCode in ('W','S')
  
  select @Count = @@rowcount
  
  while @Count > 0
  begin
    select @Count = @Count - 1
    
    select @InstructionId = InstructionId
      from @TableInstruction
    
    delete @TableInstruction where InstructionId = @InstructionId
    
    exec p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
    
    exec p_Instruction_Update
     @InstructionId     = @InstructionId,
     @StatusId          = @StartedId,
     @ConfirmedQuantity = 0
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Status_Rollup
     @jobId               = @JobId,
     @instructionId       = @instructionId
    
    if @Error <> 0
      goto error
    
    exec p_Instruction_Update
     @InstructionId     = @InstructionId,
     @StatusId          = @NoStockId
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_Job_Update
   @jobId    = @JobId,
   @StatusId = @Complete
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Mobile_Finish_Job'
    rollback transaction
    select @Error
    return @Error
end
