﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DangerousGoods_Search
  ///   Filename       : p_DangerousGoods_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:31
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the DangerousGoods table.
  /// </remarks>
  /// <param>
  ///   @DangerousGoodsId int = null output,
  ///   @DangerousGoodsCode nvarchar(20) = null,
  ///   @DangerousGoods nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   DangerousGoods.DangerousGoodsId,
  ///   DangerousGoods.DangerousGoodsCode,
  ///   DangerousGoods.DangerousGoods,
  ///   DangerousGoods.Class,
  ///   DangerousGoods.UNNumber,
  ///   DangerousGoods.PackClass,
  ///   DangerousGoods.DangerFactor 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DangerousGoods_Search
(
 @DangerousGoodsId int = null output,
 @DangerousGoodsCode nvarchar(20) = null,
 @DangerousGoods nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @DangerousGoodsId = '-1'
    set @DangerousGoodsId = null;
  
  if @DangerousGoodsCode = '-1'
    set @DangerousGoodsCode = null;
  
  if @DangerousGoods = '-1'
    set @DangerousGoods = null;
  
 
  select
         DangerousGoods.DangerousGoodsId
        ,DangerousGoods.DangerousGoodsCode
        ,DangerousGoods.DangerousGoods
        ,DangerousGoods.Class
        ,DangerousGoods.UNNumber
        ,DangerousGoods.PackClass
        ,DangerousGoods.DangerFactor
    from DangerousGoods
   where isnull(DangerousGoods.DangerousGoodsId,'0')  = isnull(@DangerousGoodsId, isnull(DangerousGoods.DangerousGoodsId,'0'))
     and isnull(DangerousGoods.DangerousGoodsCode,'%')  like '%' + isnull(@DangerousGoodsCode, isnull(DangerousGoods.DangerousGoodsCode,'%')) + '%'
     and isnull(DangerousGoods.DangerousGoods,'%')  like '%' + isnull(@DangerousGoods, isnull(DangerousGoods.DangerousGoods,'%')) + '%'
  
end
