﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Adjustment
  ///   Filename       : p_Production_Adjustment.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Mar 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Adjustment
(
 @InstructionId int,
 @Sign          char(2)
)
 
as
begin
	 set nocount on;
  
  declare @Error                   int,
          @Errormsg                nvarchar(500),
          @GetDate                 datetime,
          @WarehouseId             int,
          @ReceiptId               int,
          @ReceiptLineId           int,
          @OrderNumber             nvarchar(30),
          @InboundDocumentTypeCode nvarchar(10),
          @DeliveryNoteNumber      nvarchar(30),
          @DeliveryDate            datetime,
          @InterfaceExportHeaderId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select @WarehouseId   = WarehouseId,
         @ReceiptLineId = ReceiptLineId
    from Instruction (nolock)
   where InstructionId = @InstructionId
  
  select @ReceiptId = ReceiptId
    from ReceiptLine (nolock)
   where ReceiptLineId = @ReceiptLineId
  
  select @OrderNumber             = id.OrderNumber,
         @InboundDocumentTypeCode = idt.InboundDocumentTypeCode,
         @DeliveryNoteNumber      = r.DeliveryNoteNumber,
         @DeliveryDate            = r.DeliveryDate
    from Receipt               r (nolock)
    join InboundDocument      id (nolock) on r.InboundDocumentId      = id.InboundDocumentId
    join InboundDocumentType idt (nolock)on id.InboundDocumentTypeId  = idt.InboundDocumentTypeId
   where r.ReceiptId = @ReceiptId
     and idt.InboundDocumentTypeCode = 'PRV'
  
  if @@rowcount > 0
  begin
    insert InterfaceExportHeader
          (PrimaryKey,
           OrderNumber,
           RecordType,
           RecordStatus,
           CompanyCode,
           Company,
           Address,
           FromWarehouseCode,
           ToWarehouseCode,
           DeliveryNoteNumber,
           ContainerNumber,
           SealNumber,
           DeliveryDate,
           Remarks,
           NumberOfLines,
           Additional1,
           Additional2,
           Additional3,
           Additional4,
           Additional5,
           ProcessedDate)
    select @ReceiptId,
           @OrderNumber,
           case when @Sign = '+-'
                then 'PRV'
                else @InboundDocumentTypeCode
                end,
           'N',
           null,
           null,
           null,
           null,
           null,
           @DeliveryNoteNumber,
           null,
           null,
           @DeliveryDate,
           null,
           1,
           null,
           null,
           null,
           null,
           null,
           null
    
    select @Error = @@error, @InterfaceExportHeaderId = scope_identity()
    
    if @Error <> 0
      goto error
    
    if @Sign != '+-'
    begin
      insert InterfaceExportDetail
            (InterfaceExportHeaderId,
             ForeignKey,
             LineNumber,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             Quantity,
             Weight,
             Additional1,
             Additional2,
             Additional3,
             Additional4,
             Additional5)
      select @InterfaceExportHeaderId,
             @ReceiptId,
             i.InstructionId,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             case when @Sign = '-'
                  then i.ConfirmedQuantity * -1
                  else i.ConfirmedQuantity
                  end,
             j.NettWeight,
             isnull(p.ParentProductCode,p.ProductCode),
             null,
             null,
             null,
             'InstructionId = ' + convert(nvarchar(10), i.InstructionId)
        from Instruction        i (nolock)
        join Job                j (nolock) on i.JobId              = j.JobId
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
        join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
        join Product            p (nolock) on su.ProductId         = p.ProductId
        join SKU              sku (nolock) on su.SKUId             = sku.SKUId
        join Batch              b (nolock) on sub.BatchId          = b.BatchId
       where i.InstructionId = @InstructionId
    end
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
    
    if @Sign = '+-'
    begin
      insert InterfaceExportDetail
            (InterfaceExportHeaderId,
             ForeignKey,
             LineNumber,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             Quantity,
             Weight,
             Additional1,
             Additional2,
             Additional3,
             Additional4,
             Additional5)
      select @InterfaceExportHeaderId,
             @ReceiptId,
             i.InstructionId,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             i.ConfirmedQuantity,
             j.NettWeight,
             isnull(p.ParentProductCode,p.ProductCode),
             null,
             null,
             null,
           'InstructionId = ' + convert(nvarchar(10), i.InstructionId)
        from Instruction        i (nolock)
        join Job                j (nolock) on i.JobId              = j.JobId
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
        join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
        join Product            p (nolock) on su.ProductId         = p.ProductId
        join SKU              sku (nolock) on su.SKUId             = sku.SKUId
        join Batch              b (nolock) on sub.BatchId          = b.BatchId
       where i.InstructionId = @InstructionId
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
    end
  end
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Production_Adjustment'
    rollback transaction
    return @Error
end

