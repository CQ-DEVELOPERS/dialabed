﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLog_Delete
  ///   Filename       : p_InterfaceLog_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:26
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceLog table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLog_Delete
(
 @InterfaceLogId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceLog
     where InterfaceLogId = @InterfaceLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
