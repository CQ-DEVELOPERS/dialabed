﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLog_Update
  ///   Filename       : p_InterfaceLog_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:26
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceLog table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogId int = null,
  ///   @Data nvarchar(510) = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLog_Update
(
 @InterfaceLogId int = null,
 @Data nvarchar(510) = null,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceLogId = '-1'
    set @InterfaceLogId = null;
  
	 declare @Error int
 
  update InterfaceLog
     set Data = isnull(@Data, Data),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate) 
   where InterfaceLogId = @InterfaceLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
