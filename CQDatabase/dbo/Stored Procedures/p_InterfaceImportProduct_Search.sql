﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportProduct_Search
  ///   Filename       : p_InterfaceImportProduct_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:03
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportProduct table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportProductId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportProduct.InterfaceImportProductId,
  ///   InterfaceImportProduct.RecordStatus,
  ///   InterfaceImportProduct.ProcessedDate,
  ///   InterfaceImportProduct.ProductCode,
  ///   InterfaceImportProduct.Product,
  ///   InterfaceImportProduct.SKUCode,
  ///   InterfaceImportProduct.SKU,
  ///   InterfaceImportProduct.PalletQuantity,
  ///   InterfaceImportProduct.Barcode,
  ///   InterfaceImportProduct.MinimumQuantity,
  ///   InterfaceImportProduct.ReorderQuantity,
  ///   InterfaceImportProduct.MaximumQuantity,
  ///   InterfaceImportProduct.CuringPeriodDays,
  ///   InterfaceImportProduct.ShelfLifeDays,
  ///   InterfaceImportProduct.QualityAssuranceIndicator,
  ///   InterfaceImportProduct.ProductType,
  ///   InterfaceImportProduct.ProductCategory,
  ///   InterfaceImportProduct.OverReceipt,
  ///   InterfaceImportProduct.RetentionSamples,
  ///   InterfaceImportProduct.HostId,
  ///   InterfaceImportProduct.InsertDate,
  ///   InterfaceImportProduct.Additional1,
  ///   InterfaceImportProduct.Additional2,
  ///   InterfaceImportProduct.Additional3,
  ///   InterfaceImportProduct.Additional4,
  ///   InterfaceImportProduct.Additional5,
  ///   InterfaceImportProduct.Additional6,
  ///   InterfaceImportProduct.Additional7,
  ///   InterfaceImportProduct.Additional8,
  ///   InterfaceImportProduct.Additional9,
  ///   InterfaceImportProduct.Additional10,
  ///   InterfaceImportProduct.DangerousGoodsCode,
  ///   InterfaceImportProduct.ABCStatus,
  ///   InterfaceImportProduct.Samples 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportProduct_Search
(
 @InterfaceImportProductId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceImportProductId = '-1'
    set @InterfaceImportProductId = null;
  
 
  select
         InterfaceImportProduct.InterfaceImportProductId
        ,InterfaceImportProduct.RecordStatus
        ,InterfaceImportProduct.ProcessedDate
        ,InterfaceImportProduct.ProductCode
        ,InterfaceImportProduct.Product
        ,InterfaceImportProduct.SKUCode
        ,InterfaceImportProduct.SKU
        ,InterfaceImportProduct.PalletQuantity
        ,InterfaceImportProduct.Barcode
        ,InterfaceImportProduct.MinimumQuantity
        ,InterfaceImportProduct.ReorderQuantity
        ,InterfaceImportProduct.MaximumQuantity
        ,InterfaceImportProduct.CuringPeriodDays
        ,InterfaceImportProduct.ShelfLifeDays
        ,InterfaceImportProduct.QualityAssuranceIndicator
        ,InterfaceImportProduct.ProductType
        ,InterfaceImportProduct.ProductCategory
        ,InterfaceImportProduct.OverReceipt
        ,InterfaceImportProduct.RetentionSamples
        ,InterfaceImportProduct.HostId
        ,InterfaceImportProduct.InsertDate
        ,InterfaceImportProduct.Additional1
        ,InterfaceImportProduct.Additional2
        ,InterfaceImportProduct.Additional3
        ,InterfaceImportProduct.Additional4
        ,InterfaceImportProduct.Additional5
        ,InterfaceImportProduct.Additional6
        ,InterfaceImportProduct.Additional7
        ,InterfaceImportProduct.Additional8
        ,InterfaceImportProduct.Additional9
        ,InterfaceImportProduct.Additional10
        ,InterfaceImportProduct.DangerousGoodsCode
        ,InterfaceImportProduct.ABCStatus
        ,InterfaceImportProduct.Samples
    from InterfaceImportProduct
   where isnull(InterfaceImportProduct.InterfaceImportProductId,'0')  = isnull(@InterfaceImportProductId, isnull(InterfaceImportProduct.InterfaceImportProductId,'0'))
  
end
