﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceTables_Parameter
  ///   Filename       : p_InterfaceTables_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:50
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceTables table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceTables.InterfaceTableId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceTables_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceTableId
        ,null as 'InterfaceTables'
  union
  select
         InterfaceTables.InterfaceTableId
        ,InterfaceTables.InterfaceTableId as 'InterfaceTables'
    from InterfaceTables
  
end
