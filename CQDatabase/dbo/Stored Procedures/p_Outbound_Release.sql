﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Release
  ///   Filename       : p_Outbound_Release.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Release
(
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @IssueLineId        int = null,
 @StatusCode         nvarchar(10) = 'RL'
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @rowcount          int,
          @jobcount          int,
          @Transaction       bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
  begin
    begin transaction
    set @Transaction = 1
  end
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @IssueLineId = -1
    set @IssueLineId = null
  
  begin
    declare @TableHeader as table
    (
     OutboundShipmentId int,
     IssueId            int,
     IssueLineId        int
    )
    if @StatusCode in ('M','RL','PC','PS','SA')
    begin
      if @OutboundShipmentId is null
      begin
        if @IssueId is not null or @IssueLineId is not null
          insert @TableHeader
                (OutboundShipmentId,
                 IssueId,
                 IssueLineId)
          select null,
                 il.IssueId,
                 il.IssueLineId
            from IssueLine         il (nolock)
            join Issue              i (nolock) on il.IssueId  = i.IssueId
            join Status             s (nolock) on i.StatusId  = s.StatusId
           where il.IssueId             = isnull(@IssueId, il.IssueId)
             and il.IssueLineId         = isnull(@IssueLineId, il.IssueLineId)
             and s.StatusCode          in ('M','RL','PC','PS','SA')
          
          update th
             set OutboundShipmentId = osi.OutboundShipmentId
            from @TableHeader th
            join OutboundShipmentIssue osi on th.IssueId = osi.IssueId
      end
      else
      begin
        insert @TableHeader
              (OutboundShipmentId,
               IssueId,
               IssueLineId)
        select osi.OutboundShipmentId,
               il.IssueId,
               il.IssueLineId
          from OutboundShipmentIssue osi (nolock)
          join Issue                   i (nolock) on osi.IssueId  = i.IssueId
          join IssueLine              il (nolock) on i.IssueId = il.IssueId
          join Status                  s (nolock) on i.StatusId  = s.StatusId
         where osi.OutboundShipmentId = @OutboundShipmentId
           and s.StatusCode          in ('M','RL','PC','PS','SA')
      end
    end
    else
    begin
      if @OutboundShipmentId is null
      begin
        if @IssueId is not null or @IssueLineId is not null
          insert @TableHeader
                (OutboundShipmentId,
                 IssueId,
                 IssueLineId)
          select null,
                 il.IssueId,
                 il.IssueLineId
            from IssueLine         il (nolock)
           where il.IssueId             = isnull(@IssueId, il.IssueId)
             and il.IssueLineId         = isnull(@IssueLineId, il.IssueLineId)
      end
      else
      begin
        insert @TableHeader
              (OutboundShipmentId,
               IssueId,
               IssueLineId)
        select osi.OutboundShipmentId,
               il.IssueId,
               il.IssueLineId
          from OutboundShipmentIssue osi (nolock)
          join IssueLine         il (nolock) on osi.IssueId = il.IssueId
         where osi.OutboundShipmentId = @OutboundShipmentId
      end
    end
    select @StatusId = dbo.ufn_StatusId('IS',@StatusCode)
    
    update IssueLine
       set StatusId = @StatusId
      from @TableHeader th
      join IssueLine    il on th.IssueLineId = il.IssueLineId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    if @StatusCode in ('M','RL')
    begin
      update IssueLine
         set StatusId = dbo.ufn_StatusId('IS','CD')
        from @TableHeader th
        join IssueLine    il on th.IssueLineId = il.IssueLineId
       where Quantity <= 0
      
      select @Error = @@Error, @rowcount = @@rowcount
      
      if @Error <> 0
        goto error
    end
    
    insert IssueLineHistory
          (IssueLineId,
           StatusId,
           CommandType,
           InsertDate)
    select IssueLineId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    update i
       set StatusId = @StatusId
      from @TableHeader th
      join Issue         i on th.IssueId = i.IssueId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    insert IssueHistory
          (IssueId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           IssueId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    update os
       set StatusId = @StatusId
      from @TableHeader     th
      join OutboundShipment os on th.OutboundShipmentId = os.OutboundShipmentId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    insert OutboundShipmentHistory
          (OutboundShipmentId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           OutboundShipmentId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    if @StatusCode in ('M','RL','PC','PS','SA')
    begin
      update j
         set StatusId = @StatusId,
             OperatorId = null
        from @TableHeader     th
        join Instruction       i on th.IssueLineId = i.IssueLineId
        join Job               j on i.JobId        = j.JobId
        join Status            s on j.StatusId     = s.StatusId
       where s.StatusCode != 'NS'
         and s.StatusCode in ('M','RL','PC','PS','SA')
      
      select @Error = @@Error, @rowcount = @@rowcount
      
      select @jobcount = isnull(@jobcount,0) + isnull(@rowcount,0)
    end
    else
    begin
      update j
         set StatusId   = @StatusId
        from @TableHeader     th
        join Instruction       i on th.IssueLineId = i.IssueLineId
        join Job               j on i.JobId        = j.JobId
        join Status            s on j.StatusId     = s.StatusId
       where s.StatusCode != 'NS'
      
      select @Error = @@Error, @rowcount = @@rowcount
      
      select @jobcount = isnull(@jobcount,0) + isnull(@rowcount,0)
    end
    
    if @Error <> 0
      goto error
    
    insert JobHistory
          (JobId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           j.JobId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader     th
      join Instruction       i on th.IssueLineId = i.IssueLineId
      join Job               j on i.JobId        = j.JobId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    if @StatusCode in ('M','RL','PC','PS','SA')
    begin
      update j
         set StatusId = @StatusId,
             OperatorId = null
        from @TableHeader     th
        join Instruction       i on th.OutboundShipmentId = i.OutboundShipmentId
        join Job               j on i.JobId               = j.JobId
        join Status            s on j.StatusId            = s.StatusId
       where s.StatusCode != 'NS'
         and s.StatusCode in ('M','RL','PC','PS','SA')
      
      select @Error = @@Error, @rowcount = @@rowcount
      
      select @jobcount = isnull(@jobcount,0) + isnull(@rowcount,0)
      
      if @Error <> 0
        goto error
      
      if @StatusCode = 'RL'
      begin
        update op
           set Release = @Getdate
          from @TableHeader     th
          join Instruction       i on th.IssueLineId = i.IssueLineId
          join Job                  j on i.JobId               = j.JobId
          join OutboundPerformance op on j.JobId               = op.JobId
          join Status               s on j.StatusId            = s.StatusId
         where s.StatusCode != 'NS'
           and s.StatusCode in ('M','RL','PC','PS','SA')
           and op.Release is null
        
        select @Error = @@Error, @rowcount = @@rowcount
        
        if @Error <> 0
          goto error
        
        update op
           set Release = @Getdate
          from @TableHeader     th
          join Instruction          i on th.OutboundShipmentId = i.OutboundShipmentId
          join Job                  j on i.JobId               = j.JobId
          join OutboundPerformance op on j.JobId               = op.JobId
          join Status               s on j.StatusId            = s.StatusId
         where s.StatusCode != 'NS'
           and s.StatusCode in ('M','RL','PC','PS','SA')
           and op.Release is null
        
        select @Error = @@Error, @rowcount = @@rowcount
        
        if @Error <> 0
          goto error
      end
    end
    else
    begin
      update j
         set StatusId = @StatusId
        from @TableHeader     th
        join Instruction       i on th.OutboundShipmentId = i.OutboundShipmentId
        join Job               j on i.JobId               = j.JobId
        join Status            s on j.StatusId            = s.StatusId
       where s.StatusCode != 'NS'
      
      select @jobcount = isnull(@jobcount,0) + isnull(@rowcount,0)
      
      select @Error = @@Error, @rowcount = @@rowcount
      
      if @Error <> 0
        goto error
    end
    
    if @StatusCode in ('RL','PS') and @jobcount = 0
    begin
      set @error = -1
      goto error
    end
    
    insert JobHistory
          (JobId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           j.JobId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader     th
      join Instruction       i on th.OutboundShipmentId = i.OutboundShipmentId
      join Job               j on i.JobId               = j.JobId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
  end
  
  if @Transaction = 1
    commit transaction
  return
  
  error:
    if @Transaction = 1
    begin
      raiserror 900000 'Error executing p_Outbound_Release'
      rollback transaction
    end
    
    return @Error
end
