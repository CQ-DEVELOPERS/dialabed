﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_BOMHeader_Search
  ///   Filename       : p_Static_BOMHeader_Search.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@ProductCode> 
  ///       <@Product>	 
  ///       <@Barcode>	 
  /// </param>
  /// <returns>
  ///        <ProductId>
  ///        <StatusId> 
  ///        <Status> 
  ///        <ProductCode> 
  ///        <Product> 
  ///        <Barcode> 
  ///        <MinimumQuantity> 
  ///        <ReorderQuantity> 
  ///        <MaximumQuantity> 
  ///        <CuringPeriodDays>        
  ///        <ShelfLifeDays> 
  ///        <QualityAssuranceIndicator>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_BOMHeader_Search
(
 @ProductCode           nvarchar(30),
 @Product	            nvarchar(50),
 @Barcode	            nvarchar(50)
)
 
as
begin
	 set nocount on;

SELECT		P.ProductId, 
			P.StatusId, 
			S.Status, 
			P.ProductCode, 
			P.Product, 
			P.Barcode, 
			P.MinimumQuantity, 
			P.ReorderQuantity, 
			P.MaximumQuantity, 
			P.CuringPeriodDays,           
			P.ShelfLifeDays, 
			P.QualityAssuranceIndicator,
			P.ProductType,
			P.OverReceipt,
			P.HostId,
			p.RetentionSamples,
			Bh.BOMHeaderId,
			Bh.Description,
			Bh.Remarks,
			Bh.Instruction,
			Bh.BOMType
FROM		BOMheader Bh 
			Inner join StorageUnit SU ON Bh.StorageUnitId = SU.StorageUnitId 
			Inner join Product AS P ON SU.ProductId = P.ProductId
            Inner join Status AS S ON P.StatusId = S.StatusId
			where P.ProductCode like isnull(@ProductCode,p.ProductCode) + '%'
			And p.Product like isnull(@Product,p.Product) + '%'
			And isnull(p.Barcode,'') like isnull(@Barcode,isnull(p.Barcode,'')) + '%'
 
end
