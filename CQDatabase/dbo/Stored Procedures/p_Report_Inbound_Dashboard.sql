﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Inbound_Dashboard
  ///   Filename       : p_Report_Inbound_Dashboard.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Inbound_Dashboard
(
 @WarehouseId int,
 @Production  bit = null
)
 
as
begin
	 set nocount on;
  
  select @WarehouseId as 'WarehouseId',
         @Production as 'Production'
end
