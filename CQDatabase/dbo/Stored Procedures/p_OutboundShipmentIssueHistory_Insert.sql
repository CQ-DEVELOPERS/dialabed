﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssueHistory_Insert
  ///   Filename       : p_OutboundShipmentIssueHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jun 2013 12:36:32
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundShipmentIssueHistory table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null,
  ///   @DropSequence int = null,
  ///   @CommandType varchar(10) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   OutboundShipmentIssueHistory.OutboundShipmentId,
  ///   OutboundShipmentIssueHistory.IssueId,
  ///   OutboundShipmentIssueHistory.DropSequence,
  ///   OutboundShipmentIssueHistory.CommandType,
  ///   OutboundShipmentIssueHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssueHistory_Insert
(
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @DropSequence int = null,
 @CommandType varchar(10) = null,
 @InsertDate datetime = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OutboundShipmentIssueHistory
        (OutboundShipmentId,
         IssueId,
         DropSequence,
         CommandType,
         InsertDate)
  select @OutboundShipmentId,
         @IssueId,
         @DropSequence,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
