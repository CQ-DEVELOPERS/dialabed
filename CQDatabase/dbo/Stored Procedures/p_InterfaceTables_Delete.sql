﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceTables_Delete
  ///   Filename       : p_InterfaceTables_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:50
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceTables table.
  /// </remarks>
  /// <param>
  ///   @InterfaceTableId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceTables_Delete
(
 @InterfaceTableId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceTables
     where InterfaceTableId = @InterfaceTableId
  
  select @Error = @@Error
  
  
  return @Error
  
end
