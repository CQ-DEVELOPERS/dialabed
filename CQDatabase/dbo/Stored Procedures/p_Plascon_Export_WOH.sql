﻿
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Export_WOH
  ///   Filename       : p_Plascon_Export_WOH.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 2011-11-04
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Plascon_Export_WOH]
as
begin
	 set nocount on;
  
    declare @Error             int,
            @Errormsg          varchar(500),
            @GetDate           datetime,
            @RecordType        char(3),
            @SequenceNumber    int,
            @Increment         int
  
    select @RecordType = 'WOH'
          ,@GetDate = dbo.ufn_Getdate()
          ,@Errormsg = 'p_Plascon_Export_WOH - Error executing p_Plascon_Export_WOH'
  
    begin transaction
        
    update InterfaceExportStockAdjustment
       set RecordStatus  = 'Y',
           ProcessedDate = @getdate
     where RecordStatus = 'N'
       and RecordType   = isnull(@RecordType,RecordType)
       
    set @Increment = @@ROWCOUNT
  
    select @Error = @@Error, @Errormsg = 'p_Plascon_Export_WOH - Error updating InterfaceExportStockAdjustment'
  
    if @Error <> 0
        goto error

    exec p_Sequence_Number 
                 @Value = @SequenceNumber output
                ,@Increment = @Increment
    
    insert InterfaceExtract
           (
            RecordType,
            SequenceNumber,
            OrderNumber,
            LineNumber,
            RecordStatus,
            ExtractedDate,
            Data
           )
    select  @RecordType,
            sequence_no,
            '',
            null,
            'N',
            extract_date,            
            replicate('0',9  - datalength(convert(varchar(9),sequence_no))) + convert(varchar(9),sequence_no) + 
            record_type + 
            replicate('0',3  - datalength(convert(varchar(3),branch_code))) + convert(varchar(3),branch_code) + 
            replicate(' ',9 - datalength(convert(varchar(9),ltrim(stock_no)))) + convert(varchar(9),ltrim(stock_no))     +  
            replicate('0',4  - datalength(convert(varchar(4),sku_code))) + convert(varchar(4),sku_code) + 
            reason_code +
            replicate('0',6 - datalength(convert(varchar(6),convert(int,wms_qty)))) + convert(varchar(6),convert(int,wms_qty))
    From
        (Select  s.sequence_no
                ,@RecordType As record_type
                ,'045' As branch_code 
                ,h.ProductCode As stock_no 
                ,h.SKUCode As sku_code
                ,h.Quantity As wms_qty
                ,isnull(h.Additional3,'16') As reason_code
                ,CONVERT(Varchar(10), @GetDate, 120) As extract_date
         From 
            (select InterfaceExportStockAdjustmentId, ROW_NUMBER() Over (Order By InterfaceExportStockAdjustmentId) + @SequenceNumber As sequence_no 
             from InterfaceExportStockAdjustment Where ProcessedDate = @GetDate) s 
            Inner Join InterfaceExportStockAdjustment h on s.InterfaceExportStockAdjustmentId = h.InterfaceExportStockAdjustmentId 
            ) ExtractTable
        Order By sequence_no
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end

