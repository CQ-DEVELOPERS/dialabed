﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_Packaging_Update
  ///   Filename       : p_BOM_Packaging_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Dec 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_Packaging_Update
(
 @bomPackagingId int,
 @quantity       float
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update BOMPackaging
     set Quantity = @quantity
   where BOMPackagingId = @bomPackagingId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_BOM_Packaging_Update'
    rollback transaction
    return @Error
end
