﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_InstructionType
  ///   Filename       : p_Mobile_InstructionType.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Sep 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_InstructionType
(
 @InstructionId int
)
 
as
begin
	 set nocount on;
	 
	 declare @InstructionRefId   int,
	         @OutboundDocumentTypeId int,
	         @InstructionTypeCode    nvarchar(10)
  
  select @InstructionTypeCode = it.InstructionTypeCode
    from Instruction            i (nolock)
    join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where i.InstructionId = @InstructionId
	 
	 if @InstructionTypeCode != 'R'
	 begin
	   select @InstructionRefId = isnull(InstructionRefId, InstructionId)
	     from Instruction (nolock)
	    where InstructionId = @InstructionId
  	 
	   select @OutboundDocumentTypeId = OutboundDocumentTypeId
	     from IssueLineInstruction (nolock)
	    where InstructionId = @InstructionRefId
  	 
	   if (select OutboundDocumentTypeCode
	         from OutboundDocumentType (nolock)
	        where OutboundDocumentTypeId = @OutboundDocumentTypeId) in ('MO','WAVE','KIT')
      select @InstructionTypeCode = 'R'
  end
  
  select @InstructionTypeCode as 'InstructionTypeCode'
end
