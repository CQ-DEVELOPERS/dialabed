﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_TransferMaster_Insert
  ///   Filename       : p_TransferMaster_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:36:03
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the TransferMaster table.
  /// </remarks>
  /// <param>
  ///   @FromWarehouseCode nvarchar(100) = null,
  ///   @ProductCode nvarchar(100) = null,
  ///   @ToWarehouseCode nvarchar(100) = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   TransferMaster.FromWarehouseCode,
  ///   TransferMaster.ProductCode,
  ///   TransferMaster.ToWarehouseCode,
  ///   TransferMaster.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_TransferMaster_Insert
(
 @FromWarehouseCode nvarchar(100) = null,
 @ProductCode nvarchar(100) = null,
 @ToWarehouseCode nvarchar(100) = null,
 @Quantity float = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert TransferMaster
        (FromWarehouseCode,
         ProductCode,
         ToWarehouseCode,
         Quantity)
  select @FromWarehouseCode,
         @ProductCode,
         @ToWarehouseCode,
         @Quantity 
  
  select @Error = @@Error
  
  
  return @Error
  
end
