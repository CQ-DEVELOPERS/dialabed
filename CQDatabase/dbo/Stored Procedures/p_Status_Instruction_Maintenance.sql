﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Instruction_Maintenance
  ///   Filename       : p_Status_Instruction_Maintenance.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Instruction_Maintenance
 
as
begin
	 set nocount on;
  
  select -1      as 'StatusId',
         '{All}' as 'Status'
  union
  select StatusId,
         Status
    from Status (nolock)
   where Type = 'R'
     and StatusCode in ('RC','PR')
  order by Status
end
