﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DI_Planning_Order_Search
  ///   Filename       : p_DI_Planning_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DI_Planning_Order_Search
(
 @WarehouseId            int,
 @OutboundShipmentId     int,
 @OutboundDocumentTypeId int,
 @WaveId                 int = null,
 @ExternalCompanyCode    nvarchar(30),
 @ExternalCompany        nvarchar(255),
 @OrderNumber            nvarchar(30),
 @FromDate               datetime,
 @ToDate                 datetime,
 @PrincipalId           int = null,
 @ShowOrders             nvarchar(10)
)
 
as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   WaveId                    int,
   Wave                      nvarchar(50),
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   ReferenceNumber           nvarchar(30),
   ExternalCompanyId         int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   RouteId                   int,
   Route                     nvarchar(50),
   NumberOfLines             int,
   ShortPicks                int,
   DeliveryDate              datetime,
   CreateDate                datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   PriorityId                int,
   Priority                  nvarchar(50),
   LocationId                int,
   Location                  nvarchar(15),
   Rating                    int,
   AvailabilityIndicator     nvarchar(20),
   Remarks                   nvarchar(255),
   Total                     numeric(13,3),
   Complete                  numeric(13,3),
   PercentageComplete        numeric(13,3),
   Units                     int,
   Releases                  int,
   Weight                    float,
   Orderby                   int,
   PrincipalId               int,
   PrincipalCode             nvarchar(30),
   
   NumberOfOrders            int,
   PercentageAvailable       numeric(13,3),
   PercentageDrawDown        numeric(13,3),
   PercentageFulfilled       numeric(13,3),
   BulkPallets               int,
   PalletSpaces              int,
   SimilarSKUDI              nvarchar(max)
  );
  
  declare @GetDate           datetime,
          @PalletSpaces      int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @ExternalCompanyCode is null
    set @ExternalCompanyCode = ''
  
  if @ExternalCompany is null
    set @ExternalCompany = ''
  
  if @PrincipalId = -1
    set @PrincipalId = null
  
  if @WaveId in (-1,0)
    set @WaveId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (WaveId,
           Wave,
           OutboundShipmentId,
           ReferenceNumber,
           OutboundDocumentId,
           IssueId,
           LocationId,
           OrderNumber,
           ExternalCompanyId,
           CustomerCode,
           Customer,
           RouteId,
           Route,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           OutboundDocumentType,
           Rating,
           Remarks,
           NumberOfLines,
           ShortPicks,
           Total,
           Complete,
           Units,
           Releases,
           Weight,
           PrincipalId,
           NumberOfOrders)
    select w.WaveId,
           w.Wave,
           osi.OutboundShipmentId,
           os.ReferenceNumber,
           od.OutboundDocumentId,
           i.IssueId,
           isnull(os.LocationId, i.LocationId),
           od.OrderNumber,
           od.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           isnull(os.RouteId, i.RouteId),
           os.Route,
           isnull(os.StatusId, i.StatusId),
           s.Status,
           i.PriorityId,
           isnull(os.ShipmentDate, i.DeliveryDate),
           od.CreateDate,
           odt.OutboundDocumentType,
           null,
           i.Remarks,
           i.NumberOfLines,
           i.ShortPicks,
           i.Total,
           i.Complete,
           i.Units,
           i.Releases,
           i.Weight,
           od.PrincipalId,
           isnull(os.NumberOfOrders,1)
      from OutboundDocument      od (nolock)
      join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      left 
      join OutboundShipmentIssue osi (nolock) on i.IssueId                = osi.IssueId
      left
      join OutboundShipment       os (nolock) on osi.OutboundShipmentId   = os.OutboundShipmentId 
      join Status                 s (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      left
      join Wave                    w (nolock) on i.WaveId                  = w.WaveId
     where od.OutboundDocumentTypeId  = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode  like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany      like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber          like isnull(@OrderNumber  + '%', od.OrderNumber)
       and (isnull(os.ShipmentDate, isnull(i.DeliveryDate, od.CreateDate)) between @FromDate and @ToDate
       or   od.CreateDate                                                  between @FromDate and @ToDate)
       and s.Type                     = 'IS'
       and s.StatusCode             in ('W')--,'P','SA')
       and i.WarehouseId              = @WarehouseId
       and isnull(od.PrincipalId, -1) = isnull(@PrincipalId, isnull(od.PrincipalId, -1))
       and odt.OutboundDocumentTypeCode not in ('REP','KIT')
       and isnull(i.WaveId, -1) = isnull(@WaveId, isnull(i.WaveId, -1))
  end
  else
    insert @TableResult
          (WaveId,
           Wave,
           OutboundShipmentId,
           ReferenceNumber,
           OutboundDocumentId,
           IssueId,
           LocationId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           OutboundDocumentType,
           Rating,
           Remarks,
           NumberOfLines,
           ShortPicks,
           Total,
           Complete,
           Units,
           Releases,
           Weight,
           PrincipalId,
           NumberOfOrders)
    select w.WaveId,
           w.Wave,
           osi.OutboundShipmentId,
           os.ReferenceNumber,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           odt.OutboundDocumentType,
           ec.Rating,
           i.Remarks,
           i.NumberOfLines,
           i.ShortPicks,
           i.Total,
           i.Complete,
           i.Units,
           i.Releases,
           i.Weight,
           od.PrincipalId,
           1
      from OutboundDocument       od (nolock)
      join ExternalCompany        ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                   i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                  s (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue osi (nolock) on i.IssueId                 = osi.IssueId
      left
      join OutboundShipment       os (nolocK) on osi.OutboundShipmentId    = os.OutboundShipmentId
      left
      join Wave                    w (nolock) on i.WaveId                  = w.WaveId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and s.StatusCode             in ('W')--,'P','SA')
       and isnull(od.PrincipalId, -1) = isnull(@PrincipalId, isnull(od.PrincipalId, -1))
       and isnull(i.WaveId, -1) = isnull(@WaveId, isnull(i.WaveId, -1))
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Priority = p.Priority,
         OrderBy  = p.OrderBy
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal     p (nolock) on tr.PrincipalId = p.PrincipalId
  
  update @TableResult
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @WarehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set PercentageComplete = (Complete / Total) * 100
   where Complete > 0
     and Total    > 0
  
  update @TableResult
     set PercentageComplete = 0
   where PercentageComplete is null
  
  update @TableResult
     set PercentageComplete = 0
   where PercentageComplete is null
  
  select @PalletSpaces = count(1)
    from viewLocation
   where Used = 0
     and Area = 'WavePick'
  
  update @TableResult
     set PalletSpaces = @PalletSpaces
  
  update tr
     set PercentageAvailable = wa.PercentageAvailable,
         PercentageDrawDown  = wa.PercentageDrawDown,
         PercentageFulfilled = wa.PercentageAvailable + wa.PercentageDrawDown,
         BulkPallets         = wa.BulkPallets,
         NumberOfLines       = wa.NumberOfLines,
         SimilarSKUDI        = wa.SimilarSKUDI
    from @TableResult    tr
    join WaveAvailablity wa (nolock) on tr.IssueId = wa.IssueId or tr.OutboundShipmentId = wa.OutboundShipmentId
  
  --delete @TableResult where OutboundShipmentId != 2 or OutboundShipmentId is null
  
  if @ShowOrders = 'false'
    select WaveId,
           Wave,
           case when WaveId is null
                then null
                else 'Release'
                end as 'Release',
           min(IssueId) as 'IssueId',
           isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
           case when OutboundShipmentId is null then OrderNumber else ReferenceNumber end as 'OrderNumber',
           case when OutboundShipmentId is null then CustomerCode end as 'CustomerCode',
           case when OutboundShipmentId is null then Customer end as 'Customer',
           isnull(RouteId,-1) as 'RouteId',
           Route,
           max(NumberOfLines),
           sum(ShortPicks) as 'ShortPicks',
           sum(ShortPicks) as UnitShort,
           min(DeliveryDate) as 'DeliveryDate',
           min(CreateDate) as 'CreateDate',
           Status,
           PriorityId,
           Priority,
           OutboundDocumentType,
           isnull(LocationId,-1) as 'LocationId',
           Location,
           Rating,
           AvailabilityIndicator,
           max(Remarks) as 'Remarks',
           sum(Complete) as 'Complete',
           sum(Total) as 'Total',
           sum(convert(int, round(PercentageComplete,0))) / count(1) as 'PercentageComplete',
           sum(Units) as 'Units',
           sum(Releases) as 'Releases',
           sum(Weight) as 'Weight',
           PrincipalCode,
           max(NumberOfOrders) as 'NumberOfOrders',
           min(PercentageAvailable) as 'PercentageAvailable',
           min(PercentageDrawDown) as 'PercentageDrawDown',
           min(PercentageFulfilled) as 'PercentageFulfilled',
           min(BulkPallets) as 'BulkPallets',
           PalletSpaces,
           max(SimilarSKUDI) as 'SimilarSKUDI'
      from @TableResult
     group by WaveId,
              Wave,
              isnull(OutboundShipmentId, -1),
              case when OutboundShipmentId is null then OrderNumber else ReferenceNumber end,
              case when OutboundShipmentId is null then CustomerCode end,
              case when OutboundShipmentId is null then Customer end,
              isnull(RouteId,-1),
              Route,
              Status,
              PriorityId,
              Priority,
              OutboundDocumentType,
              isnull(LocationId,-1),
              Location,
              Rating,
              AvailabilityIndicator,
              PrincipalCode,
              PalletSpaces
    order by Location,
             OutboundShipmentId,
             OrderNumber
  else
    select WaveId,
           Wave,
           case when WaveId is null
                then null
                else 'Release'
                end as 'Release',
           IssueId,
           isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
           OrderNumber,
           CustomerCode,
           Customer,
           isnull(RouteId,-1) as 'RouteId',
           Route,
           NumberOfLines,
           ShortPicks,
           ShortPicks as UnitShort,
           DeliveryDate,
           CreateDate,
           Status,
           PriorityId,
           Priority,
           OutboundDocumentType,
           isnull(LocationId,-1) as 'LocationId',
           Location,
           Rating,
           AvailabilityIndicator,
           Remarks,
           Complete,
           Total,
           convert(int, round(PercentageComplete,0)) as 'PercentageComplete',
           Units,
           Releases,
           Weight,
           PrincipalCode,
           NumberOfOrders,
           PercentageAvailable,
           PercentageDrawDown,
           PercentageFulfilled,
           BulkPallets,
           PalletSpaces,
           SimilarSKUDI
      from @TableResult
  order by Location,
           OrderBy,
           OutboundShipmentId,
           OrderNumber
end

