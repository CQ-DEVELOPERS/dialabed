﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Create_Load
  ///   Filename       : p_Mobile_Create_Load.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Create_Load
(
 @WarehouseId int,
 @OperatorId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @IntransitLoadId   int,
          @StatusId          int
  
  set @Errormsg = 'Error executing p_Mobile_Create_Load'
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('IT','LG')
  
  begin transaction
  
  exec @Error = p_IntransitLoad_Insert
   @IntransitLoadId = @IntransitLoadId output,
   @WarehouseId     = @WarehouseId,
   @StatusId        = @StatusId,
   @CreatedById     = @OperatorId,
   @CreateDate      = @GetDate
   
  if @Error <> 0
    goto error
  
  if @IntransitLoadId is null
  begin
    set @IntransitLoadId = -1
    goto result
  end
  
  result:
    commit transaction
    select @IntransitLoadId as '@IntransitLoadId'
    return @IntransitLoadId
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    select @Error as '@Error'
    return @Error
end
