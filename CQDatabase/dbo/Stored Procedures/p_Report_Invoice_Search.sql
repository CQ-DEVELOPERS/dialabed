﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Invoice_Search
  ///   Filename       : p_Report_Invoice_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Invoice_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   CreateDate                datetime,
   Extracted                 datetime,
   InvoiceDate               datetime,
   Printed                   int,
   InvoiceNumber             nvarchar(30),
   InterfaceImportIPHeaderId int
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (OutboundDocumentId,
           IssueId,
           OrderNumber,
           CustomerCode,
           Customer,
           CreateDate)
    select od.OutboundDocumentId,
           i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           od.CreateDate
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
       and od.CreateDate      between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('CD','D','DC','C')
       and od.WarehouseId            = @WarehouseId
  end
  else
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           OrderNumber,
           CustomerCode,
           Customer,
           CreateDate)
    select osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           od.CreateDate
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
      join OutboundShipment       os (nolock) on osi.OutboundShipmentId = os.OutboundShipmentId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and s.StatusCode          in ('CD','D','DC','C')
  
  --update tr
  --   set OutboundShipmentId = si.OutboundShipmentId
  --  from @TableResult  tr
  --  join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
  --  join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  
  update tr
     set Extracted = h.ProcessedDate
    from @TableResult           tr
    join InterfaceExportSOHeader h (nolock) on tr.IssueId = h.IssueId
  
  update tr
     set InterfaceImportIPHeaderId = (select max(h.InterfaceImportIPHeaderId)
                                        from InterfaceImportIPHeader h (nolock)
                                       where tr.IssueId = h.IssueId)
    from @TableResult           tr
  
  update tr
     set Printed = h.Printed,
         InvoiceDate = h.InvoiceDate,
         InvoiceNumber = h.PrimaryKey
    from @TableResult           tr
    join InterfaceImportIPHeader h (nolock) on tr.InterfaceImportIPHeaderId = h.InterfaceImportIPHeaderId
  
  --update tr
  --   set InvoiceDate = h.ProcessedDate,
  --       InvoiceNumber = h.InvoiceNumber
  --  from @TableResult    tr
  --  join InterfaceInvoice h (nolock) on tr.OrderNumber = h.OrderNumber
  
  select IssueId,
         isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
         OrderNumber,
         InvoiceNumber,
         CustomerCode,
         Customer,
         CreateDate,
         Extracted,
         InvoiceDate,
         Printed
    from @TableResult
  order by Printed desc,
           InvoiceNumber,
           'OutboundShipmentId',
           OrderNumber
end
