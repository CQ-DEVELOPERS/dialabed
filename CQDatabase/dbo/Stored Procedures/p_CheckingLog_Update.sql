﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingLog_Update
  ///   Filename       : p_CheckingLog_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:15
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the CheckingLog table.
  /// </remarks>
  /// <param>
  ///   @CheckingLogId int = null,
  ///   @WarehouseId int = null,
  ///   @ReferenceNumber nvarchar(100) = null,
  ///   @JobId int = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @StorageUnitId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @OperatorId int = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingLog_Update
(
 @CheckingLogId int = null,
 @WarehouseId int = null,
 @ReferenceNumber nvarchar(100) = null,
 @JobId int = null,
 @Barcode nvarchar(100) = null,
 @StorageUnitId int = null,
 @StorageUnitBatchId int = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @OperatorId int = null,
 @StartDate datetime = null,
 @EndDate datetime = null 
)
 
as
begin
	 set nocount on;
  
  if @CheckingLogId = '-1'
    set @CheckingLogId = null;
  
	 declare @Error int
 
  update CheckingLog
     set WarehouseId = isnull(@WarehouseId, WarehouseId),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         JobId = isnull(@JobId, JobId),
         Barcode = isnull(@Barcode, Barcode),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         Batch = isnull(@Batch, Batch),
         Quantity = isnull(@Quantity, Quantity),
         OperatorId = isnull(@OperatorId, OperatorId),
         StartDate = isnull(@StartDate, StartDate),
         EndDate = isnull(@EndDate, EndDate) 
   where CheckingLogId = @CheckingLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
