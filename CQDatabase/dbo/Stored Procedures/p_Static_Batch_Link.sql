﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Batch_Link
  ///   Filename       : p_Static_Batch_Link.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Batch_Link
(
 @WarehouseId   int,
 @StorageUnitId int,
 @BatchId       int,
 @OperatorId    int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_StorageUnitBatch_Insert
   @StorageUnitBatchId = null,
   @BatchId = @BatchId,
   @StorageUnitId = @StorageUnitId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Static_Batch_Link'
    rollback transaction
    return @Error
end
