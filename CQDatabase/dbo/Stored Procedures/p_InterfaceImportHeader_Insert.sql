﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportHeader_Insert
  ///   Filename       : p_InterfaceImportHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:26
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportHeaderId int = null output,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @InvoiceNumber nvarchar(60) = null,
  ///   @RecordType nvarchar(60) = null,
  ///   @RecordStatus char(1) = null,
  ///   @CompanyCode nvarchar(60) = null,
  ///   @Company nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @Route nvarchar(100) = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @ContainerNumber nvarchar(60) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @DeliveryDate nvarchar(60) = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @VatPercentage decimal(13,3) = null,
  ///   @VatSummary decimal(13,3) = null,
  ///   @Total decimal(13,3) = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null,
  ///   @ErrorMsg nvarchar(510) = null,
  ///   @HostStatus nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportHeader.InterfaceImportHeaderId,
  ///   InterfaceImportHeader.PrimaryKey,
  ///   InterfaceImportHeader.OrderNumber,
  ///   InterfaceImportHeader.InvoiceNumber,
  ///   InterfaceImportHeader.RecordType,
  ///   InterfaceImportHeader.RecordStatus,
  ///   InterfaceImportHeader.CompanyCode,
  ///   InterfaceImportHeader.Company,
  ///   InterfaceImportHeader.Address,
  ///   InterfaceImportHeader.FromWarehouseCode,
  ///   InterfaceImportHeader.ToWarehouseCode,
  ///   InterfaceImportHeader.Route,
  ///   InterfaceImportHeader.DeliveryNoteNumber,
  ///   InterfaceImportHeader.ContainerNumber,
  ///   InterfaceImportHeader.SealNumber,
  ///   InterfaceImportHeader.DeliveryDate,
  ///   InterfaceImportHeader.Remarks,
  ///   InterfaceImportHeader.NumberOfLines,
  ///   InterfaceImportHeader.VatPercentage,
  ///   InterfaceImportHeader.VatSummary,
  ///   InterfaceImportHeader.Total,
  ///   InterfaceImportHeader.Additional1,
  ///   InterfaceImportHeader.Additional2,
  ///   InterfaceImportHeader.Additional3,
  ///   InterfaceImportHeader.Additional4,
  ///   InterfaceImportHeader.Additional5,
  ///   InterfaceImportHeader.Additional6,
  ///   InterfaceImportHeader.Additional7,
  ///   InterfaceImportHeader.Additional8,
  ///   InterfaceImportHeader.Additional9,
  ///   InterfaceImportHeader.Additional10,
  ///   InterfaceImportHeader.ProcessedDate,
  ///   InterfaceImportHeader.InsertDate,
  ///   InterfaceImportHeader.ErrorMsg,
  ///   InterfaceImportHeader.HostStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportHeader_Insert
(
 @InterfaceImportHeaderId int = null output,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @InvoiceNumber nvarchar(60) = null,
 @RecordType nvarchar(60) = null,
 @RecordStatus char(1) = null,
 @CompanyCode nvarchar(60) = null,
 @Company nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @Route nvarchar(100) = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @ContainerNumber nvarchar(60) = null,
 @SealNumber nvarchar(60) = null,
 @DeliveryDate nvarchar(60) = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @VatPercentage decimal(13,3) = null,
 @VatSummary decimal(13,3) = null,
 @Total decimal(13,3) = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null,
 @ErrorMsg nvarchar(510) = null,
 @HostStatus nvarchar(20) = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceImportHeaderId = '-1'
    set @InterfaceImportHeaderId = null;
  
	 declare @Error int
 
  insert InterfaceImportHeader
        (PrimaryKey,
         OrderNumber,
         InvoiceNumber,
         RecordType,
         RecordStatus,
         CompanyCode,
         Company,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         Route,
         DeliveryNoteNumber,
         ContainerNumber,
         SealNumber,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         VatPercentage,
         VatSummary,
         Total,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         ProcessedDate,
         InsertDate,
         ErrorMsg,
         HostStatus)
  select @PrimaryKey,
         @OrderNumber,
         @InvoiceNumber,
         @RecordType,
         @RecordStatus,
         @CompanyCode,
         @Company,
         @Address,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @Route,
         @DeliveryNoteNumber,
         @ContainerNumber,
         @SealNumber,
         @DeliveryDate,
         @Remarks,
         @NumberOfLines,
         @VatPercentage,
         @VatSummary,
         @Total,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         @ProcessedDate,
         isnull(@InsertDate, getdate()),
         @ErrorMsg,
         @HostStatus 
  
  select @Error = @@Error, @InterfaceImportHeaderId = scope_identity()
  
  
  return @Error
  
end
