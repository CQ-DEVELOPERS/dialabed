﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Culture_Delete
  ///   Filename       : p_Culture_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:36
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Culture table.
  /// </remarks>
  /// <param>
  ///   @CultureId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Culture_Delete
(
 @CultureId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Culture
     where CultureId = @CultureId
  
  select @Error = @@Error
  
  
  return @Error
  
end
