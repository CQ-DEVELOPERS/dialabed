﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualDataOutbound_Insert
  ///   Filename       : p_VisualDataOutbound_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:36:09
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the VisualDataOutbound table.
  /// </remarks>
  /// <param>
  ///   @Location varchar(15) = null,
  ///   @OrderNumber varchar(30) = null,
  ///   @ExternalCompany varchar(255) = null,
  ///   @JobId int = null,
  ///   @Pallet varchar(30) = null,
  ///   @InstructionType varchar(50) = null,
  ///   @StatusCode varchar(10) = null,
  ///   @Scanned datetime = null,
  ///   @DespatchDate datetime = null,
  ///   @Minutes int = null 
  /// </param>
  /// <returns>
  ///   VisualDataOutbound.Location,
  ///   VisualDataOutbound.OrderNumber,
  ///   VisualDataOutbound.ExternalCompany,
  ///   VisualDataOutbound.JobId,
  ///   VisualDataOutbound.Pallet,
  ///   VisualDataOutbound.InstructionType,
  ///   VisualDataOutbound.StatusCode,
  ///   VisualDataOutbound.Scanned,
  ///   VisualDataOutbound.DespatchDate,
  ///   VisualDataOutbound.Minutes 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualDataOutbound_Insert
(
 @Location varchar(15) = null,
 @OrderNumber varchar(30) = null,
 @ExternalCompany varchar(255) = null,
 @JobId int = null,
 @Pallet varchar(30) = null,
 @InstructionType varchar(50) = null,
 @StatusCode varchar(10) = null,
 @Scanned datetime = null,
 @DespatchDate datetime = null,
 @Minutes int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert VisualDataOutbound
        (Location,
         OrderNumber,
         ExternalCompany,
         JobId,
         Pallet,
         InstructionType,
         StatusCode,
         Scanned,
         DespatchDate,
         Minutes)
  select @Location,
         @OrderNumber,
         @ExternalCompany,
         @JobId,
         @Pallet,
         @InstructionType,
         @StatusCode,
         @Scanned,
         @DespatchDate,
         @Minutes 
  
  select @Error = @@Error
  
  
  return @Error
  
end
