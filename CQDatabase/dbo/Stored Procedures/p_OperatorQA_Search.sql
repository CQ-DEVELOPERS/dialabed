﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorQA_Search
  ///   Filename       : p_OperatorQA_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:29
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorQA table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorQA.OperatorId,
  ///   OperatorQA.EndDate,
  ///   OperatorQA.JobId,
  ///   OperatorQA.QA,
  ///   OperatorQA.InstructionTypeId,
  ///   OperatorQA.ActivityStatus,
  ///   OperatorQA.PickingRate,
  ///   OperatorQA.Units,
  ///   OperatorQA.Weight,
  ///   OperatorQA.Instructions,
  ///   OperatorQA.Orders,
  ///   OperatorQA.OrderLines,
  ///   OperatorQA.Jobs,
  ///   OperatorQA.ActiveTime,
  ///   OperatorQA.DwellTime 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorQA_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OperatorQA.OperatorId
        ,OperatorQA.EndDate
        ,OperatorQA.JobId
        ,OperatorQA.QA
        ,OperatorQA.InstructionTypeId
        ,OperatorQA.ActivityStatus
        ,OperatorQA.PickingRate
        ,OperatorQA.Units
        ,OperatorQA.Weight
        ,OperatorQA.Instructions
        ,OperatorQA.Orders
        ,OperatorQA.OrderLines
        ,OperatorQA.Jobs
        ,OperatorQA.ActiveTime
        ,OperatorQA.DwellTime
    from OperatorQA
  
end
