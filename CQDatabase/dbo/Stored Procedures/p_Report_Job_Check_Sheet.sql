﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Job_Check_Sheet
  ///   Filename       : p_Report_Job_Check_Sheet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Job_Check_Sheet
(
 @JobId            nvarchar(30)
)
 
as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(255),
   RouteId            int,
   Route              nvarchar(50),
   JobId              int,
   InstructionId      int,
   Pallet             nvarchar(10),
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   EndDate            datetime,
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   OperatorId         int,
   Operator           nvarchar(50),
   DropSequence       int,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   PickArea           nvarchar(50),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   StoreArea          nvarchar(50),
   NettWeight		       float
  )
  
  declare @OutboundShipmentId int,
          @WarehouseId        int,
          @Indicator		        char(1)
  
  set @JobId = replace(@JobId, 'J:', '')
   
  if @JobId like 'R:%'
  	 select @JobId = convert(nvarchar(30), JobId)
  	   from Job (nolock)
  	  where ReferenceNumber = @JobId
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         OperatorId,
         OutboundDocumentId,
         PickLocationId,
         StoreLocationId)
  select ili.OutboundShipmentId,
         ili.IssueId,
         ili.IssueLineId,
         ins.JobId,
         ins.InstructionId,
         ins.StorageUnitBatchId,
         ins.EndDate,
         ili.Quantity,
         isnull(ili.ConfirmedQuantity,0),
         ili.Quantity - isnull(ili.ConfirmedQuantity,0),
         ins.OperatorId,
         ili.OutboundDocumentId,
         ins.PickLocationId,
         ins.StoreLocationId
    from IssueLineInstruction ili (nolock)
    join Instruction           ins (nolock) on ili.InstructionId    = ins.InstructionId
   where ins.JobId = @JobId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set RouteId = os.RouteId
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch       = b.Batch
    from @TableResult    tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set StoreLocation = l.Location,
         StoreArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId       = al.LocationId
    join Area          a (nolock) on al.AreaId          = a.AreaId
  
  update tr
     set PickLocation = l.Location,
         PickArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId       = al.LocationId
    join Area          a (nolock) on al.AreaId          = a.AreaId
  
  select @OutboundShipmentId = OutboundShipmentId
    from @TableResult
  
  update tr
     set Pallet = convert(nvarchar(10), j.DropSequence)
               + ' of '
               + convert(nvarchar(10), j.Pallets)
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
  
  select @WarehouseId = WarehouseId
    from Instruction (nolock)
   where JobId = @JobId
  
  if dbo.ufn_Configuration(71, @warehouseId) = 0
    update @TableResult
       set ExternalCompany = null
  
  if dbo.ufn_Configuration(219, @warehouseId) = 0
    update @TableResult
       set ConfirmedQuantity = null
     
  declare @InboundSequence smallint,
		 @PackTypeId	int,
		 @PackType   	nvarchar(30),
		 @ShowWeight	bit = null
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence

  
  select @ShowWeight = Indicator
  from Configuration where ConfigurationId = 206

  if  @ShowWeight = 1
	update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   where pk.PackTypeId = @PackTypeId
                       and p.ProductCode = tr.ProductCode
                       and isnull(su.ProductCategory,'') != 'V')* tr.ConfirmedQuantity)
    
    from @TableResult tr
  
  select i.OutboundShipmentId,
         null as 'Route',
         null as 'OrderNumber',
         null as 'DropSequence',
         i.JobId,
         null as 'Pallet',
         null as 'ExternalCompany',
         p.ProductCode,
         p.Product,
         max(i.EndDate) as 'EndDate',
         sku.SKUCode,
         b.Batch,
         null as 'PickArea',
         pick.Location as 'PickLocation',
         null as 'StoreArea',
         min(store.Location) as 'StoreLocation',
         sum(i.Quantity) as 'Quantity',
         sum(isnull(i.ConfirmedQuantity,0)) as 'ConfirmedQuantity',
         sum(i.Quantity - isnull(i.ConfirmedQuantity,0)) as 'ShortQuantity',
         null as 'QuantityOnHand',
         isnull(o.Operator,'Unknown') as 'Operator',
         null as 'NettWeight'
    from Instruction        i (nolock)
    join Status            si (nolock) on i.StatusId           = si.StatusId
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on j.StatusId           = s.StatusId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
    join Batch              b (nolock) on sub.BatchId          = b.BatchId
    join Location        pick (nolock) on i.PickLocationId     = pick.LocationId
    left
    join Location       store (nolock) on i.StoreLocationId    = store.LocationId
    left
    join Operator           o (nolock) on j.OperatorId         = o.OperatorId
   where j.JobId = @JobId
  group by i.OutboundShipmentId,
         i.JobId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         pick.Location,
         isnull(Operator,'Unknown')
--  select OutboundShipmentId,
--         Route,
--         null as 'OrderNumber',
--         isnull(DropSequence,1) as 'DropSequence',
--         JobId,
--         Pallet,
--         null as 'ExternalCompany',
--         ProductCode,
--         Product,
--         EndDate,
--         SKUCode,
--         Batch,
--         PickArea,
--         PickLocation,
--         isnull(StoreArea,'Undefined') as 'StoreArea',
--         isnull(StoreLocation,'Undefined') as 'StoreLocation',
--         sum(Quantity) as 'Quantity',
--         sum(ConfirmedQuantity) as 'ConfirmedQuantity',
--         sum(ShortQuantity) as 'ShortQuantity',
--         sum(QuantityOnHand) as 'QuantityOnHand',
--         isnull(Operator,'Unknown') as 'Operator'
--    from @TableResult
--   group by OutboundShipmentId,
--         Route,
--         isnull(DropSequence,1),
--         JobId,
--         Pallet,
--         ExternalCompany,
--         ProductCode,
--         Product,
--         EndDate,
--         SKUCode,
--         Batch,
--         PickArea,
--         PickLocation,
--         isnull(StoreArea,'Undefined'),
--         isnull(StoreLocation,'Undefined'),
--         isnull(Operator,'Unknown')
union
select OutboundShipmentId,
         Route,
         OrderNumber,
         isnull(DropSequence,1) as 'DropSequence',
         JobId,
         Pallet,
         ExternalCompany,
         'Blank',
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         MAX(StoreLocation),
         null,
         null,
         null,
         null,
         MAX(Operator),
         sum(NettWeight)
    from @TableResult
   group by OutboundShipmentId,
         Route,
         OrderNumber,
         isnull(DropSequence,1),
         JobId,
         Pallet,
         ExternalCompany
   order by OutboundShipmentId,
         JobId,
         Pallet desc,
         OrderNumber,
         SKUCode,
         ProductCode
  
--  select vi.OutboundShipmentId,
--         null as 'Route',
--         null as 'OrderNumber',
--         null as 'DropSequence',
--         vi.JobId,
--         null as 'Pallet',
--         null as 'ExternalCompany',
--         vi.ProductCode,
--         vi.Product,
--         vi.EndDate,
--         vi.SKUCode,
--         vi.Batch,
--         null as 'PickArea',
--         vi.PickLocation,
--         null as 'StoreArea',
--         vi.StoreLocation as 'StoreLocation',
--         'Quantity' = CASE when @Indicator = '3'
--					then 0
--					else vi.Quantity
--					end,
--		 'ConfirmedQuantity' = CASE when @Indicator = '2' or @Indicator = '3'
--					then 0
--					else vi.ConfirmedQuantity
--					end,
--		 'ShortQuantity' = CASE when @Indicator = '2' or @Indicator = '3'
--					then 0
--					else vi.Quantity - vi.ConfirmedQuantity
--					end,
----         vi.Quantity as 'Quantity',
----         vi.ConfirmedQuantity as 'ConfirmedQuantity',
----         vi.Quantity - vi.ConfirmedQuantity as 'ShortQuantity',
--         null as 'QuantityOnHand',
--         isnull(vi.Operator,'Unknown') as 'Operator',
--         tr.NettWeight as 'NettWeight'
--    from viewInstruction vi
--    join @TableResult tr on vi.OutboundShipmentId = tr.OutboundShipmentId
--   where vi.JobId = @JobId
--   and   vi.ProductCode = tr.ProductCode
--union
--select OutboundShipmentId,
--         Route,
--         OrderNumber,
--         isnull(DropSequence,1) as 'DropSequence',
--         JobId,
--         Pallet,
--         ExternalCompany,
--         'Blank',
--         null,
--         null,
--         null,
--         null,
--         null,
--         null,
--         null,
--         MAX(StoreLocation),
--         null,
--         null,
--         null,
--         null,
--         MAX(Operator),
--         sum(NettWeight)
--    from @TableResult
--   group by OutboundShipmentId,
--         Route,
--         OrderNumber,
--         isnull(DropSequence,1),
--         JobId,
--         Pallet,
--         ExternalCompany
--   order by OutboundShipmentId,
--         JobId,
--         Pallet desc,
--         OrderNumber
end
 
