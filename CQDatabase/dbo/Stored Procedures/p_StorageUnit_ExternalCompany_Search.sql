﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnit_ExternalCompany_Search
  ///   Filename       : p_StorageUnit_ExternalCompany_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnit_ExternalCompany_Search
	(@WarehouseId int,
	@StorageUnitId nvarchar(50) = null,
	@ExternalCompanyId nvarchar(50) = null)
 
as
begin
	 set nocount on;
	If (@StorageUnitId=-1) or (@StorageUnitId='{All}') set @StorageUnitId = null
	If (@ExternalCompanyId=-1) or (@ExternalCompanyId='{All}') set @ExternalCompanyId = null

Select e.StorageUnitId,
		e.ExternalCompanyId,
		c.ExternalCompany,
		p.ProductCode,
		p.Product,
		u.UOM,
		e.AllocationCategory,
		e.MinimumShelfLife
from StorageUnitExternalCompany e
		Join StorageUnit su on su.StorageUnitId = e.StorageUnitId
		Join Product p on p.ProductId = su.ProductId
		Join Sku sk on sk.SKUId = su.SKUId
		Join UOM u on u.UOMId = sk.UOMId
		Join ExternalCompany c on c.ExternalCompanyId = e.ExternalCompanyId

Where e.StorageUnitId = Isnull(@StorageUnitId,e.StorageUnitId) and e.ExternalCompanyId = Isnull(@ExternalCompanyId,e.ExternalCompanyId)

  
  
end
