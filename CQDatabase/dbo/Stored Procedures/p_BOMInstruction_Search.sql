﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstruction_Search
  ///   Filename       : p_BOMInstruction_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:07
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the BOMInstruction table.
  /// </remarks>
  /// <param>
  ///   @BOMInstructionId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   BOMInstruction.BOMInstructionId,
  ///   BOMInstruction.BOMHeaderId,
  ///   BOMInstruction.Quantity,
  ///   BOMInstruction.OrderNumber,
  ///   BOMInstruction.InboundDocumentId,
  ///   BOMInstruction.OutboundDocumentId,
  ///   BOMInstruction.ParentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMInstruction_Search
(
 @BOMInstructionId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @BOMInstructionId = '-1'
    set @BOMInstructionId = null;
  
 
  select
         BOMInstruction.BOMInstructionId
        ,BOMInstruction.BOMHeaderId
        ,BOMInstruction.Quantity
        ,BOMInstruction.OrderNumber
        ,BOMInstruction.InboundDocumentId
        ,BOMInstruction.OutboundDocumentId
        ,BOMInstruction.ParentId
    from BOMInstruction
   where isnull(BOMInstruction.BOMInstructionId,'0')  = isnull(@BOMInstructionId, isnull(BOMInstruction.BOMInstructionId,'0'))
  
end
