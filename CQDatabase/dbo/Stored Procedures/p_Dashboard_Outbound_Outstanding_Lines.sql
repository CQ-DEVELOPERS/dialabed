﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Outstanding_Lines
  ///   Filename       : p_Dashboard_Outbound_Outstanding_Lines.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Outstanding_Lines
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select WarehouseId,
           Legend,
           Value
      from DashboardOutboundOutstandingLines
     where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardOutboundOutstandingLines
	   
	   insert DashboardOutboundOutstandingLines
	         (WarehouseId,
           Legend,
           Value)
	   select i.WarehouseId,
	          'Units'       as 'Legend',
	          sum(il.Quantity) as 'Value'
	     from IssueLine  il (nolock)
	     join Issue       i (nolock) on il.IssueId = i.IssueId
	     join Status     s (nolock) on il.StatusId = s.StatusId
	    where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and s.StatusCode in ('RL')
	   group by i.WarehouseId,
	            s.Status
	   
	   insert DashboardOutboundOutstandingLines
	         (WarehouseId,
           Legend,
           Value)
	   select i.WarehouseId,
	          'Lines'       as 'Legend',
	          count(IssueLineId) as 'Value'
	     from IssueLine  il (nolock)
	     join Issue       i (nolock) on il.IssueId = i.IssueId
	     join Status     s (nolock) on i.StatusId = s.StatusId
	    where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and s.StatusCode in ('RL')
	   group by i.WarehouseId,
	            s.Status
	 end
end
