﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssue_Unlink
  ///   Filename       : p_OutboundShipmentIssue_Unlink.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssue_Unlink
(
 @OutboundShipmentId int,
 @IssueId            int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @NumberOfOrders    int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_OutboundShipmentIssue_Unlink'
  
  begin transaction
  
  if exists(select top 1 1
              from OutboundShipmentIssue osi (nolock)
              join IssueLine              il (nolock) on osi.IssueId    = il.IssueId
              join IssueLineInstruction  ili (nolock) on il.IssueLineId = ili.IssueLineId
              join Instruction             i (nolock) on ili.InstructionId = i.InstructionId
              join Job                     j (nolock) on j.JobId           = i.JobId
              join Status                  s (nolock) on j.StatusId        = s.StatusId
             where il.IssueId    = @IssueId
               and s.StatusCode not in ('P','NS'))
  begin
    set @Errormsg = 'Error executing p_OutboundShipmentIssue_Unlink'
    set @Error = -1
    goto error
  end
  
  delete ili
    from OutboundShipmentIssue osi
    join IssueLine              il on osi.IssueId    = il.IssueId
    join IssueLineInstruction  ili on il.IssueLineId = ili.IssueLineId
   where il.IssueId = @IssueId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_OutboundShipmentIssue_Delete
   @OutboundShipmentId = @OutboundShipmentId,
   @IssueId            = @IssueId
  
  if @Error <> 0
    goto error
  
  update i
     set OutboundShipmentId = osi.OutboundShipmentId
    from Instruction             i
    join IssueLine              il on i.IssueLineId = il.IssueLineId
    join OutboundShipmentIssue osi on il.IssueId = osi.IssueId
   where i.OutboundShipmentId is not null
     and il.IssueId                = @IssueId
  
  if @Error <> 0
    goto error
  
  select @NumberOfOrders = isnull(NumberOfOrders,1) - 1
    from OutboundShipment (nolock)
   where OutboundShipmentId = @OutboundShipmentId
  
  if @NumberOfOrders < 0
    set @NumberOfOrders = 0
  
  exec @Error = p_OutboundShipment_Update
   @OutboundShipmentId = @OutboundShipmentId,
   @NumberOfOrders     = @NumberOfOrders
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
