﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Interleaving
  ///   Filename       : p_Mobile_Interleaving.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Nov 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Interleaving
(
 @OperatorId int
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Mobile_Interleaving'
    rollback transaction
    return @Error
end
