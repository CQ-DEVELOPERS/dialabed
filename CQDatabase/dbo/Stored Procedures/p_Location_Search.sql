﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search
  ///   Filename       : p_Location_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2013 08:13:22
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Location table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null output,
  ///   @LocationTypeId int = null,
  ///   @Location nvarchar(30) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Location.LocationId,
  ///   Location.LocationTypeId,
  ///   LocationType.LocationType,
  ///   Location.Location,
  ///   Location.Ailse,
  ///   Location.[Column],
  ///   Location.[Level],
  ///   Location.PalletQuantity,
  ///   Location.SecurityCode,
  ///   Location.RelativeValue,
  ///   Location.NumberOfSUB,
  ///   Location.StocktakeInd,
  ///   Location.ActiveBinning,
  ///   Location.ActivePicking,
  ///   Location.Used,
  ///   Location.Latitude,
  ///   Location.Longitude,
  ///   Location.Direction,
  ///   Location.Height,
  ///   Location.Length,
  ///   Location.Width,
  ///   Location.Volume 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search
(
 @LocationId int = null output,
 @LocationTypeId int = null,
 @Location nvarchar(30) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @LocationTypeId = '-1'
    set @LocationTypeId = null;
  
  if @Location = '-1'
    set @Location = null;
  
 
  select
         Location.LocationId
        ,Location.LocationTypeId
         ,LocationTypeLocationTypeId.LocationType as 'LocationType'
        ,Location.Location
        ,Location.Ailse
        ,Location.[Column]
        ,Location.Level
        ,Location.PalletQuantity
        ,Location.SecurityCode
        ,Location.RelativeValue
        ,Location.NumberOfSUB
        ,Location.StocktakeInd
        ,Location.ActiveBinning
        ,Location.ActivePicking
        ,Location.Used
        ,Location.Latitude
        ,Location.Longitude
        ,Location.Direction
        ,Location.Height
        ,Location.Length
        ,Location.Width
        ,Location.Volume
    from Location
    left
    join LocationType LocationTypeLocationTypeId on LocationTypeLocationTypeId.LocationTypeId = Location.LocationTypeId
   where isnull(Location.LocationId,'0')  = isnull(@LocationId, isnull(Location.LocationId,'0'))
     and isnull(Location.LocationTypeId,'0')  = isnull(@LocationTypeId, isnull(Location.LocationTypeId,'0'))
     and isnull(Location.Location,'%')  like '%' + isnull(@Location, isnull(Location.Location,'%')) + '%'
  order by Location
  
end
