﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentType_Delete
  ///   Filename       : p_InterfaceDocumentType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 16:26:01
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceDocumentTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentType_Delete
(
 @InterfaceDocumentTypeId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceDocumentType
     where InterfaceDocumentTypeId = @InterfaceDocumentTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
