﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Ticketing_Exception
  ///   Filename       : p_Report_Ticketing_Exception.sql
  ///   Create By      : Karen
  ///   Date Created   : 22 Oct 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Ticketing_Exception
(
 @StorageUnitId		int = null
)
 
as
begin
	 set nocount on;
  
  if @StorageUnitId = -1
	set @StorageUnitId = null
  
  
  declare @TableResult as Table
  (
   ProductCode			nvarchar(30),
   Product				nvarchar(255),
   SKUCode				nvarchar(50),
   StorageUnitId		int,
   StorageUnitBatchId	int,
   LocationId			int,
   PreviousPrice		float,
   Price				float,
   SOH					float
  );
  
    insert @TableResult
          (ProductCode,
           Product,
           SKUCode,
           StorageUnitId,
           StorageUnitBatchId,
           LocationId,
           PreviousPrice,
           Price,
           SOH)
    select ProductCode,
           Product,
           SKUCode,
           su.StorageUnitId,
           sub.StorageUnitBatchId,
           subl.LocationId,
           su.PreviousUnitPrice,
           su.UnitPrice,
           subl.ActualQuantity
      from StorageUnit				  su (nolock)
      join Product					   p (nolock) on su.ProductId = p.ProductId
      join StorageUnitBatch		     sub (nolock) on su.StorageUnitId = sub.StorageUnitId
      left join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
      join SKU					      sk (nolock) on su.SKUId = sk.SKUId
      left join AreaLocation               al (nolock) on subl.LocationId            = al.LocationId
	  left join Area                        a (nolock) on al.AreaId               = a.AreaId
      where su.StorageUnitId = isnull(@StorageUnitId, su.StorageUnitId)
      --and a.StockOnHand      = 1   
  
  select *
    from @TableResult  tr
    
end
