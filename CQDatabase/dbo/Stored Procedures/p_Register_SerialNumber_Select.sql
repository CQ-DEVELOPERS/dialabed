﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber_Select
  ///   Filename       : p_Register_SerialNumber_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber_Select
(
 @ReceiptId     int = null,
 @ReceiptLineId int = null,
 @InstructionId int = null
)
 
as
begin
	 set nocount on;
	 
	 declare @AcceptedQuantity float,
	         @SerialNumberCount int,
	         @SerialNumber      nvarchar(50)
  
  if @InstructionId is null
  begin
    select top 1
           @SerialNumber      = SerialNumber
      from SerialNumber (nolock)
     where ReceiptLineId = @ReceiptLineId
    order by SerialNumberId desc
    
    select @SerialNumberCount = count(1)
      from SerialNumber (nolock)
     where ReceiptLineId = @ReceiptLineId
    
    select @AcceptedQuantity = AcceptedQuantity
      from ReceiptLine  (nolock)
     where ReceiptLineId = @ReceiptLineId
    
    if @SerialNumberCount is null
       set @SerialNumberCount = 0
    
    if @AcceptedQuantity is null
       set @AcceptedQuantity = 0
    
    select @SerialNumber as 'SerialNumber'
    union
    select convert(varchar(10), @SerialNumberCount) + ' of ' + CONVERT(varchar(10), @AcceptedQuantity)
  end
  else
    select SerialNumber
      from SerialNumber (nolock)
     where StoreInstructionId = @InstructionId
        or PickInstructionId  = @InstructionId
end
