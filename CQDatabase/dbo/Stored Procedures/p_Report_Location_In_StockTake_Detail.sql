﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Location_In_StockTake_Detail
  ///   Filename       : p_Report_Location_In_StockTake_Detail.sql
  ///   Create By      : Karen
  ///   Date Created   : October 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Location_In_StockTake_Detail
(
 @AreaId					Int
)
as
begin
	 set nocount on; 

Declare @AreaLocationCount as Table  
	(AreaId					int,
	 Area					nvarchar(50),
	 LocationId				int,
	 Location				nvarchar(15),
	 StockTakeInd			bit,
	 StockTake				nvarchar(10),
	 OperatorId				int,
	 Operator				nvarchar(100),
	 InstructionType		nvarchar(30),
	 StatusId				int,
	 Status					nvarchar(30)) 

	Insert into @AreaLocationCount 
			(AreaId,
			Area,
			LocationId,
			Location,
			StockTakeInd,
			StockTake)
	Select	al.AreaId,
			a.Area,
			al.LocationId,
			l.Location,
			l.StocktakeInd,
			'No'
	  from AreaLocation al (nolock)
	  Join Area a on a.AreaId = al.AreaId
	  join Location l on al.LocationId = l.LocationId
	 where al.AreaId = @AreaId

	update alc
	   set StockTake = 'Yes'
	  from @AreaLocationCount alc
	 where StockTakeInd = 1
	
	--select * from @AreaLocationCount
	
	update alc
	   set OperatorId = isnull(j.OperatorId,i.OperatorId),
		   InstructionType = it.InstructionType,
		   StatusId = i.StatusId,
		   Status = s.Status
	  from @AreaLocationCount alc
	  join instruction		i (nolock) on i.picklocationid = alc.locationid
	  join InstructionType it (nolock) on it.InstructionTypeId = i.InstructionTypeId
	  join Job				j (nolock) on i.JobId = j.JobId
	  join Status			s (nolock) on i.StatusId = s.StatusId
	 where i.StatusId not in (76,10)
	 and InstructionTypeCode in ('STE','STA','STL','STP')
	 and StockTake = 'Yes'
	 
	update alc
	   set Operator = o.Operator
	  from @AreaLocationCount alc
	  join Operator			o (nolock) on alc.OperatorId = o.OperatorId
		 
	select  *
	  from @AreaLocationCount 

end
