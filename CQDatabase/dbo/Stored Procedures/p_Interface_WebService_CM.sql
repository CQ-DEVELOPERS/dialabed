﻿create procedure [dbo].p_Interface_WebService_CM
(
 @doc2			varchar(max) output
)
--with encryption
as
begin

	declare @doc xml
	set @doc = convert(xml,@doc2)
	DECLARE @idoc int
	Declare @InsertDate datetime
	
	Set @InsertDate = Getdate()
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
		Insert Into 
		InterfaceCustomer(RecordStatus,
							InsertDate,
								HostId,
								CustomerCode,
								CustomerName)
		SELECT    'W',@InsertDate,*
		FROM       OPENXML (@idoc, '/root/Customer',1)
            WITH (HostId varchar(50) 'DCLink',
				  CustomerCode  varchar(50) 'Account',
                  CustomerName varchar(50) 'Name')
		
		Update InterfaceCustomer set RecordStatus = 'N'
		Where RecordSTatus = 'W' and InsertDate = @InsertDate
		EXEC p_Pastel_Import_Customer
		Set @doc2 = ''
End


