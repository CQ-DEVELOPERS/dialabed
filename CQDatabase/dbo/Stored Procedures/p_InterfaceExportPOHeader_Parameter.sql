﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportPOHeader_Parameter
  ///   Filename       : p_InterfaceExportPOHeader_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:09
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportPOHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceExportPOHeader.InterfaceExportPOHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportPOHeader_Parameter
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceExportPOHeaderId
        ,null as 'InterfaceExportPOHeader'
  union
  select
         InterfaceExportPOHeader.InterfaceExportPOHeaderId
        ,InterfaceExportPOHeader.InterfaceExportPOHeaderId as 'InterfaceExportPOHeader'
    from InterfaceExportPOHeader
  
end
