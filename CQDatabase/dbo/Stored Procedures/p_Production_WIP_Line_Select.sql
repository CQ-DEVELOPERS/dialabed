﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_WIP_Line_Select
  ///   Filename       : p_Production_WIP_Line_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_WIP_Line_Select
(
 @OutboundShipmentId int = null,
 @IssueId int = null
)
 
as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   IssueLineId                     int,
   IssueId                         int,
   OutboundDocumentId              int,
   OrderNumber                     nvarchar(30),
   StorageUnitBatchId              int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   BatchId                         int,
   Batch                           nvarchar(50),
   SKUCode                         nvarchar(50),
   RequiredQuantity                numeric(13,6) default 0,
   ConfirmedQuatity                numeric(13,6) default 0,
   AllocatedQuantity               numeric(13,6) default 0,
   ManufacturingQuantity           numeric(13,6) default 0,
   RawQuantity                     numeric(13,6) default 0,
   ReplenishmentQuantity            numeric(13,6) default 0,
   ReceivingQuantity            numeric(13,6) default 0,
   AvailableQuantity               numeric(13,6) default 0,
   AvailabilityIndicator           nvarchar(20),
   AvailablePercentage             numeric(13,2) default 0,
   AreaType                        nvarchar(10),
   WarehouseId                     int,
   DropSequence                    int,
   StatusCode                      nvarchar(10),
   Status                          nvarchar(50),
   StockOnOrder                    float
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null;
  
  if @IssueId = -1
    set @IssueId = null;
  
  if @IssueId is not null
  begin
    insert @TableResult
          (IssueLineId,
           IssueId,
           OutboundDocumentId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           RequiredQuantity,
           ConfirmedQuatity,
           AreaType,
           WarehouseId,
           DropSequence,
           StatusCode,
           Status)
    select il.IssueLineId,
           il.IssueId,
           od.OutboundDocumentId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           il.ConfirmedQuatity,
           isnull(i.AreaType,''),
           i.WarehouseId,
           i.DropSequence,
           s.StatusCode,
           s.Status
      from IssueLine         il
      join Issue              i (nolock) on il.IssueId            = i.IssueId
      join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument  od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status             s (nolock) on il.StatusId           = s.StatusId
     where il.IssueId = @IssueId
  end
  else if @OutboundShipmentId is not null
  begin
    insert @TableResult
          (IssueLineId,
           IssueId,
           OutboundDocumentId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           RequiredQuantity,
           ConfirmedQuatity,
           AreaType,
           WarehouseId,
           DropSequence,
           StatusCode,
           Status)
    select il.IssueLineId,
           il.IssueId,
           od.OutboundDocumentId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           il.ConfirmedQuatity,
           isnull(i.AreaType,''),
           i.WarehouseId,
           i.DropSequence,
           s.StatusCode,
           s.Status
      from IssueLine         il
      join Issue              i (nolock) on il.IssueId            = i.IssueId
      join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument  od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status             s (nolock) on il.StatusId           = s.StatusId
      join OutboundShipmentIssue osi (nolock) on il.IssueId       = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  
  --update tr
  --   set AllocatedQuantity = isnull((select sum(Quantity)
  --                                     from Issue              i (nolock)
  --                                     join IssueLine         il (nolock) on i.IssueId = il.IssueId
  --                                     join Status             s (nolock) on i.StatusId = s.StatusId
  --                                     join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
  --                                    where s.StatusCode         in ('W','P','SA','M','RL','PC','PS','CK','A','WC','QA')
  --                                      and sub.StorageUnitId     = tr.StorageUnitId
  --                                      and isnull(i.AreaType,'') = isnull(tr.AreaType,'')
  --                                      and i.WarehouseId         = tr.WarehouseId
  --                                      and i.DropSequence        < tr.DropSequence),0)
  --  from @TableResult tr
  
  --update @TableResult
  --   set AllocatedQuantity = 0
  -- where AllocatedQuantity is null
  
  --update tr
  --   set AvailableQuantity = (select sum(subl.ActualQuantity)
  --                              from StorageUnitBatchLocation subl (nolock)
  --                              join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  --                              join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
  --                              join Area                        a (nolock) on al.AreaId               = a.AreaId
  --                             where a.StockOnHand     = 1
  --                               and sub.StorageUnitId = tr.StorageUnitId
  --                               and a.AreaType        = tr.AreaType
  --                               and a.WarehouseId     = tr.WarehouseId) -
  --                           isnull((select sum(Quantity)
  --                                     from Issue              i (nolock)
  --                                     join IssueLine         il (nolock) on i.IssueId = il.IssueId
  --                                     join Status             s (nolock) on i.StatusId = s.StatusId
  --                                     join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
  --                                    where s.StatusCode         in ('W','P','SA','M','RL','PC','PS','CK','A','WC','QA')
  --                                      and sub.StorageUnitId     = tr.StorageUnitId
  --                                      and isnull(i.AreaType,'') = isnull(tr.AreaType,'')
  --                                      and i.WarehouseId         = tr.WarehouseId
  --                                      and i.DropSequence        < tr.DropSequence),0)
  --  from @TableResult tr
  
  --update tr
  --   set RAWQuantity = (select sum(subl.ActualQuantity - subl.ReservedQuantity)
  --                                  from StorageUnitBatchLocation subl (nolock)
  --                                  join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  --                                  join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
  --                                  join Area                        a (nolock) on al.AreaId               = a.AreaId
  --                                 where a.StockOnHand     = 1
  --                                   and sub.StorageUnitId = tr.StorageUnitId
  --                                   and a.AreaType        = 'backup'
  --                                   and a.WarehouseId     = tr.WarehouseId)
  --  from @TableResult tr
  
  update tr
     set AllocatedQuantity = a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ManufacturingQuantity = a.ActualQuantity - a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set RAWQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ReceivingQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailableArea a on tr.WarehouseId   = a.WarehouseId
                            and a.AreaType       = 'backup'
                            and tr.StorageUnitId = a.StorageUnitId
                            and a.AreaCode = 'R'
  
  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  --update tr
  --   set AllocatedQuantity = case when (AllocatedQuantity - RequiredQuantity) >= 0
  --                                then AllocatedQuantity - RequiredQuantity
  --                                else 0
  --                                end
  --  from @TableResult tr
  -- where StatusCode in ('RL','CK')
  
  --update tr
  --   set ManufacturingQuantity = ManufacturingQuantity + RequiredQuantity
  --  from @TableResult tr
  -- where StatusCode in ('RL','CK')
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  declare @TableIssue as table
  (
   StorageUnitId                   int,
   RequiredQuantity                numeric(13,6) default 0
  )
  
  declare @DropSequence int
  
  select @DropSequence = DropSequence
    from Issue
   where IssueId = @IssueId
  
  insert @TableIssue
        (StorageUnitId,
         RequiredQuantity)
  select sub.StorageUnitId,
         sum(il.Quantity)
    from OutboundDocument       od (nolock)
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                          and odt.OutboundDocumentTypeCode in ('MO','KIT')
    join Issue                   i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join IssueLine              il (nolock) on i.IssueId            = il.IssueId
    join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
    join Status                  s (nolock) on il.StatusId           = s.StatusId
   where s.Type                 = 'IS'
     and s.StatusCode             in ('W')
     and i.DropSequence < @DropSequence
  group by sub.StorageUnitId
  
  update tr
     set AllocatedQuantity = tr.AllocatedQuantity + ti.RequiredQuantity
    from @TableResult tr
    join @TableIssue  ti on tr.StorageUnitId = ti.StorageUnitId
   where StatusCode = 'W'
  
  update tr
     set AvailableQuantity = (ManufacturingQuantity + RAWQuantity) - AllocatedQuantity
    from @TableResult tr
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), ManufacturingQuantity - AllocatedQuantity) / convert(numeric(13,3), RequiredQuantity)) * 100
    where convert(numeric(13,3), RequiredQuantity) > 0
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where AvailableQuantity >= RequiredQuantity
     --and RequiredQuantity   > ManufacturingQuantity
     and RequiredQuantity   > ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity  <= ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where AvailablePercentage >= 100
  
  insert @TableResult
        (IssueLineId,
         IssueId,
         OrderNumber,
         StorageUnitId,
         BatchId,
         RequiredQuantity,
         AvailabilityIndicator)
  select distinct -1,
         @IssueId,
         od.OrderNumber,
         od.StorageUnitId,
         od.BatchId,
         od.Quantity,
         'Standard'
    from @TableResult tr
    join OutboundDocument od on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         Batch       = b.Batch,
         SKUCode     = sku.SKUCode
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    left
    join Batch            b   (nolock) on tr.BatchId            = b.BatchId
  
  update tr
     set StockOnOrder = (select sum(RequiredQuantity)
                           from ReceiptLine rl (nolock)
                          where rl.StorageUnitBatchId = sub.StorageUnitBatchId
                            and rl.StatusId = dbo.ufn_StatusId('R','W'))
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitId = sub.StorageUnitId
  
  select IssueLineId,
         ProductCode,
         Product,
         Batch,
         SKUCode,
         RequiredQuantity,
         AllocatedQuantity,
         ManufacturingQuantity,
         RawQuantity,
         ReceivingQuantity,
         ReplenishmentQuantity,
         AvailableQuantity,
         AvailabilityIndicator,
         AvailablePercentage,
         StockOnOrder
    from @TableResult
  order by ProductCode,
           Product,
           Batch,
           SKUCode
end
