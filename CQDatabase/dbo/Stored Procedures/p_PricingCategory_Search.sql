﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PricingCategory_Search
  ///   Filename       : p_PricingCategory_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:51:24
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the PricingCategory table.
  /// </remarks>
  /// <param>
  ///   @PricingCategoryId int = null output,
  ///   @PricingCategory nvarchar(510) = null,
  ///   @PricingCategoryCode nvarchar(60) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   PricingCategory.PricingCategoryId,
  ///   PricingCategory.PricingCategory,
  ///   PricingCategory.PricingCategoryCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PricingCategory_Search
(
 @PricingCategoryId int = null output,
 @PricingCategory nvarchar(510) = null,
 @PricingCategoryCode nvarchar(60) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @PricingCategoryId = '-1'
    set @PricingCategoryId = null;
  
  if @PricingCategory = '-1'
    set @PricingCategory = null;
  
  if @PricingCategoryCode = '-1'
    set @PricingCategoryCode = null;
  
 
  select
         PricingCategory.PricingCategoryId
        ,PricingCategory.PricingCategory
        ,PricingCategory.PricingCategoryCode
    from PricingCategory
   where isnull(PricingCategory.PricingCategoryId,'0')  = isnull(@PricingCategoryId, isnull(PricingCategory.PricingCategoryId,'0'))
     and isnull(PricingCategory.PricingCategory,'%')  like '%' + isnull(@PricingCategory, isnull(PricingCategory.PricingCategory,'%')) + '%'
     and isnull(PricingCategory.PricingCategoryCode,'%')  like '%' + isnull(@PricingCategoryCode, isnull(PricingCategory.PricingCategoryCode,'%')) + '%'
  order by PricingCategory
  
end
 
