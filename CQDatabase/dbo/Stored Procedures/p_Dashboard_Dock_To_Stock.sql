﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Dock_To_Stock
  ///   Filename       : p_Dashboard_Dock_To_Stock.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Dock_To_Stock
(
 @WarehouseId int = null,
 @Summarise   bit = 0,
 @Production  bit = null
)
 
as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select InboundDocumentType,
           Lines,
           Units,
           Minutes,
           KPI
      from DashboardDockToStock (nolock)
     where WarehouseId = @WarehouseId
  end
  else
  begin
    truncate table DashboardDockToStock
    
    insert DashboardDockToStock
          (WarehouseId,
           InboundDocumentType,
           Lines,
           Units,
           Minutes,
           KPI)
	   select i.WarehouseId,
	          idt.InboundDocumentType,
           count(distinct(rl.ReceiptLineId)) as 'Lines',
           sum(i.Quantity) as 'Units',
           sum(datediff(mi, j.CheckedDate, i.EndDate)) / count(i.InstructionId) as 'Minutes',
           18 as 'KPI'
      from Instruction           i (nolock)
      join AreaLocation         al (nolock) on i.PickLocationId         = al.LocationId
      join Area                  a (nolock) on al.AreaId                = a.AreaId
      join InstructionType      it (nolock) on i.InstructionTypeId      = it.InstructionTypeId
      join Job                   j (nolock) on i.JobId                  = j.JobId
      join ReceiptLine          rl (nolock) on i.ReceiptLineId          = rl.ReceiptLineId
      join Receipt               r (nolock) on rl.ReceiptId             = r.ReceiptId
      join InboundDocument      id (nolock) on r.InboundDocumentId      = id.InboundDocumentId
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
       and (it.InstructionTypeCode in ('PR','S','SM')
       or   (it.InstructionTypeCode in ('M')) and a.AreaCode = 'R')
       and isnull(j.CheckedDate, i.StartDate) > '2012-11-01'-- convert(varchar(10), getdate(), 120)
       and j.CheckedDate < i.EndDate
    group by i.WarehouseId,
             idt.InboundDocumentType
  end
end
