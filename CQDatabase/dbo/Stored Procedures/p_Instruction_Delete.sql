﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Delete
  ///   Filename       : p_Instruction_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Aug 2013 13:13:50
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Instruction table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Delete
(
 @InstructionId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Instruction
     where InstructionId = @InstructionId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_InstructionHistory_Insert
         @InstructionId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
