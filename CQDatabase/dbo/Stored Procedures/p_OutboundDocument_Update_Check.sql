﻿
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Update_Check
  ///   Filename       : p_OutboundDocument_Update_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007 15:29:06
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null,
  ///   @OutboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(30) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Update_Check
(
 @OutboundDocumentId int = null,
 @OutboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(30) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @IssueId           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @ExternalCompanyId = 0
    set @ExternalCompanyId = null
  
  select @IssueId = IssueId
    from Issue
   where OutboundDocumentId = @OutboundDocumentId
  
  begin transaction
  
  exec @Error = p_OutboundDocument_Update
   @OutboundDocumentId     = @OutboundDocumentId,
   @OutboundDocumentTypeId = @OutboundDocumentTypeId,
   @ExternalCompanyId     = @ExternalCompanyId,
   @StatusId              = @StatusId,
   @WarehouseId           = @WarehouseId,
   @OrderNumber           = @OrderNumber,
   @DeliveryDate          = @DeliveryDate
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Issue_Update
   @IssueId      = @IssueId,
   @DeliveryDate = @DeliveryDate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_OutboundDocument_Update_Check'
    rollback transaction
    return @Error
end
