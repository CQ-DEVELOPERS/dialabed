﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingFile_Insert
  ///   Filename       : p_InterfaceMappingFile_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 09:03:31
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceMappingFile table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingFileId int = null output,
  ///   @PrincipalId int = null,
  ///   @InterfaceFileTypeId int = null,
  ///   @InterfaceDocumentTypeId int = null,
  ///   @Delimiter char(2) = null,
  ///   @StyleSheet xml = null,
  ///   @PrincipalFTPSite nvarchar(max) = null,
  ///   @FTPUserName nvarchar(100) = null,
  ///   @FTPPassword nvarchar(100) = null,
  ///   @HeaderRow int = null,
  ///   @SubDirectory nvarchar(max) = null,
  ///   @FilePrefix nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   InterfaceMappingFile.InterfaceMappingFileId,
  ///   InterfaceMappingFile.PrincipalId,
  ///   InterfaceMappingFile.InterfaceFileTypeId,
  ///   InterfaceMappingFile.InterfaceDocumentTypeId,
  ///   InterfaceMappingFile.Delimiter,
  ///   InterfaceMappingFile.StyleSheet,
  ///   InterfaceMappingFile.PrincipalFTPSite,
  ///   InterfaceMappingFile.FTPUserName,
  ///   InterfaceMappingFile.FTPPassword,
  ///   InterfaceMappingFile.HeaderRow,
  ///   InterfaceMappingFile.SubDirectory,
  ///   InterfaceMappingFile.FilePrefix 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingFile_Insert
(
 @InterfaceMappingFileId int = null output,
 @PrincipalId int = null,
 @InterfaceFileTypeId int = null,
 @InterfaceDocumentTypeId int = null,
 @Delimiter char(2) = null,
 @StyleSheet xml = null,
 @PrincipalFTPSite nvarchar(max) = null,
 @FTPUserName nvarchar(100) = null,
 @FTPPassword nvarchar(100) = null,
 @HeaderRow int = null,
 @SubDirectory nvarchar(max) = null,
 @FilePrefix nvarchar(60) = null 
)
 
as
begin
	 set nocount on;
  
  if @InterfaceMappingFileId = '-1'
    set @InterfaceMappingFileId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @InterfaceFileTypeId = '-1'
    set @InterfaceFileTypeId = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
	 declare @Error int
 
  insert InterfaceMappingFile
        (PrincipalId,
         InterfaceFileTypeId,
         InterfaceDocumentTypeId,
         Delimiter,
         StyleSheet,
         PrincipalFTPSite,
         FTPUserName,
         FTPPassword,
         HeaderRow,
         SubDirectory,
         FilePrefix)
  select @PrincipalId,
         @InterfaceFileTypeId,
         @InterfaceDocumentTypeId,
         @Delimiter,
         @StyleSheet,
         @PrincipalFTPSite,
         @FTPUserName,
         @FTPPassword,
         @HeaderRow,
         @SubDirectory,
         @FilePrefix 
  
  select @Error = @@Error, @InterfaceMappingFileId = scope_identity()
  
  
  return @Error
  
end
