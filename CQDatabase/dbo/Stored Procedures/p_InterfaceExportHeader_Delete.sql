﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportHeader_Delete
  ///   Filename       : p_InterfaceExportHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:02
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceExportHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportHeader_Delete
(
 @InterfaceExportHeaderId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceExportHeader
     where InterfaceExportHeaderId = @InterfaceExportHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
