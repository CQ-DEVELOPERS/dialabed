﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLocation_Search
  ///   Filename       : p_InterfaceLocation_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:24
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceLocation table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceLocation.HostId,
  ///   InterfaceLocation.Location,
  ///   InterfaceLocation.LocationDescription,
  ///   InterfaceLocation.Address1,
  ///   InterfaceLocation.Address2,
  ///   InterfaceLocation.Address3,
  ///   InterfaceLocation.Address4,
  ///   InterfaceLocation.City,
  ///   InterfaceLocation.State,
  ///   InterfaceLocation.Zip,
  ///   InterfaceLocation.ProcessedDate,
  ///   InterfaceLocation.RecordStatus,
  ///   InterfaceLocation.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLocation_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceLocation.HostId
        ,InterfaceLocation.Location
        ,InterfaceLocation.LocationDescription
        ,InterfaceLocation.Address1
        ,InterfaceLocation.Address2
        ,InterfaceLocation.Address3
        ,InterfaceLocation.Address4
        ,InterfaceLocation.City
        ,InterfaceLocation.State
        ,InterfaceLocation.Zip
        ,InterfaceLocation.ProcessedDate
        ,InterfaceLocation.RecordStatus
        ,InterfaceLocation.InsertDate
    from InterfaceLocation
  
end
