﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questionaire_Update
  ///   Filename       : p_Questionaire_Update.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Questionaire_Update
(
	@WarehouseId		int,	
	@QuestionaireId		int,
	@QuestionaireType				nvarchar(50),
	@QuestionaireDesc				nvarchar(255)
)

as
begin
	 set nocount on;
	Update QuestionAire Set QuestionaireType = Isnull(@QuestionaireType,@QuestionaireType),
						QuestionaireDesc = Isnull(@QuestionaireDesc,QuestionaireDesc)
	Where WarehouseId = @WarehouseId and QuestionaireId = @QuestionaireId

end
