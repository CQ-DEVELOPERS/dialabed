﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorPerformance_Search
  ///   Filename       : p_OperatorPerformance_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:27
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorPerformance table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorPerformance.OperatorId,
  ///   OperatorPerformance.InstructionTypeId,
  ///   OperatorPerformance.EndDate,
  ///   OperatorPerformance.ActivityStatus,
  ///   OperatorPerformance.PickingRate,
  ///   OperatorPerformance.Units,
  ///   OperatorPerformance.Weight,
  ///   OperatorPerformance.Instructions,
  ///   OperatorPerformance.Orders,
  ///   OperatorPerformance.OrderLines,
  ///   OperatorPerformance.Jobs,
  ///   OperatorPerformance.ActiveTime,
  ///   OperatorPerformance.DwellTime,
  ///   OperatorPerformance.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorPerformance_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OperatorPerformance.OperatorId
        ,OperatorPerformance.InstructionTypeId
        ,OperatorPerformance.EndDate
        ,OperatorPerformance.ActivityStatus
        ,OperatorPerformance.PickingRate
        ,OperatorPerformance.Units
        ,OperatorPerformance.Weight
        ,OperatorPerformance.Instructions
        ,OperatorPerformance.Orders
        ,OperatorPerformance.OrderLines
        ,OperatorPerformance.Jobs
        ,OperatorPerformance.ActiveTime
        ,OperatorPerformance.DwellTime
        ,OperatorPerformance.WarehouseId
    from OperatorPerformance
  
end
