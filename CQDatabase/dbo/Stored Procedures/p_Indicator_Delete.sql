﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Indicator_Delete
  ///   Filename       : p_Indicator_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:25
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Indicator table.
  /// </remarks>
  /// <param>
  ///   @IndicatorId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Indicator_Delete
(
 @IndicatorId int = null 
)
 
as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Indicator
     where IndicatorId = @IndicatorId
  
  select @Error = @@Error
  
  
  return @Error
  
end
