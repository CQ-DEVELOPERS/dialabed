﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_ContainerType_Update
  ///   Filename       : p_Mobile_Product_Check_ContainerType_Update.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 01 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_ContainerType_Update
    @JobId int
   ,@ContainerTypeId int
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update Job
     set ContainerTypeId = @ContainerTypeId
   where JobId = @JobId  
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Mobile_Product_Check_ContainerType_Update'
    rollback transaction
    return @Error
end
 
