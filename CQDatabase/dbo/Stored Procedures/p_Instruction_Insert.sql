﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Insert
  ///   Filename       : p_Instruction_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Aug 2013 13:13:49
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Instruction table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null output,
  ///   @ReasonId int = null,
  ///   @InstructionTypeId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @WarehouseId int = null,
  ///   @StatusId int = null,
  ///   @JobId int = null,
  ///   @OperatorId int = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @ReceiptLineId int = null,
  ///   @IssueLineId int = null,
  ///   @InstructionRefId int = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null,
  ///   @PalletId int = null,
  ///   @CreateDate datetime = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null,
  ///   @Picked bit = null,
  ///   @Stored bit = null,
  ///   @CheckQuantity float = null,
  ///   @CheckWeight float = null,
  ///   @OutboundShipmentId int = null,
  ///   @DropSequence int = null,
  ///   @PackagingWeight float = null,
  ///   @PackagingQuantity float = null,
  ///   @NettWeight float = null,
  ///   @PreviousQuantity float = null,
  ///   @AuthorisedBy int = null,
  ///   @AuthorisedStatus nvarchar(20) = null,
  ///   @SOH float = null,
  ///   @AutoComplete bit = null 
  /// </param>
  /// <returns>
  ///   Instruction.InstructionId,
  ///   Instruction.ReasonId,
  ///   Instruction.InstructionTypeId,
  ///   Instruction.StorageUnitBatchId,
  ///   Instruction.WarehouseId,
  ///   Instruction.StatusId,
  ///   Instruction.JobId,
  ///   Instruction.OperatorId,
  ///   Instruction.PickLocationId,
  ///   Instruction.StoreLocationId,
  ///   Instruction.ReceiptLineId,
  ///   Instruction.IssueLineId,
  ///   Instruction.InstructionRefId,
  ///   Instruction.Quantity,
  ///   Instruction.ConfirmedQuantity,
  ///   Instruction.Weight,
  ///   Instruction.ConfirmedWeight,
  ///   Instruction.PalletId,
  ///   Instruction.CreateDate,
  ///   Instruction.StartDate,
  ///   Instruction.EndDate,
  ///   Instruction.Picked,
  ///   Instruction.Stored,
  ///   Instruction.CheckQuantity,
  ///   Instruction.CheckWeight,
  ///   Instruction.OutboundShipmentId,
  ///   Instruction.DropSequence,
  ///   Instruction.PackagingWeight,
  ///   Instruction.PackagingQuantity,
  ///   Instruction.NettWeight,
  ///   Instruction.PreviousQuantity,
  ///   Instruction.AuthorisedBy,
  ///   Instruction.AuthorisedStatus,
  ///   Instruction.SOH,
  ///   Instruction.AutoComplete 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Insert
(
 @InstructionId int = null output,
 @ReasonId int = null,
 @InstructionTypeId int = null,
 @StorageUnitBatchId int = null,
 @WarehouseId int = null,
 @StatusId int = null,
 @JobId int = null,
 @OperatorId int = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @ReceiptLineId int = null,
 @IssueLineId int = null,
 @InstructionRefId int = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null,
 @Weight float = null,
 @ConfirmedWeight float = null,
 @PalletId int = null,
 @CreateDate datetime = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @Picked bit = null,
 @Stored bit = null,
 @CheckQuantity float = null,
 @CheckWeight float = null,
 @OutboundShipmentId int = null,
 @DropSequence int = null,
 @PackagingWeight float = null,
 @PackagingQuantity float = null,
 @NettWeight float = null,
 @PreviousQuantity float = null,
 @AuthorisedBy int = null,
 @AuthorisedStatus nvarchar(20) = null,
 @SOH float = null,
 @AutoComplete bit = null 
)
 
as
begin
	 set nocount on;
  
  if @InstructionId = '-1'
    set @InstructionId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @JobId = '-1'
    set @JobId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @PalletId = '-1'
    set @PalletId = null;
  
  if @AuthorisedBy = '-1'
    set @AuthorisedBy = null;
  
	 declare @Error int
 
  insert Instruction
        (ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         CheckQuantity,
         CheckWeight,
         OutboundShipmentId,
         DropSequence,
         PackagingWeight,
         PackagingQuantity,
         NettWeight,
         PreviousQuantity,
         AuthorisedBy,
         AuthorisedStatus,
         SOH,
         AutoComplete)
  select @ReasonId,
         @InstructionTypeId,
         @StorageUnitBatchId,
         @WarehouseId,
         @StatusId,
         @JobId,
         @OperatorId,
         @PickLocationId,
         @StoreLocationId,
         @ReceiptLineId,
         @IssueLineId,
         @InstructionRefId,
         @Quantity,
         @ConfirmedQuantity,
         @Weight,
         @ConfirmedWeight,
         @PalletId,
         @CreateDate,
         @StartDate,
         @EndDate,
         @Picked,
         @Stored,
         @CheckQuantity,
         @CheckWeight,
         @OutboundShipmentId,
         @DropSequence,
         @PackagingWeight,
         @PackagingQuantity,
         @NettWeight,
         @PreviousQuantity,
         @AuthorisedBy,
         @AuthorisedStatus,
         @SOH,
         @AutoComplete 
  
  select @Error = @@Error, @InstructionId = scope_identity()
  
  if @Error = 0
    exec @Error = p_InstructionHistory_Insert
         @InstructionId = @InstructionId,
         @ReasonId = @ReasonId,
         @InstructionTypeId = @InstructionTypeId,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @WarehouseId = @WarehouseId,
         @StatusId = @StatusId,
         @JobId = @JobId,
         @OperatorId = @OperatorId,
         @PickLocationId = @PickLocationId,
         @StoreLocationId = @StoreLocationId,
         @ReceiptLineId = @ReceiptLineId,
         @IssueLineId = @IssueLineId,
         @InstructionRefId = @InstructionRefId,
         @Quantity = @Quantity,
         @ConfirmedQuantity = @ConfirmedQuantity,
         @Weight = @Weight,
         @ConfirmedWeight = @ConfirmedWeight,
         @PalletId = @PalletId,
         @CreateDate = @CreateDate,
         @StartDate = @StartDate,
         @EndDate = @EndDate,
         @Picked = @Picked,
         @Stored = @Stored,
         @CheckQuantity = @CheckQuantity,
         @CheckWeight = @CheckWeight,
         @OutboundShipmentId = @OutboundShipmentId,
         @DropSequence = @DropSequence,
         @PackagingWeight = @PackagingWeight,
         @PackagingQuantity = @PackagingQuantity,
         @NettWeight = @NettWeight,
         @PreviousQuantity = @PreviousQuantity,
         @AuthorisedBy = @AuthorisedBy,
         @AuthorisedStatus = @AuthorisedStatus,
         @SOH = @SOH,
         @AutoComplete = @AutoComplete,
         @CommandType = 'Insert'
  
  return @Error
  
end
