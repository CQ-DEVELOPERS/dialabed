﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Unlinked_Search
  ///   Filename       : p_Wave_Planning_Unlinked_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Unlinked_Search
(
 @WarehouseId            int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime,
 @PrincipalId            int = null,
 @ShowOrders             nvarchar(10)
)
 
as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   WarehouseId           int,
   OutboundShipmentId    int,
   IssueId               int,
   OutboundDocumentId    int,
   OrderNumber           nvarchar(30),
   ReferenceNumber       nvarchar(30),
   ExternalCompanyCode   nvarchar(30),
   ExternalCompany       nvarchar(255),
   RouteId               int,
   Route                 nvarchar(50),
   NumberOfLines         int,
   DeliveryDate          datetime,
   StatusId              int,
   Status                nvarchar(50),
   OutboundDocumentType  nvarchar(50),
   LocationId            int,
   Location              nvarchar(15),
   CreateDate            datetime,
   Rating                int,
   AvailabilityIndicator nvarchar(20),
   OrderVolume           float,
   OrderWeight           float
  );
  
  declare @TableDetail as table
  (
   WarehouseId        int,
   OutboundDocumentId int,
   OrderVolume        numeric(18,3),
   OrderWeight        numeric(18,3),
   NumberOfLines      int
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
	 
	 if @ExternalCompanyCode is null
	   set @ExternalCompanyCode = ''
	 
	 if @ExternalCompany is null
	   set @ExternalCompany = ''
	 
	 if @PrincipalId = -1
    set @PrincipalId = null
  
  insert @TableHeader
        (WarehouseId,
         OutboundShipmentId,
         IssueId,
         od.OutboundDocumentId,
         OrderNumber,
         ReferenceNumber,
         ExternalCompanyCode,
         ExternalCompany,
         RouteId,
         DeliveryDate,
         StatusId,
         Status,
         OutboundDocumentType,
         LocationId,
         CreateDate,
         Rating,
         NumberOfLines,
         OrderVolume,
         OrderWeight)
  select i.WarehouseId,
         osi.OutboundShipmentId,
         i.IssueId,
         od.OutboundDocumentId,
         od.OrderNumber,
         os.ReferenceNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         i.RouteId,
         i.DeliveryDate,
         i.StatusId,
         s.Status,
         idt.OutboundDocumentType,
         i.LocationId,
         od.CreateDate,
         ec.Rating,
         i.NumberOfLines,
         i.Weight,
         null
    from OutboundDocument     od  (nolock)
    join OutboundDocumentType idt (nolock) on od.OutboundDocumentTypeId = idt.OutboundDocumentTypeId
    join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
    join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join Status               s   (nolock) on i.StatusId               = s.StatusId
    left
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
    left
    join OutboundShipment       os (nolock) on osi.OutboundShipmentId = os.OutboundShipmentId
   where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
     and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and od.OrderNumber           like isnull(@OrderNumber  + '%', od.OrderNumber)
     and i.DeliveryDate        between @FromDate and @ToDate
     and od.WarehouseId              = @WarehouseId
     and s.StatusCode               in ('W','P')
     --and osi.OutboundShipmentId     is null
     and idt.OutboundDocumentTypeCode != 'PRQ'
     and i.WaveId is null
     and isnull(od.PrincipalId, -1) = isnull(@PrincipalId, isnull(od.PrincipalId, -1))
  
  update tr
     set Location = l.Location
    from @TableHeader tr
    join Location     l  (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Route = r.Route
    from @TableHeader  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
--  insert @TableDetail
--        (OutboundDocumentId,
--         OrderVolume,
--         OrderWeight,
--         NumberOfLines)
--  select th.OutboundDocumentId,
--         round(sum(il.Quantity) * sum(p.Quantity),0),
--         round(sum(il.Quantity) * sum(p.Weight),0),
--         count(distinct(ol.OutboundLineId))
--    from @TableHeader th
--    join OutboundLine ol (nolock) on th.OutboundDocumentId = ol.OutboundDocumentId
--    join IssueLine    il (nolock) on ol.OutboundLineId     = il.OutboundLineId
--    join Pack          p (nolock) on ol.StorageUnitId      = p.StorageUnitId
--   where p.PackTypeId = 2 --p.Quantity = 1 -- Performance Issue
--     and p.WarehouseId = @WarehouseId
--  group by th.OutboundDocumentId
  
  --declare @TableTemp as table
  --(
  -- WarehouseId        int,
  -- OutboundDocumentId int,
  -- StorageUnitId      int,
  -- Quantity           float,
  -- Weight             numeric(13,3),
  -- Volume             numeric(13,3)
  --);
  
  --insert @TableTemp
  --      (WarehouseId,
  --       OutboundDocumentId,
  --       StorageUnitId,
  --       Quantity)
  --select th.WarehouseId,
  --       th.OutboundDocumentId,
  --       ol.StorageUnitId,
  --       sum(il.Quantity)
  --  from @TableHeader th
  --  join OutboundLine ol (nolock) on th.OutboundDocumentId = ol.OutboundDocumentId
  --  join IssueLine    il (nolock) on ol.OutboundLineId     = il.OutboundLineId
  --group by th.WarehouseId,
  --         th.OutboundDocumentId,
  --         ol.StorageUnitId
  
  --update tt
  --   set Weight = p.Weight,
  --       Volume = isnull(p.Volume, p.Quantity)
  --  from @TableTemp tt
  --  join Pack        p (nolock) on tt.StorageUnitId = p.StorageUnitId
  --                             and tt.Warehouseid   = p.WarehouseId
  --  join PackType   pt (nolock) on p.PackTypeId     = pt.PackTypeId
  -- where pt.InboundSequence in (select max(InboundSequence) from PackType (nolock))
  --   and p.WarehouseId      = @WarehouseId
  
  --insert @TableDetail
  --      (OutboundDocumentId,
  --       OrderVolume,
  --       OrderWeight,
  --       NumberOfLines)
  --select OutboundDocumentId,
  --       Quantity * Volume,
  --       Quantity * Weight,
  --       null
  --  from @TableTemp
  
  update @TableHeader
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @WarehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableHeader
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableHeader
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableHeader
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  if @ShowOrders != 'true'
  begin
    update @TableHeader
       set OrderNumber         = null,
           ExternalCompanyCode = null,
           ExternalCompany     = null
     where OutboundShipmentId is not null
       and ReferenceNumber is not null
  end
  
  select isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',
         case when @ShowOrders = 'true' or OutboundShipmentId is null
                then IssueId
                else -1
                end as 'IssueId',
         isnull(OrderNumber, ReferenceNumber) as 'OrderNumber',
         ExternalCompanyCode,
         ExternalCompany,
         Route,
         sum(NumberOfLines) as 'NumberOfLines',
         max(DeliveryDate) as 'DeliveryDate',
         Status,
         OutboundDocumentType,
         sum(OrderVolume) as 'OrderVolume',
         sum(OrderWeight) as 'OrderWeight'
    from @TableHeader
   --where ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ExternalCompanyCode)
   --  and ExternalCompany     like isnull(@ExternalCompany + '%', ExternalCompany)
   --  and OrderNumber         like isnull(@OrderNumber  + '%', OrderNumber)
     --and ISNULL(OutboundShipmentId, -1) case when @ShowOrders = 'true'
     --           then IssueId
     --           else -1
     --           end
  group by case when @ShowOrders = 'true' or OutboundShipmentId is null
                then IssueId
                else -1
                end,
           isnull(OutboundShipmentId,-1),
           isnull(OrderNumber, ReferenceNumber),
           ExternalCompanyCode,
           ExternalCompany,
           Route,
           Status,
           OutboundDocumentType
end
