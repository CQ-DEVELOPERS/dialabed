﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PickException_Update_Line
  ///   Filename       : p_PickException_Update_Line.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PickException_Update_Line
(
 @instructionId int,
 @CheckQuantity float
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int,
          @StatusId          int,
          @StatusCode        nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @JobId      = i.JobId,
         @StatusCode = s.StatusCode
    from Instruction i (nolock)
    join Job         j (nolock) on i.JobId = j.JobId
    join Status      s (nolock) on j.StatusId = s.StatusId
   where i.InstructionId = @instructionId
  
  if @StatusCode in ('QA')
    select @StatusId = dbo.ufn_StatusId('IS','CK'), @StatusCode = 'CK'
  
  if @StatusCode in ('A')
    select @StatusId = dbo.ufn_StatusId('IS','M'), @StatusCode = 'M'
  
  begin transaction
  
  if @StatusCode = 'M'
  begin
    exec @Error = p_Instruction_Update
     @instructionId = @instructionId,
     @ConfirmedQuantity = @CheckQuantity,
     @CheckQuantity = 0
  end
  else
  begin
    exec @Error = p_Instruction_Update
     @instructionId = @instructionId,
     @CheckQuantity = @CheckQuantity
  end
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Job_Update
   @jobId    = @jobId,
   @StatusId = @StatusId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Status_Rollup
   @jobId = @JobId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_PickException_Update_Line'
    rollback transaction
    return @Error
end
