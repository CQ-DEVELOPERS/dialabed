﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WarehouseCodeReference_Search
  ///   Filename       : p_WarehouseCodeReference_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 09:52:56
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the WarehouseCodeReference table.
  /// </remarks>
  /// <param>
  ///   @WarehouseCodeReferenceId int = null output,
  ///   @WarehouseId int = null,
  ///   @InboundDocumentTypeId int = null,
  ///   @OutboundDocumentTypeId int = null,
  ///   @ToWarehouseId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   WarehouseCodeReference.WarehouseCodeReferenceId,
  ///   WarehouseCodeReference.WarehouseCode,
  ///   WarehouseCodeReference.DownloadType,
  ///   WarehouseCodeReference.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   WarehouseCodeReference.InboundDocumentTypeId,
  ///   InboundDocumentType.InboundDocumentType,
  ///   WarehouseCodeReference.OutboundDocumentTypeId,
  ///   OutboundDocumentType.OutboundDocumentType,
  ///   WarehouseCodeReference.ToWarehouseId,
  ///   Warehouse.Warehouse,
  ///   WarehouseCodeReference.ToWarehouseCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WarehouseCodeReference_Search
(
 @WarehouseCodeReferenceId int = null output,
 @WarehouseId int = null,
 @InboundDocumentTypeId int = null,
 @OutboundDocumentTypeId int = null,
 @ToWarehouseId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)
 
as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @WarehouseCodeReferenceId = '-1'
    set @WarehouseCodeReferenceId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @OutboundDocumentTypeId = '-1'
    set @OutboundDocumentTypeId = null;
  
  if @ToWarehouseId = '-1'
    set @ToWarehouseId = null;
  
 
  select
         WarehouseCodeReference.WarehouseCodeReferenceId
        ,WarehouseCodeReference.WarehouseCode
        ,WarehouseCodeReference.DownloadType
        ,WarehouseCodeReference.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,WarehouseCodeReference.InboundDocumentTypeId
         ,InboundDocumentTypeInboundDocumentTypeId.InboundDocumentType as 'InboundDocumentType'
        ,WarehouseCodeReference.OutboundDocumentTypeId
         ,OutboundDocumentTypeOutboundDocumentTypeId.OutboundDocumentType as 'OutboundDocumentType'
        ,WarehouseCodeReference.ToWarehouseId
         ,WarehouseToWarehouseId.Warehouse as 'WarehouseToWarehouseId'
        ,WarehouseCodeReference.ToWarehouseCode
    from WarehouseCodeReference
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = WarehouseCodeReference.WarehouseId
    left
    join InboundDocumentType InboundDocumentTypeInboundDocumentTypeId on InboundDocumentTypeInboundDocumentTypeId.InboundDocumentTypeId = WarehouseCodeReference.InboundDocumentTypeId
    left
    join OutboundDocumentType OutboundDocumentTypeOutboundDocumentTypeId on OutboundDocumentTypeOutboundDocumentTypeId.OutboundDocumentTypeId = WarehouseCodeReference.OutboundDocumentTypeId
    left
    join Warehouse WarehouseToWarehouseId on WarehouseToWarehouseId.WarehouseId = WarehouseCodeReference.ToWarehouseId
   where isnull(WarehouseCodeReference.WarehouseCodeReferenceId,'0')  = isnull(@WarehouseCodeReferenceId, isnull(WarehouseCodeReference.WarehouseCodeReferenceId,'0'))
     and isnull(WarehouseCodeReference.WarehouseId,'0')  = isnull(@WarehouseId, isnull(WarehouseCodeReference.WarehouseId,'0'))
     and isnull(WarehouseCodeReference.InboundDocumentTypeId,'0')  = isnull(@InboundDocumentTypeId, isnull(WarehouseCodeReference.InboundDocumentTypeId,'0'))
     and isnull(WarehouseCodeReference.OutboundDocumentTypeId,'0')  = isnull(@OutboundDocumentTypeId, isnull(WarehouseCodeReference.OutboundDocumentTypeId,'0'))
     and isnull(WarehouseCodeReference.ToWarehouseId,'0')  = isnull(@ToWarehouseId, isnull(WarehouseCodeReference.ToWarehouseId,'0'))
  
end
