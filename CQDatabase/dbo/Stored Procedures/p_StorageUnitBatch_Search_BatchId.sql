﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Search_BatchId
  ///   Filename       : p_StorageUnitBatch_Search_BatchId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007 11:54:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(30) = null,
  ///   @Product nvarchar(50) = null,
  ///   @Barcode nvarchar(50) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   p.ProductId,
  ///   p.StatusId,
  ///   s.Status,
  ///   p.ProductCode,
  ///   p.Product,
  ///   p.Barcode,
  ///   p.MinimumQuantity,
  ///   p.ReorderQuantity,
  ///   p.MaximumQuantity,
  ///   p.CuringPeriodDays,
  ///   p.ShelfLifeDays,
  ///   p.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Search_BatchId
(
 @ProductCode nvarchar(30) = null,
 @Product     nvarchar(50) = null,
 @SKUCode     nvarchar(50) = null,
 @SKU         nvarchar(50) = null,
 @Batch       nvarchar(50) = null,
 @ECLNumber   nvarchar(10) = null,
 @FromLocation nvarchar(15) = null,
 @ToLocation nvarchar(15) = null
)
 
as
begin
	 set nocount on;
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
  select sub.StorageUnitBatchId,
         b.BatchId,
         a.Area,
         l.Location,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         b.ECLNumber,
         isnull(rl.ReceiptLineId,-1) as 'ReceiptLineId',
         isnull(rl.NumberOfPallets, 999) as 'NumberOfPallets'
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join SKU              sku (nolock) on su.SKUId          = sku.SKUId
    join Product            p (nolock) on su.ProductId      = p.ProductId
    join Batch              b (nolock) on sub.BatchId       = b.BatchId
    left
    join ReceiptLine       rl (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    left
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    left
    join Location                    l (nolock) on subl.LocationId = l.LocationId
    left
    join AreaLocation               al (nolock) on l.LocationId = al.LocationId
    left
    join Area                        a (nolock) on al.AreaId = a.AreaId
   where isnull(p.ProductCode,'%') like '%' + isnull(@ProductCode, isnull(p.ProductCode,'%')) + '%'
     and isnull(p.Product,'%')     like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
     and isnull(sku.SKUCode,'%')   like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
     and isnull(sku.SKU,'%')       like '%' + isnull(@SKU, isnull(sku.SKU,'%')) + '%'
     and isnull(b.Batch,'%')       like '%' + isnull(@Batch, isnull(b.Batch,'%')) + '%'
     and isnull(b.ECLNumber,'%')   like '%' + isnull(@ECLNumber, isnull(b.ECLNumber,'%')) + '%'
     and isnull(l.Location,'A') between isnull(@FromLocation, isnull(l.Location,'A')) and isnull(@ToLocation, isnull(l.Location,'A'))
     --and isnull(l.Location,'A') between isnull(@FromLocation, l.Location) and isnull(@ToLocation, l.Location)
  order by p.ProductCode,
           sku.SKUCode,
           b.Batch,
           a.Area,
           l.Location
end
