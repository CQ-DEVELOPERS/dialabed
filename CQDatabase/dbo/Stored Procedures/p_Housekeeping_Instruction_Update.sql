﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Instruction_Update
  ///   Filename       : p_Housekeeping_Instruction_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Instruction_Update
(
 @InstructionId int,
 @JobId         int,
 @PriorityId    int,
 @Quantity      float
)
 
as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @WarehouseId       int,
          @OperatorId        int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Housekeeping_Instruction_Update'
  
  select @WarehouseId = WarehouseId,
         @OperatorId  = OperatorId
    from Instruction (nolock)
   where InstructionId = @InstructionId
  
--  if @Quantity > (select (subl.ActualQuantity - subl.ReservedQuantity) + i.Quantity
--                    from StorageUnitBatchLocation subl
--                    join Instruction                 i on subl.StorageUnitBatchId = i.StorageUnitBatchId
--                                                      and subl.LocationId         = i.PickLocationId
--                   where i.InstructionId = @InstructionId)
--  begin
--    set @Error = -1
--    set @Errormsg = 'p_Housekeeping_Instruction_Update - Cannot move more than is available.'
--    begin transaction
--    goto error
--  end
  
  begin transaction
  
  if (select dbo.ufn_Configuration(227, @WarehouseId)) = 1
    exec p_Instruction_Started
     @instructionId = @instructionId,
     @OperatorId    = @OperatorId
  
  exec @Error = p_Job_Update
   @JobId      = @JobId,
   @PriorityId = @PriorityId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId       = @InstructionId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @Quantity      = @Quantity
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @InstructionId
  
  if @Error <> 0
    goto error
  
  if (select dbo.ufn_Configuration(227, @WarehouseId)) = 1
    exec @Error = p_Instruction_Finished
     @InstructionId       = @InstructionId
    
    if @Error <> 0
      goto error
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end
