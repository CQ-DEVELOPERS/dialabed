﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockAdjustment_Insert
  ///   Filename       : p_StockAdjustment_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jan 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockAdjustment_Insert
(
 @operatorId         int = null
,@recordType         char(3) = null
,@storageUnitBatchId int = null
,@quantity           numeric(13,6) = null
,@fromWarehouseCode  nvarchar(30) = null
,@toWarehouseCode    nvarchar(30) = null
,@Msg                nvarchar(255) = null
)
 
as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_StockAdjustment_Insert;
    
		  -- Do the actual work here
    insert InterfaceExportStockAdjustment
          (RecordType,
           RecordStatus,
           CreatedBy,
           PrincipalCode,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           Quantity,
           Additional1,
           Additional2,
           Additional5) -- Only used for scripts
    select @recordType,
           'W',
           @operatorId,
           pn.PrincipalCode,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           b.Batch,
           @quantity,
           @fromWarehouseCode,
           @toWarehouseCode,
           @Msg
      from StorageUnitBatch sub (nolock) 
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      join Product            p (nolock) on su.ProductId = p.ProductId
      left
      join Principal         pn (nolock) on p.PrincipalId = pn.PrincipalId
      join SKU              sku (nolock) on su.SKUId = sku.SKUId
      join Batch              b (nolock) on sub.BatchId = b.BatchId
     where sub.StorageUnitBatchId = @storageUnitBatchId
	   
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_StockAdjustment_Insert;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
 
