﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_On_Hand_By_Price
  ///   Filename       : p_Report_Stock_On_Hand_By_Price.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 22 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_On_Hand_By_Price
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @BatchId           int = null,
 @StoreLocationId   int = null,
 @PickLocationId    int = null,
 @LocationTypeId    int = null,
 @DivisionId		int = null,
 @BatchMore         int = -1,
 @BatchQuantity     float = 0,
 @AllocatedMore     int = -1,
 @AllocatedQuantity float = 0,
 @ReservedMore      int = -1,
 @ReservedQuantity  float = 0
)
 
as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
        ProductCode       nvarchar(30),
        Product           nvarchar(50),
        SKUCode           nvarchar(50),
        Batch             nvarchar(30),
        BatchID           int,
        BatchCreateDate	  datetime,
        ReferenceNumber   nvarchar(30),
        GRNDate           dateTime,
        StorageUnitID     int,
        DivisionCode      nvarchar(10),
        Area              nvarchar(50),
        Location          nvarchar(15),
        ActualQuantity    numeric(13,6),
        AllocatedQuantity numeric(13,6),
        ReservedQuantity  numeric(13,6),
        BatchQuantity     numeric(13,6),
        Status			  nvarchar(50),
        Price             numeric(13,2)
     )
	 
     DECLARE @TableSub TABLE
	 (
		StorageUnitId	int,
		BatchId			int,
		Price			numeric(13,2),
		ReferenceNumber nvarchar(30),
		GRNDate         dateTime,
		DivisionCode	nvarchar(10)
	 )
	 
	 DECLARE @DivisionCode nvarchar(10)
	 
  if @StorageUnitId = -1 set @StorageUnitId = null
  if @BatchId = -1 set @BatchId = null
  if @StoreLocationId = -1 set @StoreLocationId = null
  if @PickLocationId = -1 set @PickLocationId = null
  if @LocationTypeId = -1 set @LocationTypeId = null
  if @DivisionId = -1 set @DivisionId = null
  if @BatchMore = -1 set @BatchMore = null
  if @AllocatedMore = -1 set @AllocatedMore = null
  if @ReservedMore = -1 set @ReservedMore = null
	 
	 insert @TableResult
	       (ProductCode,
         Product,
         SKUCode,
         Batch,
         BatchID,
         BatchCreateDate,
         StorageUnitID,
         Area,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         BatchQuantity,
         Status)
  select p.ProductCode,
         p.Product,
         sku.SKUCode,
         case when s.StatusCode != 'A'
            then b.Batch + '('+ s.StatusCode + ')'
            else b.Batch
            end as 'Batch',
         b.BatchID,
         b.CreateDate,
         sub.StorageUnitID,
         a.Area,
         l.Location,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         b.ActualYield,
         s.Status
from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
   where a.WarehouseId      = @WarehouseId
     and su.StorageUnitId   = isnull(@StorageUnitId, su.StorageUnitId)
     and b.BatchId          = isnull(@BatchId, b.BatchId)
     and l.LocationId between isnull(@StoreLocationId, l.LocationId) and isnull(@PickLocationId, l.LocationId)
     and l.LocationTypeId   = isnull(@LocationTypeId, l.LocationTypeId)
     and subl.ActualQuantity <> 0
     and a.StockOnHand      = 1
  
  if @@rowcount = 0
  insert @TableResult
        (ProductCode, AllocatedQuantity)
  select '', 0
  
  if @BatchMore is not null
  if @BatchMore = 0
    delete @TableResult where BatchQuantity >= @BatchQuantity
  else
    delete @TableResult where BatchQuantity <= @BatchQuantity
  
  if @AllocatedMore is not null
  if @AllocatedMore = 0
    delete @TableResult where AllocatedQuantity >= @AllocatedQuantity
  else
    delete @TableResult where AllocatedQuantity <= @AllocatedQuantity
  
  if @ReservedMore is not null
  if @ReservedMore = 0
    delete @TableResult where ReservedQuantity >= @ReservedQuantity
  else
    delete @TableResult where ReservedQuantity <= @ReservedQuantity
    
 Insert Into @TableSub
  Select Distinct
		 il.StorageUnitId 
		,il.BatchId
		,il.UnitPrice
		,id.ReferenceNumber
		,r.DeliveryDate
		,d.DivisionCode
  from InboundLine il 
  inner join InboundDocument id on il.InboundDocumentId = id.InboundDocumentId
  inner join Receipt r on id.InboundDocumentId = r.InboundDocumentId
  left join Division d on id.DivisionId = d.DivisionId
  Where id.DivisionId   = isnull(@DivisionId, id.DivisionId)
    and Exists
	(Select StorageUnitId
	 From @TableResult tr
	 Where tr.StorageUnitId = il.StorageUnitId)
  
  Update t
  Set t.Price = ts.Price
	 ,t.DivisionCode = ts.DivisionCode
	 ,t.ReferenceNumber = ts.ReferenceNumber
	 ,t.GRNDate = ts.GRNDate
  From @TableResult t
  Inner join @TableSub ts on t.StorageUnitId = ts.StorageUnitId
						 and t.BatchId = ts.BatchId
  
  select @DivisionCode = DivisionCode
  From Division
  Where DivisionId = @DivisionId
  
  select ProductCode,
         Product,
         SKUCode,
         ReferenceNumber,
         GRNDate,
         DivisionCode,
         Area,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         Status,
         Price
    from @TableResult
  where DivisionCode = ISNULL(@DivisionCode, DivisionCode)
  order by ProductCode
end
