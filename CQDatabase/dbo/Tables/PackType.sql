﻿CREATE TABLE [dbo].[PackType] (
    [PackTypeId]       INT           IDENTITY (1, 1) NOT NULL,
    [PackType]         NVARCHAR (30) NOT NULL,
    [InboundSequence]  SMALLINT      NOT NULL,
    [OutboundSequence] SMALLINT      NOT NULL,
    [PackTypeCode]     VARCHAR (10)  NULL,
    [Unit]             BIT           DEFAULT ((0)) NULL,
    [Pallet]           BIT           DEFAULT ((0)) NULL,
    CONSTRAINT [PackType_PK] PRIMARY KEY CLUSTERED ([PackTypeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_PackType] UNIQUE NONCLUSTERED ([PackType] ASC) WITH (FILLFACTOR = 90)
);

