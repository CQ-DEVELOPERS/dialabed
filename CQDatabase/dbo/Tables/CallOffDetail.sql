﻿CREATE TABLE [dbo].[CallOffDetail] (
    [CallOffDetailId]       INT        IDENTITY (1, 1) NOT NULL,
    [CallOffHeaderId]       INT        NULL,
    [PurchaseOrderHeaderId] INT        NULL,
    [PurchaseOrderDetailId] INT        NULL,
    [StorageUnitId]         INT        NULL,
    [BatchId]               INT        NULL,
    [RequiredQuantity]      FLOAT (53) NULL,
    [StatusId]              INT        NULL,
    PRIMARY KEY CLUSTERED ([CallOffDetailId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CallOffHeaderId]) REFERENCES [dbo].[CallOffHeader] ([CallOffHeaderId]),
    FOREIGN KEY ([PurchaseOrderDetailId]) REFERENCES [dbo].[PurchaseOrderDetail] ([PurchaseOrderDetailId]),
    FOREIGN KEY ([PurchaseOrderHeaderId]) REFERENCES [dbo].[PurchaseOrderHeader] ([PurchaseOrderHeaderId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId])
);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffDetail_PurchaseOrderHeaderId]
    ON [dbo].[CallOffDetail]([PurchaseOrderHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffDetail_PurchaseOrderDetailId]
    ON [dbo].[CallOffDetail]([PurchaseOrderDetailId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffDetail_StatusId]
    ON [dbo].[CallOffDetail]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffDetail_CallOffHeaderId]
    ON [dbo].[CallOffDetail]([CallOffHeaderId] ASC) WITH (FILLFACTOR = 90);

