﻿CREATE TABLE [dbo].[UOMHistory] (
    [UOMId]       INT           NULL,
    [UOM]         NVARCHAR (50) NULL,
    [UOMCode]     NVARCHAR (10) NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL
);

