﻿CREATE TABLE [dbo].[InterfaceImportSODetail] (
    [InterfaceImportSOHeaderId] INT            NULL,
    [ForeignKey]                NVARCHAR (30)  NULL,
    [LineNumber]                INT            NULL,
    [ProductCode]               NVARCHAR (30)  NOT NULL,
    [Product]                   NVARCHAR (50)  NULL,
    [SKUCode]                   NVARCHAR (10)  NULL,
    [Batch]                     NVARCHAR (50)  NULL,
    [Quantity]                  FLOAT (53)     NOT NULL,
    [Weight]                    FLOAT (53)     NULL,
    [Additional1]               NVARCHAR (255) NULL,
    [Additional2]               NVARCHAR (255) NULL,
    [Additional3]               NVARCHAR (255) NULL,
    [Additional4]               NVARCHAR (255) NULL,
    [Additional5]               NVARCHAR (255) NULL,
    FOREIGN KEY ([InterfaceImportSOHeaderId]) REFERENCES [dbo].[InterfaceImportSOHeader] ([InterfaceImportSOHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportSODetail_InterfaceImportSOHeaderId]
    ON [dbo].[InterfaceImportSODetail]([InterfaceImportSOHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportSODetail]
    ON [dbo].[InterfaceImportSODetail]([InterfaceImportSOHeaderId] ASC, [ForeignKey] ASC) WITH (FILLFACTOR = 90);

