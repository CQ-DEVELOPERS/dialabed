﻿CREATE TABLE [dbo].[InboundLineHistory] (
    [InboundLineId]      INT             NULL,
    [InboundDocumentId]  INT             NULL,
    [StorageUnitId]      INT             NULL,
    [StatusId]           INT             NULL,
    [LineNumber]         INT             NULL,
    [Quantity]           FLOAT (53)      NULL,
    [BatchId]            INT             NULL,
    [CommandType]        NVARCHAR (10)   NOT NULL,
    [InsertDate]         DATETIME        NOT NULL,
    [ReasonId]           INT             NULL,
    [UnitPrice]          NUMERIC (13, 2) NULL,
    [ChildStorageUnitId] INT             NULL,
    [BOELineNumber]      NVARCHAR (50)   NULL,
    [TariffCode]         NVARCHAR (50)   NULL,
    [CountryofOrigin]    NVARCHAR (100)  NULL,
    [Reference1]         NVARCHAR (50)   NULL,
    [Reference2]         NVARCHAR (50)   NULL
);

