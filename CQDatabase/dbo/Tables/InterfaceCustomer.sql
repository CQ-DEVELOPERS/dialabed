﻿CREATE TABLE [dbo].[InterfaceCustomer] (
    [HostId]         NVARCHAR (30)  NULL,
    [CustomerCode]   NVARCHAR (30)  NULL,
    [CustomerName]   NVARCHAR (255) NULL,
    [Class]          NVARCHAR (255) NULL,
    [DeliveryPoint]  NVARCHAR (255) NULL,
    [Address1]       NVARCHAR (255) NULL,
    [Address2]       NVARCHAR (255) NULL,
    [Address3]       NVARCHAR (255) NULL,
    [Address4]       NVARCHAR (255) NULL,
    [Modified]       NVARCHAR (10)  NULL,
    [ContactPerson]  NVARCHAR (100) NULL,
    [Phone]          NVARCHAR (255) NULL,
    [Fax]            NVARCHAR (255) NULL,
    [Email]          NVARCHAR (255) NULL,
    [DeliveryGroup]  NVARCHAR (255) NULL,
    [DepotCode]      NVARCHAR (255) NULL,
    [VisitFrequency] NVARCHAR (255) NULL,
    [ProcessedDate]  DATETIME       NULL,
    [RecordStatus]   CHAR (1)       DEFAULT ('N') NULL,
    [InsertDate]     DATETIME       CONSTRAINT [DF_InterfaceCustomer_InsertDate] DEFAULT (getdate()) NULL,
    [PrincipalCode]  NVARCHAR (30)  NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceCustomer_Status_Dates]
    ON [dbo].[InterfaceCustomer]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceCustomer_CustomerCode_ProcessedDate_RecordStatus]
    ON [dbo].[InterfaceCustomer]([CustomerCode] ASC, [ProcessedDate] ASC, [RecordStatus] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceCustomer_ProcessedDate]
    ON [dbo].[InterfaceCustomer]([ProcessedDate] ASC)
    INCLUDE([HostId], [CustomerCode], [CustomerName], [Class], [DeliveryPoint], [Address1], [Address2], [Address3], [Address4], [Modified], [ContactPerson], [Phone], [Fax], [Email], [DeliveryGroup], [DepotCode], [VisitFrequency], [PrincipalCode]) WITH (FILLFACTOR = 90);

