﻿CREATE TABLE [dbo].[CompanyHistory] (
    [CompanyId]    INT           NULL,
    [Company]      NVARCHAR (50) NULL,
    [CompanyCode]  NVARCHAR (10) NULL,
    [CommandType]  NVARCHAR (10) NOT NULL,
    [InsertDate]   DATETIME      NOT NULL,
    [CustomsCode]  NVARCHAR (20) NULL,
    [Registration] NVARCHAR (20) NULL
);

