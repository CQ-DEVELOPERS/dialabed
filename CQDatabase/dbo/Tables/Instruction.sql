﻿CREATE TABLE [dbo].[Instruction] (
    [InstructionId]      INT           IDENTITY (1, 1) NOT NULL,
    [ReasonId]           INT           NULL,
    [InstructionTypeId]  INT           NOT NULL,
    [StorageUnitBatchId] INT           NULL,
    [WarehouseId]        INT           NOT NULL,
    [StatusId]           INT           NOT NULL,
    [JobId]              INT           NULL,
    [OperatorId]         INT           NULL,
    [PickLocationId]     INT           NULL,
    [StoreLocationId]    INT           NULL,
    [ReceiptLineId]      INT           NULL,
    [IssueLineId]        INT           NULL,
    [InstructionRefId]   INT           NULL,
    [Quantity]           FLOAT (53)    NOT NULL,
    [ConfirmedQuantity]  FLOAT (53)    NULL,
    [Weight]             FLOAT (53)    NULL,
    [ConfirmedWeight]    FLOAT (53)    NULL,
    [PalletId]           INT           NULL,
    [CreateDate]         DATETIME      NULL,
    [StartDate]          DATETIME      NULL,
    [EndDate]            DATETIME      NULL,
    [Picked]             BIT           NULL,
    [Stored]             BIT           NULL,
    [CheckQuantity]      FLOAT (53)    NULL,
    [CheckWeight]        FLOAT (53)    NULL,
    [OutboundShipmentId] INT           NULL,
    [DropSequence]       INT           NULL,
    [PackagingWeight]    FLOAT (53)    NULL,
    [PackagingQuantity]  FLOAT (53)    NULL,
    [NettWeight]         FLOAT (53)    NULL,
    [PreviousQuantity]   FLOAT (53)    NULL,
    [AuthorisedBy]       INT           NULL,
    [AuthorisedStatus]   NVARCHAR (10) NULL,
    [SOH]                FLOAT (53)    NULL,
    [AutoComplete]       BIT           NULL,
    [OverrideStore]      BIT           NULL,
    CONSTRAINT [Instruction_PK] PRIMARY KEY CLUSTERED ([InstructionId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([IssueLineId]) REFERENCES [dbo].[IssueLine] ([IssueLineId]),
    FOREIGN KEY ([OutboundShipmentId]) REFERENCES [dbo].[OutboundShipment] ([OutboundShipmentId]),
    FOREIGN KEY ([ReceiptLineId]) REFERENCES [dbo].[ReceiptLine] ([ReceiptLineId]),
    CONSTRAINT [FK__Instructi__Autho__0675CF01] FOREIGN KEY ([AuthorisedBy]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [FK_Instruction_Pallet] FOREIGN KEY ([PalletId]) REFERENCES [dbo].[Pallet] ([PalletId]),
    CONSTRAINT [InstructionType_Instruction_FK1] FOREIGN KEY ([InstructionTypeId]) REFERENCES [dbo].[InstructionType] ([InstructionTypeId]),
    CONSTRAINT [Job_Instruction_FK1] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([JobId]),
    CONSTRAINT [Reason_Instruction_FK1] FOREIGN KEY ([ReasonId]) REFERENCES [dbo].[Reason] ([ReasonId]),
    CONSTRAINT [Status_Instruction_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [StorageUnitBatch_Instruction_FK1] FOREIGN KEY ([StorageUnitBatchId]) REFERENCES [dbo].[StorageUnitBatch] ([StorageUnitBatchId]),
    CONSTRAINT [Warehouse_Instruction_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_InstructionTypeId]
    ON [dbo].[Instruction]([InstructionTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_StorageUnitBatchId]
    ON [dbo].[Instruction]([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_OperatorId]
    ON [dbo].[Instruction]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_StatusId]
    ON [dbo].[Instruction]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_ReasonId]
    ON [dbo].[Instruction]([ReasonId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_JobId]
    ON [dbo].[Instruction]([JobId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_PalletId]
    ON [dbo].[Instruction]([PalletId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_WarehouseId]
    ON [dbo].[Instruction]([WarehouseId] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [StorageUnitBatchId], [StatusId], [JobId], [OperatorId], [PickLocationId], [StoreLocationId], [ReceiptLineId], [InstructionRefId], [Quantity], [ConfirmedQuantity], [PalletId], [CreateDate], [StartDate], [EndDate], [CheckQuantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_IssueLineId]
    ON [dbo].[Instruction]([IssueLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_StatusId]
    ON [dbo].[Instruction]([StatusId] ASC) WITH (FILLFACTOR = 95);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction]
    ON [dbo].[Instruction]([InstructionTypeId] ASC, [StatusId] ASC, [StorageUnitBatchId] ASC, [PickLocationId] ASC, [StoreLocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [InstructionRefId_idx]
    ON [dbo].[Instruction]([InstructionRefId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Instruction_AuthorisedBy]
    ON [dbo].[Instruction]([AuthorisedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_OutboundShipmentId]
    ON [dbo].[Instruction]([OutboundShipmentId] ASC)
    INCLUDE([JobId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_ConfirmedQuantity]
    ON [dbo].[Instruction]([ConfirmedQuantity] ASC)
    INCLUDE([InstructionId], [JobId], [InstructionRefId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_InstructionTypeId]
    ON [dbo].[Instruction]([InstructionTypeId] ASC)
    INCLUDE([WarehouseId], [StatusId], [PickLocationId], [StartDate], [EndDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_EndDate]
    ON [dbo].[Instruction]([EndDate] ASC)
    INCLUDE([WarehouseId], [JobId], [ConfirmedQuantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_Stored_ConfirmedQuantity_CreateDate]
    ON [dbo].[Instruction]([Stored] ASC, [ConfirmedQuantity] ASC, [CreateDate] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [StorageUnitBatchId], [WarehouseId], [StatusId], [JobId], [PickLocationId], [StoreLocationId], [StartDate], [EndDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_InstructionTypeId_EndDate]
    ON [dbo].[Instruction]([InstructionTypeId] ASC, [EndDate] ASC)
    INCLUDE([InstructionId], [WarehouseId], [JobId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_InstructionTypeId_Stored_ConfirmedQuantity_CreateDate]
    ON [dbo].[Instruction]([InstructionTypeId] ASC, [Stored] ASC, [ConfirmedQuantity] ASC, [CreateDate] ASC)
    INCLUDE([InstructionId], [StorageUnitBatchId], [WarehouseId], [StatusId], [JobId], [PickLocationId], [StoreLocationId], [StartDate], [EndDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_ReceiptLineId]
    ON [dbo].[Instruction]([ReceiptLineId] ASC)
    INCLUDE([JobId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_StoreLocationId]
    ON [dbo].[Instruction]([StoreLocationId] ASC)
    INCLUDE([InstructionId], [StorageUnitBatchId], [WarehouseId], [JobId], [PickLocationId], [ReceiptLineId], [Quantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_PickLocationId]
    ON [dbo].[Instruction]([PickLocationId] ASC)
    INCLUDE([InstructionTypeId], [StatusId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_WarehouseId_StoreLocationId]
    ON [dbo].[Instruction]([WarehouseId] ASC, [StoreLocationId] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [StorageUnitBatchId], [StatusId], [JobId], [PickLocationId], [InstructionRefId], [Quantity], [ConfirmedQuantity], [Picked], [Stored], [CheckQuantity], [PreviousQuantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_WarehouseId_StoreLocationId3]
    ON [dbo].[Instruction]([WarehouseId] ASC, [StoreLocationId] ASC)
    INCLUDE([InstructionTypeId], [StatusId], [JobId], [PickLocationId], [Picked], [Stored]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_PickLocationId1]
    ON [dbo].[Instruction]([PickLocationId] ASC)
    INCLUDE([InstructionId], [StorageUnitBatchId], [StatusId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_WarehouseId_StatusId_StoreLocationId]
    ON [dbo].[Instruction]([WarehouseId] ASC, [StatusId] ASC, [StoreLocationId] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [StorageUnitBatchId], [JobId], [PickLocationId], [InstructionRefId], [Quantity], [ConfirmedQuantity], [Picked], [Stored], [CheckQuantity], [PreviousQuantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_WarehouseId_StatusId_StoreLocationId1]
    ON [dbo].[Instruction]([WarehouseId] ASC, [StatusId] ASC, [StoreLocationId] ASC)
    INCLUDE([InstructionTypeId], [JobId], [PickLocationId], [Picked], [Stored]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_StorageUnitBatchId_StoreLocationId_Stored_ConfirmedQuantity_CreateDate]
    ON [dbo].[Instruction]([StorageUnitBatchId] ASC, [StoreLocationId] ASC, [Stored] ASC, [ConfirmedQuantity] ASC, [CreateDate] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [WarehouseId], [StatusId], [JobId], [PickLocationId], [StartDate], [EndDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_EndDate1]
    ON [dbo].[Instruction]([EndDate] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [WarehouseId], [JobId], [OperatorId], [Quantity], [ConfirmedQuantity], [Weight], [ConfirmedWeight], [StartDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_EndDate2]
    ON [dbo].[Instruction]([EndDate] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [WarehouseId], [JobId], [OperatorId], [ConfirmedQuantity], [ConfirmedWeight], [StartDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_StorageUnitBatchId_WarehouseId]
    ON [dbo].[Instruction]([StorageUnitBatchId] ASC, [WarehouseId] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [StatusId], [OperatorId], [PickLocationId], [StoreLocationId], [ReceiptLineId], [InstructionRefId], [Quantity], [ConfirmedQuantity], [PalletId], [CreateDate], [StartDate], [EndDate], [CheckQuantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_EndDate3]
    ON [dbo].[Instruction]([EndDate] ASC)
    INCLUDE([InstructionId], [StorageUnitBatchId], [WarehouseId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_InstructionTypeId1]
    ON [dbo].[Instruction]([InstructionTypeId] ASC)
    INCLUDE([InstructionId], [StorageUnitBatchId], [StatusId], [JobId], [PickLocationId], [Quantity], [ConfirmedQuantity], [CreateDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_ConfirmedQuantity1]
    ON [dbo].[Instruction]([ConfirmedQuantity] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [StorageUnitBatchId], [StatusId], [JobId], [OperatorId], [StoreLocationId], [InstructionRefId], [Quantity], [StartDate], [EndDate], [OutboundShipmentId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_JobId]
    ON [dbo].[Instruction]([JobId] ASC) WITH (FILLFACTOR = 95);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_InstructionRefId]
    ON [dbo].[Instruction]([InstructionRefId] ASC) WITH (FILLFACTOR = 95);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_WarehouseId]
    ON [dbo].[Instruction]([WarehouseId] ASC) WITH (FILLFACTOR = 95);


GO
CREATE NONCLUSTERED INDEX [IX_Instruction_CLUIndex]
    ON [dbo].[Instruction]([PickLocationId] ASC, [StatusId] ASC)
    INCLUDE([InstructionId], [InstructionTypeId], [StorageUnitBatchId], [WarehouseId], [JobId], [StoreLocationId], [InstructionRefId], [Quantity]) WITH (FILLFACTOR = 95);

