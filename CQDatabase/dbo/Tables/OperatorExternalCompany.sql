﻿CREATE TABLE [dbo].[OperatorExternalCompany] (
    [OperatorId]        INT NULL,
    [ExternalCompanyId] INT NULL,
    CONSTRAINT [FK__OperatorE__Exter__5D45A389] FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    CONSTRAINT [FK__OperatorE__Opera__5C517F50] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorExternalCompany_ExternalCompanyId]
    ON [dbo].[OperatorExternalCompany]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorExternalCompany_OperatorId]
    ON [dbo].[OperatorExternalCompany]([OperatorId] ASC) WITH (FILLFACTOR = 90);

