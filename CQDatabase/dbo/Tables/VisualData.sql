﻿CREATE TABLE [dbo].[VisualData] (
    [Key1]      VARCHAR (50)     NULL,
    [Sort1]     VARCHAR (50)     NULL,
    [Sort2]     VARCHAR (50)     NULL,
    [Sort3]     VARCHAR (50)     NULL,
    [Colour]    INT              NULL,
    [Latitude]  DECIMAL (16, 13) NULL,
    [Longitude] DECIMAL (16, 13) NULL,
    [Info]      VARCHAR (1000)   NULL
);

