﻿CREATE TABLE [dbo].[ResponseTime] (
    [ResponseTimeId] INT            IDENTITY (1, 1) NOT NULL,
    [MethodName]     NVARCHAR (100) NOT NULL,
    [ResponseTime]   NVARCHAR (100) NOT NULL,
    [CreateDate]     DATETIME       NOT NULL,
    [KeyName]        NVARCHAR (100) NULL,
    [KeyId]          INT            NULL,
    [OperatorId]     INT            NULL,
    CONSTRAINT [ResponseTime_PK] PRIMARY KEY CLUSTERED ([ResponseTimeId] ASC) WITH (FILLFACTOR = 90)
);

