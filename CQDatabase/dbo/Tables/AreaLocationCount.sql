﻿CREATE TABLE [dbo].[AreaLocationCount] (
    [InstructionTypeCode] VARCHAR (25) NULL,
    [AreaId]              INT          NOT NULL,
    [LocationCount]       INT          NULL
);

