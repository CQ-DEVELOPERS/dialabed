﻿CREATE TABLE [dbo].[OutboundLine] (
    [OutboundLineId]     INT             IDENTITY (1, 1) NOT NULL,
    [OutboundDocumentId] INT             NOT NULL,
    [StorageUnitId]      INT             NOT NULL,
    [StatusId]           INT             NOT NULL,
    [LineNumber]         INT             NOT NULL,
    [Quantity]           FLOAT (53)      NOT NULL,
    [BatchId]            INT             NULL,
    [UnitPrice]          FLOAT (53)      NULL,
    [UOM]                NVARCHAR (30)   NULL,
    [BOELineNumber]      NVARCHAR (50)   NULL,
    [TariffCode]         NVARCHAR (50)   NULL,
    [ActualPrice]        NUMERIC (13, 2) NULL,
    [CaseNo]             NVARCHAR (60)   NULL,
    [CountryOfOrigin]    NVARCHAR (60)   NULL,
    [CPCCode]            NVARCHAR (60)   NULL,
    [CustomsValue]       NUMERIC (13, 2) NULL,
    [EstimatedDuty]      NUMERIC (13, 2) NULL,
    [EstimatedVAT]       NUMERIC (13, 2) NULL,
    [LineDescription]    NVARCHAR (510)  NULL,
    [PackSize]           NUMERIC (13, 2) NULL,
    [PreviousCPCCode]    NVARCHAR (60)   NULL,
    [PreviousLineNumber] NVARCHAR (60)   NULL,
    [PreviousMRNNumber]  NVARCHAR (60)   NULL,
    [ProcedureMeasure]   NVARCHAR (510)  NULL,
    [ROONumber]          NVARCHAR (60)   NULL,
    [TradeAgreementCode] NVARCHAR (60)   NULL,
    [VOCApplied]         BIT             NULL,
    CONSTRAINT [OutboundLine_PK] PRIMARY KEY CLUSTERED ([OutboundLineId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [OutboundDocument_OutboundLine_FK1] FOREIGN KEY ([OutboundDocumentId]) REFERENCES [dbo].[OutboundDocument] ([OutboundDocumentId]),
    CONSTRAINT [Status_OutboundLine_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [StorageUnit_OutboundLine_FK1] FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundLine_OutboundDocumentId]
    ON [dbo].[OutboundLine]([OutboundDocumentId] ASC) WITH (FILLFACTOR = 90);

