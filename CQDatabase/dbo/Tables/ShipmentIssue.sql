﻿CREATE TABLE [dbo].[ShipmentIssue] (
    [ShipmentId]   INT      NOT NULL,
    [IssueId]      INT      NOT NULL,
    [DropSequence] SMALLINT NULL,
    CONSTRAINT [ShipmentIssue_PK] PRIMARY KEY CLUSTERED ([ShipmentId] ASC, [IssueId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Issue_ShipmentIssue_FK1] FOREIGN KEY ([IssueId]) REFERENCES [dbo].[Issue] ([IssueId]),
    CONSTRAINT [Shipment_ShipmentIssue_FK1] FOREIGN KEY ([ShipmentId]) REFERENCES [dbo].[Shipment] ([ShipmentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentIssue_ShipmentId]
    ON [dbo].[ShipmentIssue]([ShipmentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentIssue_IssueId]
    ON [dbo].[ShipmentIssue]([IssueId] ASC) WITH (FILLFACTOR = 90);

