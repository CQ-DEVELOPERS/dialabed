﻿CREATE TABLE [dbo].[InterfaceLog] (
    [InterfaceLogId] INT            IDENTITY (1, 1) NOT NULL,
    [Data]           NVARCHAR (255) NULL,
    [RecordStatus]   CHAR (1)       DEFAULT ('N') NULL,
    [ProcessedDate]  DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([InterfaceLogId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceLog]
    ON [dbo].[InterfaceLog]([Data] ASC, [RecordStatus] ASC, [ProcessedDate] ASC) WITH (FILLFACTOR = 90);

