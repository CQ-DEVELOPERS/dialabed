﻿CREATE TABLE [dbo].[CheckingProduct] (
    [InstructionId]      INT            NULL,
    [WarehouseId]        INT            NULL,
    [OrderNumber]        VARCHAR (MAX)  NULL,
    [JobId]              INT            NULL,
    [ReferenceNumber]    NVARCHAR (30)  NULL,
    [StorageUnitBatchId] INT            NULL,
    [StorageUnitId]      INT            NULL,
    [ProductCode]        NVARCHAR (30)  NULL,
    [ProductBarcode]     NVARCHAR (50)  NULL,
    [PackBarcode]        NVARCHAR (50)  NULL,
    [Product]            NVARCHAR (255) NULL,
    [SKUCode]            NVARCHAR (50)  NULL,
    [Batch]              NVARCHAR (50)  NULL,
    [ExpiryDate]         DATETIME       NULL,
    [Quantity]           FLOAT (53)     NULL,
    [ConfirmedQuantity]  FLOAT (53)     NULL,
    [CheckQuantity]      FLOAT (53)     NULL,
    [Checked]            INT            NULL,
    [Total]              INT            NULL,
    [Lines]              NVARCHAR (255) NULL,
    [InstructionRefId]   INT            NULL,
    [SkipUpdate]         BIT            NULL,
    [InsertDate]         DATETIME       CONSTRAINT [DF__CheckingP__Inser__3C574F05] DEFAULT (getdate()) NULL,
    [ErrorMsg]           NVARCHAR (MAX) NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_CheckingProduct_JobId]
    ON [dbo].[CheckingProduct]([JobId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CheckingProduct_StorageUnitId]
    ON [dbo].[CheckingProduct]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CheckingProduct_StorageUnitBatchId]
    ON [dbo].[CheckingProduct]([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CheckingProduct_ProductCode]
    ON [dbo].[CheckingProduct]([ProductCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CheckingProduct_ProductBarcode]
    ON [dbo].[CheckingProduct]([ProductBarcode] ASC) WITH (FILLFACTOR = 90);

