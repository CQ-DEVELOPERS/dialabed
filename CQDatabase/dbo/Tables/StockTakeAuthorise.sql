﻿CREATE TABLE [dbo].[StockTakeAuthorise] (
    [InstructionId] INT           NULL,
    [StatusCode]    NVARCHAR (10) NULL,
    [OperatorId]    INT           NULL,
    [InsertDate]    DATETIME      NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_StockTakeAuthorise_InstructionId]
    ON [dbo].[StockTakeAuthorise]([InstructionId] ASC) WITH (FILLFACTOR = 90);

