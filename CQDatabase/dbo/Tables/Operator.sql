﻿CREATE TABLE [dbo].[Operator] (
    [OperatorId]        INT            IDENTITY (1, 1) NOT NULL,
    [OperatorGroupId]   INT            NOT NULL,
    [WarehouseId]       INT            NOT NULL,
    [Operator]          NVARCHAR (50)  NOT NULL,
    [OperatorCode]      NVARCHAR (50)  NULL,
    [Password]          NVARCHAR (50)  NULL,
    [NewPassword]       NVARCHAR (50)  NULL,
    [ActiveIndicator]   BIT            NOT NULL,
    [ExpiryDate]        DATETIME       NOT NULL,
    [LastInstruction]   DATETIME       NULL,
    [Printer]           NVARCHAR (50)  NULL,
    [Port]              NVARCHAR (50)  NULL,
    [IPAddress]         NVARCHAR (50)  NULL,
    [CultureId]         INT            NULL,
    [LoginTime]         DATETIME       NULL,
    [LogoutTime]        DATETIME       NULL,
    [LastActivity]      DATETIME       NULL,
    [SiteType]          NVARCHAR (20)  NULL,
    [Name]              NVARCHAR (100) NULL,
    [Surname]           NVARCHAR (100) NULL,
    [PrincipalId]       INT            NULL,
    [PickAisle]         NVARCHAR (10)  NULL,
    [StoreAisle]        NVARCHAR (10)  NULL,
    [MenuId]            INT            NULL,
    [ExternalCompanyId] INT            NULL,
    [Upload]            BIT            DEFAULT ((0)) NULL,
    [Deleted]           BIT            NULL,
    CONSTRAINT [Operators_PK] PRIMARY KEY CLUSTERED ([OperatorId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([MenuId]) REFERENCES [dbo].[Menu] ([MenuId]),
    FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId]),
    CONSTRAINT [FK_Operator_Culture] FOREIGN KEY ([CultureId]) REFERENCES [dbo].[Culture] ([CultureId]),
    CONSTRAINT [OperatorGroup_Operators_FK1] FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId]),
    CONSTRAINT [Warehouse_Operators_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId]),
    CONSTRAINT [uc_Operator] UNIQUE NONCLUSTERED ([Operator] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_OperatorCode] UNIQUE NONCLUSTERED ([OperatorCode] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_Operator_OperatorGroupId]
    ON [dbo].[Operator]([OperatorGroupId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Operator_PrincipalId]
    ON [dbo].[Operator]([PrincipalId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Operator_ExternalCompanyId]
    ON [dbo].[Operator]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Operator_MenuId]
    ON [dbo].[Operator]([MenuId] ASC) WITH (FILLFACTOR = 90);

