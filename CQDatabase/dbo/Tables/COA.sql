﻿CREATE TABLE [dbo].[COA] (
    [COAId]              INT            IDENTITY (1, 1) NOT NULL,
    [COACode]            NVARCHAR (255) NULL,
    [COA]                NVARCHAR (MAX) NULL,
    [StorageUnitBatchId] INT            NULL,
    [StorageUnitId]      INT            NULL,
    PRIMARY KEY CLUSTERED ([COAId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([StorageUnitBatchId]) REFERENCES [dbo].[StorageUnitBatch] ([StorageUnitBatchId]),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_COA_StorageUnitId]
    ON [dbo].[COA]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_COA_StorageUnitBatchId]
    ON [dbo].[COA]([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);

