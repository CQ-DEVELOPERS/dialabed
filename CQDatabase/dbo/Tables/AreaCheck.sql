﻿CREATE TABLE [dbo].[AreaCheck] (
    [AreaCheckId] INT IDENTITY (1, 1) NOT NULL,
    [AreaId]      INT NOT NULL,
    [WarehouseId] INT NULL,
    CONSTRAINT [PK_AreaCheck] PRIMARY KEY CLUSTERED ([AreaCheckId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AreaCheck_AreaCheck] FOREIGN KEY ([AreaCheckId]) REFERENCES [dbo].[Area] ([AreaId])
);

