﻿CREATE TABLE [dbo].[BayType] (
    [BayTypeId]   INT            IDENTITY (1, 1) NOT NULL,
    [BayTypeCode] NVARCHAR (20)  NOT NULL,
    [BayType]     NVARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([BayTypeId] ASC) WITH (FILLFACTOR = 90)
);

