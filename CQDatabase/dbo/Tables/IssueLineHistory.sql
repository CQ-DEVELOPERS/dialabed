﻿CREATE TABLE [dbo].[IssueLineHistory] (
    [IssueLineId]        INT             NULL,
    [IssueId]            INT             NULL,
    [OutboundLineId]     INT             NULL,
    [StorageUnitBatchId] INT             NULL,
    [StatusId]           INT             NULL,
    [OperatorId]         INT             NULL,
    [Quantity]           FLOAT (53)      NULL,
    [ConfirmedQuatity]   FLOAT (53)      NULL,
    [CommandType]        NVARCHAR (10)   NOT NULL,
    [InsertDate]         DATETIME        NOT NULL,
    [Weight]             FLOAT (53)      NULL,
    [ConfirmedWeight]    FLOAT (53)      NULL,
    [ParentIssueLineId]  INT             NULL,
    [DeliveredQuantity]  NUMERIC (13, 6) NULL,
    [SOHQuantity]        NUMERIC (13, 6) NULL
);

