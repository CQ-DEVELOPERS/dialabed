﻿CREATE TABLE [dbo].[AreaOperatorHistory] (
    [AreaId]      INT           NULL,
    [OperatorId]  INT           NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL
);

