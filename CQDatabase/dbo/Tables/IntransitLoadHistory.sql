﻿CREATE TABLE [dbo].[IntransitLoadHistory] (
    [IntransitLoadId]     INT            NULL,
    [WarehouseId]         INT            NULL,
    [StatusId]            INT            NULL,
    [DeliveryNoteNumber]  NVARCHAR (60)  NULL,
    [DeliveryDate]        DATETIME       NULL,
    [SealNumber]          NVARCHAR (100) NULL,
    [VehicleRegistration] NVARCHAR (100) NULL,
    [Route]               NVARCHAR (100) NULL,
    [DriverId]            INT            NULL,
    [Remarks]             NVARCHAR (MAX) NULL,
    [CreatedById]         INT            NULL,
    [ReceivedById]        INT            NULL,
    [CreateDate]          DATETIME       NULL,
    [LoadClosed]          DATETIME       NULL,
    [ReceivedDate]        DATETIME       NULL,
    [Offloaded]           DATETIME       NULL,
    [CommandType]         NVARCHAR (10)  NOT NULL,
    [InsertDate]          DATETIME       NOT NULL
);

