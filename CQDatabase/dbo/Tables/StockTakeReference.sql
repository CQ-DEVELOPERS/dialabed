﻿CREATE TABLE [dbo].[StockTakeReference] (
    [StockTakeReferenceId] INT           IDENTITY (1, 1) NOT NULL,
    [WarehouseId]          INT           NOT NULL,
    [StockTakeType]        NVARCHAR (50) NOT NULL,
    [StartDate]            DATETIME      NULL,
    [EndDate]              DATETIME      NULL,
    [OperatorId]           INT           NULL,
    [Reference]            NVARCHAR (50) NULL
);

