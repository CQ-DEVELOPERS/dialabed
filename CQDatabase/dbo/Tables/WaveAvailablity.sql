﻿CREATE TABLE [dbo].[WaveAvailablity] (
    [OutboundShipmentId]  INT             NULL,
    [IssueId]             INT             NULL,
    [OrderNumber]         NVARCHAR (30)   NULL,
    [PercentageAvailable] NUMERIC (13, 2) NULL,
    [PercentageDrawDown]  NUMERIC (13, 2) NULL,
    [BulkPallets]         INT             NULL,
    [SimilarSKUDI]        NVARCHAR (MAX)  NULL,
    [NumberOfLines]       INT             NULL
);

