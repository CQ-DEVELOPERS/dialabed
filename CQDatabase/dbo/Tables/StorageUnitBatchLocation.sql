﻿CREATE TABLE [dbo].[StorageUnitBatchLocation] (
    [StorageUnitBatchId] INT        NOT NULL,
    [LocationId]         INT        NOT NULL,
    [ActualQuantity]     FLOAT (53) CONSTRAINT [DF_StorageUnitBatchLocation_ActualQuantity] DEFAULT ((0)) NOT NULL,
    [AllocatedQuantity]  FLOAT (53) CONSTRAINT [DF_StorageUnitBatchLocation_AllocatedQuantity] DEFAULT ((0)) NOT NULL,
    [ReservedQuantity]   FLOAT (53) CONSTRAINT [DF_StorageUnitBatchLocation_ReservedQuantity] DEFAULT ((0)) NOT NULL,
    [NettWeight]         FLOAT (53) NULL,
    CONSTRAINT [SUBLocation_PK] PRIMARY KEY CLUSTERED ([StorageUnitBatchId] ASC, [LocationId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Location_SUBLocation_FK1] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [StorageUnitBatch_SUBLocation_FK1] FOREIGN KEY ([StorageUnitBatchId]) REFERENCES [dbo].[StorageUnitBatch] ([StorageUnitBatchId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitBatchLocation_LocationId]
    ON [dbo].[StorageUnitBatchLocation]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitBatchLocation_StorageUnitBatchId]
    ON [dbo].[StorageUnitBatchLocation]([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO


CREATE trigger [dbo].[tr_StorageUnitBatchLocation_u]
on [dbo].[StorageUnitBatchLocation]
for update
as
begin
  DECLARE @ExecStr varchar(50), @Qry nvarchar(255)

    CREATE TABLE #inputbuffer 
    (
      EventType nvarchar(30), 
      Parameters int, 
      EventInfo nvarchar(max)
    )

    SET @ExecStr = 'DBCC INPUTBUFFER(' + STR(@@SPID) + ')'

    INSERT INTO #inputbuffer 
    EXEC (@ExecStr)

    SET @Qry = (SELECT EventInfo FROM #inputbuffer)
	
	insert  StorageUnitBatchLocationTriggerLog
			(Query,
			LoginName,
			UserName,
			CurrentTime,
			StorageUnitBatchId,
			LocationId,
			ActualQuantity,
			ReservedQuantity,
			AllocatedQuantity)
    SELECT @Qry , 
     SYSTEM_USER , 
     USER AS UserName, 
     CURRENT_TIMESTAMP AS CurrentTime,
     u.storageunitbatchid,
     u.LocationId,
     u.ActualQuantity,
     u.ReservedQuantity,
     u.AllocatedQuantity
     from inserted u
     
     
end




GO
DISABLE TRIGGER [dbo].[tr_StorageUnitBatchLocation_u]
    ON [dbo].[StorageUnitBatchLocation];


GO
create trigger tr_StorageUnitBatchLocation_i
on StorageUnitBatchLocation
for insert
as
begin
  update l
     set Used = 1
    from inserted i
    join Location l on i.LocationId = l.LocationId
end

GO
DISABLE TRIGGER [dbo].[tr_StorageUnitBatchLocation_i]
    ON [dbo].[StorageUnitBatchLocation];

