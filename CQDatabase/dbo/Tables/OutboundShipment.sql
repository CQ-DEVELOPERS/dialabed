﻿CREATE TABLE [dbo].[OutboundShipment] (
    [OutboundShipmentId]  INT             IDENTITY (1, 1) NOT NULL,
    [StatusId]            INT             NOT NULL,
    [WarehouseId]         INT             NOT NULL,
    [LocationId]          INT             NULL,
    [ShipmentDate]        DATETIME        NOT NULL,
    [Remarks]             NVARCHAR (250)  NULL,
    [SealNumber]          NVARCHAR (30)   NULL,
    [VehicleRegistration] NVARCHAR (10)   NULL,
    [Route]               NVARCHAR (40)   NULL,
    [RouteId]             INT             NULL,
    [Weight]              FLOAT (53)      NULL,
    [Volume]              DECIMAL (13, 3) NULL,
    [FP]                  BIT             NULL,
    [SS]                  BIT             NULL,
    [LP]                  BIT             NULL,
    [FM]                  BIT             NULL,
    [MP]                  BIT             NULL,
    [PP]                  BIT             NULL,
    [AlternatePallet]     BIT             DEFAULT ((0)) NULL,
    [SubstitutePallet]    BIT             DEFAULT ((0)) NULL,
    [DespatchBay]         INT             NULL,
    [NumberOfOrders]      INT             NULL,
    [ContactListId]       INT             NULL,
    [TotalOrders]         INT             NULL,
    [IDN]                 BIT             NULL,
    [WaveId]              INT             NULL,
    [ReferenceNumber]     NVARCHAR (30)   NULL,
    [VehicleId]           INT             NULL,
    CONSTRAINT [OutboundShipment_PK] PRIMARY KEY CLUSTERED ([OutboundShipmentId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([RouteId]) REFERENCES [dbo].[Route] ([RouteId]),
    CONSTRAINT [FK__OutboundS__Conta__48923D61] FOREIGN KEY ([ContactListId]) REFERENCES [dbo].[ContactList] ([ContactListId]),
    CONSTRAINT [FK__OutboundS__Despa__658C0CBD] FOREIGN KEY ([DespatchBay]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [FK__OutboundS__Vehic__1C077F72] FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([VehicleId]),
    CONSTRAINT [FK__OutboundS__WaveI__2D9A891A] FOREIGN KEY ([WaveId]) REFERENCES [dbo].[Wave] ([WaveId]),
    CONSTRAINT [FK_OutboundShipment_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [FK_OutboundShipment_Warehouse] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipment_DespatchBay]
    ON [dbo].[OutboundShipment]([DespatchBay] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipment_WarehouseId]
    ON [dbo].[OutboundShipment]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipment_LocationId]
    ON [dbo].[OutboundShipment]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipment_StatusId]
    ON [dbo].[OutboundShipment]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipment_WaveId]
    ON [dbo].[OutboundShipment]([WaveId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipment_ContactListId]
    ON [dbo].[OutboundShipment]([ContactListId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipment_VehicleId]
    ON [dbo].[OutboundShipment]([VehicleId] ASC) WITH (FILLFACTOR = 90);

