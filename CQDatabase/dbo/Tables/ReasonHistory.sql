﻿CREATE TABLE [dbo].[ReasonHistory] (
    [ReasonId]    INT           NULL,
    [Reason]      NVARCHAR (50) NULL,
    [ReasonCode]  NVARCHAR (10) NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL
);

