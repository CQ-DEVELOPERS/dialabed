﻿CREATE TABLE [dbo].[BOMInstructionLine] (
    [BOMInstructionId] INT        NOT NULL,
    [BOMLineId]        INT        NOT NULL,
    [LineNumber]       INT        NOT NULL,
    [Quantity]         FLOAT (53) NOT NULL,
    CONSTRAINT [FK_BOMInstructionLine_BOMInstruction] FOREIGN KEY ([BOMInstructionId]) REFERENCES [dbo].[BOMInstruction] ([BOMInstructionId]),
    CONSTRAINT [FK_BOMInstructionLine_BOMLine] FOREIGN KEY ([BOMLineId]) REFERENCES [dbo].[BOMLine] ([BOMLineId])
);


GO
CREATE NONCLUSTERED INDEX [nci_BOMInstructionLine_BOMInstructionId]
    ON [dbo].[BOMInstructionLine]([BOMInstructionId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_BOMInstructionLine_BOMLineId]
    ON [dbo].[BOMInstructionLine]([BOMLineId] ASC) WITH (FILLFACTOR = 90);

