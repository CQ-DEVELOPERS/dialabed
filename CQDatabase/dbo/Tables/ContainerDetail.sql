﻿CREATE TABLE [dbo].[ContainerDetail] (
    [ContainerDetailId]  INT        IDENTITY (1, 1) NOT NULL,
    [ContainerHeaderId]  INT        NULL,
    [StorageUnitBatchId] INT        NULL,
    [Quantity]           FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([ContainerDetailId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ContainerHeaderId]) REFERENCES [dbo].[ContainerHeader] ([ContainerHeaderId]),
    FOREIGN KEY ([StorageUnitBatchId]) REFERENCES [dbo].[StorageUnitBatch] ([StorageUnitBatchId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ContainerDetail_StorageUnitBatchId]
    ON [dbo].[ContainerDetail]([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ContainerDetail_ContainerHeaderId]
    ON [dbo].[ContainerDetail]([ContainerHeaderId] ASC) WITH (FILLFACTOR = 90);

