﻿CREATE TABLE [dbo].[BoxLink] (
    [BoxLinkId]          INT           IDENTITY (1, 1) NOT NULL,
    [Barcode]            NVARCHAR (50) NULL,
    [DropSequence]       INT           NULL,
    [OutboundShipmentId] INT           NULL,
    [IssueId]            INT           NULL,
    [JobId]              INT           NULL,
    [InsertDate]         DATETIME      DEFAULT ([dbo].[ufn_Getdate]()) NULL,
    PRIMARY KEY CLUSTERED ([BoxLinkId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([IssueId]) REFERENCES [dbo].[Issue] ([IssueId]),
    FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([JobId]),
    FOREIGN KEY ([OutboundShipmentId]) REFERENCES [dbo].[OutboundShipment] ([OutboundShipmentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_BoxLink_JobId]
    ON [dbo].[BoxLink]([JobId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_BoxLink_IssueId]
    ON [dbo].[BoxLink]([IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_BoxLink_OutboundShipmentId]
    ON [dbo].[BoxLink]([OutboundShipmentId] ASC) WITH (FILLFACTOR = 90);

