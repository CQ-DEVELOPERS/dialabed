﻿CREATE TABLE [dbo].[InterfaceExtract] (
    [InterfaceExtractId] INT           IDENTITY (1, 1) NOT NULL,
    [RecordType]         VARCHAR (20)  NULL,
    [SequenceNumber]     CHAR (9)      NULL,
    [OrderNumber]        VARCHAR (20)  NOT NULL,
    [LineNumber]         INT           NULL,
    [RecordStatus]       CHAR (1)      NULL,
    [ExtractCounter]     INT           NULL,
    [ExtractedDate]      DATETIME      NOT NULL,
    [ProcessedDate]      DATETIME      NULL,
    [Data]               VARCHAR (500) NOT NULL
);

