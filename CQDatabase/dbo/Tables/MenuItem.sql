﻿CREATE TABLE [dbo].[MenuItem] (
    [MenuItemId]       INT            IDENTITY (1, 1) NOT NULL,
    [MenuId]           INT            NOT NULL,
    [MenuItem]         NVARCHAR (50)  NULL,
    [ToolTip]          NVARCHAR (255) NULL,
    [NavigateTo]       NVARCHAR (255) NULL,
    [ParentMenuItemId] INT            NULL,
    [OrderBy]          SMALLINT       NULL,
    PRIMARY KEY CLUSTERED ([MenuItemId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([MenuId]) REFERENCES [dbo].[Menu] ([MenuId]),
    FOREIGN KEY ([ParentMenuItemId]) REFERENCES [dbo].[MenuItem] ([MenuItemId])
);

