﻿CREATE TABLE [dbo].[InboundShipmentReceipt] (
    [InboundShipmentId] INT NOT NULL,
    [ReceiptId]         INT NOT NULL,
    CONSTRAINT [PK_InboundShipmentReceipt] PRIMARY KEY CLUSTERED ([InboundShipmentId] ASC, [ReceiptId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_InboundShipmentReceipt_InboundShipment] FOREIGN KEY ([InboundShipmentId]) REFERENCES [dbo].[InboundShipment] ([InboundShipmentId]),
    CONSTRAINT [FK_InboundShipmentReceipt_Receipt] FOREIGN KEY ([ReceiptId]) REFERENCES [dbo].[Receipt] ([ReceiptId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InboundShipmentReceipt_ReceiptId]
    ON [dbo].[InboundShipmentReceipt]([ReceiptId] ASC) WITH (FILLFACTOR = 90);

