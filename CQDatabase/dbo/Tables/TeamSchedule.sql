﻿CREATE TABLE [dbo].[TeamSchedule] (
    [TeamScheduleId]     INT             IDENTITY (1, 1) NOT NULL,
    [OutboundShipmentId] INT             NULL,
    [IssueId]            INT             NULL,
    [InboundShipmentId]  INT             NULL,
    [ReceiptId]          INT             NULL,
    [OperatorGroupId]    INT             NOT NULL,
    [InstructionTypeId]  INT             NULL,
    [Subject]            NVARCHAR (510)  NOT NULL,
    [PlannedStart]       DATETIME        NOT NULL,
    [PlannedEnd]         DATETIME        NOT NULL,
    [ActualStart]        DATETIME        NULL,
    [ActualEnd]          DATETIME        NULL,
    [LocationId]         INT             NULL,
    [RecurrenceRule]     NVARCHAR (1024) NULL,
    [RecurrenceParentID] INT             NULL,
    [Description]        NVARCHAR (MAX)  NULL,
    PRIMARY KEY CLUSTERED ([TeamScheduleId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([InboundShipmentId]) REFERENCES [dbo].[InboundShipment] ([InboundShipmentId]),
    FOREIGN KEY ([InstructionTypeId]) REFERENCES [dbo].[InstructionType] ([InstructionTypeId]),
    FOREIGN KEY ([IssueId]) REFERENCES [dbo].[Issue] ([IssueId]),
    FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId]),
    FOREIGN KEY ([OutboundShipmentId]) REFERENCES [dbo].[OutboundShipment] ([OutboundShipmentId]),
    FOREIGN KEY ([ReceiptId]) REFERENCES [dbo].[Receipt] ([ReceiptId])
);


GO
CREATE NONCLUSTERED INDEX [nci_TeamSchedule_InstructionTypeId]
    ON [dbo].[TeamSchedule]([InstructionTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TeamSchedule_OperatorGroupId]
    ON [dbo].[TeamSchedule]([OperatorGroupId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TeamSchedule_ReceiptId]
    ON [dbo].[TeamSchedule]([ReceiptId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TeamSchedule_IssueId]
    ON [dbo].[TeamSchedule]([IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TeamSchedule_OutboundShipmentId]
    ON [dbo].[TeamSchedule]([OutboundShipmentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TeamSchedule_InboundShipmentId]
    ON [dbo].[TeamSchedule]([InboundShipmentId] ASC) WITH (FILLFACTOR = 90);

