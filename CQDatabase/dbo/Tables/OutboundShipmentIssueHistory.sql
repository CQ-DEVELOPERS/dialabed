﻿CREATE TABLE [dbo].[OutboundShipmentIssueHistory] (
    [OutboundShipmentId] INT           NULL,
    [IssueId]            INT           NULL,
    [DropSequence]       INT           NULL,
    [CommandType]        NVARCHAR (10) NOT NULL,
    [InsertDate]         DATETIME      NOT NULL
);

