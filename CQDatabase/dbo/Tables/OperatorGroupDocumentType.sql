﻿CREATE TABLE [dbo].[OperatorGroupDocumentType] (
    [OperatorGroupId]        INT NULL,
    [OutboundDocumentTypeId] INT NULL,
    [PriorityId]             INT NULL,
    FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId]),
    FOREIGN KEY ([OutboundDocumentTypeId]) REFERENCES [dbo].[OutboundDocumentType] ([OutboundDocumentTypeId]),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorGroupDocumentType_OutboundDocumentTypeId]
    ON [dbo].[OperatorGroupDocumentType]([OutboundDocumentTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorGroupDocumentType_OperatorGroupId]
    ON [dbo].[OperatorGroupDocumentType]([OperatorGroupId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorGroupDocumentType_PriorityId]
    ON [dbo].[OperatorGroupDocumentType]([PriorityId] ASC) WITH (FILLFACTOR = 90);

