﻿CREATE TABLE [dbo].[Location] (
    [LocationId]        INT              IDENTITY (1, 1) NOT NULL,
    [LocationTypeId]    INT              NOT NULL,
    [Location]          NVARCHAR (15)    NOT NULL,
    [Ailse]             NVARCHAR (10)    NULL,
    [Column]            NVARCHAR (10)    NULL,
    [Level]             NVARCHAR (10)    NULL,
    [PalletQuantity]    FLOAT (53)       NULL,
    [SecurityCode]      NVARCHAR (50)    NULL,
    [RelativeValue]     NUMERIC (13, 3)  NULL,
    [NumberOfSUB]       INT              NOT NULL,
    [StocktakeInd]      BIT              NOT NULL,
    [ActiveBinning]     BIT              NOT NULL,
    [ActivePicking]     BIT              NOT NULL,
    [Used]              BIT              NOT NULL,
    [Latitude]          DECIMAL (16, 13) NULL,
    [Longitude]         DECIMAL (16, 13) NULL,
    [Direction]         NVARCHAR (10)    NULL,
    [Height]            NUMERIC (13, 6)  NULL,
    [Length]            NUMERIC (13, 6)  NULL,
    [Width]             NUMERIC (13, 6)  NULL,
    [Volume]            NUMERIC (13, 6)  NULL,
    [Weight]            NUMERIC (13, 6)  NULL,
    [NumberOfSU]        INT              NULL,
    [TrackLPN]          BIT              NULL,
    [FreightForwarding] NVARCHAR (100)   NULL,
    CONSTRAINT [Location_PK] PRIMARY KEY CLUSTERED ([LocationId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [LocationType_Location_FK1] FOREIGN KEY ([LocationTypeId]) REFERENCES [dbo].[LocationType] ([LocationTypeId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Location_LocationTypeId]
    ON [dbo].[Location]([LocationTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_ActiveBinning]
    ON [dbo].[Location]([ActiveBinning] ASC)
    INCLUDE([LocationId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_ActivePicking]
    ON [dbo].[Location]([ActivePicking] ASC)
    INCLUDE([LocationId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_StocktakeInd]
    ON [dbo].[Location]([StocktakeInd] ASC)
    INCLUDE([LocationId], [Location]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_Location]
    ON [dbo].[Location]([Location] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_StocktakeInd1]
    ON [dbo].[Location]([StocktakeInd] ASC)
    INCLUDE([LocationId], [Location], [Ailse], [Column], [Level]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_StocktakeInd_ActiveBinning_Used_LocationId]
    ON [dbo].[Location]([StocktakeInd] ASC, [ActiveBinning] ASC, [Used] ASC, [LocationId] ASC)
    INCLUDE([RelativeValue]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_Used]
    ON [dbo].[Location]([Used] ASC)
    INCLUDE([LocationId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_LocationTypeId]
    ON [dbo].[Location]([LocationTypeId] ASC)
    INCLUDE([LocationId], [Location]) WITH (FILLFACTOR = 90);


GO
create trigger tr_Location_i
on Location
for insert
as
begin
  insert LocationOperatorGroup
        (LocationId,
         OperatorGroupId,
         OrderBy)
  select l.LocationId,
         og.OperatorGroupId,
         0
    from inserted       l,
         OperatorGroup og
end
