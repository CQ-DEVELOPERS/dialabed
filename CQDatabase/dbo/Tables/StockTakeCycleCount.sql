﻿CREATE TABLE [dbo].[StockTakeCycleCount] (
    [StockTakeCycleCountId]   INT      IDENTITY (1, 1) NOT NULL,
    [WarehouseId]             INT      NULL,
    [ProductCategory]         CHAR (1) NOT NULL,
    [CountsPerYear]           INT      NOT NULL,
    [StockTakeAlreadyCounted] INT      NULL,
    [RequiredDailyCount]      INT      NULL,
    [StorageUnitTotal]        INT      NULL,
    PRIMARY KEY CLUSTERED ([StockTakeCycleCountId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StockTakeCycleCount_WarehouseId]
    ON [dbo].[StockTakeCycleCount]([WarehouseId] ASC) WITH (FILLFACTOR = 90);

