﻿CREATE TABLE [dbo].[Wave] (
    [WaveId]         INT           IDENTITY (1, 1) NOT NULL,
    [Wave]           NVARCHAR (50) NOT NULL,
    [CreateDate]     DATETIME      CONSTRAINT [DF__Wave__CreateDate__1C6FFD18] DEFAULT (getdate()) NULL,
    [ReleasedDate]   DATETIME      NULL,
    [StatusId]       INT           NULL,
    [WarehouseId]    INT           NULL,
    [CompleteDate]   DATETIME      NULL,
    [ReleasedOrders] DATETIME      NULL,
    [PutawayDate]    DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([WaveId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Wave_StatusId]
    ON [dbo].[Wave]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Wave_WarehouseId]
    ON [dbo].[Wave]([WarehouseId] ASC) WITH (FILLFACTOR = 90);

