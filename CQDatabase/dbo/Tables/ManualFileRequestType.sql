﻿CREATE TABLE [dbo].[ManualFileRequestType] (
    [ManualFileRequestTypeId] INT           IDENTITY (1, 1) NOT NULL,
    [Description]             NVARCHAR (50) NULL,
    [DocumentTypeCode]        NVARCHAR (10) NULL,
    CONSTRAINT [PK_ManualFileRequestType] PRIMARY KEY CLUSTERED ([ManualFileRequestTypeId] ASC) WITH (FILLFACTOR = 90)
);

