﻿CREATE TABLE [dbo].[SKUInfo] (
    [SKUId]            INT        NOT NULL,
    [WarehouseId]      INT        NOT NULL,
    [Quantity]         FLOAT (53) NOT NULL,
    [AlternatePallet]  INT        NULL,
    [SubstitutePallet] INT        NULL,
    FOREIGN KEY ([SKUId]) REFERENCES [dbo].[SKU] ([SKUId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_SKUInfo_WarehouseId]
    ON [dbo].[SKUInfo]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SKUInfo_SKUId]
    ON [dbo].[SKUInfo]([SKUId] ASC) WITH (FILLFACTOR = 90);

