﻿CREATE TABLE [dbo].[OperatorGroupMenuItemHistory] (
    [OperatorGroupId] INT           NULL,
    [MenuItemId]      INT           NULL,
    [Access]          BIT           NULL,
    [CommandType]     NVARCHAR (10) NOT NULL,
    [InsertDate]      DATETIME      NOT NULL
);

