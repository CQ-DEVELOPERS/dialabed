﻿CREATE TABLE [dbo].[PurchaseOrderHeader] (
    [PurchaseOrderHeaderId] INT            IDENTITY (1, 1) NOT NULL,
    [WarehouseId]           INT            NULL,
    [PurchaseOrder]         NVARCHAR (30)  NULL,
    [DocumentTypeId]        INT            NULL,
    [ExternalCompanyId]     INT            NULL,
    [PriorityId]            INT            NULL,
    [DeliveryNoteNumber]    NVARCHAR (30)  NULL,
    [DeliveryDate]          DATETIME       NULL,
    [Remarks]               NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([PurchaseOrderHeaderId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[InboundDocumentType] ([InboundDocumentTypeId]),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_PurchaseOrderHeader_DocumentTypeId]
    ON [dbo].[PurchaseOrderHeader]([DocumentTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_PurchaseOrderHeader_WarehouseId]
    ON [dbo].[PurchaseOrderHeader]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_PurchaseOrderHeader_ExternalCompanyId]
    ON [dbo].[PurchaseOrderHeader]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_PurchaseOrderHeader_PriorityId]
    ON [dbo].[PurchaseOrderHeader]([PriorityId] ASC) WITH (FILLFACTOR = 90);

