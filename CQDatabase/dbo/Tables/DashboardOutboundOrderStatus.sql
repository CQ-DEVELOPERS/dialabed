﻿CREATE TABLE [dbo].[DashboardOutboundOrderStatus] (
    [WarehouseId]       INT           NULL,
    [ExternalCompanyId] INT           NULL,
    [PrincipalId]       INT           NULL,
    [Legend]            NVARCHAR (50) NULL,
    [Value]             INT           NULL,
    [Units]             INT           NULL
);

