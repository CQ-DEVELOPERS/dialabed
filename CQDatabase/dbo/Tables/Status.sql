﻿CREATE TABLE [dbo].[Status] (
    [StatusId]        INT           IDENTITY (1, 1) NOT NULL,
    [Status]          NVARCHAR (50) NOT NULL,
    [StatusCode]      NVARCHAR (10) NOT NULL,
    [Type]            CHAR (2)      NOT NULL,
    [OrderBy]         INT           NULL,
    [GrafanaSequence] INT           NULL,
    CONSTRAINT [Status_PK] PRIMARY KEY CLUSTERED ([StatusId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_Status_StatusCode]
    ON [dbo].[Status]([StatusCode] ASC) WITH (FILLFACTOR = 90);

