﻿CREATE TABLE [dbo].[AreaSequence] (
    [AreaSequenceId]      INT           IDENTITY (1, 1) NOT NULL,
    [PickAreaId]          INT           NOT NULL,
    [StoreAreaId]         INT           NOT NULL,
    [AdjustmentType]      NVARCHAR (10) NOT NULL,
    [ReasonCode]          VARCHAR (10)  NULL,
    [InstructionTypeCode] NVARCHAR (10) NULL,
    [StoreAdjustmentType] NVARCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([AreaSequenceId] ASC) WITH (FILLFACTOR = 90)
);

