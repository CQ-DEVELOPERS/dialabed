﻿CREATE TABLE [dbo].[AccessLog] (
    [AccessLogId] INT            IDENTITY (1, 1) NOT NULL,
    [AccessLog]   NVARCHAR (MAX) NULL,
    [InsertDate]  DATETIME       CONSTRAINT [DF_AccessLog_InsertDate] DEFAULT (getdate()) NOT NULL
);

