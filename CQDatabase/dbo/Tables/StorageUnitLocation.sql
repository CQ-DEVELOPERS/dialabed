﻿CREATE TABLE [dbo].[StorageUnitLocation] (
    [StorageUnitId]    INT        NOT NULL,
    [LocationId]       INT        NOT NULL,
    [MinimumQuantity]  FLOAT (53) NULL,
    [HandlingQuantity] FLOAT (53) NULL,
    [MaximumQuantity]  FLOAT (53) NULL,
    CONSTRAINT [StorageUnitLocation_PK] PRIMARY KEY CLUSTERED ([StorageUnitId] ASC, [LocationId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Location_StorageUnitLocation_FK1] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [StorageUnit_StorageUnitLocation_FK1] FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitLocation_StorageUnitId]
    ON [dbo].[StorageUnitLocation]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitLocation_LocationId]
    ON [dbo].[StorageUnitLocation]([LocationId] ASC) WITH (FILLFACTOR = 90);

