﻿CREATE TABLE [dbo].[DivisionHistory] (
    [DivisionId]   INT           IDENTITY (1, 1) NOT NULL,
    [DivisionCode] NVARCHAR (10) NULL,
    [Division]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([DivisionId] ASC) WITH (FILLFACTOR = 90)
);

