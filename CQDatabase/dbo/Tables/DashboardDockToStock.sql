﻿CREATE TABLE [dbo].[DashboardDockToStock] (
    [WarehouseId]         INT           NULL,
    [InboundDocumentType] NVARCHAR (60) NOT NULL,
    [Lines]               INT           NULL,
    [Units]               FLOAT (53)    NULL,
    [Minutes]             INT           NULL,
    [KPI]                 INT           NOT NULL
);

