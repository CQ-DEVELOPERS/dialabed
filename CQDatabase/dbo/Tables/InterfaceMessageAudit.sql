﻿CREATE TABLE [dbo].[InterfaceMessageAudit] (
    [InterfaceMessageId]   INT            NOT NULL,
    [InterfaceMessageCode] NVARCHAR (50)  NULL,
    [InterfaceMessage]     NVARCHAR (MAX) NULL,
    [InterfaceId]          INT            NULL,
    [InterfaceTable]       [sysname]      NULL,
    [Status]               NVARCHAR (20)  NULL,
    [OrderNumber]          NVARCHAR (30)  NULL,
    [CreateDate]           DATETIME       NULL,
    [ProcessedDate]        DATETIME       NULL,
    [InsertDate]           DATETIME       NULL
);

