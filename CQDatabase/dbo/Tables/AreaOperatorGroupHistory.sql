﻿CREATE TABLE [dbo].[AreaOperatorGroupHistory] (
    [AreaId]          INT           NULL,
    [OperatorGroupId] INT           NULL,
    [CommandType]     NVARCHAR (10) NOT NULL,
    [InsertDate]      DATETIME      NOT NULL
);

