﻿CREATE TABLE [dbo].[OperatorPerformance] (
    [OperatorId]        INT        NULL,
    [InstructionTypeId] INT        NULL,
    [EndDate]           DATETIME   NULL,
    [ActivityStatus]    CHAR (1)   NULL,
    [PickingRate]       INT        NULL,
    [Units]             INT        NULL,
    [Weight]            FLOAT (53) NULL,
    [Instructions]      INT        NULL,
    [Orders]            INT        NULL,
    [OrderLines]        INT        NULL,
    [Jobs]              INT        NULL,
    [ActiveTime]        INT        NULL,
    [DwellTime]         INT        NULL,
    [WarehouseId]       INT        NULL,
    CONSTRAINT [FK__OperatorP__Opera__1EC94E23] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorPerformance_OperatorId]
    ON [dbo].[OperatorPerformance]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_OperatorPerformance_OperatorId_EndDate]
    ON [dbo].[OperatorPerformance]([OperatorId] ASC, [EndDate] ASC)
    INCLUDE([Units]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_OperatorPerformance_OperatorId_EndDate1]
    ON [dbo].[OperatorPerformance]([OperatorId] ASC, [EndDate] ASC) WITH (FILLFACTOR = 90);

