﻿CREATE TABLE [dbo].[ShipmentDetail] (
    [ShipmentDetailId] INT        IDENTITY (1, 1) NOT NULL,
    [ShipmentHeaderId] INT        NULL,
    [CallOffHeaderId]  INT        NULL,
    [CallOffDetailId]  INT        NULL,
    [StorageUnitId]    INT        NULL,
    [BatchId]          INT        NULL,
    [RequiredQuantity] FLOAT (53) NULL,
    [StatusId]         INT        NULL,
    PRIMARY KEY CLUSTERED ([ShipmentDetailId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CallOffDetailId]) REFERENCES [dbo].[CallOffDetail] ([CallOffDetailId]),
    FOREIGN KEY ([CallOffHeaderId]) REFERENCES [dbo].[CallOffHeader] ([CallOffHeaderId]),
    FOREIGN KEY ([ShipmentHeaderId]) REFERENCES [dbo].[ShipmentHeader] ([ShipmentHeaderId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentDetail_ShipmentHeaderId]
    ON [dbo].[ShipmentDetail]([ShipmentHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentDetail_CallOffDetailId]
    ON [dbo].[ShipmentDetail]([CallOffDetailId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentDetail_CallOffHeaderId]
    ON [dbo].[ShipmentDetail]([CallOffHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentDetail_StatusId]
    ON [dbo].[ShipmentDetail]([StatusId] ASC) WITH (FILLFACTOR = 90);

