﻿CREATE TABLE [dbo].[InterfaceExportShipmentDelivery] (
    [InterfaceExportShipmentDeliveryId] INT            IDENTITY (1, 1) NOT NULL,
    [InterfaceExportShipmentId]         INT            NULL,
    [IssueId]                           INT            NULL,
    [ForeignKey]                        NVARCHAR (40)  NULL,
    [WaybillNo]                         NVARCHAR (50)  NULL,
    [DropSequence]                      INT            NULL,
    [DeliveryDate]                      DATETIME       NULL,
    [DeliveryMethod]                    NVARCHAR (50)  NULL,
    [DeliveryNoteNumber]                NVARCHAR (50)  NULL,
    [SealNumber]                        NVARCHAR (30)  NULL,
    [WarehouseCode]                     NVARCHAR (30)  NULL,
    [ConsigneeName]                     NVARCHAR (50)  NULL,
    [ConsigneeCode]                     NVARCHAR (10)  NULL,
    [ConsigneeType]                     NVARCHAR (50)  NULL,
    [Street]                            NVARCHAR (100) NULL,
    [Suburb]                            NVARCHAR (100) NULL,
    [Town]                              NVARCHAR (100) NULL,
    [Country]                           NVARCHAR (100) NULL,
    [PostalCode]                        NVARCHAR (20)  NULL,
    [ContactName]                       NVARCHAR (50)  NULL,
    [Telephone]                         NVARCHAR (20)  NULL,
    [EMail]                             NVARCHAR (30)  NULL,
    [Comments]                          NVARCHAR (255) NULL,
    [TrustedDelivery]                   CHAR (1)       NULL,
    [ParcelDelivery]                    CHAR (1)       NULL,
    [DespatchDate]                      DATETIME       NULL,
    [PrincipalCode]                     NVARCHAR (30)  NULL,
    [ProcessedDate]                     DATETIME       NULL,
    [RecordStatus]                      CHAR (1)       NULL,
    [RecordType]                        NVARCHAR (10)  NULL,
    [InvoiceNumber]                     NVARCHAR (30)  NULL,
    [WaveId]                            INT            NULL,
    [JobId]                             INT            NULL,
    CONSTRAINT [PK_InterfaceExportShipmentDelivery] PRIMARY KEY CLUSTERED ([InterfaceExportShipmentDeliveryId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_InterfaceExportShipmentDelivery_InterfaceExportShipment] FOREIGN KEY ([InterfaceExportShipmentId]) REFERENCES [dbo].[InterfaceExportShipment] ([InterfaceExportShipmentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportShipmentDelivery_InterfaceExportShipmentId]
    ON [dbo].[InterfaceExportShipmentDelivery]([InterfaceExportShipmentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceExportShipmentDelivery_IssueId]
    ON [dbo].[InterfaceExportShipmentDelivery]([IssueId] ASC)
    INCLUDE([InterfaceExportShipmentDeliveryId], [JobId]) WITH (FILLFACTOR = 90);

