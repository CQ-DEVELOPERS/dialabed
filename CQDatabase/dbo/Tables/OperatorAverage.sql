﻿CREATE TABLE [dbo].[OperatorAverage] (
    [OperatorGroupId]   INT        NULL,
    [InstructionTypeId] INT        NULL,
    [EndDate]           DATETIME   NULL,
    [Units]             INT        NULL,
    [Weight]            FLOAT (53) NULL,
    [Instructions]      INT        NULL,
    [Orders]            INT        NULL,
    [OrderLines]        INT        NULL,
    [Jobs]              INT        NULL,
    [ActiveTime]        INT        NULL,
    [DwellTime]         INT        NULL,
    [WarehouseId]       INT        NULL,
    FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorAverage_OperatorGroupId]
    ON [dbo].[OperatorAverage]([OperatorGroupId] ASC) WITH (FILLFACTOR = 90);

