﻿CREATE TABLE [dbo].[InterfaceImportPOHeader] (
    [InterfaceImportPOHeaderId] INT            IDENTITY (1, 1) NOT NULL,
    [PrimaryKey]                NVARCHAR (30)  NULL,
    [OrderNumber]               NVARCHAR (30)  NULL,
    [RecordType]                CHAR (3)       DEFAULT ('PUR') NOT NULL,
    [RecordStatus]              CHAR (1)       DEFAULT ('N') NOT NULL,
    [SupplierCode]              NVARCHAR (30)  NULL,
    [Supplier]                  NVARCHAR (255) NULL,
    [Address]                   NVARCHAR (255) NULL,
    [FromWarehouseCode]         NVARCHAR (30)  NULL,
    [ToWarehouseCode]           NVARCHAR (30)  NULL,
    [DeliveryNoteNumber]        NVARCHAR (30)  NULL,
    [ContainerNumber]           NVARCHAR (30)  NULL,
    [SealNumber]                NVARCHAR (30)  NULL,
    [DeliveryDate]              DATETIME       NULL,
    [Remarks]                   NVARCHAR (255) NULL,
    [NumberOfLines]             INT            NULL,
    [Additional1]               NVARCHAR (255) NULL,
    [Additional2]               NVARCHAR (255) NULL,
    [Additional3]               NVARCHAR (255) NULL,
    [Additional4]               NVARCHAR (255) NULL,
    [Additional5]               NVARCHAR (255) NULL,
    [ProcessedDate]             DATETIME       NULL,
    [Additional6]               NVARCHAR (255) NULL,
    [Additional7]               NVARCHAR (255) NULL,
    [Additional8]               NVARCHAR (255) NULL,
    [Additional9]               NVARCHAR (255) NULL,
    [Additional10]              NVARCHAR (255) NULL,
    [InsertDate]                DATETIME       CONSTRAINT [DF_InterfaceImportPOHeader_InsertDate] DEFAULT (getdate()) NULL,
    [PrincipalCode]             NVARCHAR (30)  NULL,
    PRIMARY KEY CLUSTERED ([InterfaceImportPOHeaderId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportPOHeader]
    ON [dbo].[InterfaceImportPOHeader]([RecordStatus] ASC, [ProcessedDate] ASC, [FromWarehouseCode] ASC, [ToWarehouseCode] ASC, [PrimaryKey] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportPOHeader_Status_Dates]
    ON [dbo].[InterfaceImportPOHeader]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

