﻿CREATE TABLE [dbo].[InterfaceBOM] (
    [InterfaceBOMId]    INT            IDENTITY (1, 1) NOT NULL,
    [ParentProductCode] NVARCHAR (50)  NOT NULL,
    [OrderDescription]  NVARCHAR (255) NULL,
    [CreateDate]        NVARCHAR (50)  NULL,
    [PlannedDate]       NVARCHAR (50)  NULL,
    [Revision]          NVARCHAR (50)  NULL,
    [BuildQuantity]     FLOAT (53)     NULL,
    [Units]             NVARCHAR (50)  NULL,
    [Location]          NVARCHAR (50)  NULL,
    [ProcessedDate]     DATETIME       NULL,
    [RecordStatus]      CHAR (1)       DEFAULT ('N') NULL,
    PRIMARY KEY CLUSTERED ([InterfaceBOMId] ASC) WITH (FILLFACTOR = 90)
);

