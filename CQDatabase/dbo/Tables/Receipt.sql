﻿CREATE TABLE [dbo].[Receipt] (
    [ReceiptId]             INT            IDENTITY (1, 1) NOT NULL,
    [InboundDocumentId]     INT            NOT NULL,
    [PriorityId]            INT            NOT NULL,
    [WarehouseId]           INT            NOT NULL,
    [LocationId]            INT            NULL,
    [StatusId]              INT            NOT NULL,
    [OperatorId]            INT            NULL,
    [DeliveryNoteNumber]    NVARCHAR (30)  NULL,
    [DeliveryDate]          DATETIME       NULL,
    [SealNumber]            NVARCHAR (30)  NULL,
    [VehicleRegistration]   NVARCHAR (10)  NULL,
    [Remarks]               NVARCHAR (250) NULL,
    [CreditAdviceIndicator] BIT            NULL,
    [Delivery]              INT            CONSTRAINT [DF_Receipt_Delivery] DEFAULT ((1)) NOT NULL,
    [AllowPalletise]        BIT            NULL,
    [Interfaced]            BIT            NULL,
    [StagingLocationId]     INT            NULL,
    [NumberOfLines]         INT            NULL,
    [ReceivingCompleteDate] DATETIME       NULL,
    [ParentReceiptId]       INT            NULL,
    [GRN]                   NVARCHAR (30)  NULL,
    [PlannedDeliveryDate]   DATETIME       NULL,
    [ShippingAgentId]       INT            NULL,
    [ContainerNumber]       NVARCHAR (50)  NULL,
    [AdditionalText1]       NVARCHAR (255) NULL,
    [AdditionalText2]       NVARCHAR (255) NULL,
    [BOE]                   NVARCHAR (255) NULL,
    [Incoterms]             NVARCHAR (50)  NULL,
    [ReceiptConfirmed]      DATETIME       NULL,
    [ReceivingStarted]      DATETIME       NULL,
    [VehicleId]             INT            NULL,
    [ParentIssueId]         INT            NULL,
    [VehicleTypeId]         INT            NULL,
    [Locked]                BIT            CONSTRAINT [DF_Receipt_Locked] DEFAULT ((0)) NULL,
    CONSTRAINT [Receipt_PK] PRIMARY KEY CLUSTERED ([ReceiptId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__Receipt__Shippin__6E3FE96B] FOREIGN KEY ([ShippingAgentId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    CONSTRAINT [FK__Receipt__Vehicle__1EE3EC1D] FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([VehicleId]),
    CONSTRAINT [InboundDocument_Receipt_FK1] FOREIGN KEY ([InboundDocumentId]) REFERENCES [dbo].[InboundDocument] ([InboundDocumentId]),
    CONSTRAINT [Location_Receipt_FK1] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [Operators_Receipt_FK1] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [Priority_Receipt_FK1] FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    CONSTRAINT [Status_Receipt_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [Warehouse_Receipt_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_PriorityId]
    ON [dbo].[Receipt]([PriorityId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_OperatorId]
    ON [dbo].[Receipt]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_InboundDocumentId]
    ON [dbo].[Receipt]([InboundDocumentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_WarehouseId]
    ON [dbo].[Receipt]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_StatusId]
    ON [dbo].[Receipt]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_LocationId]
    ON [dbo].[Receipt]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_NumberOfLines]
    ON [dbo].[Receipt]([NumberOfLines] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_VehicleId]
    ON [dbo].[Receipt]([VehicleId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Receipt_ShippingAgentId]
    ON [dbo].[Receipt]([ShippingAgentId] ASC) WITH (FILLFACTOR = 90);

