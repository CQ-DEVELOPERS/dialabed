﻿CREATE TABLE [dbo].[ShipmentHistory] (
    [ShipmentId]          INT            NULL,
    [StatusId]            INT            NULL,
    [WarehouseId]         INT            NULL,
    [LocationId]          INT            NULL,
    [ShipmentDate]        DATETIME       NULL,
    [Remarks]             NVARCHAR (250) NULL,
    [SealNumber]          NVARCHAR (30)  NULL,
    [VehicleRegistration] NVARCHAR (10)  NULL,
    [Route]               NVARCHAR (10)  NULL,
    [CommandType]         NVARCHAR (10)  NOT NULL,
    [InsertDate]          DATETIME       NOT NULL
);

