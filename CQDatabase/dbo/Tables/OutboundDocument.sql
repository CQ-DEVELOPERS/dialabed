﻿CREATE TABLE [dbo].[OutboundDocument] (
    [OutboundDocumentId]       INT            IDENTITY (1, 1) NOT NULL,
    [OutboundDocumentTypeId]   INT            NOT NULL,
    [ExternalCompanyId]        INT            NOT NULL,
    [StatusId]                 INT            NOT NULL,
    [WarehouseId]              INT            NOT NULL,
    [OrderNumber]              NVARCHAR (30)  NOT NULL,
    [DeliveryDate]             DATETIME       NOT NULL,
    [CreateDate]               DATETIME       NOT NULL,
    [ModifiedDate]             DATETIME       NULL,
    [ReferenceNumber]          NVARCHAR (30)  NULL,
    [FromLocation]             NVARCHAR (10)  NULL,
    [ToLocation]               NVARCHAR (10)  NULL,
    [StorageUnitId]            INT            NULL,
    [BatchId]                  INT            NULL,
    [Quantity]                 FLOAT (53)     NULL,
    [PrincipalId]              INT            NULL,
    [OperatorId]               INT            NULL,
    [ExternalOrderNumber]      NVARCHAR (30)  NULL,
    [DirectDeliveryCustomerId] INT            NULL,
    [EmployeeCode]             NVARCHAR (50)  NULL,
    [Collector]                NVARCHAR (50)  NULL,
    [AdditionalText1]          NVARCHAR (255) NULL,
    [VendorCode]               NVARCHAR (30)  NULL,
    [CustomerOrderNumber]      NVARCHAR (30)  NULL,
    [AdditionalText2]          NVARCHAR (255) NULL,
    [BOE]                      NVARCHAR (255) NULL,
    [Address1]                 NVARCHAR (255) NULL,
    CONSTRAINT [OutboundDocument_PK] PRIMARY KEY CLUSTERED ([OutboundDocumentId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([OutboundDocumentTypeId]) REFERENCES [dbo].[OutboundDocumentType] ([OutboundDocumentTypeId]),
    CONSTRAINT [ExternalCompany_OutboundDocument_FK1] FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    CONSTRAINT [FK__OutboundD__Batch__70069F9E] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [FK__OutboundD__Opera__7244B922] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [FK__OutboundD__Princ__63CBB142] FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId]),
    CONSTRAINT [FK__OutboundD__Stora__6F127B65] FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId]),
    CONSTRAINT [Status_OutboundDocument_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [Warehouse_OutboundDocument_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_WarehouseId]
    ON [dbo].[OutboundDocument]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_ExternalCompanyId]
    ON [dbo].[OutboundDocument]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_StatusId]
    ON [dbo].[OutboundDocument]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument]
    ON [dbo].[OutboundDocument]([WarehouseId] ASC, [OutboundDocumentTypeId] ASC, [OutboundDocumentId] ASC, [CreateDate] ASC, [OrderNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_ReferenceNumber]
    ON [dbo].[OutboundDocument]([ReferenceNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_DirectDeliveryCustomerId]
    ON [dbo].[OutboundDocument]([DirectDeliveryCustomerId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_BatchId]
    ON [dbo].[OutboundDocument]([BatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_StorageUnitId]
    ON [dbo].[OutboundDocument]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_OperatorId]
    ON [dbo].[OutboundDocument]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocument_PrincipalId]
    ON [dbo].[OutboundDocument]([PrincipalId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_OutboundDocument_WarehouseId]
    ON [dbo].[OutboundDocument]([WarehouseId] ASC)
    INCLUDE([OutboundDocumentId], [OutboundDocumentTypeId], [ExternalCompanyId], [OrderNumber], [CreateDate], [PrincipalId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_OutboundDocument_OrderNumber]
    ON [dbo].[OutboundDocument]([OrderNumber] ASC) WITH (FILLFACTOR = 90);

