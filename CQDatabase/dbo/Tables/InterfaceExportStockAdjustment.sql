﻿CREATE TABLE [dbo].[InterfaceExportStockAdjustment] (
    [InterfaceExportStockAdjustmentId] INT            IDENTITY (1, 1) NOT NULL,
    [RecordType]                       CHAR (3)       DEFAULT ('ADJ') NOT NULL,
    [RecordStatus]                     CHAR (1)       DEFAULT ('N') NOT NULL,
    [ProductCode]                      NVARCHAR (30)  NOT NULL,
    [SKUCode]                          NVARCHAR (10)  NULL,
    [Batch]                            NVARCHAR (50)  NULL,
    [Quantity]                         FLOAT (53)     NULL,
    [Weight]                           FLOAT (53)     NULL,
    [Additional1]                      NVARCHAR (255) NULL,
    [Additional2]                      NVARCHAR (255) NULL,
    [Additional3]                      NVARCHAR (255) NULL,
    [Additional4]                      NVARCHAR (255) NULL,
    [Additional5]                      NVARCHAR (255) NULL,
    [ProcessedDate]                    DATETIME       NULL,
    [InsertDate]                       DATETIME       CONSTRAINT [DF_InterfaceExportStockAdjustment_InsertDate] DEFAULT (getdate()) NULL,
    [Product]                          NVARCHAR (255) NULL,
    [PrincipalCode]                    NVARCHAR (30)  NULL,
    [ReceiptId]                        INT            NULL,
    [CreatedBy]                        INT            NULL,
    [AuthorisedBy]                     INT            NULL,
    [AuthorisedDate]                   DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([InterfaceExportStockAdjustmentId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportStockAdjustment_Status_Dates]
    ON [dbo].[InterfaceExportStockAdjustment]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

