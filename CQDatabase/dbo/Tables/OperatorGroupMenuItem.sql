﻿CREATE TABLE [dbo].[OperatorGroupMenuItem] (
    [OperatorGroupId] INT NOT NULL,
    [MenuItemId]      INT NOT NULL,
    [Access]          BIT DEFAULT ((0)) NULL,
    CONSTRAINT [OperatorGroupMenuItem_PK] PRIMARY KEY CLUSTERED ([OperatorGroupId] ASC, [MenuItemId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([MenuItemId]) REFERENCES [dbo].[MenuItem] ([MenuItemId]),
    FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId])
);

