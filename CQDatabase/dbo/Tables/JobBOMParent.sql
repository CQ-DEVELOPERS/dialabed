﻿CREATE TABLE [dbo].[JobBOMParent] (
    [InstructionId]    INT             NULL,
    [JobId]            INT             NULL,
    [BOMInstructionId] INT             NULL,
    [StorageUnitId]    INT             NULL,
    [Quantity]         NUMERIC (13, 6) NULL,
    [CheckQuantity]    NUMERIC (13, 6) NULL,
    [OutboundLineId]   INT             NULL,
    [Checked]          BIT             NULL
);

