﻿CREATE TABLE [dbo].[PriorityHistory] (
    [PriorityId]     INT           NULL,
    [Priority]       NVARCHAR (50) NULL,
    [PriorityCode]   NVARCHAR (10) NULL,
    [OrderBy]        INT           NULL,
    [CommandType]    NVARCHAR (10) NOT NULL,
    [InsertDate]     DATETIME      NOT NULL,
    [CheckingAreaId] INT           NULL
);

