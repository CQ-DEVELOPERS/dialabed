﻿CREATE TABLE [dbo].[DashboardReplenishmentTime] (
    [WarehouseId] INT            NULL,
    [Type]        NVARCHAR (255) NULL,
    [AreaId]      INT            NULL,
    [Area]        NVARCHAR (50)  NULL,
    [Value]       INT            NULL
);

