﻿CREATE TABLE [dbo].[OutboundPerformance] (
    [JobId]            INT      NOT NULL,
    [CreateDate]       DATETIME NULL,
    [Release]          DATETIME NULL,
    [StartDate]        DATETIME NULL,
    [EndDate]          DATETIME NULL,
    [Checking]         DATETIME NULL,
    [Despatch]         DATETIME NULL,
    [DespatchChecked]  DATETIME NULL,
    [Complete]         DATETIME NULL,
    [Checked]          DATETIME NULL,
    [PlanningComplete] DATETIME NULL,
    [WarehouseId]      INT      NULL,
    PRIMARY KEY CLUSTERED ([JobId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_OutboundPerformance_PlanningComplete]
    ON [dbo].[OutboundPerformance]([PlanningComplete] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_OutboundPerformance_Checked]
    ON [dbo].[OutboundPerformance]([Checked] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_OutboundPerformance_Release]
    ON [dbo].[OutboundPerformance]([Release] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundPerformance_EndDate]
    ON [dbo].[OutboundPerformance]([EndDate] ASC) WITH (FILLFACTOR = 90);

