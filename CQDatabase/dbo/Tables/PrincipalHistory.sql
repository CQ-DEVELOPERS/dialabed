﻿CREATE TABLE [dbo].[PrincipalHistory] (
    [PrincipalId]   INT             NULL,
    [PrincipalCode] NVARCHAR (30)   NULL,
    [Principal]     NVARCHAR (255)  NULL,
    [CommandType]   NVARCHAR (10)   NULL,
    [InsertDate]    DATETIME        NULL,
    [Email]         NVARCHAR (50)   NULL,
    [FirstName]     NVARCHAR (50)   NULL,
    [LastName]      NVARCHAR (50)   NULL,
    [Address1]      NVARCHAR (1000) NULL,
    [Address2]      NVARCHAR (1000) NULL,
    [Address3]      NVARCHAR (1000) NULL,
    [Address4]      NVARCHAR (1000) NULL,
    [Address5]      NVARCHAR (1000) NULL,
    [Address6]      NVARCHAR (1000) NULL
);

