﻿CREATE TABLE [dbo].[InterfaceMappingColumn] (
    [InterfaceMappingColumnId]   INT           IDENTITY (1, 1) NOT NULL,
    [InterfaceMappingColumnCode] NVARCHAR (30) NULL,
    [InterfaceMappingColumn]     NVARCHAR (50) NULL,
    [InterfaceMappingFileId]     INT           NULL,
    [InterfaceDocumentTypeId]    INT           NULL,
    [InterfaceFieldId]           INT           NULL,
    [ColumnNumber]               INT           NULL,
    [StartPosition]              INT           NULL,
    [EndPostion]                 INT           NULL,
    [Format]                     NVARCHAR (30) NULL,
    PRIMARY KEY CLUSTERED ([InterfaceMappingColumnId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([InterfaceDocumentTypeId]) REFERENCES [dbo].[InterfaceDocumentType] ([InterfaceDocumentTypeId]),
    FOREIGN KEY ([InterfaceFieldId]) REFERENCES [dbo].[InterfaceField] ([InterfaceFieldId]),
    FOREIGN KEY ([InterfaceMappingFileId]) REFERENCES [dbo].[InterfaceMappingFile] ([InterfaceMappingFileId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceMappingColumn_InterfaceDocumentTypeId]
    ON [dbo].[InterfaceMappingColumn]([InterfaceDocumentTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceMappingColumn_InterfaceFieldId]
    ON [dbo].[InterfaceMappingColumn]([InterfaceFieldId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceMappingColumn_InterfaceMappingFileId]
    ON [dbo].[InterfaceMappingColumn]([InterfaceMappingFileId] ASC) WITH (FILLFACTOR = 90);

