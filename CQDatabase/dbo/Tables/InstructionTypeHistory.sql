﻿CREATE TABLE [dbo].[InstructionTypeHistory] (
    [InstructionTypeId]   INT           NULL,
    [PriorityId]          INT           NULL,
    [InstructionType]     NVARCHAR (30) NULL,
    [InstructionTypeCode] NVARCHAR (10) NULL,
    [CommandType]         NVARCHAR (10) NOT NULL,
    [InsertDate]          DATETIME      NOT NULL
);

