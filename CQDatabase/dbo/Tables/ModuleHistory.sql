﻿CREATE TABLE [dbo].[ModuleHistory] (
    [ModuleId]    INT           NULL,
    [Module]      NVARCHAR (50) NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL
);

