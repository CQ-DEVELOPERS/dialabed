﻿CREATE TABLE [dbo].[Principal] (
    [PrincipalId]   INT             IDENTITY (1, 1) NOT NULL,
    [PrincipalCode] NVARCHAR (30)   NULL,
    [Principal]     NVARCHAR (255)  NULL,
    [Email]         NVARCHAR (50)   NULL,
    [FirstName]     NVARCHAR (50)   NULL,
    [LastName]      NVARCHAR (50)   NULL,
    [Address1]      NVARCHAR (1000) NULL,
    [Address2]      NVARCHAR (1000) NULL,
    [Address3]      NVARCHAR (1000) NULL,
    [Address4]      NVARCHAR (1000) NULL,
    [Address5]      NVARCHAR (1000) NULL,
    [Address6]      NVARCHAR (1000) NULL,
    PRIMARY KEY CLUSTERED ([PrincipalId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_PrincipalCode] UNIQUE NONCLUSTERED ([PrincipalCode] ASC) WITH (FILLFACTOR = 90)
);


GO
create trigger tr_Principal_i
on Principal
for insert
as
begin
  declare @GetDate           datetime,
          @PrincipalId       int,
          @FTPUserName       nvarchar(50),
          @INTUserName       nvarchar(50),
          @FTPPassword       nvarchar(50) = cast((Abs(Checksum(NewId()))%10) as varchar(1)) + 
                                            char(ascii('a')+(Abs(Checksum(NewId()))%25)) +
                                            char(ascii('A')+(Abs(Checksum(NewId()))%25)) +
                                            left(newid(),5),
          @INTPassword       nvarchar(50) = cast((Abs(Checksum(NewId()))%10) as varchar(1)) + 
                                            char(ascii('a')+(Abs(Checksum(NewId()))%25)) +
                                            char(ascii('A')+(Abs(Checksum(NewId()))%25)) +
                                            left(newid(),5)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if exists(select top 1 1
              from CQCommon.dbo.sysobjects
             where xtype = 'U'
               and name = 'InterfaceLogin')
  begin
    select @FTPUserName = 'FTP' + replace(i.PrincipalCode,' ',''),
           @INTUserName = 'INT' + replace(i.PrincipalCode,' ','')
      from inserted i
    
    insert InterfaceMappingFile
          (PrincipalId,
           InterfaceFileTypeId,
           InterfaceDocumentTypeId,
           Delimiter,
           StyleSheet,
           PrincipalFTPSite,
           FTPUserName,
           FTPPassword,
           HeaderRow,
           SubDirectory,
           FilePrefix)
    select i.PrincipalId,
           mf.InterfaceFileTypeId,
           mf.InterfaceDocumentTypeId,
           mf.Delimiter,
           mf.StyleSheet,
           mf.PrincipalFTPSite,
           mf.FTPUserName,
           mf.FTPPassword,
           mf.HeaderRow,
           mf.SubDirectory,
           mf.FilePrefix
      from Principal             p,
           InterfaceMappingFile mf,
           inserted              i
     where p.PrincipalCode = 'Defaults'
       and p.PrincipalId = mf.PrincipalId
       and not exists(select top 1 1 from InterfaceMappingFile mf2 where mf2.PrincipalId = i.PrincipalId and mf.InterfaceDocumentTypeId = mf2.InterfaceDocumentTypeId)
    
    insert InterfaceMappingColumn
          (InterfaceMappingColumnCode,
           InterfaceMappingColumn,
           InterfaceDocumentTypeId,
           InterfaceFieldId,
           ColumnNumber,
           StartPosition,
           EndPostion,
           Format,
           InterfaceMappingFileId)
    select mc.InterfaceMappingColumnCode,
           mc.InterfaceMappingColumn,
           mc.InterfaceDocumentTypeId,
           mc.InterfaceFieldId,
           mc.ColumnNumber,
           mc.StartPosition,
           mc.EndPostion,
           mc.Format,
           mf2.InterfaceMappingFileId
      from inserted i,
           InterfaceMappingFile   mf,
           InterfaceMappingColumn mc,
           InterfaceMappingFile   mf2
     where mf.PrincipalId = 1
       and mf.InterfaceMappingFileId = mc.InterfaceMappingFileId
       and mf2.PrincipalId = i.PrincipalId
       and mf2.InterfaceFileTypeId = mf.InterfaceFileTypeId
       and mf2.InterfaceDocumentTypeId = mf.InterfaceDocumentTypeId
       and not exists(select top 1 1
                        from InterfaceMappingColumn mc2
                       where mf2.InterfaceMappingFileId = mc2.InterfaceMappingFileId
                         and mc.InterfaceFieldId       = mc2.InterfaceFieldId
                         and mc.ColumnNumber           = mc2.ColumnNumber)
    
    insert CQCommon.dbo.InterfaceLogin
          (DatabaseName,
           PrincipalCode,
           InterfaceUsername,
           InterfacePassword,
           FTPUsername,
           FTPPassword,
           InsertDate,
           IsSetup,
           Email,
           FirstName,
           LastName)
    select DB_NAME(),
           i.PrincipalCode,
           @INTUserName,
           @INTPassword,
           @FTPUserName,
           @FTPPassword,
           @GetDate,
           0,
           Email,
           FirstName,
           LastName
      from inserted i
    
    select @PrincipalId = PrincipalId
      from inserted
    
    exec p_Operator_Create
     @OperatorGroupId = 1,
     @WarehouseId     = 1,
     @Operator        = @INTUserName,
     @OperatorCode    = @INTUserName,
     @Password        = @INTPassword,
     @ActiveIndicator = 1,
     @ExpiryDate      = '2020-01-01',
     @PrincipalId     = @PrincipalId 
  end
end

GO
create trigger tr_Principal_d
on Principal
instead of delete
as
begin
  begin tran
  
  delete t
    from deleted              d
    join InterfaceMappingFile t on d.PrincipalId = t.PrincipalId
  
  if @@ERROR != 0
    goto Error
  
  delete t
    from deleted       d
    join Principal t on d.PrincipalId = t.PrincipalId
  
  if @@ERROR != 0
    goto Error
  
  Error:
    rollback tran
end

GO
create trigger tr_Principal_u
on Principal
after update
as
begin
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if exists(select top 1 1
              from CQCommon.dbo.sysobjects
             where xtype = 'U'
               and name = 'InterfaceLogin')
  begin
    update il
       set DatabaseName      = DB_NAME(),
           PrincipalCode     = i.PrincipalCode,
           Email             = i.Email,
           FirstName         = i.FirstName,
           LastName          = i.LastName
      from inserted                     i
      join deleted                      d on i.PrincipalId = d.PrincipalId
      join CQCommon.dbo.InterfaceLogin il on il.DatabaseName = DB_NAME()
                                         and il.PrincipalCode = d.PrincipalCode
  end
end
