﻿CREATE TABLE [dbo].[Method] (
    [MethodId]   INT            IDENTITY (1, 1) NOT NULL,
    [MethodCode] NVARCHAR (10)  NULL,
    [Method]     NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([MethodId] ASC) WITH (FILLFACTOR = 90)
);

