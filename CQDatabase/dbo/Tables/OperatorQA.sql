﻿CREATE TABLE [dbo].[OperatorQA] (
    [OperatorId]        INT        NULL,
    [EndDate]           DATETIME   NULL,
    [JobId]             INT        NULL,
    [QA]                BIT        NULL,
    [InstructionTypeId] INT        NULL,
    [ActivityStatus]    CHAR (1)   NULL,
    [PickingRate]       INT        NULL,
    [Units]             INT        NULL,
    [Weight]            FLOAT (53) NULL,
    [Instructions]      INT        NULL,
    [Orders]            INT        NULL,
    [OrderLines]        INT        NULL,
    [Jobs]              INT        NULL,
    [ActiveTime]        INT        NULL,
    [DwellTime]         INT        NULL,
    FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([JobId]),
    CONSTRAINT [FK__OperatorQ__Opera__24822779] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorQA_OperatorId]
    ON [dbo].[OperatorQA]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorQA_JobId]
    ON [dbo].[OperatorQA]([JobId] ASC) WITH (FILLFACTOR = 90);

