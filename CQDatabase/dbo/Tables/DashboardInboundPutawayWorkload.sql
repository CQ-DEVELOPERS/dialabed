﻿CREATE TABLE [dbo].[DashboardInboundPutawayWorkload] (
    [WarehouseId] INT           NULL,
    [GroupBy]     NVARCHAR (50) NULL,
    [Legend]      NVARCHAR (50) NULL,
    [Value]       INT           NULL,
    [Jobs]        INT           NULL
);

