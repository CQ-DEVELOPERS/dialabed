﻿CREATE TABLE [dbo].[SavedFileType] (
    [SavedFileTypeId]   INT            IDENTITY (1, 1) NOT NULL,
    [SavedFileTypeCode] NVARCHAR (50)  NULL,
    [SavedFileType]     NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([SavedFileTypeId] ASC) WITH (FILLFACTOR = 90)
);

