﻿CREATE TABLE [dbo].[ReceiptLine] (
    [ReceiptLineId]           INT             IDENTITY (1, 1) NOT NULL,
    [ReceiptId]               INT             NOT NULL,
    [InboundLineId]           INT             NOT NULL,
    [StorageUnitBatchId]      INT             NOT NULL,
    [StatusId]                INT             NOT NULL,
    [OperatorId]              INT             NULL,
    [RequiredQuantity]        FLOAT (53)      NULL,
    [ReceivedQuantity]        FLOAT (53)      NULL,
    [AcceptedQuantity]        FLOAT (53)      NULL,
    [RejectQuantity]          FLOAT (53)      NULL,
    [DeliveryNoteQuantity]    FLOAT (53)      NULL,
    [SampleQuantity]          FLOAT (53)      NULL,
    [AcceptedWeight]          FLOAT (53)      NULL,
    [ChildStorageUnitBatchId] INT             NULL,
    [NumberOfPallets]         INT             NULL,
    [AssaySamples]            FLOAT (53)      NULL,
    [ParentReceiptLineId]     INT             NULL,
    [BOELineNumber]           NVARCHAR (50)   NULL,
    [CountryofOrigin]         NVARCHAR (50)   NULL,
    [TariffCode]              NVARCHAR (50)   NULL,
    [Reference1]              NVARCHAR (50)   NULL,
    [Reference2]              NVARCHAR (50)   NULL,
    [UnitPrice]               DECIMAL (13, 2) NULL,
    [ReceivedDate]            DATETIME        NULL,
    [PutawayStarted]          DATETIME        NULL,
    [PutawayEnded]            DATETIME        NULL,
    CONSTRAINT [PK_ReceiptLine] PRIMARY KEY CLUSTERED ([ReceiptLineId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ReceiptLine_Operator] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [InboundLine_ReceiptLine_FK1] FOREIGN KEY ([InboundLineId]) REFERENCES [dbo].[InboundLine] ([InboundLineId]),
    CONSTRAINT [Receipt_ReceiptLine_FK1] FOREIGN KEY ([ReceiptId]) REFERENCES [dbo].[Receipt] ([ReceiptId]),
    CONSTRAINT [Status_ReceiptLine_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ReceiptLine_ReceiptId]
    ON [dbo].[ReceiptLine]([ReceiptId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ReceiptLine_StorageUnitBatchId]
    ON [dbo].[ReceiptLine]([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ReceiptLine_InboundLineId]
    ON [dbo].[ReceiptLine]([InboundLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ReceiptLine_OperatorId]
    ON [dbo].[ReceiptLine]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ReceiptLine_StatusId]
    ON [dbo].[ReceiptLine]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ReceiptLine_ChildStorageUnitBatchId]
    ON [dbo].[ReceiptLine]([ChildStorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ReceiptLine_StatusId]
    ON [dbo].[ReceiptLine]([StatusId] ASC)
    INCLUDE([ReceiptLineId], [ReceiptId], [StorageUnitBatchId], [AcceptedQuantity], [RejectQuantity], [SampleQuantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_ReceiptLine_NumberOfPallets_includes]
    ON [dbo].[ReceiptLine]([NumberOfPallets] ASC)
    INCLUDE([ReceiptLineId], [ReceiptId], [InboundLineId]) WITH (FILLFACTOR = 100);

