﻿CREATE TABLE [dbo].[StorageUnitExternalCompany] (
    [StorageUnitId]      INT             NOT NULL,
    [ExternalCompanyId]  INT             NOT NULL,
    [AllocationCategory] NUMERIC (13, 3) DEFAULT ((100)) NOT NULL,
    [MinimumShelfLife]   NUMERIC (13, 3) DEFAULT ((0)) NOT NULL,
    [ProductCode]        NVARCHAR (30)   NULL,
    [SKUCode]            NVARCHAR (10)   NULL,
    [DefaultQC]          BIT             DEFAULT ((0)) NULL,
    [SendToQC]           BIT             DEFAULT ((0)) NULL,
    [SerialTracked]      BIT             NULL,
    CONSTRAINT [StorageUnitExternalCompany_PK] PRIMARY KEY CLUSTERED ([StorageUnitId] ASC, [ExternalCompanyId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitExternalCompany_ExternalCompanyId]
    ON [dbo].[StorageUnitExternalCompany]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitExternalCompany_StorageUnitId]
    ON [dbo].[StorageUnitExternalCompany]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);

