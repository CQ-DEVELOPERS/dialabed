﻿CREATE TABLE [dbo].[FloScheduleImport] (
    [Route]               VARCHAR (50)   NULL,
    [Driver]              VARCHAR (50)   NULL,
    [Vehicle]             VARCHAR (50)   NULL,
    [VehicleClass]        VARCHAR (20)   NULL,
    [StopNumber]          INT            NULL,
    [UniqueNumber]        VARCHAR (20)   NULL,
    [DeliveryPoint]       VARCHAR (50)   NULL,
    [CustomerNumber]      VARCHAR (40)   NULL,
    [CompanyName]         VARCHAR (255)  NULL,
    [PickupTime]          VARCHAR (20)   NULL,
    [Arrival]             VARCHAR (20)   NULL,
    [TimeforJob]          INT            NULL,
    [DeliveryDate]        DATETIME       NULL,
    [LineNumber]          INT            NULL,
    [CheckSummary]        INT            NULL,
    [RecordStatus]        CHAR (1)       DEFAULT ('N') NOT NULL,
    [ProcessedDate]       DATETIME       NULL,
    [FloScheduleImportId] INT            IDENTITY (1, 1) NOT NULL,
    [InsertDate]          DATETIME       CONSTRAINT [DF__FloSchedu__Inser__7E0516D1] DEFAULT (getdate()) NULL,
    [ErrorMsg]            NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([FloScheduleImportId] ASC) WITH (FILLFACTOR = 90)
);

