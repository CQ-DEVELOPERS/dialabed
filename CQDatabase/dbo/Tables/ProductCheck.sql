﻿CREATE TABLE [dbo].[ProductCheck] (
    [JobId]      INT NULL,
    [OperatorId] INT NULL,
    FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([JobId]),
    FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ProductCheck_OperatorId]
    ON [dbo].[ProductCheck]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ProductCheck_JobId]
    ON [dbo].[ProductCheck]([JobId] ASC) WITH (FILLFACTOR = 90);

