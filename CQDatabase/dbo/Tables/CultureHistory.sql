﻿CREATE TABLE [dbo].[CultureHistory] (
    [CultureId]   INT           NULL,
    [CultureName] NVARCHAR (50) NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL
);

