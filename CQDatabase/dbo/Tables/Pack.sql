﻿CREATE TABLE [dbo].[Pack] (
    [PackId]             INT             IDENTITY (1, 1) NOT NULL,
    [StorageUnitId]      INT             NOT NULL,
    [PackTypeId]         INT             NOT NULL,
    [Quantity]           FLOAT (53)      NOT NULL,
    [Barcode]            NVARCHAR (50)   NULL,
    [Length]             INT             NULL,
    [Width]              INT             NULL,
    [Height]             INT             NULL,
    [Volume]             NUMERIC (13, 3) NULL,
    [Weight]             FLOAT (53)      NULL,
    [WarehouseId]        INT             NULL,
    [NettWeight]         FLOAT (53)      NULL,
    [TareWeight]         FLOAT (53)      NULL,
    [ProductionQuantity] FLOAT (53)      NULL,
    [StatusId]           INT             NULL,
    CONSTRAINT [Pack_PK] PRIMARY KEY CLUSTERED ([PackId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [FK__Pack__WarehouseI__0BB679AF] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId]),
    CONSTRAINT [PackType_Pack_FK1] FOREIGN KEY ([PackTypeId]) REFERENCES [dbo].[PackType] ([PackTypeId]),
    CONSTRAINT [StorageUnit_Pack_FK1] FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Pack_Barcode]
    ON [dbo].[Pack]([Barcode] ASC)
    INCLUDE([StorageUnitId], [WarehouseId], [PackTypeId], [Quantity]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Pack_StorageUnitId]
    ON [dbo].[Pack]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Pack_WarehouseId]
    ON [dbo].[Pack]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Pack]
    ON [dbo].[Pack]([WarehouseId] ASC, [PackTypeId] ASC, [StorageUnitId] ASC, [Quantity] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Pack_StatusId]
    ON [dbo].[Pack]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Pack_WarehouseId]
    ON [dbo].[Pack]([WarehouseId] ASC)
    INCLUDE([StorageUnitId], [Barcode]) WITH (FILLFACTOR = 90);

