﻿CREATE TABLE [dbo].[AddressHistory] (
    [AddressId]         INT            NULL,
    [ExternalCompanyId] INT            NULL,
    [Street]            NVARCHAR (100) NULL,
    [Suburb]            NVARCHAR (100) NULL,
    [Town]              NVARCHAR (100) NULL,
    [Country]           NVARCHAR (100) NULL,
    [Code]              NVARCHAR (20)  NULL,
    [CommandType]       NVARCHAR (10)  NOT NULL,
    [InsertDate]        DATETIME       NOT NULL
);

