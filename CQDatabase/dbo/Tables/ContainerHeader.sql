﻿CREATE TABLE [dbo].[ContainerHeader] (
    [ContainerHeaderId] INT           IDENTITY (1, 1) NOT NULL,
    [PalletId]          INT           NULL,
    [JobId]             INT           NULL,
    [ReferenceNumber]   NVARCHAR (30) NULL,
    [PickLocationId]    INT           NULL,
    [StoreLocationId]   INT           NULL,
    [CreatedBy]         INT           NULL,
    [CreateDate]        DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([ContainerHeaderId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[Operator] ([OperatorId]),
    FOREIGN KEY ([PickLocationId]) REFERENCES [dbo].[Location] ([LocationId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ContainerHeader_StoreLocationId]
    ON [dbo].[ContainerHeader]([StoreLocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ContainerHeader_PickLocationId]
    ON [dbo].[ContainerHeader]([PickLocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ContainerHeader_CreatedBy]
    ON [dbo].[ContainerHeader]([CreatedBy] ASC) WITH (FILLFACTOR = 90);

