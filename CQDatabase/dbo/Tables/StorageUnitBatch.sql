﻿CREATE TABLE [dbo].[StorageUnitBatch] (
    [StorageUnitBatchId] INT IDENTITY (1, 1) NOT NULL,
    [BatchId]            INT NOT NULL,
    [StorageUnitId]      INT NOT NULL,
    CONSTRAINT [StorageUnitBatch_PK] PRIMARY KEY CLUSTERED ([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Batch_StorageUnitBatch_FK1] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [StorageUnit_StorageUnitBatch_FK1] FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId]),
    CONSTRAINT [uc_batchid_storageunit] UNIQUE NONCLUSTERED ([BatchId] ASC, [StorageUnitId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitBatch_StorageUnitId]
    ON [dbo].[StorageUnitBatch]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);

