﻿CREATE TABLE [dbo].[InterfaceExportSOHeader] (
    [InterfaceExportSOHeaderId] INT            IDENTITY (1, 1) NOT NULL,
    [IssueId]                   INT            NULL,
    [PrimaryKey]                NVARCHAR (30)  NULL,
    [OrderNumber]               NVARCHAR (30)  NULL,
    [RecordType]                NVARCHAR (30)  DEFAULT ('SAL') NULL,
    [RecordStatus]              CHAR (1)       DEFAULT ('N') NOT NULL,
    [CustomerCode]              NVARCHAR (30)  NULL,
    [Customer]                  NVARCHAR (255) NULL,
    [Address]                   NVARCHAR (255) NULL,
    [FromWarehouseCode]         NVARCHAR (30)  NULL,
    [ToWarehouseCode]           NVARCHAR (30)  NULL,
    [Route]                     NVARCHAR (50)  NULL,
    [DeliveryDate]              DATETIME       NULL,
    [Remarks]                   NVARCHAR (255) NULL,
    [NumberOfLines]             INT            NULL,
    [Additional1]               NVARCHAR (255) NULL,
    [Additional2]               NVARCHAR (255) NULL,
    [Additional3]               NVARCHAR (255) NULL,
    [Additional4]               NVARCHAR (255) NULL,
    [Additional5]               NVARCHAR (255) NULL,
    [Additional6]               NVARCHAR (255) NULL,
    [Additional7]               NVARCHAR (255) NULL,
    [Additional8]               NVARCHAR (255) NULL,
    [Additional9]               NVARCHAR (255) NULL,
    [Additional10]              NVARCHAR (255) NULL,
    [ProcessedDate]             DATETIME       NULL,
    [InsertDate]                DATETIME       CONSTRAINT [DF_InterfaceExportSOHeader_InsertDate] DEFAULT (getdate()) NULL,
    [PrincipalCode]             NVARCHAR (30)  NULL,
    [AutoInvoice]               BIT            DEFAULT ((1)) NULL,
    [Backorder]                 BIT            DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([InterfaceExportSOHeaderId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportSOHeader_IssueId]
    ON [dbo].[InterfaceExportSOHeader]([IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportSOHeader_Status_Dates]
    ON [dbo].[InterfaceExportSOHeader]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

