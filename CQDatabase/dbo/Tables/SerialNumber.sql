﻿CREATE TABLE [dbo].[SerialNumber] (
    [SerialNumberId]     INT            IDENTITY (1, 1) NOT NULL,
    [StorageUnitId]      INT            NOT NULL,
    [LocationId]         INT            NULL,
    [ReceiptId]          INT            NULL,
    [StoreInstructionId] INT            NULL,
    [PickInstructionId]  INT            NULL,
    [SerialNumber]       NVARCHAR (50)  NOT NULL,
    [ReceivedDate]       DATETIME       NOT NULL,
    [DespatchDate]       DATETIME       NULL,
    [ReferenceNumber]    NVARCHAR (30)  NULL,
    [BatchId]            INT            NULL,
    [IssueId]            INT            NULL,
    [IssueLineId]        INT            NULL,
    [ReceiptLineId]      INT            NULL,
    [StoreJobId]         INT            NULL,
    [PickJobId]          INT            NULL,
    [Remarks]            NVARCHAR (255) NULL,
    [StatusId]           INT            NULL,
    CONSTRAINT [SerialNumber_PK] PRIMARY KEY CLUSTERED ([SerialNumberId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [FK__SerialNum__Issue__7C3CC9F0] FOREIGN KEY ([IssueId]) REFERENCES [dbo].[Issue] ([IssueId]),
    CONSTRAINT [FK__SerialNum__Issue__7D30EE29] FOREIGN KEY ([IssueLineId]) REFERENCES [dbo].[IssueLine] ([IssueLineId]),
    CONSTRAINT [FK__SerialNum__PickJ__47261C4A] FOREIGN KEY ([PickJobId]) REFERENCES [dbo].[Job] ([JobId]),
    CONSTRAINT [FK__SerialNum__Recei__7E251262] FOREIGN KEY ([ReceiptLineId]) REFERENCES [dbo].[ReceiptLine] ([ReceiptLineId]),
    CONSTRAINT [Location_SerialNumber_FK1] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [Receipt_SerialNumber_FK1] FOREIGN KEY ([ReceiptId]) REFERENCES [dbo].[Receipt] ([ReceiptId]),
    CONSTRAINT [StorageUnit_SerialNumber_FK1] FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId]),
    CONSTRAINT [uc_SerialNumber] UNIQUE NONCLUSTERED ([SerialNumber] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_StorageUnitId]
    ON [dbo].[SerialNumber]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_LocationId]
    ON [dbo].[SerialNumber]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_ReceiptId]
    ON [dbo].[SerialNumber]([ReceiptId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_Product]
    ON [dbo].[SerialNumber]([StorageUnitId] ASC, [BatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_BatchId]
    ON [dbo].[SerialNumber]([BatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_IssueLineId]
    ON [dbo].[SerialNumber]([IssueLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_IssueId]
    ON [dbo].[SerialNumber]([IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_ReceiptLineId]
    ON [dbo].[SerialNumber]([ReceiptLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_StatusId]
    ON [dbo].[SerialNumber]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_PickJobId]
    ON [dbo].[SerialNumber]([PickJobId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_SerialNumber_StoreJobId]
    ON [dbo].[SerialNumber]([StoreJobId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_SerialNumber_StorageUnitId]
    ON [dbo].[SerialNumber]([StorageUnitId] ASC)
    INCLUDE([SerialNumber], [ReferenceNumber]) WITH (FILLFACTOR = 90);

