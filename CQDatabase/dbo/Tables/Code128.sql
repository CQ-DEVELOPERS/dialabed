﻿CREATE TABLE [dbo].[Code128] (
    [Code128]     NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Char]        NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Variant]     NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AsciiNumber] NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

