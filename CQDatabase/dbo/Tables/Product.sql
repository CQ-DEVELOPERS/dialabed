﻿CREATE TABLE [dbo].[Product] (
    [ProductId]                 INT             IDENTITY (1, 1) NOT NULL,
    [StatusId]                  INT             NOT NULL,
    [ProductCode]               NVARCHAR (30)   NOT NULL,
    [Product]                   NVARCHAR (255)  NULL,
    [Barcode]                   NVARCHAR (50)   NULL,
    [MinimumQuantity]           FLOAT (53)      NULL,
    [ReorderQuantity]           FLOAT (53)      NULL,
    [MaximumQuantity]           FLOAT (53)      NULL,
    [CuringPeriodDays]          INT             NULL,
    [ShelfLifeDays]             INT             NULL,
    [QualityAssuranceIndicator] BIT             NOT NULL,
    [ProductType]               NVARCHAR (20)   NULL,
    [OverReceipt]               NUMERIC (13, 3) CONSTRAINT [DF_Product_OverReceipt] DEFAULT ((0)) NULL,
    [HostId]                    NVARCHAR (30)   NULL,
    [RetentionSamples]          INT             DEFAULT ((0)) NULL,
    [Samples]                   INT             DEFAULT ((0)) NULL,
    [ParentProductCode]         NVARCHAR (30)   NULL,
    [DangerousGoodsId]          INT             NULL,
    [Description2]              NVARCHAR (255)  NULL,
    [Description3]              NVARCHAR (255)  NULL,
    [PrincipalId]               INT             NULL,
    [AssaySamples]              FLOAT (53)      DEFAULT ((0)) NULL,
    [Category]                  NVARCHAR (50)   NULL,
    [ProductCategoryId]         INT             NULL,
    [ProductAlias]              NVARCHAR (50)   NULL,
    [StockItem]                 BIT             DEFAULT ((1)) NULL,
    [ProductGroup]              NVARCHAR (255)  NULL,
    CONSTRAINT [Product_PK] PRIMARY KEY CLUSTERED ([ProductId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([DangerousGoodsId]) REFERENCES [dbo].[DangerousGoods] ([DangerousGoodsId]),
    FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId]),
    CONSTRAINT [Status_Product_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [UQ_Product] UNIQUE NONCLUSTERED ([ProductCode] ASC, [PrincipalId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_Product_StatusId]
    ON [dbo].[Product]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Product_HostId]
    ON [dbo].[Product]([HostId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Product_Prd]
    ON [dbo].[Product]([ProductCode] ASC, [Product] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Product_PrincipalId]
    ON [dbo].[Product]([PrincipalId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Product_DangerousGoodsId]
    ON [dbo].[Product]([DangerousGoodsId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Product_ProductCategoryId]
    ON [dbo].[Product]([ProductCategoryId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Product_ProductCode_Product]
    ON [dbo].[Product]([ProductCode] ASC, [Product] ASC)
    INCLUDE([ProductId], [StatusId], [PrincipalId], [Category]) WITH (FILLFACTOR = 90);

