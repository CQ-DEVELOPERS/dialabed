﻿CREATE TABLE [dbo].[VisualDataOutbound] (
    [Location]        VARCHAR (15)  NULL,
    [OrderNumber]     VARCHAR (30)  NULL,
    [ExternalCompany] VARCHAR (255) NULL,
    [JobId]           INT           NULL,
    [Pallet]          VARCHAR (30)  NULL,
    [InstructionType] VARCHAR (50)  NULL,
    [StatusCode]      VARCHAR (10)  NULL,
    [Scanned]         DATETIME      NULL,
    [DespatchDate]    DATETIME      NULL,
    [Minutes]         INT           NULL
);

