﻿CREATE TABLE [dbo].[CQBuild] (
    [CQBuildId]    INT            IDENTITY (1, 1) NOT NULL,
    [MajorVersion] INT            NOT NULL,
    [MinorVersion] INT            NOT NULL,
    [BuildNumber]  INT            NOT NULL,
    [Revision]     INT            NOT NULL,
    [QaVersion]    NVARCHAR (255) NULL,
    [CreatedBy]    NVARCHAR (255) NOT NULL,
    [CreatedDate]  DATETIME       CONSTRAINT [DF_CQBuild_CreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CQBuild] PRIMARY KEY CLUSTERED ([CQBuildId] ASC) WITH (FILLFACTOR = 90)
);

