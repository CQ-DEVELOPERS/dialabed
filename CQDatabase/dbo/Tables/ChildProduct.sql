﻿CREATE TABLE [dbo].[ChildProduct] (
    [ChildProductId]           INT IDENTITY (1, 1) NOT NULL,
    [ChildStorageUnitBatchId]  INT NULL,
    [ParentStorageUnitBatchId] INT NULL,
    PRIMARY KEY CLUSTERED ([ChildProductId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ChildStorageUnitBatchId]) REFERENCES [dbo].[StorageUnitBatch] ([StorageUnitBatchId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ChildProduct_ParentStorageUnitBatchId]
    ON [dbo].[ChildProduct]([ParentStorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ChildProduct_ChildStorageUnitBatchId]
    ON [dbo].[ChildProduct]([ChildStorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);

