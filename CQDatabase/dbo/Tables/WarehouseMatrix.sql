﻿CREATE TABLE [dbo].[WarehouseMatrix] (
    [WarehouseMatrixId] INT           IDENTITY (1, 1) NOT NULL,
    [PrincipalId]       INT           NULL,
    [WarehouseCode]     NVARCHAR (30) NULL,
    [HostCode]          NVARCHAR (30) NULL,
    PRIMARY KEY CLUSTERED ([WarehouseMatrixId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId])
);


GO
CREATE NONCLUSTERED INDEX [nci_WarehouseMatrix_PrincipalId]
    ON [dbo].[WarehouseMatrix]([PrincipalId] ASC) WITH (FILLFACTOR = 90);

