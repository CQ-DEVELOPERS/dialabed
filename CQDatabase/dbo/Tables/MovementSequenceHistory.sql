﻿CREATE TABLE [dbo].[MovementSequenceHistory] (
    [MovementSequenceId] INT           NULL,
    [PickAreaId]         INT           NULL,
    [StoreAreaId]        INT           NULL,
    [StageAreaId]        INT           NULL,
    [OperatorGroupId]    INT           NULL,
    [StatusCode]         NVARCHAR (10) NULL,
    [CommandType]        NVARCHAR (10) NOT NULL,
    [InsertDate]         DATETIME      NOT NULL
);

