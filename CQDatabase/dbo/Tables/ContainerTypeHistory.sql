﻿CREATE TABLE [dbo].[ContainerTypeHistory] (
    [ContainerTypeId]   INT           NULL,
    [ContainerType]     NVARCHAR (50) NULL,
    [ContainerTypeCode] NVARCHAR (10) NULL,
    [CommandType]       NVARCHAR (10) NOT NULL,
    [InsertDate]        DATETIME      NOT NULL
);

