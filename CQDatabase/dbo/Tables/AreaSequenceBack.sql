﻿CREATE TABLE [dbo].[AreaSequenceBack] (
    [AreaSequenceId]      INT           IDENTITY (1, 1) NOT NULL,
    [PickAreaId]          INT           NOT NULL,
    [StoreAreaId]         INT           NOT NULL,
    [AdjustmentType]      NVARCHAR (10) NOT NULL,
    [ReasonCode]          VARCHAR (10)  NULL,
    [InstructionTypeCode] NVARCHAR (10) NULL,
    [StoreAdjustmentType] NVARCHAR (10) NULL
);

