﻿CREATE TABLE [dbo].[InterfaceImportPONum] (
    [id]              NVARCHAR (20) NULL,
    [Ordernumber]     NVARCHAR (50) NULL,
    [ReferenceNumber] NVARCHAR (50) NULL,
    [docstate]        NVARCHAR (50) NULL,
    [statusId]        NVARCHAR (50) NULL,
    [OrderDate]       DATETIME      NULL
);

