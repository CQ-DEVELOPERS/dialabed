﻿CREATE TABLE [dbo].[StorageUnitLocationHistory] (
    [StorageUnitId]    INT           NULL,
    [LocationId]       INT           NULL,
    [CommandType]      NVARCHAR (10) NOT NULL,
    [InsertDate]       DATETIME      NOT NULL,
    [MinimumQuantity]  FLOAT (53)    NULL,
    [HandlingQuantity] FLOAT (53)    NULL,
    [MaximumQuantity]  FLOAT (53)    NULL
);

