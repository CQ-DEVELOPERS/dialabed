﻿CREATE TABLE [dbo].[InboundDocumentHistory] (
    [InboundDocumentId]     INT            NULL,
    [InboundDocumentTypeId] INT            NULL,
    [ExternalCompanyId]     INT            NULL,
    [StatusId]              INT            NULL,
    [WarehouseId]           INT            NULL,
    [OrderNumber]           NVARCHAR (30)  NULL,
    [DeliveryDate]          DATETIME       NULL,
    [CreateDate]            DATETIME       NULL,
    [ModifiedDate]          DATETIME       NULL,
    [CommandType]           NVARCHAR (10)  NOT NULL,
    [InsertDate]            DATETIME       NOT NULL,
    [ReasonId]              INT            NULL,
    [Remarks]               NVARCHAR (255) NULL,
    [ReferenceNumber]       NVARCHAR (30)  NULL,
    [FromLocation]          NVARCHAR (10)  NULL,
    [ToLocation]            NVARCHAR (10)  NULL,
    [StorageUnitId]         INT            NULL,
    [BatchId]               INT            NULL,
    [DivisionId]            INT            NULL,
    [PrincipalId]           INT            NULL
);

