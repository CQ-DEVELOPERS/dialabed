﻿CREATE TABLE [dbo].[AreaLocation] (
    [AreaId]     INT NOT NULL,
    [LocationId] INT NOT NULL,
    CONSTRAINT [AreaLocation_PK] PRIMARY KEY CLUSTERED ([AreaId] ASC, [LocationId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Area_AreaLocation_FK1] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([AreaId]),
    CONSTRAINT [Location_AreaLocation_FK1] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId])
);


GO
CREATE NONCLUSTERED INDEX [nci_AreaLocation_LocationId]
    ON [dbo].[AreaLocation]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_AreaLocation_AreaId]
    ON [dbo].[AreaLocation]([AreaId] ASC) WITH (FILLFACTOR = 90);

