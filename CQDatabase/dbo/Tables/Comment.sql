﻿CREATE TABLE [dbo].[Comment] (
    [CommentId]    INT            IDENTITY (1, 1) NOT NULL,
    [Comment]      NVARCHAR (MAX) NULL,
    [OperatorId]   INT            NULL,
    [CreateDate]   DATETIME       CONSTRAINT [DF__Comment__CreateD__0F80164E] DEFAULT (getdate()) NULL,
    [ModifiedDate] DATETIME       NULL,
    [ReadDate]     DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([CommentId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Comment_OperatorId]
    ON [dbo].[Comment]([OperatorId] ASC) WITH (FILLFACTOR = 90);

