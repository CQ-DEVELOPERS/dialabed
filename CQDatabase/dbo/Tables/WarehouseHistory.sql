﻿CREATE TABLE [dbo].[WarehouseHistory] (
    [WarehouseId]       INT           NULL,
    [CompanyId]         INT           NULL,
    [Warehouse]         NVARCHAR (50) NULL,
    [WarehouseCode]     NVARCHAR (30) NULL,
    [CommandType]       NVARCHAR (10) NOT NULL,
    [InsertDate]        DATETIME      NOT NULL,
    [HostId]            NVARCHAR (30) NULL,
    [ParentWarehouseId] INT           NULL,
    [AddressId]         INT           NULL,
    [DesktopMaximum]    INT           NULL,
    [DesktopWarning]    INT           NULL,
    [MobileWarning]     INT           NULL,
    [MobileMaximum]     INT           NULL,
    [UploadToHost]      BIT           NULL,
    [PostalAddressId]   INT           NULL,
    [ContactListId]     INT           NULL,
    [PrincipalId]       INT           NULL,
    [AllowInbound]      BIT           NULL,
    [AllowOutbound]     BIT           NULL,
    [WarehouseCode2]    NVARCHAR (30) NULL
);

