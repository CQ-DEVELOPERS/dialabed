﻿CREATE TABLE [dbo].[DashboardOutboundCheckingWorkload] (
    [WarehouseId] INT           NULL,
    [Legend]      NVARCHAR (50) NULL,
    [Value]       INT           NULL,
    [Jobs]        INT           NULL
);

