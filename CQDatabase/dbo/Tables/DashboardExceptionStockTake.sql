﻿CREATE TABLE [dbo].[DashboardExceptionStockTake] (
    [WarehouseId] INT            NULL,
    [Type]        NVARCHAR (255) NULL,
    [AreaId]      INT            NULL,
    [Area]        NVARCHAR (50)  NULL,
    [Count]       INT            NULL
);

