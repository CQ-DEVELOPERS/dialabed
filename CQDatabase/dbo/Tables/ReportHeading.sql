﻿CREATE TABLE [dbo].[ReportHeading] (
    [ReportHeadingId]   INT            IDENTITY (1, 1) NOT NULL,
    [CultureId]         INT            NULL,
    [ReportHeadingCode] NVARCHAR (100) NULL,
    [ReportHeading]     NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([ReportHeadingId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CultureId]) REFERENCES [dbo].[Culture] ([CultureId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ReportHeading_CultureId]
    ON [dbo].[ReportHeading]([CultureId] ASC) WITH (FILLFACTOR = 90);

