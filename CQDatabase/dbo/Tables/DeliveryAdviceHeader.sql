﻿CREATE TABLE [dbo].[DeliveryAdviceHeader] (
    [DeliveryAdviceHeaderId] INT            IDENTITY (1, 1) NOT NULL,
    [DeliveryAdvice]         NVARCHAR (30)  NULL,
    [PurchaseOrderHeaderId]  INT            NULL,
    [CallOffHeaderId]        INT            NULL,
    [DocumentTypeId]         INT            NULL,
    [StatusId]               INT            NULL,
    [PriorityId]             INT            NULL,
    [ExternalCompanyId]      INT            NULL,
    [WarehouseId]            INT            NULL,
    [RequiredDate]           DATETIME       NULL,
    [DeliveryNoteNumber]     NVARCHAR (30)  NULL,
    [DeliveryDate]           DATETIME       NULL,
    [SealNumber]             NVARCHAR (30)  NULL,
    [ReceivedDate]           DATETIME       NULL,
    [TransportModeId]        INT            NULL,
    [NotificationMethodId]   INT            NULL,
    [ConfirmReceipt]         BIT            DEFAULT ((0)) NULL,
    [ConfirmAccpeted]        BIT            DEFAULT ((0)) NULL,
    [NumberOfOrders]         INT            NULL,
    [NumberOfLines]          INT            NULL,
    [Remarks]                NVARCHAR (MAX) NULL,
    [ShipmentHeaderId]       INT            NULL,
    [InvoiceNumber]          NVARCHAR (30)  NULL,
    PRIMARY KEY CLUSTERED ([DeliveryAdviceHeaderId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CallOffHeaderId]) REFERENCES [dbo].[CallOffHeader] ([CallOffHeaderId]),
    FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[InboundDocumentType] ([InboundDocumentTypeId]),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([NotificationMethodId]) REFERENCES [dbo].[NotificationMethod] ([NotificationMethodId]),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    FOREIGN KEY ([PurchaseOrderHeaderId]) REFERENCES [dbo].[PurchaseOrderHeader] ([PurchaseOrderHeaderId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    FOREIGN KEY ([TransportModeId]) REFERENCES [dbo].[TransportMode] ([TransportModeId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_PurchaseOrderHeaderId]
    ON [dbo].[DeliveryAdviceHeader]([PurchaseOrderHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_WarehouseId]
    ON [dbo].[DeliveryAdviceHeader]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_DocumentTypeId]
    ON [dbo].[DeliveryAdviceHeader]([DocumentTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_ExternalCompanyId]
    ON [dbo].[DeliveryAdviceHeader]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_StatusId]
    ON [dbo].[DeliveryAdviceHeader]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_CallOffHeaderId]
    ON [dbo].[DeliveryAdviceHeader]([CallOffHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_PriorityId]
    ON [dbo].[DeliveryAdviceHeader]([PriorityId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_TransportModeId]
    ON [dbo].[DeliveryAdviceHeader]([TransportModeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceHeader_NotificationMethodId]
    ON [dbo].[DeliveryAdviceHeader]([NotificationMethodId] ASC) WITH (FILLFACTOR = 90);

