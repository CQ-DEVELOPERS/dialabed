﻿CREATE TABLE [dbo].[QAAudit] (
    [QAAuditId]          INT        IDENTITY (1, 1) NOT NULL,
    [JobId]              INT        NULL,
    [StorageUnitBatchId] INT        NULL,
    [OrderedQuantity]    FLOAT (53) NULL,
    [PickedQuantity]     FLOAT (53) NULL,
    [CheckedQuantity]    FLOAT (53) NULL,
    [WHQuantity]         FLOAT (53) NULL,
    [PickedBy]           INT        NULL,
    [PickedDate]         DATETIME   NULL,
    [CheckedBy]          INT        NULL,
    [CheckedDate]        DATETIME   CONSTRAINT [DF__QAAudit__Checked__28A6276A] DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([QAAuditId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CheckedBy]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [FK__QAAudit__JobId__25C9BABF] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([JobId])
);


GO
CREATE NONCLUSTERED INDEX [nci_QAAudit_JobId]
    ON [dbo].[QAAudit]([JobId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_QAAudit_CheckedBy]
    ON [dbo].[QAAudit]([CheckedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_QAAudit_PickedBy]
    ON [dbo].[QAAudit]([PickedBy] ASC) WITH (FILLFACTOR = 90);

