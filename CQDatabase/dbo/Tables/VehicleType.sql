﻿CREATE TABLE [dbo].[VehicleType] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [VehicleTypeId] NVARCHAR (30) DEFAULT ('') NOT NULL,
    [Name]          NVARCHAR (80) DEFAULT ('') NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 90)
);

