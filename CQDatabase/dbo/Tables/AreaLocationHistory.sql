﻿CREATE TABLE [dbo].[AreaLocationHistory] (
    [AreaId]      INT           NULL,
    [LocationId]  INT           NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL
);

