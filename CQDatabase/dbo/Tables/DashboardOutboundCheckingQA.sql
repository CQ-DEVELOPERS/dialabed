﻿CREATE TABLE [dbo].[DashboardOutboundCheckingQA] (
    [WarehouseId] INT           NULL,
    [Legend]      NVARCHAR (50) NULL,
    [Value]       INT           NULL,
    [KPI]         INT           NULL
);

