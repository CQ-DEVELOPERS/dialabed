﻿CREATE TABLE [dbo].[InterfaceSupplierProduct] (
    [SupplierCode]  NVARCHAR (30) NULL,
    [ProductCode]   NVARCHAR (50) NULL,
    [ProcessedDate] DATETIME      NULL,
    [RecordStatus]  CHAR (1)      DEFAULT ('N') NULL
);

