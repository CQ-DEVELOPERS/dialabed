﻿CREATE TABLE [dbo].[StockOnHandCompare] (
    [WarehouseId]    INT             NULL,
    [ComparisonDate] DATETIME        NULL,
    [StorageUnitId]  INT             NULL,
    [BatchId]        INT             NULL,
    [Quantity]       FLOAT (53)      NULL,
    [Sent]           BIT             DEFAULT ((0)) NULL,
    [UnitPrice]      NUMERIC (13, 3) NULL,
    [WarehouseCode]  NVARCHAR (30)   NULL,
    FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StockOnHandCompare_StorageUnitId]
    ON [dbo].[StockOnHandCompare]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StockOnHandCompare_BatchId]
    ON [dbo].[StockOnHandCompare]([BatchId] ASC) WITH (FILLFACTOR = 90);

