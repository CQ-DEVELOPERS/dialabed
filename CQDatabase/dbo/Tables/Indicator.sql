﻿CREATE TABLE [dbo].[Indicator] (
    [IndicatorId]      INT           IDENTITY (1, 1) NOT NULL,
    [Header]           NVARCHAR (50) NULL,
    [Indicator]        NVARCHAR (50) NULL,
    [IndicatorValue]   SQL_VARIANT   NULL,
    [IndicatorDisplay] SQL_VARIANT   NULL,
    [ThresholdGreen]   INT           NULL,
    [ThresholdYellow]  INT           NULL,
    [ThresholdRed]     INT           NULL,
    [ModifiedDate]     DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([IndicatorId] ASC) WITH (FILLFACTOR = 90)
);

