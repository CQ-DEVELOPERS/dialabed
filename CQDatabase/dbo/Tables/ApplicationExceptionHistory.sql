﻿CREATE TABLE [dbo].[ApplicationExceptionHistory] (
    [ApplicationExceptionId] INT            NULL,
    [PriorityLevel]          INT            NULL,
    [Module]                 NVARCHAR (50)  NULL,
    [Page]                   NVARCHAR (50)  NULL,
    [Method]                 NVARCHAR (100) NULL,
    [ErrorMsg]               NTEXT          NULL,
    [CreateDate]             DATETIME       NULL,
    [CommandType]            NVARCHAR (10)  NOT NULL,
    [InsertDate]             DATETIME       NOT NULL
);

