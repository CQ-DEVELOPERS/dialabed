﻿CREATE TABLE [dbo].[CheckingLog] (
    [CheckingLogId]      INT           IDENTITY (1, 1) NOT NULL,
    [WarehouseId]        INT           NULL,
    [ReferenceNumber]    NVARCHAR (50) NULL,
    [JobId]              INT           NULL,
    [Barcode]            NVARCHAR (50) NULL,
    [StorageUnitId]      INT           NULL,
    [StorageUnitBatchId] INT           NULL,
    [Batch]              NVARCHAR (50) NULL,
    [Quantity]           FLOAT (53)    NULL,
    [OperatorId]         INT           NULL,
    [StartDate]          DATETIME      NULL,
    [EndDate]            DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([CheckingLogId] ASC) WITH (FILLFACTOR = 90)
);

