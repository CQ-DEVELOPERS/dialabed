﻿CREATE TABLE [dbo].[DashboardInboundPutawayAchieved] (
    [WarehouseId] INT           NULL,
    [OperatorId]  INT           NULL,
    [CreateDate]  DATETIME      NULL,
    [Legend]      NVARCHAR (50) NULL,
    [Value]       INT           NULL,
    [Pallets]     INT           NULL,
    [KPI]         INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_DashboardInboundPutAwayAchieved_WarhouseId]
    ON [dbo].[DashboardInboundPutawayAchieved]([WarehouseId] ASC)
    INCLUDE([OperatorId], [CreateDate], [Value], [Pallets]) WITH (FILLFACTOR = 90);

