﻿CREATE TABLE [dbo].[Reason] (
    [ReasonId]        INT           IDENTITY (1, 1) NOT NULL,
    [Reason]          NVARCHAR (50) NULL,
    [ReasonCode]      NVARCHAR (10) NULL,
    [AdjustmentType]  VARCHAR (20)  NULL,
    [ToWarehouseCode] NVARCHAR (30) NULL,
    [RecordType]      VARCHAR (20)  NULL,
    CONSTRAINT [Reason_PK] PRIMARY KEY CLUSTERED ([ReasonId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_Reason] UNIQUE NONCLUSTERED ([Reason] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_ReasonCode] UNIQUE NONCLUSTERED ([ReasonCode] ASC) WITH (FILLFACTOR = 90)
);

