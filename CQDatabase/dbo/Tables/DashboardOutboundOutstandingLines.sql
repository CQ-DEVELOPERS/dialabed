﻿CREATE TABLE [dbo].[DashboardOutboundOutstandingLines] (
    [WarehouseId] INT           NULL,
    [Legend]      NVARCHAR (50) NULL,
    [Value]       FLOAT (53)    NULL
);

