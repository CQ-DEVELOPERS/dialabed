﻿CREATE TABLE [dbo].[MenuItemHistory] (
    [MenuItemId]       INT            NULL,
    [MenuId]           INT            NULL,
    [MenuItemText]     NVARCHAR (50)  NULL,
    [ToolTip]          NVARCHAR (255) NULL,
    [NavigateTo]       NVARCHAR (255) NULL,
    [ParentMenuItemId] INT            NULL,
    [OrderBy]          SMALLINT       NULL,
    [CommandType]      NVARCHAR (10)  NOT NULL,
    [InsertDate]       DATETIME       NOT NULL
);

