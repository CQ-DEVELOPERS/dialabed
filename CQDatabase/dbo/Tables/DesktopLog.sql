﻿CREATE TABLE [dbo].[DesktopLog] (
    [DesktopLogId]       INT           IDENTITY (1, 1) NOT NULL,
    [ProcName]           [sysname]     NOT NULL,
    [WarehouseId]        INT           NULL,
    [OutboundShipmentId] INT           NULL,
    [IssueId]            INT           NULL,
    [OperatorId]         INT           NULL,
    [ErrorMsg]           VARCHAR (MAX) NULL,
    [StartDate]          DATETIME      NULL,
    [EndDate]            DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([DesktopLogId] ASC) WITH (FILLFACTOR = 90)
);

