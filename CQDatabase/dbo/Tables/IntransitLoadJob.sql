﻿CREATE TABLE [dbo].[IntransitLoadJob] (
    [IntransitLoadJobId] INT      IDENTITY (1, 1) NOT NULL,
    [IntransitLoadId]    INT      NULL,
    [StatusId]           INT      NULL,
    [JobId]              INT      NULL,
    [LoadedById]         INT      NULL,
    [UnLoadedById]       INT      NULL,
    [CreateDate]         DATETIME NULL,
    [ReceivedDate]       DATETIME NULL,
    PRIMARY KEY CLUSTERED ([IntransitLoadJobId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([LoadedById]) REFERENCES [dbo].[Operator] ([OperatorId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [FK__Intransit__Intra__36696B85] FOREIGN KEY ([IntransitLoadId]) REFERENCES [dbo].[IntransitLoad] ([IntransitLoadId]),
    CONSTRAINT [FK__Intransit__JobId__3851B3F7] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([JobId])
);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoadJob_LoadedById]
    ON [dbo].[IntransitLoadJob]([LoadedById] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoadJob_UnLoadedById]
    ON [dbo].[IntransitLoadJob]([UnLoadedById] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoadJob_JobId]
    ON [dbo].[IntransitLoadJob]([JobId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoadJob_IntransitLoadId]
    ON [dbo].[IntransitLoadJob]([IntransitLoadId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoadJob_StatusId]
    ON [dbo].[IntransitLoadJob]([StatusId] ASC) WITH (FILLFACTOR = 90);

