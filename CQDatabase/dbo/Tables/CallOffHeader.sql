﻿CREATE TABLE [dbo].[CallOffHeader] (
    [CallOffHeaderId]       INT            IDENTITY (1, 1) NOT NULL,
    [CallOffNumber]         NVARCHAR (30)  NULL,
    [PurchaseOrderHeaderId] INT            NULL,
    [DocumentTypeId]        INT            NULL,
    [StatusId]              INT            NULL,
    [PriorityId]            INT            NULL,
    [ExternalCompanyId]     INT            NULL,
    [WarehouseId]           INT            NULL,
    [RequiredDate]          DATETIME       NULL,
    [DeliveryNoteNumber]    NVARCHAR (30)  NULL,
    [DeliveryDate]          DATETIME       NULL,
    [TransportModeId]       INT            NULL,
    [NotificationMethodId]  INT            NULL,
    [ConfirmReceipt]        BIT            DEFAULT ((0)) NULL,
    [ConfirmAccpeted]       BIT            DEFAULT ((0)) NULL,
    [NumberOfOrders]        INT            NULL,
    [NumberOfLines]         INT            NULL,
    [Remarks]               NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([CallOffHeaderId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[InboundDocumentType] ([InboundDocumentTypeId]),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([NotificationMethodId]) REFERENCES [dbo].[NotificationMethod] ([NotificationMethodId]),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    FOREIGN KEY ([PurchaseOrderHeaderId]) REFERENCES [dbo].[PurchaseOrderHeader] ([PurchaseOrderHeaderId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    FOREIGN KEY ([TransportModeId]) REFERENCES [dbo].[TransportMode] ([TransportModeId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffHeader_WarehouseId]
    ON [dbo].[CallOffHeader]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffHeader_DocumentTypeId]
    ON [dbo].[CallOffHeader]([DocumentTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffHeader_StatusId]
    ON [dbo].[CallOffHeader]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffHeader_PurchaseOrderHeaderId]
    ON [dbo].[CallOffHeader]([PurchaseOrderHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffHeader_PriorityId]
    ON [dbo].[CallOffHeader]([PriorityId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffHeader_NotificationMethodId]
    ON [dbo].[CallOffHeader]([NotificationMethodId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffHeader_ExternalCompanyId]
    ON [dbo].[CallOffHeader]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffHeader_TransportModeId]
    ON [dbo].[CallOffHeader]([TransportModeId] ASC) WITH (FILLFACTOR = 90);

