﻿CREATE TABLE [dbo].[SavedFile] (
    [SavedFileId]     INT             IDENTITY (1, 1) NOT NULL,
    [SavedFileTypeId] INT             NULL,
    [SavedFile]       NVARCHAR (255)  NULL,
    [Extention]       NVARCHAR (50)   NULL,
    [Data]            VARBINARY (MAX) NULL,
    [CreateDate]      DATETIME        CONSTRAINT [DF__SavedFile__Creat__3C52BCC5] DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([SavedFileId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([SavedFileTypeId]) REFERENCES [dbo].[SavedFileType] ([SavedFileTypeId])
);


GO
CREATE NONCLUSTERED INDEX [nci_SavedFile_SavedFileTypeId]
    ON [dbo].[SavedFile]([SavedFileTypeId] ASC) WITH (FILLFACTOR = 90);

