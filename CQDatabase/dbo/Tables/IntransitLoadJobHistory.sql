﻿CREATE TABLE [dbo].[IntransitLoadJobHistory] (
    [IntransitLoadJobId] INT           NULL,
    [IntransitLoadId]    INT           NULL,
    [StatusId]           INT           NULL,
    [JobId]              INT           NULL,
    [LoadedById]         INT           NULL,
    [UnLoadedById]       INT           NULL,
    [CreateDate]         DATETIME      NULL,
    [ReceivedDate]       DATETIME      NULL,
    [CommandType]        NVARCHAR (10) NOT NULL,
    [InsertDate]         DATETIME      NOT NULL
);

