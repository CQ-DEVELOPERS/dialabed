﻿CREATE TABLE [dbo].[ProductMovement] (
    [ProductMovementId]  INT           IDENTITY (1, 1) NOT NULL,
    [WarehouseId]        INT           NOT NULL,
    [StorageUnitId]      INT           NULL,
    [SKUId]              INT           NULL,
    [AreaId]             INT           NULL,
    [ProductCode]        NVARCHAR (30) NOT NULL,
    [MovementDate]       DATETIME      NULL,
    [OpeningBalance]     INT           NULL,
    [ReceivedQuantity]   FLOAT (53)    NULL,
    [IssuedQuantity]     FLOAT (53)    NULL,
    [Balance]            INT           NULL,
    [StorageUnitBatchId] INT           NULL,
    FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([AreaId])
);


GO
CREATE NONCLUSTERED INDEX [ProductMovement_Date]
    ON [dbo].[ProductMovement]([WarehouseId] ASC, [MovementDate] ASC, [ProductCode] ASC) WITH (FILLFACTOR = 90);

