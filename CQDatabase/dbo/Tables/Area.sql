﻿CREATE TABLE [dbo].[Area] (
    [AreaId]           INT           IDENTITY (1, 1) NOT NULL,
    [WarehouseId]      INT           NOT NULL,
    [Area]             NVARCHAR (50) NOT NULL,
    [AreaCode]         NVARCHAR (10) NOT NULL,
    [StockOnHand]      BIT           NOT NULL,
    [AreaSequence]     INT           NULL,
    [AreaType]         NVARCHAR (10) DEFAULT ('') NULL,
    [WarehouseCode]    NVARCHAR (30) NULL,
    [Replenish]        BIT           DEFAULT ((0)) NULL,
    [MinimumShelfLife] FLOAT (53)    DEFAULT ((0)) NULL,
    [Undefined]        BIT           DEFAULT ((0)) NULL,
    [STEKPI]           INT           DEFAULT ((0)) NULL,
    [BatchTracked]     BIT           DEFAULT ((1)) NULL,
    [StockTake]        BIT           DEFAULT ((1)) NULL,
    [NarrowAisle]      BIT           DEFAULT ((0)) NULL,
    [Interleaving]     BIT           DEFAULT ((0)) NULL,
    [PandD]            BIT           DEFAULT ((0)) NULL,
    [SmallPick]        BIT           DEFAULT ((0)) NULL,
    [AutoLink]         BIT           DEFAULT ((0)) NULL,
    [WarehouseCode2]   NVARCHAR (30) NULL,
    [CheckingAreaId]   INT           NULL,
    [AutoStore]        BIT           NULL,
    [AutoCheck]        BIT           DEFAULT ((0)) NULL,
    CONSTRAINT [Area_PK] PRIMARY KEY CLUSTERED ([AreaId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Warehouse_Area_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId]),
    CONSTRAINT [uc_Area_WarehouseId] UNIQUE NONCLUSTERED ([WarehouseId] ASC, [Area] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_Area_WarehouseId]
    ON [dbo].[Area]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
create trigger tr_Area_i
on Area
for insert
as
begin
  insert AreaOperatorGroup
        (AreaId,
         OperatorGroupId)
  select a.AreaId,
         og.OperatorGroupId
    from inserted a,
         OperatorGroup og
end

GO
create trigger tr_Area_d
on Area
instead of delete
as
begin
  delete t
    from deleted d
    join AreaOperator t on d.AreaId = t.AreaId
  
  delete t
    from deleted d
    join AreaOperatorGroup t on d.AreaId = t.AreaId
  
  delete t
    from deleted d
    join AreaPrincipal t on d.AreaId = t.AreaId
  
  delete t
    from deleted d
    join AreaSequence t on (d.AreaId = t.PickAreaId or d.AreaId = t.StoreAreaId)
  
  delete t
    from deleted d
    join Area t on d.AreaId = t.AreaId
end
