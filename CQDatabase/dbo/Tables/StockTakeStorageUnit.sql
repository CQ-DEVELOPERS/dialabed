﻿CREATE TABLE [dbo].[StockTakeStorageUnit] (
    [StockTakeStorageUnitId] INT      IDENTITY (1, 1) NOT NULL,
    [WarehouseId]            INT      NOT NULL,
    [StorageUnitId]          INT      NOT NULL,
    [StockTakeCounts]        INT      NULL,
    [CreateDate]             DATETIME CONSTRAINT [DF__StockTake__Creat__5A2316CE] DEFAULT (getdate()) NULL,
    [SOH]                    BIT      DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([StockTakeStorageUnitId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StockTakeStorageUnit_WarehouseId]
    ON [dbo].[StockTakeStorageUnit]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StockTakeStorageUnit_StorageUnitId]
    ON [dbo].[StockTakeStorageUnit]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);

