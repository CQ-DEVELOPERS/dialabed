﻿CREATE TABLE [dbo].[CallOffSequence] (
    [CallOffSequenceId]    INT            IDENTITY (1, 1) NOT NULL,
    [CallOffHeaderId]      INT            NULL,
    [ExternalCompanyId]    INT            NULL,
    [TransportModeId]      INT            NULL,
    [NotificationMethodId] INT            NULL,
    [CollectionDate]       DATETIME       NULL,
    [ReceivedDate]         DATETIME       NULL,
    [DespatchDate]         DATETIME       NULL,
    [PlannedDelivery]      DATETIME       NULL,
    [Remarks]              NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([CallOffSequenceId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CallOffHeaderId]) REFERENCES [dbo].[CallOffHeader] ([CallOffHeaderId]),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([NotificationMethodId]) REFERENCES [dbo].[NotificationMethod] ([NotificationMethodId]),
    FOREIGN KEY ([TransportModeId]) REFERENCES [dbo].[TransportMode] ([TransportModeId])
);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffSequence_CallOffHeaderId]
    ON [dbo].[CallOffSequence]([CallOffHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffSequence_ExternalCompanyId]
    ON [dbo].[CallOffSequence]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffSequence_NotificationMethodId]
    ON [dbo].[CallOffSequence]([NotificationMethodId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CallOffSequence_TransportModeId]
    ON [dbo].[CallOffSequence]([TransportModeId] ASC) WITH (FILLFACTOR = 90);

