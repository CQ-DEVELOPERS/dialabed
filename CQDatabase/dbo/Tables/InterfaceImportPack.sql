﻿CREATE TABLE [dbo].[InterfaceImportPack] (
    [InterfaceImportProductId] INT             NULL,
    [PackCode]                 NVARCHAR (30)   NULL,
    [PackDescription]          NVARCHAR (255)  NULL,
    [Quantity]                 FLOAT (53)      NULL,
    [Barcode]                  NVARCHAR (50)   NULL,
    [Length]                   NUMERIC (13, 6) NULL,
    [Width]                    NUMERIC (13, 6) NULL,
    [Height]                   NUMERIC (13, 6) NULL,
    [Volume]                   FLOAT (53)      NULL,
    [NettWeight]               FLOAT (53)      NULL,
    [GrossWeight]              FLOAT (53)      NULL,
    [TareWeight]               FLOAT (53)      NULL,
    [ProductCategory]          CHAR (1)        NULL,
    [PackingCategory]          CHAR (1)        NULL,
    [PickEmpty]                BIT             NULL,
    [StackingCategory]         INT             NULL,
    [MovementCategory]         INT             NULL,
    [ValueCategory]            INT             NULL,
    [StoringCategory]          INT             NULL,
    [PickPartPallet]           INT             NULL,
    [HostId]                   INT             NULL,
    [ForeignKey]               NVARCHAR (255)  NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportPack_InterfaceImportProductId]
    ON [dbo].[InterfaceImportPack]([InterfaceImportProductId] ASC) WITH (FILLFACTOR = 90);

