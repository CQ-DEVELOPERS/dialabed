﻿CREATE TABLE [dbo].[Configuration] (
    [ConfigurationId] INT            NOT NULL,
    [WarehouseId]     INT            NOT NULL,
    [ModuleId]        INT            NOT NULL,
    [Configuration]   NVARCHAR (255) NOT NULL,
    [Indicator]       BIT            NULL,
    [IntegerValue]    INT            NULL,
    [Value]           SQL_VARIANT    NULL,
    CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED ([WarehouseId] ASC, [ConfigurationId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__Configura__Wareh__0CAA9DE8] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId]),
    CONSTRAINT [FK_Configuration_Module] FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[Module] ([ModuleId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Configuration_ModuleId]
    ON [dbo].[Configuration]([ModuleId] ASC) WITH (FILLFACTOR = 90);

