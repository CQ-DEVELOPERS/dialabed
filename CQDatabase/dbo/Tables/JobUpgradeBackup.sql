﻿CREATE TABLE [dbo].[JobUpgradeBackup] (
    [JobId]            INT           IDENTITY (1, 1) NOT NULL,
    [PriorityId]       INT           NOT NULL,
    [OperatorId]       INT           NULL,
    [StatusId]         INT           NOT NULL,
    [WarehouseId]      INT           NOT NULL,
    [ReceiptLineId]    INT           NULL,
    [IssueLineId]      INT           NULL,
    [ContainerTypeId]  INT           NULL,
    [ReferenceNumber]  NVARCHAR (30) NULL,
    [TareWeight]       FLOAT (53)    NULL,
    [Weight]           FLOAT (53)    NULL,
    [NettWeight]       FLOAT (53)    NULL,
    [CheckedBy]        INT           NULL,
    [CheckedDate]      DATETIME      NULL,
    [DropSequence]     INT           NULL,
    [Pallets]          INT           NULL,
    [BackFlush]        BIT           NULL,
    [Prints]           INT           NULL,
    [CheckingClear]    BIT           NULL,
    [OriginalJobId]    INT           NULL,
    [AuthorisedBy]     INT           NULL,
    [AuthorisedStatus] NVARCHAR (10) NULL
);

