﻿CREATE TABLE [dbo].[SavedFileLink] (
    [SavedFileLinkId] INT       IDENTITY (1, 1) NOT NULL,
    [SavedFileId]     INT       NULL,
    [UploadTableId]   INT       NULL,
    [UploadTableName] [sysname] NOT NULL,
    PRIMARY KEY CLUSTERED ([SavedFileLinkId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([SavedFileId]) REFERENCES [dbo].[SavedFile] ([SavedFileId])
);


GO
CREATE NONCLUSTERED INDEX [nci_SavedFileLink_SavedFileId]
    ON [dbo].[SavedFileLink]([SavedFileId] ASC) WITH (FILLFACTOR = 90);

