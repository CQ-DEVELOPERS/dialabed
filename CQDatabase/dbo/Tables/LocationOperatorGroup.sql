﻿CREATE TABLE [dbo].[LocationOperatorGroup] (
    [LocationId]      INT NOT NULL,
    [OperatorGroupId] INT NOT NULL,
    [OrderBy]         INT NOT NULL,
    FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId])
);


GO
CREATE NONCLUSTERED INDEX [nci_LocationOperatorGroup_LocationId]
    ON [dbo].[LocationOperatorGroup]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_LocationOperatorGroup_OperatorGroupId]
    ON [dbo].[LocationOperatorGroup]([OperatorGroupId] ASC) WITH (FILLFACTOR = 90);

