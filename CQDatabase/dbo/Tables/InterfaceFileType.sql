﻿CREATE TABLE [dbo].[InterfaceFileType] (
    [InterfaceFileTypeId]   INT           IDENTITY (1, 1) NOT NULL,
    [InterfaceFileTypeCode] NVARCHAR (30) NULL,
    [InterfaceFileType]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([InterfaceFileTypeId] ASC) WITH (FILLFACTOR = 90)
);

