﻿CREATE TABLE [dbo].[RaiserrorCode] (
    [RaiserrorCodeId]  INT            IDENTITY (1, 1) NOT NULL,
    [RaiserrorCode]    NVARCHAR (10)  NULL,
    [RaiserrorMessage] NVARCHAR (255) NULL,
    [StoredProcedure]  [sysname]      NOT NULL,
    PRIMARY KEY CLUSTERED ([RaiserrorCodeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_RaiserrorCode] UNIQUE NONCLUSTERED ([RaiserrorCode] ASC) WITH (FILLFACTOR = 90)
);

