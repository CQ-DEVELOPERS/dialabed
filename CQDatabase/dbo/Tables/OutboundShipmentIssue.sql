﻿CREATE TABLE [dbo].[OutboundShipmentIssue] (
    [OutboundShipmentId] INT NOT NULL,
    [IssueId]            INT NOT NULL,
    [DropSequence]       INT NULL,
    CONSTRAINT [OutboundShipmentIssue_PK] PRIMARY KEY CLUSTERED ([OutboundShipmentId] ASC, [IssueId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Issue_OutboundShipmentIssue_FK1] FOREIGN KEY ([IssueId]) REFERENCES [dbo].[Issue] ([IssueId]),
    CONSTRAINT [OutboundShipment_OutboundShipmentIssue_FK1] FOREIGN KEY ([OutboundShipmentId]) REFERENCES [dbo].[OutboundShipment] ([OutboundShipmentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipmentIssue_IssueId]
    ON [dbo].[OutboundShipmentIssue]([IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundShipmentIssue_OutboundShipmentId]
    ON [dbo].[OutboundShipmentIssue]([OutboundShipmentId] ASC) WITH (FILLFACTOR = 90);

