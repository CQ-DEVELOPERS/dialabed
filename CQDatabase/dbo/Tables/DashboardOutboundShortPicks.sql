﻿CREATE TABLE [dbo].[DashboardOutboundShortPicks] (
    [WarehouseId]       INT            NULL,
    [ExternalCompanyId] INT            NULL,
    [PrincipalId]       INT            NULL,
    [ProductCode]       NVARCHAR (30)  NULL,
    [Product]           NVARCHAR (255) NULL,
    [SKUCode]           NVARCHAR (50)  NULL,
    [ShortQuantity]     FLOAT (53)     NULL,
    [Orders]            FLOAT (53)     NULL
);

