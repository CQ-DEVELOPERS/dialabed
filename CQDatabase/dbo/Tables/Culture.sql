﻿CREATE TABLE [dbo].[Culture] (
    [CultureId]   INT           IDENTITY (1, 1) NOT NULL,
    [CultureName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Culture] PRIMARY KEY CLUSTERED ([CultureId] ASC) WITH (FILLFACTOR = 90)
);

