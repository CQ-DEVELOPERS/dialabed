﻿CREATE TABLE [dbo].[StockTakeGroup] (
    [StockTakeGroupId]   INT          IDENTITY (1, 1) NOT NULL,
    [StockTakeGroupCode] VARCHAR (20) NOT NULL,
    [StockTakeGroup]     VARCHAR (50) NOT NULL,
    UNIQUE NONCLUSTERED ([StockTakeGroupCode] ASC) WITH (FILLFACTOR = 90)
);

