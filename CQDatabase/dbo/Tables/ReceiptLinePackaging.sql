﻿CREATE TABLE [dbo].[ReceiptLinePackaging] (
    [PackageLineId]        INT        NOT NULL,
    [ReceiptId]            INT        NOT NULL,
    [InboundLineId]        INT        NULL,
    [StorageUnitBatchId]   INT        NOT NULL,
    [StatusId]             INT        NULL,
    [OperatorId]           INT        NULL,
    [RequiredQuantity]     FLOAT (53) NULL,
    [ReceivedQuantity]     FLOAT (53) NULL,
    [AcceptedQuantity]     FLOAT (53) NULL,
    [RejectQuantity]       FLOAT (53) NULL,
    [DeliveryNoteQuantity] FLOAT (53) NULL,
    [jobid]                INT        NULL,
    [ReceiptLineId]        INT        NULL
);

