﻿CREATE TABLE [dbo].[OutboundStatusAudit] (
    [IssueId]            INT             NULL,
    [ExternalCompanyId]  INT             NULL,
    [CreateDate]         DATETIME        NULL,
    [PlanningComplete]   DATETIME        NULL,
    [Checking]           DATETIME        NULL,
    [Checked]            DATETIME        NULL,
    [Despatch]           DATETIME        NULL,
    [DespatchChecked]    DATETIME        NULL,
    [Complete]           DATETIME        NULL,
    [PickComplete]       INT             NULL,
    [PickTotal]          INT             NULL,
    [PickPercentage]     NUMERIC (13, 3) NULL,
    [CheckingComplete]   INT             NULL,
    [CheckingTotal]      INT             NULL,
    [CheckingPercentage] NUMERIC (13, 3) NULL
);

