﻿CREATE TABLE [dbo].[ShipmentIssueHistory] (
    [ShipmentId]   INT           NULL,
    [IssueId]      INT           NULL,
    [DropSequence] SMALLINT      NULL,
    [CommandType]  NVARCHAR (10) NOT NULL,
    [InsertDate]   DATETIME      NOT NULL
);

