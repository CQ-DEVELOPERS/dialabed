﻿CREATE TABLE [dbo].[ReportType] (
    [ReportTypeId]   INT           IDENTITY (1, 1) NOT NULL,
    [ReportType]     NVARCHAR (50) NOT NULL,
    [ReportTypeCode] NVARCHAR (10) NOT NULL,
    PRIMARY KEY CLUSTERED ([ReportTypeId] ASC) WITH (FILLFACTOR = 90)
);

