﻿CREATE TABLE [dbo].[InterfaceExportPODetail] (
    [InterfaceExportPOHeaderId] INT            NULL,
    [ForeignKey]                NVARCHAR (30)  NULL,
    [LineNumber]                INT            NULL,
    [ProductCode]               NVARCHAR (30)  NOT NULL,
    [Product]                   NVARCHAR (50)  NULL,
    [SKUCode]                   NVARCHAR (10)  NULL,
    [Batch]                     NVARCHAR (50)  NULL,
    [Quantity]                  FLOAT (53)     NOT NULL,
    [Weight]                    FLOAT (53)     NULL,
    [Additional1]               NVARCHAR (255) NULL,
    [Additional2]               NVARCHAR (255) NULL,
    [Additional3]               NVARCHAR (255) NULL,
    [Additional4]               NVARCHAR (255) NULL,
    [Additional5]               NVARCHAR (255) NULL,
    [Additional6]               NVARCHAR (255) NULL,
    [Additional7]               NVARCHAR (255) NULL,
    [Additional8]               NVARCHAR (255) NULL,
    [Additional9]               NVARCHAR (255) NULL,
    [Additional10]              NVARCHAR (255) NULL,
    [ReceiptLineId]             INT            NULL,
    FOREIGN KEY ([InterfaceExportPOHeaderId]) REFERENCES [dbo].[InterfaceExportPOHeader] ([InterfaceExportPOHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportPODetail_InterfaceExportPOHeaderId]
    ON [dbo].[InterfaceExportPODetail]([InterfaceExportPOHeaderId] ASC) WITH (FILLFACTOR = 90);

