﻿CREATE TABLE [dbo].[ExternalCompanyTypeHistory] (
    [ExternalCompanyTypeId]   INT           NULL,
    [ExternalCompanyType]     NVARCHAR (50) NULL,
    [ExternalCompanyTypeCode] NVARCHAR (10) NULL,
    [CommandType]             NVARCHAR (10) NOT NULL,
    [InsertDate]              DATETIME      NOT NULL
);

