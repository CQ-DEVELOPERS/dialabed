﻿CREATE TABLE [dbo].[InboundDocumentTypeHistory] (
    [InboundDocumentTypeId]     INT           NULL,
    [InboundDocumentType]       NVARCHAR (30) NULL,
    [OverReceiveIndicator]      BIT           NULL,
    [AllowManualCreate]         BIT           NULL,
    [QuantityToFollowIndicator] BIT           NULL,
    [PriorityId]                INT           NULL,
    [CommandType]               NVARCHAR (10) NOT NULL,
    [InsertDate]                DATETIME      NOT NULL,
    [AutoSendup]                BIT           NULL,
    [InboundDocumentTypeCode]   NVARCHAR (10) NULL,
    [BatchButton]               BIT           NULL,
    [BatchSelect]               BIT           NULL,
    [DeliveryNoteNumber]        BIT           NULL,
    [VehicleRegistration]       BIT           NULL,
    [AreaType]                  NVARCHAR (10) DEFAULT ('') NULL
);

