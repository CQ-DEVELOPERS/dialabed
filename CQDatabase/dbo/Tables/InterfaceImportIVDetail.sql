﻿CREATE TABLE [dbo].[InterfaceImportIVDetail] (
    [InterfaceImportIVHeaderId] INT             NULL,
    [ForeignKey]                NVARCHAR (30)   NULL,
    [LineNumber]                INT             NULL,
    [ProductCode]               NVARCHAR (30)   NOT NULL,
    [Product]                   NVARCHAR (50)   NULL,
    [SKUCode]                   NVARCHAR (10)   NULL,
    [Batch]                     NVARCHAR (50)   NULL,
    [Quantity]                  FLOAT (53)      NULL,
    [Weight]                    FLOAT (53)      NULL,
    [RetailPrice]               DECIMAL (13, 2) NULL,
    [NetPrice]                  DECIMAL (13, 2) NULL,
    [LineTotal]                 DECIMAL (13, 2) NULL,
    [Volume]                    DECIMAL (13, 3) NULL,
    [Additional1]               NVARCHAR (255)  NULL,
    [Additional2]               NVARCHAR (255)  NULL,
    [Additional3]               NVARCHAR (255)  NULL,
    [Additional4]               NVARCHAR (255)  NULL,
    [Additional5]               NVARCHAR (255)  NULL,
    FOREIGN KEY ([InterfaceImportIVHeaderId]) REFERENCES [dbo].[InterfaceImportIVHeader] ([InterfaceImportIVHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportIVDetail_InterfaceImportIVHeaderId]
    ON [dbo].[InterfaceImportIVDetail]([InterfaceImportIVHeaderId] ASC) WITH (FILLFACTOR = 90);

