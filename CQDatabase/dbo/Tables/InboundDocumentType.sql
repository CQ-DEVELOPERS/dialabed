﻿CREATE TABLE [dbo].[InboundDocumentType] (
    [InboundDocumentTypeId]     INT           IDENTITY (1, 1) NOT NULL,
    [InboundDocumentTypeCode]   NVARCHAR (10) NULL,
    [InboundDocumentType]       NVARCHAR (30) NOT NULL,
    [OverReceiveIndicator]      BIT           NOT NULL,
    [AllowManualCreate]         BIT           NOT NULL,
    [QuantityToFollowIndicator] BIT           NOT NULL,
    [PriorityId]                INT           NOT NULL,
    [AutoSendup]                BIT           DEFAULT ((0)) NULL,
    [BatchButton]               BIT           NULL,
    [BatchSelect]               BIT           NULL,
    [DeliveryNoteNumber]        BIT           NULL,
    [VehicleRegistration]       BIT           NULL,
    [AreaType]                  NVARCHAR (10) DEFAULT ('') NULL,
    CONSTRAINT [PK_InboundDocumentType] PRIMARY KEY CLUSTERED ([InboundDocumentTypeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_InboundDocumentType_Priority] FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    CONSTRAINT [uc_InboundDocumentType] UNIQUE NONCLUSTERED ([InboundDocumentType] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocumentType_PriorityId]
    ON [dbo].[InboundDocumentType]([PriorityId] ASC) WITH (FILLFACTOR = 90);

