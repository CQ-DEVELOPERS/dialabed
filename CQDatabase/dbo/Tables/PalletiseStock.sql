﻿CREATE TABLE [dbo].[PalletiseStock] (
    [OutboundShipmentId] INT             NULL,
    [IssueId]            INT             NULL,
    [IssueLineId]        INT             NULL,
    [DropSequence]       INT             NOT NULL,
    [AreaSequence]       INT             NULL,
    [JobId]              INT             NULL,
    [StorageUnitBatchId] INT             NOT NULL,
    [StorageUnitId]      INT             NOT NULL,
    [ProductCode]        NVARCHAR (30)   NOT NULL,
    [SKUCode]            NVARCHAR (10)   NOT NULL,
    [Quantity]           FLOAT (53)      NOT NULL,
    [OriginalQuantity]   FLOAT (53)      NULL,
    [PalletQuantity]     FLOAT (53)      NULL,
    [UnitWeight]         FLOAT (53)      NULL,
    [UnitVolume]         NUMERIC (13, 6) NULL,
    [ExternalCompanyId]  INT             NULL,
    [LineNumber]         INT             NULL,
    [BoxQuantity]        FLOAT (53)      NULL,
    [UnitQuantity]       FLOAT (53)      NULL
);

