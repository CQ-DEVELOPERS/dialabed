﻿CREATE TABLE [dbo].[RecordStatus] (
    [StatusId]   INT           IDENTITY (1, 1) NOT NULL,
    [StatusCode] NVARCHAR (20) NULL,
    [StatusDesc] NVARCHAR (50) NULL
);

