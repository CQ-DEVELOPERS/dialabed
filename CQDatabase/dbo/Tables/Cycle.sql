﻿CREATE TABLE [dbo].[Cycle] (
    [CycleId]   INT           IDENTITY (1, 1) NOT NULL,
    [CycleCode] NVARCHAR (20) NULL,
    [Cycle]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([CycleId] ASC) WITH (FILLFACTOR = 90)
);

