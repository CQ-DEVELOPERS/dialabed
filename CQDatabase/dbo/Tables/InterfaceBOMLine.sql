﻿CREATE TABLE [dbo].[InterfaceBOMLine] (
    [InterfaceBOMId]     INT            NULL,
    [ParentProductCode]  NVARCHAR (50)  NULL,
    [LineNumber]         INT            NULL,
    [ProductCode]        NVARCHAR (50)  NULL,
    [ProductDescription] NVARCHAR (255) NULL,
    [Quantity]           FLOAT (53)     NULL,
    [Units]              NVARCHAR (50)  NULL,
    FOREIGN KEY ([InterfaceBOMId]) REFERENCES [dbo].[InterfaceBOM] ([InterfaceBOMId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceBOMLine_InterfaceBOMId]
    ON [dbo].[InterfaceBOMLine]([InterfaceBOMId] ASC) WITH (FILLFACTOR = 90);

