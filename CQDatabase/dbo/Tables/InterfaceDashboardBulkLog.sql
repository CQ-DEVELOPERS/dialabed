﻿CREATE TABLE [dbo].[InterfaceDashboardBulkLog] (
    [FileKeyId]        INT            NULL,
    [FileType]         NVARCHAR (100) NOT NULL,
    [ProcessedDate]    DATETIME       NOT NULL,
    [FileName]         NVARCHAR (50)  NULL,
    [ErrorCode]        CHAR (5)       NOT NULL,
    [ErrorDescription] NVARCHAR (255) NULL
);

