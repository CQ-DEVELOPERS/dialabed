﻿CREATE TABLE [dbo].[ExternalCompany] (
    [ExternalCompanyId]         INT            IDENTITY (1, 1) NOT NULL,
    [ExternalCompanyTypeId]     INT            NOT NULL,
    [RouteId]                   INT            NULL,
    [ExternalCompany]           NVARCHAR (255) NOT NULL,
    [ExternalCompanyCode]       NVARCHAR (30)  NOT NULL,
    [Rating]                    INT            NULL,
    [RedeliveryIndicator]       BIT            NULL,
    [QualityAssuranceIndicator] BIT            NULL,
    [ContainerTypeId]           INT            NULL,
    [Backorder]                 BIT            CONSTRAINT [DF__ExternalC__Backo__6D3CDF87] DEFAULT ((0)) NULL,
    [HostId]                    NVARCHAR (30)  NULL,
    [OrderLineSequence]         BIT            DEFAULT ((0)) NULL,
    [DropSequence]              INT            NULL,
    [AutoInvoice]               BIT            DEFAULT ((0)) NULL,
    [OneProductPerPallet]       BIT            DEFAULT ((0)) NULL,
    [PrincipalId]               INT            NULL,
    [ProcessId]                 INT            NULL,
    [TrustedDelivery]           BIT            NULL,
    [OutboundSLAHours]          INT            NULL,
    [PricingCategoryId]         INT            NULL,
    [LabelingCategoryId]        INT            NULL,
    [Priority]                  INT            NULL,
    [PriorityId]                INT            NULL,
    CONSTRAINT [ExternalCompany_PK] PRIMARY KEY CLUSTERED ([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([LabelingCategoryId]) REFERENCES [dbo].[LabelingCategory] ([LabelingCategoryId]),
    FOREIGN KEY ([PricingCategoryId]) REFERENCES [dbo].[PricingCategory] ([PricingCategoryId]),
    FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId]),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    FOREIGN KEY ([ProcessId]) REFERENCES [dbo].[Process] ([ProcessId]),
    CONSTRAINT [ExternalCompanyType_ExternalCompany_FK1] FOREIGN KEY ([ExternalCompanyTypeId]) REFERENCES [dbo].[ExternalCompanyType] ([ExternalCompanyTypeId]),
    CONSTRAINT [FK__ExternalC__Conta__0F5C220A] FOREIGN KEY ([ContainerTypeId]) REFERENCES [dbo].[ContainerType] ([ContainerTypeId]),
    CONSTRAINT [FK_ExternalCompany_Route] FOREIGN KEY ([RouteId]) REFERENCES [dbo].[Route] ([RouteId]),
    CONSTRAINT [uc_ExternalCompanyCode] UNIQUE NONCLUSTERED ([ExternalCompanyCode] ASC, [ExternalCompanyTypeId] ASC, [PrincipalId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_ExternalCompany_ContainerTypeId]
    ON [dbo].[ExternalCompany]([ContainerTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ExternalCompany_RouteId]
    ON [dbo].[ExternalCompany]([RouteId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ExternalCompany_ExternalCompanyTypeId]
    ON [dbo].[ExternalCompany]([ExternalCompanyTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ExternalCompany_PrincipalId]
    ON [dbo].[ExternalCompany]([PrincipalId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ExternalCompany_ProcessId]
    ON [dbo].[ExternalCompany]([ProcessId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ExternalCompany_LabelingCategoryId]
    ON [dbo].[ExternalCompany]([LabelingCategoryId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ExternalCompany_PricingCategoryId]
    ON [dbo].[ExternalCompany]([PricingCategoryId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ExternalCompany_ExternalCompanyTypeId]
    ON [dbo].[ExternalCompany]([ExternalCompanyTypeId] ASC)
    INCLUDE([ExternalCompanyId], [ExternalCompany], [ExternalCompanyCode]) WITH (FILLFACTOR = 90);

