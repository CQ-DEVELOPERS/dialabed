﻿CREATE TABLE [dbo].[Questions] (
    [QuestionId]     INT            IDENTITY (1, 1) NOT NULL,
    [QuestionaireId] INT            NOT NULL,
    [Category]       NVARCHAR (50)  NULL,
    [Code]           NVARCHAR (50)  NULL,
    [Sequence]       INT            NOT NULL,
    [Active]         BIT            NULL,
    [QuestionText]   NVARCHAR (255) NULL,
    [QuestionType]   NVARCHAR (255) NULL,
    [DateUpdated]    DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([QuestionId] ASC) WITH (FILLFACTOR = 90)
);

