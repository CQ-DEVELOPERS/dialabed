﻿CREATE TABLE [dbo].[PriceList] (
    [PriceListId]       INT             IDENTITY (1, 1) NOT NULL,
    [PricingCategoryId] INT             NULL,
    [StorageUnitId]     INT             NULL,
    [RRP]               NUMERIC (13, 2) NULL,
    PRIMARY KEY CLUSTERED ([PriceListId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([PricingCategoryId]) REFERENCES [dbo].[PricingCategory] ([PricingCategoryId]),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_PriceList_PricingCategoryId]
    ON [dbo].[PriceList]([PricingCategoryId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_PriceList_StorageUnitId]
    ON [dbo].[PriceList]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);

