﻿CREATE TABLE [dbo].[InterfaceImportIPDetail] (
    [InterfaceImportIPHeaderId] INT             NULL,
    [ForeignKey]                NVARCHAR (30)   NULL,
    [ProductCode]               NVARCHAR (30)   NULL,
    [Product]                   NVARCHAR (255)  NULL,
    [Batch]                     NVARCHAR (50)   NULL,
    [Quantity]                  FLOAT (53)      NULL,
    [Excluding]                 NUMERIC (13, 2) NULL,
    [Including]                 NUMERIC (13, 2) NULL,
    [NettValue]                 NUMERIC (13, 2) NULL,
    [LineNumber]                INT             NULL,
    CONSTRAINT [FK__Interface__Inter__6724DED5] FOREIGN KEY ([InterfaceImportIPHeaderId]) REFERENCES [dbo].[InterfaceImportIPHeader] ([InterfaceImportIPHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportIPDetail_InterfaceImportIPHeaderId]
    ON [dbo].[InterfaceImportIPDetail]([InterfaceImportIPHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
--drop trigger tr_InterfaceImportIPDetail_i
--go
create trigger tr_InterfaceImportIPDetail_i
on InterfaceImportIPDetail
for insert
as
begin
  update d
     set InterfaceImportIPHeaderId = (select max(InterfaceImportIPHeaderId)
                                        from InterfaceImportIPHeader h
                                       where d.ForeignKey = h.PrimaryKey)
    from InterfaceImportIPDetail d
   where InterfaceImportIPHeaderId is null
end


--insert Interf