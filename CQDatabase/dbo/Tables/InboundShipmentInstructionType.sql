﻿CREATE TABLE [dbo].[InboundShipmentInstructionType] (
    [InboundShipmentId] INT             NULL,
    [InstructionTypeId] INT             NULL,
    [Quantity]          NUMERIC (13, 6) NULL,
    [OperatorGroupId]   INT             NULL
);

