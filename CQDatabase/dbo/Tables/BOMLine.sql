﻿CREATE TABLE [dbo].[BOMLine] (
    [BOMLineId]   INT IDENTITY (1, 1) NOT NULL,
    [BOMHeaderId] INT NULL,
    CONSTRAINT [PK_BOMLine] PRIMARY KEY CLUSTERED ([BOMLineId] ASC) WITH (FILLFACTOR = 90)
);

