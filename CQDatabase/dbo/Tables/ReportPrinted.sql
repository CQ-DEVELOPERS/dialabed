﻿CREATE TABLE [dbo].[ReportPrinted] (
    [ReportPrintedId]    INT      IDENTITY (1, 1) NOT NULL,
    [ReportTypeId]       INT      NOT NULL,
    [OutboundShipmentId] INT      NULL,
    [IssueId]            INT      NULL,
    [InboundShipmentId]  INT      NULL,
    [ReceiptId]          INT      NULL,
    [StorageUnitId]      INT      NULL,
    [PrintedCopies]      INT      NULL,
    [CreateDate]         DATETIME NULL,
    [EmailDate]          DATETIME NULL,
    [InstructionId]      INT      NULL,
    PRIMARY KEY CLUSTERED ([ReportPrintedId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [ReportType_ReportPrinted_FK1] FOREIGN KEY ([ReportTypeId]) REFERENCES [dbo].[ReportType] ([ReportTypeId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ReportPrinted_ReportTypeId]
    ON [dbo].[ReportPrinted]([ReportTypeId] ASC) WITH (FILLFACTOR = 90);

