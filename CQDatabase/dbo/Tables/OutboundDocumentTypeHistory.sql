﻿CREATE TABLE [dbo].[OutboundDocumentTypeHistory] (
    [OutboundDocumentTypeId]   INT             NULL,
    [OutboundDocumentType]     NVARCHAR (30)   NULL,
    [PriorityId]               INT             NULL,
    [CommandType]              NVARCHAR (10)   NOT NULL,
    [InsertDate]               DATETIME        NOT NULL,
    [MinimumShelfLife]         NUMERIC (13, 3) NULL,
    [AlternatePallet]          BIT             DEFAULT ((0)) NULL,
    [SubstitutePallet]         BIT             DEFAULT ((0)) NULL,
    [OutboundDocumentTypeCode] NVARCHAR (10)   DEFAULT ((0)) NULL,
    [AutoRelease]              BIT             DEFAULT ((0)) NULL,
    [AddToShipment]            BIT             DEFAULT ((0)) NULL,
    [MultipleOnShipment]       BIT             DEFAULT ((0)) NULL,
    [LIFO]                     BIT             DEFAULT ((0)) NULL,
    [AreaType]                 NVARCHAR (10)   DEFAULT ('') NULL,
    [AutoSendup]               BIT             DEFAULT ((0)) NULL,
    [CheckingLane]             INT             NULL,
    [DespatchBay]              INT             NULL,
    [PlanningComplete]         BIT             DEFAULT ((0)) NULL,
    [AutoInvoice]              BIT             NULL,
    [Backorder]                BIT             NULL,
    [AutoCheck]                BIT             NULL,
    [ReserveBatch]             BIT             NULL,
    [Packaging]                BIT             NULL,
    [CheckingAreaId]           INT             NULL,
    [DespatchAreaId]           INT             NULL
);

