﻿CREATE TABLE [dbo].[StorageUnitBatchLocationHistory] (
    [StorageUnitBatchId] INT           NULL,
    [LocationId]         INT           NULL,
    [ActualQuantity]     FLOAT (53)    NULL,
    [AllocatedQuantity]  FLOAT (53)    NULL,
    [ReservedQuantity]   FLOAT (53)    NULL,
    [CommandType]        NVARCHAR (10) NOT NULL,
    [InsertDate]         DATETIME      NOT NULL,
    [NettWeight]         FLOAT (53)    NULL
);

