﻿CREATE TABLE [dbo].[HousekeepingCompare] (
    [WarehouseId]           INT             NULL,
    [WarehouseCode]         NVARCHAR (30)   NULL,
    [ComparisonDate]        DATETIME        NULL,
    [StorageUnitId]         INT             NULL,
    [BatchId]               INT             NULL,
    [WMSQuantity]           FLOAT (53)      NULL,
    [HostQuantity]          FLOAT (53)      NULL,
    [Sent]                  BIT             DEFAULT ((0)) NULL,
    [UnitPrice]             NUMERIC (13, 3) NULL,
    [HostDate]              DATETIME        NULL,
    [SentDate]              DATETIME        NULL,
    [InterfaceId]           INT             NULL,
    [HousekeepingCompareId] INT             IDENTITY (1, 1) NOT NULL,
    [RecordStatus]          CHAR (1)        NULL,
    [CustomField1]          NVARCHAR (50)   NULL,
    PRIMARY KEY CLUSTERED ([HousekeepingCompareId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_HousekeepingCompare_BatchId]
    ON [dbo].[HousekeepingCompare]([BatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_HousekeepingCompare_StorageUnitId]
    ON [dbo].[HousekeepingCompare]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_HousekeepingCompare_ComparisonDate]
    ON [dbo].[HousekeepingCompare]([ComparisonDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_HousekeepingCompare_WarehouseCode]
    ON [dbo].[HousekeepingCompare]([WarehouseCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_HousekeepingCompare_WarehouseId_ComparisonDate]
    ON [dbo].[HousekeepingCompare]([WarehouseId] ASC, [ComparisonDate] ASC)
    INCLUDE([HostDate]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_HousekeepingCompare_ComparisonDate]
    ON [dbo].[HousekeepingCompare]([ComparisonDate] ASC)
    INCLUDE([WarehouseId], [InterfaceId], [HousekeepingCompareId]) WITH (FILLFACTOR = 90);

