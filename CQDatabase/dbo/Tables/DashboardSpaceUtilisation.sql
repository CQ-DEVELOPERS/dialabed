﻿CREATE TABLE [dbo].[DashboardSpaceUtilisation] (
    [WarehouseId] INT           NULL,
    [AreaId]      INT           NULL,
    [Area]        NVARCHAR (50) NULL,
    [Legend]      NVARCHAR (50) NULL,
    [Value]       INT           NULL
);

