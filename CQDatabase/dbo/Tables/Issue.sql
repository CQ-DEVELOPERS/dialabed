﻿CREATE TABLE [dbo].[Issue] (
    [IssueId]             INT             IDENTITY (1, 1) NOT NULL,
    [OutboundDocumentId]  INT             NOT NULL,
    [PriorityId]          INT             NOT NULL,
    [WarehouseId]         INT             NOT NULL,
    [LocationId]          INT             NULL,
    [StatusId]            INT             NOT NULL,
    [OperatorId]          INT             NULL,
    [RouteId]             INT             NULL,
    [DeliveryNoteNumber]  NVARCHAR (30)   NULL,
    [DeliveryDate]        DATETIME        NULL,
    [SealNumber]          NVARCHAR (30)   NULL,
    [VehicleRegistration] NVARCHAR (10)   NULL,
    [Remarks]             NVARCHAR (500)  NULL,
    [DropSequence]        INT             NULL,
    [LoadIndicator]       BIT             CONSTRAINT [DF_Issue_LoadIndicator] DEFAULT ((0)) NULL,
    [DespatchBay]         INT             NULL,
    [RoutingSystem]       BIT             NULL,
    [Delivery]            INT             NULL,
    [NumberOfLines]       INT             NULL,
    [Total]               INT             NULL,
    [Complete]            INT             NULL,
    [AreaType]            NVARCHAR (10)   NULL,
    [Units]               INT             CONSTRAINT [DF__Issue__Units__04A0FE7A] DEFAULT ((1)) NULL,
    [ShortPicks]          INT             NULL,
    [Releases]            INT             NULL,
    [Weight]              FLOAT (53)      NULL,
    [ConfirmedWeight]     FLOAT (53)      NULL,
    [FirstLoaded]         DATETIME        NULL,
    [Interfaced]          BIT             DEFAULT ((0)) NULL,
    [Loaded]              INT             NULL,
    [Pallets]             INT             NULL,
    [Volume]              FLOAT (53)      NULL,
    [ContactListId]       INT             NULL,
    [ManufacturingId]     INT             NULL,
    [ReservedId]          INT             NULL,
    [ParentIssueId]       INT             NULL,
    [ASN]                 BIT             NULL,
    [WaveId]              INT             NULL,
    [ReserveBatch]        BIT             DEFAULT ((0)) NULL,
    [PlanningComplete]    DATETIME        NULL,
    [PlannedBy]           INT             NULL,
    [Release]             DATETIME        NULL,
    [ReleasedBy]          INT             NULL,
    [Checking]            DATETIME        NULL,
    [Checked]             DATETIME        NULL,
    [Despatch]            DATETIME        NULL,
    [DespatchChecked]     DATETIME        NULL,
    [CompleteDate]        DATETIME        NULL,
    [VehicleId]           INT             NULL,
    [AddressLine1]        NVARCHAR (255)  NULL,
    [AddressLine2]        NVARCHAR (255)  NULL,
    [AddressLine3]        NVARCHAR (255)  NULL,
    [AddressLine4]        NVARCHAR (255)  NULL,
    [AddressLine5]        NVARCHAR (255)  NULL,
    [Availability]        NVARCHAR (20)   NULL,
    [InvoiceNumber]       NVARCHAR (30)   NULL,
    [TotalDue]            NUMERIC (13, 2) NULL,
    [Branding]            BIT             NULL,
    [DownSizing]          BIT             NULL,
    [CheckSheetPrinted]   BIT             NULL,
    [CreateDate]          DATETIME        DEFAULT ([dbo].[ufn_getdate]()) NULL,
    [CheckingCount]       INT             NULL,
    CONSTRAINT [Issue_PK] PRIMARY KEY CLUSTERED ([IssueId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__Issue__ContactLi__7EA4354F] FOREIGN KEY ([ContactListId]) REFERENCES [dbo].[ContactList] ([ContactListId]),
    CONSTRAINT [FK__Issue__DespatchB__55D98BEB] FOREIGN KEY ([DespatchBay]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [FK__Issue__PlannedBy__78294658] FOREIGN KEY ([PlannedBy]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [FK__Issue__VehicleId__1CFBA3AB] FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([VehicleId]),
    CONSTRAINT [FK__Issue__WaveId__1D642151] FOREIGN KEY ([WaveId]) REFERENCES [dbo].[Wave] ([WaveId]),
    CONSTRAINT [FK_Issue_Route] FOREIGN KEY ([RouteId]) REFERENCES [dbo].[Route] ([RouteId]),
    CONSTRAINT [OutboundDocument_Issue_FK1] FOREIGN KEY ([OutboundDocumentId]) REFERENCES [dbo].[OutboundDocument] ([OutboundDocumentId]),
    CONSTRAINT [Priority_Issue_FK1] FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    CONSTRAINT [Status_Issue_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [Warehouse_Issue_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_LocationId]
    ON [dbo].[Issue]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_DespatchBay]
    ON [dbo].[Issue]([DespatchBay] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_OperatorId]
    ON [dbo].[Issue]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_WarehouseId]
    ON [dbo].[Issue]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_OutboundDocumentId]
    ON [dbo].[Issue]([OutboundDocumentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_RouteId]
    ON [dbo].[Issue]([RouteId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_StatusId]
    ON [dbo].[Issue]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_PriorityId]
    ON [dbo].[Issue]([PriorityId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue]
    ON [dbo].[Issue]([WarehouseId] ASC, [OutboundDocumentId] ASC, [PriorityId] ASC, [StatusId] ASC, [DeliveryDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_NumberOfLines]
    ON [dbo].[Issue]([NumberOfLines] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_ContactListId]
    ON [dbo].[Issue]([ContactListId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_WaveId]
    ON [dbo].[Issue]([WaveId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_ManufacturingId]
    ON [dbo].[Issue]([ManufacturingId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_VehicleId]
    ON [dbo].[Issue]([VehicleId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_PlannedBy]
    ON [dbo].[Issue]([PlannedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Issue_ReleasedBy]
    ON [dbo].[Issue]([ReleasedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Issue_StatusId]
    ON [dbo].[Issue]([StatusId] ASC)
    INCLUDE([IssueId], [WarehouseId], [OutboundDocumentId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Issue_DeliveryDate]
    ON [dbo].[Issue]([DeliveryDate] ASC)
    INCLUDE([IssueId], [OutboundDocumentId], [PriorityId], [LocationId], [StatusId], [RouteId]) WITH (FILLFACTOR = 90);

