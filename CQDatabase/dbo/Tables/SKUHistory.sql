﻿CREATE TABLE [dbo].[SKUHistory] (
    [SKUId]            INT             NULL,
    [UOMId]            INT             NULL,
    [SKU]              NVARCHAR (50)   NULL,
    [SKUCode]          NVARCHAR (50)   NULL,
    [Quantity]         FLOAT (53)      NULL,
    [CommandType]      NVARCHAR (10)   NOT NULL,
    [InsertDate]       DATETIME        NOT NULL,
    [AlternatePallet]  INT             NULL,
    [SubstitutePallet] INT             NULL,
    [Litres]           NUMERIC (13, 3) NULL,
    [ParentSKUCode]    NVARCHAR (50)   NULL
);

