﻿CREATE TABLE [dbo].[InterfaceImportORD] (
    [RecordStatus] NVARCHAR (20) NULL,
    [InsertDate]   DATETIME      NULL,
    [RecordType]   NVARCHAR (20) NULL,
    [ID]           NVARCHAR (20) NULL,
    [Ordernumber]  NVARCHAR (50) NULL,
    [StatusId]     NVARCHAR (50) NULL,
    [OrderDate]    DATETIME      NULL
);

