﻿CREATE TABLE [dbo].[ProductKPI] (
    [StorageUnitId]      INT           NULL,
    [PackType]           NVARCHAR (50) NULL,
    [EndDate]            DATETIME      NULL,
    [Quantity]           INT           NULL,
    [WarehouseId]        INT           NULL,
    [Received]           INT           NULL,
    [Damages]            INT           NULL,
    [Samples]            INT           NULL,
    [PutAway]            INT           NULL,
    [Movements]          INT           NULL,
    [Replenishments]     INT           NULL,
    [Picking]            INT           NULL,
    [Checking]           INT           NULL,
    [MoveToDespatch]     INT           NULL,
    [Despatch]           INT           NULL,
    [StorageUnitBatchId] INT           NULL,
    [SKUCode]            NVARCHAR (50) NULL,
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [IX_ProductKPI_EndDate]
    ON [dbo].[ProductKPI]([EndDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ProductKPI_StorageUnitId]
    ON [dbo].[ProductKPI]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);

