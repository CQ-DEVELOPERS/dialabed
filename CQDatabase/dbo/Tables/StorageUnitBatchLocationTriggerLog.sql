﻿CREATE TABLE [dbo].[StorageUnitBatchLocationTriggerLog] (
    [Query]              NVARCHAR (255) NULL,
    [LoginName]          NVARCHAR (255) NULL,
    [UserName]           NVARCHAR (255) NULL,
    [CurrentTime]        DATETIME       NULL,
    [StorageUnitBatchId] INT            NULL,
    [LocationId]         INT            NULL,
    [ActualQuantity]     FLOAT (53)     NULL,
    [ReservedQuantity]   FLOAT (53)     NULL,
    [AllocatedQuantity]  FLOAT (53)     NULL
);

