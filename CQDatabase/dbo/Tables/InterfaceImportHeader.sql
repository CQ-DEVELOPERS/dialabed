﻿CREATE TABLE [dbo].[InterfaceImportHeader] (
    [InterfaceImportHeaderId] INT             IDENTITY (1, 1) NOT NULL,
    [PrimaryKey]              NVARCHAR (30)   NULL,
    [OrderNumber]             NVARCHAR (30)   NULL,
    [InvoiceNumber]           NVARCHAR (30)   NULL,
    [RecordType]              NVARCHAR (30)   DEFAULT ('SAL') NULL,
    [RecordStatus]            CHAR (1)        DEFAULT ('N') NOT NULL,
    [CompanyCode]             NVARCHAR (30)   NULL,
    [Company]                 NVARCHAR (255)  NULL,
    [Address]                 NVARCHAR (255)  NULL,
    [FromWarehouseCode]       NVARCHAR (30)   NULL,
    [ToWarehouseCode]         NVARCHAR (30)   NULL,
    [Route]                   NVARCHAR (50)   NULL,
    [DeliveryNoteNumber]      NVARCHAR (30)   NULL,
    [ContainerNumber]         NVARCHAR (30)   NULL,
    [SealNumber]              NVARCHAR (30)   NULL,
    [DeliveryDate]            NVARCHAR (30)   NULL,
    [Remarks]                 NVARCHAR (255)  NULL,
    [NumberOfLines]           INT             NULL,
    [VatPercentage]           DECIMAL (13, 3) NULL,
    [VatSummary]              DECIMAL (13, 3) NULL,
    [Total]                   DECIMAL (13, 3) NULL,
    [Additional1]             NVARCHAR (255)  NULL,
    [Additional2]             NVARCHAR (255)  NULL,
    [Additional3]             NVARCHAR (255)  NULL,
    [Additional4]             NVARCHAR (255)  NULL,
    [Additional5]             NVARCHAR (255)  NULL,
    [Additional6]             NVARCHAR (255)  NULL,
    [Additional7]             NVARCHAR (255)  NULL,
    [Additional8]             NVARCHAR (255)  NULL,
    [Additional9]             NVARCHAR (255)  NULL,
    [Additional10]            NVARCHAR (255)  NULL,
    [ProcessedDate]           DATETIME        NULL,
    [InsertDate]              DATETIME        CONSTRAINT [DF_InterfaceImportHeader_InsertDate] DEFAULT (getdate()) NULL,
    [ErrorMsg]                NVARCHAR (255)  NULL,
    [HostStatus]              NVARCHAR (10)   NULL,
    [PrincipalCode]           NVARCHAR (30)   NULL,
    [BillOfEntry]             NVARCHAR (50)   NULL,
    [Reference1]              NVARCHAR (50)   NULL,
    [Reference2]              NVARCHAR (50)   NULL,
    [Incoterms]               NVARCHAR (50)   NULL,
    [Module]                  NVARCHAR (50)   NULL,
    [ItemLine]                XML             NULL,
    PRIMARY KEY CLUSTERED ([InterfaceImportHeaderId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportHeader]
    ON [dbo].[InterfaceImportHeader]([RecordStatus] ASC, [ProcessedDate] ASC, [FromWarehouseCode] ASC, [ToWarehouseCode] ASC, [PrimaryKey] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportHeader_Status_Dates]
    ON [dbo].[InterfaceImportHeader]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceImportHeader_ProcessedDate]
    ON [dbo].[InterfaceImportHeader]([ProcessedDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceImportHeader_PrimaryKey]
    ON [dbo].[InterfaceImportHeader]([PrimaryKey] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceImportHeader_OrderNumber_RecordStatus]
    ON [dbo].[InterfaceImportHeader]([OrderNumber] ASC, [RecordStatus] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceImportHeader_OrderNumber_RecordStatus_PrincipalCode]
    ON [dbo].[InterfaceImportHeader]([OrderNumber] ASC, [RecordStatus] ASC, [PrincipalCode] ASC) WITH (FILLFACTOR = 90);


GO
create trigger tr_InterfaceImportHeader_i
on InterfaceImportHeader
for insert
as
begin
  insert WarehouseCodeReference
        (WarehouseCode,
         DownloadType,
         WarehouseId,
         InboundDocumentTypeId,
         OutboundDocumentTypeId,
         ToWarehouseId,
         ToWarehouseCode,
         Module)
  select distinct h.FromWarehouseCode,
         h.RecordType,
         fromWH.WarehouseId,
         idt.InboundDocumentTypeId,
         odt.OutboundDocumentTypeId,
         toWH.WarehouseId,
         h.ToWarehouseCode,
         h.Module
    from inserted              h
    left
    join InboundDocumentType idt (nolock) on h.RecordType = idt.InboundDocumentTypeCode
                                         and h.Module = 'Inbound'
    left
    join OutboundDocumentType odt (nolock) on h.RecordType = odt.OutboundDocumentTypeCode
                                          and h.Module = 'Outbound'
    --left
    join Warehouse          fromWH (nolock) on h.FromWarehouseCode  = fromWH.WarehouseCode
    left
    join Warehouse            toWH (nolock) on h.ToWarehouseCode  = toWH.WarehouseCode
    left
    join WarehouseCodeReference wc (nolock) on h.RecordType = wc.DownloadType
                                           and isnull(h.FromWarehouseCode,'-1') = isnull(wc.WarehouseCode,'-1')
                                           and isnull(h.ToWarehouseCode,'-1') = isnull(wc.ToWarehouseCode,'-1')
                                           and ISNULL(h.Module, '-1') = isnull(wc.Module, '-1')
   where wc.WarehouseCodeReferenceId is null
     and isnull(h.RecordType,'') != ''
     and (isnull(h.FromWarehouseCode,'') != '' or isnull(h.ToWarehouseCode,'') != '')
end
