﻿CREATE TABLE [dbo].[ManualFileRequestColumn] (
    [ManualFileRequestTypeId] INT           NOT NULL,
    [ColumnName]              NVARCHAR (50) NULL,
    [ColumnLength]            INT           NULL,
    [ColumnDescription]       NVARCHAR (50) NULL
);

