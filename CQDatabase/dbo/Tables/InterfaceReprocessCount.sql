﻿CREATE TABLE [dbo].[InterfaceReprocessCount] (
    [InterfaceReprocessId] INT           IDENTITY (1, 1) NOT NULL,
    [PrimaryKey]           NVARCHAR (30) NULL,
    [OrderNumber]          NVARCHAR (30) NULL,
    [OtherKey]             NVARCHAR (30) NULL,
    [TableType]            NVARCHAR (30) NULL
);

