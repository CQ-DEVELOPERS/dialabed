﻿CREATE TABLE [dbo].[InterfaceField] (
    [InterfaceFieldId]        INT           IDENTITY (1, 1) NOT NULL,
    [InterfaceFieldCode]      NVARCHAR (30) NULL,
    [InterfaceField]          NVARCHAR (50) NULL,
    [InterfaceDocumentTypeId] INT           NULL,
    [Datatype]                NVARCHAR (50) NULL,
    [Mandatory]               BIT           NULL,
    PRIMARY KEY CLUSTERED ([InterfaceFieldId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([InterfaceDocumentTypeId]) REFERENCES [dbo].[InterfaceDocumentType] ([InterfaceDocumentTypeId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceField_InterfaceDocumentTypeId]
    ON [dbo].[InterfaceField]([InterfaceDocumentTypeId] ASC) WITH (FILLFACTOR = 90);

