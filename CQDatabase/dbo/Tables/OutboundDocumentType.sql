﻿CREATE TABLE [dbo].[OutboundDocumentType] (
    [OutboundDocumentTypeId]   INT             IDENTITY (1, 1) NOT NULL,
    [OutboundDocumentTypeCode] NVARCHAR (10)   NULL,
    [OutboundDocumentType]     NVARCHAR (30)   NOT NULL,
    [PriorityId]               INT             NOT NULL,
    [AlternatePallet]          BIT             DEFAULT ((0)) NULL,
    [SubstitutePallet]         BIT             DEFAULT ((0)) NULL,
    [AutoRelease]              BIT             DEFAULT ((0)) NULL,
    [AddToShipment]            BIT             DEFAULT ((0)) NULL,
    [MultipleOnShipment]       BIT             DEFAULT ((0)) NULL,
    [LIFO]                     BIT             DEFAULT ((0)) NULL,
    [MinimumShelfLife]         NUMERIC (13, 3) NULL,
    [AreaType]                 NVARCHAR (10)   DEFAULT ('') NULL,
    [AutoSendup]               BIT             DEFAULT ((0)) NULL,
    [CheckingLane]             INT             NULL,
    [DespatchBay]              INT             NULL,
    [PlanningComplete]         BIT             DEFAULT ((0)) NULL,
    [AutoInvoice]              BIT             DEFAULT ((1)) NULL,
    [Backorder]                BIT             DEFAULT ((0)) NULL,
    [AutoCheck]                BIT             DEFAULT ((0)) NULL,
    [ReserveBatch]             BIT             DEFAULT ((0)) NULL,
    [Packaging]                BIT             DEFAULT ((0)) NULL,
    [CheckingAreaId]           INT             NULL,
    [DespatchAreaId]           INT             NULL,
    CONSTRAINT [PK_OutboundDocumentType] PRIMARY KEY CLUSTERED ([OutboundDocumentTypeId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CheckingAreaId]) REFERENCES [dbo].[Area] ([AreaId]),
    FOREIGN KEY ([CheckingLane]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [FK_OutboundDocumentType_Priority] FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    CONSTRAINT [uc_OutboundDocumentType] UNIQUE NONCLUSTERED ([OutboundDocumentType] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocumentType_PriorityId]
    ON [dbo].[OutboundDocumentType]([PriorityId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocumentType_DespatchBay]
    ON [dbo].[OutboundDocumentType]([DespatchBay] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OutboundDocumentType_CheckingLane]
    ON [dbo].[OutboundDocumentType]([CheckingLane] ASC) WITH (FILLFACTOR = 90);

