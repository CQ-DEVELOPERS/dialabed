﻿CREATE TABLE [dbo].[QuestionAnswers] (
    [QuestionId]        INT           NULL,
    [Answer]            NVARCHAR (50) NULL,
    [JobId]             INT           NULL,
    [ReceiptId]         INT           NULL,
    [OrderNumber]       INT           NULL,
    [PalletId]          INT           NULL,
    [DateAsked]         DATETIME      NULL,
    [QuestionAnswersId] INT           IDENTITY (1, 1) NOT NULL,
    PRIMARY KEY CLUSTERED ([QuestionAnswersId] ASC) WITH (FILLFACTOR = 90)
);

