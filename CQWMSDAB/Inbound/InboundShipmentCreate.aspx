<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="InboundShipmentCreate.aspx.cs" Inherits="Inbound_InboundShipmentCreate"
    Title="<%$ Resources:Default, InboundShipmentCreateTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, InboundShipmentCreateTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, InboundShipmentCreateAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <table>
        <tr>
            <td valign="top">
                <asp:Label ID="LabelShipmentDate" runat="server" Text="Shipment Date:"></asp:Label>
            </td>
            <td valign="top">
                <asp:TextBox ID="TextBoxShipmentDate" runat="server" ValidationGroup="ShipmentDate"
                    MaxLength="1" Style="text-align: justify" Width="65px"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="defaultCalendarExtender" runat="server" TargetControlID="TextBoxShipmentDate"
                    Format="<%$ Resources:Default,DateFormat %>" Animated="true" />
                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="TextBoxShipmentDate"
                    Mask="<%$ Resources:Default,DateMask %>" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" />
                <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1"
                    ControlToValidate="TextBoxShipmentDate" IsValidEmpty="False" EmptyValueMessage="Date is required"
                    InvalidValueMessage="Date is invalid" ValidationGroup="ShipmentDate" Display="Dynamic"
                    TooltipMessage="Input a Date" Width="58px" />
                <asp:TextBox ID="TextBoxShipmentTime" runat="server" Width="40px" />
                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="TextBoxShipmentTime"
                    Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                    MaskType="Time" AcceptAMPM="True" CultureName="<%$ Resources:Default,CultureCode %>" />
                <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlExtender="MaskedEditExtender3"
                    ControlToValidate="TextBoxShipmentTime" IsValidEmpty="False" EmptyValueMessage="Time is required"
                    InvalidValueMessage="Time is invalid" ValidationGroup="ShipmentTime" Display="Dynamic"
                    TooltipMessage="Input a time" />
                <asp:RequiredFieldValidator runat="server" ID="rfvDate" ControlToValidate="TextBoxShipmentDate"
                    ErrorMessage="You must enter a Delivery date." Width="184px" />
                <asp:RequiredFieldValidator runat="server" ID="rfvTime" ControlToValidate="TextBoxShipmentTime"
                    ErrorMessage="You must enter a delivery time." />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="LabelRemarks" runat="server" Text="Remarks"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxRemarks" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <asp:Button ID="ButtonAdd" runat="server" Text="Add" OnClick="ButtonAdd_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
