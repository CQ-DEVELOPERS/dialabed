using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_ReceiveIntoWarehouse : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

                TextBoxBarcode.Focus();

                if (Session["checkedList"] != null)
                {
                    GetNextIssueLine();
                }
            }
            
            //if (DetailsViewInstruction.Rows.Count > 0)
            //{
            //    Session["JobId"] = int.Parse(DetailsViewInstruction.DataKey["JobId"].ToString());
            //    //PalletWeight1.DataBind();
            //}
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ButtonEnter_Click
    protected void ButtonEnter_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEnter_Click";

        try
        {
            GetDetails();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEnter_Click

    #region ButtonNext_Click
    protected void ButtonNext_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonNext_Click";

        try
        {
            PalletWeight pw = new PalletWeight();
            int nw = (pw.CheckWeights(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]));
            if (nw == 1)
            {
                GetNextIssueLine();
            }
            else
            {
                Master.ErrorText = Resources.Messages.NettWeightRequired;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonNext_Click

    #region GetDetails
    protected void GetDetails()
    {
        theErrMethod = "GetDetails";

        try
        {
            Master.ErrorText = "";

            Session["Barcode"] = TextBoxBarcode.Text;
            TextBoxBarcode.Text = "";

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 56))
            {
                DetailsViewInstruction.Visible = false;
                DetailsViewInstruction.DataBind();
            }
            else
            {
                DetailsViewInstruction.Visible = true;
                DetailsViewInstruction.DataBind();
            }

            if (DetailsViewInstruction.Rows.Count > 0)
            {
                Session["JobId"] = int.Parse(DetailsViewInstruction.DataKey["JobId"].ToString());
                PalletWeight1.DataBind();
            }
            else
            {
                Master.ErrorText = Resources.Messages.ErrorRetrieving;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GetDetails

    #region GetNextIssueLine
    protected void GetNextIssueLine()
    {
        theErrMethod = "GetNextIssueLine";

        try
        {
            if (Session["checkedList"] == null && Session["FromURL"] == null)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "ClosePopup", "window.close()", true);
            }
            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList.Count <= 0)
            {
                Session.Remove("checkedList");

                if (Session["FromURL"] != null)
                {
                    Response.Redirect(Session["FromURL"].ToString());
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "ClosePopup", "window.close()", true);
                }
            }
            else
            {
                int jobId = Convert.ToInt32(rowList[0].ToString());

                rowList.RemoveAt(0);

                Session["JobId"] = jobId;

                TextBoxBarcode.Text = "J:" + jobId.ToString();

                TextBoxBarcode.Visible = false;
                ButtonEnter.Visible = false;

                GetDetails();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletiseLocate" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetNextIssueLine"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceiveIntoWarehouse", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceiveIntoWarehouse", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}
