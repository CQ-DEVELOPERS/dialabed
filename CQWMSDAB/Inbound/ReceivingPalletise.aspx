<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReceivingPalletise.aspx.cs" Inherits="Inbound_ReceivingPalletise"
    Title="<%$ Resources:Default, ReceivingPalletiseTitle %>"
    StylesheetTheme="Default" Theme="Default"
    Culture="Auto"
    UICulture="Auto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/InboundSearch.ascx" TagName="InboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReceivingPalletiseTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingPalletiseAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <div style="float:left;">
                    <uc1:InboundSearch id="InboundSearch1" runat="server">
                    </uc1:InboundSearch>
                </div>
                <asp:Button ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonDocumentSearch_Click" />
                <div style="clear:left;"></div>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptDocument">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewReceiptDocument" runat="server" DataSourceID="ObjectDataSourceReceiptDocument" DataKeyNames="InboundShipmentId,ReceiptId"
                            AutoGenerateSelectButton="True" AllowPaging="true" AutoGenerateColumns="False"
                            OnSelectedIndexChanged="GridViewReceiptDocument_SelectedIndexChanged" >
                            <Columns>
                                <asp:BoundField DataField="InboundShipmentId" HeaderText="<%$ Resources:Default, InboundShipmentId%>" SortExpression="InboundShipmentId" Visible="False" ReadOnly="True" />
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"/>
                                <asp:BoundField DataField="SupplierCode" HeaderText="<%$ Resources:Default, SupplierCode %>"/>
                                <asp:BoundField DataField="Supplier" HeaderText="<%$ Resources:Default, Supplier %>"/>
                                <asp:BoundField DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"/>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"/>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>"/>
                                <asp:BoundField DataField="InboundDocumentType" HeaderText="<%$ Resources:Default, InboundDocumentType %>"/>
                                <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>"/>
                                <asp:BoundField DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>"/>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceReceiptDocument" runat="server" TypeName="Receiving" SelectMethod="GetReceivedReceivingDocuments" OnSelecting="ObjectDataSourceReceiptDocument_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="InboundShipmentId" SessionField="InboundShipmentId" Type="int32"/>
                                <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId" Type="Int32" />
                                <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                                <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewReceiptDocument" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, PalletiseLocate%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptLine">
                    <ContentTemplate>
                    <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>" OnClick="ButtonSelect_Click" />
                    <asp:Button ID="ButtonManualPalletise" runat="server" Text="<%$ Resources:Default, ManualPalletise%>" OnClick="ButtonManualPalletise_Click" style="Width:auto;"/>
                    <asp:Button ID="ButtonPalletise" runat="server" Text="<%$ Resources:Default, Palletise%>" OnClick="ButtonPalletise_Click" />
                    <asp:Button ID="ButtonAutoLocations" runat="server" Text="<%$ Resources:Default, AutoAllocation%>" OnClick="ButtonAutoLocations_Click" style="Width:auto;"/>
                    <asp:Button ID="ButtonManualLocations" Visible="False" runat="server" Text="<%$ Resources:Default, ManualAllocation%>" OnClick="ButtonManualLocations_Click" />
                    <br />
                        <asp:GridView ID="GridViewReceiptLine" runat="server" AutoGenerateSelectButton="False" AllowPaging="true" AutoGenerateColumns="False"
                            DataKeyNames="ReceiptLineId" DataSourceID="ObjectDataSourceReceiptLine">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"/>
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>"/>
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"/>
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"/>
                                
                                <asp:BoundField DataField="DeliveryNoteQuantity" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>"/>
                                <asp:BoundField DataField="AcceptedQuantity" HeaderText="<%$ Resources:Default, AcceptedQuantity %>"/>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"/>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>"/>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceReceiptLine" runat="server"
                            TypeName="Receiving"
                            UpdateMethod="SetReceiptLines"
                            SelectMethod="GetReceiptLines">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="inboundShipmentId" SessionField="InboundShipmentId" Type="int32"/>
                                <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="int32"/>
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
