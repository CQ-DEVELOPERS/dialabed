<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="YardExit.aspx.cs" Inherits="Telerik.GridExamplesCSharp.Programming.SavingGridSettingsOnPerUserBasis.DefaultCS"
    Title="<%$ Resources:Default, YardManagementTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, YardManagementTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, InboundModule %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <table>
        <tr>
            <td align="center">
                <asp:Label ID="LabelContainer" runat="server" Text="Container No:" Font-Size="X-Large"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxBarcode" runat="server" Text="" Font-Size="X-Large" Width="300px" Height="60px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <telerik:RadButton ID="ButtonOut" runat="server" Text="CONTAINER OUT" OnClick="ButtonOut_Click" Font-Size="X-Large" Height="60px"/>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
</asp:Content>
