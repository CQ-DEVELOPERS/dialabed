﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StationSheetCapture.aspx.cs" Inherits="Inbound_StationSheetCapture"
    Title="<%$ Resources:Default, ReceivingDocumentSearchTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/InboundStationSheetSearch.ascx" TagName="InboundSearch"
    TagPrefix="uc1" %>
<%@ Register Src="../Common/ProductSearchPackaging.ascx" TagName="ProductSearch"
    TagPrefix="ucpsp" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

    <script type="text/javascript" language="JavaScript">

        function openNewWindowBatch() {
            window.open("../StaticInfo/BatchMaintenance.aspx", "_blank",
      "height=600px width=450px top=200 left=200 resizable=no scrollbars=no ");
        }

    </script>

    <ajaxToolkit:TabContainer runat="server" ID="Tabs" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Orders%>">
            <ContentTemplate>
                <div style="float: left;">
                    <uc1:InboundSearch ID="InboundSearch1" runat="server"></uc1:InboundSearch>
                </div>
                <asp:Button ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                    OnClick="ButtonDocumentSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:Image ID="ImageGreen3" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                <asp:Label ID="LabelGreen3" runat="server" Text="<%$ Resources:Default, ProdNotRegLT12Hrs%>"></asp:Label>
                <asp:Image ID="ImageYellow3" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow3" runat="server" Text="<%$ Resources:Default, ProdGT12Hrs%>"></asp:Label>
                <asp:Image ID="ImageRed3" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed3" runat="server" Text="<%$ Resources:Default, ProdNotRegGT24Hrs%>"></asp:Label>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptDocument">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewReceiptDocument" runat="server" DataSourceID="ObjectDataSourceReceiptDocument"
                            DataKeyNames="ReceiptId,AllowPalletise,OrderNumber" AutoGenerateSelectButton="true"
                            AutoGenerateColumns="false" AllowPaging="true" OnSelectedIndexChanged="GridViewReceiptDocument_SelectedIndexChanged"
                            AutoGenerateEditButton="true" OnRowDataBound="GridViewReceiptDocument_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default, ManufacturingOrders%>' ReadOnly="true" />
                                <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default, ProductCode %>' ReadOnly="true" />
                                <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default, Product%>' ReadOnly="true" />
                                <asp:BoundField DataField="CreateDate" HeaderText='<%$ Resources:Default,CreateDate %>' ReadOnly="true" />
                                <asp:BoundField DataField="PlannedDeliveryDate" HeaderText='<%$ Resources:Default, PlannedManufactureDate%>' ApplyFormatInEditMode="False" ReadOnly="True" />
                                <asp:BoundField DataField="Batch" HeaderText='<%$ Resources:Default, BatchNumber%>' ReadOnly="true" />
                                <asp:TemplateField HeaderText='<%$ Resources:Default,Location %>'>
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DDLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RequiredQuantity" HeaderText='<%$ Resources:Default, PlannedYield%>' ReadOnly="true" />
                                <asp:BoundField DataField="Status" SortExpression="Status" HeaderText='<%$ Resources:Default, OrderStatus%>' ReadOnly="true" />
                                <asp:BoundField DataField="InterfaceStatus" SortExpression="InterfaceStatus" HeaderText='<%$ Resources:Default, InterfaceStatus%>' ReadOnly="true" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, DwellTime%>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewReceiptDocument" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceReceiptDocument" runat="server" TypeName="StationSheetReceiving"
                    SelectMethod="GetReceivingDocuments" UpdateMethod="UpdateReceipt">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="InboundDocumentTypeId" DefaultValue="-1" SessionField="InboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="InboundShipmentId" DefaultValue="-1" Type="Int32" SessionField="InboundShipmentId" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="ProductCode" SessionField="ProductCode" Type="String" />
                        <asp:SessionParameter Name="Batch" SessionField="Batch" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="locationId" DefaultValue="-1" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="AreaCode" Type="String" DefaultValue="R" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" TypeName="Priority"
                    SelectMethod="GetPriorities">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Details%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="border: 1px 1px"></td>
                        <td>
                            <%--<asp:Button ID="ButtonSave" runat="server" Text="Upload" OnClick="ButtonSave_Click">--%>
                            <asp:Button ID="ButtonUpload" runat="server" Text="<%$ Resources:Default, Upload%>"
                                OnClick="ButtonUpload_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptLine" OnLoad="UpdatePanelGridViewReceiptLine_Load">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewReceiptLine" runat="server" DataKeyNames="ReceiptLineId"
                            AllowPaging="true" PageSize="30" AutoGenerateEditButton="True" AutoGenerateSelectButton="True"
                            OnSelectedIndexChanged="GridViewReceiptLine_SelectedIndexChanged" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode%>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product%>"
                                    SortExpression="Product" />
                                <asp:BoundField DataField="RequiredQuantity" HeaderText='<%$ Resources:Default, PlannedYield%>' ReadOnly="true" />
                                <asp:BoundField DataField="ReceivedQuantity" ReadOnly="True" HeaderText="<%$ Resources:Default, ManufacturedYield%>"
                                    SortExpression="Quantity" />
                                <asp:BoundField DataField="AcceptedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, FilledYield%>"
                                    SortExpression="AcceptedQuantity" />
                                <asp:BoundField DataField="Remarks" ReadOnly="True" HeaderText="<%$ Resources:Default, Comments%>"
                                    SortExpression="Remarks" />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceReceiptLine" runat="server" TypeName="StationSheetReceiving"
                            SelectMethod="GetReceiptLines" OnSelecting="ObjectDataSourceReceiptLine_OnSelecting"
                            UpdateMethod="SetReceiptLines" OnUpdating="ObjectDataSourceReceiptLine_OnUpdating"
                            OnUpdated="ObjectDataSourceReceiptLine_Updated">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="inboundShipmentId" Type="Int32" />
                                <asp:Parameter Name="receiptId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="ReceiptLineId" Type="Int32" />
                                <asp:Parameter Name="AcceptedQuantity" Type="Decimal" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridViewReceiptLine" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <br />
                <asp:Button ID="ButtonInsert" runat="server" Text="<%$ Resources:Default, InsertException%>"
                    OnClick="ButtonInsert_Click" Style="Width: auto;"></asp:Button>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewExceptions">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewExceptions" runat="server" DataKeyNames="ExceptionId" DataSourceID="ObjectDataSourceException"
                            AutoGenerateEditButton="true" AutoGenerateDeleteButton="true" OnRowDeleting="GridViewExceptions_RowDeleting"
                            OnSelectedIndexChanged="GridViewException_SelectedIndexChanged" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default,AdjustmentCode %>">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceReason"
                                            DataValueField="ReasonId" DataTextField="Reason" SelectedValue='<%# Bind("ReasonId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceReason"
                                            DataValueField="ReasonId" DataTextField="Reason" SelectedValue='<%# Bind("ReasonId") %>'>
                                        </asp:DropDownList>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ReasonId" runat="server" Text='<%# Bind("ReasonId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="reason" HeaderText='<%$ Resources:Default, AdjustmentDescription%>'
                                    ReadOnly="true" />
                                <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default, AdjustmentQuantity%>'
                                    ReadOnly="false" />
                                <asp:BoundField DataField="Exception" HeaderText='<%$ Resources:Default, Comments%>'
                                    ReadOnly="false" />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
                            SelectMethod="GetReasonsByType">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="MOW" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceException" runat="server" TypeName="ReceiptException"
                            SelectMethod="GetReceiptExceptions" UpdateMethod="UpdateReceiptException" DeleteMethod="DeleteReceiptException"
                            InsertMethod="InsertReceiptException" OnUpdating="ObjectDataSourceException_OnUpdating">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="ReceiptLineID" SessionField="ReceiptLineID" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="ExceptionId" Type="Int32" />
                                <asp:SessionParameter Name="ReceiptLineID" SessionField="ReceiptLineID" Type="Int32" />
                                <asp:Parameter Name="Exception" Type="String" />
                                <asp:Parameter Name="ReasonId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="ReceiptLineID" SessionField="ReceiptLineID" Type="Int32" />
                                <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
                            </InsertParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="ExceptionId" Type="Int32" />
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonInsert" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="GridViewReceiptLine" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
