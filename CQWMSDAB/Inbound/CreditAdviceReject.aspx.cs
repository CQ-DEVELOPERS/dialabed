using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_CreditAdviceReject : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                GetNext();
            }

            Master.MsgText = "Page load successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_CreditAdviceReject" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ButtonCancel_Click
    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCancel_Click";
        try
        {
            Response.Redirect("ReceivingDocumentSearch.aspx");
            Master.MsgText = "Cancel"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_CreditAdviceReject" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonCancel_Click

    #region ObjectDataSourceCreditAdvice_OnSelecting
    protected void ObjectDataSourceCreditAdvice_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceCreditAdvice_OnSelecting";
        try
        {
            e.InputParameters["ReceiptLineId"] = int.Parse(Session["ReceiptLineId"].ToString());
            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_CreditAdviceReject" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ObjectDataSourceCreditAdvice_OnSelecting

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";

            Session["ReportName"] = "Credit Advice";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[6];

            // Create the ConnectionString report parameter
            string strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);

            // Create the InboundShipmentId report parameter
            string inboundShipmentId = Session["InboundShipmentId"].ToString();
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("InboundShipmentId", inboundShipmentId);

            // Create the ReceiptId report parameter
            string receiptId = Session["ReceiptId"].ToString();
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ReceiptId", receiptId);

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());


            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_CreditAdviceReject" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click

    #region ButtonEmail_Click
    protected void ButtonEmail_Click(object sender, EventArgs e)
    {

    }
    #endregion ButtonEmail_Click

    #region GetNext
    protected void GetNext()
    {
        theErrMethod = "GetNext";
        try
        {
            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList == null)
                Session.Remove("checkedList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("checkedList");
                    Response.Redirect("ReceivingDocumentSearch.aspx");
                }
                else
                {
                    if (index > 0)
                    {
                        Session["receiptLineId"] = rowList[0];

                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    DetailsViewCreditAdvice.DataBind();
                }
            }
            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_CreditAdviceReject" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GetNext

    #region ButtonNext_Click
    protected void ButtonNext_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonNext_Click";
        try
        {
            GetNext();

            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_CreditAdviceReject" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonNext_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "CreditAdviceReject", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "CreditAdviceReject", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}