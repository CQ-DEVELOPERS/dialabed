using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_ManualPalletise : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region  Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            //Sets the menu for Receiving
            Session["MenuId"] = 3;

            if (GridViewReceiptLine.Rows.Count < 1)
                GetNextReceiptLine();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualLocationAllocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region GetNextReceiptLine 
    protected void GetNextReceiptLine()
    {
        theErrMethod = "GetNextReceiptLine";

        try
        {
            ArrayList rowList = (ArrayList)Session["receiptList"];

            if (rowList == null)
            {
                Response.Redirect("ReceivingPalletise.aspx?ActiveTab=1");
                Session.Remove("receiptList");
            }
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("receiptList");
                    Response.Redirect("ReceivingPalletise.aspx?ActiveTab=1");
                }
                else
                {
                    ManualPalletise ds = new ManualPalletise();
                    
                    if (index > 0)
                    {
                        Session["ReceiptLineId"] = Convert.ToInt32(rowList[0].ToString());

                        GridViewReceiptLine.DataBind();
                        
                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    GridViewReceiptLine.DataBind();

                    if (GridViewReceiptLine.Rows.Count > 0)
                    {
                        GridViewInstruction.DataBind();
                        TextBoxLines.Enabled = true;
                        TextBoxLines.Text = "1";
                        ButtonCreateLines.Enabled = true;
                    }
                }
            }
            Master.MsgText = "Next Line"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualLocationAllocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GetNextReceiptLine

    #region ButtonCreateLines_Click 
    protected void ButtonCreateLines_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCreateLines_Click";
        try
        {
            if (TextBoxLines.Text == "" || TextBoxLines.Text == "0")
                return;

            ManualPalletise ds = new ManualPalletise();

            if (ds.CreateLines(Session["ConnectionStringName"].ToString(),
                                int.Parse(Session["ReceiptLineId"].ToString()),
                                int.Parse(TextBoxLines.Text)) == true)
            {
                GridViewInstruction.DataBind();
                TextBoxLines.Enabled = false;
                ButtonCreateLines.Enabled = false;
                Master.MsgText = "New Line";
            }
            else
                Master.ErrorText = "Error on: " + GridViewReceiptLine.Rows[0].Cells[0].Text;

             
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualLocationAllocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonCreateLines_Click

    #region GetEditIndex 
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(GridViewInstruction.DataKeys[index].Values["InstructionId"]);
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualLocationAllocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GetEditIndex

    #region ButtonAutoLocations_Click 
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";
        try
        {
            GetEditIndex();
            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList == null)
                Session.Remove("checkedList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("checkedList");
                }
                else
                {
                    ManualPalletise ds = new ManualPalletise();

                    while (index > 0)
                    {
                        if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), Convert.ToInt32(rowList[0])) == false)
                            Master.ErrorText = "Please try again";

                        rowList.Remove(rowList[0]);

                        index--;
                    }
                    
                    GridViewInstruction.DataBind();
                    Master.MsgText = "Allocated"; 
                }
            }
         
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualLocationAllocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations_Click
    
    #region   ButtonManualLocations_Click 
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Inbound/ManualPalletise.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualLocationAllocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonManualLocations_Click

    #region GetNextReceiptLine_Click 
    protected void GetNextReceiptLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "GetNextReceiptLine_Click";
        
        try
        {
        
            GetNextReceiptLine();

            Master.MsgText = "Next Line"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualLocationAllocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GetNextReceiptLine_Click

    #region ButtonReturn_Click
    protected void ButtonReturn_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReturn_Click";

        try
        {
            if (Session["receiptList"] != null)
                Session.Remove("receiptList");

            Response.Redirect("ReceivingPalletise.aspx?ActiveTab=1");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonReturn_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ManualLocationAllocation", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ManualLocationAllocation", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
