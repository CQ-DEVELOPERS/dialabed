using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_ReceivingPalletise : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

                if (Request.QueryString["ActiveTab"] != null)
                    if (Request.QueryString["ActiveTab"].ToString() == "1")
                        Tabs.ActiveTabIndex = 1;
                
                ButtonAutoLocations.Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 21);
                ButtonPalletise.Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 29);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page Load

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";
        try
        {
            GridViewReceiptDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDocumentSearch_Click

    #region DataSourceReceiptDocument_OnSelecting 
    protected void ObjectDataSourceReceiptDocument_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
    }
    #endregion DataSourceReceiptDocument_OnSelecting

    #region GridViewReceiptDocument_SelectedIndexChanged 
    protected void GridViewReceiptDocument_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewReceiptDocument_SelectedIndexChanged";
        try
        {
            Session["InboundShipmentId"] = GridViewReceiptDocument.DataKeys[GridViewReceiptDocument.SelectedIndex].Values["InboundShipmentId"];
            Session["ReceiptId"] = GridViewReceiptDocument.SelectedDataKey["ReceiptId"];

            if (Session["InboundShipmentId"].ToString() == "")
                Session["InboundShipmentId"] = -1;

            GridViewReceiptLine.DataBind();

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion GridViewReceiptDocument_SelectedIndexChanged

    #region GetEditIndex 
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewReceiptLine.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    rowList.Add(GridViewReceiptLine.DataKeys[row.RowIndex].Values["ReceiptLineId"]);
            }

            if (rowList.Count > 0)
                Session["receiptList"] = rowList;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GetEditIndex

    #region ButtonAutoLocations_Click 
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";
        try
        {
            GetEditIndex();
            ArrayList rowList = (ArrayList)Session["receiptList"];

            if (rowList == null)
                Session.Remove("receiptList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("receiptList");
                }
                else
                {
                    Receiving ds = new Receiving();

                    while (index > 0)
                    {
                        if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], Convert.ToInt32(rowList[0])) == false)
                            Master.MsgText = "There was an error!!!";
                        else
                            Master.MsgText = "Locations Allocated Successfully";

                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    Session.Remove("receiptList");

                    GridViewReceiptLine.DataBind();
                }
            }

            //Master.MsgText = "Auto Location"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonAutoLocations_Click

    #region ButtonPalletise_Click 
    protected void ButtonPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletise_Click";

        Master.MsgText = "Palletise"; Master.ErrorText = "";

        try
        {
            GetEditIndex();
            ArrayList rowList = (ArrayList)Session["receiptList"];

            if (rowList == null)
                Session.Remove("receiptList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("receiptList");
                }
                else
                {
                    Receiving ds = new Receiving();

                    while (index > 0)
                    {
                        if (ds.Palletise(Session["ConnectionStringName"].ToString(), int.Parse(GridViewReceiptDocument.SelectedDataKey["ReceiptId"].ToString()), Convert.ToInt32(rowList[0])) == false)
                            Master.MsgText = "There was an error.";

                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    Session.Remove("receiptList");

                    GridViewReceiptLine.DataBind();
                }
            }

            //Master.MsgText = "Palletise"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPalletise_Click

    #region ButtonManualLocations_Click 
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            GetEditIndex();
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonManualLocations_Click

    #region ButtonManualPalletise_Click 
    protected void ButtonManualPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualPalletise_Click";

        try
        {
            ArrayList rowList = (ArrayList)Session["receiptList"];
            Session["checkedList"] = rowList;
            Session.Remove("receiptList");

            GetEditIndex();
            Response.Redirect("ManualPalletise.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonManualPalletise_Click

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewReceiptLine.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "InboundDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "InboundDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
