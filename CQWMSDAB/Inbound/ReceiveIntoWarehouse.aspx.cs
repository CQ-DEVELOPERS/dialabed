using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_ReceiveIntoWarehouse : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    private int jobId = -1;
    private int instructionId = -1;
    private int reasonId = -1;
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

                //Form.Attributes.Add("onKeyPress", "myKeyHandler()");
                HiddenFieldProduct.Value = "false";
                HiddenFieldBatch.Value = "false";
                HiddenFieldQuantity.Value = "false";
                HiddenFieldWeight.Value = "false";

                ButtonReject.Visible = false;

                TextBoxBarcode.Focus();
            }
            
            if (DetailsViewInstruction.Rows.Count > 0)
            {
                jobId = int.Parse(DetailsViewInstruction.DataKey["JobId"].ToString());
                instructionId = int.Parse(DetailsViewInstruction.DataKey["InstructionId"].ToString());
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ResetPage
    protected void ResetPage()
    {
        HiddenFieldProduct.Value = "false";
        HiddenFieldBatch.Value = "false";
        HiddenFieldQuantity.Value = "false";
        HiddenFieldWeight.Value = "false";

        ButtonEnter.Visible = true;
        ButtonReject.Visible = false;
        TextBoxBarcode.Text = "";
        MultiView1.ActiveViewIndex = 0;
        DetailsViewInstruction.Visible = false;
        
        TextBoxBarcode.Focus();
    }
    #endregion ResetPage

    #region ButtonEnter_Click
    protected void ButtonEnter_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEnter_Click";

        try
        {
            if (MultiView1.ActiveViewIndex == 0)
            {
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 56))
                {
                    DetailsViewInstruction.Visible = false;
                    DetailsViewInstruction.DataBind();
                }
                else
                {
                    DetailsViewInstruction.Visible = true;
                    DetailsViewInstruction.DataBind();
                }

                if (DetailsViewInstruction.Rows.Count > 0)
                {
                    CheckNext();
                    ButtonReject.Visible = true;
                }
                else
                    Master.ErrorText = Resources.Messages.ErrorRetrieving;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEnter_Click

    #region ButtonAccept_Click
    protected void ButtonAccept_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAccept_Click";

        try
        {
            ReceiveIntoWarehouse rec = new ReceiveIntoWarehouse();

            if (rec.Accept(Session["ConnectionStringName"].ToString(), jobId, (int)Session["OperatorId"]))
            {
                PrintAccept();
                ResetPage();
                Master.MsgText = Resources.Default.Successful;
                Master.ErrorText = "";
            }
            else
            {
                Master.MsgText = "";
                Master.ErrorText = Resources.Messages.ErrorAccepting;
            }

            TextBoxBarcode.Focus();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAccept_Click

    #region ButtonReject_Click
    protected void ButtonReject_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReject_Click";

        try
        {
            DetailsViewInstruction.Visible = false;
            MultiView1.ActiveViewIndex = 6;
            ButtonReject.Visible = false;
            ButtonEnter.Visible = false;
            //ResetPage();
            //Response.Redirect("~/Inbound/ReceiveIntoWarehouse.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonReject_Click

    #region ButtonRejectReason_Click
    protected void ButtonRejectReason_Click(object sender, EventArgs e)
    {
        theErrMethod = "Reject";

        try
        {
            ReceiveIntoWarehouse rec = new ReceiveIntoWarehouse();

            reasonId = int.Parse(RadioButtonListReason.SelectedValue.ToString());

            if (rec.Reject(Session["ConnectionStringName"].ToString(), jobId, reasonId, (int)Session["OperatorId"]))
            {
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 195))
                {
                    PrintReject();
                }
                ResetPage();
            }
            else
            {
                Master.ErrorText = Resources.Messages.ErrorRejecting;
            }

            MultiView1.ActiveViewIndex = 0;

            TextBoxBarcode.Focus();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonRejectReason_Click

    #region PrintAccept
    protected void PrintAccept()
    {
        theErrMethod = "PrintAccept";

        try
        {
            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 195))
            {
                ArrayList checkedList = new ArrayList();
                checkedList.Add(instructionId);

                Session["checkedList"] = checkedList;

                Status status = new Status();

                if (!status.Print(Session["ConnectionStringName"].ToString(), instructionId, (int)Session["OperatorId"]))
                {
                    Master.ErrorText = "Please try again";
                    return;
                }

                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 390))
                    Session["LabelName"] = "Pallet Label Small.lbl";
                else
                    Session["LabelName"] = "Pallet Label.lbl";

                Session["FromURL"] = "~/Inbound/ReceiveIntoWarehouse.aspx";

                if (Session["Printer"] == null)
                    Session["Printer"] = "";

                var sessionPort = Session["Port"];
                var sessionPrinter = Session["Printer"];
                var sesionIndicatorList = Session["indicatorList"];
                var sessionCheckedList = Session["checkedList"];
                var sessionLocationList = Session["locationList"];
                var sessionConnectionString = Session["ConnectionStringName"];
                var sessionTitle = Session["Title"];
                var sessionBarcode = Session["Barcode"];
                var sessionWarehouseId = Session["WarehouseId"];
                var sessionProductList = Session["ProductList"];
                var sessionTakeOnLabel = Session["TakeOnLabel"];
                var sessionLabelCopies = Session["LabelCopies"];

                NLWAXPrint nl = new NLWAXPrint();
                nl.onSessionUpdate += Nl_onSessionUpdate;
                string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                          Session["LabelName"].ToString(),
                                                          sessionPrinter,
                                                          sessionPort,
                                                          sesionIndicatorList,
                                                          sessionCheckedList,
                                                          sessionLocationList,
                                                          sessionConnectionString,
                                                          sessionTitle,
                                                          sessionBarcode,
                                                          sessionWarehouseId,
                                                          sessionProductList,
                                                          sessionTakeOnLabel,
                                                          sessionLabelCopies);
                if (printResponse == string.Empty)
                {
                    Master.ErrorText = "An error has occurred! For more details please check automation manager.";
                }
                else
                {
                    Master.MsgText = "Label has been printed successfuly.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
    #endregion PrintAccept

    #region PrintReject
    protected void PrintReject()
    {
        theErrMethod = "PrintAccept";

        try
        {
            ArrayList checkedList = new ArrayList();
            checkedList.Add(jobId);

            Session["checkedList"] = checkedList;

            Session["LabelName"] = "Reject Label.lbl";

            Session["FromURL"] = "~/Inbound/ReceiveIntoWarehouse.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }
    #endregion PrintReject

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceiveIntoWarehouse", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceiveIntoWarehouse", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region CheckNext
    protected void CheckNext()
    {
        theErrMethod = "CheckNext";

        try
        {
            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 57))
            {
                if (HiddenFieldProduct.Value == "false")
                {
                    MultiView1.ActiveViewIndex = 1;
                    TextBoxProduct.Focus();
                    return;
                }
            }
            else
            {
                HiddenFieldProduct.Value = "true";
            }

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 58))
            {
                if (HiddenFieldBatch.Value == "false")
                {
                    MultiView1.ActiveViewIndex = 2;
                    TextBoxBatch.Focus();
                    return;
                }
            }
            else
            {
                HiddenFieldBatch.Value = "true";
            }

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 59))
            {
                if (HiddenFieldQuantity.Value == "false")
                {
                    MultiView1.ActiveViewIndex = 3;
                    TextBoxQuantity.Focus();
                    return;
                }
                //else
                //{
                //    MultiView1.ActiveViewIndex = 4;
                //}
            }
            else
            {
                HiddenFieldQuantity.Value = "true";
                MultiView1.ActiveViewIndex = 4;
            }

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 182))
            {
                if (HiddenFieldWeight.Value == "false")
                {
                    MultiView1.ActiveViewIndex = 4;
                    TextBoxWeight.Focus();
                }
                else
                {
                    MultiView1.ActiveViewIndex = 5;
                    ButtonEnter.Visible = false;
                }
            }
            else
            {
                HiddenFieldWeight.Value = "true";
                MultiView1.ActiveViewIndex = 5;
                ButtonEnter.Visible = false;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion CheckNext

    #region TextBoxProduct_TextChanged
    protected void TextBoxProduct_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxProduct_TextChanged";

        try
        {
            Transact tran = new Transact();
            if (tran.ConfirmProduct(Session["ConnectionStringName"].ToString(), instructionId, TextBoxProduct.Text, "RCV") == 0)
            {
                Master.MsgText = Resources.Messages.ConfirmProductPassed;
                Master.ErrorText = "";
                HiddenFieldProduct.Value = "true";
                //TextBoxProduct.Enabled = false;
                CheckNext();
            }
            else
            {
                Master.MsgText = "";
                Master.ErrorText = Resources.Messages.ConfirmProductFailed;
                HiddenFieldProduct.Value = "false";
            }

            TextBoxProduct.Text = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxProduct_TextChanged

    #region TextBoxBatch_TextChanged
    protected void TextBoxBatch_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxBatch_TextChanged";

        try
        {
            Transact tran = new Transact();
            if (tran.ConfirmBatch(Session["ConnectionStringName"].ToString(), instructionId, TextBoxBatch.Text) == 0)
            {
                Master.MsgText = Resources.Messages.ConfirmBatchPassed;
                Master.ErrorText = "";
                HiddenFieldBatch.Value = "true";
                //TextBoxBatch.Enabled = false;
                CheckNext();
            }
            else
            {
                Master.MsgText = "";
                Master.ErrorText = Resources.Messages.ConfirmBatchFailed;
                HiddenFieldBatch.Value = "false";
            }

            TextBoxBatch.Text = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxBatch_TextChanged

    #region TextBoxQuantity_TextChanged
    protected void TextBoxQuantity_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxQuantity_TextChanged";

        try
        {
            Transact tran = new Transact();
            Decimal quantity = 0;

            if (Decimal.TryParse(TextBoxQuantity.Text, out quantity))
            {
                if (tran.ConfirmQuantity(Session["ConnectionStringName"].ToString(), instructionId, quantity) == 0)
                {
                    Master.MsgText = Resources.Messages.ConfirmQuantityPassed;
                    Master.ErrorText = "";
                    HiddenFieldQuantity.Value = "true";
                    //TextBoxQuantity.Enabled = false;
                    CheckNext();
                }
                else
                {
                    Master.MsgText = "";
                    Master.ErrorText = Resources.Messages.ConfirmQuantityFailed;
                    HiddenFieldQuantity.Value = "false";
                }
            }

            TextBoxQuantity.Text = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxQuantity_TextChanged

    #region TextBoxWeight_TextChanged
    protected void TextBoxWeight_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxWeight_TextChanged";

        try
        {
            Transact tran = new Transact();
            Decimal weight = 0;

            if (Decimal.TryParse(TextBoxWeight.Text, out weight))
            {
                if (tran.ConfirmPalletWeight(Session["ConnectionStringName"].ToString(), instructionId, weight) == 0)
                {
                    Master.MsgText = Resources.Messages.ConfirmWeightPassed;
                    Master.ErrorText = "";
                    HiddenFieldWeight.Value = "true";
                    //TextBoxWeight.Enabled = false;
                    CheckNext();
                }
                else
                {
                    Master.MsgText = "";
                    Master.ErrorText = Resources.Messages.ConfirmWeightFailed;
                    HiddenFieldWeight.Value = "false";
                }
            }

            TextBoxWeight.Text = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxWeight_TextChanged
}
