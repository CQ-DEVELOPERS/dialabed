<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BatchReceive.aspx.cs" Inherits="Inbound_BatchReceive" Title="<%$ Resources:Default, BatchReceiveTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, BatchReceiveTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, BatchReceiveAganda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div style="text-align: center; width: 800px">
        <table>
            <tr>
                <td>
                    <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, BatchReceiveScan %>"></asp:Label>
                    <br />
                    <asp:TextBox ID="TextBoxBarcode" runat="server" EnableTheming="false" Width="200px"
                        Font-Size="X-Large"></asp:TextBox>
                    <br />
                    <br />
                    <br />
                    <asp:Button ID="ButtonAccept" runat="server" Text="<%$ Resources:Default, BatchReceiveAccept %>"
                        OnClick="ButtonAccept_Click" EnableTheming="false" Height="80px" Width="200px"
                        Font-Size="X-Large" style="Width:auto;"/>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
