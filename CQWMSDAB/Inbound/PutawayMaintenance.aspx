<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" 
    CodeFile="PutawayMaintenance.aspx.cs" Inherits="Inbound_PutawayMaintenance" Title="<%$ Resources:Default, PutawayMaintenanceTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PutawayMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PutawayMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, PutawayMaintenanceTitle %>">
            <ContentTemplate>
                <asp:Panel ID="PanelSearch" runat="server" Width="650px" BackColor="#EFEFEC">
                    <table>
                        <tr>
                            <td>
                                <uc1:DateRange ID="DateRange1" runat="server" />
                                <asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="<%$ Resources:Default, ButtonSearch %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default, SKUCode%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxSKUCode" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxBatch" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelPalletId" runat="server" Text="<%$ Resources:Default, PalletId%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxPalletId" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelJobId" runat="server" Text="<%$ Resources:Default, JobId%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxJobId" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelStatus" runat="server" Text="<%$ Resources:Default, Status%>" Width="100px"></asp:Label>
                                <asp:DropDownList ID="DropDownListStatus" runat="server" DataTextField="Status" DataValueField="StatusId"
                                    DataSourceID="ObjectDataSourceStatus" Width="150px">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
                                    SelectMethod="GetStatus">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:Parameter Name="Type" Type="string" DefaultValue="PR" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers" TargetControlID="TextBoxPalletId">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE2" runat="server"                    FilterType="Numbers" TargetControlID="TextBoxJobId">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                    TargetControlID="PanelSearch"
                    Radius="10"
                    Color="#EFEFEC"
                    BorderColor="#404040"
                    Corners="All" />
                <table cellpadding="5pt" cellspacing="10pt">
                    <tr>
                        <td >
                            <asp:Button ID="ButtonPrintLabel" runat="server" OnClick="ButtonPrintLabel_Click" Text="<%$ Resources:Default, PrintLabel%>" />
                            <asp:Button ID="ButtonPrint" runat="server" OnClick="ButtonPrint_Click" Text="<%$ Resources:Default, PutawaySheet%>" />
                            <asp:Button ID="ButtonSelect" runat="server" OnClick="ButtonSelect_Click" Text="<%$ Resources:Default, SelectAll%>" />
                            <asp:Button ID="ButtonEdit" runat="server" OnClick="ButtonEdit_Click" Text="<%$ Resources:Default, Edit%>" />
                            <asp:Button ID="ButtonSave" runat="server" OnClick="ButtonSave_Click" Text="<%$ Resources:Default, Save%>" />
                            <%--<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonSave" runat="server" ConfirmText="Press OK to save." TargetControlID="ButtonSave"></ajaxToolkit:ConfirmButtonExtender>--%>
                        <%--</td>
                        <td >--%>
                            <asp:Button ID="ButtonSplit" runat="server" OnClick="ButtonSplit_Click" Text="<%$ Resources:Default, SplitLine%>" />
                            <asp:Button ID="ButtonLocation" runat="server" OnClick="ButtonLocation_Click" Text="<%$ Resources:Default, ChangeLocation%>" />
                            <asp:Button ID="ButtonSerial" runat="server" OnClick="ButtonSerial_Click" Text="<%$ Resources:Default, SerialNumbers%>" />
                        <%--</td>
                        <td >--%>
                            <asp:Button ID="ButtonReset" runat="server" OnClick="ButtonReset_Click" Text="<%$ Resources:Default, ResetStatus%>" />
                            <asp:Button Visible="false" ID="ButtonComplete" runat="server" OnClick="ButtonComplete_Click" Text="<%$ Resources:Default, Complete%>" />
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel ID="UpdatePanelGridViewPutAway" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewPutAway" runat="server" AllowSorting="true" AllowPaging="true" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePutAway"
                            DataKeyNames="InstructionId,PalletId, JobId">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" ReadOnly="True" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="Quantity">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" SortExpression="ConfirmedQuantity">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" ReadOnly="True" HeaderText="<%$ Resources:Default, JobId %>" SortExpression="JobId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PickLocation" ReadOnly="True" HeaderText="<%$ Resources:Default, PickLocation %>" SortExpression="PickLocation">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="StoreLocation" ReadOnly="True" HeaderText="<%$ Resources:Default, StoreLocation %>" SortExpression="StoreLocation">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="StoreArea" ReadOnly="True" HeaderText="<%$ Resources:Default, Area %>" SortExpression="StoreArea">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PalletId" ReadOnly="True" HeaderText="<%$ Resources:Default, PalletId %>" SortExpression="PalletId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DwellTime" ReadOnly="True" HeaderText="<%$ Resources:Default, DwellTime %>" SortExpression="DwellTime">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CreateDate" ReadOnly="True" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" ReadOnly="True" HeaderText="<%$ Resources:Default, InstructionType %>" SortExpression="InstructionType">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Operator %>" SortExpression="OperatorId">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                                            DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Operator") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
								<asp:BoundField DataField="PalletIdStatusId" Visible="false"></asp:BoundField>
                                <asp:TemplateField HeaderText="Pallet Status" SortExpression="PalletIdStatus">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPalletStatus" runat="server" DataSourceID="ObjectDataSourcePalletStatus"
                                            DataTextField="Status" DataValueField="PalletIdStatusId" SelectedValue='<%# Bind("PalletIdStatusId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelPalletStatus" runat="server" Text='<%# Bind("PalletIdStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourcePutAway" runat="server" TypeName="PutAwayMaintenance"
                            SelectMethod="GetInstructions"
                            UpdateMethod="UpdateInstructions">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                                <asp:ControlParameter ControlID="TextBoxPalletId" Name="palletId" ConvertEmptyStringToNull="true" DefaultValue="-1" PropertyName="Text" Type="Int32" />
                                <asp:ControlParameter ControlID="TextBoxJobId" Name="jobId" ConvertEmptyStringToNull="true" DefaultValue="-1" PropertyName="Text" Type="Int32" />
                                <asp:SessionParameter Name="FromDate" Type="DateTime" SessionField="FromDate" />
                                <asp:SessionParameter Name="ToDate" Type="DateTime" SessionField="ToDate" />
                                <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="0" Name="statusId" PropertyName="SelectedValue" Type="Int32" />
                                <asp:ControlParameter ControlID="TextBoxProductCode" Name="productCode" DefaultValue="%" PropertyName="Text" Type="string" />
                                <asp:ControlParameter ControlID="TextBoxProduct" Name="product" DefaultValue="%" PropertyName="Text" Type="string" />
                                <asp:ControlParameter ControlID="TextBoxSKUCode" Name="skuCode" DefaultValue="%" PropertyName="Text" Type="string" />
                                <asp:ControlParameter ControlID="TextBoxBatch" Name="batch" DefaultValue="%" PropertyName="Text" Type="string" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="instructionId" Type="Int32" />
                                <asp:Parameter Name="confirmedQuantity" Type="Decimal" />
                                <asp:Parameter Name="operatorId" Type="Int32" />
								<asp:Parameter Name="PalletId" Type="Int32" />
								<asp:Parameter Name="PalletIdStatusId" Type="Int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
                            SelectMethod="GetOperators">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
						 <asp:ObjectDataSource ID="ObjectDataSourcePalletStatus" runat="server" TypeName="Status"
                            SelectMethod="GetPalletStatus">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonReset" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonComplete" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPrint" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
