using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_InboundDocument : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        theErrMethod = "Page Load";

        if (!Page.IsPostBack)
        {
            try
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                Session["ExternalCompanyId"] = null;

                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                string pageType = Request.QueryString["pageType"];

                if (pageType == "" || pageType == null)
                    pageType = "Edit";

                if (pageType == "Insert")
                {
                    Session["InboundDocumentId"] = null;
                    Title = "Inbound Document Insert";
                    LabelHeading.Text = "Inbound Document Insert";
                    //TabPanel1.HeaderText = "";
                   // TabPanel2.HeaderText = "Insert";
                    TabPanel1.Enabled = false;

                    DetailsViewInboundDocument.ChangeMode(DetailsViewMode.Insert);
                }
                DetailsViewInboundDocument.ChangeMode(DetailsViewMode.Insert);

                Master.MsgText = "Page load successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
                Master.MsgText = result; Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            GridViewInboundDocument.DataBind();

            Master.MsgText = "GridView Succussful";
            Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonInsertLine_Click
    protected void ButtonInsertLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInsertLine_Click";
        
        try
        {
            if (Session["StorageUnitId"] == null || (int)Session["StorageUnitId"] == -1)
            {
                Master.ErrorText = "Please select a Product.";
                LabelErrorMsg.Text = "Please select a Product.";
                return;
            }

            if (TextBoxQuantity.Text == "" || TextBoxQuantity.Text == "0")
            {
                Master.ErrorText = "Please enter a Quantity.";
                LabelErrorMsg.Text = "Please enter a Quantity.";
                return;
            }

            Decimal quantity = Decimal.Parse(TextBoxQuantity.Text);

            if (Session["InboundDocumentId"] == null)
            {
                Master.ErrorText = "Please select a Document.";
                LabelErrorMsg.Text = "Please select a Document.";
                return;
            }

            InboundDocument outboundDocument = new InboundDocument();

            if (Session["BatchId"] == null)
                Session["BatchId"] = -1;

            if (Session["ReasonId"] == null)
                Session["ReasonId"] = -1;

            if (outboundDocument.CreateInboundLine(Session["ConnectionStringName"].ToString(),
                                                    int.Parse(Session["OperatorId"].ToString()),
                                                    int.Parse(Session["InboundDocumentId"].ToString()),
                                                    int.Parse(Session["StorageUnitId"].ToString()),
                                                    quantity,
                                                    int.Parse(Session["BatchId"].ToString()),
                                                    int.Parse(Session["ReasonId"].ToString())))
            {
                TextBoxQuantity.Text = "";
                Session["StorageUnitId"] = null;
                Session["BatchId"] = null;
                Master.ErrorText = "";
                LabelErrorMsg.Text = "";

                GridViewInboundLine.DataBind();

                Master.MsgText = "InsertLine";
                Master.ErrorText = "";
            }
            else
            {
                Master.ErrorText = "May not enter duplicate Product /  Batch Combination";
                LabelErrorMsg.Text = "May not enter duplicate Product /  Batch Combination";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonInsertLine_Click"

    #region ButtonDeleteLine_Click
    protected void ButtonDeleteLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeleteLine_Click";
        try
        {
            if (GridViewInboundLine.SelectedIndex != -1)
                GridViewInboundLine.DeleteRow(GridViewInboundLine.SelectedIndex);

            Master.MsgText = "Deleted"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDeleteLine_Click"

    #region GridViewInboundDocument_OnSelectedIndexChanged
    protected void GridViewInboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewInboundDocument.SelectedIndex != -1)
            {
                Session["InboundDocumentId"] = int.Parse(GridViewInboundDocument.SelectedDataKey["InboundDocumentId"].ToString());
                Session["ExternalCompanyId"] = int.Parse(GridViewInboundDocument.SelectedDataKey["ExternalCompanyId"].ToString());
            }

            DetailsViewInboundDocument.DataBind();

            DetailsViewInboundDocument.ChangeMode(DetailsViewMode.ReadOnly);

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "GridViewInboundDocument_OnSelectedIndexChanged"

    #region DetailsViewInboundDocument_OnModeChanging
    protected void DetailsViewInboundDocument_OnModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        theErrMethod = "DetailsViewInboundDocument_OnModeChanging";
        try
        {
            if (e.NewMode == DetailsViewMode.Insert)
                Session["ExternalCompanyId"] = null;

            Master.MsgText = "Document Changed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion DetailsViewInboundDocument_OnModeChanging

    #region DetailsViewInboundDocument_OnItemInserting
    protected void DetailsViewInboundDocument_OnItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        theErrMethod = "DetailsViewInboundDocument_OnItemInserting";
        try
        {
            if (Session["ExternalCompanyId"] == null || (int)Session["ExternalCompanyId"] == -1)
            {
                Master.ErrorText = "Please select a Supplier first.";
                LabelErrorTab2.Text = "Please select a Supplier first";
                e.Cancel = true;
            }
            else
                LabelErrorTab2.Text = "";

            Master.MsgText = "Item Insert"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion DetailsViewInboundDocument_OnItemInserting

    #region ObjectDataSourceInboundDocumentUpdate_OnInserted
    protected void ObjectDataSourceInboundDocumentUpdate_OnInserted(object source, ObjectDataSourceStatusEventArgs e)
    {
        theErrMethod = "ObjectDataSourceInboundDocumentUpdate_OnInserted";
        try
        {
            if (e.ReturnValue.ToString() != "0")
            {
                Session["InboundDocumentId"] = e.ReturnValue;
                DetailsViewInboundDocument.ChangeMode(DetailsViewMode.ReadOnly);
                DetailsViewInboundDocument.DataBind();
            }
            else
            {
                ButtonChangeMode.Visible = true;
                LabelErrorTab2.Text = "Duplicate Order Number and Supplier Combination.";
            }
            Master.MsgText = "Updated"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceInboundDocumentUpdate_OnInserted

    #region ButtonChangeMode_Click
    protected void ButtonChangeMode_Click(object sender, EventArgs e)
    {
        DetailsViewInboundDocument.ChangeMode(DetailsViewMode.Insert);
        ButtonChangeMode.Visible = false;
        LabelErrorTab2.Text = "";
    }
    #endregion ButtonChangeMode_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "InboundDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + exMsg.Message.ToString());
                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
   #region DetailsViewInboundDocument_OnDataBound
    protected void DetailsViewOutboundDocument_OnDataBound(object sender, EventArgs e)
    {
        try
        {
            string returnType = "RET";

            if (Session["ReturnType"] != null)
                returnType = Session["ReturnType"].ToString();

            switch (DetailsViewInboundDocument.CurrentMode.ToString())
            {
                case "ReadOnly":
                    break;
                case "Insert":
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 259))
                    {
                        ((TextBox)(DetailsViewInboundDocument.FindControl("TextBoxOrderNumberInsert"))).Enabled = false;
                        ((RequiredFieldValidator)(DetailsViewInboundDocument.FindControl("RequiredFieldValidatorOrderNumberInsert"))).Enabled = false;

                    }

                    break;
                case "Edit":
                    break;
            }

            if (DetailsViewInboundDocument.Rows.Count == 0)
                ButtonChangeMode.Visible = true; //DetailsViewInboundDocument.ChangeMode(DetailsViewMode.Insert);
        }
        catch { }
    }
    #endregion DetailsViewInboundDocument_OnDataBound
}
