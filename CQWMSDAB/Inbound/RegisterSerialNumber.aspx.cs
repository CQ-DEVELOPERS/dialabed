using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_RegisterSerialNumber : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (CheckBoxRefernceNumber.Checked)
            {
                TextBoxReferenceNumber.Enabled = true;
                TextBoxReferenceNumber.Visible = true;
            }
            else
            {
                TextBoxReferenceNumber.Text = "";
                TextBoxReferenceNumber.Enabled = false;
                TextBoxReferenceNumber.Visible = false;
            }

            //Sets the menu for Receiving
            Session["MenuId"] = 3;

            FormViewSerialNumber.DataBind();
            RepeaterSerialNumber.DataBind();

            ScriptManager.GetCurrent(this.Page).SetFocus(TextBoxSerialNumber);

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_RegisterSerialNumber" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page Load

    #region ButtonAdd_Click
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAdd_Click";
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            SerialNumber ds = new SerialNumber();

            int receiptId = (int)FormViewSerialNumber.DataKey.Values["ReceiptId"];
            int receiptLineId = (int)FormViewSerialNumber.DataKey.Values["ReceiptLineId"];
            int storageUnitId = (int)FormViewSerialNumber.DataKey.Values["StorageUnitId"];
            int batchId = (int)FormViewSerialNumber.DataKey.Values["BatchId"];
            Session["ReceiptLineId"] = receiptLineId;

            string a = TextBoxSerialNumber.Text;

            string[] delimiter = { Environment.NewLine };

            string[] b = a.Split(delimiter, StringSplitOptions.None);

            foreach (string serialNumber in b)
            {
                if (!ds.RegisterSerialNumber(Session["ConnectionStringName"].ToString(), receiptId, receiptLineId, serialNumber, storageUnitId, batchId, TextBoxReferenceNumber.Text))
                    Master.ErrorText = "There was an error inserting the Serial Number.";
                else
                {
                    Master.MsgText = "Serial Number Successfully Added";
                    TextBoxSerialNumber.Text = "";
                }
            }
            RepeaterSerialNumber.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_RegisterSerialNumber" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAdd_Click

    #region ButtonBack_Click
    protected void ButtonBack_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonBack_Click";
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            if (Session["FromURL"] != null)
            {
                Response.Redirect(Session["FromURL"].ToString());
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_RegisterSerialNumber" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonBack_Click
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "StockTakeCreateLocation", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "StockTakeCreateLocation", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
