using Microsoft.Reporting.WebForms;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Telerik.GridExamplesCSharp.Programming.SavingGridSettingsOnPerUserBasis
{
	public partial class DefaultCS : System.Web.UI.Page
	{
		#region InitializeCulture
		protected virtual void InitializeCulture()
		{
			try
			{
				if (Session["CultureName"] != null)
				{
					System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
					System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
				}
			}
			catch { }
		}
		#endregion "InitializeCulture"

		#region Private Variables
		private string result = "";
		private string theErrMethod = "";
		#endregion Private Variables

		#region Page_Load
		protected void Page_Load(object sender, EventArgs e)
		{
			theErrMethod = "Page Load";

			try
			{
				if (!Page.IsPostBack)
				{

					PrintMutipleCheckSheets();
					if (Session["FromDate"] == null) Session["FromDate"] = DateTime.Now;
					if (Session["ToDate"] == null) Session["ToDate"] = DateTime.Now;

					//if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 176))
					//    ConfirmButtonExtenderButtonReceived.ConfirmText = "Press OK to Confirm as Received";
				}

				Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "Page_Load"

		#region Page_LoadComplete
		protected void Page_LoadComplete(object sender, EventArgs e)
		{
			try
			{
				if (Session["FromURL"] != null)
				{
					Session["FromURL"] = null;
					if (Session["ReceiptId"] != null)
					{
						int receiptId = (int)Session["ReceiptId"];

						foreach (GridDataItem item in RadGridOrders.Items)
						{
							theErrMethod = item.RowIndex.ToString();
							if (item.GetDataKeyValue("ReceiptId").ToString() == receiptId.ToString())
							{
								RadGridOrders.SelectedIndexes.Add(item.ItemIndex);
								RadGridOrderLines.DataBind();
								//Tabs.ActiveTab = Tabs.Tabs[(int)Session["ActiveTabIndex"]];
								Tabs.SelectedIndex = (int)Session["ActiveTabIndex"];
								receiptId = -1;
								break;
							}
						}

						if (receiptId != -1)
						{
							RadGridOrders.SelectedIndexes.Clear();
							RadGridJobs.SelectedIndexes.Clear();
							Session["InboundShipmentId"] = null;
							Session["ReceiptId"] = null;
							Session["JobId"] = null;

							RadGridOrderLines.DataBind();
							RadGridJobs.DataBind();
							RadGridDetails.DataBind();
						}
					}
				}
			}
			catch { }
		}
		#endregion Page_LoadComplete

		#region ButtonDocumentSearch_Click
		protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonDocumentSearch_Click";
			try
			{
				RadGridOrders.DataBind();

				Master.MsgText = "Search"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ButtonDocumentSearch_Click"

		#region ObjectDataSourcePackaging_OnSelecting
		protected void ObjectDataSourcePackaging_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			theErrMethod = "ObjectDataSourcePackaging_OnSelecting";

			try
			{
				//   e.InputParameters["inboundShipmentId"] = (int)Session["InboundShipmentId"];
				e.InputParameters["receiptId"] = (int)Session["ReceiptId"];
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ObjectDataSourcePackaging_OnSelecting"

		#region ObjectDataSourceReceiptLine_OnSelecting
		protected void ObjectDataSourceReceiptLine_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			theErrMethod = "ObjectDataSourceReceiptLine_OnSelecting";

			try
			{
				e.InputParameters["inboundShipmentId"] = (int)Session["InboundShipmentId"];
				e.InputParameters["receiptId"] = (int)Session["ReceiptId"];
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				//Master.ErrorText = result;
			}

		}
		#endregion "ObjectDataSourceReceiptLine_OnSelecting"

		#region ObjectDataSourceBatch_OnSelecting
		protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			theErrMethod = "ObjectDataSourceBatch_OnSelecting";

			try
			{
				e.InputParameters["storageUnitId"] = (int)Session["StorageUnitId"];

				Master.MsgText = "Select"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ObjectDataSourceBatch_OnSelecting"

		#region ObjectDataSourceReceiptLine_OnUpdating
		protected void ObjectDataSourceReceiptLine_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
		{

			theErrMethod = "ObjectDataSourceReceiptLine_OnUpdating";

			try
			{
				e.InputParameters["operatorId"] = (int)Session["OperatorId"];
				e.InputParameters["storageUnitBatchId"] = (int)Session["StorageUnitBatchId"];
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ObjectDataSourceReceiptLine_OnUpdating"

		#region ObjectDataSourcePackaging_OnUpdating
		protected void ObjectDataSourcePackaging_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
		{

			theErrMethod = "ObjectDataSourcePackaging_OnUpdating";

			try
			{
				e.InputParameters["operatorId"] = (int)Session["OperatorId"];


			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ObjectDataSourcePackaging_OnUpdating"

		#region ObjectDataSourcePackaging_Updated
		protected void ObjectDataSourcePackaging_Updated(object sender, ObjectDataSourceStatusEventArgs e)
		{
			DetailsPackageLinesReceived_Databind();
		}
		#endregion ObjectDataSourcePackaging_Updated

		#region ObjectDataSourceReceiptLine_Updated
		protected void ObjectDataSourceReceiptLine_Updated(object sender, ObjectDataSourceStatusEventArgs e)
		{
			switch ((int)e.ReturnValue)
			{
				case -1:
					Master.MsgText = "";
					Master.ErrorText = "Over receipt not allowed.";
					break;
				case -2:
					Master.MsgText = "";
					Master.ErrorText = "Default batch not allowed.";
					break;
				case -3:
					Master.MsgText = "";
					Master.ErrorText = "You cannot change quantities after a redelivery has been requested.";
					break;
				default:
					Master.MsgText = "Update successful.";
					Master.ErrorText = "";
					break;
			}
		}
		#endregion ObjectDataSourceReceiptLine_Updated

		#region RadGridOrders_RowDataBound
		protected void RadGridOrders_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowIndex != -1)
			{
				if (e.Row.Cells[09].Text == "On Hold")
				{
					e.Row.Cells[0].Enabled = false;
				}
			}
		}
		#endregion

		#region RadGridOrders_SelectedIndexChanged
		protected void RadGridOrders_SelectedIndexChanged(object sender, EventArgs e)
		{
			theErrMethod = "RadGridOrders_SelectedIndexChanged";

			try
			{
				foreach (GridDataItem item in RadGridOrders.SelectedItems)
				{
					Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
					Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");
					Session["ParameterOrderNumber"] = item.GetDataKeyValue("OrderNumber").ToString();

					//if (Session["InboundShipmentId"].ToString() != "-1")
					//    ButtonDeliveryConformance.Enabled = true;
					//else
					//    ButtonDeliveryConformance.Enabled = false;

					RadGridOrderLines.DataBind();

					RadGridJobs.SelectedIndexes.Clear();
					Session["Jobid"] = null;
					RadGridDetails.DataBind();
					break;
				}

				RadGridOrderLines.EditIndexes.Clear();
				GridViewPackageLines.EditIndexes.Clear();

				Session["rowList"] = null;

				RadGridOrderLines.DataBind();

				GridViewPackageLines.AutoGenerateColumns = false;
				GridViewPackageLines.DataSourceID = "ObjectDataSourcePackageLines";
				GridViewPackageLines.DataBind();

				DetailsPackageLinesReceived_Databind();


				GridViewPackageLines2.AutoGenerateColumns = false;
				GridViewPackageLines2.DataSourceID = "ObjectDataSourcePackageLines2";
				GridViewPackageLines2.DataBind();

				DetailsLinesReceived_Databind();

				Master.MsgText = "Select"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion "RadGridOrders_SelectedIndexChanged"

		#region RadGridOrders_ItemCommand
		protected void RadGridOrders_ItemCommand(object source, GridCommandEventArgs e)
		{

			try
			{
				if (e.CommandName == "Redelivery")
				{
					Receiving r = new Receiving();

					foreach (GridDataItem item in RadGridOrders.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
							Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

							r.RedeliveryInsert(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

							RadGridOrders.DataBind();
							RadGridOrderLines.DataBind();

							break;
						}
					}
				}
				if (e.CommandName == "Complete")
				{
					Status status = new Status();
					LinkButton lb = e.CommandSource as LinkButton;

					if (lb.Text.ToLower() == "qty mismatch")
						if (Session["OperatorGroupCode"] != null)
							if (Session["OperatorGroupCode"].ToString() != "A" && Session["OperatorGroupCode"].ToString() != "S")
							{
								ShowAlertMessage("Not authorised to upload quantity mismatch");
								return;
							}

					foreach (GridDataItem item in RadGridOrders.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
							Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

							if (status.InboundStatusChange(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"], -1, "RC"))
							{
								RadGridOrders.DataBind();
								RadGridOrderLines.DataBind();

								Master.MsgText = "Received"; Master.ErrorText = "";
							}
							else
							{
								result = SendErrorNow("Received" + "_" + "Error p_Receipt_Update_Status_Received");
								Master.MsgText = "";
								Master.ErrorText = "Exception, please make sure there are no default batches and there is a delivery note number";
							}

							break;
						}
					}
				}
				if (e.CommandName == "DeliveryConformance")
				{
					foreach (GridDataItem item in RadGridOrders.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
							Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

							Session["Sequence"] = 0;
							Session["QuestionaireId"] = 5;
							Session["QuestionaireType"] = "Supplier Load Conformance";

							ScriptManager.RegisterStartupScript(this.Page,
																this.Page.GetType(),
																"newWindow",
																"window.open('../Common/Question.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
																true);
							break;
						}
					}
				}
				if (e.CommandName == "CheckSheet")
				{
					foreach (GridDataItem item in RadGridOrders.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
							Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

							Session["FromURL"] = "~/Reports/ReceivingCheckSheet.aspx";

							if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
								Session["ReportName"] = "Receiving Check Sheet Batches";
							else
								Session["ReportName"] = "Receiving Check Sheet";

							ReportParameter[] RptParameters = new ReportParameter[7];

							// Create the ConnectionString report parameter
							string strReportConnectionString = Session["ReportConnectionString"].ToString();
							RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

							// Create the InboundShipmentId report parameter
							string inboundShipmentId = Session["InboundShipmentId"].ToString();

							if (inboundShipmentId == "")
								inboundShipmentId = "-1";

							RptParameters[1] = new ReportParameter("InboundShipmentId", inboundShipmentId);

							// Create the ReceiptId report parameter
							string receiptId = Session["ReceiptId"].ToString();
							RptParameters[2] = new ReportParameter("ReceiptId", receiptId);

							// Create the ReceiptLineId report parameter
							RptParameters[3] = new ReportParameter("ReceiptLineId", "-1");

							// Create the ServerName report parameter
							RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

							// Create the DatabaseName report parameter
							RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

							// Create the UserName report parameter
							RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

							Session["ReportParameters"] = RptParameters;

							ScriptManager.RegisterStartupScript(this.Page,
																this.Page.GetType(),
																"newWindow",
																"window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
																true);
							break;
						}
					}
				}
				if (e.CommandName == "PrintGRN")
				{

					foreach (GridDataItem item in RadGridOrders.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
							Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

							Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
							//Session["ReportName"] = "Goods Received Note";

							if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 424))
								Session["ReportName"] = "GRN and Serial Numbers";
							else
								Session["ReportName"] = "Goods Received Note";

							ReportParameter[] RptParameters = new ReportParameter[5];

							// Create the ConnectionString report parameter
							//string strReportConnectionString = Session["ReportConnectionString"].ToString();
							//RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

							// Create the InboundShipmentId report parameter
							string inboundShipmentId = Session["InboundShipmentId"].ToString();
							RptParameters[0] = new ReportParameter("InboundShipmentId", inboundShipmentId);

							// Create the ReceiptId report parameter
							string receiptId = Session["ReceiptId"].ToString();
							RptParameters[1] = new ReportParameter("ReceiptId", receiptId);

							RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

							RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

							RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

							Session["ReportParameters"] = RptParameters;

							ScriptManager.RegisterStartupScript(this.Page,
																this.Page.GetType(),
																"newWindow",
																"window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
																true);
						}
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("Received" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion RadGridOrders_ItemCommand

		#region ShowAlertMessage
		public static void ShowAlertMessage(string error)
		{
			Page page = HttpContext.Current.Handler as Page;

			if (page != null)
			{
				error = error.Replace("'", "\'");

				ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "alert('" + error + "');", true);
			}
		}
		#endregion

		#region RadGridOrderLines_ItemCommand
		protected void RadGridOrderLines_ItemCommand(object source, GridCommandEventArgs e)
		{
			try
			{
				if (e.CommandName == "CheckSheet")
				{
					foreach (GridDataItem item in RadGridOrderLines.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							Session["ReceiptLineId"] = item.GetDataKeyValue("ReceiptLineId");

							Session["FromURL"] = "~/Reports/ReceivingCheckSheet.aspx";

							if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
								Session["ReportName"] = "Receiving Check Sheet Batches";
							else
								Session["ReportName"] = "Receiving Check Sheet";

							ReportParameter[] RptParameters = new ReportParameter[7];

							// Create the ConnectionString report parameter
							string strReportConnectionString = Session["ReportConnectionString"].ToString();
							RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

							// Create the InboundShipmentId report parameter
							string inboundShipmentId = Session["InboundShipmentId"].ToString();

							if (inboundShipmentId == "")
								inboundShipmentId = "-1";

							RptParameters[1] = new ReportParameter("InboundShipmentId", inboundShipmentId);

							// Create the ReceiptId report parameter
							string receiptId = Session["ReceiptId"].ToString();
							RptParameters[2] = new ReportParameter("ReceiptId", receiptId);

							// Create the ReceiptLineId report parameter
							string receiptLineId = Session["ReceiptLineId"].ToString();
							RptParameters[3] = new ReportParameter("ReceiptLineId", receiptLineId);

							// Create the ServerName report parameter
							RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

							// Create the DatabaseName report parameter
							RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

							// Create the UserName report parameter
							RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

							Session["ReportParameters"] = RptParameters;

							ScriptManager.RegisterStartupScript(this.Page,
																this.Page.GetType(),
																"newWindow",
																"window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
																true);
							break;
						}
					}
				}
				if (e.CommandName == "SerialNumber")
				{
					//Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
					//Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

					//Response.Redirect("RegisterSerialNumber.aspx");

					//Master.MsgText = "Link"; Master.ErrorText = "";
					foreach (GridDataItem item in RadGridOrderLines.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							Session["ReceiptLineId"] = item.GetDataKeyValue("ReceiptLineId");

							ScriptManager.RegisterStartupScript(this.Page,
																this.Page.GetType(),
																"newWindow",
																"window.open('../Inbound/RegisterSerialNumber.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=400,height=525,top=100,left=100');",
																true);
							break;
						}
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("Received" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion RadGridOrderLines_ItemCommand

		#region RadGridOrderLines_EditCommand
		protected void RadGridOrderLines_EditCommand(object source, GridCommandEventArgs e)
		{
			try
			{
				foreach (GridDataItem item in RadGridOrderLines.Items)
				{
					if (item.RowIndex == e.Item.RowIndex)
					{
						Session["StorageUnitId"] = item.GetDataKeyValue("StorageUnitId");
						ObjectDataSourceBatch.DataBind();
						break;
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("Received" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion RadGridOrderLines_EditCommand

		#region RadGridJobs_ItemCommand
		protected void RadGridJobs_ItemCommand(object source, GridCommandEventArgs e)
		{
			try
			{
				if (e.CommandName == "CheckSheet")
				{
					ReportParameter[] RptParameters = new ReportParameter[4];
					ArrayList checkedList;

					foreach (GridDataItem item in RadGridJobs.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							if (Session["checkedList"] != null)
							{
								checkedList = (ArrayList)Session["checkedList"];
							}
							else
							{
								//if (RadGridJobs.Items.Count > 0)
								//    return;

								checkedList = new ArrayList();

								checkedList.Add(item.GetDataKeyValue("JobId"));
							}

							if (checkedList.Count > 0)
							{
								Session["checkedList"] = checkedList;

								Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
								Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

								if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
									Session["ReportName"] = "Receiving Check Sheet Job Batches";
								else
									Session["ReportName"] = "Receiving Check Sheet Job";

								// Create the JobId report parameter
								RptParameters[0] = new ReportParameter("JobId", checkedList[0].ToString());

								checkedList.RemoveAt(0);

								RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

								RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

								RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

								Session["ReportParameters"] = RptParameters;

								//Response.Redirect("~/Reports/Report.aspx");

								ScriptManager.RegisterStartupScript(this.Page,
																this.Page.GetType(),
																"newWindow",
																"window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
																true);
							}
							else
							{
								Session["checkedList"] = null;
							}

							break;
						}
					}
				}
				if (e.CommandName == "ReferenceNumber")
				{

					Session["LabelName"] = "Multi Purpose Label.lbl";
					Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
					Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

					if (Session["Printer"] == null)
						Session["Printer"] = "";

					foreach (GridDataItem item in RadGridJobs.Items)
					{
						if (item.RowIndex == e.Item.RowIndex)
						{
							Session["Title"] = item.GetDataKeyValue("JobId").ToString();
							Session["Barcode"] = item.GetDataKeyValue("ReferenceNumber").ToString();

							if (Session["Printer"].ToString() == "")
								Response.Redirect("~/Common/NLLabels.aspx");
							else
							{
								Session["Printing"] = true;
								//Response.Redirect("~/Common/NLPrint.aspx");
								Session["LabelClose"] = true;
								ScriptManager.RegisterStartupScript(this.Page,
																	this.Page.GetType(),
																	"newWindow",
																	"window.open('../Common/Label.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
																	true);
							}
						}
					}
				}
				if (e.CommandName == "PalletWeight")
				{
					ArrayList checkedList = new ArrayList();

					foreach (GridDataItem item in RadGridJobs.Items)
					{
						if (item.Selected)
							checkedList.Add((int)item.GetDataKeyValue("JobId"));
					}

					if (checkedList.Count < 1)
						return;

					Session["checkedList"] = checkedList;

					ScriptManager.RegisterStartupScript(this.Page,
														this.Page.GetType(),
														"newWindow",
														"window.open('../Inbound/PalletWeight.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=550,height=500,top=100,left=100');",
														true);
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("Received" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion RadGridJobs_ItemCommand

		#region RadGridDetails_ItemCommand
		protected void RadGridDetails_ItemCommand(object source, GridCommandEventArgs e)
		{
			try
			{
				if (e.CommandName == "PalletLabel")
				{
					CheckBox cb = new CheckBox();
					Status status = new Status();
					ArrayList rowList = new ArrayList();

					Session["checkedList"] = null;

					foreach (GridDataItem item in RadGridDetails.Items)
					{
						if (item.Selected)
						{
							if (status.Print(Session["ConnectionStringName"].ToString(), int.Parse(item.GetDataKeyValue("InstructionId").ToString()), (int)Session["OperatorId"]))
								rowList.Add(item.GetDataKeyValue("InstructionId"));
							else
								Master.ErrorText = "Please try again";
						}
					}

					if (rowList.Count > 0)
						Session["checkedList"] = rowList;

					if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 390))
						Session["LabelName"] = "Pallet Label Small.lbl";
					else
						Session["LabelName"] = "Pallet Label.lbl";

					Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
					Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

					if (Session["Printer"] == null)
						Session["Printer"] = "";

					if (Session["Printer"].ToString() == "")
						Response.Redirect("~/Common/NLLabels.aspx");
					else
					{
						Session["Printing"] = true;
						//Response.Redirect("~/Common/NLPrint.aspx");
						Session["LabelClose"] = true;
						ScriptManager.RegisterStartupScript(this.Page,
															this.Page.GetType(),
															"newWindow",
															"window.open('../Common/Label.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
															true);
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("Received" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion RadGridDetails_ItemCommand

		#region ButtonPOD_Click
		//protected void ButtonPOD_Click(object sender, EventArgs e)
		//{
		//    theErrMethod = "ButtonPOD_Click";

		//    try
		//    {
		//        Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
		//        //Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());
		//        Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

		//        Session["ReportName"] = "Goods Received Note";

		//        ReportParameter[] RptParameters = new ReportParameter[5];

		//        // Create the ConnectionString report parameter
		//        //string strReportConnectionString = Session["ReportConnectionString"].ToString();
		//        //RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

		//        // Create the InboundShipmentId report parameter
		//        string inboundShipmentId = Session["InboundShipmentId"].ToString();
		//        RptParameters[0] = new ReportParameter("InboundShipmentId", inboundShipmentId);

		//        // Create the ReceiptId report parameter
		//        string receiptId = Session["ReceiptId"].ToString();
		//        RptParameters[1] = new ReportParameter("ReceiptId", receiptId);

		//        RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

		//        RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

		//        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

		//        Session["ReportParameters"] = RptParameters;

		//        Response.Redirect("~/Reports/Report.aspx");

		//        Master.MsgText = "POD"; Master.ErrorText = "";
		//    }
		//    catch (Exception ex)
		//    {
		//        result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
		//        Master.ErrorText = result;
		//    }

		//}
		#endregion ButtonPOD_Click

		#region ButtonPrint_Click
		//protected void ButtonPrint_Click(object sender, EventArgs e)
		//{
		//    theErrMethod = "ButtonPrint_Click";

		//    try
		//    {
		//        Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
		//        Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

		//        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
		//            Session["ReportName"] = "Receiving Check Sheet Batches";
		//        else
		//            Session["ReportName"] = "Receiving Check Sheet";

		//        ReportParameter[] RptParameters = new ReportParameter[7];

		//        // Create the ConnectionString report parameter
		//        String strReportConnectionString = Session["ReportConnectionString"].ToString();
		//        RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

		//        // Create the InboundShipmentId report parameter
		//        String inboundShipmentId = Session["InboundShipmentId"].ToString();
		//        RptParameters[1] = new ReportParameter("InboundShipmentId", inboundShipmentId);

		//        // Create the ReceiptId report parameter
		//        String receiptId = Session["ReceiptId"].ToString();
		//        RptParameters[2] = new ReportParameter("ReceiptId", receiptId);

		//        // Create the ReceiptLineId report parameter
		//        String receiptLineId = "-1";
		//        CheckBox cb = new CheckBox();

		//        foreach (GridDataItem item in RadGridOrderLines.Items)
		//        {
		//            if (item.Selected)
		//            {
		//                receiptLineId = item.GetDataKeyValue("ReceiptLineId").ToString();
		//                break;
		//            }
		//        }

		//        RptParameters[3] = new ReportParameter("ReceiptLineId", receiptLineId);

		//        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

		//        RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

		//        RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

		//        Session["ReportParameters"] = RptParameters;

		//        Response.Redirect("~/Reports/Report.aspx");

		//        Master.MsgText = "Print"; Master.ErrorText = "";
		//    }
		//    catch (Exception ex)
		//    {
		//        result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
		//        Master.ErrorText = result;
		//    }

		//}
		#endregion ButtonPrint_Click

		#region ButtonManualLocations_Click
		protected void ButtonManualLocations_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonManualLocations_Click";

			try
			{
				GetEditIndex();
				Response.Redirect("ManualPalletise.aspx");

			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ButtonManualLocations_Click"

		#region RadGridOrderLines_OnRowUpdating
		protected void RadGridOrderLines_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
		{
			theErrMethod = "RadGridOrderLines_OnRowUpdating";

			try
			{
				GridDataItem item = RadGridOrderLines.Items[e.RowIndex];

				if (item != null)
				{
					DropDownList drp = (DropDownList)item.FindControl("DropDownListBatch");
					Session["drp"] = drp.Text;
				}

				foreach (DictionaryEntry entry in e.NewValues)
				{
					e.NewValues[entry.Key] =
						Server.HtmlEncode(entry.Value.ToString());
				}

				Master.MsgText = "Row Update"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion "RadGridOrderLines_OnRowUpdating"

		#region ButtonPackageEdit_Click
		// Sets the GridView into edit mode for selected row	
		protected void ButtonPackageEdit_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonEdit_Click";

			try
			{
				GetEditIndexRowNumber();
				SetEditIndexRowNumber();

				Master.MsgText = "Edit Row"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}


		}
		#endregion "ButtonPackageEdit_Click"

		#region GetEditIndex
		protected void GetEditIndex()
		{
			theErrMethod = "GetEditIndex";

			try
			{
				ArrayList rowList = new ArrayList();

				foreach (GridDataItem item in RadGridOrderLines.Items)
				{
					if (item.Selected)
						rowList.Add(item.GetDataKeyValue("ReceiptLineId"));
				}

				if (rowList.Count > 0)
					Session["checkedList"] = rowList;
				else
					Session["checkedList"] = null;

				Master.MsgText = "Edit"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "GetEditIndex"

		#region GetEditIndexRowNumber
		protected void GetEditIndexRowNumber()
		{
			theErrMethod = "GetEditIndexRowNumber";

			try
			{
				if (Session["rowList"] == null)
				{
					ArrayList rowList = new ArrayList();

					foreach (GridDataItem item in RadGridOrderLines.Items)
					{
						if (item.Selected)
							rowList.Add(item.RowIndex);
					}

					if (rowList.Count > 0)
						Session["rowList"] = rowList;
				}

				Master.MsgText = "Edit"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "GetEditIndexRowNumber"

		#region SetEditIndexRowNumber
		protected void SetEditIndexRowNumber()
		{
			theErrMethod = "SetEditIndexRowNumber";

			try
			{
				ArrayList rowList = (ArrayList)Session["rowList"];
				if (rowList == null)
					Session.Remove("rowList");
				else
				{
					//if (RadGridOrderLines.EditIndexes.Count > 0)
					//    RadGridOrderLines.UpdateRow(RadGridOrderLines.EditIndex, true);

					if (rowList.Count < 1)
					{
						Session.Remove("rowList");
						RadGridOrderLines.EditIndexes.Clear();
					}
					else
					{
						RadGridOrderLines.SelectedIndexes.Add((int)rowList[0]);
						RadGridOrderLines.EditIndexes.Add((int)rowList[0]);

						GridDataItem item = RadGridOrderLines.Items[(int)rowList[0]];

						Session["StorageUnitId"] = item.GetDataKeyValue("StorageUnitId");
						Session["BatchId"] = item.GetDataKeyValue("BatchId");
						Session["StorageUnitBatchId"] = -1;
						Session["DefaultMode"] = "Insert";

						rowList.Remove(rowList[0]);
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "SetEditIndexRowNumber"

		#region ButtonProductAdd_Click
		protected void ButtonProductAdd_Click(object sender, EventArgs e)
		{
			try
			{
				if (Session["PassStorageUnitBatchId"] != null)
				{
					Receiving r = new Receiving();

					r.InsertPackageLines(Session["ConnectionStringName"].ToString(),
										(int)Session["WarehouseId"],
										(int)Session["ReceiptId"],
										(int)Session["PassStorageUnitBatchId"],
										(int)Session["OperatorId"]);


					DetailsPackageLinesReceived_Databind();
					GridViewPackageLines.DataBind();
					GridViewPackageLines.DataBind();

				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}


		}



		#endregion ButtonProductAdd_Click

		#region DetailsLinesReceived_Databind
		protected void DetailsLinesReceived_Databind()
		{
			DetailsLinesReceived.DataBind();

			if ((int)DetailsLinesReceived.DataKey["LinesReceived"] >= (int)DetailsLinesReceived.DataKey["TotalLines"])
			{
				ButtonDefaultReceived.Enabled = false;
				ButtonDefaultDelivery.Enabled = false;
				ButtonDefaultZero.Enabled = false;
			}
			else
			{
				ButtonDefaultReceived.Enabled = true;
				ButtonDefaultDelivery.Enabled = true;
				ButtonDefaultZero.Enabled = true;
			}

			// May override above settings depending on configuration value
			DisableButtons();
		}
		#endregion DetailsLinesReceived_Databind

		#region DetailsPackageLinesReceived_Databind
		protected void DetailsPackageLinesReceived_Databind()
		{
			DetailsPackageLinesReceived.DataBind();


			if ((int)DetailsPackageLinesReceived.DataKey["LinesReceived"] >= (int)DetailsPackageLinesReceived.DataKey["TotalLines"])
			{
				ButtonPackageComplete.Enabled = true;
			}
			else
			{
				ButtonPackageComplete.Enabled = false;
			}

			// May override above settings depending on configuration value

		}
		#endregion DetailsPackageLinesReceived_Databind

		#region DisableButtons
		protected void DisableButtons()
		{
			foreach (GridDataItem item in RadGridOrders.Items)
			{
				if (item.GetDataKeyValue("AllowPalletise").ToString().ToLower() == "false")
				{
					//ButtonReject.Enabled = false;
					//ButtonDeliveryConformance.Enabled = false;
				}
				else
				{
					//ButtonReject.Enabled = true;
					//ButtonDeliveryConformance.Enabled = true;
				}
			}
		}
		#endregion DisableButtons

		#region ButtonDefaultDelivery_Click
		protected void ButtonDefaultDelivery_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonDefaultDelivery_Click";
			try
			{
				Receiving r = new Receiving();

				r.SetDefaultDeliveryNoteQty(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

				this.RadGridOrderLines.DataBind();

				DetailsLinesReceived_Databind();

				Master.MsgText = "Delivery"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion "ButtonDefaultDelivery_Click"

		#region ButtonDefaultActual_Click
		// --------------------------------------------
		// METHOD : ButtonDefaultReceived_Click
		// --------------------------------------------
		/// <summary>
		/// When the user clicks to set default values.
		/// </summary>        
		protected void ButtonDefaultActual_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonDefaultReceived_Click";
			try
			{
				Receiving r = new Receiving();

				r.SetDefaultActualQty(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

				this.RadGridOrderLines.DataBind();

				DetailsLinesReceived_Databind();

				Master.MsgText = "Default"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ButtonDefaultActual_Click"

		#region ButtonDefaultZero_Click
		// --------------------------------------------
		// METHOD : ButtonDefaultZero_Click
		// --------------------------------------------
		/// <summary>
		/// When the user clicks to set default values.
		/// </summary>        
		protected void ButtonDefaultZero_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonDefaultZero_Click";
			try
			{
				Receiving r = new Receiving();

				r.SetDefaultZero(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

				this.RadGridOrderLines.DataBind();

				DetailsLinesReceived_Databind();

				Master.MsgText = "Default"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ButtonDefaultZero_Click"

		#region ButtonDeliveryConformance_Click
		//protected void ButtonDeliveryConformance_Click(object sender, EventArgs e)
		//{
		//    theErrMethod = "ButtonDeliveryConformance_Click";
		//    try
		//    {
		//        Session["Sequence"] = 0;
		//        Session["QuestionaireId"] = 5;
		//        Session["QuestionaireType"] = "Supplier Load Conformance";


		//        //            Session["ActiveViewIndex"] = MultiViewConfirm.ActiveViewIndex;
		//        Session["ReturnURL"] = "~/Inbound/ReceivingDocument.aspx";
		//        Response.Redirect("~/Common/Question.aspx");

		//    }
		//    catch { }
		//}
		#endregion "ButtonDeliveryConformance_Click"

		#region ButtonReject_Click
		//protected void ButtonReject_Click(object sender, EventArgs e)
		//{
		//    theErrMethod = "ButtonReject_Click";

		//    try
		//    {
		//        GetEditIndex();
		//        if (Session["checkedList"] != null)
		//            Response.Redirect("CreditAdviceReject.aspx?ReasonCode=REJ");

		//        Master.MsgText = "Reject"; Master.ErrorText = "";
		//    }
		//    catch (Exception ex)
		//    {
		//        result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
		//        Master.ErrorText = result;
		//    }

		//}
		#endregion "ButtonReject_Click"

		#region ButtonLink_Click
		//protected void ButtonLink_Click(object sender, EventArgs e)
		//{
		//    theErrMethod = "ButtonLink_Click";

		//    try
		//    {
		//        Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
		//        Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

		//        Response.Redirect("RegisterSerialNumber.aspx");

		//        Master.MsgText = "Link"; Master.ErrorText = "";
		//    }
		//    catch (Exception ex)
		//    {
		//        result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
		//        Master.ErrorText = result;
		//    }

		//}
		#endregion "ButtonLink_Click"

		#region ButtonCreditAdvice_Click
		protected void ButtonCreditAdvice_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonCreditAdvice_Click";
			try
			{
				GetEditIndex();

				if (Session["checkedList"] != null)
					Response.Redirect("CreditAdviceUpdate.aspx?ReasonCode=CAV");

				Master.MsgText = "Advice"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ButtonCreditAdvice_Click"

		#region ButtonPackageReceived_Click
		protected void ButtonPackageReceived_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonPackageReceived_Click";
			try
			{
				Status status = new Status();
				Receiving r = new Receiving();

				if (r.CompletePackageLines(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["ReceiptId"]) == 1)
				{
					ButtonPackageComplete.Enabled = false;
					Master.MsgText = "Packaging Received";
					Master.ErrorText = "";
				}
				else
				{
					result = SendErrorNow("Received" + "_" + "Error p_Receiving_PackageLine_Complete");
					Master.MsgText = "";
					Master.ErrorText = "Exception, Error completing Packaging";
				}
				GridViewPackageLines.DataBind();
				GridViewPackageLines2.DataBind();
				DetailsLinesReceived_Databind();

			}
			catch (Exception ex)
			{
				result = SendErrorNow("Received" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ButtonPackageReceived_Click"

		#region ButtonPrintCheckSheet_Click
		protected void ButtonPrintCheckSheet_Click(object sender, EventArgs e)
		{
			PrintMutipleCheckSheets();
		}
		#endregion ButtonPrintCheckSheet_Click

		#region PrintMutipleCheckSheets
		protected void PrintMutipleCheckSheets()
		{
			try
			{
				ReportParameter[] RptParameters = new ReportParameter[4];
				ArrayList checkedList;

				if (Session["checkedList"] != null)
				{
					checkedList = (ArrayList)Session["checkedList"];
				}
				else
				{
					if (RadGridJobs.Items.Count > 0)
						return;

					checkedList = new ArrayList();

					foreach (GridDataItem item in RadGridJobs.Items)
					{
						if (item.Selected)
							checkedList.Add(item.GetDataKeyValue("JobId"));
					}
				}

				if (checkedList.Count > 0)
				{
					Session["checkedList"] = checkedList;

					Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
					Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

					if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
						Session["ReportName"] = "Receiving Check Sheet Job Batches";
					else
						Session["ReportName"] = "Receiving Check Sheet Job";

					// Create the JobId report parameter
					RptParameters[0] = new ReportParameter("JobId", checkedList[0].ToString());

					checkedList.RemoveAt(0);

					RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

					RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

					RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

					Session["ReportParameters"] = RptParameters;

					//Response.Redirect("~/Reports/Report.aspx");
				}
				else
				{
					Session["checkedList"] = null;
				}
			}
			catch { }
		}
		#endregion PrintMutipleCheckSheets

		#region ButtonPrintJob_Click
		//protected void ButtonPrintJob_Click(object sender, EventArgs e)
		//{
		//    theErrMethod = "ButtonPrintJob_Click";

		//    try
		//    {
		//        Session["LabelName"] = "Multi Purpose Label.lbl";
		//        Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
		//        Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

		//        if (Session["Printer"] == null)
		//            Session["Printer"] = "";

		//        foreach (GridDataItem item in RadGridJobs.Items)
		//        {
		//            if (item.Selected)
		//            {
		//                Session["Title"] = item.GetDataKeyValue("JobId").ToString();
		//                Session["Barcode"] = item.GetDataKeyValue("ReferenceNumber").ToString();

		//                if (Session["Printer"].ToString() == "")
		//                    Response.Redirect("~/Common/NLLabels.aspx");
		//                else
		//                {
		//                    Session["Printing"] = true;
		//                    Response.Redirect("~/Common/NLPrint.aspx");
		//                }
		//            }
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        result = SendErrorNow("ReceivingDocument " + "_" + ex.Message.ToString());
		//        Master.ErrorText = result;
		//    }
		//}
		#endregion ButtonPrintJob_Click

		#region ButtonPrintPallet_Click
		//protected void ButtonPrintPallet_Click(object sender, EventArgs e)
		//{
		//    theErrMethod = "ButtonPrintPallet_Click";

		//    try
		//    {
		//        CheckBox cb = new CheckBox();
		//        Status status = new Status();
		//        ArrayList rowList = new ArrayList();

		//        Session["checkedList"] = null;

		//        foreach (GridDataItem item in RadGridDetails.Items)
		//        {
		//            if (item.Selected)
		//            {
		//                if (status.Print(Session["ConnectionStringName"].ToString(), int.Parse(item.GetDataKeyValue("InstructionId").ToString()), (int)Session["OperatorId"]))
		//                    rowList.Add(item.GetDataKeyValue("InstructionId"));
		//                else
		//                    Master.ErrorText = "Please try again";
		//            }
		//        }

		//        if (rowList.Count > 0)
		//            Session["checkedList"] = rowList;

		//        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 390))
		//            Session["LabelName"] = "Pallet Label Small.lbl";
		//        else
		//            Session["LabelName"] = "Pallet Label.lbl";

		//        Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
		//        Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

		//        if (Session["Printer"] == null)
		//            Session["Printer"] = "";

		//        if (Session["Printer"].ToString() == "")
		//            Response.Redirect("~/Common/NLLabels.aspx");
		//        else
		//        {
		//            Session["Printing"] = true;
		//            Response.Redirect("~/Common/NLPrint.aspx");
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
		//        Master.ErrorText = result;
		//    }
		//}
		#endregion ButtonPrintPallet_Click

		#region RadGridJobs_OnSelectedIndexChanged
		protected void RadGridJobs_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			theErrMethod = "RadGridJobs_OnSelectedIndexChanged";

			try
			{
				Session["JobId"] = -1;

				foreach (GridDataItem item in RadGridJobs.Items)
				{
					if (item.Selected)
						Session["JobId"] = (int)item.GetDataKeyValue("JobId");
				}

				RadGridDetails.DataBind();

				Master.MsgText = "Selected"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion "RadGridJobs_OnSelectedIndexChanged"

		#region ButtonPrintLabel_Click
		protected void ButtonPrintLabel_Click(object sender, EventArgs e)
		{
			try
			{
				ArrayList checkedList = new ArrayList();
				CheckBox cb = new CheckBox();
				int quantity = 1;
				string nop;
				int numberToPrint;
				string rli;
				bool verifyLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 159);
				bool useReceiptLineId = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 178);
				bool customerLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 320);
				bool utiLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 431);

				foreach (GridDataItem item in RadGridOrderLines.SelectedItems)
				{
					if (verifyLabel || useReceiptLineId)
						checkedList.Add((int)item.GetDataKeyValue("ReceiptLineId"));
					else
						checkedList.Add((int)item.GetDataKeyValue("BatchId"));

					quantity = Convert.ToInt32(item.GetDataKeyValue("Boxes"));

					Exceptions ex = new Exceptions();
					rli = ((HiddenField)item.FindControl("HiddenReceiptLineId")).Value;
					Session["rli"] = int.Parse(rli);
					int count = ex.CountRedPrint(Session["ConnectionStringName"].ToString(), (int)Session["rli"]);
					nop = ((HiddenField)item.FindControl("HiddenNumberOfPallets")).Value;
					//numberToPrint = int.Parse(nop) - count;

					if (TextBoxQty.Text != "")
						int.TryParse(TextBoxQty.Text, out quantity);

					if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 500))
					{
						Session["LabelCopies"] = quantity;
					}
					else
					{
						numberToPrint = int.Parse(nop) - count;
						if (quantity > numberToPrint)
						{
							Session["LabelCopies"] = numberToPrint;
							Master.MsgText = "";
							Master.ErrorText = "Some lines exceeded number of allowed labels - defaulted to number of pallets";
							if (numberToPrint < 1)
							{
								Master.MsgText = "";
								Master.ErrorText = "Maximum number of labels have already been printed";
								return;
							}
							//break;
						}
						else
						{
							Session["LabelCopies"] = quantity;
						}

						if (ex.CreateException(Session["ConnectionStringName"].ToString(), (int)Session["rli"], (int)Session["LabelCopies"], (int)Session["OperatorId"]) == false)
						{
							return;
						}
					}

				}

				if (checkedList.Count < 1)
					return;

				Session["checkedList"] = checkedList;

				if (utiLabel)
					if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 432))
						Session["LabelName"] = "UTI Product Label Small.lbl";
					else
						Session["LabelName"] = "UTI Product Label Large.lbl";
				else
					if (customerLabel)
					Session["LabelName"] = "Customer Product Label Small.lbl";
				else
						if (verifyLabel)
					if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 199))
						Session["LabelName"] = "Receiving Batch Label Small.lbl";
					else
						Session["LabelName"] = "Receiving Batch Label Large.lbl";
				else
							if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
					Session["LabelName"] = "Product Label Small.lbl";
				else
					Session["LabelName"] = "Product Batch Label Large.lbl";

				Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
				Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

				if (Session["Printer"] == null)
					Session["Printer"] = "";

				var sessionCheckedListCount = (ArrayList)Session["checkedList"];
				var numberOfLabels = sessionCheckedListCount.Count;

				for (int i = 0; i < numberOfLabels; i++)
				{
					var sessionPort = Session["Port"];
					var sessionPrinter = Session["Printer"];
					var sesionIndicatorList = Session["indicatorList"];
					var sessionCheckedList = Session["checkedList"];
					var sessionLocationList = Session["locationList"];
					var sessionConnectionString = Session["ConnectionStringName"];
					var sessionTitle = Session["Title"];
					var sessionBarcode = Session["Barcode"];
					var sessionWarehouseId = Session["WarehouseId"];
					var sessionProductList = Session["ProductList"];
					var sessionTakeOnLabel = Session["TakeOnLabel"];
					var sessionLabelCopies = Session["LabelCopies"];

					NLWAXPrint nl = new NLWAXPrint();
					nl.onSessionUpdate += Nl_onSessionUpdate;
					string printResponse = nl.CreatePrintFile("/DialaBed/",
															  Session["LabelName"].ToString(),
															  sessionPrinter,
															  sessionPort,
															  sesionIndicatorList,
															  sessionCheckedList,
															  sessionLocationList,
															  sessionConnectionString,
															  sessionTitle,
															  sessionBarcode,
															  sessionWarehouseId,
															  sessionProductList,
															  sessionTakeOnLabel,
															  sessionLabelCopies);
					if (printResponse == string.Empty)
					{
						Master.ErrorText = "An error has occurred! For more details please check automation manager.";
					}
					else
					{
						Master.MsgText = "Label has been printed successfuly.";
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("GenericLabel " + "_" + ex.Message.ToString());
				Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
			}
		}

		private void Nl_onSessionUpdate(string arg1, object arg2)
		{
			Session[arg1] = arg2;
		}
		#endregion ButtonPrintLabel_Click

		#region ButtonTicketLabel_Click
		protected void ButtonTicketLabel_Click(object sender, EventArgs e)
		{
			try
			{
				ArrayList checkedList = new ArrayList();
				CheckBox cb = new CheckBox();
				int quantity = 1;

				foreach (GridDataItem item in RadGridOrderLines.Items)
				{
					if (item.Selected)
					{
						checkedList.Add((int)item.GetDataKeyValue("StorageUnitId") * -1);
						quantity = Convert.ToInt32(item.GetDataKeyValue("Boxes"));
					}
				}

				if (TextBoxTicket.Text != "")
					if (!int.TryParse(TextBoxTicket.Text, out quantity))
						quantity = 1;

				Session["LabelCopies"] = quantity;

				if (checkedList.Count < 1)
					return;

				Session["checkedList"] = checkedList;

				Session["LabelName"] = "Moresport Ticket.lbl";

				Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
				Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

				if (Session["Printer"] == null)
					Session["Printer"] = "";

				var sessionCheckedListCount = (ArrayList)Session["checkedList"];
				var numberOfLabels = sessionCheckedListCount.Count;

				for (int i = 0; i < numberOfLabels; i++)
				{
					var sessionPort = Session["Port"];
					var sessionPrinter = Session["Printer"];
					var sesionIndicatorList = Session["indicatorList"];
					var sessionCheckedList = Session["checkedList"];
					var sessionLocationList = Session["locationList"];
					var sessionConnectionString = Session["ConnectionStringName"];
					var sessionTitle = Session["Title"];
					var sessionBarcode = Session["Barcode"];
					var sessionWarehouseId = Session["WarehouseId"];
					var sessionProductList = Session["ProductList"];
					var sessionTakeOnLabel = Session["TakeOnLabel"];
					var sessionLabelCopies = Session["LabelCopies"];

					NLWAXPrint nl = new NLWAXPrint();
					nl.onSessionUpdate += Nl_onSessionUpdate;
					string printResponse = nl.CreatePrintFile("/DialaBed/",
															  Session["LabelName"].ToString(),
															  sessionPrinter,
															  sessionPort,
															  sesionIndicatorList,
															  sessionCheckedList,
															  sessionLocationList,
															  sessionConnectionString,
															  sessionTitle,
															  sessionBarcode,
															  sessionWarehouseId,
															  sessionProductList,
															  sessionTakeOnLabel,
															  sessionLabelCopies);
					if (printResponse == string.Empty)
					{
						Master.ErrorText = "An error has occurred! For more details please check automation manager.";
					}
					else
					{
						Master.MsgText = "Label has been printed successfuly.";
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("GenericLabel " + "_" + ex.Message.ToString());
				Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
			}
		}
		#endregion ButtonTicketLabel_Click

		#region ButtonPrintSample_Click
		protected void ButtonPrintSample_Click(object sender, EventArgs e)
		{
			try
			{
				ArrayList checkedList = new ArrayList();
				CheckBox cb = new CheckBox();
				int quantity = 1;
				bool useReceiptLineId = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 178);

				foreach (GridDataItem item in RadGridOrderLines.Items)
				{
					if (item.Selected)
					{
						if (useReceiptLineId)
							checkedList.Add((int)item.GetDataKeyValue("ReceiptLineId"));
						else
							checkedList.Add((int)item.GetDataKeyValue("BatchId"));

						quantity = Convert.ToInt32(item.GetDataKeyValue("Boxes"));
					}
				}

				if (checkedList.Count < 1)
					return;

				Session["checkedList"] = checkedList;

				if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
					Session["LabelName"] = "Sample Label Small.lbl";
				else
					Session["LabelName"] = "Sample Label Large.lbl";

				Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
				Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

				if (TextBoxSample.Text != "")
					if (int.TryParse(TextBoxSample.Text, out quantity))
						quantity = 1;

				Session["LabelCopies"] = quantity;

				if (Session["Printer"] == null)
					Session["Printer"] = "";

				var sessionCheckedListCount = (ArrayList)Session["checkedList"];
				var numberOfLabels = sessionCheckedListCount.Count;

				for (int i = 0; i < numberOfLabels; i++)
				{
					var sessionPort = Session["Port"];
					var sessionPrinter = Session["Printer"];
					var sesionIndicatorList = Session["indicatorList"];
					var sessionCheckedList = Session["checkedList"];
					var sessionLocationList = Session["locationList"];
					var sessionConnectionString = Session["ConnectionStringName"];
					var sessionTitle = Session["Title"];
					var sessionBarcode = Session["Barcode"];
					var sessionWarehouseId = Session["WarehouseId"];
					var sessionProductList = Session["ProductList"];
					var sessionTakeOnLabel = Session["TakeOnLabel"];
					var sessionLabelCopies = Session["LabelCopies"];

					NLWAXPrint nl = new NLWAXPrint();
					nl.onSessionUpdate += Nl_onSessionUpdate;
					string printResponse = nl.CreatePrintFile("/DialaBed/",
															  Session["LabelName"].ToString(),
															  sessionPrinter,
															  sessionPort,
															  sesionIndicatorList,
															  sessionCheckedList,
															  sessionLocationList,
															  sessionConnectionString,
															  sessionTitle,
															  sessionBarcode,
															  sessionWarehouseId,
															  sessionProductList,
															  sessionTakeOnLabel,
															  sessionLabelCopies);
					if (printResponse == string.Empty)
					{
						Master.ErrorText = "An error has occurred! For more details please check automation manager.";
					}
					else
					{
						Master.MsgText = "Label has been printed successfuly.";
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("GenericLabel " + "_" + ex.Message.ToString());
				Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
			}
		}
		#endregion ButtonPrintSample_Click

		#region ButtonPalletWeight
		protected void ButtonPalletWeight_Click(object sender, EventArgs e)
		{
			theErrMethod = "ButtonPalletWeight";

			try
			{
				ArrayList checkedList = new ArrayList();

				foreach (GridDataItem item in RadGridJobs.Items)
				{
					if (item.Selected)
						checkedList.Add((int)item.GetDataKeyValue("JobId"));
				}

				if (checkedList.Count < 1)
					return;

				Session["checkedList"] = checkedList;

				Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
				Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

				Response.Redirect("~/Inbound/PalletWeight.aspx");

				Master.MsgText = "Edit"; Master.ErrorText = "";
			}
			catch (Exception ex)
			{
				result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}

		}
		#endregion "ButtonPalletWeight"

		#region DropDownListBatch_OnSelectedIndexChanged
		protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				DropDownList ddl = (DropDownList)sender;
				Session["StorageUnitBatchId"] = int.Parse(ddl.SelectedValue);
			}
			catch { }
		}
		#endregion "DropDownListBatch_OnSelectedIndexChanged"

		#region ErrorHandling
		private string SendErrorNow(string ex)
		{

			try
			{
				int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

				if (loopPrevention == 0)
				{
					CQExceptionLayer cqexception = new CQExceptionLayer();

					result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocument", theErrMethod, ex);
				}

				Session["countLoopsToPreventInfinLoop"] = "0";

				// throw new System.Exception();  
				loopPrevention++;

				Session["countLoopsToPreventInfinLoop"] = loopPrevention;

			}
			catch (Exception exMsg)
			{

				int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

				if (loopPrevention == 0)
				{
					CQExceptionLayer cqexception = new CQExceptionLayer();

					Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

					result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocument", theErrMethod, exMsg.Message.ToString());

					Master.ErrorText = result;

					loopPrevention++;

					Session["countLoopsToPreventInfinLoop"] = loopPrevention;

					return result;
				}
				else
				{
					loopPrevention++;

					Session["countLoopsToPreventInfinLoop"] = loopPrevention;

					return "Please refresh this page.";
				}

			}

			return result;
		}
		#endregion ErrorHandling

		#region Render
		protected override void Render(HtmlTextWriter writer)
		{
			try
			{
				base.Render(writer);
				SavePetNames();
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion Render

		#region SavePetNames
		protected void SavePetNames()
		{
			try
			{
				{
					GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
					settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "ReceivingDocumentRadGridOrders", (int)Session["OperatorId"]);
				}
				{
					GridSettingsPersister settings = new GridSettingsPersister(RadGridOrderLines);
					settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrderLines, "ReceivingDocumentRadGridOrderLines", (int)Session["OperatorId"]);
				}
				{
					GridSettingsPersister settings = new GridSettingsPersister(RadGridJobs);
					settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridJobs, "ReceivingDocumentRadGridJobs", (int)Session["OperatorId"]);
				}
				{
					GridSettingsPersister settings = new GridSettingsPersister(RadGridDetails);
					settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridDetails, "ReceivingDocumentRadGridDetails", (int)Session["OperatorId"]);
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion SavePetNames

		#region ButtonSaveSettings_Click
		protected void ButtonSaveSettings_Click(object sender, EventArgs e)
		{
			try
			{
				SavePetNames();
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion ButtonSaveSettings_Click

		#region Page_Init
		protected void Page_Init(object sender, EventArgs e)
		{
			try
			{
				Session["countLoopsToPreventInfinLoop"] = 0;

				if (!Page.IsPostBack)
				{
					{
						GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
						settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "ReceivingDocumentRadGridOrders", (int)Session["OperatorId"]);
					}
					{
						GridSettingsPersister settings = new GridSettingsPersister(RadGridOrderLines);
						settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrderLines, "ReceivingDocumentRadGridOrderLines", (int)Session["OperatorId"]);
					}
					{
						GridSettingsPersister settings = new GridSettingsPersister(RadGridJobs);
						settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridJobs, "ReceivingDocumentRadGridJobs", (int)Session["OperatorId"]);
					}
					{
						GridSettingsPersister settings = new GridSettingsPersister(RadGridDetails);
						settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridDetails, "ReceivingDocumentRadGridDetails", (int)Session["OperatorId"]);
					}
				}
			}
			catch (Exception ex)
			{
				result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
				Master.ErrorText = result;
			}
		}
		#endregion Page_Init
		protected void RadGridOrderLines_DataBound(object sender, EventArgs e)
		{
			DetailsLinesReceived.DataBind();
		}
		protected void ObjectDataSourceReceiptDocument_Updating(object sender, ObjectDataSourceMethodEventArgs e)
		{

		}
	}
}