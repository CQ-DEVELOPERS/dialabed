using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_CustomerReturn : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        theErrMethod = "Page Load";

        if (!Page.IsPostBack)
        {
            try
            {
                Session["countLoopsToPreventInfinLoop"] = 0;

                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                string pageType = Request.QueryString["pageType"];

                if (pageType == "" || pageType == null)
                    Session["ReturnType"] = "RET";
                else
                    Session["ReturnType"] = pageType;

                DocumentTypeChange();

                Master.MsgText = "Page load successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
                Master.MsgText = result; Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            GridViewInboundDocument.DataBind();

            Master.MsgText = "GridView Successful";
            Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonInsertLine_Click
    protected void ButtonInsertLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInsertLine_Click";
        
        try
        {
            if (Session["StorageUnitId"] == null || (int)Session["StorageUnitId"] == -1)
            {
                Master.ErrorText = "Please select a Product.";
                LabelErrorMsg.Text = "Please select a Product.";
                return;
            }

            if (TextBoxQuantity.Text == "" || TextBoxQuantity.Text == "0")
            {
                Master.ErrorText = "Please enter a Quantity.";
                LabelErrorMsg.Text = "Please enter a Quantity.";
                return;
            }

            Decimal quantity = Decimal.Parse(TextBoxQuantity.Text);

            if (Session["InboundDocumentId"] == null)
            {
                Master.ErrorText = "Please select a Document.";
                LabelErrorMsg.Text = "Please select a Document.";
                return;
            }

            InboundDocument outboundDocument = new InboundDocument();

            if (Session["BatchId"] == null)
                Session["BatchId"] = -1;

            if (Session["ReasonId"] == null)
                Session["ReasonId"] = -1;

            if (Session["ReasonId"].ToString() == "-1")
            {
                Master.ErrorText = "Please select a Reason.";
                LabelErrorMsg.Text = "Please select a Reason.";
                return;
            }

            if (outboundDocument.CreateInboundLine(Session["ConnectionStringName"].ToString(),
                                                    int.Parse(Session["OperatorId"].ToString()),
                                                    int.Parse(Session["InboundDocumentId"].ToString()),
                                                    int.Parse(Session["StorageUnitId"].ToString()),
                                                    quantity,
                                                    int.Parse(Session["BatchId"].ToString()),
                                                    int.Parse(Session["ReasonId"].ToString())))
            {
                TextBoxQuantity.Text = "";
                Session["StorageUnitId"] = null;
                Session["BatchId"] = null;
                Session["ReasonId"] = null;
                Master.ErrorText = "";
                LabelErrorMsg.Text = "";

                GridViewInboundLine.DataBind();

                Master.MsgText = "InsertLine";
                Master.ErrorText = "";
            }
            else
            {
                Master.ErrorText = "May not enter duplicate Product /  Batch Combination";
                LabelErrorMsg.Text = "May not enter duplicate Product /  Batch Combination";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonInsertLine_Click"

    #region ButtonDeleteLine_Click
    protected void ButtonDeleteLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeleteLine_Click";
        try
        {
            if (GridViewInboundLine.SelectedIndex != -1)
                GridViewInboundLine.DeleteRow(GridViewInboundLine.SelectedIndex);

            Master.MsgText = "Deleted"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDeleteLine_Click"

    #region GridViewInboundDocument_OnSelectedIndexChanged
    protected void GridViewInboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewInboundDocument.SelectedIndex != -1)
            {
                Session["InboundDocumentId"] = int.Parse(GridViewInboundDocument.SelectedDataKey["InboundDocumentId"].ToString());
                Session["ExternalCompanyId"] = int.Parse(GridViewInboundDocument.SelectedDataKey["ExternalCompanyId"].ToString());
                Session["ReferenceNumber"] = GridViewInboundDocument.SelectedDataKey["ReferenceNumber"].ToString();
            }

            DetailsViewInboundDocument.DataBind();

            DetailsViewInboundDocument.ChangeMode(DetailsViewMode.ReadOnly);

            DocumentTypeChange(Session["ReferenceNumber"].ToString());

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "GridViewInboundDocument_OnSelectedIndexChanged"

    #region DetailsViewInboundDocument_OnModeChanging
    protected void DetailsViewInboundDocument_OnModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        theErrMethod = "DetailsViewInboundDocument_OnModeChanging";
        try
        {
            if (e.NewMode == DetailsViewMode.Insert)
            {
                Session["ExternalCompanyId"] = null;
                Session["InboundDocumentId"] = null;
            }

            if (DetailsViewInboundDocument.Rows.Count == 0)
                ButtonChangeMode.Visible = true;

            Master.MsgText = "Document Changed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion DetailsViewInboundDocument_OnModeChanging

    #region DetailsViewInboundDocument_OnItemInserting
    protected void DetailsViewInboundDocument_OnItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        theErrMethod = "DetailsViewInboundDocument_OnItemInserting";
        try
        {
            string returnType = "RET";

            if (Session["ReturnType"] != null)
                returnType = Session["ReturnType"].ToString();

            if (returnType == "UAC")
            {
                if (Session["ExternalCompanyId"] == null || (int)Session["ExternalCompanyId"] == -1)
                {
                    Master.ErrorText = "Please select a Customer first.";
                    LabelErrorTab2.Text = "Please select a Customer first";
                    e.Cancel = true;
                }
                else
                    LabelErrorTab2.Text = "";
            }

            if (returnType == "RET")
            {
                InboundDocument id = new InboundDocument();

                if (!id.ValidInvoice(Session["ConnectionStringName"].ToString(), e.Values["ReferenceNumber"].ToString()))
                {
                    Master.ErrorText = "Invoice Number is Invalid.";
                    LabelErrorTab2.Text = "Invoice Number is Invalid.";
                    e.Cancel = true;
                }
                else
                    Session["ReferenceNumber"] = e.Values["ReferenceNumber"];
                    LabelErrorTab2.Text = "";
            }

            Master.MsgText = "Item Insert"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion DetailsViewInboundDocument_OnItemInserting

    #region ObjectDataSourceInboundDocumentUpdate_OnInserted
    protected void ObjectDataSourceInboundDocumentUpdate_OnInserted(object source, ObjectDataSourceStatusEventArgs e)
    {
        theErrMethod = "ObjectDataSourceInboundDocumentUpdate_OnInserted";
        try
        {
            if (e.ReturnValue.ToString() != "0")
            {
                Session["InboundDocumentId"] = e.ReturnValue;
                DetailsViewInboundDocument.ChangeMode(DetailsViewMode.ReadOnly);
                DetailsViewInboundDocument.DataBind();
                // Karen - June 2012 - Populate invoice lines
                InboundDocument id = new InboundDocument();
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 316))
                    if (id.CreateInboundLineFromInvoice(Session["ConnectionStringName"].ToString(),
                                                int.Parse(Session["OperatorId"].ToString()),
                                                int.Parse(Session["InboundDocumentId"].ToString()),
                                                int.Parse(Session["referenceNumber"].ToString())))
                // ** End              
                LabelErrorTab2.Text = "";
            }
            else
            {
                ButtonChangeMode.Visible = true;
                LabelErrorTab2.Text = "Duplicate Order Number and Customer Combination.";
            }
            Master.MsgText = "Updated"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceInboundDocumentUpdate_OnInserted

    #region ButtonChangeMode_Click
    protected void ButtonChangeMode_Click(object sender, EventArgs e)
    {
        DetailsViewInboundDocument.ChangeMode(DetailsViewMode.Insert);
        ButtonChangeMode.Visible = false;
        LabelErrorTab2.Text = "";
    }
    #endregion ButtonChangeMode_Click

    #region ButtonComplete_Click
    protected void ButtonComplete_Click(object sender, EventArgs e)
    {
        try
        {
            InboundDocument id = new InboundDocument();

            if (id.CustomerReturnComplete(Session["ConnectionStringName"].ToString(), (int)Session["InboundDocumentId"], (int)Session["OperatorId"]))
            {
                Master.MsgText = "Customer Return Complete"; Master.ErrorText = "";
            }
            else
            {
                Master.MsgText = ""; Master.ErrorText = "Customer Return Failed";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonComplete_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "InboundDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + exMsg.Message.ToString());
                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

    #region DocumentTypeChange
    protected void DocumentTypeChange()
    {
        string returnType = "RET";

        if (Session["ReturnType"] != null)
            returnType = Session["ReturnType"].ToString();

        if (returnType == "UAC")
        {
            PanelCustomer.Visible = true;
            GridViewInboundLine.Columns[8].Visible = true;
        }
        else
        {
            PanelCustomer.Visible = false;
            GridViewInboundLine.Columns[8].Visible = false;
        }

        if (DetailsViewInboundDocument.Rows.Count == 0)
        {
            ButtonChangeMode.Visible = false;
            DetailsViewInboundDocument.ChangeMode(DetailsViewMode.Insert);
        }
    }
    protected void DocumentTypeChange(string referenceNumber)
    {
        if (referenceNumber == "")
        {
            PanelCustomer.Visible = true;
        }
        else
        {
            PanelCustomer.Visible = false;
        }
    }
    #endregion DocumentTypeChange

    #region DetailsViewInboundDocument_OnDataBound
    protected void DetailsViewInboundDocument_OnDataBound(object sender, EventArgs e)
    {
        try
        {
            string returnType = "RET";

            if (Session["ReturnType"] != null)
                returnType = Session["ReturnType"].ToString();

            switch (DetailsViewInboundDocument.CurrentMode.ToString())
            {
                case "ReadOnly":
                    break;
                case "Insert":
                    if (returnType == "UAC")
                    {
                        ((TextBox)(DetailsViewInboundDocument.FindControl("TextBoxReferenceNumberInsert"))).Text = "";
                        //((Label)(DetailsViewInboundDocument.FindControl("LabelReferenceNumberInsert"))).Visible = true;
                        //((TextBox)(DetailsViewInboundDocument.FindControl("TextBoxReferenceNumberInsert"))).Visible = false;
                        ((TextBox)(DetailsViewInboundDocument.FindControl("TextBoxReferenceNumberInsert"))).Visible = true;
                        ((RequiredFieldValidator)(DetailsViewInboundDocument.FindControl("REQReferenceNumberInsert"))).Enabled = false;
                    }
                    else
                    {
                        ((Label)(DetailsViewInboundDocument.FindControl("LabelReferenceNumberInsert"))).Visible = false;
                        ((TextBox)(DetailsViewInboundDocument.FindControl("TextBoxReferenceNumberInsert"))).Visible = true;
                        ((RequiredFieldValidator)(DetailsViewInboundDocument.FindControl("REQReferenceNumberInsert"))).Enabled = true;
                    }

                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 253))
                    {
                        ((TextBox)(DetailsViewInboundDocument.FindControl("TextBoxOrderNumberInsert"))).Enabled = false;
                        ((RequiredFieldValidator)(DetailsViewInboundDocument.FindControl("REQOrderNumberInsert"))).Enabled = false;

                    }
                    
                    break;
                case "Edit":
                    if (returnType == "UAC")
                    {
                        ((TextBox)(DetailsViewInboundDocument.FindControl("LabelReferenceNumberEdit"))).Text = "";
                        ((Label)(DetailsViewInboundDocument.FindControl("LabelUnappliedCredit"))).Visible = true;
                        ((TextBox)(DetailsViewInboundDocument.FindControl("LabelReferenceNumberEdit"))).Visible = false;
                        ((RequiredFieldValidator)(DetailsViewInboundDocument.FindControl("REQReferenceNumberEdit"))).Enabled = false;
                    }
                    else
                    {
                        ((Label)(DetailsViewInboundDocument.FindControl("LabelUnappliedCredit"))).Visible = false;
                        ((TextBox)(DetailsViewInboundDocument.FindControl("LabelReferenceNumberEdit"))).Visible = true;
                        ((RequiredFieldValidator)(DetailsViewInboundDocument.FindControl("REQReferenceNumberEdit"))).Enabled = true;
                    }
                    break;
            }

            if (DetailsViewInboundDocument.Rows.Count == 0)
                ButtonChangeMode.Visible = true; //DetailsViewInboundDocument.ChangeMode(DetailsViewMode.Insert);
        }
        catch { }
    }
    #endregion DetailsViewInboundDocument_OnDataBound

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            Session["FromURL"] = "~/Inbound/CustomerReturn.aspx";

            Session["ReportName"] = "Driver Upliftment Instructions";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[4];

            //// Create the ConnectionString report parameter
            //string strReportConnectionString = Session["ReportConnectionString"].ToString();
            //RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);

            //// Create the InboundShipmentId report parameter
            //string inboundShipmentId = GridViewInboundShipment.SelectedDataKey.Value.ToString();
            //RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("InboundShipmentId", inboundShipmentId);

            // Create the ReceiptId report parameter
            //string receiptId = "-1";
            //RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("InboundDocumentId", inboundDocumentId);
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("InboundDocumentId", Session["InboundDocumentId"].ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
            Master.MsgText = "Printed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("CustomerReturn" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrint_Click
}