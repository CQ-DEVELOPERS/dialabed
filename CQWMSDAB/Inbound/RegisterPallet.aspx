<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="RegisterPallet.aspx.cs" Inherits="Inbound_RegisterPallet" Title="<%$ Resources:Default, RegisterPalletTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/StorageUnitBatchIdSearch.ascx" TagName="StorageUnitBatchIdSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, RegisterPalletTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, RegisterPalletAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionRegisterPallet" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <a href="" class="accordionLink">1. Select a Product</a></Header>
                <Content>
                    <asp:UpdatePanel runat="server" ID="UpdatePanelProductSearch">
                        <ContentTemplate>
                            <uc1:StorageUnitBatchIdSearch ID="StorageUnitBatchIdSearch1" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                <Header>
                    <a href="" class="accordionLink">2. Enter Quantity</a></Header>
                <Content>
                    <asp:UpdatePanel runat="server" ID="UpdatePanelTextBoxQuantity">
                        <ContentTemplate>
                            <asp:Label ID="LabelQuantity" runat="server"></asp:Label>
                            <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers" TargetControlID="TextBoxQuantity"></ajaxToolkit:FilteredTextBoxExtender>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonInsertLine" runat="server" Text="Insert" OnClick="ButtonInsertLine_Click" />
    <asp:Button ID="ButtonDeleteLine" runat="server" Text="Delete" OnClick="ButtonDeleteLine_Click" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDeleteLine" runat="server" ConfirmText="Press OK if you are sure you want to delete the selected row." TargetControlID="ButtonDeleteLine"></ajaxToolkit:ConfirmButtonExtender>
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInboundLine">
        <ContentTemplate>
            <asp:Label ID="LabelErrorMsg" runat="server" Text="" EnableTheming="false" ForeColor="Red"></asp:Label>
            <asp:GridView ID="GridViewInboundLine" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                AutoGenerateSelectButton="true" AutoGenerateEditButton="true" DataKeyNames="InboundLineId"
                DataSourceID="ObjectDataSourceInboundLine">
                <Columns>
                    <asp:BoundField DataField="ProductCode" ReadOnly="true" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" ReadOnly="true" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="SKUCode" ReadOnly="true" HeaderText="<%$ Resources:Default, SKUCode %>" />
                    <asp:BoundField DataField="Batch" ReadOnly="true" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ECLNumber" ReadOnly="true" HeaderText="<%$ Resources:Default, ECLNumber %>" />
                    <asp:BoundField DataField="Status" ReadOnly="true" HeaderText="<%$ Resources:Default, Status %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
                    <asp:BoundField DataField="PalletQuantity" ReadOnly="true" HeaderText="<%$ Resources:Default, PalletQuantity %>" />
                    <asp:BoundField DataField="OverYield" ReadOnly="true" HeaderText="<%$ Resources:Default, OverYield %>" />
                    <asp:BoundField DataField="ExpectedYield" ReadOnly="true" HeaderText="<%$ Resources:Default, ExpectedYield %>" />
                    <asp:BoundField DataField="ActualYield" ReadOnly="true" HeaderText="<%$ Resources:Default, ActualYield %>" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonDeleteLine" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSourceInboundLine" runat="server" TypeName="Production"
        SelectMethod="SearchProductionLines" UpdateMethod="UpdateInboundLine" InsertMethod="CreateInboundLine"
        DeleteMethod="DeleteInboundLine">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Type="Int32" Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="InboundLineId" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="Quantity" Type="Decimal"></asp:Parameter>
        </UpdateParameters>
        <InsertParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
            <asp:SessionParameter Name="InboundDocumentId" SessionField="InboundDocumentId" Type="Int32" />
            <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" />
            <asp:ControlParameter Name="Quantity" ControlID="TextBoxQuantity" Type="Decimal" />
            <asp:SessionParameter Name="BatchId" SessionField="BatchId" Type="Int32" />
        </InsertParameters>
        <DeleteParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="InboundLineId" Type="Int32"></asp:Parameter>
        </DeleteParameters>
    </asp:ObjectDataSource>
</asp:Content>

