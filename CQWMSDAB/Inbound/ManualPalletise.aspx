<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManualPalletise.aspx.cs" Inherits="Inbound_ManualPalletise"
    Title="<%$ Resources:Default, ManualPalletiseTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ManualPalletiseTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ManualPalletiseAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptLine">
        <ContentTemplate>
            <asp:GridView ID="GridViewReceiptLine" runat="server" AllowPaging="true" DataKeyNames="ReceiptLineId" AutoGenerateColumns="false" DataSourceID="ObjectDataSourceGridViewReceiptLine">
                <Columns>
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                    <asp:BoundField DataField="AcceptedQuantity" HeaderText="<%$ Resources:Default, AcceptedQuantity %>" />
                    <asp:BoundField DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="PalletQuantity" HeaderText="<%$ Resources:Default, PalletQuantity %>" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceGridViewReceiptLine" runat="server"
                TypeName="ManualPalletise"
                SelectMethod="GetReceiptLines">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="ReceiptLineId" SessionField="ReceiptLineId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNextReceipt" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonCreateLines" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelQuantity" runat="server" Text="Enter number of put away lines to create:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLines" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="ButtonCreateLines" runat="server" Text="Create Lines" OnClick="ButtonCreateLines_Click" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table>
        <tr>
            <td>
                <asp:Button ID="ButtonAutoLocations" runat="server" Text="Auto Allocate" OnClick="ButtonAutoLocations_Click" />
            </td>
            <td>
                <asp:Button ID="ButtonManualLocations" runat="server" Text="Manual Allocate" OnClick="ButtonManualLocations_Click" />
            </td>
            <td>
                <asp:Button ID="ButtonGetNextReceipt" runat="server" Text="Next Receipt" OnClick="GetNextReceiptLine_Click" />
            </td>
            <td>
                <asp:Button ID="ButtonReturn" runat="server" Text="Return" OnClick="ButtonReturn_Click" />
            </td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
        <ContentTemplate>
            <asp:GridView ID="GridViewInstruction" runat="server" AllowPaging="true" DataKeyNames="InstructionId" AutoGenerateColumns="false" AutoGenerateEditButton="true" DataSourceID="ObjectDataSourceGridViewInstruction">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server" Checked="true"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
                    <asp:BoundField ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" />
                    <asp:BoundField ReadOnly="true" DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>" />
                    <asp:BoundField ReadOnly="true" DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>" />
                    <asp:BoundField ReadOnly="true" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" />
                    <asp:BoundField ReadOnly="true" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField ReadOnly="true" DataField="ECLNumber" HeaderText="<%$ Resources:Default, ECLNumber %>" />
                </Columns>
            </asp:GridView>
            
            <asp:ObjectDataSource ID="ObjectDataSourceGridViewInstruction" runat="server"
                TypeName="ManualPalletise"
                SelectMethod="GetInstructions"
                UpdateMethod="UpdateInstructions">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="ReceiptLineId" SessionField="ReceiptLineId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="InstructionId" Type="Int32" />
                    <asp:Parameter Name="Quantity" Type="Decimal" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonCreateLines" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonAutoLocations" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNextReceipt" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

