<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReceivingDocumentSearch.aspx.cs" Inherits="Inbound_ReceivingDocumentSearch"
    Title="<%$ Resources:Default, ReceivingDocumentSearchTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/InboundShipmentSearch.ascx" TagName="InboundShipmentSearch" TagPrefix="uc1" %>
<%@ Register Src="../Common/ProductSearchPackaging.ascx" TagName="ProductSearch" TagPrefix="ucpsp" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

    <script type="text/javascript" language="JavaScript">

        function openNewWindowBatch() {
            window.open("../StaticInfo/BatchMaintenance.aspx", "_blank",
      "height=600px width=450px top=200 left=200 resizable=no scrollbars=no ");
        }


    </script>

    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <div style="float: left;">
                    <uc1:InboundShipmentSearch ID="InboundShipmentSearch1" runat="server"></uc1:InboundShipmentSearch>
                </div>
                <asp:Button ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search %>"
                    OnClick="ButtonDocumentSearch_Click" />
                <asp:Button ID="ButtonDeliveryConformance" runat="server" Text="<%$ Resources:Default, DeliveryConformance %>"
                    OnClick="ButtonDeliveryConformance_Click" Style="width: auto;" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptDocument">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewReceiptDocument" runat="server" DataSourceID="ObjectDataSourceReceiptDocument"
                            DataKeyNames="ReceiptId,AllowPalletise,OrderNumber" AutoGenerateSelectButton="true"
                            AutoGenerateColumns="false" AllowPaging="true" OnSelectedIndexChanged="GridViewReceiptDocument_SelectedIndexChanged"
                            AutoGenerateEditButton="true" AllowSorting="true" OnRowDataBound="GridViewReceiptDocument_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="InboundShipmentId" HeaderText='<%$ Resources:Default,InboundShipmentId %>'
                                    ReadOnly="true" SortExpression="InboundShipmentId" />
                                <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>'
                                    ReadOnly="true" SortExpression="OrderNumber" />
                                <asp:BoundField ReadOnly="True" DataField="PrincipalCode" HeaderText="<%$ Resources:Default, Principal %>"
                                    SortExpression="PrincipalCode"></asp:BoundField>
                                <asp:TemplateField HeaderText='<%$ Resources:Default,Delivery %>' SortExpression="Delivery">
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelDelivery" runat="server" Text='<%# Bind("Delivery") %>' Font-Bold="true"></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SupplierCode" HeaderText='<%$ Resources:Default,SupplierCode %>'
                                    ReadOnly="true" SortExpression="SupplierCode" />
                                <asp:BoundField DataField="Supplier" HeaderText='<%$ Resources:Default,Supplier %>'
                                    ReadOnly="true" SortExpression="Supplier" />
                                <asp:BoundField DataField="NumberOfLines" HeaderText='<%$ Resources:Default,NumberOfLines %>'
                                    ReadOnly="true" SortExpression="NumberOfLines" />
                                <asp:BoundField DataField="PlannedDeliveryDate" HeaderText='<%$ Resources:Default,PlannedDeliveryDate %>'
                                    ReadOnly="true" SortExpression="PlannedDeliveryDate" />
                                <asp:BoundField DataField="DeliveryDate" HeaderText='<%$ Resources:Default,DeliveryDate %>'
                                    SortExpression="DeliveryDate" ApplyFormatInEditMode="true" DataFormatString="{0:d}" />
                                <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' ReadOnly="true"
                                    SortExpression="Status" />
                                <asp:BoundField DataField="InboundDocumentType" HeaderText='<%$ Resources:Default,InboundDocumentType %>'
                                    ReadOnly="true" SortExpression="InboundDocumentType" />
                                <asp:TemplateField HeaderText='<%$ Resources:Default,Location %>' SortExpression="Location">
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DDLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Rating" HeaderText='<%$ Resources:Default,Rating %>' ReadOnly="true"
                                    SortExpression="Rating" />
                                <asp:BoundField DataField="DeliveryNoteNumber" HeaderText='<%$ Resources:Default,DeliveryNoteNumber %>'
                                    SortExpression="DeliveryNoteNumber" />
                                <asp:BoundField DataField="SealNumber" HeaderText='<%$ Resources:Default,SealNumber %>'
                                    SortExpression="SealNumber" />
                                <asp:TemplateField HeaderText='<%$ Resources:Default,Priority %>' SortExpression="Priority">
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelPriority" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DDPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="VehicleRegistration" HeaderText='<%$ Resources:Default,VehicleRegistration %>'
                                    SortExpression="VehicleRegistration" />
                                <asp:TemplateField HeaderText='<%$ Resources:Default,ShippingAgent %>' SortExpression="ShippingAgent">
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelShippingAgent" runat="server" Text='<%# Bind("ShippingAgent") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DDShippingAgent" runat="server" DataSourceID="ObjectDataSourceShippingAgent"
                                            DataTextField="ShippingAgent" DataValueField="ShippingAgentId" SelectedValue='<%# Bind("ShippingAgentId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ContainerNumber" HeaderText='<%$ Resources:Default,ContainerNumber %>'
                                    SortExpression="ContainerNumber" />
                                <asp:BoundField DataField="Remarks" HeaderText='<%$ Resources:Default,Remarks %>'
                                    SortExpression="Remarks" />
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewReceiptDocument" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceReceiptDocument" runat="server" TypeName="Receiving"
                    SelectMethod="GetReceivingDocuments" UpdateMethod="UpdateReceipt">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="InboundShipmentId" DefaultValue="-1" Type="Int32" SessionField="ParameterInboundShipmentId" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="allowPalletise" Type="Boolean" />
                        <asp:Parameter Name="deliveryDate" Type="DateTime" />
                        <asp:Parameter Name="locationId" DefaultValue="-1" Type="Int32" />
                        <asp:Parameter Name="deliveryNoteNumber" Type="String" />
                        <asp:Parameter Name="sealNumber" Type="String" />
                        <asp:Parameter Name="priorityId" DefaultValue="-1" Type="Int32" />
                        <asp:Parameter Name="vehicleRegistration" Type="String" />
                        <asp:Parameter Name="remarks" Type="String" />
                        <asp:Parameter Name="receiptId" DefaultValue="-1" Type="Int32" />
                        <asp:Parameter Name="delivery" DefaultValue="-1" Type="Int32" />
                        <asp:Parameter Name="orderNumber" DefaultValue="-1" Type="String" />
                        <asp:Parameter Name="inboundShipmentId" DefaultValue="-1" Type="Int32" />
                        <asp:Parameter Name="containerNumber" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="AreaCode" Type="String" DefaultValue="R" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" TypeName="Priority"
                    SelectMethod="GetPriorities">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceShippingAgent" runat="server" TypeName="ExternalCompany"
                    SelectMethod="ExternalCompaniesByType">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="externalCompanyTypeCode" DefaultValue="SHAG" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, ReceiveLines%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="border: 1px 1px">
                            <asp:Panel ID="Panel1" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small"
                                Font-Names="Tahoma" Width="180" HorizontalAlign="Center" DefaultButton="ButtonSelect">
                                <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                                    OnClick="ButtonSelect_Click"></asp:Button>
                                <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, CheckSheet%>"
                                    OnClick="ButtonPrint_Click" ToolTip="To view only one line, select that line first and then click Check Sheet..."></asp:Button>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel ID="Panel2" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small"
                                Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel4">
                                    <ContentTemplate>
                                        <asp:Panel ID="PanelEdit" runat="server">
                                            <asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, Edit%>" OnClick="ButtonEdit_Click"></asp:Button>
                                            <asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, Save%>" OnClick="ButtonSave_Click"></asp:Button>
                                            <br />
                                            <br />
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelButtonRedelivery">
                                    <ContentTemplate>
                                        <asp:Panel ID="PanelLinesReceived" runat="server">
                                            <asp:Button ID="ButtonRedelivery" runat="server" Text="<%$ Resources:Default, Redelivery%>"
                                                OnClick="ButtonRedelivery_Click" Enabled="false"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonRedelivery" runat="server"
                                                TargetControlID="ButtonRedelivery" ConfirmText="<%$ Resources:Default, PressOKConfirmRedelivery%>">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                            <asp:Button ID="ButtonReceived" runat="server" Text="<%$ Resources:Default, Complete%>"
                                                OnClick="ButtonReceived_Click" Enabled="false"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonReceived" runat="server"
                                                TargetControlID="ButtonReceived" ConfirmText="<%$ Resources:Default, RedeliveryNotRequested%>">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultDelivery" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultReceived" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultZero" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel ID="Panel3" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small"
                                Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel6" runat="server">
                                            <asp:Button ID="ButtonReject" runat="server" Text="<%$ Resources:Default, Reject%>"
                                                OnClick="ButtonReject_Click"></asp:Button>
                                            <asp:Button ID="ButtonCreditAdvice" runat="server" Text="<%$ Resources:Default, CreditAdviceIndicator%>"
                                                OnClick="ButtonCreditAdvice_Click"></asp:Button>
                                            <br />
                                            <br />
                                            <asp:Button ID="ButtonPalletise" runat="server" Text="<%$ Resources:Default, Palletise%>"
                                                OnClick="ButtonPalletise_Click" Enabled="false"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="ButtonPalletise"
                                                ConfirmText="<%$ Resources:Default, PressOKPalletise%>">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                            <asp:Button ID="ButtonAutoLocations" runat="server" Text="<%$ Resources:Default, AutoLocate%>"
                                                OnClick="ButtonAutoLocations_Click" Enabled="false"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="ButtonAutoLocations"
                                                ConfirmText="<%$ Resources:Default, PressOKAllocateLocations%>">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultDelivery" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultReceived" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultZero" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel ID="Panel4" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small"
                                Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                <asp:Button ID="ButtonManualLocations" Visible="False" runat="server" Text="<%$ Resources:Default, ManualAllocation%>"
                                    OnClick="ButtonManualLocations_Click" Enabled="false"></asp:Button>
                                <asp:Button ID="ButtonLink" runat="server" Text="<%$ Resources:Default, LinkSerial%>"
                                    OnClick="ButtonLink_Click"></asp:Button>
                                <br />
                                <br />
                                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel5" runat="server">
                                            <asp:Button ID="ButtonPOD" runat="server" Text="<%$ Resources:Default, PrintGRN%>"
                                                OnClick="ButtonPOD_Click" Enabled="false"></asp:Button>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="UpdatePanelLinesReceived">
                                <ContentTemplate>
                                    <asp:DetailsView ID="DetailsLinesReceived" runat="server" DataKeyNames="LinesReceived,TotalLines"
                                        DataSourceID="ObjectDataSourceLinesReceived" AutoGenerateRows="False">
                                        <Fields>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, LinesReceived %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="TextBoxLinesReceived" runat="server" Text='<%# Bind("LinesReceived") %>'></asp:Label>
                                                    <asp:Label ID="LabelOf" runat="server" Text="<%$ Resources:Default, Of%>"></asp:Label>
                                                    <asp:Label ID="TextBoxTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceLinesReceived" runat="server" TypeName="Receiving"
                                        SelectMethod="LinesReceived">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="inboundShipmentId" SessionField="InboundShipmentId" Type="Int32" />
                                            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanel3">
                    <ContentTemplate>
                        <asp:Panel ID="PanelDefault" runat="server" GroupingText="<%$ Resources:Default, DefaultQuantities%>"
                            ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                            <asp:Button ID="ButtonDefaultDelivery" runat="server" Text="<%$ Resources:Default, Delivery%>"
                                OnClick="ButtonDefaultDelivery_Click"></asp:Button>
                            <asp:Button ID="ButtonDefaultReceived" runat="server" Text="<%$ Resources:Default, Received%>"
                                OnClick="ButtonDefaultActual_Click"></asp:Button>
                            <asp:Button ID="ButtonDefaultZero" runat="server" Text="<%$ Resources:Default, ZeroLines%>"
                                OnClick="ButtonDefaultZero_Click"></asp:Button>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultDelivery" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultReceived" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultZero" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptLine">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewReceiptLine" runat="server" DataSourceID="ObjectDataSourceReceiptLine"
                            AutoGenerateColumns="false" DataKeyNames="ReceiptLineId,StorageUnitId,BatchId"
                            AllowPaging="true" AutoGenerateSelectButton="False" PageSize="30">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                        <%--<asp:HiddenField ID="HiddenNumberOfPallets" runat="server" Value='<%# Bind("NumberOfPallets") %>' />--%>
                                        <asp:HiddenField ID="HiddenReceiptLineId" runat="server" Value='<%# Bind("ReceiptLineId") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="LineNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, LineNumber %>"
                                    SortExpression="LineNumber" />
                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product" />
                                <asp:BoundField DataField="ChildProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ChildProductCode %>"
                                    SortExpression="ChildProductCode" />
                                <asp:BoundField DataField="ChildProduct" ReadOnly="True" HeaderText="<%$ Resources:Default, ChildProduct %>"
                                    SortExpression="ChildProduct" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>">
                                    <EditItemTemplate>
                                        <asp:Button ID="ButtonBatchPopup" runat="server" Text="<%$ Resources:Default, ButtonBatchPopup %>"
                                            Visible='<%# Eval("BatchButton") %>' CausesValidation="false" OnClientClick="javascript:openNewWindowBatch();" />
                                        <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceBatch"
                                            DataTextField="Batch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>'
                                            Visible='<%# Eval("BatchSelect") %>' OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode" />
                                <asp:BoundField DataField="SKU" ReadOnly="True" HeaderText="<%$ Resources:Default, SKU %>"
                                    SortExpression="SKU" />
                                <asp:BoundField DataField="RequiredQuantity" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                    SortExpression="RequiredQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>"
                                    SortExpression="DeliveryNoteQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="ReceivedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, ReceivedQuantity %>"
                                    SortExpression="ReceivedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <%--
                                <asp:BoundField DataField="AcceptedQuantity" ReadOnly="True" HeaderText="<%$ Resources:Default, AcceptedQuantity %>"
                                    SortExpression="AcceptedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                --%>
                                <asp:BoundField DataField="AcceptedWeight" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedWeight %>"
                                    SortExpression="AcceptedWeight" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AlternateBatch %>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxAlternateBatch" runat="server" Checked='<%# Bind("AlternateBatch") %>'></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SampleQuantity" HeaderText="<%$ Resources:Default, SampleQuantity %>"
                                    SortExpression="SampleQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="AssaySamples" HeaderText="<%$ Resources:Default, AssaySamples %>"
                                    SortExpression="AssaySamples" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="RejectQuantity" HeaderText="<%$ Resources:Default, RejectQuantity %>"
                                    SortExpression="RejectQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Reason %>">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceReason"
                                            DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelBindReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>"
                                    SortExpression="CreditAdviceIndicator" />
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber" />
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status" />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceReceiptLine" runat="server" TypeName="Receiving"
                            SelectMethod="GetReceiptLines" OnSelecting="ObjectDataSourceReceiptLine_OnSelecting"
                            UpdateMethod="SetReceiptLines" OnUpdating="ObjectDataSourceReceiptLine_OnUpdating"
                            OnUpdated="ObjectDataSourceReceiptLine_Updated">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="inboundShipmentId" Type="Int32" />
                                <asp:Parameter Name="receiptId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="ReceiptLineId" Type="Int32" />
                                <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" />
                                <asp:Parameter Name="Batch" Type="String" />
                                <asp:Parameter Name="OperatorId" Type="Int32" />
                                <asp:Parameter Name="ReceivedQuantity" Type="Decimal" />
                                <asp:Parameter Name="DeliveryNoteQuantity" Type="Decimal" />
                                <asp:Parameter Name="SampleQuantity" Type="Decimal" />
                                <asp:Parameter Name="AssaySamples" Type="Decimal" />
                                <asp:Parameter Name="AlternateBatch" Type="Boolean" />
                                <asp:Parameter Name="ReasonId" Type="Int32" />
                                <asp:Parameter Name="AcceptedWeight" Type="Decimal" />
                                <asp:Parameter Name="RejectQuantity" Type="Decimal" />
                                <asp:Parameter Name="StorageUnitId" Type="Int32" />
                                <asp:Parameter Name="BatchId" Type="Int32" />
                                <asp:Parameter Name="Boxes" Type="Decimal" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                            SelectMethod="GetBatchesNotExpired" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="storageUnitId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
                            SelectMethod="GetReasonsByType">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="REJ" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPalletise" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoLocations" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultDelivery" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultReceived" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultZero" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Label ID="LabelQty" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
                <asp:TextBox ID="TextBoxQty" runat="server" Text="1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers"
                    TargetControlID="TextBoxQty">
                </ajaxToolkit:FilteredTextBoxExtender>
                <asp:Button ID="ButtonPrintLabel" runat="server" OnClick="ButtonPrintLabel_Click"
                    Text="<%$ Resources:Default, PrintLabel%>" />
                <asp:Label ID="LabelSample" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
                <asp:TextBox ID="TextBoxSample" runat="server" Text="1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE2" runat="server" FilterType="Numbers"
                    TargetControlID="TextBoxSample">
                </ajaxToolkit:FilteredTextBoxExtender>
                <asp:Button ID="ButtonSample" runat="server" OnClick="ButtonPrintSample_Click" Text="<%$ Resources:Default, PrintSample%>" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Jobs%>">
            <ContentTemplate>
                <asp:Button ID="ButtonSelectJobs" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                    OnClick="ButtonSelectJobs_Click"></asp:Button>
                <asp:Button ID="ButtonPrintJob" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                    OnClick="ButtonPrintJob_Click" />
                <asp:Button ID="ButtonPrintCheckSheet" runat="server" Text="<%$ Resources:Default, ButtonPrintCheckSheet %>"
                    OnClick="ButtonPrintCheckSheet_Click" />
                <asp:Button ID="ButtonPalletWeight" runat="server" OnClick="ButtonPalletWeight_Click"
                    Text="<%$ Resources:Default, PalletWeight%>" />
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                            DataKeyNames="JobId,ReferenceNumber">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Label%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="Receiving"
                            SelectMethod="SearchPickingJobs">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="InboundShipmentId" Type="Int32" SessionField="InboundShipmentId" />
                                <asp:SessionParameter Name="ReceiptId" Type="Int32" SessionField="ReceiptId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelectJobs" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPrintJob" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="<%$ Resources:Default, Details%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelDetails" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelectPallet" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelectPallet_Click"></asp:Button>
                        <asp:Button ID="ButtonPrintPallet" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                            OnClick="ButtonPrintPallet_Click" />
                        <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                        <asp:Label ID="LabelLocationStandard" runat="server" Text="<%$ Resources:Default, NoLocation%>"></asp:Label>
                        <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                        <asp:Label ID="LabelLocationBlue" runat="server" Text="<%$ Resources:Default, Allocated%>"></asp:Label>
                        <asp:GridView ID="GridViewDetails" runat="server" DataSourceID="ObjectDataSourceDetails"
                            AutoGenerateColumns="False" DataKeyNames="InstructionId" AllowPaging="True" AllowSorting="True"
                            AutoGenerateEditButton="true">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Check%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>"
                                    SortExpression="SKU"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                    SortExpression="Quantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}"></asp:BoundField>
                                <asp:BoundField ReadOnly="False" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>"
                                    SortExpression="ConfirmedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                    SortExpression="InstructionType"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>"
                                    SortExpression="PickLocation"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>"
                                    SortExpression="StoreLocation"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>"
                                    SortExpression="Operator"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelectPallet" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPrintPallet" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="Receiving"
                    SelectMethod="SearchLinesByJob" UpdateMethod="UpdateLine">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="InstructionId" Type="Int32" />
                        <asp:Parameter Name="ConfirmedQuantity" Type="Decimal" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel5" HeaderText="<%$ Resources:Default, Packaging%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel8" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small"
                                Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel6">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel10" runat="server">
                                            <asp:Button ID="ButtonPackageComplete" runat="server" Text="<%$ Resources:Default, Complete%>"
                                                OnClick="ButtonPackageReceived_Click" Enabled="false"></asp:Button>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultDelivery" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultActual" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultZero" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="UpdatePanel9">
                                <ContentTemplate>
                                    <asp:DetailsView ID="DetailsPackageLinesReceived" runat="server" DataKeyNames="LinesReceived,TotalLines"
                                        DataSourceID="ObjectDataSourcePackageLinesReceived" AutoGenerateRows="False">
                                        <Fields>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, LinesReceived %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="TextBoxLinesReceived" runat="server" Text='<%# Bind("LinesReceived") %>'></asp:Label>
                                                    <asp:Label ID="LabelOf" runat="server" Text=" <%$ Resources:Default, Of%> "></asp:Label>
                                                    <asp:Label ID="TextBoxTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:ObjectDataSource ID="ObjectDataSourcePackageLinesReceived" runat="server" TypeName="Receiving"
                                        SelectMethod="PackageLinesReceived">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <table>
                    <td>
                        <asp:UpdatePanel runat="server" ID="UpdatePanelProductSearch">
                            <ContentTemplate>
                                <asp:Panel ID="PanelProductSearch" runat="server" GroupingText="<%$ Resources:Default, AddProduct%>"
                                    ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="380" HorizontalAlign="Center">
                                    <ucpsp:ProductSearch ID="ProductSearch2" runat="server" />
                                    <asp:Button ID="ButtonProductAdd" runat="server" Text="<%$ Resources:Default, Add%>"
                                        OnClick="ButtonProductAdd_Click" />
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel10">
                            <ContentTemplate>
                                <asp:Panel ID="Panel15" runat="server" GroupingText="<%$ Resources:Default, DefaultQuantities%>"
                                    ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                    <asp:Button ID="ButtonPackageDefaultDelivery" runat="server" Text="<%$ Resources:Default, Delivery%>"
                                        OnClick="ButtonDefaultDelivery_Click"></asp:Button>
                                    <asp:Button ID="ButtonPackageDefaultActual" runat="server" Text="<%$ Resources:Default, Actual%>"
                                        OnClick="ButtonDefaultActual_Click"></asp:Button>
                                    <asp:Button ID="ButtonPackageDefaultZero" runat="server" Text="<%$ Resources:Default, ZeroLines%>"
                                        OnClick="ButtonDefaultZero_Click"></asp:Button>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultDelivery" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultActual" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultZero" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanel11">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewPackageLines" runat="server" DataKeyNames="PackageLineId"
                            AllowPaging="true" AutoGenerateEditButton="true" AutoGenerateSelectButton="False"
                            PageSize="30">
                            <Columns>
                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product" />
                                <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode" />
                                <asp:BoundField DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>"
                                    SortExpression="DeliveryNoteQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="AcceptedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedQuantity %>"
                                    SortExpression="AcceptedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="RejectQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, RejectQuantity %>"
                                    SortExpression="RejectQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>"
                                    SortExpression="CreditAdviceIndicator" />
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber" />
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status" />
                            </Columns>
                        </asp:GridView>
                        <br />
                        <asp:GridView ID="GridViewPackageLines2" runat="server" DataKeyNames="PackageLineId"
                            AllowPaging="true" AutoGenerateEditButton="False" AutoGenerateSelectButton="False"
                            PageSize="30">
                            <Columns>
                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product" />
                                <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode" />
                                <asp:BoundField DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>"
                                    SortExpression="DeliveryNoteQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="AcceptedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedQuantity %>"
                                    SortExpression="AcceptedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="RejectQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, RejectQuantity %>"
                                    SortExpression="RejectQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>"
                                    SortExpression="CreditAdviceIndicator" />
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber" />
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status" />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourcePackageLines2" runat="server" TypeName="Receiving"
                            SelectMethod="GetPackageLinesComplete" OnSelecting="ObjectDataSourcePackaging_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="receiptId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourcePackageLines" runat="server" TypeName="Receiving"
                            SelectMethod="GetPackageLines" OnSelecting="ObjectDataSourcePackaging_OnSelecting"
                            UpdateMethod="SetPackageLines" OnUpdating="ObjectDataSourcePackaging_OnUpdating"
                            OnUpdated="ObjectDataSourcePackaging_Updated">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="receiptId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId"
                                    Type="Int32" />
                                <asp:Parameter Name="OperatorId" Type="Int32" />
                                <asp:Parameter Name="AcceptedQuantity" Type="Decimal" />
                                <asp:Parameter Name="DeliveryNoteQuantity" Type="Decimal" />
                                <asp:Parameter Name="RejectQuantity" Type="Decimal" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultDelivery" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultActual" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultZero" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
