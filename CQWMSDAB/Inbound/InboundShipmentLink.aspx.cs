using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_InboundShipmentLink : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

                ButtonPrint.Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 294);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";
        try
        {
            GridViewInboundShipment_DataBind();
            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonDocumentSearch_Click

    #region GridViewInboundShipment_SelectedIndexChanged
    protected void GridViewInboundShipment_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInboundShipment_SelectedIndexChanged";
        try
        {
            GridViewLinked_DataBind();
            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewInboundShipment_SelectedIndexChanged

    #region ButtonUnlinkedSearch_Click
    protected void ButtonUnlinkedSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonUnlinkedSearch_Click";
        try
        {
            GridViewUnlinked_DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonUnlinkedSearch_Click

    #region GridViewLinked_SelectedIndexChanged
    protected void GridViewLinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewLinked_SelectedIndexChanged";
        try
        {
            InboundShipment ds = new InboundShipment();

            if (ds.Remove(Session["ConnectionStringName"].ToString(),
                            Convert.ToInt32(GridViewLinked.SelectedDataKey["InboundShipmentId"]),
                            Convert.ToInt32(GridViewLinked.SelectedDataKey["ReceiptId"])) == true)
            {
                GridViewUnlinked_DataBind();

                GridViewLinked_DataBind();
            }
            else
                Master.ErrorText = "Please try again";
            
            Master.MsgText = "Selected"; 
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewLinked_SelectedIndexChanged

    #region GridViewUnlinked_SelectedIndexChanged
    protected void GridViewUnlinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewUnlinked_SelectedIndexChanged";

        try
        {
            int inboundShipmentId = -1;
            InboundShipment inboundShipment = new InboundShipment();

            if (GridViewInboundShipment.Rows.Count == 0)
            {
                int warehouseId = int.Parse(Session["WarehouseId"].ToString());
                if (!inboundShipment.InsertInboundShipment(Session["ConnectionStringName"].ToString(), warehouseId, DateTime.Today, "Auto insert"))
                    Master.ErrorText = "There was an error!!!";

                //Get the newly create InboundShipmentId
                inboundShipmentId = inboundShipment.inboundShipmentId;
                GridViewInboundShipment_Get(inboundShipmentId);

                if (GridViewInboundShipment.Rows.Count > 0)
                {
                    GridViewInboundShipment.SelectedIndex = 0;

                    if (inboundShipment.Transfer(Session["ConnectionStringName"].ToString(), inboundShipmentId, int.Parse(GridViewUnlinked.SelectedDataKey.Value.ToString())))
                    {
                        GridViewUnlinked_DataBind();
                        GridViewLinked_DataBind();
                    }
                    else
                      Master.ErrorText = "Please try again";
                }
            }
            else
                if (GridViewInboundShipment.SelectedDataKey["InboundShipmentId"].ToString() != null)
                {
                    if (inboundShipment.Transfer(Session["ConnectionStringName"].ToString(), int.Parse(GridViewInboundShipment.SelectedDataKey["InboundShipmentId"].ToString()), int.Parse(GridViewUnlinked.SelectedDataKey.Value.ToString())))
                    {
                        GridViewUnlinked_DataBind();
                        GridViewLinked_DataBind();
                    }
                    else
                        Master.ErrorText = "Please try again";
                }

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewUnlinked_SelectedIndexChanged

    #region GridViewInboundShipment_PageIndexChanging
    protected void GridViewInboundShipment_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        theErrMethod = "GridViewInboundShipment_PageIndexChanging";
        
        try
        {
            InboundShipment ds = new InboundShipment();

            GridViewLinked.PageIndex = e.NewPageIndex;
            GridViewLinked_DataBind();

            Master.MsgText = "New Page"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewInboundShipment_PageIndexChanging

    #region GridViewUnlinked_DataBind
    protected void GridViewUnlinked_DataBind()
    {
        theErrMethod = "GridViewUnlinked_DataBind";

        InboundShipment inboundShipment = new InboundShipment();

        try
        {
            GridViewUnlinked.DataSource = inboundShipment.GetUnlinkedReceipts(Session["ConnectionStringName"].ToString(),
                                                                                int.Parse(Session["WarehouseId"].ToString()),
                                                                                int.Parse(Session["InboundDocumentTypeId"].ToString()),
                                                                                Session["OrderNumber"].ToString(),
                                                                                Session["ExternalCompanyCode"].ToString(),
                                                                                Session["ExternalCompany"].ToString(),
                                                                                Convert.ToDateTime(Session["FromDate"].ToString()),
                                                                                Convert.ToDateTime(Session["ToDate"].ToString()));
            GridViewUnlinked.DataBind();
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewUnlinked_DataBind

    #region GridViewLinked_DataBind
    protected void GridViewLinked_DataBind()
    {
        theErrMethod = "GridViewLinked_DataBind";

        InboundShipment inboundShipment = new InboundShipment();

        try
        {
            GridViewLinked.DataSource = inboundShipment.GetLinkedReceipts(Session["ConnectionStringName"].ToString(), Convert.ToInt32(GridViewInboundShipment.SelectedDataKey.Value));
            GridViewLinked.DataBind();
            Master.MsgText = "Linked"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewLinked_DataBind

    #region GridViewInboundShipment_DataBind
    protected void GridViewInboundShipment_DataBind()
    {
        theErrMethod = "GridViewInboundShipment_DataBind";

        InboundShipment ds = new InboundShipment();

        try
        {
            GridViewInboundShipment.DataSource = ds.SearchInboundShipmentCreate(Session["ConnectionStringName"].ToString(),
                                                                                int.Parse(Session["WarehouseId"].ToString()),
                                                                                int.Parse(Session["InboundDocumentTypeId"].ToString()),
                                                                                -1,
                                                                                Session["OrderNumber"].ToString(),
                                                                                Session["ExternalCompanyCode"].ToString(),
                                                                                Session["ExternalCompany"].ToString(),
                                                                                Convert.ToDateTime(Session["FromDate"].ToString()),
                                                                                Convert.ToDateTime(Session["ToDate"].ToString()));

            GridViewInboundShipment.DataBind();
            Master.MsgText = "New Data"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewInboundShipment_DataBind

    #region GridViewInboundShipment_Get
    protected void GridViewInboundShipment_Get(int inboundShipmentId)
    {
        theErrMethod = "GridViewInboundShipment_Get";
        try
        {
            InboundShipment ds = new InboundShipment();

            GridViewInboundShipment.DataSource = ds.GetInboundShipmentCreate(Session["ConnectionStringName"].ToString(), inboundShipmentId);
            GridViewInboundShipment.DataBind();
            Master.MsgText = "Fetched"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewInboundShipment_Get

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            Session["FromURL"] = "~/Inbound/InboundShipmentLink.aspx";

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
                Session["ReportName"] = "Receiving Check Sheet Batches";
            else
                Session["ReportName"] = "Receiving Check Sheet";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[6];

            // Create the ConnectionString report parameter
            string strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);

            // Create the InboundShipmentId report parameter
            string inboundShipmentId = GridViewInboundShipment.SelectedDataKey.Value.ToString();
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("InboundShipmentId", inboundShipmentId);

            // Create the ReceiptId report parameter
            string receiptId = "-1";
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ReceiptId", receiptId);

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
            Master.MsgText = "Printed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrint_Click

    #region ButtonConsolidate_Click
    protected void ButtonConsolidate_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonConsolidate_Click";
        try
        {
            // Create the InboundShipmentId report parameter
            int inboundShipmentId = int.Parse(GridViewInboundShipment.SelectedDataKey.Value.ToString());

            InboundShipment inboundShipment = new InboundShipment();

            if (inboundShipment.CreateConsolidatedLoad(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], inboundShipmentId))
            {
                Master.MsgText = ""; Master.ErrorText = "";

                Session["OrderNumber"] = inboundShipment.GetOrderNumnber(Session["ConnectionStringName"].ToString(), inboundShipmentId).ToString();

                Response.Redirect("~/Inbound/ReceivingDocumentSearch.aspx");
            }
            else
            {
                Master.MsgText = ""; Master.ErrorText = "Error";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonConsolidate_Click

    #region ButtonInsert_Click
    protected void ButtonInsert_Click(object sender, EventArgs e)
    { 
        theErrMethod = "ButtonInsert_Click";

        try
        {
            int inboundShipmentId = -1;
            InboundShipment inboundShipment = new InboundShipment();

            int warehouseId = int.Parse(Session["WarehouseId"].ToString());
            if (!inboundShipment.InsertInboundShipment(Session["ConnectionStringName"].ToString(), warehouseId, DateTime.Today, "Auto insert"))
                Master.ErrorText = "There was an error!!!";

            //Get the newly create InboundShipmentId
            inboundShipmentId = inboundShipment.inboundShipmentId;
            GridViewInboundShipment_Get(inboundShipmentId);

            if (GridViewInboundShipment.Rows.Count > 0)
            {
                GridViewInboundShipment.SelectedIndex = 0;
            }
            
            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonInsert_Click

    #region GridViewInboundShipment_Deleting
    protected void GridViewInboundShipment_Deleting(object sender, GridViewDeleteEventArgs e)
    {
        InboundShipment delete = new InboundShipment();
        int inboundShipmentId = (int)GridViewInboundShipment.DataKeys[e.RowIndex].Values["InboundShipmentId"];

        if(delete.DeleteInboundShipment(Session["ConnectionStringName"].ToString(), inboundShipmentId))
            GridViewInboundShipment_DataBind();
    }
    #endregion GridViewInboundShipment_Deleting

    #region GridViewUnlinked_PageIndexChanging
    protected void GridViewUnlinked_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        theErrMethod = "GridViewUnlinked_PageIndexChanging";
        
        GridViewUnlinked.PageIndex = e.NewPageIndex;
    }
    #endregion GridViewUnlinked_PageIndexChanging

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "InboundShipmentLink", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "InboundShipmentLink", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
  
}
