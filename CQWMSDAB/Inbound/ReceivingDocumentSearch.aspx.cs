using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;


public partial class Inbound_ReceivingDocumentSearch : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {

                PrintMutipleCheckSheets();

                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 176))
                    ConfirmButtonExtenderButtonReceived.ConfirmText = "Press OK to Confirm as Received";
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "Page_Load"

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (Session["FromURL"] != null)
            {
                Session["FromURL"] = null;
                if (Session["ReceiptId"] != null)
                {
                    int receiptId = (int)Session["ReceiptId"];

                    foreach (GridViewRow row in GridViewReceiptDocument.Rows)
                    {
                        theErrMethod = row.ToString();
                        if (GridViewReceiptDocument.DataKeys[row.RowIndex].Values["ReceiptId"].ToString() == receiptId.ToString())
                        {
                            GridViewReceiptDocument.SelectedIndex = row.RowIndex;
                            GridViewReceiptLine.DataBind();
                            Tabs.ActiveTab = Tabs.Tabs[(int)Session["ActiveTabIndex"]];
                            receiptId = -1;
                            break;
                        }
                    }

                    if (receiptId != -1)
                    {
                        GridViewReceiptDocument.SelectedIndex = -1;
                        GridViewJobs.SelectedIndex = -1;
                        Session["InboundShipmentId"] = null;
                        Session["ReceiptId"] = null;
                        Session["JobId"] = null;

                        GridViewReceiptLine.DataBind();
                        GridViewJobs.DataBind();
                        GridViewDetails.DataBind();
                    }
                }
            }
        }
        catch { }
    }
    #endregion Page_LoadComplete

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";
        try
        {

            GridViewReceiptDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDocumentSearch_Click"

    #region ObjectDataSourcePackaging_OnSelecting
    protected void ObjectDataSourcePackaging_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourcePackaging_OnSelecting";

        try
        {
            //   e.InputParameters["inboundShipmentId"] = (int)Session["InboundShipmentId"];
            e.InputParameters["receiptId"] = (int)Session["ReceiptId"];
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourcePackaging_OnSelecting"

    #region ObjectDataSourceReceiptLine_OnSelecting
    protected void ObjectDataSourceReceiptLine_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceReceiptLine_OnSelecting";

        try
        {

            e.InputParameters["inboundShipmentId"] = (int)Session["InboundShipmentId"];
            if (Session["ReceiptId"] != null)
            {
                e.InputParameters["receiptId"] = (int)Session["ReceiptId"];
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceReceiptLine_OnSelecting"

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";

        try
        {
            e.InputParameters["storageUnitId"] = int.Parse(GridViewReceiptLine.SelectedDataKey["StorageUnitId"].ToString());

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceBatch_OnSelecting"

    #region ObjectDataSourceReceiptLine_OnUpdating
    protected void ObjectDataSourceReceiptLine_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {

        theErrMethod = "ObjectDataSourceReceiptLine_OnUpdating";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];
            e.InputParameters["storageUnitBatchId"] = (int)Session["StorageUnitBatchId"];
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceReceiptLine_OnUpdating"

    #region ObjectDataSourcePackaging_OnUpdating
    protected void ObjectDataSourcePackaging_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {

        theErrMethod = "ObjectDataSourcePackaging_OnUpdating";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];


        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourcePackaging_OnUpdating"

    #region ObjectDataSourcePackaging_Updated
    protected void ObjectDataSourcePackaging_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsPackageLinesReceived_Databind();
    }
    #endregion ObjectDataSourcePackaging_Updated

    #region ObjectDataSourceReceiptLine_Updated
    protected void ObjectDataSourceReceiptLine_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        switch ((int)e.ReturnValue)
        {
            case -1:
                Master.MsgText = "";
                Master.ErrorText = "Over receipt not allowed.";
                break;
            case -2:
                Master.MsgText = "";
                Master.ErrorText = "Default batch not allowed.";
                break;
            case -3:
                Master.MsgText = "";
                Master.ErrorText = "You cannot change quantities after a redelivery has been requested.";
                break;
            default:
                Master.MsgText = "Update successful.";
                Master.ErrorText = "";
                break;
        }
    }
    #endregion ObjectDataSourceReceiptLine_Updated

    #region GridViewReceiptDocument_RowDataBound
    protected void GridViewReceiptDocument_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex != -1)
        {
            if (e.Row.Cells[09].Text == "On Hold")
            {
                e.Row.Cells[0].Enabled = false;
            }
        }
    }
    #endregion

    #region GridViewReceiptDocument_SelectedIndexChanged
    protected void GridViewReceiptDocument_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewReceiptDocument_SelectedIndexChanged";

        try
        {
            if (GridViewReceiptDocument.SelectedRow.Cells[1].Text != "&nbsp;")
            {
                Session["InboundShipmentId"] = int.Parse(GridViewReceiptDocument.SelectedRow.Cells[1].Text);
                ButtonDeliveryConformance.Enabled = true;
            }
            else
            {
                ButtonDeliveryConformance.Enabled = false;
                Session["InboundShipmentId"] = -1;
            }

            GridViewReceiptLine.EditIndex = -1;
            GridViewPackageLines.EditIndex = -1;

            Session["rowList"] = null;

            Session["ReceiptId"] = int.Parse(GridViewReceiptDocument.SelectedDataKey["ReceiptId"].ToString());
            Session["ParameterOrderNumber"] = GridViewReceiptDocument.SelectedDataKey["OrderNumber"].ToString();
            //Session["InboundShipmentId"] = int.Parse(GridViewReceiptDocument.Rows[GridViewReceiptDocument.SelectedIndex].Cells[0].ToString());

            this.GridViewReceiptLine.DataBind();

            GridViewPackageLines.AutoGenerateColumns = false;
            this.GridViewPackageLines.DataSourceID = "ObjectDataSourcePackageLines";
            GridViewPackageLines.DataBind();

            DetailsPackageLinesReceived_Databind();


            GridViewPackageLines2.AutoGenerateColumns = false;
            this.GridViewPackageLines2.DataSourceID = "ObjectDataSourcePackageLines2";
            GridViewPackageLines2.DataBind();


            DetailsLinesReceived_Databind();




            //this.rdDefaultActualQty.Enabled = true;
            //this.rdDefaultDeliveryNoteQty.Enabled = true; 

            //Receiving ds = new Receiving();
            //int inboundDocumentId = -1;

            //if (GridViewReceiptDocument.SelectedDataKey["InboundShipmentId"].ToString() != "")
            //    inboundDocumentId = int.Parse(GridViewReceiptDocument.SelectedDataKey["InboundShipmentId"].ToString());
            //inboundDocumentId = -1;
            //GridViewReceiptLine.DataSource = ds.GetReceiptLines(inboundDocumentId,
            //                                                    int.Parse(GridViewReceiptDocument.SelectedDataKey["ReceiptId"].ToString()));
            //GridViewReceiptLine.DataBind();

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewReceiptDocument_SelectedIndexChanged"

    #region ButtonPOD_Click
    protected void ButtonPOD_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPOD_Click";

        try
        {
            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

            Session["ReportName"] = "Goods Received Note";

            ReportParameter[] RptParameters = new ReportParameter[5];

            // Create the ConnectionString report parameter
            //string strReportConnectionString = Session["ReportConnectionString"].ToString();
            //RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

            // Create the InboundShipmentId report parameter
            string inboundShipmentId = Session["InboundShipmentId"].ToString();
            RptParameters[0] = new ReportParameter("InboundShipmentId", inboundShipmentId);

            // Create the ReceiptId report parameter
            string receiptId = Session["ReceiptId"].ToString();
            RptParameters[1] = new ReportParameter("ReceiptId", receiptId);

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "POD"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPOD_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
                Session["ReportName"] = "Receiving Check Sheet Batches";
            else
                Session["ReportName"] = "Receiving Check Sheet";

            ReportParameter[] RptParameters = new ReportParameter[7];

            // Create the ConnectionString report parameter
            String strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

            // Create the InboundShipmentId report parameter
            String inboundShipmentId = Session["InboundShipmentId"].ToString();
            RptParameters[1] = new ReportParameter("InboundShipmentId", inboundShipmentId);

            // Create the ReceiptId report parameter
            String receiptId = Session["ReceiptId"].ToString();
            RptParameters[2] = new ReportParameter("ReceiptId", receiptId);

            // Create the ReceiptLineId report parameter
            String receiptLineId = "-1";
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewReceiptLine.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                if (cb.Checked)
                {
                    receiptLineId = GridViewReceiptLine.DataKeys[row.RowIndex].Values["ReceiptLineId"].ToString();
                    break;
                }
            }

            RptParameters[3] = new ReportParameter("ReceiptLineId", receiptLineId);

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            //GetEditIndex();
            //ArrayList rowList = (ArrayList)Session["checkedList"];

            //if (rowList == null)
            //    Session.Remove("checkedList");
            //else
            //{
            //    int index = rowList.Count;

            //    if (index < 1)
            //    {
            //        Session.Remove("checkedList");
            //    }
            //    else
            //    {
            //        Receiving ds = new Receiving();

            //        while (index > 0)
            //        {
            //            if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], Convert.ToInt32(rowList[0])) == false)
            //                Response.Write("There was an error!!!");

            //            rowList.Remove(rowList[0]);

            //            index--;
            //        }

            //        Session.Remove("checkedList");

            //        this.GridViewReceiptLine.AutoGenerateColumns = false;
            //        this.GridViewReceiptLine.DataSourceID = "ObjectDataSourceReceiptLine";
            //        this.GridViewReceiptLine.DataBind();
            //    }
            //}

            Receiving ds = new Receiving();

            if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], -1) == false)
            {
                Master.MsgText = "Location - "; Master.ErrorText = "There was an error allocating the Stock.";
            }
            else
            {
                Master.MsgText = "Location allocated successfully"; Master.ErrorText = "";

                GridViewReceiptLine.DataBind();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonAutoLocations_Click"

    #region ButtonPalletise_Click
    protected void ButtonPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletise_Click";
        try
        {
            Master.ErrorText = "";

            //GetEditIndex();
            //ArrayList rowList = (ArrayList)Session["checkedList"];

            //if (rowList == null)
            //    Session.Remove("checkedList");
            //else
            //{
            //    int index = rowList.Count;

            //    if (index < 1)
            //    {
            //        Session.Remove("checkedList");
            //    }
            //    else
            //    {
            //        Receiving ds = new Receiving();

            //        while (index > 0)
            //        {
            //            if (ds.Palletise(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], Convert.ToInt32(rowList[0])) == false)
            //            {
            //                Master.ErrorText = "There was an error palletising the document";
            //                return;
            //            }

            //            rowList.Remove(rowList[0]);

            //            index--;
            //        }

            //        Session.Remove("checkedList");

            //        this.GridViewReceiptLine.AutoGenerateColumns = false;
            //        this.GridViewReceiptLine.DataSourceID = "ObjectDataSourceReceiptLine";
            //        this.GridViewReceiptLine.DataBind();
            //    }
            //}
            Receiving ds = new Receiving();

            if (ds.Palletise(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], -1) == false)
            {
                Master.ErrorText = "There was an error palletising the document";
                return;
            }

            this.GridViewReceiptLine.DataBind();
            this.GridViewJobs.DataBind();

            Master.MsgText = "Palletise";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPalletise_Click"

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            GetEditIndex();
            Response.Redirect("ManualPalletise.aspx");

        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonManualLocations_Click"

    #region GridViewReceiptLine_OnRowUpdating
    protected void GridViewReceiptLine_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        theErrMethod = "GridViewReceiptLine_OnRowUpdating";

        try
        {
            GridViewRow row = this.GridViewReceiptLine.Rows[e.RowIndex];

            if (row != null)
            {
                DropDownList drp = (DropDownList)row.FindControl("DropDownListBatch");
                Session["drp"] = drp.Text;
            }

            foreach (DictionaryEntry entry in e.NewValues)
            {
                e.NewValues[entry.Key] =
                    Server.HtmlEncode(entry.Value.ToString());
            }

            Master.MsgText = "Row Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "GridViewReceiptLine_OnRowUpdating"

    #region ButtonEdit_Click
    // Sets the GridView into edit mode for selected row	
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = "Edit Row"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonEdit_Click"

    #region ButtonPackageEdit_Click
    // Sets the GridView into edit mode for selected row	
    protected void ButtonPackageEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = "Edit Row"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonPackageEdit_Click"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewReceiptLine.Rows.Count)
            {
                GridViewRow checkedRow = GridViewReceiptLine.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(GridViewReceiptLine.DataKeys[index].Values["ReceiptLineId"]);
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;
            else
                Session["checkedList"] = null;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";

        try
        {
            if (Session["rowList"] == null)
            {
                int index = 0;
                CheckBox cb = new CheckBox();
                ArrayList rowList = new ArrayList();

                while (index < GridViewReceiptLine.Rows.Count)
                {
                    GridViewRow checkedRow = GridViewReceiptLine.Rows[index];
                    cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(index);

                    index++;
                }
                if (rowList.Count > 0)
                    Session["rowList"] = rowList;
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";

        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];
            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                if (GridViewReceiptLine.EditIndex != -1)
                {
                    GridViewReceiptLine.UpdateRow(GridViewReceiptLine.EditIndex, true);
                }

                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                    GridViewReceiptLine.EditIndex = -1;
                }
                else
                {
                    GridViewReceiptLine.SelectedIndex = (int)rowList[0];
                    GridViewReceiptLine.EditIndex = (int)rowList[0];
                    //this.GridViewReceiptLine.Rows[GridViewReceiptLine.EditIndex].Cells[2].Enabled = false;
                    GridViewRow row = GridViewReceiptLine.Rows[(int)rowList[0]];

                    Session["StorageUnitId"] = GridViewReceiptLine.DataKeys[(int)rowList[0]].Values["StorageUnitId"];
                    Session["BatchId"] = GridViewReceiptLine.DataKeys[(int)rowList[0]].Values["BatchId"];
                    Session["StorageUnitBatchId"] = -1;
                    Session["DefaultMode"] = "Insert";

                    rowList.Remove(rowList[0]);
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";
        try
        {
            if (GridViewReceiptLine.EditIndex != -1)
                GridViewReceiptLine.UpdateRow(GridViewReceiptLine.EditIndex, true);

            SetEditIndexRowNumber();

            DetailsLinesReceived_Databind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSave_Click"

    #region ButtonProductAdd_Click
    protected void ButtonProductAdd_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["PassStorageUnitBatchId"] != null)
            {
                Receiving r = new Receiving();

                r.InsertPackageLines(Session["ConnectionStringName"].ToString(),
                                    (int)Session["WarehouseId"],
                                    (int)Session["ReceiptId"],
                                    (int)Session["PassStorageUnitBatchId"],
                                    (int)Session["OperatorId"]);


                DetailsPackageLinesReceived_Databind();
                GridViewPackageLines.DataBind();
                GridViewPackageLines.DataBind();

            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }



    #endregion ButtonProductAdd_Click

    #region DetailsLinesReceived_Databind
    protected void DetailsLinesReceived_Databind()
    {
        DetailsLinesReceived.DataBind();

        if ((int)DetailsLinesReceived.DataKey["LinesReceived"] >= (int)DetailsLinesReceived.DataKey["TotalLines"])
        {
            ButtonRedelivery.Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 176);
            ButtonReceived.Enabled = true;
            ButtonPOD.Enabled = true;
            ButtonAutoLocations.Enabled = true;
            ButtonPalletise.Enabled = true;
            ButtonManualLocations.Enabled = true;
            PanelDefault.Enabled = false;
            ButtonDefaultReceived.Enabled = false;
            ButtonDefaultDelivery.Enabled = false;
            ButtonDefaultZero.Enabled = false;
            this.PanelLinesReceived.BackColor = System.Drawing.Color.Red;
        }
        else
        {
            ButtonRedelivery.Enabled = false;
            ButtonReceived.Enabled = false;
            ButtonPOD.Enabled = false;
            ButtonAutoLocations.Enabled = false;
            ButtonPalletise.Enabled = false;
            ButtonManualLocations.Enabled = false;
            PanelDefault.Enabled = true;
            ButtonDefaultReceived.Enabled = true;
            ButtonDefaultDelivery.Enabled = true;
            ButtonDefaultZero.Enabled = true;
            this.PanelLinesReceived.BackColor = System.Drawing.Color.Transparent;
        }

        // May override above settings depending on configuration value
        DisableButtons();
    }
    #endregion DetailsLinesReceived_Databind

    #region DetailsPackageLinesReceived_Databind
    protected void DetailsPackageLinesReceived_Databind()
    {
        DetailsPackageLinesReceived.DataBind();


        if ((int)DetailsPackageLinesReceived.DataKey["LinesReceived"] >= (int)DetailsPackageLinesReceived.DataKey["TotalLines"])
        {
            ButtonPackageComplete.Enabled = true;
        }
        else
        {
            ButtonPackageComplete.Enabled = false;
        }

        // May override above settings depending on configuration value

    }
    #endregion DetailsPackageLinesReceived_Databind

    #region DisableButtons
    protected void DisableButtons()
    {
        if (GridViewReceiptDocument.SelectedDataKey["AllowPalletise"].ToString() == "False")
        {
            ButtonPalletise.Enabled = false;
            ButtonReject.Enabled = false;
            ButtonSave.Enabled = false;
            ButtonEdit.Enabled = false;
            ButtonDeliveryConformance.Enabled = false;
        }
        else
        {
            ButtonPalletise.Enabled = true;
            ButtonReject.Enabled = true;
            ButtonSave.Enabled = true;
            ButtonEdit.Enabled = true;
            ButtonDeliveryConformance.Enabled = true;
        }
    }
    #endregion DisableButtons

    #region ButtonDefaultDelivery_Click
    protected void ButtonDefaultDelivery_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDefaultDelivery_Click";
        try
        {
            Receiving r = new Receiving();

            r.SetDefaultDeliveryNoteQty(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

            this.GridViewReceiptLine.DataBind();

            DetailsLinesReceived_Databind();

            Master.MsgText = "Delivery"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDefaultDelivery_Click"

    #region ButtonDefaultActual_Click
    // --------------------------------------------
    // METHOD : ButtonDefaultReceived_Click
    // --------------------------------------------
    /// <summary>
    /// When the user clicks to set default values.
    /// </summary>        
    protected void ButtonDefaultActual_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDefaultReceived_Click";
        try
        {
            Receiving r = new Receiving();

            r.SetDefaultActualQty(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

            this.GridViewReceiptLine.DataBind();

            DetailsLinesReceived_Databind();

            Master.MsgText = "Default"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDefaultActual_Click"

    #region ButtonDefaultZero_Click
    // --------------------------------------------
    // METHOD : ButtonDefaultZero_Click
    // --------------------------------------------
    /// <summary>
    /// When the user clicks to set default values.
    /// </summary>        
    protected void ButtonDefaultZero_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDefaultZero_Click";
        try
        {
            Receiving r = new Receiving();

            r.SetDefaultZero(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

            this.GridViewReceiptLine.DataBind();

            DetailsLinesReceived_Databind();

            Master.MsgText = "Default"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDefaultZero_Click"

    #region ButtonDeliveryConformance_Click
    // --------------------------------------------
    // METHOD : ButtonDefaultZero_Click
    // --------------------------------------------
    /// <summary>
    /// When the user clicks to set default values.
    /// </summary>        
    protected void ButtonDeliveryConformance_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeliveryConformance_Click";
        try
        {
            Session["Sequence"] = 0;
            Session["QuestionaireId"] = 5;
            Session["QuestionaireType"] = "Supplier Load Conformance";


            //            Session["ActiveViewIndex"] = MultiViewConfirm.ActiveViewIndex;
            Session["ReturnURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Response.Redirect("~/Common/Question.aspx");

        }
        catch { }
    }
    #endregion "ButtonDeliveryConformance_Click"

    #region ButtonReject_Click
    protected void ButtonReject_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReject_Click";

        try
        {
            GetEditIndex();
            if (Session["checkedList"] != null)
                Response.Redirect("CreditAdviceReject.aspx?ReasonCode=REJ");

            Master.MsgText = "Reject"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonReject_Click"

    #region ButtonLink_Click
    protected void ButtonLink_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLink_Click";

        try
        {
            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

            Response.Redirect("RegisterSerialNumber.aspx");

            Master.MsgText = "Link"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonLink_Click"

    #region ButtonCreditAdvice_Click
    protected void ButtonCreditAdvice_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCreditAdvice_Click";
        try
        {
            GetEditIndex();

            if (Session["checkedList"] != null)
                Response.Redirect("CreditAdviceUpdate.aspx?ReasonCode=CAV");

            Master.MsgText = "Advice"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonCreditAdvice_Click"

    #region ButtonRedelivery_Click
    protected void ButtonRedelivery_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRedelivery_Click";
        try
        {
            ConfirmButtonExtenderButtonReceived.ConfirmText = "Press OK to Confirm as Received";

            Receiving r = new Receiving();

            if (r.RedeliveryInsert(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]))
            {
                ButtonRedelivery.Enabled = false;

                Master.MsgText = "Redelivery"; Master.ErrorText = "";
            }
            else
            {
                result = SendErrorNow("Redelivery" + "_" + "Error p_Receiving_Redelivery_Insert");
                Master.ErrorText = result;
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("Redelivery" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonRedelivery_Click"

    #region ButtonReceived_Click
    protected void ButtonReceived_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReceived_Click";
        try
        {
            Status status = new Status();

            if (status.InboundStatusChange(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"], -1, "RC"))
            {
                ButtonReceived.Enabled = false;

                Session["InboundShipmentId"] = 0;
                Session["ReceiptId"] = 0;

                GridViewReceiptDocument.DataBind();
                GridViewReceiptLine.DataBind();

                Master.MsgText = "Received"; Master.ErrorText = "";
            }
            else
            {
                result = SendErrorNow("Received" + "_" + "Error p_Receipt_Update_Status_Received");
                Master.MsgText = "";
                Master.ErrorText = "Exception, please make sure there are no default batches and there is a delivery note number";
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonReceived_Click"

    #region ButtonPackageReceived_Click
    protected void ButtonPackageReceived_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPackageReceived_Click";
        try
        {
            Status status = new Status();
            Receiving r = new Receiving();

            if (r.CompletePackageLines(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["ReceiptId"]) == 1)
            {
                ButtonPackageComplete.Enabled = false;
                Master.MsgText = "Packaging Received";
                Master.ErrorText = "";
            }
            else
            {
                result = SendErrorNow("Received" + "_" + "Error p_Receiving_PackageLine_Complete");
                Master.MsgText = "";
                Master.ErrorText = "Exception, Error completing Packaging";
            }
            GridViewPackageLines.DataBind();
            GridViewPackageLines2.DataBind();
            DetailsLinesReceived_Databind();

        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPackageReceived_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewReceiptLine.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ButtonSelectJobs_Click
    protected void ButtonSelectJobs_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectJobs_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelectJobs_Click

    #region ButtonPrintCheckSheet_Click
    protected void ButtonPrintCheckSheet_Click(object sender, EventArgs e)
    {
        PrintMutipleCheckSheets();
    }
    #endregion ButtonPrintCheckSheet_Click

    #region PrintMutipleCheckSheets
    protected void PrintMutipleCheckSheets()
    {
        try
        {
            ReportParameter[] RptParameters = new ReportParameter[4];
            ArrayList checkedList;

            if (Session["checkedList"] != null)
            {
                checkedList = (ArrayList)Session["checkedList"];
            }
            else
            {
                if (GridViewJobs.Rows.Count > 0)
                    return;

                checkedList = new ArrayList();

                CheckBox cb = new CheckBox();

                foreach (GridViewRow row in GridViewJobs.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    if (cb.Checked)
                        checkedList.Add(GridViewJobs.DataKeys[row.RowIndex].Values["JobId"]);
                }
            }

            if (checkedList.Count > 0)
            {
                Session["checkedList"] = checkedList;

                Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
                Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
                    Session["ReportName"] = "Receiving Check Sheet Job Batches";
                else
                    Session["ReportName"] = "Receiving Check Sheet Job";

                // Create the JobId report parameter
                RptParameters[0] = new ReportParameter("JobId", checkedList[0].ToString());

                checkedList.RemoveAt(0);

                RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                Session["ReportParameters"] = RptParameters;

                Response.Redirect("~/Reports/Report.aspx");
            }
            else
            {
                Session["checkedList"] = null;
            }
        }
        catch { }
    }
    #endregion PrintMutipleCheckSheets

    #region ButtonSelectPallet_Click
    protected void ButtonSelectPallet_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectPallet_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelectPallet_Click

    #region ButtonPrintJob_Click
    protected void ButtonPrintJob_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintJob_Click";

        try
        {
            Session["LabelName"] = "Multi Purpose Label.lbl";
            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                if (cb.Checked)
                {
                    Session["Title"] = GridViewJobs.DataKeys[row.RowIndex].Values["JobId"].ToString();
                    Session["Barcode"] = GridViewJobs.DataKeys[row.RowIndex].Values["ReferenceNumber"].ToString();

                    var sessionPort = Session["Port"];
                    var sessionPrinter = Session["Printer"];
                    var sesionIndicatorList = Session["indicatorList"];
                    var sessionCheckedList = Session["checkedList"];
                    var sessionLocationList = Session["locationList"];
                    var sessionConnectionString = Session["ConnectionStringName"];
                    var sessionTitle = Session["Title"];
                    var sessionBarcode = Session["Barcode"];
                    var sessionWarehouseId = Session["WarehouseId"];
                    var sessionProductList = Session["ProductList"];
                    var sessionTakeOnLabel = Session["TakeOnLabel"];
                    var sessionLabelCopies = Session["LabelCopies"];

                    NLWAXPrint nl = new NLWAXPrint();
                    nl.onSessionUpdate += Nl_onSessionUpdate;
                    string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                              Session["LabelName"].ToString(),
                                                              sessionPrinter,
                                                              sessionPort,
                                                              sesionIndicatorList,
                                                              sessionCheckedList,
                                                              sessionLocationList,
                                                              sessionConnectionString,
                                                              sessionTitle,
                                                              sessionBarcode,
                                                              sessionWarehouseId,
                                                              sessionProductList,
                                                              sessionTakeOnLabel,
                                                              sessionLabelCopies);
                    if (printResponse == string.Empty)
                    {
                        Master.ErrorText = "An error has occurred! For more details please check automation manager.";
                    }
                    else
                    {
                        Master.MsgText = "Label has been printed successfuly.";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
    #endregion ButtonPrintJob_Click

    #region ButtonPrintPallet_Click
    protected void ButtonPrintPallet_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintPallet_Click";

        try
        {
            CheckBox cb = new CheckBox();
            Status status = new Status();
            ArrayList rowList = new ArrayList();

            Session["checkedList"] = null;

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    if (status.Print(Session["ConnectionStringName"].ToString(), int.Parse(GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"].ToString()), (int)Session["OperatorId"]))
                        rowList.Add(GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]);
                    else
                        Master.ErrorText = "Please try again";
                }
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 390))
                Session["LabelName"] = "Pallet Label Small.lbl";
            else
                Session["LabelName"] = "Pallet Label.lbl";

            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }
    #endregion ButtonPrintPallet_Click

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";

        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();
            GridViewDetails.DataBind();

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region ButtonPrintLabel_Click
    protected void ButtonPrintLabel_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();
            string nop;
            int numberToPrint;
            string rli;
            bool verifyLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 159);
            bool useReceiptLineId = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 178);
            bool customerLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 320);
            foreach (GridViewRow row in GridViewReceiptLine.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 369))
                        checkedList.Add((int)GridViewReceiptLine.DataKeys[row.RowIndex].Values["StorageUnitId"] * -1);
                    else
                        if (verifyLabel || useReceiptLineId)
                        checkedList.Add((int)GridViewReceiptLine.DataKeys[row.RowIndex].Values["ReceiptLineId"]);
                    else
                        checkedList.Add((int)GridViewReceiptLine.DataKeys[row.RowIndex].Values["BatchId"]);
                }
                Exceptions ex = new Exceptions();
                rli = ((HiddenField)row.FindControl("HiddenReceiptLineId")).Value;
                Session["rli"] = int.Parse(rli);
                int count = ex.CountRedPrint(Session["ConnectionStringName"].ToString(), (int)Session["rli"]);
                nop = ((HiddenField)row.FindControl("HiddenNumberOfPallets")).Value;
                numberToPrint = int.Parse(nop) - count;

                if (int.Parse(TextBoxQty.Text) > numberToPrint)
                {
                    Session["LabelCopies"] = numberToPrint;
                    Master.MsgText = "";
                    Master.ErrorText = "Some lines exceeded number of allowed labels - defaulted to number of pallets";
                    if (numberToPrint < 1)
                    {
                        Master.MsgText = "";
                        Master.ErrorText = "Maximum number of labels have already been printed";
                        return;
                    }
                    //break;
                }
                else
                {
                    Session["LabelCopies"] = int.Parse(TextBoxQty.Text);
                }

                if (ex.CreateException(Session["ConnectionStringName"].ToString(), (int)Session["rli"], (int)Session["LabelCopies"], (int)Session["OperatorId"]) == false)
                {
                    return;
                }

            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (customerLabel)
                Session["LabelName"] = "Customer Product Label Small.lbl";
            else
                if (verifyLabel)
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 369))
                    Session["LabelName"] = "Product Division Label Small.lbl";
                else
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 199))
                    Session["LabelName"] = "Receiving Batch Label Small.lbl";
                else
                    Session["LabelName"] = "Receiving Batch Label Large.lbl";
            else
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                Session["LabelName"] = "Product Batch Label Small.lbl";
            else
                Session["LabelName"] = "Product Batch Label Large.lbl";

            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("RecivingDocumentSearch " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }
    #endregion ButtonPrintLabel_Click

    #region ButtonPrintSample_Click
    protected void ButtonPrintSample_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();
            bool useReceiptLineId = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 178);

            foreach (GridViewRow row in GridViewReceiptLine.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    if (useReceiptLineId)
                        checkedList.Add((int)GridViewReceiptLine.DataKeys[row.RowIndex].Values["ReceiptLineId"]);
                    else
                        checkedList.Add((int)GridViewReceiptLine.DataKeys[row.RowIndex].Values["BatchId"]);
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                Session["LabelName"] = "Sample Label Small.lbl";
            else
                Session["LabelName"] = "Sample Label Large.lbl";

            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

            Session["LabelCopies"] = int.Parse(TextBoxSample.Text);

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }
    #endregion ButtonPrintSample_Click

    #region ButtonPalletWeight
    protected void ButtonPalletWeight_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletWeight";

        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    checkedList.Add((int)GridViewJobs.DataKeys[row.RowIndex].Values["JobId"]);
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            Session["FromURL"] = "~/Inbound/ReceivingDocumentSearch.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.ActiveTabIndex.ToString());

            Response.Redirect("~/Inbound/PalletWeight.aspx");

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPalletWeight"

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            Session["StorageUnitBatchId"] = int.Parse(ddl.SelectedValue);
        }
        catch { }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling


}
