<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Inbound_Default" Title="<%$ Resources:Default, DefaultInboundTitle %>" StylesheetTheme="Default"
    Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultInboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultInboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <table>
        <tr valign="top">
            <td>
                <telerik:RadButton ID="RadButtonUtilisation" runat="server" Text="Space Utilisation" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartUtilisation" runat="server" ChartTitle-TextBlock-Text="Space Utilisation" Skin="DeepBlue"
                    DataGroupColumn="Legend" DefaultType="StackedBar100" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceSpaceUtilisation" Width="500px">
                    <PlotArea>
                        <XAxis DataLabelsColumn="Area">
                        </XAxis>
                    </PlotArea>
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceSpaceUtilisation" runat="server" TypeName="Dashboard" SelectMethod="SpaceUtilisation">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <telerik:RadButton ID="RadButtonDockToStock" runat="server" Text="Dock to Stock" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridDockToStock" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePutaway" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceDockToStock">
                        <Columns>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="InboundDocumentType" HeaderText="<%$ Resources:Default, InboundDocumentType %>"
                                SortExpression="InboundDocumentType" UniqueName="InboundDocumentType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Lines" HeaderText="<%$ Resources:Default, Lines %>"
                                SortExpression="Lines" UniqueName="Lines">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Units" HeaderText="<%$ Resources:Default, Units %>"
                                SortExpression="Units" UniqueName="Units">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Minutes" HeaderText="<%$ Resources:Default, Minutes %>"
                                SortExpression="Minutes" UniqueName="Minutes">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="KPI" HeaderText="<%$ Resources:Default, KPI %>"
                                SortExpression="KPI" UniqueName="KPI">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceDockToStock" runat="server" TypeName="Dashboard" SelectMethod="DockToStock">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonPutaway" runat="server" Text="Putaway Priorities" Skin="Default" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridPutaway" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePutaway" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataKeyNames="Row" DataSourceID="ObjectDataSourcePutaway">
                        <Columns>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Row" HeaderText="<%$ Resources:Default, Row %>"
                                SortExpression="Row" UniqueName="Row">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                SortExpression="ProductCode" UniqueName="ProductCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                SortExpression="Product" UniqueName="Product">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                SortExpression="SKUCode" UniqueName="SKUCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>"
                                SortExpression="RequiredQuantity" UniqueName="RequiredQuantity">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ReceivingQuantity" HeaderText="<%$ Resources:Default, ReceivingQuantity %>"
                                SortExpression="ReceivingQuantity" UniqueName="ReceivingQuantity">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourcePutaway" runat="server" TypeName="Dashboard" SelectMethod="PutawayPriority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
