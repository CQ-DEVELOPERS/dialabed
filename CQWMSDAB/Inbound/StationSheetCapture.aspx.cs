﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Inbound_StationSheetCapture : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["FromURL"] != null)
                    Session["FromURL"] = null;

                //              if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 176))
                //                  ConfirmButtonExtenderButtonReceived.ConfirmText = "Press OK to Confirm as Received";
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }

    #endregion "Page_Load"

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";
        try
        {

            GridViewReceiptDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDocumentSearch_Click"

    #region ObjectDataSourcePackaging_OnSelecting
    protected void ObjectDataSourcePackaging_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourcePackaging_OnSelecting";

        try
        {
            //   e.InputParameters["inboundShipmentId"] = (int)Session["InboundShipmentId"];
            e.InputParameters["receiptId"] = (int)Session["ReceiptId"];
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourcePackaging_OnSelecting"

    #region ObjectDataSourceReceiptLine_OnSelecting
    protected void ObjectDataSourceReceiptLine_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceReceiptLine_OnSelecting";

        try
        {
            e.InputParameters["inboundShipmentId"] = (int)Session["InboundShipmentId"];
            e.InputParameters["receiptId"] = (int)Session["ReceiptId"];
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceReceiptLine_OnSelecting"

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";

        try
        {
            e.InputParameters["storageUnitId"] = int.Parse(GridViewReceiptLine.SelectedDataKey["StorageUnitId"].ToString());

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceBatch_OnSelecting"

    #region ObjectDataSourceException_OnUpdating
    protected void ObjectDataSourceException_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {

        theErrMethod = "ObjectDataSourceException_OnUpdating";

        try
        {
            //e.InputParameters["receiptLineId"] = (int)Session["ReceiptLineId"];
            //e.InputParameters["acceptedQuantity"] = (int)Session["AcceptedQuantity"];
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ObjectDataSourceException_OnUpdating " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceException_OnUpdating"

    #region ObjectDataSourceReceiptLine_OnUpdating
    protected void ObjectDataSourceReceiptLine_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {

        theErrMethod = "ObjectDataSourceReceiptLine_OnUpdating";

        try
        {
            //e.InputParameters["receiptLineId"] = (int)Session["ReceiptLineId"];
            //e.InputParameters["acceptedQuantity"] = (int)Session["AcceptedQuantity"];
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceReceiptLine_OnUpdating"

    #region ObjectDataSourcePackaging_OnUpdating
    protected void ObjectDataSourcePackaging_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {

        theErrMethod = "ObjectDataSourcePackaging_OnUpdating";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];


        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourcePackaging_OnUpdating"
    
    #region ObjectDataSourceReceiptLine_Updated
    protected void ObjectDataSourceReceiptLine_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        //switch ((int)e.ReturnValue)
        //{
        //    case -1:
        //        Master.MsgText = "";
        //        Master.ErrorText = "Over receipt not allowed.";
        //        break;
        //    case -2:
        //        Master.MsgText = "";
        //        Master.ErrorText = "Default batch not allowed.";
        //        break;
        //    case -3:
        //        Master.MsgText = "";
        //        Master.ErrorText = "You cannot change quantities after a redelivery has been requested.";
        //        break;
        //    default:
        //        Master.MsgText = "Update successful.";
        //        Master.ErrorText = "";
        //        break;
        //}
    }
    #endregion ObjectDataSourceReceiptLine_Updated

    #region GridViewReceiptDocument_RowDataBound
    protected void GridViewReceiptDocument_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex != -1)
        {
            if (e.Row.Cells[08].Text == "On Hold")
            {
                e.Row.Cells[0].Enabled = false;
            }
        }
    }
    #endregion

    #region GridViewReceiptDocument_SelectedIndexChanged
    protected void GridViewReceiptDocument_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewReceiptDocument_SelectedIndexChanged";

        try
        {
            Session["InboundShipmentId"] = -1;
            Session["ReceiptLineID"] = -1;

            GridViewReceiptLine.EditIndex = -1;

            Session["rowList"] = null;

            Session["ReceiptId"] = int.Parse(GridViewReceiptDocument.SelectedDataKey["ReceiptId"].ToString());
            Session["ParameterOrderNumber"] = GridViewReceiptDocument.SelectedDataKey["OrderNumber"].ToString();
            //Session["InboundShipmentId"] = int.Parse(GridViewReceiptDocument.Rows[GridViewReceiptDocument.SelectedIndex].Cells[0].ToString());

            this.GridViewReceiptLine.AutoGenerateColumns = false;
            this.GridViewReceiptLine.DataSourceID = "ObjectDataSourceReceiptLine";
            this.GridViewReceiptLine.DataBind();

            //           DetailsLinesReceived_Databind();

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewReceiptDocument_SelectedIndexChanged"

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            GetEditIndex();
            Response.Redirect("ManualPalletise.aspx");

        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonManualLocations_Click"

    #region GridViewReceiptLine_OnRowUpdating
    protected void GridViewReceiptLine_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        theErrMethod = "GridViewReceiptLine_OnRowUpdating";

        try
        {
            GridViewRow row = this.GridViewReceiptLine.Rows[e.RowIndex];

            if (row != null)
            {
                DropDownList drp = (DropDownList)row.FindControl("DropDownListBatch");
                Session["drp"] = drp.Text;
            }

            foreach (DictionaryEntry entry in e.NewValues)
            {
                e.NewValues[entry.Key] =
                    Server.HtmlEncode(entry.Value.ToString());
            }

            Master.MsgText = "Row Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewReceiptLine_OnRowUpdating"

    #region ButtonEdit_Click
    // Sets the GridView into edit mode for selected row	
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = "Edit Row"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonEdit_Click"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewReceiptLine.Rows.Count)
            {
                GridViewRow checkedRow = GridViewReceiptLine.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(GridViewReceiptLine.DataKeys[index].Values["ReceiptLineId"]);
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;
            else
                Session["checkedList"] = null;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";

        try
        {
            if (Session["rowList"] == null)
            {
                int index = 0;
                CheckBox cb = new CheckBox();
                ArrayList rowList = new ArrayList();

                while (index < GridViewReceiptLine.Rows.Count)
                {
                    GridViewRow checkedRow = GridViewReceiptLine.Rows[index];
                    cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(index);

                    index++;
                }
                if (rowList.Count > 0)
                    Session["rowList"] = rowList;
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";

        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];
            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                if (GridViewReceiptLine.EditIndex != -1)
                {
                    GridViewReceiptLine.UpdateRow(GridViewReceiptLine.EditIndex, true);
                }

                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                    GridViewReceiptLine.EditIndex = -1;
                }
                else
                {
                    GridViewReceiptLine.SelectedIndex = (int)rowList[0];
                    GridViewReceiptLine.EditIndex = (int)rowList[0];
                    //this.GridViewReceiptLine.Rows[GridViewReceiptLine.EditIndex].Cells[2].Enabled = false;
                    GridViewRow row = GridViewReceiptLine.Rows[(int)rowList[0]];

                    Session["StorageUnitId"] = GridViewReceiptLine.DataKeys[(int)rowList[0]].Values["StorageUnitId"];
                    Session["BatchId"] = GridViewReceiptLine.DataKeys[(int)rowList[0]].Values["BatchId"];
                    Session["BatchId"] = GridViewReceiptLine.DataKeys[(int)rowList[0]].Values["BatchId"];
                    Session["StorageUnitBatchId"] = -1;
                    Session["DefaultMode"] = "Insert";

                    rowList.Remove(rowList[0]);
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonUpload_Click";
        try
        {
            StationSheetReceiving r = new StationSheetReceiving();

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 377))
                r.UploadToHostAdjustment(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"]);
            else
                r.UploadToHost(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"]);

            GridViewReceiptDocument.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonUpload_Click"

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";
        try
        {
            if (GridViewReceiptLine.EditIndex != -1)
                GridViewReceiptLine.UpdateRow(GridViewReceiptLine.EditIndex, true);

            SetEditIndexRowNumber();

            //            DetailsLinesReceived_Databind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSave_Click"

    #region ButtonInsert_Click
    protected void ButtonInsert_Click(object sender, EventArgs e)
    {
        try
        {
            if (GridViewReceiptLine.SelectedIndex == -1)
            {
                //prompt error
                Session["ReceiptLineID"] = -1; 
            }
            else
            {
                ObjectDataSourceException.Insert();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ButtonInsert_Click" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonInsert_Click"

    #region DisableButtons
    protected void DisableButtons()
    {
        if (GridViewReceiptDocument.SelectedDataKey["Interfaced"].ToString() == "1")
        {

           ButtonUpload.Enabled = false;

        }
        else
        {

            ButtonUpload.Enabled = true;

        }
    }
    #endregion DisableButtons

    #region ButtonReceived_Click
    protected void ButtonReceived_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReceived_Click";
        try
        {
            Status status = new Status();

            if (status.InboundStatusChange(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"], -1, "RC"))
            {

                Session["InboundShipmentId"] = 0;
                Session["ReceiptId"] = 0;

                GridViewReceiptDocument.DataBind();
                GridViewReceiptLine.DataBind();

                Master.MsgText = "Received"; Master.ErrorText = "";
            }
            else
            {
                result = SendErrorNow("Received" + "_" + "Error p_Receipt_Update_Status_Received");
                Master.MsgText = "";
                Master.ErrorText = "Exception, please make sure there are no default batches";
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonReceived_Click"
      
    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            Session["StorageUnitBatchId"] = int.Parse(ddl.SelectedValue);
        }
        catch { }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
    
    protected void GridViewReceiptLine_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewReceiptDocument_SelectedIndexChanged(sender, e);
        Session["ReceiptLineID"] = GridViewReceiptLine.SelectedDataKey.Value;
        //GridViewExceptions.DataSource = ObjectDataSourceException;
        GridViewExceptions.DataBind();
    }
    protected void GridViewException_SelectedIndexChanged(object sender, EventArgs e)
    {
        //GridViewExceptions.EditIndex = -1;
        //GridViewExceptions.DataSource = ObjectDataSourceException;
        //GridViewExceptions.DataBind();
    }

    protected void GridViewExceptions_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //ObjectDataSourceReceiptException.Update();
        //GridViewExceptions.DataBind();
        try
        {
            Console.Write("Test");
        }
        catch { };
    }
    protected void GridViewExceptions_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ReceiptException delete = new ReceiptException();
        int exceptionId = (int)GridViewExceptions.DataKeys[e.RowIndex].Values["ExceptionId"];

        if (delete.DeleteReceiptException(Session["ConnectionStringName"].ToString(), exceptionId))
            GridViewExceptions.DataBind();
    }
    protected void UpdatePanelGridViewReceiptLine_Load(object sender, EventArgs e)
    {
        //GridViewReceiptDocument_SelectedIndexChanged(sender, e);
    }
}
