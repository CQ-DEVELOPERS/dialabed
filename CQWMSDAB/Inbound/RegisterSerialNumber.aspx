<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/Empty.master"
    AutoEventWireup="true"
    CodeFile="RegisterSerialNumber.aspx.cs"
    Inherits="Inbound_RegisterSerialNumber"
    Title="<%$ Resources:Default, RegisterSerialNumberTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    
<%@ MasterType 
    VirtualPath="~/MasterPages/Empty.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:FormView ID="FormViewSerialNumber" runat="server" DataKeyNames="ReceiptId,ReceiptLineId,StorageUnitId,BatchId" DataSourceID="ObjectDataSourceDetails" AllowPaging="true">
        <ItemTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="LabelSupplierCode" runat="server" Text="<%$ Resources:Default, Supplier %>"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label1SupplierBind" runat="server" Text='<%# Bind("Supplier") %>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelSupplierCodeBind" runat="server" Text='<%# Bind("SupplierCode") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelOrderNumberBind" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode %>"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelProductCodeBind" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelProduct" runat="server" Text='<%# Bind("Product") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelAcceptedQuantity" runat="server" Text="<%$ Resources:Default, AcceptedQuantity %>"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelAcceptedQuantityBind" runat="server" Text='<%# Bind("AcceptedQuantity") %>'></asp:Label>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="SerialNumber" SelectMethod="GetSerialDetailsByReceipt">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <contenttemplate>
            <asp:CheckBox runat="server" ID="CheckBoxRefernceNumber" Checked="false" AutoPostBack="true" />
            <asp:Label ID="LabelReferenceNumber" runat="server" Text="Box:"></asp:Label>
            <asp:TextBox ID="TextBoxReferenceNumber" runat="server"></asp:TextBox>
        </contenttemplate>
    </asp:UpdatePanel>
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelSerialNumber" runat="server" Text="Serial Number :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLineNumbers" runat="server" Text="1 2 3 4 5 6 7 8 9 1011121314151617181920" TextMode="MultiLine" Columns="3" Rows="20" ReadOnly="true" Enabled="false" BorderStyle="None"></asp:TextBox>
            </td>
            <td>
                <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                    <contenttemplate>
                            <asp:TextBox ID="TextBoxSerialNumber" runat="server" TextMode="MultiLine" Rows="20"></asp:TextBox>
                    </contenttemplate>
                    <triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="Click"></asp:AsyncPostBackTrigger>
                    </triggers>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:Button ID="ButtonAdd" runat="Server" Text="Add" OnClick="ButtonAdd_Click" />
            </td>
            <td>
                <asp:Button ID="ButtonBack" runat="Server" Text="Back" OnClick="ButtonBack_Click" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="UpdatePanelRepeaterSerialNumber">
        <contenttemplate>
            <asp:Repeater ID="RepeaterSerialNumber" DataSourceID="ObjectDataSourceRepeater" runat="server">
                <ItemTemplate>
                    <asp:TextBox ID="TextBoxRepeat" runat="server" BorderWidth="1" BorderStyle="Dashed" BorderColor="ActiveBorder" Enabled="false" ForeColor="DarkGray" Text='<%# Eval("SerialNumber") %>'></asp:TextBox>
                </ItemTemplate>
            </asp:Repeater>
        </contenttemplate>
        <triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="Click"></asp:AsyncPostBackTrigger>
        </triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSourceRepeater" runat="server" TypeName="SerialNumber" SelectMethod="GetSerialNumbersByReceipt">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="int32" />
            <asp:SessionParameter Name="receiptLineId" SessionField="ReceiptLineId" Type="int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

