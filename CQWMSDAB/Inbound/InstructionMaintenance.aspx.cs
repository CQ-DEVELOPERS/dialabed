using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;


public partial class Inbound_InstructionMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables
       
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            DropDownListStatus.Focus();
            
            if (!Page.IsPostBack)
            {
                if (Session["IMStatusId"] != null)
                    DropDownListStatus.SelectedValue = Session["IMStatusId"].ToString();
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewInstruction.DataBind();
            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSearch_Click"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(GridViewInstruction.DataKeys[checkedRow.RowIndex].Values["InstructionId"]);
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;
          
            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";
        try
        {
            GetEditIndex();

            Master.MsgText = ""; Master.ErrorText = "";

            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList == null)
                Session.Remove("checkedList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("checkedList");
                }
                else
                {
                    InstructionMaintenance ds = new InstructionMaintenance();

                    while (index > 0)
                    {
                        if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), Convert.ToInt32(rowList[0])) == false)
                            Master.ErrorText = "There was an error!!!";
                        else
                            Master.MsgText = "Allocated successful";

                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    Session.Remove("checkedList");
                    
                    GridViewInstruction.DataBind();
                }
            }
            
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonAutoLocations_Click"

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";
        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Inbound/InstructionMaintenance.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");
            Master.MsgText = "Manual Location"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonManualLocations_Click"

    #region ButtonMix_Click
    protected void ButtonMix_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonMix_Click";
        try
        {
            Response.Redirect("~/Common/MixedJobCreate.aspx");
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonMix_Click"

    #region ButtonLink_Click
    protected void ButtonLink_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLink_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Inbound/InstructionMaintenance.aspx";
            Response.Redirect("~/Common/RegisterSerialNumberPallet.aspx");
            Master.MsgText = "Linked"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonLink_Click"

    #region ButtonPrintLabel_Click
    protected void ButtonPrintLabel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            CheckBox cb = new CheckBox();
            Status status = new Status();
            Exceptions ex = new Exceptions();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                Master.MsgText = "Print";

                if (cb.Checked == true)
                    if (!status.Print(Session["ConnectionStringName"].ToString(), int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["InstructionId"].ToString()), (int)Session["OperatorId"]))
                        Master.ErrorText = "Please try again";
                {
                    Session["LabelCopies"] = int.Parse(TextBoxQty.Text);
                }

            }

            GetEditIndex();

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 61))
                Session["LabelName"] = "Receiving & Staging Label.lbl";
            else
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 390))
                Session["LabelName"] = "Pallet Label Small.lbl";
            else
                Session["LabelName"] = "Pallet Label.lbl";

            Session["FromURL"] = "~/Inbound/InstructionMaintenance.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
    #endregion ButtonPrintLabel_Click

    #region ButtonPrintProductLabel_Click
    protected void ButtonPrintProductLabel_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();
            Exceptions ex = new Exceptions();

            string nop;
            int numberToPrint;
            string rli;
            bool verifyLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 159);
            bool useReceiptLineId = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 178);
            bool customerLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 320);
            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    if (verifyLabel || useReceiptLineId)
                        checkedList.Add((int)GridViewInstruction.DataKeys[row.RowIndex].Values["ReceiptLineId"]);
                    else
                        checkedList.Add((int)GridViewInstruction.DataKeys[row.RowIndex].Values["BatchId"]);

                    rli = ((HiddenField)row.FindControl("HiddenReceiptLineId")).Value;
                    Session["rli"] = int.Parse(rli);
                    int count = ex.CountRedPrint(Session["ConnectionStringName"].ToString(), (int)Session["rli"]);
                    nop = ((HiddenField)row.FindControl("HiddenNumberOfPallets")).Value;
                    numberToPrint = int.Parse(nop) - count;

                    if (int.Parse(TextBoxQty.Text) > numberToPrint)
                    {
                        Session["LabelCopies"] = numberToPrint;
                        Master.MsgText = "";
                        Master.ErrorText = "Some lines exceeded number of allowed labels - defaulted to number of pallets";
                        if (numberToPrint < 1)
                        {
                            Master.MsgText = "";
                            Master.ErrorText = "Maximum number of labels have already been printed";
                            return;
                        }
                    }
                    else
                    {
                        Session["LabelCopies"] = int.Parse(TextBoxQty.Text);
                    }

                    if (ex.CreateException(Session["ConnectionStringName"].ToString(), (int)Session["rli"], (int)Session["LabelCopies"], (int)Session["OperatorId"]) == false)
                    {
                        return;
                    }
                }

            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (customerLabel)
                Session["LabelName"] = "Customer Product Label Small.lbl";
            else
                if (verifyLabel)
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 199))
                    Session["LabelName"] = "Receiving Batch Label Small.lbl";
                else
                    Session["LabelName"] = "Receiving Batch Label Large.lbl";
            else
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                Session["LabelName"] = "Product Batch Label Small.lbl";
            else
                Session["LabelName"] = "Product Batch Label Large.lbl";

            Session["FromURL"] = "~/Inbound/InstructionMaintenance.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      (int)Session["LabelCopies"]);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
        }
    }
    #endregion ButtonPrintProductLabel_Click

    #region GetJobs
    protected void GetJobs()
    {
        theErrMethod = "GetJobs";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            Session["ReportJobId"] = ",";

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    Session["ReportJobId"] = Session["ReportJobId"].ToString() + GridViewInstruction.DataKeys[index].Values["JobId"].ToString() + ",";

                index++;
            }

            Master.MsgText = "Fetched"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetJobs"

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            GetJobs();
            
            Session["FromURL"] = "~/Inbound/InstructionMaintenance.aspx";

            Session["ReportName"] = "Putaway Sheet";

            ReportParameter[] RptParameters = new ReportParameter[4];

            // Create the JobId report parameter
            string jobId = Session["ReportJobId"].ToString();
            RptParameters[0] = new ReportParameter("JobId", jobId);

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportJobId"] = null;

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonPrint_Click"

    #region GridViewInstruction_PageIndexChanging
    protected void GridViewInstruction_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        theErrMethod = "GridViewInstruction_PageIndexChanging";
        try
        {

            GridViewInstruction.PageIndex = e.NewPageIndex;

            GridViewInstruction.DataBind();


            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewInstruction_PageIndexChanging"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "InstructionMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "InstructionMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
