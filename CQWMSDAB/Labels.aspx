<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Labels.aspx.cs" Inherits="_Labels" ValidateRequest="false" %>

<%@ Register Src="Common\NLWAX.ascx" TagName="NLWAX" TagPrefix="nl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Label Preview & Print</title>
    <link rel="SHORTCUT ICON" href="images/label.ico" />
    <script type="text/javascript" language="javascript" src="javascript/global.js"></script>
    <script type="text/javascript" language="javascript" src="javascript/port.js"></script>
    <script type="text/javascript" language="javascript" src="javascript/printer.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:scriptmanager ID="Scriptmanager1" runat="server"></asp:scriptmanager>
        <div>
            <!-- Add the NLWAX1 User Control to form -->
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <nl:NLWAX ID="NLWAX1" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="trvLabels" EventName="SelectedNodeChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <table>
                <tr>
                    <td>
                        Printer:
                    </td>
                    <td>
                        <asp:ListBox ID="lstPrinters" runat="server" />
                        <span id="spPrinter" style="color: Red;">No Printer Selected</span>
                    </td>
                </tr>
                <tr>
                    <td id="spPort">
                        Port
                    </td>
                    <td>
                        <div id="divLabel" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Label:
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:TreeView ID="trvLabels" runat="server" CollapseImageUrl="~/images/OPENFOLD.BMP"
                                    ExpandImageUrl="~/images/CLSDFOLD.BMP" NoExpandImageUrl="~/images/Label.bmp" OnSelectedNodeChanged="trvLabels_SelectedNodeChanged">
                                </asp:TreeView>
                                <asp:Label ID="lblLabel" ForeColor="red" runat="server" Text="No Label Selected" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        Print Quantity
                    </td>
                    <td>
                        <asp:TextBox Width="40" ID="txtQuant" Font-Size="10pt" runat="server" Text="1" />
                    </td>
                    <td>
                                <asp:Button ID="btnReprint" runat="server" Text="Reprint" Visible="false" OnClick="btnReprint_Click" />
                                <asp:Button ID="btnPrint" runat="server" Text="Print Label" Enabled="false" OnClick="btnPrint_Click" />
                                <asp:Button ID="btnDirectPrint" runat="server" Text="Print to Server printer" Enabled="false" OnClick="btnDirectPrint_Click" />
<%--                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="trvLabels" EventName="SelectedNodeChanged" />
                            </Triggers>
                        </asp:UpdatePanel>--%>
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblError" ForeColor="Red" runat="server" Text="No error" Visible="False" />
                        
            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <asp:Image ID="imgPreview" AlternateText="No image loaded" runat="server" Height="314px" Width="382px" EnableViewState="true" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="trvLabels" EventName="SelectedNodeChanged" />
                </Triggers>
            </asp:UpdatePanel>
            
            <asp:Button ID="btnExpand" runat="server" Text="-" ToolTip="Hide Extended Properties" OnClick="btnExpand_Click" />
            <asp:Button ID="btnSetVals" runat="server" Text="Set Values" ToolTip="Set the Values" OnClick="btnSetVals_Click" />
            
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblVariables" Text="Label Variables - No Variables Present" runat="server" />
                    <asp:GridView ID="grvVars" runat="server" AutoGenerateColumns="False" EnableViewState="true"
                        OnRowEditing="grvVars_RowEditing" OnRowCancelingEdit="grvVars_RowCancelingEdit" OnRowUpdating="grvVars_RowUpdating">
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" />
                            <asp:BoundField DataField="Prompt" HeaderText="Prompt" ReadOnly="True" />
                            <asp:TemplateField HeaderText="Format" Visible="true" ItemStyle-HorizontalAlign="center">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# GetFormatName(DataBinder.Eval(Container.DataItem, "FormatID").ToString()) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Length" HeaderText="Length" ReadOnly="True" Visible="true"
                                ItemStyle-HorizontalAlign="center" />
                            <asp:CheckBoxField DataField="FixedLength" HeaderText="Fixed Length" ReadOnly="true"
                                Visible="true" ItemStyle-HorizontalAlign="center" />
                            <asp:CheckBoxField DataField="ValueRequired" HeaderText="Required" ReadOnly="true"
                                Visible="true" ItemStyle-HorizontalAlign="center" />
                            <asp:TemplateField HeaderText="Value" ItemStyle-Wrap="true">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Value") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEditVal" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Value") %>'
                                        Width="98%" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField EditText="Edit" CancelText="Cancel" UpdateText="Update" HeaderText=""
                                ShowCancelButton="true" ShowEditButton="true" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="trvLabels" EventName="SelectedNodeChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnExpand" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSetVals" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
