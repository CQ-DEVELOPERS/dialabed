﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Billing_RateMaintenance : System.Web.UI.Page
{

  /// <summary>
  /// Handles the SelectedIndexChanged event of the gridRate control.
  /// </summary>
  /// <param name="sender">The source of the event.</param>
  /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  protected void gridRate_SelectedIndexChanged( object sender, EventArgs e )
  {
    Session.Remove( "RateId" );
    Session.Add( "RateId", gridRate.SelectedValue );

    tabsRateDetail.Tabs[0].Selected = true;
    tabsRateDetail.Visible = true;
    Tabs.Visible = true;
    
  }

  protected void DetailsViewInbound_DataBound( object sender, EventArgs e )
  {
    var value = ((DetailsView)sender).DataKey.Value;

    if ( value != null )
    {
      Session.Remove( "RATEDETAIL_ID_INBOUND" );
      Session.Add( "RATEDETAIL_ID_INBOUND", value );
      rateDetailLineInboundDataSource.DataBind();
      gridDetailLineInbound.DataBind();
    }
  }

  protected void DetailsViewOutbound_DataBound( object sender, EventArgs e )
  {
    var value = ((DetailsView)sender).DataKey.Value;

    if ( value != null )
    {
      Session.Remove( "RATEDETAIL_ID_OUTBOUND" );
      Session.Add( "RATEDETAIL_ID_OUTBOUND", value );
      rateDetailLineOutboundDataSource.DataBind();
    }
  }

  protected void DetailsViewStorage_DataBound( object sender, EventArgs e )
  {
    var value = ((DetailsView)sender).DataKey.Value;

    if ( value != null )
    {
      Session.Remove( "RATEDETAIL_ID_STORAGE" );
      Session.Add( "RATEDETAIL_ID_STORAGE", value );
      rateDetailLineStorageDataSource.DataBind();
    }
  }

  protected void tabsRateDetail_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
  {
      switch (e.Tab.Index)
      {
          case 0:
              DetailsViewInbound.DataBind();
              break;
          case 1:
              DetailsViewOutbound.DataBind();
              break;
          case 2:
              DetailsViewStorage.DataBind();
              break;
      }
  }
}