﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
  CodeFile="BillMaintenance.aspx.cs" Inherits="Billing_Billing"
  Title="<%$ Resources:Default, BillingTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
  <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, BillingTitle %>"></asp:Label>
  <br />
  <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
  <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

  <script type="text/javascript" language="JavaScript">

    function EnableBillingCategoryDropDown(cntl) {
      if (cntl.selectedIndex > 0) {
        document.getElementById("<%=cboBillingCategory.ClientID%>").Enabled = true;
      }
      else {
        document.getElementById("<%=cboBillingCategory.ClientID%>").Enabled = true;
      }
    }


  </script>


  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="RadGridBills">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridBill" />
          <telerik:AjaxUpdatedControl ControlID="RadGridBillDetails" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>

  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
  </telerik:RadAjaxLoadingPanel>

  <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
      function onSelectedIndexChanged(sender, eventArgs) {
        var selectedItem = eventArgs.get_item();
        var selectedItemText = selectedItem != null ? selectedItem.get_text() : sender.get_text();
        __doPostBack('', '');
      }
    </script>

  </telerik:RadScriptBlock>

  <div style="float: left;">
    <asp:Panel ID="PanelSearch" runat="server" Width="100%" BackColor="#EFEFEC">
      <table>
        <tr>
          <td style="height: 54px">
            <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
          </td>
          <td style="height: 54px">
            <telerik:RadComboBox ID="cboPrincipal" Width="80%" runat="server"
              DataSourceID="ObjectDataSourcePrincipal" DataTextField="Principal" DataValueField="PrincipalId"
              OnSelectedIndexChanged="cboPrincipal_SelectedIndexChanged" AutoPostBack="False"
              OnClientSelectedIndexChanged="onSelectedIndexChanged">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal" SelectMethod="GetAllPrincipals" OldValuesParameterFormatString="original_{0}">
            </asp:ObjectDataSource>
          </td>
          <td style="height: 54px">
            <asp:Label ID="lblBillingCategory" runat="server" Text="<%$ Resources:Default, BillingCategoryId%>" Width="100px"></asp:Label>
          </td>
          <td style="height: 54px">
            <telerik:RadComboBox ID="cboBillingCategory" runat="server" DataSourceID="BillingCategoryDataSource"
              DataTextField="Name" DataValueField="Id" Width="80%">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="BillingCategoryDataSource" runat="server" TypeName="BillingCategory" SelectMethod="GetBillingCategoryByPrincipal" OldValuesParameterFormatString="original_{0}">
              <SelectParameters>
                <asp:ControlParameter ControlID="cboPrincipal" Name="principalId" PropertyName="SelectedValue" Type="Int32" />
              </SelectParameters>
            </asp:ObjectDataSource>
          </td>
        </tr>
      </table>
    </asp:Panel>

    <telerik:RadButton ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search %>" OnClick="ButtonDocumentSearch_Click" />
    <%--<telerik:RadButton ID="ButtonDeliveryConformance" runat="server" Text="<%$ Resources:Default, DeliveryConformance %>"
                OnClick="ButtonDeliveryConformance_Click" Style="width: auto;" />--%>
    <telerik:RadGrid ID="RadGridBills" runat="server" Skin="Metro" AllowAutomaticUpdates="True" AllowSorting="True" AllowPaging="True" PageSize="30"
      DataSourceID="BillingDatasource" OnSelectedIndexChanged="RadGridBills_SelectedIndexChanged"
      OnItemCommand="RadGridBills_ItemCommand" AutoGenerateColumns="False" CellSpacing="0" GridLines="None">
      <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
      <MasterTableView EnableHeaderContextMenu="true" DataSourceID="BillingDatasource" DataKeyNames="Id">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>

        <Columns>
          <telerik:GridBoundColumn DataField="BillId" FilterControlAltText="Filter BillId column" HeaderText="Bill Id" SortExpression="BillId" UniqueName="BillId">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="Type" FilterControlAltText="Filter Type column" HeaderText="Type" ReadOnly="True" SortExpression="Type" UniqueName="Type">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="DocumentType" FilterControlAltText="Filter DocumentType column" HeaderText="DocumentType" SortExpression="DocumentType" UniqueName="DocumentType">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="Shipment" FilterControlAltText="Filter Shipment column" HeaderText="Shipment" SortExpression="Shipment" UniqueName="Shipment">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="ReferenceNumber" FilterControlAltText="Filter ReferenceNumber column" HeaderText="Reference Number" SortExpression="ReferenceNumber" UniqueName="ReferenceNumber">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="BillingCategory" FilterControlAltText="Filter BillingCategory column" HeaderText="Billing Category" SortExpression="BillingCategory" UniqueName="BillingCategory">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="VechicalType" FilterControlAltText="Filter VechicalType column" HeaderText="Vechical Type" SortExpression="VechicalType" UniqueName="VechicalType">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="PrincipalName" FilterControlAltText="Filter PrincipalName column" HeaderText="Principal" SortExpression="PrincipalName" UniqueName="PrincipalName">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="TransactedOn" DataType="System.DateTime" FilterControlAltText="Filter TransactedOn column" HeaderText="Created On" SortExpression="TransactedOn" UniqueName="TransactedOn">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="PrincipalId" DataType="System.Int32" FilterControlAltText="Filter PrincipalId column" HeaderText="PrincipalId" SortExpression="PrincipalId" UniqueName="PrincipalId" Visible="False">
          </telerik:GridBoundColumn>
        </Columns>

        <EditFormSettings>
          <EditColumn UniqueName="EditCommandColumn1" FilterControlAltText="Filter EditCommandColumn1 column"></EditColumn>
        </EditFormSettings>

        <BatchEditingSettings EditType="Cell"></BatchEditingSettings>

        <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
      </MasterTableView>

      <ExportSettings>
        <Pdf>
          <PageHeader>
            <LeftCell Text=""></LeftCell>

            <MiddleCell Text=""></MiddleCell>

            <RightCell Text=""></RightCell>
          </PageHeader>

          <PageFooter>
            <LeftCell Text=""></LeftCell>

            <MiddleCell Text=""></MiddleCell>

            <RightCell Text=""></RightCell>
          </PageFooter>
        </Pdf>
      </ExportSettings>

      <ClientSettings AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
      </ClientSettings>
      <GroupingSettings ShowUnGroupButton="True" />

      <FilterMenu EnableImageSprites="False"></FilterMenu>
    </telerik:RadGrid>
    <asp:ObjectDataSource ID="BillingDatasource" runat="server" OnSelecting="BillingDatasource_Selecting" OldValuesParameterFormatString="original_{0}" SelectMethod="GetBills" TypeName="Billing" >
      <SelectParameters>
        <asp:ControlParameter ControlID="cboPrincipal" Name="PrincipalId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="cboBillingCategory" Name="BillingCategoryId" PropertyName="SelectedValue" Type="Int32" />
      </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <telerik:RadGrid ID="RadGridBills0" runat="server" Skin="Metro" AllowAutomaticUpdates="True" AllowSorting="True" AllowPaging="True" PageSize="30"
      DataSourceID="BillDetailDatasource" OnSelectedIndexChanged="RadGridBills_SelectedIndexChanged"
      OnItemCommand="RadGridBills_ItemCommand" AutoGenerateColumns="False" CellSpacing="0" EnableTheming="True" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" GridLines="None">
      <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
      <MasterTableView EnableHeaderContextMenu="true" DataSourceID="BillDetailDatasource" DataKeyNames="Id" CommandItemDisplay="Top" InsertItemPageIndexAction="ShowItemOnCurrentPage">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column" Created="True"></ExpandCollapseColumn>

        <Columns>
          <%--          <telerik:GridBoundColumn DataField="BillId" HeaderText="BillId" SortExpression="BillId" UniqueName="BillId" DataType="System.Int32" ReadOnly="True" Visible="False" Display="False"></telerik:GridBoundColumn>--%>
          <telerik:GridBoundColumn DataField="Id" HeaderText="Id" SortExpression="Id" UniqueName="Id" DataType="System.Int32" ReadOnly="True" Visible="False" Display="False"></telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="BillDetailId" HeaderText="Code" SortExpression="BillDetailId" UniqueName="BillDetailId"></telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="Name" HeaderText="Description" SortExpression="Name" UniqueName="Name"></telerik:GridBoundColumn>
          <telerik:GridDropDownColumn DataField="AreaId" DataSourceID="areaDataSource" DropDownControlType="DropDownList" HeaderText="Area" ListTextField="Area" ListValueField="AreaId" UniqueName="AreaColumn"></telerik:GridDropDownColumn>
          <telerik:GridDropDownColumn DataField="BillingEventId" DataSourceID="billingEventsDataSource" DropDownControlType="DropDownList" HeaderText="Event" SortExpression="BillingEventId" ListTextField="Name" ListValueField="Id" UniqueName="BillingEventId"></telerik:GridDropDownColumn>

          <telerik:GridNumericColumn NumericType="Number" DecimalDigits="4" DataField="Amount" FilterControlAltText="Filter Amount column" HeaderText="Amount" SortExpression="Amount" UniqueName="Amount" DataType="System.Double"></telerik:GridNumericColumn>
          <telerik:GridNumericColumn NumericType="Number" DecimalDigits="4" DataField="WeightInKg" FilterControlAltText="Filter WeightInKg column" HeaderText="WeightInKg" SortExpression="WeightInKg" UniqueName="WeightInKg" DataType="System.Double"></telerik:GridNumericColumn>
          <telerik:GridNumericColumn NumericType="Number" DecimalDigits="4" DataField="NumberOfUnits" FilterControlAltText="Filter NumberOfUnits column" HeaderText="NumberOfUnits" SortExpression="NumberOfUnits" UniqueName="NumberOfUnits" DataType="System.Double"></telerik:GridNumericColumn>
          <telerik:GridNumericColumn NumericType="Number" DecimalDigits="4" DataField="CubeInMeters" DataType="System.Double" FilterControlAltText="Filter CubeInMeters column" HeaderText="CubeInMeters" SortExpression="CubeInMeters" UniqueName="CubeInMeters"></telerik:GridNumericColumn>
          <telerik:GridNumericColumn NumericType="Number" DecimalDigits="4" DataField="NumberOfPallets" DataType="System.Double" FilterControlAltText="Filter NumberOfPallets column" HeaderText="NumberOfPallets" SortExpression="NumberOfPallets" UniqueName="NumberOfPallets"></telerik:GridNumericColumn>
          <telerik:GridNumericColumn NumericType="Number" DecimalDigits="4" DataField="NumberOfCartons" DataType="System.Double" FilterControlAltText="Filter NumberOfCartons column" HeaderText="NumberOfCartons" SortExpression="NumberOfCartons" UniqueName="NumberOfCartons"></telerik:GridNumericColumn>

        </Columns>

        <EditFormSettings ColumnNumber="2" CaptionDataField="ProductName" CaptionFormatString="Edit properties of Service {0}"
          InsertCaption="New Service">
          <FormTableItemStyle Wrap="true"></FormTableItemStyle>
          <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
          <FormMainTableStyle GridLines="Horizontal" CellSpacing="0" CellPadding="3" Width="100%"></FormMainTableStyle>
          <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px"></FormTableStyle>
          <FormTableAlternatingItemStyle Wrap="True"></FormTableAlternatingItemStyle>
          <EditColumn ButtonType="ImageButton" InsertText="Insert Order" UpdateText="Update record"
            UniqueName="EditCommandColumn1" CancelText="Cancel edit">
          </EditColumn>
          <FormTableButtonRowStyle HorizontalAlign="Left" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
        </EditFormSettings>

        <BatchEditingSettings EditType="Cell"></BatchEditingSettings>

        <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
      </MasterTableView>

      <ExportSettings>
        <Pdf>
          <PageHeader>
            <LeftCell Text=""></LeftCell>

            <MiddleCell Text=""></MiddleCell>

            <RightCell Text=""></RightCell>
          </PageHeader>

          <PageFooter>
            <LeftCell Text=""></LeftCell>

            <MiddleCell Text=""></MiddleCell>

            <RightCell Text=""></RightCell>
          </PageFooter>
        </Pdf>
      </ExportSettings>

      <ClientSettings AllowColumnsReorder="True" EnablePostBackOnRowClick="True" ReorderColumnsOnClient="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
      </ClientSettings>
      <GroupingSettings ShowUnGroupButton="True" />

      <FilterMenu EnableImageSprites="False"></FilterMenu>
    </telerik:RadGrid>

    <asp:ObjectDataSource ID="BillDetailDatasource" runat="server" InsertMethod="Update" OldValuesParameterFormatString="original_{0}" SelectMethod="GetByBillId" TypeName="BillDetail" UpdateMethod="Update">
      <InsertParameters>
        <asp:Parameter Name="id" Type="Int32" />
        <asp:Parameter Name="billDetailId" Type="String" />
        <asp:Parameter Name="name" Type="String" />
        <asp:Parameter Name="areaId" Type="Int32" />
        <asp:Parameter Name="weightInKg" Type="Double" />
        <asp:Parameter Name="numberOfUnits" Type="Double" />
        <asp:Parameter Name="cubeInMeters" Type="Double" />
        <asp:Parameter Name="numberOfPallets" Type="Double" />
        <asp:Parameter Name="numberOfCartons" Type="Double" />
        <asp:Parameter Name="amount" Type="Double" />
        <asp:Parameter Name="billingEventId" Type="Int32" />
        <asp:Parameter Name="productId" Type="Int32" />
        <asp:ControlParameter ControlID="RadGridBills" Name="BillId" PropertyName="SelectedValue" Type="Int32" />
      </InsertParameters>
      <SelectParameters>
        <asp:ControlParameter ControlID="RadGridBills" Name="BillId" PropertyName="SelectedValue" Type="Int32" />
      </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="areaDataSource" runat="server" SelectMethod="GetAreas" TypeName="Area">
      <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="billingEventsDataSource" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetBillingEvents" TypeName="BillingEvent"></asp:ObjectDataSource>
    <telerik:RadButton ID="ButtonSaveSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveSettings_Click" />
  </div>
</asp:Content>

