﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RateDefaultMaintenance.aspx.cs" Inherits="Billing_RateDefaultMaintenance"
  Title="<%$ Resources:Default, RateDefaultMaintenanceTitle %>" StylesheetTheme="Default" Theme="Default"
  MasterPageFile="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
  <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, RateDefault %>"></asp:Label>
  <br />
  <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
  <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="RadGridrateDefaults">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridrateDefault" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>

  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
  </telerik:RadAjaxLoadingPanel>


  <div style="float: left;">
    <asp:Panel runat="server">
      <table>
        <tr>
          <td style="height: 54px">
            <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
          </td>
          <td style="height: 54px">
            <telerik:RadComboBox ID="cboPrincipal" Width="80%" runat="server"
              DataSourceID="ObjectDataSourcePrincipal" DataTextField="Principal" DataValueField="PrincipalId" AutoPostBack="True">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal" SelectMethod="GetAllPrincipals" OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
          </td>
        </tr>
      </table>
    </asp:Panel>

    <asp:Panel runat="server">
      <telerik:RadGrid ID="RadGrid1" runat="server" AllowAutomaticDeletes="True"
        AllowAutomaticInserts="True" AllowAutomaticUpdates="True" DataSourceID="rateDefaultDataSource"
        AllowSorting="True" CellSpacing="0" AutoGenerateColumns="False" GroupingEnabled="False" >
        <MasterTableView Width="100%" CommandItemDisplay="Top" HorizontalAlign="Left" DataKeyNames="Id">
          <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

          <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

          <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>

          <Columns>
            <telerik:GridBoundColumn DataField="Id" HeaderText="Id" UniqueName="idColumn" Visible="False" FilterControlAltText="Filter Id column" ReadOnly="True" Display="false" InsertVisiblityMode="AlwaysHidden" />

            <telerik:GridDropDownColumn DataField="PrincipalId" DataSourceID="principalDataSource" FilterControlAltText="Filter Principal column" HeaderText="Principal" ListTextField="Principal" ListValueField="PrincipalId" UniqueName="cboPrincipalColumn" ReadOnly="True">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn DataField="AreaId" DataSourceID="areaDataSource" FilterControlAltText="Filter Area column" HeaderText="Area" ListTextField="Area" ListValueField="AreaId" UniqueName="cboAreaColumn">
            </telerik:GridDropDownColumn>

            <telerik:GridNumericColumn DataField="WeightInKg" DecimalDigits="6" DataFormatString="{0:N6}" DefaultInsertValue="0" FilterControlAltText="Filter Weight In Kg column" HeaderText="Weight In Kg" UniqueName="WeightInKgColumn" />
            <telerik:GridNumericColumn DataField="NumberOfUnit" DecimalDigits="2" DataFormatString="{0:N2}" DefaultInsertValue="0" FilterControlAltText="Filter Number Of Unit column" HeaderText="Number Of Unit" UniqueName="NumberOfUnitColumn" />
            <telerik:GridNumericColumn DataField="CubeInMeters" DecimalDigits="2" DataFormatString="{0:N2}" DefaultInsertValue="0" FilterControlAltText="Filter Cube In Meters column" HeaderText="Cube In Meters" UniqueName="CubeInMetersColumn" />
            <telerik:GridNumericColumn DataField="NumberOfPallets" DecimalDigits="2" DataFormatString="{0:N2}" DefaultInsertValue="0" FilterControlAltText="Filter Number Of Pallets column" HeaderText="Number Of Pallets" UniqueName="NumberOfPalletsColumn" />
            <telerik:GridNumericColumn DataField="NumberOfCartons" DecimalDigits="2" DataFormatString="{0:N2}" DefaultInsertValue="0" FilterControlAltText="Filter Number Of Cartons column" HeaderText="Number Of Cartons" UniqueName="NumberOfCartonsColumn" />
            <telerik:GridNumericColumn DataField="Flatrate" DecimalDigits="2" DataFormatString="{0:N2}" DefaultInsertValue="0" FilterControlAltText="Filter Flat-rate column" HeaderText="Flat-rate" UniqueName="FlatrateColumn" />


            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Edit" Display="true" Text="Edit" FilterControlAltText="Filter editColumn column" UniqueName="editColumn" />
            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Delete" Display="true" Text="Delete" ItemStyle-HorizontalAlign="Left" FilterControlAltText="Filter deleteColumn column" UniqueName="deleteColumn">
              <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </telerik:GridButtonColumn>
          </Columns>

          <EditFormSettings ColumnNumber="8" CaptionFormatString="Edit details for Rate Default">
            <FormTableItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True"></FormTableItemStyle>
            <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
            <FormMainTableStyle CellSpacing="0" CellPadding="1" HorizontalAlign="Left" />
            <FormTableStyle CellSpacing="0" CellPadding="1" CssClass="module"
              Height="100%" />
            <FormTableAlternatingItemStyle Wrap="True" HorizontalAlign="Left"></FormTableAlternatingItemStyle>
            <FormStyle Width="100%" BackColor="#EEF2EA"></FormStyle>
            <EditColumn UpdateText="Save Changes" UniqueName="EditCommandColumn1" CancelText="Cancel edit" />
            <FormTableButtonRowStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="EditFormButtonRow" />
          </EditFormSettings>

          <BatchEditingSettings EditType="Cell"></BatchEditingSettings>

          <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

        </MasterTableView>

        <ExportSettings>
          <Pdf>
            <PageHeader>
              <LeftCell Text=""></LeftCell>

              <MiddleCell Text=""></MiddleCell>

              <RightCell Text=""></RightCell>
            </PageHeader>

            <PageFooter>
              <LeftCell Text=""></LeftCell>

              <MiddleCell Text=""></MiddleCell>

              <RightCell Text=""></RightCell>
            </PageFooter>
          </Pdf>
        </ExportSettings>

        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
          <Selecting AllowRowSelect="True" />
          <ClientEvents></ClientEvents>
        </ClientSettings>

        <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

        <FilterMenu EnableImageSprites="False"></FilterMenu>
      </telerik:RadGrid>

      <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
          function onSelectedIndexChanged(sender, eventArgs) {
            var selectedItem = eventArgs.get_item();
            var selectedItemText = selectedItem != null ? selectedItem.get_text() : sender.get_text();
            __doPostBack('', '');
          }
        </script>

      </telerik:RadScriptBlock>


      <asp:ObjectDataSource ID="principalDataSource" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetAllPrincipals" TypeName="Principal"></asp:ObjectDataSource>
      <asp:ObjectDataSource ID="areaDataSource" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetAreasByWarehouse" TypeName="Area">
        <SelectParameters>
          <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
          <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
        </SelectParameters>
      </asp:ObjectDataSource>
      <asp:ObjectDataSource ID="rateDefaultDataSource" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetAllByPrincipalId" TypeName="RateDefault" DeleteMethod="Delete" InsertMethod="Update" UpdateMethod="Update" OnInserted="rateDefaultDataSource_Inserted">
        <DeleteParameters>
          <asp:Parameter Name="id" Type="Int32" />
          <asp:Parameter Name="original_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
          <asp:Parameter Name="id" Type="Int32" />
          <asp:Parameter Name="weightInKg" Type="Double" />
          <asp:Parameter Name="numberOfUnit" Type="Double" />
          <asp:Parameter Name="cubeInMeters" Type="Double" />
          <asp:Parameter Name="numberOfPallets" Type="Double" />
          <asp:Parameter Name="numberOfCartons" Type="Double" />
          <asp:Parameter Name="flatrate" Type="Double" />
          <asp:ControlParameter ControlID="cboPrincipal" Name="principalId" PropertyName="SelectedValue" Type="Int32" />
          <asp:Parameter Name="areaId" Type="Int32" />
          <asp:Parameter Name="original_id" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
          <asp:ControlParameter ControlID="cboPrincipal" Name="principalId" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
          <asp:Parameter Name="id" Type="Int32" />
          <asp:Parameter Name="weightInKg" Type="Double" />
          <asp:Parameter Name="numberOfUnit" Type="Double" />
          <asp:Parameter Name="cubeInMeters" Type="Double" />
          <asp:Parameter Name="numberOfPallets" Type="Double" />
          <asp:Parameter Name="numberOfCartons" Type="Double" />
          <asp:Parameter Name="flatrate" Type="Double" />
          <asp:ControlParameter ControlID="cboPrincipal" Name="principalId" PropertyName="SelectedValue" Type="Int32" />
          <asp:Parameter Name="areaId" Type="Int32" />
          <asp:Parameter Name="original_id" Type="Int32" />
        </UpdateParameters>
      </asp:ObjectDataSource>

    </asp:Panel>
    <telerik:RadButton ID="ButtonSaveSettings" runat="server" Text="Save Layout Settings" />
  </div>
</asp:Content>
