﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VehicleTypeMaintenance.aspx.cs" Inherits="Billing_VehicleTypeMaintenance"
  Title="<%$ Resources:Default, VehicleTypeTitle %>" StylesheetTheme="Default" Theme="Default"
  MasterPageFile="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
  <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, VehicleType %>"></asp:Label>
  <br />
  <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
  <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="RadGridvehicleTypes">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridvehicleType" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>

  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
  </telerik:RadAjaxLoadingPanel>

  <telerik:RadGrid ID="RadGrid1" runat="server" AllowAutomaticDeletes="True"
    AllowAutomaticInserts="True" AllowAutomaticUpdates="True" DataSourceID="vehicleTypeDataSource"
    AllowSorting="True" CellSpacing="0" AutoGenerateColumns="False" GroupingEnabled="False" GridLines="None" ShowHeader="False">
    <MasterTableView Width="100%" CommandItemDisplay="Top" HorizontalAlign="Left" DataKeyNames="Id">
      <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

      <Columns>
        <telerik:GridBoundColumn DataField="Id" HeaderText="Id" UniqueName="idColumn" Visible="false" FilterControlAltText="Filter Id column" ReadOnly="True" Display="false" InsertVisiblityMode="AlwaysHidden" />
        <telerik:GridBoundColumn DataField="vehicleTypeId" HeaderText="Code" UniqueName="vehicleTypeIdColumn" />
        <telerik:GridBoundColumn DataField="Name" HeaderText="Description" UniqueName="nameColumn" />
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Edit" Display="true" Text="Edit" />
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Delete" Display="true" Text="Delete" ItemStyle-HorizontalAlign="Left" />
      </Columns>

      <EditFormSettings ColumnNumber="8" CaptionFormatString="Edit details for Vehicle Type with ID {0}" CaptionDataField="VehicleTypeId">
        <FormTableItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True"></FormTableItemStyle>
        <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
        <FormMainTableStyle CellSpacing="0" CellPadding="1" HorizontalAlign="Left" />
        <FormTableStyle CellSpacing="0" CellPadding="1" CssClass="module"
          Height="110px" />
        <FormTableAlternatingItemStyle Wrap="True" HorizontalAlign="Left"></FormTableAlternatingItemStyle>
        <FormStyle Width="100%" BackColor="#EEF2EA"></FormStyle>
        <EditColumn UpdateText="Save Changes" UniqueName="EditCommandColumn1" CancelText="Cancel edit">
        </EditColumn>
        <FormTableButtonRowStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
      </EditFormSettings>

    </MasterTableView>

    <ExportSettings>
      <Pdf>
        <PageHeader>
          <LeftCell Text=""></LeftCell>

          <MiddleCell Text=""></MiddleCell>

          <RightCell Text=""></RightCell>
        </PageHeader>

        <PageFooter>
          <LeftCell Text=""></LeftCell>

          <MiddleCell Text=""></MiddleCell>

          <RightCell Text=""></RightCell>
        </PageFooter>
      </Pdf>
    </ExportSettings>

    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
      <Selecting AllowRowSelect="True" />
      <ClientEvents></ClientEvents>
    </ClientSettings>

    <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

    <FilterMenu EnableImageSprites="False"></FilterMenu>
  </telerik:RadGrid>

  <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
      function onSelectedIndexChanged(sender, eventArgs) {
        var selectedItem = eventArgs.get_item();
        var selectedItemText = selectedItem != null ? selectedItem.get_text() : sender.get_text();
        __doPostBack('', '');
      }
    </script>

  </telerik:RadScriptBlock>

  <div style="float: left;">

    <asp:ObjectDataSource ID="vehicleTypeDataSource" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetvehicleTypes" TypeName="vehicleType" DeleteMethod="Delete" InsertMethod="Update" UpdateMethod="Update">
      <DeleteParameters>
        <asp:Parameter Name="Id" Type="Int32" />
      </DeleteParameters>
      <InsertParameters>
        <asp:Parameter Name="Id" Type="Int32" />
        <asp:Parameter Name="vehicleTypeId" Type="String" />
        <asp:Parameter Name="Name" Type="String" />
      </InsertParameters>
      <UpdateParameters>
        <asp:Parameter Name="Id" Type="Int32" />
        <asp:Parameter Name="vehicleTypeId" Type="String" />
        <asp:Parameter Name="Name" Type="String" />
      </UpdateParameters>
    </asp:ObjectDataSource>

    <telerik:RadButton ID="ButtonSaveSettings" runat="server" Text="Save Layout Settings" />
  </div>
</asp:Content>
