<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutboundDocumentTypeFormView.ascx.cs" Inherits="UserControls_OutboundDocumentTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>OutboundDocumentType</h3>
<asp:FormView ID="FormViewOutboundDocumentType" runat="server" DataKeyNames="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocumentTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="OutboundDocumentTypeCodeTextBox" runat="server" Text='<%# Bind("OutboundDocumentTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundDocumentType:
                </td>
                <td>
                    <asp:TextBox ID="OutboundDocumentTypeTextBox" runat="server" Text='<%# Bind("OutboundDocumentType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="OutboundDocumentTypeTextBox"
                        ErrorMessage="* Please enter OutboundDocumentType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListPriorityId"
                        ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AlternatePallet:
                </td>
                <td>
                    <asp:CheckBox ID="AlternatePalletCheckBox" runat="server" Checked='<%# Bind("AlternatePallet") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SubstitutePallet:
                </td>
                <td>
                    <asp:CheckBox ID="SubstitutePalletCheckBox" runat="server" Checked='<%# Bind("SubstitutePallet") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AutoRelease:
                </td>
                <td>
                    <asp:CheckBox ID="AutoReleaseCheckBox" runat="server" Checked='<%# Bind("AutoRelease") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AddToShipment:
                </td>
                <td>
                    <asp:CheckBox ID="AddToShipmentCheckBox" runat="server" Checked='<%# Bind("AddToShipment") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    MultipleOnShipment:
                </td>
                <td>
                    <asp:CheckBox ID="MultipleOnShipmentCheckBox" runat="server" Checked='<%# Bind("MultipleOnShipment") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    LIFO:
                </td>
                <td>
                    <asp:CheckBox ID="LIFOCheckBox" runat="server" Checked='<%# Bind("LIFO") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    MinimumShelfLife:
                </td>
                <td>
                    <asp:TextBox ID="MinimumShelfLifeTextBox" runat="server" Text='<%# Bind("MinimumShelfLife") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AreaType:
                </td>
                <td>
                    <asp:TextBox ID="AreaTypeTextBox" runat="server" Text='<%# Bind("AreaType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AutoSendup:
                </td>
                <td>
                    <asp:CheckBox ID="AutoSendupCheckBox" runat="server" Checked='<%# Bind("AutoSendup") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CheckingLane:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCheckingLane" runat="server" DataSourceID="ObjectDataSourceDDLCheckingLane"
                        DataTextField="Location" DataValueField="CheckingLane" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCheckingLane" runat="server" TypeName="StaticInfoLocation"
                        SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DespatchBay:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListDespatchBay" runat="server" DataSourceID="ObjectDataSourceDDLDespatchBay"
                        DataTextField="Location" DataValueField="DespatchBay" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLDespatchBay" runat="server" TypeName="StaticInfoLocation"
                        SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    PlanningComplete:
                </td>
                <td>
                    <asp:CheckBox ID="PlanningCompleteCheckBox" runat="server" Checked='<%# Bind("PlanningComplete") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AutoInvoice:
                </td>
                <td>
                    <asp:CheckBox ID="AutoInvoiceCheckBox" runat="server" Checked='<%# Bind("AutoInvoice") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Backorder:
                </td>
                <td>
                    <asp:CheckBox ID="BackorderCheckBox" runat="server" Checked='<%# Bind("Backorder") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocumentTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="OutboundDocumentTypeCodeTextBox" runat="server" Text='<%# Bind("OutboundDocumentTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundDocumentType:
                </td>
                <td>
                    <asp:TextBox ID="OutboundDocumentTypeTextBox" runat="server" Text='<%# Bind("OutboundDocumentType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="OutboundDocumentTypeTextBox"
                        ErrorMessage="* Please enter OutboundDocumentType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListPriorityId"
                        ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AlternatePallet:
                </td>
                <td>
                    <asp:CheckBox ID="AlternatePalletCheckBox" runat="server" Checked='<%# Bind("AlternatePallet") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SubstitutePallet:
                </td>
                <td>
                    <asp:CheckBox ID="SubstitutePalletCheckBox" runat="server" Checked='<%# Bind("SubstitutePallet") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AutoRelease:
                </td>
                <td>
                    <asp:CheckBox ID="AutoReleaseCheckBox" runat="server" Checked='<%# Bind("AutoRelease") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AddToShipment:
                </td>
                <td>
                    <asp:CheckBox ID="AddToShipmentCheckBox" runat="server" Checked='<%# Bind("AddToShipment") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    MultipleOnShipment:
                </td>
                <td>
                    <asp:CheckBox ID="MultipleOnShipmentCheckBox" runat="server" Checked='<%# Bind("MultipleOnShipment") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    LIFO:
                </td>
                <td>
                    <asp:CheckBox ID="LIFOCheckBox" runat="server" Checked='<%# Bind("LIFO") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    MinimumShelfLife:
                </td>
                <td>
                    <asp:TextBox ID="MinimumShelfLifeTextBox" runat="server" Text='<%# Bind("MinimumShelfLife") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AreaType:
                </td>
                <td>
                    <asp:TextBox ID="AreaTypeTextBox" runat="server" Text='<%# Bind("AreaType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AutoSendup:
                </td>
                <td>
                    <asp:CheckBox ID="AutoSendupCheckBox" runat="server" Checked='<%# Bind("AutoSendup") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CheckingLane:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCheckingLane" runat="server" DataSourceID="ObjectDataSourceDDLCheckingLane"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("CheckingLane") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCheckingLane" runat="server" TypeName="StaticInfoLocation"
                        SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DespatchBay:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListDespatchBay" runat="server" DataSourceID="ObjectDataSourceDDLDespatchBay"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("DespatchBay") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLDespatchBay" runat="server" TypeName="StaticInfoLocation"
                        SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    PlanningComplete:
                </td>
                <td>
                    <asp:CheckBox ID="PlanningCompleteCheckBox" runat="server" Checked='<%# Bind("PlanningComplete") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AutoInvoice:
                </td>
                <td>
                    <asp:CheckBox ID="AutoInvoiceCheckBox" runat="server" Checked='<%# Bind("AutoInvoice") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Backorder:
                </td>
                <td>
                    <asp:CheckBox ID="BackorderCheckBox" runat="server" Checked='<%# Bind("Backorder") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocumentTypeCode:
                </td>
                <td>
                    <asp:Label ID="OutboundDocumentTypeCodeLabel" runat="server" Text='<%# Bind("OutboundDocumentTypeCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundDocumentType:
                </td>
                <td>
                    <asp:Label ID="OutboundDocumentTypeLabel" runat="server" Text='<%# Bind("OutboundDocumentType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    AlternatePallet:
                </td>
                <td>
                    <asp:Label ID="AlternatePalletLabel" runat="server" Text='<%# Bind("AlternatePallet") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SubstitutePallet:
                </td>
                <td>
                    <asp:Label ID="SubstitutePalletLabel" runat="server" Text='<%# Bind("SubstitutePallet") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AutoRelease:
                </td>
                <td>
                    <asp:Label ID="AutoReleaseLabel" runat="server" Text='<%# Bind("AutoRelease") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AddToShipment:
                </td>
                <td>
                    <asp:Label ID="AddToShipmentLabel" runat="server" Text='<%# Bind("AddToShipment") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MultipleOnShipment:
                </td>
                <td>
                    <asp:Label ID="MultipleOnShipmentLabel" runat="server" Text='<%# Bind("MultipleOnShipment") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    LIFO:
                </td>
                <td>
                    <asp:Label ID="LIFOLabel" runat="server" Text='<%# Bind("LIFO") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MinimumShelfLife:
                </td>
                <td>
                    <asp:Label ID="MinimumShelfLifeLabel" runat="server" Text='<%# Bind("MinimumShelfLife") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AreaType:
                </td>
                <td>
                    <asp:Label ID="AreaTypeLabel" runat="server" Text='<%# Bind("AreaType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AutoSendup:
                </td>
                <td>
                    <asp:Label ID="AutoSendupLabel" runat="server" Text='<%# Bind("AutoSendup") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCheckingLane" runat="server" DataSourceID="ObjectDataSourceDDLCheckingLane"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("CheckingLane") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCheckingLane" runat="server" TypeName="StaticInfoLocation"
                        SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListDespatchBay" runat="server" DataSourceID="ObjectDataSourceDDLDespatchBay"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("DespatchBay") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLDespatchBay" runat="server" TypeName="StaticInfoLocation"
                        SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    PlanningComplete:
                </td>
                <td>
                    <asp:Label ID="PlanningCompleteLabel" runat="server" Text='<%# Bind("PlanningComplete") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AutoInvoice:
                </td>
                <td>
                    <asp:Label ID="AutoInvoiceLabel" runat="server" Text='<%# Bind("AutoInvoice") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Backorder:
                </td>
                <td>
                    <asp:Label ID="BackorderLabel" runat="server" Text='<%# Bind("Backorder") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/OutboundDocumentTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="StaticInfoOutboundDocumentType"
    SelectMethod="GetOutboundDocumentType"
    DeleteMethod="DeleteOutboundDocumentType"
    UpdateMethod="UpdateOutboundDocumentType"
    InsertMethod="InsertOutboundDocumentType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OutboundDocumentTypeCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="OutboundDocumentType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AlternatePallet" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="SubstitutePallet" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="AutoRelease" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="AddToShipment" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="MultipleOnShipment" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="LIFO" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="MinimumShelfLife" Type="Decimal" DefaultValue="-1" />
        <asp:Parameter Name="AreaType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="AutoSendup" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="CheckingLane" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DespatchBay" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PlanningComplete" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="AutoInvoice" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="Backorder" Type="Boolean" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="OutboundDocumentTypeId" QueryStringField="OutboundDocumentTypeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OutboundDocumentTypeCode" Type="String" />
        <asp:Parameter Name="OutboundDocumentType" Type="String" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AlternatePallet" Type="Boolean" />
        <asp:Parameter Name="SubstitutePallet" Type="Boolean" />
        <asp:Parameter Name="AutoRelease" Type="Boolean" />
        <asp:Parameter Name="AddToShipment" Type="Boolean" />
        <asp:Parameter Name="MultipleOnShipment" Type="Boolean" />
        <asp:Parameter Name="LIFO" Type="Boolean" />
        <asp:Parameter Name="MinimumShelfLife" Type="Decimal" />
        <asp:Parameter Name="AreaType" Type="String" />
        <asp:Parameter Name="AutoSendup" Type="Boolean" />
        <asp:Parameter Name="CheckingLane" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DespatchBay" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PlanningComplete" Type="Boolean" />
        <asp:Parameter Name="AutoInvoice" Type="Boolean" />
        <asp:Parameter Name="Backorder" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
