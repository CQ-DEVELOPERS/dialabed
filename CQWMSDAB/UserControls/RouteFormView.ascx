<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RouteFormView.ascx.cs" Inherits="UserControls_RouteFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Route</h3>
<asp:FormView ID="FormViewRoute" runat="server" DataKeyNames="RouteId" DataSourceID="ObjectDataSourceRoute" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Route:
                </td>
                <td>
                    <asp:TextBox ID="RouteTextBox" runat="server" Text='<%# Bind("Route") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RouteCode:
                </td>
                <td>
                    <asp:TextBox ID="RouteCodeTextBox" runat="server" Text='<%# Bind("RouteCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Route:
                </td>
                <td>
                    <asp:TextBox ID="RouteTextBox" runat="server" Text='<%# Bind("Route") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RouteCode:
                </td>
                <td>
                    <asp:TextBox ID="RouteCodeTextBox" runat="server" Text='<%# Bind("RouteCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Route:
                </td>
                <td>
                    <asp:Label ID="RouteLabel" runat="server" Text='<%# Bind("Route") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RouteCode:
                </td>
                <td>
                    <asp:Label ID="RouteCodeLabel" runat="server" Text='<%# Bind("RouteCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/RouteGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceRoute" runat="server" TypeName="StaticInfoRoute"
    SelectMethod="GetRoute"
    DeleteMethod="DeleteRoute"
    UpdateMethod="UpdateRoute"
    InsertMethod="InsertRoute">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RouteId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RouteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Route" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="RouteCode" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="RouteId" QueryStringField="RouteId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RouteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Route" Type="String" />
        <asp:Parameter Name="RouteCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
