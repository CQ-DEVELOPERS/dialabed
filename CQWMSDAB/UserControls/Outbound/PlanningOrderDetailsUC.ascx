﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlanningOrderDetailsUC.ascx.cs" Inherits="UserControls_Outbound_PlanningOrderDetailsUC" %>

<asp:Button ID="ButtonAutoAllocation2" runat="server" Text="<%$ Resources:Default, AutoAllocation%>"
    OnClick="ButtonAutoLocations2_Click" />
<asp:Button ID="ButtonMix" runat="server" Text="<%$ Resources:Default, CreateMixedJob%>"
    OnClick="ButtonMix_Click" style="Width:auto;"/>
<asp:Button ID="ButtonManualLocations2" runat="server" Text="<%$ Resources:Default, AlternateLocation%>"
    OnClick="ButtonManualLocations_Click" style="Width:auto;"/>
<asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
<asp:Label ID="LabelLocationStandard" runat="server" Text="<%$ Resources:Default, NoLocation%>"></asp:Label>
<asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
<asp:Label ID="LabelLocationBlue" runat="server" Text="<%$ Resources:Default, Allocated%>"></asp:Label>
<telerik:RadGrid ID="GridViewDetails" runat="server" Skin="Metro" AllowAutomaticUpdates="True" MasterTableView-CommandItemSettings-ShowRefreshButton="true"
    AllowMultiRowSelection="True" AllowFilteringByColumn="False" AllowSorting="True" AllowPaging="True" PageSize="30"
    DataSourceID="ObjectDataSourceDetails">
    <MasterTableView DataKeyNames="InstructionId" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
        <Columns>
            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
            <telerik:GridTemplateColumn UniqueName="AvailabilityIndicator" HeaderText="<%$ Resources:Default, AvailabilityIndicator %>">
                <ItemTemplate>
                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>" SortExpression="StorageUnitId"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>" SortExpression="SKU"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="Quantity"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" SortExpression="JobId"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>" SortExpression="InstructionType"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>" SortExpression="PickLocation"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status"></telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
    </ClientSettings>
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="Planning"
    SelectMethod="GetOrderDetails">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
            Type="String" />
        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
            Type="Int32" DefaultValue="-1" />
        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>