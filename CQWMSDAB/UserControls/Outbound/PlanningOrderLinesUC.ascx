﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlanningOrderLinesUC.ascx.cs" Inherits="UserControls_Outbound_PlanningOrderLinesUC" %>

<telerik:RadButton ID="ButtonSelect1" runat="server" Text="<%$ Resources:Default, SelectAll%>" OnClick="ButtonSelect_Click"></telerik:RadButton>
<telerik:RadButton ID="ButtonEdit" OnClick="ButtonEdit_Click" runat="server" Text="<%$ Resources:Default, Edit%>"></telerik:RadButton>
<telerik:RadButton ID="ButtonSave" OnClick="ButtonSave_Click" runat="server" Text="<%$ Resources:Default, Save%>"></telerik:RadButton>
<telerik:RadButton ID="ManualPalletise" OnClick="ButtonManualPalletise_Click" runat="server"
    Text="<%$ Resources:Default, ManualAllocation%>" Width="106px" style="Width:auto;"></telerik:RadButton>
<%--<telerik:RadButton ID="btnAutoPlanLine" OnClick="btnAutoPlanLine_Click" runat="server" Text="<%$ Resources:Default, ButtonAutoPlan%>"
    Width="106px" Style="width: auto;" AutoPostBack="true" Visible="false">
</telerik:RadButton>--%>
<asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
<asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
<asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
<asp:Label ID="LabelYellow2" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
<asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
<asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
<telerik:RadGrid ID="GridViewLineUpdate" runat="server" Skin="Metro" AllowAutomaticUpdates="True"
    AllowMultiRowSelection="True" AllowFilteringByColumn="False" AllowSorting="True" AllowPaging="True" PageSize="30"
    DataSourceID="ObjectDataSourceOrderLines">
    <MasterTableView DataKeyNames="IssueLineId,StorageUnitId" AutoGenerateColumns="False" EnableHeaderContextMenu="true" AllowM>
        <Columns>
            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
            <%--<telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>--%>
            <telerik:GridTemplateColumn UniqueName="AvailabilityIndicator" HeaderText="<%$ Resources:Default, RequiredIn %>">
                <ItemTemplate>
                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product"></telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListBatch" runat="server" OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged"
                        DataSourceID="ObjectDataSourceBatch" DataTextField="Batch" DataValueField="StorageUnitBatchId"
                        SelectedValue='<%# Bind("StorageUnitBatchId") %>' Enabled='<%# Eval("BatchSelect") %>'>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>" SortExpression="SKU"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>" SortExpression="OrderQuantity"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>" SortExpression="AvailablePercentage"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>" SortExpression="AvailableQuantity"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="OtherAreaTypeQuantity" HeaderText="Other Areas" SortExpression="AvailableQuantity"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>" SortExpression="LocationsAllocated"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="PalletQuantity" HeaderText="<%$ Resources:Default, PalletQuantity %>" SortExpression="PalletQuantity"></telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
    </ClientSettings>
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="Planning"
                            SelectMethod="GetOrderLines" UpdateMethod="UpdateOrderLine">
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Type="Int32" Name="issueLineId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitBatchId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitId"></asp:Parameter>
                            </UpdateParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="outboundShipmentId" Type="Int32" DefaultValue="-1" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                            SelectMethod="GetBatchesByStorageUnit" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:ControlParameter ControlID="GridViewLineUpdate" Name="StorageUnitId" PropertyName="SelectedValue"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
