﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControls_Outbound_PlanningOrderDetailsUC : System.Web.UI.UserControl
{
    #region Public Events
    public delegate void ErrorMessageEventHandler(string mesgText, string errorText);
    public event ErrorMessageEventHandler ErrorMessage;
    #endregion

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region ButtonAutoLocations2_Click
    protected void ButtonAutoLocations2_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            CheckBox cb = new CheckBox();
            Planning plan = new Planning();

            foreach (GridDataItem item in GridViewDetails.SelectedItems)
            {
                if (plan.AutoLocationAllocateInstruction(Session["ConnectionStringName"].ToString(), (int)item.GetDataKeyValue("InstructionId")) == false)
                {
                    errorMessageHandling("", "Auto Allocation Not Complete");
                    break;
                }
            }

            GridViewDetails.DataBind();

            errorMessageHandling("Auto Locations", "");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion


    #region ButtonMix_Click
    protected void ButtonMix_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonMix_Click";
        try
        {
            Session["InstructionTypeCode"] = "PM";

            //foreach (GridDataItem item in GridViewInstruction.SelectedItems)
            //{
            //    Session["ShipmentId"] = (int)item.GetDataKeyValue("OutboundShipmentId");
            //    Session["OrderNumber"] = (string)item.GetDataKeyValue("OrderNumber");
            //}

            Response.Redirect("~/Common/MixedJobCreate.aspx");
            errorMessageHandling("Mix", "");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion

    #region ButtonManualLocation_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            //GetEditIndex();
            //Session["FromURL"] = "~/Outbound/OutboundPlanning.aspx" + "?ActiveTab=" + RadTabStrip1.SelectedIndex.ToString();
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

            //Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
    }
    #endregion

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundPlanning", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundPlanning", theErrMethod, ex);

                errorMessageHandling("", result);

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion

    private void errorMessageHandling(string messageText, string errorText)
    {
        if (ErrorMessage != null)
            ErrorMessage(messageText, errorText);
    }
}