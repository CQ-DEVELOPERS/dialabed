﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlanningOrdersUC.ascx.cs" Inherits="UserControls_Outbound_PlanningOrdersUC" %>

<%@ Register Src="~/Common/OutboundSearchRoute.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<table>
    <tr valign="top">
        <td valign="bottom">
            <table>
                <tr>
                    <td>
                        <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                    </td>
                    <td>
                        <asp:Panel ID="PanelBuild" runat="server" GroupingText="<%$ Resources:Default, PalletBuildResults%>"
                            Font-Size="X-Small">
                            <telerik:RadGrid ID="GridViewBuild" runat="server" Skin="Default" DataSourceID="ObjectDataSourceBuild">
                                <HeaderStyle Font-Size="X-Small" />
                                <ItemStyle Font-Size="X-Small" />
                                <MasterTableView AutoGenerateColumns="True">
                                </MasterTableView>
                            </telerik:RadGrid>
                        </asp:Panel>
                        <asp:ObjectDataSource ID="ObjectDataSourceBuild" runat="server" TypeName="Planning"
                            SelectMethod="GetBuildResults">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                    Type="Int32" DefaultValue="-1" />
                                <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                    <td>
                        <asp:Panel ID="PanelAllocation" runat="server" GroupingText="<%$ Resources:Default, StockAllocationResults%>"
                            Font-Size="X-Small">
                            <telerik:RadGrid ID="GridViewAllocation" runat="server" Skin="Default" DataSourceID="ObjectDataSourceAllocation">
                                <HeaderStyle Font-Size="X-Small" />
                                <ItemStyle Font-Size="X-Small" />
                                <MasterTableView AutoGenerateColumns="True">
                                </MasterTableView>
                            </telerik:RadGrid>
                        </asp:Panel>
                        <asp:ObjectDataSource ID="ObjectDataSourceAllocation" runat="server" TypeName="Planning"
                            SelectMethod="GetAllocationResults">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                    Type="Int32" DefaultValue="-1" />
                                <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked%>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                                </td>
                                <td>
                                    <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue%>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                                </td>
                                <td>
                                    <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours%>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                                </td>
                                <td>
                                    <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours%>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                                </td>
                                <td>
                                    <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours%>"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <telerik:RadButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, Search%>" />
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonPalletise" runat="server" Text="<%$ Resources:Default, AutoPalletise%>"
                            OnClick="ButtonPalletise_Click" />
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonAutoAllocation" runat="server" Text="<%$ Resources:Default, AutoAllocation%>"
                            OnClick="ButtonAutoLocations_Click" Style="width: auto;" />
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonDeallocate" runat="server" Text="<%$ Resources:Default, DeAllocate%>"
                            OnClick="ButtonDeallocate_Click" />
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonPlanningComplete" runat="server" Text="<%$ Resources:Default, PlanningComplete%>"
                            OnClick="ButtonPlanningComplete_Click" Style="width: auto;" />
                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderRelease" runat="server"
                            TargetControlID="ButtonPlanningComplete" ConfirmText="<%$ Resources:Default, PressOKtoconfirmplanned%>"
                            Enabled="True">
                        </ajaxToolkit:ConfirmButtonExtender>
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonLoadPlanning" runat="server" Text="<%$ Resources:Default, LoadPlanningTitle%>" Enabled ="false"/>
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonWavePlanning" runat="server" Text="<%$ Resources:Default, WavePlanning%>" Enabled ="false"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<telerik:RadGrid ID="GridViewInstruction" runat="server" AllowPaging="true"  AllowSorting="True" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged"
    OnItemDataBound ="GridViewInstruction_ItemDataBound"
    DataSourceID="ObjectDataSourcePlanning" PageSize="10" AllowFilteringByColumn="False" Skin="Metro" ClientSettings-Selecting-AllowRowSelect="true"
    AllowAutomaticUpdates="True" AllowMultiRowSelection="True">
    <MasterTableView DataKeyNames="OutboundShipmentId,IssueId,OrderNumber" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
        <NoRecordsTemplate>
            <div>There are no records to display, please select a project</div>
        </NoRecordsTemplate>
        <Columns>
            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
            <telerik:GridTemplateColumn UniqueName="AvailabilityIndicator" HeaderText="<%$ Resources:Default, RequiredIn %>">
                <ItemTemplate>
                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn UniqueName="Availability" HeaderText="SOH Available">
                <ItemTemplate>
                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "Availability", "../images/Indicators/{0}.gif") %>' />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn ReadOnly="true" DataField="OutboundShipmentId" HeaderText='<%$ Resources:Default,OutboundShipmentId %>' SortExpression="OutboundShipmentId" />
            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="PrincipalCode" HeaderText="<%$ Resources:Default, Principal %>" SortExpression="PrincipalCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>" SortExpression="CustomerCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>" SortExpression="Customer"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="AdditionalText1" HeaderText="Balance" SortExpression="AdditionalText1"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="AdditionalText2" HeaderText="Amount" SortExpression="AdditionalText2" UniqueName ="AdditionalText2"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="IsFullyPaidOrder" HeaderText="IsFullyPaidOrder" SortExpression="IsFullyPaidOrder" UniqueName ="IsFullyPaidOrder" Display="false"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" SortExpression="NumberOfLines"></telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxDeliveryDate" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderDeliveryDate" runat="server" Animated="true"
                        Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxDeliveryDate">
                    </ajaxToolkit:CalendarExtender>
                </EditItemTemplate>
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:Label ID="LabelDeliveryDate" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, AreaType %>" SortExpression="AreaType" UniqueName="AreaTypeEdit">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListAreaType" runat="server" DataSourceID="ObjectDataSourceAreaType"
                        DataTextField="AreaType" DataValueField="AreaType" SelectedValue='<%# Bind("AreaType") %>'>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("AreaType") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                SortExpression="Status">
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority"  UniqueName ="PriorityEdit">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" SortExpression="OutboundDocumentType"></telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location"  UniqueName ="LocationEdit">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>"></telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route" UniqueName ="RouteEdit">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceRouting"
                        DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>'>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:Label ID="lblRoute" runat="server" Text='<%# Bind("Route") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate"></telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
			<telerik:GridBoundColumn DataField="ContactPerson" HeaderText="Contact" readonly="True" UniqueName ="ContactPersonEdit"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AddressLine1" HeaderText="Address Line 1" UniqueName ="AddressLine1Edit"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AddressLine2" HeaderText="Address Line 2" UniqueName ="AddressLine2Edit"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AddressLine3" HeaderText="Address Line 3" UniqueName ="AddressLine3Edit"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AddressLine4" HeaderText="Address Line 4" UniqueName ="AddressLine4Edit"></telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="False">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
    </ClientSettings>
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="Planning"
    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder"
    EnablePaging="true" SelectCountMethod="OrdersCount">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName"
            Type="String" />
        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId"
            Type="Int32" DefaultValue="-1" />
        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
            Type="Int32" />
        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
            Type="String" />
        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
        <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
        <asp:SessionParameter Name="RouteId" SessionField="RouteId" Type="Int32" DefaultValue="-1" />
    </SelectParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="outboundShipmentId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="OrderNumber" Type="String" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:Parameter Name="RouteId" Type="Int32" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="AddressLine1" Type="String" />
        <asp:Parameter Name="AddressLine2" Type="String" />
        <asp:Parameter Name="AddressLine3" Type="String" />
        <asp:Parameter Name="AddressLine4" Type="String" />
        <asp:Parameter Name="AddressLine5" Type="String" />
    </UpdateParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
    TypeName="Priority">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceAreaType" runat="server" TypeName="Area"
    SelectMethod="GetAreaTypes">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
            Type="String" />
        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
    SelectMethod="GetLocationsByIssue">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
            Type="String" />
        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
            Type="Int32" DefaultValue="-1" />
        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceRouting" runat="server" SelectMethod="GetRoutes"
    TypeName="Route">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<telerik:radbutton id="ButtonSaveSettings" runat="server" text="Save Layout Settings" onclick="ButtonSaveSettings_Click" />
<telerik:radwindowmanager rendermode="Lightweight" runat="server" id="errorAlertWindowManager"></telerik:radwindowmanager>