﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControls_Outbound_PlanningOrderLinesUC : System.Web.UI.UserControl
{
    #region Public Events
    public delegate void ErrorMessageEventHandler(string mesgText, string errorText);
    public event ErrorMessageEventHandler ErrorMessage;
    public delegate void TabIndexChangeEventHandler(int tabIndex);
    public event TabIndexChangeEventHandler TabIndexChange;
    #endregion

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region ButtonEdit_Click
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            //Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
    }
    #endregion ButtonEdit_Click

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";

        try
        {
            foreach (GridDataItem item in GridViewLineUpdate.EditItems)
            {
                GridViewLineUpdate.MasterTableView.PerformUpdate(item);
            }

            SetEditIndexRowNumber();

            errorMessageHandling("", "");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridDataItem row in GridViewLineUpdate.Items)
            {
                row.Selected = true;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion

    #region ButtonManualPalletise_Click
    protected void ButtonManualPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualPalletise_Click";

        try
        {
            ArrayList rowDataKeys = new ArrayList();
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridDataItem row in GridViewLineUpdate.Items)
            {
                if (row.Selected)
                {
                    rowList.Add(row.RowIndex);
                }
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowDataKeys;

            Session["FromURL"] = "OutboundPlanning.aspx";

            errorMessageHandling("", "");
            Response.Redirect("ManualPalletiseLocate.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";

        try
        {
            if (Session["rowList"] == null)
            {
                int index = 0;
                CheckBox cb = new CheckBox();
                ArrayList rowList = new ArrayList();
                foreach (GridDataItem item in GridViewLineUpdate.SelectedItems)
                {
                    rowList.Add(item.RowIndex);

                    index++;
                }
                if (rowList.Count > 0)
                    Session["rowList"] = rowList;
            }

            errorMessageHandling("", "");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";

        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];

            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                }
                else
                {
                    foreach (GridDataItem item in GridViewLineUpdate.Items)
                    {
                        if (item.RowIndex == (int)rowList[0])
                            item.Edit = true;

                        rowList.Remove(rowList[0]);

                        break;
                    }
                }
            }

            errorMessageHandling("", "");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }

    }
    #endregion

    private void errorMessageHandling(string messageText, string errorText)
    {
        if (ErrorMessage != null)
            ErrorMessage(messageText, errorText);
    }

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundPlanning", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundPlanning", theErrMethod, ex);

                errorMessageHandling("", result);

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "DropDownListBatch_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["StorageUnitId"] = int.Parse(dpList.SelectedValue);

            errorMessageHandling("", "");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion
    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";
        try
        {
            foreach (GridDataItem item in GridViewLineUpdate.SelectedItems)
            {
                e.InputParameters["storageUnitId"] = (int)item.GetDataKeyValue("StorageUnitId");
            }

            errorMessageHandling("", "");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion

    #region ObjectDataSourceOrderLines_Selecting
    protected void ObjectDataSourceOrderLines_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceOrderLines_Selecting";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            //Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceOrderLines_Selecting

    public void RefreshLineGrid()
    {
        GridViewLineUpdate.DataBind();
    }

}