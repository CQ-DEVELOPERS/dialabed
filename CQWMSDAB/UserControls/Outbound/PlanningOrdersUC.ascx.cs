﻿using Microsoft.Reporting.WebForms;
using System;
using System.Text;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Diagnostics;

public partial class UserControls_Outbound_PlanningOrdersUC : System.Web.UI.UserControl
{
    #region Public Events
    public delegate void ErrorMessageEventHandler(string mesgText, string errorText);
    public event ErrorMessageEventHandler ErrorMessage;

    public delegate void AllocateHideEventHandler(bool isHide);
    public event AllocateHideEventHandler AllocateHide;
    #endregion

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    private Stopwatch watch { get; set; }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                {
                    GridSettingsPersister settings = new GridSettingsPersister(GridViewInstruction);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), GridViewInstruction, "OutboundPlanningGridViewInstruction", (int)Session["OperatorId"]);
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {

        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewInstruction.DataBind();
        }
        catch (Exception ex)
        {
            //result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region ButtonPalletise_Click
    protected void ButtonPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletise_Click";

        try
        {
            Planning plan = new Planning();
            //start watch
            watch = new Stopwatch();
            watch.Start();

            foreach (GridDataItem item in GridViewInstruction.SelectedItems)
            {
                if (plan.Palletise(Session["ConnectionStringName"].ToString(), int.Parse(item.GetDataKeyValue("IssueId").ToString()), -1))
                {
                    GridViewBuild.DataBind();
                    GridViewAllocation.DataBind();
                    GridViewInstruction.DataBind();
                    errorAlertWindowManager.RadConfirm(string.Format("Palletised Successfully. Time took to palletize : {0} seconds.", watch.Elapsed.TotalSeconds.ToString()), null, 300, 120, null, "Palletisation");

                    //log responsetime
                    this.CaptureResponseTime(Session["ConnectionStringName"].ToString(), watch, "Palletisation","IssueId", int.Parse(item.GetDataKeyValue("IssueId").ToString()));
                    //restart watch
                    this.RestartTime(watch);
                }
                else
                {
                    this.CaptureResponseTime(Session["ConnectionStringName"].ToString(), watch, "Palletisation Failed", "IssueId", int.Parse(item.GetDataKeyValue("IssueId").ToString()));
                    errorAlertWindowManager.RadAlert("Palletisation Failed.", 250, 100, "Palletisation", null);
                }
            }
            
            GridViewInstruction.DataBind();
            this.SelectGridViewInstruction();
            this.CaptureResponseTime(Session["ConnectionStringName"].ToString(), watch, "ReloadGrid", null, null);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorAlertWindowManager.RadAlert(result = result.Replace("'", ""), 350, 100, "Error", null);
        }
    }
    #endregion "ButtonPalletise_Click"
    public void SelectGridViewInstruction()
    {
        if (this.GridViewInstruction.MasterTableView.Items.Count > 1)
        {
            if (SelectedRow() > -1)
            {
                var gridRow = GridViewInstruction.MasterTableView.Items[SelectedRow()];
                gridRow.Selected = true;
            }
        }
    }
    public int SelectedRow()
    {
        int selectedIndex = 0;
        if (GridViewInstruction.MasterTableView != null && GridViewInstruction.MasterTableView.Items != null && GridViewInstruction.MasterTableView.Items.Count > 0)
        {
            for (int i = 0; i < GridViewInstruction.MasterTableView.Items.Count; i++)
            {
                GridDataItem item = GridViewInstruction.MasterTableView.Items[i];
                if (Session["IssueId"] != null && Session["IssueId"].ToString() == item.GetDataKeyValue("IssueId").ToString())
                {
                    selectedIndex = i;
                    return selectedIndex;
                }
                else
                {
                    selectedIndex++;
                }
            }
        }

        return selectedIndex;
    }

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            Planning plan = new Planning();

            foreach (GridDataItem item in GridViewInstruction.SelectedItems)
            {
                if (!plan.AutoLocationAllocateIssue(Session["ConnectionStringName"].ToString(), (int)item.GetDataKeyValue("IssueId")))
                {
                    errorAlertWindowManager.RadAlert("Auto Allocation Not Complete.", 300, 100, "Auto Allocation", null);
                    return;
                }
            }

            GridViewInstruction.DataBind();
            this.SelectGridViewInstruction();
            errorAlertWindowManager.RadConfirm("Auto Allocated.", null, 250, 100, null, "Auto Allocation");

        }
        catch (Exception ex)
        {
            errorAlertWindowManager.RadAlert(result = result.Replace("'", ""), 350, 100, "Error", null);
        }
    }
    #endregion

    #region ButtonDeallocate_Click
    protected void ButtonDeallocate_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeallocate_Click";

        try
        {
            Palletise palletise = new Palletise();

            foreach (GridDataItem item in GridViewInstruction.SelectedItems)
            {
                if (!palletise.DeallocateIssues(Session["ConnectionStringName"].ToString(), (int)item.GetDataKeyValue("IssueId"), -1))
                {
                    errorAlertWindowManager.RadAlert("De-Allocation Not Complete.", 300, 100, "De-Allocatio", null);
                    return;
                }
            }

            GridViewInstruction.DataBind();
            this.SelectGridViewInstruction();
            errorAlertWindowManager.RadConfirm("De-Allocation Complete.", null, 250, 100, null, "De-Allocation ");
        }
        catch (Exception ex)
        {
            errorAlertWindowManager.RadAlert(result = result.Replace("'", ""), 350, 100, "Error", null);
        }
    }
    #endregion

    #region ButtonPlanningComplete_Click
    protected void ButtonPlanningComplete_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPlanningComplete_Click";

        try
        {
            Status st = new Status();

            foreach (GridDataItem item in GridViewInstruction.SelectedItems)
            {
                int outboundShipmentId = -1;
                int issueId = -1;

                if (item.GetDataKeyValue("OutboundShipmentId").ToString() == "" || item.GetDataKeyValue("OutboundShipmentId").ToString() == "-1")
                {
                    issueId = int.Parse(item.GetDataKeyValue("IssueId").ToString());
                }
                else
                {
                    outboundShipmentId = int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString());
                }

                if (st.PlanningComplete(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId))
                {
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 70))
                    {
                        Session["FromURL"] = "~/Outbound/OutboundPlanning.aspx";

                        Session["ReportName"] = "Despatch Advice";

                        ReportParameter[] RptParameters = new ReportParameter[6];

                        RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());
                        RptParameters[1] = new ReportParameter("OutboundShipmentId", outboundShipmentId.ToString());
                        RptParameters[2] = new ReportParameter("IssueId", issueId.ToString());
                        RptParameters[3] = new ReportParameter("ServerName", Session["ServerName"].ToString());
                        RptParameters[4] = new ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
                        RptParameters[5] = new ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        Response.Redirect("~/Reports/Report.aspx", true);
                    }
                    errorAlertWindowManager.RadConfirm("Planning Completed Successful.", null, 350, 100, null, "Planning");
                }
                else
                {
                    errorAlertWindowManager.RadConfirm("The order is not yet palletised.", null, 350, 100, null, "Planning");
                }
            }
            GridViewInstruction.DataBind();
        }
        catch (Exception ex)
        {
            errorAlertWindowManager.RadAlert(result = result.Replace("'", ""), 350, 100, "Error", null);
        }
    }
    #endregion

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundPlanning", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundPlanning", theErrMethod, ex);

                //MMaster.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region GridViewInstruction_PageIndexChanging
    protected void GridViewInstruction_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        theErrMethod = "GridViewInstruction_PageIndexChanging";
        #region HoldGrid
        //GridViewInstruction.DataSource =
        //    ds.GetInstructions(start,
        //                        end,
        //                        int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString()));

        //GridViewInstruction.PageIndex = e.NewPageIndex;

        //GridViewInstruction.DataBind();       
        #endregion HoldGrid
    }
    #endregion GridViewInstruction_PageIndexChanging

    #region GridViewInstruction_RowUpdating
    protected void GridViewInstruction_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        theErrMethod = "GridViewInstruction_RowUpdating";
        try
        {
            for (int i = 0; i <= e.NewValues.Values.Count - 1; i++)
            {
                switch (i)
                {

                    case 0:
                        if (e.NewValues["IssueId"] != null)
                        {
                            e.NewValues["IssueId"] = int.Parse(e.NewValues["IssueId"].ToString());//, System.Globalization.NumberStyles.Any);
                        }
                        break;
                    case 1:
                        if (e.NewValues["locationId"] != null)
                        {
                            e.NewValues["locationId"] = int.Parse(e.NewValues["locationId"].ToString());//, System.Globalization.NumberStyles.Any);
                        }

                        break;
                    case 2:
                        if (e.NewValues["priorityId"] != null)
                        {
                            e.NewValues["priorityId"] = int.Parse(e.NewValues["priorityId"].ToString());//, System.Globalization.NumberStyles.Any);
                        }

                        break;
                    case 3:
                        if (e.NewValues["deliveryDate"] != null)
                        {
                            e.NewValues["deliveryDate"] = int.Parse(e.NewValues["deliveryDate"].ToString());//, System.Globalization.NumberStyles.Any);
                        }

                        break;
                    default:
                        break;
                }
            }

            //   e.Cancel = true;


            //Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
    }
    #endregion GridViewInstruction_RowUpdating

    #region GridViewInstruction_SelectedIndexChanged
    protected void GridViewInstruction_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInstruction_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in GridViewInstruction.SelectedItems)
            {
                Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId").ToString();
                Session["IssueId"] = item.GetDataKeyValue("IssueId");

                //int selectedIndex = GridViewInstruction.SelectedItems
            }

            //Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }

    }
    #endregion GridViewInstruction_SelectedIndexChanged

    private void errorMessageHandling(string messageText, string errorText)
    {
        if (ErrorMessage != null)
            ErrorMessage(messageText, errorText);
    }

    public void CaptureResponseTime(string connectionString, Stopwatch stopWatch, string action, string keyName, int? KeyId)
    {
        try
        {
            stopWatch.Stop();
            new Planning().LogResponseTime(connectionString, action, stopWatch.Elapsed.TotalSeconds, keyName, KeyId, Convert.ToInt32(Session["OperatorId"].ToString()));
            stopWatch.Start();
        }
        catch (Exception ex)
        {

        }
    }

    private void RestartTime(Stopwatch stopWatch) {
        stopWatch.Reset();
        stopWatch.Start();
    }

    #region SavePetNames
    protected void SavePetNames()
    {
        try
        {
            {
                GridSettingsPersister settings = new GridSettingsPersister(GridViewInstruction);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), GridViewInstruction, "OutboundPlanningGridViewInstruction", (int)Session["OperatorId"]);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion SavePetNames

    #region ButtonSaveSettings_Click
    protected void ButtonSaveSettings_Click(object sender, EventArgs e)
    {
        try
        {
            SavePetNames();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            errorMessageHandling("", result);
        }
    }
    #endregion

    public void RefreshOrderGrid()
    {
        try
        {
            foreach (GridDataItem item in GridViewInstruction.SelectedItems)
            {
                Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId").ToString();
                Session["IssueId"] = item.GetDataKeyValue("IssueId");
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
        }
    }

    protected void GridViewInstruction_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = e.Item as GridDataItem;
            if (item["IsFullyPaidOrder"].Text == "False")
            {
                item["AdditionalText2"].Attributes.Add("style", "color:red;");
            }
        }

        if (Session["OperatorGroupCode"].ToString() == "CCA")
        {
            if (e.Item.IsInEditMode)
            {
                GridEditFormItem eitem = (GridEditFormItem)e.Item;

                if (e.Item is GridEditableItem)
                {
                    eitem["LocationEdit"].Visible = false;   // for making the cell invisible during editing           
                    eitem["LocationEdit"].Parent.Visible = false; // for making its label also invisible

                    eitem["AreaTypeEdit"].Visible = false;
                    eitem["AreaTypeEdit"].Parent.Visible = false;

                    eitem["PriorityEdit"].Visible = false;
                    eitem["PriorityEdit"].Parent.Visible = false;

                    eitem["RouteEdit"].Visible = false;
                    eitem["RouteEdit"].Parent.Visible = false;
                }

                //GridBoundColumn
                eitem["AddressLine1Edit"].Visible = false;
                eitem["AddressLine1Edit"].Parent.Visible = false;

                eitem["AddressLine2Edit"].Visible = false;
                eitem["AddressLine2Edit"].Parent.Visible = false;

                eitem["AddressLine3Edit"].Visible = false;
                eitem["AddressLine3Edit"].Parent.Visible = false;

                eitem["AddressLine4Edit"].Visible = false;
                eitem["AddressLine4Edit"].Parent.Visible = false;

            }
        }
    }

}
