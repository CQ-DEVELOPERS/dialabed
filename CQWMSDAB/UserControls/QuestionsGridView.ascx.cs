using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_QuestionsGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridQuestions.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridQuestions_SelectedIndexChanged
    protected void RadGridQuestions_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridQuestions.SelectedItems)
            {
                Session["QuestionsId"] = item.GetDataKeyValue("QuestionsId");
            }
        }
        catch { }
    }
    #endregion "RadGridQuestions_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
    
        RadGridQuestions.DataBind();
    }
    #endregion BindData
    
}
