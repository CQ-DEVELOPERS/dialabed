﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationUC.ascx.cs" Inherits="UserControls_Housekeeping_LocationUC" %>

<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <telerik:RadTextBox Label="Location:" ID="txtLocationSearch" runat="server" LabelWidth="75px" Width="250px" EmptyMessage="{All}"></telerik:RadTextBox>
        </td>
        <td>
            <label>Stock Take:</label>
        </td>
        <td>
            <asp:CheckBox ID="rcbStockTake" runat="server" />
        </td>
        <td>
            <label>Used:</label>
        </td>
        <td>
            <asp:CheckBox ID="rcbUsed" runat="server" />
        </td>
        <td>
            <telerik:RadButton ID="rbtnSearch" runat="server" Text="Search" OnClick="rbtnSearch_Click">
                <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
            </telerik:RadButton>
        </td>
    </tr>
</table>

<telerik:RadGrid ID="rgLocation" DataSourceID="ObjectDataSourceLocation" runat="server" AllowPaging="True" AllowSorting="True"
    AutoGenerateColumns="False" Skin="Metro" ClientSettings-Selecting-AllowRowSelect="true"
    OnSelectedIndexChanged="rgLocation_SelectedIndexChanged" PageSize="30" AllowFilteringByColumn="False"
    EmptyDataText="<%$ Resources:Default, CurrentlyNoProducts %>" AllowAutomaticUpdates="true" AllowAutomaticInserts="true" AllowAutomaticDeletes="true">
    <MasterTableView DataKeyNames="LocationId" DataSourceID="ObjectDataSourceLocation" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true" CommandItemDisplay="Top" EditMode="EditForms">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderText='<%$ Resources:Default, ButtonEdit %>'></telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn Display="false" ReadOnly="true" DataField="LocationId" HeaderText="LocationId" SortExpression="LocationId" FilterControlAltText="Filter LocationId column" UniqueName="LocationId"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn DataField="LocationTypeId" DataSourceID="ObjectdatsourceLocationType" HeaderText="LocationType" ListValueField="LocationTypeId"
                ListTextField="LocationType" SortExpression="LocationTypeId" FilterControlAltText="Filter LocationTypeId column" UniqueName="LocationTypeId">
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="Location" HeaderText="Location" SortExpression="Location" FilterControlAltText="Filter Location column" UniqueName="Location"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Ailse" HeaderText="Ailse" SortExpression="Ailse" FilterControlAltText="Filter Ailse column" UniqueName="Ailse"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Column" HeaderText="Column" SortExpression="Column" FilterControlAltText="Filter Column column" UniqueName="Column"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Level" HeaderText="Level" SortExpression="Level" FilterControlAltText="Filter Level column" UniqueName="Level"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PalletQuantity" HeaderText="PalletQuantity" SortExpression="PalletQuantity" FilterControlAltText="Filter PalletQuantity column" UniqueName="PalletQuantity"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SecurityCode" HeaderText="SecurityCode" SortExpression="SecurityCode" FilterControlAltText="Filter SecurityCode column" UniqueName="SecurityCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="RelativeValue" HeaderText="RelativeValue" SortExpression="RelativeValue" FilterControlAltText="Filter RelativeValue column" UniqueName="RelativeValue"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="NumberOfSUB" HeaderText="NumberOfSUB" SortExpression="NumberOfSUB" FilterControlAltText="Filter NumberOfSUB column" UniqueName="NumberOfSUB"></telerik:GridBoundColumn>
            <telerik:GridCheckBoxColumn DataField="StocktakeInd" HeaderText="StocktakeInd" SortExpression="StocktakeInd" FilterControlAltText="Filter StocktakeInd column" UniqueName="StocktakeInd"></telerik:GridCheckBoxColumn>
            <telerik:GridCheckBoxColumn DataField="ActiveBinning" HeaderText="ActiveBinning" SortExpression="ActiveBinning" FilterControlAltText="Filter ActiveBinning column" UniqueName="ActiveBinning"></telerik:GridCheckBoxColumn>
            <telerik:GridCheckBoxColumn DataField="ActivePicking" HeaderText="ActivePicking" SortExpression="ActivePicking" FilterControlAltText="Filter ActivePicking column" UniqueName="ActivePicking"></telerik:GridCheckBoxColumn>
            <telerik:GridCheckBoxColumn DataField="Used" HeaderText="Used" SortExpression="Used" FilterControlAltText="Filter Used column" UniqueName="Used"></telerik:GridCheckBoxColumn>
        </Columns>
    </MasterTableView>
    <ExportSettings IgnorePaging="true" ExportOnlyData="true"></ExportSettings>
</telerik:RadGrid>


<asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="StaticInfo" SelectMethod="Location_Search" UpdateMethod="Location_Edit"
    DeleteMethod="Location_Delete" InsertMethod="Location_Add">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="areaId" SessionField="AreaId" Type="Int32" />
        <asp:ControlParameter ControlID="txtLocationSearch" Name="location" Type="String" />
        <asp:ControlParameter ControlID="rcbStockTake" Name="hasStockTake" Type="Boolean" PropertyName="Checked" />
        <asp:ControlParameter ControlID="rcbUsed" Name="isUsed" Type="Boolean" PropertyName="Checked" />
        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="locationTypeId" Type="Int32" DefaultValue="1" />
        <asp:Parameter Name="location" Type="string" />
        <asp:Parameter Name="ailse" Type="string" DefaultValue="1" />
        <asp:Parameter Name="column" Type="string" DefaultValue="1" />
        <asp:Parameter Name="level" Type="string" DefaultValue="1" />
        <asp:Parameter Name="palletQuantity" Type="Int32" DefaultValue="1" />
        <asp:Parameter Name="securityCode" Type="string" DefaultValue="99" />
        <asp:Parameter Name="relativeValue" Type="Decimal" DefaultValue="1" />
        <asp:Parameter Name="numberofSub" Type="Int32" DefaultValue="1" />
        <asp:Parameter Name="stockTakeInd" Type="string" />
        <asp:Parameter Name="activeBinning" Type="string"/>
        <asp:Parameter Name="activePicking" Type="string" />
        <asp:Parameter Name="used" Type="string" />
        <asp:Parameter Name="locationId" Type="Int32" />
        <asp:SessionParameter Name="areaId" SessionField="AreaId" Type="Int32" />
    </UpdateParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="locationId" Type="Int32" />
        <asp:SessionParameter Name="areaId" SessionField="AreaId" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="locationTypeId" Type="Int32" />
        <asp:Parameter Name="location" Type="string" />
        <asp:Parameter Name="ailse" Type="string" />
        <asp:Parameter Name="column" Type="string" />
        <asp:Parameter Name="level" Type="string" />
        <asp:Parameter Name="palletQuantity" Type="Int32" />
        <asp:Parameter Name="securityCode" Type="string"/>
        <asp:Parameter Name="relativeValue" Type="Decimal" />
        <asp:Parameter Name="numberofSub" Type="Int32" />
        <asp:Parameter Name="stockTakeInd" Type="string" />
        <asp:Parameter Name="activeBinning" Type="string" />
        <asp:Parameter Name="activePicking" Type="string" />
        <asp:Parameter Name="used" Type="string" />
        <asp:Parameter Name="locationId" Type="Int32" />
        <asp:SessionParameter Name="areaId" SessionField="AreaId" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectdatsourceLocationType" runat="server" TypeName="LocationType" SelectMethod="GetLocationTypes">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceAreaList" runat="server" TypeName="StaticInfo" SelectMethod="Area_Search">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<telerik:RadButton ID="btnLocationSettings" runat="server" Text="Save Layout Settings" OnClick="btnLocationSettings_Click" />
<telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="alertWindowManager"></telerik:RadWindowManager>
