﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControls_HouseKeeping_ProductDescription : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            this.LoadSettings();
        }
        catch (Exception ex)
        {
            var message = ex.Message == null ? string.Empty : ex.Message.ToString();
            productAlertWindowManager.RadAlert(message, 350, 100, "Load Product Grid Settings", null);
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        if (Session["PrincipalId"] == null)
            try { Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString()); }
            catch { Session["PrincipalId"] = -1; }
        else
        {
            DropDownListPrincipal.SelectedValue = Session["PrincipalId"].ToString();
        }
        GridViewProduct.DataBind();
    }

    protected void lbInsert_Click(object sender, EventArgs e)
    {
        ObjectDataSourceProduct.Insert();
        GridViewProduct.ShowFooter = false;
    }

    protected void LinkButtonCancel_Click(object sender, EventArgs e)
    {
        GridViewProduct.ShowFooter = false;
    }

    protected void ObjectDataSourceProduct_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        Session["ProductId"] = int.Parse(e.ReturnValue.ToString());
    }

    protected void GridViewProduct_RowCommand(object sender, GridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "SOH":
                this.ProductStockOnHand(e.CommandArgument.ToString());
                break;
        }
    }

    protected void ObjectDataSourceProduct_Updating(object sender, ObjectDataSourceMethodEventArgs e) { }

    private void ProductStockOnHand(string productId)
    {
        try
        {

            var negativeValue = "-1";
            var zeroValue = "0";
            var RptParameters = new List<ReportParameter>()
                        {
                                new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString()),
                                new ReportParameter("WarehouseId", Session["WarehouseId"].ToString()),
                                //new ReportParameter("ProductId", productId),
                                new ReportParameter("StorageUnitId", negativeValue),
                                new ReportParameter("BatchId", negativeValue),
                                new ReportParameter("StoreLocationId", negativeValue),
                                new ReportParameter("PickLocationId", negativeValue),
                                new ReportParameter("LocationTypeId", negativeValue),
                                new ReportParameter("BatchMore", negativeValue),
                                new ReportParameter("BatchQuantity", zeroValue),
                                new ReportParameter("AllocatedMore", negativeValue),
                                new ReportParameter("AllocatedQuantity", zeroValue),
                                new ReportParameter("ReservedMore", negativeValue),
                                new ReportParameter("ReservedQuantity", zeroValue),
                                new ReportParameter("ServerName", Session["ServerName"].ToString()),
                                new ReportParameter("DatabaseName", Session["DatabaseName"].ToString()),
                                new ReportParameter("UserName", Session["UserName"].ToString())
                        }.ToArray();

            Session["FromURL"] = "~/Housekeeping/ProductMaintenance.aspx";
            Session["ReportName"] = "Stock On Hand";
            Session["ReportParameters"] = RptParameters;

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", "openReportRadWindow()", true);

        }
        catch (Exception ex)
        {
            productAlertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Stock On Hand Report", null);
        }
    }

    protected void ObjectDataSourceProduct_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
    }

    protected void GridViewProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridDataItem Item = TelerikControls.GetSelectedItem(this.GridViewProduct);
            this.SelectGridViewProduct();

            if (Session["PrincipalId"] == null)
                try { Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString()); }
                catch { Session["PrincipalId"] = -1; }
            else
            {
                DropDownListPrincipal.SelectedValue = Session["PrincipalId"].ToString();
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void SelectGridViewProduct()
    {
        if (this.GridViewProduct.MasterTableView.Items.Count > 0)
        {
            int selectedRow = this.SelectedRow();
            if (selectedRow > -1)
            {
                GridDataItem item = null;
                if (selectedRow == 0)
                {
                    item = GridViewProduct.MasterTableView.Items[selectedRow];
                    Session["ProductId"] = item.GetDataKeyValue("ProductId").ToString();
                }
                else
                {
                    item = GridViewProduct.MasterTableView.Items[selectedRow];
                    Session["ProductId"] = item.GetDataKeyValue("ProductId").ToString();
                }
                var gridRow = GridViewProduct.MasterTableView.Items[selectedRow];
                gridRow.Selected = true;
            }
        }
    }

    public int SelectedRow()
    {
        int selectedIndex = 0;
        if (GridViewProduct.MasterTableView != null && GridViewProduct.MasterTableView.Items != null && GridViewProduct.MasterTableView.Items.Count > 0)
        {
            foreach (GridDataItem item2 in GridViewProduct.SelectedItems)
            {
                selectedIndex = item2.ItemIndex;
                return selectedIndex;
            }
        }

        return selectedIndex;
    }
    protected void DropDownListPrincipal_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString());
    }
    protected void GridViewProduct_ItemDataBound(object sender, EventArgs e)
    {
    }
    protected void ButtonSaveProductDescriptionSettings_Click(object sender, EventArgs e)
    {
        this.SaveSettings();
    }

    private void SaveSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewProduct);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), GridViewProduct, "ProductDescriptionGridViewProduct", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
            productAlertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Save Product Grid Settings", null); ;
        }
    }

    private void LoadSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewProduct);
            settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), GridViewProduct, "ProductDescriptionGridViewProduct", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
            productAlertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Load Product Grid Settings", null);
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);
            this.SaveSettings();
        }
        catch (Exception ex)
        {
            productAlertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Save Settings", null);
        }
    }

    #region Export Grid
    public void ExportGrid(string type)
    {
        GridViewProduct.ExportSettings.ExportOnlyData = true;
        GridViewProduct.ExportSettings.OpenInNewWindow = true;
        switch (type)
        {
            case "CSV":
                GridViewProduct.MasterTableView.ExportToCSV();
                break;
            case "Pdf":
                GridViewProduct.MasterTableView.ExportToPdf();
                break;
            case "Excel":
                GridViewProduct.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
                GridViewProduct.MasterTableView.ExportToExcel();
                break;
            case "Word":
                GridViewProduct.MasterTableView.ExportToWord();
                break;
        }
    }
    #endregion
}