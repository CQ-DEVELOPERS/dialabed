﻿using System;
using Telerik.Web.UI;

public partial class UserControls_HouseKeeping_ProductBatch : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonSearchStorage_Click(object sender, EventArgs e)
    {
        GridViewBatchSearch.DataBind();
    }

    protected void GridViewBatchSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in GridViewBatchSearch.SelectedItems)
            {
                Session["BatchId"] = item.GetDataKeyValue("BatchId");
                Session["Batch"] = item.GetDataKeyValue("Batch");
            }

        }
        catch (Exception ex)
        {
            batchAlertWindowManager.RadAlert(ex.Message, 550, 100, "Batch select", null);
        }

    }

    protected void buttonstorageUnitBatch_Click(object sender, EventArgs e)
    {
        var result = StaticInfo.storageUnitBatch_Add(Session["ConnectionStringName"].ToString(), int.Parse(Session["StorageUnitId"].ToString()), int.Parse(Session["BatchId"].ToString()));
        if (result)
        {
            batchAlertWindowManager.RadAlert("Batch added successfully.", 350, 100, "Add batch", null);
        }
        GridViewBatch.DataBind();
    }

    protected void GridViewBatch_DeleteCommand(object sender, GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (GridViewBatch.MasterTableView.Items.Count > 0)
        {
            int storageUnitBatchId = Convert.ToInt32(GridViewBatch.MasterTableView.Items[e.Item.ItemIndex].GetDataKeyValue("StorageUnitBatchId").ToString());
            var result = new StaticInfo().storageUnitBatch_Delete(Session["ConnectionStringName"].ToString(), storageUnitBatchId);
            if (!result)
            {
                batchAlertWindowManager.RadAlert("Failed to delete a batch as it is being used somewhere else.", 550, 100, "Delete Batch", null);
            }
            else
            {
                batchAlertWindowManager.RadAlert("Batch deleted successfully.", 350, 100, "Delete Batch", null);
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            this.LoadSettings();
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonSaveGridViewBatchSettings_Click(object sender, EventArgs e)
    {
        this.SaveSettings();
    }

    private void SaveSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewBatch);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), GridViewBatch, "ProductBatchGridViewBatch", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewBatch);
            settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), GridViewBatch, "ProductBatchGridViewBatch", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    //#region Export Grid
    public void ExportGrid(string type)
    {
        GridViewBatch.ExportSettings.ExportOnlyData = true;
        GridViewBatch.ExportSettings.OpenInNewWindow = true;
        switch (type)
        {
            case "CSV":
                GridViewBatch.MasterTableView.ExportToCSV();
                break;
            case "Pdf":
                GridViewBatch.MasterTableView.ExportToPdf();
                break;
            case "Excel":
                GridViewBatch.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
                GridViewBatch.MasterTableView.ExportToExcel();
                break;
            case "Word":
                GridViewBatch.MasterTableView.ExportToWord();
                break;
        }
    }
    //#endregion
}