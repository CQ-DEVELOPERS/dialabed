﻿using System;
using System.Linq;
using Telerik.Web.UI;

public partial class UserControls_HouseKeeping_ProductSKU : System.Web.UI.UserControl
{
    private int EditModeSKUID { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void buttonaddproductsku_Click(object sender, EventArgs e)
    {
        if (addSkuAutoComplete.Entries != null && addSkuAutoComplete.Entries.Count == 0)
        {
            skuAlertWindowManager.RadAlert("Please type in a SKU to add.", 350, 100, "Add SKU", null);
        }

        if (Session["ProductId"] != null && Session["ConnectionStringName"] != null && addSkuAutoComplete.Entries != null && addSkuAutoComplete.Entries.Count > 0)
        {
            int skuId = int.Parse(addSkuAutoComplete.Entries[0].Value);
            int result = StaticInfo.sku_Add(Session["ConnectionStringName"].ToString(), skuId, int.Parse(Session["ProductId"].ToString()));
            if (result == 0)
            {
                skuAlertWindowManager.RadAlert("SKU added successfully.", 350, 100, "Add SKU", null);
            }
            GridViewSku.DataBind();
            addSkuAutoComplete.Entries.Clear();
        }
    }

    protected void GridViewSku_DeleteCommand(object sender, GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (GridViewSku.MasterTableView.Items.Count > 0)
        {
            int storageUnitId = Convert.ToInt32(GridViewSku.MasterTableView.Items[e.Item.ItemIndex].GetDataKeyValue("StorageUnitId").ToString());
            var result = StaticInfo.sku_Delete(Session["ConnectionStringName"].ToString(), storageUnitId);
            if (result)
            {
                skuAlertWindowManager.RadAlert("SKU deleted successfully.", 350, 100, "Delete SKU", null);
                GridViewSku.DataBind();
            }
            else
            {
                skuAlertWindowManager.RadAlert("Failed to delete SKU as it is linked to a batch.", 450, 100, "Delete SKU", null);
            }
        }
    }

    protected void GridViewSku_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridDataItem Item = TelerikControls.GetSelectedItem(GridViewSku);
            if (Item != null)
            {
                Session["StorageUnitId"] = Converter.String2Int(Item.GetDataKeyValue("StorageUnitId"));
            }
        }
        catch (Exception ex)
        {
            skuAlertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "SKU Select", null);
        }
    }

    protected void ObjectDataSourceSku_Updated(object sender, System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs e)
    {

    }

    protected void ObjectDataSourceSku_Updating(object sender, System.Web.UI.WebControls.ObjectDataSourceMethodEventArgs e)
    {
        GridEditFormItem editItem = (GridViewSku.EditItems[0] as GridDataItem).EditFormItem;
        int storageUnitId = e.InputParameters["storageunitid"] != null ? int.Parse(e.InputParameters["storageunitid"].ToString()) : 0;
        int skuid = this.EditModeSKUID > 0 ? this.EditModeSKUID : -1;
        string packingCategory = Convert.ToString(e.InputParameters["packingCategory"]);
        string productCategory = Convert.ToString(editItem.GetDataKeyValue("productCategory"));
        string pickEmpty = bool.Parse(e.InputParameters["pickEmpty"].ToString()) ? "True" : "False";
        int stackingCategory = e.InputParameters["stackingCategory"] != null ? int.Parse(e.InputParameters["stackingCategory"].ToString()) : 0;
        int movementCategory = e.InputParameters["movementCategory"] != null ? int.Parse(e.InputParameters["movementCategory"].ToString()) : 0;
        int valueCategory = e.InputParameters["valueCategory"] != null ? int.Parse(e.InputParameters["valueCategory"].ToString()) : 0;
        int storingCategory = e.InputParameters["storingCategory"] != null ? int.Parse(e.InputParameters["storingCategory"].ToString()) : 0;
        int pickPartPallet = e.InputParameters["pickPartPallet"] != null ? int.Parse(e.InputParameters["pickPartPallet"].ToString()) : 0;
        decimal minimumQuantity = e.InputParameters["minimumQuantity"] != null ? decimal.Parse(e.InputParameters["minimumQuantity"].ToString()) : 0;
        decimal reorderQuantity = e.InputParameters["reorderQuantity"] != null ? decimal.Parse(e.InputParameters["reorderQuantity"].ToString()) : 0;
        decimal maximumQuantity = e.InputParameters["maximumQuantity"] != null ? decimal.Parse(e.InputParameters["maximumQuantity"].ToString()) : 0;
        string defaultQC = bool.Parse(e.InputParameters["DefaultQC"].ToString()) ? "True" : "False";
        decimal unitPrice = e.InputParameters["UnitPrice"] != null ? decimal.Parse(e.InputParameters["UnitPrice"].ToString()) : 0;
        string size = Convert.ToString(e.InputParameters["Size"]);
        string colour = Convert.ToString(e.InputParameters["Colour"]);
        string style = Convert.ToString(e.InputParameters["Style"]);
        string serialTracked = Convert.ToString(e.InputParameters["SerialTracked"]);

        string lotAttributeRule = "";
        if (storageUnitId > 0 && skuid > 0)
        {
            var result = StaticInfo.sku_Update(Session["ConnectionStringName"].ToString(),
                storageUnitId,
                skuid,
                packingCategory,
                productCategory,
                pickEmpty,
                stackingCategory,
                movementCategory,
                valueCategory,
                storingCategory,
                pickPartPallet,
                minimumQuantity,
                reorderQuantity,
                maximumQuantity,
                defaultQC,
                unitPrice,
                size,
                colour,
                style,
                lotAttributeRule,
                serialTracked
                );
            if (result)
            {
                skuAlertWindowManager.RadAlert("SKU updated successfully.", 350, 100, "Update SKU", null);
            }
            else
            {
                skuAlertWindowManager.RadAlert("Failed to updated SKU.", 350, 100, "Update SKU", null);
            }
        }
    }

    protected void GridViewSku_RowCommand(object sender, GridCommandEventArgs e)
    {
        var item = e.Item;
        if (e.Item.IsInEditMode)
        {
            GridEditableItem editItem = e.Item as GridEditableItem;

            if (e.Item is GridEditFormInsertItem || e.Item is GridDataInsertItem)
            {
                // Insert mode
                // In insert mode, the row is new, there is no data for the AutoCompleteBox to populate
            }
            else
            {
                // Edit mode
                // get reference to RadAutoCompleteBox
                RadAutoCompleteBox racb = editItem["EditAutoComplete"].FindControl("editSkuAutoComplete") as RadAutoCompleteBox;
                if (racb.Entries != null && racb.Entries.Count > 0)
                {
                    this.EditModeSKUID = int.Parse(racb.Entries[0].Value);

                    Session["StorageUnitId"] = editItem.GetDataKeyValue("StorageUnitId").ToString();
                }
                else
                {
                    this.EditModeSKUID = (int)((((System.Data.DataRowView)editItem.DataItem).Row).ItemArray[1]);
                }
            }
        }

        if (item != null && GridViewSku.EditItems.Count == 0)
        {
            Session["StorageUnitId"] = ((GridDataItem)item).GetDataKeyValue("StorageUnitId").ToString();
            var gridRow = GridViewSku.MasterTableView.Items[item.DataSetIndex];
            gridRow.Selected = true;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            this.LoadSettings();
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonSaveProductSKUSettings_Click(object sender, EventArgs e)
    {
        this.SaveSettings();
    }

    private void SaveSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewSku);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), GridViewSku, "ProductSKUGridViewSku", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewSku);
            settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), GridViewSku, "ProductSKUGridViewSku", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    #region Export Grid
    public void ExportGrid(string type)
    {
        GridViewSku.ExportSettings.ExportOnlyData = true;
        GridViewSku.ExportSettings.OpenInNewWindow = true;
        switch (type)
        {
            case "CSV":
                GridViewSku.MasterTableView.ExportToCSV();
                break;
            case "Pdf":
                GridViewSku.MasterTableView.ExportToPdf();
                break;
            case "Excel":
                GridViewSku.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
                GridViewSku.MasterTableView.ExportToExcel();
                break;
            case "Word":
                GridViewSku.MasterTableView.ExportToWord();
                break;
        }
    }
    #endregion
}