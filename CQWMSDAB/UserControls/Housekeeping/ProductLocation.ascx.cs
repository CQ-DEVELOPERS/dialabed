﻿using System;
using Telerik.Web.UI;

public partial class UserControls_HouseKeeping_ProductLocation : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //this.location1.GridViewLocationSearch_DataBind();
    }

    protected void addStorageLocation_Click(object sender, EventArgs e)
    {
        var result = StaticInfo.storageUnitLocation_Add(Session["ConnectionStringName"].ToString(), int.Parse(Session["StorageUnitId"].ToString()), int.Parse(Session["ParameterLocationId"].ToString()));
        if (result)
        {
            locationAlertWindowManager.RadAlert("Location added successfully.", 350, 100, "Add location", null);
        }
        ObjectDataSourceStorageLocation.DataBind();
        GridViewLocation.DataBind();
    }

    protected void GridViewLocation_DeleteCommand(object sender, GridCommandEventArgs e)
    {

        GridDataItem dataItem = e.Item as GridDataItem;
        if (GridViewLocation.MasterTableView.Items.Count > 0)
        {
            int locationId = Convert.ToInt32(GridViewLocation.MasterTableView.Items[e.Item.ItemIndex].GetDataKeyValue("LocationId").ToString());
            int storageUnitId = Convert.ToInt32(Session["StorageUnitId"].ToString());

            var result = StaticInfo.storageUnitLocation_Delete(Session["ConnectionStringName"].ToString(), storageUnitId, locationId);
            if (!result)
            {
                locationAlertWindowManager.RadAlert("Failed to delete location as it is used somewhere.", 450, 100, "Delete location", null);
            }
            else
            {
                locationAlertWindowManager.RadAlert("Location deleted successfully.", 350, 100, "Delete Location", null);
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            this.LoadSettings();
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonSaveGridViewLocationSettings_Click(object sender, EventArgs e)
    {
        this.SaveSettings();
    }

    private void SaveSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewLocation);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), GridViewLocation, "ProductLocationGridViewLocation", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewLocation);
            settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), GridViewLocation, "ProductLocationGridViewLocation", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    #region Export Grid
    public void ExportGrid(string type)
    {
        GridViewLocation.ExportSettings.ExportOnlyData = true;
        GridViewLocation.ExportSettings.OpenInNewWindow = true;
        switch (type)
        {
            case "CSV":
                GridViewLocation.MasterTableView.ExportToCSV();
                break;
            case "Pdf":
                GridViewLocation.MasterTableView.ExportToPdf();
                break;
            case "Excel":
                GridViewLocation.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
                GridViewLocation.MasterTableView.ExportToExcel();
                break;
            case "Word":
                GridViewLocation.MasterTableView.ExportToWord();
                break;
        }
    }
    #endregion
}