﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductSKU.ascx.cs" Inherits="UserControls_HouseKeeping_ProductSKU" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<div style="margin-left: 10px" id="skus">
    <asp:UpdatePanel ID="updatePanel_skus" runat="server" EnableViewState="false">
        <ContentTemplate>
            <br />
            <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="addSkuAutoComplete" Label="SKU" Font-Size="Medium" Font-Names="Arial" MaxResultCount="50"
                DataSourceID="ObjectDataSourceSkusList" DataTextField="SKU" DataValueField="SKUId" InputType="Text" Width="200px" DropDownWidth="200px" DropDownHeight="200px" TextSettings-SelectionMode="Single">
            </telerik:RadAutoCompleteBox>
            <br />
            <br />
            <telerik:RadButton ID="buttonaddproductsku" runat="server" Text="<%$ Resources:Default, AddSKU%>"
                OnClick="buttonaddproductsku_Click" />
            <br />
            <br />

            <telerik:RadGrid ID="GridViewSku" DataSourceID="ObjectDataSourceSku"
                runat="server" AllowPaging="True" AllowSorting="True" PageSize="30" AutoGenerateColumns="False" Skin="Metro" ClientSettings-Selecting-AllowRowSelect="true"
                AllowAutomaticUpdates="true" AllowAutomaticInserts="true" OnItemCommand="GridViewSku_RowCommand" OnDeleteCommand="GridViewSku_DeleteCommand" OnSelectedIndexChanged="GridViewSku_SelectedIndexChanged">
                <MasterTableView DataKeyNames="StorageUnitId" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderText='<%$ Resources:Default, ButtonEdit %>'></telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn UniqueName="SKUDelete" ButtonType="ImageButton" HeaderText='<%$ Resources:Default, ButtonDelete %>'
                            CommandName="Delete" />
                        <telerik:GridTemplateColumn DataField="SKUCode" HeaderText='<%$ Resources:Default, SKUCode %>' UniqueName="EditAutoComplete">
                            <ItemStyle Wrap="False"></ItemStyle>
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="lblSKUCODE" runat="server" Text='<%# Bind("SKUCode") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadAutoCompleteBox RenderMode="Lightweight" runat="server" ID="editSkuAutoComplete" Font-Size="Medium" Font-Names="Arial" MaxResultCount="50"
                                    DataSourceID="ObjectDataSourceSkusList" DataTextField="SKUCode" DataValueField="SKUId" InputType="Text" Width="200px" DropDownWidth="200px" DropDownHeight="200px" TextSettings-SelectionMode="Single">
                                </telerik:RadAutoCompleteBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, SKU %>" DataField="SKU"
                            SortExpression="SKU" ReadOnly="true">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, ProductCode %>" DataField="ProductCode"
                            SortExpression="ProductCode" ReadOnly="true" />
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, Product %>" DataField="Product"
                            SortExpression="Product" ReadOnly="true">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PackingCategory" HeaderText='<%$ Resources:Default, PackingCategory %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ProductCategory" HeaderText='<%$ Resources:Default, ProductCategory %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="PickEmpty" HeaderText='<%$ Resources:Default, PickEmpty %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelPickEmpty" runat="server" Text='<%# Bind("PickEmpty") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkPickEmpty" runat="server" Checked='<%# Bind("PickEmpty") %>'></asp:CheckBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="StackingCategory" HeaderText='<%$ Resources:Default, StackingCategory %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MovementCategory" HeaderText='<%$ Resources:Default, MovementCategory %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ValueCategory" HeaderText='<%$ Resources:Default, ValueCategory %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StoringCategory" HeaderText='<%$ Resources:Default, StoringCategory %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PickPartPallet" HeaderText='<%$ Resources:Default, PickPartPallet %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MinimumQuantity" HeaderText='<%$ Resources:Default, MinimumQuantity %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ReorderQuantity" HeaderText='<%$ Resources:Default, ReorderQuantity  %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MaximumQuantity" HeaderText='<%$ Resources:Default, MaximumQuantity %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="DefaultQC" HeaderText='<%$ Resources:Default, QAIndicator %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelQualityAssuranceIndicator" runat="server" Text='<%# Bind("DefaultQC") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkQualityAssuranceIndicator" runat="server" Checked='<%# Bind("DefaultQC") %>'></asp:CheckBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="UnitPrice" HeaderText='<%$ Resources:Default, UnitPrice %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Size" HeaderText='<%$ Resources:Default, Size %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Colour" HeaderText='<%$ Resources:Default, Colour %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Style" HeaderText='<%$ Resources:Default, Style %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default, Batch %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="labelLotAttributeRuleHeader" runat="server" Text='<%# Bind("LotAttributeRule") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadDropDownList ID="dropdownlistLotAttributeRule" runat="server" DataSourceID="ObjectDataSourceLotAttributeRule"
                                    SelectedValue='<%# Bind("LotAttributeRule") %>'>
                                </telerik:RadDropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="SerialTracked" HeaderText='<%$ Resources:Default, SerialTracked %>'>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="label1" runat="server" Text='<%# Bind("SerialTracked") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkSerialTracked" runat="server" Checked='<%# Bind("SerialTracked") %>'></asp:CheckBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
                    <Selecting AllowRowSelect="True" /> 
                </ClientSettings>
            </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourceSku" runat="server" TypeName="StaticInfo"
                SelectMethod="sku_Search" DeleteMethod="sku_Delete" UpdateMethod="sku_Update"
                InsertMethod="sku_Add" OnUpdated="ObjectDataSourceSku_Updated" OnUpdating="ObjectDataSourceSku_Updating">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="productId" SessionField="ProductId" Type="Int32" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="storageUnitId" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="storageUnitId" />
                    <asp:Parameter Name="skuId" />
                    <asp:Parameter Name="packingCategory" Type="String" />
                    <asp:Parameter Name="productCategory" Type="String" />
                    <asp:Parameter Name="pickEmpty" Type="String" />
                    <asp:Parameter Name="stackingCategory" Type="Int32" />
                    <asp:Parameter Name="movementCategory" Type="Int32" />
                    <asp:Parameter Name="valueCategory" Type="Int32" />
                    <asp:Parameter Name="storingCategory" Type="Int32" />
                    <asp:Parameter Name="pickPartPallet" Type="Int32" />
                    <asp:Parameter Name="minimumQuantity" Type="Decimal" />
                    <asp:Parameter Name="reorderQuantity" Type="Decimal" />
                    <asp:Parameter Name="maximumQuantity" Type="Decimal" />
                    <asp:Parameter Name="DefaultQC" Type="String" />
                    <asp:Parameter Name="UnitPrice" Type="Decimal" />
                    <asp:Parameter Name="Size" Type="String" />
                    <asp:Parameter Name="Colour" Type="String" />
                    <asp:Parameter Name="Style" Type="String" />
                    <asp:Parameter Name="SerialTracked" Type="String" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="storageUnitId" />
                    <asp:Parameter Name="skuId" />
                    <asp:Parameter Name="packingCategory" Type="String" />
                    <asp:Parameter Name="productCategory" Type="String" />
                    <asp:Parameter Name="pickEmpty" Type="String" />
                    <asp:Parameter Name="stackingCategory" Type="Int32" />
                    <asp:Parameter Name="movementCategory" Type="Int32" />
                    <asp:Parameter Name="valueCategory" Type="Int32" />
                    <asp:Parameter Name="storingCategory" Type="Int32" />
                    <asp:Parameter Name="pickPartPallet" Type="Int32" />
                    <asp:Parameter Name="minimumQuantity" Type="Decimal" />
                    <asp:Parameter Name="reorderQuantity" Type="Decimal" />
                    <asp:Parameter Name="maximumQuantity" Type="Decimal" />
                    <asp:Parameter Name="DefaultQC" Type="String" />
                    <asp:Parameter Name="UnitPrice" Type="Decimal" />
                    <asp:Parameter Name="Size" Type="String" />
                    <asp:Parameter Name="Colour" Type="String" />
                    <asp:Parameter Name="Style" Type="String" />
                    <asp:Parameter Name="SerialTracked" Type="String" />
                </InsertParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceSkusList" runat="server" TypeName="StaticInfo"
                SelectMethod="GetSku">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceLotAttributeRule" runat="server" TypeName="StaticInfo"
                SelectMethod="GetLotAttributeRules"></asp:ObjectDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
    <telerik:RadCodeBlock runat="server">
        <style type="text/css">
            .racTokenList {
                color: #404040;
                background-color: White;
                border-color: #000000;
                border-width: 1pt;
                border-style: Solid;
                font-family: Segoe UI;
                font-size: 0.8em;
                height: 15px;
            }
        </style>
    </telerik:RadCodeBlock>
    <br />
    <telerik:RadButton ID="ButtonSaveProductSKUSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveProductSKUSettings_Click" />
    <telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="skuAlertWindowManager"></telerik:RadWindowManager>
    <br />
    <br />
</div>