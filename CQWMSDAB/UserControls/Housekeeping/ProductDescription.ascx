﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductDescription.ascx.cs" Inherits="UserControls_HouseKeeping_ProductDescription" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:UpdatePanel ID="updatePanel_Products" runat="server" EnableViewState="False">
    <ContentTemplate>
        <asp:Panel ID="PanelSearch" runat="server" Width="450px" BackColor="#EFEFEC" DefaultButton="ButtonSearch">
            <table>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="TextBoxProductCode" runat="server" Label='<%$ Resources:Default,ProductCode %>' Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="TextBoxProduct" runat="server" Label='<%$ Resources:Default,Product %>' Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="TextBoxBarcode" runat="server" Label='<%$ Resources:Default,Barcode %>' Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="TextBoxSKUCode" runat="server" Label='<%$ Resources:Default, SKUCode%>' Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="TextBoxSKU" runat="server" Label='<%$ Resources:Default, SKU%>' Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal %>"></asp:Label>
                    </td>
                    <td>
                        <telerik:RadDropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                            DataTextField="Principal" DataValueField="PrincipalId" Width="150px" OnSelectedIndexChanged="DropDownListPrincipal_SelectedIndexChanged" autopostback ="true">
                        </telerik:RadDropDownList>
                        <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                            SelectMethod="GetPrincipalParameter">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right">
                        <telerik:RadButton ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search %>" OnClick="ButtonSearch_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
            TargetControlID="PanelSearch" Radius="10" Color="239, 239, 236" BorderColor="64, 64, 64"
            Enabled="True" />
        <br />

        <telerik:RadGrid ID="GridViewProduct" DataSourceID="ObjectDataSourceProduct" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" Skin="Metro" ClientSettings-Selecting-AllowRowSelect="true"
            OnSelectedIndexChanged="GridViewProduct_SelectedIndexChanged" PageSize="10" AllowFilteringByColumn="False"
            EmptyDataText="<%$ Resources:Default, CurrentlyNoProducts %>" OnItemCommand="GridViewProduct_RowCommand" AllowAutomaticUpdates="true" AllowAutomaticInserts="true" AllowAutomaticDeletes="true"
            PagerStyle-AlwaysVisible="true"
            >
            <MasterTableView DataKeyNames="ProductId" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true" CommandItemDisplay="Top" EditMode="EditForms">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderText='<%$ Resources:Default, ButtonEdit %>'></telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="ProductCode" HeaderText='<%$ Resources:Default, ProductCode %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default, Status %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <div align="center">
                                <asp:Label ID="LabelStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadDropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceStatus"
                                DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'>
                            </telerik:RadDropDownList>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Product" HeaderText='<%$ Resources:Default, Product %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="BarCode" HeaderText='<%$ Resources:Default, Barcode %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MinimumQuantity" HeaderText='<%$ Resources:Default, MinimumQuantity %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReorderQuantity" HeaderText='<%$ Resources:Default, ReorderQuantity %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MaximumQuantity" HeaderText='<%$ Resources:Default, MaximumQuantity %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CuringPeriodDays" HeaderText='<%$ Resources:Default, CuringPeriod %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShelfLifeDays" HeaderText='<%$ Resources:Default, ShelfLife %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn ReadOnly="False" DataField="qualityAssuranceIndicator" HeaderText='<%$ Resources:Default, QAIndicator %>' SortExpression="qualityAssuranceIndicator" FilterControlAltText="Filter qualityAssuranceIndicator column" UniqueName="qualityAssuranceIndicator"></telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="ProductType" HeaderText='<%$ Resources:Default, ProductType %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OverReceipt" HeaderText='<%$ Resources:Default, OverReceipt %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HostId" HeaderText='<%$ Resources:Default, HostId %>' />
                    <telerik:GridBoundColumn DataField="RetentionSamples" HeaderText='<%$ Resources:Default, RetentionSamples %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Samples" HeaderText='<%$ Resources:Default, Samples %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AssaySamples" HeaderText='<%$ Resources:Default, AssaySamples %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Category" HeaderText='<%$ Resources:Default, Category %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ParentProductCode" HeaderText='<%$ Resources:Default, ParentProductCode %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default, DangerousGoods %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <div align="center">
                                <asp:Label ID="LabelDangerousGoods" runat="server" Text='<%# Bind("DangerousGoods") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadDropDownList ID="DropDownListDangerousGoodsId" runat="server" DataSourceID="ObjectDataSourceDangerousGoods"
                                DataTextField="DangerousGoods" DataValueField="DangerousGoodsId" SelectedValue='<%# Bind("DangerousGoodsId") %>'>
                            </telerik:RadDropDownList>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default, Principal %>'>
                        <ItemStyle Wrap="False"></ItemStyle>
                        <ItemTemplate>
                            <div align="center">
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Principal") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadDropDownList ID="DropDownListPrincipalId" runat="server" DataSourceID="ObjectDataSourcePrincipal2"
                                DataTextField="Principal" DataValueField="PrincipalId" SelectedValue='<%# Bind("PrincipalId") %>'>
                            </telerik:RadDropDownList>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,StockOnHand %>'>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButtonSOH" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ProductId")%>' CommandName="SOH"
                                Text="<%$ Resources:Default, StockOnHand%>" SkinID="linkButtonBlack"></asp:LinkButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
            </ClientSettings>
        </telerik:RadGrid>
        <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="StaticInfo" EnablePaging="True"
            SelectMethod="product_Search" UpdateMethod="product_Update" SelectCountMethod="SelectProductsCount"
            MaximumRowsParameterName="PageSize" StartRowIndexParameterName="StartRowIndex" InsertMethod="product_Add"
            OnInserting="ObjectDataSourceProduct_Inserting" OnInserted="ObjectDataSourceProduct_Inserted" OnUpdating="ObjectDataSourceProduct_Updating">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                    Type="String" />
                <asp:ControlParameter ControlID="TextBoxProductCode" PropertyName="Text" Name="productCode"
                    Type="String" />
                <asp:ControlParameter ControlID="TextBoxProduct" PropertyName="Text" Name="product"
                    Type="String" />
                <asp:ControlParameter ControlID="TextBoxSKUCode" PropertyName="Text" Name="SKUCode"
                    Type="String" />
                <asp:ControlParameter ControlID="TextBoxSKU" PropertyName="Text" Name="SKU"
                    Type="String" />
                <asp:ControlParameter ControlID="TextBoxBarcode" PropertyName="Text" Name="barcode"
                    Type="String" />                
                <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId"
                    Type="String" />
            </SelectParameters>
            <UpdateParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                <asp:Parameter Name="statusId" Type="Int32" />
                <asp:Parameter Name="product" Type="String" />
                <asp:Parameter Name="productCode" Type="String" />
                <asp:Parameter Name="barcode" Type="String" />
                <asp:Parameter Name="minimumQuantity" Type="Decimal" />
                <asp:Parameter Name="reorderQuantity" Type="Decimal" />
                <asp:Parameter Name="maximumQuantity" Type="Decimal" />
                <asp:Parameter Name="curingPeriodDays" Type="Int32" />
                <asp:Parameter Name="shelfLifeDays" Type="Int32" />
                <asp:Parameter Name="qualityAssuranceIndicator" Type="String" />
                <asp:Parameter Name="productType" Type="String" />
                <asp:Parameter Name="overReceipt" Type="Decimal" />
                <asp:Parameter Name="hostId" Type="String" />
                <asp:Parameter Name="retentionSamples" Type="Decimal" />
                <asp:Parameter Name="samples" Type="Decimal" />
                <asp:Parameter Name="assaySamples" Type="Decimal" />
                <asp:Parameter Name="category" Type="String" />
                <asp:Parameter Name="parentProductCode" Type="String" />
                <asp:Parameter Name="dangerousGoodsId" Type="Int32" />
                <asp:Parameter Name="principalId" Type="Int32" />
                <asp:Parameter Name="productId" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                    Type="String" />
                <asp:Parameter Name="statusId" Type="Int32" />
                <asp:Parameter Name="productCode" Type="String" />
                <asp:Parameter Name="product" Type="String" />
                <asp:Parameter Name="barcode" Type="String" />
                <asp:Parameter Name="minimumQuantity" Type="Decimal" DefaultValue="0" />
                <asp:Parameter Name="reorderQuantity" Type="Decimal" DefaultValue="0" />
                <asp:Parameter Name="maximumQuantity" Type="Decimal" DefaultValue="0" />
                <asp:Parameter Name="curingPeriodDays" Type="Int32" DefaultValue="0" />
                <asp:Parameter Name="shelfLifeDays" Type="Int32" DefaultValue="0" />
                <asp:Parameter Name="qualityAssuranceIndicator" Type="String" />
                <asp:Parameter Name="productType" Type="String" />
                <asp:Parameter Name="overReceipt" Type="Decimal" DefaultValue="0" />
                <asp:Parameter Name="hostId" Type="String" />
                <asp:Parameter Name="retentionSamples" Type="Decimal" DefaultValue="0" />
                <asp:Parameter Name="samples" Type="Decimal" DefaultValue="0" />
                <asp:Parameter Name="assaySamples" Type="Decimal" DefaultValue="0" />
                <asp:Parameter Name="category" Type="String" />
                <asp:Parameter Name="parentProductCode" Type="String" />
                <asp:Parameter Name="dangerousGoodsId" Type="Int32" DefaultValue="0" />
                <asp:Parameter Name="principalId" Type="Int32" DefaultValue="-1" />
            </InsertParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
            SelectMethod="GetStatus">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                    Type="String" />
                <asp:Parameter Name="type" DefaultValue="P" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSourceDangerousGoods" runat="server" TypeName="DangerousGoods"
            SelectMethod="GetDangerousGoodsId">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSourcePrincipal2" runat="server" TypeName="Principal"
            SelectMethod="GetPrincipalParameter">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </ContentTemplate>
</asp:UpdatePanel>
<telerik:RadWindowManager RenderMode="Lightweight" ID="RadWindowManager2" runat="server" EnableShadow="true" Style="z-index: 1000001">
    <Windows>
        <telerik:RadWindow ID="ReportRadWindow" Title="Stock On Hand" runat="server" KeepInScreenBounds="true" Localization-Cancel="" NavigateUrl="~/Reports/Report.aspx"
            Modal="true" OffsetElementID="main" ReloadOnShow="true" ShowContentDuringLoad="false" VisibleStatusbar="false">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
<telerik:RadCodeBlock runat="server">
    <script type="text/javascript">
        function openReportRadWindow() {
            var radwindow = $find("<%=ReportRadWindow.ClientID %>");
            if (radwindow != null) {
                radwindow.show();
                radwindow.maximize();
            }
        }
    </script>
</telerik:RadCodeBlock>
<br />
<telerik:RadButton ID="ButtonSaveProductDescriptionSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveProductDescriptionSettings_Click" />
<telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="productAlertWindowManager"></telerik:RadWindowManager>
<br />
<br />