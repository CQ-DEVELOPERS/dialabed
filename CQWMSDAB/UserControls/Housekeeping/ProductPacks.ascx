﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductPacks.ascx.cs" Inherits="UserControls_HouseKeeping_ProductPacks" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div style="margin-left: 10px" id="packs">
    <br />
    <asp:UpdatePanel ID="updatePanel_packs" runat="server" EnableViewState="False">
        <ContentTemplate>
            <telerik:RadGrid ID="GridViewPacks" DataSourceID="ObjectDataSourcePacks" DataKeyNames="PackId" Skin="Metro" Width="800px"
                runat="server" AllowPaging="True" AllowSorting="True" PageSize="30" AutoGenerateColumns="False" OnDeleteCommand="GridViewPacks_DeleteCommand" AllowAutomaticInserts="true" AllowAutomaticUpdates="true">
                <MasterTableView DataKeyNames="PackId" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true" CommandItemDisplay="Top" EditMode="EditForms">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderText='<%$ Resources:Default, ButtonEdit %>'></telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn UniqueName="PackDelete" ButtonType="ImageButton"
                            CommandName="Delete" HeaderText='<%$ Resources:Default, ButtonDelete %>' />
                        <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, PackType%>" SortExpression="PackType">
                            <EditItemTemplate>
                                <telerik:RadDropDownList ID="DropDownListPackType" runat="server" DataSourceID="ObjectDataSourcePackType"
                                    DataTextField="PackType" DataValueField="PackTypeId" SelectedValue='<%# Bind("PackTypeId") %>'>
                                </telerik:RadDropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelPackType" runat="server" Text='<%# Bind("PackType") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, Quantity %>" DataField="Quantity"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, Barcode %>" DataField="Barcode" InsertVisiblityMode="AlwaysHidden"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, Length %>" DataField="Length" InsertVisiblityMode="AlwaysHidden"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, Width %>" DataField="Width" InsertVisiblityMode="AlwaysHidden"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, Height %>" DataField="Height" InsertVisiblityMode="AlwaysHidden"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, Volume %>" DataField="Volume" InsertVisiblityMode="AlwaysHidden"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, WeightDetails %>" DataField="Weight" InsertVisiblityMode="AlwaysHidden"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, NettWeight %>" DataField="NettWeight" InsertVisiblityMode="AlwaysHidden"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, TareWeight %>" DataField="TareWeight" InsertVisiblityMode="AlwaysHidden"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <br />
            <asp:ObjectDataSource ID="ObjectDataSourcePacks" runat="server" TypeName="StaticInfo"
                SelectMethod="pack_Search" UpdateMethod="pack_Update" InsertMethod="pack_Add"
                OnInserted="ObjectDataSourcePacks_Inserted" OnInserting="ObjectDataSourcePacks_Inserting">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                        DefaultValue="-1" />
                    <asp:SessionParameter Name="wareHouseId" SessionField="WareHouseId" Type="int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32" />
                    <asp:Parameter Name="packTypeId" Type="Int32" />
                    <asp:Parameter Name="quantity" Type="Decimal" />
                    <asp:Parameter Name="barcode" Type="String" />
                    <asp:Parameter Name="length" Type="Decimal" />
                    <asp:Parameter Name="width" Type="Decimal" />
                    <asp:Parameter Name="height" Type="Decimal" />
                    <asp:Parameter Name="volume" Type="Decimal" />
                    <asp:Parameter Name="weight" Type="Decimal" />
                    <asp:Parameter Name="nettWeight" Type="Decimal" />
                    <asp:Parameter Name="tareWeight" Type="Decimal" />
                    <asp:Parameter Name="packId" Type="Int32" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:Parameter Name="packTypeId" Type="Int32" />
                    <asp:Parameter Name="quantity" Type="Decimal" />
                </InsertParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="packId" Type="Int32" />
                </DeleteParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourcePackType" runat="server" TypeName="StaticInfo"
                SelectMethod="GetPackType">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
    <telerik:RadButton ID="ButtonSaveProductPacksSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveProductPacksSettings_Click" />
    <telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="packAlertWindowManager"></telerik:RadWindowManager>
    <br />
    <br />
</div>