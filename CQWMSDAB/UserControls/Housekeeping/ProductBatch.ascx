﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductBatch.ascx.cs" Inherits="UserControls_HouseKeeping_ProductBatch" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Common/BatchSearch.ascx" TagName="Batch" TagPrefix="uc1" %>


<div style="margin-left: 10px" id="batch">
    <br />
    <asp:UpdatePanel ID="updatePanel_batchesearch" runat="server" EnableViewState="False">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="TextBoxBatch" runat="server" Label="<%$ Resources:Default, Batch%>" Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="TextBoxChangeLevel" runat="server" Label="<%$ Resources:Default, ChangeLevel%>" Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td></td>
                    <td>
                        <telerik:RadButton ID="ButtonSearchStorage" runat="server" Text="<%$ Resources:Default, Search%>"
                            OnClick="ButtonSearchStorage_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <telerik:RadGrid ID="GridViewBatchSearch" runat="server" AllowPaging="True" ClientSettings-Selecting-AllowRowSelect="true" AutoGenerateColumns="False" Skin="Metro" Width="400px"
                AutoGenerateSelectButton="True" DataSourceID="ObjectDataSourceBatchStorage" DataKeyNames="BatchId,Batch"
                OnSelectedIndexChanged="GridViewBatchSearch_OnSelectedIndexChanged" PageSize="10">
                <MasterTableView DataKeyNames="BatchId,Batch" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridBoundColumn DataField="Batch" HeaderText="<%$ Resources:Default, Batch%>" />
                        <telerik:GridBoundColumn DataField="ExpiryDate" HeaderText="<%$ Resources:Default, ExpiryDate%>" />
                        <telerik:GridBoundColumn DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate%>" />
                    </Columns>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid><br />
            <asp:ObjectDataSource ID="ObjectDataSourceBatchStorage" runat="server" TypeName="StaticInfo"
                EnablePaging="true" MaximumRowsParameterName="PageSize" StartRowIndexParameterName="StartRow"
                SelectMethod="SearchBatchesByStorageUnit" SelectCountMethod="CountBatchesByStorageUnit">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:ControlParameter Name="batch" ControlID="TextBoxBatch" Type="String" />
                    <asp:ControlParameter Name="eCLNumber" ControlID="TextBoxChangeLevel" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <telerik:RadButton ID="buttonstorageUnitBatch" runat="server" Text="<%$ Resources:Default, AddBatch%>"
                OnClick="buttonstorageUnitBatch_Click" />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="updatePanel_batches" runat="server" EnableViewState="False">
        <ContentTemplate>
            <telerik:RadGrid ID="GridViewBatch" DataSourceID="ObjectDataSourceBatch" DataKeyNames="StorageUnitBatchId" Skin="Metro" Width="400px"
                runat="server" AllowPaging="True" AllowSorting="True" PageSize="30" AutoGenerateColumns="False" OnDeleteCommand="GridViewBatch_DeleteCommand">
                <MasterTableView DataKeyNames="StorageUnitBatchId" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true" AllowAutomaticDeletes="true">
                    <Columns>
                        <telerik:GridButtonColumn UniqueName="BatchDelete" ButtonType="ImageButton" ConfirmDialogType="RadWindow"
                            CommandName="Delete" HeaderText='<%$ Resources:Default, ButtonDelete %>'/>
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, StorageUnitBatch%>" DataField="StorageUnitBatchId" />
                        <telerik:GridBoundColumn HeaderText="<%$ Resources:Default, Batch%>" DataField="Batch" />
                    </Columns>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="StaticInfo"
                SelectMethod="storageUnitBatch_Search" DeleteMethod="storageUnitBatch_Delete">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                        DefaultValue="-1" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="storageUnitBatchId" Type="Int32" />
                </DeleteParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <telerik:RadButton ID="ButtonSaveGridViewBatchSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveGridViewBatchSettings_Click" />
    <br />
    <br />
    <telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="batchAlertWindowManager"></telerik:RadWindowManager>
</div>