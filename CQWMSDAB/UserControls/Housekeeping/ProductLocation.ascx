﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductLocation.ascx.cs" Inherits="UserControls_HouseKeeping_ProductLocation" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Common/LocationSearch.ascx" TagName="Location" TagPrefix="uc1" %>

<div style="margin-left: 10px">

    <asp:UpdatePanel ID="updatePanelLocationSearch" runat="server" EnableViewState="False">
        <ContentTemplate>
            <br />
            <uc1:Location ID="location1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <telerik:RadButton ID="addStorageLocation" runat="server" Text="<%$ Resources:Default, AddLocation%>"
        OnClick="addStorageLocation_Click" />
    <br />
    <br />
    <asp:UpdatePanel ID="updatePanelLocation" runat="server" EnableViewState="False">
        <ContentTemplate>
            <telerik:RadGrid ID="GridViewLocation" DataSourceID="ObjectDataSourceStorageLocation"
                DataKeyNames="LocationId" runat="server" AllowPaging="True" AllowSorting="True" Skin="Metro" Width="400px"
                PageSize="30" AutoGenerateColumns="False" OnDeleteCommand="GridViewLocation_DeleteCommand" AllowAutomaticUpdates="true">
                <MasterTableView DataKeyNames="LocationId" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true" CommandItemDisplay="Top" EditMode="EditForms">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderText='<%$ Resources:Default, ButtonEdit %>'></telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn UniqueName="LocationDelete" ButtonType="ImageButton"
                            CommandName="Delete" HeaderText='<%$ Resources:Default, ButtonDelete %>' />
                        <telerik:GridBoundColumn DataField="Location" HeaderText="<%$ Resources:Default, Location %>"
                            ReadOnly="true" />
                        <telerik:GridBoundColumn DataField="MinimumQuantity" HeaderText="<%$ Resources:Default, MinimumQuantity %>" />
                        <telerik:GridBoundColumn DataField="HandlingQuantity" HeaderText="<%$ Resources:Default, HandlingQuantity %>" />
                        <telerik:GridBoundColumn DataField="MaximumQuantity" HeaderText="<%$ Resources:Default, MaximumQuantity %>" />
                    </Columns>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <br />
            <asp:ObjectDataSource ID="ObjectDataSourceStorageLocation" runat="server" TypeName="StaticInfo"
                SelectMethod="storageUnitLocation_Search" DeleteMethod="storageUnitLocation_Delete"
                UpdateMethod="storageUnitLocation_Update">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                        DefaultValue="-1" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                        DefaultValue="-1" />
                    <asp:Parameter Name="locationId" Type="Int32" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                        DefaultValue="-1" />
                    <asp:Parameter Name="minimumQuantity" Type="Decimal" DefaultValue="0" />
                    <asp:Parameter Name="handlingQuantity" Type="Decimal" DefaultValue="0" />
                    <asp:Parameter Name="maximumQuantity" Type="Decimal" DefaultValue="0" />
                    <asp:Parameter Name="locationId" Type="Int32" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
    <telerik:RadButton ID="ButtonSaveGridViewLocationSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveGridViewLocationSettings_Click" />
    <telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="locationAlertWindowManager"></telerik:RadWindowManager>
    <br />
    <br />
</div>