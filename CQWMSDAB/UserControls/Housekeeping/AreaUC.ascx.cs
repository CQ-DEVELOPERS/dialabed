﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControls_HouseKeeping_AreaUC : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            this.LoadSettings();
        }
        catch (Exception ex)
        {
            var message = ex.Message == null ? string.Empty : ex.Message.ToString();
            alertWindowManager.RadAlert(message, 350, 100, "Load Product Grid Settings", null);
        }
    }

    protected void rbtnSearch_Click(object sender, EventArgs e)
    {

    }

    protected void rgArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridDataItem Item = TelerikControls.GetSelectedItem(this.rgArea);

            Session["AreaId"] = Converter.String2Int(Item.GetDataKeyValue("AreaId"));

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnAreaSettings_Click(object sender, EventArgs e)
    {
        SaveSettings();
    }

    private void SaveSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(rgArea);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), rgArea, "AreaGridView", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
            alertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Save Product Grid Settings", null); ;
        }
    }

    private void LoadSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(rgArea);
            settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), rgArea, "AreaGridView", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
            alertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Load Product Grid Settings", null);
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);
            this.SaveSettings();
        }
        catch (Exception ex)
        {
            alertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Save Settings", null);
        }
    }

    protected void ObjectDataSourceArea_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        Session["AreaId"] = Convert.ToInt32(e.ReturnValue.ToString());
    }

    protected void ObjectDataSourceArea_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        rgArea.Rebind();
    }
}