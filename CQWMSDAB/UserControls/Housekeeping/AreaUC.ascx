﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AreaUC.ascx.cs" Inherits="UserControls_HouseKeeping_AreaUC" %>
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <telerik:RadTextBox ID="txtArea" LabelWidth="75px" runat="server" Width="250px" EmptyMessage="{All}" Label="Area:"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click">
                <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
            </telerik:RadButton>
        </td>
    </tr>
</table>
 <telerik:RadGrid ID="rgArea" DataSourceID="ObjectDataSourceArea" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" Skin="Metro" ClientSettings-Selecting-AllowRowSelect="true"
            OnSelectedIndexChanged="rgArea_SelectedIndexChanged" PageSize="10" AllowFilteringByColumn="False"
            EmptyDataText="<%$ Resources:Default, CurrentlyNoProducts %>" AllowAutomaticUpdates="true" AllowAutomaticInserts="true" AllowAutomaticDeletes="true">
  <MasterTableView DataKeyNames="AreaId,WarehouseId" DataSourceID="ObjectDataSourceArea" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true" CommandItemDisplay="Top" EditMode="EditForms">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderText='<%$ Resources:Default, ButtonEdit %>'></telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn DataField="Area" HeaderText="Area" SortExpression="Area" FilterControlAltText="Filter Area column" UniqueName="Area"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AreaCode" HeaderText="AreaCode" SortExpression="AreaCode" FilterControlAltText="Filter AreaCode column" UniqueName="AreaCode"></telerik:GridBoundColumn>
            <telerik:GridCheckBoxColumn DataField="StockOnHand" HeaderText="StockOnHand" SortExpression="StockOnHand" FilterControlAltText="Filter StockOnHand column" UniqueName="StockOnHand"></telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="AreaSequence" HeaderText="AreaSequence" SortExpression="AreaSequence" FilterControlAltText="Filter AreaSequence column" UniqueName="AreaSequence"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AreaType" HeaderText="AreaType" SortExpression="AreaType" FilterControlAltText="Filter AreaType column" UniqueName="AreaType"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WarehouseCode" HeaderText="WarehouseCode" SortExpression="WarehouseCode" FilterControlAltText="Filter WarehouseCode column" UniqueName="WarehouseCode"></telerik:GridBoundColumn>
            <telerik:GridCheckBoxColumn DataField="Replenish" HeaderText="Replenish" SortExpression="Replenish" FilterControlAltText="Filter Replenish column" UniqueName="Replenish"></telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="MinimumShelfLife" HeaderText="MinimumShelfLife" SortExpression="MinimumShelfLife" FilterControlAltText="Filter MinimumShelfLife column" UniqueName="MinimumShelfLife"></telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
     <ClientSettings EnablePostBackOnRowClick="true"></ClientSettings>
     <ExportSettings IgnorePaging="true" ExportOnlyData="true"></ExportSettings>
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectdatasourceStatusGroup" runat="server" TypeName="Status" SelectMethod="ListStatusGroup">
	<SelectParameters>
		<asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
	</SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectdatasourcePalletType" runat="server" TypeName="Palletise" SelectMethod="ListPalletType">
	<SelectParameters>
		<asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
	</SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="StaticInfo"
                        SelectMethod="Area_Search" UpdateMethod="Area_Update" InsertMethod="Area_Add"
                        DeleteMethod="Area_Delete" OnInserting="ObjectDataSourceArea_Inserting" OnInserted="ObjectDataSourceArea_Inserted">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String"/>
        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32"/>
        <asp:ControlParameter ControlID="txtArea" Name="area" Type="String" />
    </SelectParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String"/>
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32"/>
        <asp:Parameter Name="Area" Type="String" />
        <asp:Parameter Name="AreaCode" Type="String" />
        <asp:Parameter Name="StockOnHand" Type="Boolean" />
        <asp:Parameter Name="AreaSequence" Type="Int32" />
        <asp:Parameter Name="AreaType" Type="String" />
        <asp:Parameter Name="WarehouseCode" Type="String" />
        <asp:Parameter Name="Replenish" Type="Boolean" />
        <asp:Parameter Name="MinimumShelfLife" Type="Decimal" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String"/>
        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32"/>
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:Parameter Name="Area" Type="String" />
        <asp:Parameter Name="AreaCode" Type="String" />
        <asp:Parameter Name="StockOnHand" Type="Boolean" />
        <asp:Parameter Name="AreaSequence" Type="Int32" />
        <asp:Parameter Name="AreaType" Type="String" />
        <asp:Parameter Name="WarehouseCode" Type="String" />
        <asp:Parameter Name="Replenish" Type="Boolean" />
        <asp:Parameter Name="MinimumShelfLife" Type="Decimal" />
    </InsertParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String"/>
        <asp:Parameter Name="areaId" Type="Int32"/>
    </DeleteParameters>
</asp:ObjectDataSource>
<telerik:RadButton ID="btnAreaSettings" runat="server" Text="Save Layout Settings" OnClick="btnAreaSettings_Click"/>
<telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="alertWindowManager"></telerik:RadWindowManager>