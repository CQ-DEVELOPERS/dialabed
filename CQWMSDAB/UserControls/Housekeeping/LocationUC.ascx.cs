﻿using System;
using System.Web.UI;
using Telerik.Web.UI;

public partial class UserControls_Housekeeping_LocationUC : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void rgLocation_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnLocationSettings_Click(object sender, EventArgs e)
    {
        SaveSettings();
    }

    private void SaveSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(rgLocation);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), rgLocation, "LocationGridView", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
            alertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Save Location Grid Settings", null); ;
        }
    }

    private void LoadSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(rgLocation);
            settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), rgLocation, "LocationGridView", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
            alertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Load Location Grid Settings", null);
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);
            this.SaveSettings();
        }
        catch (Exception ex)
        {
            alertWindowManager.RadAlert(ex.Message.ToString(), 350, 100, "Save Settings", null);
        }
    }

    protected void rbtnSearch_Click(object sender, EventArgs e)
    {
        rgLocation.Rebind();
    }

}