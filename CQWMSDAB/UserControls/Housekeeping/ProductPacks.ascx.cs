﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControls_HouseKeeping_ProductPacks : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void GridViewPacks_DeleteCommand(object sender, GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (GridViewPacks.MasterTableView.Items.Count > 0)
        {
            int packId = Convert.ToInt32(GridViewPacks.MasterTableView.Items[e.Item.ItemIndex].GetDataKeyValue("PackId").ToString());
            var result = StaticInfo.pack_Delete(Session["ConnectionStringName"].ToString(), packId);
            if (result)
            {
                GridViewPacks.DataBind();
                packAlertWindowManager.RadAlert("Pack deleted successfully.", 350, 100, "Delete Pack", null);
            }
            else
            {
                packAlertWindowManager.RadAlert("Failed to delete pack as it is used somewhere.", 450, 100, "Pack Delete", null);
            }
        }
    }

    protected void ObjectDataSourcePacks_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
    }

    protected void ObjectDataSourcePacks_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.ReturnValue != null)
        {
            Session["PackId"] = int.Parse(e.ReturnValue.ToString());
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            this.LoadSettings();
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonSaveProductPacksSettings_Click(object sender, EventArgs e)
    {
        this.SaveSettings();
    }

    private void SaveSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewPacks);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), GridViewPacks, "ProductPacksGridViewPacks", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewPacks);
            settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), GridViewPacks, "ProductPacksGridViewPacks", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    #region Export Grid
    public void ExportGrid(string type)
    {
        GridViewPacks.ExportSettings.ExportOnlyData = true;
        GridViewPacks.ExportSettings.OpenInNewWindow = true;
        switch (type)
        {
            case "CSV":
                GridViewPacks.MasterTableView.ExportToCSV();
                break;
            case "Pdf":
                GridViewPacks.MasterTableView.ExportToPdf();
                break;
            case "Excel":
                GridViewPacks.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
                GridViewPacks.MasterTableView.ExportToExcel();
                break;
            case "Word":
                GridViewPacks.MasterTableView.ExportToWord();
                break;
        }
    }
    #endregion
}