﻿using System;
using Telerik.Web.UI;

public partial class UserControls_HouseKeeping_ProductArea : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void buttonAddstorageArea_Click(object sender, EventArgs e)
    {
        Session["MinimumQuantity"] = 0;
        Session["ReorderQuantity"] = 0;
        Session["MaximumQuantity"] = 0;

        var result = StaticInfo.storageUnitArea_Add(Session["ConnectionStringName"].ToString(), int.Parse(Session["StorageUnitId"].ToString()), int.Parse(Session["AreaId"].ToString()), Decimal.Parse(Session["MinimumQuantity"].ToString()), Decimal.Parse(Session["ReorderQuantity"].ToString()), Decimal.Parse(Session["MaximumQuantity"].ToString()));
        if (result)
        {
            areaAlertWindowManager.RadAlert("Area added successfully.", 350, 100, "Add Area", null);
        }
        GridViewArea.DataBind();
    }

    protected void GridViewArea_DeleteCommand(object sender, GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (GridViewArea.MasterTableView.Items.Count > 0)
        {
            int storageUnitId = Convert.ToInt32(GridViewArea.MasterTableView.Items[e.Item.ItemIndex].GetDataKeyValue("StorageUnitId").ToString());
            int areaId = Convert.ToInt32(GridViewArea.MasterTableView.Items[e.Item.ItemIndex].GetDataKeyValue("AreaId").ToString());
            try
            {
                var result = StaticInfo.storageUnitArea_Delete(Session["ConnectionStringName"].ToString(), storageUnitId, areaId);
                if (!result)
                {
                    areaAlertWindowManager.RadAlert("Failed to delete an area as it is being used somewhere.", 450, 100, "Delete Area", null);
                }
                else
                {
                    areaAlertWindowManager.RadAlert("Area deleted successfully.", 350, 100, "Delete Area", null);
                }
            }
            catch (Exception ex)
            {
                areaAlertWindowManager.RadAlert(ex.Message, 350, 100, "Delete Area", null);
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            this.LoadSettings();
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonSaveGridViewAreaSettings_Click(object sender, EventArgs e)
    {
        this.SaveSettings();
    }

    private void SaveSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewArea);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), GridViewArea, "ProductAreaGridViewArea", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadSettings()
    {
        try
        {
            var settings = new GridSettingsPersister(GridViewArea);
            settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), GridViewArea, "ProductAreaGridViewArea", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
        }
    }

    #region Export Grid
    public void ExportGrid(string type)
    {
        GridViewArea.ExportSettings.ExportOnlyData = true;
        GridViewArea.ExportSettings.OpenInNewWindow = true;
        switch (type)
        {
            case "CSV":
                GridViewArea.MasterTableView.ExportToCSV();
                break;
            case "Pdf":
                GridViewArea.MasterTableView.ExportToPdf();
                break;
            case "Excel":
                GridViewArea.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
                GridViewArea.MasterTableView.ExportToExcel();
                break;
            case "Word":
                GridViewArea.MasterTableView.ExportToWord();
                break;
        }
    }
    #endregion
}