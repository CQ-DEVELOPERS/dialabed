﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductArea.ascx.cs" Inherits="UserControls_HouseKeeping_ProductArea" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Common/AreaSearch.ascx" TagName="Area" TagPrefix="uc1" %>

<div style="margin-left: 10px" id="area">
    <asp:UpdatePanel ID="updatePanel_area" runat="server" EnableViewState="False">
        <ContentTemplate>
            <br />
            <uc1:Area ID="Area1" runat="server" />
            <br />
            <telerik:RadButton ID="buttonAddstorageArea" runat="server" Text="<%$ Resources:Default, AddArea%>"
                OnClick="buttonAddstorageArea_Click" />
            <br />
            <br />
            <telerik:RadGrid ID="GridViewArea" DataSourceID="ObjectDataSourceArea" DataKeyNames="StorageUnitId,AreaId" Skin="Metro" Width="500px"
                runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" AllowAutomaticDeletes="true" PageSize="30" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" OnDeleteCommand="GridViewArea_DeleteCommand">
                <MasterTableView DataKeyNames="StorageUnitId,AreaId" AllowFilteringByColumn="False" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderText='<%$ Resources:Default, ButtonEdit %>'></telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn UniqueName="AreaDelete" ButtonType="ImageButton"
                            CommandName="Delete" HeaderText='<%$ Resources:Default, ButtonDelete %>' />
                        <telerik:GridBoundColumn ReadOnly="true" DataField="Area" HeaderText="<%$ Resources:Default, Area%>"
                            SortExpression="AreaId">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StoreOrder" HeaderText="<%$ Resources:Default, StoreOrder %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PickOrder" HeaderText="<%$ Resources:Default, PickOrder %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MinimumQuantity" HeaderText="<%$ Resources:Default, MinimumQuantity %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ReorderQuantity" HeaderText="<%$ Resources:Default, ReorderQuantity %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MaximumQuantity" HeaderText="<%$ Resources:Default, MaximumQuantity %>"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <br />
            <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="StaticInfo"
                SelectMethod="storageUnitArea_Search" UpdateMethod="storageUnitArea_Update" DeleteMethod="storageUnitArea_Delete" InsertMethod="storageUnitArea_Add">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                        DefaultValue="-1" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="storeOrder" Type="Int32" />
                    <asp:Parameter Name="pickOrder" Type="Int32" />
                    <asp:Parameter Name="storageUnitId" Type="Int32" />
                    <asp:Parameter Name="areaId" Type="Int32" />
                    <asp:Parameter Name="minimumQuantity" Type="Decimal" />
                    <asp:Parameter Name="reorderQuantity" Type="Decimal" />
                    <asp:Parameter Name="maximumQuantity" Type="Decimal" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                        DefaultValue="-1" />
                    <asp:Parameter Name="areaId" Type="Int32" />
                </DeleteParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
    <telerik:RadButton ID="ButtonSaveGridViewAreaSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveGridViewAreaSettings_Click" />
    <br />
    <br />
    <telerik:RadWindowManager RenderMode="Lightweight" runat="server" ID="areaAlertWindowManager"></telerik:RadWindowManager>
</div>
