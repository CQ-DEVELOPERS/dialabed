<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExternalCompanyGridView.ascx.cs" Inherits="UserControls_ExternalCompanyGridView" %>
 
<table style="background-color:lightgray; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListExternalCompanyTypeId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyTypeId"
                DataTextField="ExternalCompanyType" DataValueField="ExternalCompanyTypeId" Label="ExternalCompanyType" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyTypeId" runat="server" TypeName="StaticInfoExternalCompanyType"
                SelectMethod="ParameterExternalCompanyType">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListRouteId" runat="server" DataSourceID="ObjectDataSourceDDLRouteId"
                DataTextField="Route" DataValueField="RouteId" Label="Route" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLRouteId" runat="server" TypeName="StaticInfoRoute"
                SelectMethod="ParameterRoute">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadTextBox ID="TextBoxExternalCompany" runat="server" Label="ExternalCompany" Width="300px" Skin="Metro"></telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadTextBox ID="TextBoxExternalCompanyCode" runat="server" Label="ExternalCompanyCode" Width="300px" Skin="Metro"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListContainerTypeId" runat="server" DataSourceID="ObjectDataSourceDDLContainerTypeId"
                DataTextField="ContainerType" DataValueField="ContainerTypeId" Label="ContainerType" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLContainerTypeId" runat="server" TypeName="StaticInfoContainerType"
                SelectMethod="ParameterContainerType">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListPrincipalId" runat="server" DataSourceID="ObjectDataSourceDDLPrincipalId"
                DataTextField="Principal" DataValueField="PrincipalId" Label="Principal" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLPrincipalId" runat="server" TypeName="StaticInfoPrincipal"
                SelectMethod="ParameterPrincipal">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListProcessId" runat="server" DataSourceID="ObjectDataSourceDDLProcessId"
                DataTextField="Process" DataValueField="ProcessId" Label="Process" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLProcessId" runat="server" TypeName="StaticInfoProcess"
                SelectMethod="ParameterProcess">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListPricingCategoryId" runat="server" DataSourceID="ObjectDataSourceDDLPricingCategoryId"
                DataTextField="PricingCategory" DataValueField="PricingCategoryId" Label="PricingCategory" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLPricingCategoryId" runat="server" TypeName="StaticInfoPricingCategory"
                SelectMethod="ParameterPricingCategory">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListLabelingCategoryId" runat="server" DataSourceID="ObjectDataSourceDDLLabelingCategoryId"
                DataTextField="LabelingCategory" DataValueField="LabelingCategoryId" Label="LabelingCategory" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLLabelingCategoryId" runat="server" TypeName="StaticInfoLabelingCategory"
                SelectMethod="ParameterLabelingCategory">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
        <td>
            <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
        </td>
    </tr>
</table>
 
<telerik:RadGrid ID="RadGridExternalCompany" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true"
    AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="30" DataSourceID="ObjectDataSourceExternalCompany" OnSelectedIndexChanged="RadGridExternalCompany_SelectedIndexChanged" Skin="Metro">
<PagerStyle Mode="NumericPages"></PagerStyle>
<MasterTableView DataKeyNames="
ExternalCompanyId
" DataSourceID="ObjectDataSourceExternalCompany"
    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
    InsertItemPageIndexAction="ShowItemOnFirstPage">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="ExternalCompanyId" HeaderText="ExternalCompanyId" SortExpression="ExternalCompanyId" UniqueName="ExternalCompanyId"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListExternalCompanyTypeId" ListTextField="ExternalCompanyType" ListValueField="ExternalCompanyTypeId" DataSourceID="ObjectDataSourceExternalCompanyTypeId" DataField="ExternalCompanyTypeId" HeaderText="ExternalCompanyType">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListRouteId" ListTextField="Route" ListValueField="RouteId" DataSourceID="ObjectDataSourceRouteId" DataField="RouteId" HeaderText="Route">
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="ExternalCompany" HeaderText="ExternalCompany" SortExpression="ExternalCompany" UniqueName="ExternalCompany"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ExternalCompanyCode" HeaderText="ExternalCompanyCode" SortExpression="ExternalCompanyCode" UniqueName="ExternalCompanyCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Rating" HeaderText="Rating" SortExpression="Rating" UniqueName="Rating"></telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="RedeliveryIndicator" HeaderText="RedeliveryIndicator" SortExpression="RedeliveryIndicator" UniqueName="RedeliveryIndicator"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="QualityAssuranceIndicator" HeaderText="QualityAssuranceIndicator" SortExpression="QualityAssuranceIndicator" UniqueName="QualityAssuranceIndicator"></telerik:GridCheckBoxColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListContainerTypeId" ListTextField="ContainerType" ListValueField="ContainerTypeId" DataSourceID="ObjectDataSourceContainerTypeId" DataField="ContainerTypeId" HeaderText="ContainerType">
            </telerik:GridDropDownColumn>
                        <telerik:GridCheckBoxColumn DataField="Backorder" HeaderText="Backorder" SortExpression="Backorder" UniqueName="Backorder"></telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="HostId" HeaderText="HostId" SortExpression="HostId" UniqueName="HostId"></telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="OrderLineSequence" HeaderText="OrderLineSequence" SortExpression="OrderLineSequence" UniqueName="OrderLineSequence"></telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="DropSequence" HeaderText="DropSequence" SortExpression="DropSequence" UniqueName="DropSequence"></telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="AutoInvoice" HeaderText="AutoInvoice" SortExpression="AutoInvoice" UniqueName="AutoInvoice"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="OneProductPerPallet" HeaderText="OneProductPerPallet" SortExpression="OneProductPerPallet" UniqueName="OneProductPerPallet"></telerik:GridCheckBoxColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListPrincipalId" ListTextField="Principal" ListValueField="PrincipalId" DataSourceID="ObjectDataSourcePrincipalId" DataField="PrincipalId" HeaderText="Principal">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListProcessId" ListTextField="Process" ListValueField="ProcessId" DataSourceID="ObjectDataSourceProcessId" DataField="ProcessId" HeaderText="Process">
            </telerik:GridDropDownColumn>
                        <telerik:GridCheckBoxColumn DataField="TrustedDelivery" HeaderText="TrustedDelivery" SortExpression="TrustedDelivery" UniqueName="TrustedDelivery"></telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="OutboundSLAHours" HeaderText="OutboundSLAHours" SortExpression="OutboundSLAHours" UniqueName="OutboundSLAHours"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListPricingCategoryId" ListTextField="PricingCategory" ListValueField="PricingCategoryId" DataSourceID="ObjectDataSourcePricingCategoryId" DataField="PricingCategoryId" HeaderText="PricingCategory">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListLabelingCategoryId" ListTextField="LabelingCategory" ListValueField="LabelingCategoryId" DataSourceID="ObjectDataSourceLabelingCategoryId" DataField="LabelingCategoryId" HeaderText="LabelingCategory">
            </telerik:GridDropDownColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
       </ClientSettings>
    <GroupingSettings ShowUnGroupButton="True" />
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectDataSourceExternalCompanyTypeId" runat="server" TypeName="StaticInfoExternalCompanyType"
    SelectMethod="ParameterExternalCompanyType">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceRouteId" runat="server" TypeName="StaticInfoRoute"
    SelectMethod="ParameterRoute">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceContainerTypeId" runat="server" TypeName="StaticInfoContainerType"
    SelectMethod="ParameterContainerType">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourcePrincipalId" runat="server" TypeName="StaticInfoPrincipal"
    SelectMethod="ParameterPrincipal">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceProcessId" runat="server" TypeName="StaticInfoProcess"
    SelectMethod="ParameterProcess">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourcePricingCategoryId" runat="server" TypeName="StaticInfoPricingCategory"
    SelectMethod="ParameterPricingCategory">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceLabelingCategoryId" runat="server" TypeName="StaticInfoLabelingCategory"
    SelectMethod="ParameterLabelingCategory">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
 
<asp:ObjectDataSource ID="ObjectDataSourceExternalCompany" runat="server" TypeName="StaticInfoExternalCompany"
    DeleteMethod="DeleteExternalCompany"
    InsertMethod="InsertExternalCompany"
    SelectMethod="SearchExternalCompany"
    UpdateMethod="UpdateExternalCompany">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="DropDownListExternalCompanyTypeId" DefaultValue="-1" Name="ExternalCompanyTypeId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListRouteId" DefaultValue="-1" Name="RouteId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="TextBoxExternalCompany" DefaultValue="%" Name="ExternalCompany" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxExternalCompanyCode" DefaultValue="%" Name="ExternalCompanyCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="DropDownListContainerTypeId" DefaultValue="-1" Name="ContainerTypeId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListPrincipalId" DefaultValue="-1" Name="PrincipalId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListProcessId" DefaultValue="-1" Name="ProcessId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListPricingCategoryId" DefaultValue="-1" Name="PricingCategoryId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListLabelingCategoryId" DefaultValue="-1" Name="LabelingCategoryId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RouteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompany" Type="String" />
        <asp:Parameter Name="ExternalCompanyCode" Type="String" />
        <asp:Parameter Name="Rating" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RedeliveryIndicator" Type="Boolean" />
        <asp:Parameter Name="QualityAssuranceIndicator" Type="Boolean" />
        <asp:Parameter Name="ContainerTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Backorder" Type="Boolean" />
        <asp:Parameter Name="HostId" Type="String" />
        <asp:Parameter Name="OrderLineSequence" Type="Boolean" />
        <asp:Parameter Name="DropSequence" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AutoInvoice" Type="Boolean" />
        <asp:Parameter Name="OneProductPerPallet" Type="Boolean" />
        <asp:Parameter Name="PrincipalId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ProcessId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TrustedDelivery" Type="Boolean" />
        <asp:Parameter Name="OutboundSLAHours" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PricingCategoryId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="LabelingCategoryId" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="ExternalCompanyId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RouteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompany" Type="String" />
        <asp:Parameter Name="ExternalCompanyCode" Type="String" />
        <asp:Parameter Name="Rating" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RedeliveryIndicator" Type="Boolean" />
        <asp:Parameter Name="QualityAssuranceIndicator" Type="Boolean" />
        <asp:Parameter Name="ContainerTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Backorder" Type="Boolean" />
        <asp:Parameter Name="HostId" Type="String" />
        <asp:Parameter Name="OrderLineSequence" Type="Boolean" />
        <asp:Parameter Name="DropSequence" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AutoInvoice" Type="Boolean" />
        <asp:Parameter Name="OneProductPerPallet" Type="Boolean" />
        <asp:Parameter Name="PrincipalId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ProcessId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TrustedDelivery" Type="Boolean" />
        <asp:Parameter Name="OutboundSLAHours" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PricingCategoryId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="LabelingCategoryId" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
