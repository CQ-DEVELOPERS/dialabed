<%@ Control Language="C#" AutoEventWireup="true" CodeFile="COATestGridView.ascx.cs" Inherits="UserControls_COATestGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowCOATest()
{
   window.open("COATestFormView.aspx", "_blank",
      "height=640px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelCOAId" runat="server" Text="COA:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                DataTextField="COA" DataValueField="COAId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                SelectMethod="ParameterCOA">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <asp:Label ID="LabelTestId" runat="server" Text="Test:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListTestId" runat="server" DataSourceID="ObjectDataSourceDDLTestId"
                DataTextField="Test" DataValueField="TestId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLTestId" runat="server" TypeName="StaticInfoTest"
                SelectMethod="ParameterTest">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <asp:Label ID="LabelResultId" runat="server" Text="Result:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListResultId" runat="server" DataSourceID="ObjectDataSourceDDLResultId"
                DataTextField="Result" DataValueField="ResultId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLResultId" runat="server" TypeName="StaticInfoResult"
                SelectMethod="ParameterResult">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelMethodId" runat="server" Text="Method:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListMethodId" runat="server" DataSourceID="ObjectDataSourceDDLMethodId"
                DataTextField="Method" DataValueField="MethodId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLMethodId" runat="server" TypeName="StaticInfoMethod"
                SelectMethod="ParameterMethod">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"/>
        <td>
            <asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowCOATest();"/>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Visible="False"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewCOATest" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
COATestId
"
                DataSourceID="ObjectDataSourceCOATest" OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewCOATest_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="False" ShowEditButton="True" />
        <asp:BoundField DataField="COATestId" HeaderText="COATestId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="COATestId" />
            <asp:TemplateField HeaderText="COAId" meta:resourcekey="COAId" SortExpression="COAId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                        DataTextField="COA" DataValueField="COAId" SelectedValue='<%# Bind("COAId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                        SelectMethod="ParameterCOA">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkCOAId" runat="server" Text='<%# Bind("COA") %>' NavigateUrl="~/Administration/COAGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
            <asp:TemplateField HeaderText="TestId" meta:resourcekey="TestId" SortExpression="TestId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListTestId" runat="server" DataSourceID="ObjectDataSourceDDLTestId"
                        DataTextField="Test" DataValueField="TestId" SelectedValue='<%# Bind("TestId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTestId" runat="server" TypeName="StaticInfoTest"
                        SelectMethod="ParameterTest">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkTestId" runat="server" Text='<%# Bind("Test") %>' NavigateUrl="~/Administration/TestGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
            <asp:TemplateField HeaderText="ResultId" meta:resourcekey="ResultId" SortExpression="ResultId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListResultId" runat="server" DataSourceID="ObjectDataSourceDDLResultId"
                        DataTextField="Result" DataValueField="ResultId" SelectedValue='<%# Bind("ResultId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLResultId" runat="server" TypeName="StaticInfoResult"
                        SelectMethod="ParameterResult">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkResultId" runat="server" Text='<%# Bind("Result") %>' NavigateUrl="~/Administration/ResultGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
            <asp:TemplateField HeaderText="MethodId" meta:resourcekey="MethodId" SortExpression="MethodId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListMethodId" runat="server" DataSourceID="ObjectDataSourceDDLMethodId"
                        DataTextField="Method" DataValueField="MethodId" SelectedValue='<%# Bind("MethodId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMethodId" runat="server" TypeName="StaticInfoMethod"
                        SelectMethod="ParameterMethod">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkMethodId" runat="server" Text='<%# Bind("Method") %>' NavigateUrl="~/Administration/MethodGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ResultCode" HeaderText="ResultCode" SortExpression="ResultCode" />
                        <asp:BoundField DataField="Result" HeaderText="Result" SortExpression="Result" />
                        <asp:TemplateField HeaderText="Pass" SortExpression="Pass">
                            <EditItemTemplate>
                                <asp:CheckBox ID="PassCheckBox" runat="server" Checked='<%# Bind("Pass") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="PassCheckBox" runat="server" Checked='<%# Bind("Pass") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StartRange" HeaderText="StartRange" SortExpression="StartRange" />
                        <asp:BoundField DataField="EndRange" HeaderText="EndRange" SortExpression="EndRange" />
                    <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:ObjectDataSource ID="ObjectDataSourceCOATest" runat="server" TypeName="StaticInfoCOATest"
    DeleteMethod="DeleteCOATest"
    InsertMethod="InsertCOATest"
    SelectMethod="SearchCOATest"
    UpdateMethod="UpdateCOATest">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COATestId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="DropDownListCOAId" DefaultValue="-1" Name="COAId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListTestId" DefaultValue="-1" Name="TestId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListResultId" DefaultValue="-1" Name="ResultId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListMethodId" DefaultValue="-1" Name="MethodId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COATestId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COATestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MethodId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultCode" Type="String" />
        <asp:Parameter Name="Result" Type="String" />
        <asp:Parameter Name="Pass" Type="Boolean" />
        <asp:Parameter Name="StartRange" Type="Double" />
        <asp:Parameter Name="EndRange" Type="Double" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="COATestId" Type="Int32" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MethodId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultCode" Type="String" />
        <asp:Parameter Name="Result" Type="String" />
        <asp:Parameter Name="Pass" Type="Boolean" />
        <asp:Parameter Name="StartRange" Type="Double" />
        <asp:Parameter Name="EndRange" Type="Double" />
    </InsertParameters>
</asp:ObjectDataSource>
