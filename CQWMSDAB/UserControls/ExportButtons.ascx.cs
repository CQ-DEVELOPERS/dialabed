﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_ExportButtons : System.Web.UI.UserControl
{
    public event EventHandler ExportGrid;
    public string ExportTo;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgb = sender as ImageButton;
        this.ExportTo = imgb.ID;
        if (ExportGrid != null)
            ExportGrid(this, EventArgs.Empty);
    }
}