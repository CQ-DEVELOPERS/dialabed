<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReasonFormView.ascx.cs" Inherits="UserControls_ReasonFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Reason</h3>
<asp:FormView ID="FormViewReason" runat="server" DataKeyNames="ReasonId" DataSourceID="ObjectDataSourceReason" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:TextBox ID="ReasonTextBox" runat="server" Text='<%# Bind("Reason") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ReasonCode:
                </td>
                <td>
                    <asp:TextBox ID="ReasonCodeTextBox" runat="server" Text='<%# Bind("ReasonCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AdjustmentType:
                </td>
                <td>
                    <asp:TextBox ID="AdjustmentTypeTextBox" runat="server" Text='<%# Bind("AdjustmentType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ToWarehouseCode:
                </td>
                <td>
                    <asp:TextBox ID="ToWarehouseCodeTextBox" runat="server" Text='<%# Bind("ToWarehouseCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RecordType:
                </td>
                <td>
                    <asp:TextBox ID="RecordTypeTextBox" runat="server" Text='<%# Bind("RecordType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:TextBox ID="ReasonTextBox" runat="server" Text='<%# Bind("Reason") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ReasonCode:
                </td>
                <td>
                    <asp:TextBox ID="ReasonCodeTextBox" runat="server" Text='<%# Bind("ReasonCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AdjustmentType:
                </td>
                <td>
                    <asp:TextBox ID="AdjustmentTypeTextBox" runat="server" Text='<%# Bind("AdjustmentType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ToWarehouseCode:
                </td>
                <td>
                    <asp:TextBox ID="ToWarehouseCodeTextBox" runat="server" Text='<%# Bind("ToWarehouseCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RecordType:
                </td>
                <td>
                    <asp:TextBox ID="RecordTypeTextBox" runat="server" Text='<%# Bind("RecordType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:Label ID="ReasonLabel" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ReasonCode:
                </td>
                <td>
                    <asp:Label ID="ReasonCodeLabel" runat="server" Text='<%# Bind("ReasonCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AdjustmentType:
                </td>
                <td>
                    <asp:Label ID="AdjustmentTypeLabel" runat="server" Text='<%# Bind("AdjustmentType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ToWarehouseCode:
                </td>
                <td>
                    <asp:Label ID="ToWarehouseCodeLabel" runat="server" Text='<%# Bind("ToWarehouseCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RecordType:
                </td>
                <td>
                    <asp:Label ID="RecordTypeLabel" runat="server" Text='<%# Bind("RecordType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ReasonGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="StaticInfoReason"
    SelectMethod="GetReason"
    DeleteMethod="DeleteReason"
    UpdateMethod="UpdateReason"
    InsertMethod="InsertReason">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReasonId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Reason" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="AdjustmentType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ToWarehouseCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="RecordType" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ReasonId" QueryStringField="ReasonId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReasonId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Reason" Type="String" />
        <asp:Parameter Name="ReasonCode" Type="String" />
        <asp:Parameter Name="AdjustmentType" Type="String" />
        <asp:Parameter Name="ToWarehouseCode" Type="String" />
        <asp:Parameter Name="RecordType" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
