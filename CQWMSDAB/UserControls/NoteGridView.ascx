<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NoteGridView.ascx.cs" Inherits="UserControls_NoteGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowNote()
{
   window.open("NoteFormView.aspx", "_blank",
      "height=640px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelNoteCode" runat="server" Text="NoteCode:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNoteCode" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="LabelNote" runat="server" Text="Note:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNote" runat="server"></asp:TextBox>
        </td>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"/>
        <td>
            <asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowNote();"/>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Visible="False"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewNote" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
NoteId
"
                DataSourceID="ObjectDataSourceNote" OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewNote_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="False" ShowEditButton="True" />
        <asp:BoundField DataField="NoteId" HeaderText="NoteId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="NoteId" />
                        <asp:BoundField DataField="NoteCode" HeaderText="NoteCode" SortExpression="NoteCode" />
        <asp:TemplateField HeaderText="Note" SortExpression="Note">
            <EditItemTemplate>
                <asp:TextBox ID="TextBoxNote" runat="server" Text='<%# Bind("Note") %>' TextMode="MultiLine" Rows="10" Width="300px" AutoCompleteType="Notes"></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="LabelNote" runat="server" Text='<%# Bind("Note") %>' ></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:ObjectDataSource ID="ObjectDataSourceNote" runat="server" TypeName="StaticInfoNote"
    DeleteMethod="DeleteNote"
    InsertMethod="InsertNote"
    SelectMethod="SearchNote"
    UpdateMethod="UpdateNote">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="NoteId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="TextBoxNoteCode" DefaultValue="%" Name="NoteCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxNote" DefaultValue="%" Name="Note" PropertyName="Text" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="NoteId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="NoteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteCode" Type="String" />
        <asp:Parameter Name="Note" Type="String" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="NoteId" Type="Int32" />
        <asp:Parameter Name="NoteCode" Type="String" />
        <asp:Parameter Name="Note" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
