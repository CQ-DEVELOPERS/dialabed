<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageUnitAreaFormView.ascx.cs" Inherits="UserControls_StorageUnitAreaFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>StorageUnitArea</h3>
<asp:FormView ID="FormViewStorageUnitArea" runat="server" DataKeyNames="StorageUnitAreaId" DataSourceID="ObjectDataSourceStorageUnitArea" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    StoreOrder:
                </td>
                <td>
                    <asp:TextBox ID="StoreOrderTextBox" runat="server" Text='<%# Bind("StoreOrder") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="StoreOrderTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PickOrder:
                </td>
                <td>
                    <asp:TextBox ID="PickOrderTextBox" runat="server" Text='<%# Bind("PickOrder") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="PickOrderTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListStorageUnit" ErrorMessage="* Please enter StorageUnitId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Area:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="ObjectDataSourceDDLArea"
                        DataTextField="Area" DataValueField="AreaId" SelectedValue='<%# Bind("AreaId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLArea" runat="server"
                        TypeName="StaticInfoArea" SelectMethod="ParameterArea">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListArea" ErrorMessage="* Please enter AreaId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StoreOrder:
                </td>
                <td>
                    <asp:TextBox ID="StoreOrderTextBox" runat="server" Text='<%# Bind("StoreOrder") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="StoreOrderTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PickOrder:
                </td>
                <td>
                    <asp:TextBox ID="PickOrderTextBox" runat="server" Text='<%# Bind("PickOrder") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="PickOrderTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Area:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="ObjectDataSourceDDLArea"
                        DataTextField="Area" DataValueField="AreaId" SelectedValue='<%# Bind("AreaId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLArea" runat="server"
                        TypeName="StaticInfoArea" SelectMethod="ParameterArea">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StoreOrder:
                </td>
                <td>
                    <asp:Label ID="StoreOrderLabel" runat="server" Text='<%# Bind("StoreOrder") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    PickOrder:
                </td>
                <td>
                    <asp:Label ID="PickOrderLabel" runat="server" Text='<%# Bind("PickOrder") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/StorageUnitAreaGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceStorageUnitArea" runat="server" TypeName="StaticInfoStorageUnitArea"
    SelectMethod="GetStorageUnitArea"
    DeleteMethod="DeleteStorageUnitArea"
    UpdateMethod="UpdateStorageUnitArea"
    InsertMethod="InsertStorageUnitArea">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitAreaId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:Parameter Name="StoreOrder" Type="Int32" />
        <asp:Parameter Name="PickOrder" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="StorageUnitId" QueryStringField="StorageUnitId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="AreaId" QueryStringField="AreaId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:Parameter Name="StoreOrder" Type="Int32" />
        <asp:Parameter Name="PickOrder" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
