﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Events;
//using Utils;
//using System;


public partial class UserControls_Inbound_CustomerReturnNewUC : System.Web.UI.UserControl
{
    #region Public Events
    public delegate void ErrorEventHandler(object sender, ExceptionEventArgs e);
    public event ErrorEventHandler CustomerReturnError;
    public event EventHandler CustomerReturnInserted;
    public event EventHandler CancelButtonClicked;
    #endregion

    #region Public Properties

    public string ReturnType
    {
        get { return hfReturnType.Value; }

        set { hfReturnType.Value = value; }
    }

    public int InboundDocumentId
    {
        get
        {
            int inboundDocumentId;
            if (Int32.TryParse(hfInboundDocumentId.Value, out inboundDocumentId))
                return inboundDocumentId;
            return -1;
        }

        set { hfInboundDocumentId.Value = value.ToString(); }
    }


    public string InvoiceNumber
    {
        get
        {
            return TextBoxReferenceNumberInsert.Text;
        }

        set { TextBoxReferenceNumberInsert.Text = value; }
    }

    public string DocumentNumber
    {
        get
        {
            return TextBoxOrderNumberEdit.Text;
        }
        set
        {
            TextBoxOrderNumberEdit.Text = value;
        }
    }

    public DateTime? DeliveryDate
    {
        get
        {
            return CalendarExtenderFromDateInsert.SelectedDate;
        }
        set
        {
            CalendarExtenderFromDateInsert.SelectedDate = value;
        }
    }

    public bool Enabled
    {
        get
        {
            return DropDownListInboundDocumentType.Enabled && TextBoxOrderNumberEdit.Enabled && TextBoxReferenceNumberInsert.Enabled && DropDownListReasonInsert.Enabled && DropDownListSalesRepInsert.Enabled && CalendarExtenderFromDateInsert.Enabled && RadTextBoxRemarks.Enabled && RadButtonInsert.Enabled;
        }

        set
        {
            DropDownListInboundDocumentType.Enabled = TextBoxOrderNumberEdit.Enabled = TextBoxReferenceNumberInsert.Enabled = DropDownListReasonInsert.Enabled = DropDownListSalesRepInsert.Enabled = CalendarExtenderFromDateInsert.Enabled = RadTextBoxRemarks.Enabled = RadButtonInsert.Enabled = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Clear()
    {
        TextBoxReferenceNumberInsert.Text = String.Empty;
        RadTextBoxRemarks.Text = String.Empty;
        TextBoxDeliveryDateInsert.Text = String.Empty;
        TextBoxOrderNumberEdit.Text = String.Empty;
        InboundDocumentId = -1;
    }

    #region ObjectDataSourceInboundDocumentUpdate_OnInserted
    protected void InsertCustomerReturn()
    {
        try
        {
            InboundDocument inboundDocument = new InboundDocument();
            int returnValue = inboundDocument.CreateInboundDocument(Session["ConnectionStringName"].ToString(), Int32.Parse(DropDownListInboundDocumentType.SelectedValue), -1, (int)Session["WarehouseId"], TextBoxOrderNumberEdit.Text.Trim(), CalendarExtenderFromDateInsert.SelectedDate.Value, TextBoxReferenceNumberInsert.Text.Trim(), Int32.Parse(DropDownListReasonInsert.SelectedValue), RadTextBoxRemarks.Text.Trim(), Int32.Parse(DropDownListSalesRepInsert.SelectedValue));
            if (returnValue > 0)
            {
                InboundDocumentId = returnValue;
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 316))
                    inboundDocument.CreateInboundLineFromInvoice(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], InboundDocumentId, Int32.Parse(InvoiceNumber));

                if (CustomerReturnInserted != null)
                    CustomerReturnInserted(this, new EventArgs());
            }
            else
            {
                throw new Exception("Duplicate Order Number and Customer Combination.");
            }
        }
        catch (Exception ex)
        {
            if (CustomerReturnError != null)
                CustomerReturnError(this, new ExceptionEventArgs(ex));
        }
    }
    #endregion ObjectDataSourceInboundDocumentUpdate_OnInserted

    protected bool ValidateForm()
    {
        try
        {
            if (ReturnType == "RET")
            {
                REQReferenceNumberInsert.ErrorMessage = String.Empty;
                REQReferenceNumberInsert.IsValid = true;

                InboundDocument id = new InboundDocument();

                if (!id.ValidInvoice(Session["ConnectionStringName"].ToString(), InvoiceNumber.ToString()))
                {
                    REQReferenceNumberInsert.ErrorMessage = "Invoice Number is Invalid.";
                    REQReferenceNumberInsert.IsValid = false;
                }
            }

            return REQOrderNumberEdit.IsValid && REQReferenceNumberInsert.IsValid && RequiredFieldValidator1.IsValid && RequiredFieldValidatorDeliveryDateInsert.IsValid;
        }
        catch (Exception ex)
        {
            if (CustomerReturnError != null)
                CustomerReturnError(this, new ExceptionEventArgs(ex));
            throw ex;
        }
    }


    #region DetailsViewInboundDocument_OnDataBound
    protected void Initialise()
    {
        try
        {
            TextBoxReferenceNumberInsert.Visible = true;
            REQReferenceNumberInsert.Enabled = true;
        }
        catch
        {
            throw;
        }
    }
    #endregion DetailsViewInboundDocument_OnDataBound

    protected void RadButtonInsert_Click(object sender, EventArgs e)
    {
        if (ValidateForm())
        {
            InsertCustomerReturn();
        }
        else
        {
            if (CustomerReturnError != null)
                CustomerReturnError(this, new ExceptionEventArgs(new Exception("Not all required fields are completed")));
        }
    }

    protected void RadButtonCancel_Click(object sender, EventArgs e)
    {
        if (CancelButtonClicked != null)
            CancelButtonClicked(this, new EventArgs());
    }

}
