﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerReturnNewLinesUC.ascx.cs" Inherits="UserControls_Inbound_CustomerReturnNewLinesUC" %>
<%@ Register Src="../../Common/ProductSearchCustomerReturn.ascx" TagName="ProductSearch" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="uc3" %>
<%@ Register Src="../../Common/ReasonSearchCustomerReturns.ascx" TagName="ReasonSearch" TagPrefix="uc5" %>
<style type="text/css">
    .col1 {
        width: 20%;
    }
</style>
<asp:Panel ID="PanelSearch" runat="server" BackColor="#EFEFEC" Width="550px">
    <table style="width: 100%;">
        <thead>
            <tr>
                <td colspan="2" style="text-align: center;">Customer Return Lines
                </td>
            </tr>
        </thead>
        <tr>
            <td>
                <asp:Label Text="Product" ID="lblProduct" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadComboBox Width="421px" ID="RadAutoCompleteBoxProduct" MarkFirstMatch="true" HighlightTemplatedItems="true" AutoPostBack="true" EmptyMessage="Select a Product..." runat="server" DataSourceID="ObjectDataSourceProducts" DataTextField="FullProduct" DataValueField="StorageUnitId">
                    <HeaderTemplate>
                        <table style="width: 416px; text-align: left">
                            <tr>
                                <td style="width: 116px;">Product</td>
                                <td style="width: 100px;">ProductCode</td>
                                <td style="width: 100px;">SKUCode</td>
                                <td style="width: 100px;">SKU</td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table style="width: 416px; text-align: left">
                            <tr>
                                <td style="width: 116px;">
                                    <%# DataBinder.Eval(Container.DataItem, "Product") %>        
                                </td>
                                <td style="width: 100px;">
                                    <%# DataBinder.Eval(Container.DataItem, "ProductCode") %>        
                                </td>
                                <td style="width: 100px;">
                                    <%# DataBinder.Eval(Container.DataItem, "SKUCode") %>        
                                </td>
                                <td style="width: 100px;">
                                    <%# DataBinder.Eval(Container.DataItem, "SKU") %>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadComboBox>
                <asp:ObjectDataSource ID="ObjectDataSourceProducts" runat="server" SelectMethod="SearchProductsCustomerReturn" TypeName="Product" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:ControlParameter ControlID="hfInboundDocumentId" Name="inboundDocumentId" PropertyName="Value" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Batch" ID="Label1" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadComboBox Width="421px" DropDownAutoWidth="Enabled" ID="RadAutoCompleteBoxBatch" HighlightTemplatedItems="true" MarkFirstMatch="true" DataSourceID="ObjectDataSourceBatch" AutoPostBack="true" runat="server" EmptyMessage="Select a Batch..." DataTextField="FullBatch" DataValueField="BatchId" Skin="Default">
                    <HeaderTemplate>
                        <table style="width: 416px; text-align: left">
                            <tr>
                                <td style="width: 138px;">Batch</td>
                                <td style="width: 138px;">ECLNumber</td>
                                <td style="width: 138px;">CreateDate</td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table style="width: 416px; text-align: left">
                            <tr>
                                <td style="width: 138px;">
                                    <%# DataBinder.Eval(Container.DataItem, "Batch") %>        
                                </td>
                                <td style="width: 138px;">
                                    <%# DataBinder.Eval(Container.DataItem, "ECLNumber") %>        
                                </td>
                                <td style="width: 138px;">
                                    <%# DataBinder.Eval(Container.DataItem, "CreateDate") %>        
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadComboBox>
                <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" SelectMethod="SearchBatchesByStorageUnit" TypeName="Batch" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:ControlParameter ControlID="RadAutoCompleteBoxProduct" Name="storageUnitId" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Reason" ID="Label2" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadComboBox ID="RadAutoCompleteBoxReason" DataSourceID="ObjectDataSourceReason" runat="server" AutoPostBack="True" DataTextField="Reason" DataValueField="ReasonId" EmptyMessage="Select a Reason..." Skin="Default">
                </telerik:RadComboBox>
                <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" SelectMethod="GetReasonsByType" TypeName="Reason" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="reasonCode" Type="String" DefaultValue="CR%" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Quantity" ID="Label3" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtQuantity" runat="server"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers" TargetControlID="txtQuantity"></ajaxToolkit:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;" colspan="2">
                <telerik:RadButton ID="ButtonInsertLine" runat="server" Text="<%$ Resources:Default, Insert%>" OnClick="ButtonInsertLine_Click" />
                <telerik:RadButton ID="ButtonDeleteLine" runat="server" Text="<%$ Resources:Default, Delete%>" OnClick="ButtonDeleteLine_Click" />
                <telerik:RadButton ID="ButtonInsertAll" runat="server" Text="Insert All" OnClick="ButtonInsertAll_Click" OnClientClicking="showConformation" />
                <telerik:RadButton ID="ButtonDone" CausesValidation="false" runat="server" Text="Close" OnClick="ButtonDone_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDeleteLine" runat="server" ConfirmText="<%$ Resources:Default, PressOkDelete%>" TargetControlID="ButtonDeleteLine"></ajaxToolkit:ConfirmButtonExtender>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch" Radius="10" Color="239, 239, 236" BorderColor="64, 64, 64"
    Enabled="True" />
<asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
<asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, ProductNotOnInvoice%>"></asp:Label>
<asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
<asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, QuantityMoreThanOrder%>"></asp:Label>
<asp:Image ID="ImageGreen" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
<asp:Label ID="LabelGreen" runat="server" Text="<%$ Resources:Default, Correct%>"></asp:Label>
<asp:Label ID="LabelErrorMsg" runat="server" Text="" EnableTheming="false" ForeColor="Red"></asp:Label>
<asp:GridView ID="GridViewInboundLine" runat="server" AllowPaging="True" AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" AutoGenerateEditButton="True" DataKeyNames="InboundLineId"
    DataSourceID="ObjectDataSourceInboundLine" EnableModelValidation="True">
    <Columns>
        <asp:TemplateField HeaderText="<%$ Resources:Default, Status %>">
            <ItemTemplate>
                <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "Remarks", "../images/Indicators/{0}.gif") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="ProductCode" ReadOnly="true" HeaderText="<%$ Resources:Default, ProductCode %>"></asp:BoundField>
        <asp:BoundField DataField="Product" ReadOnly="true" HeaderText="<%$ Resources:Default, Product %>"></asp:BoundField>
        <asp:BoundField DataField="SKUCode" ReadOnly="true" HeaderText="<%$ Resources:Default, SKUCode %>"></asp:BoundField>
        <asp:BoundField DataField="Batch" ReadOnly="true" HeaderText="<%$ Resources:Default, Batch %>"></asp:BoundField>
        <asp:BoundField DataField="ECLNumber" ReadOnly="true" HeaderText="<%$ Resources:Default, ECLNumber %>"></asp:BoundField>
        <asp:BoundField DataField="Status" ReadOnly="true" HeaderText="<%$ Resources:Default, Status %>"></asp:BoundField>
        <asp:BoundField DataField="Quantity" ReadOnly="false" HeaderText="Original Quantity"></asp:BoundField>
        <asp:BoundField DataField="ReturnQuantity" HeaderText="Return Quantity"></asp:BoundField>
        <asp:BoundField DataField="CreditedQuantity" ReadOnly="true" HeaderText="Credited Quantity"></asp:BoundField>
        <asp:BoundField DataField="InboundLineId" ReadOnly="true" HeaderText="InboundLineId"></asp:BoundField>
        <asp:BoundField DataField="UnitPrice" HeaderText="<%$ Resources:Default, UnappliedCredit %>" Visible="false"></asp:BoundField>
        <asp:BoundField DataField="ReasonId" HeaderText="<%$ Resources:Default, ReasonId %>" Visible="False"></asp:BoundField>
        <%--<asp:BoundField DataField="Reason" HeaderText="<%$ Resources:Default, Reason %>" ReadOnly="True">
                                </asp:BoundField>--%>
        <asp:TemplateField HeaderText="<%$ Resources:Default,Reason %>">
            <EditItemTemplate>
                <asp:DropDownList ID="DropDownListReasonEdit" runat="server" DataSourceID="ObjectDataSourceReason2"
                    DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'>
                </asp:DropDownList>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:DropDownList ID="DropDownListReasonInsert" runat="server" DataSourceID="ObjectDataSourceReason"
                    DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'>
                </asp:DropDownList>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="LabelBindReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:ObjectDataSource ID="ObjectDataSourceInboundLine" runat="server" TypeName="InboundDocument"
    SelectMethod="SearchInboundLine" UpdateMethod="UpdateInboundLine" InsertMethod="CreateInboundLine"
    DeleteMethod="DeleteInboundLine" >
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:ControlParameter ControlID="hfInboundDocumentId" Name="InboundDocumentId" PropertyName="Value" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="inboundLineId" Type="Int32" />
        <asp:Parameter Name="quantity" Type="Decimal" />
        <asp:Parameter Name="returnQuantity" Type="Decimal" />
        <asp:Parameter Name="unitPrice" Type="Decimal" />
        <asp:Parameter Name="reasonId" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
        <asp:SessionParameter Name="InboundDocumentId" SessionField="InboundDocumentId" Type="Int32" />
        <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" />
        <asp:ControlParameter Name="Quantity" ControlID="TextBoxQuantity" Type="Decimal" />
        <asp:SessionParameter Name="BatchId" SessionField="BatchId" Type="Int32" />
        <asp:SessionParameter Name="ReasonId" SessionField="ReasonId" Type="Int32" />
    </InsertParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundLineId" Type="Int32"></asp:Parameter>
    </DeleteParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceReason2" runat="server" TypeName="Reason"
    SelectMethod="GetReasonsByType">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="CR" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, PrintUpliftment%>"
    OnClick="ButtonPrint_Click" Style="width: auto;" />
<asp:HiddenField ID="hfInboundDocumentId" runat="server" Value="" />