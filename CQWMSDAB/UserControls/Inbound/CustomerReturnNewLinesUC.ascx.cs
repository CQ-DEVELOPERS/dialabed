﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_Inbound_CustomerReturnNewLinesUC : System.Web.UI.UserControl
{
    #region Public Events
    public delegate void ErrorEventHandler(object sender, ExceptionEventArgs e);
    public delegate void SuccessEventHandler(object sender, MessageEventArgs e);
    public event ErrorEventHandler CustomerReturnLinesError;
    public event SuccessEventHandler CustomerReturnLinesSuccess;
    public event EventHandler DoneButtonClicked;
    #endregion

    #region Public Properties
    public GridView CustomerReturnLinesGrid
    {
        get { return GridViewInboundLine; }
        set { GridViewInboundLine = value; }
    }

    public int InboundDocumentId
    {
        get
        {
            int inboundDocumentId;
            if (Int32.TryParse(hfInboundDocumentId.Value, out inboundDocumentId))
                return inboundDocumentId;
            return -1;
        }

        set { hfInboundDocumentId.Value = value.ToString(); }
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GridViewInboundLine.DataBind();
        }
    }
    #endregion

    #region Public Methods
    public void Clear()
    {
        txtQuantity.Text = "0";
        //RadAutoCompleteBoxBatch.SelectedIndex = -1;
        RadAutoCompleteBoxProduct.SelectedIndex = -1;
        RadAutoCompleteBoxReason.SelectedIndex = -1;
        GridViewInboundLine.DataBind();
    }
    #endregion

    #region ButtonInsertAllClick
    protected void ButtonInsertAll_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridViewInboundLine.Rows)
        {
            int inboundLineId = int.Parse(GridViewInboundLine.DataKeys[row.RowIndex].Value.ToString());
            decimal quantity = decimal.Parse(row.Cells[8].Text.ToString());
            decimal returnQuantity = quantity;

            InboundDocument id = new InboundDocument();

            id.UpdateInboundLine(Session["ConnectionStringName"].ToString(), inboundLineId, quantity, returnQuantity, 0, 0);
        }
        if (CustomerReturnLinesSuccess != null)
            CustomerReturnLinesSuccess(this, new MessageEventArgs("All Lines Successfully Inserted"));
        Clear();
        GridViewInboundLine.DataBind();
    }
    #endregion

    #region ButtonInsertLine_Click
    protected void ButtonInsertLine_Click(object sender, EventArgs e)
    {
        try
        {
            if (RadAutoCompleteBoxProduct.SelectedIndex < 0)
            {
                throw new Exception("Please select a Product.");
            }
            int storageUnitId = Int32.Parse(RadAutoCompleteBoxProduct.SelectedValue);


            decimal quantity;
            if (!Decimal.TryParse(txtQuantity.Text, out quantity))
            {
                throw new Exception("Please enter a Quantity.");
            }

            if (quantity == 0)
                throw new Exception("Please enter a Quantity > 0.");

            InboundDocument outboundDocument = new InboundDocument();

            int batchId = -1;
            //if (!Int32.TryParse(RadAutoCompleteBoxBatch.SelectedValue, out batchId))
            //    batchId = -1;

            if (RadAutoCompleteBoxReason.SelectedIndex < 0)
            {
                throw new Exception("Please select a Reason.");
            }

            int reasonId = Int32.Parse(RadAutoCompleteBoxReason.SelectedValue);

            if (outboundDocument.CreateInboundLine(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], InboundDocumentId, storageUnitId, quantity, batchId, reasonId))
            {

                if (CustomerReturnLinesSuccess != null)
                    CustomerReturnLinesSuccess(this, new MessageEventArgs("Line Successfully Inserted"));
                Clear();
            }
            else
            {
                throw new Exception("May not enter duplicate Product /  Batch Combination");
            }
        }
        catch (Exception ex)
        {
            if (CustomerReturnLinesError != null)
                CustomerReturnLinesError(this, new ExceptionEventArgs(ex));
        }
    }
    #endregion

    #region ButtonDeleteLine_Click
    protected void ButtonDeleteLine_Click(object sender, EventArgs e)
    {
        try
        {
            if (GridViewInboundLine.SelectedIndex != -1)
                GridViewInboundLine.DeleteRow(GridViewInboundLine.SelectedIndex);

            if (CustomerReturnLinesSuccess != null)
                CustomerReturnLinesSuccess(this, new MessageEventArgs("Line Successfully Deleted."));

            Clear();
        }
        catch (Exception ex)
        {
            if (CustomerReturnLinesError != null)
                CustomerReturnLinesError(this, new ExceptionEventArgs(ex));
        }
    }
    #endregion

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Inbound/CustomerReturn.aspx";

            Session["ReportName"] = "Driver Upliftment Instructions";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[4];

            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("InboundDocumentId", InboundDocumentId.ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
            if (CustomerReturnLinesSuccess != null)
                CustomerReturnLinesSuccess(this, new MessageEventArgs("Report Printed"));

            Clear();
        }
        catch (Exception ex)
        {
            if (CustomerReturnLinesError != null)
                CustomerReturnLinesError(this, new ExceptionEventArgs(ex));
        }
    }
    #endregion ButtonPrint_Click

    protected void ButtonDone_Click(object sender, EventArgs e)
    {
        if (DoneButtonClicked != null)
            DoneButtonClicked(this, new EventArgs());
    }
}
