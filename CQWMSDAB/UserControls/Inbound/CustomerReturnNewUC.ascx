﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerReturnNewUC.ascx.cs" Inherits="UserControls_Inbound_CustomerReturnNewUC" %>
<%@ Register Src="~/Common/CustomerSearch.ascx" TagName="ExternalCompanySearch" TagPrefix="cqwms" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Panel ID="PanelSearch" runat="server" BackColor="#EFEFEC" Width="480px" >
    <table id="tableCustomerReturn" runat="server">
        <thead>
            <tr>
                <td colspan="2" style="text-align: center;">Customer Return</td>
            </tr>
        </thead>
        <tr>
            <td>
                <asp:Label Text="<%$ Resources:Default,InboundDocumentType %>" ID="lblInboundDocumentType" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadDropDownList ID="DropDownListInboundDocumentType" runat="server" DataSourceID="ObjectDataSourceInboundDocumentType"
                    DataValueField="InboundDocumentTypeId" DataTextField="InboundDocumentType">
                </telerik:RadDropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentType" runat="server" TypeName="InboundDocumentType"
                    SelectMethod="GetInboundDocumentTypeReturn">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="<%$ Resources:Default, DocumentNo %>" ID="Label1" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxOrderNumberEdit" runat="server" Text=""></telerik:RadTextBox>
                <asp:RequiredFieldValidator ID="REQOrderNumberEdit" ControlToValidate="TextBoxOrderNumberEdit" Text="*" runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="<%$ Resources:Default, ReferenceNumber %>" ID="Label3" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxReferenceNumberInsert" runat="server" Text='<%# Bind("ReferenceNumber") %>'></telerik:RadTextBox>
                <asp:RequiredFieldValidator ID="REQReferenceNumberInsert" ControlToValidate="TextBoxReferenceNumberInsert" Text="*" runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                <asp:Label Text="<%$ Resources:Default, Reason %>" ID="Label4" runat="server"></asp:Label>
            </td>
            <td class="auto-style1">
                <telerik:RadDropDownList ID="DropDownListReasonInsert" runat="server" DataSourceID="ObjectDataSourceReason" DataTextField="Reason" DataValueField="ReasonId">
                </telerik:RadDropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="<%$ Resources:Default, ContactPerson %>" ID="Label6" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadDropDownList ID="DropDownListSalesRepInsert" runat="server" DataSourceID="ObjectDataSourceContactListId" DataTextField="ContactPerson" DataValueField="ContactListId">
                </telerik:RadDropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="<%$ Resources:Default, DeliveryDate %>" ID="Label7" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxDeliveryDateInsert" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderFromDateInsert" runat="server" Animated="true"
                    Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxDeliveryDateInsert">
                </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeliveryDateInsert" runat="server" ControlToValidate="TextBoxDeliveryDateInsert" ErrorMessage="Please enter Delivery Date"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="<%$ Resources:Default, Remarks %>" ID="Label2" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxRemarks" runat="server" Text=""></telerik:RadTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="TextBoxReferenceNumberInsert" Text="*" runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right">
                <telerik:RadButton ID="RadButtonInsert" runat="server" Text="Insert" OnClick="RadButtonInsert_Click"></telerik:RadButton>
                <telerik:RadButton ID="RadButtonCancel" CausesValidation="false" runat="server" Text="Cancel" OnClick="RadButtonCancel_Click"></telerik:RadButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch" Radius="10" Color="239, 239, 236" BorderColor="64, 64, 64"
    Enabled="True" />
<asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentUpdate" runat="server" SelectMethod="GetInboundDocument"
    TypeName="InboundDocument" OldValuesParameterFormatString="original_{0}">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:ControlParameter ControlID="hfInboundDocumentId" Name="InboundDocumentId" PropertyName="Value" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
    SelectMethod="GetReasonsByType">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="CR" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceContactListId" runat="server" TypeName="SalesRep"
    SelectMethod="GetSalesReps">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:HiddenField ID="hfInboundDocumentId" runat="server" Value="" />
<asp:HiddenField ID="hfReturnType" runat="server" Value="RET" />
<asp:HiddenField ID="hfExternalCompanyId" runat="server" Value="" />
