using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_MenuItemCultureGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridMenuItemCulture.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridMenuItemCulture_SelectedIndexChanged
    protected void RadGridMenuItemCulture_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridMenuItemCulture.SelectedItems)
            {
                Session["MenuItemCultureId"] = item.GetDataKeyValue("MenuItemCultureId");
            }
        }
        catch { }
    }
    #endregion "RadGridMenuItemCulture_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["MenuItemId"] != null)
          DropDownListMenuItemId.SelectedValue = Session["MenuItemId"].ToString();
        if (Session["CultureId"] != null)
          DropDownListCultureId.SelectedValue = Session["CultureId"].ToString();
    
        RadGridMenuItemCulture.DataBind();
    }
    #endregion BindData
    
}
