<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimeBoxFormView.ascx.cs" Inherits="UserControls_TimeBoxFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>TimeBox</h3>
<asp:FormView ID="FormViewTimeBox" runat="server" DataKeyNames="TimeBoxId" DataSourceID="ObjectDataSourceTimeBox" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    TaskListId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTaskListId" runat="server" DataSourceID="ObjectDataSourceDDLTaskListId"
                        DataTextField="TaskList" DataValueField="TaskListId" SelectedValue='<%# Bind("TaskListId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTaskListId" runat="server" TypeName="StaticInfoTaskList"
                        SelectMethod="ParameterTaskList">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActualHours:
                </td>
                <td>
                    <asp:TextBox ID="ActualHoursTextBox" runat="server" Text='<%# Bind("ActualHours") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    TaskListId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTaskListId" runat="server" DataSourceID="ObjectDataSourceDDLTaskListId"
                        DataTextField="TaskList" DataValueField="TaskListId" SelectedValue='<%# Bind("TaskListId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTaskListId" runat="server" TypeName="StaticInfoTaskList"
                        SelectMethod="ParameterTaskList">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActualHours:
                </td>
                <td>
                    <asp:TextBox ID="ActualHoursTextBox" runat="server" Text='<%# Bind("ActualHours") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    TaskList:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTaskListId" runat="server" DataSourceID="ObjectDataSourceDDLTaskListId"
                        DataTextField="TaskList" DataValueField="TaskListId" SelectedValue='<%# Bind("TaskListId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTaskListId" runat="server" TypeName="StaticInfoTaskList"
                        SelectMethod="ParameterTaskList">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:Label ID="CreateDateLabel" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ActualHours:
                </td>
                <td>
                    <asp:Label ID="ActualHoursLabel" runat="server" Text='<%# Bind("ActualHours") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/TimeBoxGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceTimeBox" runat="server" TypeName="StaticInfoTimeBox"
    SelectMethod="GetTimeBox"
    DeleteMethod="DeleteTimeBox"
    UpdateMethod="UpdateTimeBox"
    InsertMethod="InsertTimeBox">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TimeBoxId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TimeBoxId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TaskListId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CreateDate" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="ActualHours" Type="Double" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="TimeBoxId" QueryStringField="TimeBoxId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TimeBoxId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TaskListId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ActualHours" Type="Double" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
