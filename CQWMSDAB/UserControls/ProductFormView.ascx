<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductFormView.ascx.cs" Inherits="UserControls_ProductFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Product</h3>
<asp:FormView ID="FormViewProduct" runat="server" DataKeyNames="ProductId" DataSourceID="ObjectDataSourceProduct" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ProductCode:
                </td>
                <td>
                    <asp:TextBox ID="ProductCodeTextBox" runat="server" Text='<%# Bind("ProductCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ProductCodeTextBox" ErrorMessage="* Please enter ProductCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Product:
                </td>
                <td>
                    <asp:TextBox ID="ProductTextBox" runat="server" Text='<%# Bind("Product") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ProductTextBox" ErrorMessage="* Please enter Product"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Barcode:
                </td>
                <td>
                    <asp:TextBox ID="BarcodeTextBox" runat="server" Text='<%# Bind("Barcode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    MinimumQuantity:
                </td>
                <td>
                    <asp:TextBox ID="MinimumQuantityTextBox" runat="server" Text='<%# Bind("MinimumQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="MinimumQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReorderQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ReorderQuantityTextBox" runat="server" Text='<%# Bind("ReorderQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="ReorderQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MaximumQuantity:
                </td>
                <td>
                    <asp:TextBox ID="MaximumQuantityTextBox" runat="server" Text='<%# Bind("MaximumQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="MaximumQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CuringPeriodDays:
                </td>
                <td>
                    <asp:TextBox ID="CuringPeriodDaysTextBox" runat="server" Text='<%# Bind("CuringPeriodDays") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="CuringPeriodDaysTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ShelfLifeDays:
                </td>
                <td>
                    <asp:TextBox ID="ShelfLifeDaysTextBox" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="ShelfLifeDaysTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    QualityAssuranceIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="QualityAssuranceIndicatorCheckBox" runat="server" Checked='<%# Bind("QualityAssuranceIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ProductCode:
                </td>
                <td>
                    <asp:TextBox ID="ProductCodeTextBox" runat="server" Text='<%# Bind("ProductCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ProductCodeTextBox" ErrorMessage="* Please enter ProductCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Product:
                </td>
                <td>
                    <asp:TextBox ID="ProductTextBox" runat="server" Text='<%# Bind("Product") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ProductTextBox" ErrorMessage="* Please enter Product"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Barcode:
                </td>
                <td>
                    <asp:TextBox ID="BarcodeTextBox" runat="server" Text='<%# Bind("Barcode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    MinimumQuantity:
                </td>
                <td>
                    <asp:TextBox ID="MinimumQuantityTextBox" runat="server" Text='<%# Bind("MinimumQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="MinimumQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReorderQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ReorderQuantityTextBox" runat="server" Text='<%# Bind("ReorderQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="ReorderQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MaximumQuantity:
                </td>
                <td>
                    <asp:TextBox ID="MaximumQuantityTextBox" runat="server" Text='<%# Bind("MaximumQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="MaximumQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CuringPeriodDays:
                </td>
                <td>
                    <asp:TextBox ID="CuringPeriodDaysTextBox" runat="server" Text='<%# Bind("CuringPeriodDays") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="CuringPeriodDaysTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ShelfLifeDays:
                </td>
                <td>
                    <asp:TextBox ID="ShelfLifeDaysTextBox" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="ShelfLifeDaysTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    QualityAssuranceIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="QualityAssuranceIndicatorCheckBox" runat="server" Checked='<%# Bind("QualityAssuranceIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ProductCode:
                </td>
                <td>
                    <asp:Label ID="ProductCodeLabel" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Product:
                </td>
                <td>
                    <asp:Label ID="ProductLabel" runat="server" Text='<%# Bind("Product") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Barcode:
                </td>
                <td>
                    <asp:Label ID="BarcodeLabel" runat="server" Text='<%# Bind("Barcode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MinimumQuantity:
                </td>
                <td>
                    <asp:Label ID="MinimumQuantityLabel" runat="server" Text='<%# Bind("MinimumQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ReorderQuantity:
                </td>
                <td>
                    <asp:Label ID="ReorderQuantityLabel" runat="server" Text='<%# Bind("ReorderQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MaximumQuantity:
                </td>
                <td>
                    <asp:Label ID="MaximumQuantityLabel" runat="server" Text='<%# Bind("MaximumQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CuringPeriodDays:
                </td>
                <td>
                    <asp:Label ID="CuringPeriodDaysLabel" runat="server" Text='<%# Bind("CuringPeriodDays") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ShelfLifeDays:
                </td>
                <td>
                    <asp:Label ID="ShelfLifeDaysLabel" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    QualityAssuranceIndicator:
                </td>
                <td>
                    <asp:Label ID="QualityAssuranceIndicatorLabel" runat="server" Text='<%# Bind("QualityAssuranceIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ProductGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="StaticInfoProduct"
    SelectMethod="GetProduct"
    DeleteMethod="DeleteProduct"
    UpdateMethod="UpdateProduct"
    InsertMethod="InsertProduct">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ProductId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ProductId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="ProductCode" Type="String" />
        <asp:Parameter Name="Product" Type="String" />
        <asp:Parameter Name="Barcode" Type="String" />
        <asp:Parameter Name="MinimumQuantity" Type="Int32" />
        <asp:Parameter Name="ReorderQuantity" Type="Int32" />
        <asp:Parameter Name="MaximumQuantity" Type="Int32" />
        <asp:Parameter Name="CuringPeriodDays" Type="Int32" />
        <asp:Parameter Name="ShelfLifeDays" Type="Int32" />
        <asp:Parameter Name="QualityAssuranceIndicator" Type="Boolean" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ProductId" QueryStringField="ProductId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ProductId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="ProductCode" Type="String" />
        <asp:Parameter Name="Product" Type="String" />
        <asp:Parameter Name="Barcode" Type="String" />
        <asp:Parameter Name="MinimumQuantity" Type="Int32" />
        <asp:Parameter Name="ReorderQuantity" Type="Int32" />
        <asp:Parameter Name="MaximumQuantity" Type="Int32" />
        <asp:Parameter Name="CuringPeriodDays" Type="Int32" />
        <asp:Parameter Name="ShelfLifeDays" Type="Int32" />
        <asp:Parameter Name="QualityAssuranceIndicator" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
