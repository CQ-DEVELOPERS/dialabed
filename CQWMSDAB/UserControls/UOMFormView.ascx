<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UOMFormView.ascx.cs" Inherits="UserControls_UOMFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>UOM</h3>
<asp:FormView ID="FormViewUOM" runat="server" DataKeyNames="UOMId" DataSourceID="ObjectDataSourceUOM" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    UOM:
                </td>
                <td>
                    <asp:TextBox ID="UOMTextBox" runat="server" Text='<%# Bind("UOM") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="UOMTextBox"
                        ErrorMessage="* Please enter UOM"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    UOMCode:
                </td>
                <td>
                    <asp:TextBox ID="UOMCodeTextBox" runat="server" Text='<%# Bind("UOMCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="UOMCodeTextBox"
                        ErrorMessage="* Please enter UOMCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    UOM:
                </td>
                <td>
                    <asp:TextBox ID="UOMTextBox" runat="server" Text='<%# Bind("UOM") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="UOMTextBox"
                        ErrorMessage="* Please enter UOM"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    UOMCode:
                </td>
                <td>
                    <asp:TextBox ID="UOMCodeTextBox" runat="server" Text='<%# Bind("UOMCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="UOMCodeTextBox"
                        ErrorMessage="* Please enter UOMCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    UOM:
                </td>
                <td>
                    <asp:Label ID="UOMLabel" runat="server" Text='<%# Bind("UOM") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    UOMCode:
                </td>
                <td>
                    <asp:Label ID="UOMCodeLabel" runat="server" Text='<%# Bind("UOMCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/UOMGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceUOM" runat="server" TypeName="StaticInfoUOM"
    SelectMethod="GetUOM"
    DeleteMethod="DeleteUOM"
    UpdateMethod="UpdateUOM"
    InsertMethod="InsertUOM">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="UOMId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="UOMId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="UOM" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="UOMCode" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="UOMId" QueryStringField="UOMId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="UOMId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="UOM" Type="String" />
        <asp:Parameter Name="UOMCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
