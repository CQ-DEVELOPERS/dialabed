<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShipmentIssueGridView.ascx.cs" Inherits="UserControls_ShipmentIssueGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowShipmentIssue()
{
   window.open("ShipmentIssueFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelShipmentId" runat="server" Text="Shipment:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShipment" runat="server" DataSourceID="SqlDataSourceShipmentDDL"
                DataTextField="ShipmentId" DataValueField="ShipmentId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceShipmentDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Shipment_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelIssueId" runat="server" Text="Issue:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListIssue" runat="server" DataSourceID="SqlDataSourceIssueDDL"
                DataTextField="Issue" DataValueField="IssueId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceIssueDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Issue_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowShipmentIssue();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewShipmentIssue" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
ShipmentId
,IssueId
"
                DataSourceID="SqlDataSourceShipmentIssue" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewShipmentIssue_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
            <asp:TemplateField HeaderText="Shipment" meta:resourcekey="Shipment" SortExpression="ShipmentId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListShipment" runat="server" DataSourceID="SqlDataSourceDDLShipment"
                        DataTextField="Shipment" DataValueField="ShipmentId" SelectedValue='<%# Bind("ShipmentId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLShipment" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Shipment_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkShipmentId" runat="server" Text='<%# Bind("Shipment") %>' NavigateUrl="~/Administration/ShipmentGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Shipment" HeaderText="Shipment" ReadOnly="True" SortExpression="Shipment" Visible="False" />
            <asp:TemplateField HeaderText="Issue" meta:resourcekey="Issue" SortExpression="IssueId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListIssue" runat="server" DataSourceID="SqlDataSourceDDLIssue"
                        DataTextField="Issue" DataValueField="IssueId" SelectedValue='<%# Bind("IssueId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLIssue" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Issue_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkIssueId" runat="server" Text='<%# Bind("Issue") %>' NavigateUrl="~/Administration/IssueGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Issue" HeaderText="Issue" ReadOnly="True" SortExpression="Issue" Visible="False" />
                        <asp:BoundField DataField="DropSequence" HeaderText="DropSequence" SortExpression="DropSequence" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceShipmentIssue" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_ShipmentIssue_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_ShipmentIssue_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_ShipmentIssue_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_ShipmentIssue_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListShipment" DefaultValue="" Name="ShipmentId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListIssue" DefaultValue="" Name="IssueId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="ShipmentId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="ShipmentId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="DropSequence" Type="Int16" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="ShipmentId" Type="Int32" />
        <asp:Parameter Direction="InputOutput" Name="IssueId" Type="Int32" />
        <asp:Parameter Name="DropSequence" Type="Int16" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
