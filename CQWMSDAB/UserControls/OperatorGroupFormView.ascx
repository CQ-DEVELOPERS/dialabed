<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperatorGroupFormView.ascx.cs" Inherits="UserControls_OperatorGroupFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>OperatorGroup</h3>
<asp:FormView ID="FormViewOperatorGroup" runat="server" DataKeyNames="OperatorGroupId" DataSourceID="ObjectDataSourceOperatorGroup" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:TextBox ID="OperatorGroupTextBox" runat="server" Text='<%# Bind("OperatorGroup") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="OperatorGroupTextBox"
                        ErrorMessage="* Please enter OperatorGroup"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RestrictedAreaIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="RestrictedAreaIndicatorCheckBox" runat="server" Checked='<%# Bind("RestrictedAreaIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroupCode:
                </td>
                <td>
                    <asp:TextBox ID="OperatorGroupCodeTextBox" runat="server" Text='<%# Bind("OperatorGroupCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:TextBox ID="OperatorGroupTextBox" runat="server" Text='<%# Bind("OperatorGroup") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="OperatorGroupTextBox"
                        ErrorMessage="* Please enter OperatorGroup"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RestrictedAreaIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="RestrictedAreaIndicatorCheckBox" runat="server" Checked='<%# Bind("RestrictedAreaIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroupCode:
                </td>
                <td>
                    <asp:TextBox ID="OperatorGroupCodeTextBox" runat="server" Text='<%# Bind("OperatorGroupCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:Label ID="OperatorGroupLabel" runat="server" Text='<%# Bind("OperatorGroup") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RestrictedAreaIndicator:
                </td>
                <td>
                    <asp:Label ID="RestrictedAreaIndicatorLabel" runat="server" Text='<%# Bind("RestrictedAreaIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroupCode:
                </td>
                <td>
                    <asp:Label ID="OperatorGroupCodeLabel" runat="server" Text='<%# Bind("OperatorGroupCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/OperatorGroupGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceOperatorGroup" runat="server" TypeName="StaticInfoOperatorGroup"
    SelectMethod="GetOperatorGroup"
    DeleteMethod="DeleteOperatorGroup"
    UpdateMethod="UpdateOperatorGroup"
    InsertMethod="InsertOperatorGroup">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OperatorGroup" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="RestrictedAreaIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="OperatorGroupCode" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="OperatorGroupId" QueryStringField="OperatorGroupId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OperatorGroup" Type="String" />
        <asp:Parameter Name="RestrictedAreaIndicator" Type="Boolean" />
        <asp:Parameter Name="OperatorGroupCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
