<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PackGridView.ascx.cs" Inherits="UserControls_PackGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowPack()
{
   window.open("PackFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelStorageUnitId" runat="server" Text="StorageUnit:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="SqlDataSourceStorageUnitDDL"
                DataTextField="StorageUnit" DataValueField="StorageUnitId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStorageUnitDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_StorageUnit_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelPackTypeId" runat="server" Text="PackType:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListPackType" runat="server" DataSourceID="SqlDataSourcePackTypeDDL"
                DataTextField="PackType" DataValueField="PackTypeId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourcePackTypeDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_PackType_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowPack();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewPack" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
PackId
"
                DataSourceID="SqlDataSourcePack" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewPack_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="PackId" HeaderText="PackId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="PackId" />
            <asp:TemplateField HeaderText="StorageUnit" meta:resourcekey="StorageUnit" SortExpression="StorageUnitId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="SqlDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStorageUnit" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_StorageUnit_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStorageUnitId" runat="server" Text='<%# Bind("StorageUnit") %>' NavigateUrl="~/Administration/StorageUnitGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StorageUnit" HeaderText="StorageUnit" ReadOnly="True" SortExpression="StorageUnit" Visible="False" />
            <asp:TemplateField HeaderText="PackType" meta:resourcekey="PackType" SortExpression="PackTypeId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListPackType" runat="server" DataSourceID="SqlDataSourceDDLPackType"
                        DataTextField="PackType" DataValueField="PackTypeId" SelectedValue='<%# Bind("PackTypeId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLPackType" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_PackType_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkPackTypeId" runat="server" Text='<%# Bind("PackType") %>' NavigateUrl="~/Administration/PackTypeGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PackType" HeaderText="PackType" ReadOnly="True" SortExpression="PackType" Visible="False" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                        <asp:BoundField DataField="Barcode" HeaderText="Barcode" SortExpression="Barcode" />
                        <asp:BoundField DataField="Length" HeaderText="Length" SortExpression="Length" />
                        <asp:BoundField DataField="Width" HeaderText="Width" SortExpression="Width" />
                        <asp:BoundField DataField="Height" HeaderText="Height" SortExpression="Height" />
                        <asp:BoundField DataField="Volume" HeaderText="Volume" SortExpression="Volume" />
                        <asp:BoundField DataField="Weight" HeaderText="Weight" SortExpression="Weight" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourcePack" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_Pack_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_Pack_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_Pack_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_Pack_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStorageUnit" DefaultValue="" Name="StorageUnitId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListPackType" DefaultValue="" Name="PackTypeId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="PackId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="PackId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="PackTypeId" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="Barcode" Type="String" />
        <asp:Parameter Name="Length" Type="Int16" />
        <asp:Parameter Name="Width" Type="Int16" />
        <asp:Parameter Name="Height" Type="Int16" />
        <asp:Parameter Name="Volume" Type="Decimal" />
        <asp:Parameter Name="Weight" Type="Decimal" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="PackId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="PackTypeId" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="Barcode" Type="String" />
        <asp:Parameter Name="Length" Type="Int16" />
        <asp:Parameter Name="Width" Type="Int16" />
        <asp:Parameter Name="Height" Type="Int16" />
        <asp:Parameter Name="Volume" Type="Decimal" />
        <asp:Parameter Name="Weight" Type="Decimal" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
