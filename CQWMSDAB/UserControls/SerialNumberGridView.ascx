<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SerialNumberGridView.ascx.cs" Inherits="UserControls_SerialNumberGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowSerialNumber()
{
   window.open("SerialNumberFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelStorageUnitId" runat="server" Text="StorageUnit:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="SqlDataSourceStorageUnitDDL"
                DataTextField="StorageUnit" DataValueField="StorageUnitId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStorageUnitDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_StorageUnit_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelLocationId" runat="server" Text="Location:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="SqlDataSourceLocationDDL"
                DataTextField="Location" DataValueField="LocationId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceLocationDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Location_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReceiptId" runat="server" Text="Receipt:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="SqlDataSourceReceiptDDL"
                DataTextField="Receipt" DataValueField="ReceiptId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceReceiptDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Receipt_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelSerialNumber" runat="server" Text="SerialNumber:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxSerialNumber" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowSerialNumber();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewSerialNumber" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
SerialNumberId
"
                DataSourceID="SqlDataSourceSerialNumber" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewSerialNumber_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="SerialNumberId" HeaderText="SerialNumberId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="SerialNumberId" />
            <asp:TemplateField HeaderText="StorageUnit" meta:resourcekey="StorageUnit" SortExpression="StorageUnitId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="SqlDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStorageUnit" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_StorageUnit_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStorageUnitId" runat="server" Text='<%# Bind("StorageUnit") %>' NavigateUrl="~/Administration/StorageUnitGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StorageUnit" HeaderText="StorageUnit" ReadOnly="True" SortExpression="StorageUnit" Visible="False" />
            <asp:TemplateField HeaderText="Location" meta:resourcekey="Location" SortExpression="LocationId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="SqlDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLLocation" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Location_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkLocationId" runat="server" Text='<%# Bind("Location") %>' NavigateUrl="~/Administration/LocationGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Location" HeaderText="Location" ReadOnly="True" SortExpression="Location" Visible="False" />
            <asp:TemplateField HeaderText="Receipt" meta:resourcekey="Receipt" SortExpression="ReceiptId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="SqlDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLReceipt" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Receipt_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkReceiptId" runat="server" Text='<%# Bind("Receipt") %>' NavigateUrl="~/Administration/ReceiptGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Receipt" HeaderText="Receipt" ReadOnly="True" SortExpression="Receipt" Visible="False" />
                        <asp:BoundField DataField="StoreInstructionId" HeaderText="StoreInstructionId" SortExpression="StoreInstructionId" />
                        <asp:BoundField DataField="PickInstructionId" HeaderText="PickInstructionId" SortExpression="PickInstructionId" />
                        <asp:BoundField DataField="SerialNumber" HeaderText="SerialNumber" SortExpression="SerialNumber" />
                        <asp:TemplateField HeaderText="ReceivedDate" SortExpression="ReceivedDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonReceivedDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('ReceivedDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelReceivedDate" runat="server" Text='<%# Bind("ReceivedDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DespatchDate" SortExpression="DespatchDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonDespatchDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('DespatchDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelDespatchDate" runat="server" Text='<%# Bind("DespatchDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceSerialNumber" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_SerialNumber_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_SerialNumber_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_SerialNumber_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_SerialNumber_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStorageUnit" DefaultValue="" Name="StorageUnitId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListLocation" DefaultValue="" Name="LocationId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListReceipt" DefaultValue="" Name="ReceiptId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="TextBoxSerialNumber" DefaultValue="%" Name="SerialNumber" PropertyName="Text" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="SerialNumberId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="SerialNumberId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="StoreInstructionId" Type="Int32" />
        <asp:Parameter Name="PickInstructionId" Type="Int32" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="ReceivedDate" Type="DateTime" />
        <asp:Parameter Name="DespatchDate" Type="DateTime" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="SerialNumberId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="StoreInstructionId" Type="Int32" />
        <asp:Parameter Name="PickInstructionId" Type="Int32" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="ReceivedDate" Type="DateTime" />
        <asp:Parameter Name="DespatchDate" Type="DateTime" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
