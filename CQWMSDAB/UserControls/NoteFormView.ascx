<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NoteFormView.ascx.cs" Inherits="UserControls_NoteFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Note</h3>
<asp:FormView ID="FormViewNote" runat="server" DataKeyNames="NoteId" DataSourceID="ObjectDataSourceNote" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    NoteCode:
                </td>
                <td>
                    <asp:TextBox ID="NoteCodeTextBox" runat="server" Text='<%# Bind("NoteCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Note:
                </td>
                <td>
                    <asp:TextBox ID="NoteTextBox" runat="server" Text='<%# Bind("Note") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    NoteCode:
                </td>
                <td>
                    <asp:TextBox ID="NoteCodeTextBox" runat="server" Text='<%# Bind("NoteCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Note:
                </td>
                <td>
                    <asp:TextBox ID="NoteTextBox" runat="server" Text='<%# Bind("Note") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    NoteCode:
                </td>
                <td>
                    <asp:Label ID="NoteCodeLabel" runat="server" Text='<%# Bind("NoteCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Note:
                </td>
                <td>
                    <asp:Label ID="NoteLabel" runat="server" Text='<%# Bind("Note") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/NoteGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceNote" runat="server" TypeName="StaticInfoNote"
    SelectMethod="GetNote"
    DeleteMethod="DeleteNote"
    UpdateMethod="UpdateNote"
    InsertMethod="InsertNote">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="NoteId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="NoteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Note" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="NoteId" QueryStringField="NoteId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="NoteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteCode" Type="String" />
        <asp:Parameter Name="Note" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
