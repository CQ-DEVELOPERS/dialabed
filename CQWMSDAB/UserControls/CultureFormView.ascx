<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CultureFormView.ascx.cs" Inherits="UserControls_CultureFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Culture</h3>
<asp:FormView ID="FormViewCulture" runat="server" DataKeyNames="CultureId" DataSourceID="ObjectDataSourceCulture" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    CultureName:
                </td>
                <td>
                    <asp:TextBox ID="CultureNameTextBox" runat="server" Text='<%# Bind("CultureName") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CultureNameTextBox"
                        ErrorMessage="* Please enter CultureName"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    CultureName:
                </td>
                <td>
                    <asp:TextBox ID="CultureNameTextBox" runat="server" Text='<%# Bind("CultureName") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CultureNameTextBox"
                        ErrorMessage="* Please enter CultureName"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    CultureName:
                </td>
                <td>
                    <asp:Label ID="CultureNameLabel" runat="server" Text='<%# Bind("CultureName") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/CultureGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceCulture" runat="server" TypeName="StaticInfoCulture"
    SelectMethod="GetCulture"
    DeleteMethod="DeleteCulture"
    UpdateMethod="UpdateCulture"
    InsertMethod="InsertCulture">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CultureId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CultureId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CultureName" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="CultureId" QueryStringField="CultureId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CultureId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CultureName" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
