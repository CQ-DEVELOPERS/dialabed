using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_OutboundDocumentTypeGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridOutboundDocumentType.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridOutboundDocumentType_SelectedIndexChanged
    protected void RadGridOutboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridOutboundDocumentType.SelectedItems)
            {
                Session["OutboundDocumentTypeId"] = item.GetDataKeyValue("OutboundDocumentTypeId");
            }
        }
        catch { }
    }
    #endregion "RadGridOutboundDocumentType_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["PriorityId"] != null)
          DropDownListPriorityId.SelectedValue = Session["PriorityId"].ToString();
        if (Session["CheckingLane"] != null)
          DropDownListCheckingLane.SelectedValue = Session["CheckingLane"].ToString();
        if (Session["DespatchBay"] != null)
          DropDownListDespatchBay.SelectedValue = Session["DespatchBay"].ToString();
    
        RadGridOutboundDocumentType.DataBind();
    }
    #endregion BindData
    
}
