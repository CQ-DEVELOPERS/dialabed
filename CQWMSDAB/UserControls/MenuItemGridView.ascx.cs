using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_MenuItemGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridMenuItem.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridMenuItem_SelectedIndexChanged
    protected void RadGridMenuItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridMenuItem.SelectedItems)
            {
                Session["MenuItemId"] = item.GetDataKeyValue("MenuItemId");
            }
        }
        catch { }
    }
    #endregion "RadGridMenuItem_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["MenuId"] != null)
          DropDownListMenuId.SelectedValue = Session["MenuId"].ToString();
        if (Session["ParentMenuItemId"] != null)
          DropDownListParentMenuItemId.SelectedValue = Session["ParentMenuItemId"].ToString();
    
        RadGridMenuItem.DataBind();
    }
    #endregion BindData
    
}
