using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_OperatorGroupMenuItemGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridOperatorGroupMenuItem.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridOperatorGroupMenuItem_SelectedIndexChanged
    protected void RadGridOperatorGroupMenuItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridOperatorGroupMenuItem.SelectedItems)
            {
                Session["OperatorGroupMenuItemId"] = item.GetDataKeyValue("OperatorGroupMenuItemId");
            }
        }
        catch { }
    }
    #endregion "RadGridOperatorGroupMenuItem_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["OperatorGroupId"] != null)
          DropDownListOperatorGroupId.SelectedValue = Session["OperatorGroupId"].ToString();
        if (Session["MenuItemId"] != null)
          DropDownListMenuItemId.SelectedValue = Session["MenuItemId"].ToString();
    
        RadGridOperatorGroupMenuItem.DataBind();
    }
    #endregion BindData
    
}
