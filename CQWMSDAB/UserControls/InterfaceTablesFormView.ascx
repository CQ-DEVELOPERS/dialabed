<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceTablesFormView.ascx.cs" Inherits="UserControls_InterfaceTablesFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>InterfaceTables</h3>
<asp:FormView ID="FormViewInterfaceTables" runat="server" DataKeyNames="InterfaceTablesId" DataSourceID="ObjectDataSourceInterfaceTables" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    HeaderTable:
                </td>
                <td>
                    <asp:TextBox ID="HeaderTableTextBox" runat="server" Text='<%# Bind("HeaderTable") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailTable:
                </td>
                <td>
                    <asp:TextBox ID="DetailTableTextBox" runat="server" Text='<%# Bind("DetailTable") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderTableKey:
                </td>
                <td>
                    <asp:TextBox ID="HeaderTableKeyTextBox" runat="server" Text='<%# Bind("HeaderTableKey") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Description1:
                </td>
                <td>
                    <asp:TextBox ID="Description1TextBox" runat="server" Text='<%# Bind("Description1") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Description2:
                </td>
                <td>
                    <asp:TextBox ID="Description2TextBox" runat="server" Text='<%# Bind("Description2") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField1:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField1TextBox" runat="server" Text='<%# Bind("HeaderField1") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField2:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField2TextBox" runat="server" Text='<%# Bind("HeaderField2") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField3:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField3TextBox" runat="server" Text='<%# Bind("HeaderField3") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField4:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField4TextBox" runat="server" Text='<%# Bind("HeaderField4") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField5:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField5TextBox" runat="server" Text='<%# Bind("HeaderField5") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField6:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField6TextBox" runat="server" Text='<%# Bind("HeaderField6") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField7:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField7TextBox" runat="server" Text='<%# Bind("HeaderField7") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField8:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField8TextBox" runat="server" Text='<%# Bind("HeaderField8") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField1:
                </td>
                <td>
                    <asp:TextBox ID="DetailField1TextBox" runat="server" Text='<%# Bind("DetailField1") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField2:
                </td>
                <td>
                    <asp:TextBox ID="DetailField2TextBox" runat="server" Text='<%# Bind("DetailField2") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField3:
                </td>
                <td>
                    <asp:TextBox ID="DetailField3TextBox" runat="server" Text='<%# Bind("DetailField3") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField4:
                </td>
                <td>
                    <asp:TextBox ID="DetailField4TextBox" runat="server" Text='<%# Bind("DetailField4") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField5:
                </td>
                <td>
                    <asp:TextBox ID="DetailField5TextBox" runat="server" Text='<%# Bind("DetailField5") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField6:
                </td>
                <td>
                    <asp:TextBox ID="DetailField6TextBox" runat="server" Text='<%# Bind("DetailField6") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField7:
                </td>
                <td>
                    <asp:TextBox ID="DetailField7TextBox" runat="server" Text='<%# Bind("DetailField7") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField8:
                </td>
                <td>
                    <asp:TextBox ID="DetailField8TextBox" runat="server" Text='<%# Bind("DetailField8") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StartId:
                </td>
                <td>
                    <asp:TextBox ID="StartIdTextBox" runat="server" Text='<%# Bind("StartId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator23" runat="server" ControlToValidate="StartIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InsertDetail:
                </td>
                <td>
                    <asp:TextBox ID="InsertDetailTextBox" runat="server" Text='<%# Bind("InsertDetail") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator24" runat="server" ControlToValidate="InsertDetailTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    GroupByKey:
                </td>
                <td>
                    <asp:TextBox ID="GroupByKeyTextBox" runat="server" Text='<%# Bind("GroupByKey") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AltGroupByKey:
                </td>
                <td>
                    <asp:TextBox ID="AltGroupByKeyTextBox" runat="server" Text='<%# Bind("AltGroupByKey") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    HeaderTable:
                </td>
                <td>
                    <asp:TextBox ID="HeaderTableTextBox" runat="server" Text='<%# Bind("HeaderTable") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailTable:
                </td>
                <td>
                    <asp:TextBox ID="DetailTableTextBox" runat="server" Text='<%# Bind("DetailTable") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderTableKey:
                </td>
                <td>
                    <asp:TextBox ID="HeaderTableKeyTextBox" runat="server" Text='<%# Bind("HeaderTableKey") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Description1:
                </td>
                <td>
                    <asp:TextBox ID="Description1TextBox" runat="server" Text='<%# Bind("Description1") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Description2:
                </td>
                <td>
                    <asp:TextBox ID="Description2TextBox" runat="server" Text='<%# Bind("Description2") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField1:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField1TextBox" runat="server" Text='<%# Bind("HeaderField1") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField2:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField2TextBox" runat="server" Text='<%# Bind("HeaderField2") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField3:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField3TextBox" runat="server" Text='<%# Bind("HeaderField3") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField4:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField4TextBox" runat="server" Text='<%# Bind("HeaderField4") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField5:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField5TextBox" runat="server" Text='<%# Bind("HeaderField5") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField6:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField6TextBox" runat="server" Text='<%# Bind("HeaderField6") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField7:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField7TextBox" runat="server" Text='<%# Bind("HeaderField7") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField8:
                </td>
                <td>
                    <asp:TextBox ID="HeaderField8TextBox" runat="server" Text='<%# Bind("HeaderField8") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField1:
                </td>
                <td>
                    <asp:TextBox ID="DetailField1TextBox" runat="server" Text='<%# Bind("DetailField1") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField2:
                </td>
                <td>
                    <asp:TextBox ID="DetailField2TextBox" runat="server" Text='<%# Bind("DetailField2") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField3:
                </td>
                <td>
                    <asp:TextBox ID="DetailField3TextBox" runat="server" Text='<%# Bind("DetailField3") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField4:
                </td>
                <td>
                    <asp:TextBox ID="DetailField4TextBox" runat="server" Text='<%# Bind("DetailField4") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField5:
                </td>
                <td>
                    <asp:TextBox ID="DetailField5TextBox" runat="server" Text='<%# Bind("DetailField5") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField6:
                </td>
                <td>
                    <asp:TextBox ID="DetailField6TextBox" runat="server" Text='<%# Bind("DetailField6") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField7:
                </td>
                <td>
                    <asp:TextBox ID="DetailField7TextBox" runat="server" Text='<%# Bind("DetailField7") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField8:
                </td>
                <td>
                    <asp:TextBox ID="DetailField8TextBox" runat="server" Text='<%# Bind("DetailField8") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StartId:
                </td>
                <td>
                    <asp:TextBox ID="StartIdTextBox" runat="server" Text='<%# Bind("StartId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator23" runat="server" ControlToValidate="StartIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InsertDetail:
                </td>
                <td>
                    <asp:TextBox ID="InsertDetailTextBox" runat="server" Text='<%# Bind("InsertDetail") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator24" runat="server" ControlToValidate="InsertDetailTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    GroupByKey:
                </td>
                <td>
                    <asp:TextBox ID="GroupByKeyTextBox" runat="server" Text='<%# Bind("GroupByKey") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    AltGroupByKey:
                </td>
                <td>
                    <asp:TextBox ID="AltGroupByKeyTextBox" runat="server" Text='<%# Bind("AltGroupByKey") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    HeaderTable:
                </td>
                <td>
                    <asp:Label ID="HeaderTableLabel" runat="server" Text='<%# Bind("HeaderTable") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailTable:
                </td>
                <td>
                    <asp:Label ID="DetailTableLabel" runat="server" Text='<%# Bind("DetailTable") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderTableKey:
                </td>
                <td>
                    <asp:Label ID="HeaderTableKeyLabel" runat="server" Text='<%# Bind("HeaderTableKey") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Description1:
                </td>
                <td>
                    <asp:Label ID="Description1Label" runat="server" Text='<%# Bind("Description1") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Description2:
                </td>
                <td>
                    <asp:Label ID="Description2Label" runat="server" Text='<%# Bind("Description2") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField1:
                </td>
                <td>
                    <asp:Label ID="HeaderField1Label" runat="server" Text='<%# Bind("HeaderField1") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField2:
                </td>
                <td>
                    <asp:Label ID="HeaderField2Label" runat="server" Text='<%# Bind("HeaderField2") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField3:
                </td>
                <td>
                    <asp:Label ID="HeaderField3Label" runat="server" Text='<%# Bind("HeaderField3") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField4:
                </td>
                <td>
                    <asp:Label ID="HeaderField4Label" runat="server" Text='<%# Bind("HeaderField4") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField5:
                </td>
                <td>
                    <asp:Label ID="HeaderField5Label" runat="server" Text='<%# Bind("HeaderField5") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField6:
                </td>
                <td>
                    <asp:Label ID="HeaderField6Label" runat="server" Text='<%# Bind("HeaderField6") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField7:
                </td>
                <td>
                    <asp:Label ID="HeaderField7Label" runat="server" Text='<%# Bind("HeaderField7") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HeaderField8:
                </td>
                <td>
                    <asp:Label ID="HeaderField8Label" runat="server" Text='<%# Bind("HeaderField8") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField1:
                </td>
                <td>
                    <asp:Label ID="DetailField1Label" runat="server" Text='<%# Bind("DetailField1") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField2:
                </td>
                <td>
                    <asp:Label ID="DetailField2Label" runat="server" Text='<%# Bind("DetailField2") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField3:
                </td>
                <td>
                    <asp:Label ID="DetailField3Label" runat="server" Text='<%# Bind("DetailField3") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField4:
                </td>
                <td>
                    <asp:Label ID="DetailField4Label" runat="server" Text='<%# Bind("DetailField4") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField5:
                </td>
                <td>
                    <asp:Label ID="DetailField5Label" runat="server" Text='<%# Bind("DetailField5") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField6:
                </td>
                <td>
                    <asp:Label ID="DetailField6Label" runat="server" Text='<%# Bind("DetailField6") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField7:
                </td>
                <td>
                    <asp:Label ID="DetailField7Label" runat="server" Text='<%# Bind("DetailField7") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DetailField8:
                </td>
                <td>
                    <asp:Label ID="DetailField8Label" runat="server" Text='<%# Bind("DetailField8") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StartId:
                </td>
                <td>
                    <asp:Label ID="StartIdLabel" runat="server" Text='<%# Bind("StartId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    InsertDetail:
                </td>
                <td>
                    <asp:Label ID="InsertDetailLabel" runat="server" Text='<%# Bind("InsertDetail") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    GroupByKey:
                </td>
                <td>
                    <asp:Label ID="GroupByKeyLabel" runat="server" Text='<%# Bind("GroupByKey") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AltGroupByKey:
                </td>
                <td>
                    <asp:Label ID="AltGroupByKeyLabel" runat="server" Text='<%# Bind("AltGroupByKey") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/InterfaceTablesGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceInterfaceTables" runat="server" TypeName="StaticInfoInterfaceTables"
    SelectMethod="GetInterfaceTables"
    DeleteMethod="DeleteInterfaceTables"
    UpdateMethod="UpdateInterfaceTables"
    InsertMethod="InsertInterfaceTables">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InterfaceTablesId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InterfaceTableId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="HeaderTable" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailTable" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderTableKey" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Description1" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Description2" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderField1" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderField2" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderField3" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderField4" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderField5" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderField6" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderField7" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HeaderField8" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailField1" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailField2" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailField3" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailField4" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailField5" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailField6" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailField7" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DetailField8" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="StartId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="InsertDetail" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="GroupByKey" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="AltGroupByKey" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="InterfaceTableId" QueryStringField="InterfaceTableId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InterfaceTableId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="HeaderTable" Type="String" />
        <asp:Parameter Name="DetailTable" Type="String" />
        <asp:Parameter Name="HeaderTableKey" Type="String" />
        <asp:Parameter Name="Description1" Type="String" />
        <asp:Parameter Name="Description2" Type="String" />
        <asp:Parameter Name="HeaderField1" Type="String" />
        <asp:Parameter Name="HeaderField2" Type="String" />
        <asp:Parameter Name="HeaderField3" Type="String" />
        <asp:Parameter Name="HeaderField4" Type="String" />
        <asp:Parameter Name="HeaderField5" Type="String" />
        <asp:Parameter Name="HeaderField6" Type="String" />
        <asp:Parameter Name="HeaderField7" Type="String" />
        <asp:Parameter Name="HeaderField8" Type="String" />
        <asp:Parameter Name="DetailField1" Type="String" />
        <asp:Parameter Name="DetailField2" Type="String" />
        <asp:Parameter Name="DetailField3" Type="String" />
        <asp:Parameter Name="DetailField4" Type="String" />
        <asp:Parameter Name="DetailField5" Type="String" />
        <asp:Parameter Name="DetailField6" Type="String" />
        <asp:Parameter Name="DetailField7" Type="String" />
        <asp:Parameter Name="DetailField8" Type="String" />
        <asp:Parameter Name="StartId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="InsertDetail" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="GroupByKey" Type="String" />
        <asp:Parameter Name="AltGroupByKey" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
