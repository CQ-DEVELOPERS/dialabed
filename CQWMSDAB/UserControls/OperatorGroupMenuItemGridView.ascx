<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperatorGroupMenuItemGridView.ascx.cs" Inherits="UserControls_OperatorGroupMenuItemGridView" %>
 
<table style="background-color:lightgray; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListOperatorGroupId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroupId"
                DataTextField="OperatorGroup" DataValueField="OperatorGroupId" Label="OperatorGroup" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroupId" runat="server" TypeName="StaticInfoOperatorGroup"
                SelectMethod="ParameterOperatorGroup">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLMenuItemId"
                DataTextField="MenuItem" DataValueField="MenuItemId" Label="MenuItem" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                SelectMethod="ParameterMenuItem">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
        </td>
    </tr>
</table>
 
<telerik:RadGrid ID="RadGridOperatorGroupMenuItem" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true"
    AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="30" DataSourceID="ObjectDataSourceOperatorGroupMenuItem" OnSelectedIndexChanged="RadGridOperatorGroupMenuItem_SelectedIndexChanged" Skin="Metro">
<PagerStyle Mode="NumericPages"></PagerStyle>
<MasterTableView DataKeyNames="
OperatorGroupId
,MenuItemId
" DataSourceID="ObjectDataSourceOperatorGroupMenuItem"
    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
    InsertItemPageIndexAction="ShowItemOnFirstPage">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListOperatorGroupId" ListTextField="OperatorGroup" ListValueField="OperatorGroupId" DataSourceID="ObjectDataSourceOperatorGroupId" DataField="OperatorGroupId" HeaderText="OperatorGroup">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListMenuItemId" ListTextField="MenuItem" ListValueField="MenuItemId" DataSourceID="ObjectDataSourceMenuItemId" DataField="MenuItemId" HeaderText="MenuItem">
            </telerik:GridDropDownColumn>
                        <telerik:GridCheckBoxColumn DataField="Access" HeaderText="Access" SortExpression="Access" UniqueName="Access"></telerik:GridCheckBoxColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
       </ClientSettings>
    <GroupingSettings ShowUnGroupButton="True" />
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectDataSourceOperatorGroupId" runat="server" TypeName="StaticInfoOperatorGroup"
    SelectMethod="ParameterOperatorGroup">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
    SelectMethod="ParameterMenuItem">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
 
<asp:ObjectDataSource ID="ObjectDataSourceOperatorGroupMenuItem" runat="server" TypeName="StaticInfoOperatorGroupMenuItem"
    DeleteMethod="DeleteOperatorGroupMenuItem"
    InsertMethod="InsertOperatorGroupMenuItem"
    SelectMethod="SearchOperatorGroupMenuItem"
    UpdateMethod="UpdateOperatorGroupMenuItem">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:ControlParameter ControlID="DropDownListOperatorGroupId" DefaultValue="-1" Name="OperatorGroupId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListMenuItemId" DefaultValue="-1" Name="MenuItemId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
        <asp:Parameter Name="MenuItemId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Access" Type="Boolean" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="OperatorGroupId" Type="Int32" />
        <asp:Parameter Direction="InputOutput" Name="MenuItemId" Type="Int32" />
        <asp:Parameter Name="Access" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
