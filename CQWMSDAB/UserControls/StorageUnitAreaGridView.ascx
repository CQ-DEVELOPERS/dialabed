<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageUnitAreaGridView.ascx.cs" Inherits="UserControls_StorageUnitAreaGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowStorageUnitArea()
{
   window.open("StorageUnitAreaFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelStorageUnitId" runat="server" Text="StorageUnit:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="SqlDataSourceStorageUnitDDL"
                DataTextField="StorageUnit" DataValueField="StorageUnitId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStorageUnitDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_StorageUnit_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelAreaId" runat="server" Text="Area:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="SqlDataSourceAreaDDL"
                DataTextField="Area" DataValueField="AreaId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceAreaDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Area_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowStorageUnitArea();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewStorageUnitArea" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
StorageUnitId
,AreaId
"
                DataSourceID="SqlDataSourceStorageUnitArea" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewStorageUnitArea_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
            <asp:TemplateField HeaderText="StorageUnit" meta:resourcekey="StorageUnit" SortExpression="StorageUnitId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="SqlDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStorageUnit" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_StorageUnit_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStorageUnitId" runat="server" Text='<%# Bind("StorageUnit") %>' NavigateUrl="~/Administration/StorageUnitGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StorageUnit" HeaderText="StorageUnit" ReadOnly="True" SortExpression="StorageUnit" Visible="False" />
            <asp:TemplateField HeaderText="Area" meta:resourcekey="Area" SortExpression="AreaId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="SqlDataSourceDDLArea"
                        DataTextField="Area" DataValueField="AreaId" SelectedValue='<%# Bind("AreaId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLArea" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Area_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkAreaId" runat="server" Text='<%# Bind("Area") %>' NavigateUrl="~/Administration/AreaGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Area" HeaderText="Area" ReadOnly="True" SortExpression="Area" Visible="False" />
                        <asp:BoundField DataField="StoreOrder" HeaderText="StoreOrder" SortExpression="StoreOrder" />
                        <asp:BoundField DataField="PickOrder" HeaderText="PickOrder" SortExpression="PickOrder" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceStorageUnitArea" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_StorageUnitArea_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_StorageUnitArea_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_StorageUnitArea_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_StorageUnitArea_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStorageUnit" DefaultValue="" Name="StorageUnitId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListArea" DefaultValue="" Name="AreaId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="AreaId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:Parameter Name="StoreOrder" Type="Int32" />
        <asp:Parameter Name="PickOrder" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Direction="InputOutput" Name="AreaId" Type="Int32" />
        <asp:Parameter Name="StoreOrder" Type="Int32" />
        <asp:Parameter Name="PickOrder" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
