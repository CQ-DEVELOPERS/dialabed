<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductGridView.ascx.cs" Inherits="UserControls_ProductGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowProduct()
{
   window.open("ProductFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelStatusId" runat="server" Text="Status:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceStatusDDL"
                DataTextField="Status" DataValueField="StatusId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStatusDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Status_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelProductCode" runat="server" Text="ProductCode:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelProduct" runat="server" Text="Product:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowProduct();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewProduct" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
ProductId
"
                DataSourceID="SqlDataSourceProduct" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewProduct_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="ProductId" HeaderText="ProductId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="ProductId" />
            <asp:TemplateField HeaderText="Status" meta:resourcekey="Status" SortExpression="StatusId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Status_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStatusId" runat="server" Text='<%# Bind("Status") %>' NavigateUrl="~/Administration/StatusGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" Visible="False" />
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" />
                        <asp:BoundField DataField="Product" HeaderText="Product" SortExpression="Product" />
                        <asp:BoundField DataField="Barcode" HeaderText="Barcode" SortExpression="Barcode" />
                        <asp:BoundField DataField="MinimumQuantity" HeaderText="MinimumQuantity" SortExpression="MinimumQuantity" />
                        <asp:BoundField DataField="ReorderQuantity" HeaderText="ReorderQuantity" SortExpression="ReorderQuantity" />
                        <asp:BoundField DataField="MaximumQuantity" HeaderText="MaximumQuantity" SortExpression="MaximumQuantity" />
                        <asp:BoundField DataField="CuringPeriodDays" HeaderText="CuringPeriodDays" SortExpression="CuringPeriodDays" />
                        <asp:BoundField DataField="ShelfLifeDays" HeaderText="ShelfLifeDays" SortExpression="ShelfLifeDays" />
                        <asp:TemplateField HeaderText="QualityAssuranceIndicator" SortExpression="QualityAssuranceIndicator">
                            <EditItemTemplate>
                                <asp:CheckBox ID="QualityAssuranceIndicatorCheckBox" runat="server" Checked='<%# Bind("QualityAssuranceIndicator") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="QualityAssuranceIndicatorCheckBox" runat="server" Checked='<%# Bind("QualityAssuranceIndicator") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceProduct" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_Product_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_Product_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_Product_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_Product_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="" Name="StatusId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="TextBoxProductCode" DefaultValue="%" Name="ProductCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxProduct" DefaultValue="%" Name="Product" PropertyName="Text" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="ProductId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="ProductId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="ProductCode" Type="String" />
        <asp:Parameter Name="Product" Type="String" />
        <asp:Parameter Name="Barcode" Type="String" />
        <asp:Parameter Name="MinimumQuantity" Type="Int32" />
        <asp:Parameter Name="ReorderQuantity" Type="Int32" />
        <asp:Parameter Name="MaximumQuantity" Type="Int32" />
        <asp:Parameter Name="CuringPeriodDays" Type="Int32" />
        <asp:Parameter Name="ShelfLifeDays" Type="Int32" />
        <asp:Parameter Name="QualityAssuranceIndicator" Type="Boolean" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="ProductId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="ProductCode" Type="String" />
        <asp:Parameter Name="Product" Type="String" />
        <asp:Parameter Name="Barcode" Type="String" />
        <asp:Parameter Name="MinimumQuantity" Type="Int32" />
        <asp:Parameter Name="ReorderQuantity" Type="Int32" />
        <asp:Parameter Name="MaximumQuantity" Type="Int32" />
        <asp:Parameter Name="CuringPeriodDays" Type="Int32" />
        <asp:Parameter Name="ShelfLifeDays" Type="Int32" />
        <asp:Parameter Name="QualityAssuranceIndicator" Type="Boolean" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
