<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DivisionFormView.ascx.cs" Inherits="UserControls_DivisionFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Division</h3>
<asp:FormView ID="FormViewDivision" runat="server" DataKeyNames="DivisionId" DataSourceID="ObjectDataSourceDivision" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    DivisionCode:
                </td>
                <td>
                    <asp:TextBox ID="DivisionCodeTextBox" runat="server" Text='<%# Bind("DivisionCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Division:
                </td>
                <td>
                    <asp:TextBox ID="DivisionTextBox" runat="server" Text='<%# Bind("Division") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    DivisionCode:
                </td>
                <td>
                    <asp:TextBox ID="DivisionCodeTextBox" runat="server" Text='<%# Bind("DivisionCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Division:
                </td>
                <td>
                    <asp:TextBox ID="DivisionTextBox" runat="server" Text='<%# Bind("Division") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    DivisionCode:
                </td>
                <td>
                    <asp:Label ID="DivisionCodeLabel" runat="server" Text='<%# Bind("DivisionCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Division:
                </td>
                <td>
                    <asp:Label ID="DivisionLabel" runat="server" Text='<%# Bind("Division") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/DivisionGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceDivision" runat="server" TypeName="StaticInfoDivision"
    SelectMethod="GetDivision"
    DeleteMethod="DeleteDivision"
    UpdateMethod="UpdateDivision"
    InsertMethod="InsertDivision">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="DivisionId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="DivisionId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DivisionCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Division" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="DivisionId" QueryStringField="DivisionId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="DivisionId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DivisionCode" Type="String" />
        <asp:Parameter Name="Division" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
