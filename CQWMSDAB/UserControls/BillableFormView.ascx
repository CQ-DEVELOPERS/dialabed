<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BillableFormView.ascx.cs" Inherits="UserControls_BillableFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Billable</h3>
<asp:FormView ID="FormViewBillable" runat="server" DataKeyNames="BillableId" DataSourceID="ObjectDataSourceBillable" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    BillableCode:
                </td>
                <td>
                    <asp:TextBox ID="BillableCodeTextBox" runat="server" Text='<%# Bind("BillableCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Billable:
                </td>
                <td>
                    <asp:TextBox ID="BillableTextBox" runat="server" Text='<%# Bind("Billable") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    BillableCode:
                </td>
                <td>
                    <asp:TextBox ID="BillableCodeTextBox" runat="server" Text='<%# Bind("BillableCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Billable:
                </td>
                <td>
                    <asp:TextBox ID="BillableTextBox" runat="server" Text='<%# Bind("Billable") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    BillableCode:
                </td>
                <td>
                    <asp:Label ID="BillableCodeLabel" runat="server" Text='<%# Bind("BillableCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Billable:
                </td>
                <td>
                    <asp:Label ID="BillableLabel" runat="server" Text='<%# Bind("Billable") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/BillableGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceBillable" runat="server" TypeName="StaticInfoBillable"
    SelectMethod="GetBillable"
    DeleteMethod="DeleteBillable"
    UpdateMethod="UpdateBillable"
    InsertMethod="InsertBillable">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="BillableId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="BillableId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="BillableCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Billable" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="BillableId" QueryStringField="BillableId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="BillableId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="BillableCode" Type="String" />
        <asp:Parameter Name="Billable" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
