using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_InterfaceFieldGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridInterfaceField.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridInterfaceField_SelectedIndexChanged
    protected void RadGridInterfaceField_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridInterfaceField.SelectedItems)
            {
                Session["InterfaceFieldId"] = item.GetDataKeyValue("InterfaceFieldId");
            }
        }
        catch { }
    }
    #endregion "RadGridInterfaceField_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["InterfaceDocumentTypeId"] != null)
          DropDownListInterfaceDocumentTypeId.SelectedValue = Session["InterfaceDocumentTypeId"].ToString();
    
        RadGridInterfaceField.DataBind();
    }
    #endregion BindData
    
}
