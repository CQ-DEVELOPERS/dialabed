<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperatorGroupInstructionTypeFormView.ascx.cs" Inherits="UserControls_OperatorGroupInstructionTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>OperatorGroupInstructionType</h3>
<asp:FormView ID="FormViewOperatorGroupInstructionType" runat="server" DataKeyNames="OperatorGroupInstructionTypeId" DataSourceID="ObjectDataSourceOperatorGroupInstructionType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="OrderByTextBox" ErrorMessage="* Please enter OrderBy"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="OrderByTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    InstructionType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="ObjectDataSourceDDLInstructionType"
                        DataTextField="InstructionType" DataValueField="InstructionTypeId" SelectedValue='<%# Bind("InstructionTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInstructionType" runat="server"
                        TypeName="StaticInfoInstructionType" SelectMethod="ParameterInstructionType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListInstructionType" ErrorMessage="* Please enter InstructionTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroup"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroup" runat="server"
                        TypeName="StaticInfoOperatorGroup" SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOperatorGroup" ErrorMessage="* Please enter OperatorGroupId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="OrderByTextBox" ErrorMessage="* Please enter OrderBy"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="OrderByTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    InstructionType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="ObjectDataSourceDDLInstructionType"
                        DataTextField="InstructionType" DataValueField="InstructionTypeId" SelectedValue='<%# Bind("InstructionTypeId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInstructionType" runat="server"
                        TypeName="StaticInfoInstructionType" SelectMethod="ParameterInstructionType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroup"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroup" runat="server"
                        TypeName="StaticInfoOperatorGroup" SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:Label ID="OrderByLabel" runat="server" Text='<%# Bind("OrderBy") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/OperatorGroupInstructionTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceOperatorGroupInstructionType" runat="server" TypeName="StaticInfoOperatorGroupInstructionType"
    SelectMethod="GetOperatorGroupInstructionType"
    DeleteMethod="DeleteOperatorGroupInstructionType"
    UpdateMethod="UpdateOperatorGroupInstructionType"
    InsertMethod="InsertOperatorGroupInstructionType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupInstructionTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
        <asp:Parameter Name="OrderBy" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="InstructionTypeId" QueryStringField="InstructionTypeId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="OperatorGroupId" QueryStringField="OperatorGroupId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
        <asp:Parameter Name="OrderBy" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
