<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InstructionGridView.ascx.cs" Inherits="UserControls_InstructionGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowInstruction()
{
   window.open("InstructionFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelReasonId" runat="server" Text="Reason:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="SqlDataSourceReasonDDL"
                DataTextField="Reason" DataValueField="ReasonId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceReasonDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Reason_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelInstructionTypeId" runat="server" Text="InstructionType:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="SqlDataSourceInstructionTypeDDL"
                DataTextField="InstructionType" DataValueField="InstructionTypeId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceInstructionTypeDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_InstructionType_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelStorageUnitBatchId" runat="server" Text="StorageUnitBatch:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="SqlDataSourceStorageUnitBatchDDL"
                DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStorageUnitBatchDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_StorageUnitBatch_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelStatusId" runat="server" Text="Status:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceStatusDDL"
                DataTextField="Status" DataValueField="StatusId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStatusDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Status_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelJobId" runat="server" Text="Job:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListJob" runat="server" DataSourceID="SqlDataSourceJobDDL"
                DataTextField="Job" DataValueField="JobId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceJobDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Job_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelOperatorId" runat="server" Text="Operator:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceOperatorDDL"
                DataTextField="Operator" DataValueField="OperatorId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceOperatorDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Operator_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReceiptLineId" runat="server" Text="ReceiptLine:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="SqlDataSourceReceiptLineDDL"
                DataTextField="ReceiptLine" DataValueField="ReceiptLineId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceReceiptLineDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_ReceiptLine_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelIssueLineId" runat="server" Text="IssueLine:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="SqlDataSourceIssueLineDDL"
                DataTextField="IssueLine" DataValueField="IssueLineId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceIssueLineDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_IssueLine_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelPalletId" runat="server" Text="Pallet:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListPallet" runat="server" DataSourceID="SqlDataSourcePalletDDL"
                DataTextField="PalletId" DataValueField="PalletId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourcePalletDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Pallet_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="<%$ Resources:Default, ButtonNew %>" OnClientClick="javascript:openNewWindowInstruction();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, ButtonEdit %>" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, ButtonDelete %>" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="<%$ Resources:Default, ButtonBack %>" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="<%$ Resources:Default, ButtonPrevious %>" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="<%$ Resources:Default, ButtonHelp %>"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewInstruction" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
InstructionId
"
                DataSourceID="SqlDataSourceInstruction" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="InstructionId" HeaderText="InstructionId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="InstructionId" />
            <asp:TemplateField HeaderText="Reason" meta:resourcekey="Reason" SortExpression="ReasonId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="SqlDataSourceDDLReason"
                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLReason" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Reason_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkReasonId" runat="server" Text='<%# Bind("Reason") %>' NavigateUrl="~/Administration/ReasonGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Reason" HeaderText="Reason" ReadOnly="True" SortExpression="Reason" Visible="False" />
            <asp:TemplateField HeaderText="InstructionType" meta:resourcekey="InstructionType" SortExpression="InstructionTypeId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="SqlDataSourceDDLInstructionType"
                        DataTextField="InstructionType" DataValueField="InstructionTypeId" SelectedValue='<%# Bind("InstructionTypeId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLInstructionType" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_InstructionType_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkInstructionTypeId" runat="server" Text='<%# Bind("InstructionType") %>' NavigateUrl="~/Administration/InstructionTypeGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="InstructionType" HeaderText="InstructionType" ReadOnly="True" SortExpression="InstructionType" Visible="False" />
            <asp:TemplateField HeaderText="StorageUnitBatch" meta:resourcekey="StorageUnitBatch" SortExpression="StorageUnitBatchId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="SqlDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStorageUnitBatch" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_StorageUnitBatch_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStorageUnitBatchId" runat="server" Text='<%# Bind("StorageUnitBatch") %>' NavigateUrl="~/Administration/StorageUnitBatchGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StorageUnitBatch" HeaderText="StorageUnitBatch" ReadOnly="True" SortExpression="StorageUnitBatch" Visible="False" />
                        <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" ReadOnly="True" SortExpression="Warehouse" Visible="False" />
            <asp:TemplateField HeaderText="Status" meta:resourcekey="Status" SortExpression="StatusId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Status_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStatusId" runat="server" Text='<%# Bind("Status") %>' NavigateUrl="~/Administration/StatusGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" Visible="False" />
            <asp:TemplateField HeaderText="Job" meta:resourcekey="Job" SortExpression="JobId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListJob" runat="server" DataSourceID="SqlDataSourceDDLJob"
                        DataTextField="Job" DataValueField="JobId" SelectedValue='<%# Bind("JobId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLJob" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Job_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkJobId" runat="server" Text='<%# Bind("Job") %>' NavigateUrl="~/Administration/JobGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Job" HeaderText="Job" ReadOnly="True" SortExpression="Job" Visible="False" />
            <asp:TemplateField HeaderText="Operator" meta:resourcekey="Operator" SortExpression="OperatorId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLOperator" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Operator_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkOperatorId" runat="server" Text='<%# Bind("Operator") %>' NavigateUrl="~/Administration/OperatorGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Operator" HeaderText="Operator" ReadOnly="True" SortExpression="Operator" Visible="False" />
                        <asp:BoundField DataField="PickLocationId" HeaderText="PickLocationId" SortExpression="PickLocationId" />
                        <asp:BoundField DataField="StoreLocationId" HeaderText="StoreLocationId" SortExpression="StoreLocationId" />
            <asp:TemplateField HeaderText="ReceiptLine" meta:resourcekey="ReceiptLine" SortExpression="ReceiptLineId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="SqlDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLReceiptLine" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_ReceiptLine_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkReceiptLineId" runat="server" Text='<%# Bind("ReceiptLine") %>' NavigateUrl="~/Administration/ReceiptLineGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ReceiptLine" HeaderText="ReceiptLine" ReadOnly="True" SortExpression="ReceiptLine" Visible="False" />
            <asp:TemplateField HeaderText="IssueLine" meta:resourcekey="IssueLine" SortExpression="IssueLineId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="SqlDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLIssueLine" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_IssueLine_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkIssueLineId" runat="server" Text='<%# Bind("IssueLine") %>' NavigateUrl="~/Administration/IssueLineGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IssueLine" HeaderText="IssueLine" ReadOnly="True" SortExpression="IssueLine" Visible="False" />
                        <asp:BoundField DataField="InstructionRefId" HeaderText="InstructionRefId" SortExpression="InstructionRefId" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                        <asp:BoundField DataField="ConfirmedQuantity" HeaderText="ConfirmedQuantity" SortExpression="ConfirmedQuantity" />
                        <asp:BoundField DataField="Weight" HeaderText="Weight" SortExpression="Weight" />
                        <asp:BoundField DataField="ConfirmedWeight" HeaderText="ConfirmedWeight" SortExpression="ConfirmedWeight" />
            <asp:TemplateField HeaderText="Pallet" meta:resourcekey="Pallet" SortExpression="PalletId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListPallet" runat="server" DataSourceID="SqlDataSourceDDLPallet"
                        DataTextField="Pallet" DataValueField="PalletId" SelectedValue='<%# Bind("PalletId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLPallet" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Pallet_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkPalletId" runat="server" Text='<%# Bind("Pallet") %>' NavigateUrl="~/Administration/PalletGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Pallet" HeaderText="Pallet" ReadOnly="True" SortExpression="Pallet" Visible="False" />
                        <asp:TemplateField HeaderText="CreateDate" SortExpression="CreateDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonCreateDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('CreateDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="StartDate" SortExpression="StartDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonStartDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('StartDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelStartDate" runat="server" Text='<%# Bind("StartDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EndDate" SortExpression="EndDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonEndDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('EndDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelEndDate" runat="server" Text='<%# Bind("EndDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Picked" SortExpression="Picked">
                            <EditItemTemplate>
                                <asp:CheckBox ID="PickedCheckBox" runat="server" Checked='<%# Bind("Picked") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="PickedCheckBox" runat="server" Checked='<%# Bind("Picked") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stored" SortExpression="Stored">
                            <EditItemTemplate>
                                <asp:CheckBox ID="StoredCheckBox" runat="server" Checked='<%# Bind("Stored") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="StoredCheckBox" runat="server" Checked='<%# Bind("Stored") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CheckQuantity" HeaderText="CheckQuantity" SortExpression="CheckQuantity" />
                        <asp:BoundField DataField="CheckWeight" HeaderText="CheckWeight" SortExpression="CheckWeight" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceInstruction" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_Instruction_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_Instruction_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_Instruction_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_Instruction_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListReason" DefaultValue="" Name="ReasonId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListInstructionType" DefaultValue="" Name="InstructionTypeId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStorageUnitBatch" DefaultValue="" Name="StorageUnitBatchId" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="" Name="StatusId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListJob" DefaultValue="" Name="JobId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListOperator" DefaultValue="" Name="OperatorId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListReceiptLine" DefaultValue="" Name="ReceiptLineId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListIssueLine" DefaultValue="" Name="IssueLineId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListPallet" DefaultValue="" Name="PalletId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="InstructionId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="InstructionId" Type="Int32" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="JobId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="PickLocationId" Type="Int32" />
        <asp:Parameter Name="StoreLocationId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="InstructionRefId" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="ConfirmedQuantity" Type="Int32" />
        <asp:Parameter Name="Weight" Type="Decimal" />
        <asp:Parameter Name="ConfirmedWeight" Type="Decimal" />
        <asp:Parameter Name="PalletId" Type="Int32" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="StartDate" Type="DateTime" />
        <asp:Parameter Name="EndDate" Type="DateTime" />
        <asp:Parameter Name="Picked" Type="Boolean" />
        <asp:Parameter Name="Stored" Type="Boolean" />
        <asp:Parameter Name="CheckQuantity" Type="Int32" />
        <asp:Parameter Name="CheckWeight" Type="Decimal" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="InstructionId" Type="Int32" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="JobId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="PickLocationId" Type="Int32" />
        <asp:Parameter Name="StoreLocationId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="InstructionRefId" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="ConfirmedQuantity" Type="Int32" />
        <asp:Parameter Name="Weight" Type="Decimal" />
        <asp:Parameter Name="ConfirmedWeight" Type="Decimal" />
        <asp:Parameter Name="PalletId" Type="Int32" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="StartDate" Type="DateTime" />
        <asp:Parameter Name="EndDate" Type="DateTime" />
        <asp:Parameter Name="Picked" Type="Boolean" />
        <asp:Parameter Name="Stored" Type="Boolean" />
        <asp:Parameter Name="CheckQuantity" Type="Int32" />
        <asp:Parameter Name="CheckWeight" Type="Decimal" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
