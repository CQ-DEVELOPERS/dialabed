<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StatusFormView.ascx.cs" Inherits="UserControls_StatusFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Status</h3>
<asp:FormView ID="FormViewStatus" runat="server" DataKeyNames="StatusId" DataSourceID="ObjectDataSourceStatus" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:TextBox ID="StatusTextBox" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="StatusTextBox"
                        ErrorMessage="* Please enter Status"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StatusCode:
                </td>
                <td>
                    <asp:TextBox ID="StatusCodeTextBox" runat="server" Text='<%# Bind("StatusCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="StatusCodeTextBox"
                        ErrorMessage="* Please enter StatusCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Type:
                </td>
                <td>
                    <asp:TextBox ID="TypeTextBox" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TypeTextBox"
                        ErrorMessage="* Please enter Type"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="OrderByTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:TextBox ID="StatusTextBox" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="StatusTextBox"
                        ErrorMessage="* Please enter Status"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StatusCode:
                </td>
                <td>
                    <asp:TextBox ID="StatusCodeTextBox" runat="server" Text='<%# Bind("StatusCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="StatusCodeTextBox"
                        ErrorMessage="* Please enter StatusCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Type:
                </td>
                <td>
                    <asp:TextBox ID="TypeTextBox" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TypeTextBox"
                        ErrorMessage="* Please enter Type"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="OrderByTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:Label ID="StatusLabel" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StatusCode:
                </td>
                <td>
                    <asp:Label ID="StatusCodeLabel" runat="server" Text='<%# Bind("StatusCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Type:
                </td>
                <td>
                    <asp:Label ID="TypeLabel" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:Label ID="OrderByLabel" runat="server" Text='<%# Bind("OrderBy") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/StatusGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="StaticInfoStatus"
    SelectMethod="GetStatus"
    DeleteMethod="DeleteStatus"
    UpdateMethod="UpdateStatus"
    InsertMethod="InsertStatus">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StatusId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StatusId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Status" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="StatusCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Type" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="StatusId" QueryStringField="StatusId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StatusId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Status" Type="String" />
        <asp:Parameter Name="StatusCode" Type="String" />
        <asp:Parameter Name="Type" Type="String" />
        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
