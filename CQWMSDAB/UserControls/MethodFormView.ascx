<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MethodFormView.ascx.cs" Inherits="UserControls_MethodFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Method</h3>
<asp:FormView ID="FormViewMethod" runat="server" DataKeyNames="MethodId" DataSourceID="ObjectDataSourceMethod" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    MethodCode:
                </td>
                <td>
                    <asp:TextBox ID="MethodCodeTextBox" runat="server" Text='<%# Bind("MethodCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Method:
                </td>
                <td>
                    <asp:TextBox ID="MethodTextBox" runat="server" Text='<%# Bind("Method") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    MethodCode:
                </td>
                <td>
                    <asp:TextBox ID="MethodCodeTextBox" runat="server" Text='<%# Bind("MethodCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Method:
                </td>
                <td>
                    <asp:TextBox ID="MethodTextBox" runat="server" Text='<%# Bind("Method") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    MethodCode:
                </td>
                <td>
                    <asp:Label ID="MethodCodeLabel" runat="server" Text='<%# Bind("MethodCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Method:
                </td>
                <td>
                    <asp:Label ID="MethodLabel" runat="server" Text='<%# Bind("Method") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/MethodGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceMethod" runat="server" TypeName="StaticInfoMethod"
    SelectMethod="GetMethod"
    DeleteMethod="DeleteMethod"
    UpdateMethod="UpdateMethod"
    InsertMethod="InsertMethod">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MethodId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MethodId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MethodCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Method" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="MethodId" QueryStringField="MethodId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MethodId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MethodCode" Type="String" />
        <asp:Parameter Name="Method" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
