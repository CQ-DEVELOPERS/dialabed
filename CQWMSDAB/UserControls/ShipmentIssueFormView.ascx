<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShipmentIssueFormView.ascx.cs" Inherits="UserControls_ShipmentIssueFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>ShipmentIssue</h3>
<asp:FormView ID="FormViewShipmentIssue" runat="server" DataKeyNames="ShipmentIssueId" DataSourceID="ObjectDataSourceShipmentIssue" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:TextBox ID="DropSequenceTextBox" runat="server" Text='<%# Bind("DropSequence") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Shipment:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListShipment" runat="server" DataSourceID="ObjectDataSourceDDLShipment"
                        DataTextField="Shipment" DataValueField="ShipmentId" SelectedValue='<%# Bind("ShipmentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLShipment" runat="server"
                        TypeName="StaticInfoShipment" SelectMethod="ParameterShipment">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListShipment" ErrorMessage="* Please enter ShipmentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Issue:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssue" runat="server" DataSourceID="ObjectDataSourceDDLIssue"
                        DataTextField="Issue" DataValueField="IssueId" SelectedValue='<%# Bind("IssueId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssue" runat="server"
                        TypeName="StaticInfoIssue" SelectMethod="ParameterIssue">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListIssue" ErrorMessage="* Please enter IssueId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:TextBox ID="DropSequenceTextBox" runat="server" Text='<%# Bind("DropSequence") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Shipment:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListShipment" runat="server" DataSourceID="ObjectDataSourceDDLShipment"
                        DataTextField="Shipment" DataValueField="ShipmentId" SelectedValue='<%# Bind("ShipmentId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLShipment" runat="server"
                        TypeName="StaticInfoShipment" SelectMethod="ParameterShipment">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Issue:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssue" runat="server" DataSourceID="ObjectDataSourceDDLIssue"
                        DataTextField="Issue" DataValueField="IssueId" SelectedValue='<%# Bind("IssueId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssue" runat="server"
                        TypeName="StaticInfoIssue" SelectMethod="ParameterIssue">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:Label ID="DropSequenceLabel" runat="server" Text='<%# Bind("DropSequence") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ShipmentIssueGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceShipmentIssue" runat="server" TypeName="StaticInfoShipmentIssue"
    SelectMethod="GetShipmentIssue"
    DeleteMethod="DeleteShipmentIssue"
    UpdateMethod="UpdateShipmentIssue"
    InsertMethod="InsertShipmentIssue">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ShipmentIssueId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ShipmentId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="DropSequence" Type="Int16" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ShipmentId" QueryStringField="ShipmentId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="IssueId" QueryStringField="IssueId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ShipmentId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="DropSequence" Type="Int16" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
