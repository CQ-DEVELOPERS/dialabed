<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExceptionFormView.ascx.cs" Inherits="UserControls_ExceptionFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Exception</h3>
<asp:FormView ID="FormViewException" runat="server" DataKeyNames="ExceptionId" DataSourceID="ObjectDataSourceException" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ReceiptLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="ObjectDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceiptLine" runat="server"
                        TypeName="StaticInfoReceiptLine" SelectMethod="ParameterReceiptLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Instruction:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInstruction" runat="server" DataSourceID="ObjectDataSourceDDLInstruction"
                        DataTextField="Instruction" DataValueField="InstructionId" SelectedValue='<%# Bind("InstructionId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInstruction" runat="server"
                        TypeName="StaticInfoInstruction" SelectMethod="ParameterInstruction">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    IssueLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="ObjectDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssueLine" runat="server"
                        TypeName="StaticInfoIssueLine" SelectMethod="ParameterIssueLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceDDLReason"
                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReason" runat="server"
                        TypeName="StaticInfoReason" SelectMethod="ParameterReason">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Exception:
                </td>
                <td>
                    <asp:TextBox ID="ExceptionTextBox" runat="server" Text='<%# Bind("Exception") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ExceptionCode:
                </td>
                <td>
                    <asp:TextBox ID="ExceptionCodeTextBox" runat="server" Text='<%# Bind("ExceptionCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    ReceiptLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="ObjectDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceiptLine" runat="server"
                        TypeName="StaticInfoReceiptLine" SelectMethod="ParameterReceiptLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Instruction:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInstruction" runat="server" DataSourceID="ObjectDataSourceDDLInstruction"
                        DataTextField="Instruction" DataValueField="InstructionId" SelectedValue='<%# Bind("InstructionId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInstruction" runat="server"
                        TypeName="StaticInfoInstruction" SelectMethod="ParameterInstruction">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    IssueLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="ObjectDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssueLine" runat="server"
                        TypeName="StaticInfoIssueLine" SelectMethod="ParameterIssueLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceDDLReason"
                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReason" runat="server"
                        TypeName="StaticInfoReason" SelectMethod="ParameterReason">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Exception:
                </td>
                <td>
                    <asp:TextBox ID="ExceptionTextBox" runat="server" Text='<%# Bind("Exception") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ExceptionCode:
                </td>
                <td>
                    <asp:TextBox ID="ExceptionCodeTextBox" runat="server" Text='<%# Bind("ExceptionCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    ReceiptLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="ObjectDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceiptLine" runat="server"
                        TypeName="StaticInfoReceiptLine" SelectMethod="ParameterReceiptLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Instruction:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInstruction" runat="server" DataSourceID="ObjectDataSourceDDLInstruction"
                        DataTextField="Instruction" DataValueField="InstructionId" SelectedValue='<%# Bind("InstructionId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInstruction" runat="server"
                        TypeName="StaticInfoInstruction" SelectMethod="ParameterInstruction">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    IssueLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="ObjectDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssueLine" runat="server"
                        TypeName="StaticInfoIssueLine" SelectMethod="ParameterIssueLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceDDLReason"
                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReason" runat="server"
                        TypeName="StaticInfoReason" SelectMethod="ParameterReason">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Exception:
                </td>
                <td>
                    <asp:Label ID="ExceptionLabel" runat="server" Text='<%# Bind("Exception") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ExceptionCode:
                </td>
                <td>
                    <asp:Label ID="ExceptionCodeLabel" runat="server" Text='<%# Bind("ExceptionCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:Label ID="CreateDateLabel" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/ExceptionGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceException" runat="server" TypeName="StaticInfoException"
    SelectMethod="GetException"
    DeleteMethod="DeleteException"
    UpdateMethod="UpdateException"
    InsertMethod="InsertException">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExceptionId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExceptionId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="InstructionId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
        <asp:Parameter Name="Exception" Type="String" />
        <asp:Parameter Name="ExceptionCode" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ExceptionId" QueryStringField="ExceptionId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExceptionId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="InstructionId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
        <asp:Parameter Name="Exception" Type="String" />
        <asp:Parameter Name="ExceptionCode" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
