<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InboundDocumentTypeFormView.ascx.cs" Inherits="UserControls_InboundDocumentTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>InboundDocumentType</h3>
<asp:FormView ID="FormViewInboundDocumentType" runat="server" DataKeyNames="InboundDocumentTypeId" DataSourceID="ObjectDataSourceInboundDocumentType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocumentTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="InboundDocumentTypeCodeTextBox" runat="server" Text='<%# Bind("InboundDocumentTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    InboundDocumentType:
                </td>
                <td>
                    <asp:TextBox ID="InboundDocumentTypeTextBox" runat="server" Text='<%# Bind("InboundDocumentType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="InboundDocumentTypeTextBox"
                        ErrorMessage="* Please enter InboundDocumentType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OverReceiveIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="OverReceiveIndicatorCheckBox" runat="server" Checked='<%# Bind("OverReceiveIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter OverReceiveIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AllowManualCreate:
                </td>
                <td>
                    <asp:CheckBox ID="AllowManualCreateCheckBox" runat="server" Checked='<%# Bind("AllowManualCreate") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter AllowManualCreate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    QuantityToFollowIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="QuantityToFollowIndicatorCheckBox" runat="server" Checked='<%# Bind("QuantityToFollowIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter QuantityToFollowIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DropDownListPriorityId"
                        ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AutoSendup:
                </td>
                <td>
                    <asp:CheckBox ID="AutoSendupCheckBox" runat="server" Checked='<%# Bind("AutoSendup") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    BatchButton:
                </td>
                <td>
                    <asp:CheckBox ID="BatchButtonCheckBox" runat="server" Checked='<%# Bind("BatchButton") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    BatchSelect:
                </td>
                <td>
                    <asp:CheckBox ID="BatchSelectCheckBox" runat="server" Checked='<%# Bind("BatchSelect") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocumentTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="InboundDocumentTypeCodeTextBox" runat="server" Text='<%# Bind("InboundDocumentTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    InboundDocumentType:
                </td>
                <td>
                    <asp:TextBox ID="InboundDocumentTypeTextBox" runat="server" Text='<%# Bind("InboundDocumentType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="InboundDocumentTypeTextBox"
                        ErrorMessage="* Please enter InboundDocumentType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OverReceiveIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="OverReceiveIndicatorCheckBox" runat="server" Checked='<%# Bind("OverReceiveIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter OverReceiveIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AllowManualCreate:
                </td>
                <td>
                    <asp:CheckBox ID="AllowManualCreateCheckBox" runat="server" Checked='<%# Bind("AllowManualCreate") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter AllowManualCreate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    QuantityToFollowIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="QuantityToFollowIndicatorCheckBox" runat="server" Checked='<%# Bind("QuantityToFollowIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter QuantityToFollowIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DropDownListPriorityId"
                        ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AutoSendup:
                </td>
                <td>
                    <asp:CheckBox ID="AutoSendupCheckBox" runat="server" Checked='<%# Bind("AutoSendup") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    BatchButton:
                </td>
                <td>
                    <asp:CheckBox ID="BatchButtonCheckBox" runat="server" Checked='<%# Bind("BatchButton") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    BatchSelect:
                </td>
                <td>
                    <asp:CheckBox ID="BatchSelectCheckBox" runat="server" Checked='<%# Bind("BatchSelect") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocumentTypeCode:
                </td>
                <td>
                    <asp:Label ID="InboundDocumentTypeCodeLabel" runat="server" Text='<%# Bind("InboundDocumentTypeCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    InboundDocumentType:
                </td>
                <td>
                    <asp:Label ID="InboundDocumentTypeLabel" runat="server" Text='<%# Bind("InboundDocumentType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OverReceiveIndicator:
                </td>
                <td>
                    <asp:Label ID="OverReceiveIndicatorLabel" runat="server" Text='<%# Bind("OverReceiveIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AllowManualCreate:
                </td>
                <td>
                    <asp:Label ID="AllowManualCreateLabel" runat="server" Text='<%# Bind("AllowManualCreate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    QuantityToFollowIndicator:
                </td>
                <td>
                    <asp:Label ID="QuantityToFollowIndicatorLabel" runat="server" Text='<%# Bind("QuantityToFollowIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    AutoSendup:
                </td>
                <td>
                    <asp:Label ID="AutoSendupLabel" runat="server" Text='<%# Bind("AutoSendup") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    BatchButton:
                </td>
                <td>
                    <asp:Label ID="BatchButtonLabel" runat="server" Text='<%# Bind("BatchButton") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    BatchSelect:
                </td>
                <td>
                    <asp:Label ID="BatchSelectLabel" runat="server" Text='<%# Bind("BatchSelect") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/InboundDocumentTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentType" runat="server" TypeName="StaticInfoInboundDocumentType"
    SelectMethod="GetInboundDocumentType"
    DeleteMethod="DeleteInboundDocumentType"
    UpdateMethod="UpdateInboundDocumentType"
    InsertMethod="InsertInboundDocumentType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="InboundDocumentTypeCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="InboundDocumentType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="OverReceiveIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="AllowManualCreate" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="QuantityToFollowIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AutoSendup" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="BatchButton" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="BatchSelect" Type="Boolean" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="InboundDocumentTypeId" QueryStringField="InboundDocumentTypeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="InboundDocumentTypeCode" Type="String" />
        <asp:Parameter Name="InboundDocumentType" Type="String" />
        <asp:Parameter Name="OverReceiveIndicator" Type="Boolean" />
        <asp:Parameter Name="AllowManualCreate" Type="Boolean" />
        <asp:Parameter Name="QuantityToFollowIndicator" Type="Boolean" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AutoSendup" Type="Boolean" />
        <asp:Parameter Name="BatchButton" Type="Boolean" />
        <asp:Parameter Name="BatchSelect" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
