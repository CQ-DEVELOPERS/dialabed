using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_InterfaceDocumentTypeGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridInterfaceDocumentType.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridInterfaceDocumentType_SelectedIndexChanged
    protected void RadGridInterfaceDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridInterfaceDocumentType.SelectedItems)
            {
                Session["InterfaceDocumentTypeId"] = item.GetDataKeyValue("InterfaceDocumentTypeId");
            }
        }
        catch { }
    }
    #endregion "RadGridInterfaceDocumentType_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
    
        RadGridInterfaceDocumentType.DataBind();
    }
    #endregion BindData
    
}
