<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InstructionFormView.ascx.cs" Inherits="UserControls_InstructionFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Instruction</h3>
<asp:FormView ID="FormViewInstruction" runat="server" DataKeyNames="InstructionId" DataSourceID="ObjectDataSourceInstruction" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceDDLReason"
                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReason" runat="server"
                        TypeName="StaticInfoReason" SelectMethod="ParameterReason">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="ObjectDataSourceDDLInstructionType"
                        DataTextField="InstructionType" DataValueField="InstructionTypeId" SelectedValue='<%# Bind("InstructionTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInstructionType" runat="server"
                        TypeName="StaticInfoInstructionType" SelectMethod="ParameterInstructionType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListInstructionType" ErrorMessage="* Please enter InstructionTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatch" runat="server"
                        TypeName="StaticInfoStorageUnitBatch" SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Job:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListJob" runat="server" DataSourceID="ObjectDataSourceDDLJob"
                        DataTextField="Job" DataValueField="JobId" SelectedValue='<%# Bind("JobId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLJob" runat="server"
                        TypeName="StaticInfoJob" SelectMethod="ParameterJob">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    PickLocationId:
                </td>
                <td>
                    <asp:TextBox ID="PickLocationIdTextBox" runat="server" Text='<%# Bind("PickLocationId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="PickLocationIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StoreLocationId:
                </td>
                <td>
                    <asp:TextBox ID="StoreLocationIdTextBox" runat="server" Text='<%# Bind("StoreLocationId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="StoreLocationIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReceiptLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="ObjectDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceiptLine" runat="server"
                        TypeName="StaticInfoReceiptLine" SelectMethod="ParameterReceiptLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    IssueLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="ObjectDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssueLine" runat="server"
                        TypeName="StaticInfoIssueLine" SelectMethod="ParameterIssueLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionRefId:
                </td>
                <td>
                    <asp:TextBox ID="InstructionRefIdTextBox" runat="server" Text='<%# Bind("InstructionRefId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="InstructionRefIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="QuantityTextBox" ErrorMessage="* Please enter Quantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="QuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ConfirmedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ConfirmedQuantityTextBox" runat="server" Text='<%# Bind("ConfirmedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator15" runat="server" ControlToValidate="ConfirmedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Weight:
                </td>
                <td>
                    <asp:TextBox ID="WeightTextBox" runat="server" Text='<%# Bind("Weight") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ConfirmedWeight:
                </td>
                <td>
                    <asp:TextBox ID="ConfirmedWeightTextBox" runat="server" Text='<%# Bind("ConfirmedWeight") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Pallet:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPallet" runat="server" DataSourceID="ObjectDataSourceDDLPallet"
                        DataTextField="Pallet" DataValueField="PalletId" SelectedValue='<%# Bind("PalletId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPallet" runat="server"
                        TypeName="StaticInfoPallet" SelectMethod="ParameterPallet">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StartDate:
                </td>
                <td>
                    <asp:TextBox ID="StartDateTextBox" runat="server" Text='<%# Bind("StartDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    EndDate:
                </td>
                <td>
                    <asp:TextBox ID="EndDateTextBox" runat="server" Text='<%# Bind("EndDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Picked:
                </td>
                <td>
                    <asp:CheckBox ID="PickedCheckBox" runat="server" Checked='<%# Bind("Picked") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Stored:
                </td>
                <td>
                    <asp:CheckBox ID="StoredCheckBox" runat="server" Checked='<%# Bind("Stored") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CheckQuantity:
                </td>
                <td>
                    <asp:TextBox ID="CheckQuantityTextBox" runat="server" Text='<%# Bind("CheckQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator24" runat="server" ControlToValidate="CheckQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CheckWeight:
                </td>
                <td>
                    <asp:TextBox ID="CheckWeightTextBox" runat="server" Text='<%# Bind("CheckWeight") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceDDLReason"
                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReason" runat="server"
                        TypeName="StaticInfoReason" SelectMethod="ParameterReason">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="ObjectDataSourceDDLInstructionType"
                        DataTextField="InstructionType" DataValueField="InstructionTypeId" SelectedValue='<%# Bind("InstructionTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInstructionType" runat="server"
                        TypeName="StaticInfoInstructionType" SelectMethod="ParameterInstructionType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListInstructionType" ErrorMessage="* Please enter InstructionTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatch" runat="server"
                        TypeName="StaticInfoStorageUnitBatch" SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Job:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListJob" runat="server" DataSourceID="ObjectDataSourceDDLJob"
                        DataTextField="Job" DataValueField="JobId" SelectedValue='<%# Bind("JobId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLJob" runat="server"
                        TypeName="StaticInfoJob" SelectMethod="ParameterJob">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    PickLocationId:
                </td>
                <td>
                    <asp:TextBox ID="PickLocationIdTextBox" runat="server" Text='<%# Bind("PickLocationId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="PickLocationIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StoreLocationId:
                </td>
                <td>
                    <asp:TextBox ID="StoreLocationIdTextBox" runat="server" Text='<%# Bind("StoreLocationId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="StoreLocationIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReceiptLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="ObjectDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceiptLine" runat="server"
                        TypeName="StaticInfoReceiptLine" SelectMethod="ParameterReceiptLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    IssueLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="ObjectDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssueLine" runat="server"
                        TypeName="StaticInfoIssueLine" SelectMethod="ParameterIssueLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionRefId:
                </td>
                <td>
                    <asp:TextBox ID="InstructionRefIdTextBox" runat="server" Text='<%# Bind("InstructionRefId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="InstructionRefIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="QuantityTextBox" ErrorMessage="* Please enter Quantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="QuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ConfirmedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ConfirmedQuantityTextBox" runat="server" Text='<%# Bind("ConfirmedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator15" runat="server" ControlToValidate="ConfirmedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Weight:
                </td>
                <td>
                    <asp:TextBox ID="WeightTextBox" runat="server" Text='<%# Bind("Weight") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ConfirmedWeight:
                </td>
                <td>
                    <asp:TextBox ID="ConfirmedWeightTextBox" runat="server" Text='<%# Bind("ConfirmedWeight") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Pallet:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPallet" runat="server" DataSourceID="ObjectDataSourceDDLPallet"
                        DataTextField="Pallet" DataValueField="PalletId" SelectedValue='<%# Bind("PalletId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPallet" runat="server"
                        TypeName="StaticInfoPallet" SelectMethod="ParameterPallet">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StartDate:
                </td>
                <td>
                    <asp:TextBox ID="StartDateTextBox" runat="server" Text='<%# Bind("StartDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    EndDate:
                </td>
                <td>
                    <asp:TextBox ID="EndDateTextBox" runat="server" Text='<%# Bind("EndDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Picked:
                </td>
                <td>
                    <asp:CheckBox ID="PickedCheckBox" runat="server" Checked='<%# Bind("Picked") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Stored:
                </td>
                <td>
                    <asp:CheckBox ID="StoredCheckBox" runat="server" Checked='<%# Bind("Stored") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CheckQuantity:
                </td>
                <td>
                    <asp:TextBox ID="CheckQuantityTextBox" runat="server" Text='<%# Bind("CheckQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator24" runat="server" ControlToValidate="CheckQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CheckWeight:
                </td>
                <td>
                    <asp:TextBox ID="CheckWeightTextBox" runat="server" Text='<%# Bind("CheckWeight") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceDDLReason"
                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReason" runat="server"
                        TypeName="StaticInfoReason" SelectMethod="ParameterReason">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="ObjectDataSourceDDLInstructionType"
                        DataTextField="InstructionType" DataValueField="InstructionTypeId" SelectedValue='<%# Bind("InstructionTypeId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInstructionType" runat="server"
                        TypeName="StaticInfoInstructionType" SelectMethod="ParameterInstructionType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatch" runat="server"
                        TypeName="StaticInfoStorageUnitBatch" SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouse" runat="server" DataSourceID="ObjectDataSourceDDLWarehouse"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouse" runat="server"
                        TypeName="StaticInfoWarehouse" SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Job:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListJob" runat="server" DataSourceID="ObjectDataSourceDDLJob"
                        DataTextField="Job" DataValueField="JobId" SelectedValue='<%# Bind("JobId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLJob" runat="server"
                        TypeName="StaticInfoJob" SelectMethod="ParameterJob">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    PickLocationId:
                </td>
                <td>
                    <asp:Label ID="PickLocationIdLabel" runat="server" Text='<%# Bind("PickLocationId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StoreLocationId:
                </td>
                <td>
                    <asp:Label ID="StoreLocationIdLabel" runat="server" Text='<%# Bind("StoreLocationId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ReceiptLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="ObjectDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceiptLine" runat="server"
                        TypeName="StaticInfoReceiptLine" SelectMethod="ParameterReceiptLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    IssueLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="ObjectDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssueLine" runat="server"
                        TypeName="StaticInfoIssueLine" SelectMethod="ParameterIssueLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionRefId:
                </td>
                <td>
                    <asp:Label ID="InstructionRefIdLabel" runat="server" Text='<%# Bind("InstructionRefId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ConfirmedQuantity:
                </td>
                <td>
                    <asp:Label ID="ConfirmedQuantityLabel" runat="server" Text='<%# Bind("ConfirmedQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Weight:
                </td>
                <td>
                    <asp:Label ID="WeightLabel" runat="server" Text='<%# Bind("Weight") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ConfirmedWeight:
                </td>
                <td>
                    <asp:Label ID="ConfirmedWeightLabel" runat="server" Text='<%# Bind("ConfirmedWeight") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Pallet:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPallet" runat="server" DataSourceID="ObjectDataSourceDDLPallet"
                        DataTextField="Pallet" DataValueField="PalletId" SelectedValue='<%# Bind("PalletId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPallet" runat="server"
                        TypeName="StaticInfoPallet" SelectMethod="ParameterPallet">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:Label ID="CreateDateLabel" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StartDate:
                </td>
                <td>
                    <asp:Label ID="StartDateLabel" runat="server" Text='<%# Bind("StartDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    EndDate:
                </td>
                <td>
                    <asp:Label ID="EndDateLabel" runat="server" Text='<%# Bind("EndDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Picked:
                </td>
                <td>
                    <asp:Label ID="PickedLabel" runat="server" Text='<%# Bind("Picked") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Stored:
                </td>
                <td>
                    <asp:Label ID="StoredLabel" runat="server" Text='<%# Bind("Stored") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CheckQuantity:
                </td>
                <td>
                    <asp:Label ID="CheckQuantityLabel" runat="server" Text='<%# Bind("CheckQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CheckWeight:
                </td>
                <td>
                    <asp:Label ID="CheckWeightLabel" runat="server" Text='<%# Bind("CheckWeight") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/InstructionGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="StaticInfoInstruction"
    SelectMethod="GetInstruction"
    DeleteMethod="DeleteInstruction"
    UpdateMethod="UpdateInstruction"
    InsertMethod="InsertInstruction">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InstructionId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InstructionId" Type="Int32" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="JobId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="PickLocationId" Type="Int32" />
        <asp:Parameter Name="StoreLocationId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="InstructionRefId" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Decimal" />
        <asp:Parameter Name="ConfirmedQuantity" Type="Int32" />
        <asp:Parameter Name="Weight" Type="Decimal" />
        <asp:Parameter Name="ConfirmedWeight" Type="Decimal" />
        <asp:Parameter Name="PalletId" Type="Int32" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="StartDate" Type="DateTime" />
        <asp:Parameter Name="EndDate" Type="DateTime" />
        <asp:Parameter Name="Picked" Type="Boolean" />
        <asp:Parameter Name="Stored" Type="Boolean" />
        <asp:Parameter Name="CheckQuantity" Type="Int32" />
        <asp:Parameter Name="CheckWeight" Type="Decimal" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="InstructionId" QueryStringField="InstructionId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InstructionId" Type="Int32" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="JobId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="PickLocationId" Type="Int32" />
        <asp:Parameter Name="StoreLocationId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="InstructionRefId" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="ConfirmedQuantity" Type="Int32" />
        <asp:Parameter Name="Weight" Type="Decimal" />
        <asp:Parameter Name="ConfirmedWeight" Type="Decimal" />
        <asp:Parameter Name="PalletId" Type="Int32" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="StartDate" Type="DateTime" />
        <asp:Parameter Name="EndDate" Type="DateTime" />
        <asp:Parameter Name="Picked" Type="Boolean" />
        <asp:Parameter Name="Stored" Type="Boolean" />
        <asp:Parameter Name="CheckQuantity" Type="Int32" />
        <asp:Parameter Name="CheckWeight" Type="Decimal" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
