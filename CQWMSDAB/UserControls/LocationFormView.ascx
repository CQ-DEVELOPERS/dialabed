<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationFormView.ascx.cs" Inherits="UserControls_LocationFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Location</h3>
<asp:FormView ID="FormViewLocation" runat="server" DataKeyNames="LocationId" DataSourceID="ObjectDataSourceLocation" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    LocationType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocationType" runat="server" DataSourceID="ObjectDataSourceDDLLocationType"
                        DataTextField="LocationType" DataValueField="LocationTypeId" SelectedValue='<%# Bind("LocationTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocationType" runat="server"
                        TypeName="StaticInfoLocationType" SelectMethod="ParameterLocationType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListLocationType" ErrorMessage="* Please enter LocationTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:TextBox ID="LocationTextBox" runat="server" Text='<%# Bind("Location") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="LocationTextBox" ErrorMessage="* Please enter Location"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Ailse:
                </td>
                <td>
                    <asp:TextBox ID="AilseTextBox" runat="server" Text='<%# Bind("Ailse") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Column:
                </td>
                <td>
                    <asp:TextBox ID="ColumnTextBox" runat="server" Text='<%# Bind("Column") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Level:
                </td>
                <td>
                    <asp:TextBox ID="LevelTextBox" runat="server" Text='<%# Bind("Level") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    PalletQuantity:
                </td>
                <td>
                    <asp:TextBox ID="PalletQuantityTextBox" runat="server" Text='<%# Bind("PalletQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="PalletQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SecurityCode:
                </td>
                <td>
                    <asp:TextBox ID="SecurityCodeTextBox" runat="server" Text='<%# Bind("SecurityCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="SecurityCodeTextBox" ErrorMessage="* Please enter SecurityCode"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="SecurityCodeTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RelativeValue:
                </td>
                <td>
                    <asp:TextBox ID="RelativeValueTextBox" runat="server" Text='<%# Bind("RelativeValue") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    NumberOfSUB:
                </td>
                <td>
                    <asp:TextBox ID="NumberOfSUBTextBox" runat="server" Text='<%# Bind("NumberOfSUB") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="NumberOfSUBTextBox" ErrorMessage="* Please enter NumberOfSUB"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="NumberOfSUBTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StocktakeInd:
                </td>
                <td>
                    <asp:CheckBox ID="StocktakeIndCheckBox" runat="server" Checked='<%# Bind("StocktakeInd") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActiveBinning:
                </td>
                <td>
                    <asp:CheckBox ID="ActiveBinningCheckBox" runat="server" Checked='<%# Bind("ActiveBinning") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActivePicking:
                </td>
                <td>
                    <asp:CheckBox ID="ActivePickingCheckBox" runat="server" Checked='<%# Bind("ActivePicking") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Used:
                </td>
                <td>
                    <asp:CheckBox ID="UsedCheckBox" runat="server" Checked='<%# Bind("Used") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    LocationType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocationType" runat="server" DataSourceID="ObjectDataSourceDDLLocationType"
                        DataTextField="LocationType" DataValueField="LocationTypeId" SelectedValue='<%# Bind("LocationTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocationType" runat="server"
                        TypeName="StaticInfoLocationType" SelectMethod="ParameterLocationType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListLocationType" ErrorMessage="* Please enter LocationTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:TextBox ID="LocationTextBox" runat="server" Text='<%# Bind("Location") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="LocationTextBox" ErrorMessage="* Please enter Location"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Ailse:
                </td>
                <td>
                    <asp:TextBox ID="AilseTextBox" runat="server" Text='<%# Bind("Ailse") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Column:
                </td>
                <td>
                    <asp:TextBox ID="ColumnTextBox" runat="server" Text='<%# Bind("Column") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Level:
                </td>
                <td>
                    <asp:TextBox ID="LevelTextBox" runat="server" Text='<%# Bind("Level") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    PalletQuantity:
                </td>
                <td>
                    <asp:TextBox ID="PalletQuantityTextBox" runat="server" Text='<%# Bind("PalletQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="PalletQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SecurityCode:
                </td>
                <td>
                    <asp:TextBox ID="SecurityCodeTextBox" runat="server" Text='<%# Bind("SecurityCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="SecurityCodeTextBox" ErrorMessage="* Please enter SecurityCode"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="SecurityCodeTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RelativeValue:
                </td>
                <td>
                    <asp:TextBox ID="RelativeValueTextBox" runat="server" Text='<%# Bind("RelativeValue") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    NumberOfSUB:
                </td>
                <td>
                    <asp:TextBox ID="NumberOfSUBTextBox" runat="server" Text='<%# Bind("NumberOfSUB") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="NumberOfSUBTextBox" ErrorMessage="* Please enter NumberOfSUB"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="NumberOfSUBTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StocktakeInd:
                </td>
                <td>
                    <asp:CheckBox ID="StocktakeIndCheckBox" runat="server" Checked='<%# Bind("StocktakeInd") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActiveBinning:
                </td>
                <td>
                    <asp:CheckBox ID="ActiveBinningCheckBox" runat="server" Checked='<%# Bind("ActiveBinning") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActivePicking:
                </td>
                <td>
                    <asp:CheckBox ID="ActivePickingCheckBox" runat="server" Checked='<%# Bind("ActivePicking") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Used:
                </td>
                <td>
                    <asp:CheckBox ID="UsedCheckBox" runat="server" Checked='<%# Bind("Used") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    LocationType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocationType" runat="server" DataSourceID="ObjectDataSourceDDLLocationType"
                        DataTextField="LocationType" DataValueField="LocationTypeId" SelectedValue='<%# Bind("LocationTypeId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocationType" runat="server"
                        TypeName="StaticInfoLocationType" SelectMethod="ParameterLocationType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:Label ID="LocationLabel" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Ailse:
                </td>
                <td>
                    <asp:Label ID="AilseLabel" runat="server" Text='<%# Bind("Ailse") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Column:
                </td>
                <td>
                    <asp:Label ID="ColumnLabel" runat="server" Text='<%# Bind("Column") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Level:
                </td>
                <td>
                    <asp:Label ID="LevelLabel" runat="server" Text='<%# Bind("Level") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    PalletQuantity:
                </td>
                <td>
                    <asp:Label ID="PalletQuantityLabel" runat="server" Text='<%# Bind("PalletQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SecurityCode:
                </td>
                <td>
                    <asp:Label ID="SecurityCodeLabel" runat="server" Text='<%# Bind("SecurityCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RelativeValue:
                </td>
                <td>
                    <asp:Label ID="RelativeValueLabel" runat="server" Text='<%# Bind("RelativeValue") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    NumberOfSUB:
                </td>
                <td>
                    <asp:Label ID="NumberOfSUBLabel" runat="server" Text='<%# Bind("NumberOfSUB") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StocktakeInd:
                </td>
                <td>
                    <asp:Label ID="StocktakeIndLabel" runat="server" Text='<%# Bind("StocktakeInd") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ActiveBinning:
                </td>
                <td>
                    <asp:Label ID="ActiveBinningLabel" runat="server" Text='<%# Bind("ActiveBinning") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ActivePicking:
                </td>
                <td>
                    <asp:Label ID="ActivePickingLabel" runat="server" Text='<%# Bind("ActivePicking") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Used:
                </td>
                <td>
                    <asp:Label ID="UsedLabel" runat="server" Text='<%# Bind("Used") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/LocationGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="StaticInfoLocation"
    SelectMethod="GetLocation"
    DeleteMethod="DeleteLocation"
    UpdateMethod="UpdateLocation"
    InsertMethod="InsertLocation">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="LocationId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="LocationTypeId" Type="Int32" />
        <asp:Parameter Name="Location" Type="String" />
        <asp:Parameter Name="Ailse" Type="String" />
        <asp:Parameter Name="Column" Type="String" />
        <asp:Parameter Name="Level" Type="String" />
        <asp:Parameter Name="PalletQuantity" Type="Int32" />
        <asp:Parameter Name="SecurityCode" Type="Int32" />
        <asp:Parameter Name="RelativeValue" Type="Decimal" />
        <asp:Parameter Name="NumberOfSUB" Type="Int32" />
        <asp:Parameter Name="StocktakeInd" Type="Boolean" />
        <asp:Parameter Name="ActiveBinning" Type="Boolean" />
        <asp:Parameter Name="ActivePicking" Type="Boolean" />
        <asp:Parameter Name="Used" Type="Boolean" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="LocationId" QueryStringField="LocationId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="LocationTypeId" Type="Int32" />
        <asp:Parameter Name="Location" Type="String" />
        <asp:Parameter Name="Ailse" Type="String" />
        <asp:Parameter Name="Column" Type="String" />
        <asp:Parameter Name="Level" Type="String" />
        <asp:Parameter Name="PalletQuantity" Type="Int32" />
        <asp:Parameter Name="SecurityCode" Type="Int32" />
        <asp:Parameter Name="RelativeValue" Type="Decimal" />
        <asp:Parameter Name="NumberOfSUB" Type="Int32" />
        <asp:Parameter Name="StocktakeInd" Type="Boolean" />
        <asp:Parameter Name="ActiveBinning" Type="Boolean" />
        <asp:Parameter Name="ActivePicking" Type="Boolean" />
        <asp:Parameter Name="Used" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
