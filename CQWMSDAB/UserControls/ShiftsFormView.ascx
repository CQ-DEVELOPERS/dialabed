<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShiftsFormView.ascx.cs" Inherits="UserControls_ShiftsFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Shifts</h3>
<asp:FormView ID="FormViewShifts" runat="server" DataKeyNames="ShiftsId" DataSourceID="ObjectDataSourceShifts" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Starttime:
                </td>
                <td>
                    <asp:TextBox ID="StarttimeTextBox" runat="server" Text='<%# Bind("Starttime") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="StarttimeTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    EndTime:
                </td>
                <td>
                    <asp:TextBox ID="EndTimeTextBox" runat="server" Text='<%# Bind("EndTime") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="EndTimeTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Starttime:
                </td>
                <td>
                    <asp:TextBox ID="StarttimeTextBox" runat="server" Text='<%# Bind("Starttime") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="StarttimeTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    EndTime:
                </td>
                <td>
                    <asp:TextBox ID="EndTimeTextBox" runat="server" Text='<%# Bind("EndTime") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="EndTimeTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Starttime:
                </td>
                <td>
                    <asp:Label ID="StarttimeLabel" runat="server" Text='<%# Bind("Starttime") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    EndTime:
                </td>
                <td>
                    <asp:Label ID="EndTimeLabel" runat="server" Text='<%# Bind("EndTime") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ShiftsGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceShifts" runat="server" TypeName="StaticInfoShifts"
    SelectMethod="GetShifts"
    DeleteMethod="DeleteShifts"
    UpdateMethod="UpdateShifts"
    InsertMethod="InsertShifts">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ShiftsId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ShiftId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Starttime" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="EndTime" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ShiftId" QueryStringField="ShiftId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ShiftId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Starttime" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="EndTime" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
