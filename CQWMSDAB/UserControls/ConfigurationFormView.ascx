<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfigurationFormView.ascx.cs" Inherits="UserControls_ConfigurationFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Configuration</h3>
<asp:FormView ID="FormViewConfiguration" runat="server" DataKeyNames="ConfigurationId" DataSourceID="ObjectDataSourceConfiguration" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ModuleId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListModuleId" runat="server" DataSourceID="ObjectDataSourceDDLModuleId"
                        DataTextField="Module" DataValueField="ModuleId" SelectedValue='<%# Bind("ModuleId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLModuleId" runat="server" TypeName="StaticInfoModule"
                        SelectMethod="ParameterModule">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListModuleId"
                        ErrorMessage="* Please enter ModuleId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Configuration:
                </td>
                <td>
                    <asp:TextBox ID="ConfigurationTextBox" runat="server" Text='<%# Bind("Configuration") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ConfigurationTextBox"
                        ErrorMessage="* Please enter Configuration"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Indicator:
                </td>
                <td>
                    <asp:CheckBox ID="IndicatorCheckBox" runat="server" Checked='<%# Bind("Indicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    IntegerValue:
                </td>
                <td>
                    <asp:TextBox ID="IntegerValueTextBox" runat="server" Text='<%# Bind("IntegerValue") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="IntegerValueTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Value:
                </td>
                <td>
                    <asp:TextBox ID="ValueTextBox" runat="server" Text='<%# Bind("Value") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    ConfigurationId:
                </td>
                <td>
                    <asp:TextBox ID="ConfigurationIdTextBox" runat="server" Text='<%# Bind("ConfigurationId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ConfigurationIdTextBox"
                        ErrorMessage="* Please enter ConfigurationId"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="ConfigurationIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
 
            <tr>
                <td>
                    ModuleId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListModuleId" runat="server" DataSourceID="ObjectDataSourceDDLModuleId"
                        DataTextField="Module" DataValueField="ModuleId" SelectedValue='<%# Bind("ModuleId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLModuleId" runat="server" TypeName="StaticInfoModule"
                        SelectMethod="ParameterModule">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListModuleId"
                        ErrorMessage="* Please enter ModuleId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Configuration:
                </td>
                <td>
                    <asp:TextBox ID="ConfigurationTextBox" runat="server" Text='<%# Bind("Configuration") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ConfigurationTextBox"
                        ErrorMessage="* Please enter Configuration"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Indicator:
                </td>
                <td>
                    <asp:CheckBox ID="IndicatorCheckBox" runat="server" Checked='<%# Bind("Indicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    IntegerValue:
                </td>
                <td>
                    <asp:TextBox ID="IntegerValueTextBox" runat="server" Text='<%# Bind("IntegerValue") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="IntegerValueTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Value:
                </td>
                <td>
                    <asp:TextBox ID="ValueTextBox" runat="server" Text='<%# Bind("Value") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLWarehouseId"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Module:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListModuleId" runat="server" DataSourceID="ObjectDataSourceDDLModuleId"
                        DataTextField="Module" DataValueField="ModuleId" SelectedValue='<%# Bind("ModuleId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLModuleId" runat="server" TypeName="StaticInfoModule"
                        SelectMethod="ParameterModule">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Configuration:
                </td>
                <td>
                    <asp:Label ID="ConfigurationLabel" runat="server" Text='<%# Bind("Configuration") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Indicator:
                </td>
                <td>
                    <asp:Label ID="IndicatorLabel" runat="server" Text='<%# Bind("Indicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    IntegerValue:
                </td>
                <td>
                    <asp:Label ID="IntegerValueLabel" runat="server" Text='<%# Bind("IntegerValue") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Value:
                </td>
                <td>
                    <asp:Label ID="ValueLabel" runat="server" Text='<%# Bind("Value") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ConfigurationGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceConfiguration" runat="server" TypeName="StaticInfoConfiguration"
    SelectMethod="GetConfiguration"
    DeleteMethod="DeleteConfiguration"
    UpdateMethod="UpdateConfiguration"
    InsertMethod="InsertConfiguration">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ConfigurationId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ConfigurationId" Type="Int32" DefaultValue="-1" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="ModuleId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Configuration" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Indicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="IntegerValue" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Value" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ConfigurationId" QueryStringField="ConfigurationId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="WarehouseId" QueryStringField="WarehouseId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ConfigurationId" Type="Int32" DefaultValue="-1" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="ModuleId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Configuration" Type="String" />
        <asp:Parameter Name="Indicator" Type="Boolean" />
        <asp:Parameter Name="IntegerValue" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Value" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
