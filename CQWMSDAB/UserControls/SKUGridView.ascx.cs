using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_SKUGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridSKU.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridSKU_SelectedIndexChanged
    protected void RadGridSKU_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridSKU.SelectedItems)
            {
                Session["SKUId"] = item.GetDataKeyValue("SKUId");
            }
        }
        catch { }
    }
    #endregion "RadGridSKU_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["UOMId"] != null)
          DropDownListUOMId.SelectedValue = Session["UOMId"].ToString();
    
        RadGridSKU.DataBind();
    }
    #endregion BindData
    
}
