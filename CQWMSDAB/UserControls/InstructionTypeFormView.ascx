<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InstructionTypeFormView.ascx.cs" Inherits="UserControls_InstructionTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>InstructionType</h3>
<asp:FormView ID="FormViewInstructionType" runat="server" DataKeyNames="InstructionTypeId" DataSourceID="ObjectDataSourceInstructionType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:TextBox ID="PriorityIdTextBox" runat="server" Text='<%# Bind("PriorityId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PriorityIdTextBox"
                        ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="PriorityIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionType:
                </td>
                <td>
                    <asp:TextBox ID="InstructionTypeTextBox" runat="server" Text='<%# Bind("InstructionType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="InstructionTypeTextBox"
                        ErrorMessage="* Please enter InstructionType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="InstructionTypeCodeTextBox" runat="server" Text='<%# Bind("InstructionTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="InstructionTypeCodeTextBox"
                        ErrorMessage="* Please enter InstructionTypeCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:TextBox ID="PriorityIdTextBox" runat="server" Text='<%# Bind("PriorityId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PriorityIdTextBox"
                        ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="PriorityIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionType:
                </td>
                <td>
                    <asp:TextBox ID="InstructionTypeTextBox" runat="server" Text='<%# Bind("InstructionType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="InstructionTypeTextBox"
                        ErrorMessage="* Please enter InstructionType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="InstructionTypeCodeTextBox" runat="server" Text='<%# Bind("InstructionTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="InstructionTypeCodeTextBox"
                        ErrorMessage="* Please enter InstructionTypeCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:Label ID="PriorityIdLabel" runat="server" Text='<%# Bind("PriorityId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionType:
                </td>
                <td>
                    <asp:Label ID="InstructionTypeLabel" runat="server" Text='<%# Bind("InstructionType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    InstructionTypeCode:
                </td>
                <td>
                    <asp:Label ID="InstructionTypeCodeLabel" runat="server" Text='<%# Bind("InstructionTypeCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/InstructionTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceInstructionType" runat="server" TypeName="StaticInfoInstructionType"
    SelectMethod="GetInstructionType"
    DeleteMethod="DeleteInstructionType"
    UpdateMethod="UpdateInstructionType"
    InsertMethod="InsertInstructionType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="InstructionType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="InstructionTypeId" QueryStringField="InstructionTypeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InstructionTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="InstructionType" Type="String" />
        <asp:Parameter Name="InstructionTypeCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
