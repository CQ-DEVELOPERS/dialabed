<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestGridView.ascx.cs" Inherits="UserControls_TestGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowTest()
{
   window.open("TestFormView.aspx", "_blank",
      "height=640px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelTestCode" runat="server" Text="TestCode:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxTestCode" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="LabelTest" runat="server" Text="Test:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxTest" runat="server"></asp:TextBox>
        </td>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"/>
        <td>
            <asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowTest();"/>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Visible="False"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewTest" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
TestId
"
                DataSourceID="ObjectDataSourceTest" OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewTest_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="False" ShowEditButton="True" />
        <asp:BoundField DataField="TestId" HeaderText="TestId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="TestId" />
                        <asp:BoundField DataField="TestCode" HeaderText="TestCode" SortExpression="TestCode" />
                        <asp:BoundField DataField="Test" HeaderText="Test" SortExpression="Test" />
                    <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:ObjectDataSource ID="ObjectDataSourceTest" runat="server" TypeName="StaticInfoTest"
    DeleteMethod="DeleteTest"
    InsertMethod="InsertTest"
    SelectMethod="SearchTest"
    UpdateMethod="UpdateTest">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TestId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="TextBoxTestCode" DefaultValue="%" Name="TestCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxTest" DefaultValue="%" Name="Test" PropertyName="Text" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TestId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TestCode" Type="String" />
        <asp:Parameter Name="Test" Type="String" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="TestId" Type="Int32" />
        <asp:Parameter Name="TestCode" Type="String" />
        <asp:Parameter Name="Test" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
