<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageUnitFormView.ascx.cs" Inherits="UserControls_StorageUnitFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>StorageUnit</h3>
<asp:FormView ID="FormViewStorageUnit" runat="server" DataKeyNames="StorageUnitId" DataSourceID="ObjectDataSourceStorageUnit" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    SKU:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListSKU" runat="server" DataSourceID="ObjectDataSourceDDLSKU"
                        DataTextField="SKU" DataValueField="SKUId" SelectedValue='<%# Bind("SKUId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLSKU" runat="server"
                        TypeName="StaticInfoSKU" SelectMethod="ParameterSKU">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListSKU" ErrorMessage="* Please enter SKUId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Product:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListProduct" runat="server" DataSourceID="ObjectDataSourceDDLProduct"
                        DataTextField="Product" DataValueField="ProductId" SelectedValue='<%# Bind("ProductId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLProduct" runat="server"
                        TypeName="StaticInfoProduct" SelectMethod="ParameterProduct">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListProduct" ErrorMessage="* Please enter ProductId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    SKU:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListSKU" runat="server" DataSourceID="ObjectDataSourceDDLSKU"
                        DataTextField="SKU" DataValueField="SKUId" SelectedValue='<%# Bind("SKUId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLSKU" runat="server"
                        TypeName="StaticInfoSKU" SelectMethod="ParameterSKU">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListSKU" ErrorMessage="* Please enter SKUId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Product:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListProduct" runat="server" DataSourceID="ObjectDataSourceDDLProduct"
                        DataTextField="Product" DataValueField="ProductId" SelectedValue='<%# Bind("ProductId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLProduct" runat="server"
                        TypeName="StaticInfoProduct" SelectMethod="ParameterProduct">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListProduct" ErrorMessage="* Please enter ProductId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    SKU:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListSKU" runat="server" DataSourceID="ObjectDataSourceDDLSKU"
                        DataTextField="SKU" DataValueField="SKUId" SelectedValue='<%# Bind("SKUId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLSKU" runat="server"
                        TypeName="StaticInfoSKU" SelectMethod="ParameterSKU">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Product:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListProduct" runat="server" DataSourceID="ObjectDataSourceDDLProduct"
                        DataTextField="Product" DataValueField="ProductId" SelectedValue='<%# Bind("ProductId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLProduct" runat="server"
                        TypeName="StaticInfoProduct" SelectMethod="ParameterProduct">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/StorageUnitGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceStorageUnit" runat="server" TypeName="StaticInfoStorageUnit"
    SelectMethod="GetStorageUnit"
    DeleteMethod="DeleteStorageUnit"
    UpdateMethod="UpdateStorageUnit"
    InsertMethod="InsertStorageUnit">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="SKUId" Type="Int32" />
        <asp:Parameter Name="ProductId" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="StorageUnitId" QueryStringField="StorageUnitId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="SKUId" Type="Int32" />
        <asp:Parameter Name="ProductId" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
