<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutboundLineFormView.ascx.cs" Inherits="UserControls_OutboundLineFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>OutboundLine</h3>
<asp:FormView ID="FormViewOutboundLine" runat="server" DataKeyNames="OutboundLineId" DataSourceID="ObjectDataSourceOutboundLine" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocument"
                        DataTextField="OutboundDocument" DataValueField="OutboundDocumentId" SelectedValue='<%# Bind("OutboundDocumentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocument" runat="server"
                        TypeName="StaticInfoOutboundDocument" SelectMethod="ParameterOutboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOutboundDocument" ErrorMessage="* Please enter OutboundDocumentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListStorageUnit" ErrorMessage="* Please enter StorageUnitId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    LineNumber:
                </td>
                <td>
                    <asp:TextBox ID="LineNumberTextBox" runat="server" Text='<%# Bind("LineNumber") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="LineNumberTextBox" ErrorMessage="* Please enter LineNumber"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="LineNumberTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="QuantityTextBox" ErrorMessage="* Please enter Quantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="QuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceDDLBatch"
                        DataTextField="Batch" DataValueField="BatchId" SelectedValue='<%# Bind("BatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBatch" runat="server"
                        TypeName="StaticInfoBatch" SelectMethod="ParameterBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocument"
                        DataTextField="OutboundDocument" DataValueField="OutboundDocumentId" SelectedValue='<%# Bind("OutboundDocumentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocument" runat="server"
                        TypeName="StaticInfoOutboundDocument" SelectMethod="ParameterOutboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOutboundDocument" ErrorMessage="* Please enter OutboundDocumentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListStorageUnit" ErrorMessage="* Please enter StorageUnitId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    LineNumber:
                </td>
                <td>
                    <asp:TextBox ID="LineNumberTextBox" runat="server" Text='<%# Bind("LineNumber") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="LineNumberTextBox" ErrorMessage="* Please enter LineNumber"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="LineNumberTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="QuantityTextBox" ErrorMessage="* Please enter Quantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="QuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceDDLBatch"
                        DataTextField="Batch" DataValueField="BatchId" SelectedValue='<%# Bind("BatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBatch" runat="server"
                        TypeName="StaticInfoBatch" SelectMethod="ParameterBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocument"
                        DataTextField="OutboundDocument" DataValueField="OutboundDocumentId" SelectedValue='<%# Bind("OutboundDocumentId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocument" runat="server"
                        TypeName="StaticInfoOutboundDocument" SelectMethod="ParameterOutboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    LineNumber:
                </td>
                <td>
                    <asp:Label ID="LineNumberLabel" runat="server" Text='<%# Bind("LineNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceDDLBatch"
                        DataTextField="Batch" DataValueField="BatchId" SelectedValue='<%# Bind("BatchId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBatch" runat="server"
                        TypeName="StaticInfoBatch" SelectMethod="ParameterBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/OutboundLineGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceOutboundLine" runat="server" TypeName="StaticInfoOutboundLine"
    SelectMethod="GetOutboundLine"
    DeleteMethod="DeleteOutboundLine"
    UpdateMethod="UpdateOutboundLine"
    InsertMethod="InsertOutboundLine">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundLineId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundLineId" Type="Int32" />
        <asp:Parameter Name="OutboundDocumentId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="LineNumber" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="BatchId" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="OutboundLineId" QueryStringField="OutboundLineId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundLineId" Type="Int32" />
        <asp:Parameter Name="OutboundDocumentId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="LineNumber" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="BatchId" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
