<%@ Control Language="C#" AutoEventWireup="true" CodeFile="COAGridView.ascx.cs" Inherits="UserControls_COAGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowCOA()
{
   window.open("COAFormView.aspx", "_blank",
      "height=640px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelCOACode" runat="server" Text="COACode:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCOACode" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="LabelCOA" runat="server" Text="COA:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCOA" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="LabelStorageUnitBatchId" runat="server" Text="StorageUnitBatch:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStorageUnitBatchId" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatchId"
                DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatchId" runat="server" TypeName="StaticInfoStorageUnitBatch"
                SelectMethod="ParameterStorageUnitBatch">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"/>
        <td>
            <asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowCOA();"/>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Visible="False"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewCOA" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
COAId
"
                DataSourceID="ObjectDataSourceCOA" OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewCOA_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="False" ShowEditButton="True" />
        <asp:BoundField DataField="COAId" HeaderText="COAId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="COAId" />
                        <asp:BoundField DataField="COACode" HeaderText="COACode" SortExpression="COACode" />
        <asp:TemplateField HeaderText="COA" SortExpression="COA">
            <EditItemTemplate>
                <asp:TextBox ID="TextBoxCOA" runat="server" Text='<%# Bind("COA") %>' TextMode="MultiLine" Rows="10" Width="300px" AutoCompleteType="Notes"></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="LabelCOA" runat="server" Text='<%# Bind("COA") %>' ></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
            <asp:TemplateField HeaderText="StorageUnitBatchId" meta:resourcekey="StorageUnitBatchId" SortExpression="StorageUnitBatchId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStorageUnitBatchId" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatchId"
                        DataTextField="StorageUnitBatchId" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatchId" runat="server" TypeName="StaticInfoStorageUnitBatch"
                        SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStorageUnitBatchId" runat="server" Text='<%# Bind("StorageUnitBatch") %>' NavigateUrl="~/Administration/StorageUnitBatchGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:ObjectDataSource ID="ObjectDataSourceCOA" runat="server" TypeName="StaticInfoCOA"
    DeleteMethod="DeleteCOA"
    InsertMethod="InsertCOA"
    SelectMethod="SearchCOA"
    UpdateMethod="UpdateCOA">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="TextBoxCOACode" DefaultValue="%" Name="COACode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxCOA" DefaultValue="%" Name="COA" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="DropDownListStorageUnitBatchId" DefaultValue="-1" Name="StorageUnitBatchId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COAId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COACode" Type="String" />
        <asp:Parameter Name="COA" Type="String" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="COAId" Type="Int32" />
        <asp:Parameter Name="COACode" Type="String" />
        <asp:Parameter Name="COA" Type="String" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
