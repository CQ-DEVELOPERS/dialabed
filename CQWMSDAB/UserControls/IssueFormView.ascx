<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IssueFormView.ascx.cs" Inherits="UserControls_IssueFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Issue</h3>
<asp:FormView ID="FormViewIssue" runat="server" DataKeyNames="IssueId" DataSourceID="ObjectDataSourceIssue" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocument"
                        DataTextField="OutboundDocument" DataValueField="OutboundDocumentId" SelectedValue='<%# Bind("OutboundDocumentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocument" runat="server"
                        TypeName="StaticInfoOutboundDocument" SelectMethod="ParameterOutboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOutboundDocument" ErrorMessage="* Please enter OutboundDocumentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourceDDLPriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriority" runat="server"
                        TypeName="StaticInfoPriority" SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListPriority" ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Route:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceDDLRoute"
                        DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRoute" runat="server"
                        TypeName="StaticInfoRoute" SelectMethod="ParameterRoute">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteNumber:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryNoteNumberTextBox" runat="server" Text='<%# Bind("DeliveryNoteNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryDateTextBox" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SealNumber:
                </td>
                <td>
                    <asp:TextBox ID="SealNumberTextBox" runat="server" Text='<%# Bind("SealNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    VehicleRegistration:
                </td>
                <td>
                    <asp:TextBox ID="VehicleRegistrationTextBox" runat="server" Text='<%# Bind("VehicleRegistration") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Remarks:
                </td>
                <td>
                    <asp:TextBox ID="RemarksTextBox" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:TextBox ID="DropSequenceTextBox" runat="server" Text='<%# Bind("DropSequence") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="DropSequenceTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocument"
                        DataTextField="OutboundDocument" DataValueField="OutboundDocumentId" SelectedValue='<%# Bind("OutboundDocumentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocument" runat="server"
                        TypeName="StaticInfoOutboundDocument" SelectMethod="ParameterOutboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOutboundDocument" ErrorMessage="* Please enter OutboundDocumentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourceDDLPriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriority" runat="server"
                        TypeName="StaticInfoPriority" SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListPriority" ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Route:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceDDLRoute"
                        DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRoute" runat="server"
                        TypeName="StaticInfoRoute" SelectMethod="ParameterRoute">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteNumber:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryNoteNumberTextBox" runat="server" Text='<%# Bind("DeliveryNoteNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryDateTextBox" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SealNumber:
                </td>
                <td>
                    <asp:TextBox ID="SealNumberTextBox" runat="server" Text='<%# Bind("SealNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    VehicleRegistration:
                </td>
                <td>
                    <asp:TextBox ID="VehicleRegistrationTextBox" runat="server" Text='<%# Bind("VehicleRegistration") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Remarks:
                </td>
                <td>
                    <asp:TextBox ID="RemarksTextBox" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:TextBox ID="DropSequenceTextBox" runat="server" Text='<%# Bind("DropSequence") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="DropSequenceTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocument"
                        DataTextField="OutboundDocument" DataValueField="OutboundDocumentId" SelectedValue='<%# Bind("OutboundDocumentId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocument" runat="server"
                        TypeName="StaticInfoOutboundDocument" SelectMethod="ParameterOutboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourceDDLPriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriority" runat="server"
                        TypeName="StaticInfoPriority" SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouse" runat="server" DataSourceID="ObjectDataSourceDDLWarehouse"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouse" runat="server"
                        TypeName="StaticInfoWarehouse" SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Route:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceDDLRoute"
                        DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRoute" runat="server"
                        TypeName="StaticInfoRoute" SelectMethod="ParameterRoute">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteNumber:
                </td>
                <td>
                    <asp:Label ID="DeliveryNoteNumberLabel" runat="server" Text='<%# Bind("DeliveryNoteNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:Label ID="DeliveryDateLabel" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SealNumber:
                </td>
                <td>
                    <asp:Label ID="SealNumberLabel" runat="server" Text='<%# Bind("SealNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    VehicleRegistration:
                </td>
                <td>
                    <asp:Label ID="VehicleRegistrationLabel" runat="server" Text='<%# Bind("VehicleRegistration") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Remarks:
                </td>
                <td>
                    <asp:Label ID="RemarksLabel" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:Label ID="DropSequenceLabel" runat="server" Text='<%# Bind("DropSequence") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/IssueGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceIssue" runat="server" TypeName="StaticInfoIssue"
    SelectMethod="GetIssue"
    DeleteMethod="DeleteIssue"
    UpdateMethod="UpdateIssue"
    InsertMethod="InsertIssue">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="IssueId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="OutboundDocumentId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="RouteId" Type="Int32" />
        <asp:Parameter Name="DeliveryNoteNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="SealNumber" Type="String" />
        <asp:Parameter Name="VehicleRegistration" Type="String" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="DropSequence" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="IssueId" QueryStringField="IssueId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="OutboundDocumentId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="RouteId" Type="Int32" />
        <asp:Parameter Name="DeliveryNoteNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="SealNumber" Type="String" />
        <asp:Parameter Name="VehicleRegistration" Type="String" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="DropSequence" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
