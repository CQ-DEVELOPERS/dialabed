<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TaskListFormView.ascx.cs" Inherits="UserControls_TaskListFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>TaskList</h3>
<asp:FormView ID="FormViewTaskList" runat="server" DataKeyNames="TaskListId" DataSourceID="ObjectDataSourceTaskList" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ProjectId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListProjectId" runat="server" DataSourceID="ObjectDataSourceDDLProjectId"
                        DataTextField="Project" DataValueField="ProjectId" SelectedValue='<%# Bind("ProjectId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLProjectId" runat="server" TypeName="StaticInfoProject"
                        SelectMethod="ParameterProject">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    RequestTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRequestTypeId" runat="server" DataSourceID="ObjectDataSourceDDLRequestTypeId"
                        DataTextField="RequestType" DataValueField="RequestTypeId" SelectedValue='<%# Bind("RequestTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRequestTypeId" runat="server" TypeName="StaticInfoRequestType"
                        SelectMethod="ParameterRequestType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    BillableId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBillableId" runat="server" DataSourceID="ObjectDataSourceDDLBillableId"
                        DataTextField="Billable" DataValueField="BillableId" SelectedValue='<%# Bind("BillableId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBillableId" runat="server" TypeName="StaticInfoBillable"
                        SelectMethod="ParameterBillable">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    TaskList:
                </td>
                <td>
                    <asp:TextBox ID="TaskListTextBox" runat="server" Text='<%# Bind("TaskList") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Comments:
                </td>
                <td>
                    <asp:TextBox ID="CommentsTextBox" runat="server" Text='<%# Bind("Comments") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RaisedBy:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRaisedBy" runat="server" DataSourceID="ObjectDataSourceDDLRaisedBy"
                        DataTextField="Operator" DataValueField="RaisedBy" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRaisedBy" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ApprovedBy:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListApprovedBy" runat="server" DataSourceID="ObjectDataSourceDDLApprovedBy"
                        DataTextField="Operator" DataValueField="ApprovedBy" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLApprovedBy" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    AllocatedTo:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListAllocatedTo" runat="server" DataSourceID="ObjectDataSourceDDLAllocatedTo"
                        DataTextField="Operator" DataValueField="AllocatedTo" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLAllocatedTo" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Tester:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTester" runat="server" DataSourceID="ObjectDataSourceDDLTester"
                        DataTextField="Operator" DataValueField="Tester" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTester" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    EstimateHours:
                </td>
                <td>
                    <asp:TextBox ID="EstimateHoursTextBox" runat="server" Text='<%# Bind("EstimateHours") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActualHours:
                </td>
                <td>
                    <asp:TextBox ID="ActualHoursTextBox" runat="server" Text='<%# Bind("ActualHours") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    TargetDate:
                </td>
                <td>
                    <asp:TextBox ID="TargetDateTextBox" runat="server" Text='<%# Bind("TargetDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage:
                </td>
                <td>
                    <asp:TextBox ID="PercentageTextBox" runat="server" Text='<%# Bind("Percentage") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CycleId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCycleId" runat="server" DataSourceID="ObjectDataSourceDDLCycleId"
                        DataTextField="Cycle" DataValueField="CycleId" SelectedValue='<%# Bind("CycleId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCycleId" runat="server" TypeName="StaticInfoCycle"
                        SelectMethod="ParameterCycle">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator18" runat="server" ControlToValidate="OrderByTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    PriorityId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ProjectId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListProjectId" runat="server" DataSourceID="ObjectDataSourceDDLProjectId"
                        DataTextField="Project" DataValueField="ProjectId" SelectedValue='<%# Bind("ProjectId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLProjectId" runat="server" TypeName="StaticInfoProject"
                        SelectMethod="ParameterProject">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    RequestTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRequestTypeId" runat="server" DataSourceID="ObjectDataSourceDDLRequestTypeId"
                        DataTextField="RequestType" DataValueField="RequestTypeId" SelectedValue='<%# Bind("RequestTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRequestTypeId" runat="server" TypeName="StaticInfoRequestType"
                        SelectMethod="ParameterRequestType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    BillableId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBillableId" runat="server" DataSourceID="ObjectDataSourceDDLBillableId"
                        DataTextField="Billable" DataValueField="BillableId" SelectedValue='<%# Bind("BillableId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBillableId" runat="server" TypeName="StaticInfoBillable"
                        SelectMethod="ParameterBillable">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    TaskList:
                </td>
                <td>
                    <asp:TextBox ID="TaskListTextBox" runat="server" Text='<%# Bind("TaskList") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Comments:
                </td>
                <td>
                    <asp:TextBox ID="CommentsTextBox" runat="server" Text='<%# Bind("Comments") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RaisedBy:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRaisedBy" runat="server" DataSourceID="ObjectDataSourceDDLRaisedBy"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("RaisedBy") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRaisedBy" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ApprovedBy:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListApprovedBy" runat="server" DataSourceID="ObjectDataSourceDDLApprovedBy"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("ApprovedBy") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLApprovedBy" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    AllocatedTo:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListAllocatedTo" runat="server" DataSourceID="ObjectDataSourceDDLAllocatedTo"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("AllocatedTo") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLAllocatedTo" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Tester:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTester" runat="server" DataSourceID="ObjectDataSourceDDLTester"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("Tester") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTester" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    EstimateHours:
                </td>
                <td>
                    <asp:TextBox ID="EstimateHoursTextBox" runat="server" Text='<%# Bind("EstimateHours") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActualHours:
                </td>
                <td>
                    <asp:TextBox ID="ActualHoursTextBox" runat="server" Text='<%# Bind("ActualHours") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    TargetDate:
                </td>
                <td>
                    <asp:TextBox ID="TargetDateTextBox" runat="server" Text='<%# Bind("TargetDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage:
                </td>
                <td>
                    <asp:TextBox ID="PercentageTextBox" runat="server" Text='<%# Bind("Percentage") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CycleId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCycleId" runat="server" DataSourceID="ObjectDataSourceDDLCycleId"
                        DataTextField="Cycle" DataValueField="CycleId" SelectedValue='<%# Bind("CycleId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCycleId" runat="server" TypeName="StaticInfoCycle"
                        SelectMethod="ParameterCycle">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator18" runat="server" ControlToValidate="OrderByTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                        SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Project:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListProjectId" runat="server" DataSourceID="ObjectDataSourceDDLProjectId"
                        DataTextField="Project" DataValueField="ProjectId" SelectedValue='<%# Bind("ProjectId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLProjectId" runat="server" TypeName="StaticInfoProject"
                        SelectMethod="ParameterProject">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    RequestType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRequestTypeId" runat="server" DataSourceID="ObjectDataSourceDDLRequestTypeId"
                        DataTextField="RequestType" DataValueField="RequestTypeId" SelectedValue='<%# Bind("RequestTypeId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRequestTypeId" runat="server" TypeName="StaticInfoRequestType"
                        SelectMethod="ParameterRequestType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Billable:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBillableId" runat="server" DataSourceID="ObjectDataSourceDDLBillableId"
                        DataTextField="Billable" DataValueField="BillableId" SelectedValue='<%# Bind("BillableId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBillableId" runat="server" TypeName="StaticInfoBillable"
                        SelectMethod="ParameterBillable">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    TaskList:
                </td>
                <td>
                    <asp:Label ID="TaskListLabel" runat="server" Text='<%# Bind("TaskList") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Comments:
                </td>
                <td>
                    <asp:Label ID="CommentsLabel" runat="server" Text='<%# Bind("Comments") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRaisedBy" runat="server" DataSourceID="ObjectDataSourceDDLRaisedBy"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("RaisedBy") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRaisedBy" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListApprovedBy" runat="server" DataSourceID="ObjectDataSourceDDLApprovedBy"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("ApprovedBy") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLApprovedBy" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListAllocatedTo" runat="server" DataSourceID="ObjectDataSourceDDLAllocatedTo"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("AllocatedTo") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLAllocatedTo" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTester" runat="server" DataSourceID="ObjectDataSourceDDLTester"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("Tester") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTester" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    EstimateHours:
                </td>
                <td>
                    <asp:Label ID="EstimateHoursLabel" runat="server" Text='<%# Bind("EstimateHours") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ActualHours:
                </td>
                <td>
                    <asp:Label ID="ActualHoursLabel" runat="server" Text='<%# Bind("ActualHours") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:Label ID="CreateDateLabel" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    TargetDate:
                </td>
                <td>
                    <asp:Label ID="TargetDateLabel" runat="server" Text='<%# Bind("TargetDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Percentage:
                </td>
                <td>
                    <asp:Label ID="PercentageLabel" runat="server" Text='<%# Bind("Percentage") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Cycle:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCycleId" runat="server" DataSourceID="ObjectDataSourceDDLCycleId"
                        DataTextField="Cycle" DataValueField="CycleId" SelectedValue='<%# Bind("CycleId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCycleId" runat="server" TypeName="StaticInfoCycle"
                        SelectMethod="ParameterCycle">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:Label ID="OrderByLabel" runat="server" Text='<%# Bind("OrderBy") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/TaskListGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceTaskList" runat="server" TypeName="StaticInfoTaskList"
    SelectMethod="GetTaskList"
    DeleteMethod="DeleteTaskList"
    UpdateMethod="UpdateTaskList"
    InsertMethod="InsertTaskList">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TaskListId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TaskListId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ProjectId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RequestTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="BillableId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TaskList" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Comments" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="RaisedBy" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ApprovedBy" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AllocatedTo" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Tester" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="EstimateHours" Type="Double" DefaultValue="-1" />
        <asp:Parameter Name="ActualHours" Type="Double" DefaultValue="-1" />
        <asp:Parameter Name="CreateDate" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="TargetDate" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="Percentage" Type="Double" DefaultValue="-1" />
        <asp:Parameter Name="CycleId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="TaskListId" QueryStringField="TaskListId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TaskListId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ProjectId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RequestTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="BillableId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TaskList" Type="String" />
        <asp:Parameter Name="Comments" Type="String" />
        <asp:Parameter Name="RaisedBy" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ApprovedBy" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AllocatedTo" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Tester" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="EstimateHours" Type="Double" />
        <asp:Parameter Name="ActualHours" Type="Double" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="TargetDate" Type="DateTime" />
        <asp:Parameter Name="Percentage" Type="Double" />
        <asp:Parameter Name="CycleId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
