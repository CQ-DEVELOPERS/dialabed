<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InboundDocumentGridView.ascx.cs" Inherits="UserControls_InboundDocumentGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowInboundDocument()
{
   window.open("InboundDocumentFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelInboundDocumentTypeId" runat="server" Text="<%$ Resources:Default, LabelInboundDocumentType %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataSourceID="SqlDataSourceInboundDocumentTypeDDL"
                DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceInboundDocumentTypeDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_InboundDocumentType_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelExternalCompanyId" runat="server" Text="<%$ Resources:Default, LabelExternalCompany %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListExternalCompany" runat="server" DataSourceID="SqlDataSourceExternalCompanyDDL"
                DataTextField="ExternalCompany" DataValueField="ExternalCompanyId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceExternalCompanyDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_ExternalCompany_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelStatusId" runat="server" Text="<%$ Resources:Default, LabelStatus %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceStatusDDL"
                DataTextField="Status" DataValueField="StatusId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStatusDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Status_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="<%$ Resources:Default, LabelShow %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="<%$ Resources:Default, ButtonNew %>" OnClientClick="javascript:openNewWindowInboundDocument();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, ButtonEdit %>" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, ButtonDelete %>" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="<%$ Resources:Default, ButtonBack %>" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="<%$ Resources:Default, ButtonPrevious %>" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="<%$ Resources:Default, ButtonHelp %>"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewInboundDocument" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
InboundDocumentId
"
                DataSourceID="SqlDataSourceInboundDocument" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewInboundDocument_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="InboundDocumentId" HeaderText="InboundDocumentId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="InboundDocumentId" />
            <asp:TemplateField HeaderText="InboundDocumentType" meta:resourcekey="InboundDocumentType" SortExpression="InboundDocumentTypeId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataSourceID="SqlDataSourceDDLInboundDocumentType"
                        DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId" SelectedValue='<%# Bind("InboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLInboundDocumentType" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_InboundDocumentType_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkInboundDocumentTypeId" runat="server" Text='<%# Bind("InboundDocumentType") %>' NavigateUrl="~/Administration/InboundDocumentTypeGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="InboundDocumentType" HeaderText="InboundDocumentType" ReadOnly="True" SortExpression="InboundDocumentType" Visible="False" />
            <asp:TemplateField HeaderText="ExternalCompany" meta:resourcekey="ExternalCompany" SortExpression="ExternalCompanyId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListExternalCompany" runat="server" DataSourceID="SqlDataSourceDDLExternalCompany"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLExternalCompany" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_ExternalCompany_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkExternalCompanyId" runat="server" Text='<%# Bind("ExternalCompany") %>' NavigateUrl="~/Administration/ExternalCompanyGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ExternalCompany" HeaderText="ExternalCompany" ReadOnly="True" SortExpression="ExternalCompany" Visible="False" />
            <asp:TemplateField HeaderText="Status" meta:resourcekey="Status" SortExpression="StatusId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Status_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStatusId" runat="server" Text='<%# Bind("Status") %>' NavigateUrl="~/Administration/StatusGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" Visible="False" />
                        <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" ReadOnly="True" SortExpression="Warehouse" Visible="False" />
                        <asp:BoundField DataField="OrderNumber" HeaderText="OrderNumber" SortExpression="OrderNumber" />
                        <asp:TemplateField HeaderText="DeliveryDate" SortExpression="DeliveryDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonDeliveryDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('DeliveryDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelDeliveryDate" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CreateDate" SortExpression="CreateDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonCreateDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('CreateDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ModifiedDate" SortExpression="ModifiedDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonModifiedDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('ModifiedDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelModifiedDate" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceInboundDocument" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_InboundDocument_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_InboundDocument_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_InboundDocument_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_InboundDocument_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListInboundDocumentType" DefaultValue="" Name="InboundDocumentTypeId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListExternalCompany" DefaultValue="" Name="ExternalCompanyId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="" Name="StatusId" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="OrderNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ModifiedDate" Type="DateTime" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="InboundDocumentId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="OrderNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ModifiedDate" Type="DateTime" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
