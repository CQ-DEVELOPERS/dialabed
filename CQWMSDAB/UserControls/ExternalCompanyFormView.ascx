<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExternalCompanyFormView.ascx.cs" Inherits="UserControls_ExternalCompanyFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>ExternalCompany</h3>
<asp:FormView ID="FormViewExternalCompany" runat="server" DataKeyNames="ExternalCompanyId" DataSourceID="ObjectDataSourceExternalCompany" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyTypeId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyTypeId"
                        DataTextField="ExternalCompanyType" DataValueField="ExternalCompanyTypeId" SelectedValue='<%# Bind("ExternalCompanyTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyTypeId" runat="server" TypeName="StaticInfoExternalCompanyType"
                        SelectMethod="ParameterExternalCompanyType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListExternalCompanyTypeId"
                        ErrorMessage="* Please enter ExternalCompanyTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RouteId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRouteId" runat="server" DataSourceID="ObjectDataSourceDDLRouteId"
                        DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRouteId" runat="server" TypeName="StaticInfoRoute"
                        SelectMethod="ParameterRoute">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:TextBox ID="ExternalCompanyTextBox" runat="server" Text='<%# Bind("ExternalCompany") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ExternalCompanyTextBox"
                        ErrorMessage="* Please enter ExternalCompany"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompanyCode:
                </td>
                <td>
                    <asp:TextBox ID="ExternalCompanyCodeTextBox" runat="server" Text='<%# Bind("ExternalCompanyCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ExternalCompanyCodeTextBox"
                        ErrorMessage="* Please enter ExternalCompanyCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Rating:
                </td>
                <td>
                    <asp:TextBox ID="RatingTextBox" runat="server" Text='<%# Bind("Rating") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="RatingTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RedeliveryIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="RedeliveryIndicatorCheckBox" runat="server" Checked='<%# Bind("RedeliveryIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    QualityAssuranceIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="QualityAssuranceIndicatorCheckBox" runat="server" Checked='<%# Bind("QualityAssuranceIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ContainerTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListContainerTypeId" runat="server" DataSourceID="ObjectDataSourceDDLContainerTypeId"
                        DataTextField="ContainerType" DataValueField="ContainerTypeId" SelectedValue='<%# Bind("ContainerTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLContainerTypeId" runat="server" TypeName="StaticInfoContainerType"
                        SelectMethod="ParameterContainerType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Backorder:
                </td>
                <td>
                    <asp:CheckBox ID="BackorderCheckBox" runat="server" Checked='<%# Bind("Backorder") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HostId:
                </td>
                <td>
                    <asp:TextBox ID="HostIdTextBox" runat="server" Text='<%# Bind("HostId") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    OrderLineSequence:
                </td>
                <td>
                    <asp:CheckBox ID="OrderLineSequenceCheckBox" runat="server" Checked='<%# Bind("OrderLineSequence") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:TextBox ID="DropSequenceTextBox" runat="server" Text='<%# Bind("DropSequence") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="DropSequenceTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AutoInvoice:
                </td>
                <td>
                    <asp:CheckBox ID="AutoInvoiceCheckBox" runat="server" Checked='<%# Bind("AutoInvoice") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyTypeId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyTypeId"
                        DataTextField="ExternalCompanyType" DataValueField="ExternalCompanyTypeId" SelectedValue='<%# Bind("ExternalCompanyTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyTypeId" runat="server" TypeName="StaticInfoExternalCompanyType"
                        SelectMethod="ParameterExternalCompanyType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListExternalCompanyTypeId"
                        ErrorMessage="* Please enter ExternalCompanyTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RouteId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRouteId" runat="server" DataSourceID="ObjectDataSourceDDLRouteId"
                        DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRouteId" runat="server" TypeName="StaticInfoRoute"
                        SelectMethod="ParameterRoute">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:TextBox ID="ExternalCompanyTextBox" runat="server" Text='<%# Bind("ExternalCompany") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ExternalCompanyTextBox"
                        ErrorMessage="* Please enter ExternalCompany"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompanyCode:
                </td>
                <td>
                    <asp:TextBox ID="ExternalCompanyCodeTextBox" runat="server" Text='<%# Bind("ExternalCompanyCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ExternalCompanyCodeTextBox"
                        ErrorMessage="* Please enter ExternalCompanyCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Rating:
                </td>
                <td>
                    <asp:TextBox ID="RatingTextBox" runat="server" Text='<%# Bind("Rating") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="RatingTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RedeliveryIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="RedeliveryIndicatorCheckBox" runat="server" Checked='<%# Bind("RedeliveryIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    QualityAssuranceIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="QualityAssuranceIndicatorCheckBox" runat="server" Checked='<%# Bind("QualityAssuranceIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ContainerTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListContainerTypeId" runat="server" DataSourceID="ObjectDataSourceDDLContainerTypeId"
                        DataTextField="ContainerType" DataValueField="ContainerTypeId" SelectedValue='<%# Bind("ContainerTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLContainerTypeId" runat="server" TypeName="StaticInfoContainerType"
                        SelectMethod="ParameterContainerType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Backorder:
                </td>
                <td>
                    <asp:CheckBox ID="BackorderCheckBox" runat="server" Checked='<%# Bind("Backorder") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HostId:
                </td>
                <td>
                    <asp:TextBox ID="HostIdTextBox" runat="server" Text='<%# Bind("HostId") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    OrderLineSequence:
                </td>
                <td>
                    <asp:CheckBox ID="OrderLineSequenceCheckBox" runat="server" Checked='<%# Bind("OrderLineSequence") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:TextBox ID="DropSequenceTextBox" runat="server" Text='<%# Bind("DropSequence") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="DropSequenceTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AutoInvoice:
                </td>
                <td>
                    <asp:CheckBox ID="AutoInvoiceCheckBox" runat="server" Checked='<%# Bind("AutoInvoice") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyTypeId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyTypeId"
                        DataTextField="ExternalCompanyType" DataValueField="ExternalCompanyTypeId" SelectedValue='<%# Bind("ExternalCompanyTypeId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyTypeId" runat="server" TypeName="StaticInfoExternalCompanyType"
                        SelectMethod="ParameterExternalCompanyType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Route:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListRouteId" runat="server" DataSourceID="ObjectDataSourceDDLRouteId"
                        DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLRouteId" runat="server" TypeName="StaticInfoRoute"
                        SelectMethod="ParameterRoute">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:Label ID="ExternalCompanyLabel" runat="server" Text='<%# Bind("ExternalCompany") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompanyCode:
                </td>
                <td>
                    <asp:Label ID="ExternalCompanyCodeLabel" runat="server" Text='<%# Bind("ExternalCompanyCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Rating:
                </td>
                <td>
                    <asp:Label ID="RatingLabel" runat="server" Text='<%# Bind("Rating") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RedeliveryIndicator:
                </td>
                <td>
                    <asp:Label ID="RedeliveryIndicatorLabel" runat="server" Text='<%# Bind("RedeliveryIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    QualityAssuranceIndicator:
                </td>
                <td>
                    <asp:Label ID="QualityAssuranceIndicatorLabel" runat="server" Text='<%# Bind("QualityAssuranceIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ContainerType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListContainerTypeId" runat="server" DataSourceID="ObjectDataSourceDDLContainerTypeId"
                        DataTextField="ContainerType" DataValueField="ContainerTypeId" SelectedValue='<%# Bind("ContainerTypeId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLContainerTypeId" runat="server" TypeName="StaticInfoContainerType"
                        SelectMethod="ParameterContainerType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Backorder:
                </td>
                <td>
                    <asp:Label ID="BackorderLabel" runat="server" Text='<%# Bind("Backorder") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HostId:
                </td>
                <td>
                    <asp:Label ID="HostIdLabel" runat="server" Text='<%# Bind("HostId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OrderLineSequence:
                </td>
                <td>
                    <asp:Label ID="OrderLineSequenceLabel" runat="server" Text='<%# Bind("OrderLineSequence") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:Label ID="DropSequenceLabel" runat="server" Text='<%# Bind("DropSequence") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AutoInvoice:
                </td>
                <td>
                    <asp:Label ID="AutoInvoiceLabel" runat="server" Text='<%# Bind("AutoInvoice") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ExternalCompanyGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceExternalCompany" runat="server" TypeName="StaticInfoExternalCompany"
    SelectMethod="GetExternalCompany"
    DeleteMethod="DeleteExternalCompany"
    UpdateMethod="UpdateExternalCompany"
    InsertMethod="InsertExternalCompany">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RouteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompany" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Rating" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RedeliveryIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="QualityAssuranceIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="ContainerTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Backorder" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="HostId" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="OrderLineSequence" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="DropSequence" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AutoInvoice" Type="Boolean" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ExternalCompanyId" QueryStringField="ExternalCompanyId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RouteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompany" Type="String" />
        <asp:Parameter Name="ExternalCompanyCode" Type="String" />
        <asp:Parameter Name="Rating" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RedeliveryIndicator" Type="Boolean" />
        <asp:Parameter Name="QualityAssuranceIndicator" Type="Boolean" />
        <asp:Parameter Name="ContainerTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Backorder" Type="Boolean" />
        <asp:Parameter Name="HostId" Type="String" />
        <asp:Parameter Name="OrderLineSequence" Type="Boolean" />
        <asp:Parameter Name="DropSequence" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AutoInvoice" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
