﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExportButtons.ascx.cs" Inherits="UserControls_ExportButtons" %>
<div style="margin-right:5px;">
    <label style='font: normal 12px "Segoe UI",Arial,Helvetica,sans-serif; font-weight:bold;vertical-align: super;'>Export: </label>
    <asp:ImageButton ID="Pdf" Visible="false" runat="server" ImageUrl="~/Images/Export_pdf.png" Height="20" Width="17"
        OnClick="ImageButton_Click" ToolTip="Export to PDF" AlternateText="Export to PDF" />
    <asp:ImageButton ID="Excel" runat="server" ImageUrl="~/Images/Export_Excel.png" Height="20" Width="17"
        OnClick="ImageButton_Click" ToolTip="Export to Excel" AlternateText="Export to Excel" />
    <asp:ImageButton ID="CSV" runat="server" ImageUrl="~/Images/Export_CSV.png" Height="20" Width="17"
        OnClick="ImageButton_Click" ToolTip="Export to CSV" AlternateText="Export to CSV" />
    <asp:ImageButton ID="Word" runat="server" ImageUrl="~/Images/Export_Word.png" Height="20" Width="17"
        OnClick="ImageButton_Click" ToolTip="Export to Word" AlternateText="Export to Word" />
</div>