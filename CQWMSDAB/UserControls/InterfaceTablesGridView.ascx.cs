using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_InterfaceTablesGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridInterfaceTables.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridInterfaceTables_SelectedIndexChanged
    protected void RadGridInterfaceTables_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridInterfaceTables.SelectedItems)
            {
                Session["InterfaceTablesId"] = item.GetDataKeyValue("InterfaceTablesId");
            }
        }
        catch { }
    }
    #endregion "RadGridInterfaceTables_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
    
        RadGridInterfaceTables.DataBind();
    }
    #endregion BindData
    
}
