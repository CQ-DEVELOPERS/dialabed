<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyFormView.ascx.cs" Inherits="UserControls_CompanyFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Company</h3>
<asp:FormView ID="FormViewCompany" runat="server" DataKeyNames="CompanyId" DataSourceID="ObjectDataSourceCompany" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Company:
                </td>
                <td>
                    <asp:TextBox ID="CompanyTextBox" runat="server" Text='<%# Bind("Company") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CompanyTextBox" ErrorMessage="* Please enter Company"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CompanyCode:
                </td>
                <td>
                    <asp:TextBox ID="CompanyCodeTextBox" runat="server" Text='<%# Bind("CompanyCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Company:
                </td>
                <td>
                    <asp:TextBox ID="CompanyTextBox" runat="server" Text='<%# Bind("Company") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CompanyTextBox" ErrorMessage="* Please enter Company"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CompanyCode:
                </td>
                <td>
                    <asp:TextBox ID="CompanyCodeTextBox" runat="server" Text='<%# Bind("CompanyCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Company:
                </td>
                <td>
                    <asp:Label ID="CompanyLabel" runat="server" Text='<%# Bind("Company") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CompanyCode:
                </td>
                <td>
                    <asp:Label ID="CompanyCodeLabel" runat="server" Text='<%# Bind("CompanyCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBAck %>" PostBackUrl="~/Administration/CompanyGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceCompany" runat="server" TypeName="StaticInfoCompany"
    SelectMethod="GetCompany"
    DeleteMethod="DeleteCompany"
    UpdateMethod="UpdateCompany"
    InsertMethod="InsertCompany">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CompanyId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CompanyId" Type="Int32" />
        <asp:Parameter Name="Company" Type="String" />
        <asp:Parameter Name="CompanyCode" Type="String" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="CompanyId" QueryStringField="CompanyId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CompanyId" Type="Int32" />
        <asp:Parameter Name="Company" Type="String" />
        <asp:Parameter Name="CompanyCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
