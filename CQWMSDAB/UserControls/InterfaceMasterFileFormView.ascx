<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceMasterFileFormView.ascx.cs" Inherits="UserControls_InterfaceMasterFileFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>InterfaceMasterFile</h3>
<asp:FormView ID="FormViewInterfaceMasterFile" runat="server" DataKeyNames="InterfaceMasterFileId" DataSourceID="ObjectDataSourceInterfaceMasterFile" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    MasterFile:
                </td>
                <td>
                    <asp:TextBox ID="MasterFileTextBox" runat="server" Text='<%# Bind("MasterFile") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    MasterFileDescription:
                </td>
                <td>
                    <asp:TextBox ID="MasterFileDescriptionTextBox" runat="server" Text='<%# Bind("MasterFileDescription") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    LastDate:
                </td>
                <td>
                    <asp:TextBox ID="LastDateTextBox" runat="server" Text='<%# Bind("LastDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SendFlag:
                </td>
                <td>
                    <asp:CheckBox ID="SendFlagCheckBox" runat="server" Checked='<%# Bind("SendFlag") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    MasterFile:
                </td>
                <td>
                    <asp:TextBox ID="MasterFileTextBox" runat="server" Text='<%# Bind("MasterFile") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    MasterFileDescription:
                </td>
                <td>
                    <asp:TextBox ID="MasterFileDescriptionTextBox" runat="server" Text='<%# Bind("MasterFileDescription") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    LastDate:
                </td>
                <td>
                    <asp:TextBox ID="LastDateTextBox" runat="server" Text='<%# Bind("LastDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SendFlag:
                </td>
                <td>
                    <asp:CheckBox ID="SendFlagCheckBox" runat="server" Checked='<%# Bind("SendFlag") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    MasterFile:
                </td>
                <td>
                    <asp:Label ID="MasterFileLabel" runat="server" Text='<%# Bind("MasterFile") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MasterFileDescription:
                </td>
                <td>
                    <asp:Label ID="MasterFileDescriptionLabel" runat="server" Text='<%# Bind("MasterFileDescription") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    LastDate:
                </td>
                <td>
                    <asp:Label ID="LastDateLabel" runat="server" Text='<%# Bind("LastDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SendFlag:
                </td>
                <td>
                    <asp:Label ID="SendFlagLabel" runat="server" Text='<%# Bind("SendFlag") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/InterfaceMasterFileGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceInterfaceMasterFile" runat="server" TypeName="StaticInfoInterfaceMasterFile"
    SelectMethod="GetInterfaceMasterFile"
    DeleteMethod="DeleteInterfaceMasterFile"
    UpdateMethod="UpdateInterfaceMasterFile"
    InsertMethod="InsertInterfaceMasterFile">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InterfaceMasterFileId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MasterFileId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MasterFile" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="MasterFileDescription" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="LastDate" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="SendFlag" Type="Boolean" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="MasterFileId" QueryStringField="MasterFileId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MasterFileId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MasterFile" Type="String" />
        <asp:Parameter Name="MasterFileDescription" Type="String" />
        <asp:Parameter Name="LastDate" Type="DateTime" />
        <asp:Parameter Name="SendFlag" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
