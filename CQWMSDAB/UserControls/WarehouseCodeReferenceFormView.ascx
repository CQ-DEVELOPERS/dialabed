<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WarehouseCodeReferenceFormView.ascx.cs" Inherits="UserControls_WarehouseCodeReferenceFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>WarehouseCodeReference</h3>
<asp:FormView ID="FormViewWarehouseCodeReference" runat="server" DataKeyNames="WarehouseCodeReferenceId" DataSourceID="ObjectDataSourceWarehouseCodeReference" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    WarehouseCode:
                </td>
                <td>
                    <asp:TextBox ID="WarehouseCodeTextBox" runat="server" Text='<%# Bind("WarehouseCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DownloadType:
                </td>
                <td>
                    <asp:TextBox ID="DownloadTypeTextBox" runat="server" Text='<%# Bind("DownloadType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
 
            <tr>
                <td>
                    InboundDocumentTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocumentTypeId" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocumentTypeId"
                        DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId" SelectedValue='<%# Bind("InboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocumentTypeId" runat="server" TypeName="StaticInfoInboundDocumentType"
                        SelectMethod="ParameterInboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundDocumentTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocumentTypeId" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocumentTypeId"
                        DataTextField="OutboundDocumentType" DataValueField="OutboundDocumentTypeId" SelectedValue='<%# Bind("OutboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocumentTypeId" runat="server" TypeName="StaticInfoOutboundDocumentType"
                        SelectMethod="ParameterOutboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ToWarehouseId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListToWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLToWarehouseId"
                        DataTextField="Warehouse" DataValueField="ToWarehouseId" SelectedValue='<%# Bind("WarehouseId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLToWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ToWarehouseCode:
                </td>
                <td>
                    <asp:TextBox ID="ToWarehouseCodeTextBox" runat="server" Text='<%# Bind("ToWarehouseCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    WarehouseCode:
                </td>
                <td>
                    <asp:TextBox ID="WarehouseCodeTextBox" runat="server" Text='<%# Bind("WarehouseCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DownloadType:
                </td>
                <td>
                    <asp:TextBox ID="DownloadTypeTextBox" runat="server" Text='<%# Bind("DownloadType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
 
            <tr>
                <td>
                    InboundDocumentTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocumentTypeId" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocumentTypeId"
                        DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId" SelectedValue='<%# Bind("InboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocumentTypeId" runat="server" TypeName="StaticInfoInboundDocumentType"
                        SelectMethod="ParameterInboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundDocumentTypeId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocumentTypeId" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocumentTypeId"
                        DataTextField="OutboundDocumentType" DataValueField="OutboundDocumentTypeId" SelectedValue='<%# Bind("OutboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocumentTypeId" runat="server" TypeName="StaticInfoOutboundDocumentType"
                        SelectMethod="ParameterOutboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ToWarehouseId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListToWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLToWarehouseId"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("ToWarehouseId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLToWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ToWarehouseCode:
                </td>
                <td>
                    <asp:TextBox ID="ToWarehouseCodeTextBox" runat="server" Text='<%# Bind("ToWarehouseCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    WarehouseCode:
                </td>
                <td>
                    <asp:Label ID="WarehouseCodeLabel" runat="server" Text='<%# Bind("WarehouseCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DownloadType:
                </td>
                <td>
                    <asp:Label ID="DownloadTypeLabel" runat="server" Text='<%# Bind("DownloadType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLWarehouseId"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    InboundDocumentType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocumentTypeId" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocumentTypeId"
                        DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId" SelectedValue='<%# Bind("InboundDocumentTypeId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocumentTypeId" runat="server" TypeName="StaticInfoInboundDocumentType"
                        SelectMethod="ParameterInboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundDocumentType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocumentTypeId" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocumentTypeId"
                        DataTextField="OutboundDocumentType" DataValueField="OutboundDocumentTypeId" SelectedValue='<%# Bind("OutboundDocumentTypeId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocumentTypeId" runat="server" TypeName="StaticInfoOutboundDocumentType"
                        SelectMethod="ParameterOutboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListToWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLToWarehouseId"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("ToWarehouseId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLToWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ToWarehouseCode:
                </td>
                <td>
                    <asp:Label ID="ToWarehouseCodeLabel" runat="server" Text='<%# Bind("ToWarehouseCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/WarehouseCodeReferenceGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceWarehouseCodeReference" runat="server" TypeName="StaticInfoWarehouseCodeReference"
    SelectMethod="GetWarehouseCodeReference"
    DeleteMethod="DeleteWarehouseCodeReference"
    UpdateMethod="UpdateWarehouseCodeReference"
    InsertMethod="InsertWarehouseCodeReference">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="WarehouseCodeReferenceId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="WarehouseCodeReferenceId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="WarehouseCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DownloadType" Type="String" DefaultValue="-1" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ToWarehouseId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ToWarehouseCode" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="WarehouseCodeReferenceId" QueryStringField="WarehouseCodeReferenceId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="WarehouseCodeReferenceId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="WarehouseCode" Type="String" />
        <asp:Parameter Name="DownloadType" Type="String" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ToWarehouseId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ToWarehouseCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
