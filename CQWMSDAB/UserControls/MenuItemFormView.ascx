<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuItemFormView.ascx.cs" Inherits="UserControls_MenuItemFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>MenuItem</h3>
<asp:FormView ID="FormViewMenuItem" runat="server" DataKeyNames="MenuItemId" DataSourceID="ObjectDataSourceMenuItem" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    MenuId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMenuId" runat="server" DataSourceID="ObjectDataSourceDDLMenuId"
                        DataTextField="Menu" DataValueField="MenuId" SelectedValue='<%# Bind("MenuId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuId" runat="server" TypeName="StaticInfoMenu"
                        SelectMethod="ParameterMenu">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListMenuId"
                        ErrorMessage="* Please enter MenuId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:TextBox ID="MenuItemTextBox" runat="server" Text='<%# Bind("MenuItem") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ToolTip:
                </td>
                <td>
                    <asp:TextBox ID="ToolTipTextBox" runat="server" Text='<%# Bind("ToolTip") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    NavigateTo:
                </td>
                <td>
                    <asp:TextBox ID="NavigateToTextBox" runat="server" Text='<%# Bind("NavigateTo") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ParentMenuItemId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListParentMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLParentMenuItemId"
                        DataTextField="MenuItem" DataValueField="ParentMenuItemId" SelectedValue='<%# Bind("MenuItemId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLParentMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                        SelectMethod="ParameterMenuItem">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    MenuId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMenuId" runat="server" DataSourceID="ObjectDataSourceDDLMenuId"
                        DataTextField="Menu" DataValueField="MenuId" SelectedValue='<%# Bind("MenuId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuId" runat="server" TypeName="StaticInfoMenu"
                        SelectMethod="ParameterMenu">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListMenuId"
                        ErrorMessage="* Please enter MenuId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:TextBox ID="MenuItemTextBox" runat="server" Text='<%# Bind("MenuItem") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ToolTip:
                </td>
                <td>
                    <asp:TextBox ID="ToolTipTextBox" runat="server" Text='<%# Bind("ToolTip") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    NavigateTo:
                </td>
                <td>
                    <asp:TextBox ID="NavigateToTextBox" runat="server" Text='<%# Bind("NavigateTo") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ParentMenuItemId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListParentMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLParentMenuItemId"
                        DataTextField="MenuItem" DataValueField="MenuItemId" SelectedValue='<%# Bind("ParentMenuItemId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLParentMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                        SelectMethod="ParameterMenuItem">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Menu:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMenuId" runat="server" DataSourceID="ObjectDataSourceDDLMenuId"
                        DataTextField="Menu" DataValueField="MenuId" SelectedValue='<%# Bind("MenuId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuId" runat="server" TypeName="StaticInfoMenu"
                        SelectMethod="ParameterMenu">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:Label ID="MenuItemLabel" runat="server" Text='<%# Bind("MenuItem") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ToolTip:
                </td>
                <td>
                    <asp:Label ID="ToolTipLabel" runat="server" Text='<%# Bind("ToolTip") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    NavigateTo:
                </td>
                <td>
                    <asp:Label ID="NavigateToLabel" runat="server" Text='<%# Bind("NavigateTo") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListParentMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLParentMenuItemId"
                        DataTextField="MenuItem" DataValueField="MenuItemId" SelectedValue='<%# Bind("ParentMenuItemId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLParentMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                        SelectMethod="ParameterMenuItem">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:Label ID="OrderByLabel" runat="server" Text='<%# Bind("OrderBy") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/MenuItemGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceMenuItem" runat="server" TypeName="StaticInfoMenuItem"
    SelectMethod="GetMenuItem"
    DeleteMethod="DeleteMenuItem"
    UpdateMethod="UpdateMenuItem"
    InsertMethod="InsertMenuItem">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItem" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ToolTip" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="NavigateTo" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ParentMenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int16" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="MenuItemId" QueryStringField="MenuItemId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItem" Type="String" />
        <asp:Parameter Name="ToolTip" Type="String" />
        <asp:Parameter Name="NavigateTo" Type="String" />
        <asp:Parameter Name="ParentMenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int16" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
