<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceDocumentDescFormView.ascx.cs" Inherits="UserControls_InterfaceDocumentDescFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>InterfaceDocumentDesc</h3>
<asp:FormView ID="FormViewInterfaceDocumentDesc" runat="server" DataKeyNames="InterfaceDocumentDescId" DataSourceID="ObjectDataSourceInterfaceDocumentDesc" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Doc:
                </td>
                <td>
                    <asp:TextBox ID="DocTextBox" runat="server" Text='<%# Bind("Doc") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DocDesc:
                </td>
                <td>
                    <asp:TextBox ID="DocDescTextBox" runat="server" Text='<%# Bind("DocDesc") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DocDescTextBox"
                        ErrorMessage="* Please enter DocDesc"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Doc:
                </td>
                <td>
                    <asp:TextBox ID="DocTextBox" runat="server" Text='<%# Bind("Doc") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DocDesc:
                </td>
                <td>
                    <asp:TextBox ID="DocDescTextBox" runat="server" Text='<%# Bind("DocDesc") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DocDescTextBox"
                        ErrorMessage="* Please enter DocDesc"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Doc:
                </td>
                <td>
                    <asp:Label ID="DocLabel" runat="server" Text='<%# Bind("Doc") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DocDesc:
                </td>
                <td>
                    <asp:Label ID="DocDescLabel" runat="server" Text='<%# Bind("DocDesc") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/InterfaceDocumentDescGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceInterfaceDocumentDesc" runat="server" TypeName="StaticInfoInterfaceDocumentDesc"
    SelectMethod="GetInterfaceDocumentDesc"
    DeleteMethod="DeleteInterfaceDocumentDesc"
    UpdateMethod="UpdateInterfaceDocumentDesc"
    InsertMethod="InsertInterfaceDocumentDesc">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InterfaceDocumentDescId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="DocId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Doc" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DocDesc" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="DocId" QueryStringField="DocId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="DocId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Doc" Type="String" />
        <asp:Parameter Name="DocDesc" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
