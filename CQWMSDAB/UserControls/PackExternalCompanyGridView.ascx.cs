using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_PackExternalCompanyGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridPackExternalCompany.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridPackExternalCompany_SelectedIndexChanged
    protected void RadGridPackExternalCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridPackExternalCompany.SelectedItems)
            {
                Session["PackExternalCompanyId"] = item.GetDataKeyValue("PackExternalCompanyId");
            }
        }
        catch { }
    }
    #endregion "RadGridPackExternalCompany_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
 
        if (Session["ExternalCompanyId"] != null)
          DropDownListExternalCompanyId.SelectedValue = Session["ExternalCompanyId"].ToString();
        if (Session["StorageUnitId"] != null)
          DropDownListStorageUnitId.SelectedValue = Session["StorageUnitId"].ToString();
        if (Session["FromPackId"] != null)
          DropDownListFromPackId.SelectedValue = Session["FromPackId"].ToString();
        if (Session["ToPackId"] != null)
          DropDownListToPackId.SelectedValue = Session["ToPackId"].ToString();
    
        RadGridPackExternalCompany.DataBind();
    }
    #endregion BindData
    
}
