using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_InterfaceMappingColumnGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridInterfaceMappingColumn.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridInterfaceMappingColumn_SelectedIndexChanged
    protected void RadGridInterfaceMappingColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridInterfaceMappingColumn.SelectedItems)
            {
                Session["InterfaceMappingColumnId"] = item.GetDataKeyValue("InterfaceMappingColumnId");
            }
        }
        catch { }
    }
    #endregion "RadGridInterfaceMappingColumn_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["InterfaceDocumentTypeId"] != null)
          DropDownListInterfaceDocumentTypeId.SelectedValue = Session["InterfaceDocumentTypeId"].ToString();
        if (Session["InterfaceFieldId"] != null)
          DropDownListInterfaceFieldId.SelectedValue = Session["InterfaceFieldId"].ToString();
        if (Session["InterfaceMappingFileId"] != null)
          DropDownListInterfaceMappingFileId.SelectedValue = Session["InterfaceMappingFileId"].ToString();
    
        RadGridInterfaceMappingColumn.DataBind();
    }
    #endregion BindData
    
}
