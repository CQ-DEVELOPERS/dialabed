<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceMasterFileGridView.ascx.cs" Inherits="UserControls_InterfaceMasterFileGridView" %>
 
<table style="background-color:lightgray; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
        <td>
            <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
        </td>
    </tr>
</table>
 
<telerik:RadGrid ID="RadGridInterfaceMasterFile" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true"
    AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="30" DataSourceID="ObjectDataSourceInterfaceMasterFile" OnSelectedIndexChanged="RadGridInterfaceMasterFile_SelectedIndexChanged" Skin="Metro">
<PagerStyle Mode="NumericPages"></PagerStyle>
<MasterTableView DataKeyNames="
MasterFileId
" DataSourceID="ObjectDataSourceInterfaceMasterFile"
    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
    InsertItemPageIndexAction="ShowItemOnFirstPage">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="MasterFileId" HeaderText="MasterFileId" SortExpression="MasterFileId" UniqueName="MasterFileId"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MasterFile" HeaderText="MasterFile" SortExpression="MasterFile" UniqueName="MasterFile"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MasterFileDescription" HeaderText="MasterFileDescription" SortExpression="MasterFileDescription" UniqueName="MasterFileDescription"></telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="LastDate" HeaderText="LastDate"></telerik:GridDateTimeColumn>
                        <telerik:GridCheckBoxColumn DataField="SendFlag" HeaderText="SendFlag" SortExpression="SendFlag" UniqueName="SendFlag"></telerik:GridCheckBoxColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
       </ClientSettings>
    <GroupingSettings ShowUnGroupButton="True" />
</telerik:RadGrid>
 
<asp:ObjectDataSource ID="ObjectDataSourceInterfaceMasterFile" runat="server" TypeName="StaticInfoInterfaceMasterFile"
    DeleteMethod="DeleteInterfaceMasterFile"
    InsertMethod="InsertInterfaceMasterFile"
    SelectMethod="SearchInterfaceMasterFile"
    UpdateMethod="UpdateInterfaceMasterFile">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MasterFileId" Type="Int32" DefaultValue="-1" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MasterFileId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MasterFileId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MasterFile" Type="String" />
        <asp:Parameter Name="MasterFileDescription" Type="String" />
        <asp:Parameter Name="LastDate" Type="DateTime" />
        <asp:Parameter Name="SendFlag" Type="Boolean" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="MasterFileId" Type="Int32" />
        <asp:Parameter Name="MasterFile" Type="String" />
        <asp:Parameter Name="MasterFileDescription" Type="String" />
        <asp:Parameter Name="LastDate" Type="DateTime" />
        <asp:Parameter Name="SendFlag" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
