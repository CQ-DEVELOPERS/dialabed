<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequestTypeFormView.ascx.cs" Inherits="UserControls_RequestTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>RequestType</h3>
<asp:FormView ID="FormViewRequestType" runat="server" DataKeyNames="RequestTypeId" DataSourceID="ObjectDataSourceRequestType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    RequestTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="RequestTypeCodeTextBox" runat="server" Text='<%# Bind("RequestTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RequestType:
                </td>
                <td>
                    <asp:TextBox ID="RequestTypeTextBox" runat="server" Text='<%# Bind("RequestType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    RequestTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="RequestTypeCodeTextBox" runat="server" Text='<%# Bind("RequestTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RequestType:
                </td>
                <td>
                    <asp:TextBox ID="RequestTypeTextBox" runat="server" Text='<%# Bind("RequestType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    RequestTypeCode:
                </td>
                <td>
                    <asp:Label ID="RequestTypeCodeLabel" runat="server" Text='<%# Bind("RequestTypeCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RequestType:
                </td>
                <td>
                    <asp:Label ID="RequestTypeLabel" runat="server" Text='<%# Bind("RequestType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/RequestTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceRequestType" runat="server" TypeName="StaticInfoRequestType"
    SelectMethod="GetRequestType"
    DeleteMethod="DeleteRequestType"
    UpdateMethod="UpdateRequestType"
    InsertMethod="InsertRequestType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RequestTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RequestTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RequestTypeCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="RequestType" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="RequestTypeId" QueryStringField="RequestTypeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RequestTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RequestTypeCode" Type="String" />
        <asp:Parameter Name="RequestType" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
