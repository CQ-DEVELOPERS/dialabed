<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactListGridView.ascx.cs" Inherits="UserControls_ContactListGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowContactList()
{
   window.open("ContactListFormView.aspx", "_blank",
      "height=640px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelExternalCompanyId" runat="server" Text="ExternalCompany:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                SelectMethod="ParameterExternalCompany">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <asp:Label ID="LabelOperatorId" runat="server" Text="Operator:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListOperatorId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorId"
                DataTextField="Operator" DataValueField="OperatorId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorId" runat="server" TypeName="StaticInfoOperator"
                SelectMethod="ParameterOperator">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"/>
        <td>
            <asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowContactList();"/>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Visible="False"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewContactList" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
ContactListId
"
                DataSourceID="ObjectDataSourceContactList" OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewContactList_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="False" ShowEditButton="True" />
        <asp:BoundField DataField="ContactListId" HeaderText="ContactListId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="ContactListId" />
            <asp:TemplateField HeaderText="ExternalCompanyId" meta:resourcekey="ExternalCompanyId" SortExpression="ExternalCompanyId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                        SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkExternalCompanyId" runat="server" Text='<%# Bind("ExternalCompany") %>' NavigateUrl="~/Administration/ExternalCompanyGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
            <asp:TemplateField HeaderText="OperatorId" meta:resourcekey="OperatorId" SortExpression="OperatorId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListOperatorId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorId"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorId" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkOperatorId" runat="server" Text='<%# Bind("Operator") %>' NavigateUrl="~/Administration/OperatorGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ContactPerson" HeaderText="ContactPerson" SortExpression="ContactPerson" />
                        <asp:BoundField DataField="Telephone" HeaderText="Telephone" SortExpression="Telephone" />
                        <asp:BoundField DataField="Fax" HeaderText="Fax" SortExpression="Fax" />
                        <asp:BoundField DataField="EMail" HeaderText="EMail" SortExpression="EMail" />
                        <asp:BoundField DataField="Alias" HeaderText="Alias" SortExpression="Alias" />
                        <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                    <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:ObjectDataSource ID="ObjectDataSourceContactList" runat="server" TypeName="StaticInfoContactList"
    DeleteMethod="DeleteContactList"
    InsertMethod="InsertContactList"
    SelectMethod="SearchContactList"
    UpdateMethod="UpdateContactList">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContactListId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="DropDownListExternalCompanyId" DefaultValue="-1" Name="ExternalCompanyId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListOperatorId" DefaultValue="-1" Name="OperatorId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContactListId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContactListId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OperatorId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ContactPerson" Type="String" />
        <asp:Parameter Name="Telephone" Type="String" />
        <asp:Parameter Name="Fax" Type="String" />
        <asp:Parameter Name="EMail" Type="String" />
        <asp:Parameter Name="Alias" Type="String" />
        <asp:Parameter Name="Type" Type="String" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="ContactListId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OperatorId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ContactPerson" Type="String" />
        <asp:Parameter Name="Telephone" Type="String" />
        <asp:Parameter Name="Fax" Type="String" />
        <asp:Parameter Name="EMail" Type="String" />
        <asp:Parameter Name="Alias" Type="String" />
        <asp:Parameter Name="Type" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
