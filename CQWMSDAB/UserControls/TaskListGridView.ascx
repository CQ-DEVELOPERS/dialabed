<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TaskListGridView.ascx.cs" Inherits="UserControls_TaskListGridView" %>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridTaskList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridTaskList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridTaskList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                DataTextField="Priority" DataValueField="PriorityId" Label="PriorityId" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                SelectMethod="ParameterPriority">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListProjectId" runat="server" DataSourceID="ObjectDataSourceDDLProjectId"
                DataTextField="Project" DataValueField="ProjectId" Label="ProjectId" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLProjectId" runat="server" TypeName="StaticInfoProject"
                SelectMethod="ParameterProject">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListRequestTypeId" runat="server" DataSourceID="ObjectDataSourceDDLRequestTypeId"
                DataTextField="RequestType" DataValueField="RequestTypeId" Label="RequestTypeId" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLRequestTypeId" runat="server" TypeName="StaticInfoRequestType"
                SelectMethod="ParameterRequestType">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListBillableId" runat="server" DataSourceID="ObjectDataSourceDDLBillableId"
                DataTextField="Billable" DataValueField="BillableId" Label="BillableId" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLBillableId" runat="server" TypeName="StaticInfoBillable"
                SelectMethod="ParameterBillable">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadTextBox ID="TextBoxTaskList" runat="server" Label="TaskList" Width="300px"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListRaisedBy" runat="server" DataSourceID="ObjectDataSourceDDLRaisedBy"
                DataTextField="Operator" DataValueField="OperatorId" Label="RaisedBy" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLRaisedBy" runat="server" TypeName="StaticInfoOperator"
                SelectMethod="ParameterOperator">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListApprovedBy" runat="server" DataSourceID="ObjectDataSourceDDLApprovedBy"
                DataTextField="Operator" DataValueField="OperatorId" Label="ApprovedBy" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLApprovedBy" runat="server" TypeName="StaticInfoOperator"
                SelectMethod="ParameterOperator">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListAllocatedTo" runat="server" DataSourceID="ObjectDataSourceDDLAllocatedTo"
                DataTextField="Operator" DataValueField="OperatorId" Label="AllocatedTo" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLAllocatedTo" runat="server" TypeName="StaticInfoOperator"
                SelectMethod="ParameterOperator">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListTester" runat="server" DataSourceID="ObjectDataSourceDDLTester"
                DataTextField="Operator" DataValueField="OperatorId" Label="Tester" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLTester" runat="server" TypeName="StaticInfoOperator"
                SelectMethod="ParameterOperator">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListCycleId" runat="server" DataSourceID="ObjectDataSourceDDLCycleId"
                DataTextField="Cycle" DataValueField="CycleId" Label="CycleId" Width="250px">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLCycleId" runat="server" TypeName="StaticInfoCycle"
                SelectMethod="ParameterCycle">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
        </td>
    </tr>
</table>
 
<telerik:RadGrid ID="RadGridTaskList" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true"
    AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="30" DataSourceID="ObjectDataSourceTaskList">
<PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
<MasterTableView DataKeyNames="
TaskListId
" DataSourceID="ObjectDataSourceTaskList"
    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
    InsertItemPageIndexAction="ShowItemOnFirstPage">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="TaskListId" HeaderText="TaskListId" SortExpression="TaskListId" UniqueName="TaskListId"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListPriorityId" ListTextField="Priority" ListValueField="PriorityId" DataSourceID="ObjectDataSourcePriorityId" DataField="PriorityId" HeaderText="PriorityId">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListProjectId" ListTextField="Project" ListValueField="ProjectId" DataSourceID="ObjectDataSourceProjectId" DataField="ProjectId" HeaderText="ProjectId">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListRequestTypeId" ListTextField="RequestType" ListValueField="RequestTypeId" DataSourceID="ObjectDataSourceRequestTypeId" DataField="RequestTypeId" HeaderText="RequestTypeId">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListBillableId" ListTextField="Billable" ListValueField="BillableId" DataSourceID="ObjectDataSourceBillableId" DataField="BillableId" HeaderText="BillableId">
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="TaskList" HeaderText="TaskList" SortExpression="TaskList" UniqueName="TaskList"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Comments" HeaderText="Comments" SortExpression="Comments" UniqueName="Comments"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListRaisedBy" ListTextField="Operator" ListValueField="OperatorId" DataSourceID="ObjectDataSourceOperatorId" DataField="RaisedBy" HeaderText="RaisedBy">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListApprovedBy" ListTextField="Operator" ListValueField="OperatorId" DataSourceID="ObjectDataSourceOperatorId" DataField="ApprovedBy" HeaderText="ApprovedBy">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListAllocatedTo" ListTextField="Operator" ListValueField="OperatorId" DataSourceID="ObjectDataSourceOperatorId" DataField="AllocatedTo" HeaderText="AllocatedTo">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListTester" ListTextField="Operator" ListValueField="OperatorId" DataSourceID="ObjectDataSourceOperatorId" DataField="Tester" HeaderText="Tester">
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="EstimateHours" HeaderText="EstimateHours" SortExpression="EstimateHours" UniqueName="EstimateHours"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ActualHours" HeaderText="ActualHours" SortExpression="ActualHours" UniqueName="ActualHours"></telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="CreateDate" HeaderText="CreateDate"></telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="TargetDate" HeaderText="TargetDate"></telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" SortExpression="Percentage" UniqueName="Percentage"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListCycleId" ListTextField="Cycle" ListValueField="CycleId" DataSourceID="ObjectDataSourceCycleId" DataField="CycleId" HeaderText="CycleId">
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="OrderBy" HeaderText="OrderBy" SortExpression="OrderBy" UniqueName="OrderBy"></telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectDataSourcePriorityId" runat="server" TypeName="StaticInfoPriority"
    SelectMethod="ParameterPriority">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceProjectId" runat="server" TypeName="StaticInfoProject"
    SelectMethod="ParameterProject">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceRequestTypeId" runat="server" TypeName="StaticInfoRequestType"
    SelectMethod="ParameterRequestType">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceBillableId" runat="server" TypeName="StaticInfoBillable"
    SelectMethod="ParameterBillable">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceRaisedBy" runat="server" TypeName="StaticInfoOperator"
    SelectMethod="ParameterOperator">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceApprovedBy" runat="server" TypeName="StaticInfoOperator"
    SelectMethod="ParameterOperator">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceAllocatedTo" runat="server" TypeName="StaticInfoOperator"
    SelectMethod="ParameterOperator">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceTester" runat="server" TypeName="StaticInfoOperator"
    SelectMethod="ParameterOperator">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceCycleId" runat="server" TypeName="StaticInfoCycle"
    SelectMethod="ParameterCycle">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
 
<asp:ObjectDataSource ID="ObjectDataSourceTaskList" runat="server" TypeName="StaticInfoTaskList"
    DeleteMethod="DeleteTaskList"
    InsertMethod="InsertTaskList"
    SelectMethod="SearchTaskList"
    UpdateMethod="UpdateTaskList">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TaskListId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="DropDownListPriorityId" DefaultValue="-1" Name="PriorityId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListProjectId" DefaultValue="-1" Name="ProjectId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListRequestTypeId" DefaultValue="-1" Name="RequestTypeId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListBillableId" DefaultValue="-1" Name="BillableId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="TextBoxTaskList" DefaultValue="%" Name="TaskList" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="DropDownListRaisedBy" DefaultValue="-1" Name="RaisedBy" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListApprovedBy" DefaultValue="-1" Name="ApprovedBy" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListAllocatedTo" DefaultValue="-1" Name="AllocatedTo" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListTester" DefaultValue="-1" Name="Tester" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListCycleId" DefaultValue="-1" Name="CycleId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TaskListId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TaskListId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ProjectId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RequestTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="BillableId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TaskList" Type="String" />
        <asp:Parameter Name="Comments" Type="String" />
        <asp:Parameter Name="RaisedBy" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ApprovedBy" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AllocatedTo" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Tester" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="EstimateHours" Type="Double" />
        <asp:Parameter Name="ActualHours" Type="Double" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="TargetDate" Type="DateTime" />
        <asp:Parameter Name="Percentage" Type="Double" />
        <asp:Parameter Name="CycleId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="TaskListId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ProjectId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="RequestTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="BillableId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TaskList" Type="String" />
        <asp:Parameter Name="Comments" Type="String" />
        <asp:Parameter Name="RaisedBy" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ApprovedBy" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AllocatedTo" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Tester" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="EstimateHours" Type="Double" />
        <asp:Parameter Name="ActualHours" Type="Double" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="TargetDate" Type="DateTime" />
        <asp:Parameter Name="Percentage" Type="Double" />
        <asp:Parameter Name="CycleId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
