<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutboundShipmentIssueFormView.ascx.cs" Inherits="UserControls_OutboundShipmentIssueFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>OutboundShipmentIssue</h3>
<asp:FormView ID="FormViewOutboundShipmentIssue" runat="server" DataKeyNames="OutboundShipmentIssueId" DataSourceID="ObjectDataSourceOutboundShipmentIssue" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:TextBox ID="DropSequenceTextBox" runat="server" Text='<%# Bind("DropSequence") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundShipment:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundShipment" runat="server" DataSourceID="ObjectDataSourceDDLOutboundShipment"
                        DataTextField="OutboundShipment" DataValueField="OutboundShipmentId" SelectedValue='<%# Bind("OutboundShipmentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundShipment" runat="server"
                        TypeName="StaticInfoOutboundShipment" SelectMethod="ParameterOutboundShipment">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListOutboundShipment" ErrorMessage="* Please enter OutboundShipmentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Issue:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssue" runat="server" DataSourceID="ObjectDataSourceDDLIssue"
                        DataTextField="Issue" DataValueField="IssueId" SelectedValue='<%# Bind("IssueId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssue" runat="server"
                        TypeName="StaticInfoIssue" SelectMethod="ParameterIssue">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListIssue" ErrorMessage="* Please enter IssueId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:TextBox ID="DropSequenceTextBox" runat="server" Text='<%# Bind("DropSequence") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundShipment:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundShipment" runat="server" DataSourceID="ObjectDataSourceDDLOutboundShipment"
                        DataTextField="OutboundShipment" DataValueField="OutboundShipmentId" SelectedValue='<%# Bind("OutboundShipmentId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundShipment" runat="server"
                        TypeName="StaticInfoOutboundShipment" SelectMethod="ParameterOutboundShipment">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Issue:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListIssue" runat="server" DataSourceID="ObjectDataSourceDDLIssue"
                        DataTextField="Issue" DataValueField="IssueId" SelectedValue='<%# Bind("IssueId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLIssue" runat="server"
                        TypeName="StaticInfoIssue" SelectMethod="ParameterIssue">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DropSequence:
                </td>
                <td>
                    <asp:Label ID="DropSequenceLabel" runat="server" Text='<%# Bind("DropSequence") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/OutboundShipmentIssueGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceOutboundShipmentIssue" runat="server" TypeName="StaticInfoOutboundShipmentIssue"
    SelectMethod="GetOutboundShipmentIssue"
    DeleteMethod="DeleteOutboundShipmentIssue"
    UpdateMethod="UpdateOutboundShipmentIssue"
    InsertMethod="InsertOutboundShipmentIssue">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundShipmentIssueId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundShipmentId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="DropSequence" Type="Int16" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="OutboundShipmentId" QueryStringField="OutboundShipmentId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="IssueId" QueryStringField="IssueId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundShipmentId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="DropSequence" Type="Int16" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
