﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderUC.ascx.cs" Inherits="UserControls_Common_HeaderUC" %>

<script runat="server">
          
    public String MsgText
    {
        get
        {
            return Msg.Text;
        }
        set
        {
            Msg.Text = value;
        }
    }
    public String ErrorText
    {
        get
        {
            return ErrorMesg.Text;
        }
        set
        {
            ErrorMesg.Text = value;
        }
    } 
</script>
<table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
    <tr>
        <td colspan="3">
            <table cellspacing="0" cellpadding="0" style="width: 100%; height: 100%; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;">
                <tr>
                    <td style="width: 1%; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;">
                        <telerik:RadMenu ID="RadMenu1" runat="server" Flow="Horizontal" EnableEmbeddedSkins="false" Skin="Metro">
                            <Items>
                                <telerik:RadMenuItem Text="<%$ Resources:Default, Home %>" NavigateUrl="~/Default.aspx"></telerik:RadMenuItem>
                            </Items>
                        </telerik:RadMenu>
                    </td>
                    <td style="width: 98%; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;">
                        <telerik:RadMenu ID="RadMenu2" runat="server" DataSourceID="xmlDataSource" Flow="Horizontal" Skin="Black" Width="100%" Style="z-index: 1;">
                            <DataBindings>
                                <telerik:RadMenuItemBinding DataMember="MenuItem" NavigateUrlField="NavigateUrl" TextField="MenuItemText" ToolTipField="ToolTip" />
                            </DataBindings>
                        </telerik:RadMenu>
                        <asp:XmlDataSource ID="xmlDataSource" runat="server" TransformFile="MenuItem.xsl" XPath="MenuItems/MenuItem"  />
                    </td>
                    <td style="width: 1%; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;">
                        <telerik:RadMenu ID="RadMenu3" runat="server" Flow="Horizontal" EnableEmbeddedSkins="false" Skin="Black">
                            <Items>
                                <telerik:RadMenuItem Text="<%$ Resources:Default, Logout %>" NavigateUrl="~/Security/Logout.aspx"></telerik:RadMenuItem>
                            </Items>
                        </telerik:RadMenu>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanelMessagePanel" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="MessagePanel" runat="server">
                                    <asp:Label ID="Msg" runat="server" SkinID="Message"></asp:Label>
                                    <asp:Label ID="ErrorMesg" runat="server" SkinID="Error"></asp:Label>
                                    <asp:Label ID="GenMesg" runat="server" SkinID="Message"></asp:Label>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
