﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_Common_HeaderUC : System.Web.UI.UserControl
{
    #region MsgText
    public String MsgText
    {
        get
        {
            return Msg.Text;
        }
        set
        {
            Msg.Text = value;
        }
    }
    #endregion MsgText

    #region ErrorText
    public String ErrorText
    {
        get
        {
            return ErrorMesg.Text;
        }
        set
        {
            ErrorMesg.Text = value;
        }
    }
    #endregion ErrorText

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int operatorId;

            if (Session["MenuId"] == null)
                Session["MenuId"] = 1;

            if (Session["OperatorId"] == null)
                operatorId = -1;
            else
                operatorId = (int)Session["OperatorId"];

            MenuItemsDynamic ds = new MenuItemsDynamic();
            xmlDataSource.Data = ds.GetMenuItems(Session["ConnectionStringName"].ToString(), (int)Session["MenuId"], operatorId, "Desktop").GetXml();
            xmlDataSource.EnableCaching = false;
            xmlDataSource.DataBind();

        }

    }
}