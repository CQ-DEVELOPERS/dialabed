﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_Common_FooterUC : System.Web.UI.UserControl
{
    public event LoginCancelEventHandler LoggingOut;
    public event EventHandler LoggedOut;

    public String DatabaseLabel
    {
        get { return LabelDatabase.Text; }
        set { LabelDatabase.Text = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Version"] != null)
                lblVersion.Text = "Version: " + Session["Version"].ToString();

            //lblVerison.Text = CqInfo.VerisonNumber;
            //LabelCopyRight.Text = CqInfo.LegalCopyright;

            LabelCopyRight.Text = "Copyright © " + DateTime.Now.Year + " CQuential Solutions (Pty) Ltd.";
        }
    }

    protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        // Perform any post-logout processing, such as setting the
        // user's last logout time or clearing a per-user cache of 
        // objects here.
        try
        {
            LogonCredentials lc = new LogonCredentials();

            lc.UserLoggedOut(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], "Desktop");
        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;

            FormsAuthentication.SignOut();
            Session.Abandon();
            FormsAuthentication.RedirectToLoginPage();
        }
        if (LoggingOut != null)
            LoggingOut(sender, e);
    }

    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {
        // Perform any post-logout processing, such as setting the
        // user's last logout time or clearing a per-user cache of 
        // objects here.
        try
        {
            Session.Abandon();
        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;

            FormsAuthentication.SignOut();
            Session.Abandon();
            FormsAuthentication.RedirectToLoginPage();
        }
        if (LoggedOut != null)
            LoggedOut(sender, e);
    }
}