﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FooterUC.ascx.cs" Inherits="UserControls_Common_FooterUC" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<style>

.footer {
    height: 30px;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 10px; 
    -khtml-border-radius: 10px; 
    background-color: #C0C0C0; 
}
</style>
<div ID="PanelFooter" class="footer" runat="server" >
    <div align="left" style="float: left;">
        &nbsp;&nbsp;&nbsp;<asp:Label ID="LabelCopyRight" runat="server" SkinID="CopyRight"></asp:Label>
    </div>
    <div align="right">
        <asp:LoginName ID="LoginName1" runat="server" />
        <asp:LoginStatus ID="LoginStatus1" runat="server" OnLoggingOut="LoginStatus1_LoggingOut" OnLoggedOut="LoginStatus1_LoggedOut" />
        &nbsp;&nbsp;&nbsp;
        <asp:Label ID="LabelLoggedOnTo" runat="server" SkinID="CopyRight" Text="Database: "></asp:Label>
        <asp:Label ID="LabelDatabase" runat="server" SkinID="CopyRight"></asp:Label>
        &nbsp;<asp:Label ID="lblVersion" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
    </div>
</div>
