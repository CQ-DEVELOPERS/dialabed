<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationExceptionFormView.ascx.cs" Inherits="UserControls_ApplicationExceptionFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>ApplicationException</h3>
<asp:FormView ID="FormViewApplicationException" runat="server" DataKeyNames="ApplicationExceptionId" DataSourceID="ObjectDataSourceApplicationException" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    PriorityLevel:
                </td>
                <td>
                    <asp:TextBox ID="PriorityLevelTextBox" runat="server" Text='<%# Bind("PriorityLevel") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="PriorityLevelTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Module:
                </td>
                <td>
                    <asp:TextBox ID="ModuleTextBox" runat="server" Text='<%# Bind("Module") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Page:
                </td>
                <td>
                    <asp:TextBox ID="PageTextBox" runat="server" Text='<%# Bind("Page") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Method:
                </td>
                <td>
                    <asp:TextBox ID="MethodTextBox" runat="server" Text='<%# Bind("Method") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ErrorMsg:
                </td>
                <td>
                    <asp:TextBox ID="ErrorMsgTextBox" runat="server" Text='<%# Bind("ErrorMsg") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    PriorityLevel:
                </td>
                <td>
                    <asp:TextBox ID="PriorityLevelTextBox" runat="server" Text='<%# Bind("PriorityLevel") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="PriorityLevelTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Module:
                </td>
                <td>
                    <asp:TextBox ID="ModuleTextBox" runat="server" Text='<%# Bind("Module") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Page:
                </td>
                <td>
                    <asp:TextBox ID="PageTextBox" runat="server" Text='<%# Bind("Page") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Method:
                </td>
                <td>
                    <asp:TextBox ID="MethodTextBox" runat="server" Text='<%# Bind("Method") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ErrorMsg:
                </td>
                <td>
                    <asp:TextBox ID="ErrorMsgTextBox" runat="server" Text='<%# Bind("ErrorMsg") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    PriorityLevel:
                </td>
                <td>
                    <asp:Label ID="PriorityLevelLabel" runat="server" Text='<%# Bind("PriorityLevel") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Module:
                </td>
                <td>
                    <asp:Label ID="ModuleLabel" runat="server" Text='<%# Bind("Module") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Page:
                </td>
                <td>
                    <asp:Label ID="PageLabel" runat="server" Text='<%# Bind("Page") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Method:
                </td>
                <td>
                    <asp:Label ID="MethodLabel" runat="server" Text='<%# Bind("Method") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ErrorMsg:
                </td>
                <td>
                    <asp:Label ID="ErrorMsgLabel" runat="server" Text='<%# Bind("ErrorMsg") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:Label ID="CreateDateLabel" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/ApplicationExceptionGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceApplicationException" runat="server" TypeName="StaticInfoApplicationException"
    SelectMethod="GetApplicationException"
    DeleteMethod="DeleteApplicationException"
    UpdateMethod="UpdateApplicationException"
    InsertMethod="InsertApplicationException">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ApplicationExceptionId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ApplicationExceptionId" Type="Int32" />
        <asp:Parameter Name="PriorityLevel" Type="Int32" />
        <asp:Parameter Name="Module" Type="String" />
        <asp:Parameter Name="Page" Type="String" />
        <asp:Parameter Name="Method" Type="String" />
        <asp:Parameter Name="ErrorMsg" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ApplicationExceptionId" QueryStringField="ApplicationExceptionId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ApplicationExceptionId" Type="Int32" />
        <asp:Parameter Name="PriorityLevel" Type="Int32" />
        <asp:Parameter Name="Module" Type="String" />
        <asp:Parameter Name="Page" Type="String" />
        <asp:Parameter Name="Method" Type="String" />
        <asp:Parameter Name="ErrorMsg" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
