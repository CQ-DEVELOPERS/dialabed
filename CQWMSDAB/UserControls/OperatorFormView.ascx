<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperatorFormView.ascx.cs" Inherits="UserControls_OperatorFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Operator</h3>
<asp:FormView ID="FormViewOperator" runat="server" DataKeyNames="OperatorId" DataSourceID="ObjectDataSourceOperator" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    OperatorGroupId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroupId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroupId"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroupId" runat="server" TypeName="StaticInfoOperatorGroup"
                        SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOperatorGroupId"
                        ErrorMessage="* Please enter OperatorGroupId"></asp:RequiredFieldValidator>
                </td>
            </tr>
 
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:TextBox ID="OperatorTextBox" runat="server" Text='<%# Bind("Operator") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="OperatorTextBox"
                        ErrorMessage="* Please enter Operator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorCode:
                </td>
                <td>
                    <asp:TextBox ID="OperatorCodeTextBox" runat="server" Text='<%# Bind("OperatorCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="OperatorCodeTextBox"
                        ErrorMessage="* Please enter OperatorCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="PasswordTextBox"
                        ErrorMessage="* Please enter Password"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    NewPassword:
                </td>
                <td>
                    <asp:TextBox ID="NewPasswordTextBox" runat="server" Text='<%# Bind("NewPassword") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActiveIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="ActiveIndicatorCheckBox" runat="server" Checked='<%# Bind("ActiveIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter ActiveIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExpiryDate:
                </td>
                <td>
                    <asp:TextBox ID="ExpiryDateTextBox" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ExpiryDateTextBox"
                        ErrorMessage="* Please enter ExpiryDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    LastInstruction:
                </td>
                <td>
                    <asp:TextBox ID="LastInstructionTextBox" runat="server" Text='<%# Bind("LastInstruction") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Printer:
                </td>
                <td>
                    <asp:TextBox ID="PrinterTextBox" runat="server" Text='<%# Bind("Printer") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Port:
                </td>
                <td>
                    <asp:TextBox ID="PortTextBox" runat="server" Text='<%# Bind("Port") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    IPAddress:
                </td>
                <td>
                    <asp:TextBox ID="IPAddressTextBox" runat="server" Text='<%# Bind("IPAddress") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CultureId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCultureId" runat="server" DataSourceID="ObjectDataSourceDDLCultureId"
                        DataTextField="Culture" DataValueField="CultureId" SelectedValue='<%# Bind("CultureId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCultureId" runat="server" TypeName="StaticInfoCulture"
                        SelectMethod="ParameterCulture">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    LoginTime:
                </td>
                <td>
                    <asp:TextBox ID="LoginTimeTextBox" runat="server" Text='<%# Bind("LoginTime") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    LogoutTime:
                </td>
                <td>
                    <asp:TextBox ID="LogoutTimeTextBox" runat="server" Text='<%# Bind("LogoutTime") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    LastActivity:
                </td>
                <td>
                    <asp:TextBox ID="LastActivityTextBox" runat="server" Text='<%# Bind("LastActivity") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SiteType:
                </td>
                <td>
                    <asp:TextBox ID="SiteTypeTextBox" runat="server" Text='<%# Bind("SiteType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Name:
                </td>
                <td>
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Surname:
                </td>
                <td>
                    <asp:TextBox ID="SurnameTextBox" runat="server" Text='<%# Bind("Surname") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    OperatorGroupId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroupId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroupId"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroupId" runat="server" TypeName="StaticInfoOperatorGroup"
                        SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOperatorGroupId"
                        ErrorMessage="* Please enter OperatorGroupId"></asp:RequiredFieldValidator>
                </td>
            </tr>
 
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:TextBox ID="OperatorTextBox" runat="server" Text='<%# Bind("Operator") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="OperatorTextBox"
                        ErrorMessage="* Please enter Operator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorCode:
                </td>
                <td>
                    <asp:TextBox ID="OperatorCodeTextBox" runat="server" Text='<%# Bind("OperatorCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="OperatorCodeTextBox"
                        ErrorMessage="* Please enter OperatorCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>' TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="PasswordTextBox"
                        ErrorMessage="* Please enter Password"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    NewPassword:
                </td>
                <td>
                    <asp:TextBox ID="NewPasswordTextBox" runat="server" Text='<%# Bind("NewPassword") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ActiveIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="ActiveIndicatorCheckBox" runat="server" Checked='<%# Bind("ActiveIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter ActiveIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExpiryDate:
                </td>
                <td>
                    <asp:TextBox ID="ExpiryDateTextBox" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ExpiryDateTextBox"
                        ErrorMessage="* Please enter ExpiryDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    LastInstruction:
                </td>
                <td>
                    <asp:TextBox ID="LastInstructionTextBox" runat="server" Text='<%# Bind("LastInstruction") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Printer:
                </td>
                <td>
                    <asp:TextBox ID="PrinterTextBox" runat="server" Text='<%# Bind("Printer") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Port:
                </td>
                <td>
                    <asp:TextBox ID="PortTextBox" runat="server" Text='<%# Bind("Port") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    IPAddress:
                </td>
                <td>
                    <asp:TextBox ID="IPAddressTextBox" runat="server" Text='<%# Bind("IPAddress") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CultureId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCultureId" runat="server" DataSourceID="ObjectDataSourceDDLCultureId"
                        DataTextField="Culture" DataValueField="CultureId" SelectedValue='<%# Bind("CultureId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCultureId" runat="server" TypeName="StaticInfoCulture"
                        SelectMethod="ParameterCulture">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    LoginTime:
                </td>
                <td>
                    <asp:TextBox ID="LoginTimeTextBox" runat="server" Text='<%# Bind("LoginTime") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    LogoutTime:
                </td>
                <td>
                    <asp:TextBox ID="LogoutTimeTextBox" runat="server" Text='<%# Bind("LogoutTime") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    LastActivity:
                </td>
                <td>
                    <asp:TextBox ID="LastActivityTextBox" runat="server" Text='<%# Bind("LastActivity") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SiteType:
                </td>
                <td>
                    <asp:TextBox ID="SiteTypeTextBox" runat="server" Text='<%# Bind("SiteType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Name:
                </td>
                <td>
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Surname:
                </td>
                <td>
                    <asp:TextBox ID="SurnameTextBox" runat="server" Text='<%# Bind("Surname") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroupId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroupId"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroupId" runat="server" TypeName="StaticInfoOperatorGroup"
                        SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLWarehouseId"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:Label ID="OperatorLabel" runat="server" Text='<%# Bind("Operator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorCode:
                </td>
                <td>
                    <asp:Label ID="OperatorCodeLabel" runat="server" Text='<%# Bind("OperatorCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <asp:Label ID="PasswordLabel" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    NewPassword:
                </td>
                <td>
                    <asp:Label ID="NewPasswordLabel" runat="server" Text='<%# Bind("NewPassword") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ActiveIndicator:
                </td>
                <td>
                    <asp:Label ID="ActiveIndicatorLabel" runat="server" Text='<%# Bind("ActiveIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ExpiryDate:
                </td>
                <td>
                    <asp:Label ID="ExpiryDateLabel" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    LastInstruction:
                </td>
                <td>
                    <asp:Label ID="LastInstructionLabel" runat="server" Text='<%# Bind("LastInstruction") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Printer:
                </td>
                <td>
                    <asp:Label ID="PrinterLabel" runat="server" Text='<%# Bind("Printer") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Port:
                </td>
                <td>
                    <asp:Label ID="PortLabel" runat="server" Text='<%# Bind("Port") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    IPAddress:
                </td>
                <td>
                    <asp:Label ID="IPAddressLabel" runat="server" Text='<%# Bind("IPAddress") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Culture:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCultureId" runat="server" DataSourceID="ObjectDataSourceDDLCultureId"
                        DataTextField="Culture" DataValueField="CultureId" SelectedValue='<%# Bind("CultureId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCultureId" runat="server" TypeName="StaticInfoCulture"
                        SelectMethod="ParameterCulture">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    LoginTime:
                </td>
                <td>
                    <asp:Label ID="LoginTimeLabel" runat="server" Text='<%# Bind("LoginTime") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    LogoutTime:
                </td>
                <td>
                    <asp:Label ID="LogoutTimeLabel" runat="server" Text='<%# Bind("LogoutTime") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    LastActivity:
                </td>
                <td>
                    <asp:Label ID="LastActivityLabel" runat="server" Text='<%# Bind("LastActivity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SiteType:
                </td>
                <td>
                    <asp:Label ID="SiteTypeLabel" runat="server" Text='<%# Bind("SiteType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Name:
                </td>
                <td>
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Surname:
                </td>
                <td>
                    <asp:Label ID="SurnameLabel" runat="server" Text='<%# Bind("Surname") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/OperatorGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="StaticInfoOperator"
    SelectMethod="GetOperator"
    DeleteMethod="DeleteOperator"
    UpdateMethod="UpdateOperator"
    InsertMethod="InsertOperator">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" DefaultValue="-1" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="Operator" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="OperatorCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Password" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="NewPassword" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ActiveIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="ExpiryDate" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="LastInstruction" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="Printer" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Port" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="IPAddress" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="CultureId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="LoginTime" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="LogoutTime" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="LastActivity" Type="DateTime" DefaultValue="-1" />
        <asp:Parameter Name="SiteType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Name" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Surname" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="OperatorId" QueryStringField="OperatorId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" DefaultValue="-1" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="Operator" Type="String" />
        <asp:Parameter Name="OperatorCode" Type="String" />
        <asp:Parameter Name="Password" Type="String" />
        <asp:Parameter Name="NewPassword" Type="String" />
        <asp:Parameter Name="ActiveIndicator" Type="Boolean" />
        <asp:Parameter Name="ExpiryDate" Type="DateTime" />
        <asp:Parameter Name="LastInstruction" Type="DateTime" />
        <asp:Parameter Name="Printer" Type="String" />
        <asp:Parameter Name="Port" Type="String" />
        <asp:Parameter Name="IPAddress" Type="String" />
        <asp:Parameter Name="CultureId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="LoginTime" Type="DateTime" />
        <asp:Parameter Name="LogoutTime" Type="DateTime" />
        <asp:Parameter Name="LastActivity" Type="DateTime" />
        <asp:Parameter Name="SiteType" Type="String" />
        <asp:Parameter Name="Name" Type="String" />
        <asp:Parameter Name="Surname" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
