﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_WaybillExceptionReport : System.Web.UI.Page
{
    #region InitializeCultureform
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCultureform"

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/WaybillExceptionReport.aspx";

            Session["ReportName"] = "WaybillExceptionReport";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[9];

            // Create the FromDate report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            // Create the OrderNumber report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("OrderNumber", TextBoxOrderNumber.Text);

            // Create the ExternalClientId report parameter
            string externalCompanyId = "-1";

            if (Session["ExternalCompanyId"] != null)
                externalCompanyId = Session["ExternalCompanyId"].ToString();

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ExternalCompanyId", externalCompanyId);

            // Create the DatabaseName report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
            
            // Create the ServerName report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
            
            // Create the UserName report parameter
            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            // Create the WarehouseId report parameter
            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());
            
            // Create the OutboundDocumentTypeId report parameter
            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("OutboundDocumentTypeId", DropDownListOutboundDocumentType.SelectedValue.ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
    protected void DropDownListOutboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
    }
}
