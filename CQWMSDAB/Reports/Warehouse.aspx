﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Warehouse.aspx.cs" Inherits="Reports_Warehouse" StylesheetTheme="Default" Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Warehouse Report</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAth4JqjOWonLf6R38neNybBSqhegbPwXKPmSQlW_kCKFWBJ4ryRSZFoAb7PEmyCygam8dgzVIkKl87A"></script>
    <%--<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAArbW_wMihA_2cf0k_cQ3FhxQaH9G5tHpvhbhHKNc4Tx7EFstKnBQ9w8dHtvQ-5bnITGH7cvnDlu7YuA"></script>--%>
    <%--<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAth4JqjOWonLf6R38neNybBSbI85HPJO3VMjNxs6Tq6Zz-KMkWxRqN_Cn2_8qlqiEwydbPol_3gafPA"></script>--%>
    <%--<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAx1OU7bOR-6m6q1yTzD04xBTZsqfG-eSb38rzAxI3wgjyFQQF4hSdAc4YwLe5fD9_fiIF8txxq7Tdag"></script>--%>
    <%--<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAArbW_wMihA_2cf0k_cQ3FhxR38NiEnD-PZmv9xbzoOn4hXwbhshQH9sFdeO_IhzN33TcZKORoNb9d-g"></script>--%>
<script type="text/javascript">
  
google.load("earth", "1");
<!-- google.load("maps", "2.99");  // For JS geocoder -->

var ge = null;
var geocoder;
var bolFlyTo = true;
var networkLink=null;
//var link=null;

function init() {
	<!--  geocoder = new GClientGeocoder(); -->
 	 google.earth.createInstance("map3d", initCB, failureCB);
}

function initCB(object) {
    ge = object;
    ge.getWindow().setVisibility(true);
    ge.getNavigationControl().setVisibility(ge.VISIBILITY_SHOW);
 	networkLink = ge.createNetworkLink("");

	networkLink.setDescription("NetworkLink open to fetched content");
	networkLink.setName("Open NetworkLink");
	//networkLink.setFlyToView(true);  

	link = ge.createLink("");
	link.setHref("");
	networkLink.setLink(link);
	ge.getWindow().clear;
	ge.getFeatures().appendChild(networkLink);
	}

function failureCB(object) {
  alert('load failed');}

var go = function(count) {
  var idealTilt = 65;  // all angles in the API are in degrees.
  var c0 = 0.65;
  var la = ge.getView().copyAsLookAt(ge.ALTITUDE_RELATIVE_TO_GROUND);
  
  var tilt = la.getTilt();
  tilt = c0 * tilt + (1 - c0) * idealTilt;
  la.setTilt(tilt);
  ge.getView().setAbstractView(la);
  
  if (count < 60) {
    setTimeout('go(' + (count+1) + ')', 33);
  }
}


function submitLocation(str) {
	
	var link = ge.createLink(str);
	link.setHref(str);
	networkLink.setLink(link);
    go(0);
	//ge.getWindow().clear;
	//ge.getFeatures().appendChild(networkLink);
	
}
function submitNoFlyToView(str) {
var networkLink = ge.createNetworkLink("");

networkLink.setFlyToView(true);
ge.getFeatures().appendChild(networkLink);
}

function submitLocationold(str) {
networkLink.setDescription("NetworkLink open to fetched content");
networkLink.setName("Open NetworkLink");
networkLink.setFlyToView(bolFlyTo   );

var link = ge.createLink("");
link.setHref(str);

networkLink.setLink(link);
ge.getFeatures().appendChild(networkLink);
link.setRefreshMode(1); 
link.setRefreshInterval(10); 


//var link = ge.createLink(str);
//link.setHref(str);
//networkLink.setLink(link);

//ge.getFeatures().appendChild(networkLink);

//go(0);

}
function Selected_Node(str){
 alert(str);
}


function Checkbox1_onclick() {

if (window.bolFlyTo)
    {
        window.bolFlyTo = false;
        networkLink.setFlyToView(window.bolFlyTo);
        
    }
else
    {
    window.bolFlyTo = true;
    networkLink.setFlyToView(window.bolFlyTo);    
    ge.getFeatures().appendChild(networkLink);
    };
   
}


  </script>


</head>
<body onload="init()" onunload="GUnload()">
        <form id="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></asp:ScriptManager>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" width="680px">
                        <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="Warehouse Activity"></asp:Label>
                        <br />
                        <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="CQuential 3D Model"></asp:Label>
                        <br />
                        <br />
                    </td>
                    <td>
                        <asp:Image runat="server" ID="ImageHeader" ImageUrl="~/Images/Cq logoRGB Tiny.jpg" AlternateText="CQuential Solutions" />
                    </td>
                </tr>
            </table>
                <div id="MainMenu">
                    <asp:Menu ID="Menu1" runat="server" SkinID="MainMenuHorizontal" Orientation="Horizontal" cssselectorclass="PrettyMenu" DataSourceID="xmlDataSource" DisappearAfter="500">
                        <DataBindings>
                            <asp:MenuItemBinding DataMember="MenuItem" NavigateUrlField="NavigateUrl" TextField="MenuItem"
                                ToolTipField="ToolTip" />
                        </DataBindings>
                    </asp:Menu>
                     
                </div>
                <asp:XmlDataSource ID="xmlDataSource" runat="server" TransformFile="~/MasterPages/MenuItem.xsl"
                    XPath="MenuItems/MenuItem" />
               
            <br />
            <div>
                <a href="" target="_blank"></a>
                <button style="width:90px" id='All' onclick="return submitLocationold('http://196.34.138.122/CQDemo/VisualData/war.zip')">All</button>&nbsp;&nbsp;
                &nbsp; &nbsp;&nbsp;&nbsp;
                <button style="width:90px" id='Receiving' onclick="return submitLocationold('http://196.34.138.122/CQDemo/VisualData/Sort1/VisualData_Floor_Receiving.zip')">Receiving</button>&nbsp;&nbsp;
                <button style="width:90px" id='Production' onclick="return submitLocationold('http://196.34.138.122/CQDemo/VisualData/Sort1/VisualData_Floor_Production.zip')">Production</button>
                <input id="Checkbox1" style="width: 39px" type="checkbox" checked="CHECKED" onclick="return Checkbox1_onclick()" /><br />
                <%--<button style="width:90px" id='Fly' onclick="return submitNoFlyToView()">No Fly to</button>&nbsp;&nbsp;--%>
                <table >
                    <tr>
                        <td style="width: 122px; height: 21px;">
            <div id='map3d_container' style='border: 1px solid silver; height: 400px;'>
                <div id='map3d' style="height: 100%; width: 949px;" onclick="return map3d_onclick()">
                    1</div>
            </div>
                        </td>
                        <td style="width: 275px; height: 21px;">
                            <asp:TreeView ID="TreeView1" runat="server" 
                                DataSourceID="xmlDataSourceReports" ImageSet="Arrows" OnSelectedNodeChanged="TreeView1_SelectedNodeChanged" ExpandDepth="1" >
                                <DataBindings>
                                    <asp:TreeNodeBinding DataMember="r" TextField="report" SelectAction="Expand" />
                                    <asp:TreeNodeBinding DataMember="t" TextField="type" SelectAction="Expand" />
                                    <asp:TreeNodeBinding DataMember="s" TextField="Sort1" ValueField="path" NavigateUrlField="NavigateField" />
                                </DataBindings>
                                <ParentNodeStyle Font-Bold="False" />
                                <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                                <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px"
                                    VerticalPadding="0px" />
                                <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px"
                                    NodeSpacing="0px" VerticalPadding="0px" />
                            </asp:TreeView>
                <asp:XmlDataSource ID="xmlDataSourceReports" runat="server" />
                        </td>
                        
                    </tr>
                    
                    
                </table>
            </div>
            
            <br />
            
            <asp:Panel ID="PanelFooter" runat="server" Width="100%" BackColor="#C0C0C0">
                <div align="left" style="float:left;">
                    &nbsp;&nbsp;&nbsp;<asp:Label ID="LabelCopyRight" runat="server" SkinID="CopyRight" Text="Copyright &copy; 2007 CQuential Solutions (Pty) Ltd. "></asp:Label>
                </div>
                <div align="right">
                    <asp:LoginStatus ID="LoginStatus1" runat="server" />
                    <asp:LoginName ID="LoginName1" runat="server" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Label ID="LabelLoggedOnTo" runat="server" SkinID="CopyRight" Text="Database: "></asp:Label>
                    <asp:Label ID="LabelDatabase" runat="server" SkinID="CopyRight"></asp:Label>
                    &nbsp;&nbsp;&nbsp;
                </div>
            </asp:Panel>
            <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderFooter" runat="server"
                TargetControlID="PanelFooter"
                Radius="10"
                Color="#C0C0C0"
                BorderColor="#909385"
                Corners="Bottom" />
        </form>
</body>
</html>
