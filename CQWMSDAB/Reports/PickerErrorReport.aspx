﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PickerErrorReport.aspx.cs" Inherits="Reports_PickerErrorReport" Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
        ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
        TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
        <Panes>
            <%--              <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select an Operator Group</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DD_OperatorGroupId" runat="server" DataSourceID="OperatorGroupDataSource"
                            DataTextField="OperatorGroup" DataValueField="OperatorGroupId">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>--%>
            <ajaxToolkit:AccordionPane ID="AccordionPaneOperator" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select an Operator</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DD_Operator" runat="server" DataSourceID="OperatorDataSource"
                                DataTextField="Operator" DataValueField="OperatorId">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:ObjectDataSource ID="OperatorGroupDataSource" runat="server" SelectMethod="GetOperatorGroups"
        TypeName="OperatorGroup">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" Type="String" SessionField="ConnectionStringName" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="OperatorDataSource" runat="server" SelectMethod="GetOperatorList"
        TypeName="Operator">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" Type="String" SessionField="ConnectionStringName" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WareHouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
        OnClick="ButtonPrint_Click" />
</asp:Content>
