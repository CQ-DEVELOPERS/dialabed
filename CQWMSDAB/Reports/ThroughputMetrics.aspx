﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="ThroughputMetrics.aspx.cs"
    Inherits="Reports_ThroughputMetrics"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>
<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanePrincipal" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Principal</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPrincipal" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                DataTextField="Principal" DataValueField="PrincipalId">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                SelectMethod="GetPrincipalParameter">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane> 
                     
            <ajaxToolkit:AccordionPane ID="AccordionPaneFromTime" runat="server">
                <Header>
                    <a href="" class="accordionLink">Enter From Time</a></Header>
                <Content>
                    <cc1:TimeSelector ID="TimeSelector1" runat="server"></cc1:TimeSelector>
                    <%--<asp:UpdatePanel ID="UpdatePanelFromTime" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelFromTime" runat="server" Text="From Time:"></asp:Label>                            
                            <asp:TextBox ID="TextBoxFromTime" runat="server" Text=""></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidatorFromTime" runat="server" ControlToValidate="TextBoxFromTime"  ErrorMessage="Numbers only" MaximumValue="24" MinimumValue="00" Type="Integer"></asp:RangeValidator> 
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneToTime" runat="server">
                <Header>
                    <a href="" class="accordionLink">Enter To Time</a></Header>
                <Content>
                    <cc1:TimeSelector ID="TimeSelector2" runat="server"></cc1:TimeSelector>
                    <%--<asp:UpdatePanel ID="UpdatePanelToTime" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelToTime" runat="server" Text="TTo Time:"></asp:Label>
                            <asp:TextBox ID="TextBoxToTime" runat="server" Text=""></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidatorToTime" runat="server" ControlToValidate="TextBoxToTime"  ErrorMessage="Numbers only" MaximumValue="24" MinimumValue="00" Type="Integer"></asp:RangeValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>


