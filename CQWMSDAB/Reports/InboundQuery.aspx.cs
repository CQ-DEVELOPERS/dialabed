﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_InboundQuery : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                //Session["StorageUnitId"] = -1;
            }
        }
        catch { }
    }


    #region ButtonSelectInboundDocumentType_Click
    protected void ButtonSelectInboundDocumentType_Click(object sender, EventArgs e)
    {
        //theErrMethod = "ButtonSelectInboundDocumentType_Click";
        try
        {
            foreach (ListItem item in CheckBoxListInboundDocumentType.Items)
            {
                item.Selected = true;
            }

            //Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            //result = SendErrorNow("SelectInboundDocumentType" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectInboundDocumentType_Click

    #region ButtonDeselectInboundDocumentType_Click
    protected void ButtonDeselectInboundDocumentType_Click(object sender, EventArgs e)
    {
        //theErrMethod = "ButtonDeselectLocation_Click";

        try
        {
            foreach (ListItem item in CheckBoxListInboundDocumentType.Items)
            {
                item.Selected = false;
            }

            //Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            //result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectInboundDocumentType_Click

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {

            Session["FromURL"] = "~/Reports/InboundQuery.aspx";

            Session["ReportName"] = "Inbound Query";

            ReportParameter[] RptParameters = new ReportParameter[15];

            //  Set up selected radio button values
            string DateSearch = RadioButtonDateSearch.SelectedValue;

            string InboundDocumentTypeId = CheckBoxListInboundDocumentType.SelectedValue;

            if (InboundDocumentTypeId ==  "")
            {
                InboundDocumentTypeId = "-1";
            }

            RptParameters[0] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            RptParameters[1] = new ReportParameter("InboundShipmentId", Session["InboundShipmentId"].ToString());

            RptParameters[2] = new ReportParameter("PrincipalId", Session["PrincipalId"].ToString());

            RptParameters[3] = new ReportParameter("InboundDocumentTypeId", InboundDocumentTypeId.ToString());

            RptParameters[4] = new ReportParameter("OrderNumber", Session["OrderNumber"].ToString());

            RptParameters[5] = new ReportParameter("DeliveryNoteNumber", Session["DeliveryNoteNumber"].ToString());

            RptParameters[6] = new ReportParameter("SupplierCode", Session["SupplierCode"].ToString());

            RptParameters[7] = new ReportParameter("SupplierName", Session["SupplierName"].ToString());

            RptParameters[8] = new ReportParameter("FromDate", Session["FromDate"].ToString());

            RptParameters[9] = new ReportParameter("ToDate", Session["ToDate"].ToString());
      
            RptParameters[10] = new ReportParameter("DateUsed", DateSearch.ToString());

            RptParameters[11] = new ReportParameter("Status", Session["Status"].ToString());

            RptParameters[12] = new ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[13] = new ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[14] = new ReportParameter("UserName", Session["UserName"].ToString());


            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}