using System;
using System.Collections;
using System.Threading;
using System.Web.UI;

public partial class Reports_JobLabel : System.Web.UI.Page
{
    #region InitializeCulture

    protected override void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());

                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch (Exception)
        {
            // ignored
        }
    }

    #endregion InitializeCulture

    #region Page_Load

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["JobId"] != null)
            {
                ArrayList checkedList = new ArrayList();
                int jobId = -1;

                if (int.TryParse(Request.QueryString["JobId"], out jobId))
                {
                    TextBoxJob.Text = "J:" + jobId.ToString();

                    checkedList.Add(jobId);

                    Session["checkedList"] = checkedList;

                    Session["PrintLabel"] = false;

                    Session["LabelName"] = "Despatch By Order Label.lbl";

                    Session["FromURL"] = "~/Reports/JobLabel.aspx";

                    if (Session["Printer"] == null)
                        Session["Printer"] = "";

                    var sessionPort = Session["Port"];
                    var sessionPrinter = Session["Printer"];
                    var sesionIndicatorList = Session["indicatorList"];
                    var sessionCheckedList = Session["checkedList"];
                    var sessionLocationList = Session["locationList"];
                    var sessionConnectionString = Session["ConnectionStringName"];
                    var sessionTitle = Session["Title"];
                    var sessionBarcode = Session["Barcode"];
                    var sessionWarehouseId = Session["WarehouseId"];
                    var sessionProductList = Session["ProductList"];
                    var sessionTakeOnLabel = Session["TakeOnLabel"];
                    var sessionLabelCopies = Session["LabelCopies"];

                    NLWAXPrint nl = new NLWAXPrint();
                    nl.onSessionUpdate += Nl_onSessionUpdate;
                    string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                              Session["LabelName"].ToString(),
                                                              sessionPrinter,
                                                              sessionPort,
                                                              sesionIndicatorList,
                                                              sessionCheckedList,
                                                              sessionLocationList,
                                                              sessionConnectionString,
                                                              sessionTitle,
                                                              sessionBarcode,
                                                              sessionWarehouseId,
                                                              sessionProductList,
                                                              sessionTakeOnLabel,
                                                              sessionLabelCopies);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }
        catch { }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }

    #endregion Page_Load
}