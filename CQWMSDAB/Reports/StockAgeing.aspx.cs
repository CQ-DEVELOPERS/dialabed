using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_StockAgeing : System.Web.UI.Page
{

    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["StorageUnitId"] = -1;
                Session["BatchId"] = -1;
            }
        }
        catch { }
    }
    #endregion Page_Load

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
             string days = "0";

               if (txt_amount.Text != "")
            {
             switch (dd_Dmy.SelectedValue){
                 case "D" :
                     days = int.Parse(txt_amount.Text).ToString();
                     break;
                                    
                 case "M" :
                     days = (int.Parse(txt_amount.Text) * 30).ToString();
                     break;
                     
                 case "Y" :
                     days = (int.Parse(txt_amount.Text) * 365).ToString();
                     break;
                    
             }
            }

                

            Session["FromURL"] = "~/Reports/StockAgeing.aspx";

            Session["ReportName"] = "Ageing Report";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[8];

            // Create the ConnectionString report parameter
   //         string strReportConnectionString = Session["ReportConnectionString"].ToString();
  //          RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);

            // Create the WarehouseId report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());
            // Create the StorageUnitId report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());
            // Create the BatchId report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("BatchId", Session["BatchId"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("Dayvalue", days);

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("PrincipalId", DropDownListPrincipal.SelectedValue.ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch  { }
    }
}
