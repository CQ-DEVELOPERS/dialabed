﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="OutboundSummaryReport.aspx.cs"
    Inherits="Reports_OutboundSummaryReport"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="ucbtch" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="ucprd" %>
<%@ Register Src="../Common/StatusDropdown.ascx" TagName="StatusSearch" TagPrefix="ucss" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneProductSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Enter an Order Number</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelOrderNumber" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelOrderNumber" runat="server" Text="Order Number:"></asp:Label>
                            <asp:TextBox ID="TextBoxOrderNumber" runat="server"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionProductSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Product / SKU</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelProductSearch" runat="server">
                        <ContentTemplate>
                            <ucprd:ProductSearch ID="ProductSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneBatchNumberSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Batch</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelBatch" runat="server">
                        <ContentTemplate>
                            <ucbtch:BatchSearch ID="BatchSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanelStatusSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Status</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelStatus" runat="server">
                        <ContentTemplate>
                            <ucss:StatusSearch ID="StatusSearch" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanePrincipal" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Principal</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPrincipal" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                DataTextField="Principal" DataValueField="PrincipalId">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                SelectMethod="PrincipalSelect">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="userName" SessionField="UserName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>

    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>


