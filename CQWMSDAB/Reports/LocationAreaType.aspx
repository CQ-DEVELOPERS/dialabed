<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="LocationAreaType.aspx.cs"
    Inherits="Reports_LocationAreaType"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/AreaSearch.ascx" TagName="AreaSearch" TagPrefix="ucas" %>
<%@ Register Src="../Common/LocationType.ascx" TagName="LocationTypeSearch" TagPrefix="uclt" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
           
            <ajaxToolkit:AccordionPane ID="AccordionPaneArea" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select an Area</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelArea" runat="server">
                        <ContentTemplate>
                            <ucas:AreaSearch ID="AreaSearch" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            
            <ajaxToolkit:AccordionPane ID="AccordionPaneLocationType" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Location Type</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelLocationType" runat="server">
                        <ContentTemplate>
                            <uclt:LocationTypeSearch ID="LocationTypeSearch" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>

