﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_DespatchReport : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = -1;
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GridView1.DataBind();
        }
        catch { }
    }
    #endregion ButtonSearch_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/DespatchReport.aspx";

            Session["ReportName"] = "Despatch Report";

            ReportParameter[] RptParameters = new ReportParameter[6];

            // Create the ConnectionString report parameter
            RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());
            // Create the OutboundShipmentId report parameter
            RptParameters[1] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());
            // Create the IssueId report parameter
            RptParameters[2] = new ReportParameter("IssueId", Session["IssueId"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
    #endregion ButtonPrint_Click

    #region GridView1_SelectedIndexChanged
    protected void GridView1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            Session["OutboundShipmentId"] = GridView1.SelectedDataKey["OutboundShipmentId"].ToString();
            Session["IssueId"] = GridView1.SelectedDataKey["IssueId"].ToString();
        }
        catch { }
    }
    #endregion GridView1_SelectedIndexChanged
}
