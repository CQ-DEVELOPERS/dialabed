﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_BilledTransaction : System.Web.UI.Page
{
  protected void Page_Load( object sender, EventArgs e )
  {

  }

  /// <summary>
  /// Handles the Click event of the ButtonPrint control.
  /// </summary>
  /// <param name="sender">The source of the event.</param>
  /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  protected void ButtonPrint_Click( object sender, EventArgs e )
  {
    var id = "";

    if (grdBilledTransactions.SelectedValue != null)
    {
       id = grdBilledTransactions.SelectedValue.ToString();
    }

    if ( string.IsNullOrEmpty( id ) )
      return;

    Session["FromUrl"] = HttpContext.Current.Request.Url;
    Session["ReportName"] = "Bill Processing";

    // Create the Report Parameters holder
    var reportParameters = new List<ReportParameter>();

    // Create the BilledTransactionId (Id) report parameter
    reportParameters.Add( new ReportParameter( "Id", id ) );
    reportParameters.Add( new ReportParameter( "ServerName", Session["ServerName"].ToString() ) );
    reportParameters.Add( new ReportParameter( "DatabaseName", Session["DatabaseName"].ToString() ) );
    reportParameters.Add( new ReportParameter( "UserName", Session["UserName"].ToString() ) );

    Session["ReportParameters"] = reportParameters.ToArray();

    Response.Redirect( "~/Reports/Report.aspx" );
  }
}