using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Inbound_CreditAdviceReport : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Init    
        protected void Page_Init(object sender, EventArgs e)
        {
            //Session["countLoopsToPreventInfinLoop"] = 0;
            //theErrMethod = "Page Load";
            //try
            //{
            //    if (!Page.IsPostBack)
            //    {
            //        Session["CreditAdviceInboundShipmentId"] = 1;
            //        Session["CreditAdviceReceiptId"] = 1;

            //        if (Session["CreditAdviceInboundShipmentId"] == null || Session["CreditAdviceReceiptId"] == null)
            //            return;

            //        // Set the processing mode for the ReportViewer to Remote
            //        ReportViewer1.ProcessingMode = ProcessingMode.Remote;

            //        // Set the report server URL and report path
            //        this.ReportViewer1.ServerReport.ReportServerUrl = new System.Uri("http://CQSERVER0001/ReportServer");

            //        string strReport = "/CQuentialWarehouseReports/Credit Advice";
            //        this.ReportViewer1.ServerReport.ReportPath = strReport;

            //        ReportParameter[] RptParameters = new ReportParameter[2];

            //        // Create the InboundShipmentId report parameter
            //        string inboundShipmentId = Session["CreditAdviceInboundShipmentId"].ToString();
            //        RptParameters[0] = new ReportParameter("InboundShipmentId", inboundShipmentId);

            //        // Create the ReceiptId report parameter
            //        string receiptId = Session["CreditAdviceReceiptId"].ToString();
            //        RptParameters[1] = new ReportParameter("ReceiptId", receiptId);

            //        // Set the report parameters for the report
            //        ReportViewer1.ServerReport.SetParameters(RptParameters);

            //        // Display the Report
            //        this.ReportViewer1.ServerReport.Refresh();
            //    }

            //    Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            //}
            //catch (Exception ex)
            //{
            //    result = SendErrorNow("Inbound_CreditAdviceReport" + "_" + ex.Message.ToString());
            //    Master.ErrorText = result;
            //}

        }

    #endregion Page_Init

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "StockTakeCreateLocation", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "StockTakeCreateLocation", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
