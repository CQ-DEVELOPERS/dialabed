<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" 
    CodeFile="BatchQuery.aspx.cs" Inherits="Reports_BatchQuery" Title="<%$ Resources:Default, ReportTitle %>" Theme="Default"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
        
            <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <a href="" class="accordionLink">Batch Number</a></Header>
                <Content>
                
                
                   <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label1" runat="server" Text="Batch Number:"></asp:Label>
                            <asp:TextBox ID="TextBoxBatchNumber" runat="server" Text=""></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                        </Content>
            </ajaxToolkit:AccordionPane>
            
            <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                <Header>
                    <a href="" class="accordionLink">Batch Refererence Number</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label2" runat="server" Text="Batch Ref Number:"></asp:Label>
                            <asp:TextBox ID="TextBoxBatchRefNumber" runat="server" Text=""></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </Content>
                 </ajaxToolkit:AccordionPane>
        
            <ajaxToolkit:AccordionPane ID="AccordionPaneProductSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Product</a></Header>
                <Content>
                
                    
                    <uc2:ProductSearch ID="ProductSearch1" runat="server" />
                    
                    </Content>
            </ajaxToolkit:AccordionPane>
                    
                    
            <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                <Header>
                    <a href="" class="accordionLink">Enter a Status</a></Header>
                <Content>
            <asp:DropDownList ID="DropDownBatchStatus" runat="server" DataSourceID="ObjectDataSourceStatus" 
                            DataTextField="Status" DataValueField="StatusId" >
             </asp:DropDownList>
             </Content>
             
            </ajaxToolkit:AccordionPane>
            
        
            <ajaxToolkit:AccordionPane ID="AccordionPanePurchaseOrderNumber" runat="server">
                <Header>
                    <a href="" class="accordionLink">Enter a Purchase Order Number</a></Header>
                <Content>
                
                
                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelPurchaseOrderNumber" runat="server" Text="Order Number:"></asp:Label>
                            <asp:TextBox ID="TextBoxPurchaseOrderNumber" runat="server" Text=""></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
               </Content>
            </ajaxToolkit:AccordionPane>
            
            
            
             
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click"/>
    
<%--<asp:ObjectDataSource runat="server" SelectMethod="SelectOperatorExternalCompanies" ID="ExternalCompanies" TypeName="ExternalCompany">
    <SelectParameters>
    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    <asp:SessionParameter Name="OperatorID" SessionField="OperatorGroupId" Type="Int16" />
    
    </SelectParameters>
</asp:ObjectDataSource>
--%>    

    
    <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status" SelectMethod="GetStatus">   
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="type" DefaultValue="PR" Type="String" /> 
        </SelectParameters>
    </asp:ObjectDataSource>
    
</asp:Content>



