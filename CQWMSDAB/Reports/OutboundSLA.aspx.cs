﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_OutboundSLA : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["PrincipalId"] = -1;
                Session["StatusId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/OutboundSLA.aspx";

            string ReportType = RadioButtonListSummaryDetail.SelectedValue;

            if (ReportType == "D")
            {
                Session["ReportName"] = "Outbound SLA Detail";
            }
            else
            {
                Session["ReportName"] = "Outbound SLA";
            }

            ReportParameter[] RptParameters = new ReportParameter[10];

            //  Set up selected radio button values
            string ShipDateOption = RadioButtonListShipDate.SelectedValue;
            string Measure = RadioButtonSLAMeasured.SelectedValue;

            RptParameters[0] = new ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[1] = new ReportParameter("ToDate", Session["ToDate"].ToString());

            // Create the PrincipalId report parameter
            RptParameters[2] = new ReportParameter("PrincipalId", DropDownListPrincipal.SelectedValue.ToString());

            RptParameters[3] = new ReportParameter("ShipDateOption", ShipDateOption.ToString());

            RptParameters[4] = new ReportParameter("Measure", Measure.ToString());

            //string orderNumber = TextBoxOrderNumber.Text;

            //if (orderNumber == "")
            //    orderNumber = "-1";

            RptParameters[5] = new ReportParameter("OrderNumber", TextBoxOrderNumber.Text);

            RptParameters[6] = new ReportParameter("StatusId", Session["StatusId"].ToString());

            RptParameters[7] = new ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[8] = new ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[9] = new ReportParameter("UserName", Session["UserName"].ToString());


            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
