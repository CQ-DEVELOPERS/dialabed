using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Inbound_ReceivingCheckSheet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                int inboundShipmentId = -1;
                int receiptId = -1;

                if (int.TryParse(Session["InboundShipmentId"].ToString(), out inboundShipmentId) && int.TryParse(Session["ReceiptId"].ToString(), out receiptId))
                {
                    Reports r = new Reports();
                    ReportDataSource rds = new ReportDataSource();

                    rds.Name = "ReceivingCheckSheet_p_Receiving_Check_Sheet";
                    rds.Value = r.ReceivingCheckSheet(Session["ConnectionStringName"].ToString(), inboundShipmentId, receiptId).Tables[0];

                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReceivingCheckSheet.rdlc");

                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.LocalReport.Refresh();
                }
            }
        }
        catch { }
    }
}
