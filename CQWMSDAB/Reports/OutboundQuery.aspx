<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OutboundQuery.aspx.cs" Inherits="Reports_OutboundQuery" Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch"
    TagPrefix="uc1" %>
<%@ Register Src="../Common/ShipmentSearch.ascx" TagName="OutboundSearchShipment"
    TagPrefix="uc2" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:RadioButtonList ID="rblSummary" runat="server">
        <asp:ListItem Selected="True" Text="Summary" Value="true"></asp:ListItem>
        <asp:ListItem Text="Detail" Value="false"></asp:ListItem>
        <asp:ListItem Text="Order" Value="order"></asp:ListItem>
    </asp:RadioButtonList>
    <ajaxToolkit:Accordion ID="AccordionLineInsert" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <a href="" class="accordionLink">1. Select an Order</a></Header>
                <Content>
                    <uc1:OutboundSearch ID="OutboundSearch" runat="server" />
                    <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"
                        OnClick="ButtonSearch_Click" />
                    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                        OnClick="ButtonPrint_Click" />
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1" AllowPaging="True"
                                AllowSorting="True" DataKeyNames="OutboundShipmentId,IssueId" AutoGenerateColumns="False"
                                OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                                    <asp:BoundField DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                        SortExpression="OutboundShipmentId"></asp:BoundField>
                                    <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                        SortExpression="OrderNumber"></asp:BoundField>
                                    <asp:BoundField DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                        SortExpression="CustomerCode"></asp:BoundField>
                                    <asp:BoundField DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                        SortExpression="Customer"></asp:BoundField>
                                    <asp:BoundField DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                        SortExpression="NumberOfLines"></asp:BoundField>
                                    <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                        SortExpression="DeliveryDate"></asp:BoundField>
                                    <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                        SortExpression="Status"></asp:BoundField>
                                    <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server">
                <Header>
                    <a href="" class="accordionLink">2. Select a Shipment</a></Header>
                <Content>
                    <uc2:OutboundSearchShipment ID="OutboundSearchShipment" runat="server" />
                    <asp:Button ID="ButtonSearchShipment" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"
                        OnClick="ButtonSearchShipment_Click" />
                    <asp:Button ID="ButtonPrintShipment" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                        OnClick="ButtonPrintShipment_Click" />
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="GridView2" runat="server" DataSourceID="ObjectDataSourceShipment"
                                AllowPaging="True" AllowSorting="True" DataKeyNames="OutboundShipmentId" AutoGenerateColumns="False"
                                OnSelectedIndexChanged="GridView2_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                                    <asp:BoundField DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                        SortExpression="OutboundShipmentId"></asp:BoundField>
                                    <asp:BoundField DataField="Orders" HeaderText="<%$ Resources:Default, Orders %>"
                                        SortExpression="Orders"></asp:BoundField>
                                    <asp:BoundField DataField="TotalJobs" HeaderText="<%$ Resources:Default, TotalJobs %>"
                                        SortExpression="TotalJobs"></asp:BoundField>
                                    <asp:BoundField DataField="JobsWaiting" HeaderText="<%$ Resources:Default, JobsWaiting %>"
                                        SortExpression="JobsWaiting"></asp:BoundField>
                                    <asp:BoundField DataField="JobsPalletised" HeaderText="<%$ Resources:Default, JobsPalletised %>"
                                        SortExpression="JobsPalletised"></asp:BoundField>
                                    <asp:BoundField DataField="JobsPlanComplete" HeaderText="<%$ Resources:Default, JobsPlanComplete %>"
                                        SortExpression="JobsPlanComplete"></asp:BoundField>
                                    <asp:BoundField DataField="JobsRelease" HeaderText="<%$ Resources:Default, JobsRelease %>"
                                        SortExpression="JobsRelease"></asp:BoundField>
                                    <asp:BoundField DataField="JobsStarted" HeaderText="<%$ Resources:Default, JobsStarted %>"
                                        SortExpression="JobsStarted"></asp:BoundField>
                                    <asp:BoundField DataField="JobsChecking" HeaderText="<%$ Resources:Default, JobsChecking %>"
                                        SortExpression="JobsChecking"></asp:BoundField>
                                    <asp:BoundField DataField="JobsChecked" HeaderText="<%$ Resources:Default, JobsChecked %>"
                                        SortExpression="JobsChecked"></asp:BoundField>
                                    <asp:BoundField DataField="JobsDespatch" HeaderText="<%$ Resources:Default, JobsDespatch %>"
                                        SortExpression="JobsDespatch"></asp:BoundField>
                                    <asp:BoundField DataField="JobsDespatchChecked" HeaderText="<%$ Resources:Default, JobsDespatchChecked %>"
                                        SortExpression="JobsDespatchChecked"></asp:BoundField>
                                    <asp:BoundField DataField="JobsComplete" HeaderText="<%$ Resources:Default, JobsComplete %>"
                                        SortExpression="JobsComplete"></asp:BoundField>
                                    <asp:BoundField DataField="JobsQualityAssurance" HeaderText="<%$ Resources:Default, JobsQualityAssurance %>"
                                        SortExpression="JobsQualityAssurance"></asp:BoundField>
                                    <asp:BoundField DataField="JobsNoStock" HeaderText="<%$ Resources:Default, JobsNoStock %>"
                                        SortExpression="JobsNoStock"></asp:BoundField>
                                    <asp:BoundField DataField="JobsPause" HeaderText="<%$ Resources:Default, JobsPause %>"
                                        SortExpression="JobsPause"></asp:BoundField>
                                    <asp:BoundField DataField="Releases" HeaderText="<%$ Resources:Default, Releases %>"
                                        SortExpression="Releases"></asp:BoundField>
                                    <%--<asp:BoundField DataField="DespatchLocation" HeaderText="<%$ Resources:Default, DespatchLocation %>"
                                        SortExpression="DespatchLocation"></asp:BoundField>--%>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ButtonSearchShipment" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                <Header>
                    <a href="" class="accordionLink">3. Select a Pallet</a></Header>
                <Content>
                    <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
                    <asp:TextBox ID="TextBoxBarcode" runat="server"></asp:TextBox>
                    <asp:Button ID="ButtonPrintJob" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                        OnClick="ButtonPrintJob_Click" />
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                <Header>
                    <a href="" class="accordionLink">4. Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Button ID="Button1" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                        OnClick="ButtonPrintDate_Click" />
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Reports" SelectMethod="SearchDespatchAdvice">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId"
                Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                Type="Int32" />
            <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
            <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
            <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                Type="String" />
            <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
            <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceShipment" runat="server" TypeName="Reports"
        SelectMethod="SearchDespatchAdviceShipment">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId2"
                Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                Type="Int32" />
            <asp:SessionParameter Name="FromDate" SessionField="FromDate2" Type="DateTime" />
            <asp:SessionParameter Name="ToDate" SessionField="ToDate2" Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
