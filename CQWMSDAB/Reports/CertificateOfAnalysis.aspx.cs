﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_CertificateOfAnalysis : System.Web.UI.Page
{
    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region GridViewProductSearchCOA_OnSelectedIndexChanged
    protected void GridViewProductSearchCOA_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["ProductCode"] = GridViewProductSearchCOA.SelectedDataKey["ProductCode"];
        Session["Batch"] = GridViewProductSearchCOA.SelectedDataKey["Batch"];
    }
    #endregion GridViewProductSearchCOA_OnSelectedIndexChanged

    #region GridViewProductSearchCOA_PageIndexChanging
    protected void GridViewProductSearchCOA_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewProductSearchCOA.PageIndex = e.NewPageIndex;
            GridViewProductSearchCOA_DataBind();
        }
        catch { }
    }
    #endregion "GridViewProductSearchCOA_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewProductSearchCOA.PageIndex = 0;

        GridViewProductSearchCOA_DataBind();
    }
    #endregion "ButtonSearch_Click"

    #region GridViewProductSearchCOA_DataBind
    protected void GridViewProductSearchCOA_DataBind()
    {
        Product product = new Product();

        //GridViewProductSearchCOA.DataSource = product.SearchProductsStorageUnitBatchCOA(Session["ConnectionStringName"].ToString(), TextBoxProductCode.Text, TextBoxProduct.Text, TextBoxSKUCode.Text, TextBoxSKU.Text, TextBoxBatch.Text);
        //GridViewProductSearchCOA.DataSource = product.SearchProductsStorageUnitBatchCOA(Session["ConnectionStringName"].ToString(), TextBoxProductCode.Text, TextBoxProduct.Text);

        GridViewProductSearchCOA.DataBind();
    }
    #endregion GridViewProductSearchCOA_DataBind

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();
            ArrayList checkedList = new ArrayList();
            string Batch = "-1";

            foreach (GridViewRow row in GridViewProductSearchCOA.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                //checkedList.Add((int)GridViewProductSearchCOA.DataKeys[row.RowIndex].Values["Batch"]);
                Batch = (string)GridViewProductSearchCOA.DataKeys[row.RowIndex].Values["Batch"];
                Session["Batch"] = Batch;
                cb.Checked = true;
            }
            Session["Batch"] = Batch;
            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletLabel" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            CheckBox cb = new CheckBox();
            foreach (GridViewRow row in GridViewProductSearchCOA.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                Session["FromURL"] = "~/Reports/CertificateOfAnalysis.aspx";

                Session["ReportName"] = "Certificate Of Analysis";

                ReportParameter[] RptParameters = new ReportParameter[5];

                // Create the JobId report parameter

                RptParameters[0] = new ReportParameter("ProductCode", GridViewProductSearchCOA.DataKeys[row.RowIndex].Values["ProductCode"].ToString());


                string batch = Session["Batch"].ToString();

                if (batch == "")
                    batch = "-1";

                RptParameters[1] = new ReportParameter("Batch", batch);

                //RptParameters[1] = new ReportParameter("Batch", GridViewProductSearchCOA.DataKeys[row.RowIndex].Values["Batch"].ToString());
                //RptParameters[1] = new ReportParameter("Batch", Session["Batch"].ToString());

                RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                Session["ReportParameters"] = RptParameters;

                Response.Redirect("~/Reports/Report.aspx");
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion ButtonPrint_Click
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "PutawayMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "PutawayMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
