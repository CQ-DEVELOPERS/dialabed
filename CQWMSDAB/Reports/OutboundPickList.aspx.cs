using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using System.IO;

public partial class Reports_OutboundPickList : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = -1;
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            RadGridOrders.DataBind();
        }
        catch { }
    }
    #endregion ButtonSearch_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
    #endregion ButtonPrint_Click

    #region RadGridOrders_ItemCommand
    protected void RadGridOrders_ItemCommand(object source, GridCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Print")
            {
                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {

                        Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                        Session["IssueId"] = item.GetDataKeyValue("IssueId");

                        Session["FromURL"] = "~/Reports/DespatchDocument.aspx";

                        Session["ReportName"] = "Pick Check Sheet";

                        ReportParameter[] RptParameters = new ReportParameter[5];

                        // Create the OutboundShipmentId report parameter
                        RptParameters[0] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());
                        // Create the IssueId report parameter
                        RptParameters[1] = new ReportParameter("IssueId", Session["IssueId"].ToString());

                        RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                                        this.Page.GetType(),
                                                                        "newWindow",
                                                                        "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                                        true);
                        break;
                    }
                }
            }
        }
        catch { }
    }
    #endregion RadGridOrders_ItemCommand
}
