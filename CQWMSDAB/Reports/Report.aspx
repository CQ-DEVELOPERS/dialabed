<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="Report.aspx.cs"
    Inherits="Reports_Report"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
 <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
    <%--<asp:Label ID="LabelMessage" runat="server"></asp:Label>--%>
    <asp:Label ID="LabelError" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Button ID="ButtonBack" runat="server" Text="<%$ Resources:Default,Back %>" OnClick="ButtonBack_Click" />
    <asp:Button ID="but_CreateSubscription" runat="server" Text="Create Subscription" SkinID="ButtonLarge" OnClick="but_CreateSubscription_Click" />
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="600px">
    </rsweb:ReportViewer>
    <br />
    <br />
</asp:Content>

