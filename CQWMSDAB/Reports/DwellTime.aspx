<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="DwellTime.aspx.cs"
    Inherits="Reports_DwellTime"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ReceivingBay.ascx" TagName="ReceivingBay" TagPrefix="ucrb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneReceivingBay" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Instruction Type</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelReceivingBay" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="ObjectDataSourceInstructionType"
                                DataTextField="InstructionType" DataValueField="InstructionTypeId">
                            </asp:DropDownList>
                            <asp:ObjectDataSource id="ObjectDataSourceInstructionType" runat="server"
                                TypeName="InstructionType"
                                SelectMethod="GetInstructionTypesByCode">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="InstructionTypeCode" DefaultValue="D" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanePeriod" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPeriod" runat="server">
                        <ContentTemplate>
                            <uc1:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>

