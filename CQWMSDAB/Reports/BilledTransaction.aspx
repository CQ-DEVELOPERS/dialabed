﻿<%@ Page Language="C#"
  AutoEventWireup="true"
  CodeFile="BilledTransaction.aspx.cs"
  MasterPageFile="~/MasterPages/MasterPage.master"
  Title="<%$ Resources:Default, ReportTitle %>"
  Inherits="Reports_BilledTransaction"
  StylesheetTheme="Default"
  Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
  <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
  <br />
  <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
  <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
  
  <telerik:RadGrid ID="grdBilledTransactions" runat="server" AutoGenerateColumns="False" CellSpacing="0" DataSourceID="billedTransactionDataSource" GridLines="None">
    
    <ClientSettings>
      <Selecting AllowRowSelect="True" />
    </ClientSettings>

    <MasterTableView DataSourceID="billedTransactionDataSource" DataKeyNames="Id">
      <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

      <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
      </RowIndicatorColumn>

      <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
      </ExpandCollapseColumn>

      <Columns>
        <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" Display="False" FilterControlAltText="Filter Id column" HeaderText="Id" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Principal" FilterControlAltText="Filter Principal column" HeaderText="Principal" SortExpression="Principal" UniqueName="Principal">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Warehouse" FilterControlAltText="Filter Warehouse column" HeaderText="Warehouse" SortExpression="Warehouse" UniqueName="Warehouse">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Status" FilterControlAltText="Filter Status column" HeaderText="Status" ReadOnly="True" SortExpression="Status" UniqueName="Status">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="BilledOn" DataType="System.DateTime" FilterControlAltText="Filter BilledOn column" HeaderText="Billed On" SortExpression="BilledOn" UniqueName="BilledOn">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="From" DataType="System.DateTime" FilterControlAltText="Filter From column" HeaderText="Period Start" SortExpression="From" UniqueName="From">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="To" DataType="System.DateTime" FilterControlAltText="Filter To column" HeaderText="Period End" SortExpression="To" UniqueName="To">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="LineCount" DataType="System.Int32" FilterControlAltText="Filter LineCount column" HeaderText="Line Count" ReadOnly="True" SortExpression="LineCount" UniqueName="LineCount">
        </telerik:GridBoundColumn>
      </Columns>

      <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
      </EditFormSettings>

      <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
    </MasterTableView>

    <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

    <FilterMenu EnableImageSprites="False"></FilterMenu>
  </telerik:RadGrid>
  <asp:ObjectDataSource runat="server" ID="billedTransactionDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetAll" TypeName="BilledTransaction" />
  <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" Style="height: 20px" />
</asp:Content>


