﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="WaybillExceptionReport.aspx.cs" Inherits="Reports_WaybillExceptionReport"
    Title="<%$ Resources:Default, ReportTitle %>" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ Register Src="../Common/CustomerSearch.ascx" TagName="CustomerSearch" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
        ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
        TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneOrderNumber" runat="server">
                <Header>
                    <a href="" class="accordionLink">Enter an Order Number</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelOrderNumber" runat="server" Text="Order Number:"></asp:Label>
                            <asp:TextBox ID="TextBoxOrderNumber" runat="server" Text=""></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneCustomerSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Customer</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelOrderNumber" runat="server">
                        <ContentTemplate>
                            <uc2:CustomerSearch ID="CustomerSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDocumentType" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Document Type</a></Header>
                <Content>
                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataTextField="OutboundDocumentType"
                        DataValueField="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentTypeId"
                        Width="150px" OnSelectedIndexChanged="DropDownListOutboundDocumentType_SelectedIndexChanged">
                    </asp:DropDownList>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanePeriod" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPeriod" runat="server">
                        <ContentTemplate>
                            <uc1:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
        OnClick="ButtonPrint_Click" />
    <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentTypeId" runat="server"
        SelectMethod="GetOutboundDocumentTypes" TypeName="OutboundDocumentType">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
