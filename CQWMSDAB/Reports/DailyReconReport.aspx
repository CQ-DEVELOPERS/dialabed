﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="DailyReconReport.aspx.cs"
    Inherits="Reports_DailyReconReport"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="ucprd" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="ucbtch" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a WarehouseCode</a>
                </Header>
                <Content>
                    <asp:TextBox ID="TextBoxWarehouseCode" runat="server"></asp:TextBox>
                    <%--<asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneProductSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Product / SKU</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelProductSearch" runat="server">
                        <ContentTemplate>
                            <ucprd:ProductSearch ID="ProductSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneBatch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Variance or All</a>
                </Header>
                <Content>
                    <asp:RadioButtonList ID="RadioButtonListVariance" runat="server" AutoPostBack="False" DataValueField="Sort"
                        Font-Size="Small">
                        <asp:ListItem Text="All" Value="A" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Variance" Value="V"></asp:ListItem>
                    </asp:RadioButtonList>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>

