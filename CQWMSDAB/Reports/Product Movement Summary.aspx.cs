using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_ProductMovementSummary : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ProductId"] = -1;
                Session["BatchId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/Product Movement Summary.aspx";

            Session["ReportName"] = "Product Movement Summary";

            ReportParameter[] RptParameters = new ReportParameter[6];

            // Create the ConnectionString report parameter
            RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());
            
            // Create the WarehouseId report parameter
            RptParameters[1] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());
            
            // Create the StorageUnitId report parameter
            string ProductId = TextBoxProductId.Text;
            
            if (ProductId == "")
                ProductId = "-1";

            RptParameters[2] = new ReportParameter("ProductId", ProductId);
            
            // Create the BatchId report parameter
            RptParameters[3] = new ReportParameter("BatchId", Session["BatchId"].ToString());
            
            // Create the FromDate report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
