<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewStockAdjustments.aspx.cs" Inherits="Reports_ViewStockAdjustments"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <%--<telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="Textbox" />--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadFilter1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridStockAdjustment">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridStockAdjustment" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridStockAdjustment" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
        <tr align="right">
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="ProductCode"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxProductCode" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="Product"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxProduct" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelBatch" runat="server" Text="Batch"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxBatch" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr align="right">
            <td>
                <asp:Label ID="LabelStatus" runat="server" Text="Status"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxStatus" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelMessage" runat="server" Text="Message"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxMessage" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelRecordType" runat="server" Text="RecordType"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxRecordType" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr align="right">
            <td>
                <asp:Label ID="LabelAdditional1" runat="server" Text="Additional1"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxAdditional1" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelAdditional2" runat="server" Text="Additional2"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxAdditional2" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelAdditional3" runat="server" Text="Additional3"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxAdditional3" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr align="right">
            <td>
                <asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>"></asp:Label>
                <telerik:RadDatePicker ID="rdpFromDate" runat="server" Width="200px"></telerik:RadDatePicker>
            </td>
            <td>
                <asp:Label ID="labeToDate" runat="server" Text="<%$ Resources:Default, ToDate %>"></asp:Label>
                <telerik:RadDatePicker ID="rdpToDate" runat="server" Width="200px"></telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="RadGridStockAdjustment" runat="server"
        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceStockAdjustment"
        PageSize="30" ShowGroupPanel="True" Skin="Metro"
        OnItemCommand="RadGridStockAdjustment_ItemCommand">
        <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
        <MasterTableView DataKeyNames="InterfaceId,InterfaceTableId,Status" DataSourceID="ObjectDataSourceStockAdjustment"
            AutoGenerateColumns="False" EnableHeaderContextMenu="true" AllowFilteringByColumn="true">
            <Columns>
                <telerik:GridBoundColumn DataField="Status" HeaderText="Status" SortExpression="Status" UniqueName="Status"></telerik:GridBoundColumn>
                <telerik:GridButtonColumn Text="Reprocess" HeaderText="Reprocess" CommandName="Reprocess"></telerik:GridButtonColumn>
                <telerik:GridBoundColumn DataField="InterfaceMessage" HeaderText="InterfaceMessage" SortExpression="InterfaceMessage" UniqueName="InterfaceMessage"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InterfaceMessageCode" HeaderText="InterfaceMessageCode" SortExpression="InterfaceMessageCode" UniqueName="InterfaceMessageCode"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InterfaceTableId" HeaderText="InterfaceTableId" SortExpression="InterfaceTableId" UniqueName="InterfaceTableId"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InterfaceId" HeaderText="InterfaceId" SortExpression="InterfaceId" UniqueName="InterfaceId"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RecordType" HeaderText="RecordType" SortExpression="RecordType" UniqueName="RecordType"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RecordStatus" HeaderText="RecordStatus" SortExpression="RecordStatus" UniqueName="RecordStatus"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ProcessedDate" HeaderText="ProcessedDate" SortExpression="ProcessedDate" UniqueName="ProcessedDate"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InsertDate" HeaderText="InsertDate" SortExpression="InsertDate" UniqueName="InsertDate"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" UniqueName="ProductCode"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Product" HeaderText="Product" SortExpression="Product" UniqueName="Product"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Batch" HeaderText="Batch" SortExpression="Batch" UniqueName="Batch"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" UniqueName="Quantity"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Additional1" HeaderText="Additional1" SortExpression="Additional1" UniqueName="Additional1"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Additional2" HeaderText="Additional2" SortExpression="Additional2" UniqueName="Additional2"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Additional3" HeaderText="Additional3" SortExpression="Additional3" UniqueName="Additional3"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Additional4" HeaderText="Additional4" SortExpression="Additional4" UniqueName="Additional4"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Additional5" HeaderText="Additional5" SortExpression="Additional5" UniqueName="Additional5"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
                <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                    <Resizing AllowColumnResize="true"></Resizing>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="True" />
    </telerik:RadGrid>
    <asp:ObjectDataSource ID="ObjectDataSourceStockAdjustment" runat="server" TypeName="InterfaceConsole"
        SelectMethod="SearchStockAdjustments">
        <SelectParameters>
            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            
            <asp:ControlParameter ControlID="RadTextBoxProductCode" Name="ProductCode" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxProduct" Name="Product" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxBatch" Name="Batch" PropertyName="Text" Type="String" />
            
            <asp:ControlParameter ControlID="RadTextBoxAdditional1" Name="Additional1" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxAdditional2" Name="Additional2" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxAdditional3" Name="Additional3" PropertyName="Text" Type="String" />
            
            <asp:ControlParameter ControlID="RadTextBoxStatus" Name="Status" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxMessage" Name="Message" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxRecordType" Name="RecordType" PropertyName="Text" Type="String" />
            
            <asp:ControlParameter ControlID="rdpFromDate" Name="FromDate" />
            <asp:ControlParameter ControlID="rdpToDate" Name="ToDate"  />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>