<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="CustomerReturnsDetail.aspx.cs"
    Inherits="Reports_CustomerReturnsDetail"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>
<%@ Register Src="../Common/CustomerSearch.ascx" TagName="CustomerSearch" TagPrefix="uccs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            
            <ajaxToolkit:AccordionPane ID="AccordionPaneCustomer" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Customer</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelCustomer" runat="server">
                        <ContentTemplate>
                            <uccs:CustomerSearch ID="CustomerSearch1" runat="server" />
                            
                            
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            
            <ajaxToolkit:AccordionPane ID="AccordionPaneStatus" runat="server">
                <Header>
                    <a href="" class="accordionLink">Status</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                        <asp:RadioButtonList ID="RadioButtonListStatus" runat="server">
                            <asp:ListItem Text="All" Value="All"></asp:ListItem>
                            <asp:ListItem Text="New" Value="New"></asp:ListItem>
                            <asp:ListItem Text="Waiting" Value="New"></asp:ListItem>
                        </asp:RadioButtonList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            
            <ajaxToolkit:AccordionPane ID="AccordionPaneRFCNumber" runat="server">
                <Header>
                    <a href="" class="accordionLink">RFCNumber</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Label runat="server" ID="label1" Text="RFC Number:"></asp:Label>
                            <asp:TextBox runat="server" ID="TextBoxRFCNumber"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            
            <ajaxToolkit:AccordionPane ID="AccordionPaneInvoiceNumber" runat="server">
                <Header>
                    <a href="" class="accordionLink">InvoiceNumber</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                        <asp:Label runat="server" ID="labelInvoice" Text="Invoice Number:"></asp:Label>
                        <asp:TextBox runat="server" ID="TextBoxInvoiceNumber"></asp:TextBox>                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>     
                 
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>

