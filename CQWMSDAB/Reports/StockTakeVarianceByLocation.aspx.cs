﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_StockTakeVarianceByLocation : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["AreaId"] = -1;
                Session["StockTakeReferenceId"] = -1;
            }
        }
        catch { }
    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        //try
        //{
        Session["FromURL"] = "~/Reports/StockTakeVarianceByLocation.aspx";

        Session["ReportName"] = "Stock Take Variances By Location";

        Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[9];


        RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

        RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

        RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

        RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

        // Create the ToDate report parameter
        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

        // Create the AreaId report parameter
        RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("AreaId", Session["AreaId"].ToString());

        // Create the StockTakeRef report parameter
        RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("StockTakeReferenceId", Session["StockTakeReferenceId"].ToString());

        RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

        RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("PrincipalId", DropDownListPrincipal.SelectedValue.ToString());


        Session["ReportParameters"] = RptParameters;

        Response.Redirect("~/Reports/Report.aspx");
        //}
        //   catch { }

    }

    protected void ButtonSet_Click(object sender, EventArgs e)
    {
        UpdatePanelDateRange.DataBind();
    }
}

