<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="StockOnHand.aspx.cs"
    Inherits="Reports_StockOnHand"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="ucprd" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="ucbtch" %>
<%@ Register Src="../Common/FromLocationSearch.ascx" TagName="FromLocationSearch" TagPrefix="ucfl" %>
<%@ Register Src="../Common/ToLocationSearch.ascx" TagName="ToLocationSearch" TagPrefix="uctl" %>
<%@ Register Src="../Common/LocationType.ascx" TagName="LocationType" TagPrefix="uclt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPanePrincipal" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Principal</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPrincipal" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                DataTextField="Principal" DataValueField="PrincipalId">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                SelectMethod="PrincipalSelect">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="userName" SessionField="UserName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneProductSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Product / SKU</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelProductSearch" runat="server">
                        <ContentTemplate>
                            <ucprd:ProductSearch ID="ProductSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneBatch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Batch</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelBatch" runat="server">
                        <ContentTemplate>
                            <ucbtch:BatchSearch ID="BatchSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneFromLocation" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a From Location</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelFromLocation" runat="server">
                        <ContentTemplate>
                            <ucfl:FromLocationSearch ID="FromLocationSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneToLocation" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a To Location</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelToLocation" runat="server">
                        <ContentTemplate>
                            <uctl:ToLocationSearch ID="ToLocationSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneLocationType" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Location Type</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelLocationType" runat="server">
                        <ContentTemplate>
                            <uclt:LocationType ID="LocationType" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanePerQuantity" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Location Per Quantity</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPerQuantity" runat="server">
                        <ContentTemplate>
                            <asp:Label runat="server" ID="lblBatchQty" Text="Batch Quantity:"></asp:Label>
                            <asp:RadioButton runat="server" ID="rbBatchMore" Text="More than" GroupName="Batch" />
                            <asp:RadioButton runat="server" ID="rbBatchLess" Text="Less than" GroupName="Batch" />
                            <asp:TextBox runat="server" ID="tbBatchQuantity" Text="0"></asp:TextBox>
                            <br />
                            <asp:Label runat="server" ID="lblAllocatedQty" Text="Allocated Quantity:"></asp:Label>
                            <asp:RadioButton runat="server" ID="rbAllocatedMore" Text="More than" GroupName="Allocated" />
                            <asp:RadioButton runat="server" ID="rbAllocatedLess" Text="Less than" GroupName="Allocated" />
                            <asp:TextBox runat="server" ID="tbAllocatedQuantity" Text="0"></asp:TextBox>
                            <br />
                            <asp:Label runat="server" ID="lblReservedQty" Text="Reserved Quantity:"></asp:Label>
                            <asp:RadioButton runat="server" ID="rbReservedMore" Text="More than" GroupName="Reserved" />
                            <asp:RadioButton runat="server" ID="rbReservedLess" Text="Less than" GroupName="Reserved" />
                            <asp:TextBox runat="server" ID="tbReservedQuantity" Text="0"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>

