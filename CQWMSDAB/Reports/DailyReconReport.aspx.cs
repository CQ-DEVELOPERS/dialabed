﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_DailyReconReport : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["StorageUnitId"] = -1;
                Session["BatchId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            string VarianceOnly = RadioButtonListVariance.SelectedValue;

            Session["WarehouseCode"] = TextBoxWarehouseCode.Text;

            Session["FromURL"] = "~/Reports/DailyReconReport.aspx";

            Session["ReportName"] = "Daily Recon Report";

            ReportParameter[] RptParameters = new ReportParameter[7];

            RptParameters[0] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            // Create the WarehouseId report parameter
            RptParameters[1] = new ReportParameter("WarehouseCode", Session["WarehouseCode"].ToString());

            // Create the StorageUnitId report parameter
            RptParameters[2] = new ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());

            RptParameters[3] = new ReportParameter("VarianceOnly", VarianceOnly.ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}