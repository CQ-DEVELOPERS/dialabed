<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockOnHandByPrice.aspx.cs" Inherits="Reports_StockOnHandByPrice" Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="ucprd" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="ucbtch" %>
<%@ Register Src="../Common/FromLocationSearch.ascx" TagName="FromLocationSearch"
    TagPrefix="ucfl" %>
<%@ Register Src="../Common/ToLocationSearch.ascx" TagName="ToLocationSearch" TagPrefix="uctl" %>
<%@ Register Src="../Common/LocationType.ascx" TagName="LocationType" TagPrefix="uclt" %>
<%@ Register Src="../Common/DivisionSearch.ascx" TagName="DivisionSearch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
        ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
        TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDivision" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Division</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDivision" runat="server">
                        <ContentTemplate>
                            <asp:CheckBoxList ID="CheckBoxDivision" runat="server" DataSourceID="ObjectDataSourceDivision"
                                DataTextField="Division" DataValueField="DivisionId" RepeatColumns="10">
                            </asp:CheckBoxList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectDivision" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectDivision" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Button ID="ButtonSelectDivision" runat="server" Text="Select All" OnClick="ButtonSelectDivision_Click" />
                    <asp:Button ID="ButtonDeselectDivision" runat="server" Text="Deselect All" OnClick="ButtonDeselectDivision_Click" />
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneBatch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Batch</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelBatch" runat="server">
                        <ContentTemplate>
                            <ucbtch:BatchSearch ID="BatchSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneFromLocation" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a From Location</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelFromLocation" runat="server">
                        <ContentTemplate>
                            <ucfl:FromLocationSearch ID="FromLocationSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneToLocation" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a To Location</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelToLocation" runat="server">
                        <ContentTemplate>
                            <uctl:ToLocationSearch ID="ToLocationSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneLocationType" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Location Type</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelLocationType" runat="server">
                        <ContentTemplate>
                            <uclt:LocationType ID="LocationType" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanePerQuantity" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Location Per Quantity</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPerQuantity" runat="server">
                        <ContentTemplate>
                            <asp:Label runat="server" ID="lblBatchQty" Text="Batch Quantity:"></asp:Label>
                            <asp:RadioButton runat="server" ID="rbBatchMore" Text="More than" GroupName="Batch" />
                            <asp:RadioButton runat="server" ID="rbBatchLess" Text="Less than" GroupName="Batch" />
                            <asp:TextBox runat="server" ID="tbBatchQuantity" Text="0"></asp:TextBox>
                            <br />
                            <asp:Label runat="server" ID="lblAllocatedQty" Text="Allocated Quantity:"></asp:Label>
                            <asp:RadioButton runat="server" ID="rbAllocatedMore" Text="More than" GroupName="Allocated" />
                            <asp:RadioButton runat="server" ID="rbAllocatedLess" Text="Less than" GroupName="Allocated" />
                            <asp:TextBox runat="server" ID="tbAllocatedQuantity" Text="0"></asp:TextBox>
                            <br />
                            <asp:Label runat="server" ID="lblReservedQty" Text="Reserved Quantity:"></asp:Label>
                            <asp:RadioButton runat="server" ID="rbReservedMore" Text="More than" GroupName="Reserved" />
                            <asp:RadioButton runat="server" ID="rbReservedLess" Text="Less than" GroupName="Reserved" />
                            <asp:TextBox runat="server" ID="tbReservedQuantity" Text="0"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
        OnClick="ButtonPrint_Click" />
    <asp:ObjectDataSource ID="ObjectDataSourceDivision" runat="server" TypeName="Division"
        SelectMethod="GetDivisions">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
