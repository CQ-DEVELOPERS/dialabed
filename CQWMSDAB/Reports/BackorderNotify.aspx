<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="BackorderNotify.aspx.cs" Inherits="Reports_BackorderSummary" Title="<%$ Resources:Default, ReportTitle %>" Theme="Default"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="uc1" %>
<%@ Register Src="../Common/CustomerSearch.ascx" TagName="CustomerSearch" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneOrderNumber" runat="server">
                <Header>
                    <a href="" class="accordionLink">Search for a Product</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc1:ProductSearch ID="ProductSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneCustomerSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Search for a Customer</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelOrderNumber" runat="server">
                        <ContentTemplate>
                            <uc2:CustomerSearch ID="CustomerSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanelDocumentType" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Document Type</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDocumentType" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, OutboundDocumentType %>"></asp:Label>
                            <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataTextField="OutboundDocumentType"
                                DataValueField="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentTypeId" Width="150px">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentTypeId" runat="server" SelectMethod="GetOutboundDocumentTypes"
                                TypeName="OutboundDocumentType">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click"/>
    
<%--<asp:ObjectDataSource runat="server" SelectMethod="SelectOperatorExternalCompanies" ID="ExternalCompanies" TypeName="ExternalCompany">
    <SelectParameters>
    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    <asp:SessionParameter Name="OperatorID" SessionField="OperatorGroupId" Type="Int16" />
    
    </SelectParameters>
</asp:ObjectDataSource>
--%>    
</asp:Content>

