﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="InboundSLA.aspx.cs"
    Inherits="Reports_InboundSLA"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanePrincipal" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Principal</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPrincipal" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                DataTextField="Principal" DataValueField="PrincipalId">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                SelectMethod="GetPrincipalParameter">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
   
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:UpdatePanel ID="UpdatePanelMeasure" runat="server">
        <ContentTemplate>
            <table style="border: 2px 2px" border="1">
                <tr>
                    <td>Specify Data to measure Receive SLA from
                    </td>
                    <td>  </td>
                    <td>Specify Weekend Type
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonListMeasure" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <%--<asp:ListItem Text="Trailer Check-in DateTime" Value="1" Selected="True"></asp:ListItem>--%>
                            <asp:ListItem Text="First Receipt Scan DateTime" Value="2" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>  </td>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonWeekend" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="Friday" Value="1" ></asp:ListItem>
                            <asp:ListItem Text="Friday and Saturday" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Saturday Only" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Saturday and Sunday" Value="4" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Sunday Only" Value="5"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>


                <tr>
                    <td>Select SLA hours threshold for each task
                    </td>
                </tr>
                <tr>
                    <td>To Lines Received
                    </td>
                    <td></td>
                    <td>To Put-away
                    </td>
                    <td></td>
                    <td>To ASN/Receipt Close
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonToLineReceived" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="24 hours" Value="24" ></asp:ListItem>
                            <asp:ListItem Text="36 hours" Value="36" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="48 hours" Value="48"></asp:ListItem>
                            <asp:ListItem Text="72 hours" Value="72" ></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>  </td>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonToPutAway" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="24 hours" Value="24" ></asp:ListItem>
                            <asp:ListItem Text="36 hours" Value="36" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="48 hours" Value="48"></asp:ListItem>
                            <asp:ListItem Text="72 hours" Value="72"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>  </td>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonToASNReceiptClose" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="24 hours" Value="24" ></asp:ListItem>
                            <asp:ListItem Text="36 hours" Value="36" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="48 hours" Value="48"></asp:ListItem>
                            <asp:ListItem Text="72 hours" Value="72"></asp:ListItem>
                        </asp:RadioButtonList>
                        </td>
                </tr>
                <tr>
                    <td>Select Summary or Detail Report
                    </td>
                                   </tr>
                <tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonListSummaryDetail" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="Summary" Value="S" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Detail" Value="D"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                   
                   
                </tr>
                <%--<tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonToPutAway" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="24 hours" Value="1" ></asp:ListItem>
                            <asp:ListItem Text="36 hours" Value="2" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="48 hours" Value="3"></asp:ListItem>
                            <asp:ListItem Text="72 hours" Value="4"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr--%>

                <%--<tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonToASNReceiptClose" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="24 hours" Value="1" ></asp:ListItem>
                            <asp:ListItem Text="36 hours" Value="2" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="48 hours" Value="3"></asp:ListItem>
                            <asp:ListItem Text="72 hours" Value="4"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>--%>
            </table>
        </ContentTemplate>
       
    </asp:UpdatePanel>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>


