﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_ReceivingSummaryReport : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["OrderNumber"] = -1;
                Session["Batch"] = -1;
                Session["StorageUnitId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/ReceivingSummaryReport.aspx";

            Session["ReportName"] = "Receiving Summary Report";

            ReportParameter[] RptParameters = new ReportParameter[12];

            // Create the Sort report parameter
            string Received = RadioButtonListReceived.SelectedValue;

            // Create the OrderNumber report parameter
            string orderNumber = TextBoxOrderNumber.Text;

            if (orderNumber == "")
                orderNumber = "-1";

            RptParameters[0] = new ReportParameter("OrderNumber", orderNumber);

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            // Create the StorageUnitId report parameter
            RptParameters[2] = new ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());

            // Create the Batch report parameter
            RptParameters[3] = new ReportParameter("Batch", Session["Batch"].ToString());

            // Create the FromDate report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            RptParameters[9] = new ReportParameter("Received", Received.ToString());

            RptParameters[10] = new ReportParameter("PrincipalId", DropDownListPrincipal.SelectedValue.ToString());

            RptParameters[11] = new Microsoft.Reporting.WebForms.ReportParameter("OperatorId", Session["OperatorId"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch (Exception ex)
        {

        }
    }
}