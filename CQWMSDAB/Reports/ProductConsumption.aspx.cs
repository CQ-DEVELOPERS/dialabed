﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_ProductConsumption : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ProductCode"] = -1;
                Session["ReportWarehouseId"] = -1;
                Session["Sort"] = -1;
            }
            if (DropDownListWarehouse.SelectedItem.Text == "{All}")
            {
                Session["ReportWarehouseId"] = -1;
            }
        }
        catch { }
    }
    #region DropDownListWarehouse_SelectedIndexChanged
    protected void DropDownListWarehouse_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            Session["ReportWarehouseId"] = int.Parse(ddl.SelectedValue);
        }
        catch { }
    }
    #endregion "DropDownListWarehouse_SelectedIndexChanged"

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/ProductConsumption.aspx";

            Session["ReportName"] = "Product Consumption Report";

            ReportParameter[] RptParameters = new ReportParameter[6];
            

            // Create the Product Code report parameter
            string productCode = TextBoxProductCode.Text;

            if (productCode == "")
                productCode = "-1";

            // Create the Sort report parameter
            string Sort = RadioButtonList1.SelectedValue;
            
            RptParameters[0] = new ReportParameter("ProductCode", productCode);

            // Create the FromDate report parameter
            //RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            //RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            // Create the WarehouseId report parameter
            RptParameters[1] = new ReportParameter("WarehouseId", Session["ReportWarehouseId"].ToString());

            RptParameters[2] = new ReportParameter("Sort", Sort.ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch (Exception ex)
        {

        }
    }
}
