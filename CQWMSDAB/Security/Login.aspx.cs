using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Security_Login : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        Login1.Focus();

        if (!Page.IsPostBack)
        {

            if (Session["LogonErrorMessage"] != null)
            {
                LabelUserNotSetup.Visible = true;
                LabelUserNotSetup.Text = Session["LogonErrorMessage"].ToString();
            }
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (Request.QueryString["UserName"] != null && Request.QueryString["Password"] != null)
        {
            Membership.ValidateUser(Request.QueryString["UserName"], Request.QueryString["Password"]);
            {
                FormsAuthentication.RedirectFromLoginPage(Request.QueryString["UserName"], true);

                HttpCookie authCookie = FormsAuthentication.GetAuthCookie(Request.QueryString["UserName"], false);
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration, ticket.IsPersistent, "");
                authCookie.Value = FormsAuthentication.Encrypt(newTicket);
                Response.Cookies.Add(authCookie);

                Login1.UserName = Request.QueryString["UserName"];
                Login1_OnLoggedIn(sender, e);

                string redirUrl = "";

                if (Request.QueryString["ReturnURL"] != null)
                {
                    redirUrl = Request.RawUrl.Substring(Request.RawUrl.LastIndexOf("ReturnUrl") + 10);
                    Response.Redirect(redirUrl);
                }
                else
                {
                    redirUrl = FormsAuthentication.GetRedirectUrl(Request.QueryString["UserName"], false);
                    Response.Redirect(redirUrl);
                }
            }
        }
    }

    #region Login1_OnLoggedIn
    protected void Login1_OnLoggedIn(object sender, EventArgs e)
    {
        try
        {
            Session["UserName"] = Login1.UserName.ToString();

            LogonCredentials lc = new LogonCredentials();

            lc.GetOperatorsDatabase(Session["UserName"].ToString());

            Session["ServerName"] = lc.serverName;
            Session["DatabaseName"] = lc.databaseName;
            Session["ConnectionStringName"] = lc.databaseName;

            if (Session["ConnectionStringName"] == null)
                return;

            Session["ReportConnectionString"] = System.Configuration.ConfigurationManager.AppSettings.Get("ReportConnectionString").ToString() + Session["ConnectionStringName"].ToString();

            if (lc.GetLogonCredentials(Session["ConnectionStringName"].ToString(), Session["UserName"].ToString()))
            {
                Session["WarehouseId"] = lc.warehouseId;

                if ((int)Session["WarehouseId"] == -1)
                {
                    LabelUserNotSetup.Visible = true;
                    return;
                }
                Session["LogonErrorMessage"] = "";

                Session["OperatorGroupId"] = lc.operatorGroupId;
                Session["OperatorId"] = lc.operatorId;
                Session["CultureName"] = lc.cultureName;
                Session["Printer"] = lc.printer;
                Session["Port"] = lc.port;
                Session["IPAddress"] = lc.ipAddress;
                Session["OperatorGroupCode"] = lc.operatorGroupCode;
                Session["OperatorMenuId"] = lc.menuId;

                if (lc.CheckConcurrentUsers(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], "Desktop"))
                {
                    lc.UserLoggedIn(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], "Desktop");
                }
                else
                {
                    Session["LogonErrorMessage"] = Resources.Default.MaximumConcurrentUsers;

                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                }

                if (lc.GetVersion(Session["ConnectionStringName"].ToString()))
                {
                    Session["Version"] = lc.Version;
                }
            }
            else
            {

                Session["LogonErrorMessage"] = Resources.Default.ActiveLogin;

                FormsAuthentication.SignOut();

                FormsAuthentication.RedirectToLoginPage();
            }

        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;

            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
    #endregion Login1_OnLoggedIn
}
