using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Security_PrintExternalLabel : System.Web.UI.Page
{
    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Context.Session.IsNewSession)
            {
                if (Request.QueryString["UserName"] != null && Request.QueryString["Password"] != null)
                {
                    Response.Redirect("Login.aspx?UserName=" + Request.QueryString["UserName"]
                        + "&Password=" + Request.QueryString["Password"]
                        + "&ReturnUrl=" + Request.Url);
                }
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

            ArrayList checkedList = new ArrayList();

            Session["checkedList"] = checkedList;

            if (Request.QueryString["LabelName"] != null)
            {
                Session["LabelName"] = Request.QueryString["LabelName"];
            }

            if (Request.QueryString["LabelCopies"] != null)
            {
                try { Session["LabelCopies"] = int.Parse(Request.QueryString["LabelCopies"]); }
                catch { Session["LabelCopies"] = 1; }
            }

            if (Request.QueryString["Key"] != null)
            {
                try
                {
                    checkedList.Add(int.Parse(Request.QueryString["Key"].ToString()));
                }
                catch
                {
                    if (checkedList.Count < 1)
                        return;
                }

                Session["FromURL"] = "~/Security/PrintExternalLabel.aspx";

                if (Session["Printer"] == null)
                    Session["Printer"] = "";

                var sessionPort = Session["Port"];
                var sessionPrinter = Session["Printer"];
                var sesionIndicatorList = Session["indicatorList"];
                var sessionCheckedList = Session["checkedList"];
                var sessionLocationList = Session["locationList"];
                var sessionConnectionString = Session["ConnectionStringName"];
                var sessionTitle = Session["Title"];
                var sessionBarcode = Session["Barcode"];
                var sessionWarehouseId = Session["WarehouseId"];
                var sessionProductList = Session["ProductList"];
                var sessionTakeOnLabel = Session["TakeOnLabel"];
                var sessionLabelCopies = Session["LabelCopies"];

                NLWAXPrint nl = new NLWAXPrint();
                nl.onSessionUpdate += Nl_onSessionUpdate;
                string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                          Session["LabelName"].ToString(),
                                                          sessionPrinter,
                                                          sessionPort,
                                                          sesionIndicatorList,
                                                          sessionCheckedList,
                                                          sessionLocationList,
                                                          sessionConnectionString,
                                                          sessionTitle,
                                                          sessionBarcode,
                                                          sessionWarehouseId,
                                                          sessionProductList,
                                                          sessionTakeOnLabel,
                                                          sessionLabelCopies);
                if (printResponse == string.Empty)
                {
                    Master.ErrorText = "An error has occurred! For more details please check automation manager.";
                }
                else
                {
                    Master.MsgText = "Label has been printed successfuly.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PrintExternalLabel " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
    }

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "PrintExternalLabel", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "PrintExternalLabel", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
