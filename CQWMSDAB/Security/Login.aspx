<%@ Page
    Language="C#"
    AutoEventWireup="true"
    CodeFile="Login.aspx.cs"
    Inherits="Security_Login"
    StylesheetTheme="Default"
    Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Login to CQuential Solutions</title>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <asp:Image ID="ImageLogo" runat="server" ImageUrl="~/Security/CQ Logo.png" />
            <asp:Login ID="Login1" runat="server" DisplayRememberMe="False" PasswordRecoveryUrl="~/Security/ForgotPassword.aspx" PasswordRecoveryText="Forgot your password?" FailureText="Your login attempt was not <br />successful. Please try again." OnLoggedIn="Login1_OnLoggedIn">
            </asp:Login>
            <asp:Label ID="LabelUserNotSetup" runat="server" Text="<%$ Resources:Default, LabelUserNotSetup %>" EnableTheming="false" ForeColor="red" Visible="false"></asp:Label>
        </div>
    </form>
</body>
</html>
