﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class StaticInfo_DownloadFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DonwloadFile();
    }
    protected void btn1_Click(object sender, EventArgs e)
    {
        DonwloadFile();
    }

    protected void DonwloadFile()
    {
        Intergration ig = new Intergration();
        string filePrefix = "";
        string fileExtension = "";
        DataSet ds = new DataSet();
        
        ds = ig.SampleDownload(Session["ConnectionStringName"].ToString(), (int)Session["InterfaceMappingFileId"]);

        DataTable dt = ds.Tables[0];
        
        String str = "";

        foreach (DataRow dr in dt.Rows)
        {
            foreach (Object item in dr.ItemArray)
            {
                str = str + item.ToString();
            }
            str = str + Environment.NewLine;
        }

        //Read the Results of file into a byte array.
        Byte[] fileBytes = System.Text.Encoding.Unicode.GetBytes(str);

        filePrefix = ig.GetInterfaceFilePrefix(Session["ConnectionStringName"].ToString(), (int)Session["InterfaceMappingFileId"]);
        fileExtension = ig.GetInterfaceFileExtension(Session["ConnectionStringName"].ToString(), (int)Session["InterfaceMappingFileId"]);

        //Clear the response               
        Response.Clear();
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Cookies.Clear();
        //Add the header & other information      
        Response.Cache.SetCacheability(HttpCacheability.Private);
        Response.CacheControl = "private";
        Response.Charset = System.Text.UTF8Encoding.UTF8.WebName;
        Response.ContentEncoding = System.Text.UTF8Encoding.UTF8;
        Response.AppendHeader("Content-Length", fileBytes.Length.ToString());
        Response.AppendHeader("Pragma", "cache");
        Response.AppendHeader("Expires", "60");
        Response.AppendHeader("Content-Disposition",
        "attachment; " +
        "filename=\"" + filePrefix + "-SampleDownload(" + Session["InterfaceMappingFileId"].ToString() + ")." + fileExtension + "\"; " +
        "size=" + fileBytes.Length.ToString() + "; " +
        "creation-date=" + DateTime.Now.ToString("R") + "; " +
        "modification-date=" + DateTime.Now.ToString("R") + "; " +
        "read-date=" + DateTime.Now.ToString("R"));
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //Write it back to the client    
        Response.BinaryWrite(fileBytes);
        Response.End();
    }
}