using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Microsoft.Reporting.WebForms;

public partial class StaticInfo_PrincipalWizard : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "";
                Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ContainerPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            switch (RadTabSContainer1.SelectedIndex)
            {
                case 0:
                    //ContactListGridView.BindData();
                    break;
                case 1:
                    //AddressGridView.BindData();
                    break;
                default:
                    break;
            }
        }

        catch { }
    }
    #endregion Page_LoadComplete

    //#region ButtonDocumentSearch_Click
    //protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    //{
    //    theErrMethod = "ButtonDocumentSearch_Click";

    //    try
    //    {
    //        RadGridWave.DataBind();

    //        Master.MsgText = ""; Master.ErrorText = "";
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("ContainerPlanning" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }

    //}
    //#endregion "ButtonDocumentSearch_Click"

    //#region RadGridWave_SelectedIndexChanged
    //protected void RadGridWave_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    theErrMethod = "RadGridWave_SelectedIndexChanged";

    //    try
    //    {
    //        Session["IssueId"] = -1;

    //        foreach (GridDataItem item in RadGridWave.SelectedItems)
    //        {
    //            Session["TableId"] = item.GetDataKeyValue("IssueId");
                
    //            Session["TableName"] = "Issue";
    //        }

    //        RadGridComments.DataBind();

    //        Master.MsgText = "Shipment Selected"; Master.ErrorText = "";
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("ContainerPlanning" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion "RadGridWave_SelectedIndexChanged"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "ContainerPlanning", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "ContainerPlanning", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

    //#region Render
    //protected override void Render(HtmlTextWriter writer)
    //{
    //    try
    //    {
    //        base.Render(writer);
    //        SavePetNames();
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion Render

    //#region SavePetNames
    //protected void SavePetNames()
    //{
    //    try
    //    {
    //        {
    //            GridSettingsPersister settings = new GridSettingsPersister(RadGridWave);
    //            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridWave, "OrderQueryRadGridWave", (int)Session["OperatorId"]);
    //        }
    //        {
    //            GridSettingsPersister settings = new GridSettingsPersister(RadGridUnreadCommunication);
    //            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridUnreadCommunication, "OrderQueryRadGridUnreadCommunication", (int)Session["OperatorId"]);
    //        }

    //        {
    //            GridSettingsPersister settings = new GridSettingsPersister(RadGridComments);
    //            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridComments, "OrderQueryRadGridComments", (int)Session["OperatorId"]);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion SavePetNames

    //#region ButtonSaveSettings_Click
    //protected void ButtonSaveSettings_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        SavePetNames();
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion ButtonSaveSettings_Click

    //#region Page_Init
    //protected void Page_Init(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        Session["countLoopsToPreventInfinLoop"] = 0;

    //        if (!Page.IsPostBack)
    //        {
    //            {
    //                GridSettingsPersister settings = new GridSettingsPersister(RadGridWave);
    //                settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridWave, "OrderQueryRadGridWave", (int)Session["OperatorId"]);
    //            }
    //            {
    //                GridSettingsPersister settings = new GridSettingsPersister(RadGridUnreadCommunication);
    //                settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridUnreadCommunication, "OrderQueryRadGridUnreadCommunication", (int)Session["OperatorId"]);
    //            }

    //            {
    //                GridSettingsPersister settings = new GridSettingsPersister(RadGridComments);
    //                settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridComments, "OrderQueryRadGridComments", (int)Session["OperatorId"]);
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("PickingProgress" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion Page_Init

    protected void RadButtonArea_Click(object sender, EventArgs e)
    {
        Session["FromURL"] = "~/StaticInfo/PrincipalWizard.aspx";
        Response.Redirect("~/StaticInfo/LocationMaintenance.aspx");
    }
    protected void RadButtonStockOnHand_Click(object sender, EventArgs e)
    {
        Session["FromURL"] = "~/StaticInfo/PrincipalWizard.aspx";
        Response.Redirect("~/Reports/StockOnHand.aspx");
    }
    protected void RadButtonProducts_Click(object sender, EventArgs e)
    {
        Session["FromURL"] = "~/StaticInfo/PrincipalWizard.aspx";
        Response.Redirect("~/Housekeeping/ProductMaintenance.aspx");
    }
    protected void RadButtonInegration_Click(object sender, EventArgs e)
    {
        Session["FromURL"] = "~/StaticInfo/PrincipalWizard.aspx";
        Response.Redirect("~/StaticInfo/IntegrationWizard.aspx");
    }
    protected void RadButtonInterface_Click(object sender, EventArgs e)
    {
        Session["FromURL"] = "~/StaticInfo/PrincipalWizard.aspx";
        Response.Redirect("~/StaticInfo/InterfaceWizard.aspx");
    }
    protected void RadButtonBilling_Click(object sender, EventArgs e)
    {
        Session["FromURL"] = "~/StaticInfo/PrincipalWizard.aspx";
        Response.Redirect("~/Billing/BillMaintenance.aspx");
    }
    protected void RadButtonLocationMaster_Click(object sender, EventArgs e)
    {
        Session["FromURL"] = "~/StaticInfo/PrincipalWizard.aspx";
        Response.Redirect("~/Housekeeping/LocationMasterUpload.aspx");
    }
    protected void RadButtonProductMaster_Click(object sender, EventArgs e)
    {
        Session["FromURL"] = "~/StaticInfo/PrincipalWizard.aspx";
        Response.Redirect("~/Housekeeping/ProductMasterUpload.aspx");
    }
}


