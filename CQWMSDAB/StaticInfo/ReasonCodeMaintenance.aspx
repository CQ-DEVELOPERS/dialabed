﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReasonCodeMaintenance.aspx.cs" Inherits="StaticInfo_ReasonCodeMaintenance"
    Title="" StylesheetTheme="Default" Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="ucProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text='<%$ Resources:Default,MaintainReason %>'></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel ID="GroupUpdate" runat="server">
        <ContentTemplate>
            <asp:Button ID="ButtonAddReason" runat="server" Text="<%$ Resources:Default, AddReason%>"
                OnClick="ButtonAddReason_Click" />
            <asp:UpdatePanel ID="updatePanel_Reason" runat="server" EnableViewState="False">
                <ContentTemplate>
                    <br />
                    <asp:GridView ID="GridViewReasons" DataSourceID="ObjectDataSourceReasons" runat="server"
                        DataKeyNames="ReasonId" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                        EmptyDataText="There are currently no Reasons..." OnSelectedIndexChanged="GridViewReasons_SelectedIndexChanged">
                        <Columns>
                            <asp:CommandField ShowEditButton="True" />
                            <asp:BoundField DataField="reasonCode" HeaderText='<%$ Resources:Default,ReasonCode %>'
                                ReadOnly="True" />
                            <asp:BoundField DataField="reason" HeaderText='<%$ Resources:Default,Reason %>' ReadOnly="False" />
                        </Columns>
                    </asp:GridView>
                    <br />
                    <asp:FormView ID="FormViewReason" runat="server" DataSourceID="ObjectDataSourceReasons"
                        Visible="False" DefaultMode="Insert" OnPageIndexChanging="FormViewReason_PageIndexChanging"
                        DataKeyNames="ReasonCodeId">
                        <InsertItemTemplate>
                            <br />
                            <p>
                                <b>Add a New Reason</b></p>
                            <table style="font-size: medium;">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelReasonCode" Text='<%$ Resources:Default,ReasonCode %>' runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxReasonCode" Text='<%# Bind("ReasonCode") %>' runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelReason" Text='<%$ Resources:Default,Reason %>' runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxReason" Text='<%# Bind("Reason") %>' runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="ButtonInsert" CommandName="Insert" Text="Insert" runat="server" />
                                        <asp:Button ID="ButtonCancel" CommandName="Cancel" Text="Cancel" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                    </asp:FormView>
                    <asp:ObjectDataSource ID="ObjectDataSourceReasons" runat="server" TypeName="StaticInfoReason"
                        SelectMethod="ListReasonDetails" InsertMethod="InsertReason" UpdateMethod="UpdateReason">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                Type="String" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                Type="String" />
                            <asp:SessionParameter Name="reason" SessionField="Reason" Type="String" />
                            <asp:SessionParameter Name="reasonCode" SessionField="ReasonCode" Type="String" />
                        </UpdateParameters>
                        <InsertParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                Type="String" />
                            <asp:SessionParameter Name="reasonId" SessionField="ReasonId" Type="Int32" />
                            <asp:SessionParameter Name="reason" SessionField="Reason" Type="String" />
                            <asp:SessionParameter Name="reasonCode" SessionField="ReasonCode" Type="String" />
                        </InsertParameters>
                    </asp:ObjectDataSource>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
