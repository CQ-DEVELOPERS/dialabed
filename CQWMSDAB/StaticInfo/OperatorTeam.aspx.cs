﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

public partial class StaticInfo_OperatorTeam : System.Web.UI.Page
{
   #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewTeam_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewTeamGroupLoc_SelectedIndexChanged";
        try
        {
            Session["TeamId"] = GridViewTeam.SelectedDataKey["TeamId"];

        }
        catch (Exception ex)
        {
            result = SendErrorNow("OperatorTeamMaintenance" + "_" + ex.Message.ToString());
        }
    }

    
    protected void ObjectDataSourceTeam_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertOperatorTeam.Visible = false;
        GridViewTeam.Visible = true;
        
        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_Team, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceTeam_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string Team = ((TextBox)DetailsViewInsertOperatorTeam.FindControl("DetailTextTeam")).Text;
        //string RestrictedOperatorIndicator = ((TextBox)DetailsViewInsertAreaTeam.FindControl("DetailTextRestrictedAreaIndicator")).Text;

        e.InputParameters["Team"] = Team;
        //e.InputParameters["RestrictedAreaIndicator"] = RestrictedAreaIndicator;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertOperatorTeam.Visible = false;
        GridViewTeam.Visible = true;
    }

    protected void DetailsViewInsertOperatorTeam_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

    #region ButtonSelectOperator_Click
    protected void ButtonSelectOperator_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectOperator_Click";
        try
        {
            foreach (ListItem item in CheckBoxListOperator.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectOperator_Click

    #region ButtonSelectOperatorUnsel_Click
    protected void ButtonSelectOperatorUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectOperatorUnsel_Click";
        try
        {
            foreach (ListItem item in CheckBoxListOperatorUnsel.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectOperatorUnsel_Click

    #region ButtonDeselectOperator_Click
    protected void ButtonDeselectOperator_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectOperator_Click";

        try
        {
            foreach (ListItem item in CheckBoxListOperator.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectOperatorUnsel_Click

    #region ButtonDeselectOperatorUnsel_Click
    protected void ButtonDeselectOperatorUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectOperatorUnsel_Click";

        try
        {
            foreach (ListItem item in CheckBoxListOperatorUnsel.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectOperatorUnsel_Click

    #region ButtonLinkOperator_Click

    protected void ButtonLinkOperator_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectOperator_Click";
        try
        {
            List<int> OperatorList = new List<int>();

            foreach (ListItem item in CheckBoxListOperatorUnsel.Items)
            {
                if (item.Selected == true)
                {
                    OperatorList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int Operator in OperatorList)
            {
                Shifts.OperatorTeam_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["TeamId"]), Operator);
            }

            CheckBoxListOperatorUnsel.DataBind();
            CheckBoxListOperator.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OperatorTeam" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkOperator_Click

    #region ButtonLinkOperatorUnsel_Click

    protected void ButtonLinkOperatorUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectOperator_Click";
        try
        {
            List<int> OperatorList = new List<int>();

            foreach (ListItem item in CheckBoxListOperator.Items)
            {
                if (item.Selected == true)
                {
                    OperatorList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int Operator in OperatorList)
            {
                Shifts.OperatorTeam_Delete(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["TeamId"]), Operator);
            }

            CheckBoxListOperatorUnsel.DataBind();
            CheckBoxListOperator.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OperatorTeam" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkOperatorUnsel_Click
}
