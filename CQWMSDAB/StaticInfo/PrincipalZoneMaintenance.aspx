﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PrincipalZoneMaintenance.aspx.cs" Inherits="StaticInfo_PrincipalZoneMaintenance"
    Title="<%$ Resources:Default, AreaOperatorGroupTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, AreaOperatorGroupTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanelPrincipalZone" HeaderText="Principal Area Link">
            <HeaderTemplate>
                Area Principal
            </HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelPrincipalZone" runat="server">
                    <ContentTemplate>
                        <table border="solid" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Principal" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="Linked Areas" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text=" " Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Unlinked Areas" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="updatePanel_Principal" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                                            <asp:GridView ID="GridViewPrincipal" DataSourceID="ObjectDataSourcePrincipal"
                                                DataKeyNames="PrincipalId" runat="server" AllowPaging="True" AllowSorting="True"
                                                PageSize="15" AutoGenerateColumns="False" EmptyDataText="There are currently no Principals..."
                                                OnSelectedIndexChanged="GridViewPrincipal_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" ShowEditButton="False" ShowDeleteButton="False"
                                                        ShowCancelButton="False" />
                                                    <asp:BoundField HeaderText="PrincipalId" DataField="PrincipalId" SortExpression="PrincipalId"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="PrincipalCode" DataField="PrincipalCode" SortExpression="PrincipalCode"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="Principal" DataField="Principal" SortExpression="Principal" />
                                                    <asp:BoundField HeaderText="RestrictedAreaIndicator" DataField="RestrictedAreaIndicator"
                                                        SortExpression="RestrictedAreaIndicator" Visible="False" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:DetailsView Visible="false" ID="DetailsViewInsertAreaPrincipal" runat="server"
                                                AutoGenerateRows="False" DataSourceID="ObjectDataSourcePrincipal" OnLoad="DetailsViewInsertAreaPrincipal_Load">
                                                <Fields>
                                                    <asp:TemplateField HeaderText="PrincipalId" InsertVisible="False" SortExpression="PrincipalId">
                                                        <InsertItemTemplate>
                                                            <asp:Button ID="AddPrincipal" runat="server" CommandName="Insert" Text="Add" />
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PrincipalCode" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextPrincipalCode" runat="server" Text='<%# Bind("PrincipalCode") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="RestrictedAreaIndicator" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextRestrictedAreaIndicator" runat="server" Text='<%# Bind("RestrictedAreaIndicator") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                </Fields>
                                            </asp:DetailsView>
                                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                                SelectMethod="ListPrincipals">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="Area" SelectMethod="GetAreasPrincipalSel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="principalId" SessionField="PrincipalId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceAreaUnsel" runat="server" TypeName="Area"
                                                SelectMethod="GetAreaPrincipalUnsel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="principalId" SessionField="PrincipalId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListArea" runat="server" DataSourceID="ObjectDataSourceArea"
                                                DataTextField="Area" DataValueField="AreaId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="LabelErrorMsg" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectArea" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectArea" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectArea" runat="server" Text="Select All" OnClick="ButtonSelectArea_Click" />
                                    <asp:Button ID="ButtonDeselectArea" runat="server" Text="Deselect All" OnClick="ButtonDeselectArea_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:Button ID="Button4" runat="server" Text=">>" OnClick="ButtonLinkAreaUnsel_Click" />
                                    <br />
                                    <br />
                                    <asp:Button ID="Button3" runat="server" Text="<<" OnClick="ButtonLinkArea_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListAreaUnsel" runat="server" DataSourceID="ObjectDataSourceAreaUnsel"
                                                DataTextField="Area" DataValueField="AreaId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="Label3" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectAreaUnsel" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectAreaUnsel" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectAreaUnsel" runat="server" Text="Select All" OnClick="ButtonSelectAreaUnsel_Click" />
                                    <asp:Button ID="ButtonDeselectAreaUnsel" runat="server" Text="Deselect All" OnClick="ButtonDeselectAreaUnsel_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Button4" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="Button3" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
