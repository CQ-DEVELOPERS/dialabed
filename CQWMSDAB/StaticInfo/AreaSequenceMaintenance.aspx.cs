﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

public partial class StaticInfo_AreaSequenceMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewAreaAll_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewAreaAllGroupLoc_SelectedIndexChanged";
        try
        {
            Session["AreaId"] = GridViewAreaAll.SelectedDataKey["AreaId"];

        }
        catch (Exception ex)
        {
            result = SendErrorNow("AreaOperatorGroupMaintenance" + "_" + ex.Message.ToString());
        }
    }


    protected void ObjectDataSourceAreaAll_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertAreaAll.Visible = false;
        GridViewAreaAll.Visible = true;

        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_AreaAll, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceAreaAll_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string AreaCode = ((TextBox)DetailsViewInsertAreaAll.FindControl("DetailTextAreaCode")).Text;
        string RestrictedAreaIndicator = ((TextBox)DetailsViewInsertAreaAll.FindControl("DetailTextRestrictedAreaIndicator")).Text;

        e.InputParameters["AreaCode"] = AreaCode;
        e.InputParameters["RestrictedAreaIndicator"] = RestrictedAreaIndicator;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertAreaAll.Visible = false;
        GridViewAreaAll.Visible = true;
    }

    protected void DetailsViewInsertAreaAll_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    #region ButtonSelectPickArea_Click
    protected void ButtonSelectPickArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectPickArea_Click";
        try
        {
            foreach (ListItem item in CheckBoxListAreaPick.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectPickArea_Click

    #region ButtonSelectStoreArea_Click
    protected void ButtonSelectStoreArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectStoreArea_Click";
        try
        {
            foreach (ListItem item in CheckBoxListAreaStore.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectStoreArea_Click

    #region ButtonDeselectPickArea_Click
    protected void ButtonDeselectPickArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectPickArea_Click";

        try
        {
            foreach (ListItem item in CheckBoxListAreaPick.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectPickArea_Click

    #region ButtonDeselectStoreArea_Click
    protected void ButtonDeselectStoreArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectStoreArea_Click";

        try
        {
            foreach (ListItem item in CheckBoxListAreaStore.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectStoreArea_Click

    #region ButtonLinkAreaPick_Click

    protected void ButtonLinkAreaPick_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectPickArea_Click";
        try
        {
            List<int> areaList = new List<int>();

            foreach (ListItem item in CheckBoxListAreaPick.Items)
            {
                if (item.Selected == true)
                {
                    areaList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int pickarea in areaList)
            {
                Area.AreaPickArea_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["AreaId"]), pickarea);
            }

            //CheckBoxListAreaStore.DataBind();
            //CheckBoxListAreaPick.DataBind();

            //Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            //result = SendErrorNow("AreaOperatorGroup" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
        //theErrMethod = "ButtonSelectPickArea_Click";
        try
        {
            List<int> areaList = new List<int>();

            foreach (ListItem item in CheckBoxListAreaStore.Items)
            {
                if (item.Selected == true)
                {
                    areaList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int storearea in areaList)
            {
                Area.AreaStoreArea_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["AreaId"]), storearea);
            }

            CheckBoxListAreaStore.DataBind();
            CheckBoxListAreaPick.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("AreaOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkAreaPick_Click

    //#region ButtonLinkAreaStore_Click

    //protected void ButtonLinkAreaStore_Click(object sender, EventArgs e)
    //{
    //    theErrMethod = "ButtonSelectPickArea_Click";
    //    try
    //    {
    //        List<int> areaList = new List<int>();

    //        foreach (ListItem item in CheckBoxListAreaPick.Items)
    //        {
    //            if (item.Selected == true)
    //            {
    //                areaList.Add(Convert.ToInt32(item.Value));
    //            }
    //        }


    //        foreach (int storearea in areaList)
    //        {
    //            Area.AreaStoreArea_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["AreaId"]), area);
    //        }

    //        CheckBoxListAreaStore.DataBind();
    //        CheckBoxListAreaPick.DataBind();

    //        Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("AreaOperatorGroup" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion ButtonLinkAreaStore_Click
}

