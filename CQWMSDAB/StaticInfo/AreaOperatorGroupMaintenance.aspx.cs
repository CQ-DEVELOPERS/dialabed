﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

public partial class StaticInfo_AreaOperatorGroupMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewOperatorGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOperatorGroupGroupLoc_SelectedIndexChanged";
        try
        {
            Session["OperatorGroupId"] = GridViewOperatorGroup.SelectedDataKey["OperatorGroupId"];

        }
        catch (Exception ex)
        {
            result = SendErrorNow("AreaOperatorGroupMaintenance" + "_" + ex.Message.ToString());
        }
    }

    
    protected void ObjectDataSourceOperatorGroup_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertAreaOperatorGroup.Visible = false;
        GridViewOperatorGroup.Visible = true;
        
        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_OperatorGroup, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceOperatorGroup_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string OperatorGroupCode = ((TextBox)DetailsViewInsertAreaOperatorGroup.FindControl("DetailTextOperatorGroupCode")).Text;
        string RestrictedAreaIndicator = ((TextBox)DetailsViewInsertAreaOperatorGroup.FindControl("DetailTextRestrictedAreaIndicator")).Text;

        e.InputParameters["OperatorGroupCode"] = OperatorGroupCode;
        e.InputParameters["RestrictedAreaIndicator"] = RestrictedAreaIndicator;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertAreaOperatorGroup.Visible = false;
        GridViewOperatorGroup.Visible = true;
    }

    protected void DetailsViewInsertAreaOperatorGroup_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

    #region ButtonSelectArea_Click
    protected void ButtonSelectArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectArea_Click";
        try
        {
            foreach (ListItem item in CheckBoxListArea.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectArea_Click

    #region ButtonSelectAreaUnsel_Click
    protected void ButtonSelectAreaUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectAreaUnsel_Click";
        try
        {
            foreach (ListItem item in CheckBoxListAreaUnsel.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectAreaUnsel_Click

    #region ButtonDeselectArea_Click
    protected void ButtonDeselectArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectArea_Click";

        try
        {
            foreach (ListItem item in CheckBoxListArea.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectAreaUnsel_Click

    #region ButtonDeselectAreaUnsel_Click
    protected void ButtonDeselectAreaUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectAreaUnsel_Click";

        try
        {
            foreach (ListItem item in CheckBoxListAreaUnsel.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectAreaUnsel_Click

    #region ButtonLinkArea_Click

    protected void ButtonLinkArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectArea_Click";
        try
        {
            List<int> areaList = new List<int>();

            foreach (ListItem item in CheckBoxListAreaUnsel.Items)
            {
                if (item.Selected == true)
                {
                    areaList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int area in areaList)
            {
                Area.AreaOperatorGroup_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["OperatorGroupId"]), area);
            }

            CheckBoxListAreaUnsel.DataBind();
            CheckBoxListArea.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("AreaOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkArea_Click

    #region ButtonLinkAreaUnsel_Click

    protected void ButtonLinkAreaUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectArea_Click";
        try
        {
            List<int> areaList = new List<int>();

            foreach (ListItem item in CheckBoxListArea.Items)
            {
                if (item.Selected == true)
                {
                    areaList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int area in areaList)
            {
                Area.AreaOperatorGroup_Delete(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["OperatorGroupId"]), area);
            }

            CheckBoxListAreaUnsel.DataBind();
            CheckBoxListArea.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("AreaOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkAreaUnsel_Click
}
