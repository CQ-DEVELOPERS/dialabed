﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class StaticInfo_CompanyEmailMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewExternalCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewExternalCompany_SelectedIndexChanged";
        try
        {
            Session["ExternalCompanyId"] = GridViewExternalCompany.SelectedDataKey["ExternalCompanyId"];

            ObjectDataSourceContact.DataBind();
            GridViewContact.SelectedIndex = -1;
            PanelContact.Visible = true;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ContactMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewExternalCompany.DataBind();
    }
    #endregion ButtonSearch_Click

    protected void ButtonAddContact_Click(object sender, EventArgs e)
    {
        DetailsViewInsertContact.Visible = true;
        GridViewContact.Visible = false;
    }    

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        //ObjectDataSourceCompany.Insert();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertExternalCompany.Visible = false;
        GridViewExternalCompany.Visible = true;
    }

    protected void btnlocInsert_Click(object sender, EventArgs e)
    {
        // ObjectDataSourceLocation.Insert();
    }

    protected void btnlocCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertContact.Visible = false;
        GridViewContact.Visible = true;
    }

    protected void ObjectDataSourceContact_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        int ReportTypeId = int.Parse(((DropDownList)DetailsViewInsertContact.FindControl("DropDownListReportType")).SelectedValue.ToString());        
        string ContactPerson = ((TextBox)DetailsViewInsertContact.FindControl("TextboxContactPerson")).Text;
        int ContactListId = int.Parse("0");
        int ExternalCompanyId = int.Parse(Session["ExternalCompanyId"].ToString());

        e.InputParameters["ReportTypeId"] = ReportTypeId;
        e.InputParameters["ContactPerson"] = ContactPerson;
        e.InputParameters["ContactListId"] = ContactListId;
        e.InputParameters["ExternalCompanyId"] = ExternalCompanyId;

    }

    protected void ObjectDataSourceContact_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        ObjectDataSourceContact.DataBind();
        DetailsViewInsertContact.Visible = false;
        GridViewContact.Visible = true;

    }
}
