using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class StaticInfo_BatchEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["FromURL"] = "~/StaticInfo/BatchEdit.aspx";
            }
        }
        catch { }
    }

    #region ButtonAdd_Click
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        try
        {
            Master.ErrorText = "";

            Batch b = new Batch();
            int storageUnitId = -1;
            int batchId = -1;

            if (Session["StorageUnitId"] != null)
                storageUnitId = (int)Session["StorageUnitId"];

            if (storageUnitId == -1)
            {
                Master.ErrorText = "Please select a product";
                return;
            }

            if (Session["BatchId"] != null)
                batchId = (int)Session["BatchId"];

            if (batchId == -1)
            {
                Master.ErrorText = "Please select a batch";
                return;
            }

            if (!b.LinkBatch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], storageUnitId, batchId, (int)Session["OperatorId"]))
                Master.ErrorText = "Product / Batch Link failed";
            else
                GridViewProductSearch.DataBind();
            Session["StorageUnitId"] = null;
        }
        catch { }
    }
    #endregion ButtonAdd_Click
}