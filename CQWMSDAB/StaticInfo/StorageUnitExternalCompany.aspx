<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" 
CodeFile="StorageUnitExternalCompany.aspx.cs" Inherits="StaticInfo_StorageUnitExternalCompany" 
    Title="<%$ Resources:Default, MovementTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="prod" %>
<%@ Register Src="../Common/ExternalCompanySearch.ascx" TagName="ExtSearch" TagPrefix="comp" %>
    
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Product</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelProduct" runat="server">
                        <ContentTemplate>
                            <prod:ProductSearch ID="ProductSearch1" runat="server" />
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneExceptionCode" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select an External Company</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelExceptionCode" runat="server">
                        <ContentTemplate>
                            <comp:ExtSearch ID="ExternalSearch1" runat="server" />
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
                <asp:ObjectDataSource ID="ObjectDataSourceStorageUnitExternalCompany" TypeName="StaticInfo" SelectMethod="SelectStorageUnitExternalCompanies" 
    
               
</asp:Content>