﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
//using System.Collections.Generic;
//using System.Linq;

public partial class StaticInfo_IntergrationDelimiter : System.Web.UI.UserControl
{
    //public void RemoveHeaderContextMenuColumnsByUniqueName(HashSet<string> uniqueNames, IList<RadMenuItem> menuItems)
    //{

    //    string outerMenuName = "Columns";
    //    RadMenuItemCollection col_rmi = menuItems.First<RadMenuItem>(rmi => rmi.Text == outerMenuName).Items;

    //    if (col_rmi != null && col_rmi.Count() > 0)
    //    {
    //        foreach (string targetName in uniqueNames)
    //        {
                

    //            RadMenuItem targetControl = col_rmi.FindItem(rmi => rmi.Value.Split('|').Last() == targetName);
    //            lblLabel.Text = lblLabel.Text + " " + targetName.ToString();
    //            if (targetControl != null)
    //            {
    //                targetControl.Enabled = false;
    //            }

    //        }
    //    }
    //}

    //protected void Grid_Load(object sender, EventArgs e)
    //{

    //    Grid.HeaderContextMenu.PreRender += new EventHandler(GridHeaderContextMenu_PreRender);
    //}

    //protected void ProductsGridHeaderContextMenu_PreRender(object sender, EventArgs e)
    //{
    //    RadGridUtility.RemoveHeaderContextMenuColumnsByUniqueName(_columnsToHide, Grid.HeaderContextMenu.GetAllItems());
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        RadToolBar toolBar = RadFileExplorer1.ToolBar;

        // Remove commands from the ToolBar control;  
        int i = 0;
        while (i < toolBar.Items.Count)
        {
            //lblLabel.Text = lblLabel.Text + " " + toolBar.Items[i].Value.ToString();

            if (toolBar.Items[i].Value.ToUpper() == "DELETE" || toolBar.Items[i].Value.ToUpper() == "UPLOAD")
            {
                toolBar.Items.RemoveAt(i);
                continue;// Next item  
            }

            i++;// Next item  
        }

        RadContextMenu menu = RadFileExplorer1.GridContextMenu;
        i = 0;
        while (i < menu.Items.Count)
        {
            if (menu.Items[i].Value.ToUpper() == "DELETE" || menu.Items[i].Value.ToUpper() == "RENAME" || menu.Items[i].Value.ToUpper() == "UPLOAD")
            {
                menu.Items.RemoveAt(i);
                continue;
            }

            i++;
        }

        ////RadFileExplorer1.ExplorerMode
        //RadListView listView = RadFileExplorer1.ListView;
        //i = 0;
        //while (i < listView.Items.Count)
        //{
        //    if (listView.Items[i].Value.ToUpper() == "DELETE" || listView.Items[i].Value.ToUpper() == "UPLOAD")
        //    {
        //        toolBar.Items.RemoveAt(i);
        //        continue;// Next item  
        //    }

        //    i++;// Next item  
        //}

        RadFileExplorer1.EnableCreateNewFolder = false;

        string[] paths = new string[] { "~/FTP" };//{ "/ROOT/" };
        RadFileExplorer1.Configuration.ViewPaths = paths;

        //string[] deletePaths = new string[] { "~/FTP/" };
        //RadFileExplorer1.Configuration.DeletePaths = deletePaths;

        //string[] uploadPaths = new string[] { "~/FTP/" };
        //RadFileExplorer1.Configuration.UploadPaths = uploadPaths;

        // The "ROOT/CanDelAndUpload/" folder is configured with full permissions
        //RadFileExplorer1.Configuration.ContentProviderTypeName = typeof(FtpContentProvider).AssemblyQualifiedName;
    }
}