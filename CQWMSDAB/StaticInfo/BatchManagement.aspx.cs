using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class StaticInfo_BatchManagement : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                Session["FromURL"] = "~/StaticInfo/BatchManagement.aspx";
                
                if (Request.QueryString["Batch"] != null)
                {
                    Session["BatchNumber"] = Request.QueryString["Batch"].ToString();
                }
            }
        }
        catch { }
    }

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {


        try
        {
            GridViewBatchManagement1.DataBind();


            Master.MsgText = "GridView Succussful";
            Master.ErrorText = "";
        }
        catch (Exception ex)
        {

            // result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = ex.Message.ToString();
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonPrintLocations_Click
    protected void ButtonPrintLocations_Click(object sender, EventArgs e)
    {

        Session["FromURL"] = "~/StaticInfo/BatchManagement.aspx";
        Session["ReportName"] = "Batch Product Location Barcode";
        ReportParameter[] RptParameters = new ReportParameter[6];

        if (Session["StorageUnitIdReport"] == null)
        {
            Session["StorageUnitIdReport"] = -1;
        };

        if (Session["BatchId"] == null)
        {
            Session["BatchId"] = -1;
        };


        RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
        RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
        RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());
        RptParameters[3] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());
        RptParameters[4] = new ReportParameter("StorageUnitId", Session["StorageUnitIdReport"].ToString());
        RptParameters[5] = new ReportParameter("BatchId", Session["BatchId"].ToString());
        

        Session["ReportParameters"] = RptParameters;

        Response.Redirect("~/Reports/Report.aspx");
        
    }
    #endregion "ButtonPrintLocations_Click"

    #region GridViewBatchManagement1_SelectedIndexChanged
    protected void GridViewBatchManagement1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //theErrMethod = "GridViewBatchManagement1_SelectedIndexChanged";
        try
        {
            Session["BatchId"] = int.Parse(GridViewBatchManagement1.SelectedDataKey["BatchId"].ToString());
            Session["StorageUnitIdReport"] = int.Parse(GridViewBatchManagement1.SelectedDataKey["StorageUnitId"].ToString());
            Session["Batch"] = GridViewBatchManagement1.SelectedDataKey["Batch"].ToString();
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.Message.ToString();
        }

    }
    #endregion GridViewInstruction_SelectedIndexChanged


    #region ButtonSearchLinkedProducts_Click
    protected void ButtonSearchLinkedProducts_Click(object sender, EventArgs e)
    {
        GridViewUnlinkedProducts.PageIndex = 0;

        GridViewUnlinkedProducts.DataBind();
    }
    #endregion "ButtonSearchLinkedProducts_Click"

    #region GridViewUnlinkedProducts_SelectedIndexChanged
    protected void GridViewUnlinkedProducts_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Master.ErrorText = "";

            Batch b = new Batch();

            if (Session["BatchId"] != null)
                if ((int)Session["BatchId"] == -1)
                {
                    Master.ErrorText = "Please select a batch";
                    return;
                }

            if (!b.LinkBatch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)GridViewUnlinkedProducts.SelectedDataKey["StorageUnitId"], (int)Session["BatchId"], (int)Session["OperatorId"]))
                Master.ErrorText = "Product / Batch Link failed";
            else
            {
                GridViewProductSearch.DataBind();
                GridViewUnlinkedProducts.DataBind();
            }
            GridViewUnlinkedProducts.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.Message.ToString();
        }
    }
    #endregion "GridViewUnlinkedProducts_SelectedIndexChanged"

    #region GridViewProductSearch_SelectedIndexChanged
    protected void GridViewProductSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Master.ErrorText = "";

            Batch b = new Batch();
            
            if (!b.UnlinkBatch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)GridViewProductSearch.SelectedDataKey["StorageUnitBatchId"], (int)Session["OperatorId"]))
                Master.ErrorText = "Product / Batch De-link failed";
            else
            {
                GridViewProductSearch.DataBind();
                GridViewUnlinkedProducts.DataBind();
            }

            GridViewProductSearch.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.Message.ToString();
        }
    }
    #endregion "GridViewProductSearch_SelectedIndexChanged"
}