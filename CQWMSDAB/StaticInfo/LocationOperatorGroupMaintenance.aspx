﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="LocationOperatorGroupMaintenance.aspx.cs" Inherits="StaticInfo_LocationOperatorGroupMaintenance"
    Title="<%$ Resources:Default, LocationOperatorGroupTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, LocationOperatorGroupTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Location Operator Group">
            <HeaderTemplate>
                Location Operator Group
            </HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table border="solid" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Operator Groups" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text="Select Aisle and Level Filter" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="Linked Locations" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text=" " Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Unlinked Locations" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="updatePanel_OperatorGroup" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                                            <asp:GridView ID="GridViewOperatorGroup" DataSourceID="ObjectDataSourceOperatorGroup"
                                                DataKeyNames="OperatorGroupId" runat="server" AllowPaging="True" AllowSorting="True"
                                                PageSize="15" AutoGenerateColumns="False" EmptyDataText="There are currently no Operator Groups..."
                                                OnSelectedIndexChanged="GridViewOperatorGroup_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" ShowEditButton="False" ShowDeleteButton="False"
                                                        ShowCancelButton="False" />
                                                    <asp:BoundField HeaderText="OperatorGroupId" DataField="OperatorGroupId" SortExpression="OperatorGroupId"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="OperatorGroupCode" DataField="OperatorGroupCode" SortExpression="OperatorGroupCode"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="OperatorGroup" DataField="OperatorGroup" SortExpression="OperatorGroup" />
                                                    <asp:BoundField HeaderText="RestrictedAreaIndicator" DataField="RestrictedAreaIndicator"
                                                        SortExpression="RestrictedAreaIndicator" Visible="False" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:DetailsView Visible="false" ID="DetailsViewInsertLocationOperatorGroup" runat="server"
                                                AutoGenerateRows="False" DataSourceID="ObjectDataSourceOperatorGroup" OnLoad="DetailsViewInsertLocationOperatorGroup_Load">
                                                <Fields>
                                                    <asp:TemplateField HeaderText="OperatorGroupId" InsertVisible="False" SortExpression="OperatorGroupId">
                                                        <InsertItemTemplate>
                                                            <asp:Button ID="AddOperatorGroup" runat="server" CommandName="Insert" Text="Add" />
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OperatorGroupCode" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextOperatorGroupCode" runat="server" Text='<%# Bind("OperatorGroupCode") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="RestrictedAreaIndicator" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextRestrictedAreaIndicator" runat="server" Text='<%# Bind("RestrictedAreaIndicator") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                </Fields>
                                            </asp:DetailsView>
                                            <asp:ObjectDataSource ID="ObjectDataSourceOperatorGroup" runat="server" TypeName="OperatorGroup"
                                                SelectMethod="ListOperatorGroups">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                                                SelectMethod="GetLocationsSel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="operatorGroupId" SessionField="OperatorGroupId" Type="Int32"
                                                        DefaultValue="1" />
                                                    <asp:SessionParameter Name="FromAisle" SessionField="FromAisle" Type="String" />
                                                    <asp:SessionParameter Name="ToAisle" SessionField="ToAisle" Type="String" />
                                                    <asp:SessionParameter Name="FromLevel" SessionField="FromLevel" Type="String" />
                                                    <asp:SessionParameter Name="ToLevel" SessionField="ToLevel" Type="String" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceLocationUnsel" runat="server" TypeName="Location"
                                                SelectMethod="GetLocationsUnsel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="operatorGroupId" SessionField="OperatorGroupId" Type="Int32" />
                                                    <asp:SessionParameter Name="FromAisle" SessionField="FromAisle" Type="String" />
                                                    <asp:SessionParameter Name="ToAisle" SessionField="ToAisle" Type="String" />
                                                    <asp:SessionParameter Name="FromLevel" SessionField="FromLevel" Type="String" />
                                                    <asp:SessionParameter Name="ToLevel" SessionField="ToLevel" Type="String" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceAisle" runat="server" TypeName="StockTake"
                                                SelectMethod="AislesList">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceLevel" runat="server" TypeName="StockTake"
                                                SelectMethod="LevelsList">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:Label ID="LabelFromAisle" runat="server" Text="From Aisle"></asp:Label>
                                    <asp:DropDownList ID="DropDownListFromAisle" runat="server" DataSourceID="ObjectDataSourceAisle"
                                        DataValueField="Aisle" DataTextField="Aisle">
                                    </asp:DropDownList>
                                    <asp:Label ID="LabelToAisle" runat="server" Text="To Aisle"></asp:Label>
                                    <asp:DropDownList ID="DropDownListToAisle" runat="server" DataSourceID="ObjectDataSourceAisle"
                                        DataValueField="Aisle" DataTextField="Aisle">
                                    </asp:DropDownList>
                                    <br>
                                    <br></br>
                                    <asp:Label ID="LabelFromLevel" runat="server" Text="From Level:"></asp:Label>
                                    <asp:DropDownList ID="DropDownListFromLevel" runat="server" 
                                        DataSourceID="ObjectDataSourceLevel" DataTextField="Level" 
                                        DataValueField="Level">
                                    </asp:DropDownList>
                                    <asp:Label ID="LabelToLevel" runat="server" Text="To Level:"></asp:Label>
                                    <asp:DropDownList ID="DropDownListToLevel" runat="server" 
                                        DataSourceID="ObjectDataSourceLevel" DataTextField="Level" 
                                        DataValueField="Level">
                                    </asp:DropDownList>
                                    <br>
                                    <br></br>
                                    <asp:Button ID="ButtonDisplayLocation" runat="server" 
                                        OnClick="ButtonDisplayLocation_Click" Style="width: auto;" 
                                        Text="Display Locations" />
                                    </br>
                                    </br>
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                                DataTextField="Location" DataValueField="LocationId" RepeatColumns="4">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="LabelErrorMsg" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectLocation" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectLocation" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectLocation" runat="server" Text="Select All" OnClick="ButtonSelectLocation_Click" />
                                    <asp:Button ID="ButtonDeselectLocation" runat="server" Text="Deselect All" OnClick="ButtonDeselectLocation_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:Button ID="ButtonMakeNotSelected" runat="server" Text=">>" OnClick="ButtonLinkLocationUnsel_Click" />
                                    <br />
                                    <br />
                                    <asp:Button ID="ButtonMakeSelected" runat="server" Text="<<" OnClick="ButtonLinkLocation_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListLocationUnsel" runat="server" DataSourceID="ObjectDataSourceLocationUnsel"
                                                DataTextField="Location" DataValueField="LocationId" RepeatColumns="4">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="Label3" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectLocationUnsel" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectLocationUnsel" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectLocationUnsel" runat="server" Text="Select All" OnClick="ButtonSelectLocationUnsel_Click" />
                                    <asp:Button ID="ButtonDeselectLocationUnsel" runat="server" Text="Deselect All" OnClick="ButtonDeselectLocationUnsel_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonMakeNotSelected" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonMakeSelected" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
