﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OperatorGroupInstructionTypeMaint.aspx.cs" Inherits="StaticInfo_OperatorGroupInstructionTypeMaint"
    Title="<%$ Resources:Default, OperatorGroupInstrType %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OperatorGroupInstrType %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Operator Group Instruction Type">
            <HeaderTemplate>
                Operator Group Instruction Type
            </HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table border="solid" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Operator Groups" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="Linked Instruction Types" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text=" " Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Unlinked Instruction Types" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="updatePanel_OperatorGroup" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                                            <asp:GridView ID="GridViewOperatorGroup" DataSourceID="ObjectDataSourceOperatorGroup"
                                                DataKeyNames="OperatorGroupId" runat="server" AllowPaging="True" AllowSorting="True"
                                                PageSize="15" AutoGenerateColumns="False" EmptyDataText="There are currently no Operator Groups..."
                                                OnSelectedIndexChanged="GridViewOperatorGroup_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" ShowEditButton="False" ShowDeleteButton="False"
                                                        ShowCancelButton="False" />
                                                    <asp:BoundField HeaderText="OperatorGroupId" DataField="OperatorGroupId" SortExpression="OperatorGroupId"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="OperatorGroupCode" DataField="OperatorGroupCode" SortExpression="OperatorGroupCode"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="OperatorGroup" DataField="OperatorGroup" SortExpression="OperatorGroup" />
                                                    <asp:BoundField HeaderText="RestrictedAreaIndicator" DataField="RestrictedAreaIndicator"
                                                        SortExpression="RestrictedAreaIndicator" Visible="False" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:DetailsView Visible="false" ID="DetailsViewInsertOperatorGroupInstructionType" runat="server"
                                                AutoGenerateRows="False" DataSourceID="ObjectDataSourceOperatorGroup" OnLoad="DetailsViewInsertOperatorGroupInstructionType_Load">
                                                <Fields>
                                                    <asp:TemplateField HeaderText="OperatorGroupId" InsertVisible="False" SortExpression="OperatorGroupId">
                                                        <InsertItemTemplate>
                                                            <asp:Button ID="AddOperatorGroup" runat="server" CommandName="Insert" Text="Add" />
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OperatorGroupCode" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextOperatorGroupCode" runat="server" Text='<%# Bind("OperatorGroupCode") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="RestrictedAreaIndicator" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextRestrictedAreaIndicator" runat="server" Text='<%# Bind("RestrictedAreaIndicator") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                </Fields>
                                            </asp:DetailsView>
                                            <asp:ObjectDataSource ID="ObjectDataSourceOperatorGroup" runat="server" TypeName="OperatorGroup"
                                                SelectMethod="ListOperatorGroups">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceInstructionType" runat="server" TypeName="InstructionType" SelectMethod="GetInstructionTypesSel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="operatorGroupId" SessionField="OperatorGroupId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceInstructionTypeUnsel" runat="server" TypeName="InstructionType"
                                                SelectMethod="GetInstructionTypesUnsel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="operatorGroupId" SessionField="OperatorGroupId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListInstructionType" runat="server" DataSourceID="ObjectDataSourceInstructionType"
                                                DataTextField="InstructionType" DataValueField="InstructionTypeId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="LabelErrorMsg" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectInstructionType" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectInstructionType" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectInstructionType" runat="server" Text="Select All" OnClick="ButtonSelectInstructionType_Click" />
                                    <asp:Button ID="ButtonDeselectInstructionType" runat="server" Text="Deselect All" OnClick="ButtonDeselectInstructionType_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:Button ID="Button4" runat="server" Text=">>" OnClick="ButtonLinkInstructionTypeUnsel_Click" />
                                    <br />
                                    <br />
                                    <asp:Button ID="Button3" runat="server" Text="<<" OnClick="ButtonLinkInstructionType_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListInstructionTypeUnsel" runat="server" DataSourceID="ObjectDataSourceInstructionTypeUnsel"
                                                DataTextField="InstructionType" DataValueField="InstructionTypeId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="Label3" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectInstructionTypeUnsel" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectInstructionTypeUnsel" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectInstructionTypeUnsel" runat="server" Text="Select All" OnClick="ButtonSelectInstructionTypeUnsel_Click" />
                                    <asp:Button ID="ButtonDeselectInstructionTypeUnsel" runat="server" Text="Deselect All" OnClick="ButtonDeselectInstructionTypeUnsel_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Button4" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="Button3" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
