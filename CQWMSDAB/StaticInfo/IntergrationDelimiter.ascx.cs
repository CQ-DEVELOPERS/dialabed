﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Script.Serialization;
using System.Text;
using System.Windows;
using System.IO;
using System.Diagnostics;
using Microsoft.Reporting.WebForms;

public partial class StaticInfo_IntergrationDelimiter : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RadListBox1.ItemDataBound += new RadListBoxItemEventHandler(RadListBox1_ItemDataBound);
            RadListBox1.Transferring += new RadListBoxTransferringEventHandler(RadListBox1_Transferring);
        }
    }
    private JavaScriptSerializer serializer;
    public JavaScriptSerializer Serializer
    {
        get
        {
            if (serializer == null)
            {
                serializer = new JavaScriptSerializer();
            }

            return serializer;
        }
    }

    void RadListBox1_Transferring(object sender, RadListBoxTransferringEventArgs e)
    {
        //e.Cancel = true;

        //foreach (RadListBoxItem item in e.Items)
        //{
        //    var dataItem = Serializer.Deserialize<DogInfo>(item.Attributes["dataItem"]);
        //    var destItem = new Telerik.Web.UI.RadListBoxItem();
        //    destItem.Attributes["dataItem"] = item.Attributes["dataItem"];

        //    e.DestinationListBox.Items.Add(destItem);

        //    destItem.DataItem = dataItem;
        //    destItem.DataBind();

        //    e.SourceListBox.Items.Remove(item);
        //}
    }

    void RadListBox1_ItemDataBound(object sender, RadListBoxItemEventArgs e)
    {
        //var serializedDataItem = GetSerializedDataItem((DataRowView)e.Item.DataItem);
        //e.Item.Attributes.Add("dataItem", serializedDataItem);
    }

    #region BindData
    public void BindData()
    {
        try
        {
            RadListBox1.DataBind();
            RadListBox2.DataBind();
        }
        catch { }
    }
    #endregion BindData

    protected void RadButtonSave_Click(object sender, EventArgs e)
    {
        try
        {
            lblErrorMessage.Text = "";
            lblErrorMessage.ForeColor = System.Drawing.Color.Black;

            int count = 0;
            int key = -1;
            int value = -1;
            int start = 0;
            int end = 0;

            foreach (RadListBoxItem item in this.RadListBox2.Items)
            {
                key = -1;
                value = -1;

                RadTextBox startPos = (RadTextBox)item.FindControl("rtbStartPos");
                RadTextBox endPos = (RadTextBox)item.FindControl("rtbEndPos");
                RadTextBox format = (RadTextBox)item.FindControl("rtbFormat");

                int.TryParse(startPos.Text, out start);
                int.TryParse(endPos.Text, out end);

                if (item.DataKey != null)
                    int.TryParse(item.DataKey.ToString(), out key);

                if (item.Value != null)
                    int.TryParse(item.Value, out value);

                if (!Intergration.SaveMapping(Session["ConnectionStringName"].ToString(), (int)Session["interfaceMappingFileId"], key, value, count, start, end, format.Text))
                {
                    lblErrorMessage.Text = "Error trying to save " + item.Text;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                    break;
                }
                count++;
            }

            BindData();
        }
        catch { }
    }

    protected void RadListBox1_Transferred(object sender, RadListBoxTransferredEventArgs e)
    {
        RadButtonSave_Click(sender, e);
    }

    protected void RadListBox2_Deleted(object sender, RadListBoxEventArgs e)
    {
        foreach (RadListBoxItem item in e.Items)
        {
            lblErrorMessage.Text = "";
            lblErrorMessage.ForeColor = System.Drawing.Color.Black;

            lblErrorMessage.Text = "Error trying to save ";
            if (!Intergration.DeleteMapping(Session["ConnectionStringName"].ToString(), int.Parse(item.Value.ToString())))
            {
                lblErrorMessage.Text = "Error trying to save " + item.Text;
                lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                break;
            }
        }
    }

    //protected async void AppendButton_Click(object sender, EventArgs e)
    //{
    //    string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
    //    StringBuilder sb = new StringBuilder();

    //    //sb.AppendLine("New User Input");
    //    //sb.AppendLine("= = = = = =");
    //    sb.Append("ProductCode, Product, SKUCode, SKU, PalletQuantity, Barcode, MinimumQuantity, ReorderQuantity, MaximumQuantity, CuringPeriodDays, ShelfLifeDays, QualityAssuranceIndicator, ProductType, ProductCategory, OverReceipt, RetentionSamples, HostId, PackCode, PackDescription, Quantity, Barcode, Length, Width, Height, Volume, NettWeight, GrossWeight, TareWeight, PackingCategory, Place Holder, PickEmpty, StackingCategory, MovementCategory, ValueCategory, StoringCategory, PickPartPallet, PrincipalCode, UnitPrice, StyleCode, Style, ColourCode, Colour, SizeCode, Size, Description2, Description3, ProductAlias, PutawayRule, AllocationRule, BillingRule, LotAttributeRule, StorageCondition");
    //    sb.AppendLine();
    //    sb.Append(", , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , ");
    //    sb.AppendLine();

    //    using (StreamWriter outfile = new StreamWriter(mydocpath + @"\UserInputFile.txt", true))
    //    {
    //        outfile.write
    //        //await outfile.WriteAsync(sb.ToString());
    //    }
    //}

    #region RadButtonWriteFile_Click
    protected void RadButtonWriteFile_Click(object sender, EventArgs e)
    {
        //using (StreamWriter w = new StreamWriter(Server.MapPath("~/data.txt"), true))
        //{
        //    w.WriteLine(txtData.Text); // Write the text
        //}

        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=fname.ext");
    }
    #endregion RadButtonWriteFile_Click

    #region RadButtonLayout_Click
    protected void RadButtonLayout_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = null;

            Session["ReportName"] = "Interface Format";

            ReportParameter[] RptParameters = new ReportParameter[5];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            // Create the InterfaceMappingFileId report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("PrincipalId", "-1");

            // Create the InterfaceMappingFileId report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("InterfaceMappingFileId", Session["InterfaceMappingFileId"].ToString());

            Session["ReportParameters"] = RptParameters;

            ScriptManager.RegisterStartupScript(this.Page,
                                                this.Page.GetType(),
                                                "newWindow",
                                                "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                true);
        }
        catch { }
    }
    #endregion RadButtonLayout_Click

    #region GetTextAsStream
    private MemoryStream GetTextAsStream()
    {
        //Create the return memorystream object
        MemoryStream ReturnStream = new MemoryStream();


        //Create a streamwriter to write to the memory stream
        StreamWriter WriteStream = new StreamWriter(ReturnStream);


        //Write the text in the textbox to the Memory Stream.
        WriteStream.WriteLine("Hello World!");


        //Clean up the stream writer
        WriteStream.Flush();
        WriteStream.Close();


        //Return the memory Stream
        return ReturnStream;
    }
    #endregion GetTextAsStream

    #region RadButtonDownload_Click
    protected void RadButtonDownload_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page,
                                            this.Page.GetType(),
                                            "newWindow",
                                            "window.open('../StaticInfo/DownloadFile.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=25,height=25,top=100,left=100');",
                                            true);

        //try
        //{
        //    //Create and populate a memorystream with the contents of the textbox
        //    MemoryStream mstream = GetTextAsStream();

        //    //Convert the memorystream to an array of bytes.
        //    byte[] byteArray = mstream.ToArray();

        //    //Clean up the memory stream
        //    mstream.Flush();
        //    mstream.Close();

        //    // Clear all content output from the buffer stream
        //    Response.Clear();

        //    // Add a HTTP header to the output stream that specifies the default filename
        //    // for the browser's download dialog
        //    Response.AddHeader("Content-Disposition", "attachment; filename=message.txt");

        //    // Add a HTTP header to the output stream that contains the
        //    // content length(File Size). This lets the browser know how much data is being transfered
        //    Response.AddHeader("Content-Length", byteArray.Length.ToString());

        //    // Set the HTTP MIME type of the output stream
        //    Response.ContentType = "application/octet-stream";

        //    // Write the data out to the client.
        //    Response.BinaryWrite(byteArray);

        //    Response.Flush();
        //    Response.Close();

        //    //Session["FromURL"] = null;

        //    //Session["ReportName"] = "Interface Sample";

        //    //ReportParameter[] RptParameters = new ReportParameter[4];

        //    //// Create the ServerName report parameter
        //    //RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

        //    //// Create the DatabaseName report parameter
        //    //RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

        //    //// Create the UserName report parameter
        //    //RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

        //    //// Create the InterfaceMappingFileId report parameter
        //    //RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("InterfaceMappingFileId", Session["InterfaceMappingFileId"].ToString());

        //    //Session["ReportParameters"] = RptParameters;

        //    //ScriptManager.RegisterStartupScript(this.Page,
        //    //                                    this.Page.GetType(),
        //    //                                    "newWindow",
        //    //                                    "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
        //    //                                    true);
        //}
        //catch { }
    }
    #endregion RadButtonDownload_Click
}