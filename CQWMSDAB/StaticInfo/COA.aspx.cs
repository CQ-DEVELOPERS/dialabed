using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using System.IO;

public partial class SupplierPortal_CallOffMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["MenuId"] == null)
                    Session["MenuId"] = 1;
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("CallOffMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridCOASearch.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("CallOffMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region RadGridCOASearch_SelectedIndexChanged
    protected void RadGridCOASearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridCOASearch_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridCOASearch.Items)
            {
                if (item.Selected)
                {
                    Session["COAId"] = item.GetDataKeyValue("COAId");
                    Session["StorageUnitId"] = item.GetDataKeyValue("StorageUnitId");
                    
                    break;
                }
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("CallOffMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridCOASearch_SelectedIndexChanged

    #region RadGridCOATest_ItemCommand
    protected void RadGridCOATest_ItemCommand(object source, GridCommandEventArgs e)
    {
        foreach (GridColumn col in RadGridCOATest.Columns)
        {
            if (col.ColumnType == "GridBoundColumn")
            {
                if (col.UniqueName == "Test" || col.UniqueName == "Result" || col.UniqueName == "Method")
                {
                    if (e.CommandName == RadGrid.RebindGridCommandName || e.CommandName == RadGrid.PerformInsertCommandName || e.CommandName == RadGrid.UpdateCommandName)
                    {
                        col.Visible = false;
                    }
                }
            }
        }
    }
    #endregion RadGridCOATest_ItemCommand

    #region RadGridCOADetail_ItemCommand
    protected void RadGridCOADetail_ItemCommand(object source, GridCommandEventArgs e)
    {
        foreach (GridColumn col in RadGridCOADetail.Columns)
        {
            if (col.ColumnType == "GridBoundColumn")
            {
                if (col.UniqueName == "Note")
                {
                    if (e.CommandName == RadGrid.RebindGridCommandName || e.CommandName == RadGrid.PerformInsertCommandName || e.CommandName == RadGrid.UpdateCommandName)
                    {
                        col.Visible = false;
                    }
                }
            }
        }
    }
    #endregion RadGridCOADetail_ItemCommand

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "CallOffMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "CallOffMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);
        Profile.Pet = new Pet();
        Profile.Pet.SavePetNames(RadGridCOASearch, "RadGridCOASearch");
        Profile.Pet.SavePetNames(RadGridCOATest, "RadGridCOATest");
        Profile.Pet.SavePetNames(RadGridCOADetail, "RadGridCOADetail");
    }
    #endregion Render

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
            if (Profile.Pet.Names.Count > 0)
            {
                /*This Grid is an AutoGeneratedColumn grid. At this point in the page life cycle the AutoGeneratedColumns array 
                is not initililized and cannot load the settings for the grid. There is a CreateColumnSet() method that I thought would work 
                to make the columns available at this point but this did not work. I also tried to put LoadSettings call in the PreRender event
                but this would occur after columns were made invisible thus making the columns visible again by loading the previous settings.
                The only place that you might be able to do this is on the ColumnCreated event or the ItemCreated event.	 
                For grid columns that are declaratively set at design time or added manually in the Page_Init event this will work perfectly.*/
                if (Profile.Pet.Names.ContainsKey("RadGridCOASearch"))
                {
                    //RadGrid1.MasterTableView.GenerateColumnsCreateColumnSet(true);
                    GridSettings settings = new GridSettings(RadGridCOASearch);
                    settings.LoadSettings(Profile.Pet.Names["RadGridCOASearch"].ToString());
                }

                /*Columns declared on this grid are done so at design time*/
                if (Profile.Pet.Names.ContainsKey("RadGridCOATest"))
                {
                    GridSettings settings = new GridSettings(RadGridCOATest);
                    settings.LoadSettings(Profile.Pet.Names["RadGridCOATest"].ToString());
                }

                /*Columns declared on this grid are done so at design time*/
                if (Profile.Pet.Names.ContainsKey("RadGridCOADetail"))
                {
                    GridSettings settings = new GridSettings(RadGridCOADetail);
                    settings.LoadSettings(Profile.Pet.Names["RadGridCOADetail"].ToString());
                }
            }
    }
    #endregion Page_Init

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            foreach (GridDataItem item in RadGridCOASearch.Items)
            {
                if (item.Selected)
                {
                    Session["FromURL"] = "~/StaticInfo/COA.aspx";

                    Session["ReportName"] = "Certificate Of Analysis";

                    ReportParameter[] RptParameters = new ReportParameter[5];

                    // Create the JobId report parameter

                    RptParameters[0] = new ReportParameter("ProductCode", item.GetDataKeyValue("ProductCode").ToString());

                    RptParameters[1] = new ReportParameter("Batch", item.GetDataKeyValue("Batch").ToString());

                    RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                    RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                    Session["ReportParameters"] = RptParameters;

                    Response.Redirect("~/Reports/Report.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("CallOffMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrint_Click
}