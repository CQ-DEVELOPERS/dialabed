<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="COA.aspx.cs" Inherits="SupplierPortal_CallOffMaintenance"
    Title="<%$ Resources:Default, COATitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, COATitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, COAAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCOASearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridCOASearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCOATest" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridCOADetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridCOATest">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCOATest" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridCOADetail">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCOADetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Search %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Tests %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Notes %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtProductCode" runat="server" Label="<%$ Resources:Default, ProductCode %>" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtProduct" runat="server" Label="<%$ Resources:Default, Product %>" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtSKUCode" runat="server" Label="<%$ Resources:Default, SKUCode %>" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtBatch" runat="server" Label="<%$ Resources:Default, Batch %>" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadButton ID="ButtonPrint" OnClick="ButtonPrint_Click" runat="server" Text="<%$ Resources:Default, Print %>"></telerik:RadButton>
                        </td>
                        <td>
                            <telerik:RadButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"></telerik:RadButton>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="RadGridCOASearch" runat="server" DataSourceID="ObjectDataSourceCOASearch" OnSelectedIndexChanged="RadGridCOASearch_SelectedIndexChanged"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" Skin="Metro">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="COAId,StorageUnitId,ProductCode,Batch">
                        <Columns>
                            <telerik:GridBoundColumn DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Product" HeaderText="<%$ Resources:Default, Product %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                
                <asp:ObjectDataSource ID="ObjectDataSourceCOASearch" runat="server" TypeName="COA"
                    SelectMethod="SearchCOA">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:ControlParameter Name="ProductCode" ControlID="rtxtProductCode" Type="String" />
                        <asp:ControlParameter Name="Product" ControlID="rtxtProduct" Type="String" />
                        <asp:ControlParameter Name="SKUCode" ControlID="rtxtSKUCode" Type="String" />
                        <asp:ControlParameter Name="Batch" ControlID="rtxtBatch" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="RadPageView1">
                <telerik:RadGrid ID="RadGridCOATest" runat="server" DataSourceID="ObjectDataSourceCOATest" OnItemCommand="RadGridCOATest_ItemCommand"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true" Skin="Metro">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="COATestId"
                        Width="100%" CommandItemDisplay="Top" DataSourceID="ObjectDataSourceCOATest"
                        AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridEditCommandColumn InsertText="<%$ Resources:Default, Insert %>" EditText="<%$ Resources:Default, Edit %>" UpdateText="<%$ Resources:Default, Update %>" CancelText="<%$ Resources:Default, Cancel %>"></telerik:GridEditCommandColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListTestId" 
                                ListTextField="Test" ListValueField="TestId"
                                DataSourceID="ObjectDataSourceTest" DataField="TestId" 
                                HeaderText="<%$ Resources:Default, Test %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn DataField="Test" HeaderText="<%$ Resources:Default, NewTest %>"></telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListResultId" 
                                ListTextField="Result" ListValueField="ResultId"
                                DataSourceID="ObjectDataSourceResult" DataField="ResultId" 
                                HeaderText="<%$ Resources:Default, Result %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn DataField="Result" HeaderText="<%$ Resources:Default, NewResult %>"></telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListMethodId"
                                ListTextField="Method" ListValueField="MethodId"
                                DataSourceID="ObjectDataSourceMethod" DataField="MethodId" 
                                HeaderText="<%$ Resources:Default, Method %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn DataField="Method" HeaderText="<%$ Resources:Default, NewMethod %>"></telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="Pass" HeaderText="<%$ Resources:Default, Pass %>"></telerik:GridCheckBoxColumn>
                            <telerik:GridBoundColumn DataField="StartRange" HeaderText="<%$ Resources:Default, StartRange %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EndRange" HeaderText="<%$ Resources:Default, EndRange %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Value %>"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceCOATest" runat="server" TypeName="COA"
                    SelectMethod="SelectCOATest" InsertMethod="InsertCOATest" UpdateMethod="UpdateCOATest" DeleteMethod="DeleteCOATest">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="COAId" Type="Int32" SessionField="COAId" />
                        <asp:SessionParameter Name="StorageUnitId" Type="Int32" SessionField="StorageUnitId" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="COAId" Type="Int32" SessionField="COAId" />
                        <asp:Parameter Name="COATestId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="TestId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Test" Type="String"></asp:Parameter>
                        <asp:Parameter Name="ResultId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Result" Type="String"></asp:Parameter>
                        <asp:Parameter Name="MethodId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Method" Type="String"></asp:Parameter>
                        <asp:Parameter Name="Pass" Type="Boolean"></asp:Parameter>
                        <asp:Parameter Name="StartRange" Type="Decimal"></asp:Parameter>
                        <asp:Parameter Name="EndRange" Type="Decimal"></asp:Parameter>
                        <asp:Parameter Name="Value" Type="String"></asp:Parameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="COAId" Type="Int32" SessionField="COAId" />
                        <asp:Parameter Name="COATestId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="TestId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Test" Type="String"></asp:Parameter>
                        <asp:Parameter Name="ResultId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Result" Type="String"></asp:Parameter>
                        <asp:Parameter Name="MethodId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Method" Type="String"></asp:Parameter>
                        <asp:Parameter Name="Pass" Type="Boolean"></asp:Parameter>
                        <asp:Parameter Name="StartRange" Type="Decimal"></asp:Parameter>
                        <asp:Parameter Name="EndRange" Type="Decimal"></asp:Parameter>
                        <asp:Parameter Name="Value" Type="String"></asp:Parameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="COATestId" Type="Int32"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="RadPageView2">
                <telerik:RadGrid ID="RadGridCOADetail" runat="server" DataSourceID="ObjectDataSourceCOADetail" OnItemCommand="RadGridCOADetail_ItemCommand"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="COADetailId"
                        Width="100%" CommandItemDisplay="Top" DataSourceID="ObjectDataSourceCOADetail"
                        AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridEditCommandColumn InsertText="<%$ Resources:Default, Insert %>" EditText="<%$ Resources:Default, Edit %>" UpdateText="<%$ Resources:Default, Update %>" CancelText="<%$ Resources:Default, Cancel %>"></telerik:GridEditCommandColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListNoteId" 
                                ListTextField="Note" ListValueField="NoteId"
                                DataSourceID="ObjectDataSourceNote" DataField="NoteId" 
                                HeaderText="<%$ Resources:Default, Note %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn DataField="NoteCode" HeaderText="<%$ Resources:Default, NewNoteCode %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Note" HeaderText="<%$ Resources:Default, NewNote %>"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceCOADetail" runat="server" TypeName="COA"
                    SelectMethod="SelectCOADetail" InsertMethod="InsertCOADetail" UpdateMethod="UpdateCOADetail" DeleteMethod="DeleteCOADetail">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="COAId" Type="Int32" SessionField="COAId" />
                        <asp:SessionParameter Name="StorageUnitId" Type="Int32" SessionField="StorageUnitId" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="COAId" Type="Int32" SessionField="COAId" />
                        <asp:Parameter Name="COADetailId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="NoteId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="NoteCode" Type="String"></asp:Parameter>
                        <asp:Parameter Name="Note" Type="String"></asp:Parameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="COAId" Type="Int32" SessionField="COAId" />
                        <asp:Parameter Name="COADetailId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="NoteId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="NoteCode" Type="String"></asp:Parameter>
                        <asp:Parameter Name="Note" Type="String"></asp:Parameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="COADetailId" Type="Int32"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    
    <asp:ObjectDataSource ID="ObjectDataSourceTest" TypeName="COA" SelectMethod="GetTests" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceResult" TypeName="COA" SelectMethod="GetResults" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceMethod" TypeName="COA" SelectMethod="GetMethods" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceNote" TypeName="COA" SelectMethod="GetNotes" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
