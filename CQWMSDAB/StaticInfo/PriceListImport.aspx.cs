using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using Microsoft.Reporting.WebForms;
using System.Net;
using System.Security.Principal;
using System.Runtime.InteropServices;

public partial class StaticInfo_PriceListImport : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";
    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        Master.MsgText = "The file was successfully imported.";
        Master.ErrorText = "";

        if (FileUpload1.HasFile)
        {
            try
            {
                string xml = "";
                System.IO.Stream s = FileUpload1.PostedFile.InputStream;
                BulkImport import = new BulkImport();

                using (StreamReader sr = new StreamReader(s))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] tmpArr = line.Split(Convert.ToChar(","));
                        if (tmpArr.Length >= 4)
                        {
                            xml = "<root>";

                            xml = xml + "<Line>";
                            int count = 0;

                            xml = xml + "<PricingCategoryCode>" + tmpArr[count] + "</PricingCategoryCode>";
                            count++;
                            xml = xml + "<ProductCode>" + tmpArr[count] + "</ProductCode>";
                            count++;
                            xml = xml + "<SKUCode>" + tmpArr[count] + "</SKUCode>";
                            count++;
                            xml = xml + "<RRP>" + tmpArr[count] + "</RRP>";
                            count++;
                            
                            xml = xml + "</Line>";

                            xml = xml + "</root>";
                            
                            if (!import.PriceListImport(Session["ConnectionStringName"].ToString(), xml))
                            {
                                Master.MsgText = "";
                                Master.ErrorText = "There was an processing the file : " + Server.MapPath(FileUpload1.FileName).ToString() + " " + xml.ToString();
                                break;
                            }
                        }
                    }
                }
                s.Close();
            }
            catch (Exception ex)
            {
                Master.ErrorText = "There was an error saving the file : " + ex.Message.ToString();
            }
        }
        else
        {
            Master.ErrorText = "There was no file selected to upload";
        }
    }
    #endregion ButtonUpload_Click
    
    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            Session["ReportName"] = "Price List";

            ReportParameter[] RptParameters = new ReportParameter[3];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            //Response.Redirect("~/Reports/Report.aspx");

            // Set the processing mode for the ReportViewer to Remote
            ReportViewer1.ProcessingMode = ProcessingMode.Remote;

            // Do not prompt user for Parameters
            ReportViewer1.ShowParameterPrompts = false;

            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings.Get("UserName").ToString(),
                                                                        ConfigurationManager.AppSettings.Get("Password").ToString(),
                                                                        ConfigurationManager.AppSettings.Get("Domain").ToString());

            ReportViewer1.ServerReport.ReportServerCredentials = rvc;

            // Set the report server URL and report path
            string reportServerUrl = ConfigurationManager.AppSettings.Get("ReportServerUrl").ToString();
            this.ReportViewer1.ServerReport.ReportServerUrl = new System.Uri(reportServerUrl);

            string strReportPath = ConfigurationManager.AppSettings.Get("ReportPath").ToString();
            string strReportName = Session["ReportName"].ToString();
            string strReport = strReportPath + strReportName;
            this.ReportViewer1.ServerReport.ReportPath = strReport;

            //ReportParameter[] RptParameters = (ReportParameter[])Session["ReportParameters"];

            // Set the report parameters for the report
            ReportViewer1.ServerReport.SetParameters(RptParameters);

            // Display the Report
            this.ReportViewer1.ServerReport.Refresh();

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StaticInfo_PriceListImport" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click

    #region ReportViewerCredentials
    public class ReportViewerCredentials : IReportServerCredentials
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        public extern static bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public extern static bool DuplicateToken(IntPtr ExistingTokenHandle,
            int SECURITY_IMPERSONATION_LEVEL, ref IntPtr DuplicateTokenHandle);

        public ReportViewerCredentials()
        {
        }

        public ReportViewerCredentials(string username)
        {
            this.Username = username;
        }


        public ReportViewerCredentials(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }


        public ReportViewerCredentials(string username, string password, string domain)
        {
            this.Username = username;
            this.Password = password;
            this.Domain = domain;
        }


        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                string username = value;
                if (username.Contains("\\"))
                {
                    this.domain = username.Substring(0, username.IndexOf("\\"));
                    this.username = username.Substring(username.IndexOf("\\") + 1);
                }
                else
                {
                    this.username = username;
                }
            }
        }
        private string username;



        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }
        private string password;


        public string Domain
        {
            get
            {
                return this.domain;
            }
            set
            {
                this.domain = value;
            }
        }
        private string domain;




        #region IReportServerCredentials Members

        public bool GetBasicCredentials(out string basicUser, out string basicPassword, out string basicDomain)
        {
            basicUser = username;
            basicPassword = password;
            basicDomain = domain;
            return username != null && password != null && domain != null;
        }

        public bool GetFormsCredentials(out string formsUser, out string formsPassword, out string formsAuthority)
        {
            formsUser = username;
            formsPassword = password;
            formsAuthority = domain;
            return username != null && password != null && domain != null;

        }

        public bool GetFormsCredentials(out Cookie authCookie,
      out string user, out string password, out string authority)
        {
            authCookie = null;
            user = password = authority = null;
            return false;  // Not implemented
        }


        public WindowsIdentity ImpersonationUser
        {
            get
            {

                string[] args = new string[3] { this.Domain.ToString(), this.Username.ToString(), this.Password.ToString() };
                IntPtr tokenHandle = new IntPtr(0);
                IntPtr dupeTokenHandle = new IntPtr(0);

                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;
                const int SecurityImpersonation = 2;

                tokenHandle = IntPtr.Zero;
                dupeTokenHandle = IntPtr.Zero;
                try
                {
                    // Call LogonUser to obtain an handle to an access token.
                    bool returnValue = LogonUser(args[1], args[0], args[2],
                        LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                        ref tokenHandle);

                    if (false == returnValue)
                    {
                        Console.WriteLine("LogonUser failed with error code : {0}",
                            Marshal.GetLastWin32Error());
                        return null;
                    }

                    // Check the identity.
                    System.Diagnostics.Trace.WriteLine("Before impersonation: "
                        + WindowsIdentity.GetCurrent().Name);


                    bool retVal = DuplicateToken(tokenHandle, SecurityImpersonation, ref dupeTokenHandle);
                    if (false == retVal)
                    {
                        CloseHandle(tokenHandle);
                        Console.WriteLine("Exception in token duplication.");
                        return null;
                    }


                    // The token that is passed to the following constructor must 
                    // be a primary token to impersonate.
                    WindowsIdentity newId = new WindowsIdentity(dupeTokenHandle);
                    WindowsImpersonationContext impersonatedUser = newId.Impersonate();


                    // Free the tokens.
                    if (tokenHandle != IntPtr.Zero)
                        CloseHandle(tokenHandle);
                    if (dupeTokenHandle != IntPtr.Zero)
                        CloseHandle(dupeTokenHandle);

                    // Check the identity.
                    System.Diagnostics.Trace.WriteLine("After impersonation: "
                        + WindowsIdentity.GetCurrent().Name);

                    return newId;

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception occurred. " + ex.Message);
                }

                return null;
            }
        }

        public ICredentials NetworkCredentials
        {
            get
            {
                return null;  // Not using NetworkCredentials to authenticate.
            }
        }


        #endregion
    }
    #endregion ReportViewerCredentials

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "StaticInfo_PriceListImport", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "StaticInfo_PriceListImport", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
