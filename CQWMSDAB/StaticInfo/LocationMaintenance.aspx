<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="LocationMaintenance.aspx.cs" Inherits="StaticInfo_LocationMaintenance"
    Title="<%$ Resources:Default, LocationMaintenance %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, LocationMaintenance %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabArea" HeaderText="<%$ Resources:Default, Area%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="updatePanel_Area" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonAddArea" runat="server" Text="<%$ Resources:Default, AddArea%>" OnClick="ButtonAddArea_Click" />
                        <asp:GridView ID="GridViewArea" DataSourceID="ObjectDataSourceArea" DataKeyNames="AreaId"
                            runat="server" AllowPaging="True" AllowSorting="True" PageSize="15" AutoGenerateColumns="False"
                            EmptyDataText="<%$ Resources:Default, CurrentlynoAreas%>" OnSelectedIndexChanged="GridViewArea_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ShowEditButton="True" ShowDeleteButton="True" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, Area%>" DataField="Area" SortExpression="Area" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, AreaCode%>" DataField="AreaCode"
                                    SortExpression="AreaCode" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, StockOnHand%>" SortExpression="StockOnHand">
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="CheckBoxStockOnHand" runat="server" Checked='<%# Bind("StockOnHand") %>' />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelStockOnHand" runat="server" Text='<%# Bind("StockOnHand") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Replenish%>" SortExpression="Replenish">
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="CheckBoxReplenish" runat="server" Checked='<%# Bind("Replenish") %>' />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelReplenish" runat="server" Text='<%# Bind("Replenish") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="<%$ Resources:Default, AreaSequence %>" DataField="AreaSequence" SortExpression="AreaSequence" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, AreaType %>" DataField="AreaType" SortExpression="AreaType" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, WarehouseCode %>" DataField="WarehouseCode" SortExpression="WarehouseCode" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, MinimumShelfLife %>" DataField="MinimumShelfLife" SortExpression="MinimumShelfLife" />
                            </Columns>
                        </asp:GridView>
                        <asp:DetailsView Visible="false" ID="DetailsViewInsertArea" runat="server" AutoGenerateRows="False"
                            DataSourceID="ObjectDataSourceArea" DefaultMode="Insert">
                            <Fields>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AreaId%>" InsertVisible="False"
                                    SortExpression="AreaId">
                                    <InsertItemTemplate>
                                        <asp:Button ID="AddArea" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Add%>" />
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Area%>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextArea" runat="server" Text='<%# Bind("area") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                    <FooterStyle Wrap="False" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AreaCode %>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextAreaCode" runat="server" Text='<%# Bind("areaCode") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, StockOnHand%>">
                                    <InsertItemTemplate>
                                        <asp:CheckBox ID="CheckBoxStockOnHand" runat="server" Checked='<%# Bind("StockOnHand") %>' />
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Replenish%>">
                                    <InsertItemTemplate>
                                        <asp:CheckBox ID="CheckBoxReplenish" runat="server" Checked='<%# Bind("Replenish") %>' />
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AreaSequence %>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextAreaSequence" runat="server" Text='<%# Bind("AreaSequence") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AreaType %>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextAreaType" runat="server" Text='<%# Bind("AreaType") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, WarehouseCode %>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextWarehouseCode" runat="server" Text='<%# Bind("WarehouseCode") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, MinimumShelfLife %>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextMinimumShelfLife" runat="server" Text='<%# Bind("MinimumShelfLife") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <InsertItemTemplate>
                                        <asp:LinkButton ID="btnInsert" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Insert%>"
                                            OnClick="btnInsert_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                            Text="<%$ Resources:Default, Cancel%>" OnClick="btnCancel_Click"></asp:LinkButton>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                        <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="StaticInfo"
                            SelectMethod="Area_Search" UpdateMethod="Area_Update" InsertMethod="Area_Add"
                            DeleteMethod="Area_Delete" OnInserting="ObjectDataSourceArea_Inserting" OnInserted="ObjectDataSourceArea_Inserted">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:Parameter Name="area" Type="string" />
                                <asp:Parameter Name="areaCode" Type="string" />
                                <asp:Parameter Name="stockOnHand" Type="Boolean" />
                                <asp:Parameter Name="areaId" Type="Int32" />
                                <asp:Parameter Name="Replenish" Type="Boolean" />
                                <asp:Parameter Name="AreaSequence" Type="Int32" />
                                <asp:Parameter Name="AreaType" Type="String" />
                                <asp:Parameter Name="WarehouseCode" Type="String" />
                                <asp:Parameter Name="MinimumShelfLife" Type="Decimal" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:Parameter Name="area" Type="string" />
                                <asp:Parameter Name="areaCode" Type="string" />
                                <asp:Parameter Name="stockOnHand" Type="string" />
                                <asp:Parameter Name="areaId" Type="Int32" />
                                <asp:Parameter Name="Replenish" Type="Boolean" />
                                <asp:Parameter Name="AreaSequence" Type="Int32" />
                                <asp:Parameter Name="AreaType" Type="String" />
                                <asp:Parameter Name="WarehouseCode" Type="String" />
                                <asp:Parameter Name="MinimumShelfLife" Type="Decimal" />
                            </InsertParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="areaId" Type="Int32" />
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Location%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="updatePanel_Location" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PanelLocation" runat="server" Width="450px" Visible="false">
                            <asp:Button ID="ButtonLocation" runat="server" Text="<%$ Resources:Default, AddLocation%>"
                                OnClick="ButtonAddLocation_Click" />
                            <asp:GridView ID="GridViewLocation" DataSourceID="ObjectDataSourceLocation" DataKeyNames="LocationId"
                                runat="server" AllowPaging="True" AllowSorting="True" PageSize="15" AutoGenerateColumns="False"
                                EmptyDataText="<%$ Resources:Default, NoLocationsforArea%>">
                                <Columns>
                                    <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, LocationType%>" SortExpression="LocationType">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownListLocationType" runat="server" DataSourceID="ObjectdatsourceLocationType"
                                                DataTextField="LocationType" DataValueField="LocationTypeId" SelectedValue='<%# Bind("LocationTypeId") %>'>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("LocationType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Location%>" DataField="Location"
                                        SortExpression="Location" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Aisle%>" DataField="Ailse" SortExpression="Ailse" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Column%>" DataField="Column" SortExpression="Column" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Level%>" DataField="Level" SortExpression="Level" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, PalletQuantity%>" DataField="PalletQuantity"
                                        SortExpression="PalletQuantity" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, SecurityCode%>" DataField="SecurityCode"
                                        SortExpression="SecurityCode" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, RelativeValue%>" DataField="RelativeValue"
                                        SortExpression="RelativeValue" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, NumberOfSUB%>" DataField="NumberOfSUB"
                                        SortExpression="NumberOfSUB" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, StockTake%>" SortExpression="StocktakeInd">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("StocktakeInd") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="CheckBox1" runat="server" Text='<%# Bind("StocktakeInd") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ActiveBinning%>" SortExpression="ActiveBinning">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("ActiveBinning") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="CheckBox2" runat="server" Text='<%# Bind("ActiveBinning") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ActivePicking%>" SortExpression="ActivePicking">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="CheckBox3" runat="server" Checked='<%# Bind("ActivePicking") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="CheckBox3" runat="server" Text='<%# Bind("ActivePicking") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Used%>" SortExpression="Used">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="CheckBox4" runat="server" Checked='<%# Bind("Used") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="CheckBox4" runat="server" Text='<%# Bind("Used") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Area%>" SortExpression="Area">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownAreaList" runat="server" SelectedValue='<%# Bind("AreaId") %>'
                                                DataSourceID="ObjectDataSourceAreaList" DataTextField="Area" DataValueField="AreaId" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="TextBoxArea" runat="server" Text='<%# Bind("Area") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:DetailsView Visible="false" ID="DetailsViewInsertLocation" runat="server" AutoGenerateRows="False"
                                DataSourceID="ObjectDataSourceLocation" DefaultMode="Insert">
                                <Fields>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, LocationId%>" InsertVisible="False"
                                        SortExpression="LocationId">
                                        <InsertItemTemplate>
                                            <asp:Button ID="AddArea" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Add%>" />
                                        </InsertItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, LocationType%>">
                                        <InsertItemTemplate>
                                            <asp:DropDownList ID="DropDownListLocationType" runat="server" DataSourceID="ObjectdatsourceLocationType"
                                                DataTextField="LocationType" DataValueField="LocationTypeId" SelectedValue='<%# Bind("LocationTypeId") %>'>
                                            </asp:DropDownList>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Location%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxLocation" runat="server" Text='<%# Bind("Location") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Aisle%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxAilse" runat="server" Text='<%# Bind("Ailse") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Column%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxColumn" runat="server" Text='<%# Bind("Column") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Level%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxLevel" runat="server" Text='<%# Bind("Level") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, PalletQuantity%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxPalletQuantity" runat="server" Text='<%# Bind("PalletQuantity") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, SecurityCode%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxSecurityCode" runat="server" Text='<%# Bind("SecurityCode") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, RelativeValue%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxRelativeValue" runat="server" Text='<%# Bind("RelativeValue") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, NumberOfSUB%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxNumberOfSUB" runat="server" Text='<%# Bind("NumberOfSUB") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, StockTake%>">
                                        <InsertItemTemplate>
                                            <asp:CheckBox ID="CheckBoxStocktakeInd" runat="server" Checked='<%# Bind("StocktakeInd") %>' />
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ActiveBinning%>">
                                        <InsertItemTemplate>
                                            <asp:CheckBox ID="CheckBoxActiveBinning" runat="server" Checked='<%# Bind("ActiveBinning") %>' />
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ActivePicking%>">
                                        <InsertItemTemplate>
                                            <asp:CheckBox ID="CheckBoxActivePicking" runat="server" Checked='<%# Bind("ActivePicking") %>' />
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Used%>">
                                        <InsertItemTemplate>
                                            <asp:CheckBox ID="CheckBoxUsed" runat="server" Checked='<%# Bind("Used") %>' />
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <InsertItemTemplate>
                                            <asp:LinkButton ID="btnlocInsert" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Insert%>"
                                                OnClick="btnlocInsert_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="btnlocCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                                Text="<%$ Resources:Default, Cancel%>" OnClick="btnlocCancel_Click"></asp:LinkButton>
                                        </InsertItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>
                        </asp:Panel>
                        <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="StaticInfo"
                            SelectMethod="Location_Search" UpdateMethod="Location_Edit" DeleteMethod="Location_Delete"
                            InsertMethod="Location_Add" OnInserting="ObjectDataSourceLocation_Inserting"
                            OnInserted="ObjectDataSourceLocation_Inserted">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="areaId" SessionField="AreaId" Type="Int32" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="locationTypeId" Type="Int32" DefaultValue="1" />
                                <asp:Parameter Name="location" Type="string" />
                                <asp:Parameter Name="ailse" Type="string" DefaultValue="1" />
                                <asp:Parameter Name="column" Type="string" DefaultValue="1" />
                                <asp:Parameter Name="level" Type="string" DefaultValue="1" />
                                <asp:Parameter Name="palletQuantity" Type="Int32" DefaultValue="1" />
                                <asp:Parameter Name="securityCode" Type="string" DefaultValue="99" />
                                <asp:Parameter Name="relativeValue" Type="Decimal" DefaultValue="1" />
                                <asp:Parameter Name="numberofSub" Type="Int32" DefaultValue="1" />
                                <asp:Parameter Name="stockTakeInd" Type="string" DefaultValue="0" />
                                <asp:Parameter Name="activeBinning" Type="string" DefaultValue="1" />
                                <asp:Parameter Name="activePicking" Type="string" DefaultValue="1" />
                                <asp:Parameter Name="used" Type="string" DefaultValue="0" />
                                <asp:Parameter Name="locationId" Type="Int32" />
                                <asp:Parameter Name="areaId" Type="Int32" />
                            </UpdateParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="locationId" Type="Int32" />
                                <asp:SessionParameter Name="areaId" SessionField="AreaId" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="locationTypeId" Type="Int32" />
                                <asp:Parameter Name="location" Type="string" />
                                <asp:Parameter Name="ailse" Type="string" />
                                <asp:Parameter Name="column" Type="string" />
                                <asp:Parameter Name="level" Type="string" />
                                <asp:Parameter Name="palletQuantity" Type="Int32" />
                                <asp:Parameter Name="securityCode" Type="string" DefaultValue="99" />
                                <asp:Parameter Name="relativeValue" Type="Decimal" DefaultValue="1" />
                                <asp:Parameter Name="numberofSub" Type="Int32" DefaultValue="1" />
                                <asp:Parameter Name="stockTakeInd" Type="string" DefaultValue="0" />
                                <asp:Parameter Name="activeBinning" Type="string" DefaultValue="1" />
                                <asp:Parameter Name="activePicking" Type="string" DefaultValue="1" />
                                <asp:Parameter Name="used" Type="string" DefaultValue="0" />
                                <asp:Parameter Name="locationId" Type="Int32" />
                                <asp:SessionParameter Name="areaId" SessionField="AreaId" Type="Int32" />
                            </InsertParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectdatsourceLocationType" runat="server" TypeName="LocationType"
                            SelectMethod="GetLocationTypes">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceAreaList" runat="server" TypeName="StaticInfo"
                            SelectMethod="Area_Search">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanelPrincipalZone" HeaderText="Principal Area Link">
            <HeaderTemplate>
                Area Principal
            </HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelPrincipalZone" runat="server">
                    <ContentTemplate>
                        <table border="solid" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Principal" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="Linked Areas" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text=" " Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Unlinked Areas" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="updatePanel_Principal" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                                            <asp:GridView ID="GridViewPrincipal" DataSourceID="ObjectDataSourcePrincipal"
                                                DataKeyNames="PrincipalId" runat="server" AllowPaging="True" AllowSorting="True"
                                                PageSize="15" AutoGenerateColumns="False" EmptyDataText="There are currently no Principals..."
                                                OnSelectedIndexChanged="GridViewPrincipal_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" ShowEditButton="False" ShowDeleteButton="False"
                                                        ShowCancelButton="False" />
                                                    <asp:BoundField HeaderText="PrincipalId" DataField="PrincipalId" SortExpression="PrincipalId"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="PrincipalCode" DataField="PrincipalCode" SortExpression="PrincipalCode"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="Principal" DataField="Principal" SortExpression="Principal" />
                                                    <asp:BoundField HeaderText="RestrictedAreaIndicator" DataField="RestrictedAreaIndicator"
                                                        SortExpression="RestrictedAreaIndicator" Visible="False" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:DetailsView Visible="false" ID="DetailsViewInsertAreaPrincipal" runat="server"
                                                AutoGenerateRows="False" DataSourceID="ObjectDataSourcePrincipal" OnLoad="DetailsViewInsertAreaPrincipal_Load">
                                                <Fields>
                                                    <asp:TemplateField HeaderText="PrincipalId" InsertVisible="False" SortExpression="PrincipalId">
                                                        <InsertItemTemplate>
                                                            <asp:Button ID="AddPrincipal" runat="server" CommandName="Insert" Text="Add" />
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PrincipalCode" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextPrincipalCode" runat="server" Text='<%# Bind("PrincipalCode") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="RestrictedAreaIndicator" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextRestrictedAreaIndicator" runat="server" Text='<%# Bind("RestrictedAreaIndicator") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                </Fields>
                                            </asp:DetailsView>
                                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                                SelectMethod="ListPrincipals">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceAreaSel" runat="server" TypeName="Area" SelectMethod="GetAreasPrincipalSel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="principalId" SessionField="PrincipalId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceAreaUnsel" runat="server" TypeName="Area"
                                                SelectMethod="GetAreaPrincipalUnsel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="principalId" SessionField="PrincipalId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListArea" runat="server" DataSourceID="ObjectDataSourceAreaSel"
                                                DataTextField="Area" DataValueField="AreaId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="LabelErrorMsg" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectArea" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectArea" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectArea" runat="server" Text="Select All" OnClick="ButtonSelectArea_Click" />
                                    <asp:Button ID="ButtonDeselectArea" runat="server" Text="Deselect All" OnClick="ButtonDeselectArea_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:Button ID="Button4" runat="server" Text=">>" OnClick="ButtonLinkAreaUnsel_Click" />
                                    <br />
                                    <br />
                                    <asp:Button ID="Button3" runat="server" Text="<<" OnClick="ButtonLinkArea_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListAreaUnsel" runat="server" DataSourceID="ObjectDataSourceAreaUnsel"
                                                DataTextField="Area" DataValueField="AreaId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="Label3" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectAreaUnsel" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectAreaUnsel" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectAreaUnsel" runat="server" Text="Select All" OnClick="ButtonSelectAreaUnsel_Click" />
                                    <asp:Button ID="ButtonDeselectAreaUnsel" runat="server" Text="Deselect All" OnClick="ButtonDeselectAreaUnsel_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Button4" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="Button3" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
