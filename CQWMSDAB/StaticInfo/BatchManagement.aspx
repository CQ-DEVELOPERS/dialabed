<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BatchManagement.aspx.cs" Inherits="StaticInfo_BatchManagement" Title="Batch Management"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/BatchManagementSearch.ascx" TagName="BatchManagementSearch" TagPrefix="ucbms" %>
<%@ Register Src="../Common/BatchMaintenace.ascx" TagName="BatchMaintenace" TagPrefix="uc2" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, Batch%>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, SelectaBatch%>">
            <ContentTemplate>
                <ucbms:BatchManagementSearch ID="BatchManagementSearch1" runat="server" />
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search %>" OnClick="ButtonSearch_Click" style="Width:auto;" />
                <asp:Button ID="ButtonPrintLocations" runat="server" Text="<%$ Resources:Default, PrintLocations %>"
                    OnClick="ButtonPrintLocations_Click" style="Width:auto;"/>
                <table>
                    <tr>
                        <td>
                            <asp:Image ID="ImageGreen" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        </td>
                        <td>
                            <asp:Label ID="LabelGreen" runat="server" 
                                Text="<%$ Resources:Default, Active %>"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        </td>
                        <td>
                            <asp:Label ID="LabelYellow" runat="server" 
                                Text="<%$ Resources:Default, NonActiveQC %>"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <asp:Image ID="ImageBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                        </td>
                        <td>
                            <asp:Label ID="LabelBlue" runat="server" 
                                Text="<%$ Resources:Default, LimitedSLLT60 %>"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        </td>
                        <td>
                            <asp:Label ID="LabelRed" runat="server" 
                                Text="<%$ Resources:Default, Expired %>"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div style="clear: left;">
                </div>
                <br />
                <asp:UpdatePanel ID="UpdatePanelGridViewBatches" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewBatchManagement1" runat="server" DataSourceID="ObjectDataSourceBatch"
                            AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false" DataKeyNames="BatchId,Batch,StorageUnitId"
                            OnSelectedIndexChanged="GridViewBatchManagement1_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "Indicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ExpiryDate" HeaderText="<%$ Resources:Default, ExpiryDate %>"
                                    SortExpression="ExpiryDate"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Remaining" HeaderText="<%$ Resources:Default, Remaining %>"
                                    SortExpression="Remaining"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                    SortExpression="Quantity"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewBatchManagement1" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                    SelectMethod="SearchBatchesDate">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="String" />
                        <asp:SessionParameter Name="BatchNumber" SessionField="BatchNumber" Type="String" />
                        <asp:SessionParameter Name="ProductCode" SessionField="ProductCode" Type="String" />
                        <asp:SessionParameter Name="Product" SessionField="Product" Type="String" />
                        <asp:SessionParameter Name="BatchStatusId" SessionField="BatchStatusId" Type="Int32" />
                        <asp:SessionParameter Name="DaysoldLt" SessionField="DaysoldLt" Type="Int32" />
                        <asp:SessionParameter Name="DaysoldGt" SessionField="DaysoldGt" Type="Int32" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="SOHOnly" SessionField="SOHOnly" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="<%$ Resources:Default, UpdateaBatch%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <uc2:BatchMaintenace ID="BatchMaintenace1" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel3" HeaderText="<%$ Resources:Default, LinkProducts%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <h4>
                                Search</h4>
                        </td>
                        <td>
                            <h4>
                                Linked</h4>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="right">
                                        <asp:Button ID="ButtonSearchLinkedProducts" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearchLinkedProducts_Click" />
                                    </td>
                                </tr>
                            </table>
                            <asp:UpdatePanel ID="UpdatePanelUnlinkedProducts" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewUnlinkedProducts" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        DataKeyNames="StorageUnitId" DataSourceID="ObjectDataSourceLinkedProducts"
                                        OnSelectedIndexChanged="GridViewUnlinkedProducts_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Add%>" ShowSelectButton="True" />
                                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product%>" />
                                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode%>" />
                                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode%>" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <h5>No rows</h5>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceLinkedProducts" runat="server" TypeName="Batch" SelectMethod="SearchUnlinkedProducts">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                            <asp:ControlParameter Name="Product" ControlID="TextBoxProduct" Type="String" />
                                            <asp:ControlParameter Name="ProductCode" ControlID="TextBoxProductCode" Type="String" />
                                            <asp:SessionParameter Name="BatchId" SessionField="BatchId" Type="Int32" DefaultValue="-1" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonSearchLinkedProducts" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="GridViewUnlinkedProducts" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanelLinkedProducts" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewProductSearch" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        DataKeyNames="StorageUnitBatchId" DataSourceID="ObjectDataSourceProduct"
                                        OnSelectedIndexChanged="GridViewProductSearch_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Remove%>" ShowSelectButton="True" />
                                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product%>" />
                                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode%>" />
                                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode%>" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <h5>No rows</h5>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="GridViewUnlinkedProducts" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <%--
                <asp:UpdatePanel ID="UpdatePanelBatchFields" runat="server">
                    <ContentTemplate>
                        <asp:DetailsView ID="DetailsViewInsertBatchFields" runat="server" AutoGenerateRows="TRUE" DataSourceID="ObjectDataSourceBatchFields" DefaultMode="Insert"  >
                            <Fields>
                            </Fields>
                        </asp:DetailsView>
                    </ContentTemplate>
                </asp:UpdatePanel>--%>
    <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Batch"
        SelectMethod="LinkedProducts">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="String" />
            <asp:SessionParameter Name="BatchId" SessionField="BatchId" Type="int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <%--    <asp:ObjectDataSource ID="ObjectDataSourceBatchFields" runat="server" TypeName="Batch" SelectMethod="GetBatchFields">   
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>   --%>
</asp:Content>
