﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

public partial class StaticInfo_OperatorGroupInstructionTypeMaint : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewOperatorGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOperatorGroup_SelectedIndexChanged";
        try
        {
            Session["OperatorGroupId"] = GridViewOperatorGroup.SelectedDataKey["OperatorGroupId"];

        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionTypeOperatorGroupMaintenance" + "_" + ex.Message.ToString());
        }
    }


    protected void ObjectDataSourceOperatorGroup_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertOperatorGroupInstructionType.Visible = false;
        GridViewOperatorGroup.Visible = true;

        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_OperatorGroup, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceOperatorGroup_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string OperatorGroupCode = ((TextBox)DetailsViewInsertOperatorGroupInstructionType.FindControl("DetailTextOperatorGroupCode")).Text;
        string RestrictedAreaIndicator = ((TextBox)DetailsViewInsertOperatorGroupInstructionType.FindControl("DetailTextRestrictedAreaIndicator")).Text;

        e.InputParameters["OperatorGroupCode"] = OperatorGroupCode;
        e.InputParameters["RestrictedAreaIndicator"] = RestrictedAreaIndicator;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertOperatorGroupInstructionType.Visible = false;
        GridViewOperatorGroup.Visible = true;
    }

    protected void DetailsViewInsertOperatorGroupInstructionType_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    #region ButtonSelectInstructionType_Click
    protected void ButtonSelectInstructionType_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectInstructionType_Click";
        try
        {
            foreach (ListItem item in CheckBoxListInstructionType.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectInstructionType_Click

    #region ButtonSelectInstructionTypeUnsel_Click
    protected void ButtonSelectInstructionTypeUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectInstructionTypeUnsel_Click";
        try
        {
            foreach (ListItem item in CheckBoxListInstructionTypeUnsel.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectInstructionTypeUnsel_Click

    #region ButtonDeselectInstructionType_Click
    protected void ButtonDeselectInstructionType_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectInstructionType_Click";

        try
        {
            foreach (ListItem item in CheckBoxListInstructionType.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectInstructionTypeUnsel_Click

    #region ButtonDeselectInstructionTypeUnsel_Click
    protected void ButtonDeselectInstructionTypeUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectInstructionTypeUnsel_Click";

        try
        {
            foreach (ListItem item in CheckBoxListInstructionTypeUnsel.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectInstructionTypeUnsel_Click

    #region ButtonLinkInstructionType_Click

    protected void ButtonLinkInstructionType_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectInstructionType_Click";
        try
        {
            List<int> instructionTypeList = new List<int>();

            foreach (ListItem item in CheckBoxListInstructionTypeUnsel.Items)
            {
                if (item.Selected == true)
                {
                    instructionTypeList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int instructionType in instructionTypeList)
            {
                InstructionType.InstructionTypeOperatorGroup_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["OperatorGroupId"]), instructionType);
            }

            CheckBoxListInstructionTypeUnsel.DataBind();
            CheckBoxListInstructionType.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionTypeOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkInstructionType_Click

    #region ButtonLinkInstructionTypeUnsel_Click

    protected void ButtonLinkInstructionTypeUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectInstructionType_Click";
        try
        {
            List<int> instructionTypeList = new List<int>();

            foreach (ListItem item in CheckBoxListInstructionType.Items)
            {
                if (item.Selected == true)
                {
                    instructionTypeList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int instructionType in instructionTypeList)
            {
                InstructionType.InstructionTypeOperatorGroup_Delete(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["OperatorGroupId"]), instructionType);
            }

            CheckBoxListInstructionTypeUnsel.DataBind();
            CheckBoxListInstructionType.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstructionTypeOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkInstructionTypeUnsel_Click
}
