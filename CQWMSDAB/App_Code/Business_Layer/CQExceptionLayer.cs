using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

/// <summary>
/// Summary description for CQExceptionLayer
/// </summary>
/// 

public class CQExceptionLayer
{

    #region private variables
    //private StringBuilder strBuilder = new StringBuilder();
    private string result = "Exception";
    private int countErrors = -1;

    #endregion private variables

    #region Construct
    public CQExceptionLayer()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion Construct

    #region Outbound
    
    #region Generic Outbound Error Handling
    /// <summary>
    /// 
    /// </summary>
    /// <param name="priorityLevel">1</param>
    /// <param name="errpage">Default.aspx</param>
    /// <param name="errMethod">Page_Load</param>
    /// <param name="errMsg">Session Variables</param>
    /// <returns></returns>
    public string GenericOutboundErrorHandling(int intId, string ex, string someMessage)
      
    {
        try
        {
            DateTime createDate = DateTime.Now;      

            //ErrorReporting errReport = new ErrorReporting();
            //result = errReport.SetupErrorReporting(priorityLevel, "Outbound", errpage, errMethod, errMsg, DateTime.Now);
            
            /*
            result = errReport.SetupErrorReporting(priorityLevel,
                                                   "Outbound",
                                                   errpage,
                                                   errMethod,
                                                   errMsg,
                                                   createDate);
            */
        }
        catch 
        {
            //result = SendErrorNow(ex.Message.ToString());
        }

        return result;

    }

    public string GenericOutboundErrorHandling(string connectionStringName,
                                                int priorityLevel,
                                                string errpage,
                                                string errMethod,
                                                string errMsg)
    {
        try
        {
            DateTime createDate = DateTime.Now;      

            ErrorReporting errReport = new ErrorReporting();
            errReport.SetupErrorReporting(connectionStringName, priorityLevel, "Outbound", errpage, errMethod, errMsg, DateTime.Now);
            
            /*
            result = errReport.SetupErrorReporting(priorityLevel,
                                                   "Outbound",
                                                   errpage,
                                                   errMethod,
                                                   errMsg,
                                                   createDate);
            */
        }
        catch 
        {
            //result = SendErrorNow(ex.Message.ToString());
        }

        return result;

    }

  
  

    #endregion Generic Outbound Error Handling  

    #endregion Outbound

    #region Inbound
    
    #region SendError

    public string GenericInboundErrorHandling(int priorityLevel,
                                               string errpage,
                                               string errMethod,
                                               string errMsg)
    {
        try
        {
            /*
            DateTime createDate = DateTime.Now;

            ErrorReporting erReport = new ErrorReporting();

            result = erReport.SetupErrorReporting(priorityLevel,
                                                  "Inbound",
                                                  errpage,
                                                  errMethod,
                                                  errMsg,
                                                  createDate);
             * */
        }
        catch 
        {
            // result = SendErrorNow(ex.Message.ToString());
        }
        return result;
    
    }


    public string SendInboundErrorNow(int intId, string ex, string something)
    {
        try
        {

            /*
            DateTime createDate = DateTime.Now;

            ErrorReporting erReport = new ErrorReporting();

            result = erReport.SetupErrorReporting(priorityLevel,
                                                  "Inbound",
                                                  errpage,
                                                  errMethod,
                                                  errMsg,
                                                  createDate);
             * */
        }
        catch 
        {
            // result = SendErrorNow(ex.Message.ToString());
        }
        return result;
    }

    #endregion SendError

    #endregion Inbound

    #region SendError
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ex">ex</param>
    /// <param name="inOrout">Inbound or Outbound</param>
    /// <param name="errMethod">SendInboundErrorNow</param>
    /// <returns></returns>
    private string SendErrorNow(string ex, string inOrout, string errMethod)
    {
        try
        {
            //result = GenericOutboundErrorHandling(0, "CQExceptionLayer_GenericOutboundErrorHandling" + "_" + ex, "101");
        }
        catch 
        {
            if (countErrors <= 0)
            {
                //result = SendOutboundErrorNow(exMsg.Message.ToString());
            }
            else
            {
                /*
                DateTime createDate = DateTime.Now;

                ErrorReporting erReport = new ErrorReporting();

                result = erReport.SetupErrorReporting(1,
                                                      inOrout,
                                                      "CQExceptionLayer",
                                                      errMethod,
                                                      errMsg.Message,
                                                      createDate); 
                 */
            }
               

        }
        return result;
    }
    #endregion "Send Error Now"
}
