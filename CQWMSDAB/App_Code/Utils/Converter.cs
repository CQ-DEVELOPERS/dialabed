﻿using System;

/// <summary>
/// 
/// </summary>
public static class Converter
{
    /// <summary>
    /// String2s the int.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static int String2Int(object value)
    {
        return String2Int(value, -1);
    }

    /// <summary>
    /// String2s the int.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    public static int String2Int(object value, int defaultValue)
    {
        try
        {
            int tempValue;
            int OutputValue;
            string stringValue;
            if (value == null)
                return defaultValue;
            else
                stringValue = value.ToString();
            OutputValue = Int32.TryParse(stringValue, out tempValue) ? tempValue : defaultValue;
            return OutputValue;
        }
        catch
        {
            return defaultValue;
        }
    }

    /// <summary>
    /// String2s the int16.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static short String2Int16(object value)
    {
        try
        {
            short tempValue;
            short OutputValue;
            string stringValue;
            if (value == null)
                return -1;
            else
                stringValue = value.ToString();
            OutputValue = Int16.TryParse(stringValue, out tempValue) ? tempValue : Int16.Parse("0");
            return OutputValue;
        }
        catch
        {
            return -1;
        }
    }

    public static short String2Int16(object value, short defaultValue)
    {
        try
        {
            short tempValue;
            short OutputValue;
            string stringValue;
            if (value == null)
                return defaultValue;
            else
                stringValue = value.ToString();
            OutputValue = Int16.TryParse(stringValue, out tempValue) ? tempValue : Int16.Parse("0");
            return OutputValue;
        }
        catch
        {
            return defaultValue;
        }
    }

    /// <summary>
    /// Object2s the string.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static string Object2String(object value)
    {
        if (value == null)
            return "";
        else
            return value.ToString().Trim();

    }

    /// <summary>
    /// String2s the nullable int.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static int? String2NullableInt(object value)
    {
        try
        {
            int tempValue;
            int? OutputValue = null;
            string stringValue;
            if (value == null)
                return null;
            else
                stringValue = value.ToString();
            OutputValue = int.TryParse(stringValue, out tempValue) ? (int?)tempValue : null;
            return OutputValue;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    /// <summary>
    /// String2s the datetime.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static DateTime String2Datetime(object value)
    {
        try
        {
            DateTime tempValue;
            DateTime OutputValue = DateTime.Now;
            string stringValue;
            if (value == null)
                return OutputValue;
            else
                stringValue = value.ToString();
            OutputValue = DateTime.TryParse(stringValue, out tempValue) ? (DateTime)tempValue : DateTime.Now;
            return OutputValue;
        }
        catch (Exception ex)
        {
            return DateTime.Now;
        }
    }

    /// <summary>
    /// String2s the nullable datetime.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static DateTime? String2NullableDatetime(object value)
    {
        try
        {
            DateTime tempValue;
            DateTime? OutputValue = null;
            string stringValue;
            if (value == null)
                return null;
            else
                stringValue = value.ToString();
            OutputValue = DateTime.TryParse(stringValue, out tempValue) ? (DateTime?)tempValue : null;
            return OutputValue;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    /// <summary>
    /// String2s the nullable short.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static short? String2NullableShort(object value)
    {
        try
        {
            short tempValue;
            short? OutputValue = null;
            string stringValue;
            if (value == null)
                return null;
            else
                stringValue = value.ToString();
            OutputValue = short.TryParse(stringValue, out tempValue) ? (short?)tempValue : null;
            return OutputValue;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    /// <summary>
    /// String2s the bool.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static bool String2Bool(object value)
    {
        try
        {
            bool tempValue;
            bool OutputValue;
            string stringValue;
            if (value == null)
                return false;
            else
                stringValue = value.ToString();
            OutputValue = Boolean.TryParse(stringValue, out tempValue) ? tempValue : false;
            return OutputValue;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    /// <summary>
    /// String2s the double.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static double String2Double(object value)
    {
        try
        {
            double tempValue;
            double OutputValue;
            string stringValue;
            if (value == null)
                return 0.00;
            else
                stringValue = value.ToString();
            OutputValue = Double.TryParse(stringValue, out tempValue) ? tempValue : 0.00;
            return OutputValue;
        }
        catch
        {
            return 0.00;
        }
    }

    /// <summary>
    /// String2s the double.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    /// This element was inserted by Venkat on machine VENKAT-PC.
    /// 11/1/2015:4:30 PM
    public static double String2Double(object value, double defaultValue)
    {
        try
        {
            double tempValue;
            double OutputValue;
            string stringValue;
            if (value == null)
                return defaultValue;
            else
                stringValue = value.ToString();
            OutputValue = Double.TryParse(stringValue, out tempValue) ? tempValue : defaultValue;
            return OutputValue;
        }
        catch
        {
            return defaultValue;
        }
    }

    /// <summary>
    /// String2s the decimal.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static decimal String2Decimal(object value)
    {
        try
        {
            decimal tempValue;
            decimal OutputValue;
            string stringValue;
            if (value == null)
                return 0;
            else
                stringValue = value.ToString();
            OutputValue = Decimal.TryParse(stringValue, out tempValue) ? tempValue : 0.00M;
            return OutputValue;
        }
        catch
        {
            return 0;
        }
    }

    /// <summary>
    /// String2s the nullable decimal.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static decimal? String2NullableDecimal(object value)
    {
        try
        {
            decimal tempValue;
            decimal? OutputValue = null;
            string stringValue;
            if (value == null)
                return null;
            else
                stringValue = value.ToString();
            OutputValue = decimal.TryParse(stringValue, out tempValue) ? (decimal?)tempValue : null;
            return OutputValue;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    /// <summary>
    /// Strings to short.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static short StringToShort(object value)
    {
        return StringToShort(value, "0");
    }

    /// <summary>
    /// Strings to short.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    public static short StringToShort(object value, object defaultValue)
    {
        try
        {
            short returnValue;

            Int16.TryParse((value ?? defaultValue).ToString(), out returnValue);

            return returnValue;
        }
        catch
        {
            return short.Parse((defaultValue ?? "0").ToString());
        }
    }

    /// <summary>
    /// Strings to character.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static char StringToChar(object value)
    {
        return StringToChar(value, 'A');
    }

    /// <summary>
    /// Strings to character.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    public static char StringToChar(object value, char defaultValue)
    {
        try
        {
            char returnValue;

            if (value.ToString().Trim().Length == 0)
                return defaultValue;

            Char.TryParse((value ?? defaultValue).ToString(), out returnValue);

            return returnValue;
        }
        catch
        {
            return defaultValue;
        }
    }

    /// <summary>
    /// Strings to double.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static double? StringToDouble(object value)
    {
        return StringToDouble(value, 0.00D);
    }

    /// <summary>
    /// Strings to double.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    public static double? StringToDouble(object value, double? defaultValue)
    {
        try
        {
            double returnValue;

            if (value.ToString().Trim().Length == 0)
                return defaultValue;

            Double.TryParse((value ?? defaultValue).ToString(), out returnValue);

            return returnValue;
        }
        catch
        {
            return defaultValue;
        }
    }

    /// <summary>
    /// String2s the unique identifier.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static Guid String2GUID(string value)
    {
        Guid OutputValue = Guid.Empty;
        try
        {
            if (value == null || value == "")
                return OutputValue;
            else
                return new Guid(value);
        }
        catch (Exception ex)
        {
            return OutputValue;
        }
    }
}