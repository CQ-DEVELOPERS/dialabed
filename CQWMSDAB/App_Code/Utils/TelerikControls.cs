﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.UI;
using System.Collections;
using Microsoft.Reporting.WebForms;
using System.Drawing;
using System.Data;
using System.Reflection;


public static class TelerikControls
{
    public static void ShowNotification(RadNotification NotificationControl, string Title, string Message)
    {
        switch (Title)
        {
            case "Error":
                NotificationControl.Text = Message;
                NotificationControl.TitleIcon = "Warning";
                NotificationControl.Title = Title + " Message";
                break;
            case "Alert":
                NotificationControl.Text = Message;
                NotificationControl.TitleIcon = "info";
                NotificationControl.Title = Title + " Message";
                break;
        }
        NotificationControl.Show();
    }

    public static void ManageRadWindow(RadWindow radWindow, string action, Page PageName)
    {
        string script;
        switch (action)
        {
            case "open":
                script = "function f(){$find(\"" + radWindow.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(PageName, PageName.GetType(), "key", script, true);
                break;
            case "close":
                script = "function f(){$find(\"" + radWindow.ClientID + "\").close(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(PageName, PageName.GetType(), "close", script, true);
                break;
        }
    }

    public static string GetColumnIndexByName(GridViewRow row, string SearchColumnName)
    {
        int columnIndex = 0;
        foreach (DataControlFieldCell cell in row.Cells)
        {
            if (cell.ContainingField is BoundField)
            {
                if (((BoundField)cell.ContainingField).DataField.Equals(SearchColumnName))
                {
                    break;
                }
            }
            columnIndex++;
        }
        return row.Cells[columnIndex].Text.Trim().Replace("&nbsp;", "");
    }

    public static DataTable GetDataTable(GridView dtg)
    {
        DataTable dt = new DataTable();

        // add the columns to the datatable            
        if (dtg.HeaderRow != null)
        {

            for (int i = 0; i < dtg.HeaderRow.Cells.Count; i++)
            {
                dt.Columns.Add(dtg.HeaderRow.Cells[i].Text);
            }
        }

        //  add each of the data rows to the table
        foreach (GridViewRow row in dtg.Rows)
        {
            DataRow dr;
            dr = dt.NewRow();

            for (int i = 0; i < row.Cells.Count; i++)
            {
                dr[i] = row.Cells[i].Text.Replace(" ", "").Replace("&nbsp;", "");
            }
            dt.Rows.Add(dr);
        }
        return dt;
    }

    public static ArrayList getGridColumnHeaders(RadGrid radGrid)
    {
        ArrayList listHeaders = new ArrayList();
        foreach (GridHeaderItem itm in radGrid.MasterTableView.GetItems(GridItemType.Header))
            foreach (GridColumn col in radGrid.MasterTableView.Columns)
                listHeaders.Add(col.HeaderText);
        return listHeaders;
    }

    public static void setColorReadOnlyColumn(RadGrid radGrid)
    {
        foreach (GridColumn col in radGrid.MasterTableView.Columns)
        {
            switch (col.ColumnType)
            {
                case "GridTemplateColumn":
                    if (!col.IsEditable)
                        col.ItemStyle.ForeColor = Color.Red;
                    break;
                case "GridBoundColumn":
                    if (!col.IsEditable)
                        col.ItemStyle.ForeColor = Color.Red;
                    break;
            }
        }



        foreach (GridColumn col in radGrid.MasterTableView.Columns)
        {
            GridBoundColumn fg = new GridBoundColumn();

        }
    }

    public static List<string> getReadOnlyProperties(RadGrid radGrid, string primaryKey)
    {
        //primaryKey = primaryKey + ",ModifiedDate";
        List<string> listReadOnlyCoulumns = new List<string>();
        foreach (GridHeaderItem itm in radGrid.MasterTableView.GetItems(GridItemType.Header))
            foreach (GridColumn col in radGrid.MasterTableView.Columns)
                if (!col.IsEditable && col.HeaderText != primaryKey && col.HeaderText != "ModifiedDate")
                    listReadOnlyCoulumns.Add(col.UniqueName);
        /*{
            if (col.HeaderText == "StatusId")
                primaryKey = primaryKey + "";
            if (!col.IsEditable && !primaryKey.Contains(col.HeaderText))
                listReadOnlyCoulumns.Add(col.UniqueName);
        }*/

        return listReadOnlyCoulumns;
    }

    public static List<string> getReadOnlyProperties(Type classType, string[] Keys, string primaryKey)
    {
        IList<PropertyInfo> props = new List<PropertyInfo>(classType.GetProperties());
        List<string> listReadOnlyCoulumns = new List<string>();
        foreach (PropertyInfo prop in props)
        {
            string colName = prop.Name;
            if (!Keys.Contains<string>(colName) && colName != primaryKey && !prop.PropertyType.Name.Contains("ICollection"))
                listReadOnlyCoulumns.Add(colName);
        }
        return listReadOnlyCoulumns;
    }

    public static void ResizeGridColumns(RadGrid radGrid)
    {
        foreach (GridColumn column in radGrid.MasterTableView.AutoGeneratedColumns)
            column.HeaderStyle.Width = Unit.Pixel(150);
        foreach (GridColumn column in radGrid.MasterTableView.Columns)
        {
            switch (column.ColumnType)
            {
                case "GridCheckBoxColumn":
                    column.HeaderStyle.Width = Unit.Pixel(80);
                    break;
                case "GridButtonColumn":
                    column.HeaderStyle.Width = Unit.Pixel(75);
                    break;
                case "GridTemplateColumn":
                    if (column.UniqueName != "TemplateButtonColumn")
                        column.HeaderStyle.Width = Unit.Pixel(150);
                    else
                        column.HeaderStyle.Width = Unit.Pixel(75);
                    break;
                case "GridBoundColumn":
                    if (column.UniqueName == "Indicator")
                        column.HeaderStyle.Width = Unit.Pixel(30);
                    else
                        column.HeaderStyle.Width = Unit.Pixel(150);
                    break;
                default:
                    column.HeaderStyle.Width = Unit.Pixel(200);
                    break;
            }
        }
    }

    public static void setValidationState(RadGrid radGrid)
    {
        foreach (GridColumn column in radGrid.MasterTableView.Columns)
        {
            switch (column.ColumnType)
            {
                case "GridBoundColumn":

                    /*
                    //column.HeaderStyle.Width = Unit.Pixel(80);
                    GridEditableItem item = e.Item as GridEditableItem;
                    GridTextBoxColumnEditor editor = (GridTextBoxColumnEditor)item.EditManager.GetColumnEditor("OrderNumber");
                    TableCell cell = (TableCell)editor.TextBoxControl.Parent;
                    RequiredFieldValidator validator = new RequiredFieldValidator();
                    validator.ControlToValidate = editor.TextBoxControl.ID;
                    validator.ErrorMessage = "*";
                    //validator.ForeColor = 
                        
                    cell.Controls.Add(validator);*/
                    break;
                case "GridButtonColumn":
                    column.HeaderStyle.Width = Unit.Pixel(75);
                    break;
                default:
                    column.HeaderStyle.Width = Unit.Pixel(150);
                    break;
            }
        }
    }

    public static bool fieldidExist(RadGrid radGrid, int fieldId, string filedName)
    {
        bool exist = false;
        foreach (GridDataItem gdt in radGrid.MasterTableView.Items)
        {
            //int SUId = Converter.String2Int(gdt[filedName].Text);
            //if (SUId == fieldId)
            //    exist = true;
        }
        return exist;
    }

    public static void getDoubleClickItemValues(RadGrid radGrid, GridDataItem selectedItem)
    {
        Table table = new Table();
        foreach (GridColumn column in radGrid.MasterTableView.Columns)
        {
            TableRow tableRow = new TableRow();
            //var cell1 = new HtmlTableCell("th") { InnerText = column.UniqueName };
            //var cell2 = new HtmlTableCell("th") { selectedItem[column.UniqueName].Text };
            //tableRow.Cells.Add(cell1);

            string dsad = selectedItem[column.UniqueName].Text;
        }
    }

    public static ReportParameter[] GetReportParameterValues(List<string> Parameters, List<string> listMappingParams, RadGrid radGrid)
    {
        try
        {
            ReportParameter[] RptParameters = new ReportParameter[Parameters.Count];
            List<GridDataItem> Items = (from item in radGrid.MasterTableView.Items.Cast<GridDataItem>()
                                        where item.Selected
                                        select item).ToList();
            if (Items != null && Items.Count > 0)
            {
                for (int i = 0; i < Parameters.Count; i++)
                {
                    switch (Parameters[i].ToLower())
                    {
                        case "connectionstring":
                            string strReportConnectionString = HttpContext.Current.Session["ReportConnectionString"].ToString();
                            RptParameters[i] = new ReportParameter("ConnectionString", strReportConnectionString);
                            break;
                        case "servername":
                            RptParameters[i] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", HttpContext.Current.Session["ServerName"].ToString());
                            break;
                        case "warehouseid":
                            RptParameters[i] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", HttpContext.Current.Session["WarehouseId"].ToString());
                            break;
                        case "databasename":
                            RptParameters[i] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", HttpContext.Current.Session["DatabaseName"].ToString());
                            break;
                        case "username":
                            RptParameters[i] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", HttpContext.Current.Session["UserName"].ToString());
                            break;
                        default:
                            bool found = (from d in radGrid.MasterTableView.RenderColumns select d).Any(d => d.UniqueName == listMappingParams[i]);
                            if (found)
                                RptParameters[i] = new ReportParameter(Parameters[i], ((GridDataItem)Items[0])[listMappingParams[i]].Text);
                            else if (listMappingParams[i].Substring(listMappingParams[i].Length - 2).ToLower() == "id")
                                RptParameters[i] = new ReportParameter(listMappingParams[i], "-1");
                            else
                            {
                                RptParameters[i] = new ReportParameter(listMappingParams[i], "-1");
                            }
                            break;
                    }
                }
            }
            return RptParameters;
        }

        catch (Exception e)
        {
            if (e.InnerException != null)
                throw e.InnerException;
            else
                throw;
        }
    }

    public static void AddComboBoxDefaultItem(RadComboBox radComboBox)
    {
        radComboBox.Items.Insert(0, new RadComboBoxItem("None", null));
    }

    public static GridDataItem GetSelectedItem(RadGrid radGrid)
    {
        GridDataItem selectedItem;
        List<GridDataItem> Items = (from item in radGrid.MasterTableView.Items.Cast<GridDataItem>()
                                    where item.Selected
                                    select item).ToList();
        if (Items != null && Items.Count > 0)
            selectedItem = Items[0];
        else
            selectedItem = null;
        return selectedItem;
    }

    public static void GridItemSelect(RadGrid radGrid, string key, string keyValue)
    {
        foreach (GridDataItem item in radGrid.MasterTableView.Items)
            if (item[key].Text == keyValue)
                item.Selected = true;
    }

    public static List<GridDataItem> GetSelectedItems(RadGrid radGrid)
    {
        GridDataItem selectedItem;
        List<GridDataItem> Items = (from item in radGrid.MasterTableView.Items.Cast<GridDataItem>()
                                    where item.Selected
                                    select item).ToList();
        return Items;
    }

    public static string GetResourceKeyValue(string resourcefileName, string keyName)
    {
        string resourceValue = string.Empty;
        try
        {
            resourceValue = (string)HttpContext.GetGlobalResourceObject(resourcefileName, keyName);
        }
        catch (Exception ex)
        {
            resourceValue = string.Empty;
        }
        return resourceValue;
    }

    //public static void AddRadGridProperties(GridTableView masterTableView, string dbTableName, string connectionStringName, int operatorId, int warehouseId, int? principalId, int? storageUnitId, int? moduleId)
    //{
    //    List<FieldConfiguration> fieldConfigurations = CQWMSBusinessLogic.Common.RequiredFieldValidationBL.GetFieldConfiguration(connectionStringName, operatorId, warehouseId, dbTableName, principalId, storageUnitId, null, moduleId);
    //    masterTableView.OwnerGrid.GroupingSettings.CaseSensitive = false;
    //    masterTableView.OwnerGrid.SortingSettings.EnableSkinSortStyles = false;
    //    masterTableView.OwnerGrid.ClientSettings.AllowColumnsReorder = true;
    //    masterTableView.OwnerGrid.ClientSettings.AllowDragToGroup = true;
    //    masterTableView.OwnerGrid.GroupingSettings.ShowUnGroupButton = true;
    //    masterTableView.OverrideDataSourceControlSorting = true;
    //    foreach (GridColumn column in masterTableView.Columns)
    //    {
    //        if (column.GetType() == typeof(GridTemplateColumn))
    //        {
    //            GridTemplateColumn clm = (GridTemplateColumn)column;
    //            clm.Groupable = true;
    //            //Enable Column Sorting
    //            clm.AllowSorting = true;
    //            clm.SortExpression = clm.DataField;
    //            clm.GroupByExpression = clm.DataField + " Group By " + clm.DataField;
    //        }
    //        else if (column.GetType() == typeof(GridBoundColumn) || column.GetType().IsSubclassOf(typeof(GridBoundColumn)))
    //        {
    //            GridBoundColumn clm = (GridBoundColumn)column;
    //            clm.Groupable = true;
    //            //Enable Column Sorting
    //            clm.AllowSorting = true;
    //            clm.SortExpression = clm.DataField;
    //            clm.GroupByExpression = clm.DataField + " Group By " + clm.DataField;

    //            var result = fieldConfigurations.Where(a => a.FieldName.ToUpper() == clm.DataField.ToUpper());

    //            if (clm.ReadOnly)
    //                masterTableView.GetColumn(clm.UniqueName).ItemStyle.CssClass = "readOnlyColumn";


    //            if (result.Count() > 0 && !clm.ReadOnly && clm.Visible)
    //            {
    //                FieldConfiguration fieldConfig = result.FirstOrDefault();
    //                clm.ColumnValidationSettings.RequiredFieldValidator.ForeColor = Color.Transparent;
    //                Style style = new Style();
    //                style.CssClass = "requiredFieldValidator";
    //                clm.ColumnValidationSettings.RequiredFieldValidator.ApplyStyle(style);
    //                clm.ColumnValidationSettings.RequiredFieldValidator.ToolTip = global::Resources.ResMessages.RequiredField;
    //                clm.ColumnValidationSettings.EnableRequiredFieldValidation = fieldConfig.FieldRequired == null ? false : fieldConfig.FieldRequired.Value;
    //                clm.ColumnValidationSettings.EnableModelErrorMessageValidation = fieldConfig.FieldRequired == null ? false : fieldConfig.FieldRequired.Value;
    //                clm.ColumnValidationSettings.RequiredFieldValidator.ErrorMessage = "*";
    //                clm.ColumnValidationSettings.RequiredFieldValidator.Enabled = fieldConfig.FieldRequired == null ? false : fieldConfig.FieldRequired.Value;

    //            }
    //            else
    //            {
    //                clm.ColumnValidationSettings.EnableRequiredFieldValidation = false;
    //                clm.ColumnValidationSettings.EnableModelErrorMessageValidation = false;
    //                clm.ColumnValidationSettings.RequiredFieldValidator.Enabled = false;
    //            }
    //        }
    //        else if (column.GetType() == typeof(GridDropDownColumn))
    //        {
    //            GridDropDownColumn dropDownColumn = (GridDropDownColumn)column;
    //            //Enable Column Sorting
    //            dropDownColumn.AllowSorting = true;
    //            //dropDownColumn.SortExpression = dropDownColumn.DataField;
    //            dropDownColumn.FilterControlAltText = "Filter " + dropDownColumn.DataField.Remove(dropDownColumn.DataField.Length - 2, 2) + " column";
    //            var result = fieldConfigurations.Where(a => a.FieldName.ToUpper() == dropDownColumn.DataField.ToUpper());
    //            if (result.Count() > 0 && column.Visible)
    //            {
    //                FieldConfiguration fieldConfig = result.FirstOrDefault();
    //                dropDownColumn.ColumnValidationSettings.RequiredFieldValidator.ForeColor = Color.Transparent;
    //                dropDownColumn.ColumnValidationSettings.RequiredFieldValidator.Font.Bold = true;
    //                Style style = new Style();
    //                style.CssClass = "requiredFieldValidator";
    //                dropDownColumn.ColumnValidationSettings.RequiredFieldValidator.ToolTip = global::Resources.ResMessages.RequiredField;
    //                dropDownColumn.ColumnValidationSettings.RequiredFieldValidator.ApplyStyle(style);
    //                dropDownColumn.ColumnValidationSettings.EnableRequiredFieldValidation = fieldConfig.FieldRequired == null ? false : fieldConfig.FieldRequired.Value;
    //                dropDownColumn.ColumnValidationSettings.EnableModelErrorMessageValidation = fieldConfig.FieldRequired == null ? false : fieldConfig.FieldRequired.Value;
    //                dropDownColumn.ColumnValidationSettings.RequiredFieldValidator.ErrorMessage = "*";
    //                dropDownColumn.ColumnValidationSettings.RequiredFieldValidator.Enabled = fieldConfig.FieldRequired == null ? false : fieldConfig.FieldRequired.Value;
    //            }
    //            else
    //            {
    //                dropDownColumn.ColumnValidationSettings.EnableRequiredFieldValidation = false;
    //                dropDownColumn.ColumnValidationSettings.EnableModelErrorMessageValidation = false;
    //                dropDownColumn.ColumnValidationSettings.RequiredFieldValidator.Enabled = false;
    //            }
    //        }
    //        else if (column.GetType() == typeof(GridButtonColumn))
    //        {
    //            GridButtonColumn clm = (GridButtonColumn)column;
    //            clm.Exportable = false;
    //        }
    //    }
    //}

    public static void ClearFilters(RadGrid radGrid)
    {
        foreach (GridColumn column in radGrid.MasterTableView.OwnerGrid.Columns)
        {
            column.CurrentFilterFunction = GridKnownFunction.NoFilter;
            column.CurrentFilterValue = string.Empty;
        }
        radGrid.MasterTableView.FilterExpression = string.Empty;
        radGrid.Rebind();
    }

    public static void EnableRequiredFieldValidation(GridTableView masterTableView, string colName)
    {
        Style style = new Style();
        style.CssClass = "requiredFieldValidator";
        switch (masterTableView.GetColumn(colName).GetType().Name)
        {
            case "GridDropDownColumn":
                //((GridDropDownColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ToolTip = global::Resources.ResMessages.RequiredField;
                ((GridDropDownColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ApplyStyle(style);
                ((GridDropDownColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ErrorMessage = "*";
                ((GridDropDownColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableRequiredFieldValidation = true;
                ((GridDropDownColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.Enabled = true;
                break;
            case "GridBoundColumn":
                //((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ToolTip = global::Resources.ResMessages.RequiredField;
                ((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ApplyStyle(style);
                ((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableRequiredFieldValidation = true;
                //((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableModelErrorMessageValidation = true;
                ((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ErrorMessage = "*";
                ((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.Enabled = true;
                break;
            case "GridNumericColumn":
                //((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ToolTip = global::Resources.ResMessages.RequiredField;
                ((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ApplyStyle(style);
                ((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableRequiredFieldValidation = true;
                //((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableModelErrorMessageValidation = true;
                ((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.ErrorMessage = "*";
                ((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.Enabled = true;
                break;
        }

    }

    public static void DisableRequiredFieldValidation(GridTableView masterTableView, string colName)
    {
        switch (masterTableView.GetColumn(colName).GetType().Name)
        {
            case "GridDropDownColumn":
                ((GridDropDownColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableRequiredFieldValidation = false;
                //((GridDropDownColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableModelErrorMessageValidation = false;
                ((GridDropDownColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.Enabled = false;
                break;
            case "GridBoundColumn":
                ((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableRequiredFieldValidation = false;
                //((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableModelErrorMessageValidation = false;
                ((GridBoundColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.Enabled = false;
                break;
            case "GridNumericColumn":
                ((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableRequiredFieldValidation = false;
                //((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.EnableModelErrorMessageValidation = false;
                ((GridNumericColumn)masterTableView.GetColumn(colName)).ColumnValidationSettings.RequiredFieldValidator.Enabled = false;
                break;
        }
    }

    public static void EnableTableGroupingSettings(GridTableView masterTableView, bool enable)
    {
        try
        {
            masterTableView.OwnerGrid.GroupPanelPosition = GridGroupPanelPosition.Top;
            masterTableView.OwnerGrid.GroupingSettings.ShowUnGroupButton = enable;
            masterTableView.OwnerGrid.ShowGroupPanel = enable;
        }
        catch
        {
            throw;
        }
    }

    public static void EndableTableSorting(GridTableView masterTableView, bool enable)
    {
        try
        {
            //masterTableView.OwnerGrid.AllowSorting = enable;
            masterTableView.AllowSorting = enable;
            masterTableView.AllowMultiColumnSorting = enable;
        }
        catch
        {
            throw;
        }
    }

    private static int GetStartOrderIndex(GridTableView masterTableView)
    {
        try
        {
            int startOrderIndex = 0;

            for (int i = 0; i < masterTableView.Columns.Count; i++)
            {
                startOrderIndex = masterTableView.Columns[i].OrderIndex;
                if (masterTableView.Columns[i].GetType() != typeof(GridTemplateColumn)
                    && masterTableView.Columns[i].GetType() != typeof(GridButtonColumn)
                    && !masterTableView.Columns[i].GetType().IsSubclassOf(typeof(GridButtonColumn)))
                {
                    if (masterTableView.Columns[i].GetType() == typeof(GridBoundColumn) || masterTableView.Columns[i].GetType().IsSubclassOf(typeof(GridBoundColumn)))
                    {
                        GridBoundColumn clm = (GridBoundColumn)masterTableView.Columns[i];
                        if (clm.ReadOnly)
                        {
                            continue;
                        }
                    }
                    break;
                }
            }

            startOrderIndex = startOrderIndex < 0 ? 0 : startOrderIndex;
            return startOrderIndex;
        }
        catch
        {
            throw;
        }
    }
}