using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Batch
/// </summary>
public class Location
{
    #region Constructor Logic
    public Location()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region ListLocations
    public DataSet ListLocations(string connectionStringName)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;

    }

    #endregion ListLocations

    #region GetLocations
    public DataSet GetLocations(string connectionStringName, Int32 warehouseId, string StartLocation, string EndLocation)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StartLocation", DbType.String, StartLocation);
        db.AddInParameter(dbCommand, "EndLocation", DbType.String, EndLocation);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;

    }
    public DataSet GetLocations(string connectionStringName, Int32 warehouseId, string fromAisle, string toAisle, string fromLevel, string toLevel)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Location_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "fromAisle", DbType.String, fromAisle);
        db.AddInParameter(dbCommand, "toAisle", DbType.String, toAisle);
        db.AddInParameter(dbCommand, "fromLevel", DbType.String, fromLevel);
        db.AddInParameter(dbCommand, "toLevel", DbType.String, toLevel);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;

    }

    #endregion GetLocations

    #region DeleteLocations
    public void DeleteLocations(String location, String aisle, String column, String level, String securityCode)
    {
        //Does nothing on purpose
    }
    #endregion DeleteLocations

    #region GetLocationsByAreaCode
    /// <summary>
    /// Searches for Area by AreaCode
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="areaCode"></param>
    /// <returns></returns>
    public DataSet GetLocationsByAreaCode(string connectionStringName, int warehouseId, string areaCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_AreaCode";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "AreaCode", DbType.String, areaCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsByAreaCode

    #region GetLocationsByAreaId
    /// <summary>
    /// Searches for Area by AreaId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="areaId"></param>
    /// <returns></returns>
    public DataSet GetLocationsByAreaId(string connectionStringName, int areaId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_AreaId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "areaId", DbType.Int32, areaId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsByAreaId

    #region GetLocationsByStorageUnitId
    /// <summary>
    /// Set Locations by StorageUnitId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <returns></returns>
    public DataSet GetLocationsByStorageUnitId(string connectionStringName, int warehouseId, int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_StorageUnitId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsByStorageUnitId

    #region GetLocationsByStorageUnitBatchId
    /// <summary>
    /// Set Locations by StorageUnitId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <returns></returns>
    public DataSet GetLocationsByStorageUnitBatchId(string connectionStringName, int warehouseId, int storageUnitBatchId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_StorageUnitBatchId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsByStorageUnitBatchId

    #region GetLocationsByIssue
    /// <summary>
    /// Searches for Area by AreaCode
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="areaCode"></param>
    /// <returns></returns>
    public DataSet GetLocationsByIssue(string connectionStringName, int warehouseId, int outboundShipmentId, int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_Issue";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsByIssue

    #region SearchLocation
    /// <summary>
    /// Searches for Area by AreaCode
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="areaCode"></param>
    /// <returns></returns>
    public DataSet SearchLocation(string connectionStringName, int warehouseId, string location)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "location", DbType.String, location);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchLocation

    #region SearchLocationStockTake
    /// <summary>
    /// Searches for Area by AreaCode
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="areaCode"></param>
    /// <returns></returns>
    public DataSet SearchLocationStockTake(string connectionStringName, int warehouseId, string location)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_Location_Stock_Take";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "location", DbType.String, location);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchLocationStockTake

    #region SearchLocationByGroup
    /// <summary>
    /// Searches for Area by AreaCode
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="areaCode"></param>
    /// <returns></returns>
    public DataSet SearchLocationByGroup(string connectionStringName, int warehouseId, string location, int stockTakeGroupId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_Location_By_Group";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "stockTakeGroupId", DbType.Int32, stockTakeGroupId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchLocationByGroup

    #region SearchLocationByAisleLevel
    /// <summary>
    /// Searches for Area by AreaCode
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="areaCode"></param>
    /// <returns></returns>
    public DataSet SearchLocationByAisleLevel(string connectionStringName, int warehouseId, string location, int stockTakeGroupId, string fromAisle, string toAisle, string fromLevel, string toLevel)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_Location_By_AisleLevel";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "stockTakeGroupId", DbType.Int32, stockTakeGroupId);
        db.AddInParameter(dbCommand, "fromAisle", DbType.String, fromAisle);
        db.AddInParameter(dbCommand, "toAisle", DbType.String, toAisle);
        db.AddInParameter(dbCommand, "fromLevel", DbType.String, fromLevel);
        db.AddInParameter(dbCommand, "toLevel", DbType.String, toLevel);


        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchLocationByAisleLevel

    #region GetDespatchBayByAreaCode
    /// <summary>
    /// Searches for Area by AreaCode
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="areaCode"></param>
    /// <returns></returns>
    public DataSet GetDespatchBayByAreaCode(string connectionStringName, int warehouseId, string areaCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DespatchBay_Search_AreaCode";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "AreaCode", DbType.String, areaCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetDespatchBayByAreaCode

    #region LocationOperatorGroup_Add
    public static bool LocationOperatorGroup_Add(string connectionStringName, int operatorGroupId, int locationId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_LocationOperatorGroup_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion LocationOperatorGroup_Add

    #region LocationOperatorGroup_Delete
    public static bool LocationOperatorGroup_Delete(string connectionStringName, int operatorGroupId, int locationId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_LocationOperatorGroup_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion LocationOperatorGroup_Delete

    #region GetLocationsSel
    /// <summary>
    /// Gets a list of Locations
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetLocationsSel(string connectionStringName, int operatorGroupId, string fromAisle, string toAisle, string fromLevel, string toLevel)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Locations_LinkedToOperatorGroup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "fromAisle", DbType.String, fromAisle);
        db.AddInParameter(dbCommand, "toAisle", DbType.String, toAisle);
        db.AddInParameter(dbCommand, "fromLevel", DbType.String, fromLevel);
        db.AddInParameter(dbCommand, "toLevel", DbType.String, toLevel);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsSel

    #region GetLocationsUnsel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetLocationsUnsel(string connectionStringName, int operatorGroupId, string fromAisle, string toAisle, string fromLevel, string toLevel)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Locations_NotLinkedToOperatorGroup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "fromAisle", DbType.String, fromAisle);
        db.AddInParameter(dbCommand, "toAisle", DbType.String, toAisle);
        db.AddInParameter(dbCommand, "fromLevel", DbType.String, fromLevel);
        db.AddInParameter(dbCommand, "toLevel", DbType.String, toLevel);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsUnsel
}

