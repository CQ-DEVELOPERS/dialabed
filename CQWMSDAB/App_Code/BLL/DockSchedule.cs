using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   24 May 2013
/// Summary description for DockSchedule
/// </summary>
public class DockSchedule
{
    #region Constructor Logic
    public int _outboundDocumentId = -1;

    public DockSchedule()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region OutboundShipmentTeamPlan
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet OutboundShipmentTeamPlan(string connectionStringName,
                                            int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Team_Plan";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "OutboundShipmentTeamPlan"

    #region InboundShipmentTeamPlan
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet InboundShipmentTeamPlan(string connectionStringName,
                                            int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Team_Plan";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "InboundShipmentTeamPlan"

    #region InboundShipmentBay
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet InboundShipmentBay(string connectionStringName,
                                            int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Bay";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "InboundShipmentBay"

    #region OutboundShipmentBay
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet OutboundShipmentBay(string connectionStringName,
                                            int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Bay";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "OutboundShipmentBay"

    #region OutboundShipmentTeamResult
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet OutboundShipmentTeamResult(string connectionStringName,
                                              int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Team_Result";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "OutboundShipmentTeamPlan"

    #region InboundShipmentTeamResult
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet InboundShipmentTeamResult(string connectionStringName,
                                              int inboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Team_Result";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "InboundShipmentTeamResult"

    #region AssignTeam
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="instructionTypeId"></param>
    /// <param name="operatorGroupId"></param>
    /// <returns></returns>
    public bool AssignTeam(string connectionStringName,
                                              int outboundShipmentId,
                                              int instructionTypeId,
                                              int operatorGroupId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Team_Assign";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "instructionTypeId", DbType.Int32, instructionTypeId);
        db.AddInParameter(dbCommand, "operatorGroupId", DbType.Int32, operatorGroupId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "AssignTeam"

    #region AssignBay
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="instructionTypeId"></param>
    /// <param name="operatorGroupId"></param>
    /// <returns></returns>
    public bool AssignBay(string connectionStringName,
                           int outboundShipmentId,
                           int locationId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Bay_Assign";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "AssignBay"

    #region UpdateBay
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="instructionTypeId"></param>
    /// <param name="operatorGroupId"></param>
    /// <returns></returns>
    public bool UpdateBay(string connectionStringName,
                           int outboundShipmentId,
                           int locationId,
                           DateTime plannedStart)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Bay_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "plannedStart", DbType.DateTime, plannedStart);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateBay"

    #region GetLocationsByShipment
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetLocationsByShipment(string connectionStringName, int warehouseId, int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_Shipment";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetLocationsByInShipment(string connectionStringName, int warehouseId, int inboundShipmentId, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_Shipment";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);
		db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsByShipment

    #region GetLocationsById
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetLocationsById(string connectionStringName, int locationId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_Id";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLocationsById

    #region DockScheduleSelect
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet DockScheduleSelect(String connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Maintenance_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet DockScheduleSelect(String connectionStringName, Int32 warehouseId, Int32 operatorId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Maintenance_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="locationId"></param>
    /// <returns></returns>
    public DataSet DockScheduleSelect(String connectionStringName, Int32 locationId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Maintenance_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion DockScheduleSelect

    #region DockScheduleInsert
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="Subject"></param>
    /// <param name="PlannedStart"></param>
    /// <param name="PlannedEnd"></param>
    /// <param name="RecurrenceRule"></param>
    /// <param name="RecurrenceParentID"></param>
    /// <param name="Description"></param>
    /// <param name="LocationId"></param>
    /// <returns></returns>
    public bool DockScheduleInsert(String connectionStringName,
                                   Int32 operatorId,
                                   String subject,
                                   DateTime plannedStart,
                                   DateTime plannedEnd,
                                   String recurrenceRule,
                                   Int32 recurrenceParentID,
                                   String description,
                                   Int32 locationId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Maintenance_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "Subject", DbType.String, subject);
        db.AddInParameter(dbCommand, "PlannedStart", DbType.DateTime, plannedStart);
        db.AddInParameter(dbCommand, "PlannedEnd", DbType.DateTime, plannedEnd);
        db.AddInParameter(dbCommand, "RecurrenceRule", DbType.String, recurrenceRule);
        db.AddInParameter(dbCommand, "RecurrenceParentID", DbType.Int32, recurrenceParentID);
        db.AddInParameter(dbCommand, "Description", DbType.String, description);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="Subject"></param>
    /// <param name="PlannedStart"></param>
    /// <param name="PlannedEnd"></param>
    /// <param name="RecurrenceRule"></param>
    /// <param name="RecurrenceParentID"></param>
    /// <param name="Description"></param>
    /// <param name="LocationId"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <returns></returns>
    public bool DockScheduleInsert(String connectionStringName,
                                   Int32 operatorId,
                                   String subject,
                                   DateTime plannedStart,
                                   DateTime plannedEnd,
                                   String recurrenceRule,
                                   Int32 recurrenceParentID,
                                   String description,
                                   Int32 locationId,
                                   Int32 outboundShipmentId,
                                   Int32 issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Maintenance_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "Subject", DbType.String, subject);
        db.AddInParameter(dbCommand, "PlannedStart", DbType.DateTime, plannedStart);
        db.AddInParameter(dbCommand, "PlannedEnd", DbType.DateTime, plannedEnd);
        db.AddInParameter(dbCommand, "RecurrenceRule", DbType.String, recurrenceRule);
        db.AddInParameter(dbCommand, "RecurrenceParentID", DbType.Int32, recurrenceParentID);
        db.AddInParameter(dbCommand, "Description", DbType.String, description);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="Subject"></param>
    /// <param name="PlannedStart"></param>
    /// <param name="PlannedEnd"></param>
    /// <param name="RecurrenceRule"></param>
    /// <param name="RecurrenceParentID"></param>
    /// <param name="Description"></param>
    /// <param name="LocationId"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <returns></returns>
    public bool DockScheduleInsertInbound(String connectionStringName,
                                   Int32 operatorId,
                                   String subject,
                                   DateTime plannedStart,
                                   DateTime plannedEnd,
                                   String recurrenceRule,
                                   Int32 recurrenceParentID,
                                   String description,
                                   Int32 locationId,
                                   Int32 inboundShipmentId,
                                   Int32 receiptId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Maintenance_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "Subject", DbType.String, subject);
        db.AddInParameter(dbCommand, "PlannedStart", DbType.DateTime, plannedStart);
        db.AddInParameter(dbCommand, "PlannedEnd", DbType.DateTime, plannedEnd);
        db.AddInParameter(dbCommand, "RecurrenceRule", DbType.String, recurrenceRule);
        db.AddInParameter(dbCommand, "RecurrenceParentID", DbType.Int32, recurrenceParentID);
        db.AddInParameter(dbCommand, "Description", DbType.String, description);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DockScheduleInsert"

    #region DockScheduleUpdate
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="Subject"></param>
    /// <param name="PlannedStart"></param>
    /// <param name="PlannedEnd"></param>
    /// <param name="RecurrenceRule"></param>
    /// <param name="RecurrenceParentID"></param>
    /// <param name="Description"></param>
    /// <param name="LocationId"></param>
    /// <returns></returns>
    public bool DockScheduleUpdate(String connectionStringName,
                                   Int32 operatorId,
                                   String subject,
                                   DateTime plannedStart,
                                   DateTime plannedEnd,
                                   String recurrenceRule,
                                   Int32 recurrenceParentID,
                                   String description,
                                   String cssClass,
                                   Int32 locationId,
                                   Int32 dockScheduleId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Maintenance_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "Subject", DbType.String, subject);
        db.AddInParameter(dbCommand, "PlannedStart", DbType.DateTime, plannedStart);
        db.AddInParameter(dbCommand, "PlannedEnd", DbType.DateTime, plannedEnd);
        db.AddInParameter(dbCommand, "RecurrenceRule", DbType.String, recurrenceRule);
        db.AddInParameter(dbCommand, "RecurrenceParentID", DbType.Int32, recurrenceParentID);
        db.AddInParameter(dbCommand, "Description", DbType.String, description);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "CssClass", DbType.String, cssClass);
        db.AddInParameter(dbCommand, "DockScheduleId", DbType.Int32, dockScheduleId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DockScheduleUpdate"

    #region DockScheduleDelete
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="Subject"></param>
    /// <param name="PlannedStart"></param>
    /// <param name="PlannedEnd"></param>
    /// <param name="RecurrenceRule"></param>
    /// <param name="RecurrenceParentID"></param>
    /// <param name="Description"></param>
    /// <param name="LocationId"></param>
    /// <returns></returns>
    public bool DockScheduleDelete(String connectionStringName,
                                   Int32 dockScheduleId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Maintenance_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "DockScheduleId", DbType.Int32, dockScheduleId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DockScheduleDelete"

    #region CssClass
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="locationId"></param>
    /// <returns></returns>
    public DataSet CssClass(String connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_CssClass_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion CssClass

    #region SearchReceipt
    /// <summary>
    /// Retreives Receipt Documents available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="orderNumber"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="principalId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public DataSet SearchReceipt(string connectionStringName,
                                        int warehouseId,
                                        int inboundDocumentTypeId,
                                        int inboundShipmentId,
                                        string externalCompanyCode,
                                        string externalCompany,
                                        string orderNumber,
                                        DateTime fromDate,
                                        DateTime toDate,
                                        int principalId,
                                        int operatorId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Receiving_Document_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, principalId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchReceipt"

    #region UpdateReceipt
    /// <summary>
    /// Updates a receipt
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <param name="allowPalletise"></param>
    /// <param name="deliveryDate"></param>
    /// <param name="locationId"></param>
    /// <param name="deliveryNoteNumber"></param>
    /// <param name="sealNumber"></param>
    /// <param name="grn"></param>
    /// <param name="priorityId"></param>
    /// <param name="vehicleRegistration"></param>
    /// <param name="vehicleTypeId"></param>
    /// <param name="remarks"></param>
    /// <param name="delivery"></param>
    /// <param name="orderNumber"></param>
    /// <param name="containerNumber"></param>
    /// <param name="containerSize"></param>
    /// <param name="shippingAgentId"></param>
    /// <param name="boe"></param>
    /// <param name="additionalText1"></param>
    /// <param name="additionalText2"></param>
    /// <returns></returns>
    public bool UpdateReceipt(string connectionStringName
                                , int inboundShipmentId
                                , int receiptId
                                , Boolean allowPalletise
                                , DateTime deliveryDate
                                , int locationId
                                , string deliveryNoteNumber
                                , string sealNumber
                                , string grn
                                , int priorityId
                                , string vehicleRegistration
                                , int vehicleTypeId
                                , string remarks
                                , int delivery
                                , string orderNumber
                                , string containerNumber
                                , string containerSize
                                , int shippingAgentId
                                , string boe
                                , string additionalText1
                                , string additionalText2
        //,int inboundShipmentId

        )
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DockSchedule_Receipt_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, priorityId);
        db.AddInParameter(dbCommand, "DeliveryNoteNumber", DbType.String, deliveryNoteNumber);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, sealNumber);
        db.AddInParameter(dbCommand, "GRN", DbType.String, grn);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, vehicleRegistration);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "ContainerNumber", DbType.String, containerNumber);
        db.AddInParameter(dbCommand, "ShippingAgentId", DbType.Int32, shippingAgentId);
        db.AddInParameter(dbCommand, "BOE", DbType.String, boe);
        db.AddInParameter(dbCommand, "AdditionalText1", DbType.String, additionalText1);
        db.AddInParameter(dbCommand, "AdditionalText2", DbType.String, additionalText2);
        db.AddInParameter(dbCommand, "vehicleTypeId", DbType.Int32, vehicleTypeId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateReceipt"
}
