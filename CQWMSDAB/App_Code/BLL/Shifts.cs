﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
/// <summary>
/// Summary description for Shifts
/// </summary>
public class Shifts
{
	 #region Constructor Logic
    public Shifts()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion Constructor Logic

    #region ListTeams
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet ListTeams(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Team_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListTeams
 
    #region GetOperatorsSel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetOperatorsSel(string connectionStringName, int teamId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_LinkedToTeam";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "TeamId", DbType.Int32, teamId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetOperatorsSel

    #region GetOperatorsUnsel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetOperatorsUnsel(string connectionStringName, int teamId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_NotLinkedToTeam";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "TeamId", DbType.Int32, teamId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetOperatorsUnsel

    #region OperatorTeam_Add
    public static bool OperatorTeam_Add(string connectionStringName, int teamId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OperatorTeam_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "TeamId", DbType.Int32, teamId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion OperatorTeam_Add

    #region OperatorTeam_Delete
    public static bool OperatorTeam_Delete(string connectionStringName, int TeamId, int OperatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OperatorTeam_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "TeamId", DbType.Int32, TeamId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion OperatorTeam_Delete

    #region GetShiftCodes
    public DataSet GetShiftCodes(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ShiftCode_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetShiftCodes

    #region GetTeams
    public DataSet GetTeams(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Team_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetTeams
}
