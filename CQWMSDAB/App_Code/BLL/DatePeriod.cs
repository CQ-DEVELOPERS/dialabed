using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

/// <summary>
/// Summary description for DatePeriod
/// </summary>
public class DatePeriod
{
	public DatePeriod()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    #region DatePeriodsLkup
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SelectDatePeriods(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_SelectDatePeriod_byID";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "@DatePeriodID", DbType.Int32, 0);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion DatePeriodsLkup
}
