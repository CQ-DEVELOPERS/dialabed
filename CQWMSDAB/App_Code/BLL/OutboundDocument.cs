using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for OutboundDocument
/// </summary>
public class OutboundDocument
{
    #region Constructor Logic
    public int _outboundDocumentId = -1;

    public OutboundDocument()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region CreateOutboundDocument
    /// <summary>
    /// Inserts an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    public int CreateOutboundDocument(string connectionStringName,
                                       int outboundDocumentTypeId,
                                       int externalCompanyId,
                                       int warehouseId,
                                       string orderNumber,
                                       DateTime deliveryDate)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundDocument_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);

        //Return the correct id for the new record
        db.AddOutParameter(dbCommand, "OutboundDocumentId", DbType.Int32, _outboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                _outboundDocumentId = (int)db.GetParameterValue(dbCommand, "@OutboundDocumentId");

                result = _outboundDocumentId;
            }
            catch { }
            connection.Close();

            return result;
        }
    }
    /// <summary>
    /// Inserts an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    public int CreateOutboundDocument(string connectionStringName,
                                       int outboundDocumentTypeId,
                                       int externalCompanyId,
                                       int warehouseId,
                                       string orderNumber,
                                       DateTime deliveryDate,
                                       string referenceNumber)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundDocument_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        //Return the correct id for the new record
        db.AddOutParameter(dbCommand, "OutboundDocumentId", DbType.Int32, _outboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                _outboundDocumentId = (int)db.GetParameterValue(dbCommand, "@OutboundDocumentId");

                result = _outboundDocumentId;
            }
            catch { }
            connection.Close();

            return result;
        }
    }
    #endregion "CreateOutboundDocument"

    #region UpdateOutboundDocument
    /// <summary>
    /// Updates an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <returns></returns>
    public bool UpdateOutboundDocument(string connectionStringName, 
                                       int OutboundDocumentId,
                                       int OutboundDocumentTypeId,
                                       int ExternalCompanyId,
                                       string OrderNumber,
                                       DateTime DeliveryDate)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundDocument_Update_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();
            
            return result;
        }
    }
    #endregion "UpdateOutboundDocument"

    #region DeleteOutboundDocument
    /// <summary>
    /// Deletes an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <returns></returns>
    public bool DeleteOutboundDocument(string connectionStringName, int OutboundDocumentId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundDocument_Remove";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();
            
            return result;
        }
    }
    #endregion "DeleteOutboundDocument"

    #region SearchOutboundDocument
    /// <summary>
    /// Searches for an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundDocumentTypeId"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchOutboundDocument(string connectionStringName,
                                            int outboundDocumentTypeId,
                                            string externalCompanyCode,
                                            string externalCompany,
                                            int warehouseId,
                                            string orderNumber,
                                            DateTime fromDate,
                                            DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundDocument_Search_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOutboundDocument"

    #region GetOutboundDocument
    /// <summary>
    /// Gets an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <returns></returns>
    public DataSet GetOutboundDocument(string connectionStringName, int OutboundDocumentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundDocument_Search_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    #endregion "GetOutboundDocument"

    #region CreateOutboundLine
    /// <summary>
    /// Inserts an OutboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="operatorId"></param>
    /// <param name="outboundDocumentId"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="quantity"></param>
    /// <param name="batchId"></param>
    /// <returns></returns>
    public bool CreateOutboundLine(string connectionStringName, 
                                    int operatorId,
                                    int outboundDocumentId,
                                    int storageUnitId,
                                    Decimal quantity,
                                    int batchId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundLine_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.String, operatorId);
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, outboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateOutboundLine"

    #region CreateOutboundLine 2
    /// <summary>
    /// Inserts an OutboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="operatorId"></param>
    /// <param name="outboundDocumentId"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="quantity"></param>
    /// <param name="batchId"></param>
    /// <returns></returns>

    public bool CreateOutboundLine(string connectionStringName,
                                   int operatorId,
                                   int outboundDocumentId,
                                   int inboundDocumentId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundLine_CreateFromPO";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.String, operatorId);
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, outboundDocumentId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    #endregion

    #region SearchOutboundLine
    /// <summary>
    /// Gets an OutboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundDocumentId"></param>
    /// <returns></returns>
    public DataSet SearchOutboundLine(string connectionStringName, int outboundDocumentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundLine_Create_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, outboundDocumentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOutboundLine"

    #region DeleteOutboundLine
    /// <summary>
    /// Deletes an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundLineId"></param>
    /// <returns></returns>
    public bool DeleteOutboundLine(string connectionStringName, int outboundLineId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundLine_Delete_Line";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, outboundLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
            connection.Close();
            
            return result;
        }
    }
    #endregion "DeleteOutboundLine"

    #region UpdateOutboundLine
    /// <summary>
    /// Updates an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundLineId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool UpdateOutboundLine(string connectionStringName, int outboundLineId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundLine_Edit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, outboundLineId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();
            
            return result;
        }
    }
    #endregion UpdateOutboundLine

    #region CreateIssue
    /// <summary>
    /// Creates Receipt and ReceiptLine records from OutboundDocument and OutboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundDocumentId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool CreateIssue(string connectionStringName, int outboundDocumentId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Despatch_Create_Issue";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, outboundDocumentId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();
            
            return result;
        }
    }
    #endregion "CreateReceipt"

    #region GetOrderNumber
    /// <summary>
    /// Retrieves a single outbound document record.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundDocumentId"></param>
    /// <returns></returns>
    public DataSet GetOrderNumber(string connectionStringName,
                                         int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundDocument_Order_Number_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderNumber"

    #region SupplierReturnComplete
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundDocumentId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool SupplierReturnComplete(string connectionStringName, int outboundDocumentId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Supplier_Return_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, outboundDocumentId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SupplierReturnComplete"
}
