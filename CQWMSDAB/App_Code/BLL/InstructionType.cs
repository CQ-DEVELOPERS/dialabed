using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for OutboundDocument
/// </summary>
public class InstructionType
{
    #region Constructor Logic
    public InstructionType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    /// <summary>
    /// Gets InstructionTypes as Parameters
    /// </summary>
    /// <returns></returns>
    #region GetInstructionTypes
    public DataSet GetInstructionTypes(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetInstructionTypes"

    #region GetInstructionTypesByCode
    public DataSet GetInstructionTypesByCode(string connectionStringName, string instructionTypeCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_By_Code";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "instructionTypeCode", DbType.String, instructionTypeCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetInstructionTypesByCode"

    #region GetInstructionTypeCode
    /// <summary>
    /// Gets InstructionTypeCode from InstructionTypeId
    /// </summary>
    /// <param name="instructionTypeId"></param>
    /// <returns></returns>
    public string GetInstructionTypeCode(string connectionStringName, int instructionTypeId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InstructionTypeCode";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        string instructionTypeCode = "";

        try
        {
            dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, instructionTypeId);

            // DataSet that will hold the returned results
            instructionTypeCode = db.ExecuteScalar(dbCommand).ToString();
        }
        catch { }

        return instructionTypeCode;
    }
    #endregion "GetInstructionTypeCode"

    #region InstructionTypeOperatorGroup_Add
    public static bool InstructionTypeOperatorGroup_Add(string connectionStringName, int operatorGroupId, int instructionTypeId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_InstructionType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, instructionTypeId); 
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        //db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, orderBy);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion InstructionTypeOperatorGroup_Add

    #region InstructionTypeOperatorGroup_Delete
    public static bool InstructionTypeOperatorGroup_Delete(string connectionStringName, int operatorGroupId, int instructionTypeId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupInstructionType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, instructionTypeId); 
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion InstructionTypeOperatorGroup_Delete

    #region GetInstructionTypesSel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetInstructionTypesSel(string connectionStringName, int operatorGroupId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_LinkedToOperatorGroup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructionTypesSel

    #region GetInstructionTypesUnsel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetInstructionTypesUnsel(string connectionStringName, int operatorGroupId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_NotLinkedToOperatorGroup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructionTypesUnsel

}
