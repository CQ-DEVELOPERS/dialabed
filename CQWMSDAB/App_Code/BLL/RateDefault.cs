﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RateDefault
/// </summary>
public class RateDefault : BaseBLL
{
  public RateDefault()
  {

  }

  /// <summary>
  /// Gets all.
  /// </summary>
  /// <returns></returns>
  public DataSet GetAll()
  {
    return ExecuteDataSet( "p_RateDefault_Select" );
  }

  public DataSet GetAllByPrincipalId( int principalId )
  {
    return ExecuteDataSet( "p_RateDefault_Search", new ParamItem( "PrincipalId", principalId ) );
  }


  /// <summary>
  /// Updates the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="vehicleTypeId">The vehicle type id.</param>
  /// <param name="name">The name.</param>
  /// <returns></returns>
  public int Update( int id, double weightInKg, double numberOfUnit, double cubeInMeters, double numberOfPallets, double numberOfCartons, double flatrate, int principalId, int areaId, int original_id )
  {
    string sql = string.Format( "p_RateDefault_{0}", original_id > 0 ? "Update" : "Insert" );

    var idParam = new ParamItem( "Id", DbType.Int32, original_id, original_id > 0 ? ParameterDirection.Input : ParameterDirection.InputOutput );

    var idValue = ExecuteNonQuery( sql, idParam
                                      , new ParamItem( "WeightInKg", weightInKg )
                                      , new ParamItem( "NumberOfUnit", numberOfUnit )
                                      , new ParamItem( "CubeInMeters", cubeInMeters )
                                      , new ParamItem( "NumberOfPallets", numberOfPallets )
                                      , new ParamItem( "NumberOfCartons", numberOfCartons )
                                      , new ParamItem( "Flatrate", flatrate )
                                      , new ParamItem( "PrincipalId", principalId )
                                      , new ParamItem( "AreaId", areaId ) );

    return (int)idParam.Value;
  }

  /// <summary>
  /// Deletes the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="original_id">The original_id.</param>
  /// <returns></returns>
  public bool Delete( int id, int original_id )
  {
    try
    {
      ExecuteNonQuery( "p_RateDefault_Delete", new ParamItem( "Id", original_id ) );
    }
    catch ( Exception ex )
    {
      return false;
    }

    return true;
  }


}