﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Rate
/// </summary>
public class Rate : BaseBLL
{
  /// <summary>
  /// Initializes a new instance of the <see cref="Rate"/> class.
  /// </summary>
  public Rate()
  {
  }

  /// <summary>
  /// Gets the rate by principal.
  /// </summary>
  /// <param name="principalId">The principal id.</param>
  /// <returns></returns>
  public DataSet GetRateByPrincipal( int principalId )
  {
    var data = ExecuteDataSet( "p_Rate_Search", new ParamItem( "PrincipalId", principalId ) );
    return data;
  }

  /// <summary>
  /// Updates the specified rate id.
  /// </summary>
  /// <param name="rateId">The rate id.</param>
  /// <param name="principalId">The principal id.</param>
  /// <param name="billingCategoryId">The billing category id.</param>
  /// <param name="warehouseId">The warehouse id.</param>
  public int Update( int id, string rateId, int principalId, int billingCategoryId, int warehouseId, int original_Id )
  {
    string sql = string.Format( "p_Rate_{0}", original_Id > 0 ? "Update" : "Insert" );

    var idParam = new ParamItem( "Id", DbType.Int32, original_Id, original_Id > 0 ? ParameterDirection.Input : ParameterDirection.InputOutput );

    var idValue = ExecuteNonQuery( sql, idParam
                                      , new ParamItem( "RateId", rateId )
                                      , new ParamItem( "PrincipalId", principalId )
                                      , new ParamItem( "BillingCategoryId", billingCategoryId )
                                      , new ParamItem( "WarehouseId", warehouseId ) );

    return (int)idParam.Value;
  }

  /// <summary>
  /// Deletes the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="original_id">The original_id.</param>
  /// <returns></returns>
  public bool Delete( int id, int original_id )
  {
    try
    {
      ExecuteNonQuery( "p_Rate_Delete", new ParamItem( "Id", original_id ) );
    }
    catch ( Exception ex )
    {
      return false;
    }

    return true;
  }
}