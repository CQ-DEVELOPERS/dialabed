﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Contact
/// </summary>
public class Contact
{
    public Contact()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region GetSalesRepByContactId
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>
    public DataSet GetSalesRepByContactId(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ContactPerson_by_ContactListId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        //db.AddInParameter(dbCommand, "ContactListId", DbType.String, contactListId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetSalesRepByContactId

    #region GetDriverByContactId
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>
    public DataSet GetDriverByContactId(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Driver_by_ContactListId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //// Add paramters
        //db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetDriverByContactId

    #region GetPRCreatorByContactId
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>
    public DataSet GetPRCreatorByContactId(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_PRCreator_by_ContactListId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //// Add paramters
        //db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetPRCreatorByContactId

    #region GetEmail
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>
    public DataSet GetEmail(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_List_EmailAddresses";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetEmail

    #region GetEmailsSel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetEmailsSel(string connectionStringName, int inboundDocumentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Email_LinkedToInboundDoc";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetEmailsSel

    #region GetEmailsUnsel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetEmailsUnsel(string connectionStringName, int inboundDocumentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Email_NotLinkedToInboundDocument";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetEmailsUnsel

    #region Contact_Search_By_ExternalCompanyId
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>
    public DataSet Contact_Search_By_ExternalCompanyId(string connectionStringName, int ExternalCompanyId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_List_Contact_By_ExternalCompanyId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion Contact_Search_By_ExternalCompanyId

    #region Contact_Edit
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>


    public static int Contact_Edit(string connectionStringName,
                                int ContactListId,
                                string ContactPerson,
                                string Telephone,
                                string Fax,
                                string EMail,
                                string ReportTypeId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ContactList_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
        db.AddInParameter(dbCommand, "ContactPerson", DbType.String, ContactPerson);
        db.AddInParameter(dbCommand, "Telephone", DbType.String, Telephone);
        db.AddInParameter(dbCommand, "Fax", DbType.String, Fax);
        db.AddInParameter(dbCommand, "EMail", DbType.Boolean, EMail);
        db.AddInParameter(dbCommand, "ReportTypeId", DbType.Int32, ReportTypeId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = int.Parse(db.GetParameterValue(dbCommand, "AreaId").ToString());
            }
            catch { }
        }
        //connection.Close();

        return result;
    }
    #endregion Contact_Edit

    #region Contact_Add
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>


    public static int Contact_Add(string connectionStringName,
                                int ContactListId,                                
                                int ExternalCompanyId,
                                string ContactPerson,
                                string Telephone,
                                string Fax,
                                string EMail,
                                int ReportTypeId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ContactList_Insert_Email";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "ContactPerson", DbType.String, ContactPerson);        
        db.AddInParameter(dbCommand, "Telephone", DbType.String, Telephone);
        db.AddInParameter(dbCommand, "Fax", DbType.String, Fax);
        db.AddInParameter(dbCommand, "EMail", DbType.String, EMail);
        db.AddInParameter(dbCommand, "ReportTypeId", DbType.Int32, ReportTypeId);

        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = int.Parse(db.GetParameterValue(dbCommand, "ContactListId").ToString());
            }
            catch { }
        }
        //connection.Close();

        return result;
    }
    #endregion Contact_Add

    #region Contact_Delete
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>


    public static bool Contact_Delete(string connectionStringName, int ContactListId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ContactList_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion Contact_Delete
}

