using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Palletise
/// </summary>
public class Palletise
{
    public Palletise()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region DeallocateReceipts
    /// <summary>
    /// De-allocates instructions locations with an ReceiptId or ReceiptLineId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptId"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public bool DeallocateReceipts(string connectionStringName, int receiptId, int receiptLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Palletise_Deallocate";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(createCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(createCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeallocateReceipts

    #region DeallocateIssues
    /// <summary>
    /// De-allocates instructions locations with an IssueId or IssueLineId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueId"></param>
    /// <param name="issueLineId"></param>
    /// <returns></returns>
    public bool DeallocateIssues(string connectionStringName, int issueId, int issueLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Palletise_Deallocate";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(createCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(createCommand, "issueLineId", DbType.Int32, issueLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeallocateIssues

    #region DeallocateOutboundShipment
    /// <summary>
    /// De-allocates instructions locations with an IssueId or IssueLineId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueId"></param>
    /// <param name="issueLineId"></param>
    /// <returns></returns>
    public bool DeallocateOutboundShipment(string connectionStringName, int outboundShipmentId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Palletise_Deallocate";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(createCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeallocateOutboundShipment

    #region DeallocateInstruction
    /// <summary>
    /// De-allocates instructions locations
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueId"></param>
    /// <param name="issueLineId"></param>
    /// <returns></returns>
    public bool DeallocateInstruction(string connectionStringName, int instructionId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Palletise_Deallocate";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(createCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeallocateInstruction
}
