﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for SalesRep
/// </summary>
public class SalesRep
{
	public SalesRep()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// Searches for Sales Rep
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContactListId"></param>
    /// <returns></returns>
    public DataSet GetSalesRepByContactId(string connectionStringName, int ContactListId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SalesRep_by_ContactListId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
}
