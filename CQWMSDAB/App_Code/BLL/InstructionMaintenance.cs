using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class InstructionMaintenance
{
    #region Constructor Logic
    public InstructionMaintenance()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    /// <summary>
    /// Searches for Store Instructions
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="statusId"></param>
    /// <returns></returns>
    public DataSet GetInstructions(string connectionStringName, int warehouseId, int palletId, int jobId, DateTime fromDate, DateTime toDate, int statusId, string productCode, string product, string skuCode, string batch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Store_Instruction_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "statusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    /// <summary>
    /// Allocates a Location to an Instruction (Automatically).
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <returns></returns>
    public bool AutoLocationAllocate(string connectionStringName, int InstructionId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    /// <summary>
    /// Allocates a Location to an Instruction (Manually selected).
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <param name="storeLocationId"></param>
    /// <returns></returns>
    public bool ManualLocationAllocate(string connectionStringName, int instructionId, int locationId)
    {
        bool result = false;
        int count = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Change_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                count = db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }

    /// <summary>
    /// Retreives a single instructions details.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public DataSet GetManualInstruction(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Instruction_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    /// <summary>
    /// Get a list of available areas for StorageUnit linked to Instruction
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public DataSet GetAreas(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_Search_Instruction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    /// <summary>
    /// Get locations and products linked to the selected Area
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="areaId"></param>
    /// <returns></returns>
    public DataSet GetLocations(string connectionStringName, int instructionId, int areaId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_StorageUnit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
}
