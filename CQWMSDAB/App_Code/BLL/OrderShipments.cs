using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Joe Black
/// Date:   20 Junie 2007
/// Summary description for OrderShipments
/// </summary>
public class OrderShipments
{

    private bool errCtrl = false;
    protected System.Data.DbType dbType = new System.Data.DbType();

    public OrderShipments()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    ///<summary>
    /// By Default the page will have a <useCaseLabel> CTRL to display all kinds of errors or loading.  This is to inform the user that somethig is going on.
    /// By Default the page will have user rights that are checked on each page in <Session Variables>
    /// By Default the page will hve Read and Edit writes that will also be displayed in <Session Variables>
    /// 
    /// * Search For the Document
    /// * Parmas consist of the following 
    
    /// * <Order shipment Number> 
    /// * <Supplier Code>
    /// * <Supplier Description>
    /// * <Document Type>
    /// * <Date Created>
    /// * <Lines Retrieved>
    /// * 
    /// Tables Affected:
    /// Inbound Document: 
    /// 
    /// //These textboxes could be labels if the text are not editable:
    /// <PANEL 1>
    /// Table Name:            ControlName:  Table[column Name]    Label[Description Name]                  Method[]
    /// Inbound Document       txtbox :   OrderNumber :            label<Order shipment Number>             <LOAD>  //This is for the ctrl to have focus as soon as the page load 
    ///                                                                                                     txtOrderNumber.Focus();  
    ///                        txtbox :   Supplier Code :          label<Supplier Code>
    ///                        txtbox :   Supplier Description :   label<Supplier Description>
    ///                        txtbox :   Document Type  :`        label<Document Type>
    ///                        txtbox :   Date Created  :`         label<Document Type>
    ///                        txtbox :   Lines Retrieved  :       label<Lines Retrieved>
    ///                        
    ///  <SP_NAMES>
    /// 
    ///  <PROCEDURES>
    ///  [getOrderByID]      Params<@OrderID>
    /// 
    ///
    /// 
    /// 
    /// <PANEL 2>
    /// 
    /// Order Lines:   
    /// 
    /// <ACTION Buttons>                    <Redirect to <Page> >
    /// [Default Delivery Qty]              "DeliveryQty.aspx"
    /// [Actual Qty]                        "ActualQty.aspx"
    /// [Print POD]                         "Print.aspx"
    /// [Link Serial Numbers]               "SerialLink.aspx"
    /// [Reject Stock]                      "RejectStock.aspx"
    /// [Re-print check sheet]              "Check Sheet"
    /// [Create Credit Advice]              "CreditAdvice.aspx"
    /// 
    /// <PANEL 3>
    /// Controls <2> Option buttons | 
    ///             <Order Number> default value 1
    ///             <Product>      default value 0
    /// 
    /// Control <1> GridView
    ///             <Fields>                <Template Fields>
    ///             Product Code                <Search By>
    ///             Product Description     
    ///             Batch Code      
    ///             Pack Code       
    ///             Expected    
    ///             Delivery Note Qty     
    ///             Actual Qty                                                                                         
    ///             Order Nubmer               <Search By>
    /// 
    /// Grid    <Select> <Insert> <Update> <Delete>
    ///         <Paging>    |YES|
    ///         <Sorting>   |YES|
    /// 
    /// <CLASS>
    /// 
    /// This class will cater for the following:
    /// Grid 
    /// <Select> @OrderNumber
    /// <Update> <Fields>
    /// <Insert> <Fields>
    /// <Delete> @OrderNumber
    /// 
    /// 
    /// </summary>


    //public System.Data.DataSet GetOrderByID(int InboundDocumentId, int OperatorId)

    public System.Data.DataSet GetOrderByID()
    {
       string spName = "p_Test_Select";
       DataSet ReceiptDataSet = new DataSet();

       try
       {

           // Database.ClearParameterCache();        
           // Create the Database object, using the default database service. The
           // default database service is determined through configuration.
           Database db = DatabaseFactory.CreateDatabase("Connection String");

           DbCommand dbCommand = db.GetStoredProcCommand(spName);

           // Retrieve products from the specified category.
           // db.AddInParameter(dbCommand, "@nboundDocumentId", DbType.Int32, InboundDocumentId);
           // db.AddInParameter(dbCommand, "@OperatorId", DbType.Int32, OperatorId);
           // DataSet that will hold the returned results		

           ReceiptDataSet = db.ExecuteDataSet(dbCommand);

           // Note: connection was closed by ExecuteDataSet method call 




       }
       catch { }


        return ReceiptDataSet;
    
    }
    public System.Data.DataSet GetOrderByID(string spName, int OrderNumber)
    {
       
        DataSet ReceiptDataSet = new DataSet();
        try
        {

            //  Database.ClearParameterCache();

            // Create the Database object, using the default database service. The
            // default database service is determined through configuration.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            DbCommand dbCommand = db.GetStoredProcCommand(spName);

            // Retrieve products from the specified category.
            // db.AddInParameter(dbCommand, "@nboundDocumentId", DbType.Int32, InboundDocumentId);
            // db.AddInParameter(dbCommand, "@OperatorId", DbType.Int32, OperatorId);
            // DataSet that will hold the returned results		

            ReceiptDataSet = db.ExecuteDataSet(dbCommand);

            // Note: connection was closed by ExecuteDataSet method call 




        }
        catch { }


        return ReceiptDataSet;

    }

    private bool UpdateOrderByID()
    {
        //UpdateDataSet
        return errCtrl;
    }

    private bool DeleteOrderByID()
    {

        //ExecuteNonQuery
        return errCtrl;
    }
}
