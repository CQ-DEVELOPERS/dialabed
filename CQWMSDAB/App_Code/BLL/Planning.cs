using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Planning
/// </summary>
public class Planning
{
    #region Constructor Logic
    public Planning()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchOrders

    public int OrdersCount(string ConnectionStringName,
                        int WarehouseId,
                        int OutboundShipmentId,
                        int OutboundDocumentTypeId,
                        string OrderNumber,
                        string ExternalCompany,
                        string ExternalCompanyCode,
                        DateTime FromDate,
                        DateTime ToDate,
                        int PrincipalId,
                        int RouteId)
    {

        int count = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = ConnectionStringName + ".dbo.p_Planning_Order_Count";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, PrincipalId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                count = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return count;
        }
    }
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchOrders(string connectionStringName,
                                int warehouseId,
                                int outboundShipmentId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet SearchOrders(string connectionStringName,
                                int warehouseId,
                                int outboundShipmentId,
                                int OutboundDocumentTypeId,
                                string OrderNumber,
                                string ExternalCompany,
                                string ExternalCompanyCode,
                                DateTime FromDate,
                                DateTime ToDate,
                                int PrincipalId,
                                int RouteId)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet SearchOrders(string connectionStringName,
                                int warehouseId,
                                int outboundShipmentId,
                                int OutboundDocumentTypeId,
                                string OrderNumber,
                                string ExternalCompany,
                                string ExternalCompanyCode,
                                DateTime FromDate,
                                DateTime ToDate,
                                int PrincipalId,
                                int RouteId,
                                int startRowIndex,
                                int maximumRows)
    {
        int pageNumber = startRowIndex == 0 ? 1 : (startRowIndex + maximumRows) / maximumRows;
        // This is for search purposes in pages.
        pageNumber = OrderNumber != null || ExternalCompany != null || ExternalCompanyCode != null || PrincipalId > 0 ? 1 : pageNumber;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "PageNum", DbType.Int32, pageNumber);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, maximumRows);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    #endregion "SearchOrders"

    #region UpdateOrder
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrder(string connectionStringName,
                            int outboundShipmentId,
                            int issueId,
                            string orderNumber,
                            int locationId,
                            int priorityId,
                            int routeId,
                            DateTime deliveryDate,
                            string remarks,
                            string areaType,
                            string AddressLine1,
                            string AddressLine2,
                            string AddressLine3,
                            string AddressLine4,
                            string AddressLine5)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "priorityId", DbType.Int32, priorityId);
        db.AddInParameter(dbCommand, "routeId", DbType.Int32, routeId);
        db.AddInParameter(dbCommand, "deliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "areaType", DbType.String, areaType);
        db.AddInParameter(dbCommand, "AddressLine1", DbType.String, AddressLine1);
        db.AddInParameter(dbCommand, "AddressLine2", DbType.String, AddressLine2);
        db.AddInParameter(dbCommand, "AddressLine3", DbType.String, AddressLine3);
        db.AddInParameter(dbCommand, "AddressLine4", DbType.String, AddressLine4);
        db.AddInParameter(dbCommand, "AddressLine5", DbType.String, AddressLine5);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrder"

    #region SearchLoads
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchLoads(string connectionStringName,
                                int warehouseId,
                                int outboundShipmentId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Load_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchLoads"

    #region UpdateLoad
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateLoad(string connectionStringName,
                            int outboundShipmentId,
                            int locationId,
                            int priorityId,
                            int routeId,
                            DateTime deliveryDate,
                            string remarks,
                            string areaType)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Load_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "priorityId", DbType.Int32, priorityId);
        db.AddInParameter(dbCommand, "routeId", DbType.Int32, routeId);
        db.AddInParameter(dbCommand, "deliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "areaType", DbType.String, areaType);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateLoad"

    #region GetOrderLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetOrderLines(string connectionStringName, int outboundShipmentId, int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Line_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderLines"

    #region SearchOrderLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchOrderLines(string connectionStringName,
                                    int warehouseId,
                                    int outboundDocumentTypeId,
                                    string orderNumber,
                                    string externalCompany,
                                    string externalCompanyCode,
                                    DateTime fromDate,
                                    DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Line_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrderLines"

    #region GetOrderDetails
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetOrderDetails(string connectionStringName,
                                    int outboundShipmentId,
                                    int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Detail_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderDetails"

    #region UpdateOrderLine
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrderLine(string connectionStringName,
                                int issueLineId,
                                int storageUnitBatchId,
                                int storageUnitId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrderLine"

    #region Palletise
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool Palletise(string connectionStringName,
                            int issueId,
                            int issueLineId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        
        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Palletise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public bool Palletise(string connectionStringName,
                            int warehouseId,
                            int outboundShipmentId,
                            int issueId,
                            int operatorId,
                            int weight,
                            int volume)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Palletise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);
        db.AddInParameter(dbCommand, "volume", DbType.Decimal, volume);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public bool Palletise(string connectionStringName,
                            int outboundDocumentId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Auto_Release";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "outboundDocumentId", DbType.Int32, outboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public bool Palletise(string connectionStringName,
                            int warehouseId,
                            int outboundShipmentId,
                            int issueId,
                            int operatorId,
                            bool bulkPick,
                            bool selectiveBulkPick,
                            bool pickByOrder)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Palletise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "bulkPick", DbType.Boolean, bulkPick);
        db.AddInParameter(dbCommand, "SelectiveBulkPickByOrder", DbType.Boolean, selectiveBulkPick);
        db.AddInParameter(dbCommand, "pickByOrder", DbType.Boolean, pickByOrder);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Palletise"

    #region SearchReleaseDocuments
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchReleaseDocuments(string connectionStringName,
                                            int warehouseId,
                                            int outboundDocumentTypeId,
                                            string orderNumber,
                                            string externalCompany,
                                            string externalCompanyCode,
                                            DateTime fromDate,
                                            DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Release_Document_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchReleaseDocuments"

    #region SearchReleaseLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchReleaseLines(string connectionStringName, int outboundShipmentId, int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Release_Lines_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchReleaseLines"

    #region AutoLocationAllocate
    /// <summary>
    /// Allocates a Location to an IssueLine (Automatically).
    /// </summary>
    public bool AutoLocationAllocate(string connectionStringName, int issueLineId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        
        db.AddInParameter(dbCommand, "issueLineId", DbType.Int32, issueLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocate"

    #region AutoLocationAllocateIssue
    /// <summary>
    /// Allocates a Location to an Issue (Automatically).
    /// </summary>
    public bool AutoLocationAllocateIssue(string connectionStringName, int issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocateIssue"

    #region AutoLocationAllocateShipment
    /// <summary>
    /// Allocates a Location to an OutboundShipment (Automatically).
    /// </summary>
    public bool AutoLocationAllocateShipment(string connectionStringName, int outboundShipmentId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocateShipment"

    #region AutoLocationAllocateInstruction
    /// <summary>
    /// Allocates a Location to an Instruction (Automatically).
    /// </summary>
    public bool AutoLocationAllocateInstruction(string connectionStringName, int instructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocateInstruction"

    #region GetBuilResults
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetBuildResults(string connectionStringName,
                                    int outboundShipmentId,
                                    int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Build_Result";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetBuilResults"

    #region GetAllocationResults
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetAllocationResults(string connectionStringName,
                                        int outboundShipmentId,
                                        int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Allocation_Result";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetAllocationResults"

    #region GetPalletisingOptions
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetPalletisingOptions(string connectionStringName,
                                    int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Palletising_Options";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetPalletisingOptions"

    #region GetSizeOptions
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetSizeOptions(string connectionStringName,
                                    int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Pallet_Size_Options";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetSizeOptions"

    #region UpdatePalletisingOptions
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdatePalletisingOptions(string connectionStringName,
                            int outboundShipmentId,
                            Boolean FP,
                            Boolean SS,
                            Boolean LP,
                            Boolean FM,
                            Boolean MP,
                            Boolean PP,
                            Boolean AlternatePallet,
                            Boolean SubstitutePallet)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Palletising_Options_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "FP", DbType.Boolean, FP);
        db.AddInParameter(dbCommand, "SS", DbType.Boolean, SS);
        db.AddInParameter(dbCommand, "LP", DbType.Boolean, LP);
        db.AddInParameter(dbCommand, "FM", DbType.Boolean, FM);
        db.AddInParameter(dbCommand, "MP", DbType.Boolean, MP);
        db.AddInParameter(dbCommand, "PP", DbType.Boolean, PP);
        db.AddInParameter(dbCommand, "AlternatePallet", DbType.Boolean, AlternatePallet);
        db.AddInParameter(dbCommand, "SubstitutePallet", DbType.Boolean, SubstitutePallet);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdatePalletisingOptions"

    #region UpdateSizeOptions
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateSizeOptions(string connectionStringName,
                                    int outboundShipmentId,
                                    decimal weight,
                                    decimal volume)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Pallet_Size_Options_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);
        db.AddInParameter(dbCommand, "volume", DbType.Decimal, volume);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateSizeOptions"

    #region "LogResponseTime"
    public bool LogResponseTime(string connectionStringName, 
        string methodName, 
        double timeinSeconds, 
        string keyName, 
        int? keyId,
        int operatorId)
    {

        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Log_ResponseTime";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "ResponseTimeId", DbType.Int32, 5);
        db.AddInParameter(dbCommand, "MethodName", DbType.String, methodName);
        db.AddInParameter(dbCommand, "ResponseTime", DbType.String, timeinSeconds.ToString());
        db.AddInParameter(dbCommand, "KeyName", DbType.String, keyName);
        db.AddInParameter(dbCommand, "KeyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = true;
            }
            catch { }
            connection.Close();
            return result;
        }
    }
    #endregion
}
