using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   16 Oct 2007
/// Summary description for Planning
/// </summary>
public class COA
{
    #region Constructor Logic
    public COA()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchCOA
    /// <summary>
    /// Search for a COA
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="skuCode"></param>
    /// <param name="batch"></param>
    /// <returns></returns>
    public DataSet SearchCOA(string connectionStringName,
                                string productCode,
                                string product,
                                string skuCode,
                                string batch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Product_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchCOA"

    #region SelectCOATest
    public DataSet SelectCOATest(string connectionStringName,
                                  int coaId,
                                  int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Test_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "coaId", DbType.Int32, coaId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectCOATest"

    #region InsertCOATest
    public bool InsertCOATest(string connectionStringName,
                              int coaId,
                              int coaTestId,
                              int testId,
                              string test,
                              int resultId,
                              string result,
                              int methodId,
                              string method,
                              Boolean pass,
                              Decimal startRange,
                              Decimal endRange,
                              string value)
    {
        bool resultVal = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Test_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "coaId", DbType.Int32, coaId);
        db.AddInParameter(dbCommand, "coaTestId", DbType.Int32, coaTestId);
        db.AddInParameter(dbCommand, "testId", DbType.Int32, testId);
        db.AddInParameter(dbCommand, "test", DbType.String, test);
        db.AddInParameter(dbCommand, "resultId", DbType.Int32, resultId);
        db.AddInParameter(dbCommand, "result", DbType.String, result);
        db.AddInParameter(dbCommand, "methodId", DbType.Int32, methodId);
        db.AddInParameter(dbCommand, "method", DbType.String, method);
        db.AddInParameter(dbCommand, "pass", DbType.Boolean, pass);
        db.AddInParameter(dbCommand, "startRange", DbType.Decimal, startRange);
        db.AddInParameter(dbCommand, "endRange", DbType.Decimal, endRange);
        db.AddInParameter(dbCommand, "value", DbType.String, value);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                resultVal = true;
            }
            catch { }

            connection.Close();

            return resultVal;
        }
    }
    #endregion "InsertCOATest"

    #region UpdateCOATest
    public bool UpdateCOATest(string connectionStringName,
                              int coaId,
                              int coaTestId,
                              int testId,
                              string test,
                              int resultId,
                              string result,
                              int methodId,
                              string method,
                              Boolean pass,
                              Decimal startRange,
                              Decimal endRange,
                              string value)
    {
        bool resultVal = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Test_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "coaId", DbType.Int32, coaId);
        db.AddInParameter(dbCommand, "coaTestId", DbType.Int32, coaTestId);
        db.AddInParameter(dbCommand, "testId", DbType.Int32, testId);
        db.AddInParameter(dbCommand, "test", DbType.String, test);
        db.AddInParameter(dbCommand, "resultId", DbType.Int32, resultId);
        db.AddInParameter(dbCommand, "result", DbType.String, result);
        db.AddInParameter(dbCommand, "methodId", DbType.Int32, methodId);
        db.AddInParameter(dbCommand, "method", DbType.String, method);
        db.AddInParameter(dbCommand, "pass", DbType.Boolean, pass);
        db.AddInParameter(dbCommand, "startRange", DbType.Decimal, startRange);
        db.AddInParameter(dbCommand, "endRange", DbType.Decimal, endRange);
        db.AddInParameter(dbCommand, "value", DbType.String, value);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                resultVal = true;
            }
            catch { }

            connection.Close();

            return resultVal;
        }
    }
    #endregion "UpdateCOATest"

    #region DeleteCOATest
    public bool DeleteCOATest(string connectionStringName,
                              int coaTestId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Test_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "coaTestId", DbType.Int32, coaTestId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteCOATest"

    #region SelectCOADetail
    public DataSet SelectCOADetail(string connectionStringName,
                                  int coaId,
                                  int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Detail_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "coaId", DbType.Int32, coaId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectCOADetail"

    #region InsertCOADetail
    public bool InsertCOADetail(string connectionStringName,
                                int coaId,
                                int coaDetailId,
                                int noteId,
                                string noteCode,
                                string note)
    {
        bool resultVal = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Detail_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "coaId", DbType.Int32, coaId);
        db.AddInParameter(dbCommand, "coaDetailId", DbType.Int32, coaDetailId);
        db.AddInParameter(dbCommand, "noteId", DbType.Int32, noteId);
        db.AddInParameter(dbCommand, "noteCode", DbType.String, noteCode);
        db.AddInParameter(dbCommand, "note", DbType.String, note);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                resultVal = true;
            }
            catch { }

            connection.Close();

            return resultVal;
        }
    }
    #endregion "InsertCOADetail"

    #region UpdateCOADetail
    public bool UpdateCOADetail(string connectionStringName,
                                int coaId,
                                int coaDetailId,
                                int noteId,
                                string noteCode,
                                string note)
    {
        bool resultVal = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Detail_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "coaId", DbType.Int32, coaId);
        db.AddInParameter(dbCommand, "coaDetailId", DbType.Int32, coaDetailId);
        db.AddInParameter(dbCommand, "noteId", DbType.Int32, noteId);
        db.AddInParameter(dbCommand, "noteCode", DbType.String, noteCode);
        db.AddInParameter(dbCommand, "note", DbType.String, note);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                resultVal = true;
            }
            catch { }

            connection.Close();

            return resultVal;
        }
    }
    #endregion "UpdateCOADetail"

    #region DeleteCOADetail
    public bool DeleteCOADetail(string connectionStringName,
                              int coaDetailId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_COA_Detail_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "coaDetailId", DbType.Int32, coaDetailId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteCOADetail"

    #region GetTests
    public DataSet GetTests(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Test_Parameter";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetTests"

    #region GetResults
    public DataSet GetResults(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Result_Parameter";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetResults"

    #region GetMethods
    public DataSet GetMethods(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Method_Parameter";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetMethods"

    #region GetNotes
    public DataSet GetNotes(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Note_Parameter";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetNotes"
}
