﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LotAttributeRule
/// </summary>
public enum LotAttributeRule
{
	None,
    SelectExisting,
    CreateOnReceipt,
    AutoGenerate
}