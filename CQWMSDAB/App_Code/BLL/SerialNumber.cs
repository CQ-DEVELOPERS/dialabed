using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   26 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class SerialNumber
{
    public SerialNumber()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataSet GetSerialDetailsByReceipt(string connectionStringName, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet GetSerialNumbersByReceipt(string connectionStringName, int receiptId, int receiptLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet GetSerialDetailsByPallet(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Search_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet GetSerialDetailsByJob(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Search_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet GetUnlinkedSerialNumbers(string connectionStringName, int jobId, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Unlinked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet GetLinkedSerialNumbers(string connectionStringName, int instructionId)
    {
        //Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Linked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet GetSerialNumbersByPallet(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    /// <summary>
    /// Inserts a SerialNumber for a Receipt.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptId"></param>
    /// <param name="serialNumber"></param>
    /// <param name="storageUnitId"></param>
    /// <returns></returns>
    public bool RegisterSerialNumber(string connectionStringName,
                                        int receiptId,
                                        int receiptLineId,
                                        string serialNumber,
                                        int storageUnitId,
                                        int batchId,
                                        string referenceNumber)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "SerialNumber", DbType.String, serialNumber);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "ReferenceNumber", DbType.String, referenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    /// <summary>
    /// Inserts a SerialNumber.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storeInstructionId"></param>
    /// <param name="serialNumber"></param>
    /// <param name="storageUnitId"></param>
    /// <returns></returns>
    public bool RegisterSerialNumberInstruction(string connectionStringName,
                                                int storeInstructionId,
                                                string serialNumber,
                                                int storageUnitId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StoreInstructionId", DbType.Int32, storeInstructionId);
        db.AddInParameter(dbCommand, "SerialNumber", DbType.String, serialNumber);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    /// <summary>
    /// Adds a Serial to an Instruction.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="serialNumberId"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public bool Transfer(string connectionStringName, int serialNumberId, int instructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Transfer";
        DbCommand transferCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(transferCommand, "SerialNumberId", DbType.Int32, serialNumberId);
        db.AddInParameter(transferCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(transferCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    /// <summary>
    /// Removes a SerialNumber from an Instruction.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="serialNumberId"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public bool Remove(string connectionStringName, int serialNumberId, int instructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Register_SerialNumber_Remove";
        DbCommand removeCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(removeCommand, "SerialNumberId", DbType.Int32, serialNumberId);
        db.AddInParameter(removeCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(removeCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }



    #region GetSerialNumber
    /// <summary>
    ///   Selects rows from the SerialNumber table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SerialNumberId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetSerialNumber
    (
        string connectionStringName,
        Int32 SerialNumberId
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "SerialNumberId", DbType.Int32, SerialNumberId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetSerialNumber
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetSerialNumber

    #region DeleteSerialNumber
    /// <summary>
    ///   Deletes a row from the SerialNumber table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SerialNumberId"></param>
    /// <returns>bool</returns>
    public bool DeleteSerialNumber
    (
        string connectionStringName,
        Int32 SerialNumberId
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "SerialNumberId", DbType.Int32, SerialNumberId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeleteSerialNumber

    #region UpdateSerialNumber
    /// <summary>
    /// Updates a row in the SerialNumber table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SerialNumberId"></param>
    /// <param name="LocationId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="ReceivedDate"></param>
    /// <param name="DespatchDate"></param>
    /// <param name="Remarks"></param>
    /// <returns></returns>
    public bool UpdateSerialNumber
    (
        string connectionStringName,
        Int32 SerialNumberId,
        String SerialNumber,
        DateTime ReceivedDate,
        DateTime DespatchDate,
        String Remarks
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "SerialNumberId", DbType.Int32, SerialNumberId);
        db.AddInParameter(dbCommand, "SerialNumber", DbType.String, SerialNumber);
        db.AddInParameter(dbCommand, "ReceivedDate", DbType.DateTime, ReceivedDate);
        db.AddInParameter(dbCommand, "DespatchDate", DbType.DateTime, DespatchDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateSerialNumber

    #region InsertSerialNumber
    /// <summary>
    /// Inserts a row into the SerialNumber table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SerialNumberId"></param>
    /// <param name="LocationId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="ReceivedDate"></param>
    /// <param name="DespatchDate"></param>
    /// <param name="Remarks"></param>
    /// <returns></returns>
    public bool InsertSerialNumber
    (
        string connectionStringName,
        Int32 SerialNumberId,
        String SerialNumber,
        DateTime ReceivedDate,
        DateTime DespatchDate,
        String Remarks
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "SerialNumberId", DbType.Int32, SerialNumberId);
        db.AddInParameter(dbCommand, "SerialNumber", DbType.String, SerialNumber);
        db.AddInParameter(dbCommand, "ReceivedDate", DbType.DateTime, ReceivedDate);
        db.AddInParameter(dbCommand, "DespatchDate", DbType.DateTime, DespatchDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InsertSerialNumber

    #region SearchSerialNumber
    /// <summary>
    /// Selects rows from the SerialNumber table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="LocationId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="BatchId"></param>
    /// <returns></returns>
    public DataSet SearchSerialNumber
    (
        string connectionStringName,
        string serialNumber,
        string productCode,
        string product,
        string skuCode,
        string sku,
        string batch,
        string location
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_SerialNumber_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "serialNumber", DbType.String, serialNumber);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "sku", DbType.String, sku);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "location", DbType.String, location);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchSerialNumber

    #region ListSerialNumber
    /// <summary>
    ///   Lists rows from the SerialNumber table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListSerialNumber
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListSerialNumber

    #region ParameterSerialNumber
    /// <summary>
    ///   Lists rows from the SerialNumber table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterSerialNumber
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ParameterSerialNumber

    #region GetSerialNumbers
    public DataSet GetSerialNumbers(string connectionStringName, int keyId, string keyType, int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Get_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "keyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "keyType", DbType.String, keyType);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetSerialNumbers

    #region GetTotal
    public DataSet GetTotal(string connectionStringName, int keyId, string keyType)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Get_Total";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "keyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "keyType", DbType.String, keyType);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetTotal(string connectionStringName, int keyId, string keyType, int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Get_Total";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "keyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "keyType", DbType.String, keyType);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetTotal

    #region SetSerialNumber
    public int SetSerialNumbers(string connectionStringName, int warehouseId, int operatorId, int keyId, string keyType, string serialNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Set_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "keyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "keyType", DbType.String, keyType);
        db.AddInParameter(dbCommand, "serialNumber", DbType.String, serialNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SetSerialNumber

    #region DeleteSerialNumber
    public int DeleteSerialNumber(string connectionStringName, int operatorId, int serialNumberId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Delete_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "serialNumberId", DbType.Int32, serialNumberId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SetSerialNumbers

    #region IsTracked
    public static bool IsTrackedReceiptLineId(string connectionStringName, int receiptLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Tracked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (bool)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public static bool IsTrackedStorageUnitId(string connectionStringName, int storageUnitId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Tracked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (bool)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }

    public static bool IsTrackedStorageUnitId(string connectionStringName, int storageUnitId, int ExternalCompanyId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Tracked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (bool)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion IsTracked
}
