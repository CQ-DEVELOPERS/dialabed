using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for PutAway
/// </summary>
public class Status
{
    #region Constructor Logic
    public Status()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region GetStatus
    public DataSet GetStatus(string connectionStringName, string type)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Status_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "Type", DbType.String, type);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet GetStatus(string connectionStringName, string type, string statusCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Status_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, statusCode);
        db.AddInParameter(dbCommand, "Type", DbType.String, type);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetStatus"

    #region GetStatusAll
    public DataSet GetStatusAll(string connectionStringName, string type)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Status_By_Type_All";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "Type", DbType.String, type);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetStatusAll"

    #region GetStatusScheduling
    public DataSet GetStatusScheduling(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Status_Scheduling";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetStatusScheduling"

    #region GetStatusInstructionMaintenance
    public DataSet GetStatusInstructionMaintenance(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Status_Instruction_Maintenance";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetStatusInstructionMaintenance"

    #region GetStatusPutawayMaintenance
    public DataSet GetStatusPutawayMaintenance(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Status_Putaway_Maintenance";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetStatusPutawayMaintenance"

    #region SetRelease
    /// <summary>
    /// Set Release Status
    /// </summary>
    /// <param name="IssueId"></param>
    /// <param name="IssueLineId"></param>
    /// <returns></returns>
    public bool SetRelease(string connectionStringName,
                           int outboundShipmentId,
                           int issueId,
                           int issueLineId,
                           string statusCode)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Release";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        
        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "statusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SetRelease"

    #region ReleaseJob
    /// <summary>
    /// Set Release Status
    /// </summary>
    /// <param name="IssueId"></param>
    /// <param name="IssueLineId"></param>
    /// <returns></returns>
    public bool ReleaseJob(string connectionStringName,
                           int jobId,
                           int operatorId,
                           string statusCode)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Release_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "statusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "ReleaseJob"

    #region SetReleaseOperator
    /// <summary>
    /// Set Release Status
    /// </summary>
    /// <param name="IssueId"></param>
    /// <param name="IssueLineId"></param>
    /// <returns></returns>
    public bool SetReleaseOperator(string connectionStringName,
                           int outboundShipmentId,
                           int issueId,
                           int OperatorId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Release_By_Operator";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SetReleaseOperator"

    #region Release
    /// <summary>
    /// Set Release Status
    /// </summary>
    /// <param name="IssueId"></param>
    /// <param name="IssueLineId"></param>
    /// <returns></returns>
    public bool Release(string connectionStringName,
                           int instructionId,
                           int operatorId,
                           string statusCode)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Movement_Release";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "statusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Release"

    #region InboundStatusChange
    /// <summary>
    /// Changes the Status of InboundShipment
    /// </summary>
    /// <param name="IssueId"></param>
    /// <param name="IssueLineId"></param>
    /// <returns></returns>
    public bool InboundStatusChange(string connectionStringName,
                           int inboundShipmentId,
                           int receiptId,
                           int receiptLineId,
                           string statusCode)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Inbound_Release";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "statusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "InboundStatusChange"

    #region Authorise
    /// <summary>
    /// Authorise Shipment / Order
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="issueId"></param>
    /// <returns></returns>
    public bool Authorise(string connectionStringName,
                           int outboundShipmentId,
                           int issueId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Authorise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    /// <summary>
    /// Authorise Job
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <returns></returns>
    public bool Authorise(string connectionStringName,
                           int jobId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Authorise_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Authorise"

    #region CheckingComplete
    /// <summary>
    /// Set Release Status
    /// </summary>
    /// <param name="IssueId"></param>
    /// <param name="IssueLineId"></param>
    /// <returns></returns>
    public bool CheckingComplete(string connectionStringName,
                           int warehouseId,
                           int outboundShipmentId,
                           int issueId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_PickChecking_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SetRelease"

    #region ResetStatus
    /// <summary>
    /// Reset the status of an instruction to Waiting
    /// </summary>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public bool ResetStatus(string connectionStringName, int instructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Reset";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "ResetStatus"

    #region SetNoStock
    /// <summary>
    /// Reset the status of an instruction to No-Stock
    /// </summary>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public bool SetNoStock(string connectionStringName, int instructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Instruction_No_Stock";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SetNoStock"

    #region Finished
    /// <summary>
    /// Set the status of instruction to Finished and allocates stock to Store Location
    /// </summary>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public bool Finished(string connectionStringName, int instructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Finished";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Finished"

    #region Print
    /// <summary>
    /// Set the status of instruction to Finished and allocates stock to Store Location
    /// </summary>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public bool Print(string connectionStringName, int instructionId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Print";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Print"

    #region PlanningComplete
    public bool PlanningComplete(string connectionStringName,
                           int outboundShipmentId,
                           int issueId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion PlanningComplete

    #region PlanningComplete
    public bool PlanningComplete(string connectionStringName, int jobId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Complete_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion PlanningComplete

    #region GetStatusId
    public int GetStatusId(string connectionStringName, string type, string statusCode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Status_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, statusCode);
        db.AddInParameter(dbCommand, "Type", DbType.String, type);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetStatusId

    #region GetAllStatusByType
    public DataSet GetAllStatusByType(string connectionStringName, string type)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_All_Status_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "Type", DbType.String, type);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetAllStatusByType"
	
	#region GetPalletStatus
    public DataSet GetPalletStatus(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Status_By_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetPalletStatus"

    #region StockAdjustment
    public bool StockAdjustment(string connectionStringName, int interfaceExportStockAdjustmentId, string statusCode, int operatorId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StockAdjustment_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "interfaceExportStockAdjustmentId", DbType.Int32, interfaceExportStockAdjustmentId);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, statusCode);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    public bool StockAdjustment(string connectionStringName, int interfaceExportStockAdjustmentId, string statusCode)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StockAdjustment_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "interfaceExportStockAdjustmentId", DbType.Int32, interfaceExportStockAdjustmentId);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion StockAdjustment
}
