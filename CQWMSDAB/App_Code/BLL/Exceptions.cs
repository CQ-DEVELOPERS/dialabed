﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Exceptions
/// </summary>
public class Exceptions
{
    #region Constructor logic
    public Exceptions()
    {
        //
        // TODO: Add Constructor logic here
        //
    }
    #endregion "Constructor logic"


    #region CreateException
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool CreateException(
        string   connectionStringName,
        int      receiptLineId,
        Decimal  quantity,
        int      operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Insert_Red_Label";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateException"

    #region CreateExceptionGreen
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool CreateExceptionGreen(
        string connectionStringName,
        int receiptLineId,
        Decimal quantity,
        int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Insert_Green_Label";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateExceptionGreen"

    #region CountRedPrint
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public int CountRedPrint(
        string connectionStringName,
        int receiptLineId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Count_Red_Labels";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = int.Parse(db.ExecuteScalar(dbCommand).ToString());
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CountRedPrint"

    #region CountGreenPrint
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public int CountGreenPrint(
        string connectionStringName,
        int receiptLineId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Count_Green_Labels";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = int.Parse(db.ExecuteScalar(dbCommand).ToString());
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CountGreenPrint"

    #region CreateLabelAuditException
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public bool CreateLabelAuditException(
        string connectionStringName,
        int jobId,
        int instructionId,
        int operatorId,
        string labelName)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Label_Audit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "LabelName", DbType.String, labelName);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateLabelAuditException"

    #region CheckIfAdministrator
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public bool CheckIfAdministrator(
        string connectionStringName,
        int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Check_OperatorGroup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CheckIfAdministrator"

    #region CheckIfApproved
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public bool CheckIfApproved(
        string connectionStringName,
        int batchId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Check_Batch_Approved";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CheckIfApproved"

    #region CountGreenPalletPrint
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public int CountGreenPalletPrint(
        string connectionStringName,
        int batchId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Count_Green_Pallet_Labels";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = int.Parse(db.ExecuteScalar(dbCommand).ToString());
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CountGreenPalletPrint"

    #region CountRedPalletPrint
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public int CountRedPalletPrint(
        string connectionStringName,
        int batchId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Count_Red_Pallet_Labels";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = int.Parse(db.ExecuteScalar(dbCommand).ToString());
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CountRedPalletPrint"

    #region CreateExceptionPalletGreen
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool CreateExceptionPalletGreen(
        string connectionStringName,
        int batchId,
        Decimal quantity,
        int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Insert_Green_Pallet_Label";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateExceptionPalletGreen"

    #region CreateExceptionPalletRed
    /// <summary>
    /// Adds an Exception record to the Exception table
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool CreateExceptionPalletRed(
        string connectionStringName,
        int batchId,
        Decimal quantity,
        int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Exception_Insert_Red_Pallet_Label";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateExceptionPalletRed"
}
