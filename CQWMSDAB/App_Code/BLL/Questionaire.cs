using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Questionaire
/// </summary>
public class Questionaire
{
	public Questionaire()
	{
		//
		// TODO: Add constructor logic here
		//

    }
    #region GetQuestionaire
    public DataSet GetQuestionaire(string connectionStringName, Int32 warehouseId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Questionaires";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;

    }

    #endregion GetQuestionaire

    #region GetQuestions
    public DataSet GetQuestions(string connectionStringName, Int32 warehouseId,Int32 QuestionaireId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Questions";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "QuestionaireId", DbType.Int32, QuestionaireId);
        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;

    }

    #endregion GetQuestions

    #region InsertQuestionaire
    public int InsertQuestionaire(string connectionStringName,
                                    int warehouseId,
                                    string QuestionaireType,
                                    string QuestionaireDesc)
    {

        int QuestionaireId = -1;
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Questionaire_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "QuestionaireType", DbType.String, QuestionaireType);
        db.AddInParameter(dbCommand, "QuestionaireDesc", DbType.String, QuestionaireDesc);

        //Add out parameter to return the OperatorId
        db.AddParameter(dbCommand, "QuestionaireId", DbType.Int32, ParameterDirection.Output, "QuestionaireId", DataRowVersion.Current, null);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                // Get output parameter
                QuestionaireId = (int)db.GetParameterValue(dbCommand, "QuestionaireId");
            }
            catch { }

            connection.Close();

            return QuestionaireId;
        }
    }
    #endregion InsertQuestionaire

    #region UpdateQuestionaire
    public bool UpdateQuestionaire(string connectionStringName,
                                    int warehouseId,
                                    int QuestionaireId,
                                    string QuestionaireType,
                                    string QuestionaireDesc)
    {

        bool Res;
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Questionaire_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "QuestionaireId", DbType.Int32, QuestionaireId);
        db.AddInParameter(dbCommand, "QuestionaireType", DbType.String, QuestionaireType);
        db.AddInParameter(dbCommand, "QuestionaireDesc", DbType.String, QuestionaireDesc);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
                Res = true;
            }
            catch { Res = false; }

            connection.Close();

            return Res;
        }
    }
    #endregion UpdateQuestionaire



    #region InsertQuestion
    public int InsertQuestion(  string connectionStringName,
                                int QuestionaireId,
	                            string Category,
	                            string Code,
	                            int Sequence,
	                            int Active,
	                            string QuestionText,
	                            string QuestionType)
    {
            
        int QuestionId = -1;
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Question_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "QuestionaireId", DbType.Int32, QuestionaireId);
        db.AddInParameter(dbCommand, "Category", DbType.String, Category);
        db.AddInParameter(dbCommand, "Code", DbType.String, Code);
        db.AddInParameter(dbCommand, "Sequence", DbType.String, Sequence);
        db.AddInParameter(dbCommand, "Active", DbType.String, Active);
        db.AddInParameter(dbCommand, "QuestionText", DbType.String, QuestionText);
        db.AddInParameter(dbCommand, "QuestionType", DbType.String, QuestionType);
        


        //Add out parameter to return the OperatorId
        db.AddParameter(dbCommand, "QuestionId", DbType.Int32, ParameterDirection.Output, "QuestionId", DataRowVersion.Current, null);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                // Get output parameter
                QuestionId = (int)db.GetParameterValue(dbCommand, "QuestionId");
            }
            catch { }

            connection.Close();

            return QuestionId;
        }
    }
    #endregion InsertQuestion


    #region UpdateQuestion
    public bool UpdateQuestion(string connectionStringName,
                                int QuestionaireId,
                                int QuestionId,
                                string Category,
                                string Code,
                                int Sequence,
                                int Active,
                                string QuestionText,
                                string QuestionType)
    {
        bool Res;
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Question_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "QuestionaireId", DbType.Int32, QuestionaireId);
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
        db.AddInParameter(dbCommand, "Category", DbType.String, Category);
        db.AddInParameter(dbCommand, "Code", DbType.String, Code);
        db.AddInParameter(dbCommand, "Sequence", DbType.String, Sequence);
        db.AddInParameter(dbCommand, "Active", DbType.String, Active);
        db.AddInParameter(dbCommand, "QuestionText", DbType.String, QuestionText);
        db.AddInParameter(dbCommand, "QuestionType", DbType.String, QuestionType);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                Res = true;
            }
            catch { Res = false; }

            connection.Close();
            return Res;
            
        }
    }
    #endregion UpdateQuestion




}


