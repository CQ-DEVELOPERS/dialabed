﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillIingEvent
/// </summary>
public class BillingEvent : BaseBLL
{
  public BillingEvent()
  {
  }

  public DataSet GetBillingEvents()
  {
    var data = ExecuteDataSet( "p_BillingEvent_Select" );

    return data;
  }


  public int Update( int id, string billingEventId, string name )
  {
    string sql = string.Format( "p_BillingEvent_{0}", id > 0 ? "Update" : "Insert" );

    var idParam = new ParamItem( "Id", DbType.Int32, id, id > 0 ? ParameterDirection.Input : ParameterDirection.InputOutput );

    var idValue = ExecuteNonQuery( sql, idParam
                                      , new ParamItem( "BillingEventId", billingEventId )
                                      , new ParamItem( "Name", name ) );

    return (int)idParam.Value;
  }

  /// <summary>
  /// Deletes the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="original_id">The original_id.</param>
  /// <returns></returns>
  public bool Delete( int id, int original_id )
  {
    try
    {
      ExecuteNonQuery( "p_BillingEvent_Delete", new ParamItem( "Id", original_id ) );
    }
    catch ( Exception ex )
    {
      return false;
    }

    return true;
  }



}