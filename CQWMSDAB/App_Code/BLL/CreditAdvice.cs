using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   26 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class CreditAdvice
{
    #region Constructor Logic
    public CreditAdvice()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetCreditAdvice
    /// <summary>
    /// Retreives Credit Advice Details for ReceiptLine.
    /// </summary>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public DataSet GetCreditAdvice(string connectionStringName, int receiptLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Credit_Advice_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        // DataSet that will hold the returned results		
        DataSet creditAdviceTypeDataSet = null;

        creditAdviceTypeDataSet = db.ExecuteDataSet(dbCommand);

        return creditAdviceTypeDataSet;
    }
    #endregion GetCreditAdvice

    #region GetCreditAdviceReject
    /// <summary>
    /// Retreives Credit Advice Details for ReceiptLine.
    /// </summary>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public DataSet GetCreditAdviceReject(string connectionStringName, int receiptLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Credit_Advice_Search_Reject";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        // DataSet that will hold the returned results		
        DataSet creditAdviceTypeDataSet = null;

        creditAdviceTypeDataSet = db.ExecuteDataSet(dbCommand);

        return creditAdviceTypeDataSet;
    }
    #endregion GetCreditAdviceReject

    #region CreditAdviceUpdate
    /// <summary>
    /// Updates an Exception
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="reasonId"></param>
    /// <param name="exception"></param>
    /// <returns></returns>
    public bool CreditAdviceUpdate(string connectionStringName,
                                    int receiptLineId,
                                    int reasonId,
                                    string exception)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Credit_Advice_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, reasonId);
        db.AddInParameter(dbCommand, "Exception", DbType.String, exception);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreditAdviceUpdate

    #region CreditAdviceRejectUpdate
    /// <summary>
    /// Upadtes Exception and Reject Quantity of ReceiptLine
    /// </summary>
    /// <param name="receiptLineId"></param>
    /// <param name="rejectQuantity"></param>
    /// <param name="reasonId"></param>
    /// <param name="exception"></param>
    /// <returns></returns>
    public bool CreditAdviceRejectUpdate(string connectionStringName,
                                            int receiptLineId,
                                            Decimal rejectQuantity,
                                            int reasonId,
                                            string exception)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Credit_Advice_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "RejectQuantity", DbType.Decimal, rejectQuantity);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, reasonId);
        db.AddInParameter(dbCommand, "Exception", DbType.String, exception);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreditAdviceRejectUpdate
}
