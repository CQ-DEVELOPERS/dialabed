using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Visual
/// </summary>
public class Visual
{
	public Visual()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #region GetReports
    public string GetReports(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Visual_Reports";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);
        
        string str = "";
        int cnt = 1;

        while (cnt < dataSet.Tables[0].Rows.Count + 1)
        {
            str = str + dataSet.Tables[0].Rows[cnt - 1][0].ToString();
            cnt++;
        }
        return str;
    }
    #endregion GetReports
	
}
