using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

/// <summary>
/// Summary description for LocationImport
/// </summary>
public class LocationImport
{
	public LocationImport()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    #region LocationImport

    public DataSet LocationImport_Search(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Wizard_Location_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static bool LocationImport_Truncate(string connectionStringName)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Wizard_Location_Truncate";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

   public bool LocationImport_Update(string connectionStringName,
   string warehouseCode,
   string area,
   string areaCode,
   string location,
   string ailse,
   string column,
   string level,
   int? palletQuantity,
   int? securityCode,
   decimal? relativeValue,
   int? numberOfSUB,
   decimal? width,
   decimal? height,
   decimal? length,
   int locationImportId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_LocationImport_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "LocationImportId", DbType.Int32, locationImportId);
        db.AddInParameter(dbCommand, "WarehouseCode", DbType.String , warehouseCode);
        db.AddInParameter(dbCommand, "Area", DbType.String, area);
        db.AddInParameter(dbCommand, "AreaCode", DbType.String, areaCode);
        db.AddInParameter(dbCommand, "Location", DbType.String, location);
        db.AddInParameter(dbCommand, "Ailse", DbType.String, ailse);
        db.AddInParameter(dbCommand, "Column", DbType.String, column);
        db.AddInParameter(dbCommand, "Level", DbType.String, level);
        db.AddInParameter(dbCommand, "PalletQuantity", DbType.Int32, palletQuantity);
        db.AddInParameter(dbCommand, "SecurityCode", DbType.Int32, securityCode);
        db.AddInParameter(dbCommand, "RelativeValue", DbType.Decimal, relativeValue);
        db.AddInParameter(dbCommand, "NumberOfSUB", DbType.Int32, numberOfSUB);
        db.AddInParameter(dbCommand, "Width", DbType.Decimal, width);
        db.AddInParameter(dbCommand, "Height", DbType.Decimal, height);
        db.AddInParameter(dbCommand, "Length", DbType.Decimal, length);
        db.AddInParameter(dbCommand, "HasError", DbType.Boolean, false);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static bool LocationImport_Delete(string connectionStringName, int locationImportId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_LocationImport_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "LocationImportId", DbType.Int32, locationImportId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    public static bool LocationImport_Add(string connectionStringName)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Wizard_Location_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    public static bool LocationImport_CheckError(string connectionStringName)
    { 
        
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Wizard_Location_CheckError";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                DataSet dataSet = null;
                dataSet = db.ExecuteDataSet(dbCommand);
                //db.ExecuteNonQuery(dbCommand);
                result = bool.Parse(dataSet.Tables[0].Rows[0]["Error"].ToString());
            }
            catch { }
        }
        //connection.Close();

        return result;
    }


    #endregion

}
