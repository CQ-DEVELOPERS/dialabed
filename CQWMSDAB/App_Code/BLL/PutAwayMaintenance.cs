using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class PutAwayMaintenance
{
    #region Constructor Logic
    public PutAwayMaintenance()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetInstructions
    /// <summary>
    /// Gets outstanding Store instructions within a date range.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="statusId"></param>
    /// <returns></returns>
    public DataSet GetInstructions(string connectionStringName, int warehouseId, int palletId, int jobId, DateTime fromDate, DateTime toDate, int statusId, string productCode, string product, string skuCode, string batch)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Putaway_Instruction_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "statusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructions

    #region UpdateInstructions
    /// <summary>
    /// Updates ConfirmedQuantity and OperatorId of Instruction
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="confirmedQuantity"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool UpdateInstructions(string connectionStringName, int instructionId, Decimal confirmedQuantity, int operatorId, int palletId, int PalletIdStatusId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Confirm";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
		db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
		db.AddInParameter(dbCommand, "PalletIdStatusId", DbType.Int32, PalletIdStatusId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion UpdateInstructions
	
	#region UpdateInstructions
    /// <summary>
    /// Updates ConfirmedQuantity and OperatorId of Instruction
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="confirmedQuantity"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool UpdateInstructions(string connectionStringName, int instructionId, Decimal confirmedQuantity, int operatorId, int palletId, int PalletIdStatusId, int JobId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Confirm";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
		db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
		db.AddInParameter(dbCommand, "PalletIdStatusId", DbType.Int32, PalletIdStatusId);
		

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion UpdateInstructions
	
    #region UpdatePutawayOperator
    /// <summary>
    /// Updates ConfirmedQuantity and OperatorId of putaway maintenance
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="confirmedQuantity"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool UpdatePutawayOperator(string connectionStringName, int instructionId, decimal confirmedQuantity, int operatorId, int palletId, int PalletIdStatusId)
    {
        bool result = false;
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        string sqlCommand = connectionStringName + ".dbo.p_Job_Operator_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "PalletIdStatusId", DbType.Int32, PalletIdStatusId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = true;
            }
            catch
            {

            }
            connection.Close();

            return result;
        }
    }
    #endregion UpdatePutawayOperator
}
