using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Scheduling
/// </summary>
public class Scheduling
{
    #region Constructor Logic
    public Scheduling()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region SearchOrders
    /// <summary>
    /// Search for Receipts
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="externalCompany"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchOrders(string connectionStringName,
                                int warehouseId,
                                int inboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Scheduling_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrders"

    #region GetOrder
    /// <summary>
    /// Returns a DataSet with the required Receipt
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public DataSet GetOrder(string connectionStringName, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Scheduling_Search_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrder"

    #region UpdateOrder
    /// <summary>
    /// Updates a Receipt
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <param name="plannedDeliveryDate"></param>
    /// <param name="actualDeliveryDate"></param>
    /// <param name="locationId"></param>
    /// <param name="statusId"></param>
    /// <returns></returns>
    public bool UpdateOrder(string connectionStringName,
                            int inboundShipmentId,
                            int receiptId,
                            DateTime plannedDeliveryDate,
                            string plannedDeliveryTime,
                            DateTime actualDeliveryDate,
                            string actualDeliveryTime,
                            int locationId,
                            int statusId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Scheduling_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "PlannedDeliveryDate", DbType.DateTime, plannedDeliveryDate);
        db.AddInParameter(dbCommand, "ActualDeliveryDate", DbType.DateTime, actualDeliveryDate);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, statusId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteDataSet(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrder"

    #region GetOrderLines
    /// <summary>
    /// Returns a DataSet with the required Receipt
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public DataSet GetOrderLines(string connectionStringName, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Scheduling_Line_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderLines"
}
