using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Area
/// </summary>
public class Area
{
    #region Constructor Logic
    public Area()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion Constructor Logic

    #region GetAreas
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreas(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreas
    
    #region GetAreasParameter
    /// <summary>
    /// Gets a list of Areas used for parameter into another proc
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreasParameter(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreasParameter

    #region GetAreasByWarehouse
    /// <summary>
    /// Gets a list of Areas by WarehouseId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreasByWarehouse(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_Search_Warehouse";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreasByWarehouse

    #region GetAreaTypes
    /// <summary>
    /// Gets a list of Areas by WarehouseId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreaTypes(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_AreaType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreaTypes

    #region GetAreasSel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreasSel(string connectionStringName, int operatorGroupId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_LinkedToOperatorGroup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreasSel

    #region GetAreasUnsel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreasUnsel(string connectionStringName, int operatorGroupId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_NotLinkedToOperatorGroup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreasUnsel

    #region AreaOperatorGroup_Add
    public static bool AreaOperatorGroup_Add(string connectionStringName, int operatorGroupId, int areaId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion AreaOperatorGroup_Add

    #region AreaOperatorGroup_Delete
    public static bool AreaOperatorGroup_Delete(string connectionStringName, int operatorGroupId, int areaId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion AreaOperatorGroup_Delete

    #region GetAreasForPastelLink
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreasForPastelLink(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_Search_Pastel_Link";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreasForPastelLink

    #region GetAreasPick
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreasPick(string connectionStringName, int AreaId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_PickArea_NotLinked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreasPick

    #region GetAreasStore
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreasStore(string connectionStringName, int AreaId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StoreArea_NotLinked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreasStore

    #region AreaPickArea_Add
    public static bool AreaPickArea_Add(string connectionStringName, int AreaId, int pickarea)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_AreaPickArea_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "PickareaId", DbType.Int32, pickarea);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion AreaPickArea_Add

    #region AreaStoreArea_Add
    public static bool AreaStoreArea_Add(string connectionStringName, int AreaId, int storearea)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_AreaStoreArea_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "StoreAreaId", DbType.Int32, storearea);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion AreaStoreArea_Add

    #region AreaPrincipal_Add
    public static bool AreaPrincipal_Add(string connectionStringName, int principalId, int areaId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_AreaPrincipal_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion AreaPrincipal_Add

    #region AreaPrincipal_Delete
    public static bool AreaPrincipal_Delete(string connectionStringName, int principalId, int areaId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_AreaPrincipal_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion AreaPrincipal_Delete

    #region GetAreaPrincipalUnsel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreaPrincipalUnsel(string connectionStringName, int principalId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_NotLinkedToPrincipal";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreaPrincipalUnsel

    #region GetAreasPrincipalSel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetAreasPrincipalSel(string connectionStringName, int principalId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_LinkedToPrincipal";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreasPrincipalSel


}
