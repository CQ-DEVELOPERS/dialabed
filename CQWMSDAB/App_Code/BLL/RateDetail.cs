﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for RateDetail
/// </summary>
public class RateDetail : BaseBLL
{
  /// <summary>
  /// Initializes a new instance of the <see cref="RateDetail"/> class.
  /// </summary>
  public RateDetail()
  {
  }

  /// <summary>
  /// Gets the type of the by bill id and.
  /// </summary>
  /// <param name="billId">The bill id.</param>
  /// <param name="type">The type.</param>
  /// <returns></returns>
  public DataSet GetByRateIdAndType( int rateId, int type )
  {
    var data = ExecuteDataSet( "p_RateDetail_Search", new ParamItem( "RateId", rateId )
                                                    , new ParamItem( "Type", type ) );
    return data;
  }

  /// <summary>
  /// Updates the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="orignal_id">The orignal_id.</param>
  /// <param name="type">The type.</param>
  /// <param name="storageDays">The storage days.</param>
  /// <param name="shipment">The shipment.</param>
  /// <param name="documentation">The documentation.</param>
  /// <param name="documentTypeAmount">The document type amount.</param>
  /// <param name="vehicleTypeAmount">The vehicle type amount.</param>
  /// <param name="rateId">The rate id.</param>
  /// <param name="documentTypeId">The document type id.</param>
  /// <param name="vehicleTypeId">The vehicle type id.</param>
  /// <returns></returns>
  public int Update( int id, int orignal_id, int type, byte storageDays, double shipment
                   , double documentation, double documentTypeAmount, double vehicleTypeAmount
                   , int rateId, int? documentTypeId, int? vehicleTypeId )
  {
    string sql = "p_RateDetail_" + (id > 0 ? "Update" : "Insert");

    var idParam = new ParamItem( "Id", DbType.Int32, id, id > 0 ? ParameterDirection.Input : ParameterDirection.InputOutput );

    ExecuteNonQuery( sql, idParam
                        , new ParamItem( "Type", type )
                        , new ParamItem( "StorageDays", storageDays )
                        , new ParamItem( "Shipment", shipment )
                        , new ParamItem( "Documentation", documentation )
                        , new ParamItem( "DocumentTypeAmount", documentTypeAmount )
                        , new ParamItem( "VehicleTypeAmount", vehicleTypeAmount )
                        , new ParamItem( "RateId", rateId )
                        , new ParamItem( "DocumentTypeId", documentTypeId )
                        , new ParamItem( "VehicleTypeId", vehicleTypeId ) );

    return (int)idParam.Value;
  }

  /// <summary>
  /// Updates the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="type">The type.</param>
  /// <param name="Shipment">The shipment.</param>
  /// <param name="documentation">The documentation.</param>
  /// <param name="rateId">The rate id.</param>
  /// <param name="original_Id">The original_ id.</param>
  /// <returns></returns>
  public int Update( int id, int type, double shipment, double documentation, int rateId, int original_Id, byte storageDays  )
  {
    return Update( original_Id, original_Id, type, storageDays, shipment, documentation, 0.0, 0.0, rateId, null, null );
  }
}