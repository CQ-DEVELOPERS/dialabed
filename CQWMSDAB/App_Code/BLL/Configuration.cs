using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class Configuration
{
    #region Constructor Logic
    public Configuration()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetSwitch
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public static bool GetSwitch(string connectionStringName, int configurationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_GetSwitch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public static bool GetSwitch(string connectionStringName, int warehouseId, int configurationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_GetSwitch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetSwitch

    #region GetIntValue
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public int GetIntValue(string connectionStringName, int configurationId)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Get_Value";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (Int32)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public int GetIntValue(string connectionStringName, int warehouseId, int configurationId)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Get_Value";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (Int32)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetIntValue

    #region GetStringValue
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public static string GetStringValue(string connectionStringName, int warehouseId, int configurationId)
    {
        string result = "";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Get_Value_String";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (string)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetStringValue

    #region GetDescriptionIntValue
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public int GetDescriptionIntValue(string connectionStringName, string configuration)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Description_Value";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configuration", DbType.String, configuration);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (Int32)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public int GetDescriptionIntValue(string connectionStringName, int warehouseId, string configuration)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Description_Value";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configuration", DbType.String, configuration);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (Int32)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetDescriptionIntValue

    #region GetDescriptionStringValue
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public string GetDescriptionStringValue(string connectionStringName, string configuration)
    {
        string result = "";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Description_Value_String";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configuration", DbType.String, configuration);
        db.AddOutParameter(dbCommand, "Value", DbType.String, 255);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                 
                 db.ExecuteNonQuery(dbCommand);
                result = (string)db.GetParameterValue(dbCommand, "Value");
            }
            catch (Exception ex)
            
            {
                result = ex.Message;
            }


            connection.Close();

            return result;
        }
    }
    public string GetDescriptionStringValue(string connectionStringName, int warehouseId, string configuration)
    {
        string result = "";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Description_Value_String";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configuration", DbType.String, configuration);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddOutParameter(dbCommand, "Value", DbType.String, 255);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                int result1 = (Int32)db.ExecuteScalar(dbCommand);
                result = (string)db.GetParameterValue(dbCommand, "Value");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetDescriptionIntValue

    #region ConfigurationSelect
    public DataSet ConfigurationSelect(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ConfigurationSelect

    #region ConfigurationInsert
    public bool ConfigurationInsert(String connectionStringName, int configurationId, int moduleId, String configuration, bool indicator, String value)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, configurationId);
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, moduleId);
        db.AddInParameter(dbCommand, "Indicator", DbType.Boolean, indicator);
        db.AddInParameter(dbCommand, "Configuration", DbType.String, configuration);
        db.AddInParameter(dbCommand, "Value", DbType.String, value);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfigurationInsert

    #region ConfigurationUpdate
    public bool ConfigurationUpdate(String connectionStringName, int configurationId, int moduleId, String configuration, bool indicator, String value)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, configurationId);
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, moduleId);
        db.AddInParameter(dbCommand, "Indicator", DbType.Boolean, indicator);
        db.AddInParameter(dbCommand, "Configuration", DbType.String, configuration);
        db.AddInParameter(dbCommand, "Value", DbType.String, value);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfigurationUpdate

    #region ConfigurationDelete
    public bool ConfigurationDelete(String connectionStringName, int configurationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, configurationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfigurationDelete

    #region GetNextNumber
    /// <summary>
    /// Gets the booleam (true/false) value of configuration item
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="configurationId"></param>
    /// <returns></returns>
    public int GetNextNumber(string connectionStringName, int warehouseId, int configurationId)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Get_Next_Number";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "configurationId", DbType.Int32, configurationId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (Int32)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetNextNumber

    #region GetLotAttributeRule

    public static LotAttributeRule GetLotAttricuteRule(string connectionStringName, int receiptLineId, int storageUnitId)
    {
        LotAttributeRule result = LotAttributeRule.None;
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Configuration_GetLotAttributeRule";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (LotAttributeRule)Enum.Parse(typeof(LotAttributeRule), db.ExecuteScalar(dbCommand).ToString(), true);
            }
            catch { }

            connection.Close();

            return result;
        }
    }


    #endregion
}
