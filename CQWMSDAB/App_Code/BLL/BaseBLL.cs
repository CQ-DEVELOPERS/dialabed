﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BaseBLL
/// </summary>
[DataObject]
public abstract class BaseBLL
{
  static Database _db = null;

  private string ConnectionStringName { get { return HttpContext.Current.Session["ConnectionStringName"].ToString(); } }

  private Database GetDatabase()
  {
    Database db = DatabaseFactory.CreateDatabase( ConnectionStringName );

    return db;
  }

  protected DataSet ExecuteDataSet( string storedProc, params ParamItem[] parameters )
  {
    var db = GetDatabase();

    // Get the Sql Stored Procedure command
    var cmd = db.GetStoredProcCommand( storedProc );

    // Add the Parameters
    foreach ( var item in parameters )
      cmd.Parameters.Add( item.ToSqlParameter() );

    // Execute the DataSet and return the result
    return GetDatabase().ExecuteDataSet( cmd );
  }

  protected object ExecuteNonQuery( string storedProc, params ParamItem[] parameters )
  {
    var db = GetDatabase();

    // Get the Sql Stored Procedure command
    var cmd = db.GetStoredProcCommand( storedProc );

    // Add the Parameters
    foreach ( var item in parameters )
      cmd.Parameters.Add( item.ToSqlParameter() );

    // Execute the Command and return the result
    var result = GetDatabase().ExecuteNonQuery( cmd );

    // Get a list of all the output parameters
    var outParams = parameters.ToList().FindAll( o => o.Direction == ParameterDirection.InputOutput || o.Direction == ParameterDirection.Output );

    // Update the Output Parameters
    foreach ( ParamItem item in outParams )
      item.SetValue( cmd.Parameters[item.Name].Value );

    // Return the Result
    return result;
  }


}

public class ParamItem
{
  private string _name;
  private object _value;
  private DbType _type = DbType.Object;
  private ParameterDirection _direction;

  public ParamItem( string name, DbType type, object value )
    : this( name, type, value, ParameterDirection.Input )
  {
  }

  public ParamItem( string name, DbType type, object value, ParameterDirection direction )
  {
    _name = name;
    _type = type;
    _value = value;
    _direction = direction;
  }

  public ParamItem( string name, object value )
  {
    _name = name;
    _value = value;
  }

  public object Value
  {
    get { return _value; }
  }

  public DbType Type
  {
    get { return _type; }
  }

  public string Name
  {
    get { return _name; }
  }

  public ParameterDirection Direction
  {
    get { return _direction; }
  }

  public System.Data.SqlClient.SqlParameter ToSqlParameter()
  {
    // Create the Sql Parameter
    System.Data.SqlClient.SqlParameter p = new System.Data.SqlClient.SqlParameter( Name, Value );

    // Set the Direction of the parameter
    if ( Direction != null )
      p.Direction = Direction;

    // Set the Data Type if specified
    if ( Type != DbType.Object )
      p.DbType = Type;
    else
      p.DbType = GetDbType( Value );

    if ( p.DbType == DbType.DateTime && (DateTime)p.Value == DateTime.MinValue )
      p.Value = null;


    return p;
  }

  private DbType GetDbType( object value )
  {
    if ( value == null )
      return DbType.Object;

    DbType type = DbType.String;
    var dataTypeName = value.GetType().Name;

    if ( value.GetType().IsEnum )
      type = DbType.Int32;
    else
      type = (DbType)Enum.Parse( typeof( DbType ), dataTypeName );

    return type;
  }

  public void SetValue( object value )
  {
    _value = value;
  }
}