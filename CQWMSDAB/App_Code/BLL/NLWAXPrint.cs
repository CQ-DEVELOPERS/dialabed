﻿using System;
using System.Data;
using System.Configuration;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using System.Collections;

/// <summary>
/// Summary description for NLWAX_Print
/// </summary>
public class NLWAXPrint
{
	public event Action<string, object> onSessionUpdate;

	public string CreatePrintFile(string labelGroup,
								  string labelFileName,
								  object printer,
								  object port,
								  object sesionIndicatorList,
								  object sessionCheckedList,
								  object sessionLocationList,
								  object sessionConnectionString,
								  object sessionTitle,
								  object sessionBarcode,
								  object sessionWarehouseId,
								  object sessionProductList,
								  object sessionTakeOnLabel,
								  object sessionLabelCopies)
	{
		string response = "";
		int quantity = 1;

		if (sessionLabelCopies != null)
		{
			quantity = (int)sessionLabelCopies;
		}

		try
		{
			string requestBody = GetRequestBody(labelGroup,
												labelFileName,
												printer,
												sesionIndicatorList,
												sessionCheckedList,
												sessionLocationList,
												sessionConnectionString,
												sessionTitle,
												sessionBarcode,
												sessionWarehouseId,
												sessionProductList,
												sessionTakeOnLabel,
												quantity);

			var niceLabelApiUrl = ConfigurationManager.AppSettings["NiceLabelApiUrl"] + port.ToString();
			var apiKey = ConfigurationManager.AppSettings["NiceLabelApiKey"];

			var httpWebRequest = (HttpWebRequest)WebRequest.Create(niceLabelApiUrl);
			httpWebRequest.Headers.Add("Ocp-Apim-Subscription-Key", apiKey);
			httpWebRequest.Headers.Add("Api-Version", "v1");
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = "POST";
			ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;

			using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
			{
				streamWriter.Write(requestBody);
			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
			{
				var result = streamReader.ReadToEnd();
				response = "Label has been printed";
			}

			return response;
		}
		catch (Exception e)
		{
			return response;
		}
	}

	public string GetRequestBody(string labelGroup,
								 string labelFileName,
								 object printer,
								 object sesionIndicatorList,
								 object sessionCheckedList,
								 object sessionLocationList,
								 object sessionConnectionString,
								 object sessionTitle,
								 object sessionBarcode,
								 object sessionWarehouseId,
								 object sessionProductList,
								 object sessionTakeOnLabel,
								 int? quantity)
	{
		LabelVariables lv = new LabelVariables();
		var variables = GetVariables(labelFileName,
									 sesionIndicatorList,
									 sessionCheckedList,
									 sessionLocationList,
									 sessionConnectionString,
									 sessionTitle,
									 sessionBarcode,
									 sessionWarehouseId,
									 sessionProductList,
									 sessionTakeOnLabel);

		StringBuilder sb = new StringBuilder();
		foreach (DataRow item in variables.Tables[0].Rows)
		{
			sb.AppendFormat("{0}", item["Variables"]);
		}

		string result = "[{" + sb.ToString() + "}]";

		DataSet ds = new DataSet();

		var dt = new DataTable();
		var filePathCol = new DataColumn("FilePath", Type.GetType("System.String"));
		var quantityCol = new DataColumn("Quantity", Type.GetType("System.String"));
		var printerCol = new DataColumn("Printer", Type.GetType("System.String"));
		var variablesCol = new DataColumn("Variables", Type.GetType("System.String"));

		dt.Columns.Add(filePathCol);
		dt.Columns.Add(quantityCol);
		dt.Columns.Add(printerCol);
		dt.Columns.Add(variablesCol);

		var dr = dt.NewRow();
		dr["FilePath"] = labelGroup + labelFileName.Replace(".lbl", ".nlbl");
		dr["Quantity"] = quantity.ToString();
		dr["Printer"] = printer.ToString(); ;
		dr["Variables"] = result;
		dt.Rows.Add(dr);

		ds.Tables.Add(dt);

		string dsJson = JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented).Replace("\\\"", "\"");
		dsJson = dsJson.Substring(5);
		dsJson = dsJson.Substring(0, dsJson.Length - 3);

		int firstSquareBracketIndex = dsJson.IndexOf('[');
		int lastSquareBracketIndex = dsJson.IndexOf(']');

		dsJson = dsJson.Remove(firstSquareBracketIndex - 1, 1);
		dsJson = dsJson.Remove(lastSquareBracketIndex, 1);

		return dsJson;
	}

	protected DataSet GetVariables(string labelFileName,
								   object sesionIndicatorList,
								   object sessionCheckedList,
								   object sessionLocationList,
								   object connectionString,
								   object title,
								   object sessionBarcode,
								   object warehouseId,
								   object sessionProductList,
								   object sessionTakeOnLabel)
	{
		LabelVariables ds = new LabelVariables();
		DataSet Variables = new DataSet();

		ArrayList indicatorList = new ArrayList();

		if (sesionIndicatorList != null)
		{
			indicatorList = (ArrayList)sesionIndicatorList;
		}

		if (sessionCheckedList != null)
		{
			ArrayList checkList = (ArrayList)sessionCheckedList;
			int locationId = -1;

			try
			{
				if (sessionLocationList != null)
				{
					ArrayList locationList = (ArrayList)sessionLocationList;

					locationId = (int)locationList[0];

					locationList.Remove(locationId);

					if (locationList.Count < 1)
						//Session["locationList"] = null;
						onSessionUpdate("locationList", null);
					else
						//Session["locationList"] = locationList;
						onSessionUpdate("locationList", locationList);

				}
			}
			catch
			{
				locationId = -1;
			}

			if (checkList.Count > 0)
			{
				int key = -1;
				bool indicator = false;
				string barcode = "";

				try
				{
					key = (int)checkList[0];

					checkList.Remove(key);
				}
				catch
				{
					barcode = checkList[0].ToString();
					checkList.Remove(barcode);
				}
				if (indicatorList.Count > 0)
				{
					try
					{
						indicator = (bool)indicatorList[0];
						indicatorList.Remove(indicator);
					}
					catch (Exception)
					{
					}
				}

				switch (labelFileName)
				{
					case "Pallet Label.lbl":
						if (sessionTakeOnLabel == null)
							Variables = ds.GetInstructionLabel(connectionString.ToString(), labelFileName, key);
						else
							Variables = ds.GetPalletLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Pallet Label Small.lbl":
						if (sessionTakeOnLabel == null)
							Variables = ds.GetInstructionLabel(connectionString.ToString(), labelFileName, key);
						else
							Variables = ds.GetPalletLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Despatch By Order Label.lbl":
						Variables = ds.GetJobLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "RTTLabel.lbl":
						Variables = ds.GetJobLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "NonRTTLabel.lbl":
						Variables = ds.GetJobLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Despatch By Route Label.lbl":
						Variables = ds.GetJobLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Pick Job Label.lbl":
						Variables = ds.GetJobLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Receiving & Staging Label.lbl":
						Variables = ds.GetInstructionLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Receiving & Staging Label Large.lbl":
						Variables = ds.GetInstructionLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Reject Label.lbl":
						Variables = ds.GetJobLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Box Label.lbl":
						if (key == -1)
							Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), barcode);
						else
							Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), key.ToString());
						break;
					case "Multi Purpose Label.lbl":
						if (key == -1)
							Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), barcode);
						else
							Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), key.ToString());
						break;
					//case "Multi Purpose Label Job.lbl":
					//    if (key == -1)
					//        Variables = ds.GetMultiPurposeJobLabel(connectionString.ToString(), labelFileName, title.ToString(), barcode);
					//    else
					//        Variables = ds.GetMultiPurposeJobLabel(connectionString.ToString(), labelFileName, title.ToString(), key.ToString());
					//    break;
					//case "Multi Purpose Label Job Order.lbl":
					//    if (key == -1)
					//        Variables = ds.GetMultiPurposeJobOrderLabel(connectionString.ToString(), labelFileName, title.ToString(), barcode);
					//    else
					//        Variables = ds.GetMultiPurposeJobOrderLabel(connectionString.ToString(), labelFileName, title.ToString(), key.ToString());
					//    break;
					case "Sample Label Large.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Sample Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Product Batch Label Large.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Product Batch Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Picking Label Large.nlbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Picking Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Verify Batch Label Large.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Verify Batch Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Verify Batch Label Large Version 2.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key, locationId);
						break;
					case "Receiving Batch Label Large.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Receiving Batch Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Sign In Card.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Customer Product Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "EAN Product Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "NOTEAN Product Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Product Division Label Small.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Sensory FX Label Large.lbl":
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Builders Product Label Large.lbl":
						Variables = ds.GetJobLabel(connectionString.ToString(), labelFileName, key);
						break;
					case "Product Label Small.lbl":
						if (sessionProductList != null)
						{
							ArrayList productList = (ArrayList)sessionProductList;
							string productCode = "";
							string product = "";

							if (productList.Count > 0)
							{
								string[] v = ((string)productList[0]).Split(new char[] { ',' });

								productCode = v.GetValue(0).ToString();
								product = v.GetValue(1).ToString();

								productList.RemoveAt(0);
							}
							else
							{
								//Session["ProductList"] = null;
								onSessionUpdate("ProductList", null);
							}

							Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, product, productCode);

							if (productList.Count > 0)
							{
								checkList.Add(0);

								//Session["checkedList"] = checkList;
								onSessionUpdate("checkedList", checkList);
							}
						}
						else
						{
							StaticInfo si = new StaticInfo();
							var product = si.GetProductById(connectionString.ToString(), key);
							Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, product.ProductName, product.ProductCode);
						}
						break;
					default:
						Variables = ds.GetSampleLabel(connectionString.ToString(), labelFileName, key);
						break;
				}

				if (checkList.Count == 0)
				{
					//Session["checkedList"] = null;
					//Session["Printing"] = false;
					//Session["TakeOnLabel"] = null;
					onSessionUpdate("checkedList", null);
					onSessionUpdate("Printing", false);
					onSessionUpdate("TakeOnLabel", null);
				}
				else
				{
					//Session["checkedList"] = checkList;
					onSessionUpdate("checkedList", checkList);

				}
			}
			else
				onSessionUpdate("checkedList", null);
		}
		else //if (Session["Barcode"] != null)
		{
			switch (labelFileName)
			{
				case "Multi Purpose Label.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Multi Purpose Label Job.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Pickface Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Pickface Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Racking Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), "");
					break;
				case "Racking Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), "");
					break;
				case "Security Code Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), "");
					break;
				case "Security Code Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), "");
					break;
				case "Second Level Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Second Level Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Second Level Plain Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Second Level Plain Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Left Location Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Left Location Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Right Location Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Right Location Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString());
					break;
				case "Product Bin Down Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString(), (int)warehouseId);
					break;
				case "Product Bin Down Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString(), (int)warehouseId);
					break;
				case "Product Bin Up Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString(), (int)warehouseId);
					break;
				case "Product Bin Up Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString(), (int)warehouseId);
					break;
				case "Location Label Small.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString(), (int)warehouseId);
					break;
				case "Location Label Large.lbl":
					Variables = ds.GetMultiPurposeLabel(connectionString.ToString(), labelFileName, title.ToString(), sessionBarcode.ToString(), (int)warehouseId);
					break;
				default:
					break;
			}

			onSessionUpdate("Printing", false);
		}

		return Variables;
	}
}