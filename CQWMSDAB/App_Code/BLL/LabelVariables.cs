using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

/// <summary>
/// Summary description for Label
/// </summary>
public class LabelVariables
{
    #region Constructor Logic
    public LabelVariables()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetLabelVariables
    public DataSet GetLabelVariables(string labelName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "p_Label_Variables";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "label", DbType.String, labelName);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLabelVariables

    #region GetPalletLabel
    public DataSet GetPalletLabel(string connectionStringName, string labelName, int palletId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Variables";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "label", DbType.String, labelName);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetPalletLabel

    #region SetPalletLabel
    public int SetPalletLabel(string connectionStringName)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //enter a default value, the correct value will be returned
        db.AddOutParameter(dbCommand, "Value", DbType.Int32, result);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = (int)db.GetParameterValue(dbCommand, "@Value");
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion SetPalletLabel

    #region SetPalletLabel
    public int SetPalletLabel(string connectionStringName, int storageUnitBatchId, int locationId, Decimal quantity)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        //enter a default value, the correct value will be returned
        db.AddOutParameter(dbCommand, "Value", DbType.Int32, result);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = (int)db.GetParameterValue(dbCommand, "@Value");
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion SetPalletLabel

    #region GetInstructionLabel
    public DataSet GetInstructionLabel(string connectionStringName, string labelName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Variables";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "label", DbType.String, labelName);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructionLabel

    #region GetMultiPurposeLabel
    public DataSet GetMultiPurposeLabel(string connectionStringName, string labelName, string title, string barcode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Variables";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "label", DbType.String, labelName);
        db.AddInParameter(dbCommand, "title", DbType.String, title);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetMultiPurposeLabel(string connectionStringName, string labelName, string title, string barcode, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Variables";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "label", DbType.String, labelName);
        db.AddInParameter(dbCommand, "title", DbType.String, title);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetMultiPurposeLabel

    #region GetSampleLabel
    public DataSet GetSampleLabel(string connectionStringName, string labelName, int keyId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Variables";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "label", DbType.String, labelName);
        db.AddInParameter(dbCommand, "KeyId", DbType.Int32, keyId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetSampleLabel(string connectionStringName, string labelName, int keyId, int locationId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Variables";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "label", DbType.String, labelName);
        db.AddInParameter(dbCommand, "KeyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetSampleLabel

    #region SetReferenceNumber
    public int SetReferenceNumber(string connectionStringName, int warehouseId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Reference";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //enter a default value, the correct value will be returned
        db.AddOutParameter(dbCommand, "Value", DbType.Int32, result);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = (int)db.GetParameterValue(dbCommand, "@Value");
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion SetReferenceNumber

    #region GetJobLabel
    public DataSet GetJobLabel(string connectionStringName, string labelName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Variables";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "label", DbType.String, labelName);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetJobLabel

    #region Putaway
    public DataSet Putaway(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Putaway";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion Putaway

    #region GetVars
    PrintService.NPSService NicePrintService;
    public System.Data.DataSet GetVars(PrintService.FileLocation LabelFile)
    {
        System.Data.DataSet LabelVars = new System.Data.DataSet();

        // Retrieve table of variables from label
        LabelVars = NicePrintService.GetVariables(LabelFile);

        // Return retrieved dataset
        return LabelVars;
    }
    #endregion GetVars

    #region JobLabel
    public DataSet JobLabel(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion JobLabel

    #region PalletLabel
    public DataSet PalletLabel(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Label_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion PalletLabel
}
