using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   23 June 2007
/// Summary description for Menu
/// </summary>
public class LogonCredentials
{
    public int warehouseId = -1;
    public int operatorGroupId = -1;
    public int operatorId = -1;

    public String serverName = "12345678901234567890123456789012345678901234567890";
    public String databaseName = "12345678901234567890123456789012345678901234567890";
    public String cultureName = "12345678901234567890123456789012345678901234567890";
    public String printer = "12345678901234567890123456789012345678901234567890";
    public String port = "12345678901234567890123456789012345678901234567890";
    public String ipAddress = "12345678901234567890123456789012345678901234567890";
    public String operatorGroupCode = "1234567890";
    public int menuId = -1;
    public String Version = "12345678901234567890";
    public bool IsActive = true;
    public bool IsDeleted = true;

    public LogonCredentials()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region GetOperatorsDatabase
    /// <summary>
    /// Get the users database connection string name
    /// </summary>
    /// <param name="userName"></param>
    /// <returns></returns>
    public String GetOperatorsDatabase(String userName)
    {
        String result = "-1";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "dbo.p_UserOperator_Database";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "userName", DbType.String, userName);

        //Add out parameters
        db.AddParameter(dbCommand, "serverName", DbType.String, ParameterDirection.InputOutput, "serverName", DataRowVersion.Current, serverName);
        db.AddParameter(dbCommand, "databaseName", DbType.String, ParameterDirection.InputOutput, "databaseName", DataRowVersion.Current, databaseName);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                serverName = db.GetParameterValue(dbCommand, "serverName").ToString();
                databaseName = db.GetParameterValue(dbCommand, "databaseName").ToString();
            }
            catch (Exception ex) { }

            connection.Close();

            return result;
        }
    }
    #endregion GetOperatorsDatabase

    #region GetLogonCredentials
    /// <summary>
    /// Get all user settings for the web application
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="userName"></param>
    /// <returns></returns>
    public bool GetLogonCredentials(String connectionStringName, String userName)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_LogonCredentials_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add in parameters
        db.AddInParameter(dbCommand, "userName", DbType.String, userName);

        //Add out parameters
        db.AddParameter(dbCommand, "warehouseId", DbType.Int32, ParameterDirection.InputOutput, "warehouseId", DataRowVersion.Current, warehouseId);
        db.AddParameter(dbCommand, "operatorGroupId", DbType.Int32, ParameterDirection.InputOutput, "operatorGroupId", DataRowVersion.Current, operatorGroupId);
        db.AddParameter(dbCommand, "operatorId", DbType.Int32, ParameterDirection.InputOutput, "operatorId", DataRowVersion.Current, operatorId);
        db.AddParameter(dbCommand, "cultureName", DbType.String, ParameterDirection.InputOutput, "cultureName", DataRowVersion.Current, cultureName);
        db.AddParameter(dbCommand, "printer", DbType.String, ParameterDirection.InputOutput, "printer", DataRowVersion.Current, printer);
        db.AddParameter(dbCommand, "port", DbType.String, ParameterDirection.InputOutput, "port", DataRowVersion.Current, port);
        db.AddParameter(dbCommand, "iPAddress", DbType.String, ParameterDirection.InputOutput, "iPAddress", DataRowVersion.Current, ipAddress);
        db.AddParameter(dbCommand, "operatorGroupCode", DbType.String, ParameterDirection.InputOutput, "operatorGroupCode", DataRowVersion.Current, operatorGroupCode);
        db.AddParameter(dbCommand, "menuId", DbType.Int32, ParameterDirection.InputOutput, "menuId", DataRowVersion.Current, menuId);
        db.AddParameter(dbCommand, "IsActive", DbType.Boolean, ParameterDirection.InputOutput, "IsActive", DataRowVersion.Current, IsActive);
        db.AddParameter(dbCommand, "IsDeleted", DbType.Boolean, ParameterDirection.InputOutput, "IsDeleted", DataRowVersion.Current, IsDeleted);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                warehouseId = (int)db.GetParameterValue(dbCommand, "warehouseId");
                operatorGroupId = (int)db.GetParameterValue(dbCommand, "operatorGroupId");
                operatorId = (int)db.GetParameterValue(dbCommand, "operatorId");
                cultureName = db.GetParameterValue(dbCommand, "cultureName").ToString();
                printer = db.GetParameterValue(dbCommand, "printer").ToString();
                port = db.GetParameterValue(dbCommand, "port").ToString();
                ipAddress = db.GetParameterValue(dbCommand, "ipAddress").ToString();
                operatorGroupCode = db.GetParameterValue(dbCommand, "operatorGroupCode").ToString();
                menuId = (int)db.GetParameterValue(dbCommand, "menuId");
                IsActive = (bool)db.GetParameterValue(dbCommand, "IsActive");
                IsDeleted = (bool)db.GetParameterValue(dbCommand, "IsDeleted");

                result = false;
                if (IsActive == true && IsDeleted == false)
                    result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetLogonCredentials

    #region EnableUserInCommonDB
    public bool EnableUserInCommonDB(String connectionStringName, String userName)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_UserOperatorInsert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "userName", DbType.String, userName);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion EnableUserInCommonDB

    #region UnlockUser
    public bool UnlockUser(String userName)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "dbo.p_UnlockUser";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "UserName", DbType.String, userName);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UnlockUser

    #region ChangePassword
    public bool ChangePassword(String userName, String password)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "dbo.p_ChangePassword";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "UserName", DbType.String, userName);
        db.AddInParameter(dbCommand, "Password", DbType.String, password);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ChangePassword

    #region UserLoggedIn
    public bool UserLoggedIn(String connectionStringName, int operatorId, string siteType)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Login";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add in parameters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "SiteType", DbType.String, siteType);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UserLoggedIn

    #region UserLoggedOut
    public bool UserLoggedOut(String connectionStringName, int operatorId, string siteType)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Logout";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add in parameters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "SiteType", DbType.String, siteType);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UserLoggedOut

    #region CheckConcurrentUsers
    public bool CheckConcurrentUsers(String connectionStringName, int warehouseId, string siteType)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Concurrent_Users";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add in parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "SiteType", DbType.String, siteType);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CheckConcurrentUsers

    #region GetVersion
    public bool GetVersion(String connectionStringName)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CQ_Show_BuildNumber";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Add out parameters
        db.AddParameter(dbCommand, "Version", DbType.String, ParameterDirection.InputOutput, "Version", DataRowVersion.Current, Version);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                Version = (String)db.GetParameterValue(dbCommand, "Version");

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion
}
