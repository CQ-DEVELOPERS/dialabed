using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Planning
/// </summary>
public class DespatchConfirmation
{
    #region Constructor Logic
    public DespatchConfirmation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchPickingDocuments
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="outboundDocumentTypeId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="externalCompany"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchPickingDocuments(string connectionStringName,
                                            int warehouseId,
                                            int outboundDocumentTypeId,
                                            string orderNumber,
                                            string externalCompany,
                                            string externalCompanyCode,
                                            DateTime fromDate,
                                            DateTime toDate)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DespatchConfirmation_Document_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPickingDocuments"

    #region SearchPackingSlip
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="outboundDocumentTypeId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="externalCompany"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchPackingSlip(string connectionStringName,
                                            int warehouseId,
                                            int outboundDocumentTypeId,
                                            string orderNumber,
                                            string externalCompany,
                                            string externalCompanyCode,
                                            DateTime fromDate,
                                            DateTime toDate)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DespatchConfirmation_PackingSlip_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPackingSlip"

    #region UpdateLoad
    /// <summary>
    /// Updates a loads despatch date
    /// </summary>
    /// <param name="outboundShipmentId"></param>
    /// <param name="issueId"></param>
    /// <param name="despatchedDate"></param>
    /// <returns></returns>
    public bool UpdateLoad(string connectionStringName, int outboundShipmentId, int issueId, DateTime despatchedDate)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DespatchConfirmation_Update_Load";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "despatchedDate", DbType.DateTime, despatchedDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateLoad"

    #region PackingOverride
    /// <summary>
    /// Overrides a packing export
    /// </summary>
    /// <param name="outboundShipmentId"></param>
    /// <param name="issueId"></param>
    /// <param name="despatchedDate"></param>
    /// <returns></returns>
    public bool PackingOverride(string connectionStringName, string OrderNumber, string deliveryMethod, int OperatorId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DespatchConfirmation_PackingSlip_Override";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "deliveryMethod", DbType.String, deliveryMethod);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion
}
