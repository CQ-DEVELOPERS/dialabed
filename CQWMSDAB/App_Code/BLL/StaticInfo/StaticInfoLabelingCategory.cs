using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoLabelingCategory
///   Create By      : Grant Schultz
///   Date Created   : 30 Apr 2014 11:52:31
/// </summary>
/// <remarks>
///   Inserts a row into the LabelingCategory table.
///   Selects rows from the LabelingCategory table.
///   Searches for rows from the LabelingCategory table.
///   Updates a rows in the LabelingCategory table.
///   Deletes a row from the LabelingCategory table.
/// </remarks>
/// <param>
public class StaticInfoLabelingCategory
{
    #region "Constructor Logic"
    public StaticInfoLabelingCategory()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetLabelingCategory
    /// <summary>
    ///   Selects rows from the LabelingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetLabelingCategory
    (
        string connectionStringName,
        Int32 LabelingCategoryId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetLabelingCategory
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetLabelingCategory
 
    #region DeleteLabelingCategory
    /// <summary>
    ///   Deletes a row from the LabelingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <returns>bool</returns>
    public bool DeleteLabelingCategory
    (
        string connectionStringName,
        Int32 LabelingCategoryId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteLabelingCategory
 
    #region UpdateLabelingCategory
    /// <summary>
    ///   Updates a row in the LabelingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <param name="LabelingCategory"></param>
    /// <param name="LabelingCategoryCode"></param>
    /// <param name="Additional1"></param>
    /// <param name="Additional2"></param>
    /// <param name="Additional3"></param>
    /// <param name="Additional4"></param>
    /// <param name="Additional5"></param>
    /// <param name="ManualLabel"></param>
    /// <param name="ScreenGaurd"></param>
    /// <returns>bool</returns>
    public bool UpdateLabelingCategory
    (
        string connectionStringName,
        Int32 LabelingCategoryId,
        String LabelingCategory,
        String LabelingCategoryCode,
        String Additional1,
        String Additional2,
        String Additional3,
        String Additional4,
        String Additional5,
        Boolean ManualLabel,
        Boolean ScreenGaurd 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
        db.AddInParameter(dbCommand, "LabelingCategory", DbType.String, LabelingCategory);
        db.AddInParameter(dbCommand, "LabelingCategoryCode", DbType.String, LabelingCategoryCode);
        db.AddInParameter(dbCommand, "Additional1", DbType.String, Additional1);
        db.AddInParameter(dbCommand, "Additional2", DbType.String, Additional2);
        db.AddInParameter(dbCommand, "Additional3", DbType.String, Additional3);
        db.AddInParameter(dbCommand, "Additional4", DbType.String, Additional4);
        db.AddInParameter(dbCommand, "Additional5", DbType.String, Additional5);
        db.AddInParameter(dbCommand, "ManualLabel", DbType.Boolean, ManualLabel);
        db.AddInParameter(dbCommand, "ScreenGaurd", DbType.Boolean, ScreenGaurd);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateLabelingCategory
 
    #region InsertLabelingCategory
    /// <summary>
    ///   Inserts a row into the LabelingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <param name="LabelingCategory"></param>
    /// <param name="LabelingCategoryCode"></param>
    /// <param name="Additional1"></param>
    /// <param name="Additional2"></param>
    /// <param name="Additional3"></param>
    /// <param name="Additional4"></param>
    /// <param name="Additional5"></param>
    /// <param name="ManualLabel"></param>
    /// <param name="ScreenGaurd"></param>
    /// <returns>bool</returns>
    public bool InsertLabelingCategory
    (
        string connectionStringName,
        Int32 LabelingCategoryId,
        String LabelingCategory,
        String LabelingCategoryCode,
        String Additional1,
        String Additional2,
        String Additional3,
        String Additional4,
        String Additional5,
        Boolean ManualLabel,
        Boolean ScreenGaurd 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
        db.AddInParameter(dbCommand, "LabelingCategory", DbType.String, LabelingCategory);
        db.AddInParameter(dbCommand, "LabelingCategoryCode", DbType.String, LabelingCategoryCode);
        db.AddInParameter(dbCommand, "Additional1", DbType.String, Additional1);
        db.AddInParameter(dbCommand, "Additional2", DbType.String, Additional2);
        db.AddInParameter(dbCommand, "Additional3", DbType.String, Additional3);
        db.AddInParameter(dbCommand, "Additional4", DbType.String, Additional4);
        db.AddInParameter(dbCommand, "Additional5", DbType.String, Additional5);
        db.AddInParameter(dbCommand, "ManualLabel", DbType.Boolean, ManualLabel);
        db.AddInParameter(dbCommand, "ScreenGaurd", DbType.Boolean, ScreenGaurd);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertLabelingCategory
 
    #region SearchLabelingCategory
    /// <summary>
    ///   Selects rows from the LabelingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <param name="LabelingCategory"></param>
    /// <param name="LabelingCategoryCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchLabelingCategory
    (
        string connectionStringName,
        Int32 LabelingCategoryId,
        String LabelingCategory,
        String LabelingCategoryCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
        db.AddInParameter(dbCommand, "LabelingCategory", DbType.String, LabelingCategory);
        db.AddInParameter(dbCommand, "LabelingCategoryCode", DbType.String, LabelingCategoryCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchLabelingCategory
 
    #region PageSearchLabelingCategory
    /// <summary>
    ///   Selects rows from the LabelingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <param name="LabelingCategory"></param>
    /// <param name="LabelingCategoryCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchLabelingCategory
    (
        string connectionStringName,
        Int32 LabelingCategoryId,
        String LabelingCategory,
        String LabelingCategoryCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
        db.AddInParameter(dbCommand, "LabelingCategory", DbType.String, LabelingCategory);
        db.AddInParameter(dbCommand, "LabelingCategoryCode", DbType.String, LabelingCategoryCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchLabelingCategory
 
    #region ListLabelingCategory
    /// <summary>
    ///   Lists rows from the LabelingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListLabelingCategory
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListLabelingCategory
 
    #region ParameterLabelingCategory
    /// <summary>
    ///   Lists rows from the LabelingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterLabelingCategory
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LabelingCategory_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterLabelingCategory
}
