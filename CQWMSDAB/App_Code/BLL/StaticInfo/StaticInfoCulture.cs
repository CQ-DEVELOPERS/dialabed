using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoCulture
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:11:50
/// </summary>
/// <remarks>
///   Inserts a row into the Culture table.
///   Selects rows from the Culture table.
///   Searches for rows from the Culture table.
///   Updates a rows in the Culture table.
///   Deletes a row from the Culture table.
/// </remarks>
/// <param>
public class StaticInfoCulture
{
    #region "Constructor Logic"
    public StaticInfoCulture()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetCulture
    /// <summary>
    ///   Selects rows from the Culture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CultureId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetCulture
    (
        string connectionStringName,
        Int32 CultureId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetCulture
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetCulture
 
    #region DeleteCulture
    /// <summary>
    ///   Deletes a row from the Culture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CultureId"></param>
    /// <returns>bool</returns>
    public bool DeleteCulture
    (
        string connectionStringName,
        Int32 CultureId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteCulture
 
    #region UpdateCulture
    /// <summary>
    ///   Updates a row in the Culture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CultureId"></param>
    /// <param name="CultureName"></param>
    /// <returns>bool</returns>
    public bool UpdateCulture
    (
        string connectionStringName,
        Int32 CultureId,
        String CultureName 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "CultureName", DbType.String, CultureName);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateCulture
 
    #region InsertCulture
    /// <summary>
    ///   Inserts a row into the Culture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CultureId"></param>
    /// <param name="CultureName"></param>
    /// <returns>bool</returns>
    public bool InsertCulture
    (
        string connectionStringName,
        Int32 CultureId,
        String CultureName 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "CultureName", DbType.String, CultureName);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertCulture
 
    #region SearchCulture
    /// <summary>
    ///   Selects rows from the Culture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CultureId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchCulture
    (
        string connectionStringName,
        Int32 CultureId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchCulture
 
    #region PageSearchCulture
    /// <summary>
    ///   Selects rows from the Culture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CultureId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchCulture
    (
        string connectionStringName,
        Int32 CultureId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchCulture
 
    #region ListCulture
    /// <summary>
    ///   Lists rows from the Culture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListCulture
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListCulture
 
    #region ParameterCulture
    /// <summary>
    ///   Lists rows from the Culture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterCulture
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Culture_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterCulture
}
