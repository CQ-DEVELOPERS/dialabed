using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOperatorGroupMenuItem
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:15
/// </summary>
/// <remarks>
///   Inserts a row into the OperatorGroupMenuItem table.
///   Selects rows from the OperatorGroupMenuItem table.
///   Searches for rows from the OperatorGroupMenuItem table.
///   Updates a rows in the OperatorGroupMenuItem table.
///   Deletes a row from the OperatorGroupMenuItem table.
/// </remarks>
/// <param>
public class StaticInfoOperatorGroupMenuItem
{
    #region "Constructor Logic"
    public StaticInfoOperatorGroupMenuItem()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOperatorGroupMenuItem
    /// <summary>
    ///   Selects rows from the OperatorGroupMenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="MenuItemId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOperatorGroupMenuItem
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        Int32 MenuItemId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetOperatorGroupMenuItem
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOperatorGroupMenuItem
 
    #region DeleteOperatorGroupMenuItem
    /// <summary>
    ///   Deletes a row from the OperatorGroupMenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="MenuItemId"></param>
    /// <returns>bool</returns>
    public bool DeleteOperatorGroupMenuItem
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        Int32 MenuItemId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOperatorGroupMenuItem
 
    #region UpdateOperatorGroupMenuItem
    /// <summary>
    ///   Updates a row in the OperatorGroupMenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="Access"></param>
    /// <returns>bool</returns>
    public bool UpdateOperatorGroupMenuItem
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        Int32 MenuItemId,
        Boolean Access 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "Access", DbType.Boolean, Access);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOperatorGroupMenuItem
 
    #region InsertOperatorGroupMenuItem
    /// <summary>
    ///   Inserts a row into the OperatorGroupMenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="Access"></param>
    /// <returns>bool</returns>
    public bool InsertOperatorGroupMenuItem
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        Int32 MenuItemId,
        Boolean Access 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "Access", DbType.Boolean, Access);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOperatorGroupMenuItem
 
    #region SearchOperatorGroupMenuItem
    /// <summary>
    ///   Selects rows from the OperatorGroupMenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="MenuItemId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOperatorGroupMenuItem
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        Int32 MenuItemId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOperatorGroupMenuItem
 
    #region PageSearchOperatorGroupMenuItem
    /// <summary>
    ///   Selects rows from the OperatorGroupMenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="MenuItemId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchOperatorGroupMenuItem
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        Int32 MenuItemId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchOperatorGroupMenuItem
 
    #region ListOperatorGroupMenuItem
    /// <summary>
    ///   Lists rows from the OperatorGroupMenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOperatorGroupMenuItem
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOperatorGroupMenuItem
 
    #region ParameterOperatorGroupMenuItem
    /// <summary>
    ///   Lists rows from the OperatorGroupMenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOperatorGroupMenuItem
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupMenuItem_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOperatorGroupMenuItem
}
