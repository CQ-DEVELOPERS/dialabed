using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInterfaceField
///   Create By      : Grant Schultz
///   Date Created   : 19 Mar 2014 08:42:45
/// </summary>
/// <remarks>
///   Inserts a row into the InterfaceField table.
///   Selects rows from the InterfaceField table.
///   Searches for rows from the InterfaceField table.
///   Updates a rows in the InterfaceField table.
///   Deletes a row from the InterfaceField table.
/// </remarks>
/// <param>
public class StaticInfoInterfaceField
{
    #region "Constructor Logic"
    public StaticInfoInterfaceField()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInterfaceField
    /// <summary>
    ///   Selects rows from the InterfaceField table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInterfaceField
    (
        string connectionStringName,
        Int32 InterfaceFieldId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInterfaceField
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInterfaceField
 
    #region DeleteInterfaceField
    /// <summary>
    ///   Deletes a row from the InterfaceField table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <returns>bool</returns>
    public bool DeleteInterfaceField
    (
        string connectionStringName,
        Int32 InterfaceFieldId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInterfaceField
 
    #region UpdateInterfaceField
    /// <summary>
    ///   Updates a row in the InterfaceField table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <param name="InterfaceFieldCode"></param>
    /// <param name="InterfaceField"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="Datatype"></param>
    /// <returns>bool</returns>
    public bool UpdateInterfaceField
    (
        string connectionStringName,
        Int32 InterfaceFieldId,
        String InterfaceFieldCode,
        String InterfaceField,
        Int32 InterfaceDocumentTypeId,
        String Datatype 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
        db.AddInParameter(dbCommand, "InterfaceFieldCode", DbType.String, InterfaceFieldCode);
        db.AddInParameter(dbCommand, "InterfaceField", DbType.String, InterfaceField);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "Datatype", DbType.String, Datatype);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInterfaceField
 
    #region InsertInterfaceField
    /// <summary>
    ///   Inserts a row into the InterfaceField table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <param name="InterfaceFieldCode"></param>
    /// <param name="InterfaceField"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="Datatype"></param>
    /// <returns>bool</returns>
    public bool InsertInterfaceField
    (
        string connectionStringName,
        Int32 InterfaceFieldId,
        String InterfaceFieldCode,
        String InterfaceField,
        Int32 InterfaceDocumentTypeId,
        String Datatype 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
        db.AddInParameter(dbCommand, "InterfaceFieldCode", DbType.String, InterfaceFieldCode);
        db.AddInParameter(dbCommand, "InterfaceField", DbType.String, InterfaceField);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "Datatype", DbType.String, Datatype);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInterfaceField
 
    #region SearchInterfaceField
    /// <summary>
    ///   Selects rows from the InterfaceField table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <param name="InterfaceFieldCode"></param>
    /// <param name="InterfaceField"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInterfaceField
    (
        string connectionStringName,
        Int32 InterfaceFieldId,
        String InterfaceFieldCode,
        String InterfaceField,
        Int32 InterfaceDocumentTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
        db.AddInParameter(dbCommand, "InterfaceFieldCode", DbType.String, InterfaceFieldCode);
        db.AddInParameter(dbCommand, "InterfaceField", DbType.String, InterfaceField);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInterfaceField
 
    #region PageSearchInterfaceField
    /// <summary>
    ///   Selects rows from the InterfaceField table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <param name="InterfaceFieldCode"></param>
    /// <param name="InterfaceField"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInterfaceField
    (
        string connectionStringName,
        Int32 InterfaceFieldId,
        String InterfaceFieldCode,
        String InterfaceField,
        Int32 InterfaceDocumentTypeId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
        db.AddInParameter(dbCommand, "InterfaceFieldCode", DbType.String, InterfaceFieldCode);
        db.AddInParameter(dbCommand, "InterfaceField", DbType.String, InterfaceField);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInterfaceField
 
    #region ListInterfaceField
    /// <summary>
    ///   Lists rows from the InterfaceField table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInterfaceField
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInterfaceField
 
    #region ParameterInterfaceField
    /// <summary>
    ///   Lists rows from the InterfaceField table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInterfaceField
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceField_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInterfaceField
}
