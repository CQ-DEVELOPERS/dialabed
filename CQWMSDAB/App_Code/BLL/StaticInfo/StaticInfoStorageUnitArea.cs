using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoStorageUnitArea
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:47
/// </summary>
/// <remarks>
///   Inserts a row into the StorageUnitArea table.
///   Selects rows from the StorageUnitArea table.
///   Searches for rows from the StorageUnitArea table.
///   Updates a rows in the StorageUnitArea table.
///   Deletes a row from the StorageUnitArea table.
/// </remarks>
/// <param>
public class StaticInfoStorageUnitArea
{
    #region "Constructor Logic"
    public StaticInfoStorageUnitArea()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetStorageUnitArea
    /// <summary>
    ///   Selects rows from the StorageUnitArea table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="AreaId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetStorageUnitArea
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 AreaId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetStorageUnitArea
 
    #region DeleteStorageUnitArea
    /// <summary>
    ///   Deletes a row from the StorageUnitArea table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="AreaId"></param>
    /// <returns>bool</returns>
    public bool DeleteStorageUnitArea
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 AreaId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteStorageUnitArea
 
    #region UpdateStorageUnitArea
    /// <summary>
    ///   Updates a row in the StorageUnitArea table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="AreaId"></param>
    /// <param name="StoreOrder"></param>
    /// <param name="PickOrder"></param>
    /// <returns>bool</returns>
    public bool UpdateStorageUnitArea
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 AreaId,
        Int32 StoreOrder,
        Int32 PickOrder 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "StoreOrder", DbType.Int32, StoreOrder);
        db.AddInParameter(dbCommand, "PickOrder", DbType.Int32, PickOrder);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateStorageUnitArea
 
    #region InsertStorageUnitArea
    /// <summary>
    ///   Inserts a row into the StorageUnitArea table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="AreaId"></param>
    /// <param name="StoreOrder"></param>
    /// <param name="PickOrder"></param>
    /// <returns>bool</returns>
    public bool InsertStorageUnitArea
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 AreaId,
        Int32 StoreOrder,
        Int32 PickOrder 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "StoreOrder", DbType.Int32, StoreOrder);
        db.AddInParameter(dbCommand, "PickOrder", DbType.Int32, PickOrder);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertStorageUnitArea
 
    #region SearchStorageUnitArea
    /// <summary>
    ///   Selects rows from the StorageUnitArea table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="AreaId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchStorageUnitArea
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 AreaId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchStorageUnitArea
 
    #region ListStorageUnitArea
    /// <summary>
    ///   Lists rows from the StorageUnitArea table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListStorageUnitArea
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListStorageUnitArea
 
    #region ParameterStorageUnitArea
    /// <summary>
    ///   Lists rows from the StorageUnitArea table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterStorageUnitArea
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterStorageUnitArea
}
