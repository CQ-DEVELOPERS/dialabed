using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoCOATest
///   Create By      : Grant Schultz
///   Date Created   : 04 Oct 2012 15:03:18
/// </summary>
/// <remarks>
///   Inserts a row into the COATest table.
///   Selects rows from the COATest table.
///   Searches for rows from the COATest table.
///   Updates a rows in the COATest table.
///   Deletes a row from the COATest table.
/// </remarks>
/// <param>
public class StaticInfoCOATest
{
    #region "Constructor Logic"
    public StaticInfoCOATest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetCOATest
    /// <summary>
    ///   Selects rows from the COATest table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COATestId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetCOATest
    (
        string connectionStringName,
        Int32 COATestId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COATestId", DbType.Int32, COATestId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetCOATest
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetCOATest
 
    #region DeleteCOATest
    /// <summary>
    ///   Deletes a row from the COATest table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COATestId"></param>
    /// <returns>bool</returns>
    public bool DeleteCOATest
    (
        string connectionStringName,
        Int32 COATestId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COATestId", DbType.Int32, COATestId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteCOATest
 
    #region UpdateCOATest
    /// <summary>
    ///   Updates a row in the COATest table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COATestId"></param>
    /// <param name="COAId"></param>
    /// <param name="TestId"></param>
    /// <param name="ResultId"></param>
    /// <param name="MethodId"></param>
    /// <param name="ResultCode"></param>
    /// <param name="Result"></param>
    /// <param name="Pass"></param>
    /// <param name="StartRange"></param>
    /// <param name="EndRange"></param>
    /// <returns>bool</returns>
    public bool UpdateCOATest
    (
        string connectionStringName,
        Int32 COATestId,
        Int32 COAId,
        Int32 TestId,
        Int32 ResultId,
        Int32 MethodId,
        String ResultCode,
        String Result,
        Boolean Pass,
        Double StartRange,
        Double EndRange 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COATestId", DbType.Int32, COATestId);
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
        db.AddInParameter(dbCommand, "ResultCode", DbType.String, ResultCode);
        db.AddInParameter(dbCommand, "Result", DbType.String, Result);
        db.AddInParameter(dbCommand, "Pass", DbType.Boolean, Pass);
        db.AddInParameter(dbCommand, "StartRange", DbType.Double, StartRange);
        db.AddInParameter(dbCommand, "EndRange", DbType.Double, EndRange);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateCOATest
 
    #region InsertCOATest
    /// <summary>
    ///   Inserts a row into the COATest table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COATestId"></param>
    /// <param name="COAId"></param>
    /// <param name="TestId"></param>
    /// <param name="ResultId"></param>
    /// <param name="MethodId"></param>
    /// <param name="ResultCode"></param>
    /// <param name="Result"></param>
    /// <param name="Pass"></param>
    /// <param name="StartRange"></param>
    /// <param name="EndRange"></param>
    /// <returns>bool</returns>
    public bool InsertCOATest
    (
        string connectionStringName,
        Int32 COATestId,
        Int32 COAId,
        Int32 TestId,
        Int32 ResultId,
        Int32 MethodId,
        String ResultCode,
        String Result,
        Boolean Pass,
        Double StartRange,
        Double EndRange 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COATestId", DbType.Int32, COATestId);
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
        db.AddInParameter(dbCommand, "ResultCode", DbType.String, ResultCode);
        db.AddInParameter(dbCommand, "Result", DbType.String, Result);
        db.AddInParameter(dbCommand, "Pass", DbType.Boolean, Pass);
        db.AddInParameter(dbCommand, "StartRange", DbType.Double, StartRange);
        db.AddInParameter(dbCommand, "EndRange", DbType.Double, EndRange);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertCOATest
 
    #region SearchCOATest
    /// <summary>
    ///   Selects rows from the COATest table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COATestId"></param>
    /// <param name="COAId"></param>
    /// <param name="TestId"></param>
    /// <param name="ResultId"></param>
    /// <param name="MethodId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchCOATest
    (
        string connectionStringName,
        Int32 COATestId,
        Int32 COAId,
        Int32 TestId,
        Int32 ResultId,
        Int32 MethodId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COATestId", DbType.Int32, COATestId);
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchCOATest
 
    #region PageSearchCOATest
    /// <summary>
    ///   Selects rows from the COATest table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COATestId"></param>
    /// <param name="COAId"></param>
    /// <param name="TestId"></param>
    /// <param name="ResultId"></param>
    /// <param name="MethodId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchCOATest
    (
        string connectionStringName,
        Int32 COATestId,
        Int32 COAId,
        Int32 TestId,
        Int32 ResultId,
        Int32 MethodId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COATestId", DbType.Int32, COATestId);
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchCOATest
 
    #region ListCOATest
    /// <summary>
    ///   Lists rows from the COATest table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListCOATest
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListCOATest
 
    #region ParameterCOATest
    /// <summary>
    ///   Lists rows from the COATest table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterCOATest
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COATest_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterCOATest
}
