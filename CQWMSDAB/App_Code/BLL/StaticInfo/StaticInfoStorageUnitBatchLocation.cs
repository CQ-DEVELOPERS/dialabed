using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoStorageUnitBatchLocation
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:49
/// </summary>
/// <remarks>
///   Inserts a row into the StorageUnitBatchLocation table.
///   Selects rows from the StorageUnitBatchLocation table.
///   Searches for rows from the StorageUnitBatchLocation table.
///   Updates a rows in the StorageUnitBatchLocation table.
///   Deletes a row from the StorageUnitBatchLocation table.
/// </remarks>
/// <param>
public class StaticInfoStorageUnitBatchLocation
{
    #region "Constructor Logic"
    public StaticInfoStorageUnitBatchLocation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetStorageUnitBatchLocation
    /// <summary>
    ///   Selects rows from the StorageUnitBatchLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetStorageUnitBatchLocation
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatchLocation_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetStorageUnitBatchLocation
 
    #region DeleteStorageUnitBatchLocation
    /// <summary>
    ///   Deletes a row from the StorageUnitBatchLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="LocationId"></param>
    /// <returns>bool</returns>
    public bool DeleteStorageUnitBatchLocation
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 LocationId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatchLocation_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteStorageUnitBatchLocation
 
    #region UpdateStorageUnitBatchLocation
    /// <summary>
    ///   Updates a row in the StorageUnitBatchLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="LocationId"></param>
    /// <param name="ActualQuantity"></param>
    /// <param name="AllocatedQuantity"></param>
    /// <param name="ReservedQuantity"></param>
    /// <returns>bool</returns>
    public bool UpdateStorageUnitBatchLocation
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 LocationId,
        Decimal ActualQuantity,
        Decimal AllocatedQuantity,
        Decimal ReservedQuantity 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatchLocation_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "ActualQuantity", DbType.Decimal, ActualQuantity);
        db.AddInParameter(dbCommand, "AllocatedQuantity", DbType.Decimal, AllocatedQuantity);
        db.AddInParameter(dbCommand, "ReservedQuantity", DbType.Decimal, ReservedQuantity);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateStorageUnitBatchLocation
 
    #region InsertStorageUnitBatchLocation
    /// <summary>
    ///   Inserts a row into the StorageUnitBatchLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="LocationId"></param>
    /// <param name="ActualQuantity"></param>
    /// <param name="AllocatedQuantity"></param>
    /// <param name="ReservedQuantity"></param>
    /// <returns>bool</returns>
    public bool InsertStorageUnitBatchLocation
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 LocationId,
        Decimal ActualQuantity,
        Decimal AllocatedQuantity,
        Decimal ReservedQuantity 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatchLocation_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "ActualQuantity", DbType.Decimal, ActualQuantity);
        db.AddInParameter(dbCommand, "AllocatedQuantity", DbType.Decimal, AllocatedQuantity);
        db.AddInParameter(dbCommand, "ReservedQuantity", DbType.Decimal, ReservedQuantity);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertStorageUnitBatchLocation
 
    #region SearchStorageUnitBatchLocation
    /// <summary>
    ///   Selects rows from the StorageUnitBatchLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchStorageUnitBatchLocation
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatchLocation_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchStorageUnitBatchLocation
 
    #region ListStorageUnitBatchLocation
    /// <summary>
    ///   Lists rows from the StorageUnitBatchLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListStorageUnitBatchLocation
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatchLocation_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListStorageUnitBatchLocation
 
    #region ParameterStorageUnitBatchLocation
    /// <summary>
    ///   Lists rows from the StorageUnitBatchLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterStorageUnitBatchLocation
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatchLocation_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterStorageUnitBatchLocation
}
