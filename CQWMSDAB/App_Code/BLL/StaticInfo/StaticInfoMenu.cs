using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoMenu
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:05
/// </summary>
/// <remarks>
///   Inserts a row into the Menu table.
///   Selects rows from the Menu table.
///   Searches for rows from the Menu table.
///   Updates a rows in the Menu table.
///   Deletes a row from the Menu table.
/// </remarks>
/// <param>
public class StaticInfoMenu
{
    #region "Constructor Logic"
    public StaticInfoMenu()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetMenu
    /// <summary>
    ///   Selects rows from the Menu table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetMenu
    (
        string connectionStringName,
        Int32 MenuId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetMenu
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetMenu
 
    #region DeleteMenu
    /// <summary>
    ///   Deletes a row from the Menu table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuId"></param>
    /// <returns>bool</returns>
    public bool DeleteMenu
    (
        string connectionStringName,
        Int32 MenuId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteMenu
 
    #region UpdateMenu
    /// <summary>
    ///   Updates a row in the Menu table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuId"></param>
    /// <param name="Menu"></param>
    /// <returns>bool</returns>
    public bool UpdateMenu
    (
        string connectionStringName,
        Int32 MenuId,
        String Menu 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "Menu", DbType.String, Menu);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateMenu
 
    #region InsertMenu
    /// <summary>
    ///   Inserts a row into the Menu table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuId"></param>
    /// <param name="Menu"></param>
    /// <returns>bool</returns>
    public bool InsertMenu
    (
        string connectionStringName,
        Int32 MenuId,
        String Menu 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "Menu", DbType.String, Menu);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertMenu
 
    #region SearchMenu
    /// <summary>
    ///   Selects rows from the Menu table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuId"></param>
    /// <param name="Menu"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchMenu
    (
        string connectionStringName,
        Int32 MenuId,
        String Menu 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "Menu", DbType.String, Menu);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchMenu
 
    #region PageSearchMenu
    /// <summary>
    ///   Selects rows from the Menu table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuId"></param>
    /// <param name="Menu"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchMenu
    (
        string connectionStringName,
        Int32 MenuId,
        String Menu,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "Menu", DbType.String, Menu);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchMenu
 
    #region ListMenu
    /// <summary>
    ///   Lists rows from the Menu table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListMenu
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListMenu
 
    #region ParameterMenu
    /// <summary>
    ///   Lists rows from the Menu table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterMenu
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Menu_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterMenu
}
