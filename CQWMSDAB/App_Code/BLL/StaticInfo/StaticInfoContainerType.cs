using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoContainerType
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:11:49
/// </summary>
/// <remarks>
///   Inserts a row into the ContainerType table.
///   Selects rows from the ContainerType table.
///   Searches for rows from the ContainerType table.
///   Updates a rows in the ContainerType table.
///   Deletes a row from the ContainerType table.
/// </remarks>
/// <param>
public class StaticInfoContainerType
{
    #region "Constructor Logic"
    public StaticInfoContainerType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetContainerType
    /// <summary>
    ///   Selects rows from the ContainerType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContainerTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetContainerType
    (
        string connectionStringName,
        Int32 ContainerTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetContainerType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetContainerType
 
    #region DeleteContainerType
    /// <summary>
    ///   Deletes a row from the ContainerType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContainerTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteContainerType
    (
        string connectionStringName,
        Int32 ContainerTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteContainerType
 
    #region UpdateContainerType
    /// <summary>
    ///   Updates a row in the ContainerType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContainerTypeId"></param>
    /// <param name="ContainerType"></param>
    /// <param name="ContainerTypeCode"></param>
    /// <param name="Description"></param>
    /// <param name="Length"></param>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    /// <param name="TareWeight"></param>
    /// <returns>bool</returns>
    public bool UpdateContainerType
    (
        string connectionStringName,
        Int32 ContainerTypeId,
        String ContainerType,
        String ContainerTypeCode,
        String Description,
        Int32 Length,
        Int32 Width,
        Int32 Height,
        Decimal TareWeight 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
        db.AddInParameter(dbCommand, "ContainerType", DbType.String, ContainerType);
        db.AddInParameter(dbCommand, "ContainerTypeCode", DbType.String, ContainerTypeCode);
        db.AddInParameter(dbCommand, "Description", DbType.String, Description);
        db.AddInParameter(dbCommand, "Length", DbType.Int32, Length);
        db.AddInParameter(dbCommand, "Width", DbType.Int32, Width);
        db.AddInParameter(dbCommand, "Height", DbType.Int32, Height);
        db.AddInParameter(dbCommand, "TareWeight", DbType.Decimal, TareWeight);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateContainerType
 
    #region InsertContainerType
    /// <summary>
    ///   Inserts a row into the ContainerType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContainerTypeId"></param>
    /// <param name="ContainerType"></param>
    /// <param name="ContainerTypeCode"></param>
    /// <param name="Description"></param>
    /// <param name="Length"></param>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    /// <param name="TareWeight"></param>
    /// <returns>bool</returns>
    public bool InsertContainerType
    (
        string connectionStringName,
        Int32 ContainerTypeId,
        String ContainerType,
        String ContainerTypeCode,
        String Description,
        Int32 Length,
        Int32 Width,
        Int32 Height,
        Decimal TareWeight 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
        db.AddInParameter(dbCommand, "ContainerType", DbType.String, ContainerType);
        db.AddInParameter(dbCommand, "ContainerTypeCode", DbType.String, ContainerTypeCode);
        db.AddInParameter(dbCommand, "Description", DbType.String, Description);
        db.AddInParameter(dbCommand, "Length", DbType.Int32, Length);
        db.AddInParameter(dbCommand, "Width", DbType.Int32, Width);
        db.AddInParameter(dbCommand, "Height", DbType.Int32, Height);
        db.AddInParameter(dbCommand, "TareWeight", DbType.Decimal, TareWeight);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertContainerType
 
    #region SearchContainerType
    /// <summary>
    ///   Selects rows from the ContainerType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContainerTypeId"></param>
    /// <param name="ContainerType"></param>
    /// <param name="ContainerTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchContainerType
    (
        string connectionStringName,
        Int32 ContainerTypeId,
        String ContainerType,
        String ContainerTypeCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
        db.AddInParameter(dbCommand, "ContainerType", DbType.String, ContainerType);
        db.AddInParameter(dbCommand, "ContainerTypeCode", DbType.String, ContainerTypeCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchContainerType
 
    #region PageSearchContainerType
    /// <summary>
    ///   Selects rows from the ContainerType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContainerTypeId"></param>
    /// <param name="ContainerType"></param>
    /// <param name="ContainerTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchContainerType
    (
        string connectionStringName,
        Int32 ContainerTypeId,
        String ContainerType,
        String ContainerTypeCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
        db.AddInParameter(dbCommand, "ContainerType", DbType.String, ContainerType);
        db.AddInParameter(dbCommand, "ContainerTypeCode", DbType.String, ContainerTypeCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchContainerType
 
    #region ListContainerType
    /// <summary>
    ///   Lists rows from the ContainerType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListContainerType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListContainerType
 
    #region ParameterContainerType
    /// <summary>
    ///   Lists rows from the ContainerType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterContainerType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterContainerType
}
