using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInterfaceMasterFile
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:01
/// </summary>
/// <remarks>
///   Inserts a row into the InterfaceMasterFile table.
///   Selects rows from the InterfaceMasterFile table.
///   Searches for rows from the InterfaceMasterFile table.
///   Updates a rows in the InterfaceMasterFile table.
///   Deletes a row from the InterfaceMasterFile table.
/// </remarks>
/// <param>
public class StaticInfoInterfaceMasterFile
{
    #region "Constructor Logic"
    public StaticInfoInterfaceMasterFile()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInterfaceMasterFile
    /// <summary>
    ///   Selects rows from the InterfaceMasterFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MasterFileId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInterfaceMasterFile
    (
        string connectionStringName,
        Int32 MasterFileId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MasterFileId", DbType.Int32, MasterFileId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInterfaceMasterFile
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInterfaceMasterFile
 
    #region DeleteInterfaceMasterFile
    /// <summary>
    ///   Deletes a row from the InterfaceMasterFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MasterFileId"></param>
    /// <returns>bool</returns>
    public bool DeleteInterfaceMasterFile
    (
        string connectionStringName,
        Int32 MasterFileId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MasterFileId", DbType.Int32, MasterFileId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInterfaceMasterFile
 
    #region UpdateInterfaceMasterFile
    /// <summary>
    ///   Updates a row in the InterfaceMasterFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MasterFileId"></param>
    /// <param name="MasterFile"></param>
    /// <param name="MasterFileDescription"></param>
    /// <param name="LastDate"></param>
    /// <param name="SendFlag"></param>
    /// <returns>bool</returns>
    public bool UpdateInterfaceMasterFile
    (
        string connectionStringName,
        Int32 MasterFileId,
        String MasterFile,
        String MasterFileDescription,
        DateTime LastDate,
        Boolean SendFlag 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MasterFileId", DbType.Int32, MasterFileId);
        db.AddInParameter(dbCommand, "MasterFile", DbType.String, MasterFile);
        db.AddInParameter(dbCommand, "MasterFileDescription", DbType.String, MasterFileDescription);
        db.AddInParameter(dbCommand, "LastDate", DbType.DateTime, LastDate);
        db.AddInParameter(dbCommand, "SendFlag", DbType.Boolean, SendFlag);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInterfaceMasterFile
 
    #region InsertInterfaceMasterFile
    /// <summary>
    ///   Inserts a row into the InterfaceMasterFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MasterFileId"></param>
    /// <param name="MasterFile"></param>
    /// <param name="MasterFileDescription"></param>
    /// <param name="LastDate"></param>
    /// <param name="SendFlag"></param>
    /// <returns>bool</returns>
    public bool InsertInterfaceMasterFile
    (
        string connectionStringName,
        Int32 MasterFileId,
        String MasterFile,
        String MasterFileDescription,
        DateTime LastDate,
        Boolean SendFlag 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MasterFileId", DbType.Int32, MasterFileId);
        db.AddInParameter(dbCommand, "MasterFile", DbType.String, MasterFile);
        db.AddInParameter(dbCommand, "MasterFileDescription", DbType.String, MasterFileDescription);
        db.AddInParameter(dbCommand, "LastDate", DbType.DateTime, LastDate);
        db.AddInParameter(dbCommand, "SendFlag", DbType.Boolean, SendFlag);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInterfaceMasterFile
 
    #region SearchInterfaceMasterFile
    /// <summary>
    ///   Selects rows from the InterfaceMasterFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MasterFileId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInterfaceMasterFile
    (
        string connectionStringName,
        Int32 MasterFileId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MasterFileId", DbType.Int32, MasterFileId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInterfaceMasterFile
 
    #region PageSearchInterfaceMasterFile
    /// <summary>
    ///   Selects rows from the InterfaceMasterFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MasterFileId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInterfaceMasterFile
    (
        string connectionStringName,
        Int32 MasterFileId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MasterFileId", DbType.Int32, MasterFileId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInterfaceMasterFile
 
    #region ListInterfaceMasterFile
    /// <summary>
    ///   Lists rows from the InterfaceMasterFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInterfaceMasterFile
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInterfaceMasterFile
 
    #region ParameterInterfaceMasterFile
    /// <summary>
    ///   Lists rows from the InterfaceMasterFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInterfaceMasterFile
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMasterFile_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInterfaceMasterFile
}
