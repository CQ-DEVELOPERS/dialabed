using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInterfaceMappingFile
///   Create By      : Grant Schultz
///   Date Created   : 19 Mar 2014 08:42:54
/// </summary>
/// <remarks>
///   Inserts a row into the InterfaceMappingFile table.
///   Selects rows from the InterfaceMappingFile table.
///   Searches for rows from the InterfaceMappingFile table.
///   Updates a rows in the InterfaceMappingFile table.
///   Deletes a row from the InterfaceMappingFile table.
/// </remarks>
/// <param>
public class StaticInfoInterfaceMappingFile
{
    #region "Constructor Logic"
    public StaticInfoInterfaceMappingFile()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInterfaceMappingFile
    /// <summary>
    ///   Selects rows from the InterfaceMappingFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInterfaceMappingFile
    (
        string connectionStringName,
        Int32 InterfaceMappingFileId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInterfaceMappingFile
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInterfaceMappingFile
 
    #region DeleteInterfaceMappingFile
    /// <summary>
    ///   Deletes a row from the InterfaceMappingFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <returns>bool</returns>
    public bool DeleteInterfaceMappingFile
    (
        string connectionStringName,
        Int32 InterfaceMappingFileId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInterfaceMappingFile
 
    #region UpdateInterfaceMappingFile
    /// <summary>
    ///   Updates a row in the InterfaceMappingFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="Delimiter"></param>
    /// <param name="StyleSheet"></param>
    /// <param name="PrincipalFTPSite"></param>
    /// <param name="FTPUserName"></param>
    /// <param name="FTPPassword"></param>
    /// <param name="HeaderRow"></param>
    /// <param name="SubDirectory"></param>
    /// <param name="FilePrefix"></param>
    /// <returns>bool</returns>
    public bool UpdateInterfaceMappingFile
    (
        string connectionStringName,
        Int32 InterfaceMappingFileId,
        Int32 PrincipalId,
        Int32 InterfaceFileTypeId,
        Int32 InterfaceDocumentTypeId,
        String Delimiter,
        String StyleSheet,
        String PrincipalFTPSite,
        String FTPUserName,
        String FTPPassword,
        Int32 HeaderRow,
        String SubDirectory,
        String FilePrefix 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "Delimiter", DbType.String, Delimiter);
        db.AddInParameter(dbCommand, "StyleSheet", DbType.String, StyleSheet);
        db.AddInParameter(dbCommand, "PrincipalFTPSite", DbType.String, PrincipalFTPSite);
        db.AddInParameter(dbCommand, "FTPUserName", DbType.String, FTPUserName);
        db.AddInParameter(dbCommand, "FTPPassword", DbType.String, FTPPassword);
        db.AddInParameter(dbCommand, "HeaderRow", DbType.Int32, HeaderRow);
        db.AddInParameter(dbCommand, "SubDirectory", DbType.String, SubDirectory);
        db.AddInParameter(dbCommand, "FilePrefix", DbType.String, FilePrefix);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInterfaceMappingFile
 
    #region InsertInterfaceMappingFile
    /// <summary>
    ///   Inserts a row into the InterfaceMappingFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="Delimiter"></param>
    /// <param name="StyleSheet"></param>
    /// <param name="PrincipalFTPSite"></param>
    /// <param name="FTPUserName"></param>
    /// <param name="FTPPassword"></param>
    /// <param name="HeaderRow"></param>
    /// <param name="SubDirectory"></param>
    /// <param name="FilePrefix"></param>
    /// <returns>bool</returns>
    public bool InsertInterfaceMappingFile
    (
        string connectionStringName,
        Int32 InterfaceMappingFileId,
        Int32 PrincipalId,
        Int32 InterfaceFileTypeId,
        Int32 InterfaceDocumentTypeId,
        String Delimiter,
        String StyleSheet,
        String PrincipalFTPSite,
        String FTPUserName,
        String FTPPassword,
        Int32 HeaderRow,
        String SubDirectory,
        String FilePrefix 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "Delimiter", DbType.String, Delimiter);
        db.AddInParameter(dbCommand, "StyleSheet", DbType.String, StyleSheet);
        db.AddInParameter(dbCommand, "PrincipalFTPSite", DbType.String, PrincipalFTPSite);
        db.AddInParameter(dbCommand, "FTPUserName", DbType.String, FTPUserName);
        db.AddInParameter(dbCommand, "FTPPassword", DbType.String, FTPPassword);
        db.AddInParameter(dbCommand, "HeaderRow", DbType.Int32, HeaderRow);
        db.AddInParameter(dbCommand, "SubDirectory", DbType.String, SubDirectory);
        db.AddInParameter(dbCommand, "FilePrefix", DbType.String, FilePrefix);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInterfaceMappingFile
 
    #region SearchInterfaceMappingFile
    /// <summary>
    ///   Selects rows from the InterfaceMappingFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInterfaceMappingFile
    (
        string connectionStringName,
        Int32 InterfaceMappingFileId,
        Int32 PrincipalId,
        Int32 InterfaceFileTypeId,
        Int32 InterfaceDocumentTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInterfaceMappingFile
 
    #region PageSearchInterfaceMappingFile
    /// <summary>
    ///   Selects rows from the InterfaceMappingFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInterfaceMappingFile
    (
        string connectionStringName,
        Int32 InterfaceMappingFileId,
        Int32 PrincipalId,
        Int32 InterfaceFileTypeId,
        Int32 InterfaceDocumentTypeId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInterfaceMappingFile
 
    #region ListInterfaceMappingFile
    /// <summary>
    ///   Lists rows from the InterfaceMappingFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInterfaceMappingFile
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInterfaceMappingFile
 
    #region ParameterInterfaceMappingFile
    /// <summary>
    ///   Lists rows from the InterfaceMappingFile table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInterfaceMappingFile
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingFile_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInterfaceMappingFile
}
