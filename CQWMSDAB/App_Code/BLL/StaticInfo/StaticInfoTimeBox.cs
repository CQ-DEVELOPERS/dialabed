using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoTimeBox
///   Create By      : Grant Schultz
///   Date Created   : 03 Mar 2014 12:46:00
/// </summary>
/// <remarks>
///   Inserts a row into the TimeBox table.
///   Selects rows from the TimeBox table.
///   Searches for rows from the TimeBox table.
///   Updates a rows in the TimeBox table.
///   Deletes a row from the TimeBox table.
/// </remarks>
/// <param>
public class StaticInfoTimeBox
{
    #region "Constructor Logic"
    public StaticInfoTimeBox()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetTimeBox
    /// <summary>
    ///   Selects rows from the TimeBox table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TimeBoxId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetTimeBox
    (
        string connectionStringName,
        Int32 TimeBoxId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TimeBoxId", DbType.Int32, TimeBoxId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetTimeBox
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetTimeBox
 
    #region DeleteTimeBox
    /// <summary>
    ///   Deletes a row from the TimeBox table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TimeBoxId"></param>
    /// <returns>bool</returns>
    public bool DeleteTimeBox
    (
        string connectionStringName,
        Int32 TimeBoxId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TimeBoxId", DbType.Int32, TimeBoxId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteTimeBox
 
    #region UpdateTimeBox
    /// <summary>
    ///   Updates a row in the TimeBox table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TimeBoxId"></param>
    /// <param name="TaskListId"></param>
    /// <param name="CreateDate"></param>
    /// <param name="ActualHours"></param>
    /// <returns>bool</returns>
    public bool UpdateTimeBox
    (
        string connectionStringName,
        Int32 TimeBoxId,
        Int32 TaskListId,
        DateTime CreateDate,
        Double ActualHours 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TimeBoxId", DbType.Int32, TimeBoxId);
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "ActualHours", DbType.Double, ActualHours);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateTimeBox
 
    #region InsertTimeBox
    /// <summary>
    ///   Inserts a row into the TimeBox table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TimeBoxId"></param>
    /// <param name="TaskListId"></param>
    /// <param name="CreateDate"></param>
    /// <param name="ActualHours"></param>
    /// <returns>bool</returns>
    public bool InsertTimeBox
    (
        string connectionStringName,
        Int32 TimeBoxId,
        Int32 TaskListId,
        DateTime CreateDate,
        Double ActualHours 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TimeBoxId", DbType.Int32, TimeBoxId);
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "ActualHours", DbType.Double, ActualHours);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertTimeBox
 
    #region SearchTimeBox
    /// <summary>
    ///   Selects rows from the TimeBox table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TimeBoxId"></param>
    /// <param name="TaskListId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchTimeBox
    (
        string connectionStringName,
        Int32 TimeBoxId,
        Int32 TaskListId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TimeBoxId", DbType.Int32, TimeBoxId);
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchTimeBox
 
    #region PageSearchTimeBox
    /// <summary>
    ///   Selects rows from the TimeBox table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TimeBoxId"></param>
    /// <param name="TaskListId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchTimeBox
    (
        string connectionStringName,
        Int32 TimeBoxId,
        Int32 TaskListId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TimeBoxId", DbType.Int32, TimeBoxId);
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchTimeBox
 
    #region ListTimeBox
    /// <summary>
    ///   Lists rows from the TimeBox table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListTimeBox
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListTimeBox
 
    #region ParameterTimeBox
    /// <summary>
    ///   Lists rows from the TimeBox table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterTimeBox
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TimeBox_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterTimeBox
}
