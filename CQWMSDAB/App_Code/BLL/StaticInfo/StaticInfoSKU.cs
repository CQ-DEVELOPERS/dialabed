using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoSKU
///   Create By      : Grant Schultz
///   Date Created   : 14 May 2014 16:58:13
/// </summary>
/// <remarks>
///   Inserts a row into the SKU table.
///   Selects rows from the SKU table.
///   Searches for rows from the SKU table.
///   Updates a rows in the SKU table.
///   Deletes a row from the SKU table.
/// </remarks>
/// <param>
public class StaticInfoSKU
{
    #region "Constructor Logic"
    public StaticInfoSKU()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetSKU
    /// <summary>
    ///   Selects rows from the SKU table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SKUId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetSKU
    (
        string connectionStringName,
        Int32 SKUId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetSKU
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetSKU
 
    #region DeleteSKU
    /// <summary>
    ///   Deletes a row from the SKU table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SKUId"></param>
    /// <returns>bool</returns>
    public bool DeleteSKU
    (
        string connectionStringName,
        Int32 SKUId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteSKU
 
    #region UpdateSKU
    /// <summary>
    ///   Updates a row in the SKU table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SKUId"></param>
    /// <param name="UOMId"></param>
    /// <param name="SKU"></param>
    /// <param name="SKUCode"></param>
    /// <param name="Quantity"></param>
    /// <param name="AlternatePallet"></param>
    /// <param name="SubstitutePallet"></param>
    /// <param name="Litres"></param>
    /// <param name="ParentSKUCode"></param>
    /// <returns>bool</returns>
    public bool UpdateSKU
    (
        string connectionStringName,
        Int32 SKUId,
        Int32 UOMId,
        String SKU,
        String SKUCode,
        Double Quantity,
        Int32 AlternatePallet,
        Int32 SubstitutePallet,
        Decimal Litres,
        String ParentSKUCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
        db.AddInParameter(dbCommand, "SKU", DbType.String, SKU);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, SKUCode);
        db.AddInParameter(dbCommand, "Quantity", DbType.Double, Quantity);
        db.AddInParameter(dbCommand, "AlternatePallet", DbType.Int32, AlternatePallet);
        db.AddInParameter(dbCommand, "SubstitutePallet", DbType.Int32, SubstitutePallet);
        db.AddInParameter(dbCommand, "Litres", DbType.Decimal, Litres);
        db.AddInParameter(dbCommand, "ParentSKUCode", DbType.String, ParentSKUCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateSKU
 
    #region InsertSKU
    /// <summary>
    ///   Inserts a row into the SKU table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SKUId"></param>
    /// <param name="UOMId"></param>
    /// <param name="SKU"></param>
    /// <param name="SKUCode"></param>
    /// <param name="Quantity"></param>
    /// <param name="AlternatePallet"></param>
    /// <param name="SubstitutePallet"></param>
    /// <param name="Litres"></param>
    /// <param name="ParentSKUCode"></param>
    /// <returns>bool</returns>
    public bool InsertSKU
    (
        string connectionStringName,
        Int32 SKUId,
        Int32 UOMId,
        String SKU,
        String SKUCode,
        Double Quantity,
        Int32 AlternatePallet,
        Int32 SubstitutePallet,
        Decimal Litres,
        String ParentSKUCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
        db.AddInParameter(dbCommand, "SKU", DbType.String, SKU);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, SKUCode);
        db.AddInParameter(dbCommand, "Quantity", DbType.Double, Quantity);
        db.AddInParameter(dbCommand, "AlternatePallet", DbType.Int32, AlternatePallet);
        db.AddInParameter(dbCommand, "SubstitutePallet", DbType.Int32, SubstitutePallet);
        db.AddInParameter(dbCommand, "Litres", DbType.Decimal, Litres);
        db.AddInParameter(dbCommand, "ParentSKUCode", DbType.String, ParentSKUCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertSKU
 
    #region SearchSKU
    /// <summary>
    ///   Selects rows from the SKU table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SKUId"></param>
    /// <param name="UOMId"></param>
    /// <param name="SKU"></param>
    /// <param name="SKUCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchSKU
    (
        string connectionStringName,
        Int32 SKUId,
        Int32 UOMId,
        String SKU,
        String SKUCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
        db.AddInParameter(dbCommand, "SKU", DbType.String, SKU);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, SKUCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchSKU
 
    #region PageSearchSKU
    /// <summary>
    ///   Selects rows from the SKU table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SKUId"></param>
    /// <param name="UOMId"></param>
    /// <param name="SKU"></param>
    /// <param name="SKUCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchSKU
    (
        string connectionStringName,
        Int32 SKUId,
        Int32 UOMId,
        String SKU,
        String SKUCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
        db.AddInParameter(dbCommand, "SKU", DbType.String, SKU);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, SKUCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchSKU
 
    #region ListSKU
    /// <summary>
    ///   Lists rows from the SKU table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListSKU
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListSKU
 
    #region ParameterSKU
    /// <summary>
    ///   Lists rows from the SKU table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterSKU
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SKU_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterSKU
}
