using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoArea
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:14:51
/// </summary>
/// <remarks>
///   Inserts a row into the Area table.
///   Selects rows from the Area table.
///   Searches for rows from the Area table.
///   Updates a rows in the Area table.
///   Deletes a row from the Area table.
/// </remarks>
/// <param>
public class StaticInfoArea
{
    #region "Constructor Logic"
    public StaticInfoArea()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetArea
    /// <summary>
    ///   Selects rows from the Area table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetArea
    (
        string connectionStringName,
        Int32 AreaId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Area_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetArea
 
    #region DeleteArea
    /// <summary>
    ///   Deletes a row from the Area table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <returns>bool</returns>
    public bool DeleteArea
    (
        string connectionStringName,
        Int32 AreaId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Area_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteArea
 
    #region UpdateArea
    /// <summary>
    ///   Updates a row in the Area table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Area"></param>
    /// <param name="AreaCode"></param>
    /// <param name="StockOnHand"></param>
    /// <returns>bool</returns>
    public bool UpdateArea
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 WarehouseId,
        String Area,
        String AreaCode,
        Boolean StockOnHand 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Area_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Area", DbType.String, Area);
        db.AddInParameter(dbCommand, "AreaCode", DbType.String, AreaCode);
        db.AddInParameter(dbCommand, "StockOnHand", DbType.Boolean, StockOnHand);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateArea
 
    #region InsertArea
    /// <summary>
    ///   Inserts a row into the Area table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Area"></param>
    /// <param name="AreaCode"></param>
    /// <param name="StockOnHand"></param>
    /// <returns>bool</returns>
    public bool InsertArea
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 WarehouseId,
        String Area,
        String AreaCode,
        Boolean StockOnHand 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Area_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Area", DbType.String, Area);
        db.AddInParameter(dbCommand, "AreaCode", DbType.String, AreaCode);
        db.AddInParameter(dbCommand, "StockOnHand", DbType.Boolean, StockOnHand);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertArea
 
    #region SearchArea
    /// <summary>
    ///   Selects rows from the Area table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Area"></param>
    /// <param name="AreaCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchArea
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 WarehouseId,
        String Area,
        String AreaCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Area_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Area", DbType.String, Area);
        db.AddInParameter(dbCommand, "AreaCode", DbType.String, AreaCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchArea
 
    #region ListArea
    /// <summary>
    ///   Lists rows from the Area table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListArea
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Area_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListArea
 
    #region ParameterArea
    /// <summary>
    ///   Lists rows from the Area table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterArea
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Area_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterArea
}
