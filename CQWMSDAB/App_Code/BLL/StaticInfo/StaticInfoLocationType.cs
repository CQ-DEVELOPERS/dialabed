using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoLocationType
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:04
/// </summary>
/// <remarks>
///   Inserts a row into the LocationType table.
///   Selects rows from the LocationType table.
///   Searches for rows from the LocationType table.
///   Updates a rows in the LocationType table.
///   Deletes a row from the LocationType table.
/// </remarks>
/// <param>
public class StaticInfoLocationType
{
    #region "Constructor Logic"
    public StaticInfoLocationType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetLocationType
    /// <summary>
    ///   Selects rows from the LocationType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetLocationType
    (
        string connectionStringName,
        Int32 LocationTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetLocationType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetLocationType
 
    #region DeleteLocationType
    /// <summary>
    ///   Deletes a row from the LocationType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteLocationType
    (
        string connectionStringName,
        Int32 LocationTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteLocationType
 
    #region UpdateLocationType
    /// <summary>
    ///   Updates a row in the LocationType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationTypeId"></param>
    /// <param name="LocationType"></param>
    /// <param name="LocationTypeCode"></param>
    /// <param name="DeleteIndicator"></param>
    /// <param name="ReplenishmentIndicator"></param>
    /// <param name="MultipleProductsIndicator"></param>
    /// <param name="BatchTrackingIndicator"></param>
    /// <returns>bool</returns>
    public bool UpdateLocationType
    (
        string connectionStringName,
        Int32 LocationTypeId,
        String LocationType,
        String LocationTypeCode,
        Boolean DeleteIndicator,
        Boolean ReplenishmentIndicator,
        Boolean MultipleProductsIndicator,
        Boolean BatchTrackingIndicator 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
        db.AddInParameter(dbCommand, "LocationType", DbType.String, LocationType);
        db.AddInParameter(dbCommand, "LocationTypeCode", DbType.String, LocationTypeCode);
        db.AddInParameter(dbCommand, "DeleteIndicator", DbType.Boolean, DeleteIndicator);
        db.AddInParameter(dbCommand, "ReplenishmentIndicator", DbType.Boolean, ReplenishmentIndicator);
        db.AddInParameter(dbCommand, "MultipleProductsIndicator", DbType.Boolean, MultipleProductsIndicator);
        db.AddInParameter(dbCommand, "BatchTrackingIndicator", DbType.Boolean, BatchTrackingIndicator);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateLocationType
 
    #region InsertLocationType
    /// <summary>
    ///   Inserts a row into the LocationType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationTypeId"></param>
    /// <param name="LocationType"></param>
    /// <param name="LocationTypeCode"></param>
    /// <param name="DeleteIndicator"></param>
    /// <param name="ReplenishmentIndicator"></param>
    /// <param name="MultipleProductsIndicator"></param>
    /// <param name="BatchTrackingIndicator"></param>
    /// <returns>bool</returns>
    public bool InsertLocationType
    (
        string connectionStringName,
        Int32 LocationTypeId,
        String LocationType,
        String LocationTypeCode,
        Boolean DeleteIndicator,
        Boolean ReplenishmentIndicator,
        Boolean MultipleProductsIndicator,
        Boolean BatchTrackingIndicator 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
        db.AddInParameter(dbCommand, "LocationType", DbType.String, LocationType);
        db.AddInParameter(dbCommand, "LocationTypeCode", DbType.String, LocationTypeCode);
        db.AddInParameter(dbCommand, "DeleteIndicator", DbType.Boolean, DeleteIndicator);
        db.AddInParameter(dbCommand, "ReplenishmentIndicator", DbType.Boolean, ReplenishmentIndicator);
        db.AddInParameter(dbCommand, "MultipleProductsIndicator", DbType.Boolean, MultipleProductsIndicator);
        db.AddInParameter(dbCommand, "BatchTrackingIndicator", DbType.Boolean, BatchTrackingIndicator);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertLocationType
 
    #region SearchLocationType
    /// <summary>
    ///   Selects rows from the LocationType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationTypeId"></param>
    /// <param name="LocationType"></param>
    /// <param name="LocationTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchLocationType
    (
        string connectionStringName,
        Int32 LocationTypeId,
        String LocationType,
        String LocationTypeCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
        db.AddInParameter(dbCommand, "LocationType", DbType.String, LocationType);
        db.AddInParameter(dbCommand, "LocationTypeCode", DbType.String, LocationTypeCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchLocationType
 
    #region PageSearchLocationType
    /// <summary>
    ///   Selects rows from the LocationType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationTypeId"></param>
    /// <param name="LocationType"></param>
    /// <param name="LocationTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchLocationType
    (
        string connectionStringName,
        Int32 LocationTypeId,
        String LocationType,
        String LocationTypeCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
        db.AddInParameter(dbCommand, "LocationType", DbType.String, LocationType);
        db.AddInParameter(dbCommand, "LocationTypeCode", DbType.String, LocationTypeCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchLocationType
 
    #region ListLocationType
    /// <summary>
    ///   Lists rows from the LocationType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListLocationType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListLocationType
 
    #region ParameterLocationType
    /// <summary>
    ///   Lists rows from the LocationType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterLocationType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_LocationType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterLocationType
}
