using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoWarehouseCodeReference
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:30
/// </summary>
/// <remarks>
///   Inserts a row into the WarehouseCodeReference table.
///   Selects rows from the WarehouseCodeReference table.
///   Searches for rows from the WarehouseCodeReference table.
///   Updates a rows in the WarehouseCodeReference table.
///   Deletes a row from the WarehouseCodeReference table.
/// </remarks>
/// <param>
public class StaticInfoWarehouseCodeReference
{
    #region "Constructor Logic"
    public StaticInfoWarehouseCodeReference()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetWarehouseCodeReference
    /// <summary>
    ///   Selects rows from the WarehouseCodeReference table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseCodeReferenceId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetWarehouseCodeReference
    (
        string connectionStringName,
        Int32 WarehouseCodeReferenceId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseCodeReferenceId", DbType.Int32, WarehouseCodeReferenceId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetWarehouseCodeReference
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetWarehouseCodeReference
 
    #region DeleteWarehouseCodeReference
    /// <summary>
    ///   Deletes a row from the WarehouseCodeReference table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseCodeReferenceId"></param>
    /// <returns>bool</returns>
    public bool DeleteWarehouseCodeReference
    (
        string connectionStringName,
        Int32 WarehouseCodeReferenceId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseCodeReferenceId", DbType.Int32, WarehouseCodeReferenceId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteWarehouseCodeReference
 
    #region UpdateWarehouseCodeReference
    /// <summary>
    ///   Updates a row in the WarehouseCodeReference table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseCodeReferenceId"></param>
    /// <param name="WarehouseCode"></param>
    /// <param name="DownloadType"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="ToWarehouseId"></param>
    /// <param name="ToWarehouseCode"></param>
    /// <param name="Module"></param>
    /// <returns>bool</returns>
    public bool UpdateWarehouseCodeReference
    (
        string connectionStringName,
        Int32 WarehouseCodeReferenceId,
        String WarehouseCode,
        String DownloadType,
        Int32 WarehouseId,
        Int32 InboundDocumentTypeId,
        Int32 OutboundDocumentTypeId,
        Int32 ToWarehouseId,
        String ToWarehouseCode,
        String Module 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseCodeReferenceId", DbType.Int32, WarehouseCodeReferenceId);
        db.AddInParameter(dbCommand, "WarehouseCode", DbType.String, WarehouseCode);
        db.AddInParameter(dbCommand, "DownloadType", DbType.String, DownloadType);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ToWarehouseId", DbType.Int32, ToWarehouseId);
        db.AddInParameter(dbCommand, "ToWarehouseCode", DbType.String, ToWarehouseCode);
        db.AddInParameter(dbCommand, "Module", DbType.String, Module);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateWarehouseCodeReference
 
    #region InsertWarehouseCodeReference
    /// <summary>
    ///   Inserts a row into the WarehouseCodeReference table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseCodeReferenceId"></param>
    /// <param name="WarehouseCode"></param>
    /// <param name="DownloadType"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="ToWarehouseId"></param>
    /// <param name="ToWarehouseCode"></param>
    /// <param name="Module"></param>
    /// <returns>bool</returns>
    public bool InsertWarehouseCodeReference
    (
        string connectionStringName,
        Int32 WarehouseCodeReferenceId,
        String WarehouseCode,
        String DownloadType,
        Int32 WarehouseId,
        Int32 InboundDocumentTypeId,
        Int32 OutboundDocumentTypeId,
        Int32 ToWarehouseId,
        String ToWarehouseCode,
        String Module 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseCodeReferenceId", DbType.Int32, WarehouseCodeReferenceId);
        db.AddInParameter(dbCommand, "WarehouseCode", DbType.String, WarehouseCode);
        db.AddInParameter(dbCommand, "DownloadType", DbType.String, DownloadType);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ToWarehouseId", DbType.Int32, ToWarehouseId);
        db.AddInParameter(dbCommand, "ToWarehouseCode", DbType.String, ToWarehouseCode);
        db.AddInParameter(dbCommand, "Module", DbType.String, Module);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertWarehouseCodeReference
 
    #region SearchWarehouseCodeReference
    /// <summary>
    ///   Selects rows from the WarehouseCodeReference table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseCodeReferenceId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="ToWarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchWarehouseCodeReference
    (
        string connectionStringName,
        Int32 WarehouseCodeReferenceId,
        Int32 WarehouseId,
        Int32 InboundDocumentTypeId,
        Int32 OutboundDocumentTypeId,
        Int32 ToWarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseCodeReferenceId", DbType.Int32, WarehouseCodeReferenceId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ToWarehouseId", DbType.Int32, ToWarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchWarehouseCodeReference
 
    #region PageSearchWarehouseCodeReference
    /// <summary>
    ///   Selects rows from the WarehouseCodeReference table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseCodeReferenceId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="ToWarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchWarehouseCodeReference
    (
        string connectionStringName,
        Int32 WarehouseCodeReferenceId,
        Int32 WarehouseId,
        Int32 InboundDocumentTypeId,
        Int32 OutboundDocumentTypeId,
        Int32 ToWarehouseId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseCodeReferenceId", DbType.Int32, WarehouseCodeReferenceId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ToWarehouseId", DbType.Int32, ToWarehouseId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchWarehouseCodeReference
 
    #region ListWarehouseCodeReference
    /// <summary>
    ///   Lists rows from the WarehouseCodeReference table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ToWarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListWarehouseCodeReference
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 ToWarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ToWarehouseId", DbType.Int32, ToWarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ListWarehouseCodeReference
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListWarehouseCodeReference
 
    #region ParameterWarehouseCodeReference
    /// <summary>
    ///   Lists rows from the WarehouseCodeReference table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ToWarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterWarehouseCodeReference
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 ToWarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ToWarehouseId", DbType.Int32, ToWarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ParameterWarehouseCodeReference
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_WarehouseCodeReference_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterWarehouseCodeReference
}
