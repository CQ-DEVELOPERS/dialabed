using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInterfaceDocumentDesc
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:00
/// </summary>
/// <remarks>
///   Inserts a row into the InterfaceDocumentDesc table.
///   Selects rows from the InterfaceDocumentDesc table.
///   Searches for rows from the InterfaceDocumentDesc table.
///   Updates a rows in the InterfaceDocumentDesc table.
///   Deletes a row from the InterfaceDocumentDesc table.
/// </remarks>
/// <param>
public class StaticInfoInterfaceDocumentDesc
{
    #region "Constructor Logic"
    public StaticInfoInterfaceDocumentDesc()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInterfaceDocumentDesc
    /// <summary>
    ///   Selects rows from the InterfaceDocumentDesc table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DocId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInterfaceDocumentDesc
    (
        string connectionStringName,
        Int32 DocId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DocId", DbType.Int32, DocId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInterfaceDocumentDesc
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInterfaceDocumentDesc
 
    #region DeleteInterfaceDocumentDesc
    /// <summary>
    ///   Deletes a row from the InterfaceDocumentDesc table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DocId"></param>
    /// <returns>bool</returns>
    public bool DeleteInterfaceDocumentDesc
    (
        string connectionStringName,
        Int32 DocId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DocId", DbType.Int32, DocId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInterfaceDocumentDesc
 
    #region UpdateInterfaceDocumentDesc
    /// <summary>
    ///   Updates a row in the InterfaceDocumentDesc table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DocId"></param>
    /// <param name="Doc"></param>
    /// <param name="DocDesc"></param>
    /// <returns>bool</returns>
    public bool UpdateInterfaceDocumentDesc
    (
        string connectionStringName,
        Int32 DocId,
        String Doc,
        String DocDesc 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DocId", DbType.Int32, DocId);
        db.AddInParameter(dbCommand, "Doc", DbType.String, Doc);
        db.AddInParameter(dbCommand, "DocDesc", DbType.String, DocDesc);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInterfaceDocumentDesc
 
    #region InsertInterfaceDocumentDesc
    /// <summary>
    ///   Inserts a row into the InterfaceDocumentDesc table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DocId"></param>
    /// <param name="Doc"></param>
    /// <param name="DocDesc"></param>
    /// <returns>bool</returns>
    public bool InsertInterfaceDocumentDesc
    (
        string connectionStringName,
        Int32 DocId,
        String Doc,
        String DocDesc 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DocId", DbType.Int32, DocId);
        db.AddInParameter(dbCommand, "Doc", DbType.String, Doc);
        db.AddInParameter(dbCommand, "DocDesc", DbType.String, DocDesc);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInterfaceDocumentDesc
 
    #region SearchInterfaceDocumentDesc
    /// <summary>
    ///   Selects rows from the InterfaceDocumentDesc table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DocId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInterfaceDocumentDesc
    (
        string connectionStringName,
        Int32 DocId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DocId", DbType.Int32, DocId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInterfaceDocumentDesc
 
    #region PageSearchInterfaceDocumentDesc
    /// <summary>
    ///   Selects rows from the InterfaceDocumentDesc table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DocId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInterfaceDocumentDesc
    (
        string connectionStringName,
        Int32 DocId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DocId", DbType.Int32, DocId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInterfaceDocumentDesc
 
    #region ListInterfaceDocumentDesc
    /// <summary>
    ///   Lists rows from the InterfaceDocumentDesc table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInterfaceDocumentDesc
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInterfaceDocumentDesc
 
    #region ParameterInterfaceDocumentDesc
    /// <summary>
    ///   Lists rows from the InterfaceDocumentDesc table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInterfaceDocumentDesc
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentDesc_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInterfaceDocumentDesc
}
