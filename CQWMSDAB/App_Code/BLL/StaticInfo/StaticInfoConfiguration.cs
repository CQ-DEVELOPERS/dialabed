using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoConfiguration
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:11:46
/// </summary>
/// <remarks>
///   Inserts a row into the Configuration table.
///   Selects rows from the Configuration table.
///   Searches for rows from the Configuration table.
///   Updates a rows in the Configuration table.
///   Deletes a row from the Configuration table.
/// </remarks>
/// <param>
public class StaticInfoConfiguration
{
    #region "Constructor Logic"
    public StaticInfoConfiguration()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetConfiguration
    /// <summary>
    ///   Selects rows from the Configuration table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ConfigurationId"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetConfiguration
    (
        string connectionStringName,
        Int32 ConfigurationId,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, ConfigurationId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetConfiguration
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetConfiguration
 
    #region DeleteConfiguration
    /// <summary>
    ///   Deletes a row from the Configuration table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ConfigurationId"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>bool</returns>
    public bool DeleteConfiguration
    (
        string connectionStringName,
        Int32 ConfigurationId,
        Int32 WarehouseId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, ConfigurationId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteConfiguration
 
    #region UpdateConfiguration
    /// <summary>
    ///   Updates a row in the Configuration table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ConfigurationId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ModuleId"></param>
    /// <param name="Configuration"></param>
    /// <param name="Indicator"></param>
    /// <param name="IntegerValue"></param>
    /// <param name="Value"></param>
    /// <returns>bool</returns>
    public bool UpdateConfiguration
    (
        string connectionStringName,
        Int32 ConfigurationId,
        Int32 WarehouseId,
        Int32 ModuleId,
        String Configuration,
        Boolean Indicator,
        Int32 IntegerValue,
        String Value 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, ConfigurationId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
        db.AddInParameter(dbCommand, "Configuration", DbType.String, Configuration);
        db.AddInParameter(dbCommand, "Indicator", DbType.Boolean, Indicator);
        db.AddInParameter(dbCommand, "IntegerValue", DbType.Int32, IntegerValue);
        db.AddInParameter(dbCommand, "Value", DbType.String, Value);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateConfiguration
 
    #region InsertConfiguration
    /// <summary>
    ///   Inserts a row into the Configuration table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ConfigurationId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ModuleId"></param>
    /// <param name="Configuration"></param>
    /// <param name="Indicator"></param>
    /// <param name="IntegerValue"></param>
    /// <param name="Value"></param>
    /// <returns>bool</returns>
    public bool InsertConfiguration
    (
        string connectionStringName,
        Int32 ConfigurationId,
        Int32 WarehouseId,
        Int32 ModuleId,
        String Configuration,
        Boolean Indicator,
        Int32 IntegerValue,
        String Value 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, ConfigurationId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
        db.AddInParameter(dbCommand, "Configuration", DbType.String, Configuration);
        db.AddInParameter(dbCommand, "Indicator", DbType.Boolean, Indicator);
        db.AddInParameter(dbCommand, "IntegerValue", DbType.Int32, IntegerValue);
        db.AddInParameter(dbCommand, "Value", DbType.String, Value);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertConfiguration
 
    #region SearchConfiguration
    /// <summary>
    ///   Selects rows from the Configuration table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ConfigurationId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ModuleId"></param>
    /// <param name="Configuration"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchConfiguration
    (
        string connectionStringName,
        Int32 ConfigurationId,
        Int32 WarehouseId,
        Int32 ModuleId,
        String Configuration 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, ConfigurationId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
        db.AddInParameter(dbCommand, "Configuration", DbType.String, Configuration);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchConfiguration
 
    #region PageSearchConfiguration
    /// <summary>
    ///   Selects rows from the Configuration table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ConfigurationId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ModuleId"></param>
    /// <param name="Configuration"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchConfiguration
    (
        string connectionStringName,
        Int32 ConfigurationId,
        Int32 WarehouseId,
        Int32 ModuleId,
        String Configuration,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ConfigurationId", DbType.Int32, ConfigurationId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
        db.AddInParameter(dbCommand, "Configuration", DbType.String, Configuration);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchConfiguration
 
    #region ListConfiguration
    /// <summary>
    ///   Lists rows from the Configuration table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListConfiguration
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ListConfiguration
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListConfiguration
 
    #region ParameterConfiguration
    /// <summary>
    ///   Lists rows from the Configuration table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterConfiguration
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ParameterConfiguration
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Configuration_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterConfiguration
}
