using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOperator
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:25:17
/// </summary>
/// <remarks>
///   Inserts a row into the Operator table.
///   Selects rows from the Operator table.
///   Searches for rows from the Operator table.
///   Updates a rows in the Operator table.
///   Deletes a row from the Operator table.
/// </remarks>
/// <param>
public class StaticInfoOperator
{
    #region "Constructor Logic"
    public StaticInfoOperator()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOperator
    /// <summary>
    ///   Selects rows from the Operator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOperator
    (
        string connectionStringName,
        Int32 OperatorId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetOperator
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOperator
 
    #region DeleteOperator
    /// <summary>
    ///   Deletes a row from the Operator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorId"></param>
    /// <returns>bool</returns>
    public bool DeleteOperator
    (
        string connectionStringName,
        Int32 OperatorId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOperator
 
    #region UpdateOperator
    /// <summary>
    ///   Updates a row in the Operator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Operator"></param>
    /// <param name="OperatorCode"></param>
    /// <param name="Password"></param>
    /// <param name="NewPassword"></param>
    /// <param name="ActiveIndicator"></param>
    /// <param name="ExpiryDate"></param>
    /// <param name="LastInstruction"></param>
    /// <param name="Printer"></param>
    /// <param name="Port"></param>
    /// <param name="IPAddress"></param>
    /// <param name="CultureId"></param>
    /// <param name="LoginTime"></param>
    /// <param name="LogoutTime"></param>
    /// <param name="LastActivity"></param>
    /// <param name="SiteType"></param>
    /// <param name="Name"></param>
    /// <param name="Surname"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="PickAisle"></param>
    /// <param name="StoreAisle"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="MenuId"></param>
    /// <param name="Upload"></param>
    /// <returns>bool</returns>
    public bool UpdateOperator
    (
        string connectionStringName,
        Int32 OperatorId,
        Int32 OperatorGroupId,
        Int32 WarehouseId,
        String Operator,
        String OperatorCode,
        String Password,
        String NewPassword,
        Boolean ActiveIndicator,
        DateTime ExpiryDate,
        DateTime LastInstruction,
        String Printer,
        String Port,
        String IPAddress,
        Int32 CultureId,
        DateTime LoginTime,
        DateTime LogoutTime,
        DateTime LastActivity,
        String SiteType,
        String Name,
        String Surname,
        Int32 PrincipalId,
        String PickAisle,
        String StoreAisle,
        Int32 ExternalCompanyId,
        Int32 MenuId,
        Boolean Upload 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
        db.AddInParameter(dbCommand, "OperatorCode", DbType.String, OperatorCode);
        db.AddInParameter(dbCommand, "Password", DbType.String, Password);
        db.AddInParameter(dbCommand, "NewPassword", DbType.String, NewPassword);
        db.AddInParameter(dbCommand, "ActiveIndicator", DbType.Boolean, ActiveIndicator);
        db.AddInParameter(dbCommand, "ExpiryDate", DbType.DateTime, ExpiryDate);
        db.AddInParameter(dbCommand, "LastInstruction", DbType.DateTime, LastInstruction);
        db.AddInParameter(dbCommand, "Printer", DbType.String, Printer);
        db.AddInParameter(dbCommand, "Port", DbType.String, Port);
        db.AddInParameter(dbCommand, "IPAddress", DbType.String, IPAddress);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "LoginTime", DbType.DateTime, LoginTime);
        db.AddInParameter(dbCommand, "LogoutTime", DbType.DateTime, LogoutTime);
        db.AddInParameter(dbCommand, "LastActivity", DbType.DateTime, LastActivity);
        db.AddInParameter(dbCommand, "SiteType", DbType.String, SiteType);
        db.AddInParameter(dbCommand, "Name", DbType.String, Name);
        db.AddInParameter(dbCommand, "Surname", DbType.String, Surname);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "PickAisle", DbType.String, PickAisle);
        db.AddInParameter(dbCommand, "StoreAisle", DbType.String, StoreAisle);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "Upload", DbType.Boolean, Upload);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOperator
 
    #region InsertOperator
    /// <summary>
    ///   Inserts a row into the Operator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Operator"></param>
    /// <param name="OperatorCode"></param>
    /// <param name="Password"></param>
    /// <param name="NewPassword"></param>
    /// <param name="ActiveIndicator"></param>
    /// <param name="ExpiryDate"></param>
    /// <param name="LastInstruction"></param>
    /// <param name="Printer"></param>
    /// <param name="Port"></param>
    /// <param name="IPAddress"></param>
    /// <param name="CultureId"></param>
    /// <param name="LoginTime"></param>
    /// <param name="LogoutTime"></param>
    /// <param name="LastActivity"></param>
    /// <param name="SiteType"></param>
    /// <param name="Name"></param>
    /// <param name="Surname"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="PickAisle"></param>
    /// <param name="StoreAisle"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="MenuId"></param>
    /// <param name="Upload"></param>
    /// <returns>bool</returns>
    public bool InsertOperator
    (
        string connectionStringName,
        Int32 OperatorId,
        Int32 OperatorGroupId,
        Int32 WarehouseId,
        String Operator,
        String OperatorCode,
        String Password,
        String NewPassword,
        Boolean ActiveIndicator,
        DateTime ExpiryDate,
        DateTime LastInstruction,
        String Printer,
        String Port,
        String IPAddress,
        Int32 CultureId,
        DateTime LoginTime,
        DateTime LogoutTime,
        DateTime LastActivity,
        String SiteType,
        String Name,
        String Surname,
        Int32 PrincipalId,
        String PickAisle,
        String StoreAisle,
        Int32 ExternalCompanyId,
        Int32 MenuId,
        Boolean Upload 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
        db.AddInParameter(dbCommand, "OperatorCode", DbType.String, OperatorCode);
        db.AddInParameter(dbCommand, "Password", DbType.String, Password);
        db.AddInParameter(dbCommand, "NewPassword", DbType.String, NewPassword);
        db.AddInParameter(dbCommand, "ActiveIndicator", DbType.Boolean, ActiveIndicator);
        db.AddInParameter(dbCommand, "ExpiryDate", DbType.DateTime, ExpiryDate);
        db.AddInParameter(dbCommand, "LastInstruction", DbType.DateTime, LastInstruction);
        db.AddInParameter(dbCommand, "Printer", DbType.String, Printer);
        db.AddInParameter(dbCommand, "Port", DbType.String, Port);
        db.AddInParameter(dbCommand, "IPAddress", DbType.String, IPAddress);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "LoginTime", DbType.DateTime, LoginTime);
        db.AddInParameter(dbCommand, "LogoutTime", DbType.DateTime, LogoutTime);
        db.AddInParameter(dbCommand, "LastActivity", DbType.DateTime, LastActivity);
        db.AddInParameter(dbCommand, "SiteType", DbType.String, SiteType);
        db.AddInParameter(dbCommand, "Name", DbType.String, Name);
        db.AddInParameter(dbCommand, "Surname", DbType.String, Surname);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "PickAisle", DbType.String, PickAisle);
        db.AddInParameter(dbCommand, "StoreAisle", DbType.String, StoreAisle);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "Upload", DbType.Boolean, Upload);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOperator
 
    #region SearchOperator
    /// <summary>
    ///   Selects rows from the Operator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Operator"></param>
    /// <param name="OperatorCode"></param>
    /// <param name="CultureId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="MenuId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOperator
    (
        string connectionStringName,
        Int32 OperatorId,
        Int32 OperatorGroupId,
        Int32 WarehouseId,
        String Operator,
        String OperatorCode,
        Int32 CultureId,
        Int32 PrincipalId,
        Int32 ExternalCompanyId,
        Int32 MenuId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
        db.AddInParameter(dbCommand, "OperatorCode", DbType.String, OperatorCode);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOperator
 
    #region PageSearchOperator
    /// <summary>
    ///   Selects rows from the Operator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Operator"></param>
    /// <param name="OperatorCode"></param>
    /// <param name="CultureId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="MenuId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchOperator
    (
        string connectionStringName,
        Int32 OperatorId,
        Int32 OperatorGroupId,
        Int32 WarehouseId,
        String Operator,
        String OperatorCode,
        Int32 CultureId,
        Int32 PrincipalId,
        Int32 ExternalCompanyId,
        Int32 MenuId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
        db.AddInParameter(dbCommand, "OperatorCode", DbType.String, OperatorCode);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchOperator
 
    #region ListOperator
    /// <summary>
    ///   Lists rows from the Operator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOperator
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ListOperator
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOperator
 
    #region ParameterOperator
    /// <summary>
    ///   Lists rows from the Operator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOperator
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ParameterOperator
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOperator
}
