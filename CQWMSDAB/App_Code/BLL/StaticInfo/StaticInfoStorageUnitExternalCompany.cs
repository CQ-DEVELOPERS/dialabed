using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoStorageUnitExternalCompany
///   Create By      : Grant Schultz
///   Date Created   : 14 May 2014 16:58:16
/// </summary>
/// <remarks>
///   Inserts a row into the StorageUnitExternalCompany table.
///   Selects rows from the StorageUnitExternalCompany table.
///   Searches for rows from the StorageUnitExternalCompany table.
///   Updates a rows in the StorageUnitExternalCompany table.
///   Deletes a row from the StorageUnitExternalCompany table.
/// </remarks>
/// <param>
public class StaticInfoStorageUnitExternalCompany
{
    #region "Constructor Logic"
    public StaticInfoStorageUnitExternalCompany()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetStorageUnitExternalCompany
    /// <summary>
    ///   Selects rows from the StorageUnitExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetStorageUnitExternalCompany
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 ExternalCompanyId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetStorageUnitExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetStorageUnitExternalCompany
 
    #region DeleteStorageUnitExternalCompany
    /// <summary>
    ///   Deletes a row from the StorageUnitExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>bool</returns>
    public bool DeleteStorageUnitExternalCompany
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 ExternalCompanyId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteStorageUnitExternalCompany
 
    #region UpdateStorageUnitExternalCompany
    /// <summary>
    ///   Updates a row in the StorageUnitExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="AllocationCategory"></param>
    /// <param name="MinimumShelfLife"></param>
    /// <param name="ProductCode"></param>
    /// <param name="SKUCode"></param>
    /// <param name="DefaultQC"></param>
    /// <param name="SendToQC"></param>
    /// <returns>bool</returns>
    public bool UpdateStorageUnitExternalCompany
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 ExternalCompanyId,
        Decimal AllocationCategory,
        Decimal MinimumShelfLife,
        String ProductCode,
        String SKUCode,
        Boolean DefaultQC,
        Boolean SendToQC 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "AllocationCategory", DbType.Decimal, AllocationCategory);
        db.AddInParameter(dbCommand, "MinimumShelfLife", DbType.Decimal, MinimumShelfLife);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, ProductCode);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, SKUCode);
        db.AddInParameter(dbCommand, "DefaultQC", DbType.Boolean, DefaultQC);
        db.AddInParameter(dbCommand, "SendToQC", DbType.Boolean, SendToQC);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateStorageUnitExternalCompany
 
    #region InsertStorageUnitExternalCompany
    /// <summary>
    ///   Inserts a row into the StorageUnitExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="AllocationCategory"></param>
    /// <param name="MinimumShelfLife"></param>
    /// <param name="ProductCode"></param>
    /// <param name="SKUCode"></param>
    /// <param name="DefaultQC"></param>
    /// <param name="SendToQC"></param>
    /// <returns>bool</returns>
    public bool InsertStorageUnitExternalCompany
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 ExternalCompanyId,
        Decimal AllocationCategory,
        Decimal MinimumShelfLife,
        String ProductCode,
        String SKUCode,
        Boolean DefaultQC,
        Boolean SendToQC 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "AllocationCategory", DbType.Decimal, AllocationCategory);
        db.AddInParameter(dbCommand, "MinimumShelfLife", DbType.Decimal, MinimumShelfLife);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, ProductCode);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, SKUCode);
        db.AddInParameter(dbCommand, "DefaultQC", DbType.Boolean, DefaultQC);
        db.AddInParameter(dbCommand, "SendToQC", DbType.Boolean, SendToQC);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertStorageUnitExternalCompany
 
    #region SearchStorageUnitExternalCompany
    /// <summary>
    ///   Selects rows from the StorageUnitExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchStorageUnitExternalCompany
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 ExternalCompanyId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchStorageUnitExternalCompany
 
    #region PageSearchStorageUnitExternalCompany
    /// <summary>
    ///   Selects rows from the StorageUnitExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchStorageUnitExternalCompany
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 ExternalCompanyId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchStorageUnitExternalCompany
 
    #region ListStorageUnitExternalCompany
    /// <summary>
    ///   Lists rows from the StorageUnitExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListStorageUnitExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListStorageUnitExternalCompany
 
    #region ParameterStorageUnitExternalCompany
    /// <summary>
    ///   Lists rows from the StorageUnitExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterStorageUnitExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitExternalCompany_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterStorageUnitExternalCompany
}
