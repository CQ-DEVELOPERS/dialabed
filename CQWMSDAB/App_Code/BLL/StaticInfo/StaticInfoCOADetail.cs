using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoCOADetail
///   Create By      : Grant Schultz
///   Date Created   : 04 Oct 2012 15:03:14
/// </summary>
/// <remarks>
///   Inserts a row into the COADetail table.
///   Selects rows from the COADetail table.
///   Searches for rows from the COADetail table.
///   Updates a rows in the COADetail table.
///   Deletes a row from the COADetail table.
/// </remarks>
/// <param>
public class StaticInfoCOADetail
{
    #region "Constructor Logic"
    public StaticInfoCOADetail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetCOADetail
    /// <summary>
    ///   Selects rows from the COADetail table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COADetailId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetCOADetail
    (
        string connectionStringName,
        Int32 COADetailId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COADetailId", DbType.Int32, COADetailId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetCOADetail
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetCOADetail
 
    #region DeleteCOADetail
    /// <summary>
    ///   Deletes a row from the COADetail table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COADetailId"></param>
    /// <returns>bool</returns>
    public bool DeleteCOADetail
    (
        string connectionStringName,
        Int32 COADetailId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COADetailId", DbType.Int32, COADetailId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteCOADetail
 
    #region UpdateCOADetail
    /// <summary>
    ///   Updates a row in the COADetail table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COADetailId"></param>
    /// <param name="COAId"></param>
    /// <param name="NoteId"></param>
    /// <param name="NoteCode"></param>
    /// <param name="Note"></param>
    /// <returns>bool</returns>
    public bool UpdateCOADetail
    (
        string connectionStringName,
        Int32 COADetailId,
        Int32 COAId,
        Int32 NoteId,
        String NoteCode,
        String Note 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COADetailId", DbType.Int32, COADetailId);
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
        db.AddInParameter(dbCommand, "NoteCode", DbType.String, NoteCode);
        db.AddInParameter(dbCommand, "Note", DbType.String, Note);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateCOADetail
 
    #region InsertCOADetail
    /// <summary>
    ///   Inserts a row into the COADetail table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COADetailId"></param>
    /// <param name="COAId"></param>
    /// <param name="NoteId"></param>
    /// <param name="NoteCode"></param>
    /// <param name="Note"></param>
    /// <returns>bool</returns>
    public bool InsertCOADetail
    (
        string connectionStringName,
        Int32 COADetailId,
        Int32 COAId,
        Int32 NoteId,
        String NoteCode,
        String Note 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COADetailId", DbType.Int32, COADetailId);
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
        db.AddInParameter(dbCommand, "NoteCode", DbType.String, NoteCode);
        db.AddInParameter(dbCommand, "Note", DbType.String, Note);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertCOADetail
 
    #region SearchCOADetail
    /// <summary>
    ///   Selects rows from the COADetail table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COADetailId"></param>
    /// <param name="COAId"></param>
    /// <param name="NoteId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchCOADetail
    (
        string connectionStringName,
        Int32 COADetailId,
        Int32 COAId,
        Int32 NoteId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COADetailId", DbType.Int32, COADetailId);
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchCOADetail
 
    #region PageSearchCOADetail
    /// <summary>
    ///   Selects rows from the COADetail table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COADetailId"></param>
    /// <param name="COAId"></param>
    /// <param name="NoteId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchCOADetail
    (
        string connectionStringName,
        Int32 COADetailId,
        Int32 COAId,
        Int32 NoteId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COADetailId", DbType.Int32, COADetailId);
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchCOADetail
 
    #region ListCOADetail
    /// <summary>
    ///   Lists rows from the COADetail table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListCOADetail
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListCOADetail
 
    #region ParameterCOADetail
    /// <summary>
    ///   Lists rows from the COADetail table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterCOADetail
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COADetail_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterCOADetail
}
