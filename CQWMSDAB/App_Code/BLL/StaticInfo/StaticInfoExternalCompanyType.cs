using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoExternalCompanyType
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:11:55
/// </summary>
/// <remarks>
///   Inserts a row into the ExternalCompanyType table.
///   Selects rows from the ExternalCompanyType table.
///   Searches for rows from the ExternalCompanyType table.
///   Updates a rows in the ExternalCompanyType table.
///   Deletes a row from the ExternalCompanyType table.
/// </remarks>
/// <param>
public class StaticInfoExternalCompanyType
{
    #region "Constructor Logic"
    public StaticInfoExternalCompanyType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetExternalCompanyType
    /// <summary>
    ///   Selects rows from the ExternalCompanyType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetExternalCompanyType
    (
        string connectionStringName,
        Int32 ExternalCompanyTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetExternalCompanyType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetExternalCompanyType
 
    #region DeleteExternalCompanyType
    /// <summary>
    ///   Deletes a row from the ExternalCompanyType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteExternalCompanyType
    (
        string connectionStringName,
        Int32 ExternalCompanyTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteExternalCompanyType
 
    #region UpdateExternalCompanyType
    /// <summary>
    ///   Updates a row in the ExternalCompanyType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="ExternalCompanyType"></param>
    /// <param name="ExternalCompanyTypeCode"></param>
    /// <returns>bool</returns>
    public bool UpdateExternalCompanyType
    (
        string connectionStringName,
        Int32 ExternalCompanyTypeId,
        String ExternalCompanyType,
        String ExternalCompanyTypeCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyType", DbType.String, ExternalCompanyType);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeCode", DbType.String, ExternalCompanyTypeCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateExternalCompanyType
 
    #region InsertExternalCompanyType
    /// <summary>
    ///   Inserts a row into the ExternalCompanyType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="ExternalCompanyType"></param>
    /// <param name="ExternalCompanyTypeCode"></param>
    /// <returns>bool</returns>
    public bool InsertExternalCompanyType
    (
        string connectionStringName,
        Int32 ExternalCompanyTypeId,
        String ExternalCompanyType,
        String ExternalCompanyTypeCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyType", DbType.String, ExternalCompanyType);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeCode", DbType.String, ExternalCompanyTypeCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertExternalCompanyType
 
    #region SearchExternalCompanyType
    /// <summary>
    ///   Selects rows from the ExternalCompanyType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="ExternalCompanyType"></param>
    /// <param name="ExternalCompanyTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchExternalCompanyType
    (
        string connectionStringName,
        Int32 ExternalCompanyTypeId,
        String ExternalCompanyType,
        String ExternalCompanyTypeCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyType", DbType.String, ExternalCompanyType);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeCode", DbType.String, ExternalCompanyTypeCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchExternalCompanyType
 
    #region PageSearchExternalCompanyType
    /// <summary>
    ///   Selects rows from the ExternalCompanyType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="ExternalCompanyType"></param>
    /// <param name="ExternalCompanyTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchExternalCompanyType
    (
        string connectionStringName,
        Int32 ExternalCompanyTypeId,
        String ExternalCompanyType,
        String ExternalCompanyTypeCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyType", DbType.String, ExternalCompanyType);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeCode", DbType.String, ExternalCompanyTypeCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchExternalCompanyType
 
    #region ListExternalCompanyType
    /// <summary>
    ///   Lists rows from the ExternalCompanyType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListExternalCompanyType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListExternalCompanyType
 
    #region ParameterExternalCompanyType
    /// <summary>
    ///   Lists rows from the ExternalCompanyType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterExternalCompanyType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompanyType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterExternalCompanyType
}
