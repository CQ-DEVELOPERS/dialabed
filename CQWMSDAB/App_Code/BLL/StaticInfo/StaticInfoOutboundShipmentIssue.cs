using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOutboundShipmentIssue
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:29
/// </summary>
/// <remarks>
///   Inserts a row into the OutboundShipmentIssue table.
///   Selects rows from the OutboundShipmentIssue table.
///   Searches for rows from the OutboundShipmentIssue table.
///   Updates a rows in the OutboundShipmentIssue table.
///   Deletes a row from the OutboundShipmentIssue table.
/// </remarks>
/// <param>
public class StaticInfoOutboundShipmentIssue
{
    #region "Constructor Logic"
    public StaticInfoOutboundShipmentIssue()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOutboundShipmentIssue
    /// <summary>
    ///   Selects rows from the OutboundShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOutboundShipmentIssue
    (
        string connectionStringName,
        Int32 OutboundShipmentId,
        Int32 IssueId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipmentIssue_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOutboundShipmentIssue
 
    #region DeleteOutboundShipmentIssue
    /// <summary>
    ///   Deletes a row from the OutboundShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <returns>bool</returns>
    public bool DeleteOutboundShipmentIssue
    (
        string connectionStringName,
        Int32 OutboundShipmentId,
        Int32 IssueId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipmentIssue_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOutboundShipmentIssue
 
    #region UpdateOutboundShipmentIssue
    /// <summary>
    ///   Updates a row in the OutboundShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <param name="DropSequence"></param>
    /// <returns>bool</returns>
    public bool UpdateOutboundShipmentIssue
    (
        string connectionStringName,
        Int32 OutboundShipmentId,
        Int32 IssueId,
        Int16 DropSequence 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipmentIssue_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int16, DropSequence);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOutboundShipmentIssue
 
    #region InsertOutboundShipmentIssue
    /// <summary>
    ///   Inserts a row into the OutboundShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <param name="DropSequence"></param>
    /// <returns>bool</returns>
    public bool InsertOutboundShipmentIssue
    (
        string connectionStringName,
        Int32 OutboundShipmentId,
        Int32 IssueId,
        Int16 DropSequence 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipmentIssue_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int16, DropSequence);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOutboundShipmentIssue
 
    #region SearchOutboundShipmentIssue
    /// <summary>
    ///   Selects rows from the OutboundShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOutboundShipmentIssue
    (
        string connectionStringName,
        Int32 OutboundShipmentId,
        Int32 IssueId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipmentIssue_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOutboundShipmentIssue
 
    #region ListOutboundShipmentIssue
    /// <summary>
    ///   Lists rows from the OutboundShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOutboundShipmentIssue
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipmentIssue_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOutboundShipmentIssue
 
    #region ParameterOutboundShipmentIssue
    /// <summary>
    ///   Lists rows from the OutboundShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOutboundShipmentIssue
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipmentIssue_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOutboundShipmentIssue
}
