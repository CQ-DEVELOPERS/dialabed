using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoIssue
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:11
/// </summary>
/// <remarks>
///   Inserts a row into the Issue table.
///   Selects rows from the Issue table.
///   Searches for rows from the Issue table.
///   Updates a rows in the Issue table.
///   Deletes a row from the Issue table.
/// </remarks>
/// <param>
public class StaticInfoIssue
{
    #region "Constructor Logic"
    public StaticInfoIssue()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetIssue
    /// <summary>
    ///   Selects rows from the Issue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetIssue
    (
        string connectionStringName,
        Int32 IssueId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Issue_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetIssue
 
    #region DeleteIssue
    /// <summary>
    ///   Deletes a row from the Issue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueId"></param>
    /// <returns>bool</returns>
    public bool DeleteIssue
    (
        string connectionStringName,
        Int32 IssueId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Issue_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteIssue
 
    #region UpdateIssue
    /// <summary>
    ///   Updates a row in the Issue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueId"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="RouteId"></param>
    /// <param name="DeliveryNoteNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <param name="SealNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="Remarks"></param>
    /// <param name="DropSequence"></param>
    /// <returns>bool</returns>
    public bool UpdateIssue
    (
        string connectionStringName,
        Int32 IssueId,
        Int32 OutboundDocumentId,
        Int32 PriorityId,
        Int32 WarehouseId,
        Int32 LocationId,
        Int32 StatusId,
        Int32 OperatorId,
        Int32 RouteId,
        String DeliveryNoteNumber,
        DateTime DeliveryDate,
        String SealNumber,
        String VehicleRegistration,
        String Remarks,
        Int32 DropSequence 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Issue_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "DeliveryNoteNumber", DbType.String, DeliveryNoteNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, SealNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, VehicleRegistration);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int32, DropSequence);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateIssue

    public bool UpdateIssueStatus(string connectionStringName, Int32 IssueId, Int32 StatusId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Issue_Update_Status";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    #region InsertIssue
    /// <summary>
    ///   Inserts a row into the Issue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueId"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="RouteId"></param>
    /// <param name="DeliveryNoteNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <param name="SealNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="Remarks"></param>
    /// <param name="DropSequence"></param>
    /// <returns>bool</returns>
    public bool InsertIssue
    (
        string connectionStringName,
        Int32 IssueId,
        Int32 OutboundDocumentId,
        Int32 PriorityId,
        Int32 WarehouseId,
        Int32 LocationId,
        Int32 StatusId,
        Int32 OperatorId,
        Int32 RouteId,
        String DeliveryNoteNumber,
        DateTime DeliveryDate,
        String SealNumber,
        String VehicleRegistration,
        String Remarks,
        Int32 DropSequence 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Issue_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "DeliveryNoteNumber", DbType.String, DeliveryNoteNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, SealNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, VehicleRegistration);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int32, DropSequence);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertIssue
 
    #region SearchIssue
    /// <summary>
    ///   Selects rows from the Issue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueId"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="RouteId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchIssue
    (
        string connectionStringName,
        Int32 IssueId,
        Int32 OutboundDocumentId,
        Int32 PriorityId,
        Int32 WarehouseId,
        Int32 LocationId,
        Int32 StatusId,
        Int32 OperatorId,
        Int32 RouteId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Issue_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchIssue
 
    #region ListIssue
    /// <summary>
    ///   Lists rows from the Issue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListIssue
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Issue_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListIssue
 
    #region ParameterIssue
    /// <summary>
    ///   Lists rows from the Issue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterIssue
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Issue_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterIssue
}
