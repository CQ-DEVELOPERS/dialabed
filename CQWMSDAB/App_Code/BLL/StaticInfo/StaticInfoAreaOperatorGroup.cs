using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoAreaOperatorGroup
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:14:54
/// </summary>
/// <remarks>
///   Inserts a row into the AreaOperatorGroup table.
///   Selects rows from the AreaOperatorGroup table.
///   Searches for rows from the AreaOperatorGroup table.
///   Updates a rows in the AreaOperatorGroup table.
///   Deletes a row from the AreaOperatorGroup table.
/// </remarks>
/// <param>
public class StaticInfoAreaOperatorGroup
{
    #region "Constructor Logic"
    public StaticInfoAreaOperatorGroup()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetAreaOperatorGroup
    /// <summary>
    ///   Selects rows from the AreaOperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetAreaOperatorGroup
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorGroupId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetAreaOperatorGroup
 
    #region DeleteAreaOperatorGroup
    /// <summary>
    ///   Deletes a row from the AreaOperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>bool</returns>
    public bool DeleteAreaOperatorGroup
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorGroupId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteAreaOperatorGroup
 
    #region UpdateAreaOperatorGroup
    /// <summary>
    ///   Updates a row in the AreaOperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>bool</returns>
    public bool UpdateAreaOperatorGroup
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorGroupId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateAreaOperatorGroup
 
    #region InsertAreaOperatorGroup
    /// <summary>
    ///   Inserts a row into the AreaOperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>bool</returns>
    public bool InsertAreaOperatorGroup
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorGroupId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertAreaOperatorGroup
 
    #region SearchAreaOperatorGroup
    /// <summary>
    ///   Selects rows from the AreaOperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchAreaOperatorGroup
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorGroupId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchAreaOperatorGroup
 
    #region ListAreaOperatorGroup
    /// <summary>
    ///   Lists rows from the AreaOperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListAreaOperatorGroup
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListAreaOperatorGroup
 
    #region ParameterAreaOperatorGroup
    /// <summary>
    ///   Lists rows from the AreaOperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterAreaOperatorGroup
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperatorGroup_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterAreaOperatorGroup
}
