using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOutboundShipment
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:28
/// </summary>
/// <remarks>
///   Inserts a row into the OutboundShipment table.
///   Selects rows from the OutboundShipment table.
///   Searches for rows from the OutboundShipment table.
///   Updates a rows in the OutboundShipment table.
///   Deletes a row from the OutboundShipment table.
/// </remarks>
/// <param>
public class StaticInfoOutboundShipment
{
    #region "Constructor Logic"
    public StaticInfoOutboundShipment()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOutboundShipment
    /// <summary>
    ///   Selects rows from the OutboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOutboundShipment
    (
        string connectionStringName,
        Int32 OutboundShipmentId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOutboundShipment
 
    #region DeleteOutboundShipment
    /// <summary>
    ///   Deletes a row from the OutboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <returns>bool</returns>
    public bool DeleteOutboundShipment
    (
        string connectionStringName,
        Int32 OutboundShipmentId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOutboundShipment
 
    #region UpdateOutboundShipment
    /// <summary>
    ///   Updates a row in the OutboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="ShipmentDate"></param>
    /// <param name="Remarks"></param>
    /// <param name="SealNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="Route"></param>
    /// <returns>bool</returns>
    public bool UpdateOutboundShipment
    (
        string connectionStringName,
        Int32 OutboundShipmentId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 LocationId,
        DateTime ShipmentDate,
        String Remarks,
        String SealNumber,
        String VehicleRegistration,
        String Route 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "ShipmentDate", DbType.DateTime, ShipmentDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, SealNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, VehicleRegistration);
        db.AddInParameter(dbCommand, "Route", DbType.String, Route);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOutboundShipment
 
    #region InsertOutboundShipment
    /// <summary>
    ///   Inserts a row into the OutboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="ShipmentDate"></param>
    /// <param name="Remarks"></param>
    /// <param name="SealNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="Route"></param>
    /// <returns>bool</returns>
    public bool InsertOutboundShipment
    (
        string connectionStringName,
        Int32 OutboundShipmentId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 LocationId,
        DateTime ShipmentDate,
        String Remarks,
        String SealNumber,
        String VehicleRegistration,
        String Route 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "ShipmentDate", DbType.DateTime, ShipmentDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, SealNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, VehicleRegistration);
        db.AddInParameter(dbCommand, "Route", DbType.String, Route);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOutboundShipment
 
    #region SearchOutboundShipment
    /// <summary>
    ///   Selects rows from the OutboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOutboundShipment
    (
        string connectionStringName,
        Int32 OutboundShipmentId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOutboundShipment
 
    #region ListOutboundShipment
    /// <summary>
    ///   Lists rows from the OutboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOutboundShipment
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOutboundShipment
 
    #region ParameterOutboundShipment
    /// <summary>
    ///   Lists rows from the OutboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOutboundShipment
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundShipment_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOutboundShipment
}
