using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoLocation
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:14
/// </summary>
/// <remarks>
///   Inserts a row into the Location table.
///   Selects rows from the Location table.
///   Searches for rows from the Location table.
///   Updates a rows in the Location table.
///   Deletes a row from the Location table.
/// </remarks>
/// <param>
public class StaticInfoLocation
{
    #region "Constructor Logic"
    public StaticInfoLocation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetLocation
    /// <summary>
    ///   Selects rows from the Location table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetLocation
    (
        string connectionStringName,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Location_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetLocation
 
    #region DeleteLocation
    /// <summary>
    ///   Deletes a row from the Location table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationId"></param>
    /// <returns>bool</returns>
    public bool DeleteLocation
    (
        string connectionStringName,
        Int32 LocationId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Location_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteLocation
 
    #region UpdateLocation
    /// <summary>
    ///   Updates a row in the Location table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationId"></param>
    /// <param name="LocationTypeId"></param>
    /// <param name="Location"></param>
    /// <param name="Ailse"></param>
    /// <param name="Column"></param>
    /// <param name="Level"></param>
    /// <param name="PalletQuantity"></param>
    /// <param name="SecurityCode"></param>
    /// <param name="RelativeValue"></param>
    /// <param name="NumberOfSUB"></param>
    /// <param name="StocktakeInd"></param>
    /// <param name="ActiveBinning"></param>
    /// <param name="ActivePicking"></param>
    /// <param name="Used"></param>
    /// <returns>bool</returns>
    public bool UpdateLocation
    (
        string connectionStringName,
        Int32 LocationId,
        Int32 LocationTypeId,
        String Location,
        String Ailse,
        String Column,
        String Level,
        Int32 PalletQuantity,
        Int32 SecurityCode,
        Decimal RelativeValue,
        Int32 NumberOfSUB,
        Boolean StocktakeInd,
        Boolean ActiveBinning,
        Boolean ActivePicking,
        Boolean Used 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Location_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
        db.AddInParameter(dbCommand, "Location", DbType.String, Location);
        db.AddInParameter(dbCommand, "Ailse", DbType.String, Ailse);
        db.AddInParameter(dbCommand, "Column", DbType.String, Column);
        db.AddInParameter(dbCommand, "Level", DbType.String, Level);
        db.AddInParameter(dbCommand, "PalletQuantity", DbType.Decimal, PalletQuantity);
        db.AddInParameter(dbCommand, "SecurityCode", DbType.Int32, SecurityCode);
        db.AddInParameter(dbCommand, "RelativeValue", DbType.Decimal, RelativeValue);
        db.AddInParameter(dbCommand, "NumberOfSUB", DbType.Int32, NumberOfSUB);
        db.AddInParameter(dbCommand, "StocktakeInd", DbType.Boolean, StocktakeInd);
        db.AddInParameter(dbCommand, "ActiveBinning", DbType.Boolean, ActiveBinning);
        db.AddInParameter(dbCommand, "ActivePicking", DbType.Boolean, ActivePicking);
        db.AddInParameter(dbCommand, "Used", DbType.Boolean, Used);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateLocation
 
    #region InsertLocation
    /// <summary>
    ///   Inserts a row into the Location table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationId"></param>
    /// <param name="LocationTypeId"></param>
    /// <param name="Location"></param>
    /// <param name="Ailse"></param>
    /// <param name="Column"></param>
    /// <param name="Level"></param>
    /// <param name="PalletQuantity"></param>
    /// <param name="SecurityCode"></param>
    /// <param name="RelativeValue"></param>
    /// <param name="NumberOfSUB"></param>
    /// <param name="StocktakeInd"></param>
    /// <param name="ActiveBinning"></param>
    /// <param name="ActivePicking"></param>
    /// <param name="Used"></param>
    /// <returns>bool</returns>
    public bool InsertLocation
    (
        string connectionStringName,
        Int32 LocationId,
        Int32 LocationTypeId,
        String Location,
        String Ailse,
        String Column,
        String Level,
        Int32 PalletQuantity,
        Int32 SecurityCode,
        Decimal RelativeValue,
        Int32 NumberOfSUB,
        Boolean StocktakeInd,
        Boolean ActiveBinning,
        Boolean ActivePicking,
        Boolean Used 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Location_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
        db.AddInParameter(dbCommand, "Location", DbType.String, Location);
        db.AddInParameter(dbCommand, "Ailse", DbType.String, Ailse);
        db.AddInParameter(dbCommand, "Column", DbType.String, Column);
        db.AddInParameter(dbCommand, "Level", DbType.String, Level);
        db.AddInParameter(dbCommand, "PalletQuantity", DbType.Decimal, PalletQuantity);
        db.AddInParameter(dbCommand, "SecurityCode", DbType.Int32, SecurityCode);
        db.AddInParameter(dbCommand, "RelativeValue", DbType.Decimal, RelativeValue);
        db.AddInParameter(dbCommand, "NumberOfSUB", DbType.Int32, NumberOfSUB);
        db.AddInParameter(dbCommand, "StocktakeInd", DbType.Boolean, StocktakeInd);
        db.AddInParameter(dbCommand, "ActiveBinning", DbType.Boolean, ActiveBinning);
        db.AddInParameter(dbCommand, "ActivePicking", DbType.Boolean, ActivePicking);
        db.AddInParameter(dbCommand, "Used", DbType.Boolean, Used);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertLocation
 
    #region SearchLocation
    /// <summary>
    ///   Selects rows from the Location table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="LocationId"></param>
    /// <param name="LocationTypeId"></param>
    /// <param name="Location"></param>
    /// <param name="SecurityCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchLocation
    (
        string connectionStringName,
        Int32 LocationId,
        Int32 LocationTypeId,
        String Location,
        Int32 SecurityCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Location_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, LocationTypeId);
        db.AddInParameter(dbCommand, "Location", DbType.String, Location);
        db.AddInParameter(dbCommand, "SecurityCode", DbType.Int32, SecurityCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchLocation
 
    #region ListLocation
    /// <summary>
    ///   Lists rows from the Location table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListLocation
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Location_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListLocation
 
    #region ParameterLocation
    /// <summary>
    ///   Lists rows from the Location table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterLocation
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Location_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterLocation
}
