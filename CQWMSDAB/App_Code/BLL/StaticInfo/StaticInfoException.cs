using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoException
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:00
/// </summary>
/// <remarks>
///   Inserts a row into the Exception table.
///   Selects rows from the Exception table.
///   Searches for rows from the Exception table.
///   Updates a rows in the Exception table.
///   Deletes a row from the Exception table.
/// </remarks>
/// <param>
public class StaticInfoException
{
    #region "Constructor Logic"
    public StaticInfoException()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetException
    /// <summary>
    ///   Selects rows from the Exception table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExceptionId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetException
    (
        string connectionStringName,
        Int32 ExceptionId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Exception_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExceptionId", DbType.Int32, ExceptionId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetException
 
    #region DeleteException
    /// <summary>
    ///   Deletes a row from the Exception table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExceptionId"></param>
    /// <returns>bool</returns>
    public bool DeleteException
    (
        string connectionStringName,
        Int32 ExceptionId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Exception_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExceptionId", DbType.Int32, ExceptionId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteException
 
    #region UpdateException
    /// <summary>
    ///   Updates a row in the Exception table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExceptionId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="InstructionId"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="ReasonId"></param>
    /// <param name="Exception"></param>
    /// <param name="ExceptionCode"></param>
    /// <param name="CreateDate"></param>
    /// <returns>bool</returns>
    public bool UpdateException
    (
        string connectionStringName,
        Int32 ExceptionId,
        Int32 ReceiptLineId,
        Int32 InstructionId,
        Int32 IssueLineId,
        Int32 ReasonId,
        String Exception,
        String ExceptionCode,
        DateTime CreateDate 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Exception_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExceptionId", DbType.Int32, ExceptionId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "Exception", DbType.String, Exception);
        db.AddInParameter(dbCommand, "ExceptionCode", DbType.String, ExceptionCode);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateException
 
    #region InsertException
    /// <summary>
    ///   Inserts a row into the Exception table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExceptionId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="InstructionId"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="ReasonId"></param>
    /// <param name="Exception"></param>
    /// <param name="ExceptionCode"></param>
    /// <param name="CreateDate"></param>
    /// <returns>bool</returns>
    public bool InsertException
    (
        string connectionStringName,
        Int32 ExceptionId,
        Int32 ReceiptLineId,
        Int32 InstructionId,
        Int32 IssueLineId,
        Int32 ReasonId,
        String Exception,
        String ExceptionCode,
        DateTime CreateDate 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Exception_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExceptionId", DbType.Int32, ExceptionId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "Exception", DbType.String, Exception);
        db.AddInParameter(dbCommand, "ExceptionCode", DbType.String, ExceptionCode);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertException
 
    #region SearchException
    /// <summary>
    ///   Selects rows from the Exception table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExceptionId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="InstructionId"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="ReasonId"></param>
    /// <param name="Exception"></param>
    /// <param name="ExceptionCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchException
    (
        string connectionStringName,
        Int32 ExceptionId,
        Int32 ReceiptLineId,
        Int32 InstructionId,
        Int32 IssueLineId,
        Int32 ReasonId,
        String Exception,
        String ExceptionCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Exception_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExceptionId", DbType.Int32, ExceptionId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "Exception", DbType.String, Exception);
        db.AddInParameter(dbCommand, "ExceptionCode", DbType.String, ExceptionCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchException
 
    #region ListException
    /// <summary>
    ///   Lists rows from the Exception table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListException
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Exception_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListException
 
    #region ParameterException
    /// <summary>
    ///   Lists rows from the Exception table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterException
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Exception_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterException
}
