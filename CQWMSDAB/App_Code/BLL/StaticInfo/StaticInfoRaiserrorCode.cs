using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoRaiserrorCode
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:35
/// </summary>
/// <remarks>
///   Inserts a row into the RaiserrorCode table.
///   Selects rows from the RaiserrorCode table.
///   Searches for rows from the RaiserrorCode table.
///   Updates a rows in the RaiserrorCode table.
///   Deletes a row from the RaiserrorCode table.
/// </remarks>
/// <param>
public class StaticInfoRaiserrorCode
{
    #region "Constructor Logic"
    public StaticInfoRaiserrorCode()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetRaiserrorCode
    /// <summary>
    ///   Selects rows from the RaiserrorCode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RaiserrorCodeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetRaiserrorCode
    (
        string connectionStringName,
        Int32 RaiserrorCodeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RaiserrorCode_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RaiserrorCodeId", DbType.Int32, RaiserrorCodeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetRaiserrorCode
 
    #region DeleteRaiserrorCode
    /// <summary>
    ///   Deletes a row from the RaiserrorCode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RaiserrorCodeId"></param>
    /// <returns>bool</returns>
    public bool DeleteRaiserrorCode
    (
        string connectionStringName,
        Int32 RaiserrorCodeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RaiserrorCode_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RaiserrorCodeId", DbType.Int32, RaiserrorCodeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteRaiserrorCode
 
    #region UpdateRaiserrorCode
    /// <summary>
    ///   Updates a row in the RaiserrorCode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RaiserrorCodeId"></param>
    /// <param name="RaiserrorCode"></param>
    /// <param name="RaiserrorMessage"></param>
    /// <param name="StoredProcedure"></param>
    /// <returns>bool</returns>
    public bool UpdateRaiserrorCode
    (
        string connectionStringName,
        Int32 RaiserrorCodeId,
        String RaiserrorCode,
        String RaiserrorMessage,
        String StoredProcedure 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RaiserrorCode_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RaiserrorCodeId", DbType.Int32, RaiserrorCodeId);
        db.AddInParameter(dbCommand, "RaiserrorCode", DbType.String, RaiserrorCode);
        db.AddInParameter(dbCommand, "RaiserrorMessage", DbType.String, RaiserrorMessage);
        db.AddInParameter(dbCommand, "StoredProcedure", DbType.String, StoredProcedure);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateRaiserrorCode
 
    #region InsertRaiserrorCode
    /// <summary>
    ///   Inserts a row into the RaiserrorCode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RaiserrorCodeId"></param>
    /// <param name="RaiserrorCode"></param>
    /// <param name="RaiserrorMessage"></param>
    /// <param name="StoredProcedure"></param>
    /// <returns>bool</returns>
    public bool InsertRaiserrorCode
    (
        string connectionStringName,
        Int32 RaiserrorCodeId,
        String RaiserrorCode,
        String RaiserrorMessage,
        String StoredProcedure 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RaiserrorCode_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RaiserrorCodeId", DbType.Int32, RaiserrorCodeId);
        db.AddInParameter(dbCommand, "RaiserrorCode", DbType.String, RaiserrorCode);
        db.AddInParameter(dbCommand, "RaiserrorMessage", DbType.String, RaiserrorMessage);
        db.AddInParameter(dbCommand, "StoredProcedure", DbType.String, StoredProcedure);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertRaiserrorCode
 
    #region SearchRaiserrorCode
    /// <summary>
    ///   Selects rows from the RaiserrorCode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RaiserrorCodeId"></param>
    /// <param name="RaiserrorCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchRaiserrorCode
    (
        string connectionStringName,
        Int32 RaiserrorCodeId,
        String RaiserrorCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RaiserrorCode_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RaiserrorCodeId", DbType.Int32, RaiserrorCodeId);
        db.AddInParameter(dbCommand, "RaiserrorCode", DbType.String, RaiserrorCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchRaiserrorCode
 
    #region ListRaiserrorCode
    /// <summary>
    ///   Lists rows from the RaiserrorCode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListRaiserrorCode
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RaiserrorCode_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListRaiserrorCode
 
    #region ParameterRaiserrorCode
    /// <summary>
    ///   Lists rows from the RaiserrorCode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterRaiserrorCode
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RaiserrorCode_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterRaiserrorCode
}
