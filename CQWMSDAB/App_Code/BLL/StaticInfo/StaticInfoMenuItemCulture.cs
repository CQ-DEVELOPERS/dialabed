using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoMenuItemCulture
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:09
/// </summary>
/// <remarks>
///   Inserts a row into the MenuItemCulture table.
///   Selects rows from the MenuItemCulture table.
///   Searches for rows from the MenuItemCulture table.
///   Updates a rows in the MenuItemCulture table.
///   Deletes a row from the MenuItemCulture table.
/// </remarks>
/// <param>
public class StaticInfoMenuItemCulture
{
    #region "Constructor Logic"
    public StaticInfoMenuItemCulture()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetMenuItemCulture
    /// <summary>
    ///   Selects rows from the MenuItemCulture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemCultureId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetMenuItemCulture
    (
        string connectionStringName,
        Int32 MenuItemCultureId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemCultureId", DbType.Int32, MenuItemCultureId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetMenuItemCulture
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetMenuItemCulture
 
    #region DeleteMenuItemCulture
    /// <summary>
    ///   Deletes a row from the MenuItemCulture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemCultureId"></param>
    /// <returns>bool</returns>
    public bool DeleteMenuItemCulture
    (
        string connectionStringName,
        Int32 MenuItemCultureId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemCultureId", DbType.Int32, MenuItemCultureId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteMenuItemCulture
 
    #region UpdateMenuItemCulture
    /// <summary>
    ///   Updates a row in the MenuItemCulture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemCultureId"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="CultureId"></param>
    /// <param name="MenuItem"></param>
    /// <param name="ToolTip"></param>
    /// <returns>bool</returns>
    public bool UpdateMenuItemCulture
    (
        string connectionStringName,
        Int32 MenuItemCultureId,
        Int32 MenuItemId,
        Int32 CultureId,
        String MenuItem,
        String ToolTip 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemCultureId", DbType.Int32, MenuItemCultureId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "MenuItem", DbType.String, MenuItem);
        db.AddInParameter(dbCommand, "ToolTip", DbType.String, ToolTip);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateMenuItemCulture
 
    #region InsertMenuItemCulture
    /// <summary>
    ///   Inserts a row into the MenuItemCulture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemCultureId"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="CultureId"></param>
    /// <param name="MenuItem"></param>
    /// <param name="ToolTip"></param>
    /// <returns>bool</returns>
    public bool InsertMenuItemCulture
    (
        string connectionStringName,
        Int32 MenuItemCultureId,
        Int32 MenuItemId,
        Int32 CultureId,
        String MenuItem,
        String ToolTip 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemCultureId", DbType.Int32, MenuItemCultureId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "MenuItem", DbType.String, MenuItem);
        db.AddInParameter(dbCommand, "ToolTip", DbType.String, ToolTip);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertMenuItemCulture
 
    #region SearchMenuItemCulture
    /// <summary>
    ///   Selects rows from the MenuItemCulture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemCultureId"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="CultureId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchMenuItemCulture
    (
        string connectionStringName,
        Int32 MenuItemCultureId,
        Int32 MenuItemId,
        Int32 CultureId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemCultureId", DbType.Int32, MenuItemCultureId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchMenuItemCulture
 
    #region PageSearchMenuItemCulture
    /// <summary>
    ///   Selects rows from the MenuItemCulture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemCultureId"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="CultureId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchMenuItemCulture
    (
        string connectionStringName,
        Int32 MenuItemCultureId,
        Int32 MenuItemId,
        Int32 CultureId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemCultureId", DbType.Int32, MenuItemCultureId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchMenuItemCulture
 
    #region ListMenuItemCulture
    /// <summary>
    ///   Lists rows from the MenuItemCulture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListMenuItemCulture
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListMenuItemCulture
 
    #region ParameterMenuItemCulture
    /// <summary>
    ///   Lists rows from the MenuItemCulture table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterMenuItemCulture
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItemCulture_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterMenuItemCulture
}
