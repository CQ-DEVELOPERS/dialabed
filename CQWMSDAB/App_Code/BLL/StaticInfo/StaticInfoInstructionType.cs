using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInstructionType
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:11:58
/// </summary>
/// <remarks>
///   Inserts a row into the InstructionType table.
///   Selects rows from the InstructionType table.
///   Searches for rows from the InstructionType table.
///   Updates a rows in the InstructionType table.
///   Deletes a row from the InstructionType table.
/// </remarks>
/// <param>
public class StaticInfoInstructionType
{
    #region "Constructor Logic"
    public StaticInfoInstructionType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInstructionType
    /// <summary>
    ///   Selects rows from the InstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInstructionType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInstructionType
 
    #region DeleteInstructionType
    /// <summary>
    ///   Deletes a row from the InstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInstructionType
 
    #region UpdateInstructionType
    /// <summary>
    ///   Updates a row in the InstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="InstructionType"></param>
    /// <param name="InstructionTypeCode"></param>
    /// <returns>bool</returns>
    public bool UpdateInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        Int32 PriorityId,
        String InstructionType,
        String InstructionTypeCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "InstructionType", DbType.String, InstructionType);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, InstructionTypeCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInstructionType
 
    #region InsertInstructionType
    /// <summary>
    ///   Inserts a row into the InstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="InstructionType"></param>
    /// <param name="InstructionTypeCode"></param>
    /// <returns>bool</returns>
    public bool InsertInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        Int32 PriorityId,
        String InstructionType,
        String InstructionTypeCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "InstructionType", DbType.String, InstructionType);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, InstructionTypeCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInstructionType
 
    #region SearchInstructionType
    /// <summary>
    ///   Selects rows from the InstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="InstructionType"></param>
    /// <param name="InstructionTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        String InstructionType,
        String InstructionTypeCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "InstructionType", DbType.String, InstructionType);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, InstructionTypeCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInstructionType
 
    #region PageSearchInstructionType
    /// <summary>
    ///   Selects rows from the InstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="InstructionType"></param>
    /// <param name="InstructionTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        String InstructionType,
        String InstructionTypeCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "InstructionType", DbType.String, InstructionType);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, InstructionTypeCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInstructionType
 
    #region ListInstructionType
    /// <summary>
    ///   Lists rows from the InstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInstructionType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInstructionType
 
    #region ParameterInstructionType
    /// <summary>
    ///   Lists rows from the InstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInstructionType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InstructionType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInstructionType
}
