using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoShipmentIssue
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:43
/// </summary>
/// <remarks>
///   Inserts a row into the ShipmentIssue table.
///   Selects rows from the ShipmentIssue table.
///   Searches for rows from the ShipmentIssue table.
///   Updates a rows in the ShipmentIssue table.
///   Deletes a row from the ShipmentIssue table.
/// </remarks>
/// <param>
public class StaticInfoShipmentIssue
{
    #region "Constructor Logic"
    public StaticInfoShipmentIssue()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetShipmentIssue
    /// <summary>
    ///   Selects rows from the ShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetShipmentIssue
    (
        string connectionStringName,
        Int32 ShipmentId,
        Int32 IssueId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ShipmentIssue_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetShipmentIssue
 
    #region DeleteShipmentIssue
    /// <summary>
    ///   Deletes a row from the ShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <returns>bool</returns>
    public bool DeleteShipmentIssue
    (
        string connectionStringName,
        Int32 ShipmentId,
        Int32 IssueId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ShipmentIssue_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteShipmentIssue
 
    #region UpdateShipmentIssue
    /// <summary>
    ///   Updates a row in the ShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <param name="DropSequence"></param>
    /// <returns>bool</returns>
    public bool UpdateShipmentIssue
    (
        string connectionStringName,
        Int32 ShipmentId,
        Int32 IssueId,
        Int16 DropSequence 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ShipmentIssue_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int16, DropSequence);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateShipmentIssue
 
    #region InsertShipmentIssue
    /// <summary>
    ///   Inserts a row into the ShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <param name="DropSequence"></param>
    /// <returns>bool</returns>
    public bool InsertShipmentIssue
    (
        string connectionStringName,
        Int32 ShipmentId,
        Int32 IssueId,
        Int16 DropSequence 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ShipmentIssue_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int16, DropSequence);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertShipmentIssue
 
    #region SearchShipmentIssue
    /// <summary>
    ///   Selects rows from the ShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchShipmentIssue
    (
        string connectionStringName,
        Int32 ShipmentId,
        Int32 IssueId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ShipmentIssue_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchShipmentIssue
 
    #region ListShipmentIssue
    /// <summary>
    ///   Lists rows from the ShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListShipmentIssue
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ShipmentIssue_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListShipmentIssue
 
    #region ParameterShipmentIssue
    /// <summary>
    ///   Lists rows from the ShipmentIssue table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterShipmentIssue
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ShipmentIssue_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterShipmentIssue
}
