using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInstruction
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:09
/// </summary>
/// <remarks>
///   Inserts a row into the Instruction table.
///   Selects rows from the Instruction table.
///   Searches for rows from the Instruction table.
///   Updates a rows in the Instruction table.
///   Deletes a row from the Instruction table.
/// </remarks>
/// <param>
public class StaticInfoInstruction
{
    #region "Constructor Logic"
    public StaticInfoInstruction()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInstruction
    /// <summary>
    ///   Selects rows from the Instruction table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInstruction
    (
        string connectionStringName,
        Int32 InstructionId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInstruction
 
    #region DeleteInstruction
    /// <summary>
    ///   Deletes a row from the Instruction table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <returns>bool</returns>
    public bool DeleteInstruction
    (
        string connectionStringName,
        Int32 InstructionId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInstruction
 
    #region UpdateInstruction
    /// <summary>
    ///   Updates a row in the Instruction table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <param name="ReasonId"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="StatusId"></param>
    /// <param name="JobId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="PickLocationId"></param>
    /// <param name="StoreLocationId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="InstructionRefId"></param>
    /// <param name="Quantity"></param>
    /// <param name="ConfirmedQuantity"></param>
    /// <param name="Weight"></param>
    /// <param name="ConfirmedWeight"></param>
    /// <param name="PalletId"></param>
    /// <param name="CreateDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="Picked"></param>
    /// <param name="Stored"></param>
    /// <param name="CheckQuantity"></param>
    /// <param name="CheckWeight"></param>
    /// <returns>bool</returns>
    public bool UpdateInstruction
    (
        string connectionStringName,
        Int32 InstructionId,
        Int32 ReasonId,
        Int32 InstructionTypeId,
        Int32 StorageUnitBatchId,
        Int32 WarehouseId,
        Int32 StatusId,
        Int32 JobId,
        Int32 OperatorId,
        Int32 PickLocationId,
        Int32 StoreLocationId,
        Int32 ReceiptLineId,
        Int32 IssueLineId,
        Int32 InstructionRefId,
        Decimal Quantity,
        Decimal ConfirmedQuantity,
        Decimal Weight,
        Decimal ConfirmedWeight,
        Int32 PalletId,
        DateTime CreateDate,
        DateTime StartDate,
        DateTime EndDate,
        Boolean Picked,
        Boolean Stored,
        Decimal CheckQuantity,
        Decimal CheckWeight 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "PickLocationId", DbType.Int32, PickLocationId);
        db.AddInParameter(dbCommand, "StoreLocationId", DbType.Int32, StoreLocationId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "InstructionRefId", DbType.Int32, InstructionRefId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "ConfirmedQuantity", DbType.Decimal, ConfirmedQuantity);
        db.AddInParameter(dbCommand, "Weight", DbType.Decimal, Weight);
        db.AddInParameter(dbCommand, "ConfirmedWeight", DbType.Decimal, ConfirmedWeight);
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "StartDate", DbType.DateTime, StartDate);
        db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);
        db.AddInParameter(dbCommand, "Picked", DbType.Boolean, Picked);
        db.AddInParameter(dbCommand, "Stored", DbType.Boolean, Stored);
        db.AddInParameter(dbCommand, "CheckQuantity", DbType.Decimal, CheckQuantity);
        db.AddInParameter(dbCommand, "CheckWeight", DbType.Decimal, CheckWeight);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInstruction
 
    #region InsertInstruction
    /// <summary>
    ///   Inserts a row into the Instruction table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <param name="ReasonId"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="StatusId"></param>
    /// <param name="JobId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="PickLocationId"></param>
    /// <param name="StoreLocationId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="InstructionRefId"></param>
    /// <param name="Quantity"></param>
    /// <param name="ConfirmedQuantity"></param>
    /// <param name="Weight"></param>
    /// <param name="ConfirmedWeight"></param>
    /// <param name="PalletId"></param>
    /// <param name="CreateDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="Picked"></param>
    /// <param name="Stored"></param>
    /// <param name="CheckQuantity"></param>
    /// <param name="CheckWeight"></param>
    /// <returns>bool</returns>
    public bool InsertInstruction
    (
        string connectionStringName,
        Int32 InstructionId,
        Int32 ReasonId,
        Int32 InstructionTypeId,
        Int32 StorageUnitBatchId,
        Int32 WarehouseId,
        Int32 StatusId,
        Int32 JobId,
        Int32 OperatorId,
        Int32 PickLocationId,
        Int32 StoreLocationId,
        Int32 ReceiptLineId,
        Int32 IssueLineId,
        Int32 InstructionRefId,
        Decimal Quantity,
        Decimal ConfirmedQuantity,
        Decimal Weight,
        Decimal ConfirmedWeight,
        Int32 PalletId,
        DateTime CreateDate,
        DateTime StartDate,
        DateTime EndDate,
        Boolean Picked,
        Boolean Stored,
        Decimal CheckQuantity,
        Decimal CheckWeight 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "PickLocationId", DbType.Int32, PickLocationId);
        db.AddInParameter(dbCommand, "StoreLocationId", DbType.Int32, StoreLocationId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "InstructionRefId", DbType.Int32, InstructionRefId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "ConfirmedQuantity", DbType.Decimal, ConfirmedQuantity);
        db.AddInParameter(dbCommand, "Weight", DbType.Decimal, Weight);
        db.AddInParameter(dbCommand, "ConfirmedWeight", DbType.Decimal, ConfirmedWeight);
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "StartDate", DbType.DateTime, StartDate);
        db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);
        db.AddInParameter(dbCommand, "Picked", DbType.Boolean, Picked);
        db.AddInParameter(dbCommand, "Stored", DbType.Boolean, Stored);
        db.AddInParameter(dbCommand, "CheckQuantity", DbType.Decimal, CheckQuantity);
        db.AddInParameter(dbCommand, "CheckWeight", DbType.Decimal, CheckWeight);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInstruction
 
    #region SearchInstruction
    /// <summary>
    ///   Selects rows from the Instruction table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <param name="ReasonId"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="StatusId"></param>
    /// <param name="JobId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="PalletId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInstruction
    (
        string connectionStringName,
        Int32 InstructionId,
        Int32 ReasonId,
        Int32 InstructionTypeId,
        Int32 StorageUnitBatchId,
        Int32 WarehouseId,
        Int32 StatusId,
        Int32 JobId,
        Int32 OperatorId,
        Int32 ReceiptLineId,
        Int32 IssueLineId,
        Int32 PalletId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInstruction
 
    #region ListInstruction
    /// <summary>
    ///   Lists rows from the Instruction table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInstruction
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Instruction_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInstruction
 
    #region ParameterInstruction
    /// <summary>
    ///   Lists rows from the Instruction table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInstruction
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInstruction
}
