using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoMethod
///   Create By      : Grant Schultz
///   Date Created   : 04 Oct 2012 15:03:22
/// </summary>
/// <remarks>
///   Inserts a row into the Method table.
///   Selects rows from the Method table.
///   Searches for rows from the Method table.
///   Updates a rows in the Method table.
///   Deletes a row from the Method table.
/// </remarks>
/// <param>
public class StaticInfoMethod
{
    #region "Constructor Logic"
    public StaticInfoMethod()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetMethod
    /// <summary>
    ///   Selects rows from the Method table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MethodId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetMethod
    (
        string connectionStringName,
        Int32 MethodId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetMethod
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetMethod
 
    #region DeleteMethod
    /// <summary>
    ///   Deletes a row from the Method table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MethodId"></param>
    /// <returns>bool</returns>
    public bool DeleteMethod
    (
        string connectionStringName,
        Int32 MethodId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteMethod
 
    #region UpdateMethod
    /// <summary>
    ///   Updates a row in the Method table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MethodId"></param>
    /// <param name="MethodCode"></param>
    /// <param name="Method"></param>
    /// <returns>bool</returns>
    public bool UpdateMethod
    (
        string connectionStringName,
        Int32 MethodId,
        String MethodCode,
        String Method 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
        db.AddInParameter(dbCommand, "MethodCode", DbType.String, MethodCode);
        db.AddInParameter(dbCommand, "Method", DbType.String, Method);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateMethod
 
    #region InsertMethod
    /// <summary>
    ///   Inserts a row into the Method table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MethodId"></param>
    /// <param name="MethodCode"></param>
    /// <param name="Method"></param>
    /// <returns>bool</returns>
    public bool InsertMethod
    (
        string connectionStringName,
        Int32 MethodId,
        String MethodCode,
        String Method 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
        db.AddInParameter(dbCommand, "MethodCode", DbType.String, MethodCode);
        db.AddInParameter(dbCommand, "Method", DbType.String, Method);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertMethod
 
    #region SearchMethod
    /// <summary>
    ///   Selects rows from the Method table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MethodId"></param>
    /// <param name="MethodCode"></param>
    /// <param name="Method"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchMethod
    (
        string connectionStringName,
        Int32 MethodId,
        String MethodCode,
        String Method 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
        db.AddInParameter(dbCommand, "MethodCode", DbType.String, MethodCode);
        db.AddInParameter(dbCommand, "Method", DbType.String, Method);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchMethod
 
    #region PageSearchMethod
    /// <summary>
    ///   Selects rows from the Method table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MethodId"></param>
    /// <param name="MethodCode"></param>
    /// <param name="Method"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchMethod
    (
        string connectionStringName,
        Int32 MethodId,
        String MethodCode,
        String Method,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MethodId", DbType.Int32, MethodId);
        db.AddInParameter(dbCommand, "MethodCode", DbType.String, MethodCode);
        db.AddInParameter(dbCommand, "Method", DbType.String, Method);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchMethod
 
    #region ListMethod
    /// <summary>
    ///   Lists rows from the Method table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListMethod
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListMethod
 
    #region ParameterMethod
    /// <summary>
    ///   Lists rows from the Method table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterMethod
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Method_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterMethod
}
