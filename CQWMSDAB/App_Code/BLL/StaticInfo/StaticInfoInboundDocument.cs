using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInboundDocument
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:03
/// </summary>
/// <remarks>
///   Inserts a row into the InboundDocument table.
///   Selects rows from the InboundDocument table.
///   Searches for rows from the InboundDocument table.
///   Updates a rows in the InboundDocument table.
///   Deletes a row from the InboundDocument table.
/// </remarks>
/// <param>
public class StaticInfoInboundDocument
{
    #region "Constructor Logic"
    public StaticInfoInboundDocument()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInboundDocument
    /// <summary>
    ///   Selects rows from the InboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInboundDocument
    (
        string connectionStringName,
        Int32 InboundDocumentId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocument_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInboundDocument
 
    #region DeleteInboundDocument
    /// <summary>
    ///   Deletes a row from the InboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentId"></param>
    /// <returns>bool</returns>
    public bool DeleteInboundDocument
    (
        string connectionStringName,
        Int32 InboundDocumentId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocument_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInboundDocument
 
    #region UpdateInboundDocument
    /// <summary>
    ///   Updates a row in the InboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <param name="CreateDate"></param>
    /// <param name="ModifiedDate"></param>
    /// <returns>bool</returns>
    public bool UpdateInboundDocument
    (
        string connectionStringName,
        Int32 InboundDocumentId,
        Int32 InboundDocumentTypeId,
        Int32 ExternalCompanyId,
        Int32 StatusId,
        Int32 WarehouseId,
        String OrderNumber,
        DateTime DeliveryDate,
        DateTime CreateDate,
        DateTime ModifiedDate 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocument_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "ModifiedDate", DbType.DateTime, ModifiedDate);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInboundDocument
 
    #region InsertInboundDocument
    /// <summary>
    ///   Inserts a row into the InboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <param name="CreateDate"></param>
    /// <param name="ModifiedDate"></param>
    /// <returns>bool</returns>
    public bool InsertInboundDocument
    (
        string connectionStringName,
        Int32 InboundDocumentId,
        Int32 InboundDocumentTypeId,
        Int32 ExternalCompanyId,
        Int32 StatusId,
        Int32 WarehouseId,
        String OrderNumber,
        DateTime DeliveryDate,
        DateTime CreateDate,
        DateTime ModifiedDate 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocument_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "ModifiedDate", DbType.DateTime, ModifiedDate);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInboundDocument
 
    #region SearchInboundDocument
    /// <summary>
    ///   Selects rows from the InboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInboundDocument
    (
        string connectionStringName,
        Int32 InboundDocumentId,
        Int32 InboundDocumentTypeId,
        Int32 ExternalCompanyId,
        Int32 StatusId,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocument_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInboundDocument
 
    #region ListInboundDocument
    /// <summary>
    ///   Lists rows from the InboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInboundDocument
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocument_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInboundDocument
 
    #region ParameterInboundDocument
    /// <summary>
    ///   Lists rows from the InboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInboundDocument
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocument_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInboundDocument
}
