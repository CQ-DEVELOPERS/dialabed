using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoQuestionAnswers
///   Create By      : Grant Schultz
///   Date Created   : 14 May 2014 20:30:24
/// </summary>
/// <remarks>
///   Inserts a row into the QuestionAnswers table.
///   Selects rows from the QuestionAnswers table.
///   Searches for rows from the QuestionAnswers table.
///   Updates a rows in the QuestionAnswers table.
///   Deletes a row from the QuestionAnswers table.
/// </remarks>
/// <param>
public class StaticInfoQuestionAnswers
{
    #region "Constructor Logic"
    public StaticInfoQuestionAnswers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetQuestionAnswers
    /// <summary>
    ///   Selects rows from the QuestionAnswers table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionAnswersId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetQuestionAnswers
    (
        string connectionStringName,
        Int32 QuestionAnswersId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionAnswersId", DbType.Int32, QuestionAnswersId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetQuestionAnswers
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetQuestionAnswers
 
    #region DeleteQuestionAnswers
    /// <summary>
    ///   Deletes a row from the QuestionAnswers table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionAnswersId"></param>
    /// <returns>bool</returns>
    public bool DeleteQuestionAnswers
    (
        string connectionStringName,
        Int32 QuestionAnswersId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionAnswersId", DbType.Int32, QuestionAnswersId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteQuestionAnswers
 
    #region UpdateQuestionAnswers
    /// <summary>
    ///   Updates a row in the QuestionAnswers table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionId"></param>
    /// <param name="Answer"></param>
    /// <param name="JobId"></param>
    /// <param name="ReceiptId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="PalletId"></param>
    /// <param name="DateAsked"></param>
    /// <param name="QuestionAnswersId"></param>
    /// <returns>bool</returns>
    public bool UpdateQuestionAnswers
    (
        string connectionStringName,
        Int32 QuestionId,
        String Answer,
        Int32 JobId,
        Int32 ReceiptId,
        Int32 OrderNumber,
        Int32 PalletId,
        DateTime DateAsked,
        Int32 QuestionAnswersId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
        db.AddInParameter(dbCommand, "Answer", DbType.String, Answer);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.Int32, OrderNumber);
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
        db.AddInParameter(dbCommand, "DateAsked", DbType.DateTime, DateAsked);
        db.AddInParameter(dbCommand, "QuestionAnswersId", DbType.Int32, QuestionAnswersId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateQuestionAnswers
 
    #region InsertQuestionAnswers
    /// <summary>
    ///   Inserts a row into the QuestionAnswers table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionId"></param>
    /// <param name="Answer"></param>
    /// <param name="JobId"></param>
    /// <param name="ReceiptId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="PalletId"></param>
    /// <param name="DateAsked"></param>
    /// <param name="QuestionAnswersId"></param>
    /// <returns>bool</returns>
    public bool InsertQuestionAnswers
    (
        string connectionStringName,
        Int32 QuestionId,
        String Answer,
        Int32 JobId,
        Int32 ReceiptId,
        Int32 OrderNumber,
        Int32 PalletId,
        DateTime DateAsked,
        Int32 QuestionAnswersId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
        db.AddInParameter(dbCommand, "Answer", DbType.String, Answer);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.Int32, OrderNumber);
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
        db.AddInParameter(dbCommand, "DateAsked", DbType.DateTime, DateAsked);
        db.AddInParameter(dbCommand, "QuestionAnswersId", DbType.Int32, QuestionAnswersId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertQuestionAnswers
 
    #region SearchQuestionAnswers
    /// <summary>
    ///   Selects rows from the QuestionAnswers table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionAnswersId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchQuestionAnswers
    (
        string connectionStringName,
        Int32 QuestionAnswersId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionAnswersId", DbType.Int32, QuestionAnswersId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchQuestionAnswers
 
    #region PageSearchQuestionAnswers
    /// <summary>
    ///   Selects rows from the QuestionAnswers table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionAnswersId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchQuestionAnswers
    (
        string connectionStringName,
        Int32 QuestionAnswersId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionAnswersId", DbType.Int32, QuestionAnswersId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchQuestionAnswers
 
    #region ListQuestionAnswers
    /// <summary>
    ///   Lists rows from the QuestionAnswers table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListQuestionAnswers
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListQuestionAnswers
 
    #region ParameterQuestionAnswers
    /// <summary>
    ///   Lists rows from the QuestionAnswers table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterQuestionAnswers
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_QuestionAnswers_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterQuestionAnswers
}
