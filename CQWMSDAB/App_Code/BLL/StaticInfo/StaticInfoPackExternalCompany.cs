using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoPackExternalCompany
///   Create By      : Grant Schultz
///   Date Created   : 14 May 2014 16:58:10
/// </summary>
/// <remarks>
///   Inserts a row into the PackExternalCompany table.
///   Selects rows from the PackExternalCompany table.
///   Searches for rows from the PackExternalCompany table.
///   Updates a rows in the PackExternalCompany table.
///   Deletes a row from the PackExternalCompany table.
/// </remarks>
/// <param>
public class StaticInfoPackExternalCompany
{
    #region "Constructor Logic"
    public StaticInfoPackExternalCompany()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetPackExternalCompany
    /// <summary>
    ///   Selects rows from the PackExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetPackExternalCompany
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 ExternalCompanyId,
        Int32 StorageUnitId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetPackExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetPackExternalCompany
 
    #region DeletePackExternalCompany
    /// <summary>
    ///   Deletes a row from the PackExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>bool</returns>
    public bool DeletePackExternalCompany
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 ExternalCompanyId,
        Int32 StorageUnitId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeletePackExternalCompany
 
    #region UpdatePackExternalCompany
    /// <summary>
    ///   Updates a row in the PackExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="FromPackId"></param>
    /// <param name="FromQuantity"></param>
    /// <param name="ToPackId"></param>
    /// <param name="ToQuantity"></param>
    /// <param name="Comments"></param>
    /// <returns>bool</returns>
    public bool UpdatePackExternalCompany
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 ExternalCompanyId,
        Int32 StorageUnitId,
        Int32 FromPackId,
        Double FromQuantity,
        Int32 ToPackId,
        Double ToQuantity,
        String Comments 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "FromPackId", DbType.Int32, FromPackId);
        db.AddInParameter(dbCommand, "FromQuantity", DbType.Double, FromQuantity);
        db.AddInParameter(dbCommand, "ToPackId", DbType.Int32, ToPackId);
        db.AddInParameter(dbCommand, "ToQuantity", DbType.Double, ToQuantity);
        db.AddInParameter(dbCommand, "Comments", DbType.String, Comments);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdatePackExternalCompany
 
    #region InsertPackExternalCompany
    /// <summary>
    ///   Inserts a row into the PackExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="FromPackId"></param>
    /// <param name="FromQuantity"></param>
    /// <param name="ToPackId"></param>
    /// <param name="ToQuantity"></param>
    /// <param name="Comments"></param>
    /// <returns>bool</returns>
    public bool InsertPackExternalCompany
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 ExternalCompanyId,
        Int32 StorageUnitId,
        Int32 FromPackId,
        Double FromQuantity,
        Int32 ToPackId,
        Double ToQuantity,
        String Comments 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "FromPackId", DbType.Int32, FromPackId);
        db.AddInParameter(dbCommand, "FromQuantity", DbType.Double, FromQuantity);
        db.AddInParameter(dbCommand, "ToPackId", DbType.Int32, ToPackId);
        db.AddInParameter(dbCommand, "ToQuantity", DbType.Double, ToQuantity);
        db.AddInParameter(dbCommand, "Comments", DbType.String, Comments);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertPackExternalCompany
 
    #region SearchPackExternalCompany
    /// <summary>
    ///   Selects rows from the PackExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="FromPackId"></param>
    /// <param name="ToPackId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchPackExternalCompany
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 ExternalCompanyId,
        Int32 StorageUnitId,
        Int32 FromPackId,
        Int32 ToPackId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "FromPackId", DbType.Int32, FromPackId);
        db.AddInParameter(dbCommand, "ToPackId", DbType.Int32, ToPackId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchPackExternalCompany
 
    #region PageSearchPackExternalCompany
    /// <summary>
    ///   Selects rows from the PackExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="FromPackId"></param>
    /// <param name="ToPackId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchPackExternalCompany
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 ExternalCompanyId,
        Int32 StorageUnitId,
        Int32 FromPackId,
        Int32 ToPackId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "FromPackId", DbType.Int32, FromPackId);
        db.AddInParameter(dbCommand, "ToPackId", DbType.Int32, ToPackId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchPackExternalCompany
 
    #region ListPackExternalCompany
    /// <summary>
    ///   Lists rows from the PackExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListPackExternalCompany
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ListPackExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListPackExternalCompany
 
    #region ParameterPackExternalCompany
    /// <summary>
    ///   Lists rows from the PackExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterPackExternalCompany
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ParameterPackExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackExternalCompany_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterPackExternalCompany
}
