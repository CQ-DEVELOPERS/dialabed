using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoResult
///   Create By      : Grant Schultz
///   Date Created   : 04 Oct 2012 15:03:28
/// </summary>
/// <remarks>
///   Inserts a row into the Result table.
///   Selects rows from the Result table.
///   Searches for rows from the Result table.
///   Updates a rows in the Result table.
///   Deletes a row from the Result table.
/// </remarks>
/// <param>
public class StaticInfoResult
{
    #region "Constructor Logic"
    public StaticInfoResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetResult
    /// <summary>
    ///   Selects rows from the Result table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ResultId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetResult
    (
        string connectionStringName,
        Int32 ResultId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetResult
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetResult
 
    #region DeleteResult
    /// <summary>
    ///   Deletes a row from the Result table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ResultId"></param>
    /// <returns>bool</returns>
    public bool DeleteResult
    (
        string connectionStringName,
        Int32 ResultId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteResult
 
    #region UpdateResult
    /// <summary>
    ///   Updates a row in the Result table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ResultId"></param>
    /// <param name="ResultCode"></param>
    /// <param name="Result"></param>
    /// <param name="Pass"></param>
    /// <param name="StartRange"></param>
    /// <param name="EndRange"></param>
    /// <returns>bool</returns>
    public bool UpdateResult
    (
        string connectionStringName,
        Int32 ResultId,
        String ResultCode,
        String Result,
        Boolean Pass,
        Double StartRange,
        Double EndRange 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
        db.AddInParameter(dbCommand, "ResultCode", DbType.String, ResultCode);
        db.AddInParameter(dbCommand, "Result", DbType.String, Result);
        db.AddInParameter(dbCommand, "Pass", DbType.Boolean, Pass);
        db.AddInParameter(dbCommand, "StartRange", DbType.Double, StartRange);
        db.AddInParameter(dbCommand, "EndRange", DbType.Double, EndRange);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateResult
 
    #region InsertResult
    /// <summary>
    ///   Inserts a row into the Result table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ResultId"></param>
    /// <param name="ResultCode"></param>
    /// <param name="Result"></param>
    /// <param name="Pass"></param>
    /// <param name="StartRange"></param>
    /// <param name="EndRange"></param>
    /// <returns>bool</returns>
    public bool InsertResult
    (
        string connectionStringName,
        Int32 ResultId,
        String ResultCode,
        String Result,
        Boolean Pass,
        Double StartRange,
        Double EndRange 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
        db.AddInParameter(dbCommand, "ResultCode", DbType.String, ResultCode);
        db.AddInParameter(dbCommand, "Result", DbType.String, Result);
        db.AddInParameter(dbCommand, "Pass", DbType.Boolean, Pass);
        db.AddInParameter(dbCommand, "StartRange", DbType.Double, StartRange);
        db.AddInParameter(dbCommand, "EndRange", DbType.Double, EndRange);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertResult
 
    #region SearchResult
    /// <summary>
    ///   Selects rows from the Result table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ResultId"></param>
    /// <param name="ResultCode"></param>
    /// <param name="Result"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchResult
    (
        string connectionStringName,
        Int32 ResultId,
        String ResultCode,
        String Result 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
        db.AddInParameter(dbCommand, "ResultCode", DbType.String, ResultCode);
        db.AddInParameter(dbCommand, "Result", DbType.String, Result);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchResult
 
    #region PageSearchResult
    /// <summary>
    ///   Selects rows from the Result table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ResultId"></param>
    /// <param name="ResultCode"></param>
    /// <param name="Result"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchResult
    (
        string connectionStringName,
        Int32 ResultId,
        String ResultCode,
        String Result,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ResultId", DbType.Int32, ResultId);
        db.AddInParameter(dbCommand, "ResultCode", DbType.String, ResultCode);
        db.AddInParameter(dbCommand, "Result", DbType.String, Result);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchResult
 
    #region ListResult
    /// <summary>
    ///   Lists rows from the Result table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListResult
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListResult
 
    #region ParameterResult
    /// <summary>
    ///   Lists rows from the Result table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterResult
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Result_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterResult
}
