using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoReportType
///   Create By      : Grant Schultz
///   Date Created   : 30 Apr 2014 11:19:43
/// </summary>
/// <remarks>
///   Inserts a row into the ReportType table.
///   Selects rows from the ReportType table.
///   Searches for rows from the ReportType table.
///   Updates a rows in the ReportType table.
///   Deletes a row from the ReportType table.
/// </remarks>
/// <param>
public class StaticInfoReportType
{
    #region "Constructor Logic"
    public StaticInfoReportType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetReportType
    /// <summary>
    ///   Selects rows from the ReportType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReportTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetReportType
    (
        string connectionStringName,
        Int32 ReportTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReportTypeId", DbType.Int32, ReportTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetReportType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetReportType
 
    #region DeleteReportType
    /// <summary>
    ///   Deletes a row from the ReportType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReportTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteReportType
    (
        string connectionStringName,
        Int32 ReportTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReportTypeId", DbType.Int32, ReportTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteReportType
 
    #region UpdateReportType
    /// <summary>
    ///   Updates a row in the ReportType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReportTypeId"></param>
    /// <param name="ReportType"></param>
    /// <param name="ReportTypeCode"></param>
    /// <returns>bool</returns>
    public bool UpdateReportType
    (
        string connectionStringName,
        Int32 ReportTypeId,
        String ReportType,
        String ReportTypeCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReportTypeId", DbType.Int32, ReportTypeId);
        db.AddInParameter(dbCommand, "ReportType", DbType.String, ReportType);
        db.AddInParameter(dbCommand, "ReportTypeCode", DbType.String, ReportTypeCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateReportType
 
    #region InsertReportType
    /// <summary>
    ///   Inserts a row into the ReportType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReportTypeId"></param>
    /// <param name="ReportType"></param>
    /// <param name="ReportTypeCode"></param>
    /// <returns>bool</returns>
    public bool InsertReportType
    (
        string connectionStringName,
        Int32 ReportTypeId,
        String ReportType,
        String ReportTypeCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReportTypeId", DbType.Int32, ReportTypeId);
        db.AddInParameter(dbCommand, "ReportType", DbType.String, ReportType);
        db.AddInParameter(dbCommand, "ReportTypeCode", DbType.String, ReportTypeCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertReportType
 
    #region SearchReportType
    /// <summary>
    ///   Selects rows from the ReportType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReportTypeId"></param>
    /// <param name="ReportType"></param>
    /// <param name="ReportTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchReportType
    (
        string connectionStringName,
        Int32 ReportTypeId,
        String ReportType,
        String ReportTypeCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReportTypeId", DbType.Int32, ReportTypeId);
        db.AddInParameter(dbCommand, "ReportType", DbType.String, ReportType);
        db.AddInParameter(dbCommand, "ReportTypeCode", DbType.String, ReportTypeCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchReportType
 
    #region PageSearchReportType
    /// <summary>
    ///   Selects rows from the ReportType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReportTypeId"></param>
    /// <param name="ReportType"></param>
    /// <param name="ReportTypeCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchReportType
    (
        string connectionStringName,
        Int32 ReportTypeId,
        String ReportType,
        String ReportTypeCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReportTypeId", DbType.Int32, ReportTypeId);
        db.AddInParameter(dbCommand, "ReportType", DbType.String, ReportType);
        db.AddInParameter(dbCommand, "ReportTypeCode", DbType.String, ReportTypeCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchReportType
 
    #region ListReportType
    /// <summary>
    ///   Lists rows from the ReportType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListReportType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListReportType
 
    #region ParameterReportType
    /// <summary>
    ///   Lists rows from the ReportType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterReportType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReportType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterReportType
}
