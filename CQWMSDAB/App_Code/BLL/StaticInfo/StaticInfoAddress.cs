using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoAddress
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:52:57
/// </summary>
/// <remarks>
///   Inserts a row into the Address table.
///   Selects rows from the Address table.
///   Searches for rows from the Address table.
///   Updates a rows in the Address table.
///   Deletes a row from the Address table.
/// </remarks>
/// <param>
public class StaticInfoAddress
{
    #region "Constructor Logic"
    public StaticInfoAddress()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetAddress
    /// <summary>
    ///   Selects rows from the Address table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AddressId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetAddress
    (
        string connectionStringName,
        Int32 AddressId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetAddress
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetAddress
 
    #region DeleteAddress
    /// <summary>
    ///   Deletes a row from the Address table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AddressId"></param>
    /// <returns>bool</returns>
    public bool DeleteAddress
    (
        string connectionStringName,
        Int32 AddressId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteAddress
 
    #region UpdateAddress
    /// <summary>
    ///   Updates a row in the Address table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AddressId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="Street"></param>
    /// <param name="Suburb"></param>
    /// <param name="Town"></param>
    /// <param name="Country"></param>
    /// <param name="Code"></param>
    /// <returns>bool</returns>
    public bool UpdateAddress
    (
        string connectionStringName,
        Int32 AddressId,
        Int32 ExternalCompanyId,
        String Street,
        String Suburb,
        String Town,
        String Country,
        String Code 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "Street", DbType.String, Street);
        db.AddInParameter(dbCommand, "Suburb", DbType.String, Suburb);
        db.AddInParameter(dbCommand, "Town", DbType.String, Town);
        db.AddInParameter(dbCommand, "Country", DbType.String, Country);
        db.AddInParameter(dbCommand, "Code", DbType.String, Code);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateAddress
 
    #region InsertAddress
    /// <summary>
    ///   Inserts a row into the Address table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AddressId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="Street"></param>
    /// <param name="Suburb"></param>
    /// <param name="Town"></param>
    /// <param name="Country"></param>
    /// <param name="Code"></param>
    /// <returns>bool</returns>
    public bool InsertAddress
    (
        string connectionStringName,
        Int32 AddressId,
        Int32 ExternalCompanyId,
        String Street,
        String Suburb,
        String Town,
        String Country,
        String Code 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "Street", DbType.String, Street);
        db.AddInParameter(dbCommand, "Suburb", DbType.String, Suburb);
        db.AddInParameter(dbCommand, "Town", DbType.String, Town);
        db.AddInParameter(dbCommand, "Country", DbType.String, Country);
        db.AddInParameter(dbCommand, "Code", DbType.String, Code);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertAddress
 
    #region SearchAddress
    /// <summary>
    ///   Selects rows from the Address table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AddressId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchAddress
    (
        string connectionStringName,
        Int32 AddressId,
        Int32 ExternalCompanyId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchAddress
 
    #region PageSearchAddress
    /// <summary>
    ///   Selects rows from the Address table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AddressId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchAddress
    (
        string connectionStringName,
        Int32 AddressId,
        Int32 ExternalCompanyId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchAddress
 
    #region ListAddress
    /// <summary>
    ///   Lists rows from the Address table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListAddress
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListAddress
 
    #region ParameterAddress
    /// <summary>
    ///   Lists rows from the Address table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterAddress
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Address_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterAddress
}
