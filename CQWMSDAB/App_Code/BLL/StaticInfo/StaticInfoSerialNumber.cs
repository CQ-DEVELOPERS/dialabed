using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoSerialNumber
///   Create By      : Grant Schultz
///   Date Created   : 29 Jul 2013 14:33:11
/// </summary>
/// <remarks>
///   Inserts a row into the SerialNumber table.
///   Selects rows from the SerialNumber table.
///   Searches for rows from the SerialNumber table.
///   Updates a rows in the SerialNumber table.
///   Deletes a row from the SerialNumber table.
/// </remarks>
/// <param>
public class StaticInfoSerialNumber
{
    #region "Constructor Logic"
    public StaticInfoSerialNumber()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
}
