using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoPriceList
///   Create By      : Grant Schultz
///   Date Created   : 30 Sep 2013 15:01:39
/// </summary>
/// <remarks>
///   Inserts a row into the PriceList table.
///   Selects rows from the PriceList table.
///   Searches for rows from the PriceList table.
///   Updates a rows in the PriceList table.
///   Deletes a row from the PriceList table.
/// </remarks>
/// <param>
public class StaticInfoPriceList
{
    #region "Constructor Logic"
    public StaticInfoPriceList()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetPriceList
    /// <summary>
    ///   Selects rows from the PriceList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriceListId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetPriceList
    (
        string connectionStringName,
        Int32 PriceListId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriceListId", DbType.Int32, PriceListId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetPriceList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetPriceList
 
    #region DeletePriceList
    /// <summary>
    ///   Deletes a row from the PriceList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriceListId"></param>
    /// <returns>bool</returns>
    public bool DeletePriceList
    (
        string connectionStringName,
        Int32 PriceListId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriceListId", DbType.Int32, PriceListId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeletePriceList
 
    #region UpdatePriceList
    /// <summary>
    ///   Updates a row in the PriceList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriceListId"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="RRP"></param>
    /// <returns>bool</returns>
    public bool UpdatePriceList
    (
        string connectionStringName,
        Int32 PriceListId,
        Int32 PricingCategoryId,
        Int32 StorageUnitId,
        Decimal RRP 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriceListId", DbType.Int32, PriceListId);
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "RRP", DbType.Decimal, RRP);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdatePriceList
 
    #region InsertPriceList
    /// <summary>
    ///   Inserts a row into the PriceList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriceListId"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="RRP"></param>
    /// <returns>bool</returns>
    public bool InsertPriceList
    (
        string connectionStringName,
        Int32 PriceListId,
        Int32 PricingCategoryId,
        Int32 StorageUnitId,
        Decimal RRP 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriceListId", DbType.Int32, PriceListId);
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "RRP", DbType.Decimal, RRP);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertPriceList
 
    #region SearchPriceList
    /// <summary>
    ///   Selects rows from the PriceList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriceListId"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchPriceList
    (
        string connectionStringName,
        Int32 PriceListId,
        Int32 PricingCategoryId,
        Int32 StorageUnitId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriceListId", DbType.Int32, PriceListId);
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchPriceList
 
    #region PageSearchPriceList
    /// <summary>
    ///   Selects rows from the PriceList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriceListId"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchPriceList
    (
        string connectionStringName,
        Int32 PriceListId,
        Int32 PricingCategoryId,
        Int32 StorageUnitId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriceListId", DbType.Int32, PriceListId);
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchPriceList
 
    #region ListPriceList
    /// <summary>
    ///   Lists rows from the PriceList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListPriceList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListPriceList
 
    #region ParameterPriceList
    /// <summary>
    ///   Lists rows from the PriceList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterPriceList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PriceList_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterPriceList
}
