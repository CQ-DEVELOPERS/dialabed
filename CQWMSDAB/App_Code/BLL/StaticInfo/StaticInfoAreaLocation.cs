using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoAreaLocation
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:14:52
/// </summary>
/// <remarks>
///   Inserts a row into the AreaLocation table.
///   Selects rows from the AreaLocation table.
///   Searches for rows from the AreaLocation table.
///   Updates a rows in the AreaLocation table.
///   Deletes a row from the AreaLocation table.
/// </remarks>
/// <param>
public class StaticInfoAreaLocation
{
    #region "Constructor Logic"
    public StaticInfoAreaLocation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetAreaLocation
    /// <summary>
    ///   Selects rows from the AreaLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetAreaLocation
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetAreaLocation
 
    #region DeleteAreaLocation
    /// <summary>
    ///   Deletes a row from the AreaLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="LocationId"></param>
    /// <returns>bool</returns>
    public bool DeleteAreaLocation
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 LocationId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteAreaLocation
 
    #region UpdateAreaLocation
    /// <summary>
    ///   Updates a row in the AreaLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="LocationId"></param>
    /// <returns>bool</returns>
    public bool UpdateAreaLocation
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 LocationId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateAreaLocation
 
    #region InsertAreaLocation
    /// <summary>
    ///   Inserts a row into the AreaLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="LocationId"></param>
    /// <returns>bool</returns>
    public bool InsertAreaLocation
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 LocationId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertAreaLocation
 
    #region SearchAreaLocation
    /// <summary>
    ///   Selects rows from the AreaLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchAreaLocation
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchAreaLocation
 
    #region ListAreaLocation
    /// <summary>
    ///   Lists rows from the AreaLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListAreaLocation
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaLocation_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListAreaLocation
 
    #region ParameterAreaLocation
    /// <summary>
    ///   Lists rows from the AreaLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterAreaLocation
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterAreaLocation
}
