using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

/// <summary>
///   Summary description for StaticInfoRoute
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:21
/// </summary>
/// <remarks>
///   Inserts a row into the Route table.
///   Selects rows from the Route table.
///   Searches for rows from the Route table.
///   Updates a rows in the Route table.
///   Deletes a row from the Route table.
/// </remarks>
/// <param>
public class StaticInfoRoute
{
    #region "Constructor Logic"
    public StaticInfoRoute()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region GetRoute
    /// <summary>
    ///   Selects rows from the Route table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RouteId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetRoute
    (
        string connectionStringName,
        Int32 RouteId
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetRoute
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetRoute

    #region DeleteRoute
    /// <summary>
    ///   Deletes a row from the Route table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RouteId"></param>
    /// <returns>bool</returns>
    public bool DeleteRoute
    (
        string connectionStringName,
        Int32 RouteId
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeleteRoute

    #region UpdateRoute
    /// <summary>
    ///   Updates a row in the Route table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RouteId"></param>
    /// <param name="Route"></param>
    /// <param name="RouteCode"></param>
    /// <returns>bool</returns>
    public bool UpdateRoute
    (
        string connectionStringName,
        Int32 RouteId,
        String Route,
        String RouteCode,
        int CheckingLaneId
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "CheckingLaneId", DbType.Int32, CheckingLaneId);
        db.AddInParameter(dbCommand, "Route", DbType.String, Route);
        db.AddInParameter(dbCommand, "RouteCode", DbType.String, RouteCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateRoute

    #region InsertRoute
    /// <summary>
    ///   Inserts a row into the Route table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RouteId"></param>
    /// <param name="Route"></param>
    /// <param name="RouteCode"></param>
    /// <returns>bool</returns>
    public bool InsertRoute
    (
        string connectionStringName,
        Int32 RouteId,
        String Route,
        String RouteCode,
        int CheckingLaneId
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "Route", DbType.String, Route);
        db.AddInParameter(dbCommand, "RouteCode", DbType.String, RouteCode);
        db.AddInParameter(dbCommand, "CheckingLaneId", DbType.Int32, CheckingLaneId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InsertRoute

    #region SearchRoute
    /// <summary>
    ///   Selects rows from the Route table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RouteId"></param>
    /// <param name="Route"></param>
    /// <param name="RouteCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchRoute
    (
        string connectionStringName,
        Int32 RouteId,
        String Route,
        String RouteCode
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "Route", DbType.String, Route);
        db.AddInParameter(dbCommand, "RouteCode", DbType.String, RouteCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchRoute

    #region PageSearchRoute
    /// <summary>
    ///   Selects rows from the Route table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RouteId"></param>
    /// <param name="Route"></param>
    /// <param name="RouteCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchRoute
    (
        string connectionStringName,
        Int32 RouteId,
        String Route,
        String RouteCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "Route", DbType.String, Route);
        db.AddInParameter(dbCommand, "RouteCode", DbType.String, RouteCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion PageSearchRoute

    #region ListRoute
    /// <summary>
    ///   Lists rows from the Route table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListRoute
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListRoute

    #region ParameterRoute
    /// <summary>
    ///   Lists rows from the Route table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterRoute
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Route_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ParameterRoute

    #region GetCheckingLocation
    public DataSet GetCheckingLocation
   (
       string connectionStringName
   )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CheckingLocation_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion
}
