using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoUOM
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:27
/// </summary>
/// <remarks>
///   Inserts a row into the UOM table.
///   Selects rows from the UOM table.
///   Searches for rows from the UOM table.
///   Updates a rows in the UOM table.
///   Deletes a row from the UOM table.
/// </remarks>
/// <param>
public class StaticInfoUOM
{
    #region "Constructor Logic"
    public StaticInfoUOM()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetUOM
    /// <summary>
    ///   Selects rows from the UOM table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="UOMId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetUOM
    (
        string connectionStringName,
        Int32 UOMId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetUOM
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetUOM
 
    #region DeleteUOM
    /// <summary>
    ///   Deletes a row from the UOM table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="UOMId"></param>
    /// <returns>bool</returns>
    public bool DeleteUOM
    (
        string connectionStringName,
        Int32 UOMId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteUOM
 
    #region UpdateUOM
    /// <summary>
    ///   Updates a row in the UOM table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="UOMId"></param>
    /// <param name="UOM"></param>
    /// <param name="UOMCode"></param>
    /// <returns>bool</returns>
    public bool UpdateUOM
    (
        string connectionStringName,
        Int32 UOMId,
        String UOM,
        String UOMCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
        db.AddInParameter(dbCommand, "UOM", DbType.String, UOM);
        db.AddInParameter(dbCommand, "UOMCode", DbType.String, UOMCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateUOM
 
    #region InsertUOM
    /// <summary>
    ///   Inserts a row into the UOM table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="UOMId"></param>
    /// <param name="UOM"></param>
    /// <param name="UOMCode"></param>
    /// <returns>bool</returns>
    public bool InsertUOM
    (
        string connectionStringName,
        Int32 UOMId,
        String UOM,
        String UOMCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
        db.AddInParameter(dbCommand, "UOM", DbType.String, UOM);
        db.AddInParameter(dbCommand, "UOMCode", DbType.String, UOMCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertUOM
 
    #region SearchUOM
    /// <summary>
    ///   Selects rows from the UOM table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="UOMId"></param>
    /// <param name="UOM"></param>
    /// <param name="UOMCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchUOM
    (
        string connectionStringName,
        Int32 UOMId,
        String UOM,
        String UOMCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
        db.AddInParameter(dbCommand, "UOM", DbType.String, UOM);
        db.AddInParameter(dbCommand, "UOMCode", DbType.String, UOMCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchUOM
 
    #region PageSearchUOM
    /// <summary>
    ///   Selects rows from the UOM table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="UOMId"></param>
    /// <param name="UOM"></param>
    /// <param name="UOMCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchUOM
    (
        string connectionStringName,
        Int32 UOMId,
        String UOM,
        String UOMCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "UOMId", DbType.Int32, UOMId);
        db.AddInParameter(dbCommand, "UOM", DbType.String, UOM);
        db.AddInParameter(dbCommand, "UOMCode", DbType.String, UOMCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchUOM
 
    #region ListUOM
    /// <summary>
    ///   Lists rows from the UOM table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListUOM
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListUOM
 
    #region ParameterUOM
    /// <summary>
    ///   Lists rows from the UOM table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterUOM
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_UOM_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterUOM
}
