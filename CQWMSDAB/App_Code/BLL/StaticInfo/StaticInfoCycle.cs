using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoCycle
///   Create By      : Grant Schultz
///   Date Created   : 03 Mar 2014 12:45:51
/// </summary>
/// <remarks>
///   Inserts a row into the Cycle table.
///   Selects rows from the Cycle table.
///   Searches for rows from the Cycle table.
///   Updates a rows in the Cycle table.
///   Deletes a row from the Cycle table.
/// </remarks>
/// <param>
public class StaticInfoCycle
{
    #region "Constructor Logic"
    public StaticInfoCycle()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetCycle
    /// <summary>
    ///   Selects rows from the Cycle table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CycleId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetCycle
    (
        string connectionStringName,
        Int32 CycleId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetCycle
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetCycle
 
    #region DeleteCycle
    /// <summary>
    ///   Deletes a row from the Cycle table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CycleId"></param>
    /// <returns>bool</returns>
    public bool DeleteCycle
    (
        string connectionStringName,
        Int32 CycleId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteCycle
 
    #region UpdateCycle
    /// <summary>
    ///   Updates a row in the Cycle table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CycleId"></param>
    /// <param name="CycleCode"></param>
    /// <param name="Cycle"></param>
    /// <returns>bool</returns>
    public bool UpdateCycle
    (
        string connectionStringName,
        Int32 CycleId,
        String CycleCode,
        String Cycle 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
        db.AddInParameter(dbCommand, "CycleCode", DbType.String, CycleCode);
        db.AddInParameter(dbCommand, "Cycle", DbType.String, Cycle);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateCycle
 
    #region InsertCycle
    /// <summary>
    ///   Inserts a row into the Cycle table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CycleId"></param>
    /// <param name="CycleCode"></param>
    /// <param name="Cycle"></param>
    /// <returns>bool</returns>
    public bool InsertCycle
    (
        string connectionStringName,
        Int32 CycleId,
        String CycleCode,
        String Cycle 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
        db.AddInParameter(dbCommand, "CycleCode", DbType.String, CycleCode);
        db.AddInParameter(dbCommand, "Cycle", DbType.String, Cycle);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertCycle
 
    #region SearchCycle
    /// <summary>
    ///   Selects rows from the Cycle table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CycleId"></param>
    /// <param name="CycleCode"></param>
    /// <param name="Cycle"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchCycle
    (
        string connectionStringName,
        Int32 CycleId,
        String CycleCode,
        String Cycle 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
        db.AddInParameter(dbCommand, "CycleCode", DbType.String, CycleCode);
        db.AddInParameter(dbCommand, "Cycle", DbType.String, Cycle);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchCycle
 
    #region PageSearchCycle
    /// <summary>
    ///   Selects rows from the Cycle table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CycleId"></param>
    /// <param name="CycleCode"></param>
    /// <param name="Cycle"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchCycle
    (
        string connectionStringName,
        Int32 CycleId,
        String CycleCode,
        String Cycle,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
        db.AddInParameter(dbCommand, "CycleCode", DbType.String, CycleCode);
        db.AddInParameter(dbCommand, "Cycle", DbType.String, Cycle);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchCycle
 
    #region ListCycle
    /// <summary>
    ///   Lists rows from the Cycle table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListCycle
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListCycle
 
    #region ParameterCycle
    /// <summary>
    ///   Lists rows from the Cycle table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterCycle
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Cycle_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterCycle
}
