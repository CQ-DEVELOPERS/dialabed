using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoPackType
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:31
/// </summary>
/// <remarks>
///   Inserts a row into the PackType table.
///   Selects rows from the PackType table.
///   Searches for rows from the PackType table.
///   Updates a rows in the PackType table.
///   Deletes a row from the PackType table.
/// </remarks>
/// <param>
public class StaticInfoPackType
{
    #region "Constructor Logic"
    public StaticInfoPackType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetPackType
    /// <summary>
    ///   Selects rows from the PackType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetPackType
    (
        string connectionStringName,
        Int32 PackTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetPackType
 
    #region DeletePackType
    /// <summary>
    ///   Deletes a row from the PackType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackTypeId"></param>
    /// <returns>bool</returns>
    public bool DeletePackType
    (
        string connectionStringName,
        Int32 PackTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeletePackType
 
    #region UpdatePackType
    /// <summary>
    ///   Updates a row in the PackType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackTypeId"></param>
    /// <param name="PackType"></param>
    /// <param name="InboundSequence"></param>
    /// <param name="OutboundSequence"></param>
    /// <returns>bool</returns>
    public bool UpdatePackType
    (
        string connectionStringName,
        Int32 PackTypeId,
        String PackType,
        Int16 InboundSequence,
        Int16 OutboundSequence 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
        db.AddInParameter(dbCommand, "PackType", DbType.String, PackType);
        db.AddInParameter(dbCommand, "InboundSequence", DbType.Int16, InboundSequence);
        db.AddInParameter(dbCommand, "OutboundSequence", DbType.Int16, OutboundSequence);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdatePackType
 
    #region InsertPackType
    /// <summary>
    ///   Inserts a row into the PackType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackTypeId"></param>
    /// <param name="PackType"></param>
    /// <param name="InboundSequence"></param>
    /// <param name="OutboundSequence"></param>
    /// <returns>bool</returns>
    public bool InsertPackType
    (
        string connectionStringName,
        Int32 PackTypeId,
        String PackType,
        Int16 InboundSequence,
        Int16 OutboundSequence 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
        db.AddInParameter(dbCommand, "PackType", DbType.String, PackType);
        db.AddInParameter(dbCommand, "InboundSequence", DbType.Int16, InboundSequence);
        db.AddInParameter(dbCommand, "OutboundSequence", DbType.Int16, OutboundSequence);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertPackType
 
    #region SearchPackType
    /// <summary>
    ///   Selects rows from the PackType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackTypeId"></param>
    /// <param name="PackType"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchPackType
    (
        string connectionStringName,
        Int32 PackTypeId,
        String PackType 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
        db.AddInParameter(dbCommand, "PackType", DbType.String, PackType);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchPackType
 
    #region ListPackType
    /// <summary>
    ///   Lists rows from the PackType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListPackType
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListPackType
 
    #region ParameterPackType
    /// <summary>
    ///   Lists rows from the PackType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterPackType
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PackType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterPackType
}
