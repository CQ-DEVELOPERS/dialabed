using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoApplicationException
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:14:50
/// </summary>
/// <remarks>
///   Inserts a row into the ApplicationException table.
///   Selects rows from the ApplicationException table.
///   Searches for rows from the ApplicationException table.
///   Updates a rows in the ApplicationException table.
///   Deletes a row from the ApplicationException table.
/// </remarks>
/// <param>
public class StaticInfoApplicationException
{
    #region "Constructor Logic"
    public StaticInfoApplicationException()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetApplicationException
    /// <summary>
    ///   Selects rows from the ApplicationException table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ApplicationExceptionId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetApplicationException
    (
        string connectionStringName,
        Int32 ApplicationExceptionId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ApplicationException_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ApplicationExceptionId", DbType.Int32, ApplicationExceptionId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetApplicationException
 
    #region DeleteApplicationException
    /// <summary>
    ///   Deletes a row from the ApplicationException table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ApplicationExceptionId"></param>
    /// <returns>bool</returns>
    public bool DeleteApplicationException
    (
        string connectionStringName,
        Int32 ApplicationExceptionId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ApplicationException_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ApplicationExceptionId", DbType.Int32, ApplicationExceptionId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteApplicationException
 
    #region UpdateApplicationException
    /// <summary>
    ///   Updates a row in the ApplicationException table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ApplicationExceptionId"></param>
    /// <param name="PriorityLevel"></param>
    /// <param name="Module"></param>
    /// <param name="Page"></param>
    /// <param name="Method"></param>
    /// <param name="ErrorMsg"></param>
    /// <param name="CreateDate"></param>
    /// <returns>bool</returns>
    public bool UpdateApplicationException
    (
        string connectionStringName,
        Int32 ApplicationExceptionId,
        Int32 PriorityLevel,
        String Module,
        String Page,
        String Method,
        String ErrorMsg,
        DateTime CreateDate 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ApplicationException_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ApplicationExceptionId", DbType.Int32, ApplicationExceptionId);
        db.AddInParameter(dbCommand, "PriorityLevel", DbType.Int32, PriorityLevel);
        db.AddInParameter(dbCommand, "Module", DbType.String, Module);
        db.AddInParameter(dbCommand, "Page", DbType.String, Page);
        db.AddInParameter(dbCommand, "Method", DbType.String, Method);
        db.AddInParameter(dbCommand, "ErrorMsg", DbType.String, ErrorMsg);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateApplicationException
 
    #region InsertApplicationException
    /// <summary>
    ///   Inserts a row into the ApplicationException table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ApplicationExceptionId"></param>
    /// <param name="PriorityLevel"></param>
    /// <param name="Module"></param>
    /// <param name="Page"></param>
    /// <param name="Method"></param>
    /// <param name="ErrorMsg"></param>
    /// <param name="CreateDate"></param>
    /// <returns>bool</returns>
    public bool InsertApplicationException
    (
        string connectionStringName,
        Int32 ApplicationExceptionId,
        Int32 PriorityLevel,
        String Module,
        String Page,
        String Method,
        String ErrorMsg,
        DateTime CreateDate 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ApplicationException_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ApplicationExceptionId", DbType.Int32, ApplicationExceptionId);
        db.AddInParameter(dbCommand, "PriorityLevel", DbType.Int32, PriorityLevel);
        db.AddInParameter(dbCommand, "Module", DbType.String, Module);
        db.AddInParameter(dbCommand, "Page", DbType.String, Page);
        db.AddInParameter(dbCommand, "Method", DbType.String, Method);
        db.AddInParameter(dbCommand, "ErrorMsg", DbType.String, ErrorMsg);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertApplicationException
 
    #region SearchApplicationException
    /// <summary>
    ///   Selects rows from the ApplicationException table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ApplicationExceptionId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchApplicationException
    (
        string connectionStringName,
        Int32 ApplicationExceptionId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ApplicationException_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ApplicationExceptionId", DbType.Int32, ApplicationExceptionId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchApplicationException
 
    #region ListApplicationException
    /// <summary>
    ///   Lists rows from the ApplicationException table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListApplicationException
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ApplicationException_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListApplicationException
 
    #region ParameterApplicationException
    /// <summary>
    ///   Lists rows from the ApplicationException table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterApplicationException
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ApplicationException_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterApplicationException
}
