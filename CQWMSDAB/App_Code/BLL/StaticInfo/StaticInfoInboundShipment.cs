using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInboundShipment
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:06
/// </summary>
/// <remarks>
///   Inserts a row into the InboundShipment table.
///   Selects rows from the InboundShipment table.
///   Searches for rows from the InboundShipment table.
///   Updates a rows in the InboundShipment table.
///   Deletes a row from the InboundShipment table.
/// </remarks>
/// <param>
public class StaticInfoInboundShipment
{
    #region "Constructor Logic"
    public StaticInfoInboundShipment()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInboundShipment
    /// <summary>
    ///   Selects rows from the InboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInboundShipment
    (
        string connectionStringName,
        Int32 InboundShipmentId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInboundShipment
 
    #region DeleteInboundShipment
    /// <summary>
    ///   Deletes a row from the InboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <returns>bool</returns>
    public bool DeleteInboundShipment
    (
        string connectionStringName,
        Int32 InboundShipmentId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInboundShipment
 
    #region UpdateInboundShipment
    /// <summary>
    ///   Updates a row in the InboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ShipmentDate"></param>
    /// <param name="Remarks"></param>
    /// <returns>bool</returns>
    public bool UpdateInboundShipment
    (
        string connectionStringName,
        Int32 InboundShipmentId,
        Int32 StatusId,
        Int32 WarehouseId,
        DateTime ShipmentDate,
        String Remarks 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ShipmentDate", DbType.DateTime, ShipmentDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInboundShipment
 
    #region InsertInboundShipment
    /// <summary>
    ///   Inserts a row into the InboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ShipmentDate"></param>
    /// <param name="Remarks"></param>
    /// <returns>bool</returns>
    public bool InsertInboundShipment
    (
        string connectionStringName,
        Int32 InboundShipmentId,
        Int32 StatusId,
        Int32 WarehouseId,
        DateTime ShipmentDate,
        String Remarks 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ShipmentDate", DbType.DateTime, ShipmentDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInboundShipment
 
    #region SearchInboundShipment
    /// <summary>
    ///   Selects rows from the InboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInboundShipment
    (
        string connectionStringName,
        Int32 InboundShipmentId,
        Int32 StatusId,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInboundShipment
 
    #region ListInboundShipment
    /// <summary>
    ///   Lists rows from the InboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInboundShipment
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInboundShipment
 
    #region ParameterInboundShipment
    /// <summary>
    ///   Lists rows from the InboundShipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInboundShipment
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInboundShipment
}
