using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoReason
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:20
/// </summary>
/// <remarks>
///   Inserts a row into the Reason table.
///   Selects rows from the Reason table.
///   Searches for rows from the Reason table.
///   Updates a rows in the Reason table.
///   Deletes a row from the Reason table.
/// </remarks>
/// <param>
public class StaticInfoReason
{
    #region "Constructor Logic"
    public StaticInfoReason()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetReason
    /// <summary>
    ///   Selects rows from the Reason table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReasonId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetReason
    (
        string connectionStringName,
        Int32 ReasonId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetReason
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetReason
 
    #region DeleteReason
    /// <summary>
    ///   Deletes a row from the Reason table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReasonId"></param>
    /// <returns>bool</returns>
    public bool DeleteReason
    (
        string connectionStringName,
        Int32 ReasonId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteReason
 
    #region UpdateReason
    /// <summary>
    ///   Updates a row in the Reason table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReasonId"></param>
    /// <param name="Reason"></param>
    /// <param name="ReasonCode"></param>
    /// <param name="AdjustmentType"></param>
    /// <param name="ToWarehouseCode"></param>
    /// <param name="RecordType"></param>
    /// <returns>bool</returns>
    public bool UpdateReason
    (
        string connectionStringName,
        Int32 ReasonId,
        String Reason,
        String ReasonCode,
        String AdjustmentType,
        String ToWarehouseCode,
        String RecordType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "Reason", DbType.String, Reason);
        db.AddInParameter(dbCommand, "ReasonCode", DbType.String, ReasonCode);
        db.AddInParameter(dbCommand, "AdjustmentType", DbType.String, AdjustmentType);
        db.AddInParameter(dbCommand, "ToWarehouseCode", DbType.String, ToWarehouseCode);
        db.AddInParameter(dbCommand, "RecordType", DbType.String, RecordType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateReason
 
    #region InsertReason
    /// <summary>
    ///   Inserts a row into the Reason table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReasonId"></param>
    /// <param name="Reason"></param>
    /// <param name="ReasonCode"></param>
    /// <param name="AdjustmentType"></param>
    /// <param name="ToWarehouseCode"></param>
    /// <param name="RecordType"></param>
    /// <returns>bool</returns>
    public bool InsertReason
    (
        string connectionStringName,
        Int32 ReasonId,
        String Reason,
        String ReasonCode,
        String AdjustmentType,
        String ToWarehouseCode,
        String RecordType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "Reason", DbType.String, Reason);
        db.AddInParameter(dbCommand, "ReasonCode", DbType.String, ReasonCode);
        db.AddInParameter(dbCommand, "AdjustmentType", DbType.String, AdjustmentType);
        db.AddInParameter(dbCommand, "ToWarehouseCode", DbType.String, ToWarehouseCode);
        db.AddInParameter(dbCommand, "RecordType", DbType.String, RecordType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertReason
 
    #region SearchReason
    /// <summary>
    ///   Selects rows from the Reason table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReasonId"></param>
    /// <param name="Reason"></param>
    /// <param name="ReasonCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchReason
    (
        string connectionStringName,
        Int32 ReasonId,
        String Reason,
        String ReasonCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "Reason", DbType.String, Reason);
        db.AddInParameter(dbCommand, "ReasonCode", DbType.String, ReasonCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchReason
 
    #region PageSearchReason
    /// <summary>
    ///   Selects rows from the Reason table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReasonId"></param>
    /// <param name="Reason"></param>
    /// <param name="ReasonCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchReason
    (
        string connectionStringName,
        Int32 ReasonId,
        String Reason,
        String ReasonCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, ReasonId);
        db.AddInParameter(dbCommand, "Reason", DbType.String, Reason);
        db.AddInParameter(dbCommand, "ReasonCode", DbType.String, ReasonCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchReason
 
    #region ListReason
    /// <summary>
    ///   Lists rows from the Reason table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListReason
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListReason
 
    #region ParameterReason
    /// <summary>
    ///   Lists rows from the Reason table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterReason
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Reason_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterReason
}
