using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoIssueLine
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:12
/// </summary>
/// <remarks>
///   Inserts a row into the IssueLine table.
///   Selects rows from the IssueLine table.
///   Searches for rows from the IssueLine table.
///   Updates a rows in the IssueLine table.
///   Deletes a row from the IssueLine table.
/// </remarks>
/// <param>
public class StaticInfoIssueLine
{
    #region "Constructor Logic"
    public StaticInfoIssueLine()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetIssueLine
    /// <summary>
    ///   Selects rows from the IssueLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueLineId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetIssueLine
    (
        string connectionStringName,
        Int32 IssueLineId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_IssueLine_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetIssueLine
 
    #region DeleteIssueLine
    /// <summary>
    ///   Deletes a row from the IssueLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueLineId"></param>
    /// <returns>bool</returns>
    public bool DeleteIssueLine
    (
        string connectionStringName,
        Int32 IssueLineId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_IssueLine_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteIssueLine
 
    #region UpdateIssueLine
    /// <summary>
    ///   Updates a row in the IssueLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="IssueId"></param>
    /// <param name="OutboundLineId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="Quantity"></param>
    /// <param name="ConfirmedQuatity"></param>
    /// <returns>bool</returns>
    public bool UpdateIssueLine
    (
        string connectionStringName,
        Int32 IssueLineId,
        Int32 IssueId,
        Int32 OutboundLineId,
        Int32 StorageUnitBatchId,
        Int32 StatusId,
        Int32 OperatorId,
        Decimal Quantity,
        Decimal ConfirmedQuatity 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_IssueLine_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, OutboundLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "ConfirmedQuatity", DbType.Decimal, ConfirmedQuatity);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateIssueLine
 
    #region InsertIssueLine
    /// <summary>
    ///   Inserts a row into the IssueLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="IssueId"></param>
    /// <param name="OutboundLineId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="Quantity"></param>
    /// <param name="ConfirmedQuatity"></param>
    /// <returns>bool</returns>
    public bool InsertIssueLine
    (
        string connectionStringName,
        Int32 IssueLineId,
        Int32 IssueId,
        Int32 OutboundLineId,
        Int32 StorageUnitBatchId,
        Int32 StatusId,
        Int32 OperatorId,
        Decimal Quantity,
        Decimal ConfirmedQuatity 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_IssueLine_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, OutboundLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "ConfirmedQuatity", DbType.Decimal, ConfirmedQuatity);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertIssueLine
 
    #region SearchIssueLine
    /// <summary>
    ///   Selects rows from the IssueLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="IssueId"></param>
    /// <param name="OutboundLineId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchIssueLine
    (
        string connectionStringName,
        Int32 IssueLineId,
        Int32 IssueId,
        Int32 OutboundLineId,
        Int32 StorageUnitBatchId,
        Int32 StatusId,
        Int32 OperatorId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_IssueLine_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, OutboundLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchIssueLine
 
    #region ListIssueLine
    /// <summary>
    ///   Lists rows from the IssueLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListIssueLine
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_IssueLine_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListIssueLine
 
    #region ParameterIssueLine
    /// <summary>
    ///   Lists rows from the IssueLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterIssueLine
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_IssueLine_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterIssueLine
}
