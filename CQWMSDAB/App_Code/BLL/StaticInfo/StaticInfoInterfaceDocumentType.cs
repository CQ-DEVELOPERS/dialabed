using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInterfaceDocumentType
///   Create By      : Grant Schultz
///   Date Created   : 19 Mar 2014 08:42:44
/// </summary>
/// <remarks>
///   Inserts a row into the InterfaceDocumentType table.
///   Selects rows from the InterfaceDocumentType table.
///   Searches for rows from the InterfaceDocumentType table.
///   Updates a rows in the InterfaceDocumentType table.
///   Deletes a row from the InterfaceDocumentType table.
/// </remarks>
/// <param>
public class StaticInfoInterfaceDocumentType
{
    #region "Constructor Logic"
    public StaticInfoInterfaceDocumentType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInterfaceDocumentType
    /// <summary>
    ///   Selects rows from the InterfaceDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInterfaceDocumentType
    (
        string connectionStringName,
        Int32 InterfaceDocumentTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInterfaceDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInterfaceDocumentType
 
    #region DeleteInterfaceDocumentType
    /// <summary>
    ///   Deletes a row from the InterfaceDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteInterfaceDocumentType
    (
        string connectionStringName,
        Int32 InterfaceDocumentTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInterfaceDocumentType
 
    #region UpdateInterfaceDocumentType
    /// <summary>
    ///   Updates a row in the InterfaceDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="InterfaceDocumentTypeCode"></param>
    /// <param name="InterfaceDocumentType"></param>
    /// <param name="RecordType"></param>
    /// <param name="InterfaceType"></param>
    /// <param name="SubDirectory"></param>
    /// <param name="FilePrefix"></param>
    /// <param name="InDirectory"></param>
    /// <param name="OutDirectory"></param>
    /// <returns>bool</returns>
    public bool UpdateInterfaceDocumentType
    (
        string connectionStringName,
        Int32 InterfaceDocumentTypeId,
        String InterfaceDocumentTypeCode,
        String InterfaceDocumentType,
        String RecordType,
        String InterfaceType,
        String SubDirectory,
        String FilePrefix,
        Boolean InDirectory,
        Boolean OutDirectory 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeCode", DbType.String, InterfaceDocumentTypeCode);
        db.AddInParameter(dbCommand, "InterfaceDocumentType", DbType.String, InterfaceDocumentType);
        db.AddInParameter(dbCommand, "RecordType", DbType.String, RecordType);
        db.AddInParameter(dbCommand, "InterfaceType", DbType.String, InterfaceType);
        db.AddInParameter(dbCommand, "SubDirectory", DbType.String, SubDirectory);
        db.AddInParameter(dbCommand, "FilePrefix", DbType.String, FilePrefix);
        db.AddInParameter(dbCommand, "InDirectory", DbType.Boolean, InDirectory);
        db.AddInParameter(dbCommand, "OutDirectory", DbType.Boolean, OutDirectory);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInterfaceDocumentType
 
    #region InsertInterfaceDocumentType
    /// <summary>
    ///   Inserts a row into the InterfaceDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="InterfaceDocumentTypeCode"></param>
    /// <param name="InterfaceDocumentType"></param>
    /// <param name="RecordType"></param>
    /// <param name="InterfaceType"></param>
    /// <param name="SubDirectory"></param>
    /// <param name="FilePrefix"></param>
    /// <param name="InDirectory"></param>
    /// <param name="OutDirectory"></param>
    /// <returns>bool</returns>
    public bool InsertInterfaceDocumentType
    (
        string connectionStringName,
        Int32 InterfaceDocumentTypeId,
        String InterfaceDocumentTypeCode,
        String InterfaceDocumentType,
        String RecordType,
        String InterfaceType,
        String SubDirectory,
        String FilePrefix,
        Boolean InDirectory,
        Boolean OutDirectory 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeCode", DbType.String, InterfaceDocumentTypeCode);
        db.AddInParameter(dbCommand, "InterfaceDocumentType", DbType.String, InterfaceDocumentType);
        db.AddInParameter(dbCommand, "RecordType", DbType.String, RecordType);
        db.AddInParameter(dbCommand, "InterfaceType", DbType.String, InterfaceType);
        db.AddInParameter(dbCommand, "SubDirectory", DbType.String, SubDirectory);
        db.AddInParameter(dbCommand, "FilePrefix", DbType.String, FilePrefix);
        db.AddInParameter(dbCommand, "InDirectory", DbType.Boolean, InDirectory);
        db.AddInParameter(dbCommand, "OutDirectory", DbType.Boolean, OutDirectory);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInterfaceDocumentType
 
    #region SearchInterfaceDocumentType
    /// <summary>
    ///   Selects rows from the InterfaceDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="InterfaceDocumentTypeCode"></param>
    /// <param name="InterfaceDocumentType"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInterfaceDocumentType
    (
        string connectionStringName,
        Int32 InterfaceDocumentTypeId,
        String InterfaceDocumentTypeCode,
        String InterfaceDocumentType 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeCode", DbType.String, InterfaceDocumentTypeCode);
        db.AddInParameter(dbCommand, "InterfaceDocumentType", DbType.String, InterfaceDocumentType);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInterfaceDocumentType
 
    #region PageSearchInterfaceDocumentType
    /// <summary>
    ///   Selects rows from the InterfaceDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="InterfaceDocumentTypeCode"></param>
    /// <param name="InterfaceDocumentType"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInterfaceDocumentType
    (
        string connectionStringName,
        Int32 InterfaceDocumentTypeId,
        String InterfaceDocumentTypeCode,
        String InterfaceDocumentType,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeCode", DbType.String, InterfaceDocumentTypeCode);
        db.AddInParameter(dbCommand, "InterfaceDocumentType", DbType.String, InterfaceDocumentType);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInterfaceDocumentType
 
    #region ListInterfaceDocumentType
    /// <summary>
    ///   Lists rows from the InterfaceDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInterfaceDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInterfaceDocumentType
 
    #region ParameterInterfaceDocumentType
    /// <summary>
    ///   Lists rows from the InterfaceDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInterfaceDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceDocumentType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInterfaceDocumentType
}
