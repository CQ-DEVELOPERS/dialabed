using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoRequestType
///   Create By      : Grant Schultz
///   Date Created   : 03 Mar 2014 12:45:57
/// </summary>
/// <remarks>
///   Inserts a row into the RequestType table.
///   Selects rows from the RequestType table.
///   Searches for rows from the RequestType table.
///   Updates a rows in the RequestType table.
///   Deletes a row from the RequestType table.
/// </remarks>
/// <param>
public class StaticInfoRequestType
{
    #region "Constructor Logic"
    public StaticInfoRequestType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetRequestType
    /// <summary>
    ///   Selects rows from the RequestType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RequestTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetRequestType
    (
        string connectionStringName,
        Int32 RequestTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetRequestType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetRequestType
 
    #region DeleteRequestType
    /// <summary>
    ///   Deletes a row from the RequestType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RequestTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteRequestType
    (
        string connectionStringName,
        Int32 RequestTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteRequestType
 
    #region UpdateRequestType
    /// <summary>
    ///   Updates a row in the RequestType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RequestTypeId"></param>
    /// <param name="RequestTypeCode"></param>
    /// <param name="RequestType"></param>
    /// <returns>bool</returns>
    public bool UpdateRequestType
    (
        string connectionStringName,
        Int32 RequestTypeId,
        String RequestTypeCode,
        String RequestType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
        db.AddInParameter(dbCommand, "RequestTypeCode", DbType.String, RequestTypeCode);
        db.AddInParameter(dbCommand, "RequestType", DbType.String, RequestType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateRequestType
 
    #region InsertRequestType
    /// <summary>
    ///   Inserts a row into the RequestType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RequestTypeId"></param>
    /// <param name="RequestTypeCode"></param>
    /// <param name="RequestType"></param>
    /// <returns>bool</returns>
    public bool InsertRequestType
    (
        string connectionStringName,
        Int32 RequestTypeId,
        String RequestTypeCode,
        String RequestType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
        db.AddInParameter(dbCommand, "RequestTypeCode", DbType.String, RequestTypeCode);
        db.AddInParameter(dbCommand, "RequestType", DbType.String, RequestType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertRequestType
 
    #region SearchRequestType
    /// <summary>
    ///   Selects rows from the RequestType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RequestTypeId"></param>
    /// <param name="RequestTypeCode"></param>
    /// <param name="RequestType"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchRequestType
    (
        string connectionStringName,
        Int32 RequestTypeId,
        String RequestTypeCode,
        String RequestType 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
        db.AddInParameter(dbCommand, "RequestTypeCode", DbType.String, RequestTypeCode);
        db.AddInParameter(dbCommand, "RequestType", DbType.String, RequestType);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchRequestType
 
    #region PageSearchRequestType
    /// <summary>
    ///   Selects rows from the RequestType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="RequestTypeId"></param>
    /// <param name="RequestTypeCode"></param>
    /// <param name="RequestType"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchRequestType
    (
        string connectionStringName,
        Int32 RequestTypeId,
        String RequestTypeCode,
        String RequestType,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
        db.AddInParameter(dbCommand, "RequestTypeCode", DbType.String, RequestTypeCode);
        db.AddInParameter(dbCommand, "RequestType", DbType.String, RequestType);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchRequestType
 
    #region ListRequestType
    /// <summary>
    ///   Lists rows from the RequestType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListRequestType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListRequestType
 
    #region ParameterRequestType
    /// <summary>
    ///   Lists rows from the RequestType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterRequestType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_RequestType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterRequestType
}
