using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInterfaceFileType
///   Create By      : Grant Schultz
///   Date Created   : 19 Mar 2014 08:42:51
/// </summary>
/// <remarks>
///   Inserts a row into the InterfaceFileType table.
///   Selects rows from the InterfaceFileType table.
///   Searches for rows from the InterfaceFileType table.
///   Updates a rows in the InterfaceFileType table.
///   Deletes a row from the InterfaceFileType table.
/// </remarks>
/// <param>
public class StaticInfoInterfaceFileType
{
    #region "Constructor Logic"
    public StaticInfoInterfaceFileType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInterfaceFileType
    /// <summary>
    ///   Selects rows from the InterfaceFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInterfaceFileType
    (
        string connectionStringName,
        Int32 InterfaceFileTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInterfaceFileType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInterfaceFileType
 
    #region DeleteInterfaceFileType
    /// <summary>
    ///   Deletes a row from the InterfaceFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteInterfaceFileType
    (
        string connectionStringName,
        Int32 InterfaceFileTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInterfaceFileType
 
    #region UpdateInterfaceFileType
    /// <summary>
    ///   Updates a row in the InterfaceFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <param name="InterfaceFileTypeCode"></param>
    /// <param name="InterfaceFileType"></param>
    /// <returns>bool</returns>
    public bool UpdateInterfaceFileType
    (
        string connectionStringName,
        Int32 InterfaceFileTypeId,
        String InterfaceFileTypeCode,
        String InterfaceFileType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
        db.AddInParameter(dbCommand, "InterfaceFileTypeCode", DbType.String, InterfaceFileTypeCode);
        db.AddInParameter(dbCommand, "InterfaceFileType", DbType.String, InterfaceFileType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInterfaceFileType
 
    #region InsertInterfaceFileType
    /// <summary>
    ///   Inserts a row into the InterfaceFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <param name="InterfaceFileTypeCode"></param>
    /// <param name="InterfaceFileType"></param>
    /// <returns>bool</returns>
    public bool InsertInterfaceFileType
    (
        string connectionStringName,
        Int32 InterfaceFileTypeId,
        String InterfaceFileTypeCode,
        String InterfaceFileType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
        db.AddInParameter(dbCommand, "InterfaceFileTypeCode", DbType.String, InterfaceFileTypeCode);
        db.AddInParameter(dbCommand, "InterfaceFileType", DbType.String, InterfaceFileType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInterfaceFileType
 
    #region SearchInterfaceFileType
    /// <summary>
    ///   Selects rows from the InterfaceFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <param name="InterfaceFileTypeCode"></param>
    /// <param name="InterfaceFileType"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInterfaceFileType
    (
        string connectionStringName,
        Int32 InterfaceFileTypeId,
        String InterfaceFileTypeCode,
        String InterfaceFileType 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
        db.AddInParameter(dbCommand, "InterfaceFileTypeCode", DbType.String, InterfaceFileTypeCode);
        db.AddInParameter(dbCommand, "InterfaceFileType", DbType.String, InterfaceFileType);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInterfaceFileType
 
    #region PageSearchInterfaceFileType
    /// <summary>
    ///   Selects rows from the InterfaceFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceFileTypeId"></param>
    /// <param name="InterfaceFileTypeCode"></param>
    /// <param name="InterfaceFileType"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInterfaceFileType
    (
        string connectionStringName,
        Int32 InterfaceFileTypeId,
        String InterfaceFileTypeCode,
        String InterfaceFileType,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceFileTypeId", DbType.Int32, InterfaceFileTypeId);
        db.AddInParameter(dbCommand, "InterfaceFileTypeCode", DbType.String, InterfaceFileTypeCode);
        db.AddInParameter(dbCommand, "InterfaceFileType", DbType.String, InterfaceFileType);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInterfaceFileType
 
    #region ListInterfaceFileType
    /// <summary>
    ///   Lists rows from the InterfaceFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInterfaceFileType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInterfaceFileType
 
    #region ParameterInterfaceFileType
    /// <summary>
    ///   Lists rows from the InterfaceFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInterfaceFileType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceFileType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInterfaceFileType
}
