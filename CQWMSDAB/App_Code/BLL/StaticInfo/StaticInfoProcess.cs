using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoProcess
///   Create By      : Grant Schultz
///   Date Created   : 30 Apr 2014 11:48:13
/// </summary>
/// <remarks>
///   Inserts a row into the Process table.
///   Selects rows from the Process table.
///   Searches for rows from the Process table.
///   Updates a rows in the Process table.
///   Deletes a row from the Process table.
/// </remarks>
/// <param>
public class StaticInfoProcess
{
    #region "Constructor Logic"
    public StaticInfoProcess()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetProcess
    /// <summary>
    ///   Selects rows from the Process table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProcessId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetProcess
    (
        string connectionStringName,
        Int32 ProcessId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetProcess
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetProcess
 
    #region DeleteProcess
    /// <summary>
    ///   Deletes a row from the Process table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProcessId"></param>
    /// <returns>bool</returns>
    public bool DeleteProcess
    (
        string connectionStringName,
        Int32 ProcessId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteProcess
 
    #region UpdateProcess
    /// <summary>
    ///   Updates a row in the Process table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProcessId"></param>
    /// <param name="ProcessCode"></param>
    /// <param name="Process"></param>
    /// <returns>bool</returns>
    public bool UpdateProcess
    (
        string connectionStringName,
        Int32 ProcessId,
        String ProcessCode,
        String Process 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
        db.AddInParameter(dbCommand, "ProcessCode", DbType.String, ProcessCode);
        db.AddInParameter(dbCommand, "Process", DbType.String, Process);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateProcess
 
    #region InsertProcess
    /// <summary>
    ///   Inserts a row into the Process table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProcessId"></param>
    /// <param name="ProcessCode"></param>
    /// <param name="Process"></param>
    /// <returns>bool</returns>
    public bool InsertProcess
    (
        string connectionStringName,
        Int32 ProcessId,
        String ProcessCode,
        String Process 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
        db.AddInParameter(dbCommand, "ProcessCode", DbType.String, ProcessCode);
        db.AddInParameter(dbCommand, "Process", DbType.String, Process);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertProcess
 
    #region SearchProcess
    /// <summary>
    ///   Selects rows from the Process table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProcessId"></param>
    /// <param name="ProcessCode"></param>
    /// <param name="Process"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchProcess
    (
        string connectionStringName,
        Int32 ProcessId,
        String ProcessCode,
        String Process 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
        db.AddInParameter(dbCommand, "ProcessCode", DbType.String, ProcessCode);
        db.AddInParameter(dbCommand, "Process", DbType.String, Process);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchProcess
 
    #region PageSearchProcess
    /// <summary>
    ///   Selects rows from the Process table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProcessId"></param>
    /// <param name="ProcessCode"></param>
    /// <param name="Process"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchProcess
    (
        string connectionStringName,
        Int32 ProcessId,
        String ProcessCode,
        String Process,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
        db.AddInParameter(dbCommand, "ProcessCode", DbType.String, ProcessCode);
        db.AddInParameter(dbCommand, "Process", DbType.String, Process);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchProcess
 
    #region ListProcess
    /// <summary>
    ///   Lists rows from the Process table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListProcess
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListProcess
 
    #region ParameterProcess
    /// <summary>
    ///   Lists rows from the Process table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterProcess
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Process_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterProcess
}
