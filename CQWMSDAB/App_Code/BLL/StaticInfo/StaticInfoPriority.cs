using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoPriority
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:19
/// </summary>
/// <remarks>
///   Inserts a row into the Priority table.
///   Selects rows from the Priority table.
///   Searches for rows from the Priority table.
///   Updates a rows in the Priority table.
///   Deletes a row from the Priority table.
/// </remarks>
/// <param>
public class StaticInfoPriority
{
    #region "Constructor Logic"
    public StaticInfoPriority()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetPriority
    /// <summary>
    ///   Selects rows from the Priority table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriorityId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetPriority
    (
        string connectionStringName,
        Int32 PriorityId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetPriority
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetPriority
 
    #region DeletePriority
    /// <summary>
    ///   Deletes a row from the Priority table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriorityId"></param>
    /// <returns>bool</returns>
    public bool DeletePriority
    (
        string connectionStringName,
        Int32 PriorityId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeletePriority
 
    #region UpdatePriority
    /// <summary>
    ///   Updates a row in the Priority table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriorityId"></param>
    /// <param name="Priority"></param>
    /// <param name="PriorityCode"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool UpdatePriority
    (
        string connectionStringName,
        Int32 PriorityId,
        String Priority,
        String PriorityCode,
        Int32 OrderBy 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "Priority", DbType.String, Priority);
        db.AddInParameter(dbCommand, "PriorityCode", DbType.String, PriorityCode);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, OrderBy);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdatePriority
 
    #region InsertPriority
    /// <summary>
    ///   Inserts a row into the Priority table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriorityId"></param>
    /// <param name="Priority"></param>
    /// <param name="PriorityCode"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool InsertPriority
    (
        string connectionStringName,
        Int32 PriorityId,
        String Priority,
        String PriorityCode,
        Int32 OrderBy 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "Priority", DbType.String, Priority);
        db.AddInParameter(dbCommand, "PriorityCode", DbType.String, PriorityCode);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, OrderBy);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertPriority
 
    #region SearchPriority
    /// <summary>
    ///   Selects rows from the Priority table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriorityId"></param>
    /// <param name="Priority"></param>
    /// <param name="PriorityCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchPriority
    (
        string connectionStringName,
        Int32 PriorityId,
        String Priority,
        String PriorityCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "Priority", DbType.String, Priority);
        db.AddInParameter(dbCommand, "PriorityCode", DbType.String, PriorityCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchPriority
 
    #region PageSearchPriority
    /// <summary>
    ///   Selects rows from the Priority table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PriorityId"></param>
    /// <param name="Priority"></param>
    /// <param name="PriorityCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchPriority
    (
        string connectionStringName,
        Int32 PriorityId,
        String Priority,
        String PriorityCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "Priority", DbType.String, Priority);
        db.AddInParameter(dbCommand, "PriorityCode", DbType.String, PriorityCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchPriority
 
    #region ListPriority
    /// <summary>
    ///   Lists rows from the Priority table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListPriority
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListPriority
 
    #region ParameterPriority
    /// <summary>
    ///   Lists rows from the Priority table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterPriority
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Priority_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterPriority
}
