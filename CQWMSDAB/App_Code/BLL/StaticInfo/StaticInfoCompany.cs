using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoCompany
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:14:56
/// </summary>
/// <remarks>
///   Inserts a row into the Company table.
///   Selects rows from the Company table.
///   Searches for rows from the Company table.
///   Updates a rows in the Company table.
///   Deletes a row from the Company table.
/// </remarks>
/// <param>
public class StaticInfoCompany
{
    #region "Constructor Logic"
    public StaticInfoCompany()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetCompany
    /// <summary>
    ///   Selects rows from the Company table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CompanyId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetCompany
    (
        string connectionStringName,
        Int32 CompanyId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Company_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetCompany
 
    #region DeleteCompany
    /// <summary>
    ///   Deletes a row from the Company table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CompanyId"></param>
    /// <returns>bool</returns>
    public bool DeleteCompany
    (
        string connectionStringName,
        Int32 CompanyId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Company_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteCompany
 
    #region UpdateCompany
    /// <summary>
    ///   Updates a row in the Company table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CompanyId"></param>
    /// <param name="Company"></param>
    /// <param name="CompanyCode"></param>
    /// <returns>bool</returns>
    public bool UpdateCompany
    (
        string connectionStringName,
        Int32 CompanyId,
        String Company,
        String CompanyCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Company_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
        db.AddInParameter(dbCommand, "Company", DbType.String, Company);
        db.AddInParameter(dbCommand, "CompanyCode", DbType.String, CompanyCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateCompany
 
    #region InsertCompany
    /// <summary>
    ///   Inserts a row into the Company table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CompanyId"></param>
    /// <param name="Company"></param>
    /// <param name="CompanyCode"></param>
    /// <returns>bool</returns>
    public bool InsertCompany
    (
        string connectionStringName,
        Int32 CompanyId,
        String Company,
        String CompanyCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Company_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
        db.AddInParameter(dbCommand, "Company", DbType.String, Company);
        db.AddInParameter(dbCommand, "CompanyCode", DbType.String, CompanyCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertCompany
 
    #region SearchCompany
    /// <summary>
    ///   Selects rows from the Company table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="CompanyId"></param>
    /// <param name="Company"></param>
    /// <param name="CompanyCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchCompany
    (
        string connectionStringName,
        Int32 CompanyId,
        String Company,
        String CompanyCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Company_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
        db.AddInParameter(dbCommand, "Company", DbType.String, Company);
        db.AddInParameter(dbCommand, "CompanyCode", DbType.String, CompanyCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchCompany
 
    #region ListCompany
    /// <summary>
    ///   Lists rows from the Company table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListCompany
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Company_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListCompany
 
    #region ParameterCompany
    /// <summary>
    ///   Lists rows from the Company table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterCompany
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Company_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterCompany
}
