using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoStorageUnit
///   Create By      : Grant Schultz
///   Date Created   : 14 May 2014 16:58:14
/// </summary>
/// <remarks>
///   Inserts a row into the StorageUnit table.
///   Selects rows from the StorageUnit table.
///   Searches for rows from the StorageUnit table.
///   Updates a rows in the StorageUnit table.
///   Deletes a row from the StorageUnit table.
/// </remarks>
/// <param>
public class StaticInfoStorageUnit
{
    #region "Constructor Logic"
    public StaticInfoStorageUnit()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetStorageUnit
    /// <summary>
    ///   Selects rows from the StorageUnit table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetStorageUnit
    (
        string connectionStringName,
        Int32 StorageUnitId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetStorageUnit
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetStorageUnit
 
    #region DeleteStorageUnit
    /// <summary>
    ///   Deletes a row from the StorageUnit table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>bool</returns>
    public bool DeleteStorageUnit
    (
        string connectionStringName,
        Int32 StorageUnitId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteStorageUnit
 
    #region UpdateStorageUnit
    /// <summary>
    ///   Updates a row in the StorageUnit table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="SKUId"></param>
    /// <param name="ProductId"></param>
    /// <param name="ProductCategory"></param>
    /// <param name="PackingCategory"></param>
    /// <param name="PickEmpty"></param>
    /// <param name="StackingCategory"></param>
    /// <param name="MovementCategory"></param>
    /// <param name="ValueCategory"></param>
    /// <param name="StoringCategory"></param>
    /// <param name="PickPartPallet"></param>
    /// <param name="MinimumQuantity"></param>
    /// <param name="ReorderQuantity"></param>
    /// <param name="MaximumQuantity"></param>
    /// <param name="UnitPrice"></param>
    /// <param name="Size"></param>
    /// <param name="Colour"></param>
    /// <param name="Style"></param>
    /// <param name="PreviousUnitPrice"></param>
    /// <param name="StockTakeCounts"></param>
    /// <param name="DefaultQC"></param>
    /// <param name="StatusId"></param>
    /// <param name="SerialTracked"></param>
    /// <param name="SizeCode"></param>
    /// <param name="ColourCode"></param>
    /// <param name="StyleCode"></param>
    /// <param name="PutawayRule"></param>
    /// <param name="AllocationRule"></param>
    /// <param name="BillingRule"></param>
    /// <param name="CycleCountRule"></param>
    /// <param name="LotAttributeRule"></param>
    /// <param name="StorageCondition"></param>
    /// <returns>bool</returns>
    public bool UpdateStorageUnit
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 SKUId,
        Int32 ProductId,
        String ProductCategory,
        String PackingCategory,
        Boolean PickEmpty,
        Int32 StackingCategory,
        Int32 MovementCategory,
        Int32 ValueCategory,
        Int32 StoringCategory,
        Int32 PickPartPallet,
        Double MinimumQuantity,
        Double ReorderQuantity,
        Double MaximumQuantity,
        Decimal UnitPrice,
        String Size,
        String Colour,
        String Style,
        Decimal PreviousUnitPrice,
        Int32 StockTakeCounts,
        Boolean DefaultQC,
        Int32 StatusId,
        Boolean SerialTracked,
        String SizeCode,
        String ColourCode,
        String StyleCode,
        String PutawayRule,
        String AllocationRule,
        String BillingRule,
        String CycleCountRule,
        String LotAttributeRule,
        String StorageCondition 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
        db.AddInParameter(dbCommand, "ProductCategory", DbType.String, ProductCategory);
        db.AddInParameter(dbCommand, "PackingCategory", DbType.String, PackingCategory);
        db.AddInParameter(dbCommand, "PickEmpty", DbType.Boolean, PickEmpty);
        db.AddInParameter(dbCommand, "StackingCategory", DbType.Int32, StackingCategory);
        db.AddInParameter(dbCommand, "MovementCategory", DbType.Int32, MovementCategory);
        db.AddInParameter(dbCommand, "ValueCategory", DbType.Int32, ValueCategory);
        db.AddInParameter(dbCommand, "StoringCategory", DbType.Int32, StoringCategory);
        db.AddInParameter(dbCommand, "PickPartPallet", DbType.Int32, PickPartPallet);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Double, MinimumQuantity);
        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Double, ReorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Double, MaximumQuantity);
        db.AddInParameter(dbCommand, "UnitPrice", DbType.Decimal, UnitPrice);
        db.AddInParameter(dbCommand, "Size", DbType.String, Size);
        db.AddInParameter(dbCommand, "Colour", DbType.String, Colour);
        db.AddInParameter(dbCommand, "Style", DbType.String, Style);
        db.AddInParameter(dbCommand, "PreviousUnitPrice", DbType.Decimal, PreviousUnitPrice);
        db.AddInParameter(dbCommand, "StockTakeCounts", DbType.Int32, StockTakeCounts);
        db.AddInParameter(dbCommand, "DefaultQC", DbType.Boolean, DefaultQC);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "SerialTracked", DbType.Boolean, SerialTracked);
        db.AddInParameter(dbCommand, "SizeCode", DbType.String, SizeCode);
        db.AddInParameter(dbCommand, "ColourCode", DbType.String, ColourCode);
        db.AddInParameter(dbCommand, "StyleCode", DbType.String, StyleCode);
        db.AddInParameter(dbCommand, "PutawayRule", DbType.String, PutawayRule);
        db.AddInParameter(dbCommand, "AllocationRule", DbType.String, AllocationRule);
        db.AddInParameter(dbCommand, "BillingRule", DbType.String, BillingRule);
        db.AddInParameter(dbCommand, "CycleCountRule", DbType.String, CycleCountRule);
        db.AddInParameter(dbCommand, "LotAttributeRule", DbType.String, LotAttributeRule);
        db.AddInParameter(dbCommand, "StorageCondition", DbType.String, StorageCondition);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateStorageUnit
 
    #region InsertStorageUnit
    /// <summary>
    ///   Inserts a row into the StorageUnit table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="SKUId"></param>
    /// <param name="ProductId"></param>
    /// <param name="ProductCategory"></param>
    /// <param name="PackingCategory"></param>
    /// <param name="PickEmpty"></param>
    /// <param name="StackingCategory"></param>
    /// <param name="MovementCategory"></param>
    /// <param name="ValueCategory"></param>
    /// <param name="StoringCategory"></param>
    /// <param name="PickPartPallet"></param>
    /// <param name="MinimumQuantity"></param>
    /// <param name="ReorderQuantity"></param>
    /// <param name="MaximumQuantity"></param>
    /// <param name="UnitPrice"></param>
    /// <param name="Size"></param>
    /// <param name="Colour"></param>
    /// <param name="Style"></param>
    /// <param name="PreviousUnitPrice"></param>
    /// <param name="StockTakeCounts"></param>
    /// <param name="DefaultQC"></param>
    /// <param name="StatusId"></param>
    /// <param name="SerialTracked"></param>
    /// <param name="SizeCode"></param>
    /// <param name="ColourCode"></param>
    /// <param name="StyleCode"></param>
    /// <param name="PutawayRule"></param>
    /// <param name="AllocationRule"></param>
    /// <param name="BillingRule"></param>
    /// <param name="CycleCountRule"></param>
    /// <param name="LotAttributeRule"></param>
    /// <param name="StorageCondition"></param>
    /// <returns>bool</returns>
    public bool InsertStorageUnit
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 SKUId,
        Int32 ProductId,
        String ProductCategory,
        String PackingCategory,
        Boolean PickEmpty,
        Int32 StackingCategory,
        Int32 MovementCategory,
        Int32 ValueCategory,
        Int32 StoringCategory,
        Int32 PickPartPallet,
        Double MinimumQuantity,
        Double ReorderQuantity,
        Double MaximumQuantity,
        Decimal UnitPrice,
        String Size,
        String Colour,
        String Style,
        Decimal PreviousUnitPrice,
        Int32 StockTakeCounts,
        Boolean DefaultQC,
        Int32 StatusId,
        Boolean SerialTracked,
        String SizeCode,
        String ColourCode,
        String StyleCode,
        String PutawayRule,
        String AllocationRule,
        String BillingRule,
        String CycleCountRule,
        String LotAttributeRule,
        String StorageCondition 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
        db.AddInParameter(dbCommand, "ProductCategory", DbType.String, ProductCategory);
        db.AddInParameter(dbCommand, "PackingCategory", DbType.String, PackingCategory);
        db.AddInParameter(dbCommand, "PickEmpty", DbType.Boolean, PickEmpty);
        db.AddInParameter(dbCommand, "StackingCategory", DbType.Int32, StackingCategory);
        db.AddInParameter(dbCommand, "MovementCategory", DbType.Int32, MovementCategory);
        db.AddInParameter(dbCommand, "ValueCategory", DbType.Int32, ValueCategory);
        db.AddInParameter(dbCommand, "StoringCategory", DbType.Int32, StoringCategory);
        db.AddInParameter(dbCommand, "PickPartPallet", DbType.Int32, PickPartPallet);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Double, MinimumQuantity);
        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Double, ReorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Double, MaximumQuantity);
        db.AddInParameter(dbCommand, "UnitPrice", DbType.Decimal, UnitPrice);
        db.AddInParameter(dbCommand, "Size", DbType.String, Size);
        db.AddInParameter(dbCommand, "Colour", DbType.String, Colour);
        db.AddInParameter(dbCommand, "Style", DbType.String, Style);
        db.AddInParameter(dbCommand, "PreviousUnitPrice", DbType.Decimal, PreviousUnitPrice);
        db.AddInParameter(dbCommand, "StockTakeCounts", DbType.Int32, StockTakeCounts);
        db.AddInParameter(dbCommand, "DefaultQC", DbType.Boolean, DefaultQC);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "SerialTracked", DbType.Boolean, SerialTracked);
        db.AddInParameter(dbCommand, "SizeCode", DbType.String, SizeCode);
        db.AddInParameter(dbCommand, "ColourCode", DbType.String, ColourCode);
        db.AddInParameter(dbCommand, "StyleCode", DbType.String, StyleCode);
        db.AddInParameter(dbCommand, "PutawayRule", DbType.String, PutawayRule);
        db.AddInParameter(dbCommand, "AllocationRule", DbType.String, AllocationRule);
        db.AddInParameter(dbCommand, "BillingRule", DbType.String, BillingRule);
        db.AddInParameter(dbCommand, "CycleCountRule", DbType.String, CycleCountRule);
        db.AddInParameter(dbCommand, "LotAttributeRule", DbType.String, LotAttributeRule);
        db.AddInParameter(dbCommand, "StorageCondition", DbType.String, StorageCondition);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertStorageUnit
 
    #region SearchStorageUnit
    /// <summary>
    ///   Selects rows from the StorageUnit table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="SKUId"></param>
    /// <param name="ProductId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchStorageUnit
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 SKUId,
        Int32 ProductId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchStorageUnit
 
    #region PageSearchStorageUnit
    /// <summary>
    ///   Selects rows from the StorageUnit table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="SKUId"></param>
    /// <param name="ProductId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchStorageUnit
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 SKUId,
        Int32 ProductId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, SKUId);
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchStorageUnit
 
    #region ListStorageUnit
    /// <summary>
    ///   Lists rows from the StorageUnit table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListStorageUnit
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListStorageUnit
 
    #region ParameterStorageUnit
    /// <summary>
    ///   Lists rows from the StorageUnit table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterStorageUnit
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterStorageUnit
}
