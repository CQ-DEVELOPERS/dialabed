using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoWarehouse
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:28
/// </summary>
/// <remarks>
///   Inserts a row into the Warehouse table.
///   Selects rows from the Warehouse table.
///   Searches for rows from the Warehouse table.
///   Updates a rows in the Warehouse table.
///   Deletes a row from the Warehouse table.
/// </remarks>
/// <param>
public class StaticInfoWarehouse
{
    #region "Constructor Logic"
    public StaticInfoWarehouse()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetWarehouse
    /// <summary>
    ///   Selects rows from the Warehouse table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetWarehouse
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetWarehouse
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetWarehouse
 
    #region DeleteWarehouse
    /// <summary>
    ///   Deletes a row from the Warehouse table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>bool</returns>
    public bool DeleteWarehouse
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteWarehouse
 
    #region UpdateWarehouse
    /// <summary>
    ///   Updates a row in the Warehouse table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="CompanyId"></param>
    /// <param name="Warehouse"></param>
    /// <param name="WarehouseCode"></param>
    /// <param name="HostId"></param>
    /// <param name="ParentWarehouseId"></param>
    /// <param name="AddressId"></param>
    /// <param name="DesktopMaximum"></param>
    /// <param name="DesktopWarning"></param>
    /// <param name="MobileWarning"></param>
    /// <param name="MobileMaximum"></param>
    /// <param name="UploadToHost"></param>
    /// <param name="PostalAddressId"></param>
    /// <param name="ContactListId"></param>
    /// <returns>bool</returns>
    public bool UpdateWarehouse
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 CompanyId,
        String Warehouse,
        String WarehouseCode,
        String HostId,
        Int32 ParentWarehouseId,
        Int32 AddressId,
        Int32 DesktopMaximum,
        Int32 DesktopWarning,
        Int32 MobileWarning,
        Int32 MobileMaximum,
        Boolean UploadToHost,
        Int32 PostalAddressId,
        Int32 ContactListId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
        db.AddInParameter(dbCommand, "Warehouse", DbType.String, Warehouse);
        db.AddInParameter(dbCommand, "WarehouseCode", DbType.String, WarehouseCode);
        db.AddInParameter(dbCommand, "HostId", DbType.String, HostId);
        db.AddInParameter(dbCommand, "ParentWarehouseId", DbType.Int32, ParentWarehouseId);
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
        db.AddInParameter(dbCommand, "DesktopMaximum", DbType.Int32, DesktopMaximum);
        db.AddInParameter(dbCommand, "DesktopWarning", DbType.Int32, DesktopWarning);
        db.AddInParameter(dbCommand, "MobileWarning", DbType.Int32, MobileWarning);
        db.AddInParameter(dbCommand, "MobileMaximum", DbType.Int32, MobileMaximum);
        db.AddInParameter(dbCommand, "UploadToHost", DbType.Boolean, UploadToHost);
        db.AddInParameter(dbCommand, "PostalAddressId", DbType.Int32, PostalAddressId);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateWarehouse
 
    #region InsertWarehouse
    /// <summary>
    ///   Inserts a row into the Warehouse table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="CompanyId"></param>
    /// <param name="Warehouse"></param>
    /// <param name="WarehouseCode"></param>
    /// <param name="HostId"></param>
    /// <param name="ParentWarehouseId"></param>
    /// <param name="AddressId"></param>
    /// <param name="DesktopMaximum"></param>
    /// <param name="DesktopWarning"></param>
    /// <param name="MobileWarning"></param>
    /// <param name="MobileMaximum"></param>
    /// <param name="UploadToHost"></param>
    /// <param name="PostalAddressId"></param>
    /// <param name="ContactListId"></param>
    /// <returns>bool</returns>
    public bool InsertWarehouse
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 CompanyId,
        String Warehouse,
        String WarehouseCode,
        String HostId,
        Int32 ParentWarehouseId,
        Int32 AddressId,
        Int32 DesktopMaximum,
        Int32 DesktopWarning,
        Int32 MobileWarning,
        Int32 MobileMaximum,
        Boolean UploadToHost,
        Int32 PostalAddressId,
        Int32 ContactListId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
        db.AddInParameter(dbCommand, "Warehouse", DbType.String, Warehouse);
        db.AddInParameter(dbCommand, "WarehouseCode", DbType.String, WarehouseCode);
        db.AddInParameter(dbCommand, "HostId", DbType.String, HostId);
        db.AddInParameter(dbCommand, "ParentWarehouseId", DbType.Int32, ParentWarehouseId);
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
        db.AddInParameter(dbCommand, "DesktopMaximum", DbType.Int32, DesktopMaximum);
        db.AddInParameter(dbCommand, "DesktopWarning", DbType.Int32, DesktopWarning);
        db.AddInParameter(dbCommand, "MobileWarning", DbType.Int32, MobileWarning);
        db.AddInParameter(dbCommand, "MobileMaximum", DbType.Int32, MobileMaximum);
        db.AddInParameter(dbCommand, "UploadToHost", DbType.Boolean, UploadToHost);
        db.AddInParameter(dbCommand, "PostalAddressId", DbType.Int32, PostalAddressId);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertWarehouse
 
    #region SearchWarehouse
    /// <summary>
    ///   Selects rows from the Warehouse table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="CompanyId"></param>
    /// <param name="Warehouse"></param>
    /// <param name="WarehouseCode"></param>
    /// <param name="ParentWarehouseId"></param>
    /// <param name="AddressId"></param>
    /// <param name="PostalAddressId"></param>
    /// <param name="ContactListId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchWarehouse
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 CompanyId,
        String Warehouse,
        String WarehouseCode,
        Int32 ParentWarehouseId,
        Int32 AddressId,
        Int32 PostalAddressId,
        Int32 ContactListId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
        db.AddInParameter(dbCommand, "Warehouse", DbType.String, Warehouse);
        db.AddInParameter(dbCommand, "WarehouseCode", DbType.String, WarehouseCode);
        db.AddInParameter(dbCommand, "ParentWarehouseId", DbType.Int32, ParentWarehouseId);
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
        db.AddInParameter(dbCommand, "PostalAddressId", DbType.Int32, PostalAddressId);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchWarehouse
 
    #region PageSearchWarehouse
    /// <summary>
    ///   Selects rows from the Warehouse table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="CompanyId"></param>
    /// <param name="Warehouse"></param>
    /// <param name="WarehouseCode"></param>
    /// <param name="ParentWarehouseId"></param>
    /// <param name="AddressId"></param>
    /// <param name="PostalAddressId"></param>
    /// <param name="ContactListId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchWarehouse
    (
        string connectionStringName,
        Int32 WarehouseId,
        Int32 CompanyId,
        String Warehouse,
        String WarehouseCode,
        Int32 ParentWarehouseId,
        Int32 AddressId,
        Int32 PostalAddressId,
        Int32 ContactListId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "CompanyId", DbType.Int32, CompanyId);
        db.AddInParameter(dbCommand, "Warehouse", DbType.String, Warehouse);
        db.AddInParameter(dbCommand, "WarehouseCode", DbType.String, WarehouseCode);
        db.AddInParameter(dbCommand, "ParentWarehouseId", DbType.Int32, ParentWarehouseId);
        db.AddInParameter(dbCommand, "AddressId", DbType.Int32, AddressId);
        db.AddInParameter(dbCommand, "PostalAddressId", DbType.Int32, PostalAddressId);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchWarehouse
 
    #region ListWarehouse
    /// <summary>
    ///   Lists rows from the Warehouse table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ParentWarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListWarehouse
    (
        string connectionStringName,
        Int32 ParentWarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ParentWarehouseId", DbType.Int32, ParentWarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ListWarehouse
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListWarehouse
 
    #region ParameterWarehouse
    /// <summary>
    ///   Lists rows from the Warehouse table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ParentWarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterWarehouse
    (
        string connectionStringName,
        Int32 ParentWarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ParentWarehouseId", DbType.Int32, ParentWarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ParameterWarehouse
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Warehouse_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterWarehouse
}
