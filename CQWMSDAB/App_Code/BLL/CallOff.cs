using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   16 Oct 2007
/// Summary description for Planning
/// </summary>
public class CallOff
{
    #region Constructor Logic
    public CallOff()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchOrders
    /// <summary>
    /// Retrieves a list of call off orders
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="callOffNumber"></param>
    /// <param name="orderNumber"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchOrders(string connectionStringName,
                                int warehouseId,
                                string callOffNumber,
                                string orderNumber,
                                string productCode,
                                string product,
                                string externalCompanyCode,
                                string externalCompany,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "CallOffNumber", DbType.String, callOffNumber);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrders"

    #region SelectOrder
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SelectOrder(string connectionStringName,
                                int callOffHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "CallOffHeaderId", DbType.Int32, callOffHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectOrder"

    #region InsertCallOffHeader
    public int InsertCallOffHeader(string connectionStringName,
                                   int callOffHeaderId,
                                   string callOffNumber,
                                   int priorityId,
                                   int externalCompanyId,
                                   int warehouseId,
                                   int transportModeId,
                                   int notificationMethodId,
                                   DateTime requiredDate,
                                   Boolean confirmReceipt,
                                   Boolean confirmAccpeted,
                                   String remarks)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Insert";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffNumber", DbType.String, callOffNumber);
        db.AddInParameter(dbCommand, "priorityId", DbType.Int32, priorityId);
        db.AddInParameter(dbCommand, "externalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "transportModeId", DbType.Int32, transportModeId);
        db.AddInParameter(dbCommand, "notificationMethodId", DbType.Int32, notificationMethodId);
        db.AddInParameter(dbCommand, "requiredDate", DbType.DateTime, requiredDate);
        db.AddInParameter(dbCommand, "confirmReceipt", DbType.Boolean, confirmReceipt);
        db.AddInParameter(dbCommand, "confirmAccpeted", DbType.Boolean, confirmAccpeted);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);

        //Return the correct id for the new record
        db.AddOutParameter(dbCommand, "CallOffHeaderId", DbType.Int32, result);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = (int)db.GetParameterValue(dbCommand, "@CallOffHeaderId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "InsertCallOffHeader"

    #region UpdateCallOffHeader
    public bool UpdateCallOffHeader(string connectionStringName,
                                    int callOffHeaderId,
                                    string callOffNumber,
                                    int priorityId,
                                    int externalCompanyId,
                                    int warehouseId,
                                    int transportModeId,
                                    int notificationMethodId,
                                    DateTime requiredDate,
                                    Boolean confirmReceipt,
                                    Boolean confirmAccpeted,
                                    String remarks)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Update";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);
        db.AddInParameter(dbCommand, "callOffNumber", DbType.String, callOffNumber);
        db.AddInParameter(dbCommand, "priorityId", DbType.Int32, priorityId);
        db.AddInParameter(dbCommand, "externalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "transportModeId", DbType.Int32, transportModeId);
        db.AddInParameter(dbCommand, "notificationMethodId", DbType.Int32, notificationMethodId);
        db.AddInParameter(dbCommand, "requiredDate", DbType.DateTime, requiredDate);
        db.AddInParameter(dbCommand, "confirmReceipt", DbType.Boolean, confirmReceipt);
        db.AddInParameter(dbCommand, "confirmAccpeted", DbType.Boolean, confirmAccpeted);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateCallOffHeader"

    #region DeleteCallOffHeader
    public bool DeleteCallOffHeader(string connectionStringName,
                                        int callOffHeaderId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOffHeader_Delete";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "CallOffHeaderId", DbType.Int32, callOffHeaderId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteCallOffHeader"

    #region SearchLinked
    /// <summary>
    /// Search for list of call off lines linked to specific header
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SearchLinked(string connectionStringName,
                                int callOffHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Linked_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchLinked"

    #region SearchUnlinked
    /// <summary>
    /// Search for list of inlined PO's
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="callOffNumber"></param>
    /// <param name="orderNumber"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchUnlinked(string connectionStringName,
                                int warehouseId,
                                int documentTypeId,
                                string orderNumber,
                                string productCode,
                                string product,
                                string externalCompanyCode,
                                string externalCompany,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Unlinked_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "documentTypeId", DbType.Int32, documentTypeId);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "externalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "externalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchUnlinked"

    #region DeleteLinked
    /// <summary>
    /// Update a linked call off line.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffDetailId"></param>
    /// <param name="newCallOffQuantity"></param>
    /// <returns></returns>
    public bool DeleteLinked(string connectionStringName,
                            int callOffDetailId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Linked_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "callOffDetailId", DbType.Int32, callOffDetailId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteLinked"

    #region UpdateLinked
    /// <summary>
    /// Update a linked call off line.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffDetailId"></param>
    /// <param name="newCallOffQuantity"></param>
    /// <returns></returns>
    public bool UpdateLinked(string connectionStringName,
                            int callOffDetailId,
                            Decimal newCallOffQuantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Linked_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "callOffDetailId", DbType.Int32, callOffDetailId);
        db.AddInParameter(dbCommand, "newCallOffQuantity", DbType.Decimal, newCallOffQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateLinked"

    #region UpdateUnlinked
    /// <summary>
    /// Link a specific quantity of Purchase Order Line to Call Off
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="purchaseOrderDetailId"></param>
    /// <param name="newCallOffQuantity"></param>
    /// <returns></returns>
    public bool UpdateUnlinked(string connectionStringName,
                               int callOffHeaderId,
                               int purchaseOrderDetailId,
                               Decimal newCallOffQuantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Unlinked_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);
        db.AddInParameter(dbCommand, "purchaseOrderDetailId", DbType.Int32, purchaseOrderDetailId);
        db.AddInParameter(dbCommand, "newCallOffQuantity", DbType.Decimal, newCallOffQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateUnlinked"

    #region SearchOrderQuery
    /// <summary>
    /// Retrieves a list of call off orders and lines
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="callOffNumber"></param>
    /// <param name="orderNumber"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchOrderQuery(string connectionStringName,
                                    int warehouseId,
                                    string callOffNumber,
                                    string orderNumber,
                                    string productCode,
                                    string product,
                                    string externalCompanyCode,
                                    string externalCompany,
                                    DateTime fromDate,
                                    DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Query_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "callOffNumber", DbType.String, callOffNumber);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "externalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "externalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrderQuery"

    #region UpdateOrderQuery
    public bool UpdateOrderQuery(string connectionStringName,
                                    int callOffHeaderId,
                                    int callOffDetailId,
                                    Decimal requestQuantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Query_Update";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);
        db.AddInParameter(dbCommand, "callOffDetailId", DbType.Int32, callOffDetailId);
        db.AddInParameter(dbCommand, "requestQuantity", DbType.Decimal, requestQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrderQuery"

    #region SearchQueryOrderLine
    /// <summary>
    /// Search for list of call off lines linked to specific header
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SearchQueryOrderLine(string connectionStringName,
                                int callOffHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Query_Line_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchQueryOrderLine"

    #region UpdateOrderLineStatus
    public bool UpdateOrderLineStatus(string connectionStringName,
                                      int callOffHeaderId,
                                      int callOffDetailId,
                                      string statusCode)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Query_Line_Update";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);
        db.AddInParameter(dbCommand, "callOffDetailId", DbType.Int32, callOffDetailId);
        db.AddInParameter(dbCommand, "statusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrderLineStatus"

    #region SearchPOQuery
    /// <summary>
    /// Retrieves a list of call off orders and lines
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="callOffNumber"></param>
    /// <param name="orderNumber"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchPOQuery(string connectionStringName,
                                int warehouseId,
                                string orderNumber,
                                string productCode,
                                string product,
                                string externalCompanyCode,
                                string externalCompany,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_POQuery_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPOQuery"

    #region SelectPOQuery
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SelectPOQuery(string connectionStringName,
                                int purchaseOrderHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_POQuery_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "purchaseOrderHeaderId", DbType.Int32, purchaseOrderHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectPOQuery"

    #region SearchPOQueryLine
    /// <summary>
    /// Search for list of call off lines linked to specific header
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SearchPOQueryLine(string connectionStringName,
                                int purchaseOrderHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_POQuery_Line_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "purchaseOrderHeaderId", DbType.Int32, purchaseOrderHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPOQueryLine"

    #region UpdatePOQueryLine
    public bool UpdatePOQueryLine(string connectionStringName,
                                      int purchaseOrderDetailId,
                                      Decimal requestQuantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_POQuery_Line_Update";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "purchaseOrderDetailId", DbType.Int32, purchaseOrderDetailId);
        db.AddInParameter(dbCommand, "requestQuantity", DbType.Int32, requestQuantity);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdatePOQueryLine

    #region GetTransportModes
    /// <summary>
    /// Retrieves a list of Transport Modes
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetTransportModes(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Parameter";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetTransportModes"

    #region GetNotificationMethods
    /// <summary>
    /// Retrieves a list of Notification Types
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetNotificationMethods(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Parameter";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetNotificationMethods"

    #region GetParticipants
    /// <summary>
    /// Retrieves a list of Participants
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetParticipants(string connectionStringName)
    {
        string externalCompanyTypeCode = "PART";

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Search_Type";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "externalCompanyTypeCode", DbType.String, externalCompanyTypeCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetParticipants"

    #region SaveFile
    /// <summary>
    /// Saves a file to the database
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="savedName"></param>
    /// <param name="savedData"></param>
    /// <returns></returns>
    public int SaveFile(string connectionStringName,
                         string fileName,
                         string extension,
                         byte[] data,
                         int uploadTableId,
                         string uploadTableName,
                         string uploadFileType)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_File_Save";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Return the correct id for the new record
        db.AddOutParameter(dbCommand, "SavedFileId", DbType.Int32, result);

        db.AddInParameter(dbCommand, "savedFile", DbType.String, fileName);
        db.AddInParameter(dbCommand, "extention", DbType.String, extension);
        db.AddInParameter(dbCommand, "data", DbType.Binary, data);
        db.AddInParameter(dbCommand, "uploadTableId", DbType.Int32, uploadTableId);
        db.AddInParameter(dbCommand, "uploadTableName", DbType.String, uploadTableName);
        db.AddInParameter(dbCommand, "uploadFileType", DbType.String, uploadFileType);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = (int)db.GetParameterValue(dbCommand, "@SavedFileId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrder"

    #region GetFileName
    /// <summary>
    /// Gets a file's name from it's id
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="savedFileId"></param>
    /// <returns></returns>
    public string GetFileName(string connectionStringName,
                         int savedFileId)
    {
        string result = "-1";

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_File_Get_Name";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Return the correct id for the new record
        db.AddInParameter(dbCommand, "SavedFileId", DbType.Int32, savedFileId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                result = (string)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "GetFileName"

    #region GetFileExtension
    /// <summary>
    /// Gets a file's extension from it's id
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="savedFileId"></param>
    /// <returns></returns>
    public string GetFileExtension(string connectionStringName,
                         int savedFileId)
    {
        string result = "-1";

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_File_Get_Extension";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Return the correct id for the new record
        db.AddInParameter(dbCommand, "SavedFileId", DbType.Int32, savedFileId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                result = (string)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "GetFileExtension"

    #region GetFileData
    /// <summary>
    /// Gets a file's data from it's id
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="savedFileId"></param>
    /// <returns></returns>
    public byte[] GetFileData(string connectionStringName,
                         int savedFileId)
    {
        byte[] result = null;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_File_Get_Data";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //Return the correct id for the new record
        db.AddInParameter(dbCommand, "SavedFileId", DbType.Int32, savedFileId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                result = (byte[])db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "GetFileData"

    #region SelectComment
    /// <summary>
    /// Retrieves comments linked to a specific CallOffHeaderId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SelectComment(string connectionStringName,
                                  int callOffHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Comment_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectComment"

    #region InsertComment
    /// <summary>
    /// Insert a comment for a specific CallOffHeaderId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <param name="comment"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool InsertComment(string connectionStringName,
                              int callOffHeaderId,
                              string comment,
                              int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Comment_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);
        db.AddInParameter(dbCommand, "comment", DbType.String, comment);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "InsertComment"

    #region UpdateComment
    /// <summary>
    /// Update a comment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="commentId"></param>
    /// <param name="comment"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool UpdateComment(string connectionStringName,
                              int commentId,
                              string comment,
                              int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Comment_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "commentId", DbType.Int32, commentId);
        db.AddInParameter(dbCommand, "comment", DbType.String, comment);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateComment"

    #region DeleteComment
    /// <summary>
    /// Delete a comment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="commentId"></param>
    /// <returns></returns>
    public bool DeleteComment(string connectionStringName,
                              int commentId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Comment_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "commentId", DbType.Int32, commentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteComment"

    #region SelectFiles
    /// <summary>
    /// Retrieves comments linked to a specific CallOffHeaderId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SelectFiles(string connectionStringName,
                                  int callOffSequenceId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_File_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffSequenceId", DbType.Int32, callOffSequenceId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    /// <summary>
    /// Retrieves comments linked to a specific Linking table and Id
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffSequenceId"></param>
    /// <returns></returns>
    public DataSet SelectFiles(string connectionStringName,
                                int uploadTableId,
                                string uploadTableName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_File_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "uploadTableId", DbType.Int32, uploadTableId);
        db.AddInParameter(dbCommand, "uploadTableName", DbType.String, uploadTableName);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectFiles"

    #region SelectSequence
    /// <summary>
    /// Retrieves comments linked to a specific CallOffHeaderId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SelectSequence(string connectionStringName,
                                  int callOffHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Sequence_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectSequence"

    #region InsertSequence
    /// <summary>
    /// Insert a CallOffSequence
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffSequenceId"></param>
    /// <param name="callOffHeaderId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="transportModeId"></param>
    /// <param name="notificationMethodId"></param>
    /// <param name="collectionDate"></param>
    /// <param name="receivedDate"></param>
    /// <param name="despatchDate"></param>
    /// <param name="plannedDelivery"></param>
    /// <param name="remarks"></param>
    /// <returns></returns>
    public bool InsertSequence(string connectionStringName,
                                int callOffSequenceId,
                                int callOffHeaderId,
                                int externalCompanyId,
                                int transportModeId,
                                int notificationMethodId,
                                DateTime collectionDate,
                                DateTime receivedDate,
                                DateTime despatchDate,
                                DateTime plannedDelivery,
                                string remarks)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Sequence_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "callOffSequenceId", DbType.Int32, callOffSequenceId);
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);
        db.AddInParameter(dbCommand, "externalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "transportModeId", DbType.Int32, transportModeId);
        db.AddInParameter(dbCommand, "notificationMethodId", DbType.Int32, notificationMethodId);
        db.AddInParameter(dbCommand, "collectionDate", DbType.DateTime, collectionDate);
        db.AddInParameter(dbCommand, "receivedDate", DbType.DateTime, receivedDate);
        db.AddInParameter(dbCommand, "despatchDate", DbType.DateTime, despatchDate);
        db.AddInParameter(dbCommand, "plannedDelivery", DbType.DateTime, plannedDelivery);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "InsertSequence"

    #region UpdateSequence
    /// <summary>
    /// Update a CallOffSequence
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffSequenceId"></param>
    /// <param name="callOffHeaderId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="transportModeId"></param>
    /// <param name="notificationMethodId"></param>
    /// <param name="collectionDate"></param>
    /// <param name="receivedDate"></param>
    /// <param name="despatchDate"></param>
    /// <param name="plannedDelivery"></param>
    /// <param name="remarks"></param>
    /// <returns></returns>
    public bool UpdateSequence(string connectionStringName,
                                int callOffSequenceId,
                                int callOffHeaderId,
                                int externalCompanyId,
                                int transportModeId,
                                int notificationMethodId,
                                DateTime collectionDate,
                                DateTime receivedDate,
                                DateTime despatchDate,
                                DateTime plannedDelivery,
                                string remarks)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Sequence_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "callOffSequenceId", DbType.Int32, callOffSequenceId);
        db.AddInParameter(dbCommand, "callOffHeaderId", DbType.Int32, callOffHeaderId);
        db.AddInParameter(dbCommand, "externalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "transportModeId", DbType.Int32, transportModeId);
        db.AddInParameter(dbCommand, "notificationMethodId", DbType.Int32, notificationMethodId);
        db.AddInParameter(dbCommand, "collectionDate", DbType.DateTime, collectionDate);
        db.AddInParameter(dbCommand, "receivedDate", DbType.DateTime, receivedDate);
        db.AddInParameter(dbCommand, "despatchDate", DbType.DateTime, despatchDate);
        db.AddInParameter(dbCommand, "plannedDelivery", DbType.DateTime, plannedDelivery);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateSequence"

    #region DeleteSequence
    /// <summary>
    /// Delete a CallOffSequence
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffSequenceId"></param>
    /// <returns></returns>
    public bool DeleteSequence(string connectionStringName,
                              int callOffSequenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_CallOff_Maintenance_Sequence_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "callOffSequenceId", DbType.Int32, callOffSequenceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteSequence"

    #region SearchDeliveryAdvice
    /// <summary>
    /// Retrieves a list of call off orders and lines
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="callOffNumber"></param>
    /// <param name="orderNumber"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchDeliveryAdvice(string connectionStringName,
                                    int warehouseId,
                                    string callOffNumber,
                                    string orderNumber,
                                    string productCode,
                                    string product,
                                    string externalCompanyCode,
                                    string externalCompany,
                                    DateTime fromDate,
                                    DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Buyer_DeliveryAdvice_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "callOffNumber", DbType.String, callOffNumber);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "externalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "externalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchDeliveryAdvice"

    #region UpdateDeliveryAdvice
    public bool UpdateDeliveryAdvice(string connectionStringName,
                                     int deliveryAdviceHeaderId,
                                     string deliveryNoteNumber,
                                     DateTime receivedDate,
                                     string sealNumber)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Buyer_DeliveryAdvice_Update";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "deliveryAdviceHeaderId", DbType.Int32, deliveryAdviceHeaderId);
        db.AddInParameter(dbCommand, "deliveryNoteNumber", DbType.String, deliveryNoteNumber);
        db.AddInParameter(dbCommand, "receivedDate", DbType.DateTime, receivedDate);
        db.AddInParameter(dbCommand, "sealNumber", DbType.String, sealNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateDeliveryAdvice"

    #region UpdateDeliveryAdviceLine
    public bool UpdateDeliveryAdviceLine(string connectionStringName,
                                     int deliveryAdviceDetailId,
                                     Decimal receivedQuantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Buyer_DeliveryAdvice_Line_Update";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "deliveryAdviceDetailId", DbType.Int32, deliveryAdviceDetailId);
        db.AddInParameter(dbCommand, "receivedQuantity", DbType.String, receivedQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateDeliveryAdviceLine"

    #region DefaultDeliveryAdviceQuantities
    public bool DefaultDeliveryAdviceQuantities(string connectionStringName,
                                                int deliveryAdviceHeaderId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Buyer_DeliveryAdvice_Default_Quantities";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "deliveryAdviceHeaderId", DbType.Int32, deliveryAdviceHeaderId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DefaultDeliveryAdviceQuantities"

    #region SelectDeliveryAdvice
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SelectDeliveryAdvice(string connectionStringName,
                                        int deliveryAdviceHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Buyer_DeliveryAdvice_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "deliveryAdviceHeaderId", DbType.Int32, deliveryAdviceHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectDeliveryAdvice"

    #region SearchDeliveryAdviceLine
    /// <summary>
    /// Search for list of call off lines linked to specific header
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SearchDeliveryAdviceLine(string connectionStringName,
                                            int deliveryAdviceHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Buyer_DeliveryAdvice_Line_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "deliveryAdviceHeaderId", DbType.Int32, deliveryAdviceHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchDeliveryAdviceLine"

    #region PalletiseDeliveryAdvice
    public bool PalletiseDeliveryAdvice(string connectionStringName,
                                        int deliveryAdviceDetailId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_DeliveryAdvice_Palletise";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "deliveryAdviceDetailId", DbType.Int32, deliveryAdviceDetailId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "PalletiseDeliveryAdvice"
}
