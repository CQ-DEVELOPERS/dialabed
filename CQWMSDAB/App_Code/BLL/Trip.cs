using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   20 July 2007
/// Summary description for OutboundShipment
/// </summary>
public class Trip
{
    #region Constructor Logic
    public int waveId;

    public Trip()
    {
        waveId = -1;
    }
    #endregion "Constructor Logic"

    #region SearchTrip
    /// <summary>
    /// Trip Search
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="trip"></param>
    /// <param name="statusId"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchTrip(string connectionStringName,
                                                Int32 warehouseId,
                                                String trip,
                                                Int32 statusId,
                                                DateTime fromDate,
                                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "trip", DbType.String, trip);
        db.AddInParameter(dbCommand, "statusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchTrip
    
    #region InsertTrip
    public bool InsertTrip(string connectionStringName, Int32 warehouseId, Int32 tripId, String trip, Int32 statusId, Int32 routeId, Int32 driverId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "tripId", DbType.Int32, tripId);
        db.AddInParameter(dbCommand, "trip", DbType.String, trip);
        db.AddInParameter(dbCommand, "statusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "routeId", DbType.Int32, routeId);
        db.AddInParameter(dbCommand, "driverId", DbType.Int32, driverId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    public int InsertTrip(string connectionStringName, Int32 warehouseId)
    {
        int result = -1;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddOutParameter(dbCommand, "TripId", DbType.Int32, result);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = (int)db.GetParameterValue(dbCommand, "@TripId");
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion InsertTrip

    #region UpdateTrip
    public bool UpdateTrip(string connectionStringName, Int32 warehouseId, Int32 tripId, String trip, Int32 statusId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "tripId", DbType.Int32, tripId);
        db.AddInParameter(dbCommand, "trip", DbType.String, trip);
        db.AddInParameter(dbCommand, "statusId", DbType.Int32, statusId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion UpdateTrip

    #region DeleteTrip
    public bool DeleteTrip(String connectionStringName, Int32 tripId, String trip, Int32 statusId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "tripId", DbType.Int32, tripId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion DeleteTrip

    #region GetLinkedIssues
    public DataSet GetLinkedIssues(string connectionStringName, int tripId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Linked_Issue";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "tripId", DbType.Int32, tripId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetLinkedIssues

    #region GetUnlinkedIssues
    public DataSet GetUnlinkedIssues(string connectionStringName,
                                        int WarehouseId,
                                        int OutboundDocumentTypeId,
                                        string OrderNumber,
                                        string ExternalCompanyCode,
                                        string ExternalCompany,
                                        DateTime FromDate,
                                        DateTime ToDate,
                                        int PrincipalId,
                                        string showOrders)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Unlinked_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ShowOrders", DbType.String, showOrders);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetUnlinkedIssues"

    #region GetLoadDetails
    public DataSet GetLoadDetails(string connectionStringName, int tripId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Load_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "tripId", DbType.Int32, tripId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetLoadDetails"

    #region GetDeliveryAddresses
    public DataSet GetDeliveryAddresses(string connectionStringName, int tripVisitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Delivery_Addresses";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "tripVisitId", DbType.Int32, tripVisitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetDeliveryAddresses"

    #region UpdateLoadDetails
    public bool UpdateLoadDetails(string connectionStringName, int tripId, int tripVisitId, int dropSequence, int processId, bool trustedDelivery)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Load_Details_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "tripId", DbType.Int32, tripId);
        db.AddInParameter(dbCommand, "tripVisitId", DbType.Int32, tripVisitId);
        db.AddInParameter(dbCommand, "dropSequence", DbType.Int32, dropSequence);
        db.AddInParameter(dbCommand, "processId", DbType.Int32, processId);
        db.AddInParameter(dbCommand, "trustedDelivery", DbType.Int32, trustedDelivery);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateLoadDetails"

    #region Transfer
    public bool Transfer(string connectionStringName, int tripId, int tripVisitId, int outboundShipmentId, int issueId, int inboundShipmentId, int receiptId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Link";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "tripId", DbType.Int32, tripId);
        db.AddInParameter(dbCommand, "tripVisitId", DbType.Int32, tripVisitId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Transfer"

    #region Remove
    public bool Remove(string connectionStringName, int tripVisitDetailId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Unlink";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "tripVisitDetailId", DbType.Int32, tripVisitDetailId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Remove"

    #region GetOutstandingTrips
    public DataSet GetOutstandingTrips(string connectionStringName,
                                        int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Outstanding";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        
        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetOutstandingTrips

    #region ReleaseTrip
    public bool ReleaseTrip(string connectionStringName, int tripId, int operatorId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Trip_Planning_Release";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "tripId", DbType.Int32, tripId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "ReleaseTrip"

    #region GetProcesses
    public DataSet GetProcesses(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Process_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetProcesses"
}
