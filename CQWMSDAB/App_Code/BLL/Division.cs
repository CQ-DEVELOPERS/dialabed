﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Division
/// </summary>
public class Division
{
    #region Constructor Logic
    public Division()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion Constructor Logic

    #region GetDivisions
    /// <summary>
    /// Gets a list of Divisions
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetDivisions(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Division_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        //db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, DivisionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetDivisions
}
