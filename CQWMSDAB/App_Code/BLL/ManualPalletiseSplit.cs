using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class ManualPalletiseSplit
{
    #region Constructor Logic
    public ManualPalletiseSplit()
    {
    }
    #endregion Constructor Logic

    #region GetInstructions
    /// <summary>
    /// Gets outstanding Store instructions within a date range.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet GetInstructions(string connectionStringName, DateTime fromDate, DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Store_Instruction_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructions

    #region GetInstructionToSplit
    /// <summary>
    /// Gets instruction which will be split
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public DataSet GetInstructionToSplit(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Putaway_Palletisation_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructionToSplit

    #region GetInstructions
    /// <summary>
    /// Gets instructions linked to a Job.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public DataSet GetInstructions(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Putaway_Palletisation_Create_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructions

    #region AutoLocationAllocate
    /// <summary>
    /// Allocates a Location to an Instruction (Automatically).
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <returns></returns>
    public bool AutoLocationAllocate(string connectionStringName, int InstructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);

        try
        {
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();

                try
                {
                    // Execute Query
                    db.ExecuteNonQuery(dbCommand);

                    result = true;
                }
                catch { }

                connection.Close();
            }
        }
        catch { }  
        
        return result;
    }
    #endregion AutoLocationAllocate

    #region CreateLines
    /// <summary>
    /// Creates X number of new Instructions
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="numberOfLines"></param>
    /// <returns></returns>
    public bool CreateLines(string connectionStringName, int instructionId, int numberOfLines)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Putaway_Palletisation_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "NumberOfLines", DbType.Int32, numberOfLines);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();
            
            return result;
        }
    }
    #endregion CreateLines

    #region SetQuantityFieldSplit
    /// <summary>
    /// Updates Instructions Quantity
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public int SetQuantityFieldSplit(string connectionStringName, int instructionId, Decimal quantity)
    {
        int splitTotal = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Putaway_Palletisation_Create_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                splitTotal = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();
            
            return splitTotal;
        }
    }
    #endregion SetQuantityFieldSplit

    #region GetSplitTotals
    /// <summary>
    /// Gets the Totals
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public DataSet GetSplitTotals(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Putaway_Palletisation_Create_SplitTotals";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetSplitTotals
}
