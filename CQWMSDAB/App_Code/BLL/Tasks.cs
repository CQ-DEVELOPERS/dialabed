using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   5 Mar 2012
/// Summary description for Planning
/// </summary>
public class Tasks
{
    #region Constructor Logic
    public Tasks()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region GetTaskList
    /// <summary>
    ///   Selects rows from the TaskList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TaskListId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetTaskList
    (
        string connectionStringName,
        Int32 TaskListId
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskList_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetTaskList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskList_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetTaskList

    #region DeleteTaskList
    /// <summary>
    ///   Deletes a row from the TaskList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TaskListId"></param>
    /// <returns>bool</returns>
    public bool DeleteTaskList
    (
        string connectionStringName,
        Int32 TaskListId
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskList_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeleteTaskList

    #region UpdateTaskList
    /// <summary>
    ///   Updates a row in the TaskList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TaskListId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="ProjectId"></param>
    /// <param name="RequestTypeId"></param>
    /// <param name="BillableId"></param>
    /// <param name="TaskList"></param>
    /// <param name="Comments"></param>
    /// <param name="RaisedBy"></param>
    /// <param name="ApprovedBy"></param>
    /// <param name="AllocatedTo"></param>
    /// <param name="Tester"></param>
    /// <param name="EstimateHours"></param>
    /// <param name="ActualHours"></param>
    /// <param name="CreateDate"></param>
    /// <param name="TargetDate"></param>
    /// <param name="Percentage"></param>
    /// <param name="CycleId"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool UpdateTaskList
    (
        string connectionStringName,
        Int32 TaskListId,
        Int32 PriorityId,
        Int32 ProjectId,
        Int32 RequestTypeId,
        Int32 BillableId,
        String TaskList,
        String Comments,
        Int32 RaisedBy,
        Int32 ApprovedBy,
        Int32 AllocatedTo,
        Int32 Tester,
        Double EstimateHours,
        Double ActualHours,
        DateTime CreateDate,
        DateTime TargetDate,
        Double Percentage,
        Int32 CycleId,
        Int32 OrderBy
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskList_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
        db.AddInParameter(dbCommand, "TaskList", DbType.String, TaskList);
        db.AddInParameter(dbCommand, "Comments", DbType.String, Comments);
        db.AddInParameter(dbCommand, "RaisedBy", DbType.Int32, RaisedBy);
        db.AddInParameter(dbCommand, "ApprovedBy", DbType.Int32, ApprovedBy);
        db.AddInParameter(dbCommand, "AllocatedTo", DbType.Int32, AllocatedTo);
        db.AddInParameter(dbCommand, "Tester", DbType.Int32, Tester);
        db.AddInParameter(dbCommand, "EstimateHours", DbType.Double, EstimateHours);
        db.AddInParameter(dbCommand, "ActualHours", DbType.Double, ActualHours);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "TargetDate", DbType.DateTime, TargetDate);
        db.AddInParameter(dbCommand, "Percentage", DbType.Double, Percentage);
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, OrderBy);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateTaskList

    #region InsertTaskList
    /// <summary>
    ///   Inserts a row into the TaskList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TaskListId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="ProjectId"></param>
    /// <param name="RequestTypeId"></param>
    /// <param name="BillableId"></param>
    /// <param name="TaskList"></param>
    /// <param name="Comments"></param>
    /// <param name="RaisedBy"></param>
    /// <param name="ApprovedBy"></param>
    /// <param name="AllocatedTo"></param>
    /// <param name="Tester"></param>
    /// <param name="EstimateHours"></param>
    /// <param name="ActualHours"></param>
    /// <param name="CreateDate"></param>
    /// <param name="TargetDate"></param>
    /// <param name="Percentage"></param>
    /// <param name="CycleId"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool InsertTaskList
    (
        string connectionStringName,
        Int32 TaskListId,
        Int32 PriorityId,
        Int32 ProjectId,
        Int32 RequestTypeId,
        Int32 BillableId,
        String TaskList,
        String Comments,
        Int32 RaisedBy,
        Int32 ApprovedBy,
        Int32 AllocatedTo,
        Int32 Tester,
        Double EstimateHours,
        Double ActualHours,
        DateTime CreateDate,
        DateTime TargetDate,
        Double Percentage,
        Int32 CycleId,
        Int32 OrderBy
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskList_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
        db.AddInParameter(dbCommand, "TaskList", DbType.String, TaskList);
        db.AddInParameter(dbCommand, "Comments", DbType.String, Comments);
        db.AddInParameter(dbCommand, "RaisedBy", DbType.Int32, RaisedBy);
        db.AddInParameter(dbCommand, "ApprovedBy", DbType.Int32, ApprovedBy);
        db.AddInParameter(dbCommand, "AllocatedTo", DbType.Int32, AllocatedTo);
        db.AddInParameter(dbCommand, "Tester", DbType.Int32, Tester);
        db.AddInParameter(dbCommand, "EstimateHours", DbType.Double, EstimateHours);
        db.AddInParameter(dbCommand, "ActualHours", DbType.Double, ActualHours);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "TargetDate", DbType.DateTime, TargetDate);
        db.AddInParameter(dbCommand, "Percentage", DbType.Double, Percentage);
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, OrderBy);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InsertTaskList

    #region SearchTaskList
    /// <summary>
    ///   Selects rows from the TaskList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TaskListId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="ProjectId"></param>
    /// <param name="RequestTypeId"></param>
    /// <param name="BillableId"></param>
    /// <param name="TaskList"></param>
    /// <param name="RaisedBy"></param>
    /// <param name="ApprovedBy"></param>
    /// <param name="AllocatedTo"></param>
    /// <param name="Tester"></param>
    /// <param name="CycleId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchTaskList
    (
        string connectionStringName,
        Int32 TaskListId,
        Int32 PriorityId,
        Int32 ProjectId,
        Int32 RequestTypeId,
        Int32 BillableId,
        String TaskList,
        Int32 RaisedBy,
        Int32 ApprovedBy,
        Int32 AllocatedTo,
        Int32 Tester,
        Int32 CycleId,
        Decimal fromPercent,
        Decimal toPercent,
        DateTime fromDate,
        DateTime toDate
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskMaster_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
        db.AddInParameter(dbCommand, "TaskList", DbType.String, TaskList);
        db.AddInParameter(dbCommand, "RaisedBy", DbType.Int32, RaisedBy);
        db.AddInParameter(dbCommand, "ApprovedBy", DbType.Int32, ApprovedBy);
        db.AddInParameter(dbCommand, "AllocatedTo", DbType.Int32, AllocatedTo);
        db.AddInParameter(dbCommand, "Tester", DbType.Int32, Tester);
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
        db.AddInParameter(dbCommand, "fromPercent", DbType.Decimal, fromPercent);
        db.AddInParameter(dbCommand, "toPercent", DbType.Decimal, toPercent);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchTaskList

    #region PageSearchTaskList
    /// <summary>
    ///   Selects rows from the TaskList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TaskListId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="ProjectId"></param>
    /// <param name="RequestTypeId"></param>
    /// <param name="BillableId"></param>
    /// <param name="TaskList"></param>
    /// <param name="RaisedBy"></param>
    /// <param name="ApprovedBy"></param>
    /// <param name="AllocatedTo"></param>
    /// <param name="Tester"></param>
    /// <param name="CycleId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchTaskList
    (
        string connectionStringName,
        Int32 TaskListId,
        Int32 PriorityId,
        Int32 ProjectId,
        Int32 RequestTypeId,
        Int32 BillableId,
        String TaskList,
        Int32 RaisedBy,
        Int32 ApprovedBy,
        Int32 AllocatedTo,
        Int32 Tester,
        Int32 CycleId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskList_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "TaskListId", DbType.Int32, TaskListId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
        db.AddInParameter(dbCommand, "RequestTypeId", DbType.Int32, RequestTypeId);
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
        db.AddInParameter(dbCommand, "TaskList", DbType.String, TaskList);
        db.AddInParameter(dbCommand, "RaisedBy", DbType.Int32, RaisedBy);
        db.AddInParameter(dbCommand, "ApprovedBy", DbType.Int32, ApprovedBy);
        db.AddInParameter(dbCommand, "AllocatedTo", DbType.Int32, AllocatedTo);
        db.AddInParameter(dbCommand, "Tester", DbType.Int32, Tester);
        db.AddInParameter(dbCommand, "CycleId", DbType.Int32, CycleId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion PageSearchTaskList

    #region ListTaskList
    /// <summary>
    ///   Lists rows from the TaskList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListTaskList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskList_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListTaskList

    #region ParameterTaskList
    /// <summary>
    ///   Lists rows from the TaskList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterTaskList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_TaskList_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ParameterTaskList
}
