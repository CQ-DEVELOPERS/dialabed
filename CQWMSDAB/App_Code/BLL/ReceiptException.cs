﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Karen Sucksmith
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class ReceiptException
{
    #region Constructor logic
    public ReceiptException()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor logic"

    public DataSet GetReceiptExceptions(string connectionStringName, int ReceiptLineID)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        string sqlCommand = connectionStringName + ".dbo.p_Station_Sheet_Exception_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        DataSet dataSet = null;

        db.AddInParameter(dbCommand, "receiptLineID", DbType.Int32, ReceiptLineID);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {

                dataSet = db.ExecuteDataSet(dbCommand);


            }
            catch { }

            connection.Close();

        }
        return dataSet;
    }

    public void InsertReceiptException(
        string connectionStringName,
        int ReceiptLineID,
        int InstructionId,
        int IssueLineId,
        int ReasonId,
        string Exception,
        string ExceptionCode,
        DateTime CreateDate,
        int OperatorId,
        DateTime ExceptionDate,
        int JobId,
        string Detail,
        Decimal Quantity)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        bool result = false;
        string sqlCommand = connectionStringName + ".dbo.p_Station_Sheet_Exception_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineID", DbType.Int32, ReceiptLineID);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, DBNull.Value);
        db.AddInParameter(dbCommand, "issueLineId", DbType.Int32, DBNull.Value);
        db.AddInParameter(dbCommand, "reasonId", DbType.Int32, DBNull.Value);
        db.AddInParameter(dbCommand, "exception", DbType.String, Exception);
        db.AddInParameter(dbCommand, "exceptionCode", DbType.String, ExceptionCode);
        db.AddInParameter(dbCommand, "createDate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, DBNull.Value);
        db.AddInParameter(dbCommand, "exceptionDate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, DBNull.Value);
        db.AddInParameter(dbCommand, "detail", DbType.String, Detail);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, Quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            connection.Close();

        }
    }


    public int UpdateReceiptException(string connectionStringName,
                                int ExceptionID,
                                int ReceiptLineID,
                                Decimal Quantity,
                                string Exception)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Station_Sheet_Exception_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "exceptionID", DbType.Int32, ExceptionID);
        db.AddInParameter(dbCommand, "receiptLineID", DbType.Int32, ReceiptLineID);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "exception", DbType.String, Exception);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    public bool DeleteReceiptException(string connectionStringName, int exceptionId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Station_Sheet_Exception_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "exceptionId", DbType.Int32, exceptionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
}
