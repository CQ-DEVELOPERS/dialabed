using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   16 Oct 2007
/// Summary description for Planning
/// </summary>
public class ProductionPlan
{
    #region Constructor Logic
    public ProductionPlan()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region Select
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    public DataSet Select(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Plan_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        
        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "Select"

    #region Update
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueId"></param>
    /// <param name="dropSequence"></param>
    /// <returns></returns>
    public bool Update(String connectionStringName, int rank, int issueId, int dropSequence, string orderNumber, DateTime deliveryDate, String batch)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Plan_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "dropSequence", DbType.Int32, dropSequence);
        db.AddInParameter(dbCommand, "rank", DbType.Int32, rank);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public bool Update(String connectionStringName, int issueId, int rank)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Plan_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "rank", DbType.Int32, rank);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Update"

    #region SearchOrders
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchOrders(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int locationId,
                                string orderNumber,
                                string productCode,
                                string product,
                                string batch,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Planning_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrders"

    #region UpdateOrder
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrder(String connectionStringName,
                            int outboundShipmentId,
                            int issueId,
                            int rank,
                            DateTime deliveryDate,
                            String batch,
                            int locationId,
                            int potId,
                            int priorityId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Planning_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "deliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "potId", DbType.Int32, potId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, priorityId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrder"
}
