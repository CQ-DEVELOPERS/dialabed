using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Reflection;

/// <summary>
/// Author: Grant Schultz
/// Date:   6 July 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class InboundDocument
{
    #region Constructor Logic
    int _inboundDocumentId = -1;

    public InboundDocument()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region CreateInboundDocument
    /// <summary>
    /// Inserts an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    public int CreateInboundDocument(string connectionStringName,
                                     int inboundDocumentTypeId,
                                     int externalCompanyId,
                                     int warehouseId,
                                     string orderNumber,
                                     DateTime deliveryDate,
                                     int contactListId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, contactListId);

        //enter a default id, so we can return the correct id for the new record
        db.AddOutParameter(dbCommand, "InboundDocumentId", DbType.Int32, _inboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                _inboundDocumentId = (int)db.GetParameterValue(dbCommand, "@InboundDocumentId");

                result = _inboundDocumentId;
            }
            catch
            {
                throw;
            }
            connection.Close();

            return result;
        }
    }

    /// <summary>
    /// Inserts an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    public int CreateInboundDocument(string connectionStringName,
                                     int inboundDocumentTypeId,
                                     int externalCompanyId,
                                     int warehouseId,
                                     string orderNumber,
                                     DateTime deliveryDate,
                                     string referenceNumber,
                                     int reasonId,
                                     string remarks,
                                     int contactListId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "reasonId", DbType.Int32, reasonId);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "ContactListId", DbType.String, contactListId);

        //enter a default id, so we can return the correct id for the new record
        db.AddOutParameter(dbCommand, "InboundDocumentId", DbType.Int32, _inboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                _inboundDocumentId = (int)db.GetParameterValue(dbCommand, "@InboundDocumentId");

                result = _inboundDocumentId;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion CreateInboundDocument

    //#region CreateInboundDocument
    ///// <summary>
    ///// Inserts an InboundDocument
    ///// </summary>
    ///// <param name="connectionStringName"></param>
    ///// <param name="inboundDocumentTypeId"></param>
    ///// <param name="externalCompanyId"></param>
    ///// <param name="warehouseId"></param>
    ///// <param name="orderNumber"></param>
    ///// <param name="deliveryDate"></param>
    ///// <returns></returns>
    //public int CreateInboundDocument(string connectionStringName,
    //                                 int principalId,
    //                                 int inboundDocumentTypeId,
    //                                 int externalCompanyId,
    //                                 int warehouseId,
    //                                 string orderNumber,
    //                                 DateTime deliveryDate,
    //                                 int contactListId)
    //{
    //    int result = 0;

    //    // Create the Database object, passing it the required database service.
    //    Database db = DatabaseFactory.CreateDatabase(connectionStringName);

    //    string sqlCommand = "p_InboundDocument_Create";
    //    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

    //    db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);
    //    db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
    //    db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
    //    db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
    //    db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
    //    db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
    //    db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, contactListId);

    //    //enter a default id, so we can return the correct id for the new record
    //    db.AddOutParameter(dbCommand, "InboundDocumentId", DbType.Int32, _inboundDocumentId);

    //    using (DbConnection connection = db.CreateConnection())
    //    {
    //        connection.Open();

    //        try
    //        {
    //            // Execute command
    //            db.ExecuteNonQuery(dbCommand);

    //            _inboundDocumentId = (int)db.GetParameterValue(dbCommand, "@InboundDocumentId");

    //            result = _inboundDocumentId;
    //        }
    //        catch
    //        {
    //        }
    //        connection.Close();

    //        return result;
    //    }
    //}
    ///// <summary>
    ///// Inserts an InboundDocument
    ///// </summary>
    ///// <param name="connectionStringName"></param>
    ///// <param name="inboundDocumentTypeId"></param>
    ///// <param name="externalCompanyId"></param>
    ///// <param name="warehouseId"></param>
    ///// <param name="orderNumber"></param>
    ///// <param name="deliveryDate"></param>
    ///// <returns></returns>
    //public int CreateInboundDocument(string connectionStringName,
    //                                int principalId,
    //                                 int inboundDocumentTypeId,
    //                                 int externalCompanyId,
    //                                 int warehouseId,
    //                                 string orderNumber,
    //                                 DateTime deliveryDate,
    //                                 string referenceNumber,
    //                                 int reasonId,
    //                                 string remarks,
    //                                 int contactListId)
    //{
    //    int result = 0;

    //    // Create the Database object, passing it the required database service.
    //    Database db = DatabaseFactory.CreateDatabase(connectionStringName);

    //    string sqlCommand = "p_InboundDocument_Create";
    //    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

    //    db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
    //    db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);
    //    db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
    //    db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
    //    db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
    //    db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
    //    db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
    //    db.AddInParameter(dbCommand, "reasonId", DbType.Int32, reasonId);
    //    db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);
    //    db.AddInParameter(dbCommand, "ContactListId", DbType.String, contactListId);

    //    //enter a default id, so we can return the correct id for the new record
    //    db.AddOutParameter(dbCommand, "InboundDocumentId", DbType.Int32, _inboundDocumentId);

    //    using (DbConnection connection = db.CreateConnection())
    //    {
    //        connection.Open();

    //        try
    //        {
    //            // Execute command
    //            db.ExecuteNonQuery(dbCommand);

    //            _inboundDocumentId = (int)db.GetParameterValue(dbCommand, "@InboundDocumentId");

    //            result = _inboundDocumentId;
    //        }
    //        catch
    //        {
    //        }
    //        connection.Close();

    //        return result;
    //    }
    //}
    //#endregion CreateInboundDocument

    #region SearchInboundDocument
    /// <summary>
    /// Searches for an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchInboundDocument(string connectionStringName,
                                            int inboundDocumentTypeId,
                                            string externalCompanyCode,
                                            string externalCompany,
                                            int warehouseId,
                                            string orderNumber,
                                            DateTime fromDate,
                                            DateTime toDate,
                                            int contactListId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Search_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.String, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, contactListId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    /// <summary>
    /// Searches for an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchInboundDocument(string connectionStringName,
                                            int inboundDocumentTypeId,
                                            string externalCompanyCode,
                                            string externalCompany,
                                            int warehouseId,
                                            string orderNumber,
                                            DateTime fromDate,
                                            DateTime toDate,
                                            string returnType,
                                            int contactListId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Search_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.String, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "returnType", DbType.String, returnType);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, contactListId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchInboundDocument"

    #region GetInboundDocument
    /// <summary>
    /// Retrieves a single inbound document record.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <returns></returns>
    public DataSet GetInboundDocument(string connectionStringName,
                                         int inboundDocumentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Search_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetInboundDocument"

    #region UpdateInboundDocument
    /// <summary>
    /// Updates an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    public bool UpdateInboundDocument(string connectionStringName,
                                        int inboundDocumentId,
                                        int inboundDocumentTypeId,
                                        int externalCompanyId,
                                        string orderNumber,
                                        DateTime deliveryDate,
                                        string referenceNumber,
                                        int reasonId,
                                        string remarks,
                                        int contactListId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Update_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "ReferenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, reasonId);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, contactListId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    #endregion "UpdateInboundDocument"

    #region UpdateInboundDocumentReason
    /// <summary>
    /// Updates an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    public bool UpdateInboundDocumentReason(string connectionStringName,
                                        int inboundDocumentId,
                                        int reasonId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Update_Reason";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, reasonId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateInboundDocumentReason"

    #region DeleteInboundDocument
    /// <summary>
    /// Deletes an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentId"></param>
    /// <returns></returns>
    public bool DeleteInboundDocument(string connectionStringName, int InboundDocumentId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Remove";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteInboundDocument"

    #region CreateInboundLine
    /// <summary>
    /// Inserts an InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="operatorId"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="quantity"></param>
    /// <param name="batchId"></param>
    /// <returns></returns>
    public bool CreateInboundLine(string connectionStringName,
                                    int operatorId,
                                    int inboundDocumentId,
                                    int storageUnitId,
                                    Decimal quantity,
                                    int batchId,
                                    int reasonId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundLine_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.String, operatorId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, reasonId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateInboundLine

    #region SearchInboundLine
    /// <summary>
    /// Retrieves and InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <returns></returns>
    public DataSet SearchInboundLine(string connectionStringName, int inboundDocumentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundLine_Create_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.String, inboundDocumentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchInboundLine

    #region SearchProductionLines
    /// <summary>
    /// Retrieves and InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <returns></returns>
    public DataSet SearchProductionLines(string connectionStringName, int storageUnitBatchId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Production_Line_Create_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.String, storageUnitBatchId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchProductionLines

    #region UpdateInboundLine
    /// <summary>
    /// Updates an InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundLineId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool UpdateInboundLine(string connectionStringName, int inboundLineId, Decimal quantity, decimal returnQuantity, decimal unitPrice, int reasonId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundLine_Edit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, inboundLineId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "ReturnQuantity", DbType.Decimal, returnQuantity);
        db.AddInParameter(dbCommand, "UnitPrice", DbType.Decimal, unitPrice);
        db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, reasonId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateInboundLine"

    //#region UpdateInboundLine
    ///// <summary>
    ///// Updates an InboundLine
    ///// </summary>
    ///// <param name="connectionStringName"></param>
    ///// <param name="inboundLineId"></param>
    ///// <param name="quantity"></param>
    ///// <returns></returns>
    //public bool UpdateInboundLine(string connectionStringName, int inboundLineId, Decimal quantity, decimal unitPrice, int reasonId)
    //{
    //    bool result = false;

    //    // Create the Database object, passing it the required database service.
    //    Database db = DatabaseFactory.CreateDatabase(connectionStringName);

    //    string sqlCommand = "p_InboundLine_Edit";
    //    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

    //    db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, inboundLineId);
    //    db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
    //    db.AddInParameter(dbCommand, "UnitPrice", DbType.Decimal, unitPrice);
    //    db.AddInParameter(dbCommand, "ReasonId", DbType.Int32, reasonId);

    //    using (DbConnection connection = db.CreateConnection())
    //    {
    //        connection.Open();

    //        try
    //        {
    //            // Execute command
    //            db.ExecuteNonQuery(dbCommand);

    //            result = true;
    //        }
    //        catch { }

    //        connection.Close();

    //        return result;
    //    }
    //}
    //#endregion "UpdateInboundLine"

    #region DeleteInboundLine
    /// <summary>
    /// Deletes an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundLineId"></param>
    /// <returns></returns>
    public bool DeleteInboundLine(string connectionStringName, int inboundLineId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundLine_Delete_Line";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, inboundLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteInboundLine"

    #region CreateReceipt
    /// <summary>
    /// Creates Receipt and ReceiptLine records from InboundDocument and InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool CreateReceipt(string connectionStringName, int inboundDocumentId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Receiving_Create_Receipt";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateReceipt"

    #region GetOrderNumber
    /// <summary>
    /// Retrieves a single inbound document record.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <returns></returns>
    public DataSet GetOrderNumber(string connectionStringName,
                                         int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Order_Number_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderNumber"

    #region CustomerReturnComplete
    /// <summary>
    /// Creates Receipt and ReceiptLine records from InboundDocument and InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool CustomerReturnComplete(string connectionStringName, int inboundDocumentId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Customer_Return_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CustomerReturnComplete"

    #region ValidInvoice
    public bool ValidInvoice(string connectionStringName, string referenceNumber)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Check_Valid_Invoice";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ValidInvoice

    #region SearchDivision
    /// <summary>
    /// Retrieves Dispatch
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DivisionCode"></param>
    /// <param name="Division"></param>
    /// <returns>Divisions</returns>
    public DataSet SearchDivision(string connectionStringName, string DivisionCode, string Division)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Division_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "DivisionCode", DbType.String, DivisionCode);
        db.AddInParameter(dbCommand, "Division", DbType.String, Division);

        //DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion

    #region CreateInboundLineFromInvoice
    /// <summary>
    /// Inserts an InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="operatorId"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="batchId"></param>
    /// <returns></returns>
    public bool CreateInboundLineFromInvoice(string connectionStringName,
                                    int operatorId,
                                    int inboundDocumentId,
                                    int referenceNumber)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundLine_CreateFromInvoice";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.String, operatorId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateInboundLineFromInvoice

    #region UpdateInboundDocumentandEmail
    /// <summary>
    /// Updates an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    public bool UpdateInboundDocumentandEmail(string connectionStringName,
                                        int inboundDocumentId,
                                        int inboundDocumentTypeId,
                                        int externalCompanyId,
                                        string orderNumber,
                                        DateTime deliveryDate,
                                        string referenceNumber,
                                        int contactListId,
                                        string email)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Update_Email";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "ReferenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, contactListId);
        db.AddInParameter(dbCommand, "Email", DbType.String, email);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    #endregion "UpdateInboundDocumentandEmail"

    #region CreateInboundDocumentandEmail
    /// <summary>
    /// Inserts an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    //public int CreateInboundDocumentandEmail(string connectionStringName,
    //                                 int warehouseId,
    //                                 int inboundDocumentTypeId,
    //                                 int externalCompanyId,                                     
    //                                 string orderNumber,
    //                                 DateTime deliveryDate,
    //                                 string referenceNumber,
    //                                 string contactPerson,
    //                                 string email,
    //                                 int divisionId,
    //                                 int contactListId)
    //{
    //    int result = 0;

    //    // Create the Database object, passing it the required database service.
    //    Database db = DatabaseFactory.CreateDatabase(connectionStringName);

    //    string sqlCommand = "p_InboundDocument_Create_Email";
    //    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
    //    db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
    //    db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
    //    db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);        
    //    db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
    //    db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
    //    db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
    //    db.AddInParameter(dbCommand, "contactPerson", DbType.String, contactPerson);
    //    db.AddInParameter(dbCommand, "Email", DbType.String, email);
    //    db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, divisionId);
    //    db.AddInParameter(dbCommand, "contactListId", DbType.Int32, contactListId);
        

    //    //enter a default id, so we can return the correct id for the new record
    //    db.AddOutParameter(dbCommand, "InboundDocumentId", DbType.Int32, _inboundDocumentId);

    //    using (DbConnection connection = db.CreateConnection())
    //    {
    //        connection.Open();

    //        try
    //        {
    //            // Execute command
    //            db.ExecuteNonQuery(dbCommand);

    //            _inboundDocumentId = (int)db.GetParameterValue(dbCommand, "@InboundDocumentId");

    //            result = _inboundDocumentId;
    //        }
    //        catch
    //        {
    //        }
    //        connection.Close();

    //        return result;
    //    }
    //}
    ///// <summary>
    ///// Inserts an InboundDocument
    ///// </summary>
    ///// <param name="connectionStringName"></param>
    ///// <param name="inboundDocumentTypeId"></param>
    ///// <param name="externalCompanyId"></param>
    ///// <param name="warehouseId"></param>
    ///// <param name="orderNumber"></param>
    ///// <param name="deliveryDate"></param>
    ///// <returns></returns>
    public int CreateInboundDocumentandEmail(string connectionStringName,
                                     int warehouseId,
                                     int inboundDocumentTypeId,
                                     int externalCompanyId,
                                     string orderNumber,
                                     DateTime deliveryDate,
                                     string referenceNumber,
                                     string contactPerson,
                                     string email,
                                     int divisionId,
                                     int contactListId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Create_Email";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, externalCompanyId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "contactPerson", DbType.String, contactPerson);
        db.AddInParameter(dbCommand, "Email", DbType.String, email);
        db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, divisionId);
        db.AddInParameter(dbCommand, "contactListId", DbType.Int32, contactListId);

        //enter a default id, so we can return the correct id for the new record
        db.AddOutParameter(dbCommand, "InboundDocumentId", DbType.Int32, _inboundDocumentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                _inboundDocumentId = (int)db.GetParameterValue(dbCommand, "@InboundDocumentId");

                sqlCommand = "p_InboundDocumentEmail_Insert";
                DbCommand dbCommand2 = db.GetStoredProcCommand(sqlCommand);

                db.AddInParameter(dbCommand2, "InboundDocumentId", DbType.Int32, (int)db.GetParameterValue(dbCommand, "@InboundDocumentId"));
                db.AddInParameter(dbCommand, "contactListId", DbType.Int32, contactListId);

                db.ExecuteNonQuery(dbCommand2);

                result = _inboundDocumentId;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion CreateInboundDocumentandEmail

    #region LinkInboundDocEmail
    /// <summary>
    /// Inserts an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyId"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    
    public static void LinkInboundDocEmail(string connectionStringName,int inboundDocumentId,int contactListId)
     {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocumentEmail_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, contactListId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = 1;
            }
            catch (Exception ex)
            {
                return;
            }
        }
        //connection.Close();

        return;
        }
    
//public static void LinkInboundDocEmail(string p,int p_2,object p_3)
//{
//    throw new NotImplementedException();
//}
    #endregion LinkInboundDocEmail

    #region InboundDocEmail_Delete
    public static void InboundDocEmail_Delete(string connectionStringName, int inboundDocumentId, int contactListId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocEmail_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "contactListId", DbType.Int32, contactListId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return;
            }
        }
        //connection.Close();

        return;
    }
    #endregion InboundDocEmail_Delete

    #region ReceiptComplete
    /// <summary>
    /// Creates Receipt and ReceiptLine records from InboundDocument and InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool ReceiptComplete(string connectionStringName, int inboundDocumentId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Receipt_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "ReceiptComplete"

    #region "SearchInboundDocumentLink"
    /// <summary>
    /// Searches for an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentTypeId"></param>
    /// <param name="externalCompanyCode"></param>
    /// <param name="externalCompany"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchInboundDocumentLink(string connectionStringName,
                                            int inboundDocumentTypeId,
                                            string division,
                                            string externalCompany,
                                            int warehouseId,
                                            string orderNumber,
                                            DateTime fromDate,
                                            DateTime toDate,
                                            string returnType,
                                            int contactListId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Link_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.String, inboundDocumentTypeId);
        db.AddInParameter(dbCommand, "Division", DbType.String, division);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "returnType", DbType.String, returnType);
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, contactListId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchInboundDocumentLink

    #region SearchInboundDocumentLinkedProducts
    public DataSet SearchInboundDocumentLinkedProducts(string connectionStringName,
                                                int WarehouseId,
                                                int InboundDocumentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_InboundDocument_Link_Products";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId); 
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.String, InboundDocumentId);
        
        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion
}

