﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ExceptionEventArgs
/// </summary>
public class ExceptionEventArgs
{

    public Exception Error { get; set; }

    public ExceptionEventArgs(Exception error)
    {
        this.Error = error;
    }
}
