﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MessageEventArgs
/// </summary>
public class MessageEventArgs
{

    public string Message { get; set; }

    public MessageEventArgs(string message)
    {
        this.Message = message;
    }
}
