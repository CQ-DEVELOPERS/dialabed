﻿//Global variable to hold NLWAXForm element
var WAX = $('NLWAXForm');

// convenience function for commonly used getElementById call
function $(id){return document.getElementById(id)}
//=============================================================

function AddClientPrinters(){
  //  Get user selected printer/port from hidden fields
  PrinterSelected(GetPrinterValue(), (GetPortValue() == ''));
}
//=============================================================

function ShowPortName(PortName){
  var lPort = $('spPort');

  if(PortName == ''){
    // Driver not installed on client => no associated port
    // Check for previously selected Port
    if(GetPortValue() != ''){
      // previously selected port exists so use it
      ShowPlainMsg(lPort, 'Port:');
      HidePrintButton(false);
    }
    else{
      // No previously selected port, prompt user for selection
      ShowErrorMsg(lPort, 'Please select Port');
      HidePrintButton(true);
    }
    // Refresh port list
    ListPorts(PortName);
  }
  else{
    // Driver install, show associated port, persist port name, and hide port selection
    ShowPlainMsg(lPort, PortName);
    SetPortValue(PortName);
    ShowPortList(false);
    ShowPrintButton(true);
  }
}
//=============================================================

function RemoveControl(control){
  if(control != null)
    control.parentNode.removeChild(control);
}
//=============================================================

function ShowErrorMsg(control, Msg){
    control.style.color = 'Red';
    changeText(control, Msg);
}
//=============================================================

function ShowPlainMsg(control, Msg){
    control.style.color = 'Black';
    changeText(control, Msg);
}
//=============================================================


//=============================================================
//Browser detection
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();
//=============================================================

//=============================================================
function changeText(elm, value){
    if (elm.textContent) {
        elm.textContent = value;
    }else{
        elm.innerText = value;
    }
}

function getText (elm){
    if (elm.textContent)
        return elm.textContent;
    else   
        return elm.innerText;
}
//=============================================================