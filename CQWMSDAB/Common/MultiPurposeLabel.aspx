<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="MultiPurposeLabel.aspx.cs"
    Inherits="Common_MultiPurposeLabel"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, EnterQuantityLabels%>"></asp:Label>
    <telerik:RadTextBox ID="TextBoxQuantity" runat="server" InputType="Number" Text="1"></telerik:RadTextBox>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>" OnClick="ButtonPrint_Click" />
    <asp:Label ID="Label1" runat="server" Text="Copies of Each"></asp:Label>
    <telerik:RadTextBox ID="TextBoxCopies" runat="server" InputType="Number" Text="1"></telerik:RadTextBox>
</asp:Content>

