using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;

public partial class Common_Change_Operator_Group_Menu : System.Web.UI.Page
{
    Operator op = new Operator();

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        if (!Page.IsPostBack)
        {
        
            DropDownList1.DataSource = op.GetOperatorGroups(Session["ConnectionStringName"].ToString());
            DropDownList1.DataBind();

            ddlAddNew.DataSource = op.GetOperatorGroups(Session["ConnectionStringName"].ToString());
            ddlAddNew.DataBind();
            ddlAddNew.Items.Insert(0,new ListItem("Select Group...","0"));
            
            DropDownList2.DataSource = op.GetMenus(Session["ConnectionStringName"].ToString());
            DropDownList2.DataBind();
        }
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.Parse(DropDownList1.SelectedValue) != 0)
        {
            RadGridAccess.DataBind();
        }
    }


    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.Parse(DropDownList2.SelectedValue) != 0)
        {
            RadGridAccess.DataBind();
        }
    }

    #region RadButtonSave_Click
    protected void RadButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "RadButtonSave_Click";
        try
        {
            MenuItemsDynamic pc = new MenuItemsDynamic();
            
            foreach (GridDataItem item in RadGridAccess.Items)
            {
                // For Normal mode
                CheckBox cbAccess = item.FindControl("cbAccess") as CheckBox;

                bool access = bool.Parse(item.GetDataKeyValue("Access").ToString());

                if(access != cbAccess.Checked)
                    pc.UpdateMenuItemAccess(Session["ConnectionStringName"].ToString(), int.Parse(item.GetDataKeyValue("OperatorGroupId").ToString()), int.Parse(item.GetDataKeyValue("MenuItemId").ToString()), cbAccess.Checked);
            }

            RadGridAccess.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OperatorGroupMenuAccess" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadButtonSave_Click
    
    protected void buttonAddNew_Click(object sender, EventArgs e)
    {   
        int SourceOperatorGroupId ;
        string OperatorGroupDesc;

        SourceOperatorGroupId = int.Parse(ddlAddNew.SelectedValue);
        OperatorGroupDesc = txtDescription.Text.ToString();
        

        op.InsertOperatorGroup(Session["ConnectionStringName"].ToString(), OperatorGroupDesc, SourceOperatorGroupId);
        DropDownList1.DataSource = op.GetOperatorGroups(Session["ConnectionStringName"].ToString());
        DropDownList1.DataBind();

        ddlAddNew.DataSource = op.GetOperatorGroups(Session["ConnectionStringName"].ToString());
        ddlAddNew.DataBind();
        ddlAddNew.Items.Insert(0, new ListItem("Select Group...", "0"));

        DropDownList2.DataSource = op.GetMenus(Session["ConnectionStringName"].ToString());
        DropDownList2.DataBind();

        txtDescription.Text = "";
    }

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OperatorGroupMenuAccess", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "OperatorGroupMenuAccess", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}
