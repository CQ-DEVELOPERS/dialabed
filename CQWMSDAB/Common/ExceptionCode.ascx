<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExceptionCode.ascx.cs" Inherits="Common_ExceptionCode" %>
<asp:DropDownList ID="DropDownListExceptionCode" runat="server" DataSourceID="ObjectDataSourceExceptionCode"
    DataValueField="ExceptionCode" DataTextField="ExceptionCode" OnSelectedIndexChanged="DropDownListExceptionCode_SelectedIndexChanged">
</asp:DropDownList>
<asp:ObjectDataSource ID="ObjectDataSourceExceptionCode" runat="server"
    TypeName="ExceptionCode" SelectMethod="GetExceptionCodes">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
