<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutboundShipmentWaveSearch.ascx.cs" Inherits="Common_OutboundShipmentWaveSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<%--<div style="float:left;">
    <asp:Label ID="LabelShowOrHide" runat="server"></asp:Label>
</div>

<div style="float:left;">--%>
<asp:Panel ID="PanelSearch" runat="server" Width="800px" BackColor="#EFEFEC">
	<table>
		<tr>
			<td>
				<asp:Label ID="LabelOutboundShipmentId" runat="server" Text="<%$ Resources:Default, OutboundShipmentId %>" Width="100px"></asp:Label>
			</td>
			<td>
				<telerik:RadTextBox ID="TextBoxOutboundShipmentId" runat="server" Width="150px" OnTextChanged="TextBoxOutboundShipmentId_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
			</td>
			<td>
				<asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
			</td>
			<td>
				<telerik:RadTextBox ID="TextBoxOrderNumber" runat="server" Width="150px" OnTextChanged="TextBoxOrderNumber_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
			</td>
			<td>
				<asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, OutboundDocumentType %>" Width="100px"></asp:Label>
			</td>
			<td>
				<asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataTextField="OutboundDocumentType"
					DataValueField="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentTypeId" Width="150px" OnSelectedIndexChanged="DropDownListOutboundDocumentType_SelectedIndexChanged">
				</asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="LabelCustomerCode" runat="server" Text="<%$ Resources:Default, CustomerCode %>" Width="100px"></asp:Label>
			</td>
			<td>
				<telerik:RadTextBox ID="TextBoxCustomerCode" runat="server" Width="150px" OnTextChanged="TextBoxCustomerCode_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
			</td>
			<td>
				<asp:Label ID="LabelcustomerName" runat="server" Text="<%$ Resources:Default, Customer %>" Width="100px"></asp:Label>
			</td>
			<td>
				<telerik:RadTextBox ID="TextBoxCustomerName" runat="server" Width="150px" OnTextChanged="TextBoxCustomerName_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
			</td>
			<td>
				<asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
			</td>
			<td>
				<asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
					DataTextField="Principal" DataValueField="PrincipalId" Width="150px" OnSelectedIndexChanged="DropDownListPrincipal_SelectedIndexChanged">
				</asp:DropDownList>
				<asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
					SelectMethod="GetPrincipalParameter">
					<SelectParameters>
						<asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
					</SelectParameters>
				</asp:ObjectDataSource>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<uc1:DateRange ID="DateRange1" runat="server" />
			</td>
			<td>
				<asp:Label ID="LabelWave" runat="server" Text="<%$ Resources:Default, Wave %>" Width="100px"></asp:Label>
			</td>
			<td>
				<asp:DropDownList ID="DropDownListWave" runat="server" DataSourceID="ObjectDataSourceWave"
					DataTextField="Wave" DataValueField="WaveId" Width="150px" OnSelectedIndexChanged="DropDownListWave_SelectedIndexChanged">
				</asp:DropDownList>
				<asp:ObjectDataSource ID="ObjectDataSourceWave" runat="server" TypeName="Wave" SelectMethod="GetOutstandingWaves">
					<SelectParameters>
						<asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
						<asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
					</SelectParameters>
				</asp:ObjectDataSource>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, Route %>" Width="100px"></asp:Label>
				<telerik:RadComboBox RenderMode="Lightweight" ID="ddlRoute" AllowCustomText="true" runat="server" Width="300" Height="400px"
					DataSourceID="odsRoute" DataTextField="Route" DataValueField="RouteId" EmptyMessage="Search for Route..." 
					OnSelectedIndexChanged="ddlRoute_SelectedIndexChanged" AppendDataBoundItems="true" Filter="Contains" >
					<Items>
						<telerik:RadComboBoxItem Text="All" Value="-1" />
					</Items>
				</telerik:RadComboBox>
				<asp:ObjectDataSource ID="odsRoute" runat="server" SelectMethod="GetRoute" TypeName="StaticInfoRoute">
					<SelectParameters>
						<asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
					</SelectParameters>
				</asp:ObjectDataSource>
			</td>
		</tr>
	</table>
</asp:Panel>
<%--</div>--%>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
	TargetControlID="PanelSearch"
	Radius="10"
	Color="#EFEFEC"
	BorderColor="#404040"
	Corners="All" />

<%--<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="Server"
    TargetControlID="PanelSearch"
    ExpandControlID="LabelShowOrHide"
    CollapseControlID="LabelShowOrHide" 
    Collapsed="False"
    TextLabelID="LabelShowOrHide"
    ExpandedText="<<<"
    CollapsedText=">>"
    SuppressPostBack="true" />--%>

<asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentTypeId" runat="server" SelectMethod="GetOutboundDocumentTypes"
	TypeName="OutboundDocumentType">
	<SelectParameters>
		<asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
	</SelectParameters>
</asp:ObjectDataSource>

<%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOutboundShipmentId" runat="server"
    TargetControlID="TextBoxOutboundShipmentId" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOrderNumber" runat="server"
    TargetControlID="TextBoxOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="TextBoxCustomerCode" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="TextBoxCustomerName" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>--%>