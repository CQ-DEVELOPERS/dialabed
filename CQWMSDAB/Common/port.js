﻿//=============================================================

function SetPortValue(val){
	// persists user selected port name to a hidden field
	if ($('NLWAX1_hdfPort') != null)
		$('NLWAX1_hdfPort').value = val;
}
//=============================================================

function GetPortValue(){
	// retrieves user selected port name from a hidden field
	if ($('NLWAX1_hdfPort') != null)
		return $('NLWAX1_hdfPort').value;
}
//=============================================================

function GetPortsList(){
	// Retireves list of ports available on client's PC
	// This value is set by the LoadPortList function in NLWAXFunctions.js
  if($('NLWAX1_hdfPortList') != null)
		return $('NLWAX1_hdfPortList').value;
}
//=============================================================

function PortSelected(){
  //  Adds user selected port to hidden field for later retrieval
  if($('ddlClientPorts') == null) return;
  
  var portNum = $('ddlClientPorts').options.selectedIndex;
  var portName = $('ddlClientPorts').options[portNum].text;
  
  if(portName == '-- Select Port --'){
  	ShowErrorMsg($('spPort'), 'Select Printer Port:')
		return;
	}
	
  SetPortValue(portName);
  ShowPlainMsg($('spPort'), 'Port:');
  
  $('NLWAX1_hdfSendTo').value = 'To Port';
  
  DisablePrintButton(false);
  RemoveControl($('btnReprint'));
}
//=============================================================

function GetPrinterPort(PrinterName){
	if(PrinterName != ''){
		var PortName = FindDriverPort(PrinterName)
		if(PortName != null)
			DisplayDriverPort(PortName)
		else{
			PortName = GetPortValue();
			if(PortName != '')
				DisplaySelectedPort(PortName)
			else
				DisplayPortRequest(PrinterName)
		}
	}
}
//=============================================================

function DisplayDriverPort(PortName){
	ShowPlainMsg($('spPort'), 'Port:');
    changeText($('divLabel'), PortName);
	
	// Let NLWAX know we are printer via driver
  $('NLWAX1_hdfSendTo').value = 'To Printer';
  ShowPortList(false);
  DisablePrintButton(false);
}
//=============================================================

function DisplaySelectedPort(PortName){
	ShowPlainMsg($('spPort'), 'Port:');
    changeText($('divLabel'), '');

	// Let NLWAX know we are sending print code directly to port
	$('NLWAX1_hdfSendTo').value = 'To Port';
	ListPorts(PortName);
	DisablePrintButton(false);
}
//=============================================================

function DisplayPortRequest(PrinterName){
	ShowErrorMsg($('spPort'), 'Select Port:')
	RemoveControl($('btnReprint'));
    changeText($('divLabel'), '');
	
	ListPorts('');
}
//=============================================================

function FindDriverPort(PrinterName){
  if (BrowserDetect.browser == 'Explorer'){
      // Create XML DOM parser
      var xmldom = new ActiveXObject('Microsoft.XMLDOM'); 
      
      // Load XML from hidden field in NLWAX1
      xmldom.loadXML(GetPrintersList()); 
      var clientPrinters = xmldom.getElementsByTagName('Printer');

      //loop through client drivers looking for selected printer
      for(var x=0;x<=clientPrinters.length;x++){ 
        var printer = clientPrinters[x];
        if (! printer) return;
        var deviceName = printer.firstChild.firstChild.nodeValue;
        //if driver is found return associated port
        if (deviceName == PrinterName){
            var portName = printer.lastChild.firstChild.nodeValue;
            return portName;
        }
      }      
      
  }else{        
      try{ //Firefox, Mozilla, Opera, etc.
        parser=new DOMParser();
        xmldom=parser.parseFromString(GetPrintersList(),"text/xml");


          //Get collection of Printer nodes
          var clientPrinters = xmldom.getElementsByTagName('Printer');
          
          //  loop through client drivers looking for selected printer
          for(var x=0;x<=clientPrinters.length;x++){ 
            var printer = clientPrinters[x];
            if (! printer) return;
            var deviceName = printer.childNodes[1].childNodes[0].nodeValue;
            //if driver is found return associated port
            if (deviceName == PrinterName){
                var portName = printer.childNodes[3].childNodes[0].nodeValue;
                return portName;
            }
          }
        
      }
      catch(e){
        alert(e.message);
        return;
      }
  }
}

//=============================================================

function ListPorts(SelPortName){
  if (BrowserDetect.browser == 'Explorer'){
    var xmldom = new ActiveXObject('Microsoft.XMLDOM');
    xmldom.loadXML(GetPortsList());    
  }else{  
    try{ //Firefox, Mozilla, Opera, etc.
      parser=new DOMParser();
      var xmldom=parser.parseFromString(GetPortsList(),"text/xml");
    
    }
    catch(e){
      alert(e.message);
      return;
    }
  }  
    
  var selIndex;
  
  var clientPorts = xmldom.getElementsByTagName('PortName');

  ShowPortList(true);

  //  Clear existing entries for port selection
  var selPort = $('ddlClientPorts');
  selPort.options.length = 0;
	selPort.options[selPort.options.length] = new Option('-- Select Port --','',false,true);

  for(var x=clientPorts.length-1;x>=0;x--){ 
    var port = clientPorts[x];
    if (! port)
      return;
    var portName = port.firstChild.nodeValue;
    var selected = (portName == SelPortName)
    if(selected == true)
			selIndex = selPort.options.length;
    selPort.options[selPort.options.length] = new Option(portName,portName,false,false);
  }
  if(selIndex != null)
		selPort.selectedIndex = selIndex;
}
//=============================================================

function CreateSelect(){
	var oSelect=document.createElement("select");
	oSelect.setAttribute('id', 'ddlClientPorts');
	
    if (BrowserDetect.browser == 'Explorer'){	
	    oSelect.attachEvent('onchange', PortSelected);
	}else{
	    oSelect.addEventListener('change', PortSelected, false);
	}        
	$('divLabel').appendChild(oSelect);
}
//=============================================================

function ShowPortList(Show){
  if(Show == true){
    if($('ddlClientPorts') == null)
			CreateSelect()
	}
	else
  	RemoveControl($('ddlClientPorts'));
}
//=============================================================
//====== End of script =======================================================
