using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_GenericLabel : System.Web.UI.Page
{
    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["Title"] != null)
                    TextBoxTitle.Text = Session["Title"].ToString();

                if (Session["Barcode"] != null)
                    TextBoxBarcode.Text = Session["Barcode"].ToString();
            }
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintLabel_Click";

        try
        {
            ArrayList rowList = new ArrayList();

            Session["Title"] = TextBoxTitle.Text;
            Session["Barcode"] = TextBoxBarcode.Text;

            int quantity = 0;
            int count = 0;

            if (int.TryParse(TextBoxQuantity.Text, out quantity))
            {

                while (count < quantity)
                {
                    count++;

                    rowList.Add(TextBoxBarcode.Text);
                }
            }

            if (rowList.Count > 0)
            {
                Session["checkedList"] = rowList;
                Session["LabelName"] = "Multi Purpose Label.lbl";
                Session["FromURL"] = "~/Common/GenericLabel.aspx";
                Session["LabelCopies"] = quantity;

                if (Session["Printer"] == null)
                    Session["Printer"] = "";

                var sessionPort = Session["Port"];
                var sessionPrinter = Session["Printer"];
                var sesionIndicatorList = Session["indicatorList"];
                var sessionCheckedList = Session["checkedList"];
                var sessionLocationList = Session["locationList"];
                var sessionConnectionString = Session["ConnectionStringName"];
                var sessionTitle = Session["Title"];
                var sessionBarcode = Session["Barcode"];
                var sessionWarehouseId = Session["WarehouseId"];
                var sessionProductList = Session["ProductList"];
                var sessionTakeOnLabel = Session["TakeOnLabel"];
                var sessionLabelCopies = Session["LabelCopies"];

                NLWAXPrint nl = new NLWAXPrint();
                nl.onSessionUpdate += Nl_onSessionUpdate;
                string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                          Session["LabelName"].ToString(),
                                                          sessionPrinter,
                                                          sessionPort,
                                                          sesionIndicatorList,
                                                          sessionCheckedList,
                                                          sessionLocationList,
                                                          sessionConnectionString,
                                                          sessionTitle,
                                                          sessionBarcode,
                                                          sessionWarehouseId,
                                                          sessionProductList,
                                                          sessionTakeOnLabel,
                                                          sessionLabelCopies);
                if (printResponse == string.Empty)
                {
                    Master.ErrorText = "An error has occurred! For more details please check automation manager.";
                }
                else
                {
                    Master.MsgText = "Label has been printed successfuly.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("GenericLabel " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "PutawayMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "PutawayMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
