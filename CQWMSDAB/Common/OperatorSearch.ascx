<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperatorSearch.ascx.cs" Inherits="Common_OperatorSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelOperatorCode" runat="server" Text="<%$ Resources:Default, OperatorCode%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxOperatorCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelOperator" runat="server" Text="<%$ Resources:Default, Operator%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxOperator" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
</table>

<asp:GridView ID="GridViewOperatorSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataKeyNames="OperatorId"
    OnSelectedIndexChanged="GridViewOperatorSearch_OnSelectedIndexChanged"
    OnPageIndexChanging="GridViewOperatorSearch_PageIndexChanging">
    <Columns>
        <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator%>" />
        <asp:BoundField DataField="OperatorCode" HeaderText="<%$ Resources:Default, OperatorCode%>" />
    </Columns>
    <EmptyDataTemplate>
        <h5>No rows</h5>
    </EmptyDataTemplate>
</asp:GridView>