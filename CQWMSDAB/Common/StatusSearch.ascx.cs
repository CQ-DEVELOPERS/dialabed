﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_StatusSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["StatusId"] == null)
                Session["StatusId"] = -1;
            if (Session["Status"] == null)
                Session["Status"] = -1;
        }
    }
    #endregion Page_Load

    #region Event handlers
    public EventHandler OnSelectedIndexChange;

    #endregion

    #region GridViewStatusSearch_OnSelectedIndexChanged
    protected void GridViewStatusSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StatusId"] = GridViewStatusSearch.SelectedDataKey["StatusId"];
        Session["Status"] = GridViewStatusSearch.SelectedDataKey["Status"];
        Session["Status"] = GridViewStatusSearch.SelectedRow.Cells[2].Text;

        if (OnSelectedIndexChange != null)
            OnSelectedIndexChange(sender, e);
    }
    #endregion GridViewStatusSearch_OnSelectedIndexChanged

    #region GridViewStatusSearch_PageIndexChanging
    protected void GridViewStatusSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewStatusSearch.PageIndex = e.NewPageIndex;
            GridViewStatusSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewStatusSearch_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewStatusSearch.PageIndex = 0;

        GridViewStatusSearch_DataBind();
    }
    #endregion "ButtonSearch_Click"

    protected void GridViewStatusSearch_DataBind()
    {
        //Product product = new Product();
        Session["Type"] = "PR";
        Status status = new Status();

        GridViewStatusSearch.DataSource = status.GetAllStatusByType(Session["ConnectionStringName"].ToString(), Session["Type"].ToString());

        GridViewStatusSearch.DataBind();
    }
}
