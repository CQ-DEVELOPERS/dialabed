<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductSearchPackaging.ascx.cs" Inherits="Common_ProductSearchPackaging" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
        </td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
    
</table>

<asp:GridView ID="GridViewProductSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataKeyNames="StorageUnitId,Product,StorageUnitBatchId"
    OnSelectedIndexChanged="GridViewProductSearch_OnSelectedIndexChanged"
    OnPageIndexChanging="GridViewProductSearch_PageIndexChanging">
    <Columns>
        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product%>" />
        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode%>" />
        <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode%>" />
    </Columns>
    <EmptyDataTemplate>
        <h5>No rows</h5>
    </EmptyDataTemplate>
</asp:GridView>

<%--<asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Product" SelectMethod="SearchProducts">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:ControlParameter Name="Product" ControlID="TextBoxProduct" Type="String" />
        <asp:ControlParameter Name="ProductCode" ControlID="TextBoxProductCode" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>--%>