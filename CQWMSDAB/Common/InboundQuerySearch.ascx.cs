﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_InboundQuerySearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListStatus.DataBind();
                //Session["InboundShipmentId"] = -1;

                if (Session["InboundShipmentId"] == null)
                    Session["InboundShipmentId"] = -1;
                else
                    TextBoxInboundShipmentId.Text = Session["InboundShipmentId"].ToString();

                if (Session["OrderNumber"] == null)
                    Session["OrderNumber"] = "";
                else
                    TextBoxOrderNumber.Text = Session["OrderNumber"].ToString();

                if (Session["DeliveryNoteNumber"] == null)
                    Session["DeliveryNoteNumber"] = "";
                else
                    TextBoxDeliveryNoteNumber.Text = Session["DeliveryNoteNumber"].ToString();

                if (Session["PrincipalId"] == null)
                    Session["PrincipalId"] = -1;
                else
                    DropDownListPrincipal.SelectedValue = Session["PrincipalId"].ToString();

                if (Session["SupplierCode"] == null)
                    Session["SupplierCode"] = "";
                else
                    TextBoxSupplierCode.Text = Session["SupplierCode"].ToString();

                if (Session["SupplierName"] == null)
                    Session["SupplierName"] = "";
                else
                    TextBoxSupplierName.Text = Session["SupplierName"].ToString();

                if (Session["Status"] == null)
                    Session["Status"] = "";
                else
                    DropDownListStatus.SelectedValue = Session["Status"].ToString();

                if (Session["PrincipalId"] == null)
                    try { Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString()); }
                    catch { Session["PrincipalId"] = -1; }
                else
                    DropDownListPrincipal.SelectedValue = Session["PrincipalId"].ToString();
            }

            if (TextBoxOrderNumber.Text == "{All}")
            {
                Session["OrderNumber"] = "";
                TextBoxOrderNumber.Text = "";
            }

            if (TextBoxSupplierCode.Text == "{All}")
            {
                Session["ExternalCompanyCode"] = "";
                TextBoxSupplierCode.Text = "";
            }

            if (TextBoxSupplierName.Text == "{All}")
            {
                Session["ExternalCompany"] = "";
                TextBoxSupplierName.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxInboundShipmentId_TextChanged(object sender, EventArgs e)
    {
        Session["InboundShipmentId"] = TextBoxInboundShipmentId.Text;
    }
    protected void TextBoxOrderNumber_TextChanged(object sender, EventArgs e)
    {
        Session["OrderNumber"] = TextBoxOrderNumber.Text;
    }
    protected void TextBoxSupplierCode_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompanyCode"] = TextBoxSupplierCode.Text;
    }
    protected void TextBoxSupplierName_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompany"] = TextBoxSupplierName.Text;
    }
    protected void DropDownListStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["Status"] = int.Parse(DropDownListStatus.SelectedValue.ToString());
    }
    protected void DropDownListPrincipal_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString());
    }
    protected void TextDeliveryNoteNumber_TextChanged(object sender, EventArgs e)
    {
        Session["TextBoxDeliveryNoteNumber"] = TextBoxDeliveryNoteNumber.Text;
    }
}