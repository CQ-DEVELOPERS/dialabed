using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_GenericLabel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) 
    {


    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        Location loc = new Location();
        DataSet ds = loc.GetLocations(Session["connectionStringName"].ToString(),
                    int.Parse(Session["warehouseId"].ToString()), ddlStartLocation.Text, ddlEndLocation.Text);

        Session["LabelName"] = "Multi Purpose Label.lbl";
        Session["FromURL"] = "~/Common/LocationPrint.aspx";

        if (Session["Printer"] == null)
            Session["Printer"] = "";

        int index = -1;
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            try
            {
                index++;
                Session["Title"] = dr["Location"].ToString();
                Session["Barcode"] = dr["Location"].ToString();

                var sessionPort = Session["Port"];
                var sessionPrinter = Session["Printer"];
                var sesionIndicatorList = Session["indicatorList"];
                var sessionCheckedList = Session["checkedList"];
                var sessionLocationList = Session["locationList"];
                var sessionConnectionString = Session["ConnectionStringName"];
                var sessionTitle = Session["Title"];
                var sessionBarcode = Session["Barcode"];
                var sessionWarehouseId = Session["WarehouseId"];
                var sessionProductList = Session["ProductList"];
                var sessionTakeOnLabel = Session["TakeOnLabel"];
                var sessionLabelCopies = Session["LabelCopies"];

                NLWAXPrint nl = new NLWAXPrint();
                nl.onSessionUpdate += Nl_onSessionUpdate;
                string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                          Session["LabelName"].ToString(),
                                                          sessionPrinter,
                                                          sessionPort,
                                                          sesionIndicatorList,
                                                          sessionCheckedList,
                                                          sessionLocationList,
                                                          sessionConnectionString,
                                                          sessionTitle,
                                                          sessionBarcode,
                                                          sessionWarehouseId,
                                                          sessionProductList,
                                                          sessionTakeOnLabel,
                                                          sessionLabelCopies);
                if (printResponse == string.Empty)
                {
                    Master.ErrorText = "An error has occurred! For more details please check automation manager.";
                }
                else
                {
                    Master.MsgText = "Label has been printed successfuly.";
                }
            }

            catch { }
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
}
