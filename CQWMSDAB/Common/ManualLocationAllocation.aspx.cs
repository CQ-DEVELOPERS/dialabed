using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ManualLocationAllocation : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if(!Page.IsPostBack)
                Session["InstructionId"] = -1;

            if (GridViewInstruction.Rows.Count < 1)
                GetNextInstruction();
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.InnerException.ToString(); 
        }
    }
    #endregion Page_Load

    #region GridViewArea_SelectedIndexChanged
    protected void GridViewArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewLocations.Visible = true;
            
            GridViewLocations.DataBind();
        }
        catch { }

    }
    #endregion GridViewArea_SelectedIndexChanged

    #region GridViewLocations_SelectedIndexChanged
    protected void GridViewLocations_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewLocations.Visible = false;

            InstructionMaintenance ds = new InstructionMaintenance();

            if (ds.ManualLocationAllocate(Session["ConnectionStringName"].ToString(),
                                            int.Parse(Session["InstructionId"].ToString()),
                                            int.Parse(GridViewLocations.SelectedDataKey["LocationId"].ToString())))
            {
                Master.MsgText = "Location successfully allocated";
                GetNextInstruction();
            }
            else
                Master.MsgText = "Error, please try again.";
        }
        catch { }
    }
    #endregion GridViewLocations_SelectedIndexChanged

    #region GetNextInstruction
    protected void GetNextInstruction()
    {
        try
        {
            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList.Count <= 0 || rowList == null)
            {
                Session.Remove("checkedList");

                if (Session["FromURL"] != null)
                    Response.Redirect(Session["FromURL"].ToString());
            }
            else
            {
                if (LabelTotal.Text == "")
                    LabelTotal.Text = rowList.Count.ToString();

                Session["InstructionId"] = rowList[0];

                LabelLines.Text = Convert.ToString(Convert.ToInt32(LabelTotal.Text) - (rowList.Count - 1));

                rowList.Remove(rowList[0]);
            }

            GridViewArea.DataBind();
            GridViewInstruction.DataBind();

            Session["checkedList"] = rowList;
        }
        catch { }
    }
    #endregion GetNextInstruction

    #region ButtonReturn_Click
    protected void ButtonReturn_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["checkedList"] != null)
                Session.Remove("checkedList");

            if (Session["FromURL"] != null)
                Response.Redirect(Session["FromURL"].ToString());
        }
        catch { }
    }
    #endregion ButtonReturn_Click
}