<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BackorderSearch.ascx.cs" Inherits="Common_BackorderSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<asp:Panel ID="PanelSearch" runat="server" Width="900px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px" OnTextChanged="TextBoxProductCode_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px" OnTextChanged="TextBoxProduct_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default, SKUCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKUCode" runat="server" Width="150px" OnTextChanged="TextBoxSKUCode_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxOrderNumber" runat="server" Width="150px" OnTextChanged="TextBoxOrderNumber_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, OutboundDocumentType %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataTextField="OutboundDocumentType"
                    DataValueField="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentTypeId" Width="150px" OnSelectedIndexChanged="DropDownListOutboundDocumentType_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentTypeId" runat="server" SelectMethod="GetOutboundDocumentTypes"
                    TypeName="OutboundDocumentType">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <asp:Label ID="LabelRetrieve" runat="server" Text="<%$ Resources:Default, Retrieve %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:RadioButtonList ID="rblRetrieve" runat="server" RepeatColumns="2" OnSelectedIndexChanged="rblRetrieve_SelectedIndexChanged">
                    <asp:ListItem Text="<%$ Resources:Default, AllLines %>" Value="true"></asp:ListItem>
                    <asp:ListItem Selected="True" Text="<%$ Resources:Default, SomeLines %>" Value="false"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelCustomerCode" runat="server" Text="<%$ Resources:Default, CustomerCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxCustomerCode" runat="server" Width="150px" OnTextChanged="TextBoxCustomerCode_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelcustomerName" runat="server" Text="<%$ Resources:Default, Customer %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxCustomerName" runat="server" Width="150px" OnTextChanged="TextBoxCustomerName_TextChanged"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <uc1:DateRange ID="DateRange1" runat="server" />
</asp:Panel>

<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />

<%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOutboundShipmentId" runat="server"
    TargetControlID="TextBoxOutboundShipmentId" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>--%>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOrderNumber" runat="server"
    TargetControlID="TextBoxOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="TextBoxCustomerCode" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="TextBoxCustomerName" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>