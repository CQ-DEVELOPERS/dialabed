using System;
using System.Web.UI;

public partial class Common_DateRange : System.Web.UI.UserControl
{
    #region Page_Load
    public bool Enabled
    {
        get { return RadDatePicker1.Enabled && RadDatePicker2.Enabled; }

        set
        {
            RadDatePicker1.Enabled = value;
            RadDatePicker2.Enabled = value;
        }
    }

    public DateTime? FromDate
    {
        get { return RadDatePicker1.SelectedDate; }
        set { RadDatePicker1.SelectedDate = value; }
    }

    public DateTime? ToDate
    {
        get { return RadDatePicker2.SelectedDate; }
        set { RadDatePicker2.SelectedDate = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Session["ToDate"] = DateRange.GetToDate().AddDays(1);
            ToDate = DateTime.Parse(Session["ToDate"].ToString());
            Session["FromDate"] = DateTime.Now.Date.ToString("yyy/MM/dd").ToString();
            FromDate = DateTime.Parse(Session["FromDate"].ToString());
        }

    }
    #endregion "Page_Load"

    protected void RadDatePicker_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
    {
        Telerik.Web.UI.RadDatePicker fromControl = (Telerik.Web.UI.RadDatePicker)sender;
        string id = fromControl.ID;
        DateTime dt = id == "RadDatePicker1" ? Convert.ToDateTime(FromDate) : Convert.ToDateTime(ToDate);
        string temp = dt.ToShortDateString() + " 00:00:00";
        if (id == "RadDatePicker1")
            Session["FromDate"] = temp;
        else
            Session["ToDate"] = temp;
    }

}
