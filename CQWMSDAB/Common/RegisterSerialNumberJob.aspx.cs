using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_RegisterSerialNumberJob : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                GridViewSerialNumber.DataBind();
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region ObjectDataSourceDetails_OnSelecting
    protected void ObjectDataSourceDetails_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["jobId"] = int.Parse(Session["JobId"].ToString());
    }
    #endregion ObjectDataSourceDetails_OnSelecting

    #region ObjectDataSourceLinked_OnSelecting
    protected void ObjectDataSourceLinked_OnSelecting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        if (GridViewSerialNumber.SelectedIndex == -1)
            e.InputParameters["instructionId"] = -1;
        else
            e.InputParameters["instructionId"] = int.Parse(GridViewSerialNumber.SelectedDataKey["InstructionId"].ToString());
    }
    #endregion ObjectDataSourceLinked_OnSelecting

    #region ObjectDataSourceUnlinked_OnSelecting
    protected void ObjectDataSourceUnlinked_OnSelecting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        if (GridViewSerialNumber.SelectedIndex == -1)
        {
            e.InputParameters["jobId"] = -1;
            e.InputParameters["instructionId"] = -1;
        }
        else
        {
            e.InputParameters["jobId"] = GridViewSerialNumber.SelectedDataKey["JobId"];
            e.InputParameters["instructionId"] = GridViewSerialNumber.SelectedDataKey["InstructionId"];
        }
    }
    #endregion ObjectDataSourceUnlinked_OnSelecting

    #region GridViewSerialNumber_SelectedIndexChanged
    protected void GridViewSerialNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewUnlinked.DataBind();
        GridViewLinked.DataBind();
    }
    #endregion GridViewSerialNumber_SelectedIndexChanged

    #region GridViewLinked_SelectedIndexChanged
    protected void GridViewLinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        SerialNumber ds = new SerialNumber();

        if (ds.Remove(Session["ConnectionStringName"].ToString(),
                        int.Parse(GridViewLinked.SelectedDataKey["SerialNumberId"].ToString()),
                        int.Parse(GridViewLinked.SelectedDataKey["InstructionId"].ToString())) == true)
        {
            GridViewUnlinked.DataBind();
            GridViewLinked.DataBind();
        }
        else
            Response.Write("There was an error!!!");
    }
    #endregion GridViewLinked_SelectedIndexChanged

    #region GridViewUnlinked_SelectedIndexChanged
    protected void GridViewUnlinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        SerialNumber ds = new SerialNumber();

        if (ds.Transfer(Session["ConnectionStringName"].ToString(),
                        int.Parse(GridViewUnlinked.SelectedDataKey["SerialNumberId"].ToString()),
                        int.Parse(GridViewSerialNumber.SelectedDataKey["InstructionId"].ToString())) == true)
        {
            GridViewUnlinked.DataBind();
            GridViewLinked.DataBind();
        }
        else
            Response.Write("There was an error!!!");
    }
    #endregion GridViewUnlinked_SelectedIndexChanged

    #region ButtonFinished_Click
    protected void ButtonFinished_Click(object sender, EventArgs e)
    {
        try
        {
            Session["JobId"] = null;
            
            if (Session["FromURL"] != null)
                Response.Redirect(Session["FromURL"].ToString());
        }
        catch { }
    }
    #endregion ButtonFinished_Click
}
