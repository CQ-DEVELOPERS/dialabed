﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_WebUserControl : System.Web.UI.UserControl
{
   
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["StockTakeReferenceId"] == null)
                Session["StockTakeReferenceId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewStockTakeRefSearch_OnSelectedIndexChanged
    protected void GridViewStockTakeRefSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StockTakeReferenceId"] = GridViewStockTakeRefSearch.SelectedDataKey["StockTakeReferenceId"];
        Session["StockTakeType"] = GridViewStockTakeRefSearch.SelectedDataKey["StockTakeType"];
        Session["StartDate"] = GridViewStockTakeRefSearch.SelectedDataKey["StartDate"];
        Session["EndDate"] = GridViewStockTakeRefSearch.SelectedDataKey["EndDate"];
    }
    #endregion GridViewStockTakeRefSearch_OnSelectedIndexChanged

}
