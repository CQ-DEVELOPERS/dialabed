﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_InboundStationSheetSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["InboundShipmentId"] = -1;

                if (Session["OrderNumber"] == null)
                    Session["OrderNumber"] = "";
                else
                    TextBoxOrderNumber.Text = Session["OrderNumber"].ToString();

                if (Session["ExternalCompanyCode"] == null)
                    Session["ExternalCompanyCode"] = "";
                else
                    TextBoxProductCode.Text = Session["ExternalCompanyCode"].ToString();

                if (Session["ExternalCompany"] == null)
                    Session["ExternalCompany"] = "";
                else
                    TextBoxBatch.Text = Session["ExternalCompany"].ToString();

                if (Session["ProductCode"] == null)
                    Session["ProductCode"] = "";
                else
                    TextBoxBatch.Text = Session["ProductCode"].ToString();

                if (Session["Batch"] == null)
                    Session["Batch"] = "";
                else
                    TextBoxBatch.Text = Session["Batch"].ToString();

            }

            if (TextBoxOrderNumber.Text == "{All}")
            {
                Session["OrderNumber"] = "";
                TextBoxOrderNumber.Text = "";
            }

            if (TextBoxBatch.Text == "{All}")
            {
                Session["Batch"] = "";
                TextBoxBatch.Text = "";
            }

            if (TextBoxProductCode.Text == "{All}")
            {
                Session["ProductCode"] = "";
                TextBoxProductCode.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxOrderNumber_TextChanged(object sender, EventArgs e)
    {
        Session["OrderNumber"] = TextBoxOrderNumber.Text;
    }
    protected void TextBoxProductCode_TextChanged(object sender, EventArgs e)
    {
        Session["ProductCode"] = TextBoxProductCode.Text;
    }
    protected void TextBoxBatch_TextChanged(object sender, EventArgs e)
    {
        Session["Batch"] = TextBoxBatch.Text;
    }
}
