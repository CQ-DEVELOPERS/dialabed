using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
//using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_Change_Operator_Group_Menu : System.Web.UI.Page
{
    Operator op = new Operator();
    
    
    protected void Page_Load(object sender, EventArgs e)
    { 
        if (!Page.IsPostBack)
        {
        
            DropDownList1.DataSource = op.GetOperatorGroups(Session["ConnectionStringName"].ToString());
            DropDownList1.DataBind();

            ddlAddNew.DataSource = op.GetOperatorGroups(Session["ConnectionStringName"].ToString());
            ddlAddNew.DataBind();
            ddlAddNew.Items.Insert(0,new ListItem("Select Group...","0"));
            
            XmlDataSource1.Data = op.GetOperatorGroupMenu(Session["ConnectionStringName"].ToString(), int.Parse(DropDownList1.SelectedValue)).ToString();
            XmlDataSource1.EnableCaching = false;
            XmlDataSource1.DataBind();

            //Force the databind to refresh now rather than after page load.
            TreeView1.DataBind();

            //This is to callapse all groups
            //TreeView1.CollapseAll();

            //You may play with the Following code depending on how you want to display your treeview.
            TreeNode node = TreeView1.FindNode("Operator_Groups");
            foreach (TreeNode tn in node.ChildNodes)
            {
                foreach (TreeNode cn in tn.ChildNodes)
                {
                    cn.Collapse();
                    cn.CollapseAll();
                    //foreach (TreeNode cn1 in cn.ChildNodes)
                    //{
                    //collapses at Third level
                    //cn1.Collapse();
                    //}
                }
                //collapses at first level
                //tn.Collapse();
            }

            DropDownList2.DataSource = op.GetMenuItems(Session["ConnectionStringName"].ToString());
            DropDownList2.DataBind();

            XmlDataSource2.Data = op.GetOperatorMenuGroup(Session["ConnectionStringName"].ToString(), int.Parse(DropDownList2.SelectedValue)).ToString();
            XmlDataSource2.EnableCaching = false;
            XmlDataSource2.DataBind();

           
        }
       
    }
    

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.Parse(DropDownList1.SelectedValue) != 0)
        {
            XmlDataSource1.Data = op.GetOperatorGroupMenu(Session["ConnectionStringName"].ToString(), int.Parse(DropDownList1.SelectedValue)).ToString();
            XmlDataSource1.EnableCaching = false;
            XmlDataSource1.DataBind();

            //Force the databind to refresh now rather than after page load.
            TreeView1.DataBind();

            if (TreeView2.SelectedNode != null)
            {
                TreeView2.SelectedNode.Parent.Select();
            }

            //This statement will Collapse all Groups.
            //TreeView1.CollapseAll();

            //You may play with the Following Code depending on how you want to display your treeview.
            TreeNode node = TreeView1.FindNode("Operator_Groups");
            foreach (TreeNode tn in node.ChildNodes)
            {

                foreach (TreeNode cn in tn.ChildNodes)
                {
                    cn.Collapse();
                    cn.CollapseAll();
                    //foreach (TreeNode cn1 in cn.ChildNodes)
                    //{
                        //collapses at second level
                      //  cn1.Collapse();

                        //this is for all sub levels as well from the second level
                       // cn1.CollapseAll();
                   // }
                }
                //collapses at first level
                //tn.Collapse();

                //this is for all sub levels as well from the first level
                //tn.CollapseAll();
            }


        }
    }


    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (int.Parse(DropDownList2.SelectedValue) != 0)
        {
            XmlDataSource2.Data = op.GetOperatorMenuGroup(Session["ConnectionStringName"].ToString(), int.Parse(DropDownList2.SelectedValue)).ToString();
            XmlDataSource2.EnableCaching = false;
            XmlDataSource2.DataBind();
            if (TreeView1.SelectedNode != null)
            {
                TreeView1.SelectedNode.Parent.Select();
            }
        }
    }

    protected void TreeView2_SelectedNodeChanged(object sender, EventArgs e)
    {
        int MenuItemId;
        int OperatorGroupId;
        string ParentText;

        string MenuItem ;
        string OperatorGroupText ;
        
        if (TreeView2.SelectedNode.Value.ToString() != "Menu_Items")
        {
            MenuItemId = Convert.ToInt32(TreeView2.SelectedNode.Parent.Value);
            MenuItem = TreeView2.SelectedNode.Parent.Text;
            ParentText= TreeView2.SelectedNode.Parent.Parent.Value;

            OperatorGroupId = Convert.ToInt32(TreeView2.SelectedNode.Value);
            OperatorGroupText = TreeView2.SelectedNode.Text;

            op.OperatorGroupSwitchAccess(Session["ConnectionStringName"].ToString(), OperatorGroupId, MenuItemId);
            TreeView2.SelectedNode.ImageUrl = op.strAccess;
            TreeNode node = TreeView1.FindNode("Operator_Groups/" + OperatorGroupId + "/" + ParentText + "/" + MenuItemId);
               if (node != null)
               {
                node.ImageUrl = op.strAccess;
               }
               if ((TreeView1.SelectedNode != null) && (TreeView1.SelectedNode.Text != "Operator_Groups"))
               {
                   TreeView1.SelectedNode.Parent.Select();
               }
               TreeView2.SelectedNode.Parent.Select();
        }
    }
    
    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        int MenuItemId ;
        int OperatorGroupId ;
        string ParentText;

        if (TreeView1.SelectedNode.Value.ToString() != "Operator_Groups")
        {
            MenuItemId = Convert.ToInt32(TreeView1.SelectedNode.Value);
            string MenuItem = TreeView1.SelectedNode.Text;

            ParentText = TreeView1.SelectedNode.Parent.Value;
            //OperatorGroupId = Convert.ToInt32(TreeView1.SelectedNode.Parent.Parent.Value);
            OperatorGroupId =  Convert.ToInt32( DropDownList1.SelectedValue.ToString());
            op.OperatorGroupSwitchAccess(Session["ConnectionStringName"].ToString(), OperatorGroupId, MenuItemId);
            TreeView1.SelectedNode.ImageUrl = op.strAccess;
            Int32 count2 = TreeView1.SelectedNode.ChildNodes.Count;
            for (int i = 0; i < count2; i++)
            {
                TreeView1.SelectedNode.ChildNodes[i].ImageUrl = op.strAccess;
            }
            if (MenuItem.Contains("All - ") == true) 
            {
                TreeView1.SelectedNode.Parent.ImageUrl = op.strAccess;
                ParentText = TreeView1.SelectedNode.Parent.Parent.Value;
                Int32 count = TreeView1.SelectedNode.Parent.ChildNodes.Count;
                for (int i = 0; i < count; i++)
                {
                    TreeView1.SelectedNode.Parent.ChildNodes[i].ImageUrl = op.strAccess;
                    
                }
            }


            TreeNode node = TreeView2.FindNode("Menu_Items/" + ParentText + "/" + MenuItemId.ToString() + "/" + OperatorGroupId.ToString());
                if (node != null)
                {
                node.ImageUrl = op.strAccess;
                }
            TreeView1.SelectedNode.Parent.Select();
            if ((TreeView2.SelectedNode != null)  && (TreeView2.SelectedNode.Text != "Menu_Items"))
            {
                TreeView2.SelectedNode.Parent.Select();
            }
        }
    }
    
    protected void buttonAddNew_Click(object sender, EventArgs e)
    {   
        int SourceOperatorGroupId ;
        string OperatorGroupDesc;

        SourceOperatorGroupId = int.Parse(ddlAddNew.SelectedValue);
        OperatorGroupDesc = txtDescription.Text.ToString();
        

        op.InsertOperatorGroup(Session["ConnectionStringName"].ToString(), OperatorGroupDesc, SourceOperatorGroupId);
        DropDownList1.DataSource = op.GetOperatorGroups(Session["ConnectionStringName"].ToString());
        DropDownList1.DataBind();

        ddlAddNew.DataSource = op.GetOperatorGroups(Session["ConnectionStringName"].ToString());
        ddlAddNew.DataBind();
        ddlAddNew.Items.Insert(0, new ListItem("Select Group...", "0"));

        XmlDataSource1.Data = op.GetOperatorGroupMenu(Session["ConnectionStringName"].ToString(), int.Parse(DropDownList1.SelectedValue)).ToString();
        XmlDataSource1.EnableCaching = false;
        XmlDataSource1.DataBind();

        DropDownList2.DataSource = op.GetMenuItems(Session["ConnectionStringName"].ToString());
        DropDownList2.DataBind();

        XmlDataSource2.Data = op.GetOperatorMenuGroup(Session["ConnectionStringName"].ToString(), int.Parse(DropDownList2.SelectedValue)).ToString();
        XmlDataSource2.EnableCaching = false;
        XmlDataSource2.DataBind();
        txtDescription.Text = "";
    }
    
}
