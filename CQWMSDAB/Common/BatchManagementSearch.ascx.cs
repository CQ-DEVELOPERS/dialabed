using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_BatchManagementSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["BatchNumber"] != null)
                    TextBoxBatchNumber.Text = Session["BatchNumber"].ToString();

                DropDownListBatchStatus.DataBind();
            }

            if (DropDownListBatchStatus.SelectedItem.Text == "{All}")

            {
                Session["BatchStatusId"] = "-1";
            }

            if (TextBoxBatchNumber.Text.Length == 0)
            {
                Session["BatchNumber"] = "-1";

                TextBoxBatchNumber.Text = "";
            }

            if (TextBoxStockBatchAge1.Text.Length == 0)
            {
                Session["DaysoldLt"] = "-1";
            }

            if (TextBoxStockBatchAge2.Text.Length == 0)
            {
                Session["DaysoldGt"] = "-1";
            }
            if (TextBoxProductCode.Text.Length == 0)
            {
                Session["ProductCode"] = "-1";
            }
            if (TextBoxProduct.Text.Length == 0)
            {
                Session["Product"] = "-1";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxBatchNumber_TextChanged(object sender, EventArgs e)
    {
        Session["BatchNumber"] = TextBoxBatchNumber.Text;
    }


    protected void TextBoxProductCode_TextChanged(object sender, EventArgs e)
    {
        Session["ProductCode"] = TextBoxProductCode.Text;
    }



    protected void TextBoxProduct_TextChanged(object sender, EventArgs e)
    {
        Session["Product"] = TextBoxProduct.Text;
    }

    protected void CheckBoxSOHOnly_Changed(object sender, EventArgs e)
    {
        Session["SOHOnly"] = CheckBoxSOHOnly.Checked.ToString();
    }
    
    protected void DropDownListBatchStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["BatchStatusId"] = int.Parse(DropDownListBatchStatus.SelectedValue.ToString());
    }


    protected void TextBoxStockBatchAge1_TextChanged(object sender, EventArgs e)
    {
        Session["DaysoldLt"] = TextBoxStockBatchAge1.Text;
    }


    protected void TextBoxStockBatchAge2_TextChanged(object sender, EventArgs e)
    {
        Session["DaysoldGt"] = TextBoxStockBatchAge2.Text;
    }
    //#region TextBoxFromDate_TextChanged
    //protected void TextBoxFromDate_TextChanged(object sender, EventArgs e)
    //{
    //    Session["FromDate"] = TextBoxFromDate.Text;
    //}
    //#endregion "TextBoxFromDate_TextChanged"

    //#region TextBoxToDate_TextChanged
    //protected void TextBoxToDate_TextChanged(object sender, EventArgs e)
    //{
    //    Session["ToDate"] = TextBoxToDate.Text + " 23:59:59";
    //}
    //#endregion "TextBoxToDate_TextChanged"
}
