<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateRange.ascx.cs" Inherits="Common_DateRange" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    .auto-style3 {
        width: 69px;
    }
    .auto-style4 {
        width: 134px;
    }
    .auto-style9 {
        width: 97px;
        font-family: Segoe UI
    }
    .auto-style10 {
        width: 79px;
    }
    .auto-style11 {
        width: 79px;
        font-family: Segoe UI
    }
</style>

<table border="0"  id="dateRageTable" runat="server">
    <tr>
        <td class="auto-style9">From Date</td>
        <td class="auto-style3">
            <telerik:RadDatePicker Calendar-FastNavigationSettings-EnableTodayButtonSelection="true" ID="RadDatePicker1" runat="server" OnSelectedDateChanged="RadDatePicker_SelectedDateChanged" >
			    <Calendar runat="server"> 
                    <FooterTemplate> 
                        <div style="width: 100%; text-align: center; background-color: Gray;"> 
                            <input id="Button1" type="button" value="Today" onclick="GoToToday(1)" /> 
                        </div> 
                    </FooterTemplate> 
                </Calendar> 

<DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%">
<EmptyMessageStyle Resize="None"></EmptyMessageStyle>

<ReadOnlyStyle Resize="None"></ReadOnlyStyle>

<FocusedStyle Resize="None"></FocusedStyle>

<DisabledStyle Resize="None"></DisabledStyle>

<InvalidStyle Resize="None"></InvalidStyle>

<HoveredStyle Resize="None"></HoveredStyle>

<EnabledStyle Resize="None"></EnabledStyle>
</DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
			</telerik:RadDatePicker>

        </td>
        <td class="auto-style11">To Date</td>
        <td class="auto-style4">
		<telerik:RadDatePicker Calendar-FastNavigationSettings-EnableTodayButtonSelection="true" ID="RadDatePicker2" runat="server" OnSelectedDateChanged="RadDatePicker_SelectedDateChanged" >
		    <Calendar runat="server"> 
                <FooterTemplate> 
                    <div style="width: 100%; text-align: center; background-color: Gray;"> 
                        <input id="Button1" type="button" value="Today" onclick="GoToToday(2)" /> 
                    </div> 
                </FooterTemplate> 
            </Calendar>
		</telerik:RadDatePicker>
        </td>
    </tr>
</table>

<script type="text/javascript">
function GoToToday(n) { 
        var datepicker = n == 1 ? $find("<%=RadDatePicker1.ClientID%>") : $find("<%=RadDatePicker2.ClientID%>"); 
        var dt = new Date(); 
        datepicker.set_selectedDate(dt); 
		datepicker.hidePopup();
    } 
	
</script>