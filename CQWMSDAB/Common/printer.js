﻿//=============================================================

function SetPrinterValue(val){
	if ($('NLWAX1_hdfPrinter') != null)
		$('NLWAX1_hdfPrinter').value = val;
}
//=============================================================

function GetPrinterValue(){
	if ($('NLWAX1_hdfPrinter') != null)
		return $('NLWAX1_hdfPrinter').value;
}
//=============================================================

function GetPrintersList(){
  if($('NLWAX1_hdfPrinterList') != null)
		return $('NLWAX1_hdfPrinterList').value;
}
//=============================================================

function DisablePrintButton(Show){
  if (getText($('lblLabel')) == "No Label Selected")
    Show = true ;
    
  $('btnPrint').disabled = Show;
  $('btnDirectPrint').disabled = Show;
}
//=============================================================

function PrinterSelected(pName, clearPort){
  // Persist selected Printer in hidden field
  SetPrinterValue(pName);
  
  if(clearPort == true){
    // Clear out any previously selected port
    SetPortValue('');
		//Disable Print Button
		DisablePrintButton(true);
  }
  
  // Update Printer Label/color
  ShowPrinterName(pName);
  
  // Retrieve Printer's associated port, if any
  GetPrinterPort(pName);
  }
//=============================================================

function ShowPrinterName(PrinterName){
  if(PrinterName == ''){
    ShowErrorMsg($('spPrinter'), 'No Printer Selected');
  }
  else{
    ShowPlainMsg($('spPrinter'), PrinterName);
  }
}
//=============================================================