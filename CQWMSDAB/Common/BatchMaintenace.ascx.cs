using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_BatchMaintenace : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ReturnedId"] = -1;

                if (Session["ReceiptId"] == null)
                    Session["ReceiptId"] = -1;
            }
        }
        catch { }
    }

    #region DetailsViewInsertBatch_OnDataBound
    protected void DetailsViewInsertBatch_OnDataBound(object sender, EventArgs e)
    {
        try
        {
            Session["ReturnedId"] = -1;

            Product product = new Product();
            DataSet ds = new DataSet();

            ds = product.GetProductsByStorageUnit(Session["ConnectionStringName"].ToString(), (int)Session["StorageUnitId"]);

            //if (Session["DefaultMode"] != null)
            //    if (Session["DefaultMode"].ToString() == "Insert")
            //        DetailsViewInsertBatch.ChangeMode(DetailsViewMode.Insert);

            switch (DetailsViewInsertBatch.CurrentMode.ToString())
            {
                case "ReadOnly":
                    ((Label)(DetailsViewInsertBatch.FindControl("LabelProductCode"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["ProductCode"].Ordinal).ToString();
                    ((Label)(DetailsViewInsertBatch.FindControl("LabelProduct"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Product"].Ordinal).ToString();
                    break;
                case "Insert":
                    ((Label)(DetailsViewInsertBatch.FindControl("LabelProductCodeInsert"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["ProductCode"].Ordinal).ToString();
                    ((Label)(DetailsViewInsertBatch.FindControl("LabelProductInsert"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Product"].Ordinal).ToString();
                    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxIncubationDaysInsert"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["CuringPeriodDays"].Ordinal).ToString();
                    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxShelfLifeInsert"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["ShelfLifeDays"].Ordinal).ToString();
                    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxExpiryDateInsert"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["ExpiryDate"].Ordinal).ToString();
                    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxProdDateInsert"))).Text = DateTime.Today.ToShortDateString();
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQBatchReferenceInsert"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 121);
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQProdDateInsert"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 123);
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQExpiryDateInsert"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124);
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQECLNumberInsert"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 125);
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQShelfLifeInsert"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 126);

                    //((Label)(DetailsViewInsertBatch.FindControl("LabelProductCode"))).Text = Session["ProductCode"].ToString();
                    //((Label)(DetailsViewInsertBatch.FindControl("LabelProductName"))).Text = Session["Product"].ToString();

                    //int ShelfLifeDays = int.Parse(productDataset.Tables[0].Rows[0]["shelfLifeDays"].ToString());

                    //((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxShelfLife"))).Text = ShelfLifeDays.ToString();
                    //((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxExpiryDate"))).Text = DateTime.Now.AddDays(ShelfLifeDays).ToString();

                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 120))//||
                        //Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), -1, (int)Session["StorageUnitId"]) == LotAttributeRule.AutoGenerate)
                    {
                        // Generate Number
                        ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxBatchNumberInsert"))).Text = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

                        // Disable Editing
                        ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxBatchNumberInsert"))).Enabled = false;
                    }
                    //else
                    //{
                    //    //Provide Drop down for Batch List
                    //    ((DropDownList)(DetailsViewInsertBatch.FindControl("DropDownBatchNumberInsert"))).Visible = true;
                    //    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxBatchNumberInsert"))).Visible = false;
                    //    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQBatchNumberInsert"))).Enabled = false;
                    //}
                    break;
                case "Edit":
                    ((Label)(DetailsViewInsertBatch.FindControl("LabelProductCodeEdit"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["ProductCode"].Ordinal).ToString();
                    ((Label)(DetailsViewInsertBatch.FindControl("LabelProductEdit"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Product"].Ordinal).ToString();
                    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxIncubationDaysEdit"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["CuringPeriodDays"].Ordinal).ToString();
                    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxShelfLifeEdit"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["ShelfLifeDays"].Ordinal).ToString();
                    //((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxExpiryDateEdit"))).Text = ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["ExpiryDate"].Ordinal).ToString();
                    //((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxProdDateEdit"))).Text = DateTime.Today.ToShortDateString();
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQBatchReferenceEdit"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 121);
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQProdDateEdit"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 123);
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQExpiryDateEdit"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124);
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQECLNumberEdit"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 125);
                    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQShelfLifeEdit"))).Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 126);

                    //if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 120))
                    //{
                    //    // Generate Number
                    //    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxBatchNumberEdit"))).Text = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

                    //    // Disable Editing
                    //    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxBatchNumberEdit"))).Enabled = false;
                    //}
                    //else
                    //{
                    //    //Provide Drop down for Batch List
                    //    ((DropDownList)(DetailsViewInsertBatch.FindControl("DropDownBatchNumberEdit"))).Visible = true;
                    //    ((TextBox)(DetailsViewInsertBatch.FindControl("TextBoxBatchNumberEdit"))).Visible = false;
                    //    ((RequiredFieldValidator)(DetailsViewInsertBatch.FindControl("REQBatchNumberEdit"))).Enabled = false;
                    //}
                    break;
            }
        }
        catch { }
    }
    #endregion DetailsViewInsertBatch_OnDataBound

    #region ObjectDataSourceBatch_Inserting
    protected void ObjectDataSourceBatch_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        try
        {
            DateTime expiryDate;
            DateTime productionDate;

            if (DetailsViewInsertBatch.CurrentMode.ToString() == "Edit")
                DateTime.TryParse(((TextBox)DetailsViewInsertBatch.FindControl("TextBoxExpiryDateEdit")).Text, out expiryDate);
            else
                DateTime.TryParse(((TextBox)DetailsViewInsertBatch.FindControl("TextBoxExpiryDateInsert")).Text, out expiryDate);

            if (expiryDate < DateTime.Today.ToUniversalTime())
                expiryDate = DateTime.Today.ToUniversalTime();

            e.InputParameters["expiryDate"] = expiryDate;

            if (DetailsViewInsertBatch.CurrentMode.ToString() == "Edit")
                DateTime.TryParse(((TextBox)DetailsViewInsertBatch.FindControl("TextBoxProdDateEdit")).Text, out productionDate);
            else
                DateTime.TryParse(((TextBox)DetailsViewInsertBatch.FindControl("TextBoxProdDateInsert")).Text, out productionDate);

            if (productionDate < DateTime.Today.ToUniversalTime())
                productionDate = DateTime.Today.ToUniversalTime();

            e.InputParameters["productionDateTime"] = productionDate;

            if (DetailsViewInsertBatch.CurrentMode.ToString() == "Insert")
            if (((TextBox)DetailsViewInsertBatch.FindControl("TextBoxBatchNumberInsert")).Visible)
            {
                Session["BatchId"] = -1;

                e.InputParameters["batch"] = ((TextBox)DetailsViewInsertBatch.FindControl("TextBoxBatchNumberInsert")).Text;
            }
            //else
            //{
            //    Int32 batchId = -1;

            //    int.TryParse(((DropDownList)DetailsViewInsertBatch.FindControl("DropDownBatchNumberInsert")).SelectedValue.ToString(), out batchId);

            //    e.InputParameters["batchId"] = batchId;

            //    Session["BatchId"] = batchId;
            //}
        }
        catch { }
    }
    #endregion ObjectDataSourceBatch_Inserting

    #region ButtonPrintLabel_Click
    protected void ButtonPrintLabel_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();

            checkedList.Add((int)DetailsViewInsertBatch.DataKey.Values["BatchId"]);

            Session["checkedList"] = checkedList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 406))
            {
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 407))
                    Session["LabelName"] = "Batch Label Small.lbl";
                else
                    Session["LabelName"] = "Batch Label Large.lbl";
            }
            else if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 382))
                Session["LabelName"] = "Sensory FX Label Large.lbl";
            else
            {
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                    Session["LabelName"] = "Sample Label Small.lbl";
                else
                    Session["LabelName"] = "Sample Label Large.lbl";
            }

            if (Session["FromURL"] == null)
                Session["FromURL"] = "~/StaticInfo/BatchMaintenance.aspx";

            Session["LabelCopies"] = int.Parse(TextBoxQty.Text);

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
        }
        catch { }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
    #endregion ButtonPrintLabel_Click

    #region ObjectDataSourceBatch_Inserted
    protected void ObjectDataSourceBatch_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        Session["StorageUnitBatchId"] = e.ReturnValue;

        GridViewWarning.DataBind();

        DetailsViewInsertBatch.ChangeMode(DetailsViewMode.ReadOnly);
    }
    #endregion ObjectDataSourceBatch_Inserted
}
