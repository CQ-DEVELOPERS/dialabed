﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StatusDropdown.ascx.cs" Inherits="Common_StatusDropdown" %>

<asp:DropDownList ID="DropDownListStatus" runat="server" DataTextField="<%$ Resources:Default, Status%>" DataValueField="StatusId"
	DataSourceID="ObjectDataSourceStatus" Width="150px" OnSelectedIndexChanged="DropDownListStatus_SelectedIndexChanged">
</asp:DropDownList>
<asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
	SelectMethod="GetStatus">
	<SelectParameters>
		<asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
		<asp:Parameter Name="Type" Type="string" DefaultValue="IS" />
	</SelectParameters>
</asp:ObjectDataSource>