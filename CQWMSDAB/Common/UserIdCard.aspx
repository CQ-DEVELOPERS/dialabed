<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="UserIdCard.aspx.cs"
    Inherits="Common_GenericLabel"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelTitle" runat="server" Text="From User"></asp:Label>
    <asp:DropDownList ID="ddlStartUser" runat="server" DataSourceID="OperatorList" DataTextField="Operator" DataValueField="OperatorId"></asp:DropDownList>
    
    <asp:Label ID="LabelBarcode" runat="server" Text="To User"></asp:Label>
    <asp:DropDownList ID="ddlEndUser" runat="server" DataSourceID="OperatorList" DataTextField="Operator" DataValueField="OperatorId"></asp:DropDownList>
    <asp:Button ID="ButtonPrint" runat="server" Text="Print" OnClick="ButtonPrint_Click" />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
    <EmptyDataTemplate>         
       
    </EmptyDataTemplate>        
    </asp:GridView>
    <asp:ObjectDataSource ID="OperatorList" runat="server" TypeName="Operator" SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="warehouseId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>