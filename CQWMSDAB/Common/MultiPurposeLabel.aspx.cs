using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_MultiPurposeLabel : System.Web.UI.Page
{
    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            string title = Request.QueryString["Title"];

            if (title == "" || title == null)
                title = "Unknown";

            Session["Title"] = title;

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintLabel_Click";

        try
        {
            LabelVariables label = new LabelVariables();
            ArrayList rowList = new ArrayList();

            int quantity = 0;
            int count = 0;
            int value = -1;

            if (!int.TryParse(TextBoxQuantity.Text, out quantity))
            {
                Master.MsgText = "";
                Master.ErrorText = "Invalid Quantity";
            }

            while (count < quantity)
            {
                count++;

                if (Session["Title"].ToString() == "Pallet Label")
                {
                    value = label.SetPalletLabel(Session["ConnectionStringName"].ToString());
                }
                else
                {
                    value = label.SetReferenceNumber(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"]);
                }
                if (value == -1)
                    break;
                else
                    rowList.Add(value);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Session["LabelCopies"] = int.Parse(TextBoxCopies.Text);
            Session["LabelName"] = "Multi Purpose Label.lbl";

            Session["FromURL"] = "~/Common/MultiPurposeLabel.aspx?Title=" + Session["Title"].ToString();

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionCheckedListCount = (ArrayList)Session["checkedList"];
            var numberOfLabels = sessionCheckedListCount.Count;

            for (int i = 0; i < numberOfLabels; i++)
            {
                var sessionPort = Session["Port"];
                var sessionPrinter = Session["Printer"];
                var sesionIndicatorList = Session["indicatorList"];
                var sessionCheckedList = Session["checkedList"];
                var sessionLocationList = Session["locationList"];
                var sessionConnectionString = Session["ConnectionStringName"];
                var sessionTitle = Session["Title"];
                var sessionBarcode = Session["Barcode"];
                var sessionWarehouseId = Session["WarehouseId"];
                var sessionProductList = Session["ProductList"];
                var sessionTakeOnLabel = Session["TakeOnLabel"];
                var sessionLabelCopies = Session["LabelCopies"];

                NLWAXPrint nl = new NLWAXPrint();
                nl.onSessionUpdate += Nl_onSessionUpdate;
                string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                          Session["LabelName"].ToString(),
                                                          sessionPrinter,
                                                          sessionPort,
                                                          sesionIndicatorList,
                                                          sessionCheckedList,
                                                          sessionLocationList,
                                                          sessionConnectionString,
                                                          sessionTitle,
                                                          sessionBarcode,
                                                          sessionWarehouseId,
                                                          sessionProductList,
                                                          sessionTakeOnLabel,
                                                          sessionLabelCopies);
                if (printResponse == string.Empty)
                {
                    Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
                }
                else
                {
                    Master.MsgText = "Label has been printed successfuly.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MultiPurposeLabel " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager or contact support.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }


    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "PutawayMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "PutawayMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
