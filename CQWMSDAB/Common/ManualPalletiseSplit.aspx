<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManualPalletiseSplit.aspx.cs" Inherits="Common_ManualPalletiseSplit"
    Title="<%$ Resources:Default, ManualPalletiseSplitTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ManualPalletiseSplitTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ManualPalletiseSplitAgenda %>"></asp:Label>
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptLine">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:GridView id="GridViewReceiptLine" runat="server" AutoGenerateColumns="false" DataKeyNames="JobId,InstructionId" AllowPaging="true">
                            <Columns>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default,JobId %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="InstructionId" HeaderText="<%$ Resources:Default,InstructionId %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>" SortExpression="Product" />
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>" SortExpression="SKUCode" />
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default,Quantity %>" SortExpression="Quantity" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                            <ContentTemplate>
                                <asp:GridView id="GridViewQuantity" runat="server" DataKeyNames="SplitQuantity" Visible="True" DataSourceID="ObjectDataSourceQuantity">
                                </asp:GridView>
                                
                                <asp:ObjectDataSource id="ObjectDataSourceQuantity" runat="server"
                                    TypeName="ManualPalletiseSplit"
                                    SelectMethod="GetSplitTotals">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:SessionParameter Name="instructionId" Type="Int32" SessionField="InstructionId" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonCreateLines" EventName="Click"></asp:AsyncPostBackTrigger>
                                <asp:AsyncPostBackTrigger ControlID="ButtonGetNext" EventName="Click"></asp:AsyncPostBackTrigger>
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNext" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, EnterNoPutawayLines%>"></asp:Label>
    <asp:TextBox ID="TextBoxLines" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonCreateLines" runat="server" Text="<%$ Resources:Default, CreateLines%>" OnClick="ButtonCreateLines_Click" />
    <br />
    <br />
    <asp:Button ID="ButtonAutoLocations" runat="server" Text="<%$ Resources:Default, AutoAllocation%>" OnClick="ButtonAutoLocations_Click" />
    <asp:Button ID="ButtonManualLocations" runat="server" Text="<%$ Resources:Default, ManualAllocation%>" OnClick="ButtonManualLocations_Click" />
    <asp:Button ID="ButtonSerial" runat="server" Text="<%$ Resources:Default, LinkSerial%>" OnClick="ButtonSerial_Click" />
    <br />
    <asp:Button ID="ButtonGetNext" runat="server" Text="<%$ Resources:Default, GetNext%>" OnClick="ButtonGetNext_Click"></asp:Button>
    <asp:Button ID="ButtonReturn" runat="server" Text="<%$ Resources:Default, Return%>" OnClick="ButtonReturn_Click" />
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
        <ContentTemplate>
            <asp:GridView id="GridViewInstruction" runat="server"
                AllowPaging="True"
                DataKeyNames="InstructionId" 
                AutoGenerateColumns="False"
                DataSourceID="ObjectDataSourceInstruction"
                OnRowUpdating="GridViewInstruction_RowUpdating"
                OnRowUpdated="GridViewInstruction_RowUpdated">
                <Columns>
                    <asp:CommandField ShowEditButton="True" ></asp:CommandField>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server" Checked="true"></asp:CheckBox>                        
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="InstructionId"  ReadOnly="True" SortExpression="InstructionId" HeaderText="<%$ Resources:Default, InstructionId %>"></asp:BoundField>
                    <asp:BoundField DataField="Quantity" SortExpression="Quantity" HeaderText="<%$ Resources:Default,Quantity %>"></asp:BoundField>
                    <asp:BoundField DataField="Status"  ReadOnly="True" SortExpression="Status" HeaderText="<%$ Resources:Default,Status %>"></asp:BoundField>
                    <asp:BoundField DataField="StoreLocation"  ReadOnly="True" SortExpression="StoreLocation" HeaderText="<%$ Resources:Default,StoreLocation %>"></asp:BoundField>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, TotalQuantitySplit%>">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("SplitQuantity") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemStyle Wrap="False" />
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("SplitQuantity") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            
            <asp:ObjectDataSource id="ObjectDataSourceInstruction" runat="server"
                TypeName="ManualPalletiseSplit"
                SelectMethod="GetInstructions"
                UpdateMethod="SetQuantityFieldSplit">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="instructionId" Type="Int32" SessionField="InstructionId" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="instructionId" Type="Int32" />
                    <asp:Parameter Name="quantity" Type="Decimal" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonCreateLines" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonAutoLocations" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNext" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
