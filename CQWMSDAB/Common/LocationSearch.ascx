<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationSearch.ascx.cs" Inherits="Common_LocationSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelLocation" runat="server" Text="<%$ Resources:Default, Location%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxLocation" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
</table>

<asp:GridView ID="GridViewLocationSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataKeyNames="LocationId"
    OnSelectedIndexChanged="GridViewLocationSearch_OnSelectedIndexChanged"
    OnPageIndexChanging="GridViewLocationSearch_PageIndexChanging">
    <Columns>
        <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location%>" />
    </Columns>
    <EmptyDataTemplate>
        <h5>No rows</h5>
    </EmptyDataTemplate>
</asp:GridView>