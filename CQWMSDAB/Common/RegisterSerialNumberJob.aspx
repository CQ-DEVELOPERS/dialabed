<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="RegisterSerialNumberJob.aspx.cs"
    Inherits="Common_RegisterSerialNumberJob"
    Title="<%$ Resources:Default, RegisterSerialNumberJobTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, RegisterSerialNumberJobTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, RegisterSerialNumberJobAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:GridView ID="GridViewSerialNumber" runat="server" DataKeyNames="JobId,InstructionId" DataSourceID="ObjectDataSourceDetails" AutoGenerateColumns="false" AutoGenerateSelectButton="true" OnSelectedIndexChanged="GridViewSerialNumber_SelectedIndexChanged">
        <Columns>
            <asp:BoundField DataField="InstructionId" HeaderText="<%$ Resources:Default, InstructionId %>"/>
            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, InstructionId %>"/>
            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"/>
            <asp:BoundField DataField="SupplierCode" HeaderText="<%$ Resources:Default, SupplierCode %>"/>
            <asp:BoundField DataField="Supplier" HeaderText="<%$ Resources:Default, Supplier %>"/>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"/>
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>"/>
            <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>"/>
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" SelectMethod="GetSerialDetailsByJob" OnSelecting="ObjectDataSourceDetails_OnSelecting" TypeName="SerialNumber">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="jobId" Type="int32"/>
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <asp:Button ID="ButtonFinished" runat="server" Text="<%$ Resources:Default, Finished%>" OnClick="ButtonFinished_Click" />
    
    <asp:UpdatePanel runat="server" ID="UpdatePanelRepeaterSerialNumber">
        <contenttemplate>
            <asp:Label ID="LabelError" runat="server" ForeColor="Red"></asp:Label>
            <br />
            <table border="solid" cellpadding="5">
                <tr>
                    <td valign="top">
                        <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewLinked">
                            <ContentTemplate>
                                <asp:Label ID="LabelLinkedOrders" runat="server" Text="<%$ Resources:Default, LinkedOrders%>" Font-Bold="true"></asp:Label>
                                <asp:GridView ID="GridViewLinked" runat="server" DataKeyNames="SerialNumberId,InstructionId" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceLinked"
                                    OnSelectedIndexChanged="GridViewLinked_SelectedIndexChanged">
                                    <Columns>
                                        <asp:CommandField SelectText="<%$ Resources:Default, Remove%>" ShowSelectButton="True" />
                                        <asp:BoundField DataField="SerialNumber" HeaderText="<%$ Resources:Default, SerialNumber %>"/>                                    </Columns>
                                </asp:GridView>
                                <asp:ObjectDataSource ID="ObjectDataSourceLinked" runat="server" SelectMethod="GetLinkedSerialNumbers" OnSelecting="ObjectDataSourceLinked_OnSelecting" TypeName="SerialNumber">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:Parameter Name="InstructionId" Type="int32"/>
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ContentTemplate>
                            <triggers>
                                <asp:asyncpostbacktrigger controlid="GridViewSerialNumber" eventname="SelectedIndexChanged"></asp:asyncpostbacktrigger>
                                <asp:asyncpostbacktrigger controlid="GridViewLinked" eventname="SelectedIndexChanged"></asp:asyncpostbacktrigger>
                            </triggers>
                        </asp:updatepanel>
                    </td>
                    <td valign="top">
                        <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewUnlinked">
                            <ContentTemplate>
                                <asp:GridView ID="GridViewUnlinked" runat="server" DataKeyNames="JobId,SerialNumberId" OnSelectedIndexChanged="GridViewUnlinked_SelectedIndexChanged" AllowPaging="true" AutoGenerateColumns="false" DataSourceID="ObjectDataSourceUnlinked">
                                    <Columns>
                                        <asp:CommandField SelectText="<%$ Resources:Default, Add%>" ShowSelectButton="True" />
                                        <asp:BoundField DataField="SerialNumber" HeaderText="<%$ Resources:Default, SerialNumber %>"/>
                                    </Columns>
                                </asp:GridView>
                                <asp:ObjectDataSource ID="ObjectDataSourceUnlinked" runat="server" SelectMethod="GetUnlinkedSerialNumbers" OnSelecting="ObjectDataSourceUnlinked_OnSelecting" TypeName="SerialNumber">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:Parameter Name="jobId" Type="int32"/>
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ContentTemplate>
                            <Triggers>
                                <asp:asyncpostbacktrigger controlid="GridViewSerialNumber" eventname="SelectedIndexChanged"></asp:asyncpostbacktrigger>
                                <asp:asyncpostbacktrigger controlid="GridViewUnlinked" eventname="SelectedIndexChanged"></asp:asyncpostbacktrigger>
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </contenttemplate>
        <triggers>
            <asp:asyncpostbacktrigger controlid="GridViewSerialNumber" eventname="SelectedIndexChanged"></asp:asyncpostbacktrigger>
        </triggers>
    </asp:UpdatePanel>
</asp:Content>

