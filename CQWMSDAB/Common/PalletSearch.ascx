<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PalletSearch.ascx.cs" Inherits="Common_PalletSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <uc1:DateRange ID="DateRange1" runat="server" />
                <%--<asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="Search" />--%>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode%>" Width="100px"></asp:Label>
                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px" OnTextChanged="TextBoxProductCode_TextChanged"></asp:TextBox>
                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product%>" Width="100px"></asp:Label>
                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px" OnTextChanged="TextBoxProduct_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default, SKUCode%>" Width="100px"></asp:Label>
                <asp:TextBox ID="TextBoxSKUCode" runat="server" Width="150px" OnTextChanged="TextBoxSKUCode_TextChanged"></asp:TextBox>
                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch%>" Width="100px"></asp:Label>
                <asp:TextBox ID="TextBoxBatch" runat="server" Width="150px" OnTextChanged="TextBoxBatch_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPalletId" runat="server" Text="<%$ Resources:Default, PalletId%>" Width="100px"></asp:Label>
                <asp:TextBox ID="TextBoxPalletId" runat="server" Width="150px" OnTextChanged="TextBoxPalletId_TextChanged"></asp:TextBox>
                <asp:Label ID="LabelJobId" runat="server" Text="<%$ Resources:Default, JobId%>" Width="100px"></asp:Label>
                <asp:TextBox ID="TextBoxJobId" runat="server" Width="150px" OnTextChanged="TextBoxJobId_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelStatus" runat="server" Text="<%$ Resources:Default, Status%>" Width="100px"></asp:Label>
                <asp:DropDownList ID="DropDownListStatus" runat="server" DataTextField="<%$ Resources:Default, Status%>" DataValueField="StatusId"
                    DataSourceID="ObjectDataSourceStatus" Width="150px" OnSelectedIndexChanged="DropDownListStatus_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
                    SelectMethod="GetStatus">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="Type" Type="string" DefaultValue="PR" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
</asp:Panel>

<ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" 
    FilterType="Numbers" TargetControlID="TextBoxPalletId">
</ajaxToolkit:FilteredTextBoxExtender>

<ajaxToolkit:FilteredTextBoxExtender ID="FTBE2" runat="server" 
    FilterType="Numbers" TargetControlID="TextBoxJobId">
</ajaxToolkit:FilteredTextBoxExtender>

<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />