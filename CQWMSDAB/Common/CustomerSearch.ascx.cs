using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_CustomerSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ExternalCompanyId"] == null)
                Session["ExternalCompanyId"] = -1;
        }
    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewExternalCompany.DataBind();
    }
    #endregion ButtonSearch_Click

    #region GridViewExternalCompany_OnSelectedIndexChanged
    protected void GridViewExternalCompany_OnSelectedIndexChanged(object sender, System.EventArgs e)
    {
        Session["ExternalCompanyShow"] = GridViewExternalCompany.SelectedDataKey["ExternalCompany"].ToString();
        Session["ExternalCompanyId"] = GridViewExternalCompany.SelectedDataKey["ExternalCompanyId"].ToString();

        //TextBoxExternalCompany.Text = GridViewExternalCompany.SelectedDataKey["ExternalCompany"].ToString();
        //TextBoxExternalCompanyCode.Text = GridViewExternalCompany.SelectedRow.Cells[2].Text;
    }
    #endregion "GridViewExternalCompany_OnSelectedIndexChanged"

}
