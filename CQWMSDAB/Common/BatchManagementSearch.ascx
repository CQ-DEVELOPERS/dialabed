<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BatchManagementSearch.ascx.cs" Inherits="Common_BatchManagementSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<div style="float:left;">
    <asp:Label ID="LabelShowOrHide" runat="server"></asp:Label>
</div>

<div style="float:left;">
<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
 
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelBatchNumber" runat="server" Text="<%$ Resources:Default, BatchNumber %>" Width="100px"></asp:Label>
                
            </td>
            <td>
                <asp:TextBox ID="TextBoxBatchNumber" runat="server" Width="150px" OnTextChanged="TextBoxBatchNumber_TextChanged"></asp:TextBox>
            </td>
            
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode %>" Width="100px"></asp:Label>
                
            </td>
            <td>
                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px" OnTextChanged="TextBoxProductCode_TextChanged"></asp:TextBox>
            </td>
            
        </tr>
        
        <tr>
         
            <td>
                <asp:Label ID="LabelBatchStatus" runat="server" Text="<%$ Resources:Default, BatchStatus %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListBatchStatus" runat="server" DataTextField="<%$ Resources:Default, Status%>"
                    DataValueField="StatusId" DataSourceID="ObjectDataSourceBatchStatus" Width="150px" OnSelectedIndexChanged="DropDownListBatchStatus_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product %>" Width="100px"></asp:Label>
                
            </td>
            <td>
                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px" OnTextChanged="TextBoxProduct_TextChanged"></asp:TextBox>
            </td>
           
        </tr>
        
        <tr>
            <td>
                <asp:Label ID="LabelStockBatchAge1" runat="server" Text="<%$ Resources:Default, StockBatchAge1 %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxStockBatchAge1" runat="server" Width="150px" OnTextChanged="TextBoxStockBatchAge1_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelStockBatchAge2" runat="server" Text="<%$ Resources:Default, StockBatchAge2 %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxStockBatchAge2" runat="server" Width="150px" OnTextChanged="TextBoxStockBatchAge2_TextChanged"></asp:TextBox>
            </td>
        </tr>
       
    </table>
    
    <uc1:DateRange ID="DateRange1" runat="server" />
    <br />
    <asp:CheckBox ID="CheckBoxSOHOnly" runat="server" Checked="false" Text="<%$ Resources:Default, StockonHandOnly%>  " TextAlign="Left" AutoPostBack="true" OnCheckedChanged="CheckBoxSOHOnly_Changed" />
</asp:Panel>
</div>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />

<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="Server"
    TargetControlID="PanelSearch"
    ExpandControlID="LabelShowOrHide"
    CollapseControlID="LabelShowOrHide" 
    Collapsed="False"
    TextLabelID="LabelShowOrHide"
    ExpandedText="<<<"
    CollapsedText=">>"
    SuppressPostBack="true" />

<asp:ObjectDataSource ID="ObjectDataSourceBatchStatus" runat="server" SelectMethod="GetBatchStatus" TypeName="Batch">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

