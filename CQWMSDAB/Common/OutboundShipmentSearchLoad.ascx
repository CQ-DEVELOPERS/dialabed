<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutboundShipmentSearchLoad.ascx.cs" Inherits="Common_OutboundShipmentSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<%--<div style="float:left;">
    <asp:Label ID="LabelShowOrHide" runat="server"></asp:Label>
</div>

<div style="float:left;">--%>
<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelOutboundShipmentId" runat="server" Text="<%$ Resources:Default, OutboundShipmentId %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxOutboundShipmentId" runat="server" Width="150px" OnTextChanged="TextBoxOutboundShipmentId_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                    DataTextField="Principal" DataValueField="PrincipalId" Width="150px" OnSelectedIndexChanged="DropDownListPrincipal_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                    SelectMethod="GetPrincipalParameter">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxOrderNumber" runat="server" Width="150px" OnTextChanged="TextBoxOrderNumber_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, OutboundDocumentType %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataTextField="OutboundDocumentType"
                    DataValueField="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentTypeId" Width="150px" OnSelectedIndexChanged="DropDownListOutboundDocumentType_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelCustomerCode" runat="server" Text="<%$ Resources:Default, CustomerCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxCustomerCode" runat="server" Width="150px" OnTextChanged="TextBoxCustomerCode_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelcustomerName" runat="server" Text="<%$ Resources:Default, Customer %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxCustomerName" runat="server" Width="150px" OnTextChanged="TextBoxCustomerName_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxReferenceNumber" runat="server" Width="150px" OnTextChanged="TextBoxReferenceNumber_OnTextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelRoute" runat="server" Text="<%$ Resources:Default, Route %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxRoute" runat="server" Width="150px" OnTextChanged="TextBoxRoute_OnTextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelVehicleRegistration" runat="server" Text="<%$ Resources:Default, VehicleRegistration %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxVehicleRegistration" runat="server" Width="150px" OnTextChanged="TextBoxVehicleRegistration_OnTextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelStatus" runat="server" Text="<%$ Resources:Default, Status %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListStatus" runat="server" DataTextField="Status"
                                  DataValueField="StatusId" DataSourceID="ObjectDataSourceStatus" Width="150px" OnSelectedIndexChanged="DropDownListStatus_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <uc1:DateRange ID="DateRange1" runat="server" />
</asp:Panel>
<%--</div>--%>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />

<%--<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="Server"
    TargetControlID="PanelSearch"
    ExpandControlID="LabelShowOrHide"
    CollapseControlID="LabelShowOrHide" 
    Collapsed="False"
    TextLabelID="LabelShowOrHide"
    ExpandedText="<<<"
    CollapsedText=">>"
    SuppressPostBack="true" />--%>

<asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentTypeId" runat="server" SelectMethod="GetOutboundDocumentTypes"
    TypeName="OutboundDocumentType">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" SelectMethod="GetStatus"
                      TypeName="Status">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="Type" SessionField="Type" Type="String" DefaultValue="IS" />
    </SelectParameters>
</asp:ObjectDataSource>

<%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOutboundShipmentId" runat="server"
    TargetControlID="TextBoxOutboundShipmentId" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOrderNumber" runat="server"
    TargetControlID="TextBoxOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="TextBoxCustomerCode" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="TextBoxCustomerName" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>--%>