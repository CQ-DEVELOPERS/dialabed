﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_DateRange4 : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                string fromDate2 = DateRange.GetFromDate().ToString();
                string toDate2 = DateRange.GetToDate().ToString();

                if (Session["FromDate2"] != null)
                    fromDate2 = Session["FromDate2"].ToString();
                else
                    Session["FromDate2"] = Convert.ToDateTime(fromDate2);

                if (Session["ToDate2"] != null)
                    toDate2 = Session["ToDate2"].ToString();
                else
                    Session["ToDate2"] = Convert.ToDateTime(toDate2);

                TextBoxFromDate2.Text = fromDate2;
                TextBoxToDate2.Text = toDate2;
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    #region TextBoxFromDate2_TextChanged
    protected void TextBoxFromDate2_TextChanged(object sender, EventArgs e)
    {
        Session["FromDate2"] = TextBoxFromDate2.Text;
    }
    #endregion "TextBoxFromDate2_TextChanged"

    #region TextBoxToDate2_TextChanged
    protected void TextBoxToDate2_TextChanged(object sender, EventArgs e)
    {
        Session["ToDate2"] = TextBoxToDate2.Text + " 23:59:59";
    }
    #endregion "TextBoxToDate2_TextChanged"
}
