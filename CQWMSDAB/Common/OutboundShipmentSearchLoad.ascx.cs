using System;
using System.Web.UI;

public partial class Common_OutboundShipmentSearch : UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListOutboundDocumentType.DataBind();
                DropDownListStatus.DataBind();

                if (Session["ParameterShipmentId"] == null)
                    Session["ParameterShipmentId"] = -1;
                else
                    TextBoxOutboundShipmentId.Text = Session["ParameterShipmentId"].ToString();

                if (Session["OrderNumber"] == null)
                    Session["OrderNumber"] = "";
                else
                    TextBoxOrderNumber.Text = Session["OrderNumber"].ToString();

                if (Session["ExternalCompanyCode"] == null)
                    Session["ExternalCompanyCode"] = "";
                else
                    TextBoxCustomerCode.Text = Session["ExternalCompanyCode"].ToString();

                if (Session["ExternalCompany"] == null)
                    Session["ExternalCompany"] = "";
                else
                    TextBoxCustomerName.Text = Session["ExternalCompany"].ToString();

                if (Session["OutboundDocumentTypeId"] == null)
                    Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
                else
                    DropDownListOutboundDocumentType.SelectedValue = Session["OutboundDocumentTypeId"].ToString();

                if (Session["PrincipalId"] == null)
                    try { Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString()); }
                    catch { Session["PrincipalId"] = -1; }
                else
                    DropDownListPrincipal.SelectedValue = Session["PrincipalId"].ToString();

                if (Session["ReferenceNumber"] == null)
                    Session["ReferenceNumber"] = "";
                else
                    TextBoxReferenceNumber.Text = Session["ReferenceNumber"].ToString();

                if (Session["Route"] == null)
                    Session["Route"] = "";
                else
                    TextBoxRoute.Text = Session["Route"].ToString();

                if (Session["VehicleRegistration"] == null)
                    Session["VehicleRegistration"] = "";
                else
                    TextBoxVehicleRegistration.Text = Session["VehicleRegistration"].ToString();

                if (Session["Status"] == null)
                    Session["Status"] = "";
                else
                    DropDownListStatus.SelectedValue = Session["Status"].ToString();
            }

            if (TextBoxOutboundShipmentId.Text == "{All}")
            {
                Session["ParameterShipmentId"] = "-1";
                TextBoxOutboundShipmentId.Text = "";
            }

            if (TextBoxOrderNumber.Text == "{All}")
            {
                Session["OrderNumber"] = "";
                TextBoxOrderNumber.Text = "";
            }

            if (TextBoxCustomerCode.Text == "{All}")
            {
                Session["ExternalCompanyCode"] = "";
                TextBoxCustomerCode.Text = "";
            }

            if (TextBoxCustomerName.Text == "{All}")
            {
                Session["ExternalCompany"] = "";
                TextBoxCustomerName.Text = "";
            }

            if (TextBoxReferenceNumber.Text == "{All}")
            {
                Session["ReferenceNumber"] = "";
                TextBoxReferenceNumber.Text = "";
            }

            if (TextBoxRoute.Text == "{All}")
            {
                Session["Route"] = "";
                TextBoxRoute.Text = "";
            }

            if (TextBoxVehicleRegistration.Text == "{All}")
            {
                Session["VehicleRegistration"] = "";
                TextBoxVehicleRegistration.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxOrderNumber_TextChanged(object sender, EventArgs e)
    {
        Session["OrderNumber"] = TextBoxOrderNumber.Text;
    }

    protected void TextBoxOutboundShipmentId_TextChanged(object sender, EventArgs e)
    {
        Session["ParameterShipmentId"] = TextBoxOutboundShipmentId.Text;
    }
    protected void TextBoxCustomerCode_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompanyCode"] = TextBoxCustomerCode.Text;
    }
    protected void TextBoxCustomerName_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompany"] = TextBoxCustomerName.Text;
    }
    protected void DropDownListOutboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
    }
    protected void DropDownListPrincipal_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString());
    }
    protected void TextBoxReferenceNumber_OnTextChanged(object sender, EventArgs e)
    {
        Session["ReferenceNumber"] = TextBoxReferenceNumber.Text;
    }

    protected void TextBoxRoute_OnTextChanged(object sender, EventArgs e)
    {
        Session["Route"] = TextBoxRoute.Text;
    }

    protected void TextBoxVehicleRegistration_OnTextChanged(object sender, EventArgs e)
    {
        Session["VehicleRegistration"] = TextBoxVehicleRegistration.Text;
    }

    protected void DropDownListStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StatusId"] = int.Parse(DropDownListStatus.SelectedValue.ToString());
    }
}
