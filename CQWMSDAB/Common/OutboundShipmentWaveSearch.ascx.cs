using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_OutboundShipmentWaveSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListOutboundDocumentType.DataBind();

                if (Session["ParameterShipmentId"] == null)
                    Session["ParameterShipmentId"] = -1;
                else
                    TextBoxOutboundShipmentId.Text = Session["ParameterShipmentId"].ToString();

                if (Session["OrderNumber"] == null)
                    Session["OrderNumber"] = "";
                else
                    TextBoxOrderNumber.Text = Session["OrderNumber"].ToString();

                if (Session["ExternalCompanyCode"] == null)
                    Session["ExternalCompanyCode"] = "";
                else
                    TextBoxCustomerCode.Text = Session["ExternalCompanyCode"].ToString();

                if (Session["ExternalCompany"] == null)
                    Session["ExternalCompany"] = "";
                else
                    TextBoxCustomerName.Text = Session["ExternalCompany"].ToString();

                if (Session["OutboundDocumentTypeId"] == null)
                    Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
                else
                    DropDownListOutboundDocumentType.SelectedValue = Session["OutboundDocumentTypeId"].ToString();

                if (Session["PrincipalId"] == null)
                    try { Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString()); }
                    catch { Session["PrincipalId"] = -1; }
                else
                    DropDownListPrincipal.SelectedValue = Session["PrincipalId"].ToString();

                if (Session["WaveId"] == null)
                    try { Session["WaveId"] = int.Parse(DropDownListWave.SelectedValue.ToString()); }
                    catch { Session["WaveId"] = -1; }
                else
                    DropDownListWave.SelectedValue = Session["WaveId"].ToString();

                if (Session["RouteId"] == null)
                    try {
                        ddlRoute.SelectedValue = "-1";
                        Session["RouteId"] = int.Parse(ddlRoute.SelectedValue.ToString()); }
                    catch { Session["RouteId"] = -1; }
                else
                    ddlRoute.SelectedValue = Session["RouteId"].ToString();
            }

            if (TextBoxOutboundShipmentId.Text == "{All}")
            {
                Session["ParameterShipmentId"] = "-1";
                TextBoxOutboundShipmentId.Text = "";
            }

            if (TextBoxOrderNumber.Text == "{All}")
            {
                Session["OrderNumber"] = "";
                TextBoxOrderNumber.Text = "";
            }

            if (TextBoxCustomerCode.Text == "{All}")
            {
                Session["ExternalCompanyCode"] = "";
                TextBoxCustomerCode.Text = "";
            }

            if (TextBoxCustomerName.Text == "{All}")
            {
                Session["ExternalCompany"] = "";
                TextBoxCustomerName.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxOrderNumber_TextChanged(object sender, EventArgs e)
    {
        Session["OrderNumber"] = TextBoxOrderNumber.Text;
    }

    protected void TextBoxOutboundShipmentId_TextChanged(object sender, EventArgs e)
    {
        Session["ParameterShipmentId"] = TextBoxOutboundShipmentId.Text;
    }
    protected void TextBoxCustomerCode_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompanyCode"] = TextBoxCustomerCode.Text;
    }
    protected void TextBoxCustomerName_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompany"] = TextBoxCustomerName.Text;
    }
    protected void DropDownListOutboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
    }
    protected void DropDownListPrincipal_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString());
    }
    protected void DropDownListWave_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["WaveId"] = int.Parse(DropDownListWave.SelectedValue.ToString());
    }

	protected void ddlRoute_SelectedIndexChanged(object sender, EventArgs e)
	{
        Session["RouteId"] = int.Parse(ddlRoute.SelectedValue.ToString());
    }
}
