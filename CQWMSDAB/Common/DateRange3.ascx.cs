using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_DateRange3 : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                string fromDate = DateRange.GetFromDate().ToString();

                if (Session["FromDate"] != null)
                    fromDate = Session["FromDate"].ToString();
                else
                    Session["FromDate"] = Convert.ToDateTime(fromDate);

                TextBoxFromDate.Text = fromDate;
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    #region TextBoxFromDate_TextChanged
    protected void TextBoxFromDate_TextChanged(object sender, EventArgs e)
    {
        Session["FromDate"] = TextBoxFromDate.Text;
    }
    #endregion "TextBoxFromDate_TextChanged"

}
