﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InboundQuerySearch.ascx.cs" Inherits="Common_InboundQuerySearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ Register Src="../Common/StatusSearch.ascx" TagName="StatusSearch" TagPrefix="ucss" %>

<%--<div style="float:left;">
    <asp:Label ID="LabelShowOrHide" runat="server"></asp:Label>
</div>

<div style="float:left;">--%>
<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelShipmentId" runat="server" Text="<%$ Resources:Default, InboundShipmentId %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:radtextbox id="TextBoxInboundShipmentId" runat="server" width="150px" ontextchanged="TextBoxInboundShipmentId_TextChanged" emptymessage="{All}"></telerik:radtextbox>
            </td>
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:radtextbox id="TextBoxOrderNumber" runat="server" width="150px" ontextchanged="TextBoxOrderNumber_TextChanged" emptymessage="{All}"></telerik:radtextbox>
            </td>
            <%-- <td>
                <asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, InboundDocumentType %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataTextField="InboundDocumentType"
                    DataValueField="InboundDocumentTypeId" DataSourceID="ObjectDataSourceInboundDocumentTypeId" Width="150px" OnSelectedIndexChanged="DropDownListInboundDocumentType_SelectedIndexChanged">
                </asp:DropDownList>
            </td>--%>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSupplierCode" runat="server" Text="<%$ Resources:Default, SupplierCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:radtextbox id="TextBoxSupplierCode" runat="server" width="150px" ontextchanged="TextBoxSupplierCode_TextChanged" emptymessage="{All}"></telerik:radtextbox>
            </td>
            <td>
                <asp:Label ID="LabelSupplierName" runat="server" Text="<%$ Resources:Default, Supplier %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:radtextbox id="TextBoxSupplierName" runat="server" width="150px" ontextchanged="TextBoxSupplierName_TextChanged" emptymessage="{All}"></telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                    DataTextField="Principal" DataValueField="PrincipalId" Width="150px" OnSelectedIndexChanged="DropDownListPrincipal_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                    SelectMethod="GetPrincipalParameter">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <asp:Label ID="LabelDeliveryNoteNumber" runat="server" Text="<%$ Resources:Default, DeliveryNoteNumber%>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:radtextbox id="TextBoxDeliveryNoteNumber" runat="server" width="150px" ontextchanged="TextDeliveryNoteNumber_TextChanged" emptymessage="{All}"></telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelStatus" runat="server" Text="<%$ Resources:Default, Status %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListStatus" runat="server" DataTextField="Status"
                    DataValueField="Status" DataSourceID="ObjectDataSourceStatus" Width="150px" OnSelectedIndexChanged="DropDownListStatus_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <uc1:DateRange ID="DateRange1" runat="server" />

</asp:Panel>
<%--</div>--%>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />


<asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentTypeId" runat="server" SelectMethod="GetInboundDocumentTypes"
    TypeName="InboundDocumentType">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" SelectMethod="GetStatus"
    TypeName="Status">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="Type" SessionField="Type" Type="String" DefaultValue="R" />
    </SelectParameters>
</asp:ObjectDataSource>


