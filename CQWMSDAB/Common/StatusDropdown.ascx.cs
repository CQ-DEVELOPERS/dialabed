﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_StatusDropdown : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["StatusId"] == null)
                Session["StatusId"] = -1;
            if (Session["Status"] == null)
                Session["Status"] = -1;
        }
    }
    #endregion Page_Load
	
	protected void DropDownListStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StatusId"] = int.Parse(DropDownListStatus.SelectedValue.ToString());
    }
}
