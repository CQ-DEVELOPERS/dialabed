using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ManageOperators : System.Web.UI.Page
{   Operator op = new Operator();

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewOperators.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            Master.MsgText = "";
            Master.ErrorText = ex.Message.ToString();
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList checkList = new ArrayList();

            Session["checkedList"] = null;

            foreach (GridViewRow row in GridViewOperators.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    checkList.Add(GridViewOperators.DataKeys[row.RowIndex].Values["OperatorId"]);
            }

            if (checkList.Count > 0)
                Session["checkedList"] = checkList;

            Session["LabelName"] = "Sign In Card.lbl";

            Session["FromURL"] = "~/Common/ManageOperators.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            Master.MsgText = "An error has occurred! For more details please check automation manager.";
            Master.ErrorText = ex.Message.ToString();
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
    #endregion ButtonPrint_Click
}
