using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ProductLabel : System.Web.UI.Page
{
    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

            ButtonPrintEAN.Visible = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 352);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region GridViewProductSearch_OnSelectedIndexChanged
    protected void GridViewProductSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StorageUnitBatchId"] = GridViewProductSearch.SelectedDataKey["StorageUnitBatchId"];
        Session["Product"] = GridViewProductSearch.SelectedDataKey["Product"];
    }
    #endregion GridViewProductSearch_OnSelectedIndexChanged

    #region GridViewProductSearch_PageIndexChanging
    protected void GridViewProductSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewProductSearch.PageIndex = e.NewPageIndex;
            GridViewProductSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewProductSearch_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewProductSearch.PageIndex = 0;

        GridViewProductSearch_DataBind();
    }
    #endregion "ButtonSearch_Click"

    #region GridViewProductSearch_DataBind
    protected void GridViewProductSearch_DataBind()
    {
        Product product = new Product();

        GridViewProductSearch.DataSource = product.SearchViewSUL(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxProductCode.Text, TextBoxProduct.Text, TextBoxSKUCode.Text, TextBoxSKU.Text, TextBoxFromLocation.Text, TextBoxToLocation.Text);

        GridViewProductSearch.DataBind();
    }
    #endregion GridViewProductSearch_DataBind

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewProductSearch.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletLabel" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintLabel_Click";

        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 431))
            {
                foreach (GridViewRow row in GridViewProductSearch.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");

                    if (cb.Checked)
                        checkedList.Add((int)GridViewProductSearch.DataKeys[row.RowIndex].Values["StorageUnitId"] * -1);
                }

                if (checkedList.Count < 1)
                    return;

                Session["LabelName"] = "Product Division Label Small.lbl";
            }
            else if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 320))
            {
                foreach (GridViewRow row in GridViewProductSearch.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");

                    if (cb.Checked)
                        checkedList.Add((int)GridViewProductSearch.DataKeys[row.RowIndex].Values["StorageUnitId"] * -1);
                }

                if (checkedList.Count < 1)
                    return;

                Session["LabelName"] = "Customer Product Label Small.lbl";
            }
            else
            {
                ArrayList rowList = new ArrayList();

                foreach (GridViewRow row in GridViewProductSearch.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(GridViewProductSearch.DataKeys[row.RowIndex].Values["ProductCode"].ToString() + "," + GridViewProductSearch.DataKeys[row.RowIndex].Values["Product"].ToString());
                }

                if (rowList.Count < 1)
                    return;

                Session["ProductList"] = rowList;

                checkedList.Add(0);

                Session["LabelName"] = "Product Label Small.lbl";
            }

            Session["checkedList"] = checkedList;

            Session["FromURL"] = "~/Common/ProductLabel.aspx";

            Session["LabelCopies"] = int.Parse(TextBoxQty.Text);

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionproductListCount = (ArrayList)Session["ProductList"];
            var numberOfLabels = sessionproductListCount.Count;

            for (int i = 0; i < numberOfLabels; i++)
            {
                var sessionPort = Session["Port"];
                var sessionPrinter = Session["Printer"];
                var sesionIndicatorList = Session["indicatorList"];
                var sessionCheckedList = Session["checkedList"];
                var sessionLocationList = Session["locationList"];
                var sessionConnectionString = Session["ConnectionStringName"];
                var sessionTitle = Session["Title"];
                var sessionBarcode = Session["Barcode"];
                var sessionWarehouseId = Session["WarehouseId"];
                var sessionProductList = Session["ProductList"];
                var sessionTakeOnLabel = Session["TakeOnLabel"];
                var sessionLabelCopies = Session["LabelCopies"];

                NLWAXPrint nl = new NLWAXPrint();
                nl.onSessionUpdate += Nl_onSessionUpdate;
                string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                          Session["LabelName"].ToString(),
                                                          sessionPrinter,
                                                          sessionPort,
                                                          sesionIndicatorList,
                                                          sessionCheckedList,
                                                          sessionLocationList,
                                                          sessionConnectionString,
                                                          sessionTitle,
                                                          sessionBarcode,
                                                          sessionWarehouseId,
                                                          sessionProductList,
                                                          sessionTakeOnLabel,
                                                          sessionLabelCopies);
                if (printResponse == string.Empty)
                {
                    Master.ErrorText = "An error has occurred! For more details please check automation manager.";
                }
                else
                {
                    Master.MsgText = "Label has been printed successfuly.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductLabel " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
    #endregion ButtonPrint_Click

    #region ButtonPrintEAN_Click
    protected void ButtonPrintEAN_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintEAN_Click";

        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewProductSearch.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    checkedList.Add((int)GridViewProductSearch.DataKeys[row.RowIndex].Values["StorageUnitId"] * -1);

                    if (GridViewProductSearch.DataKeys[row.RowIndex].Values["Barcode"].ToString().Length == 13)
                        Session["LabelName"] = "EAN Product Label Small.lbl";
                    else
                        Session["LabelName"] = "NOTEAN Product Label Small.lbl";
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            Session["FromURL"] = "~/Common/ProductLabel.aspx";

            Session["LabelCopies"] = int.Parse(TextBoxQty.Text);

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletLabel " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrintEAN_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "PutawayMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "PutawayMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
