﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DRMOutboundShipmentSearch.ascx.cs" Inherits="Common_DRMOutboundShipmentSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>


<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelOutboundShipmentId" runat="server" Text="<%$ Resources:Default, OutboundShipmentId %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxOutboundShipmentId" runat="server" Width="150px" OnTextChanged="TextBoxOutboundShipmentId_TextChanged"></asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxOrderNumber" runat="server" Width="150px" OnTextChanged="TextBoxOrderNumber_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, OutboundDocumentType %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataTextField="OutboundDocumentType"
                    DataValueField="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentTypeId" Width="150px" OnSelectedIndexChanged="DropDownListOutboundDocumentType_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelCustomerCode" runat="server" Text="<%$ Resources:Default, CustomerCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxCustomerCode" runat="server" Width="150px" OnTextChanged="TextBoxCustomerCode_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelcustomerName" runat="server" Text="<%$ Resources:Default, Customer %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxCustomerName" runat="server" Width="150px" OnTextChanged="TextBoxCustomerName_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelRoute" runat="server" Text="<%$ Resources:Default, Route %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListRoute" runat="server" DataTextField="Route"
                    DataValueField="RouteId" DataSourceID="ObjectDataSourceRoute" Width="150px" OnSelectedIndexChanged="DropDownListRoute_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="LabelDriver" runat="server" Text="<%$ Resources:Default, ContactDriver %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListDriver" runat="server" DataTextField="ContactPerson"
                    DataValueField="ContactListId" DataSourceID="ObjectDataSourceContact" Width="150px" OnSelectedIndexChanged="DropDownListDriver_SelectedIndexChanged">
                </asp:DropDownList>
            </td>            
        </tr>
    </table>
    <uc1:DateRange ID="DateRange1" runat="server" />
</asp:Panel>

<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />


<asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentTypeId" runat="server" SelectMethod="GetOutboundDocumentTypes"
    TypeName="OutboundDocumentType">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="ObjectDataSourceRoute" runat="server" SelectMethod="GetRoutes"
    TypeName="Route">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="ObjectDataSourceContact" runat="server" SelectMethod="GetDriverByContactId"
    TypeName="Contact">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOutboundShipmentId" runat="server"
    TargetControlID="TextBoxOutboundShipmentId" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOrderNumber" runat="server"
    TargetControlID="TextBoxOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="TextBoxCustomerCode" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="TextBoxCustomerName" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>