<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="DemoReset.aspx.cs"
    Inherits="Common_DemoReset"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Button ID="ButtonSearch" runat="server" Text="Reset" OnClick="ButtonSearch_Click" /><br />
    <ajaxToolkit:ConfirmButtonExtender ID="cbebbSearch" runat="server" TargetControlID="ButtonSearch" ConfirmText="Press OK to reset the entire database."></ajaxToolkit:ConfirmButtonExtender>
</asp:Content>

