using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Services.Protocols;

public partial class NLWAX : System.Web.UI.UserControl
{
    
    PrintService.NPSService NicePrintService = new PrintService.NPSService();
    StorageService.StorageService NPSLabelStorage = new StorageService.StorageService();
    PrintService.FileLocation __FileLocation = new PrintService.FileLocation();
   
    string _errorMessage = "";
    string _errorID;
    string _errorType;

    #region "Public Enums"

    enum Days {Sat=1, Sun, Mon, Tue, Wed, Thu, Fri};
    enum PrintType{ToPrinter=1, ToPort};
    public enum LabelKind { Null=1, MainLabel, HeaderLabel, TailLabel };
    public enum LabelSide { Front=1, Back };
    
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        BaseURL = ConfigurationManager.AppSettings.Get("PrintService.BaseURL");
        SessionName = DateTime.Now.Ticks.ToString();

        if (Session["Printing"] == null)
            Session["Printing"] = false;

        if ((bool)Session["Printing"] == true)
        {
            hdfIsPrinting.Value = "true";
            PrintLabels();
        }
        else/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            if (Session["checkedList"] == null)
                if (Session["FromURL"] != null)
                    Response.Redirect(Session["FromURL"].ToString());
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {

    }

    #region "Public Properties"
    public string IsConnected
    {
        get { return NicePrintService.TestEngineConnection(); }
    }

    public string BaseURL
    {
        get { return Session["BaseURL"].ToString(); }
        set
        {
            hdfBaseURL.Value = value;
            Session["BaseURL"] = value;
        }
    }

    public string SendTo
    {
        get { return Session["SendTo"].ToString(); }
        set
        {
            hdfSendTo.Value = value;
            Session["SendTo"] = value;
        }
    }

    public string Port
    {
        get { return hdfPort.Value; }
        set { hdfPort.Value = value; }
    }

    public string SessionName
    {
        get { return Session["SessionName"].ToString(); }
        set { Session["SessionName"] = value; }
    }

    public string[] LabelGroups
    {
        get
        { 
            return NPSLabelStorage.GetLabelGroups();
        }
    }

    public string LabelGroup
    {
        get { return Session["LabelGroup"].ToString(); }
        set { Session["LabelGroup"] = value; }
    }

    public string[] Labels(string LabelGroup, bool Recursive)
    {
        return NPSLabelStorage.GetLabelList(LabelGroup, Recursive);
    }

    public string Label
    {
        get { return hdfLabel.Value; }
        set
        {
            hdfLabel.Value = value;
            Session["dsVars"] = null;
            Session["PreviewUrl"] = null;
        }
    }

    private PrintService.FileLocation FileLocation
    {
        get
        {
            __FileLocation.LabelFileName = Label;
            __FileLocation.LabelGroup = LabelGroup;
            return __FileLocation;
        }
    }

    public string[] Printers
    {
        get
        {
            string[] obj;
            obj = NicePrintService.GetPrinterList();
            Array.Sort(obj);
            return obj;
        }
    }

    public string Printer
    {
        get { return hdfPrinter.Value; }
        set { hdfPrinter.Value = value; }
    }

    public DataSet Variables
    {
        get
        {
            if(Session["dsVars"] == null)
                try
                {
                    Session["dsVars"] = NicePrintService.GetVariables(FileLocation);
                }
                catch
                {
                    Session["dsVars"] = null;
                }
            
            return (DataSet)Session["dsVars"];
        }
        set
        {
            Session["dsVars"] = value;
            Session["PreviewUrl"] = null;
        }
    }
    #endregion

    #region "Public Functions"
    public string GetPreviewUrl(Int32 Width, Int32 Height, int Kind, int Side)
    {
        if(Session["PreviewUrl"] == null)
            try
            {
                string s;
                //s = BaseURL + NicePrintService.UpdatePreviewFile(Printer, FileLocation, SessionName, Variables, Width, Height, int.Parse(Kind.ToString()), int.Parse(Side.ToString()), true);
                s = BaseURL + NicePrintService.UpdatePreviewFile(Printer, FileLocation, SessionName, Variables, Width, Height, 1, 0, true);
                Session["PreviewUrl"] = s;
            }
            catch (SoapException ex)
            {
                //'Dim eh = New NPSExceptionHelper(ex)
                //'_errorType = eh.ErrorType
                //'_errorMessage = eh.Message
                //'_errorID = eh.ErrorID
                //'If _errorID = 0 Then
                //'    _errorID = -1
                //'End If
                //'If _errorMessage = "" Then
                //'    _errorMessage = "Unknown exception!"
                //'End If
                Session["PreviewUrl"] = "";
            }
            catch (Exception ex)
            {
                _errorType = "General Exception";
                _errorMessage = ex.Message;
                _errorID = "-1";
                Session["PreviewUrl"] = "";
            }
        
        return Session["PreviewUrl"].ToString();
    }

    public string GetVarFormatName(long ID)
    {
        switch(ID)
        {
            case 0:
                return "All";
            case 1:
                return "Numeric";
            case 2:
                return "AlphaNumeric";
            case 3:
                return "Letters";
            case 4:
                return "7bit";
            case 5:
                return "Hex";
            case 6:
                return "Date";
            case 7:
                return "Time";
            default:
                return "Unknown";
        }
    }

    public bool Print(Int32 Quantity, bool IsReprint)
    {
        try
        {
            if(!IsReprint)
                hdfFile.Value = CreatePrintFile(Quantity.ToString());
            
            hdfIsPrinting.Value = "true";
            return true;
        }
        catch (Exception ex)
        {
            _errorType = "General Exception";
            _errorMessage = ex.Message;
            _errorID = "-1";
            return false;
        }
    }

    private string CreatePrintFile(string Quantity)
    {
        string sFile;

        try
        {
            sFile = NicePrintService.CreatePrintFile(Printer, FileLocation, Quantity, SessionName, Variables);
            return sFile;
        }
        catch (SoapException ex)
        {
            //'Dim eh = New NPSExceptionHelper(ex)
            //'_errorType = eh.ErrorType
            //'_errorMessage = eh.Message
            //'_errorID = eh.ErrorID
            sFile = "";
            return sFile;
        }
        catch (Exception ex)
        {
            _errorType = "General Exception";
            _errorMessage = ex.Message;
            _errorID = "-1";
            sFile = "";
            return sFile;
        }
    }

    public bool DirectPrint(Int32 Quantity)
    {
        try
        {
            NicePrintService.Print(Printer, FileLocation, Quantity.ToString(), Variables);
            return true;
        }
        catch (SoapException ex)
        {
            //'Dim eh = New NPSExceptionHelper(ex)
            //'_errorType = eh.ErrorType
            //'_errorMessage = eh.Message
            //'_errorID = eh.ErrorID
            return false;
        }
        catch (Exception ex)
        {
            _errorType = "General Exception";
            _errorMessage = ex.Message;
            _errorID = "-1";
            return false;
        }
    }
    #endregion

    #region "Error handling routines"
    public void ClearError()
    {
        _errorMessage = "";
        _errorID = "0";
        _errorType = "";
    }

    public string ErrorMessage
    {
        get { return _errorMessage; }
    }

    public string ErrorType
    {
        get { return _errorType; }
    }

    public int ErrorID
    {
        get { return int.Parse(_errorID); }
    }
    #endregion "Error handling routines"

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        PrintLabels();
    }

    public void PrintLabels()
    {
        ClearError();

        SetLabelName();

        SendTo = "To Port";
        //NLWAX1.SendTo = "To Printer";

        Printer = Session["Printer"].ToString();
        Port = Session["Port"].ToString();
        
        Configuration config = new Configuration();

        int quantity = 1;

        if (Session["quantityList"] != null)
        {
            ArrayList quantityList = (ArrayList)Session["quantityList"];
            quantity = (int)quantityList[0];
            quantityList.RemoveAt(0);

            if(quantityList.Count > 0)
                Session["quantityList"] = quantityList;
            else
                Session["quantityList"] = null;
        }
        else if (Session["LabelCopies"] != null)
        {
            quantity = (int)Session["LabelCopies"];

            if(Session["checkedList"] == null)
                Session["LabelCopies"] = null;
        }
        else
            quantity = config.GetDescriptionIntValue(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString());
        
        int sleep = 0;

        //Print
        Print(quantity, false);

        if (Session["checkedList"] != null)
        {
            if (Session["Printer"].ToString() == "Intermec 3400D" || Session["Printer"].ToString() == "Intermec 3400A")
                sleep = config.GetIntValue(Session["ConnectionStringName"].ToString(), 82);
            else
                sleep = config.GetIntValue(Session["ConnectionStringName"].ToString(), 81);

            System.Threading.Thread.Sleep(sleep);
        }

        //Reprint
        //NLWAX1.Print(int.Parse(txtQuant.Text), true);

        //DirectPrint
        //NLWAX1.DirectPrint(int.Parse(txtQuant.Text));

        CheckError();
    }

    protected void SetLabelName()
    {
        string FolderPath = "CQuential Solutions\\Generic";

        FolderPath = Configuration.GetStringValue(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 212);

        if (Session["Printer"].ToString() == "Intermec 3400D" || Session["Printer"].ToString() == "Intermec 3400A")
            FolderPath = "CQuential Solutions\\Intermec 3400D";

        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 212))
            FolderPath = "CQuential Solutions\\Landscape";

        LabelGroup = FolderPath;
        Label = Session["LabelName"].ToString();
        GetVariables();
    }

    protected void GetVariables()
    {
        LabelVariables ds = new LabelVariables();

        if (Session["checkedList"] != null)
        {
            ArrayList checkList = (ArrayList)Session["checkedList"];
            int locationId = -1;

            try
            {
                if (Session["locationList"] != null)
                {
                    ArrayList locationList = (ArrayList)Session["locationList"];

                    locationId = (int)locationList[0];

                    locationList.Remove(locationId);

                    if (locationList.Count < 1)
                        Session["locationList"] = null;
                    else
                        Session["locationList"] = locationList;
                }
            }
            catch
            {
                locationId = -1;
            }
            
            if (checkList.Count > 0)
            {
                int key = -1;
                
                string barcode = "";

                try
                {
                    key = (int)checkList[0];

                    checkList.Remove(key);
                }
                catch
                {
                    barcode = checkList[0].ToString();
                    checkList.Remove(barcode);
                }

                switch (Session["LabelName"].ToString())
                {
                    case "Pallet Label.lbl":
                        if (Session["TakeOnLabel"] == null)
                            Variables = ds.GetInstructionLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        else
                            Variables = ds.GetPalletLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Pallet Label Small.lbl":
                        if (Session["TakeOnLabel"] == null)
                            Variables = ds.GetInstructionLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        else
                            Variables = ds.GetPalletLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Despatch By Order Label.lbl":
                        Variables = ds.GetJobLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
					case "Despatch By Order Label Plumblink.lbl":
                        Variables = ds.GetJobLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Despatch By Route Label.lbl":
                        Variables = ds.GetJobLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Pick Job Label.lbl":
                        Variables = ds.GetJobLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Receiving & Staging Label.lbl":
                        Variables = ds.GetInstructionLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Receiving & Staging Label Large.lbl":
                        Variables = ds.GetInstructionLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Reject Label.lbl":
                        Variables = ds.GetJobLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Multi Purpose Label.lbl":
                        if (key == -1)
                            Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), barcode);
                        else
                            Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), key.ToString());
                        break;
                    case "Sample Label Large.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Sample Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Product Batch Label Large.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Product Batch Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Picking Label Large.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Picking Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Verify Batch Label Large.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Verify Batch Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Verify Batch Label Large Version 2.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key, locationId);
                        break;
                    case "Receiving Batch Label Large.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Receiving Batch Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Sign In Card.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Customer Product Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "UTI Product Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "UTI Product Label Large.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "EAN Product Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "NOTEAN Product Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Product Division Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Sensory FX Label Large.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Builders Product Label Large.lbl":
                        Variables = ds.GetJobLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                    case "Clothing Label Small.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key, locationId);
                        break;
                    case "Clothing Label Large.lbl":
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key, locationId);
                        break;
                    case "Product Label Small.lbl":
                        if (Session["ProductList"] != null)
                        {
                            ArrayList productList = new ArrayList();
                            string productCode = "";
                            string product = "";
                            
                            productList = (ArrayList)Session["ProductList"];

                            if (productList.Count > 0)
                            {
                                string[] v = ((string)productList[0]).Split(new char[] { ',' });

                                productCode = v.GetValue(0).ToString();
                                product = v.GetValue(1).ToString();

                                productList.RemoveAt(0);
                            }
                            else
                                Session["ProductList"] = null;

                            //productCodeLength = productCode.Length;
                            //productLength = product.Length;

                            //if (productCodeLength > 19)
                            //    productCodeLength = 19;

                            //if (productLength > 49)
                            //    productLength = 49;

                            Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), product, productCode);
                            
                            if (productList.Count > 0)
                            {
                                checkList.Add(0);

                                Session["checkedList"] = checkList;
                            }
                        }
                        break;
                    default:
                        Variables = ds.GetSampleLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), key);
                        break;
                }

                if (checkList.Count == 0)
                {
                    Session["checkedList"] = null;
                    Session["Printing"] = false;
                    Session["TakeOnLabel"] = null;
                    //Session["LabelCopies"] = null;

                    //if (Session["FromURL"] != null)
                    //    Response.Redirect(Session["FromURL"].ToString());
                }
                else
                {
                    Session["checkedList"] = checkList;

                }
            }
            else
                Session["checkedList"] = null;
        }
        else //if (Session["Barcode"] != null)
        {
            switch (Session["LabelName"].ToString())
            {
                case "Multi Purpose Label.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Pickface Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Pickface Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Racking Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), "");
                    break;
                case "Racking Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), "");
                    break;
                case "Security Code Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), "");
                    break;
                case "Security Code Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), "");
                    break;
                case "Second Level Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Second Level Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Second Level Plain Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Second Level Plain Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Left Location Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Left Location Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Right Location Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Right Location Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString());
                    break;
                case "Product Bin Down Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Product Bin Down Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Product Bin Up Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Product Bin Up Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Location Up Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Location Up Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Location Down Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Location Down Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Location Left Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Location Left Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Location Right Label Small.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                case "Location Right Label Large.lbl":
                    Variables = ds.GetMultiPurposeLabel(Session["ConnectionStringName"].ToString(), Session["LabelName"].ToString(), Session["Title"].ToString(), Session["Barcode"].ToString(), (int)Session["WarehouseId"]);
                    break;
                default:
                    break;
            }
            Session["Printing"] = false;
        }
        //LabelVariables ds = new LabelVariables();

        //Variables = ds.GetLabelVariables(Session["LabelName"].ToString());
    }

    protected void SetVariables(string name, string value)
    {
        DataSet ds = Variables;

        int index = -1;

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            index++;
            if (ds.Tables[0].Rows[index]["Name"].ToString() == name)
            {
                ds.Tables[0].Rows[index]["Value"] = value;
                break;
            }
        }

        ds.AcceptChanges();
        Variables = ds;
    }

    #region "NLWAX Handlers"

    public string GetFormatName(string val)
    {
        long vallong = long.Parse(val);
        return GetVarFormatName(vallong).ToString();
    }

    private void CheckError()
    {
        if (ErrorID == 0)
            lblError.Visible = false;
        else
        {
            lblError.Text = "Error: " + ErrorMessage;
            lblError.Visible = true;
        }
    }
    #endregion "NLWAX Handlers"
}
