
using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Common_TaskMaster : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"
    
    #region PRIVATE CONSTANTS
        //private const string DEFAULT_STATUS = "IS";
        StringBuilder strBuilder = new StringBuilder();
        private string result = "";
        private string theErrMethod = "";
    #endregion

    #region Page_Load
     protected void Page_Load(object sender, EventArgs e)
     {
         Session["countLoopsToPreventInfinLoop"] = 0;
         theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["ConnectionStringCommon"] = "CQuentialCommon";
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("TaskMaster" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
     }
    #endregion Page_Load

     protected void Page_LoadComplete(object sender, EventArgs e)
     {
         try
         {
             if (Session["IssueId"] != null)
             {
                 int issueId = (int)Session["IssueId"];

                 foreach (GridViewRow row in GridViewInstruction.Rows)
                 {
                     theErrMethod = row.ToString();
                     if (GridViewInstruction.DataKeys[row.RowIndex].Values["IssueId"].ToString() == issueId.ToString())
                     {
                         GridViewInstruction.SelectedIndex = row.RowIndex;
                         issueId = -1;
                         break;
                     }
                 }

                 if (issueId != -1)
                     GridViewInstruction.SelectedIndex = -1;
             }
         }
         catch { }
     }

    #region ButtonSearch_Click
    /// <summary>
     /// 
     /// </summary>
     /// <param name="sender"></param>
     /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
     {
         theErrMethod = "ButtonSearch_Click";
         try
         {
             GridViewInstruction.DataBind();

             Master.MsgText = ""; Master.ErrorText = "";
         }
         catch (Exception ex)
         {
             result = SendErrorNow("TaskMaster" + "_" + ex.Message.ToString()); 
             Master.ErrorText = result;
         }
     }
    #endregion ButtonSearch_Click

    #region GridViewInstruction_SelectedIndexChanged
    protected void GridViewInstruction_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInstruction_SelectedIndexChanged";
        try
        {
            Session["TaskListId"] = GridViewInstruction.SelectedDataKey["TaskListId"];
            
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("TaskMaster" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion GridViewInstruction_SelectedIndexChanged

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "TaskMaster", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "TaskMaster", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
 #endregion ErrorHandling
}