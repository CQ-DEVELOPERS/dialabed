<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatePeriod.ascx.cs" Inherits="Common_DatePeriod" %>

<!-- Mailing form -->
<asp:Panel ID="pnl_Mail" runat="server"  Width="100%">
    <table>
        <tr>
            <td>
                <asp:Label ID="lbl_to" runat="server" Text="To "></asp:Label>
            </td><td>
                <asp:TextBox ID="txt_to" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="req"  runat="server" ControlToValidate="txt_to" ErrorMessage="Your entry is not a valid e-mail address."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator id="valRegEx" runat="server"
                        ControlToValidate="txt_to"
                        ValidationExpression=".*@.*\..*"
                        ErrorMessage="* Your entry is not a valid e-mail address.">
                        </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_repto" runat="server" Text="Reply To "></asp:Label>
            </td><td>
                <asp:TextBox ID="txt_repto" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_subj" runat="server" Text="Subject "></asp:Label>
            </td><td>
                <asp:TextBox ID="txt_Subject" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_Subject" ErrorMessage="Please type in a subject "></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_Comment" runat="server" Text="Comment "></asp:Label>
            </td><td>
                <asp:TextBox ID="txt_Comment" TextMode="MultiLine" Rows="4" Columns="20" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_Comment" ErrorMessage="Please type in a comment "></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</asp:Panel>

<!-- //RadioButton List for Date Periods -->
<asp:Panel ID="pnl_Radiolist" runat="server" Visible="false" Width="100%">
    <table>
        <tr>
            <td>
            <asp:Label ID="lbl_radPeriod" runat="server" Text="Report Period"></asp:Label>
            </td>

            <td>
            <asp:RadioButtonList ID="rad_DatePeriod" runat="server" DataSourceID="this_id" DataTextField="DatePeriodDesc" DataValueField="DatePeriodID">
            </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Panel>

<!-- //Report Schedule -->
<asp:Panel ID="pnl_Schedule" runat="server" Width="100%">
    <table>
        <tr>
        
            <td>
            <asp:Label ID="lbl_radSchedule" runat="server" Text="Report Schedule"></asp:Label>
            </td>

            <td>
            <asp:RadioButtonList ID="rad_Schedule" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rad_Schedule_SelectedIndexChanged" >
                <asp:ListItem Text="Hour" Value="Hour"></asp:ListItem>
                <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                <asp:ListItem Text="Once" Value="Once"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="req_Schedule" runat="server" ControlToValidate="rad_Schedule" ErrorMessage="Please select a schedule">
            </asp:RequiredFieldValidator>
            </td>

            <td>
            <!-- Hourly Panel -->
                <asp:Panel ID="pnl_Hourly" runat="server" Visible="false"  GroupingText="Hourly Schedule" SkinID="pnl_Border">                 
                   <asp:Label ID="lbl_hourly_head" runat="server" Text="Run the schedule every:"></asp:Label>
                   
                   <p>
                    <asp:TextBox Width="15px" ID="txt_Hourly_Hour" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_hourly_Hour" runat="server" Text="hours"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_Hourly_Minute" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_hourly_Minutes" runat="server" Text="minutes"></asp:Label>
                   </p>
                   
                   <p>
                    <asp:Label ID="lbl_hourly_stTime" runat="server" Text="Start time:"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_Hourly_stHour" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_hourly_stHour" runat="server" Text="hours"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_Hourly_stMin" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_hourly_stMin" runat="server" Text="minutes"></asp:Label>

                    <asp:RadioButtonList RepeatLayout="Flow" ID="rad_hourly_AP" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                    </asp:RadioButtonList>
                   </p>
                  
                </asp:Panel>
            <!-- Daily Panel -->    
                <asp:Panel ID="pnl_Day" runat="server" Visible="false"  GroupingText="Daily Schedule" SkinID="pnl_Border">
                    
                    <asp:RadioButton ID="rad_day_ffg" runat="server" Text="On the following days:" GroupName="Rad_days" SkinID="Radright"  />
                    <asp:CheckBoxList ID="chk_day_days" runat="server" TextAlign="Right" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Sun" Value="Sunday"></asp:ListItem>
                    <asp:ListItem Text="Mon" Value="Monday"></asp:ListItem>
                    <asp:ListItem Text="Tues" Value="Tuesday"></asp:ListItem>
                    <asp:ListItem Text="Wed" Value="Wednesday"></asp:ListItem>
                    <asp:ListItem Text="Thurs" Value="Thursday"></asp:ListItem>
                    <asp:ListItem Text="Fri" Value="Friday"></asp:ListItem>
                    <asp:ListItem Text="Sat" Value="Saturday"></asp:ListItem>
                    </asp:CheckBoxList>
                    <p>
                    <asp:RadioButton ID="rad_day_all" runat="server" Text="Every weekday" GroupName="Rad_days" SkinID="Radright" />
                    </p><p>
                    <asp:RadioButton ID="rad_day_repeat" runat="server" Text="Repeat after this " GroupName="Rad_days" SkinID="Radright" />
                    <asp:Label ID="lbl_day_repeat" runat="server" Text="number of days :"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_day_repeat" runat="server"></asp:TextBox>
                    </p>
                   <p>
                    <asp:Label ID="lbl_day_stTime" runat="server" Text="Start time:"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_day_stHour" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_day_stHour" runat="server" Text="hours"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_day_stMin" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_day_stMin" runat="server" Text="minutes"></asp:Label>

                    <asp:RadioButtonList RepeatLayout="Flow" ID="rad_Day_AP" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                    </asp:RadioButtonList>
                   </p>
                </asp:Panel>
             <!-- Weekly Panel -->   
                <asp:Panel ID="pnl_Week" runat="server" Visible="false"  GroupingText="Weekly Schedule" SkinID="pnl_Border">
                    <asp:Label ID="lbl_week_repeat" runat="server" Text="Repeat after this number of weeks:" />
                    <asp:TextBox Width="15px" ID="txt_week_repeat" runat="server"></asp:TextBox>
                    
                    <p>
                    <asp:Label ID="lbl_week_days" runat="server" Text="On day(s):" />
                    <asp:CheckBoxList ID="chk_week_days" runat="server" TextAlign="Right" RepeatLayout="Flow"  RepeatDirection="Horizontal">
                    <asp:ListItem Text="Sun" Value="Sunday"></asp:ListItem>
                    <asp:ListItem Text="Mon" Value="Monday"></asp:ListItem>
                    <asp:ListItem Text="Tues" Value="Tuesday"></asp:ListItem>
                    <asp:ListItem Text="Wed" Value="Wednesday"></asp:ListItem>
                    <asp:ListItem Text="Thurs" Value="Thursday"></asp:ListItem>
                    <asp:ListItem Text="Fri" Value="Friday"></asp:ListItem>
                    <asp:ListItem Text="Sat" Value="Saturday"></asp:ListItem>
                    </asp:CheckBoxList>
                    </p>
                    
                    <p>
                    <asp:Label ID="lbl_week_stTime" runat="server" Text="Start time:"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_week_stHour" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_week_stHour" runat="server" Text="hours"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_week_stMin" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_week_stMin" runat="server" Text="minutes"></asp:Label>

                    <asp:RadioButtonList RepeatLayout="Flow" ID="rad_week_AP" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                    </asp:RadioButtonList>
                   </p>
                </asp:Panel>
             <!-- Monthly Panel -->   
                <asp:Panel ID="pnl_Month" runat="server" Visible="false"  GroupingText="Monthly Schedule" SkinID="pnl_Border">
                    <asp:Label ID="lbl_Month_head" runat="server" Text="Months"></asp:Label>
                    <asp:CheckBoxList ID="chk_month_months" runat="server" TextAlign="Right" RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Jan" Value="January"></asp:ListItem>
                    <asp:ListItem Text="Feb" Value="February"></asp:ListItem>
                    <asp:ListItem Text="Mar" Value="March"></asp:ListItem>
                    <asp:ListItem Text="Apr" Value="April"></asp:ListItem>
                    <asp:ListItem Text="May" Value="May"></asp:ListItem>
                    <asp:ListItem Text="Jun" Value="June"></asp:ListItem>
                    <asp:ListItem Text="Jul" Value="July"></asp:ListItem>
                    <asp:ListItem Text="Aug" Value="August"></asp:ListItem>
                    <asp:ListItem Text="Sep" Value="September"></asp:ListItem>
                    <asp:ListItem Text="Oct" Value="October"></asp:ListItem>
                    <asp:ListItem Text="Nov" Value="November"></asp:ListItem>
                    <asp:ListItem Text="Dec" Value="December"></asp:ListItem>
                    </asp:CheckBoxList>
                    
                    <p>
                    <asp:RadioButton ID="Rad_month_Week" runat="server" Text="On week of month:" GroupName="grp_weekDays" SkinID="Radright"></asp:RadioButton>
                    <asp:DropDownList  ID="dd_month_week" runat="server" SkinID="LargeDropBox" >
                    <asp:ListItem Text="1st" Value="FirstWeek"></asp:ListItem>
                    <asp:ListItem Text="2nd" Value="SecondWeek"></asp:ListItem>
                    <asp:ListItem Text="3rd" Value="ThirdWeek"></asp:ListItem>
                    <asp:ListItem Text="4th" Value="ForthWeek"></asp:ListItem>
                    <asp:ListItem Text="Last" Value="LastWeek"></asp:ListItem>
                    </asp:DropDownList>
                        <br />
                    <asp:Label ID="lbl_month_weekdays" runat="server" Text="On day of week:" />
                    <asp:CheckBoxList ID="chk_month_days" runat="server" TextAlign="Right" RepeatLayout="Flow"  RepeatDirection="Horizontal">
                    <asp:ListItem Text="Sun" Value="Sunday"></asp:ListItem>
                    <asp:ListItem Text="Mon" Value="Monday"></asp:ListItem>
                    <asp:ListItem Text="Tues" Value="Tuesday"></asp:ListItem>
                    <asp:ListItem Text="Wed" Value="Wednesday"></asp:ListItem>
                    <asp:ListItem Text="Thurs" Value="Thursday"></asp:ListItem>
                    <asp:ListItem Text="Fri" Value="Friday"></asp:ListItem>
                    <asp:ListItem Text="Sat" Value="Saturday"></asp:ListItem>
                    </asp:CheckBoxList>
                    
                    </p>
                    
                    <p>
                    <asp:RadioButton ID="rad_month_calDays" runat="server" Text="On" GroupName="grp_weekDays" SkinID="Radright"></asp:RadioButton>
                    <asp:Label ID="lbl_month_calDays" runat="server" text="calendar day(s)"></asp:Label>
                    <asp:TextBox ID="txt_month_calDays" runat="server" Width="40px" Text="1,3-5"></asp:TextBox>
                    </p>
                    <p>
                    <asp:Label ID="lbl_month_stTime" runat="server" Text="Start time:"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_month_stHour" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_month_stHour" runat="server" Text="hours"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_month_stMin" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_month_stMin" runat="server" Text="minutes"></asp:Label>

                    <asp:RadioButtonList RepeatLayout="Flow" ID="rad_month_AP" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                    </asp:RadioButtonList>
                   </p>
                </asp:Panel>
             <!-- Once only Panel -->   
                <asp:Panel ID="pnl_Once" runat="server" Visible="false" GroupingText="One-Time Schedule" SkinID="pnl_Border"> 
                    <asp:Label ID="lbl_Once_head" runat="server" Text="Schedule runs only once."></asp:Label>
                    <p>
                    <asp:Label ID="lbl_Once_stTime" runat="server" Text="Start time:"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_once_stHour" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_Once_stHour" runat="server" Text="hours"></asp:Label>
                    <asp:TextBox Width="15px" ID="txt_once_stMin" runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_Once_stMin" runat="server" Text="minutes"></asp:Label>

                    <asp:RadioButtonList RepeatLayout="Flow" ID="rad_once_AP" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                    </asp:RadioButtonList>
                   </p>
                </asp:Panel>
            
            </td>
            
            <td>
             <asp:Label ID="lbl_Message" runat="server" SkinID="ErrorMessage"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>

<!-- //Create Data Subscription List -->
<asp:panel ID="pnl_submit" runat="server" Width="100%">
<asp:Button ID="but_CreateSubscription" runat="server" Text="Create" OnClick="but_CreateSubscription_Click" />
<asp:Label ID="lbl_ErrorMessage" runat="server" SkinID="ErrorMessage"></asp:Label>
</asp:panel>

<!-- //Object DataSource for RadioButton List -->
<asp:ObjectDataSource runat="server" SelectMethod="SelectDatePeriods" ID="this_id" TypeName="DatePeriod">
    <SelectParameters>
    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
