<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerSearch.ascx.cs" Inherits="Common_CustomerSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelExternalCompany" runat="server" Text="<%$ Resources:Default, Customer %>" Width="100px"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxExternalCompany" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelExternalCompanyCode" runat="server" Text="<%$ Resources:Default, CustomerCode %>" Width="100px"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxExternalCompanyCode" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click"></asp:Button>
        </td>
    </tr>
</table>
<asp:GridView ID="GridViewExternalCompany" 
                runat="server" 
                DataSourceID="ObjectDataSourceExternalCompany"
                AutoGenerateColumns="False" 
                AutoGenerateSelectButton="True" 
                AllowPaging="true"
                OnSelectedIndexChanged="GridViewExternalCompany_OnSelectedIndexChanged"
                DataKeyNames="ExternalCompanyId,ExternalCompany">
    <Columns>
        <asp:BoundField ReadOnly="True" DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>">
        </asp:BoundField>
        <asp:BoundField ReadOnly="True" DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>"></asp:BoundField>
    </Columns>
</asp:GridView>
<asp:ObjectDataSource ID="ObjectDataSourceExternalCompany" runat="server" TypeName="ExternalCompany" SelectMethod="SearchExternalCompanies">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:ControlParameter Name="externalCompany" ControlID="TextBoxExternalCompany" Type="String" />
        <asp:ControlParameter Name="externalCompanyCode" ControlID="TextBoxExternalCompanyCode" Type="String" />
        <asp:Parameter Name="externalCompanyTypeCode" DefaultValue="CUST" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>