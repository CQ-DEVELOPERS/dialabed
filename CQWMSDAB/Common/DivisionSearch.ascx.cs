using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_DivisionsSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["DivisionId"] == null)
                Session["DivisionId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewDivisionSearch_OnSelectedIndexChanged
    protected void GridViewDivisionSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["DivisionId"] = GridViewDivisionSearch.SelectedDataKey["DivisionId"];
        Session["DivisionCode"] = GridViewDivisionSearch.SelectedDataKey["DivisionCode"];
    }
    #endregion GridViewDivisionSearch_OnSelectedIndexChanged

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewDivisionSearch.PageIndex = 0;
        GridViewDivisionSearch.DataBind();
    }
    #endregion "ButtonSearch_Click"
}
