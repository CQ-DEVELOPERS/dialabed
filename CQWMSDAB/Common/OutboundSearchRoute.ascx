<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutboundSearchRoute.ascx.cs" Inherits="Common_OutboundSearchRoute" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                    DataTextField="Principal" DataValueField="PrincipalId" Width="150px" OnSelectedIndexChanged="DropDownListPrincipal_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                    SelectMethod="GetPrincipalParameter">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, Route %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListRoute" runat="server" DataTextField="Route"
                    DataValueField="RouteId" DataSourceID="ObjectDataSourceRouteId" Width="150px" OnSelectedIndexChanged="DropDownListRoute_SelectedIndexChanged" AppendDataBoundItems="true">
                    <asp:ListItem Text="{All}" Value="-1" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxOrderNumber" runat="server" Width="150px" OnTextChanged="TextBoxOrderNumber_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, OutboundDocumentType %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataTextField="OutboundDocumentType"
                    DataValueField="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentTypeId" Width="150px" OnSelectedIndexChanged="DropDownListOutboundDocumentType_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelCustomerCode" runat="server" Text="<%$ Resources:Default, CustomerCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxCustomerCode" runat="server" Width="150px" OnTextChanged="TextBoxCustomerCode_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelcustomerName" runat="server" Text="<%$ Resources:Default, Customer %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxCustomerName" runat="server" Width="150px" OnTextChanged="TextBoxCustomerName_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
        </tr>
        <%--<tr>
            <td>
                <asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadDatePicker ID="rdpFromDate" runat="server"></telerik:RadDatePicker>
            </td>
            <td>
                <asp:Label ID="labeToDate" runat="server" Text="<%$ Resources:Default, ToDate %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadDatePicker ID="rdpToDate" runat="server"></telerik:RadDatePicker>
            </td>
        </tr>--%>
    </table>
</asp:Panel>
<uc1:DateRange ID="DateRange1" runat="server" />
<%--</div>--%>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />

<%--<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="Server"
    TargetControlID="PanelSearch"
    ExpandControlID="LabelShowOrHide"
    CollapseControlID="LabelShowOrHide" 
    Collapsed="False"
    TextLabelID="LabelShowOrHide"
    ExpandedText="<<<"
    CollapsedText=">>"
    SuppressPostBack="true" />--%>

<asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentTypeId" runat="server" SelectMethod="GetOutboundDocumentTypes" TypeName="OutboundDocumentType">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="ObjectDataSourceRouteId" runat="server" SelectMethod="GetRoute" TypeName="StaticInfoRoute">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderJobNumber" runat="server"
    TargetControlID="TextBoxOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="TextBoxCustomerCode" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="TextBoxCustomerName" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>--%>