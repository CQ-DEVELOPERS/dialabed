using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_StorageUnitBatchIdSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["StorageUnitBatchId"] == null)
                Session["StorageUnitBatchId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewProductSearch_OnSelectedIndexChanged
    protected void GridViewProductSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StorageUnitBatchId"] = GridViewProductSearch.SelectedDataKey["StorageUnitBatchId"];
        Session["Product"] = GridViewProductSearch.SelectedDataKey["Product"];
    }
    #endregion GridViewProductSearch_OnSelectedIndexChanged

    #region GridViewProductSearch_PageIndexChanging
    protected void GridViewProductSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewProductSearch.PageIndex = e.NewPageIndex;
            GridViewProductSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewProductSearch_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewProductSearch.PageIndex = 0;

        GridViewProductSearch_DataBind();
    }
    #endregion "ButtonSearch_Click"

    protected void GridViewProductSearch_DataBind()
    {
        Product product = new Product();

        GridViewProductSearch.DataSource = product.SearchProductsStorageUnitBatch(Session["ConnectionStringName"].ToString(), TextBoxProductCode.Text, TextBoxProduct.Text, TextBoxSKUCode.Text, TextBoxSKU.Text, TextBoxBatch.Text, TextBoxECLNumber.Text);

        GridViewProductSearch.DataBind();
    }
}
