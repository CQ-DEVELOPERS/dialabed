<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="RegisterSerialNumberPallet.aspx.cs"
    Inherits="Common_RegisterSerialNumberPallet"
    Title="<%$ Resources:Default, RegisterSerialNumberPalletTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, RegisterSerialNumberPalletTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, RegisterSerialNumberPalletAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:FormView ID="FormViewSerialNumber" runat="server" DataKeyNames="InstructionId,StorageUnitId" DataSourceID="ObjectDataSourceDetails">
        <ItemTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="LabelSupplierCode" runat="server" Text='<%$ Resources:Default, SupplierCode%>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelSupplierCodeBind" runat="server" Text='<%# Bind("SupplierCode") %>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label1SupplierBind" runat="server" Text='<%# Bind("Supplier") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelOrderNumber" runat="server" Text='<%$ Resources:Default, OrderNumber%>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelOrderNumberBind" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelProductCode" runat="server" Text='<%$ Resources:Default, ProductCode%>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelProductCodeBind" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelProduct" runat="server" Text='<%# Bind("Product") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelPalletId" runat="server" Text='<%$ Resources:Default, PalletId%>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelPalletIdBind" runat="server" Text='<%# Bind("PalletId") %>'></asp:Label>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" SelectMethod="GetSerialDetailsByPallet" OnSelecting="ObjectDataSourceDetails_OnLoad" TypeName="SerialNumber">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="instructionId" Type="int32"/>
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:Button ID="ButtonGetNextInsruction" runat="server" Text="<%$ Resources:Default, NextInstruction%>" OnClick="ButtonGetNextInsruction_Click" />
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanelRepeaterSerialNumber">
        <contenttemplate>
            <asp:TextBox ID="TextBoxSerialNumber" runat="server"></asp:TextBox>
            <asp:Button ID="ButtonAdd" runat="Server" Text="<%$ Resources:Default, Add%>" OnClick="ButtonAdd_Click" />
            <asp:Label ID="LabelError" runat="server" ForeColor="Red"></asp:Label>
            <br />
            <br />
            <asp:Repeater ID="RepeaterSerialNumber" DataSourceID="ObjectDataSourceRepeater" runat="server">
                <ItemTemplate>
                    <asp:TextBox ID="TextBoxRepeat" runat="server" BorderWidth="1" BorderStyle="Dashed" BorderColor="ActiveBorder" Enabled="false" ForeColor="DarkGray" Text='<%# Eval("SerialNumber") %>'></asp:TextBox>
                </ItemTemplate>
            </asp:Repeater>
        </contenttemplate>
        <triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="Click"></asp:AsyncPostBackTrigger>
        </triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSourceRepeater" runat="server" SelectMethod="GetSerialNumbersByPallet" OnSelecting="ObjectDataSourceRepeater_OnLoad" TypeName="SerialNumber">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="instructionId" Type="int32"/>
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

