<%@ Page Language="C#" MasterPageFile="~/MasterPages/Empty.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Common_Test" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <!-- NLWAX ActiveX control -->
    <object classid="clsid:ECBB0B73-FDF4-4DBD-B7C9-8450C2D9FB66" 
        codebase="./NLWAX.ocx#version=1,0,0,34" 
        width="1" 
        height="1" 
        id="NLWAXForm" 
        name="NLWAXForm">
    </object>
    <asp:TextBox ID="txtPrinters" runat="server" TextMode="MultiLine"></asp:TextBox>
    <asp:TextBox ID="txtPorts" runat="server" TextMode="MultiLine"></asp:TextBox>
    <br />
    <asp:Button ID="Button1" runat="server" Text="Button" />
    <%--<script type="text/javascript" language="javascript" src="../javascript/NLSetup.js"></script>--%>
    
    <script type="text/jscript">
        // convenience function for commonnly used getElementById call
        function $(id){return document.getElementById(id)}
        
        document.forms[0]['ctl00_ContentPlaceHolderBody_Button1'].value=Date();
        
        // convenience function for commonnly used getElementById call
        function $(id){return document.getElementById(id)}
        
        var WAX = $('NLWAXForm');
        
        //=======================================================================================
        // Retrieve client-side printer/port lists
        //LoadPrinterList();
        LoadPortList();
        
//        function LoadPrinterList(PrinterName){
//            // Calls NLWAX.oxc's GetPrinterList to retrieve list of client-side printers.
//            //document.forms[0]['ctl00_ContentPlaceHolderBody_txtPrinters'].value = WAX.GetPrinterList();
//            
//            // Create XML DOM parser
//            var xmldom = new ActiveXObject('Microsoft.XMLDOM'); 

//            // Load XML from hidden field in NLWAX1
//            xmldom.loadXML(WAX.GetPrinterList()); 
//            document.forms[0]['ctl00_ContentPlaceHolderBody_txtPrinters'].value = xmldom.getElementsByTagName('Printer');
//            //  Get collection of Printer nodes
//            var clientPrinters = xmldom.getElementsByTagName('Printer');
//            
//            document.forms[0]['ctl00_ContentPlaceHolderBody_txtPrinters'].value = "";
//            
//            //  loop through client drivers looking for selected printer
//            for(var x=0;x<=clientPrinters.length;x++){ 
//              var printer = clientPrinters[x];
//              if (! printer) return;
//              var deviceName = printer.firstChild.firstChild.nodeValue;
//              
//              document.forms[0]['ctl00_ContentPlaceHolderBody_txtPrinters'].value = document.forms[0]['ctl00_ContentPlaceHolderBody_txtPrinters'].value + deviceName;
//              
//              //if driver is found return associated port
//              if (deviceName == PrinterName){
//                var portName = printer.lastChild.firstChild.nodeValue;
//                return portName;
//              }
//            }
//        }
        function LoadPortList(PrinterName){
            // Calls NLWAX.oxc's GetPortList to retrieve list of client-side ports.
            document.forms[0]['ctl00_ContentPlaceHolderBody_txtPorts'].value = WAX.GetPortList();
            
            var xmldom = new ActiveXObject('Microsoft.XMLDOM');
            var selIndex;
            
            xmldom.loadXML(WAX.GetPortsList());
            var clientPorts = xmldom.getElementsByTagName('PortName');

            ShowPortList(true);

            // Clear existing entries for port selection
            document.forms[0]['ctl00_ContentPlaceHolderBody_txtPorts'].value = "";
            
            selPort.options.length = 0;
            selPort.options[selPort.options.length] = new Option('-- Select Port --','',false,true);

            for(var x=clientPorts.length-1;x>=0;x--){ 
                var port = clientPorts[x];
                if (! port)
                    return;
                var portName = port.firstChild.nodeValue;
                var selected = (portName == SelPortName)
                if(selected == true)
                    selIndex = selPort.options.length;
                selPort.options[selPort.options.length] = new Option(portName,portName,false,false);
            }
            if(selIndex != null)
                selPort.selectedIndex = selIndex;
        }
    </script>
</asp:Content>

