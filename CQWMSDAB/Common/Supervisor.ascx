<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Supervisor.ascx.cs" Inherits="Common_Supervisor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Panel ID="PanelLogin" runat="server" BackColor="#FFFFFF" BorderWidth="2" BorderStyle="solid" BorderColor="DarkBlue" style="z-index: 1;">
    <table>
        <tr>
            <td colspan="2">
                <b>Log In</b>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelUserName" runat="server" Text="<%$ Resources:Default, UserName%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxUserName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPassword" runat="server" Text="<%$ Resources:Default, Password%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonLogin" runat="server" Text="<%$ Resources:Default, Login%>" OnClick="ButtonLogin_Click" />
            </td>
            <td>
                <asp:Button ID="ButtonCancel" runat="server" Text="<%$ Resources:Default, Cancel%>" OnClick="ButtonCancel_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:AlwaysVisibleControlExtender ID="avce" runat="server"
    TargetControlID="PanelLogin"
    VerticalSide="Middle"
    VerticalOffset="10"
    HorizontalSide="Right"
    HorizontalOffset="10"
    ScrollEffectDuration=".1" />