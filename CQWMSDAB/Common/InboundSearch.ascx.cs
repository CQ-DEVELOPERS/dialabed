using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_InboundSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListInboundDocumentType.DataBind();
                Session["InboundShipmentId"] = -1;

                if (Session["OrderNumber"] == null)
                    Session["OrderNumber"] = "";
                else
                    TextBoxOrderNumber.Text = Session["OrderNumber"].ToString();

                if (Session["ExternalCompanyCode"] == null)
                    Session["ExternalCompanyCode"] = "";
                else
                    TextBoxSupplierCode.Text = Session["ExternalCompanyCode"].ToString();

                if (Session["ExternalCompany"] == null)
                    Session["ExternalCompany"] = "";
                else
                    TextBoxSupplierName.Text = Session["ExternalCompany"].ToString();

                if (Session["InboundDocumentTypeId"] == null)
                    Session["InboundDocumentTypeId"] = int.Parse(DropDownListInboundDocumentType.SelectedValue.ToString());
                else
                    DropDownListInboundDocumentType.SelectedValue = Session["InboundDocumentTypeId"].ToString();

                if (Session["PrincipalId"] == null)
                    try { Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString()); }
                    catch { Session["PrincipalId"] = -1; }
                else
                    DropDownListPrincipal.SelectedValue = Session["PrincipalId"].ToString();
            }

            if (TextBoxOrderNumber.Text == "{All}")
            {
                Session["OrderNumber"] = "";
                TextBoxOrderNumber.Text = "";
            }

            if (TextBoxSupplierCode.Text == "{All}")
            {
                Session["ExternalCompanyCode"] = "";
                TextBoxSupplierCode.Text = "";
            }

            if (TextBoxSupplierName.Text == "{All}")
            {
                Session["ExternalCompany"] = "";
                TextBoxSupplierName.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxOrderNumber_TextChanged(object sender, EventArgs e)
    {
        Session["OrderNumber"] = TextBoxOrderNumber.Text;
    }
    protected void TextBoxSupplierCode_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompanyCode"] = TextBoxSupplierCode.Text;
    }
    protected void TextBoxSupplierName_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompany"] = TextBoxSupplierName.Text;
    }
    protected void DropDownListInboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["InboundDocumentTypeId"] = int.Parse(DropDownListInboundDocumentType.SelectedValue.ToString());
    }
    protected void DropDownListPrincipal_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString());
    }
}
