<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManualLocationAllocation.aspx.cs" Inherits="Common_ManualLocationAllocation"
    Title="<%$ Resources:Default, ManualLocationAllocationTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ManualLocationAllocationTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ManualLocationAllocationAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:Button ID="ButtonReturn" runat="server" Text="<%$ Resources:Default, ButtonReturn %>" OnClick="ButtonReturn_Click" />
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanelLines" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="LabelLines" runat="server" Text="" EnableTheming="false" ForeColor="red"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridViewLocations" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:Label ID="LabelOf" runat="server" Text="<%$ Resources:Default, Of%>" EnableTheming="false" ForeColor="red"></asp:Label>
                <asp:Label ID="LabelTotal" runat="server" Text="" EnableTheming="false" ForeColor="red"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
        <contenttemplate>
                <asp:GridView id="GridViewInstruction" runat="server" AutoGenerateColumns="False" DataKeyNames="InstructionId" AllowPaging="true" DataSourceID="ObjectDataSourceInstruction">
                    <Columns>
                        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"/>
                        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>"/>
                        <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"/>
                        <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"/>
                        <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"/>
                    </Columns>
                </asp:GridView>
                <asp:GridView id="GridViewArea" runat="server" AutoGenerateColumns="False" DataKeyNames="AreaId" AllowPaging="true" OnSelectedIndexChanged="GridViewArea_SelectedIndexChanged" AutoGenerateSelectButton="true" DataSourceID="ObjectDataSourceArea">
                    <Columns>
                        <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area %>"/>
                    </Columns>
                </asp:GridView>
                <asp:UpdatePanel id="UpdatePanelGridViewLocations" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewLocations" runat="server" AllowPaging="true" DataKeyNames="LocationId" AutoGenerateSelectButton="true" OnSelectedIndexChanged="GridViewLocations_SelectedIndexChanged" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceLocations">
                            <Columns>
                                <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>"/>
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"/>
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>"/>
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"/>
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"/>
                                <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default, ActualQuantity %>"/>
                                <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>"/>
                                <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default, ReservedQuantity %>"/>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridViewArea" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel> 
        </contenttemplate>
        <triggers>
            <asp:AsyncPostBackTrigger ControlID="GridViewLocations" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
        </triggers>
    </asp:UpdatePanel>
    
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server"
        TypeName="InstructionMaintenance"
        SelectMethod="GetManualInstruction">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="InstructionId" Type="Int32" SessionField="InstructionId" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server"
        TypeName="InstructionMaintenance"
        SelectMethod="GetAreas">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="InstructionId" Type="Int32" SessionField="InstructionId" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceLocations" runat="server"
        TypeName="InstructionMaintenance"
        SelectMethod="GetLocations">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="InstructionId" Type="Int32" SessionField="InstructionId" />
            <asp:ControlParameter Name="AreaId" ControlID="GridViewArea" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
