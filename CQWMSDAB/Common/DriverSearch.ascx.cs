﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_DriverSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ContactListId"] == null)
                Session["ContactListId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewContactSearch_OnSelectedIndexChanged
    protected void GridViewContactSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["ContactListId"] = GridViewContactSearch.SelectedDataKey["ContactListId"];
        Session["ContactPerson"] = GridViewContactSearch.SelectedDataKey["ContactPerson"];
    }
    #endregion GridViewContactSearch_OnSelectedIndexChanged
}
