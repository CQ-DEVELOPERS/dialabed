<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InboundSearch.ascx.cs" Inherits="Common_InboundSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<%--<div style="float:left;">
    <asp:Label ID="LabelShowOrHide" runat="server"></asp:Label>
</div>

<div style="float:left;">--%>
<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxOrderNumber" runat="server" Width="150px" OnTextChanged="TextBoxOrderNumber_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, InboundDocumentType %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataTextField="InboundDocumentType"
                    DataValueField="InboundDocumentTypeId" DataSourceID="ObjectDataSourceInboundDocumentTypeId" Width="150px" OnSelectedIndexChanged="DropDownListInboundDocumentType_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSupplierCode" runat="server" Text="<%$ Resources:Default, SupplierCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxSupplierCode" runat="server" Width="150px" OnTextChanged="TextBoxSupplierCode_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelSupplierName" runat="server" Text="<%$ Resources:Default, Supplier %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxSupplierName" runat="server" Width="150px" OnTextChanged="TextBoxSupplierName_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                    DataTextField="Principal" DataValueField="PrincipalId" Width="150px" OnSelectedIndexChanged="DropDownListPrincipal_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                    SelectMethod="GetPrincipalParameter">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            </td>
            <td>
            <td>
            </td>
        </tr>
    </table>
    <uc1:DateRange ID="DateRange1" runat="server" />
</asp:Panel>
<%--</div>--%>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />

<%--<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="Server"
    TargetControlID="PanelSearch"
    ExpandControlID="LabelShowOrHide"
    CollapseControlID="LabelShowOrHide" 
    Collapsed="False"
    TextLabelID="LabelShowOrHide"
    ExpandedText="<<<"
    CollapsedText=">>"
    SuppressPostBack="true" />--%>

<asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentTypeId" runat="server" SelectMethod="GetInboundDocumentTypes"
    TypeName="InboundDocumentType">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderJobNumber" runat="server"
    TargetControlID="TextBoxOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="TextBoxSupplierCode" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="TextBoxSupplierName" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>--%>