﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPages_MasterPage : System.Web.UI.MasterPage
{
    #region MsgText
    public String MsgText
    {
        get
        {
            return HeaderUC.MsgText;
        }
        set
        {
            HeaderUC.MsgText = value;
        }
    }
    #endregion MsgText

    #region ErrorText
    public String ErrorText
    {
        get
        {
            return HeaderUC.ErrorText;
        }
        set
        {
            HeaderUC.ErrorText = value;
        }
    }
    #endregion ErrorText

    #region Page_PreRender
    protected void Page_PreRender(object sender, EventArgs e)
    {
        // Make it Safari and Chrome compatible
        if (Request.UserAgent != null && (Request.UserAgent.IndexOf("AppleWebKit") > 0))
        {
            Request.Browser.Adapters.Clear();
        }
    }
    #endregion Page_PreRender

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Context.Session.IsNewSession)
            {
                FormsAuthentication.SignOut();
                Response.Redirect("~/Security/Login.aspx");
            }
            else
            {
                // Acquire Auth Ticket from the FormsIdentity object
                FormsAuthenticationTicket ticket = ((FormsIdentity)Context.User.Identity).Ticket;

                // Manually slide the expiration
                FormsAuthentication.RenewTicketIfOld(ticket);
            }

            if (!Page.IsPostBack)
            {
                if (Session["ConnectionStringName"] == null)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
                else
                    FooterUC.DatabaseLabel = Session["ConnectionStringName"].ToString();

                if (Session["WarehouseId"] != null)
                    DropDownListWarehouse.SelectedValue = Session["WarehouseId"].ToString();

                if (Session["OperatorGroupCode"] != null)
                    if (Session["OperatorGroupCode"].ToString() == "A" || Session["OperatorGroupCode"].ToString() == "S")
                        DropDownListWarehouse.Enabled = true;

                // DateTime ft = Convert.ToDateTime(DateTime.Now.Date.ToString());
                // string tempFromDate = ft.ToShortDateString() + " 00:00:00";
                // Session["FromDate"] = tempFromDate;

                // DateTime dt = Convert.ToDateTime(DateTime.Now.Date.ToString());
                // string tempToDate = dt.ToShortDateString() + " 23:59:59";
                // Session["ToDate"] = tempToDate;
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region DropDownListWarehouse_OnSelectedIndexChanged
    protected void DropDownListWarehouse_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Warehouse w = new Warehouse();
            Session["WarehouseId"] = int.Parse(DropDownListWarehouse.SelectedValue);

            w.SetWarehouseForOperator(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"]);

            MsgText = "";
            ErrorText = "";
        }
        catch (Exception ex)
        {
            ErrorText = ex.Message.ToString();
        }
    }
    #endregion DropDownListWarehouse_OnSelectedIndexChanged
}
