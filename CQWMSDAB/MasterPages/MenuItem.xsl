<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" encoding="utf-8"/>
	<!-- Find the root node called MenuItems 
       and call MenuListing for its children -->
	<xsl:template match="/MenuItems">
		<MenuItems>
			<xsl:call-template name="MenuListing" />
		</MenuItems>
	</xsl:template>

	<!-- Allow for recusive child node processing -->
	<xsl:template name="MenuListing">
		<xsl:apply-templates select="MenuItem" />
	</xsl:template>

	<xsl:template match="MenuItem">
		<MenuItem>
			<!-- Convert Menu child elements to MenuItem attributes -->
			<xsl:attribute name="MenuItemText">
				<xsl:value-of select="MenuItemText"/>
			</xsl:attribute>
			<xsl:attribute name="ToolTip">
				<xsl:value-of select="ToolTip"/>
			</xsl:attribute>
			<xsl:attribute name="NavigateUrl">
				<xsl:value-of select="NavigateTo"/>
			</xsl:attribute>

			<!-- Call MenuListing if there are child Menu nodes -->
			<xsl:if test="count(MenuItem) > 0">
				<xsl:call-template name="MenuListing" />
			</xsl:if>
		</MenuItem>
	</xsl:template>
</xsl:stylesheet>
