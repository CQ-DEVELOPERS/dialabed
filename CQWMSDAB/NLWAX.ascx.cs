using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Services.Protocols;

public partial class NLWAX : System.Web.UI.UserControl
{
    PrintService.NPSService NicePrintService = new PrintService.NPSService();
    StorageService.StorageService NPSLabelStorage = new StorageService.StorageService();
    PrintService.FileLocation __FileLocation = new PrintService.FileLocation();

    string _errorMessage = "";
    string _errorID;
    string _errorType;

    #region "Public Enums"

    enum Days {Sat=1, Sun, Mon, Tue, Wed, Thu, Fri};
    enum PrintType{ToPrinter=1, ToPort};
    public enum LabelKind { Null=1, MainLabel, HeaderLabel, TailLabel };
    public enum LabelSide { Front=1, Back };
    
    #endregion
    
    #region "Public Properties"
    public string IsConnected
    {
        get { return NicePrintService.TestEngineConnection(); }
    }

    public string BaseURL
    {
        get { return Session["BaseURL"].ToString(); }
        set
        {
            hdfBaseURL.Value = value;
            Session["BaseURL"] = value;
        }
    }

    public string SendTo
    {
        get { return Session["SendTo"].ToString(); }
        set
        {
            hdfSendTo.Value = value;
            Session["SendTo"] = value;
        }
    }

    public string Port
    {
        get { return hdfPort.Value; }
        set { hdfPort.Value = value; }
    }

    public string SessionName
    {
        get { return Session["SessionName"].ToString(); }
        set { Session["SessionName"] = value; }
    }

    public string[] LabelGroups
    {
        get
        { 
            return NPSLabelStorage.GetLabelGroups();
        }
    }

    public string LabelGroup
    {
        get { return Session["LabelGroup"].ToString(); }
        set { Session["LabelGroup"] = value; }
    }

    public string[] Labels(string LabelGroup, bool Recursive)
    {
        return NPSLabelStorage.GetLabelList(LabelGroup, Recursive);
    }

    public string Label
    {
        get { return hdfLabel.Value; }
        set
        {
            hdfLabel.Value = value;
            Session["dsVars"] = null;
            Session["PreviewUrl"] = null;
        }
    }

    private PrintService.FileLocation FileLocation
    {
        get
        {
            __FileLocation.LabelFileName = Label;
            __FileLocation.LabelGroup = LabelGroup;
            return __FileLocation;
        }
    }

    public string[] Printers
    {
        get
        {
            string[] obj;
            obj = NicePrintService.GetPrinterList();
            Array.Sort(obj);
            return obj;
        }
    }

    public string Printer
    {
        get { return hdfPrinter.Value; }
        set { hdfPrinter.Value = value; }
    }

    public DataSet Variables
    {
        get
        {
            if(Session["dsVars"] == null)
                try
                {
                    Session["dsVars"] = NicePrintService.GetVariables(FileLocation);
                }
                catch
                {
                    Session["dsVars"] = null;
                }
            
            return (DataSet)Session["dsVars"];
        }
        set
        {
            Session["dsVars"] = value;
            Session["PreviewUrl"] = null;
        }
    }
    #endregion

    #region "Public Functions"
    public string GetPreviewUrl(Int32 Width, Int32 Height, int Kind, int Side)
    {
        if(Session["PreviewUrl"] == null)
            try
            {
                string s;
                //s = BaseURL + NicePrintService.UpdatePreviewFile(Printer, FileLocation, SessionName, Variables, Width, Height, int.Parse(Kind.ToString()), int.Parse(Side.ToString()), true);
                s = BaseURL + NicePrintService.UpdatePreviewFile(Printer, FileLocation, SessionName, Variables, Width, Height, 1, 0, true);
                Session["PreviewUrl"] = s;
            }
            catch (SoapException ex)
            {
                //'Dim eh = New NPSExceptionHelper(ex)
                //'_errorType = eh.ErrorType
                //'_errorMessage = eh.Message
                //'_errorID = eh.ErrorID
                //'If _errorID = 0 Then
                //'    _errorID = -1
                //'End If
                //'If _errorMessage = "" Then
                //'    _errorMessage = "Unknown exception!"
                //'End If
                Session["PreviewUrl"] = "";
            }
            catch (Exception ex)
            {
                _errorType = "General Exception";
                _errorMessage = ex.Message;
                _errorID = "-1";
                Session["PreviewUrl"] = "";
            }
        
        return Session["PreviewUrl"].ToString();
    }

    public string GetVarFormatName(long ID)
    {
        switch(ID)
        {
            case 0:
                return "All";
            case 1:
                return "Numeric";
            case 2:
                return "AlphaNumeric";
            case 3:
                return "Letters";
            case 4:
                return "7bit";
            case 5:
                return "Hex";
            case 6:
                return "Date";
            case 7:
                return "Time";
            default:
                return "Unknown";
        }
    }

    public bool Print(Int32 Quantity, bool IsReprint)
    {
        try
        {
            if(!IsReprint)
                hdfFile.Value = CreatePrintFile(Quantity.ToString());
            
            hdfIsPrinting.Value = "true";
            return true;
        }
        catch (Exception ex)
        {
            _errorType = "General Exception";
            _errorMessage = ex.Message;
            _errorID = "-1";
            return false;
        }
    }

    private string CreatePrintFile(string Quantity)
    {
        string sFile;

        try
        {
            sFile = NicePrintService.CreatePrintFile(Printer, FileLocation, Quantity, SessionName, Variables);
            return sFile;
        }
        catch (SoapException ex)
        {
            //'Dim eh = New NPSExceptionHelper(ex)
            //'_errorType = eh.ErrorType
            //'_errorMessage = eh.Message
            //'_errorID = eh.ErrorID
            sFile = "";
            return sFile;
        }
        catch (Exception ex)
        {
            _errorType = "General Exception";
            _errorMessage = ex.Message;
            _errorID = "-1";
            sFile = "";
            return sFile;
        }
    }

    public bool DirectPrint(Int32 Quantity)
    {
        try
        {
            NicePrintService.Print(Printer, FileLocation, Quantity.ToString(), Variables);
            return true;
        }
        catch (SoapException ex)
        {
            //'Dim eh = New NPSExceptionHelper(ex)
            //'_errorType = eh.ErrorType
            //'_errorMessage = eh.Message
            //'_errorID = eh.ErrorID
            return false;
        }
        catch (Exception ex)
        {
            _errorType = "General Exception";
            _errorMessage = ex.Message;
            _errorID = "-1";
            return false;
        }
    }
    #endregion

    #region "Error handling routines"
    public void ClearError()
    {
        _errorMessage = "";
        _errorID = "0";
        _errorType = "";
    }

    public string ErrorMessage
    {
        get { return _errorMessage; }
    }

    public string ErrorType
    {
        get { return _errorType; }
    }

    public int ErrorID
    {
        get { return int.Parse(_errorID); }
    }
    #endregion "Error handling routines"
}
