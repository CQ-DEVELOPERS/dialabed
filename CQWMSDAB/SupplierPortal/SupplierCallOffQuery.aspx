<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="SupplierCallOffQuery.aspx.cs" Inherits="SupplierPortal_POQuery"
    Title="<%$ Resources:Default, SupplierCallOffQueryTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, SupplierCallOffQueryTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, SupplierCallOffQueryAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <%--<telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="Textbox" />--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Thumbnail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridCallOffSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                    <telerik:AjaxUpdatedControl ControlID="FormViewCallOffHeader" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridLinked">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Search %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Lines %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtOrderNumber" runat="server" Label="Order No:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtProductCode" runat="server" Label="Product Code:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtProduct" runat="server" Label="Description:" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtSupplierCode" runat="server" Label="Supplier Code:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtSupplier" runat="server" Label="Supplier Name:" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
                            <telerik:RadDatePicker ID="rdpFromDate" runat="server"></telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:Label ID="labeToDate" runat="server" Text="<%$ Resources:Default, ToDate %>" Width="100px"></asp:Label>
                            <telerik:RadDatePicker ID="rdpToDate" runat="server"></telerik:RadDatePicker>
                        </td>
                        <td>
                            <telerik:RadButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"></telerik:RadButton>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="RadGridCallOffSearch" runat="server" OnRowDataBound="RadGridCallOffSearch_RowDataBound"
                    AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true"
                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlanning"
                    OnSelectedIndexChanged="RadGridCallOffSearch_SelectedIndexChanged" PageSize="30" ShowGroupPanel="True" Height="500px" Skin="Metro"
                    OnItemCommand="RadGridCallOffSearch_ItemCommand">
                    <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                    <MasterTableView DataKeyNames="CallOffHeaderId,CallOffDetailId" DataSourceID="ObjectDataSourcePlanning"
                        CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" FilterControlAltText="Filter EditCommandColumn column"></telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="PurchaseOrder" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="PurchaseOrder" UniqueName="PurchaseOrder"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="LineNumber" HeaderText="<%$ Resources:Default, LineNumber %>" SortExpression="LineNumber" UniqueName="LineNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="CallOffNumber" HeaderText="<%$ Resources:Default, CallOffNumber %>" SortExpression="CallOffNumber" UniqueName="CallOffNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>" SortExpression="ExternalCompanyCode" UniqueName="ExternalCompanyCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>" SortExpression="ExternalCompany" UniqueName="ExternalCompany"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" UniqueName="ProductCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" UniqueName="Product"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" UniqueName="SKUCode"></telerik:GridBoundColumn>
                            
                            <telerik:GridBoundColumn ReadOnly="true" DataField="PendingQuantity" HeaderText="<%$ Resources:Default, PendingQuantity %>" SortExpression="PendingQuantity" UniqueName="PendingQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ShippedQuantity" HeaderText="<%$ Resources:Default, ShippedQuantity %>" SortExpression="ShippedQuantity" UniqueName="ShippedQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="LastDateShipped" HeaderText="<%$ Resources:Default, LastDateShipped %>" SortExpression="LastDateShipped" UniqueName="LastDateShipped"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                        ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                        AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowColumnResize="True" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="Shipment"
                    SelectMethod="SearchCOQuery" UpdateMethod="UpdateCOQuery">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:ControlParameter Name="OrderNumber" ControlID="rtxtOrderNumber" />
                        <asp:ControlParameter Name="ProductCode" ControlID="rtxtProductCode" />
                        <asp:ControlParameter Name="Product" ControlID="rtxtProduct" />
                        <asp:ControlParameter Name="ExternalCompanyCode" ControlID="rtxtSupplierCode" />
                        <asp:ControlParameter Name="ExternalCompany" ControlID="rtxtSupplier" />
                        <asp:ControlParameter Name="FromDate" ControlID="rdpFromDate" />
                        <asp:ControlParameter Name="ToDate" ControlID="rdpToDate" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="CallOffDetailId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="receivedQuantity" Type="Decimal"></asp:Parameter>
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel2">
                <asp:FormView ID="FormViewCallOffHeader" runat="server" AutoGenerateRows="false"
                     DataSourceID="ObjectDataSourceCallOffDetailsView" DataKeyNames="CallOffHeaderId" >
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <telerik:RadTextBox ID="LabelOrderNumber" runat="server" Label="<%$ Resources:Default, OrderNumber %>" Text='<%# Bind("CallOffNumber") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="LabelDeliveryDate" runat="server" Label="<%$ Resources:Default, DeliveryDate %>" Text='<%# Bind("DeliveryDate") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadTextBox ID="LabelWarehouse" runat="server" Label="<%$ Resources:Default, Warehouse %>" Text='<%# Bind("Warehouse") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="LabelPriority" runat="server" Label="<%$ Resources:Default, Priority %>" Text='<%# Bind("Priority") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="ObjectDataSourceCallOffDetailsView" runat="server" TypeName="Shipment" SelectMethod="SelectCOQuery">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Type="Int32" Name="CallOffHeaderId" SessionField="CallOffHeaderId"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
                <telerik:RadGrid ID="RadGridLinked" runat="server" DataSourceID="ObjectDataSourceLinked" 
                    AllowAutomaticUpdates="true" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" Skin="Metro"
                    OnItemCommand="RadGridLinked_ItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="CallOffDetailId,StatusCode">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" FilterControlAltText="Filter EditCommandColumn column"></telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="PurchaseOrder" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="PurchaseOrder" UniqueName="PurchaseOrder"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="LineNumber" HeaderText="<%$ Resources:Default, LineNumber %>" SortExpression="LineNumber" UniqueName="LineNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate" UniqueName="DeliveryDate"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" UniqueName="ProductCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" UniqueName="Product"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" UniqueName="SKUCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="DocumentType" HeaderText="<%$ Resources:Default, DocumentType %>" SortExpression="DocumentType" UniqueName="DocumentType"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>" SortExpression="DocumentNumber" UniqueName="DocumentNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RequestQuantity" HeaderText="<%$ Resources:Default, RequestQuantity %>" SortExpression="RequestQuantity" UniqueName="RequestQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="DeliveryNoteQuantity" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>" SortExpression="DeliveryNoteQuantity" UniqueName="DeliveryNoteQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="DeliveredQuantity" HeaderText="<%$ Resources:Default, DeliveredQuantity %>" SortExpression="DeliveredQuantity" UniqueName="DeliveredQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch" UniqueName="Batch"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ShipmentNumber" HeaderText="<%$ Resources:Default, ShipmentNumber %>" SortExpression="ShipmentNumber" UniqueName="ShipmentNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" UniqueName="Status"></telerik:GridBoundColumn>
                            <telerik:GridButtonColumn DataTextField="NextStatus" HeaderText="<%$ Resources:Default, NextStatus %>" CommandName="Select"></telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceLinked" runat="server" TypeName="Shipment"
                    SelectMethod="SearchCOQueryLine" UpdateMethod="UpdateCOQueryLine">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="CallOffHeaderId" Type="Int32" SessionField="CallOffHeaderId" DefaultValue="-1" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="CallOffDetailId" SessionField="CallOffDetailId" Type="Int32" />
                        <asp:Parameter Name="RequestQuantity" Type="Decimal" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceTransportMode" TypeName="Shipment" SelectMethod="GetTransportModes" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceExternalCompany" TypeName="Shipment" SelectMethod="GetParticipants" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceNotificationMethod" TypeName="Shipment" SelectMethod="GetNotificationMethods" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
