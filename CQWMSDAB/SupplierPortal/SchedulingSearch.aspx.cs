using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Scheduling_SchedulingSearch : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("SchedulingSearch" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonDocumentSearch_Click 
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";

        try
        {
            GridViewInboundDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SchedulingSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDocumentSearch_Click

    #region GridViewInboundDocument_SelectedIndexChanged 
    protected void GridViewInboundDocument_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInboundDocument_SelectedIndexChanged";
        
        try
        {
            Session["ReceiptId"] = GridViewInboundDocument.SelectedDataKey["ReceiptId"];

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SchedulingSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GridViewInboundDocument_SelectedIndexChanged

    protected String CheckDate(string CheckTheDate)
    {

        return CheckTheDate;
    }



    #region ObjectDataSourceInboundDocument_OnUpdating
    protected void ObjectDataSourceInboundDocument_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        theErrMethod = "ObjectDataSourceInboundDocument_OnUpdating";

        try
        {
            DateTime plannedDeliveryDate;
            DateTime actualDeliveryDate;

            DateTime.TryParse(((TextBox)DetailsViewUpdate.FindControl("TextBoxPlannedDeliveryDateEdit")).Text + " " + ((TextBox)DetailsViewUpdate.FindControl("TextBoxPlannedDeliveryTimeEdit")).Text, out plannedDeliveryDate);
            DateTime.TryParse(((TextBox)DetailsViewUpdate.FindControl("TextBoxActualDeliveryDateEdit")).Text + " " + ((TextBox)DetailsViewUpdate.FindControl("TextBoxActualDeliveryTimeEdit")).Text, out actualDeliveryDate);

            e.InputParameters["PlannedDeliveryDate"] = plannedDeliveryDate;
            e.InputParameters["ActualDeliveryDate"] = actualDeliveryDate;

            if (e.InputParameters["ActualDeliveryDate"].ToString() == "0001/01/01 12:00:00 AM")
                e.InputParameters["ActualDeliveryDate"] = DateTime.Parse("1900/01/01 00:00:00.000");

            Master.MsgText = "Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SchedulingSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceInboundDocument_OnUpdating"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "SchedulingSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "SchedulingSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling   
    
}
