<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="BayScheduler.aspx.cs" Inherits="SupplierPortal_BayScheduler" Title="<%$ Resources:Default, DefaultOutboundTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>
<%@ Register Src="../Common/InboundShipmentSearch.ascx" TagName="InboundShipmentSearch" TagPrefix="uc1" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultOutboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultOutboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridOrders">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridBay" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridPlan" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridResult" />
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerAllBays" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridBay">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridBay" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDocumentSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridOrderLines">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                    <telerik:AjaxUpdatedControl ControlID="DetailsLinesReceived" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonPalletise" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonAutoLocations" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultDelivery" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultReceived" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultZero" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonPrintLabel" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonTicketLabel" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSample" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridPlan">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPlan" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridResult" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridResult">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPlan" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridResult" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSchedulerAllBays">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerAllBays" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">  
                <UpdatedControls>  
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerAllBays" /> 
                </UpdatedControls>  
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Shipment%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Lines%>"></telerik:RadTab>
                <telerik:RadTab Text="Loading Bay"></telerik:RadTab>
                <telerik:RadTab Text="Teams"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                &nbsp;
                <uc1:InboundShipmentSearch ID="InboundShipmentSearch1" runat="server"></uc1:InboundShipmentSearch>
                <telerik:RadButton ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search %>" OnClick="ButtonSearch_Click" />
            <telerik:RadGrid ID="RadGridOrders" runat="server" Skin="Metro" AllowAutomaticUpdates="true"
                AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
                DataSourceID="ObjectDataSourceReceiptDocument" OnSelectedIndexChanged="RadGridOrders_SelectedIndexChanged"
                OnRowDataBound="RadGridOrders_RowDataBound" OnItemCommand="RadGridOrders_ItemCommand">
                <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                <MasterTableView DataKeyNames="InboundShipmentId,ReceiptId,AllowPalletise,OrderNumber" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn DataField="InboundShipmentId" HeaderText='<%$ Resources:Default,InboundShipmentId %>' ReadOnly="true" SortExpression="InboundShipmentId" />
                        <telerik:GridBoundColumn DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' ReadOnly="true" SortExpression="OrderNumber" />
                        <telerik:GridBoundColumn DataField="PrincipalCode" HeaderText='<%$ Resources:Default,PrincipalCode %>' ReadOnly="true" SortExpression="PrincipalCode" />
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,Delivery %>' SortExpression="Delivery">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelDelivery" runat="server" Text='<%# Bind("Delivery") %>' Font-Bold="true"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="SupplierCode" HeaderText='<%$ Resources:Default,SupplierCode %>' ReadOnly="true" SortExpression="SupplierCode" />
                        <telerik:GridBoundColumn DataField="Supplier" HeaderText='<%$ Resources:Default,Supplier %>' ReadOnly="true" SortExpression="Supplier" />
                        <telerik:GridBoundColumn DataField="NumberOfLines" HeaderText='<%$ Resources:Default,Lines %>' ReadOnly="true" SortExpression="NumberOfLines" />
                        <telerik:GridBoundColumn DataField="PlannedDeliveryDate" HeaderText='<%$ Resources:Default,PlannedDeliveryDate %>' ReadOnly="true" SortExpression="PlannedDeliveryDate" />
                        <telerik:GridBoundColumn DataField="DeliveryDate" HeaderText='<%$ Resources:Default,DeliveryDate %>' SortExpression="DeliveryDate" DataFormatString="{0:d}" />
                        <telerik:GridBoundColumn DataField="Status" HeaderText='<%$ Resources:Default,Status %>' ReadOnly="true" SortExpression="Status" />
                        <telerik:GridBoundColumn DataField="InboundDocumentType" HeaderText='<%$ Resources:Default,InboundDocumentType %>' ReadOnly="true" SortExpression="InboundDocumentType" />
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,Location %>' SortExpression="Location">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                    DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Rating" HeaderText='<%$ Resources:Default,Rating %>' ReadOnly="true" SortExpression="Rating" />
                        <telerik:GridBoundColumn DataField="DeliveryNoteNumber" HeaderText='<%$ Resources:Default,DeliveryNoteNumber %>' SortExpression="DeliveryNoteNumber" />
                        <telerik:GridBoundColumn DataField="SealNumber" HeaderText='<%$ Resources:Default,SealNumber %>' SortExpression="SealNumber" />
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,Priority %>' SortExpression="Priority">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelPriority" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                    DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="VehicleRegistration" HeaderText='<%$ Resources:Default,VehicleRegistration %>' SortExpression="VehicleRegistration" />
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, CheckSheet %>" HeaderText="<%$ Resources:Default, CheckSheet %>" CommandName="CheckSheet"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn DataTextField="Redelivery" Text="<%$ Resources:Default, Redelivery %>" HeaderText="<%$ Resources:Default, Redelivery %>" CommandName="Redelivery"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn DataTextField="Complete" Text="<%$ Resources:Default, Complete %>" HeaderText="<%$ Resources:Default, Complete %>" CommandName="Complete"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, DeliveryConformance %>" HeaderText="<%$ Resources:Default, DeliveryConformance %>" CommandName="DeliveryConformance"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, PrintGRN %>" HeaderText="<%$ Resources:Default, PrintGRN %>" CommandName="PrintGRN"></telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Remarks" HeaderText='<%$ Resources:Default,Remarks %>' SortExpression="Remarks" />
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                    <Resizing AllowColumnResize="true"></Resizing>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="True" />
            </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourceReceiptDocument" runat="server" TypeName="Receiving"
                SelectMethod="GetReceivingDocuments" UpdateMethod="UpdateReceipt">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId" Type="Int32" />
                    <asp:SessionParameter Name="InboundShipmentId" DefaultValue="-1" Type="Int32" SessionField="ParameterInboundShipmentId" />
                    <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                    <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                    <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                    <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="inboundShipmentId" Type="Int32" />
                    <asp:Parameter Name="allowPalletise" Type="Boolean" />
                    <asp:Parameter Name="deliveryDate" Type="DateTime" />
                    <asp:Parameter Name="locationId" DefaultValue="-1" Type="Int32" />
                    <asp:Parameter Name="deliveryNoteNumber" Type="String" />
                    <asp:Parameter Name="sealNumber" Type="String" />
                    <asp:Parameter Name="priorityId" DefaultValue="-1" Type="Int32" />
                    <asp:Parameter Name="vehicleRegistration" Type="String" />
                    <asp:Parameter Name="remarks" Type="String" />
                    <asp:Parameter Name="receiptId" DefaultValue="-1" Type="Int32" />
                    <asp:Parameter Name="delivery" DefaultValue="-1" Type="Int32" />
                    <asp:Parameter Name="orderNumber" DefaultValue="-1" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Location"
                SelectMethod="GetLocationsByAreaCode">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:Parameter Name="AreaCode" Type="String" DefaultValue="R" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" TypeName="Priority"
                SelectMethod="GetPriorities">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </telerik:RadPageView>
                
        <telerik:RadPageView runat="Server" ID="RadPageView1" HeaderText="<%$ Resources:Default, ReceiveLines%>">
            <table>
                <tr>
                    <td>
                        <telerik:RadButton ID="ButtonDefaultDelivery" runat="server" Text="<%$ Resources:Default, Delivery%>" OnClick="ButtonDefaultDelivery_Click"></telerik:RadButton>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="RadGridOrderLines" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceReceiptLine"
                AllowAutomaticUpdates="true" AllowMultiRowSelection="true" OnItemCommand="RadGridOrderLines_ItemCommand" OnEditCommand="RadGridOrderLines_EditCommand"
                AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30" ShowFooter="true" EnableLinqExpressions="true" Width="1200px">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView DataKeyNames="ReceiptLineId,StorageUnitId,BatchId,Boxes" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                <Columns>
                    <telerik:GridTemplateColumn Visible="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenNumberOfPallets" runat="server" Value='<%# Bind("NumberOfPallets") %>' />
                            <asp:HiddenField ID="HiddenReceiptLineId" runat="server" Value='<%# Bind("ReceiptLineId") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="LineNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, LineNumber %>" SortExpression="LineNumber" />
                    <telerik:GridBoundColumn DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" />
                    <telerik:GridBoundColumn DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" />
                    <telerik:GridBoundColumn DataField="ChildProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ChildProductCode %>" SortExpression="ChildProductCode" />
                    <telerik:GridBoundColumn DataField="ChildProduct" ReadOnly="True" HeaderText="<%$ Resources:Default, ChildProduct %>" SortExpression="ChildProduct" />
                    <%--<telerik:GridDropDownColumn HeaderText="<%$ Resources:Default, Batch %>" UniqueName="StorageUnitBatchId" ListTextField="Batch" ListValueField="StorageUnitBatchId" DataField="StorageUnitBatchId" DataSourceID="ObjectDataSourceBatch"></telerik:GridDropDownColumn>--%>
                    <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Batch %>">
                        <EditItemTemplate>
                            <asp:Button ID="ButtonBatchPopup" runat="server" Text="<%$ Resources:Default, ButtonBatchPopup %>"
                                Visible='<%# Eval("BatchButton") %>' CausesValidation="false" OnClientClick="javascript:openNewWindowBatch();" />
                            <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceBatch"
                                DataTextField="Batch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>'
                                Visible='<%# Eval("BatchSelect") %>' OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" />
                    <telerik:GridBoundColumn DataField="SKU" ReadOnly="True" HeaderText="<%$ Resources:Default, SKU %>" SortExpression="SKU" />
                    <telerik:GridBoundColumn DataField="Boxes" ReadOnly="True" HeaderText="<%$ Resources:Default, Packs %>" SortExpression="Boxes" />
                    <telerik:GridBoundColumn DataField="RequiredQuantity" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderQuantity %>" SortExpression="RequiredQuantity" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="<%$ Resources:Default, OrderQuantity %>" />
                    <telerik:GridBoundColumn DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>" SortExpression="DeliveryNoteQuantity" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="<%$ Resources:Default, DeliveryNoteQuantity %>" />
                    <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, AlternateBatch %>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxAlternateBatch" runat="server" Checked='<%# Bind("AlternateBatch") %>'>
                            </asp:CheckBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber" />
                    <telerik:GridBoundColumn DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" />
                    <telerik:GridBoundColumn DataField="NumberOfPallets" ReadOnly="True" HeaderText="<%$ Resources:Default, NumberOfPallets %>" />
                    <telerik:GridButtonColumn Text="<%$ Resources:Default, CheckSheet %>" HeaderText="<%$ Resources:Default, CheckSheet %>" CommandName="CheckSheet"></telerik:GridButtonColumn>
                    <telerik:GridButtonColumn Text="<%$ Resources:Default, SerialNumber %>" HeaderText="<%$ Resources:Default, SerialNumber %>" CommandName="SerialNumber"></telerik:GridButtonColumn>
                    <telerik:GridBoundColumn DataField="PalletId" ReadOnly="True" HeaderText="<%$ Resources:Default, PalletId %>" />
                    <telerik:GridBoundColumn DataField="BOELineNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, BOELineNumber %>" />
                    <telerik:GridBoundColumn DataField="CountryofOrigin" ReadOnly="True" HeaderText="<%$ Resources:Default, CountryofOrigin %>" />
                    <telerik:GridBoundColumn DataField="TariffCode" ReadOnly="True" HeaderText="<%$ Resources:Default, TariffCode %>" />
                    <telerik:GridBoundColumn DataField="Reference1" ReadOnly="True" HeaderText="<%$ Resources:Default, Reference1 %>" />
                    <telerik:GridBoundColumn DataField="Reference2" ReadOnly="True" HeaderText="<%$ Resources:Default, Reference2 %>" />
                    <telerik:GridBoundColumn DataField="UnitPrice" ReadOnly="True" HeaderText="<%$ Resources:Default, UnitPrice %>" />
                </Columns>
            </MasterTableView>
            <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                <Resizing AllowColumnResize="true"></Resizing>
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings ShowUnGroupButton="True" />
        </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourceReceiptLine" runat="server" TypeName="Receiving"
                SelectMethod="GetReceiptLines" OnSelecting="ObjectDataSourceReceiptLine_OnSelecting"
                UpdateMethod="SetReceiptLines" OnUpdating="ObjectDataSourceReceiptLine_OnUpdating"
                OnUpdated="ObjectDataSourceReceiptLine_Updated">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="inboundShipmentId" Type="Int32" />
                    <asp:Parameter Name="receiptId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" />
                    <asp:Parameter Name="Batch" Type="String" />
                    <asp:Parameter Name="OperatorId" Type="Int32" />
                    <asp:Parameter Name="DeliveryNoteQuantity" Type="Decimal" />
                    <asp:Parameter Name="AlternateBatch" Type="Boolean" />
                    <asp:Parameter Name="ReceiptLineId" Type="Int32" />
                    <asp:Parameter Name="StorageUnitId" Type="Int32" />
                    <asp:Parameter Name="BatchId" Type="Int32" />
                    <asp:Parameter Name="Boxes" Type="Decimal" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
                SelectMethod="GetReasonsByType">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="RW" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                SelectMethod="GetBatchesNotExpired" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUntiId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

            <asp:Label ID="LabelQty" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
            <telerik:RadNumericTextBox ID="TextBoxQty" runat="server" DisplayText="Default" MinValue="1" MaxValue="100" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true"></telerik:RadNumericTextBox>
            <telerik:RadButton ID="ButtonPrintLabel" runat="server" OnClick="ButtonPrintLabel_Click" Text="<%$ Resources:Default, PrintProduct%>" />

            <asp:Label ID="LabelTicket" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
            <telerik:RadNumericTextBox ID="TextBoxTicket" runat="server" DisplayText="Default" MinValue="1" MaxValue="100" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true"></telerik:RadNumericTextBox>
            <telerik:RadButton ID="ButtonTicketLabel" runat="server" OnClick="ButtonTicketLabel_Click" Text="<%$ Resources:Default, PrintTicket%>" />

            <asp:Label ID="LabelSample" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
            <telerik:RadNumericTextBox ID="TextBoxSample" runat="server" DisplayText="Default" MinValue="1" MaxValue="100" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true"></telerik:RadNumericTextBox>
            <telerik:RadButton ID="ButtonSample" runat="server" OnClick="ButtonPrintSample_Click" Text="<%$ Resources:Default, PrintSample%>" />
            </telerik:RadPageView>

            <telerik:RadPageView runat="Server" ID="TabPanel2">
                <telerik:RadScheduler ID="RadSchedulerAllBays" runat="server" DataEndField="PlannedEnd" GroupBy="Location"
                    DataKeyField="DockScheduleId" DataRecurrenceField="RecurrenceRule" 
                    DataRecurrenceParentKeyField="RecurrenceParentID" DataSourceID="ObjectDataSourceDockSchedulerAllBays"
                    DataDescriptionField="Description" OnAppointmentDataBound="RadSchedulerAllBays_AppointmentDataBound"
                    DataStartField="PlannedStart" DataSubjectField="Subject" CssClass='<%# Bind("CssClass") %>' Skin="Metro" Height="700px" >
                    <AdvancedForm Modal="true" />
                    <TimelineView UserSelectable="false"  />
                    <%--<TimelineView SlotDuration="00:15" NumberOfSlots="96" ColumnHeaderDateFormat="hh:mm"/>--%>
                    <ResourceTypes>
                        <telerik:ResourceType DataSourceID="ObjectDataSourceVehicleLocation" ForeignKeyField="LocationId" 
                            KeyField="LocationId" Name="Location" TextField="Location" />
                        <telerik:ResourceType DataSourceID="ObjectDataSourceCssClass" ForeignKeyField="CssClass" 
                            KeyField="CssClass" Name="CssClass" TextField="CssClass" />
                    </ResourceTypes>
                    <ResourceStyles>
                        <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryBlue" ApplyCssClass="rsCategoryBlue" />
                        <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryGreen" ApplyCssClass="rsCategoryGreen" />
                        <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryGray" ApplyCssClass="rsCategoryGray" />
                        <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryRed" ApplyCssClass="rsCategoryRed" />
                        <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryLime" ApplyCssClass="rsCategoryLime" />
                    </ResourceStyles>
                </telerik:RadScheduler>
                <asp:ObjectDataSource ID="ObjectDataSourceDockSchedulerAllBays" runat="server" TypeName="DockSchedule"
                    SelectMethod="DockScheduleSelect" InsertMethod="DockScheduleInsertInbound" UpdateMethod="DockScheduleUpdate" DeleteMethod="DockScheduleDelete">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
                        <asp:Parameter Name="Subject" Type="String" />
                        <asp:Parameter Name="PlannedStart" Type="DateTime" />
                        <asp:Parameter Name="PlannedEnd" Type="DateTime" />
                        <asp:Parameter Name="RecurrenceRule" Type="String" />
                        <asp:Parameter Name="RecurrenceParentID" Type="Int32" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                        <asp:SessionParameter Name="inboundShipmentId" SessionField="InboundShipmentId" Type="Int32" />
                        <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
                        <asp:Parameter Name="Subject" Type="String" />
                        <asp:Parameter Name="PlannedStart" Type="DateTime" />
                        <asp:Parameter Name="PlannedEnd" Type="DateTime" />
                        <asp:Parameter Name="RecurrenceRule" Type="String" />
                        <asp:Parameter Name="RecurrenceParentID" Type="Int32" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                        <asp:Parameter Name="CssClass" Type="String" />
                        <asp:Parameter Name="DockScheduleId" Type="Int32" />
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="DockScheduleId" Type="Int32" />
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel4">
                <table>
                    <tr>
                        <td>
                            <telerik:RadGrid ID="RadGridPlan" runat="server" 
                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlan"
                                OnSelectedIndexChanged="RadGridPlan_SelectedIndexChanged" Width="500px" Skin="Metro">
                                <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                                <MasterTableView DataKeyNames="OutboundShipmentId" DataSourceID="ObjectDataSourcePlan" >
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                            SortExpression="OutboundShipmentId" 
                                            FilterControlAltText="Filter OutboundShipmentId column" 
                                            UniqueName="OutboundShipmentId">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                            SortExpression="InstructionType" 
                                            FilterControlAltText="Filter InstructionType column" UniqueName="InstructionType">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>"
                                            SortExpression="RequiredQuantity" 
                                            FilterControlAltText="Filter RequiredQuantity column" UniqueName="RequiredQuantity" DataFormatString="{0:G0}">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OperatorGroup" HeaderText="<%$ Resources:Default, OperatorGroup %>"
                                            SortExpression="OperatorGroup" 
                                            FilterControlAltText="Filter OperatorGroup column" UniqueName="OperatorGroup">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="TeamStart" HeaderText="<%$ Resources:Default, TeamStart %>"
                                            SortExpression="TeamStart" 
                                            FilterControlAltText="Filter TeamStart column" UniqueName="TeamStart">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="TeamEnd" HeaderText="<%$ Resources:Default, TeamEnd %>"
                                            SortExpression="TeamEnd" 
                                            FilterControlAltText="Filter TeamEnd column" UniqueName="TeamEnd">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="DockTime" HeaderText="<%$ Resources:Default, DockTime %>"
                                            SortExpression="DockTime" 
                                            FilterControlAltText="Filter DockTime column" UniqueName="DockTime">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                                    AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <Resizing AllowColumnResize="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:ObjectDataSource ID="ObjectDataSourcePlan" runat="server" TypeName="DockSchedule"
                                SelectMethod="OutboundShipmentTeamPlan">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                        <td>
                            <telerik:RadGrid ID="RadGridResult" runat="server" 
                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceResult"
                                OnSelectedIndexChanged="RadGridResult_SelectedIndexChanged" Width="900px" Skin="Metro" AllowMultiRowSelection="true">
                                
                                <MasterTableView DataKeyNames="OutboundShipmentId,InstructionTypeId,OperatorGroupId,Planned" DataSourceID="ObjectDataSourceResult" >
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                            SortExpression="OutboundShipmentId" 
                                            FilterControlAltText="Filter OutboundShipmentId column" 
                                            UniqueName="OutboundShipmentId">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                            SortExpression="InstructionType" 
                                            FilterControlAltText="Filter InstructionType column" UniqueName="InstructionType">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>"
                                            SortExpression="RequiredQuantity" 
                                            FilterControlAltText="Filter RequiredQuantity column" UniqueName="RequiredQuantity" DataFormatString="{0:G0}">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OperatorGroup" HeaderText="<%$ Resources:Default, OperatorGroup %>"
                                            SortExpression="OperatorGroup" 
                                            FilterControlAltText="Filter OperatorGroup column" UniqueName="OperatorGroup">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="HourlyPickRate" HeaderText="Hourly Pick Rate"
                                            SortExpression="HourlyPickRate" 
                                            FilterControlAltText="Filter HourlyPickRate column" UniqueName="HourlyPickRate" DataFormatString="{0:G0}">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="PlannedStart" HeaderText="Planned Start"
                                            SortExpression="PlannedStart" 
                                            FilterControlAltText="Filter TeamStart column" UniqueName="PlannedStart">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="RequiredStart" HeaderText="Required Start"
                                            SortExpression="RequiredStart" 
                                            FilterControlAltText="Filter RequiredStart column" UniqueName="RequiredStart">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                                    AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <Resizing AllowColumnResize="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:ObjectDataSource ID="ObjectDataSourceResult" runat="server" TypeName="DockSchedule"
                                SelectMethod="InboundShipmentTeamResult">
                                <SelectParameters>
                                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="InboundShipmentId" SessionField="InboundShipmentId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
        SelectMethod="GetLocationsByAreaCode">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:Parameter Name="AreaCode" Type="String" DefaultValue="R" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceVehicleLocation" runat="server" TypeName="DockSchedule"
        SelectMethod="GetLocationsByInShipment">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="inboundShipmentId" SessionField="InboundShipmentId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceCssClass" runat="server" TypeName="DockSchedule"
        SelectMethod="CssClass">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <telerik:RadButton ID="ButtonSaveSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveSettings_Click" />
</asp:Content>