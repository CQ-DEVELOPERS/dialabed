using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using System.IO;

public partial class SupplierPortal_UploadFile : System.Web.UI.Page
{

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            if (Session["UploadTableId"] == null)
                throw new Exception("No UploadTableId selected.");

            if (fileUploadDocument.PostedFile.ContentLength > 0)
            {
                // Get the File name and Extension
                string strFileName = Path.GetFileName(fileUploadDocument.PostedFile.FileName);
                string strFileExtension = Path.GetExtension(fileUploadDocument.PostedFile.FileName);
                //
                // Extract the content of the Document into a Byte array
                int intlength = fileUploadDocument.PostedFile.ContentLength;
                Byte[] byteData = new Byte[intlength];
                fileUploadDocument.PostedFile.InputStream.Read(byteData, 0, intlength);

                CallOff co = new CallOff();

                co.SaveFile(Session["ConnectionStringName"].ToString(),
                            strFileName,
                            strFileExtension,
                            byteData,
                            (int)Session["UploadTableId"],
                            Session["UploadTableName"].ToString(),
                            Session["UploadFileType"].ToString());

                RadGridFiles.DataBind();

                Master.MsgText = "Document Uploaded Succesfully";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SupplierPortal_CallOffMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonUpload_Click

    #region ButtonClose_Click
    protected void ButtonClose_Click(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "ClosePopup", "window.close()", true);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SupplierPortal_CallOffMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonClose_Click

    //#region ButtonDownload_Click
    //protected void ButtonDownload_Click(object sender, EventArgs e)
    //{
    //    theErrMethod = "ButtonDownload_Click";
    //    try
    //    {
    //        CallOff co = new CallOff();
    //        DataTable dt = new DataTable();
    //        int saveFileId = -1;

    //        foreach (GridDataItem item in RadGridFiles.Items)
    //        {
    //            if (item.Selected)
    //            {
    //                saveFileId = int.Parse(item.GetDataKeyValue("SavedFileId").ToString());
    //                Byte[] bytes = co.GetFileData(Session["ConnectionStringName"].ToString(), saveFileId);
    //                string fileName = co.GetFileName(Session["ConnectionStringName"].ToString(), saveFileId);

    //                //fileName = fileName.Replace(' ', '_');

    //                Response.ContentType = "application/octet-stream";
    //                Response.AppendHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName));

    //                if (Response.IsClientConnected)
    //                {
    //                    // Read the data into the buffer and write into the output stream.
    //                    Response.OutputStream.Write(bytes, 0, bytes.Length);
    //                    Response.Flush();
    //                    Response.End();
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("SupplierPortal_CallOffMaintenance" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion ButtonDownload_Click

    #region RadGridFiles_ItemCreated
    protected void RadGridFiles_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            RadUpload upload = (item.EditManager.GetColumnEditor("AttachmentColumn") as GridAttachmentColumnEditor).RadUploadControl;
            upload.OnClientFileSelected = "uploadFileSelected";
        }
    }
    #endregion RadGridFiles_ItemCreated

    #region RadGridFiles_ItemCommand
    protected void RadGridFiles_ItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DownloadAttachmentCommandName)
        {
            //if (DownloadOptionsRBL.SelectedValue == "Manual")
            {
                e.Canceled = true;

                GridDownloadAttachmentCommandEventArgs args = e as GridDownloadAttachmentCommandEventArgs;
                CallOff co = new CallOff();
                string fileName = args.FileName;
                int saveFileId = (int)args.AttachmentKeyValues["SavedFileId"];

                Byte[] bytes = co.GetFileData(Session["ConnectionStringName"].ToString(), saveFileId);

                Response.ContentType = "application/octet-stream";
                Response.AppendHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName));

                if (Response.IsClientConnected)
                {
                    // Read the data into the buffer and write into the output stream.
                    Response.OutputStream.Write(bytes, 0, bytes.Length);
                    Response.Flush();
                    Response.End();
                }

                //Response.Clear();
                //Response.ContentType = "application/octet-stream";
                //Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                //Response.BinaryWrite(bytes);
                //Response.End();
            }
        }

        //if (e.CommandName == RadGrid.UpdateCommandName ||
        //    e.CommandName == RadGrid.PerformInsertCommandName)
        //{
        //    GridEditableItem item = e.Item as GridEditableItem;

        //    if (!(item is GridEditFormInsertItem))
        //    {
        //        fileId = (int)item.GetDataKeyValue("ID");
        //    }

        //    string fileName = (item.EditManager.GetColumnEditor("FileName") as GridTextBoxColumnEditor).Text;
        //    fileData = (item.EditManager.GetColumnEditor("AttachmentColumn") as GridAttachmentColumnEditor).UploadedFileContent;
        //    DateTime? uploadDate = (item.EditManager.GetColumnEditor("UploadDate") as GridDateTimeColumnEditor).PickerControl.SelectedDate;
        //    string uploadedBy = (item.EditManager.GetColumnEditor("UploadedBy") as GridTextBoxColumnEditor).TextBoxControl.Text;

        //    if (fileData.Length == 0 || fileName.Trim() == string.Empty)
        //    {
        //        e.Canceled = true;
        //        RadGridFiles.Controls.Add(new LiteralControl("<b style='color:red;'>No file uploaded. Action canceled.</b>"));
        //    }

        //    if (!uploadDate.HasValue || String.IsNullOrEmpty(uploadedBy.Trim()))
        //    {
        //        e.Canceled = true;
        //        RadGridFiles.Controls.Add(new LiteralControl("<b style='color:red;'>Please, fill in all the fields.</b>"));
        //    }
        //}
    }
    #endregion RadGridFiles_ItemCommand

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}
