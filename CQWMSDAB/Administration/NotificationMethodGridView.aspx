<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NotificationMethodGridView.aspx.cs" Inherits="Administration_NotificationMethodGridView" Title="NotificationMethod" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/NotificationMethodGridView.ascx" TagName="NotificationMethodGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="NotificationMethod Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:NotificationMethodGridView ID="NotificationMethodGridView1" runat="server" />
</asp:Content>
