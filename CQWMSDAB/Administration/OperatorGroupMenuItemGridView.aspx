<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OperatorGroupMenuItemGridView.aspx.cs" Inherits="Administration_OperatorGroupMenuItemGridView" Title="OperatorGroupMenuItem" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/OperatorGroupMenuItemGridView.ascx" TagName="OperatorGroupMenuItemGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="OperatorGroupMenuItem Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="OperatorGroupMenuItem">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OperatorGroupMenuItem" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:OperatorGroupMenuItemGridView ID="OperatorGroupMenuItemGridView1" runat="server" />
</asp:Content>
