<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InterfaceDocumentDescGridView.aspx.cs" Inherits="Administration_InterfaceDocumentDescGridView" Title="InterfaceDocumentDesc" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InterfaceDocumentDescGridView.ascx" TagName="InterfaceDocumentDescGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InterfaceDocumentDesc Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InterfaceDocumentDesc">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceDocumentDesc" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InterfaceDocumentDescGridView ID="InterfaceDocumentDescGridView1" runat="server" />
</asp:Content>
