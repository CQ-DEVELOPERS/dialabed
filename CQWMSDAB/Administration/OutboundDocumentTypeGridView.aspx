<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OutboundDocumentTypeGridView.aspx.cs" Inherits="Administration_OutboundDocumentTypeGridView" Title="OutboundDocumentType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/OutboundDocumentTypeGridView.ascx" TagName="OutboundDocumentTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="OutboundDocumentType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="OutboundDocumentType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OutboundDocumentType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:OutboundDocumentTypeGridView ID="OutboundDocumentTypeGridView1" runat="server" />
</asp:Content>
