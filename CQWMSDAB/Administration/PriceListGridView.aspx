<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PriceListGridView.aspx.cs" Inherits="Administration_PriceListGridView" Title="PriceList" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/PriceListGridView.ascx" TagName="PriceListGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="PriceList Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:PriceListGridView ID="PriceListGridView1" runat="server" />
</asp:Content>
