<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WarehouseCodeReferenceGridView.aspx.cs" Inherits="Administration_WarehouseCodeReferenceGridView" Title="WarehouseCodeReference" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/WarehouseCodeReferenceGridView.ascx" TagName="WarehouseCodeReferenceGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="WarehouseCodeReference Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="WarehouseCodeReference">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="WarehouseCodeReference" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:WarehouseCodeReferenceGridView ID="WarehouseCodeReferenceGridView1" runat="server" />
</asp:Content>
