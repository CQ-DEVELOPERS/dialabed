<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InterfaceMappingColumnGridView.aspx.cs" Inherits="Administration_InterfaceMappingColumnGridView" Title="InterfaceMappingColumn" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InterfaceMappingColumnGridView.ascx" TagName="InterfaceMappingColumnGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InterfaceMappingColumn Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InterfaceMappingColumn">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceMappingColumn" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InterfaceMappingColumnGridView ID="InterfaceMappingColumnGridView1" runat="server" />
</asp:Content>
