<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContainerTypeGridView.aspx.cs" Inherits="Administration_ContainerTypeGridView" Title="ContainerType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/ContainerTypeGridView.ascx" TagName="ContainerTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="ContainerType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ContainerType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ContainerType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:ContainerTypeGridView ID="ContainerTypeGridView1" runat="server" />
</asp:Content>
