<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PricingCategoryGridView.aspx.cs" Inherits="Administration_PricingCategoryGridView" Title="PricingCategory" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/PricingCategoryGridView.ascx" TagName="PricingCategoryGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="PricingCategory Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="PricingCategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PricingCategory" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:PricingCategoryGridView ID="PricingCategoryGridView1" runat="server" />
</asp:Content>
