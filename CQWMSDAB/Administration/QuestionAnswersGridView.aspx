<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QuestionAnswersGridView.aspx.cs" Inherits="Administration_QuestionAnswersGridView" Title="QuestionAnswers" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/QuestionAnswersGridView.ascx" TagName="QuestionAnswersGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="QuestionAnswers Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="QuestionAnswers">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="QuestionAnswers" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:QuestionAnswersGridView ID="QuestionAnswersGridView1" runat="server" />
</asp:Content>
