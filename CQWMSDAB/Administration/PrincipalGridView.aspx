<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrincipalGridView.aspx.cs" Inherits="Administration_PrincipalGridView" Title="Principal" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/PrincipalGridView.ascx" TagName="PrincipalGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="Principal Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Principal">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Principal" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:PrincipalGridView ID="PrincipalGridView1" runat="server" />
</asp:Content>
