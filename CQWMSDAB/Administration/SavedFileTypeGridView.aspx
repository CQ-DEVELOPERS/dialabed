<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SavedFileTypeGridView.aspx.cs" Inherits="Administration_SavedFileTypeGridView" Title="SavedFileType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/SavedFileTypeGridView.ascx" TagName="SavedFileTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="SavedFileType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:SavedFileTypeGridView ID="SavedFileTypeGridView1" runat="server" />
</asp:Content>
