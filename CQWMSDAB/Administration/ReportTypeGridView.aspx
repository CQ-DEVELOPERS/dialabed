<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportTypeGridView.aspx.cs" Inherits="Administration_ReportTypeGridView" Title="ReportType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/ReportTypeGridView.ascx" TagName="ReportTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="ReportType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ReportType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ReportType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:ReportTypeGridView ID="ReportTypeGridView1" runat="server" />
</asp:Content>
