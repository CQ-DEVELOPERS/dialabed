<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WarehouseGridView.aspx.cs" Inherits="Administration_WarehouseGridView" Title="Warehouse" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/WarehouseGridView.ascx" TagName="WarehouseGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="Warehouse Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Warehouse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Warehouse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:WarehouseGridView ID="WarehouseGridView1" runat="server" />
</asp:Content>
