<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InboundDocumentTypeGridView.aspx.cs" Inherits="Administration_InboundDocumentTypeGridView" Title="InboundDocumentType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InboundDocumentTypeGridView.ascx" TagName="InboundDocumentTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InboundDocumentType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InboundDocumentType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InboundDocumentType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InboundDocumentTypeGridView ID="InboundDocumentTypeGridView1" runat="server" />
</asp:Content>
