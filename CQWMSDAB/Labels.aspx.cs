using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Labels : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            NLWAX1.BaseURL = ConfigurationManager.AppSettings.Get("PrintService.BaseURL");
            NLWAX1.SessionName = DateTime.Now.Ticks.ToString();
            PopulateLabelTree(NLWAX1.LabelGroups);
            PopulateDriversTree(NLWAX1.Printers);
            lstPrinters.Attributes.Add("onchange", "PrinterSelected(window.document.form1.lstPrinters.options[selectedIndex].text, true)");
        }

        Session["Printer"] = NLWAX1.Printer;
        Session["Port"] = NLWAX1.Port;
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        NLWAX1.ClearError();
        NLWAX1.Print(int.Parse(txtQuant.Text), false);
        CheckError();
        btnReprint.Visible = true;
    }

    protected void btnReprint_Click(object sender, EventArgs e)
    {
        NLWAX1.ClearError();
        NLWAX1.Print(int.Parse(txtQuant.Text), true);
        CheckError();
    }

    protected void btnDirectPrint_Click(object sender, EventArgs e)
    {
        NLWAX1.ClearError();
        NLWAX1.DirectPrint(int.Parse(txtQuant.Text));
        CheckError();
    }

    protected void trvLabels_SelectedNodeChanged(object sender, EventArgs e)
    {
        string[] s = trvLabels.SelectedNode.Value.Split(Convert.ToChar("-"));
        if (s[0] == "Label")
        {
            string FolderPath;

            if (trvLabels.SelectedNode.Depth > 0)
                FolderPath = trvLabels.SelectedNode.Parent.ValuePath;
            else
                FolderPath = "";

            NLWAX1.LabelGroup = FolderPath;
            NLWAX1.Label = trvLabels.SelectedNode.Text;
            GetVariables();
            btnReprint.Visible = false;
            lblLabel.ForeColor = System.Drawing.Color.Black;
            lblLabel.Text = trvLabels.SelectedNode.Text;

            if (lstPrinters.SelectedItem == null)
            {
                btnPrint.Enabled = true;
                btnDirectPrint.Enabled = true;
            }
            BindData(-1);
            GetPreview();
        }
    }

    protected void btnExpand_Click(object sender, EventArgs e)
    {
        if (btnExpand.Text == "+")
        {
            btnExpand.Text = "-";
            grvVars.Columns[3].Visible = true;
            grvVars.Columns[4].Visible = true;
            grvVars.Columns[5].Visible = true;
            grvVars.Columns[2].Visible = true;
            btnExpand.ToolTip = "Hide Extended Properties";
            BindData(-1);
        }
        else
        {
            btnExpand.Text = "+";
            grvVars.Columns[3].Visible = false;
            grvVars.Columns[4].Visible = false;
            grvVars.Columns[5].Visible = false;
            grvVars.Columns[2].Visible = false;
            btnExpand.ToolTip = "Show Extended Properties";
        }
    }

    #region "Label Treeview Population Methods"
    private void PopulateLabelTree(string[] LabelGroups)
    {
        foreach (string objGroup in LabelGroups)
        {
            if (objGroup.GetType() == typeof(string))
            {
                string tmpGroup;
                tmpGroup = objGroup.ToString();
                AddLabelGroupNode(tmpGroup, "");
            }
            //Next
            AddLabelNodes("");
        }
    }

    private void AddLabelGroupNode(string Folder, string Parent)
    {
        if (Folder.IndexOf("/") > 0)
        {
            string ParentFolder = Folder.Substring(0, Folder.IndexOf("/"));
            string ChildFolder = Folder.Substring(Folder.IndexOf("/") + 1, Folder.Length - (Folder.IndexOf("/") + 1) );
            AddLabelGroupNode(ChildFolder, ParentFolder);
        }
        else
        {
            TreeNode tNode = new TreeNode();
            tNode.Text = Folder;
            tNode.Value = Folder;
            tNode.SelectAction = TreeNodeSelectAction.SelectExpand;
            if (Parent == "")
                trvLabels.Nodes.Add(tNode);
            else
                trvLabels.FindNode(Parent).ChildNodes.Add(tNode);

            AddLabelNodes(tNode.ValuePath);
            tNode = null;
        }
    }

    private void AddLabelNodes(string Folder)
    {
        foreach (object objLabel in NLWAX1.Labels(Folder, false))
        {
            string tmpLabel = objLabel.ToString();
            TreeNode tNode = new TreeNode();

            tNode.Text = tmpLabel;
            tNode.Value = "Label-" + tmpLabel;

            if (Folder == "")
                trvLabels.Nodes.Add(tNode);
            else
            {
                trvLabels.FindNode(Folder).ChildNodes.Add(tNode);
                trvLabels.FindNode(Folder).Collapse();
            }

            tNode = null;
            //Next
        }
    }

    private void PopulateDriversTree(string[] Printers)//ByVal Printers() As Object)
    {
        lstPrinters.Items.Clear();

        foreach (string objPrinter in Printers)
        {
            lstPrinters.Items.Add(objPrinter.ToString());
        }
        //Next
    }

    #endregion "Label Treeview Population Methods"

    #region "Variables Gridview Events"
    protected void grvVars_RowCancelingEdit(Object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
    {
        BindData(-1);
    }

    protected void grvVars_RowEditing(Object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
    {
        BindData(e.NewEditIndex);
    }

    protected void grvVars_RowUpdating(Object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
    {
        DataSet ds = NLWAX1.Variables;
        
        TextBox tb = (TextBox)grvVars.Rows[e.RowIndex].FindControl("txtEditVal");

        ds.Tables[0].Rows[e.RowIndex]["Value"] = tb.Text;

        ds.AcceptChanges();
        NLWAX1.Variables = ds;
        BindData(-1);
        GetPreview();
        btnReprint.Visible = false;
    }

    protected void btnSetVals_Click(object sender, EventArgs e)
    {
        GetVariables();
    }

    protected void GetVariables()
    {
        LabelVariables ds = new LabelVariables();

        NLWAX1.Variables = ds.GetLabelVariables(NLWAX1.Label);

        BindData(-1);
        GetPreview();
        btnReprint.Visible = false;
    }

    protected void SetVariables(string name, string value)
    {
        DataSet ds = NLWAX1.Variables;
        
        int index = -1;

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            index ++;
            if (ds.Tables[0].Rows[index]["Name"].ToString() == name)
            {
                ds.Tables[0].Rows[index]["Value"] = value;
                break;
            }
        }

        ds.AcceptChanges();
        NLWAX1.Variables = ds;
    }

    private void BindData(Int32 Index)
    {
        DataSet ds = new DataSet();
        ds = NLWAX1.Variables;

        lblVariables.Text = "Label Variables - No Variables Present";

        if (ds != null)
        {
            grvVars.EditIndex = Index;
            grvVars.DataSource = ds;
            grvVars.DataBind();

            if (ds.Tables != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        lblVariables.Text = "Label Variables";

            ds = null;
        }
    }

    #endregion "Variables Gridview Events"

    #region "NLWAX Handlers"
    private void GetPreview()
    {
        string url = "";
        try
        {
            NLWAX1.ClearError();
            url = NLWAX1.GetPreviewUrl( int.Parse(imgPreview.Width.Value.ToString()),
                                        int.Parse(imgPreview.Height.Value.ToString()),
                                        1,
                                        0);
            Trace.Warn("ImageUrl: " + url);
            string ErrorMsg = url;
            CheckError();
            if (NLWAX1.ErrorID != 0)
                imgPreview.AlternateText = NLWAX1.ErrorMessage;
            else
            {
                imgPreview.ImageUrl = url;
                imgPreview.AlternateText = NLWAX1.Label;
                lblLabel.Text = NLWAX1.Label;
            }
        }
        catch (Exception ex)
        {
            Trace.Warn("ImageUrl error: " + ex.Message + url);
            imgPreview.ImageUrl = "";
            imgPreview.AlternateText = "Failed generating image file for " + NLWAX1.Label;
            lblLabel.Text = "";
        }
    }

    public string GetFormatName(string val)
    {
        long vallong = long.Parse(val);
        return NLWAX1.GetVarFormatName(vallong).ToString();
    }

    private void CheckError()
    {
        if (NLWAX1.ErrorID == 0)
            lblError.Visible = false;
        else
        {
            lblError.Text = "Error: " + NLWAX1.ErrorMessage;
            lblError.Visible = true;
        }
    }
    #endregion "NLWAX Handlers"
}
