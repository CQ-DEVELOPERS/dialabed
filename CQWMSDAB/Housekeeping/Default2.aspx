<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Housekeeping_Default2" Title="Untitled Page" StylesheetTheme="Default" Theme="Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
            <asp:GridView ID="GridViewChange" runat="server" DataKeyNames="InstructionId,JobId" 
                DataSourceID="ObjectDataSourceInstruction" >
           </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server"
                TypeName="ManualPalletise"
                SelectMethod="GetMovementByInstructionType"
                UpdateMethod="UpdateMovements" 
                DeleteMethod="DeleteMovements">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="InstructionTypeId" Type="Int32" DefaultValue="0"/>
                </SelectParameters>
            </asp:ObjectDataSource>
</asp:Content>

