﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Housekeeping_BOMChecking : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LinkButtonBack.Visible = true;
        LinkButtonFinished.Visible = true;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                LinkButtonBack.Visible = false;
                LinkButtonFinished.Visible = false;
                TextBoxReferenceNumber.Text = "";
                TextBoxReferenceNumber.Focus();
                break;
            case 1:
                //if (gvwBOMItems.Rows.Count == 0)
                //{
                //    MultiView1.SetActiveView(View1);
                //}
                //else
                //{
                    txtBOMBarcode.Text = "";
                    txtBOMBarcode.Focus();
                //}
                break;
            case 2:
                txtItemBarcode.Text = "";
                txtItemBarcode.Focus();
                break;
            case 3:
                TextBoxQuantity.Text = "";
                TextBoxQuantity.Focus();
                break;
        }
    }

    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                MultiView1.SetActiveView(View2);
                break;
            case 1:
                foreach (GridViewRow row in gvwBOMList.Rows)
                {
                    if (row.Cells[1].Text == txtBOMBarcode.Text)
                    {
                        Session.Add("BOMInstructionId", gvwBOMList.DataKeys[row.DataItemIndex].Value);
                    }
                }

                MultiView1.SetActiveView(View3);
                break;
            case 2:
                foreach (GridViewRow row in gvwBOMItems.Rows)
                {
                    if (row.Cells[1].Text == txtItemBarcode.Text)
                    {
                        int StorageUnitId = Convert.ToInt32(gvwBOMItems.DataKeys[row.DataItemIndex].Value);
                        dsBOMItemDetails.SelectParameters["StorageUnitId"].DefaultValue = StorageUnitId.ToString();
                    }
                }

                MultiView1.SetActiveView(View4);
                break;
            case 3:
                ProductCheck pc = new ProductCheck();
                pc.ConfirmBOMQuantity(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["WarehouseId"]), Convert.ToInt32(Session["BOMInstructionId"])
                    , Convert.ToInt32(DetailsViewBatch.SelectedValue), Convert.ToDecimal(TextBoxQuantity.Text));

                gvwBOMItems.DataBind();
    
                MultiView1.SetActiveView(View3);
                break;
        }
    }
    protected void gvwBOMList_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session.Add("BOMInstructionId", Convert.ToInt32(((System.Web.UI.WebControls.GridView)(sender)).SelectedDataKey.Value));
        MultiView1.SetActiveView(View3);
    }

    protected void gvwBOMItems_SelectedIndexChanged(object sender, EventArgs e)
    {
        int StorageUnitId = Convert.ToInt32(((System.Web.UI.WebControls.GridView)(sender)).SelectedDataKey.Value);
        dsBOMItemDetails.SelectParameters["StorageUnitId"].DefaultValue = StorageUnitId.ToString();
        MultiView1.SetActiveView(View4);

    }
    protected void LinkButtonFinished_Click(object sender, EventArgs e)
    {
        int BOMInstructionId = -1;
        string Reference = "";

        BOMInstructionId = Convert.ToInt32(Session["BOMInstructionId"]);
        Reference = TextBoxReferenceNumber.Text;

        ProductCheck pc = new ProductCheck();
        string status = pc.ConfirmBOMFinished(Session["ConnectionStringName"].ToString(), BOMInstructionId, Convert.ToInt32(Session["OperatorId"]), Reference);

        if (status == "CK") { MultiView1.SetActiveView(View2); gvwBOMList.SelectedIndex = -1; gvwBOMList.DataBind(); }
        if (status == "CD") { MultiView1.SetActiveView(View1); }        
    }

    protected void ButtonQuit_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
    }
    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex--;
    }

    protected void gvwBOMItems_DataBound(object sender, EventArgs e)
    {
        if (gvwBOMItems.Rows.Count == 0)
        {
            MultiView1.ActiveViewIndex--;
        }
    }
    protected void gvwBOMList_DataBound(object sender, EventArgs e)
    {
        if (gvwBOMList.Rows.Count == 0)
        {
            MultiView1.SetActiveView(View1);
        }
    }

    protected void gvwBOMList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.Cells.Count > 7 && e.Row.Cells[7].HasControls())
        {
            if (((HiddenField)e.Row.Cells[7].FindControl("hdnChecked")).Value == "True")
            {
                e.Row.BackColor = System.Drawing.Color.GreenYellow;
            }
        }
    }
}
