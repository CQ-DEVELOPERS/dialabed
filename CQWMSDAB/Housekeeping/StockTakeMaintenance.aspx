<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockTakeMaintenance.aspx.cs" Inherits="Housekeeping_StockTakeMaintenance"
    Title="<%$ Resources:Default, StockTakeMaintenanceTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/StorageUnitBatchIdSearch.ascx" TagName="StorageUnitBatchIdSearch"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockTakeMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="LabelJobId" runat="server" Text="<%$ Resources:Default,JobId %>"></asp:Label>
                        <asp:TextBox ID="TextBoxJobId" runat="server"></asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderJobId" runat="server"
                            TargetControlID="TextBoxJobId" WatermarkText="<%$ Resources:Default, All%>">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                        <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                            OnClick="ButtonSearch_Click" />
                        <br />
                        <asp:Button ID="ButtonSelect1" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click" />
                        <asp:Button ID="ButtonRelease" runat="server" Text="<%$ Resources:Default, Release%>"
                            OnClick="ButtonRelease_Click" />
                        <asp:Button ID="ButtonCancel" runat="server" Text="<%$ Resources:Default, Cancel%>"
                            OnClick="ButtonCancel_Click" />
                        <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonCancel" runat="server" TargetControlID="ButtonCancel"
                            ConfirmText="<%$ Resources:Default, PressOKtocanceljobs%>">
                        </ajaxToolkit:ConfirmButtonExtender>
                        <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>"
                            OnClick="ButtonPrint_Click" />
                        <br />
                        <asp:Label ID="LabelOperator" runat="server" Text="<%$ Resources:Default,Operator %>"></asp:Label>
                        <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                            DataTextField="Operator" DataValueField="OperatorId">
                        </asp:DropDownList>
                        <asp:Button ID="ButtonOperatorRelease" runat="server" Text="<%$ Resources:Default, OperatorRelease%>"
                            OnClick="ButtonOperatorRelease_Click" Style="width: auto;" />
                        <asp:GridView ID="GridViewSearch" runat="server" DataSourceID="ObjectDataSourceSearch"
                            DataKeyNames="JobId" AllowPaging="true" AutoGenerateColumns="false" AutoGenerateSelectButton="true"
                            AutoGenerateEditButton="true" OnSelectedIndexChanged="GridViewSearch_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="ReferenceNumber" HeaderText='<%$ Resources:Default,ReferenceNumber %>' />
                                <asp:BoundField ReadOnly="true" DataField="Area" HeaderText='<%$ Resources:Default,Area %>' />
                                <asp:BoundField ReadOnly="true" DataField="CreateDate" HeaderText='<%$ Resources:Default,CreateDate %>' />
                                <asp:BoundField ReadOnly="true" DataField="Status" HeaderText='<%$ Resources:Default,Status %>' />
                                <asp:BoundField ReadOnly="true" DataField="Count" HeaderText='<%$ Resources:Default,Count %>' />
                                <asp:BoundField ReadOnly="true" DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>' />
                                <asp:BoundField ReadOnly="true" DataField="Operator" HeaderText='<%$ Resources:Default,Operator %>' />
                                <asp:BoundField Visible="false" ReadOnly="true" DataField="PriorityId" HeaderText="PriorityId" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelPriority" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceSearch" runat="server" TypeName="StockTake"
                            SelectMethod="SearchDetails" UpdateMethod="UpdateStockTakeJob">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="warehouseId" Type="int32" />
                                <asp:ControlParameter Name="jobId" ControlID="TextBoxJobId" Type="int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="int32" />
                                <asp:Parameter Name="JobId" Type="int32" />
                                <asp:Parameter Name="PriorityId" Type="int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" TypeName="Priority"
                            SelectMethod="GetPriorities">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect1" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonRelease" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonCancel" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonOperatorRelease" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="<%$ Resources:Default, View%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewView" runat="server" DataSourceID="ObjectDataSourceView"
                            DataKeyNames="InstructionId" AllowPaging="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="InstructionId" HeaderText='<%$ Resources:Default,InstructionId %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="StorageUnitBatchId" HeaderText="StorageUnitBatchId" Visible="false">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="LineNumber" HeaderText='<%$ Resources:Default,LineNumber %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Location" HeaderText='<%$ Resources:Default,Location %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" HeaderText='<%$ Resources:Default,SKUCode %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" HeaderText='<%$ Resources:Default,Batch %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Principal" HeaderText='<%$ Resources:Default,Principal %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExpectedQuantity" HeaderText='<%$ Resources:Default,ExpectedQuantity %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceView" runat="server" TypeName="StockTake"
                            SelectMethod="ViewLineDetails">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Update%>">
            <ContentTemplate>
                <asp:Button ID="ButtonSelect2" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                    OnClick="ButtonSelect_Click" />
                <asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, Edit%>" OnClick="ButtonEdit_Click" />
                <asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, Save%>" OnClick="ButtonSave_Click" />
                <asp:UpdatePanel ID="UpdatePanelGridViewUpdate" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewUpdate" runat="server" DataSourceID="ObjectDataSourceUpdate"
                            DataKeyNames="InstructionId" AllowPaging="true" AutoGenerateColumns="false" AutoGenerateEditButton="true" OnRowEditing="GridViewUpdate_OnRowEditing" OnRowUpdated="GridViewUpdate_Updated">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="LineNumber" HeaderText='<%$ Resources:Default,LineNumber %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Location" HeaderText='<%$ Resources:Default,Location %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Product" HeaderText='<%$ Resources:Default,Product %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKUCode" HeaderText='<%$ Resources:Default,SKUCode %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Batch" HeaderText='<%$ Resources:Default,Batch %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="LatestCount" HeaderText='<%$ Resources:Default,LatestCount %>'>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Operator %>">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                                            DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelOperator" runat="server" Text='<%# Bind("Operator") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceUpdate" runat="server" TypeName="StockTake"
                            SelectMethod="ViewUpdateDetails" UpdateMethod="UpdateStockTakeCount">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="InstructionId" Type="int32" />
                                <asp:Parameter Name="OperatorId" Type="int32" />
                                <asp:Parameter Name="LatestCount" Type="int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect2" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel4" HeaderText="<%$ Resources:Default, InsertProduct%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelStorageUnitBatchIdSearch" runat="server">
                    <ContentTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanelLinesReceived">
                            <ContentTemplate>
                                <asp:Panel ID="PanelProductInsert" runat="server">
                                    <asp:DetailsView ID="DetailsProductInsert" runat="server" DataKeyNames="LocationId"
                                        DataSourceID="ObjectDataSourceProductInsert" AutoGenerateRows="False">
                                        <Fields>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>" HeaderStyle-Font-Bold="true"
                                                HeaderStyle-Font-Size="X-Large">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LabelLocationEdit" runat="server" Text='<%# Eval("Location") %>' Font-Bold="true"
                                                        Font-Size="X-Large" EnableTheming="false"></asp:Label>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:Label ID="LabelLocationInsert" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelLocationItem" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceProductInsert" runat="server" TypeName="StockTake"
                                        SelectMethod="ProductInsertSelect">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="instructionId" SessionField="InstructionId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default,ProductCode %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default,Product %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default,SKUCode %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelSKU" runat="server" Text="<%$ Resources:Default,SKU %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxSKU" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default,Batch %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelECLNumber" runat="server" Text="<%$ Resources:Default,ECLNumber %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxECLNumber" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td align="right">
                                    <asp:Button ID="ButtonSearchProduct" runat="server" Text="<%$ Resources:Default,Search %>"
                                        OnClick="ButtonSearchProduct_Click" />
                                    <asp:Button ID="ButtonInsert" runat="server" Text="<%$ Resources:Default, Insert%>"
                                        OnClick="ButtonInsert_Click" />
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="GridViewProductSearch" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            AutoGenerateSelectButton="True" DataKeyNames="StorageUnitBatchId,Product" OnSelectedIndexChanged="GridViewProductSearch_OnSelectedIndexChanged"
                            OnPageIndexChanging="GridViewProductSearch_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>" />
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>" />
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>" />
                                <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default,SKU %>" />
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default,Batch %>" />
                                <asp:BoundField DataField="ECLNumber" HeaderText="<%$ Resources:Default,ECLNumber %>" />
                                <asp:BoundField DataField="Principal" HeaderText="<%$ Resources:Default,Principal %>" />
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
