using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_MovementMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            //Sets the menu for Housekeeping
            Session["MenuId"] = 5;

            string instructionTypeId = Request.QueryString["InstructionTypeId"];

            if (instructionTypeId != "" && instructionTypeId != null)
                DropDownListInstructionType.SelectedIndex = int.Parse(instructionTypeId);

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "Page_Load"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {

            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(GridViewInstruction.DataKeys[checkedRow.RowIndex].Values["InstructionId"]);
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";
        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Housekeeping/MovementMaintenance.aspx?InstructionTypeId=" + DropDownListInstructionType.SelectedIndex.ToString();
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonManualLocations_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonEdit_Click
    // Sets the GridView into edit mode for selected row	
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonEdit_Click"

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";
        try
        {
            if (GridViewInstruction.EditIndex != -1)
                GridViewInstruction.UpdateRow(GridViewInstruction.EditIndex, true);

            SetEditIndexRowNumber();

            Master.MsgText = "Save"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSave_Click"

    #region ButtonDelete_Click
    protected void ButtonDelete_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDelete_Click";
        try
        {
            if (Session["OperatorGroupCode"] != null)
                if (Session["OperatorGroupCode"].ToString() != "A" && Session["OperatorGroupCode"].ToString() != "S")
                {
                    ShowAlertMessage(Resources.Default.InsufficientPrivileges);
                    return;
                }

            int index = 0;
            CheckBox cb = new CheckBox();

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    GridViewInstruction.DeleteRow(index);

                index++;
            }

            Master.MsgText = "Delete"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDelete_Click"

    #region ShowAlertMessage
    public static void ShowAlertMessage(string error)
    {
        Page page = HttpContext.Current.Handler as Page;

        if (page != null)
        {
            error = error.Replace("'", "\'");

            ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "alert('" + error + "');", true);
        }
    }
    #endregion

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";
        try
        {
            if (Session["rowList"] == null)
            {
                int index = 0;
                CheckBox cb = new CheckBox();
                ArrayList rowList = new ArrayList();

                while (index < GridViewInstruction.Rows.Count)
                {
                    GridViewRow checkedRow = GridViewInstruction.Rows[index];
                    cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(index);

                    index++;
                }
                if (rowList.Count > 0)
                    Session["rowList"] = rowList;
            }
            Master.MsgText = "Edit Row"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";
        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];

            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                //if (GridViewInstruction.EditIndex != -1)
                //{
                //    GridViewInstruction.UpdateRow(GridViewInstruction.EditIndex, true);
                //}


                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                    GridViewInstruction.EditIndex = -1;
                }
                else
                {
                    GridViewInstruction.SelectedIndex = (int)rowList[0];
                    GridViewInstruction.EditIndex = (int)rowList[0];
                    //this.GridViewInstruction.Rows[GridViewInstruction.EditIndex].Cells[2].Enabled = false;

                    rowList.Remove(rowList[0]);
                }
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region GridViewInstruction_OnRowUpdating
    protected void GridViewInstruction_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        theErrMethod = "GridViewInstruction_OnRowUpdating";
        try
        {
            //GridViewRow row = this.GridViewReceiptLine.Rows[e.RowIndex];

            //if (row != null)
            //{
            //    DropDownList drp = (DropDownList)row.FindControl("DropDownListBatch");
            //    Session["drp"] = drp.Text;
            //}

            //foreach (DictionaryEntry entry in e.NewValues)
            //{
            //    e.NewValues[entry.Key] =
            //        Server.HtmlEncode(entry.Value.ToString());
            //}

            Master.MsgText = "Row Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewInstruction_OnRowUpdating"

    #region ObjectDataSourceInstruction_OnUpdating
    protected void ObjectDataSourceInstruction_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        theErrMethod = "ObjectDataSourceInstruction_OnUpdating";

        try
        {
            Master.MsgText = "Instruction Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceInstruction_OnUpdating"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewInstruction.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSearch_Click"

    #region ButtonCreateMovement_Click
    protected void ButtonCreateMovement_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCreateMovement_Click";
        try
        {
            int instructionTypeId = int.Parse(DropDownListInstructionType.SelectedValue);

            InstructionType it = new InstructionType();

            string instructionTypeCode = it.GetInstructionTypeCode(Session["ConnectionStringName"].ToString(), instructionTypeId);

            Response.Redirect("Movement.aspx?Type=" + instructionTypeCode);

        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonCreateMovement_Click"
    
    #region ButtonElectronic_Click
    protected void ButtonElectronic_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonElectronic_Click";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            int instructionId = 0;
            Status st = new Status();

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    instructionId = int.Parse(GridViewInstruction.DataKeys[index].Values["InstructionId"].ToString());
                    if (st.Release(Session["ConnectionStringName"].ToString(), instructionId, (int)Session["OperatorId"], "RL"))
                    {
                        Master.MsgText = "Released";
                        Master.ErrorText = "";
                    }
                    else
                    {
                        Master.MsgText = "";
                        Master.ErrorText = "Failed, please try again";
                    }
                }

                index++;
            }

            GridViewInstruction.DataBind();

            Master.MsgText = "Instruction"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonElectronic_Click"

    #region ButtonCheckSheet_Click
    protected void ButtonCheckSheet_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCheckSheet_Click";
        try
        {

            Master.MsgText = "Check Sheet"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }

    #endregion "ButtonCheckSheet_Click"

    #region ButtonLabel_Click
    protected void ButtonLabel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLabel_Click";
        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                if (cb.Checked)
                {
                    Session["Barcode"] = row.Cells[12].Text;
                    break;
                }
            }

            Session["Title"] = "Reference Number";
            Session["LabelName"] = "Multi Purpose Label.lbl";
            Session["FromURL"] = "~/Housekeeping/MovementMaintenance.aspx?InstructionTypeId=" + DropDownListInstructionType.SelectedIndex.ToString();

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
    #endregion "ButtonLabel_Click"

    #region ButtonSerialNumbers_Click
    protected void ButtonSerialNumbers_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSerialNumbers_Click";
        try
        {

            Master.MsgText = "Serial Numbers"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonSerialNumbers_Click"

    #region ButtonReset_Click
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            Status status = new Status();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    if (!status.ResetStatus(Session["ConnectionStringName"].ToString(), int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["InstructionId"].ToString())))
                    {
                        Master.ErrorText = "Please try again.";
                        break;
                    }
            }

            GridViewInstruction.DataBind();

            Master.MsgText = "Reset";
        }
        catch { }
    }
    #endregion "ButtonReset_Click"

    #region ButtonPauseJob_Click
    protected void ButtonPauseJob_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonPauseJob_Click";

            Master.MsgText = "";
            Master.ErrorText = "";

            Status status = new Status();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    if (!status.ReleaseJob(Session["ConnectionStringName"].ToString(),
                                      int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["JobId"].ToString()),
                                      int.Parse(Session["OperatorId"].ToString()),
                                      "PS"))
                    {
                        Master.ErrorText = "Please try again.";
                        break;
                    }
            }

            GridViewInstruction.DataBind();

            Master.MsgText = "Job Paused";
        }
        catch { }
    }
    #endregion "ButtonPauseJob_Click"
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_MovementMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_MovementMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
