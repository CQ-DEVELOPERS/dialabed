using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_MoveToLocation : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                //Sets the menu for Housekeeping
                Session["MenuId"] = 5;
            }
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Movement " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"


    #region ButtonMoveTo_Click
    protected void ButtonMoveTo_Click(object sender, EventArgs e)
    {
        if (int.Parse(Session["ParameterLocationId"].ToString()) > 0)
        {
            ManualPalletise ds = new ManualPalletise();

            ArrayList loc = new ArrayList();
            ArrayList subl = new ArrayList();

            loc = (ArrayList)Session["PassLoc"];
            subl = (ArrayList)Session["PassSubl"];

            int LocationId = -1;
            int StorageUnitBatchid = -1;

            int index = 0;

            while (index < loc.Count)
            {
                LocationId = int.Parse(loc[index].ToString());
                StorageUnitBatchid = int.Parse(subl[index].ToString());

                if (ds.CreateMovementLocation(Session["ConnectionStringName"].ToString(), "M",
                                       StorageUnitBatchid,
                                       LocationId,
                                       int.Parse(Session["ParameterLocationId"].ToString()),
                                       -1) == true)
                {
                };

                index++;
            }
        }
        else
        { Master.MsgText = "Please Select Destination Location"; }
        
        Response.Redirect(Session["ReturnURL"].ToString());


    }
    #endregion ButtonMoveTo_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_Movement", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_Movement", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
