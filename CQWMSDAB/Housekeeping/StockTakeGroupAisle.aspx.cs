﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;


public partial class Housekeeping_StockTakeGroupAisle : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                //Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewStockTake_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewStockTakeGroupLoc_SelectedIndexChanged";
        try
        {
            Session["StockTakeGroupLocId"] = GridViewStockTake.SelectedDataKey["StockTakeGroupLocId"];

            //Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeGroupMaintenance" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
    }

    //protected void ButtonAddStockTakeGroupLoc_Click(object sender, EventArgs e)
    //{
    //    DetailsViewInsertStockTakeGroupLoc.Visible = true;
    //    GridViewStockTake.Visible = false;
    //}

    protected void ObjectDataSourceStockTakeGroupLoc_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertStockTakeGroupLoc.Visible = false;
        GridViewStockTake.Visible = true;
        //Session["StockTakeReferenceId"] = int.Parse(e.ReturnValue.ToString());
        //      PanelLocation.Visible = true;
        //LabelError.Text = e.ReturnValue.ToString();
        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_StockTake, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceStockTakeGroupLoc_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string StockTakeGroupId = ((TextBox)DetailsViewInsertStockTakeGroupLoc.FindControl("DetailTextStockTakeGroupId")).Text;
        string LocationId = ((TextBox)DetailsViewInsertStockTakeGroupLoc.FindControl("DetailTextLocationId")).Text;

        e.InputParameters["stockTakeGroupId"] = StockTakeGroupId;
        e.InputParameters["locationId"] = LocationId;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        //ObjectDataSourceArea.Insert();

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertStockTakeGroupLoc.Visible = false;
        GridViewStockTake.Visible = true;
    }

    protected void DetailsViewInsertStockTakeGroupLoc_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //((TextBox)DetailsViewInsertStockTakeGroupLoc.FindControl("DetailTextStockTakeGroupId")).Text = Session["StockTakeGroupId"].ToString();
        }
    }
    //#region ButtonSelectLocation_Click
    //protected void ButtonSelectLocation_Click(object sender, EventArgs e)
    //{
    //    theErrMethod = "ButtonSelectLocation_Click";
    //    try
    //    {
    //        //ArrayList areaList = new ArrayList();

    //        foreach (ListItem item in CheckBoxListLocation.Items)
    //        {
    //            item.Selected = true;
    //            //areaList.Add(item.Value);
    //        }

    //        //Session["LocationList"] = areaList;

    //        Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }

    //}
    //#endregion ButtonSelectLocation_Click

    //#region ButtonDeselectLocation_Click
    //protected void ButtonDeselectLocation_Click(object sender, EventArgs e)
    //{
    //    theErrMethod = "ButtonDeselectLocation_Click";

    //    try
    //    {
    //        foreach (ListItem item in CheckBoxListLocation.Items)
    //        {
    //            item.Selected = false;
    //        }

    //        //Session["LocationList"] = null;
    //        Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }

    //}
    //#endregion ButtonDeselectLocation_Click
    //protected void ButtonNext_Click(object sender, EventArgs e)
    //{
    //    if (GridViewStockTake.SelectedIndex >= 0)
    //    {
    //        Session.Add("stockTakeGroupLocId", GridViewStockTake.SelectedDataKey.Value);
    //        Response.Redirect("~/Housekeeping/StockTakeGroupLocation.aspx");
    //        LabelError.Text = "";
    //    }
    //    else
    //    {
    //        LabelError.Text = "Please select a stock take group before proceeding";
    //    }
    //}
    //protected void ButtonSkip_Click(object sender, EventArgs e)
    //{


    //    //Session.Add("stockTakeReferenceId", 0);
    //    //Response.Redirect("~/Housekeeping/StockTakeCreate.aspx");
    //    //LabelError.Text = "";

    //}
    #region ButtonLinkLocation_Click

    protected void ButtonLinkLocation_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLinkLocation_Click";

        try
        {
            Session["fromAisle"] = DropDownListFromAisle.SelectedValue;
            Session["toAisle"] = DropDownListToAisle.SelectedValue;
            Session["fromLevel"] = DropDownListFromLevel.SelectedValue;
            Session["toLevel"] = DropDownListToLevel.SelectedValue;
            Response.Redirect("StockTakeGroupLocation.aspx");

            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateAisle" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonLinkLocation_Click
}
