<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockTakeUpload.aspx.cs" Inherits="Housekeeping_StockTakeUpload" Title="<%$ Resources:Default, StockTakeAuthoriseTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <%--<asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeUploadTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockTakeUploadAgenda %>"></asp:Label>
    <br />--%>
    <asp:Label ID="Label3" runat="server" SkinID="PageTitle" Text="Stock Take UpLoad"></asp:Label>
    <br />
    <asp:Label ID="Label4" runat="server" SkinID="AgendaTitle" Text="Agenda"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelWarehouseCode" runat="server" Text="<%$ Resources:Default, WarehouseCode%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxWarehouseCode" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default, SKUCode%>"></asp:Label>
            </td>
            <td style="width: 126px">
                <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
            </td>
          
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                    DataTextField="Principal" DataValueField="PrincipalId">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                    SelectMethod="GetPrincipalParameter">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
          <td>
                <asp:Label ID="LabelCategory" runat="server" Text="Category"></asp:Label>
            </td>
            <td>
              <br />
                <asp:TextBox ID="TextBoxCategory" runat="server" ></asp:TextBox>
              <br />
              <asp:Label ID="LabelExactMatch" runat="server" Text="Exact Match"></asp:Label>
            <asp:CheckBox ID="CheckBoxCategory" runat="server" />
            </td>
            <td>
                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Default,ZeroVariance %>'></asp:Label>
            </td>
            <td>
                <asp:RadioButtonList ID="rblZeroVariance" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <asp:ListItem Selected="True" Text='<%$ Resources:Default,Show %>' Value="true"></asp:ListItem>
                    <asp:ListItem Text='<%$ Resources:Default,Hide %>' Value="false"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Default,SentItems %>'></asp:Label>
            </td>
            <td>
                <asp:RadioButtonList ID="rblSent" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <asp:ListItem Selected="True" Text='<%$ Resources:Default,Show %>' Value="true"></asp:ListItem>
                    <asp:ListItem Text='<%$ Resources:Default,Hide %>' Value="false"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:Label ID="lblReasonCode" runat="server" Text="Reason Code"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlReasonCode" runat="server" DataSourceID="ObjectDataSourceReasonCode"
                    DataTextField="Reason" DataValueField="ReasonCode">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceReasonCode" runat="server" TypeName="StockTake"
                    SelectMethod="GetReasonCodes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
    <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,ButtonSearch %>' OnClick="ButtonSearch_Click" />
    <asp:Button ID="ButtonSelect" runat="server" Text='<%$ Resources:Default,ButtonSelectAll %>' OnClick="ButtonSelect_Click" />
    <asp:Button ID="ButtonReject" runat="server" Text='<%$ Resources:Default,ButtonReject %>' OnClick="ButtonReject_Click" />
    <asp:Button ID="ButtonAccept" runat="server" Text='<%$ Resources:Default,ButtonAccept %>' OnClick="ButtonAccept_Click" />
    <asp:Button ID="ButtonAcceptAll" runat="server" Text='<%$ Resources:Default,ButtonAcceptAll %>' OnClick="ButtonAcceptAll_Click" />
    <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonAcceptAll" runat="server" TargetControlID="ButtonAcceptAll" ConfirmText="<%$ Resources:Default, PressOKacceptWMSquantities%>" />
    <asp:Button ID="ButtonReload" runat="server" Text='<%$ Resources:Default, Reload%>' OnClick="ButtomReload_Click" />
    <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonReload" runat="server" TargetControlID="ButtonReload" ConfirmText="<%$ Resources:Default, PressOKloadWMSquantities%>" />
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
    <asp:Button ID="ButtonCreateStockTake" runat="server" Text="<%$ Resources:Default, ButtonCreateStockTake %>" OnClick="ButtonCreateStockTake_Click" />
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewAuthorise" runat="server" DataSourceID="ObjectDataSourceAuthorise" OnInit="GridViewAuthoriseInit" DataKeyNames="ComparisonDate,ProductCode,StorageUnitId,BatchId,WarehouseCode" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="30" EnableModelValidation="True">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Send%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server" Enabled='<%# Bind("Enabled") %>'></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ComparisonDate" HeaderText='<%$ Resources:Default,ComparisonDate %>' SortExpression="ComparisonDate"><ItemStyle Wrap="false" /></asp:BoundField>
                    <asp:BoundField DataField="HostDate" HeaderText='<%$ Resources:Default, HostDate%>' SortExpression="HostDate"><ItemStyle Wrap="false" /></asp:BoundField>
                    <asp:BoundField DataField="WarehouseCode" HeaderText='<%$ Resources:Default,WarehouseCode %>' SortExpression="WarehouseCode" />
                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' SortExpression="ProductCode" />
                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' SortExpression="Product"><ItemStyle Wrap="false" /></asp:BoundField>
                    <asp:BoundField DataField="SKUCode" HeaderText='<%$ Resources:Default,SKUCode %>' SortExpression="SKUCode" />
                    <asp:BoundField DataField="Batch" HeaderText='<%$ Resources:Default,Batch %>' SortExpression="Batch" />
                    <asp:BoundField DataField="Principal" HeaderText='<%$ Resources:Default,Principal %>' SortExpression="Principal" />
                    <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
                    <asp:BoundField DataField="WMSQuantity" HeaderText='<%$ Resources:Default,WMSQuantity %>' SortExpression="WMSQuantity" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="HostQuantity" HeaderText='<%$ Resources:Default,HostQuantity %>' SortExpression="HostQuantity" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="Variance" HeaderText='<%$ Resources:Default,Variance %>' SortExpression="Variance" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="StdCostPrice" HeaderText='<%$ Resources:Default,StdCostPrice %>' SortExpression="StdCostPrice" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="WMSPrice" HeaderText='<%$ Resources:Default,WMSPrice %>' SortExpression="WMSPrice" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="HostPrice" HeaderText='<%$ Resources:Default,HostPrice %>' SortExpression="HostPrice" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="PriceVariance" HeaderText='<%$ Resources:Default,PriceVariance %>' SortExpression="PriceVariance" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="CustomField1" Visible="true" HeaderText="CustomField1"  SortExpression="CustomField1" />
                    
                    <asp:BoundField DataField="SentDate" HeaderText='<%$ Resources:Default, DateSent%>' SortExpression="SentDate"><ItemStyle Wrap="false" /></asp:BoundField>
                    <asp:BoundField DataField="UploadStatus" HeaderText='<%$ Resources:Default, UploadStatus%>' SortExpression="UploadStatus" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceAuthorise" runat="server" TypeName="StockTake"  SelectMethod="ViewUploadDetails">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
                    <asp:ControlParameter Name="warehouseCode" ControlID="TextBoxWarehouseCode" Type="String" />
                    <asp:ControlParameter Name="productCode" ControlID="TextBoxProductCode" Type="String" />
                    <asp:ControlParameter Name="product" ControlID="TextBoxProduct" Type="String" />
                    <asp:ControlParameter Name="batch" ControlID="TextBoxBatch" Type="String" />
                    <asp:ControlParameter Name="skuCode" ControlID="TextBoxSKUCode" Type="String" />
                    <asp:ControlParameter Name="ShowZeroVariance" ControlID="rblZeroVariance" Type="Boolean" />
                    <asp:ControlParameter Name="ShowSent" ControlID="rblSent" Type="Boolean" />
                    <asp:ControlParameter Name="PrincipalId" ControlID="DropDownListPrincipal" Type="String" PropertyName="SelectedValue" />
                    <asp:ControlParameter Name="category" ControlID="TextBoxCategory" Type="String" />
                    <asp:ControlParameter Name="ExactMatch" ControlID="CheckBoxCategory" Type="Boolean" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="InstructionId" Type="int32" />
                    <asp:Parameter Name="Status" Type="string" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonReject" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonAccept" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonAcceptAll" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonReload" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

