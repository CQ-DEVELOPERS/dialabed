<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductMaintenance.aspx.cs" Inherits="Housekeeping_ProductMaintenance"
    Title="<%$ Resources:Default, MovementTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/UserControls/Housekeeping/ProductDescription.ascx" TagName="ProductDescription" TagPrefix="CQWMS" %>
<%@ Register Src="~/UserControls/Housekeeping/ProductSKU.ascx" TagName="ProductSKU" TagPrefix="CQWMS" %>
<%@ Register Src="~/UserControls/Housekeeping/ProductPacks.ascx" TagName="ProductPacks" TagPrefix="CQWMS" %>
<%@ Register Src="~/UserControls/Housekeeping/ProductBatch.ascx" TagName="ProductBatch" TagPrefix="CQWMS" %>
<%@ Register Src="~/UserControls/Housekeeping/ProductLocation.ascx" TagName="ProductLocation" TagPrefix="CQWMS" %>
<%@ Register Src="~/UserControls/Housekeeping/ProductArea.ascx" TagName="ProductArea" TagPrefix="CQWMS" %>
<%@ Register Src="~/UserControls/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="CQWMS" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ProductMaintenance %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <style type="text/css">
        .MyGridClass .rgDataDiv {
            height: auto !important;
        }
    </style>

    <telerik:RadAjaxManager ID="RadAjaxManagerMaster" runat="server" EnableAJAX="false">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ProductDescriptionUC">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ProductDescriptionUC" />
                    <telerik:AjaxUpdatedControl ControlID="ProductSKUUC" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ProductSKUUC">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ProductSKUUC" />
                    <telerik:AjaxUpdatedControl ControlID="ProductPacksUC" />
                    <telerik:AjaxUpdatedControl ControlID="ProductBatchUC" />
                    <telerik:AjaxUpdatedControl ControlID="ProductAreaUC" />
                    <telerik:AjaxUpdatedControl ControlID="ProductLocationUC" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ProductPacksUC">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ProductPacksUC" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ProductBatchUC">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ProductBatchUC" />
                    <telerik:AjaxUpdatedControl ControlID="ProductSKUUC" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ProductAreaUC">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ProductAreaUC" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ProductLocationUC">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ProductLocationUC" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            MultiPageID="Tabs" Skin="Outlook" SelectedIndex="0">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Products%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, SKUCode %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Packs %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Batch %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Area %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Location %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                <div style="margin-top:186px; float:right;">
                    <CQWMS:ExportButtons runat="server" ID="ExportProducts" OnExportGrid="Export_ExportGrid"/>
                </div>
                <CQWMS:ProductDescription runat="server" ID="ProductDescriptionUC" />
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="TabPanel2">
                <div style="margin-top:96px; float:right;">
                    <CQWMS:ExportButtons runat="server" ID="ExportSKUS" OnExportGrid="Export_ExportGrid"/>
                </div>
                <CQWMS:ProductSKU runat="server" ID="ProductSKUUC" />
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="TabPanel3">
                <div style="margin-top:46px; margin-left:700px; float:left;">
                    <CQWMS:ExportButtons runat="server" ID="ExportPacks" OnExportGrid="Export_ExportGrid"/>
                </div>
                <CQWMS:ProductPacks runat="server" ID="ProductPacksUC" />
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="TabPanel4">
                <div style="margin-top:466px; margin-left:310px; float:left; position:absolute;">
                    <CQWMS:ExportButtons runat="server" ID="ExportBatches" OnExportGrid="Export_ExportGrid"/>
                </div>
                <CQWMS:ProductBatch runat="server" ID="ProductBatchUC" />
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="TabPanel5">
                <div style="margin-top:186px; margin-left:400px; float:left; position:absolute;">
                    <CQWMS:ExportButtons runat="server" ID="ExportAreas" OnExportGrid="Export_ExportGrid"/>
                </div>
                <CQWMS:ProductArea runat="server" ID="ProductAreaUC" />
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="TabPanel6">
                <div style="margin-top:440px; margin-left:305px; float:left; position:absolute;">
                    <CQWMS:ExportButtons runat="server" ID="ExportLocations" OnExportGrid="Export_ExportGrid"/>
                </div>
                <CQWMS:ProductLocation runat="server" ID="ProductLocationUC" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
