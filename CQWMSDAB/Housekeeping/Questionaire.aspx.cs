using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_Questionaire : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
    }
    protected void ButtonAddQuestionaire_Click(object sender, EventArgs e)
    {

        Questionaire q = new Questionaire();
            q.InsertQuestionaire(Session["ConnectionStringName"].ToString(),
                             (int)Session["WarehouseId"], 
                             TextBoxQuestionaireType.Text.ToString(), 
                             TextBoxDescription.Text.ToString());
      
        
    }
    protected void ButtonAddQuestion_Click(object sender, EventArgs e)
    {

        Questionaire q = new Questionaire();
        q.InsertQuestion(Session["ConnectionStringName"].ToString(),
                            int.Parse(Session["QuestionaireId"].ToString()),
                            TextBoxCategory.Text.ToString(),
                            TextBoxCode.Text.ToString(),
                            int.Parse(TextBoxSequence.Text.ToString()),
                            1,
                            TextBoxQuestionText.Text.ToString(),
                            TextBoxType.Text.ToString());
    }

    protected void GridViewQuestionaire_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["QuestionaireId"] = GridViewQuestionaire.SelectedDataKey.Value;

        GridviewQuestions.DataBind();
        //ObjectDataSourceQuestions.DataBind();

    }

}
