<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockTakeCreate.aspx.cs" Inherits="Housekeeping_StockTakeCreate" Title="<%$ Resources:Default, StockTakeCreateTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Wizard ID="Wizard1" runat="server" Width="100%" Height="300px" OnNextButtonClick="Wizard1_NextButtonClick">
                            <WizardSteps>
                                <asp:WizardStep ID="WizardStep1" runat="server" Title="Select Type of Stock Take" StepType="Start">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                        <asp:ListItem Text="Simple" Value="StockTakeCreateSimple.aspx" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Location" Value="StockTakeCreateLocation.aspx"></asp:ListItem>
                                        <%--<asp:ListItem Text="Group" Value="StockTakeCreateGroup.aspx"></asp:ListItem>--%>
                                        <asp:ListItem Text="Category" Value="StockTakeCreateCategory.aspx"></asp:ListItem>
                                        <asp:ListItem Text="Product" Value="StockTakeCreateProduct.aspx"></asp:ListItem>
                                        <asp:ListItem Text="Batch" Value="StockTakeCreateBatch.aspx"></asp:ListItem>
                                        <asp:ListItem Text="Area" Value="StockTakeCreateArea.aspx"></asp:ListItem>
                                        <asp:ListItem Text="Aisle" Value="StockTakeCreateAisle.aspx"></asp:ListItem>
                                        <asp:ListItem Text="Wall to Wall" Value="StockTakeCreateArea.aspx"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </asp:WizardStep>
                            </WizardSteps>
                        </asp:Wizard>
                    </ContentTemplate>
                </asp:UpdatePanel>
</asp:Content>

