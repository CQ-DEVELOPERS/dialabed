using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_ManualPalletise : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            //Sets the menu for Housekeeping
            Session["MenuId"] = 5;

            if (GridViewMovementLine.Rows.Count < 1)
                GetNextMovementLine(false);

            string instructionTypeCode = Request.QueryString["Type"];

            if (instructionTypeCode == "" || instructionTypeCode == null)
                instructionTypeCode = "M";

            if (instructionTypeCode != "O")
                DropDownListReason.Visible = false;

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "Page_Load"

    #region GetNextMovementLine
    protected void GetNextMovementLine(bool remove)
    {
        theErrMethod = "GetNextMovementLine";

        try
        {
            int[,] intDataKey = (int[,])Session["intDataKey"];
            int storageUnitBatchId = 0;
            int pickLocationId = 0;
            int length = intDataKey.Length / 2;
            int index = 0;

            if (length > 0)
            {
                while (index < length)
                {
                    storageUnitBatchId = intDataKey[index, 0];
                    pickLocationId = intDataKey[index, 1];

                    if (remove && storageUnitBatchId != 0)
                    {
                        remove = false;
                        intDataKey[index, 0] = 0;
                        storageUnitBatchId = 0;
                    }

                    if (storageUnitBatchId != 0)
                        break;

                    index++;
                }

                if (index == length)
                {
                    Session.Remove("intDataKey");

                    string instructionTypeCode = Request.QueryString["Type"];

                    if (instructionTypeCode == "" || instructionTypeCode == null)
                        instructionTypeCode = "M";

                    GetEditIndex();
                    Response.Redirect("Movement.aspx?Type=" + instructionTypeCode);
                }
                GridViewMovementLine_DataBind(storageUnitBatchId, pickLocationId);
                GridViewInstruction.DataBind();
            }

            Master.MsgText = "Next Movement Line"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetNextMovementLine"

    #region ButtonCreateLines_Click
    protected void ButtonCreateLines_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCreateLines_Click";

        try
        {
            if (TextBoxLines.Text == "" || TextBoxLines.Text == "0")
                return;

            if (TextBoxQuantity.Text == "" || TextBoxQuantity.Text == "0")
                return;

            if (GridViewMovementLine.Rows.Count > 0)
            {
                int storageUnitBatchId = int.Parse(GridViewMovementLine.DataKeys[0].Values["StorageUnitBatchId"].ToString());
                int pickLocationId = int.Parse(GridViewMovementLine.DataKeys[0].Values["PickLocationId"].ToString());
                int numberOfLines = int.Parse(TextBoxLines.Text);
                Decimal quantity = Decimal.Parse(TextBoxQuantity.Text);
                string instructionTypeCode = Request.QueryString["Type"];
                int reasonId = -1;

                if (instructionTypeCode == "" || instructionTypeCode == null)
                    instructionTypeCode = "M";

                if (instructionTypeCode == "O")
                    reasonId = int.Parse(DropDownListReason.SelectedValue.ToString());

                ManualPalletise ds = new ManualPalletise();

                if (ds.CreateMovements(Session["ConnectionStringName"].ToString(), instructionTypeCode, storageUnitBatchId, pickLocationId, numberOfLines, quantity, reasonId) == true)
                {
                    GridViewMovementLine_DataBind(storageUnitBatchId, pickLocationId);
                    GridViewInstruction.DataBind();
                    TextBoxLines.Enabled = false;
                    ButtonCreateLines.Enabled = false;
                }
                else
                    Response.Write("There was an error!!!" + GridViewMovementLine.Rows[0].Cells[0].Text);
            }
            Master.MsgText = "Create Lines"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonCreateLines_Click"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    //rowList.Add(checkedRow.Cells[1].Text);
                    rowList.Add(GridViewInstruction.DataKeys[index].Values["InstructionId"].ToString());
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";
        try
        {
            GetEditIndex();
            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList == null)
                Session.Remove("checkedList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("checkedList");
                }
                else
                {
                    ManualPalletise ds = new ManualPalletise();

                    while (index > 0)
                    {
                        if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), Convert.ToInt32(rowList[0])) == false)
                            Response.Write("There was an error!!!");

                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    GridViewInstruction.DataBind();
                }
            }
            Master.MsgText = "Auto Allocation"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonAutoLocations_Click"

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Housekeeping/ManualPalletise.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

            Master.MsgText = "Manual Location"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonManualLocations_Click"

    #region ButtonGetNextMovement_Click
    protected void ButtonGetNextMovement_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonGetNextMovement_Click";

        try
        {
            GetNextMovementLine(true);

            Master.MsgText = "Get Next Movement"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonGetNextMovement_Click"

    #region GridViewMovementLine_DataBind
    protected void GridViewMovementLine_DataBind(int storageUnitBatchId, int pickLocationId)
    {
        theErrMethod = "GridViewMovementLine_DataBind";

        try
        {
            ManualPalletise ds = new ManualPalletise();

            GridViewMovementLine.DataSource = ds.GetMovementDetails(Session["ConnectionStringName"].ToString(), storageUnitBatchId, pickLocationId);
            GridViewMovementLine.DataBind();

            Master.MsgText = "Move Line"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewMovementLine_DataBind"

    #region ButtonEdit_Click
    // Sets the GridView into edit mode for selected row	
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonEdit_Click"

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";

        try
        {
            if (GridViewInstruction.EditIndex != -1)
                GridViewInstruction.UpdateRow(GridViewInstruction.EditIndex, true);

            SetEditIndexRowNumber();
            GetNextMovementLine(false);

            Master.MsgText = "Save"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSave_Click"

    #region ButtonDelete_Click
    protected void ButtonDelete_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDelete_Click";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    GridViewInstruction.DeleteRow(index);

                index++;
            }
            GetNextMovementLine(false);

            Master.MsgText = "Delete"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDelete_Click"

    #region ButtonElectronic_Click
    protected void ButtonElectronic_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonElectronic_Click";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            int instructionId = 0;
            Status st = new Status();

            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    instructionId = int.Parse(GridViewInstruction.DataKeys[index].Values["InstructionId"].ToString());
                    if (st.Release(Session["ConnectionStringName"].ToString(), instructionId, (int)Session["OperatorId"], "RL"))
                    {
                        Master.MsgText = "Released";
                        Master.ErrorText = "";
                    }
                    else
                    {
                        Master.MsgText = "";
                        Master.ErrorText = "Failed, please try again";
                    }
                }

                index++;
            }

            GridViewInstruction.DataBind();

            Master.MsgText = "Instruction"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("MovementMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonElectronic_Click"

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";

        try
        {
            if (Session["rowList"] == null)
            {
                int index = 0;
                CheckBox cb = new CheckBox();
                ArrayList rowList = new ArrayList();

                while (index < GridViewInstruction.Rows.Count)
                {
                    GridViewRow checkedRow = GridViewInstruction.Rows[index];
                    cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(index);

                    index++;
                }
                if (rowList.Count > 0)
                    Session["rowList"] = rowList;
            }
            Master.MsgText = "Edit Row"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";

        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];

            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                //if (GridViewInstruction.EditIndex != -1)
                //{
                //    GridViewInstruction.UpdateRow(GridViewInstruction.EditIndex, true);
                //}


                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                    GridViewInstruction.EditIndex = -1;
                }
                else
                {
                    GridViewInstruction.SelectedIndex = (int)rowList[0];
                    GridViewInstruction.EditIndex = (int)rowList[0];
                    //this.GridViewInstruction.Rows[GridViewInstruction.EditIndex].Cells[2].Enabled = false;

                    rowList.Remove(rowList[0]);
                }
            }

            Master.MsgText = "Edit Row"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region GridViewInstruction_OnRowUpdating
    protected void GridViewInstruction_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        theErrMethod = "GridViewInstruction_OnRowUpdating";

        try
        {
            //GridViewRow row = this.GridViewReceiptLine.Rows[e.RowIndex];

            //if (row != null)
            //{
            //    DropDownList drp = (DropDownList)row.FindControl("DropDownListBatch");
            //    Session["drp"] = drp.Text;
            //}

            //foreach (DictionaryEntry entry in e.NewValues)
            //{
            //    e.NewValues[entry.Key] =
            //        Server.HtmlEncode(entry.Value.ToString());
            //}

            Master.MsgText = "Row Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewInstruction_OnRowUpdating"

    #region ObjectDataSourceInstruction_OnUpdating
    protected void ObjectDataSourceInstruction_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        theErrMethod = "ObjectDataSourceInstruction_OnUpdating";
        try
        {
            Master.MsgText = "Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceInstruction_OnUpdating"

    #region ObjectDataSourceInstruction_OnSelecting
    protected void ObjectDataSourceInstruction_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceInstruction_OnSelecting";
        try
        {
            if (GridViewMovementLine.Rows.Count > 0)
            {
                e.InputParameters["StorageUnitBatchId"] = int.Parse(GridViewMovementLine.DataKeys[0].Values["StorageUnitBatchId"].ToString());
                e.InputParameters["PickLocationId"] = int.Parse(GridViewMovementLine.DataKeys[0].Values["PickLocationId"].ToString());

                //GridViewInstruction.DataBind();
            }
            Master.MsgText = "Selecting"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceInstruction_OnSelecting"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ManualPalletise", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ManualPalletise", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

    
}
    