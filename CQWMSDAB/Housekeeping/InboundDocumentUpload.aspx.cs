using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using Microsoft.Reporting.WebForms;

public partial class Housekeeping_InboundDocumentUpload : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";
    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        Master.MsgText = "The file was successfully imported.";
        Master.ErrorText = "";

        if (FileUpload1.HasFile)
        {
            try
            {
                string xml = "";
                System.IO.Stream s = FileUpload1.PostedFile.InputStream;
                BulkImport import = new BulkImport();

                using (StreamReader sr = new StreamReader(s))
                {
                    string line;

                    //Skip the First Line
                    sr.ReadLine();

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] tmpArr = line.Split(Convert.ToChar(","));
                        if (tmpArr.Length >= 12)
                        {
                            xml = "<root>";

                            xml = xml + "<Line>";
                            int count = 0;

                            xml = xml + "<Warehouse><![CDATA[" + tmpArr[count] + "]]></Warehouse>";
                            count++;
                            xml = xml + "<GRVNumber><![CDATA[" + tmpArr[count] + "]]></GRVNumber>";
                            count++;
                            xml = xml + "<GRVType><![CDATA[" + tmpArr[count] + "]]></GRVType>";
                            count++;
                            xml = xml + "<Principal><![CDATA[" + tmpArr[count] + "]]></Principal>";
                            count++;
                            xml = xml + "<ExpectedReceiptDate><![CDATA[" + tmpArr[count] + "]]></ExpectedReceiptDate>";
                            count++;
                            xml = xml + "<Carrier><![CDATA[" + tmpArr[count] + "]]></Carrier>";
                            count++;
                            xml = xml + "<CustomerReferenceNumber><![CDATA[" + tmpArr[count] + "]]></CustomerReferenceNumber>";
                            count++;
                            xml = xml + "<CarrierReferenceNumber><![CDATA[" + tmpArr[count] + "]]></CarrierReferenceNumber>";

                            count++;
                            xml = xml + "<SealNumber><![CDATA[" + tmpArr[count] + "]]></SealNumber>";
                            count++;
                            xml = xml + "<ContainerNumber><![CDATA[" + tmpArr[count] + "]]></ContainerNumber>";
                            count++;
                            xml = xml + "<ContainerSize><![CDATA[" + tmpArr[count] + "]]></ContainerSize>";
                            count++;
                            xml = xml + "<BillOfEntry><![CDATA[" + tmpArr[count] + "]]></BillOfEntry>";
                            count++;
                            xml = xml + "<Reference1><![CDATA[" + tmpArr[count] + "]]></Reference1>";
                            count++;
                            xml = xml + "<Reference2><![CDATA[" + tmpArr[count] + "]]></Reference2>";
                            count++;
                            xml = xml + "<Incoterms><![CDATA[" + tmpArr[count] + "]]></Incoterms>";

                            count++;
                            xml = xml + "<GRVLineNumber><![CDATA[" + tmpArr[count] + "]]></GRVLineNumber>";
                            count++;
                            xml = xml + "<ProductCode><![CDATA[" + tmpArr[count] + "]]></ProductCode>";
                            count++;
                            xml = xml + "<Product><![CDATA[" + tmpArr[count] + "]]></Product>";
                            count++;
                            xml = xml + "<SkuCode><![CDATA[" + tmpArr[count] + "]]></SkuCode>";
                            count++;
                            xml = xml + "<Sku><![CDATA[" + tmpArr[count] + "]]></Sku>";
                            count++;
                            xml = xml + "<Batch><![CDATA[" + tmpArr[count] + "]]></Batch>";
                            count++;
                            xml = xml + "<ExpectedQuantity><![CDATA[" + tmpArr[count] + "]]></ExpectedQuantity>";
                            count++;
                            xml = xml + "<ExpectedUOM><![CDATA[" + tmpArr[count] + "]]></ExpectedUOM>";
                            count++;
                            
                            xml = xml + "<BOELineNumber><![CDATA[" + tmpArr[count] + "]]></BOELineNumber>";
                            count++;
                            xml = xml + "<AlternativeSKU><![CDATA[" + tmpArr[count] + "]]></AlternativeSKU>";
                            count++;
                            xml = xml + "<CountryofOrigin><![CDATA[" + tmpArr[count] + "]]></CountryofOrigin>";
                            count++;
                            xml = xml + "<Weight><![CDATA[" + tmpArr[count] + "]]></Weight>";
                            count++;
                            xml = xml + "<Height><![CDATA[" + tmpArr[count] + "]]></Height>";
                            count++;
                            xml = xml + "<Width><![CDATA[" + tmpArr[count] + "]]></Width>";
                            count++;
                            xml = xml + "<Length><![CDATA[" + tmpArr[count] + "]]></Length>";
                            count++;
                            xml = xml + "<TariffCode><![CDATA[" + tmpArr[count] + "]]></TariffCode>";
                            count++;
                            xml = xml + "<LineReference1><![CDATA[" + tmpArr[count] + "]]></LineReference1>";
                            count++;
                            xml = xml + "<LineReference2><![CDATA[" + tmpArr[count] + "]]></LineReference2>";
                            count++;
                            xml = xml + "<ExpiryDate><![CDATA[" + tmpArr[count] + "]]></ExpiryDate>";
                            count++;
                            xml = xml + "<UnitPrice><![CDATA[" + tmpArr[count] + "]]></UnitPrice>";
                            count++;

                            xml = xml + "</Line>";

                            xml = xml + "</root>";

                            if (!import.InboundDocumentImport(Session["ConnectionStringName"].ToString(), xml))
                            {
                                Master.MsgText = "";
                                Master.ErrorText = "There was an processing the file : " + Server.MapPath(FileUpload1.FileName).ToString() + " " + xml.ToString();
                                break;
                            }
                        }
                    }
                }
                s.Close();
            }
            catch (Exception ex)
            {
                Master.ErrorText = "There was an error saving the file : " + ex.Message.ToString();
            }
        }
        else
        {
            Master.ErrorText = "There was no file selected to upload";
        }
    }
    #endregion ButtonUpload_Click
    
    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            //Read the Excel file in a byte array. here pck is the Excelworkbook              
            string pck = "Warehouse,GRV Number,GRV Type,Owner,Expected Receipt Date,Carrier,Customer Reference Number,Carrier Reference Number,SealNumber,ContainerNumber,BillOfEntry,Reference1,Reference2,Incoterms,GRV Line Number,ProductCode,Product Description,Sku Code,SKU Description,Batch,Expected Quantity,Expected UOM,BOELineNumber,AlternativeSKU,CountryofOrigin,Weight,Height,Width,Length,TariffCode,Reference1,Reference2,ExpiryDate,UnitPrice"
                            + Environment.NewLine + ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,";

            Byte[] fileBytes = System.Text.Encoding.Unicode.GetBytes(pck); ;
            //Byte[] fileBytes = pck.GetAsByteArray();

            //Clear the response               
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Cookies.Clear();
            //Add the header & other information      
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.CacheControl = "private";
            Response.Charset = System.Text.UTF8Encoding.UTF8.WebName;
            Response.ContentEncoding = System.Text.UTF8Encoding.UTF8;
            Response.AppendHeader("Content-Length", fileBytes.Length.ToString());
            Response.AppendHeader("Pragma", "cache");
            Response.AppendHeader("Expires", "60");
            Response.AppendHeader("Content-Disposition",
            "attachment; " +
            "filename=\"InboundDocument.csv\"; " +
            "size=" + fileBytes.Length.ToString() + "; " +
            "creation-date=" + DateTime.Now.ToString("R") + "; " +
            "modification-date=" + DateTime.Now.ToString("R") + "; " +
            "read-date=" + DateTime.Now.ToString("R"));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Write it back to the client    
            Response.BinaryWrite(fileBytes);
            Response.End();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
