<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockTakeCreateArea.aspx.cs" Inherits="Housekeeping_StockTakeCreateArea" Title="<%$ Resources:Default, StockTakeCreateAreaTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateAreaTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockTakeCreateAreaAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Wizard ID="Wizard1" runat="server" Width="100%" Height="300px" OnNextButtonClick="Wizard1_NextButtonClick" OnFinishButtonClick="Wizard1_FinishButtonClick">
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Select Areas" StepType="Step">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="CheckBoxListArea" runat="server" DataSourceID="ObjectDataSourceArea" DataTextField="Area" DataValueField="AreaId" RepeatColumns="6">
                                </asp:CheckBoxList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonDeselect" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="Area" SelectMethod="GetAreasByWarehouse">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:Button ID="ButtonSelect" runat="server" Text="Select All" OnClick="ButtonSelect_Click" />
                        <asp:Button ID="ButtonDeselect" runat="server" Text="Deselect All" OnClick="ButtonDeselect_Click" />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Select Locations" StepType="Step">
                        <table border="solid" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="LabelAreaOrAisle" runat="server" Text="Split Jobs into Areas or Aisles"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelColumnOrLevel" runat="server" Text="Direction Of Count"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListAreaOrAisle" runat="server">
                                        <asp:ListItem Selected="True" Text="Area" Value="1"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Aisle" Value="2"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Level" Value="3"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:DropDownList ID="DropDownListSplits" runat="server">
                                        <asp:ListItem Text="Don't split Job" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Split twice" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Split 3 times" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Split 4 times" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Split 5 times" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Split 6 times" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Split 7 times" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Split 8 times" Value="8"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListColumnOrLevel" runat="server">
                                        <asp:ListItem Selected="True" Text="Column"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Level"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="LabelErrorMsg" runat="server" Text=""></asp:Label>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="View Stock Take Jobs" StepType="Finish">
                        <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1">
                            <Columns>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText='<%$ Resources:Default,ReferenceNumber %>' />
                                <asp:BoundField DataField="Area" HeaderText='<%$ Resources:Default,Area %>' />
                                <asp:BoundField DataField="CreateDate" HeaderText='<%$ Resources:Default,CreateDate %>' />
                                <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' />
                                <asp:BoundField DataField="Count" HeaderText='<%$ Resources:Default,Count %>' />
                                <asp:BoundField DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>' />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="StockTake" SelectMethod="ViewDetails">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="ReferenceNumber" SessionField="referenceNumber" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

