<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockTakeCreateBatch.aspx.cs" Inherits="Housekeeping_StockTakeCreateBatch" Title="<%$ Resources:Default, StockTakeCreateProductTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateProductTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockTakeCreateProductAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Wizard ID="Wizard1" runat="server" Width="100%" Height="300px" OnNextButtonClick="Wizard1_NextButtonClick" OnFinishButtonClick="Wizard1_FinishButtonClick">
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Select Areas" StepType="Step">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="LabelProductCode" runat="server" Text="Product Code:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px"></asp:TextBox>
                                <br />
                                <asp:Label ID="LabelProduct" runat="server" Text="Product:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px"></asp:TextBox>
                                <br />
                                <asp:Label ID="LabelBatch" runat="server" Text="Batch:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxBatch" runat="server" Width="150px"></asp:TextBox>
                                <asp:Button ID="ButtonSearch" runat="server" Text="Search" />
                                <br />
                                <asp:GridView ID="GridViewProductSearch"
                                    runat="server"
                                    AllowPaging="True"
                                    AutoGenerateColumns="False"
                                    DataSourceID="ObjectDataSourceProduct"
                                    DataKeyNames="StorageUnitBatchId,Product">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Product" HeaderText="Product" />
                                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" />
                                        <asp:BoundField DataField="SKUCode" HeaderText="SKUCode" />
                                        <asp:BoundField DataField="Batch" HeaderText="Batch" />
                                    </Columns>
                                </asp:GridView>
                                
                                <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Product" SelectMethod="SearchProductsStorageUnitBatchLocation">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                        <asp:ControlParameter Name="Product" ControlID="TextBoxProduct" Type="String" />
                                        <asp:ControlParameter Name="ProductCode" ControlID="TextBoxProductCode" Type="String" />
                                        <asp:ControlParameter Name="Batch" ControlID="TextBoxBatch" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonDeselect" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="Area" SelectMethod="GetAreasByWarehouse">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:Button ID="ButtonSelect" runat="server" Text="Select All" OnClick="ButtonSelect_Click" />
                        <asp:Button ID="ButtonDeselect" runat="server" Text="Deselect All" OnClick="ButtonDeselect_Click" />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Select Locations" StepType="Step">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="CheckBoxListLocation" runat="server" DataSourceID="ObjectDataSourceLocation" DataTextField="Location" DataValueField="LocationId" RepeatColumns="10">
                                </asp:CheckBoxList>
                                <asp:Label ID="LabelErrorMsg" runat="server" Text=""></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSelectLocation" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonDeselectLocation" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location" SelectMethod="GetLocationsByStorageUnitBatchId">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="storageUnitBatchId" SessionField="storageUnitBatchId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:Button ID="ButtonSelectLocation" runat="server" Text="Select All" OnClick="ButtonSelectLocation_Click" />
                        <asp:Button ID="ButtonDeselectLocation" runat="server" Text="Deselect All" OnClick="ButtonDeselectLocation_Click" />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="View Stock Take Jobs" StepType="Finish">
                        <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1">
                            <Columns>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText='<%$ Resources:Default,ReferenceNumber %>' />
                                <asp:BoundField DataField="Area" HeaderText='<%$ Resources:Default,Area %>' />
                                <asp:BoundField DataField="CreateDate" HeaderText='<%$ Resources:Default,CreateDate %>' />
                                <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' />
                                <asp:BoundField DataField="Count" HeaderText='<%$ Resources:Default,Count %>' />
                                <asp:BoundField DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>' />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="StockTake" SelectMethod="ViewDetails">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="ReferenceNumber" SessionField="referenceNumber" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>