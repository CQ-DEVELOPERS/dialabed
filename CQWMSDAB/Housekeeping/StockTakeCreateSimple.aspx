<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockTakeCreateSimple.aspx.cs" Inherits="Housekeeping_StockTakeCreateSimple" Title="<%$ Resources:Default, StockTakeCreateLocationTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/LocationSearch.ascx" TagName="LocationSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateLocationTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockTakeCreateLocationAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Wizard ID="Wizard1" runat="server" Width="100%" Height="300px" OnNextButtonClick="Wizard1_NextButtonClick" OnFinishButtonClick="Wizard1_FinishButtonClick">
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Search for Location" StepType="Step">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelLocation" runat="server" Text="Location:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxLocation" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td align="right">
                                            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
                                        </td>
                                    </tr>
                                </table>

                                <asp:GridView ID="GridViewLocation"
                                    runat="server"
                                    AllowPaging="True"
                                    AllowSorting="true"
                                    AutoGenerateColumns="False"
                                    DataKeyNames="LocationId"
                                    OnPageIndexChanging="GridViewLocation_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBoxEdit" runat="server" Visible='<%# Bind("Enabled") %>'></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area %>" SortExpression="Area" />
                                        <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location" />
                                        <asp:BoundField DataField="StockTakeInd" HeaderText="<%$ Resources:Default, StockTakeInd %>" SortExpression="StockTakeInd" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <h5>No rows</h5>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="View Stock Take Jobs" StepType="Finish">
                        <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1">
                            <Columns>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText='<%$ Resources:Default,ReferenceNumber %>' />
                                <asp:BoundField DataField="Area" HeaderText='<%$ Resources:Default,Area %>' />
                                <asp:BoundField DataField="CreateDate" HeaderText='<%$ Resources:Default,CreateDate %>' />
                                <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' />
                                <asp:BoundField DataField="Count" HeaderText='<%$ Resources:Default,Count %>' />
                                <asp:BoundField DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>' />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="StockTake" SelectMethod="ViewDetails">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="ReferenceNumber" SessionField="referenceNumber" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

