<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockTakeCreateAisle.aspx.cs" Inherits="Housekeeping_StockTakeCreateAisle" Title="<%$ Resources:Default, StockTakeCreateAreaTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateAreaTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockTakeCreateAreaAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Wizard ID="Wizard1" runat="server" Width="100%" Height="300px" OnNextButtonClick="Wizard1_NextButtonClick" OnFinishButtonClick="Wizard1_FinishButtonClick">
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Select Areas" StepType="Step">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelFromAisle" runat="server" Text="From Aisle"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListFromAisle" runat="server" DataSourceID="ObjectDataSourceAisle"
                                        DataValueField="Aisle" DataTextField="Aisle">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label ID="LabelToAisle" runat="server" Text="To Aisle"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListToAisle" runat="server" DataSourceID="ObjectDataSourceAisle"
                                        DataValueField="Aisle" DataTextField="Aisle">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelFromLevel" runat="server" Text="From Level:"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListFromLevel" runat="server" DataSourceID="ObjectDataSourceLevel"
                                        DataValueField="Level" DataTextField="Level">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label ID="LabelToLevel" runat="server" Text="To Level:"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListToLevel" runat="server" DataSourceID="ObjectDataSourceLevel"
                                        DataValueField="Level" DataTextField="Level">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <asp:ObjectDataSource ID="ObjectDataSourceAisle" runat="server" TypeName="StockTake"
                            SelectMethod="AislesList">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceLevel" runat="server" TypeName="StockTake"
                            SelectMethod="LevelsList">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Select Locations" StepType="Step">
                        <table border="solid" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="LabelAisle" runat="server" Text="Split Jobs by Aisles"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelLevel" runat="server" Text="Split Jobs by Level"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelColumnOrLevel" runat="server" Text="Direction Of Count"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="CheckBoxAisle" runat="Server" Text="Job per Aisle" Checked="true" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBoxLevel" runat="Server" Text="Job per Level" Checked="true" />
                                    <asp:DropDownList ID="DropDownListSplits" runat="server">
                                        <asp:ListItem Text="Don't split Job" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Split twice" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Split 3 times" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Split 4 times" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonListColumnOrLevel" runat="server">
                                        <asp:ListItem Selected="True" Text="Column"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Level"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="LabelErrorMsg" runat="server" Text=""></asp:Label>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="View Stock Take Jobs" StepType="Finish">
                        <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1">
                            <Columns>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText='<%$ Resources:Default,ReferenceNumber %>' />
                                <asp:BoundField DataField="Area" HeaderText='<%$ Resources:Default,Area %>' />
                                <asp:BoundField DataField="CreateDate" HeaderText='<%$ Resources:Default,CreateDate %>' />
                                <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' />
                                <asp:BoundField DataField="Count" HeaderText='<%$ Resources:Default,Count %>' />
                                <asp:BoundField DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>' />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="StockTake" SelectMethod="ViewDetails">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="ReferenceNumber" SessionField="referenceNumber" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

