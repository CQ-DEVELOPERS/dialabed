<%@ Page
    Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="MoveToLocation.aspx.cs"
    Inherits="Housekeeping_MoveToLocation"
    Title="<%$ Resources:Default, MovementTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Src="../Common/LocationSearch.ascx" TagName="LocationSearch" TagPrefix="ucloc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

        <ucloc:LocationSearch ID="LocationSearch" runat="server"></ucloc:LocationSearch>

        <asp:Button ID="ButtonMoveTo" runat="server" Text="Move To Location" OnClick="ButtonMoveTo_Click" />
        


        
</asp:Content>

