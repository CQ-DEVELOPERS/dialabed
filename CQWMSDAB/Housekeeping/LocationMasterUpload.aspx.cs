using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using Microsoft.Reporting.WebForms;

public partial class Housekeeping_LocationMasterUpload : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";
    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        Master.MsgText = "The file was successfully imported.";
        Master.ErrorText = "";

        if (FileUpload1.HasFile)
        {
            try
            {
                string xml = "";
                System.IO.Stream s = FileUpload1.PostedFile.InputStream;
                BulkImport import = new BulkImport();

                using (StreamReader sr = new StreamReader(s))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] tmpArr = line.Split(Convert.ToChar(","));
                        if (tmpArr.Length >= 17)
                        {
                            xml = "<root>";

                            xml = xml + "<Line>";
                            int count = 0;

                            xml = xml + "<Warehouse>" + tmpArr[count] + "</Warehouse>";
                            count++;
                            xml = xml + "<WarehouseCode>" + tmpArr[count] + "</WarehouseCode>";
                            count++;
                            xml = xml + "<Area>" + tmpArr[count] + "</Area>";
                            count++;
                            xml = xml + "<AreaCode>" + tmpArr[count] + "</AreaCode>";
                            count++;
                            xml = xml + "<Location>" + tmpArr[count] + "</Location>";
                            count++;
                            xml = xml + "<Ailse>" + tmpArr[count] + "</Ailse>";
                            count++;
                            xml = xml + "<Column>" + tmpArr[count] + "</Column>";
                            count++;
                            xml = xml + "<Level>" + tmpArr[count] + "</Level>";
                            count++;
                            xml = xml + "<SecurityCode>" + tmpArr[count] + "</SecurityCode>";
                            count++;
                            xml = xml + "<RelativeValue>" + tmpArr[count] + "</RelativeValue>";
                            count++;
                            xml = xml + "<ActiveBinning>" + tmpArr[count] + "</ActiveBinning>";
                            count++;
                            xml = xml + "<ActivePicking>" + tmpArr[count] + "</ActivePicking>";
                            count++;
                            xml = xml + "<PalletQuantity>" + tmpArr[count] + "</PalletQuantity>";
                            count++;
                            xml = xml + "<ProductCode>" + tmpArr[count] + "</ProductCode>";
                            count++;
                            xml = xml + "<SKUCode>" + tmpArr[count] + "</SKUCode>";
                            count++;
                            xml = xml + "<MinimumQuantity>" + tmpArr[count] + "</MinimumQuantity>";
                            count++;
                            xml = xml + "<HandlingQuantity>" + tmpArr[count] + "</HandlingQuantity>";
                            count++;
                            xml = xml + "<MaximumQuantity>" + tmpArr[count] + "</MaximumQuantity>";
                            count++;
                            
                            xml = xml + "</Line>";

                            xml = xml + "</root>";
                            
                            if (!import.LocationMasterImport(Session["ConnectionStringName"].ToString(), xml))
                            {
                                Master.MsgText = "";
                                Master.ErrorText = "There was an processing the file : " + Server.MapPath(FileUpload1.FileName).ToString() + " " + xml.ToString();
                                break;
                            }
                        }
                    }
                }
                s.Close();
            }
            catch (Exception ex)
            {
                Master.ErrorText = "There was an error saving the file : " + ex.Message.ToString();
            }
        }
        else
        {
            Master.ErrorText = "There was no file selected to upload";
        }
    }
    #endregion ButtonUpload_Click
    
    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            Session["FromURL"] = "~/Housekeeping/LocationMasterUpload.aspx";

            Session["ReportName"] = "Location Master";

            ReportParameter[] RptParameters = new ReportParameter[3];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
