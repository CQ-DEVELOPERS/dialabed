﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockTakeCreateCategory.aspx.cs" Inherits="Housekeeping_StockTakeCreateCategory" Title="<%$ Resources:Default, StockTakeCreateAreaTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateAreaTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockTakeCreateAreaAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Wizard ID="Wizard1" runat="server" Width="100%" Height="300px" OnNextButtonClick="Wizard1_NextButtonClick" OnFinishButtonClick="Wizard1_FinishButtonClick">
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Search Category" StepType="Step">
                        <asp:UpdatePanel ID="UpdatePanelPrincipal" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="LabelPrincipal" runat="server" Text="Principal"></asp:Label>
                                <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                    DataTextField="Principal" DataValueField="PrincipalId">
                                </asp:DropDownList>
                                <br />
                                <asp:Label ID="LabelCategory" runat="server" Text="Category"></asp:Label>
                                <asp:TextBox ID="TextBoxCategory" runat="server"></asp:TextBox>
                                <br />
                                <asp:Label ID="LabelExactMatch" runat="server" Text="Exact Match"></asp:Label>
                                <asp:CheckBox ID="CheckBoxCategory" runat="server" />
                                <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                    SelectMethod="PrincipalSelect">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                        <asp:SessionParameter Name="userName" SessionField="UserName"
                                            Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%--<asp:Button ID="Button1" runat="server" Text="Select All" OnClick="ButtonSelect_Click" />--%>
                        <%-- <asp:Button ID="Button2" runat="server" Text="Deselect All" OnClick="ButtonDeselect_Click" />--%>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Select Categories" StepType="Step">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="CheckBoxListCategory" runat="server" DataSourceID="ObjectDataSourceCategory" DataTextField="Category" DataValueField="Category" RepeatColumns="6">
                                </asp:CheckBoxList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonDeselect" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:ObjectDataSource ID="ObjectDataSourceCategory" runat="server" TypeName="StockTake" SelectMethod="CategoryList">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:ControlParameter Name="PrincipalId" ControlID="DropDownListPrincipal" Type="String" PropertyName="SelectedValue" />
                                <asp:ControlParameter Name="Category" ControlID="TextBoxCategory" Type="String" />
                                <asp:ControlParameter Name="ExactMatch" ControlID="CheckBoxCategory" Type="Boolean" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:Button ID="ButtonSelect" runat="server" Text="Select All" OnClick="ButtonSelect_Click" />
                        <asp:Button ID="ButtonDeselect" runat="server" Text="Deselect All" OnClick="ButtonDeselect_Click" />
                    </asp:WizardStep>
                    <%--<asp:WizardStep ID="WizardStep3" runat="server" Title="Select Products" StepType="Step">
                        <asp:UpdatePanel ID="UpdatePanelProducts" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode %>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px"></asp:TextBox>
                                <br />
                                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product %>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px"></asp:TextBox>
                                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search %>" />
                                <br />
                                <asp:GridView ID="GridViewProductSearch"
                                    runat="server"
                                    AllowPaging="True"
                                    AutoGenerateColumns="False"
                                    DataSourceID="ObjectDataSourceProduct"
                                    DataKeyNames="StorageUnitId,Product">
                                    <Columns>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, Select %>">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                                        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                                        <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                                        <asp:BoundField DataField="Principal" HeaderText="<%$ Resources:Default, Principal %>" />
                                    </Columns>
                                </asp:GridView>
                                
                                <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Product" SelectMethod="SearchProductsByCategory">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:ControlParameter Name="Product" ControlID="TextBoxProduct" Type="String" />
                                        <asp:ControlParameter Name="ProductCode" ControlID="TextBoxProductCode" Type="String" />
                                        <asp:ControlParameter Name="PrincipalId" ControlID="DropDownListPrincipal" Type="String" PropertyName="SelectedValue" />
                                        <asp:ControlParameter Name="Category" ControlID="TextBoxCategory" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonDeselect" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Label ID="LabelErrorMsg" runat="server" Text=""></asp:Label>
                    </asp:WizardStep>--%>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="View Stock Take Jobs" StepType="Finish">
                        <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1">
                            <Columns>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText='<%$ Resources:Default,ReferenceNumber %>' />
                                <asp:BoundField DataField="Area" HeaderText='<%$ Resources:Default,Area %>' />
                                <asp:BoundField DataField="CreateDate" HeaderText='<%$ Resources:Default,CreateDate %>' />
                                <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' />
                                <asp:BoundField DataField="Count" HeaderText='<%$ Resources:Default,Count %>' />
                                <asp:BoundField DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>' />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="StockTake" SelectMethod="ViewDetails">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="ReferenceNumber" SessionField="referenceNumber" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


