﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Housekeeping_BOMInstruction : System.Web.UI.Page
{
    private int _setCount = 0;
    private int _totalSets = 0;

    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        theErrMethod = "Page Load";
        
        try
        {
            if (!IsPostBack)
            {
                if (Session["BOMHeaderId"] != null)
                    Session["BOMHeaderId"] = null;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewBOMs.DataBind();
        GridViewBOMs.SelectedIndex = -1;
    }

   
    protected void LinkButtonProduct_Click(object sender, EventArgs e)
    {
        GridViewBOMs.DataBind();
        PanelBOMInstrustions.Visible = false;
        PanelPopup.Visible = true;
    }

    protected void ButtonCreateInstruction_Click(object sender, EventArgs e)
    {
        if (Session["BOMHeaderId"] != null)
        {
            int BOMHeaderId = Convert.ToInt32(Session["BOMHeaderId"]);
            BOM.BOMInstruction_Add(Session["ConnectionStringName"].ToString(), BOMHeaderId, Convert.ToDecimal(TextBoxQuantity.Text), TextBoxOrderNumber.Text, (int)Session["OutboundShipmentId"], (int)Session["IssueId"]);

            Reset();

            GridViewInstruction.DataBind();
        }
        else
        {
            //error
        }
    }

    protected void GridViewBOMs_SelectedIndexChanged(object sender, EventArgs e)
    {
        LinkButtonProduct.Text = GridViewBOMs.SelectedDataKey["ProductCode"].ToString();
        TextBoxOrderNumber.Text = GridViewBOMs.SelectedDataKey["BOMOrderNumber"].ToString();
        TextBoxQuantity.Text = GridViewBOMs.SelectedDataKey["ShipmentTotal"].ToString();

        Session["BOMHeaderId"] = GridViewBOMs.SelectedDataKey["BOMHeaderId"];
        Session["OutboundShipmentId"] = GridViewBOMs.SelectedDataKey["OutboundShipmentId"];
        Session["IssueId"] = GridViewBOMs.SelectedDataKey["IssueId"];
        ButtonCreateInstruction.Enabled = true;
        PanelPopup.Visible = false;
        PanelBOMInstrustions.Visible = true;
        GridViewInstruction.DataBind();
        GridViewBOMs.SelectedIndex = -1;
    }

    protected void GridViewInstruction_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["BOMHeaderId"] = GridViewInstruction.SelectedDataKey["BOMHeaderId"];
        Session["BOMInstructionId"] = GridViewInstruction.SelectedDataKey["BOMInstructionId"];
        GridViewBOMInstructionLines.DataBind();
        LabelError.Text = "";
    }

    protected void GridViewBOMInstructionLines_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnl_InsLines.Visible = false;
        pnl_InsEdit.Visible = true;
        GridViewSubstitutions.DataBind();
    }
    protected void GridViewSubstitutions_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView GridViewProducts = (GridView)e.Row.FindControl("GridViewProducts");
            int BOMLineId = Convert.ToInt32(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[6]);
            int LineNumber = Convert.ToInt32(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0]);
            _setCount += Convert.ToInt32(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[2]);
            _totalSets = Convert.ToInt32(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[3]);

            GridViewProducts.DataSource = StaticInfo.BOMInstructionSubstitutionProduct_Search(Session["ConnectionStringName"].ToString(), BOMLineId, LineNumber);
            GridViewProducts.DataBind();
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[2].Text = "Total:";
            e.Row.Cells[3].Text = _setCount.ToString() + " of " + _totalSets.ToString();
            if (_setCount != _totalSets)
            {
                ButtonUpdateProduct.Enabled = false;
            }
            else
            {
                ButtonUpdateProduct.Enabled = true;
            }
        }
    }

    protected void ButtonUpdateProduct_Click(object sender, EventArgs e)
    {
        pnl_InsLines.Visible = true;
        pnl_InsEdit.Visible = false;
        GridViewBOMInstructionLines.DataBind();
    }

    protected void Reset()
    {
        ButtonCreateInstruction.Enabled = false;
        LinkButtonProduct.Text = Resources.Default.Select;
        TextBoxQuantity.Text = "";
        TextBoxOrderNumber.Text = "";

        PanelBOMInstrustions.Visible = true;
        PanelPopup.Visible = false;
    }

    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    #region ButtonCombine_Click
    protected void ButtonCombine_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBox cb = new CheckBox();
            BOM bom = new BOM();

            int outboundShipmetnId = -1;
            int issueId = -1;
            int firstIssueId = -1;

            foreach (GridViewRow row in GridViewBOMs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    outboundShipmetnId = (int)GridViewBOMs.DataKeys[row.RowIndex].Values["IssueId"];
                    issueId = (int)GridViewBOMs.DataKeys[row.RowIndex].Values["IssueId"];

                    if (issueId != -1 && firstIssueId == -1)
                        firstIssueId = issueId;

                    if (!bom.Combine(Session["ConnectionStringName"].ToString(), outboundShipmetnId, issueId, firstIssueId))
                    {
                        Master.MsgText = ""; Master.ErrorText = "There was an error combining the orders";
                        break;
                    }
                }
            }

            GridViewBOMs.DataBind();
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.Message.ToString();
        }
    }
    #endregion ButtonCombine_Click

    #region ButtonSplit_Click
    protected void ButtonSplit_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBox cb = new CheckBox();
            BOM bom = new BOM();

            int issueId = -1;

            foreach (GridViewRow row in GridViewBOMs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    issueId = (int)GridViewBOMs.DataKeys[row.RowIndex].Values["IssueId"];
                    
                    if (!bom.Split(Session["ConnectionStringName"].ToString(), issueId))
                    {
                        Master.MsgText = ""; Master.ErrorText = "There was an error combining the orders";
                        break;
                    }
                }
            }

            GridViewBOMs.DataBind();
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.Message.ToString();
        }
    }
    #endregion ButtonSplit_Click

    #region ButtonDelete_Click
    protected void ButtonDelete_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBox cb = new CheckBox();
            BOM bom = new BOM();

            int outboundShipmetnId = -1;
            int issueId = -1;
            
            foreach (GridViewRow row in GridViewBOMs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    outboundShipmetnId = (int)GridViewBOMs.DataKeys[row.RowIndex].Values["IssueId"];
                    issueId = (int)GridViewBOMs.DataKeys[row.RowIndex].Values["IssueId"];

                    if (!bom.DeleteOrder(Session["ConnectionStringName"].ToString(), outboundShipmetnId, issueId))
                    {
                        Master.MsgText = ""; Master.ErrorText = "There was an error deleting the orders";
                        break;
                    }
                }
            }

            GridViewBOMs.DataBind();
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.Message.ToString();
        }
    }
    #endregion ButtonDelete_Click

    #region GridViewBOMs_RowCommand
    protected void GridViewBOMs_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        string currentCommand = e.CommandName;
        int index = Int32.Parse(e.CommandArgument.ToString());

        if (currentCommand == "ViewSOH")
        {
            Session["BOMInstructionId"] = -1; // Not linked at this level yet (has not been created)
            Session["OutboundShipmentId"] = int.Parse(GridViewBOMs.DataKeys[index].Values["OutboundShipmentId"].ToString());
            Session["IssueId"] = int.Parse(GridViewBOMs.DataKeys[index].Values["IssueId"].ToString());

            TabContainerBOM.ActiveTab = TabContainerBOM.Tabs[1];

            GridViewBOMInstructionLines.DataBind();
        }
    }
    #endregion GridViewBOMs_RowCommand

    #region GridViewInstruction_RowCommand
    protected void GridViewInstruction_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        string currentCommand = e.CommandName;
        int index = Int32.Parse(e.CommandArgument.ToString());

        if (currentCommand == "Release")
        {
            int bomInstructionId = int.Parse(GridViewInstruction.DataKeys[index].Values["BOMInstructionId"].ToString());
            StaticInfo.BOMInstruction_Release(Session["ConnectionStringName"].ToString(), bomInstructionId);

            GridViewInstruction.DataBind();
            GridViewBOMInstructionLines.DataBind();
        }
    }
    #endregion GridViewInstruction_RowCommand

    #region GridViewInstruction_RowDataBound
    protected void GridViewInstruction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex != -1)
        {
            if (e.Row.Cells[3].Text.ToLower() == "false")
            {
                e.Row.Cells[2].Enabled = false;
            }
        }
    }
    #endregion

    #region ButtonInsertLine_Click
    protected void ButtonInsertLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInsertLine_Click";

        try
        {
            if (Session["BOMInstructionId"] == null || (int)Session["BOMInstructionId"] == -1)
            {
                Master.ErrorText = "Please select a MO.";
                LabelErrorMsg.Text = "Please select a MO.";
                return;
            }

            if (Session["StorageUnitId"] == null || (int)Session["StorageUnitId"] == -1)
            {
                Master.ErrorText = "Please select a Product.";
                LabelErrorMsg.Text = "Please select a Product.";
                return;
            }

            if (TextBoxPackagingQty.Text == "")
            {
                Master.ErrorText = "Please enter a Quantity.";
                LabelErrorMsg.Text = "Please enter a Quantity.";
                return;
            }

            Decimal quantity = Decimal.Parse(TextBoxPackagingQty.Text);

            BOM bom = new BOM();

            if (bom.InsertPackaging(Session["ConnectionStringName"].ToString(),
                                            (int)Session["OperatorId"],
                                            (int)Session["BOMInstructionId"],
                                            (int)Session["StorageUnitId"],
                                            quantity))
            {
                TextBoxPackagingQty.Text = "";

                Master.ErrorText = "";
                LabelErrorMsg.Text = "";

                GridViewInboundLine.DataBind();

                Master.MsgText = "InsertLine";
                Master.ErrorText = "";

                GridViewInstruction.DataBind();
            }
            else
            {
                Master.ErrorText = "May not enter duplicate Product /  Batch Combination";
                LabelErrorMsg.Text = "May not enter duplicate Product /  Batch Combination";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonInsertLine_Click"

    #region ButtonDeleteLine_Click
    protected void ButtonDeleteLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeleteLine_Click";
        try
        {
            if (GridViewInboundLine.SelectedIndex != -1)
                GridViewInboundLine.DeleteRow(GridViewInboundLine.SelectedIndex);

            Master.MsgText = "Deleted"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Inbound_InboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDeleteLine_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "InboundDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + exMsg.Message.ToString());
                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
