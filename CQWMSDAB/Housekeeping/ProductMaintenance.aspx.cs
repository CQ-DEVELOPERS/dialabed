using System;
using System.Text;

public partial class Housekeeping_ProductMaintenance : System.Web.UI.Page
{
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        RadTabStrip1.SelectedIndex = RadTabStrip1.SelectedIndex;
    }

    protected void Export_ExportGrid(object sender, EventArgs e)
    {
        UserControls_ExportButtons ExB = sender as UserControls_ExportButtons;

        switch (ExB.ID)
        {
            case "ExportProducts":
                ProductDescriptionUC.ExportGrid(ExB.ExportTo);
                break;
            case "ExportSKUS":
                ProductSKUUC.ExportGrid(ExB.ExportTo);
                break;
            case "ExportPacks":
                ProductPacksUC.ExportGrid(ExB.ExportTo);
                break;
            case "ExportBatches":
                ProductBatchUC.ExportGrid(ExB.ExportTo);
                break;
            case "ExportAreas":
                ProductAreaUC.ExportGrid(ExB.ExportTo);
                break;
            case "ExportLocations":
                ProductLocationUC.ExportGrid(ExB.ExportTo);
                break;
        }
    }
}
