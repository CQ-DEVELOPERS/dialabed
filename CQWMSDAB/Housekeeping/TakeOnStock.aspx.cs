﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;

public partial class Housekeeping_TakeOnStock : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";
    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        Master.MsgText = "The file was successfully imported.";
        Master.ErrorText = "";

        if (FileUpload1.HasFile)
        {
            try
            {
                string xml = "";
                System.IO.Stream s = FileUpload1.PostedFile.InputStream;
                BulkImport import = new BulkImport();

                using (StreamReader sr = new StreamReader(s))
                {
                    string line;

                    //Skip the First Line
                    sr.ReadLine();

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] tmpArr = line.Split(Convert.ToChar(","));
                        if (tmpArr.Length >= 5)
                        {
                            xml = "<root>";

                            xml = xml + "<Line>";
                            int count = 0;

							xml = xml + "<WarehouseCode><![CDATA[" + tmpArr[count] + "]]></WarehouseCode>";
                            count++;
                            xml = xml + "<Location><![CDATA[" + tmpArr[count] + "]]></Location>";
                            count++;
                            xml = xml + "<ProductCode><![CDATA[" + tmpArr[count] + "]]></ProductCode>";
                            count++;
                            xml = xml + "<Batch><![CDATA[" + tmpArr[count] + "]]></Batch>";
                            count++;
                            xml = xml + "<ExpiryDate><![CDATA[" + tmpArr[count] + "]]></ExpiryDate>";
                            count++;
                            xml = xml + "<Quantity><![CDATA[" + tmpArr[count] + "]]></Quantity>";
                            count++;
                            xml = xml + "<UnitPrice><![CDATA[" + tmpArr[count] + "]]></UnitPrice>";
                            count++;
                            xml = xml + "</Line>";

                            xml = xml + "</root>";

                            if (!import.TakeOnStockImport(Session["ConnectionStringName"].ToString(), xml))
                            {
                                Master.MsgText = "";
                                Master.ErrorText = "There was an error processing the file : " + Server.MapPath(FileUpload1.FileName).ToString() + " " + xml.ToString();
                                break;
                            }
                            //else
                            //{
                            //    import.TakeOnStockInsert(Session["ConnectionStringName"].ToString());
                            //}
                        }
                    }
                    import.TakeOnStockInsert(Session["ConnectionStringName"].ToString());
                }
                RadGridTakeOnStock.DataBind();
                s.Close();
            }
            catch (Exception ex)
            {
                Master.ErrorText = "There was an error saving the file : " + ex.Message.ToString();
            }
        }
        else
        {
            Master.ErrorText = "There was no file selected to upload";
        }
    }
    #endregion ButtonUpload_Click

    #region RadGridTakeOnStock_ItemCommand
    protected void RadGridTakeOnStock_ItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            if (e.CommandName == "Reprocess")
            {
                InterfaceConsole ic = new InterfaceConsole();

                foreach (GridDataItem item in RadGridTakeOnStock.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["InterfaceId"] = item.GetDataKeyValue("InterfaceId");
                        Session["InterfaceTableId"] = item.GetDataKeyValue("InterfaceTableId");

                        if (ic.Reprocess(Session["ConnectionStringName"].ToString(), (int)Session["InterfaceId"], (int)Session["InterfaceTableId"]))
                        {
                            RadGridTakeOnStock.DataBind();
                            Master.MsgText = Resources.Default.Successful;
                        }
                        else
                            Master.ErrorText = "Error";

                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridTakeOnStock_ItemCommand


    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            Session["FromURL"] = "~/Housekeeping/ExternalCompanyImport.aspx";

            Session["ReportName"] = "Take On Stock";

            ReportParameter[] RptParameters = new ReportParameter[3];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Take On Stock" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}