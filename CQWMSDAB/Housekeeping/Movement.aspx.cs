using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_Movement : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                //Sets the menu for Housekeeping
                Session["MenuId"] = 5;
            }
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Movement " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewSearch.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Movement " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSearch_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewSearch.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region MoveAll
    protected void MoveAll(object sender, EventArgs e)
    {
        
        ArrayList subl = new ArrayList();
        ArrayList loc = new ArrayList();
        int index = 0;

        CheckBox cb = new CheckBox();

        while (index < GridViewSearch.Rows.Count)
        {
            GridViewRow checkedRow = GridViewSearch.Rows[index];
            
            cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

            if (cb.Checked == true)
            {

                //intSubl = int.Parse(GridViewSearch.DataKeys[index].Values["StorageUnitBatchId"].ToString());
                //intLoc = int.Parse(GridViewSearch.DataKeys[index].Values["PickLocationId"].ToString());
                subl.Add (int.Parse(GridViewSearch.DataKeys[index].Values["StorageUnitBatchId"].ToString()));
                loc.Add(int.Parse(GridViewSearch.DataKeys[index].Values["PickLocationId"].ToString()));
            }
            index++;
        }

        Session["PassSUBL"] = subl;
        Session["PassLoc"] = loc;

        Session["ReturnURL"] = "~/Housekeeping/Movement.aspx";

        Response.Redirect("~/Housekeeping/MoveToLocation.aspx");


    }

    #endregion MoveAll

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";
        try
        {
            string instructionTypeCode = Request.QueryString["Type"];

            if (instructionTypeCode == "" || instructionTypeCode == null)
                instructionTypeCode = "M";

            GetEditIndex();
            Response.Redirect("ManualPalletise.aspx?Type=" + instructionTypeCode);


        }
        catch (Exception ex)
        {
            result = SendErrorNow("Movement " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonManualLocations_Click"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        try
        {
            int index = 0;
            int dimension = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();
            string[,] strDataKey = new string[GridViewSearch.Rows.Count, 2];
            int[,] intDataKey = new int[GridViewSearch.Rows.Count, 2];
            while (index < GridViewSearch.Rows.Count)
            {
                GridViewRow checkedRow = GridViewSearch.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    intDataKey[dimension, 0] = int.Parse(GridViewSearch.DataKeys[index].Values["StorageUnitBatchId"].ToString());
                    intDataKey[dimension, 1] = int.Parse(GridViewSearch.DataKeys[index].Values["PickLocationId"].ToString());
                    //strDataKey[dimension, 0] = GridViewSearch.DataKeys[index].Values["StorageUnitBatchId"].ToString();
                    //strDataKey[dimension, 1] = GridViewSearch.DataKeys[index].Values["PickLocationId"].ToString();
                    dimension++;
                    //rowList.Add(checkedRow.Cells[1].Text);
                }

                index++;
            }
            if (strDataKey.Length > 0)
                Session["intDataKey"] = intDataKey;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Movement " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_Movement", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_Movement", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
