﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockTakeReference.aspx.cs" Inherits="Housekeeping_StockTakeReference"
    Title="<%$ Resources:Default, StockTakeReferenceTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabStockTake" HeaderText="<%$ Resources:Default, StockTake%>">
            <HeaderTemplate>
            </HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="updatePanel_StockTake" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonAddStockTake" runat="server" Text="<%$ Resources:Default, AddStockTake%>"
                            OnClick="ButtonAddStockTake_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonSkip" runat="server" Text="<%$ Resources:Default, SkipReferenceAdd%>"
                            OnClick="ButtonSkip_Click" style="Width:auto;"/>
                        <asp:Label ID="LabelError" runat="server" ForeColor="Red" Text="" />
                        <asp:GridView ID="GridViewStockTake" DataSourceID="ObjectDataSourceStockTake" DataKeyNames="StockTakeReferenceId"
                            runat="server" AllowPaging="True" AllowSorting="True" PageSize="15" AutoGenerateColumns="False"
                            EmptyDataText="<%$ Resources:Default, CurrentlyNoStockTakes %>" OnSelectedIndexChanged="GridViewStockTake_SelectedIndexChanged" EnableModelValidation="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                                <%--<asp:BoundField HeaderText="<%$ Resources:Default, Warehouse%>" DataField="WarehouseId"
                                    SortExpression="WarehouseId" ReadOnly="True" />--%>
                                <asp:BoundField HeaderText="<%$ Resources:Default, StockTakeType%>" DataField="StockTakeType"
                                    SortExpression="StockTakeType" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, StartDate%>" DataField="startDate"
                                    SortExpression="startDate" ReadOnly="false" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, Reference%>" DataField="Reference" 
                                    SortExpression ="Reference" />
                                <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
                            </Columns>
                        </asp:GridView>
                        <asp:DetailsView Visible="False" ID="DetailsViewInsertStockTake" runat="server" AutoGenerateRows="False"
                            DataSourceID="ObjectDataSourceStockTake" DefaultMode="Insert" OnLoad="DetailsViewInsertStockTake_Load" EnableModelValidation="True">
                            <Fields>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, StockTakeReference%>" InsertVisible="False"
                                    SortExpression="StockTakeReferenceId">
                                    <InsertItemTemplate>
                                        <asp:Button ID="AddStockTake" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Add%>" />
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Warehouse%>" Visible="False">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextWarehouseId" runat="server" Text='<%# Bind("warehouseId") %>' ReadOnly="True" Visible="False"></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, StockTakeType%>">
                                    <InsertItemTemplate>
                                        <asp:RadioButtonList ID="RadioButtonStockTakeType" runat="server" DataValueField='<%# Bind("StockTakeType") %>'>
                                            <asp:ListItem Text="<%$ Resources:Default, Cycle%>" Value="Cycle"></asp:ListItem>
                                            <asp:ListItem Text="<%$ Resources:Default, Exception%>" Value="Exception"></asp:ListItem>
                                            <asp:ListItem Text="<%$ Resources:Default, WalltoWall%>" Value="Wall to Wall"></asp:ListItem>
                                            <asp:ListItem Text="<%$ Resources:Default, GeneralCount%>" Value="General Count"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, StartDate %>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="TextBoxStartDateInsert" runat="server" Text='<%# Bind("startDate") %>'></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderStartDate" runat="server" Animated="true"
                                            Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxStartDateInsert">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorStartDateInsert" runat="server"
                                            ControlToValidate="TextBoxStartDateInsert" ErrorMessage="Please enter Start Date"></asp:RequiredFieldValidator>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Reference%>" SortExpression="Reference">
                                  <InsertItemTemplate>
                                    <asp:TextBox ID="TextBoxReference" runat="server"></asp:TextBox>
                                  </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <InsertItemTemplate>
                                        <asp:LinkButton ID="btnInsert" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Insert%>"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                            Text="<%$ Resources:Default, Cancel%>" OnClick="btnCancel_Click"></asp:LinkButton>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                        <asp:ObjectDataSource ID="ObjectDataSourceStockTake" runat="server" TypeName="StockTake"
                            SelectMethod="StockTake_Search" UpdateMethod="StockTake_Update" InsertMethod="StockTake_Add"
                            DeleteMethod="StockTake_Delete" OnInserting="ObjectDataSourceStockTake_Inserting"
                            OnInserted="ObjectDataSourceStockTake_Inserted">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="Int32" />
                                <asp:Parameter Name="stockTakeReferenceId" Type="Int32" />
                                <asp:Parameter Name="stockTakeType" Type="String" />
                                <asp:Parameter Name="startDate" Type="DateTime" />
                                <asp:Parameter Name="endDate" Type="DateTime" />
                              <asp:Parameter Name="Reference" Type="String" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="stockTakeReferenceNumber" Type="Int32" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:Parameter Name="stockTakeType" Type="string" />
                                <asp:Parameter Name="startDate" Type="DateTime" />
                                <%--<asp:Parameter Name="endDate" Type="DateTime" />--%>
                                <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="Int32" />
                                <asp:Parameter Name="stockTakeReferenceId" Type="Int32" />
                              <asp:Parameter Name="Reference" Type="String" />
                            </InsertParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="StockTakeReferenceId" Type="Int32" />
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceWarehouse" runat="server" TypeName="Warehouse"
                            SelectMethod="GetWarehouseId">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="type" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
