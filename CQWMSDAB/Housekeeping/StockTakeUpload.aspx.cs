using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class Housekeeping_StockTakeUpload : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            GridViewAuthorise.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if(cb.Enabled)
                    cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonReject_Click
    protected void ButtonReject_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReject_Click";

        try
        {
            CheckBox cb = new CheckBox();
            StockTake st = new StockTake();
            
            foreach(GridViewRow row in GridViewAuthorise.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    st.UploadDetailsReject(Session["ConnectionStringName"].ToString(),
                                            (int)Session["WarehouseId"],
                                            (DateTime)GridViewAuthorise.DataKeys[row.RowIndex].Values["ComparisonDate"],
                                            (int)GridViewAuthorise.DataKeys[row.RowIndex].Values["StorageUnitId"],
                                            (int)GridViewAuthorise.DataKeys[row.RowIndex].Values["BatchId"],
                                            GridViewAuthorise.DataKeys[row.RowIndex].Values["WarehouseCode"].ToString());
            }

            GridViewAuthorise.DataBind();
            
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonReject_Click"

    #region ButtonAccept_Click
    protected void ButtonAccept_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAccept_Click";

        var st = new StockTake();

        try
        {

            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                var cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    var StorageUnit = -1;
                    var BatchId = -1;
                    try { StorageUnit = (int)GridViewAuthorise.DataKeys[row.RowIndex].Values["StorageUnitId"]; } catch { };
                    try { BatchId = (int)GridViewAuthorise.DataKeys[row.RowIndex].Values["BatchId"]; } catch { };

                    st.UploadDetailsAccept(Session["ConnectionStringName"].ToString(),
                                            (int)Session["WarehouseId"],
                                            (DateTime)GridViewAuthorise.DataKeys[row.RowIndex].Values["ComparisonDate"],
                                            StorageUnit,
                                            BatchId,
                                            GridViewAuthorise.DataKeys[row.RowIndex].Values["WarehouseCode"].ToString(),
                                            ddlReasonCode.SelectedValue);
                };
            }

            GridViewAuthorise.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }

    protected void CompleteAccept(StockTake stockTake, string reasonCode)
    {
        
    }
    #endregion "ButtonAccept_Click"

    #region ButtonAcceptAll_Click
    protected void ButtonAcceptAll_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAcceptAll_Click";

        try
        {
            CheckBox cb = new CheckBox();
            StockTake st = new StockTake();

            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                st.UploadDetailsAccept(Session["ConnectionStringName"].ToString(),
                                        (int)Session["WarehouseId"],
                                        (DateTime)GridViewAuthorise.DataKeys[row.RowIndex].Values["ComparisonDate"],
                                        -1,
                                        -1,
                                        "-1");
                break;
            }

            GridViewAuthorise.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonAcceptAll_Click"

    #region ButtomReload_Click
    protected void ButtomReload_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtomReload_Click";

        try
        {
            StockTake st = new StockTake();

            st.UploadDetailsReload(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"]);
            
            GridViewAuthorise.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtomReload_Click"

    #region GridViewAuthoriseInit
    protected void GridViewAuthoriseInit(object sender, EventArgs e)
    {
        DataControlFieldCollection Columns = GridViewAuthorise.Columns;
        Configuration config = new Configuration();
        string HeaderText = config.GetDescriptionStringValue(Session["ConnectionStringName"].ToString(), "ST Upload - Custom Field1 Description").Trim();
        if (HeaderText.Length == 0)
        {
            HeaderText = "CustomField1";
        };
        if (config.GetDescriptionIntValue(Session["ConnectionStringName"].ToString(), "ST Upload - Display Custom Field1") == 1)
        {
            foreach (DataControlField df in Columns)
            {
                if (df.HeaderText == HeaderText || df.HeaderText == "CustomField1")
                {
                    df.Visible = true;
                    df.HeaderText = HeaderText;
                };
            };
        }
        else
        {
            foreach (DataControlField df in Columns)
            {
                if (df.HeaderText == HeaderText || df.HeaderText == "")
                {
                    Columns.Remove(df);
                };
            };
        };

    }
    #endregion "GridViewAuthoriseInit"

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            //Session["ShowZeroVariance"] = ControlParameter["ShowZeroVariance"];
            Session["WarehouseCode"] = TextBoxWarehouseCode.Text;
            Session["PrincipalId"] = DropDownListPrincipal.SelectedValue;
            Response.Redirect("~/Reports/StockTakeUploadReport.aspx");
        }
        catch { }

    }
    #endregion "ButtonPrint_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_StockTakeUpload", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_StockTakeUpload", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

    protected void ButtonCreateStockTake_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCreateStockTake_Click";
        var stockTake = new StockTake();
        var productList = new List<int>();

        if (GridViewAuthorise.Rows.Count > 0)
        {
            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                var checkBox = (CheckBox) row.FindControl("CheckBoxEdit");
                if (!checkBox.Checked) continue;

                var dataKey = GridViewAuthorise.DataKeys[row.RowIndex];
                if (dataKey == null) continue;
                if (dataKey.Values == null) continue;

                int storageUnit;

                if (!int.TryParse(dataKey.Values["StorageUnitId"].ToString(), out storageUnit)) continue;

                productList.Add(storageUnit);
            }
        }
        else
        {
            Master.MsgText = ""; Master.ErrorText = "No products selected for stock take.";
            return;
        }

        string jobId;
        string referenceNumber;

        referenceNumber = Session["ReferenceNumber"] == null ? "-1" : Session["ReferenceNumber"].ToString();

        jobId = stockTake.CreateStockTakeJob(Session["ConnectionStringName"].ToString(),
                int.Parse(Session["WarehouseId"].ToString()), int.Parse(Session["OperatorId"].ToString()), "STL",
                referenceNumber, 0).ToString();

        if (jobId == "-1")
            return;

        Session["JobId"] = jobId;

        if (Session["ReferenceNumber"] == null)
            Session["ReferenceNumber"] = "Job " + jobId;

        foreach (var product in productList)
        {
            var location = new Location();
            var locationList = location.GetLocationsByStorageUnitId(Session["ConnectionStringName"].ToString(), int.Parse(Session["WarehouseId"].ToString()), product);

            if (locationList != null && locationList.Tables[0] != null && locationList.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in locationList.Tables[0].Rows)
                {
                    if (!stockTake.CreateStockTakeProduct(Session["ConnectionStringName"].ToString(),
                                                                    int.Parse(Session["WarehouseId"].ToString()),
                                                                    int.Parse(Session["OperatorId"].ToString()),
                                                                    int.Parse(Session["JobId"].ToString()),
                                                                    int.Parse(row[0].ToString()),
                                                                    product,
                                                                    0))
                    {
                        Master.MsgText = ""; Master.ErrorText = string.Format("Failed to create stock take for StorageUnitId: {0} in Location: {1}", product, row[0]);
                        break;
                    }
                }
            }
        }
    }
}
