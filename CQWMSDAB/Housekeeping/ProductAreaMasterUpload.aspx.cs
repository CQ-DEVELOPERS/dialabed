using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using Microsoft.Reporting.WebForms;

public partial class Housekeeping_ProductAreaMasterUpload : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";
    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        Master.MsgText = "The file was successfully imported.";
        Master.ErrorText = "";

        if (FileUpload1.HasFile)
        {
            try
            {
                string xml = "";
                int errorCount = 0;
                int successCount = 0;
                System.IO.Stream s = FileUpload1.PostedFile.InputStream;
                BulkImport import = new BulkImport();

                using (StreamReader sr = new StreamReader(s))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] tmpArr = line.Split(Convert.ToChar(","));
                        if (tmpArr.Length >= 10)
                        {
                            xml = "<root>";

                            xml = xml + "<Line>";
                            int count = 0;

                            xml = xml + "<PrincipalCode>" + tmpArr[count] + "</PrincipalCode>";
                            count++;
                            xml = xml + "<ProductCode>" + tmpArr[count] + "</ProductCode>";
                            count++;
                            xml = xml + "<SKUCode>" + tmpArr[count] + "</SKUCode>";
                            count++;
                            xml = xml + "<Area>" + tmpArr[count] + "</Area>";
                            count++;
                            xml = xml + "<Linked>" + tmpArr[count] + "</Linked>";
                            count++;
                            xml = xml + "<StoreOrder>" + tmpArr[count] + "</StoreOrder>";
                            count++;
                            xml = xml + "<PickOrder>" + tmpArr[count] + "</PickOrder>";
                            count++;
                            xml = xml + "<MinimumQuantity>" + tmpArr[count] + "</MinimumQuantity>";
                            count++;
                            xml = xml + "<ReorderQuantity>" + tmpArr[count] + "</ReorderQuantity>";
                            count++;
                            xml = xml + "<MaximumQuantity>" + tmpArr[count] + "</MaximumQuantity>";

                            xml = xml + "</Line>";

                            xml = xml + "</root>";

                            if (import.ProductAreaMasterImport(Session["ConnectionStringName"].ToString(), xml))
                            {
                                successCount++;
                            }
                            else
                            {
                                errorCount++;
                                Master.ErrorText = "There was an processing the file : " + Server.MapPath(FileUpload1.FileName).ToString() + " " + xml.ToString();
                            }
                        }
                    }
                }
                s.Close();

                Master.MsgText = "Success count: " + successCount.ToString();
                Master.ErrorText = "Error count: " + successCount.ToString() + Master.ErrorText;

                return;
            }
            catch (Exception ex)
            {
                Master.ErrorText = "There was an error saving the file : " + ex.Message.ToString();
            }
        }
        else
        {
            Master.ErrorText = "There was no file selected to upload";
        }
    }
    #endregion ButtonUpload_Click
    
    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            Session["FromURL"] = "~/Housekeeping/ProductAreaMasterUpload.aspx";

            Session["ReportName"] = "Product Area Master";

            ReportParameter[] RptParameters = new ReportParameter[6];

            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("PrincipalId", DropDownListPrincipal.SelectedValue.ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("AreaId", DropDownAreaList.SelectedValue.ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("Linked", cbLinked.Checked.ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
