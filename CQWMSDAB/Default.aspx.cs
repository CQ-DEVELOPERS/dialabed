using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Charting;

public partial class _Default : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                
				Session["MenuId"] = 1;
                if (Session["OperatorMenuId"] != null)
                    if((int)Session["OperatorMenuId"] != -1)
                        Session["MenuId"] = (int)Session["OperatorMenuId"];

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.ToString();
        }

    }
    #endregion Page Load
}
