<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="LoadPlanning.aspx.cs" Inherits="Outbound_LoadPlanning" Title="<%$ Resources:Default, LoadPlanningTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, LoadPlanningTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, LoadPlanningAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Loads%>">
            <ContentTemplate>
                <table>
                    <tr valign="top">
                        <td valign="bottom">
                            <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, Search %>">
                            </asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanelOptions" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelOptions" runat="server" GroupingText="<%$ Resources:Default, PalletBuildOptions%>"
                                                    Font-Size="X-Small">
                                                    <asp:GridView ID="GridViewOptions" runat="server" DataSourceID="ObjectDataSourceOptions"
                                                        AutoGenerateColumns="true" AutoGenerateEditButton="true">
                                                        <RowStyle Font-Size="X-Small" />
                                                        <HeaderStyle Font-Size="X-Small" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonBulkPick" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonSelectiveBulkPick" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonPickByOrder" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:ObjectDataSource ID="ObjectDataSourceOptions" runat="server" TypeName="Planning"
                                            SelectMethod="GetPalletisingOptions" UpdateMethod="UpdatePalletisingOptions">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                    Type="Int32" DefaultValue="-1" />
                                            </SelectParameters>
                                            <UpdateParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                    Type="Int32" DefaultValue="-1" />
                                                <asp:Parameter Name="FP" Type="Boolean" />
                                                <asp:Parameter Name="SS" Type="Boolean" />
                                                <asp:Parameter Name="LP" Type="Boolean" />
                                                <asp:Parameter Name="MP" Type="Boolean" />
                                                <asp:Parameter Name="PP" Type="Boolean" />
                                                <asp:Parameter Name="AlternatePallet" Type="Boolean" />
                                                <asp:Parameter Name="SubstitutePallet" Type="Boolean" />
                                            </UpdateParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanelSize" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelSize" runat="server" GroupingText="<%$ Resources:Default, PalletSizeOptions%>"
                                                    Font-Size="X-Small">
                                                    <asp:GridView ID="GridViewSize" runat="server" DataSourceID="ObjectDataSourceSize"
                                                        AutoGenerateColumns="false" AutoGenerateEditButton="true">
                                                        <RowStyle Font-Size="X-Small" />
                                                        <HeaderStyle Font-Size="X-Small" />
                                                        <Columns>
                                                            <asp:BoundField DataField="Weight" HeaderText="<%$ Resources:Default, WeightPerc%>">
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Volume" HeaderText="<%$ Resources:Default, VolumePerc%>">
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonBulkPick" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonSelectiveBulkPick" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonPickByOrder" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:ObjectDataSource ID="ObjectDataSourceSize" runat="server" TypeName="Planning"
                                            SelectMethod="GetSizeOptions" UpdateMethod="UpdateSizeOptions">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                    Type="Int32" DefaultValue="-1" />
                                            </SelectParameters>
                                            <UpdateParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                    Type="Int32" DefaultValue="-1" />
                                                <asp:Parameter Name="weight" Type="Decimal" />
                                                <asp:Parameter Name="volume" Type="Decimal" />
                                            </UpdateParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanelBuild" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelBuild" runat="server" GroupingText="<%$ Resources:Default, PalletBuildResults%>"
                                                    Font-Size="X-Small">
                                                    <asp:GridView ID="GridViewBuild" runat="server" DataSourceID="ObjectDataSourceBuild"
                                                        AutoGenerateColumns="true">
                                                        <RowStyle Font-Size="X-Small" />
                                                        <HeaderStyle Font-Size="X-Small" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ButtonBulkPick" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonSelectiveBulkPick" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonPickByOrder" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:ObjectDataSource ID="ObjectDataSourceBuild" runat="server" TypeName="Planning"
                                            SelectMethod="GetBuildResults">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                    Type="Int32" DefaultValue="-1" />
                                                <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanelAllocation" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelAllocation" runat="server" GroupingText="<%$ Resources:Default, StockAllocationResults%>"
                                                    Font-Size="X-Small">
                                                    <asp:GridView ID="GridViewAllocation" runat="server" DataSourceID="ObjectDataSourceAllocation"
                                                        AutoGenerateColumns="true">
                                                        <RowStyle Font-Size="X-Small" />
                                                        <HeaderStyle Font-Size="X-Small" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonBulkPick" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonSelectiveBulkPick" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonPickByOrder" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:ObjectDataSource ID="ObjectDataSourceAllocation" runat="server" TypeName="Planning"
                                            SelectMethod="GetAllocationResults">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                    Type="Int32" DefaultValue="-1" />
                                                <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked %>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue %>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours %>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours %>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours %>"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="ButtonBulkPick" runat="server" Text="<%$ Resources:Default, PalletiseBulkPick %>" OnClick="ButtonBulkPick_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonSelectiveBulkPick" runat="server" Text="<%$ Resources:Default, PalletiseSelectiveBulkPick %>" OnClick="ButtonSelectiveBulkPick_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonPickByOrder" runat="server" Text="Pick by Order" OnClick="ButtonPickByOrder_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonAutoAllocation" runat="server" Text="<%$ Resources:Default, AutoAllocation %>"
                                            OnClick="ButtonAutoLocations_Click" style="Width:auto;"/>
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonDeallocate" runat="server" Text="<%$ Resources:Default, DeAllocate %>"
                                            OnClick="ButtonDeallocate_Click" style="Width:auto;"/>
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonPlanningComplete" runat="server" Text="<%$ Resources:Default, PlanningComplete %>"
                                            OnClick="ButtonPlanningComplete_Click" style="Width:auto;"/>
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderRelease" runat="server"
                                            TargetControlID="ButtonPlanningComplete" ConfirmText="<%$ Resources:Default, PressOKtoconfirmplanned %>"
                                            Enabled="True">
                                        </ajaxToolkit:ConfirmButtonExtender>
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonLoadPlanning" runat="server" Text="<%$ Resources:Default, LoadPlanningTitle %>"
                                            Enabled="False" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonWavePlanning" runat="server" Text="<%$ Resources:Default, WavePlanning %>"
                                            Enabled="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInstruction" runat="server" DataSourceID="ObjectDataSourcePlanning"
                            OnPageIndexChanging="GridViewInstruction_PageIndexChanging" AllowPaging="True"
                            AllowSorting="True" DataKeyNames="OutboundShipmentId" AutoGenerateColumns="False"
                            OnRowUpdating="GridViewInstruction_RowUpdating" OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                    SortExpression="NumberOfLines"></asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="DeliveryDate"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AreaType %>" SortExpression="AreaType">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListAreaType" runat="server" DataSourceID="ObjectDataSourceAreaType"
                                            DataTextField="AreaType" DataValueField="AreaType" SelectedValue='<%# Bind("AreaType") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("AreaType") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceRouting"
                                            DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoute" runat="server" Text='<%# Bind("Route") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonBulkPick" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelectiveBulkPick" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPickByOrder" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPlanningComplete" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDeallocate" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewInstruction" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="Planning"
                    SelectMethod="SearchLoads" UpdateMethod="UpdateLoad">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="outboundShipmentId" Type="Int32" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="RouteId" Type="Int32" />
                        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
                        <asp:Parameter Name="Remarks" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceAreaType" runat="server" TypeName="Area"
                    SelectMethod="GetAreaTypes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByIssue">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceRouting" runat="server" SelectMethod="GetRoutes"
                    TypeName="Route">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, DropSequence%>">
            <ContentTemplate>
                <asp:GridView ID="GridViewShipment" runat="server" AutoGenerateColumns="False" DataKeyNames="OutboundShipmentId"
                    DataSourceID="ObjectDataSourceShipment">
                    <Columns>
                        <asp:BoundField DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ShipmentDate" HeaderText="<%$ Resources:Default, ShipmentDate %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, DespatchLocation %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:ObjectDataSource ID="ObjectDataSourceShipment" runat="server" TypeName="OutboundShipment"
                    SelectMethod="GetShipmentDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundShipment">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewDropSequence" runat="server" DataKeyNames="IssueId" AutoGenerateColumns="False"
                            AutoGenerateEditButton="True" DataSourceID="ObjectDataSourceDropSequence">
                            <Columns>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    ReadOnly="True">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    ReadOnly="True">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>"
                                    ReadOnly="True">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>" ReadOnly="True">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DefaultDropSequence" HeaderText="<%$ Resources:Default, DefaultDropSequence %>"
                                    ReadOnly="True">
                                    <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, CurrentDropSequence %>">
                                    <EditItemTemplate>
                                        <div align="center">
                                            <asp:DropDownList ID="DropDownListDropSequence" runat="server" DataSourceID="ObjectDataSourceDropSequenceDDL"
                                                DataTextField="DropSequence" DataValueField="DropSequence" SelectedValue='<%# Bind("DropSequence") %>'>
                                            </asp:DropDownList>
                                        </div>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("DropSequence") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDropSequence" runat="server" TypeName="OutboundShipment"
                    SelectMethod="GetDropSequenceDetails" UpdateMethod="UpdateDropSequenceDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="DropSequence" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceDropSequenceDDL" runat="server" TypeName="OutboundShipment"
                    SelectMethod="GetDropSequence">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Lines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelOrderLine">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelect1" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click" />
                        <asp:Button ID="ButtonEdit" OnClick="ButtonEdit_Click" runat="server" Text="<%$ Resources:Default, Edit%>" />
                        <asp:Button ID="ButtonSave" OnClick="ButtonSave_Click" runat="server" Text="<%$ Resources:Default, Save%>" />
                        <asp:Button ID="ManualPalletise" OnClick="ButtonManualPalletise_Click" runat="server"
                            Text="<%$ Resources:Default, ManualAllocation%>" style="Width:auto;" />
                        <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
                        <asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        <asp:Label ID="LabelYellow2" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
                        <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
                        <asp:GridView ID="GridViewLineUpdate" runat="server" DataSourceID="ObjectDataSourceOrderLines"
                            DataKeyNames="IssueLineId,StorageUnitId" AutoGenerateColumns="False" AllowPaging="True"
                            AllowSorting="True">
                            <Columns>
                                <asp:CommandField></asp:CommandField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListBatch" runat="server" OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged"
                                            DataSourceID="ObjectDataSourceBatch" DataTextField="Batch" DataValueField="StorageUnitBatchId"
                                            SelectedValue='<%# Bind("StorageUnitBatchId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                    SortExpression="OrderQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                    SortExpression="AvailablePercentage"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                    SortExpression="AvailableQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>"
                                    SortExpression="LocationsAllocated"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PalletQuantity" HeaderText="<%$ Resources:Default, PalletQuantity %>"
                                    SortExpression="PalletQuantity"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                            SelectMethod="GetBatchesByStorageUnit" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:ControlParameter ControlID="GridViewLineUpdate" Name="StorageUnitId" PropertyName="SelectedValue"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="Planning"
                            SelectMethod="GetOrderLines" UpdateMethod="UpdateOrderLine">
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Type="Int32" Name="issueLineId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitBatchId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitId"></asp:Parameter>
                            </UpdateParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                    Type="Int32" DefaultValue="-1" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel4" HeaderText="<%$ Resources:Default, Details%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelDetails" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelect2" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click" />
                        <asp:Button ID="ButtonAutoAllocation2" runat="server" Text="<%$ Resources:Default, AutoAllocation%>"
                            OnClick="ButtonAutoLocations2_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonManualLocations2" runat="server" Text="<%$ Resources:Default, AlternateLocation%>"
                            OnClick="ButtonManualLocations_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonMix" runat="server" Text="<%$ Resources:Default, ChangePallets%>"
                            OnClick="ButtonMix_Click" style="Width:auto;"/>
                        <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                        <asp:Label ID="LabelLocationStandard" runat="server" Text="<%$ Resources:Default, NoLocation%>"></asp:Label>
                        <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                        <asp:Label ID="LabelLocationBlue" runat="server" Text="<%$ Resources:Default, Allocated%>"></asp:Label>
                        <asp:GridView ID="GridViewDetails" runat="server" DataSourceID="ObjectDataSourceDetails"
                            AutoGenerateColumns="False" DataKeyNames="InstructionId" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>" SortExpression="XXX">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>"
                                    SortExpression="StorageUnitId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                    SortExpression="Quantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>"
                                    SortExpression="JobId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                    SortExpression="InstructionType"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>"
                                    SortExpression="PickLocation"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation2" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="Planning"
                    SelectMethod="GetOrderDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
