using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_ManualPalletiseLocate : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
       private string result = "";
       private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        theErrMethod = "Page_Load";

        try
        {
            if (GridViewIssueLine.Rows.Count < 1)
                GetNextIssueLine();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletiseLocate" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    #region GridViewArea_SelectedIndexChanged
    protected void GridViewArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewArea_SelectedIndexChanged";
        try
        {
            GridViewLocations.DataBind();
            GridViewLocations.Visible = true;

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletiseLocate" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewArea_SelectedIndexChanged"

    #region GridViewLocations_SelectedIndexChanged
    protected void GridViewLocations_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewLocations_SelectedIndexChanged";

        try
        {
            ManualPalletiseOutbound ds = new ManualPalletiseOutbound();

            if (ds.ManualLocationAllocate(Session["ConnectionStringName"].ToString(),
                                          int.Parse(GridViewIssueLine.DataKeys[0].Values["IssueLineId"].ToString()),
                                          Decimal.Parse(GridViewLocations.SelectedDataKey["AvailableQuantity"].ToString()),
                                          int.Parse(GridViewLocations.SelectedDataKey["LocationId"].ToString())))
            {
                if (ds.allocated)
                {
                    GetNextIssueLine();
                    GridViewLocations.Visible = false;
                }
                else
                {
                    int issueLineId = int.Parse(GridViewIssueLine.DataKeys[0].Values["IssueLineId"].ToString());

                    GridViewIssueLine.DataSource = ds.GetIssueLines(Session["ConnectionStringName"].ToString(), issueLineId);
                    GridViewArea.DataSource = ds.GetAreas(Session["ConnectionStringName"].ToString(), issueLineId);

                    GridViewArea.DataBind();
                    GridViewIssueLine.DataBind();
                }

                GridViewLocations.DataBind();
            }

            Master.MsgText = "Allocation"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletiseLocate" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewLocations_SelectedIndexChanged"

    #region GetNextIssueLine
    protected void GetNextIssueLine()
    {
        theErrMethod = "GetNextIssueLine";

        try
        {
            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList.Count <= 0)
            {
                Session.Remove("checkedList");

                if (Session["FromURL"] != null)
                {
                    Response.Redirect(Session["FromURL"].ToString());
                }
                else
                {
                    //Response.Redirect("../Security/Login.aspx");
                }
            }
            else
            {
                int issueLineId = Convert.ToInt32(rowList[0].ToString());

                Session["IssueLineId"] = issueLineId;

                ManualPalletiseOutbound ds = new ManualPalletiseOutbound();

                GridViewIssueLine.DataSource = ds.GetIssueLines(Session["ConnectionStringName"].ToString(), issueLineId);
                GridViewArea.DataSource = ds.GetAreas(Session["ConnectionStringName"].ToString(), issueLineId);
                rowList.Remove(rowList[0]);
            }

            GridViewArea.DataBind();
            GridViewIssueLine.DataBind();

            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ManualPalletiseLocate" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion "GetNextIssueLine"

    //#region GridViewLocations_DataBind
    //protected void GridViewLocations_DataBind()
    //{
    //    theErrMethod = "GridViewLocations_DataBind";
    //    try
    //    {
    //        ManualPalletiseOutbound ds = new ManualPalletiseOutbound();

    //        GridViewLocations.DataSource = ds.GetLocations(Session["ConnectionStringName"].ToString(),
    //                                                       int.Parse(GridViewIssueLine.DataKeys[0].Values["IssueLineId"].ToString()),
    //                                                       int.Parse(GridViewArea.SelectedDataKey["AreaId"].ToString()));
    //        GridViewLocations.DataBind();

    //        Master.MsgText = "Location"; Master.ErrorText = "";
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("ManualPalletiseLocate" + "_" + ex.Message.ToString()); 
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion "GridViewLocations_DataBind"
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "ManualPalletiseLocate", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "ManualPalletiseLocate", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}