using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_OutboundManualPalletise : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables 
        StringBuilder strBuild = new StringBuilder();
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session["instructionId"] = 102800;
            Session["countLoopsToPreventInfinLoop"] = 0;
            theErrMethod = "Page_Load";

            if (!Page.IsPostBack)
            {
                //Sets the menu for Receiving
                Session["MenuId"] = 3;
                try
                {
                    if (GridViewReceiptLine.Rows.Count < 1)
                        GetNextInstruction();


                    GridViewReceiptLine.DataBind();

                    Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
                }
                catch (Exception ex)
                {
                    result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString());
                    Master.ErrorText = result;
                }
            }
            else
            {
                CheckQtyTotals();
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion Page_Load

    #region GetNextInstruction
    protected void GetNextInstruction()
    {
        theErrMethod = "GetNextInstruction";

        try
        {

            int instructionID = int.Parse(Session["instructionId"].ToString());

            ManualPalletiseSplit ds = new ManualPalletiseSplit();

            GridViewReceiptLine.DataSource = ds.GetInstructionToSplit(Session["ConnectionStringName"].ToString(), instructionID);
            if (GridViewReceiptLine.DataSource == null)
                return;

            GridViewReceiptLine.DataBind();

            if (GridViewReceiptLine.Rows.Count > 0)
            {
                DataSet _dsSet = ds.GetInstructions(Session["ConnectionStringName"].ToString(), int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text));

                GridViewInstruction.DataSource = ds.GetInstructions(Session["ConnectionStringName"].ToString(), int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text));


            }/**/

            Master.MsgText = "Next Instruction"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion GetNextInstruction

    #region ButtonCreateLines_Click
    protected void ButtonCreateLines_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCreateLines_Click";

        try
        {
            if (TextBoxLines.Text == "" || TextBoxLines.Text == "0")
                return;

            ManualPalletiseSplit ds = new ManualPalletiseSplit();

            if (ds.CreateLines(Session["ConnectionStringName"].ToString(),
                               int.Parse(GridViewReceiptLine.Rows[0].Cells[1].Text),
                               int.Parse(TextBoxLines.Text)) == true)
            {
                // GridViewInstruction.DataSource = ds.GetInstructions(int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text));
                GridViewInstruction.DataBind();
                TextBoxLines.Enabled = false;
                ButtonCreateLines.Enabled = false;
            }
            else
                Master.ErrorText = "Create Lines have a problem: " + GridViewReceiptLine.Rows[0].Cells[0].Text;

            Master.MsgText = "Create Line"; 
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion ButtonCreateLines_Click

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        int index = 0;
        CheckBox cb = new CheckBox();
        ArrayList rowList = new ArrayList();
        try
        {
            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(checkedRow.Cells[1].Text);
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion GetEditIndex

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            GetEditIndex();
            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList == null)
                Session.Remove("checkedList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("checkedList");
                }
                else
                {
                    ManualPalletiseSplit ds = new ManualPalletiseSplit();

                    while (index > 0)
                    {
                        if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), Convert.ToInt32(rowList[0])) == false)
                            Response.Write("There was an error!!!");

                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    //Session.Remove("checkedList");

                    //GridViewInstruction.DataSource = ds.GetInstructions(int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text));
                    GridViewInstruction.DataBind();
                }
            }

            Master.MsgText = "Instruction"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion ButtonAutoLocations_Click

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Outbound/ManualPalletise.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

            Master.MsgText = "Manual Location"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonManualLocations_Click

    #region GetNextInstruction
    protected void GetNextInstruction_Click(object sender, EventArgs e)
    {
        theErrMethod = "GetNextInstruction_Click";
        
        try
        {
            GetNextInstruction();

            Master.MsgText = "Next Instruction"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion GetNextInstruction

    #region objManualPalletiseSplit_OnSelecting
    protected void objManualPalletiseSplit_OnSelecting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        theErrMethod = "objManualPalletiseSplit_OnSelecting";
        try
        {
            int jobId = int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text);

            e.InputParameters["jobId"] = jobId;
         
            Master.MsgText = "Manual Palletise Split"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion objManualPalletiseSplit_OnSelecting

    #region SetQtySplitUpdate
    protected void SetQtySplitUpdate(object sender, ObjectDataSourceMethodEventArgs e)
    {
        theErrMethod = "SetQtySplitUpdate";

        try
        {
            int instructionId = int.Parse(GridViewInstruction.Rows[-1].Cells[4].Text);
            e.InputParameters["InstructionId"] = instructionId;

            Decimal quantity = Decimal.Parse(GridViewInstruction.Rows[-1].Cells[4].Text);
            e.InputParameters["Quantity"] = quantity;

            Session["InstructionId"] = instructionId;
            Session["Quantity"] = quantity;

            ManualPalletiseSplit manualPalSplitQtyUpdate = new ManualPalletiseSplit();
            int splitTotal = 0;

            splitTotal = manualPalSplitQtyUpdate.SetQuantityFieldSplit(Session["ConnectionStringName"].ToString(), instructionId, quantity);
            lblSplitTotal.Text = splitTotal.ToString();
            Master.MsgText = "Split Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion SetQtySplitUpdate

    #region GridViewInstruction_RowUpdated
    protected void GridViewInstruction_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {

        theErrMethod = "GridViewInstruction_RowUpdated";

        //GridViewQuantity.Unload();

        int jobId = int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text);


        try
        {
            Instruction.p_Putaway_Palletisation_Create_SplitTotalsDataTable sdt = new Instruction.p_Putaway_Palletisation_Create_SplitTotalsDataTable();

            InstructionTableAdapters.p_Putaway_Palletisation_Create_SplitTotalsTableAdapter sdtadap = new InstructionTableAdapters.p_Putaway_Palletisation_Create_SplitTotalsTableAdapter();

            sdtadap.Fill(sdt, jobId);



            GridViewQuantity.DataSource = sdt;
            GridViewQuantity.DataBind();

            lblSplitTotal.Text = GridViewQuantity.Rows[0].Cells[2].Text;
            lblOrder.Text = GridViewQuantity.Rows[0].Cells[0].Text;
            lblQty.Text = GridViewQuantity.Rows[0].Cells[1].Text;

            #region "hold for now"

            /*

            if (sdt.Rows[0].ItemArray[12].ToString() == GridViewReceiptLine.Rows[0].Cells[5].Text)
            {
                lblSplitTotal.Text = "0";
            }
            else
            {
                int addAll = 0;
                //first add all the rows
                for (int i = 0; i <= GridViewInstruction.Rows.Count - 1; i++)
                {
                    if(GridViewInstruction.Rows[i].Cells[2].Text != "")
                        addAll = addAll + int.Parse(GridViewInstruction.Rows[i].Cells[2].Text);
                }
                int total = int.Parse(GridViewReceiptLine.Rows[0].Cells[5].Text) - int.Parse(sdt.Rows[0].ItemArray[4].ToString());
                              
                lblSplitTotal.Text = total.ToString();
            }
          
            #region "determine the label"
            /*
            //Let's do some calculations :)
            int rownumbers = int.Parse(TextBoxLines.Text);

            //check for 0 in each row before doing cals
            for (int i = 0; i <= rownumbers -1; i++)
            {
                if(GridViewInstruction.Rows[i].Cells[2].Text != "")
                    strBuild.Append(GridViewInstruction.Rows[i].Cells[2].Text);
            }

            //First add all the values together
            int iQty = 0;
            string strQty = "";

            for (int x = 0; x <= strBuild.Length - 1; x++)
            {
                if (GridViewInstruction.Rows[x].Cells[2].Text != "")
                {
                    strQty = GridViewInstruction.Rows[x].Cells[2].Text;
                    iQty += int.Parse(strQty);
                }

            }

            //Order Qty
            int orderQty = int.Parse(GridViewReceiptLine.Rows[0].Cells[5].Text);
            if (iQty != orderQty)
            {
                //doSomeCalcs(orderQty, strQty)
                if (orderQty >= iQty)
                {
                    int totalQty = (orderQty - iQty);
                    lblSplitTotal.Text = totalQty.ToString();
                }
                else
                {
                    int totalQty = (iQty - orderQty);
                    lblSplitTotal.Text = totalQty.ToString();

                }
            }
             * */
            // #endregion "determine the label" 
            #endregion "hold for now"
            
            Master.MsgText = "Row Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion GridViewInstruction_RowUpdated

    #region CheckQtyTotals
    private void CheckQtyTotals()
    {
        
        theErrMethod = "CheckQtyTotals";
        try
        {

            Master.MsgText = "Check Totals"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
           result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString());
           Master.ErrorText = result;
        }

        #region "Wait until we needed"
        /*
        if (TextBoxLines.Text != "" && lblSplitTotal.Text != "")
        {
            for (int i = 0; i <= GridViewInstruction.Rows.Count - 1; i++)
            {
                if (GridViewReceiptLine.Rows[0].Cells[5].Text != lblSplitTotal.Text)
                {
                    int topValue = int.Parse(GridViewReceiptLine.Rows[0].Cells[5].Text);
                    int lblValue = int.Parse(lblSplitTotal.Text);
                    //Which one is the >
                    if (topValue != lblValue)
                    {
                        if (topValue > lblValue)
                        {
                            lblSplitTotal.Text = (topValue - lblValue).ToString();
                        }
                        else
                        {
                            lblSplitTotal.Text = (lblValue - topValue).ToString();
                        }
                    }

                }
            }
        }
         * */
        #endregion "Wait until we needed"
    }
    #endregion CheckQtyTotals

    #region GridViewInstruction_RowUpdating
    protected void GridViewInstruction_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        theErrMethod = "GridViewInstruction_RowUpdating";
        try
        {
            Master.MsgText = "Row Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GridViewInstruction_RowUpdating

    #region FillInstructionGrid
    private void FillInstructionGrid()
    {
        theErrMethod = "FillInstructionGrid";

        try
        {
            int splitTotal = 0;
            int instructionId = -1;
            Decimal quantity = 0;
            instructionId = int.Parse(Session["jobId"].ToString());
            quantity = Decimal.Parse(Session["Quantity"].ToString());


            ManualPalletiseSplit manualPalSplitQtyUpdate = new ManualPalletiseSplit();
            splitTotal = manualPalSplitQtyUpdate.SetQuantityFieldSplit(Session["ConnectionStringName"].ToString(), instructionId, quantity);
            lblSplitTotal.Text = splitTotal.ToString();
            
            Master.MsgText = "Fill Instruction"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString()); Master.ErrorText = result;
        }

    }
    #endregion FillInstructionGrid

    #region cmdPrint_Click
    protected void cmdPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "cmdPrint_Click";
        try
        {
            //Response.Redirect("~/Documents//print_ManualPalletise.aspx");
            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundManualPalletise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }



    }
    #endregion cmdPrint_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundManualPalletise", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "OutboundManualPalletise", theErrMethod, exMsg.Message.ToString());
                
                Master.ErrorText = result;
  
                loopPrevention++;
  
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling


}