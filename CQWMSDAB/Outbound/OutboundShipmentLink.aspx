<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OutboundShipmentLink.aspx.cs" Inherits="Outbound_OutboundShipmentLink"
    Title="<%$ Resources:Default, OutboundShipmentLinkTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundSearchRoute.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundShipmentLinkTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundShipmentLinkAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, SearchLoads%>">
            <ContentTemplate>
                <div style="float: left;">
                    <asp:Label ID="LabelShowOrHide" runat="server"></asp:Label>
                </div>
                <div style="float: left;">
                    <asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
                        <table>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanelOutboundShipmentId" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="LabelOutboundShipmentId" runat="server" Text="<%$ Resources:Default, OutboundShipmentId %>"
                                                Width="100px"></asp:Label>
                                            <asp:TextBox ID="TextBoxOutboundShipmentId" runat="server" Width="150px"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBEOutboundShipmentId" runat="server" FilterType="Numbers"
                                                TargetControlID="TextBoxOutboundShipmentId" Enabled="True">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonInsert" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:Label ID="LableOutboundDocumentType" runat="server" Text="<%$ Resources:Default, OutboundDocumentType %>"
                                        Width="100px"></asp:Label>
                                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataSourceID="ObjectDataSourceOutboundDocumentType1"
                                        DataValueField="OutboundDocumentTypeId" DataTextField="OutboundDocumentType">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType1" runat="server" TypeName="OutboundDocumentType"
                            SelectMethod="GetOutboundDocumentTypes">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <br />
                        <uc2:DateRange ID="DateRange1" runat="server" />
                    </asp:Panel>
                    <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                        TargetControlID="PanelSearch" Radius="10" Color="239, 239, 236" BorderColor="64, 64, 64"
                        Enabled="True" />
                    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="server"
                        TargetControlID="PanelSearch" ExpandControlID="LabelShowOrHide" CollapseControlID="LabelShowOrHide"
                        TextLabelID="LabelShowOrHide" ExpandedText="<<<" CollapsedText=">>" SuppressPostBack="True"
                        Enabled="True" />
                </div>
                <asp:Button ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search %>"
                    OnClick="ButtonDocumentSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:Button ID="ButtonInsert" runat="server" Text="<%$ Resources:Default, Insert %>"
                    OnClick="ButtonInsert_Click" />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundShipment">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundShipment" runat="server" AllowSorting="true" DataKeyNames="OutboundShipmentId"
                            DataSourceID="ObjectDataSourceOutboundShipment" AutoGenerateColumns="False" AutoGenerateSelectButton="True"
                            AutoGenerateEditButton="true" OnSelectedIndexChanged="GridViewOutboundShipment_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField ReadOnly="true" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="NumberOfOrders" HeaderText="<%$ Resources:Default, NumberOfOrders %>"
                                    SortExpression="NumberOfOrders">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="TotalOrders" HeaderText="<%$ Resources:Default, TotalOrders %>" SortExpression="TotalOrders">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ShipmentDate %>" SortExpression="ShipmentDate">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxShipmentDate" runat="server" Text='<%# Bind("ShipmentDate") %>'></asp:TextBox>
                                        <%--<ajaxToolkit:FilteredTextBoxExtender ID="FTBEShipmentDate" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="TextBoxShipmentDate" ValidChars="0123456789/ :"></ajaxToolkit:FilteredTextBoxExtender>--%>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelShipmentDate" runat="server" Text='<%# Bind("ShipmentDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceRoute"
                                            DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelRoute" runat="server" Text='<%# Bind("Route") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="false" DataField="SealNumber" HeaderText="<%$ Resources:Default, SealNumber %>"
                                    SortExpression="SealNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="false" DataField="VehicleRegistration" HeaderText="<%$ Resources:Default, VehicleRegistration %>"
                                    SortExpression="VehicleRegistration">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="false" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"
                                    SortExpression="Remarks">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, DespatchBay %>" SortExpression="DespatchBay">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListDespatchBay" runat="server" DataSourceID="ObjectDataSourceDespatchBay"
                                            DataTextField="DespatchBayName" DataValueField="DespatchBay" SelectedValue='<%# Bind("DespatchBay") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelDespatchBay" runat="server" Text='<%# Bind("DespatchBayName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default,ContactDriver %>" SortExpression="ContactPerson">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListDriver" runat="server" DataSourceID="ObjectDataSourceDriver"
                                            DataTextField="ContactPerson" DataValueField="ContactListId" SelectedValue='<%# Bind("ContactListId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:DropDownList ID="DropDownListDriverInsert" runat="server" DataSourceID="ObjectDataSourceDriver"
                                            DataTextField="ContactPerson" DataValueField="ContactListId" SelectedValue='<%# Bind("ContactListId") %>'>
                                        </asp:DropDownList>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelBindContactPerson" runat="server" Text='<%# Bind("ContactPerson") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonInsert" EventName="click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceOutboundShipment" runat="server" TypeName="OutboundShipment"
                    SelectMethod="SearchOutboundShipmentCreate" UpdateMethod="UpdateOutboundShipment">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:ControlParameter Name="outboundDocumentTypeId" ControlID="DropDownListOutboundDocumentType"
                            DefaultValue="-1" Type="Int32" PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="outboundShipmentId" ControlID="TextBoxOutboundShipmentId"
                            DefaultValue="-1" Type="Int32" />
                        <asp:SessionParameter Name="fromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="toDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                        <asp:Parameter Name="RouteId" Type="Int32" />
                        <asp:Parameter Name="ShipmentDate" Type="DateTime" />
                        <asp:Parameter Name="SealNumber" Type="String" />
                        <asp:Parameter Name="VehicleRegistration" Type="String" />
                        <asp:Parameter Name="Remarks" Type="String" />
                        <asp:Parameter Name="DespatchBay" Type="Int32" />
                        <asp:Parameter Name="ContactListId" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="AreaCode" DefaultValue="D" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceRoute" runat="server" TypeName="Route"
                    SelectMethod="GetRoutes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceDespatchBay" runat="server" TypeName="Location"
                    SelectMethod="GetDespatchBayByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="AreaCode" DefaultValue="D" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceDriver" runat="server" TypeName="Contact"
                    SelectMethod="GetDriverByContactId">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <%--<asp:Parameter Name="ContactListId" Type="String" />--%>
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="<%$ Resources:Default, MaintainLoads%>">
            <ContentTemplate>
                <table border="solid" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Default, LinkedOrders%>"
                                Font-Bold="true"></asp:Label>
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Default, OrdersUnlinked%>"
                                Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewLinked">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="ButtonDropSequence" runat="server" Text="<%$ Resources:Default, DropSequence%>"
                                                    OnClick="ButtonDropSequence_Click" Style="width: auto;" />
                                            </td>
                                            <td>
                                                <asp:Button ID="ButtonViewDetails" runat="server" Text="<%$ Resources:Default, PlanLoad%>"
                                                    OnClick="ButtonViewDetails_Click" />
                                            </td>
                                            <td>
                                                <asp:Button ID="Button1" runat="server" Text="" />
                                            </td>
                                            <td>
                                                <asp:Button ID="Button2" runat="server" Text="" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="GridViewLoadDetails" runat="server" AutoGenerateColumns="False"
                                        DataSourceID="ObjectDataSourceLoadDetails">
                                        <Columns>
                                            <asp:BoundField DataField="LoadWeight" HeaderText="<%$ Resources:Default, LoadWeight %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LoadVolume" HeaderText="<%$ Resources:Default, LoadVolume %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NumberOfOrders" HeaderText="<%$ Resources:Default, NumberOfOrders %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NumberOfDrops" HeaderText="<%$ Resources:Default, NumberOfDrops %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceLoadDetails" runat="server" TypeName="OutboundShipment"
                                        SelectMethod="GetLoadDetails">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                    <asp:GridView ID="GridViewLinked" runat="server" DataKeyNames="OutboundShipmentId,IssueId"
                                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceLinked"
                                        OnSelectedIndexChanged="GridViewLinked_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Remove%>" ShowSelectButton="True" />
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                                SortExpression="OrderNumber" />
                                            <asp:BoundField DataField="OrderWeight" HeaderText="<%$ Resources:Default, OrderWeight %>"
                                                SortExpression="OrderWeight">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OrderVolume" HeaderText="<%$ Resources:Default, OrderVolume %>"
                                                SortExpression="OrderVolume">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceLinked" runat="server" TypeName="OutboundShipment"
                                        SelectMethod="GetLinkedIssues">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonUnlinkedSearch" EventName="click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="GridViewLinked" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonRemove" EventName="click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:Button ID="ButtonAdd" runat="server" Text="<%$ Resources:Default, ButtonAdd%>" OnClick="ButtonAdd_Click" />
                            <asp:Button ID="ButtonRemove" runat="server" Text="<%$ Resources:Default, ButtonRemove%>" OnClick="ButtonRemove_Click" />
                        </td>
                        <td valign="top" width="100%">
                            <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                            <asp:Button ID="ButtonUnlinkedSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                                OnClick="ButtonUnlinkedSearch_Click" />
                            <div style="float: left;">
                                <table>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="middle">
                                            <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="middle">
                                            <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours%>"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewUnlinked">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewUnlinked" runat="server" AllowPaging="True" AllowSorting="true"
                                        AutoGenerateColumns="False" DataKeyNames="IssueId" DataSourceID="ObjectDataSourceUnlinked"
                                        OnSelectedIndexChanged="GridViewUnlinked_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Add%>" ShowSelectButton="True">
                                            </asp:CommandField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                                SortExpression="OrderNumber"></asp:BoundField>
                                            <asp:TemplateField SortExpression="DeliveryDate">
                                                <ItemTemplate>
                                                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, DefaultRoute %>"
                                                SortExpression="Route">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                                SortExpression="ExternalCompanyCode">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>"
                                                SortExpression="ExternalCompany">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                                SortExpression="NumberOfLines">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OrderVolume" HeaderText="<%$ Resources:Default, OrderVolume %>"
                                                SortExpression="OrderVolume">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OrderWeight" HeaderText="<%$ Resources:Default, OrderWeight %>"
                                                SortExpression="OrderWeight">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                                SortExpression="DeliveryDate">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                                SortExpression="Status">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                                SortExpression="OutboundDocumentType">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>"
                                                SortExpression="Location">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                                SortExpression="CreateDate">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>"
                                                SortExpression="Rating">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonUnlinkedSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonRemove" EventName="click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="ObjectDataSourceUnlinked" runat="server" TypeName="OutboundShipment"
                    SelectMethod="GetUnlinkedIssues">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                        <asp:SessionParameter Name="RouteId" SessionField="RouteId" Type="Int32" DefaultValue ="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="OutboundDocumentType"
                    SelectMethod="GetOutboundDocumentTypes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
