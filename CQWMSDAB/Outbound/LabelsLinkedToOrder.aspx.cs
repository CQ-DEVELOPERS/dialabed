﻿
using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_LabelsLinkedToOrder : System.Web.UI.Page
{
    #region InitializeCulture
	
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
	
    #endregion InitializeCulture

    #region Private Properties
	
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = string.Empty;
    private string theErrMethod = string.Empty;
	
    #endregion

    #region Page_Load
	
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
		
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;

                Configuration config = new Configuration();

                int config44 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 44);
                int config45 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 45);
                int config46 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 46);
                int config47 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 47);

            }

            if (Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            Master.MsgText = "Page loaded successfully";
			
			Master.ErrorText = string.Empty;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LabelsLinkedToOrder" + "_" + ex.Message);
			
            Master.ErrorText = result;
        }
    }
	
    #endregion Page_Load

    #region ButtonSearch_Click
	
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        ReferenceNumber refno = new ReferenceNumber();

        theErrMethod = "ButtonSearch_Click";
		
        try
        {
            GridViewJobs.DataBind();

            Master.MsgText = string.Empty;
			
			Master.ErrorText = string.Empty;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LabelsLinkedToOrder" + "_" + ex.Message);
			
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region GridViewJobs_OnSelectedIndexChanged
	
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";

        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();
			
            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LabelsLinkedToOrder" + "_" + ex.Message);
			
            Master.ErrorText = result;
        }
    }
	
    #endregion GridViewJobs_OnSelectedIndexChanged

    #region ButtonPrintJob_Click
	
    protected void ButtonPrintJob_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintJob_Click";
		
        try
        {
            Session["FromURL"] = "~/Outbound/LabelsLinkedToOrder.aspx";

            Session["ReportName"] = "Packing Slip";

            ReportParameter[] RptParameters = new ReportParameter[4];

            // Create the JobId report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("JobId", Session["JobId"].ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LabelsLinkedToOrder" + "_" + ex.Message);
			
            Master.ErrorText = result;
        }
    }
	
    #endregion ButtonPrintJob_Click

    #region ButtonSelect_Click
	
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
				
                if(cb.Enabled)
                    cb.Checked = true;
            }

            Master.MsgText = "Select";
			
			Master.ErrorText = string.Empty;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LabelsLinkedToOrder" + "_" + ex.Message);
			
            Master.ErrorText = result;
        }
    }

    #endregion ButtonSelect_Click

    #region ButtonPrint_Click

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                var cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    DataKey dataKey = GridViewJobs.DataKeys[row.RowIndex];

                    if (dataKey != null)
                        if (dataKey.Values != null) rowList.Add(dataKey.Values["JobId"]);
                }
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            //Session["LabelName"] = "Despatch By Order Label.lbl";

            Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";

            Session["FromURL"] = "~/Outbound/LabelsLinkedToOrder.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = string.Empty;

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LabelsLinkedToOrder " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }

    #endregion ButtonPrint_Click

    #region ErrorHandling

    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "LabelsLinkedToOrder", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "LabelsLinkedToOrder", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}
