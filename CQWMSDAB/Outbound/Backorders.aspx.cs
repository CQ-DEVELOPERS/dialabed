using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_Backorders : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"
    
    #region PRIVATE CONSTANTS
        //private const string DEFAULT_STATUS = "IS";
        private string result = "";
        private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Backorders" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region GridViewOrders_SelectedIndexChanged
    protected void GridViewOrders_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOrders_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewOrders.SelectedDataKey["OutboundShipmentId"].ToString();
            Session["IssueId"] = GridViewOrders.SelectedDataKey["IssueId"];
            ObjectDataSourceOrderLines.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GridViewOrders_SelectedIndexChanged

    #region ButtonBackorder_Click
    protected void ButtonBackorder_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonOperatorRelease_Click";
        try
        {
            int index = GridViewOrders.SelectedIndex;

            if (index != -1)
            {
                Backorders back = new Backorders();
                
                back.BackorderInsert(Session["ConnectionStringName"].ToString(),
                                            int.Parse(GridViewOrders.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                            int.Parse(GridViewOrders.DataKeys[index].Values["IssueId"].ToString()),
                                            (int)Session["OperatorId"]);

                GridViewOrders.DataBind();
                Master.MsgText = "Release"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Backorders" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonBackorder_Click"

    #region ButtonSearch_Click
    /// <summary>
     /// 
     /// </summary>
     /// <param name="sender"></param>
     /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
     {
         theErrMethod = "ButtonSearch_Click";
         try
         {
             GridViewOrders.DataBind();

             Master.MsgText = ""; Master.ErrorText = "";
         }
         catch (Exception ex)
         {
             result = SendErrorNow("Outbound_Backorders" + "_" + ex.Message.ToString()); 
             Master.ErrorText = result;
         }
     }
    #endregion ButtonSearch_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "Outbound_Backorders", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "Outbound_Backorders", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
 #endregion ErrorHandling
}