using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

public partial class Outbound_Boxing : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["JobId"] = null;
                Session["NewJobId"] = null;
                ScriptManager.GetCurrent(this.Page).SetFocus(TextBoxReferenceNumber);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region RadButtonSearch_Click
    protected void RadButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "RadButtonSearch_Click";
        try
        {
            ProductCheck pc = new ProductCheck();

            Session["JobId"] = pc.GetJobId(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], TextBoxReferenceNumber.Text);

            DataSet ds = new DataSet();

            ds = GetOrderSearch();

            //RadGridDetails.DataSource = null;

            RadGridOrders.DataSource = ds;

            RadGridOrders.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";

            RadGridLinkedJobs.DataBind();
            ddJobs.DataBind();

            ddJobs.SelectedValue = Session["JobId"].ToString();

            RadButtonRadButtonConsolidationSlip.Enabled = false;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }

    private DataSet GetOrderSearch()
    {
        DataSet ds = null;

        Database db = DatabaseFactory.CreateDatabase(Session["ConnectionStringName"].ToString());

        string sqlCommand = "p_Mobile_Product_Check_Job_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, int.Parse(Session["WarehouseId"].ToString()));
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, int.Parse(Session["OperatorId"].ToString()));
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, TextBoxReferenceNumber.Text.Trim());
        db.AddInParameter(dbCommand, "newJobId", DbType.Int32, -1);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion RadButtonSearch_Click

    #region RadButtonSave_Click
    protected void RadButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "RadButtonSave_Click";
        try
        {
            ProductCheck pc = new ProductCheck();
            Session["NewJobId"] = null;

            foreach (GridDataItem item in RadGridOrders.Items)
            {
                // For Normal mode
                RadTextBox rtbPackQuantityItem = item.FindControl("rtbPackQuantityItem") as RadTextBox;
                RadNumericTextBox rntbBoxes = item.FindControl("rntbBoxes") as RadNumericTextBox;

                int jobId = (int)item.GetDataKeyValue("JobId");
                int instructionId = (int)item.GetDataKeyValue("InstructionId");
                int issueLineId = (int)item.GetDataKeyValue("IssueLineId");
                string orderNumber = item.GetDataKeyValue("OrderNumber").ToString().Trim();
                int newJobId = -1;
                Decimal packQuantity = 0;
                int boxes = 1;
                Decimal weight = 0;

                if (int.TryParse(ddJobs.SelectedValue.ToString(), out newJobId)
                    && Decimal.TryParse(rtbPackQuantityItem.Text, out packQuantity)
                    && int.TryParse(rntbBoxes.Text, out boxes))
                //&& Decimal.TryParse(TextBoxWeight.Text, out weight))
                {
                    if (packQuantity < 0)
                    {
                        result = SendErrorNow("Boxing" + "_" + "Invalid Quantity");
                        Master.ErrorText = result;
                        return;
                    }
                    if (Session["NewJobId"] == null)
                        if (newJobId == -1)
                            Session["NewJobId"] = pc.GetNewJobId(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], jobId, weight);
                        else
                            Session["NewJobId"] = newJobId;

                    pc.UpdateJob(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], jobId, instructionId, packQuantity, (int)Session["NewJobId"], boxes, weight, issueLineId, orderNumber);
                }
            }

            RadGridOrders.DataBind();
            RadGridLinkedJobs.DataBind();
            ddJobs.DataBind();
            ddJobs.SelectedValue = Session["NewJobId"].ToString();

            Master.MsgText = ""; Master.ErrorText = "";
			RadButtonSearch_Click(sender, e);
        }
        catch (Exception ex)
        {
            result = ex.Message.ToString();
            Master.ErrorText = result;
        }
    }
    #endregion RadButtonSave_Click

    #region RadButtonDefault_Click
    protected void RadButtonDefault_Click(object sender, EventArgs e)
    {
        theErrMethod = "RadButtonDefault_Click";
        try
        {
            foreach (GridDataItem item in RadGridOrders.Items)
            {
                // Default Pack Quantity
                RadTextBox rtbPackQuantityItem = item.FindControl("rtbPackQuantityItem") as RadTextBox;

                rtbPackQuantityItem.Text = item.GetDataKeyValue("BoxQuantity").ToString();

                // Default Box Quantity
                RadNumericTextBox rntbBoxes = item.FindControl("rntbBoxes") as RadNumericTextBox;

                rntbBoxes.Value = Double.Parse(item.GetDataKeyValue("Boxes").ToString());
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadButtonDefault_Click

    #region RadButtonDefaultPicked_Click
    protected void RadButtonDefaultPicked_Click(object sender, EventArgs e)
    {
        theErrMethod = "RadButtonDefaultPicked_Click";
        try
        {
            foreach (GridDataItem item in RadGridOrders.Items)
            {
                // Default Pack Quantity
                RadTextBox rtbPackQuantityItem = item.FindControl("rtbPackQuantityItem") as RadTextBox;

                rtbPackQuantityItem.Text = item.GetDataKeyValue("PickedQuantity").ToString();

                // Default Box Quantity
                RadNumericTextBox rntbBoxes = item.FindControl("rntbBoxes") as RadNumericTextBox;

                rntbBoxes.Value = 1;
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadButtonDefaultPicked_Click

    #region RadButtonFinished_Click
    protected void RadButtonFinished_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();

            if (chk.ConfirmFinished(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["OperatorId"]) == 0)
            {
                // Consolidation Slip
                int issueId = chk.DespatchAdvice(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]);

                if (issueId > 0)
                    RadButtonRadButtonConsolidationSlip.Enabled = true;
                else
                    RadButtonRadButtonConsolidationSlip.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadButtonFinished_Click

    #region RadButtonConsolidationSlip_Click
    protected void RadButtonConsolidationSlip_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();

            // Consolidation Slip
            int issueId = chk.DespatchAdvice(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]);

            if (issueId > 0)
            {
                Session["Blank"] = null; // Hide the menu on the report.aspx page

                Session["FromURL"] = "~/Outbound/ProductChecking.aspx?JobId=" + Session["JobId"].ToString();

                Session["ReportName"] = "Consolidation Slip";

                ReportParameter[] RptParameters = new ReportParameter[5];

                RptParameters[0] = new ReportParameter("OutboundShipmentId", "-1");
                RptParameters[1] = new ReportParameter("IssueId", issueId.ToString());
                RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
                RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
                RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                Session["ReportParameters"] = RptParameters;

                Session["JobId"] = null;
                Session["ReferenceNumber"] = null;

                //Response.Redirect("~/Reports/Report.aspx");

                ScriptManager.RegisterStartupScript(this.Page,
                                                    this.Page.GetType(),
                                                    "newWindow",
                                                    "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                    true);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadButtonConsolidationSlip_Click

    #region RadButtonDelete_Click
    protected void RadButtonDelete_Click(object sender, EventArgs e)
    {
        theErrMethod = "RadButtonDelete_Click";
        try
        {
            ProductCheck pc = new ProductCheck();
            Session["NewJobId"] = null;

            foreach (GridDataItem item in RadGridLinkedJobs.SelectedItems)
            {
                // For Normal mode
                RadTextBox rtbPackQuantityItem = item.FindControl("rtbPackQuantityItem") as RadTextBox;
                RadNumericTextBox rntbBoxes = item.FindControl("rntbBoxes") as RadNumericTextBox;

                int jobId = (int)item.GetDataKeyValue("JobId");

                pc.DeleteJob(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], jobId);
            }

            RadGridOrders.DataBind();
            RadGridLinkedJobs.DataBind();
            ddJobs.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
			RadButtonSearch_Click(sender, e);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadButtonDelete_Click

    #region RadGridOrders_ItemDataBound
    protected void RadGridOrders_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridDataItem)// to access a row 
            {
                GridDataItem item = (GridDataItem)e.Item;
                int variance = 0;
                int pickedQuantity = 0;

                int.TryParse(item.Cells[10].Text, out variance);
                int.TryParse(item.Cells[8].Text, out pickedQuantity);
                string jobStatus = item["JobStatus"].Text;

                if (jobStatus == "Quality Assurance")
                    item["JobStatus"].BackColor = System.Drawing.Color.Red;
                else
                    item["JobStatus"].BackColor = System.Drawing.Color.White;

                if (variance > 0 && variance != pickedQuantity)
                {
                    item.Cells[10].ForeColor = System.Drawing.Color.White;
                    item.Cells[10].BackColor = System.Drawing.Color.Orange;
                    item.Cells[10].Font.Bold = true;
                }
                else if (variance == 0)
                {
                    item.Cells[10].ForeColor = System.Drawing.Color.White;
                    item.Cells[10].BackColor = System.Drawing.Color.LimeGreen;
                    item.Cells[10].Font.Bold = true;
                }
                else if (variance > 0)
                {
                    item.Cells[10].ForeColor = System.Drawing.Color.White;
                    item.Cells[10].BackColor = System.Drawing.Color.Red;
                    item.Cells[10].Font.Bold = true;
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrders_ItemDataBound

    #region RadGridLinkedJobs_ItemCommand
    protected void RadGridLinkedJobs_ItemCommand(object source, GridCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "PackingSlip")
            {

                foreach (GridDataItem item in RadGridLinkedJobs.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["FromURL"] = "~/Outbound/ProductChecking.aspx";

                        Session["ReportName"] = "Packing Slip";

                        ReportParameter[] RptParameters = new ReportParameter[4];

                        // Create the JobId report parameter
                        RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("JobId", item.GetDataKeyValue("JobId").ToString());

                        RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        //Response.Redirect("~/Reports/Report.aspx");

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridLinkedJobs_ItemCommand

    #region RadButtonPrint_Click
    protected void RadButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["ParameterReferenceNumber"] = TextBoxReferenceNumber.Text;
            ArrayList checkedList = new ArrayList();

            foreach (GridDataItem item in RadGridLinkedJobs.Items)
            {
                if (item.Selected)
                    checkedList.Add((int)item.GetDataKeyValue("JobId"));
            }

            int copies = 0;

            int.TryParse(TextBoxLabels.Text, out copies);

            if (copies > 0)
                Session["LabelCopies"] = copies;

            if (checkedList.Count == 0)
                return;

            Session["checkedList"] = checkedList;

            Session["PrintLabel"] = false;

            Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";

            Session["FromURL"] = "~/Outbound/ProductChecking.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                //Response.Redirect("~/Common/NLPrint.aspx");
                Session["LabelClose"] = true;
                ScriptManager.RegisterStartupScript(this.Page,
                                                    this.Page.GetType(),
                                                    "newWindow",
                                                    "window.open('../Common/Label.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
                                                    true);
            }

            RadGridLinkedJobs.DataBind();
            //PrintLabel();
            //Reset();
            //Finish();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadButtonPrint_Click

    #region ddJobs_OnSelectedIndexChanged
    protected void ddJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    Session["NewJobId"] = int.Parse(ddJobs.SelectedValue);

        //    RadGridOrders.DataBind();
        //}
        //catch (Exception ex)
        //{
        //    result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
        //    Master.ErrorText = result;
        //}
    }
    #endregion ddJobs_OnSelectedIndexChanged

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "Boxing", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "Boxing", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);

            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "BoxingRadGridOrders", (int)Session["OperatorId"]);
            }
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridLinkedJobs);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridLinkedJobs, "BoxingRadGridLinkedJobs", (int)Session["OperatorId"]);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Render

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "BoxingRadGridOrders", (int)Session["OperatorId"]);
                }
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridLinkedJobs);
                    settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridLinkedJobs, "BoxingRadGridJobs", (int)Session["OperatorId"]);
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Init

    #region RadGridOrders_PageIndexChanged

    protected void RadGridOrders_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        RadButtonSearch_Click(sender, e);
    }

    #endregion
}