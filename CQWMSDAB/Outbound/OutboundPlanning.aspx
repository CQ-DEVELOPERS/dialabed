<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" 
    AutoEventWireup="true" CodeFile="OutboundPlanning.aspx.cs"
    Title="<%$ Resources:Default, OutboundPlanningTitle %>"
    StylesheetTheme="Default" Theme="Default"
    Inherits="Outbound_OutboundPlanning"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/Outbound/PlanningOrdersUC.ascx" TagName="PlanningOrdersUC" TagPrefix="CQWMS" %>
<%@ Register Src="~/UserControls/Outbound/PlanningOrderLinesUC.ascx" TagName="PlanningOrderLinesUC" TagPrefix="CQWMS" %>
<%@ Register Src="~/UserControls/Outbound/PlanningOrderDetailsUC.ascx" TagName="PlanningOrderDetailsUC" TagPrefix="CQWMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundPlanningTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundPlanningAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<asp:UpdatePanel ID="updatePanel_Products" runat="server" EnableViewState="True">
    <ContentTemplate>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="false">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ucPlanningOrders">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ucPlanningOrders" />
                    <telerik:AjaxUpdatedControl ControlID="ucPlanningOrderLines" />
                    <telerik:AjaxUpdatedControl ControlID="ucPlanningOrderDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ucPlanningOrderLines">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ucPlanningOrders" />
                    <telerik:AjaxUpdatedControl ControlID="ucPlanningOrderLines" />
                    <telerik:AjaxUpdatedControl ControlID="ucPlanningOrderDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    

    <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
        SelectedIndex="0" MultiPageID="Tabs" ontabclick="RadTabStrip1_TabClick" >
        <Tabs>
            <telerik:RadTab Text="<%$ Resources:Default, Orders%>"></telerik:RadTab>
            <telerik:RadTab Text="<%$ Resources:Default, Lines%>"></telerik:RadTab>
            <telerik:RadTab Text="<%$ Resources:Default, Details%>"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
        <telerik:RadPageView runat="server" ID="RadPageView1">
            <CQWMS:PlanningOrdersUC ID="ucPlanningOrders" runat="server" OnErrorMessage="ucOrderPlanning_ErrorMessage"></CQWMS:PlanningOrdersUC>
        </telerik:RadPageView>
        <telerik:RadPageView runat="Server" ID="RadPageView2">
            <CQWMS:PlanningOrderLinesUC ID="ucPlanningOrderLines" runat="server" OnErrorMessage="ucOrderPlanning_ErrorMessage"
                OnTabIndexChange="ucPlanningOrderLines_TabIndexChange"></CQWMS:PlanningOrderLinesUC>
        </telerik:RadPageView>
        <telerik:RadPageView runat="Server" ID="RadPageView3">
            <CQWMS:PlanningOrderDetailsUC ID="ucPlanningOrderDetails" runat="server" OnErrorMessage="ucOrderPlanning_ErrorMessage"></CQWMS:PlanningOrderDetailsUC>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    </ContentTemplate>
</asp:UpdatePanel>
    <script type="text/javascript">
        function selectTab() {
            var tabstrip = $find('<%= RadTabStrip1.ClientID %>');
            tabstrip.get_tabs().getTab("0").click();
        }
    </script>
</asp:Content>
