using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Outbound_OutboundPlanning : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ucPlanningOrderLines_TabIndexChange(int tabIndex)
    {
        ((RadGrid)ucPlanningOrders.FindControl("GridViewInstruction")).Rebind();
        ((RadGrid)ucPlanningOrders.FindControl("GridViewBuild")).Rebind();
        ((RadGrid)ucPlanningOrders.FindControl("GridViewAllocation")).Rebind();
        ((RadGrid)ucPlanningOrders.FindControl("GridViewProject")).Rebind();
        //((RadButton)ucPlanningOrders.FindControl("btnAutoPlanLine")).Rebind();
        RadTabStrip1.SelectedIndex = tabIndex;
        RadPageView1.Selected = true;
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", "selectTab()", true);
    }

    protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
    {
        if (RadTabStrip1.SelectedIndex == 1)
        {
            ucPlanningOrders.RefreshOrderGrid();
            ucPlanningOrderLines.RefreshLineGrid();        
        }
    }

    protected void ucOrderPlanning_ErrorMessage(string msgText, string errorText)
    {
        //Master.MsgText = msgText; Master.ErrorText = errorText;
    }

}