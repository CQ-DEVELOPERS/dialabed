using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Microsoft.Reporting.WebForms;

public partial class Outbound_WavePlanning : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "";
                Master.ErrorText = "";

                rdpFromDate.SelectedDate = DateRange.GetFromDate();
                rdpFromDate.DateInput.DisplayText = DateRange.GetFromDate().ToShortDateString();
                rdpFromDate.FocusedDate = DateRange.GetFromDate();
                Session["FromDate"] = DateRange.GetFromDate();

                rdpToDate.SelectedDate = DateRange.GetToDate().AddDays(1);
                rdpToDate.DateInput.DisplayText = DateRange.GetToDate().AddDays(1).ToShortDateString();
                rdpToDate.FocusedDate = DateRange.GetToDate().AddDays(1);
                Session["ToDate"] = DateRange.GetToDate().AddDays(1);

                RadGridWave.DataBind();
            }
            
			if (Session["IssueId"] == null)
					RadGridComments.Enabled = false;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ContainerPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    //#region Page_LoadComplete
    //protected void Page_LoadComplete(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        theErrMethod = "Page_LoadComplete";

    //        if (Session["FromDate"] != null)
    //            rdpFromDate.SelectedDate = DateTime.Parse(Session["FromDate"].ToString());

    //        if (Session["ToDate"] != null)
    //            rdpToDate.SelectedDate = DateTime.Parse(Session["ToDate"].ToString());
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("ContainerPlanning" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion Page_LoadComplete

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";

        try
        {
            RadGridWave.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ContainerPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDocumentSearch_Click"

    #region RadGridWave_SelectedIndexChanged
    protected void RadGridWave_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridWave_SelectedIndexChanged";

        try
        {
            Session["IssueId"] = -1;

            foreach (GridDataItem item in RadGridWave.SelectedItems)
            {
                Session["TableId"] = item.GetDataKeyValue("IssueId");
                
                Session["TableName"] = "Issue";
            }
			RadGridComments.Enabled = true;
            RadGridComments.DataBind();

            Master.MsgText = "Shipment Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ContainerPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "RadGridWave_SelectedIndexChanged"

    #region RadGridWave_ItemCommand
    protected void RadGridWave_ItemCommand(object source, GridCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Invoice")
            {
                foreach (GridDataItem item in RadGridWave.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                        Session["IssueId"] = item.GetDataKeyValue("IssueId");

                        Session["FromURL"] = "~/Outbound/OrderQuery.aspx";

                        Session["ReportName"] = "Invoice Cipla";

                        ReportParameter[] RptParameters = new ReportParameter[5];

                        RptParameters[0] = new ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[1] = new ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[2] = new ReportParameter("UserName", Session["UserName"].ToString());

                        // Create the OutboundShipmentId report parameter
                        RptParameters[3] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());

                        // Create the IssueId report parameter
                        RptParameters[4] = new ReportParameter("IssueId", Session["IssueId"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }

            if (e.CommandName == "ShortPicks")
            {
                foreach (GridDataItem item in RadGridWave.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                        Session["IssueId"] = item.GetDataKeyValue("IssueId");

                        Session["FromURL"] = "~/Outbound/OrderQuery.aspx";

                        Session["ReportName"] = "Invoice Advice";

                        ReportParameter[] RptParameters = new ReportParameter[6];

                        RptParameters[0] = new ReportParameter("ConnectionString", "-1");

                        RptParameters[1] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());

                        RptParameters[2] = new ReportParameter("IssueId", Session["IssueId"].ToString());

                        RptParameters[3] = new ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[4] = new ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[5] = new ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }

            if (e.CommandName == "DeliveryNote")
            {
                foreach (GridDataItem item in RadGridWave.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                        Session["IssueId"] = item.GetDataKeyValue("IssueId");

                        Session["FromURL"] = "~/Outbound/OrderQuery.aspx";

                        Session["ReportName"] = "Customer Order";

                        ReportParameter[] RptParameters = new ReportParameter[6];
                        
						// Create the ConnectionString report parameter
                        string strReportConnectionString = Session["ReportConnectionString"].ToString();
                        RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);
						
                        // Create the OutboundShipmentId report parameter
                        RptParameters[1] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());

                        // Create the IssueId report parameter
                        RptParameters[2] = new ReportParameter("IssueId", Session["IssueId"].ToString());
			
                        RptParameters[3] = new ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[4] = new ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[5] = new ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }

            if (e.CommandName == "Containers")
            {
                foreach (GridDataItem item in RadGridWave.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                        Session["IssueId"] = item.GetDataKeyValue("IssueId");

                        Session["FromURL"] = "~/Outbound/OrderQuery.aspx";

                        Session["ReportName"] = "Packing Slip Load";

                        ReportParameter[] RptParameters = new ReportParameter[5];

                        RptParameters[0] = new ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[1] = new ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[2] = new ReportParameter("UserName", Session["UserName"].ToString());

                        // Create the OutboundShipmentId report parameter
                        RptParameters[3] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());

                        // Create the IssueId report parameter
                        RptParameters[4] = new ReportParameter("IssueId", Session["IssueId"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridWave_ItemCommand

    //#region RadGridWave_ItemDataBound
    //protected void RadGridWave_ItemDataBound(object sender, GridItemEventArgs e)
    //{
    //    try
    //    {
    //        if (e.Item is GridDataItem)// to access a row 
    //        {
    //            GridDataItem item = (GridDataItem)e.Item;

    //            DateTime deliveryDate = DateTime.Today;
    //            DateTime otherDate = DateTime.Today;

    //            DateTime.TryParse(item.Cells[10].Text, out deliveryDate);

    //            ArrayList dateList = new ArrayList();
                
    //            dateList.Add(13);
    //            dateList.Add(14);
    //            dateList.Add(15);
    //            dateList.Add(16);
    //            dateList.Add(17);
    //            dateList.Add(18);
    //            dateList.Add(19);
                
    //            foreach (Int32 i in dateList)
    //            {
    //                DateTime.TryParse(item.Cells[i].Text, out otherDate);

    //                if (otherDate.ToString() == "0001/01/01 12:00:00 AM")
    //                {
    //                    item.Cells[i].ForeColor = System.Drawing.Color.White;
    //                    item.Cells[i].BackColor = System.Drawing.Color.Red;
    //                    item.Cells[i].Font.Bold = true;
    //                }
    //                else if (deliveryDate <= otherDate)
    //                {
    //                    item.Cells[i].ForeColor = System.Drawing.Color.White;
    //                    item.Cells[i].BackColor = System.Drawing.Color.Green;
    //                    item.Cells[i].Font.Bold = true;
    //                }
    //                else if (deliveryDate > otherDate)
    //                {
    //                    item.Cells[i].ForeColor = System.Drawing.Color.White;
    //                    item.Cells[i].BackColor = System.Drawing.Color.Orange;
    //                    item.Cells[i].Font.Bold = true;
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("Boxing" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion RadGridWave_ItemDataBound

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "ContainerPlanning", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "ContainerPlanning", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);
            SavePetNames();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Render

    #region SavePetNames
    protected void SavePetNames()
    {
        try
        {
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridWave);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridWave, "OrderQueryRadGridWave", (int)Session["OperatorId"]);
            }
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridUnreadCommunication);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridUnreadCommunication, "OrderQueryRadGridUnreadCommunication", (int)Session["OperatorId"]);
            }

            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridComments);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridComments, "OrderQueryRadGridComments", (int)Session["OperatorId"]);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion SavePetNames

    #region ButtonSaveSettings_Click
    protected void ButtonSaveSettings_Click(object sender, EventArgs e)
    {
        try
        {
            SavePetNames();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSaveSettings_Click

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridWave);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridWave, "OrderQueryRadGridWave", (int)Session["OperatorId"]);
                }
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridUnreadCommunication);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridUnreadCommunication, "OrderQueryRadGridUnreadCommunication", (int)Session["OperatorId"]);
                }

                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridComments);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridComments, "OrderQueryRadGridComments", (int)Session["OperatorId"]);
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingProgress" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Init
}


