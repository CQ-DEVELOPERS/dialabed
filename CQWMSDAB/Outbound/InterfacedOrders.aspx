<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="InterfacedOrders.aspx.cs" Inherits="Outbound_InterfacedOrders" Title="<%$ Resources:Default, InterfacedOrdersTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, InterfacedOrdersTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, InterfacedOrdersAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <table>
        <tr>
            <td>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
            </td>
            <td>
                <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"/>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="ButtonUpdate" OnClick="ButtonUpdate_Click" runat="server" Text="<%$ Resources:Default, ButtonUpdate %>"/>
    <br />
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:GridView ID="GridViewOrder" runat="server" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="False" 
                DataSourceID="ObjectDataSourceOrder" DataKeyNames="IssueId">
                <Columns>
                    <asp:CommandField ShowSelectButton="true"></asp:CommandField>
                    <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber">
                    <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                    <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" SortExpression="OutboundDocumentType">
                    <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>" SortExpression="CustomerCode">
                    <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>" SortExpression="Customer">
                    <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate">
                    <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate">
                    <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:CommandField ShowDeleteButton="True"></asp:CommandField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonUpdate" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    
    <asp:ObjectDataSource ID="ObjectDataSourceOrder" runat="server" TypeName="InterfacedOrders"
        SelectMethod="SearchOrder" DeleteMethod="DeleteOrder">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
            <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
            <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
            <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
            <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
            <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
        </SelectParameters>
        <DeleteParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="IssueId" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
</asp:Content>

