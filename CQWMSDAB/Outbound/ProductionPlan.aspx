<%@ Page Language="C#" MasterPageFile="~/MasterPages/Empty.master" AutoEventWireup="true" CodeFile="ProductionPlan.aspx.cs" Inherits="Outbound_ProductionPlan" 
    Title="Production Priorities"
    StylesheetTheme="Default"
    Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/Empty.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></asp:ScriptManager>--%>
    <ajaxToolkit:ToolkitScriptManager EnablePartialRendering="true"  runat="Server" ID="ScriptManager1" />
        <asp:UpdatePanel ID="up1" runat="server">
            <ContentTemplate>
            <div class="reorderListDemo">
                <ajaxToolkit:ReorderList ID="ReorderList1" runat="server"
                    AllowReorder="true"
                    PostBackOnReorder="true"
                    DataSourceID="ObjectDataSource1"
                    CallbackCssStyle="callbackStyle"
                    DragHandleAlignment="Left"
                    ItemInsertLocation="Beginning"
                    DataKeyField="IssueId"
                    SortOrderField="DropSequence">
                    <ItemTemplate>
                        <div class="itemArea">
                            <asp:Label ID="Label1" runat="server" Text='<%# HttpUtility.HtmlEncode(Convert.ToString(Eval("Rank"))) %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# HttpUtility.HtmlEncode(Convert.ToString(Eval("OrderNumber"))) %>' />
                            <asp:Label ID="Label3" runat="server" Text='<%# HttpUtility.HtmlEncode(Convert.ToString(Eval("DeliveryDate"))) %>' />
                            <asp:Label ID="Label4" runat="server" Text='<%# HttpUtility.HtmlEncode(Convert.ToString(Eval("Batch"))) %>' />
                        </div>
                    </ItemTemplate>
                    <ReorderTemplate>
                        <asp:Panel ID="Panel2" runat="server" CssClass="reorderCue" />
                    </ReorderTemplate>
                    <DragHandleTemplate>
                        <div class="dragHandle"></div>
                    </DragHandleTemplate>
                </ajaxToolkit:ReorderList>
            </div>
            
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="ProductionPlan" 
                SelectMethod="Select" UpdateMethod="Update">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="Rank" Type="Int32" />
                    <asp:Parameter Name="IssueId" Type="Int32" />
                    <asp:Parameter Name="DropSequence" Type="Int32" />
                    <asp:Parameter Name="OrderNumber" Type="String" />
                    <asp:Parameter Name="DeliveryDate" Type="DateTime" />   
                    <asp:Parameter Name="Batch" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>