using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_DespatchCheck : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;

                //throw new Exception();
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow(ex.Message.ToString());
                Master.ErrorText = result;
            }

        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewOutboundDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow(ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonSearch_Click"

    #region GridViewOutboundDocument_OnSelectedIndexChanged
    protected void GridViewOutboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString() == "")
                Session["OutboundShipmentId"] = -1;
            else
                Session["OutboundShipmentId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString());

            if (GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString() == "")
                Session["IssueId"] = -1;
            else
                Session["IssueId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString());

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow(ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "GridViewOutboundDocument_OnSelectedIndexChanged"

    #region ButtonCheckSheet_Click
    protected void ButtonCheckSheet_Click(object sender, EventArgs e)
    {

        theErrMethod = "ButtonCheckSheet_Click";
        try
        {
            theErrMethod = "ButtonCheckSheet_Click";

            Master.MsgText = "Check Sheet"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow(ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonCheckSheet_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            if (Tabs.ActiveTabIndex == 0)
                foreach (GridViewRow row in GridViewOutboundDocument.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }

            if (Tabs.ActiveTabIndex == 1)
                foreach (GridViewRow row in GridViewJobs.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ButtonDespatched_Click
    protected void ButtonDespatched_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDespatched_Click";
        try
        {
            CheckBox cb = new CheckBox();
            DespatchChecking dc = new DespatchChecking();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    if (dc.UpdateLine(Session["ConnectionStringName"].ToString(), int.Parse(GridViewJobs.DataKeys[row.RowIndex].Values["JobId"].ToString())))
                        cb.Checked = false;
                    else
                        Master.ErrorText = "Failed to update row";
            }

            GridViewJobs.DataBind();

            Master.MsgText = "Despatched"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow(ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDespatched_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "DespatchCheck", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "DespatchCheck", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling


}
