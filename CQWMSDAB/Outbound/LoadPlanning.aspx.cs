using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;


public partial class Outbound_LoadPlanning : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["OutboundShipmentId"] != null)
                {
                    OutboundSearch1.Visible = false;
                    ButtonSearch.Visible = false;
                }
                else
                {
                    Session["OutboundShipmentId"] = null;
                    Session["IssueId"] = null;
                }

                Configuration config = new Configuration();

                int config44 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 44);
                int config45 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 45);
                int config46 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 46);
                int config47 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 47);

                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 155))
                {
                    ButtonAutoAllocation.Visible = false;
                    ButtonAutoAllocation2.Visible = false;
                }

                LabelRed.Text = "Overdue";
                LabelOrange.Text = "Required in " + config45.ToString() + " hours";
                LabelYellow.Text = "Required in " + config46.ToString() + " hours";
                LabelStandard.Text = "Required > " + config47.ToString() + " hours";

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewInstruction.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            if (GridViewInstruction.SelectedIndex != -1)
            {
                Planning plan = new Planning();

                if (!plan.AutoLocationAllocateShipment(Session["ConnectionStringName"].ToString(), (int)GridViewInstruction.SelectedDataKey.Values["OutboundShipmentId"]))
                {
                    Master.ErrorText = "Auto Allocation Not Complete";
                    return;
                }

                GridViewLineUpdate.DataBind();
                GridViewDetails.DataBind();
                GridViewBuild.DataBind();
                GridViewAllocation.DataBind();
                GridViewInstruction.DataBind();

                Master.MsgText = "Locations allocated"; Master.ErrorText = "";
            }
            else
                Master.ErrorText = "Please select an Order.";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations_Click

    #region ButtonDeallocate_Click
    protected void ButtonDeallocate_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeallocate_Click";

        try
        {
            if (GridViewInstruction.SelectedIndex != -1)
            {
                Palletise palletise = new Palletise();

                if (!palletise.DeallocateOutboundShipment(Session["ConnectionStringName"].ToString(), (int)GridViewInstruction.SelectedDataKey.Values["OutboundShipmentId"]))
                {
                    Master.ErrorText = "De-Allocation Not Complete";
                    return;
                }

                GridViewLineUpdate.DataBind();
                GridViewDetails.DataBind();
                GridViewBuild.DataBind();
                GridViewAllocation.DataBind();
                GridViewInstruction.DataBind();

                Master.MsgText = "Locations de-allocated"; Master.ErrorText = "";
            }
            else
                Master.ErrorText = "Please select an Order.";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonDeallocate_Click

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    rowList.Add(GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndex:

    #region Refresh
    protected void Palletise(bool bulkPick, bool selectiveBulkPick, bool pickByOrder)
    {
        theErrMethod = "Palletise";

        try
        {
            if (GridViewInstruction.SelectedIndex != -1)
            {
                Planning plan = new Planning();

                //if (plan.Palletise(Session["ConnectionStringName"].ToString(), int.Parse(GridViewInstruction.DataKeys[GridViewInstruction.SelectedIndex].Values["IssueId"].ToString()), -1))
                if (plan.Palletise(Session["ConnectionStringName"].ToString(),
                                    (int)Session["WarehouseId"],
                                    int.Parse(GridViewInstruction.DataKeys[GridViewInstruction.SelectedIndex].Values["OutboundShipmentId"].ToString()),
                                    -1,
                                    (int)Session["OperatorId"],
                                    bulkPick,
                                    selectiveBulkPick,
                                    pickByOrder))
                {
                    GridViewLineUpdate.DataBind();
                    GridViewDetails.DataBind();
                    GridViewBuild.DataBind();
                    GridViewAllocation.DataBind();
                    GridViewInstruction.DataBind();

                    Master.MsgText = "Palletised"; Master.ErrorText = "";
                }
                else
                {
                    Master.ErrorText = "Palletisation Failed";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Refresh

    #region ButtonBulkPick_Click
    protected void ButtonBulkPick_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonBulkPick_Click";

        try
        {
            Palletise(true, false, false);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonBulkPick_Click

    #region ButtonSelectiveBulkPick_Click
    protected void ButtonSelectiveBulkPick_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectiveBulkPick_Click";

        try
        {
            Palletise(false, true, false);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelectiveBulkPick_Click

    #region ButtonPickByOrder_Click
    protected void ButtonPickByOrder_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPickByOrder_Click";

        try
        {
            Palletise(false, false, true);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPickByOrder_Click

    #region ButtonPlanningComplete_Click
    protected void ButtonPlanningComplete_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPlanningComplete_Click";

        try
        {
            int index = GridViewInstruction.SelectedIndex;

            if (index != -1)
            {
                Status st = new Status();

                int outboundShipmentId = -1;
                int issueId = -1;

                if (GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString() == "" || GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString() == "-1")
                {
                    issueId = int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString());
                }
                else
                {
                    outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString());
                }

                if (st.PlanningComplete(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId))
                {
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 70))
                    {
                        Session["FromURL"] = "~/Outbound/LoadPlanning.aspx";

                        Session["ReportName"] = "Despatch Advice";

                        ReportParameter[] RptParameters = new ReportParameter[6];

                        // Create the ConnectionString report parameter
                        RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());
                        // Create the OutboundShipmentId report parameter
                        RptParameters[1] = new ReportParameter("OutboundShipmentId", outboundShipmentId.ToString());
                        // Create the IssueId report parameter
                        RptParameters[2] = new ReportParameter("IssueId", issueId.ToString());

                        RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        Response.Redirect("~/Reports/Report.aspx", true);
                    }
                    else
                    {
                        GridViewInstruction.DataBind();
                    }

                    Master.MsgText = "Planning Completed Successful"; Master.ErrorText = "";
                }
                else
                {
                    Master.MsgText = "Planning Completed Failed";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPlanningComplete_Click

    #region ButtonManualLocation_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Outbound/OutboundPlanning.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonManualLocation_Click

    #region ButtonLink_Click
    protected void ButtonLink_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLink_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Outbound/OutboundPlanning.aspx";
            Response.Redirect("~/Common/RegisterSerialNumberPallet.aspx");

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLink_Click

    #region GridViewInstruction_PageIndexChanging
    protected void GridViewInstruction_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        theErrMethod = "GridViewInstruction_PageIndexChanging";
        #region HoldGrid
        //GridViewInstruction.DataSource =
        //    ds.GetInstructions(start,
        //                        end,
        //                        int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString()));

        //GridViewInstruction.PageIndex = e.NewPageIndex;

        //GridViewInstruction.DataBind();       
        #endregion HoldGrid
    }
    #endregion GridViewInstruction_PageIndexChanging

    #region GridViewInstruction_RowUpdating
    protected void GridViewInstruction_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        theErrMethod = "GridViewInstruction_RowUpdating";
        try
        {
            for (int i = 0; i <= e.NewValues.Values.Count - 1; i++)
            {
                switch (i)
                {

                    case 0:
                        if (e.NewValues["IssueId"] != null)
                        {
                            e.NewValues["IssueId"] = int.Parse(e.NewValues["IssueId"].ToString());//, System.Globalization.NumberStyles.Any);
                        }
                        break;
                    case 1:
                        if (e.NewValues["locationId"] != null)
                        {
                            e.NewValues["locationId"] = int.Parse(e.NewValues["locationId"].ToString());//, System.Globalization.NumberStyles.Any);
                        }

                        break;
                    case 2:
                        if (e.NewValues["priorityId"] != null)
                        {
                            e.NewValues["priorityId"] = int.Parse(e.NewValues["priorityId"].ToString());//, System.Globalization.NumberStyles.Any);
                        }

                        break;
                    case 3:
                        if (e.NewValues["deliveryDate"] != null)
                        {
                            e.NewValues["deliveryDate"] = int.Parse(e.NewValues["deliveryDate"].ToString());//, System.Globalization.NumberStyles.Any);
                        }

                        break;
                    default:
                        break;
                }
            }

            //   e.Cancel = true;


            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewInstruction_RowUpdating

    #region GridViewInstruction_SelectedIndexChanged
    protected void GridViewInstruction_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInstruction_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewInstruction.SelectedDataKey["OutboundShipmentId"].ToString();
            Session["IssueId"] = -1;
            GridViewLineUpdate.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = ex.Message.ToString();
        }

    }
    #endregion GridViewInstruction_SelectedIndexChanged

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";
        try
        {
            e.InputParameters["storageUnitId"] = int.Parse(GridViewLineUpdate.SelectedDataKey["StorageUnitId"].ToString());

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceBatch_OnSelecting"

    #region ObjectDataSourceOrderLines_Selecting
    protected void ObjectDataSourceOrderLines_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceOrderLines_Selecting";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceOrderLines_Selecting

    #region GridViewLineUpdate_SelectedIndexChanging
    protected void GridViewLineUpdate_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        theErrMethod = "GridViewLineUpdate_SelectedIndexChanging";

        try
        {
            Session["StorageUnitId"] = GridViewLineUpdate.DataKeys[1].Value;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewLineUpdate_SelectedIndexChanging

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "DropDownListBatch_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["StorageUnitId"] = int.Parse(dpList.SelectedValue);

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ButtonEdit_Click
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEdit_Click

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";

        try
        {
            if (Session["rowList"] == null)
            {
                int index = 0;
                CheckBox cb = new CheckBox();
                ArrayList rowList = new ArrayList();

                while (index < GridViewLineUpdate.Rows.Count)
                {
                    GridViewRow checkedRow = GridViewLineUpdate.Rows[index];
                    cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(index);

                    index++;
                }
                if (rowList.Count > 0)
                    Session["rowList"] = rowList;
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";

        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];


            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                if (GridViewLineUpdate.EditIndex != -1)
                {

                    GridViewLineUpdate.UpdateRow(GridViewLineUpdate.EditIndex, true);
                }


                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                    GridViewLineUpdate.EditIndex = -1;
                }
                else
                {
                    GridViewLineUpdate.SelectedIndex = (int)rowList[0];
                    GridViewLineUpdate.EditIndex = (int)rowList[0];
                    //this.GridViewLineUpdate.Rows[GridViewLineUpdate.EditIndex].Cells[2].Enabled = false;


                    rowList.Remove(rowList[0]);
                }
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";

        try
        {
            if (GridViewLineUpdate.EditIndex != -1)
                GridViewLineUpdate.UpdateRow(GridViewLineUpdate.EditIndex, true);

            SetEditIndexRowNumber();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSave_Click

    #region ButtonManualPalletise_Click
    protected void ButtonManualPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualPalletise_Click";

        try
        {
            ArrayList rowDataKeys = new ArrayList();
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewLineUpdate.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(row.RowIndex);

                    rowDataKeys.Add(GridViewLineUpdate.DataKeys[row.RowIndex].Value);
                }
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowDataKeys;

            Session["FromURL"] = "OutboundPlanning.aspx";

            Response.Redirect("ManualPalletiseLocate.aspx");

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonManualPalletise_Click

    #region ButtonAutoLocations2_Click
    protected void ButtonAutoLocations2_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            CheckBox cb = new CheckBox();
            Planning plan = new Planning();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                if (cb.Checked == true)
                    if (plan.AutoLocationAllocateInstruction(Session["ConnectionStringName"].ToString(), (int)GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]) == false)
                    {
                        Master.ErrorText = "Auto Allocation Not Complete";
                        break;
                    }
            }

            GridViewDetails.DataBind();

            Master.MsgText = "Auto Locations"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations2_Click

    #region ButtonMix_Click
    protected void ButtonMix_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonMix_Click";
        try
        {
            Session["InstructionTypeCode"] = "PM";
            Session["ShipmentId"] = (int)GridViewInstruction.SelectedDataKey["OutboundShipmentId"];
            //Session["OrderNumber"] = (string)GridViewInstruction.SelectedDataKey["OrderNumber"];
            Response.Redirect("~/Common/MixedJobCreate.aspx");
            Master.MsgText = "Mix"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonMix_Click

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            if (Tabs.ActiveTabIndex == 2)
                foreach (GridViewRow row in GridViewLineUpdate.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }

            if (Tabs.ActiveTabIndex == 3)
                foreach (GridViewRow row in GridViewDetails.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundPlanning", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundPlanning", theErrMethod, ex);

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}