using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_PickingMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "";

        if (!Page.IsPostBack)
        {
            try
            {
                if (Request.QueryString.Count > 0)
                {
                    string activeTab = Request.QueryString["ActiveTab"].ToString();

                    if (activeTab != null)
                    {
                        switch(activeTab)
                        {
                            case "1":
                                Tabs.ActiveTabIndex = 1;

                                //if (Session["InstructionId"] != null)
                                //    foreach (GridViewRow row in GridViewFullLines.Rows)
                                //    {
                                //        if (GridViewFullLines.DataKeys[row.RowIndex].Values["InstructionId"].ToString() == Session["InstructionId"].ToString())
                                //        {
                                //            GridViewFullLines.EditIndex = row.RowIndex;
                                //            break;
                                //        }
                                //    }
                                break;
                            case "3":
                                Tabs.ActiveTabIndex = 3;

                                if (Session["InstructionId"] != null)
                                    foreach (GridViewRow row in GridViewMixedLines.Rows)
                                    {
                                        if (GridViewMixedLines.DataKeys[row.RowIndex].Values["InstructionId"].ToString() == Session["InstructionId"].ToString())
                                        {
                                            GridViewMixedLines.EditIndex = row.RowIndex;
                                            break;
                                        }
                                    }
                                break;
                        }
                    }
                }
                else
                {
                    Session["OutboundShipmentId"] = null;
                    Session["IssueId"] = null;
                    Session["JobId"] = null;
                }

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }

        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewOutboundDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonSearch_Click"

    #region GridViewOutboundDocument_OnSelectedIndexChanged
    protected void GridViewOutboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString() == "")
                Session["OutboundShipmentId"] = -1;
            else
                Session["OutboundShipmentId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString());

            if (GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString() == "")
                Session["IssueId"] = -1;
            else
                Session["IssueId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString());

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewOutboundDocument_OnSelectedIndexChanged"

    #region ButtonEdit_Click
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEdit_Click

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";
        try
        {
            if (GridViewMixedLines.EditIndex != -1)
                GridViewMixedLines.UpdateRow(GridViewMixedLines.EditIndex, true);

            SetEditIndexRowNumber();

            Master.MsgText = "Save"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSave_Click

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewMixedLines.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    rowList.Add(row.RowIndex);
            }

            if (rowList.Count > 0)
                Session["rowList"] = rowList;

            Master.MsgText = "Selection Changed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";
        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];

            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                if (GridViewMixedLines.EditIndex != -1)
                    GridViewMixedLines.UpdateRow(GridViewMixedLines.EditIndex, true);

                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                    GridViewMixedLines.EditIndex = -1;
                }
                else
                {
                    GridViewMixedLines.EditIndex = (int)rowList[0];

                    rowList.Remove(rowList[0]);
                }
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewMixedLines.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    rowList.Add(GridViewMixedLines.DataKeys[row.RowIndex].Values["InstructionId"]);

                index++;
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndex"

    #region ButtonSplit_Click
    protected void ButtonSplit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSplit_Click";
        try
        {
            GetEditIndex();

            Session["FromURL"] = "~/Outbound/PickingMaintenance.aspx";
            Response.Redirect("~/Common/ManualPalletiseSplit.aspx");

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonSplit_Click"

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";
        try
        {
            if (GridViewMixedLines.EditIndex == -1)
            {
                GetEditIndex();
            }
            else
            {
                ArrayList rowList = new ArrayList();

                rowList.Add(GridViewMixedLines.DataKeys[GridViewMixedLines.EditIndex].Values["InstructionId"]);

                Session["checkedList"] = rowList;
            }

            Session["FromURL"] = "~/Outbound/PickingMaintenance.aspx" + "?ActiveTab=" + Tabs.ActiveTabIndex.ToString();
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

            Master.MsgText = "Manual Location"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" +  ex.Message.ToString()); 
            Master.ErrorText = result;
        }
        

    }
    #endregion "ButtonManualLocations_Click"

    #region ButtonReset_Click
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReset_Click";

        try
        {
            Status status = new Status();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewMixedLines.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    if (!status.ResetStatus(Session["ConnectionStringName"].ToString(), int.Parse(GridViewMixedLines.DataKeys[row.RowIndex].Values["InstructionId"].ToString())))
                    {
                        Master.ErrorText = "Please try again.";
                        break;
                    }
            }

            GridViewMixedLines.DataBind();

            Master.MsgText = "Reset";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonReset_Click"

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            Master.MsgText = "ButtonPrint_Click"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPrint_Click"

    #region ButtonPickLabel_Click 
    protected void ButtonPickLabel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPickLabel_Click";
        try
        {
            Master.MsgText = "Pick Label"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPickLabel_Click"

    #region ButtonJobLabel_Click
    protected void ButtonJobLabel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonJobLabel_Click";
        try
        {
            Master.MsgText = "Job Label"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonJobLabel_Click"

    #region ButtonSerial_Click
    protected void ButtonSerial_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSerial_Click";
        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Outbound/PickingMaintenance.aspx";
            Response.Redirect("~/Common/RegisterSerialNumberPallet.aspx");

            Master.MsgText = "Serial"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonSerial_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewMixedLines.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PickingMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PickingMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
