using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

public partial class Outbound_WavePlanning : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "";
                Master.ErrorText = "";

                Session["FromDate"] = DateRange.GetFromDate();
                Session["ToDate"] = DateRange.GetToDate();

                rdpFromDate.SelectedDate = DateRange.GetFromDate();
                rdpToDate.SelectedDate = DateRange.GetToDate().AddDays(1);

                RadGridWave.DataBind();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            //theErrMethod = "Page_LoadComplete";

            //if (Session["WaveId"] != null)
            //{
            //    int waveId = (int)Session["WaveId"];

            //    if (waveId > 0)
            //    {
            //        foreach (GridDataItem item in RadGridWave.Items)
            //        {
            //            if (item.GetDataKeyValue("WaveId").ToString() == waveId.ToString())
            //            {
            //                RadGridWave.SelectedIndexes.Add(item.ItemIndex);
            //                break;
            //            }
            //        }

            //        if (RadGridWave.SelectedIndexes.Count == 0)
            //        {
            //            GridViewLinked.SelectedIndexes.Clear();
            //            GridViewUnlinked.SelectedIndexes.Clear();
            //            GridViewLoadDetails.SelectedIndexes.Clear();

            //            Session["WaveId"] = null;

            //            GridViewLinked.DataBind();
            //        }
            //    }
            //}
        }

        catch { }
    }
    #endregion Page_LoadComplete

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";

        try
        {
            RadGridWave.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDocumentSearch_Click"

    #region RadGridWave_SelectedIndexChanged
    protected void RadGridWave_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridWave_SelectedIndexChanged";

        try
        {
            Session["WaveId"] = -1;

            foreach (GridDataItem item in RadGridWave.Items)
            {
                if (item.Selected)
                    Session["WaveId"] = (int)item.GetDataKeyValue("WaveId");
            }

            GridViewLinked.DataBind();
            GridViewLoadDetails.DataBind();
            
            Master.MsgText = "Shipment Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "RadGridWave_SelectedIndexChanged"

    #region ButtonUnlinkedSearch_Click
    protected void ButtonUnlinkedSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonUnlinkedSearch_Click";

        try
        {
            GridViewUnlinked.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString()); Master.ErrorText = result;
        }

    }
    #endregion "ButtonUnlinkedSearch_Click"

    //#region GridViewLinked_SelectedIndexChanged
    //protected void GridViewLinked_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    theErrMethod = "GridViewLinked_SelectedIndexChanged";

    //    try
    //    {
    //        Master.MsgText = "";
    //        Master.ErrorText = "";

    //        Wave ds = new Wave();

    //        foreach (GridDataItem item in GridViewLinked.Items)
    //        {
    //            if (item.Selected)
    //                if (!ds.Remove(Session["ConnectionStringName"].ToString(),
    //                              (int)item.GetDataKeyValue("OutboundShipmentId"),
    //                              (int)item.GetDataKeyValue("IssueId")))
    //                    throw new Exception("There was an error, please try again.");
    //        }

            
    //        {
    //            Master.MsgText = "Removed from Load";
    //            GridViewUnlinked.DataBind();

    //            GridViewLinked.DataBind();
    //            GridViewLoadDetails.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}
    //#endregion "GridViewLinked_SelectedIndexChanged"

    #region GridViewUnlinked_SelectedIndexChanged
    protected void GridViewUnlinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewUnlinked_SelectedIndexChanged";

        //try
        //{
        //    int outboundShipmentId = -1;
        //    OutboundShipment outboundShipment = new OutboundShipment();

        //    if (RadGridWave.Rows.Count == 0)
        //    {
        //        int warehouseId = int.Parse(Session["WarehouseId"].ToString());
        //        if (!outboundShipment.InsertOutboundShipment(Session["ConnectionStringName"].ToString(), warehouseId, DateTime.Today, "Auto insert"))
        //            Master.MsgText = "Please try again";

        //        //Get the newly create OutboundShipmentId
        //        Session["WaveId"] = outboundShipment.outboundShipmentId.ToString();
        //        RadGridWave.DataBind();

        //        if (RadGridWave.Rows.Count > 0)
        //        {
        //            RadGridWave.SelectedIndex = 0;

        //            if (outboundShipment.Transfer(Session["ConnectionStringName"].ToString(), outboundShipmentId, int.Parse(GridViewUnlinked.SelectedDataKey.Value.ToString())))
        //            {
        //                Master.MsgText = "Linked to Load";
        //                GridViewUnlinked.DataBind();
        //                GridViewLinked.DataBind();
        //                GridViewLoadDetails.DataBind();
        //            }
        //            else
        //                Master.MsgText = "Please try again";
        //        }
        //    }
        //    else
        //        if (RadGridWave.SelectedIndex == -1)
        //            Master.MsgText = "Please select or insert a Shipment";
        //        else
        //        {
        //            if (outboundShipment.Transfer(Session["ConnectionStringName"].ToString(), (int)Session["WaveId"], int.Parse(GridViewUnlinked.SelectedDataKey.Value.ToString())))
        //            {
        //                GridViewUnlinked.DataBind();
        //                GridViewLinked.DataBind();
        //                GridViewLoadDetails.DataBind();
        //            }
        //            else
        //                Master.MsgText = "Please try again";
        //        }
        //}
        //catch (Exception ex)
        //{
        //    result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
        //    Master.ErrorText = ex.Message;
        //}
    }
    #endregion "GridViewUnlinked_SelectedIndexChanged"

    #region ButtonAdd_Click
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAdd_Click";

        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            Wave ds = new Wave();

            foreach (GridDataItem item in GridViewUnlinked.Items)
            {
                if (item.Selected)
                    if (!ds.Transfer(Session["ConnectionStringName"].ToString(),
                                     (int)Session["WaveId"],
                                     (int)item.GetDataKeyValue("OutboundShipmentId"),
                                     (int)item.GetDataKeyValue("IssueId")))
                        throw new Exception("There was an error, please try again.");
            }

            Master.MsgText = "Linked to Load";

            GridViewUnlinked.DataBind();
            GridViewLinked.DataBind();
            GridViewLoadDetails.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAdd_Click

    #region ButtonRemove_Click
    protected void ButtonRemove_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRemove_Click";

        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            Wave ds = new Wave();

            foreach (GridDataItem item in GridViewLinked.Items)
            {
                if (item.Selected)
                    if (!ds.Remove(Session["ConnectionStringName"].ToString(),
                                  (int)item.GetDataKeyValue("OutboundShipmentId"),
                                  (int)item.GetDataKeyValue("IssueId")))
                        throw new Exception("There was an error, please try again.");
            }


            {
                Master.MsgText = "Removed from Load";

                GridViewUnlinked.DataBind();
                GridViewLinked.DataBind();
                GridViewLoadDetails.DataBind();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonRemove_Click

    #region RadGridWave_ItemCommand
    protected void RadGridWave_ItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            theErrMethod = "RadGridWave_ItemCommand";

            Master.MsgText = "";
            Master.ErrorText = "";

            Wave w = new Wave();

            if (e.CommandName == RadGrid.SelectCommandName)
            {
                foreach (GridDataItem item in RadGridWave.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["WaveId"] = item.GetDataKeyValue("WaveId");

                        if (!w.ReleaseWave(Session["ConnectionStringName"].ToString(),
                                  (int)Session["WaveId"],
                                  (int)Session["OperatorId"]))
                        throw new Exception("There was an error, please try again.");
                        
                        Master.MsgText = "Wave released";

                        RadGridWave.DataBind();
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
            RadGridWave.DataBind();
        }
    }
    #endregion RadGridWave_ItemCommand

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundShipmentLink", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "OutboundShipmentLink", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}


