using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_BackorderEdit : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        theErrMethod = "Page Load";

        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                Master.MsgText = "Page load successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("Outbound_BackorderEdit" + "_" + ex.Message.ToString()); 
                Master.MsgText = result; Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            GridViewOutboundDocument.DataBind();

            Master.MsgText = "GridView Succussful"; 
            Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_BackorderEdit" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonDeleteLine_Click
    protected void ButtonDeleteLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeleteLine_Click";
        try
        {
            if (GridViewOutboundLine.SelectedIndex != -1)
                GridViewOutboundLine.DeleteRow(GridViewOutboundLine.SelectedIndex);

            Master.MsgText = "Deleted"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_BackorderEdit" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDeleteLine_Click"

    #region GridViewOutboundDocument_OnSelectedIndexChanged
    protected void GridViewOutboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewOutboundDocument.SelectedIndex != -1)
            {
                Session["IssueId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString());
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_BackorderEdit" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "GridViewOutboundDocument_OnSelectedIndexChanged"

    #region ButtonApprove_Click
    protected void ButtonApprove_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonApprove_Click";
        try
        {
            int index = GridViewOutboundDocument.SelectedIndex;

            if (index != -1)
            {
                Backorders back = new Backorders();

                back.BackorderApprove(Session["ConnectionStringName"].ToString(),
                                            int.Parse(GridViewOutboundDocument.DataKeys[index].Values["IssueId"].ToString()),
                                            (int)Session["OperatorId"]);

                GridViewOutboundDocument.DataBind();
                Master.MsgText = "Release"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Backorders" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonApprove_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "OutboundDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                result = SendErrorNow("Outbound_BackorderEdit" + "_" + exMsg.Message.ToString());
                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
