<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="LoadOrderDetails.aspx.cs" Inherits="Outbound_LoadDetails"
    Title="<%$ Resources:Default, LoadOrderDetailsTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, LoadOrderDetailsTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, LoadOrderDetailsAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="Orders">
            <ContentTemplate>
                <asp:Button ID="ButtonReturn" runat="server" Text="<%$ Resources:Default, ButtonReturn %>" OnClick="ButtonReturn_Click" />
                <asp:GridView ID="GridViewShipment" runat="server" AutoGenerateColumns="False"
                    DataKeyNames="OutboundShipmentId" DataSourceID="ObjectDataSourceShipment">
                    <Columns>
                        <asp:BoundField DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ShipmentDate" HeaderText="<%$ Resources:Default, ShipmentDate %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                
                <br />
                
                <asp:GridView ID="GridViewOrderDetails" runat="server" DataKeyNames="OutboundShipmentId,IssueId"
                    AutoGenerateColumns="False" DataSourceID="ObjectDataSourceDropSequence">
                    <Columns>
                        <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, ExternalCompanyCode %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, ExternalCompany %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="DropSequence" HeaderText="<%$ Resources:Default, DropSequence %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="OrderVolume" HeaderText="<%$ Resources:Default, OrderVolume %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="OrderWeight" HeaderText="<%$ Resources:Default, OrderWeight %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                
                <asp:ObjectDataSource ID="ObjectDataSourceShipment" runat="server"
                    TypeName="OutboundShipment"
                    SelectMethod="GetShipmentDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <asp:ObjectDataSource ID="ObjectDataSourceDropSequence" runat="server"
                    TypeName="OutboundShipment"
                    SelectMethod="GetOrderDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Lines">
            <ContentTemplate>
                <asp:GridView ID="GridViewLineDetails" runat="server"
                    AutoGenerateColumns="False" DataSourceID="ObjectDataSourceLineDetails">
                    <Columns>
                        <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="LineNumber" HeaderText="<%$ Resources:Default, LineNumber %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="LineVolume" HeaderText="<%$ Resources:Default, LineVolume %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="LineWeight" HeaderText="<%$ Resources:Default, LineWeight %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                
                <asp:ObjectDataSource ID="ObjectDataSourceLineDetails" runat="server"
                    TypeName="OutboundShipment"
                    SelectMethod="GetOrderLineDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>