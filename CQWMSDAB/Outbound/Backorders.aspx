<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="Backorders.aspx.cs" Inherits="Outbound_Backorders" Title="<%$ Resources:Default, BackordersTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundShipmentSearch"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, BackordersTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, BackordersAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Orders%>">
            <ContentTemplate>
                <uc1:OutboundShipmentSearch ID="OutboundShipmentSearch1" runat="server"></uc1:OutboundShipmentSearch>
                <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>">
                </asp:Button>
                <asp:Button ID="ButtonBackorder" runat="server" Text="<%$ Resources:Default, ButtonBackorder %>"
                    OnClick="ButtonBackorder_Click" />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOrders">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOrders" runat="server" DataSourceID="ObjectDataSourceOrders"
                            AllowPaging="True" AllowSorting="True" DataKeyNames="OutboundShipmentId,IssueId,OrderNumber"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewOrders_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="CustomerCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="Customer"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                    SortExpression="NumberOfLines"></asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="DeliveryDate"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                    SortExpression="OutboundDocumentType"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceRouting"
                                            DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoute" runat="server" Text='<%# Bind("Route") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonBackorder" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceOrders" runat="server" TypeName="Backorders"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="ParameterShipmentId"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Lines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelOrderLine">
                    <ContentTemplate>
                        <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
                        <asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        <asp:Label ID="LabelYellow2" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
                        <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
                        <asp:GridView ID="GridViewOrderLines" runat="server" DataSourceID="ObjectDataSourceOrderLines"
                            DataKeyNames="IssueLineId,StorageUnitId" AutoGenerateColumns="False" AllowPaging="True"
                            AllowSorting="True">
                            <Columns>
                                <asp:CommandField></asp:CommandField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                    SortExpression="OrderQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PickedQuantity" HeaderText="<%$ Resources:Default, PickedQuantity %>"
                                    SortExpression="PickedQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                    SortExpression="AvailablePercentage"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                    SortExpression="AvailableQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>"
                                    SortExpression="LocationsAllocated"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="Backorders"
                            SelectMethod="GetOrderLines">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="outboundShipmentId" Type="Int32" DefaultValue="-1" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
