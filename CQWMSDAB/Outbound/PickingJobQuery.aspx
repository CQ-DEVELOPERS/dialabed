<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PickingJobQuery.aspx.cs" Inherits="Outbound_PickingJobQuery" Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:Label ID="LabelOperatorName" runat="server" Text="<%$ Resources:Default, Operator %>"></asp:Label>
    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
        DataTextField="Operator" DataValueField="OperatorId">
    </asp:DropDownList>
    <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>"></asp:Label>
    <asp:TextBox ID="TextBoxReferenceNumber" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search %>" />
    <br />
    <br />
    <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked %>"></asp:Label>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewJob" runat="server" AutoGenerateColumns="false" DataKeyNames="JobId"
                DataSourceID="ObjectDataSourceJob" AllowPaging="true" PageSize="20">
                <Columns>
                    <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                    <asp:BoundField ReadOnly="true" DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Operator %>">
                        <EditItemTemplate>
                            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                                DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelOperator" runat="server" Text='<%# Bind("Operator") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField ReadOnly="true" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="StartDate" HeaderText="<%$ Resources:Default, StartDate %>" />
                    <asp:BoundField ReadOnly="true" DataField="Duration" HeaderText="Duration" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "Indicator", "../images/Indicators/{0}.gif") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <asp:Button ID="ButtonReset" runat="server" Text="<%$ Resources:Default, ResetStatus%>"
        OnClick="ButtonReset_Click" style="Width:auto;"/>
    <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonReset" runat="server" TargetControlID="ButtonReset"
        ConfirmText="<%$ Resources:Default, PressOKtoReset%>">
    </ajaxToolkit:ConfirmButtonExtender>
    <asp:Button ID="ButtonManualLocations" runat="server" Text="<%$ Resources:Default, AlternateLocation%>"
        OnClick="ButtonManualLocations_Click" style="Width:auto;"/>
    <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonManualLocations" runat="server" TargetControlID="ButtonManualLocations"
        ConfirmText="<%$ Resources:Default, PressOKtoChgLoc%>">
    </ajaxToolkit:ConfirmButtonExtender>
    <asp:Button ID="ButtonNoStock" runat="server" Text="<%$ Resources:Default, SetNoStock%>"
        OnClick="ButtonNoStock_Click" style="Width:auto;"/>
    <ajaxToolkit:ConfirmButtonExtender ID="cbeNoStock" runat="server" TargetControlID="ButtonNoStock"
        ConfirmText="<%$ Resources:Default, PressOKMarkNoStock%>">
    </ajaxToolkit:ConfirmButtonExtender>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewInstruction" runat="server" AutoGenerateColumns="false"
                DataKeyNames="InstructionId" DataSourceID="ObjectDataSourceInstruction" AllowPaging="true">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="StartDate" HeaderText="<%$ Resources:Default, StartDate %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderText="<%$ Resources:Default, EndDate %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="GridViewJob" EventName="SelectedIndexChanged">
            </asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonReset" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonManualLocations" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonNoStock" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSourceJob" runat="server" TypeName="Query" SelectMethod="QueryPickingJob"
        UpdateMethod="QueryPickingJobUpdate">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:ControlParameter ControlID="DropDownListOperator" Name="OperatorId" ConvertEmptyStringToNull="true"
                DefaultValue="-1" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="TextBoxReferenceNumber" Name="ReferenceNumber" ConvertEmptyStringToNull="true"
                DefaultValue="-1" PropertyName="Text" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:Parameter Name="JobId" Type="Int32" />
            <asp:Parameter Name="OperatorId" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="Query"
        SelectMethod="QueryPickingLines">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:ControlParameter ControlID="GridViewJob" Name="JobId" ConvertEmptyStringToNull="true"
                DefaultValue="-1" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
