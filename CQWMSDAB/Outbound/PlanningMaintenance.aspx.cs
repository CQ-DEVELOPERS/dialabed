using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_PlanningMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;
                ButtonAutoAllocation.Visible = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 33);

                Configuration config = new Configuration();

                int config44 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 44);
                int config45 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 45);
                int config46 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 46);
                int config47 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 47);

                LabelRed.Text = "Overdue";
                LabelOrange.Text = "Required in " + config45.ToString() + " hours";
                LabelYellow.Text = "Required in " + config46.ToString() + " hours";
                LabelStandard.Text = "Required > " + config47.ToString() + " hours";
            }

            if (Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (Session["IssueId"] != null)
            {
                int issueId = (int)Session["IssueId"];

                foreach (GridViewRow row in GridViewInstruction.Rows)
                {
                    theErrMethod = row.ToString();
                    if (GridViewInstruction.DataKeys[row.RowIndex].Values["IssueId"].ToString() == issueId.ToString())
                    {
                        GridViewInstruction.SelectedIndex = row.RowIndex;
                        issueId = -1;
                        break;
                    }
                }

                if (issueId != -1)
                {
                    GridViewInstruction.SelectedIndex = -1;
                    GridViewJobs.SelectedIndex = -1;
                    Session["OutboundShipmentId"] = null;
                    Session["IssueId"] = null;
                    Session["Jobid"] = null;

                    GridViewLineUpdate.DataBind();
                    GridViewJobs.DataBind();
                    GridViewDetails.DataBind();
                }
            }
        }
        catch { }
    }

    #region ButtonOperatorRelease_Click
    protected void ButtonOperatorRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonOperatorRelease_Click";
        try
        {
            int operatorId = -1;
            int index = GridViewInstruction.SelectedIndex;
            bool skip = false;

            CheckBox cb = new CheckBox();
            Status status = new Status();

            if (int.TryParse(DropDownListOperator.SelectedValue, out operatorId))
            {
                foreach (GridViewRow row in GridViewInstruction.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");

                    if (cb.Checked)
                    {
                        skip = true;

                        status.SetReleaseOperator(Session["ConnectionStringName"].ToString(),
                                                    int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["OutboundShipmentId"].ToString()),
                                                    int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["IssueId"].ToString()),
                                                    operatorId);
                    }
                }

                if (index != -1 && skip == false)
                {
                    status.SetReleaseOperator(Session["ConnectionStringName"].ToString(),
                                                    int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                                    int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString()),
                                                    operatorId);
                }

                GridViewInstruction.DataBind();
                Master.MsgText = "Release"; Master.ErrorText = "";
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonOperatorRelease_Click"

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewInstruction.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";

        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();
            GridViewDetails.DataBind();

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            if (GridViewInstruction.SelectedIndex != -1)
            {
                OutboundWIP plan = new OutboundWIP();

                if (!plan.AutoLocationAllocateIssue(Session["ConnectionStringName"].ToString(), (int)GridViewInstruction.SelectedDataKey.Values["IssueId"]))
                {
                    Master.ErrorText = "Auto Allocation Not Complete";
                    return;
                }

                GridViewDetails.DataBind();

                Master.MsgText = ""; Master.ErrorText = "";
            }
            else
                Master.ErrorText = "Please select an Order.";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations_Click

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    rowList.Add(GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndex:

    #region ButtonPalletise_Click
    protected void ButtonPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletise_Click";

        try
        {
            if (GridViewInstruction.SelectedIndex != -1)
            {
                OutboundWIP plan = new OutboundWIP();

                if (plan.Palletise(Session["ConnectionStringName"].ToString(), int.Parse(GridViewInstruction.DataKeys[GridViewInstruction.SelectedIndex].Values["IssueId"].ToString()), -1))
                {
                    GridViewLineUpdate.DataBind();
                    GridViewDetails.DataBind();

                    Master.MsgText = "Palletised"; Master.ErrorText = "";
                }
                else
                {
                    Master.ErrorText = "Palletisation Failed";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPalletise_Click

    #region ButtonManualLocation_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonManualLocation_Click

    #region ButtonLink_Click
    protected void ButtonLink_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLink_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";
            Response.Redirect("~/Common/RegisterSerialNumberPallet.aspx");

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLink_Click

    #region GridViewInstruction_RowDataBound
    protected void GridViewInstruction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                switch (_row.Row["Status"].ToString())
                {
                    case "Pause":
                        e.Row.Cells[16].ForeColor = System.Drawing.Color.OrangeRed;
                        e.Row.Cells[16].Font.Bold = true;
                        break;
                    case "Planning Complete":
                        e.Row.Cells[16].ForeColor = System.Drawing.Color.RoyalBlue;
                        e.Row.Cells[16].Font.Bold = true;
                        break;
                    case "Release":
                        e.Row.Cells[16].ForeColor = System.Drawing.Color.Green;
                        e.Row.Cells[16].Font.Bold = true;
                        break;
                    case "Manual":
                        e.Row.Cells[16].ForeColor = System.Drawing.Color.LightSteelBlue;
                        e.Row.Cells[16].Font.Bold = true;
                        break;
                    case "Checking":
                        e.Row.Cells[16].ForeColor = System.Drawing.Color.SaddleBrown;
                        e.Row.Cells[16].Font.Bold = true;
                        break;
                    case "Quality Assurance":
                        e.Row.Cells[16].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[16].Font.Bold = true;
                        break;
                }
            }
        }
    }
    #endregion GridViewInstruction_RowDataBound

    #region GridViewInstruction_SelectedIndexChanged
    protected void GridViewInstruction_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetKey();
    }
    #endregion GridViewInstruction_SelectedIndexChanged

    #region GetKey
    protected void GetKey()
    {
        theErrMethod = "GridViewInstruction_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewInstruction.SelectedDataKey["OutboundShipmentId"];
            Session["IssueId"] = GridViewInstruction.SelectedDataKey["IssueId"];
            ObjectDataSourceOrderLines.DataBind();

            GridViewJobs.SelectedIndex = -1;
            Session["Jobid"] = null;
            GridViewDetails.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GetKey

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";
        try
        {
            e.InputParameters["storageUnitId"] = int.Parse(GridViewLineUpdate.SelectedDataKey["StorageUnitId"].ToString());

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceBatch_OnSelecting"

    #region ObjectDataSourceOrderLines_Selecting
    protected void ObjectDataSourceOrderLines_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceOrderLines_Selecting";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceOrderLines_Selecting

    #region GridViewLineUpdate_SelectedIndexChanging
    protected void GridViewLineUpdate_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        theErrMethod = "GridViewLineUpdate_SelectedIndexChanging";

        try
        {
            Session["StorageUnitId"] = GridViewLineUpdate.DataKeys[1].Value;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewLineUpdate_SelectedIndexChanging

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "DropDownListBatch_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["StorageUnitId"] = int.Parse(dpList.SelectedValue);

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ButtonDeallocate_Click
    protected void ButtonDeallocate_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeallocate_Click";
        try
        {
            int index = GridViewInstruction.SelectedIndex;
            int outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString());
            int issueId = int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString());
            bool skip = false;

            CheckBox cb = new CheckBox();
            OutboundWIP wip = new OutboundWIP();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    skip = true;

                    if (outboundShipmentId != -1)
                        issueId = -1;

                    if (wip.PalletiseDeallocate(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId))
                    {
                        Master.MsgText = "Reverse Successful"; Master.ErrorText = "";
                    }
                    else
                    {
                        Master.MsgText = ""; Master.ErrorText = "Document cannot be reversed";

                        GridViewInstruction.SelectedIndex = row.RowIndex;
                        GetKey();
                        break;
                    }
                }
            }

            if (index != -1 && skip == false)
            {
                if (outboundShipmentId != -1)
                    issueId = -1;

                if (wip.PalletiseDeallocate(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId))
                {
                    Master.MsgText = "Reverse Successful"; Master.ErrorText = "";
                }
                else
                {
                    Master.MsgText = ""; Master.ErrorText = "Document cannot be reversed";
                }
            }

            GridViewInstruction.SelectedIndex = -1;
            GridViewInstruction.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonDeallocate_Click

    #region ButtonAutoLocations2_Click
    protected void ButtonAutoLocations2_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            CheckBox cb = new CheckBox();
            OutboundWIP plan = new OutboundWIP();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                if (cb.Checked == true)
                    if (plan.AutoLocationAllocateInstruction(Session["ConnectionStringName"].ToString(), (int)GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]) == false)
                    {
                        Master.ErrorText = "Auto Allocation Not Complete";
                        break;
                    }
            }

            GridViewDetails.DataBind();

            Master.MsgText = "Auto Locations"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations2_Click

    #region ButtonMix_Click
    protected void ButtonMix_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonMix_Click";
        try
        {
            Session["InstructionTypeCode"] = "PM";
            Response.Redirect("~/Common/MixedJobCreate.aspx");
            Master.MsgText = "Mix"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonMix_Click

    #region ButtonRelease_Click
    protected void ButtonRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRelease_Click";
        try
        {
            StatusChange("RL");

            GridViewInstruction.DataBind();

            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonRelease_Click"

    #region ButtonManual_Click
    protected void ButtonManual_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManual_Click";
        try
        {
            StatusChange("M");

            GridViewInstruction.DataBind();

            Master.MsgText = "Status Manual"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonManual_Click"

    #region ButtonReleaseNoStock_Click
    protected void ButtonReleaseNoStock_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReleaseNoStock_Click";
        try
        {
            int index = GridViewInstruction.SelectedIndex;
            bool skip = false;
            int outboundShipmentId = -1;
            int issueId = -1;
            int operatorId = (int)Session["OperatorId"];

            CheckBox cb = new CheckBox();
            OutboundWIP wip = new OutboundWIP();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["OutboundShipmentId"].ToString());
                    issueId = int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["IssueId"].ToString());

                    skip = true;

                    if (outboundShipmentId != -1)
                        issueId = -1;

                    if (wip.ReleaseNoStock(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId, operatorId))
                    {
                        Master.MsgText = "Re-release of no-stock's successful"; Master.ErrorText = "";
                    }
                    else
                    {
                        Master.MsgText = ""; Master.ErrorText = "Re-release of no-stock's not complete";

                        GridViewInstruction.SelectedIndex = row.RowIndex;
                        GetKey();
                        break;
                    }
                }
            }

            if (index != -1 && skip == false)
            {
                outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString());
                issueId = int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString());
                operatorId = (int)Session["OperatorId"];

                if (outboundShipmentId != -1)
                    issueId = -1;

                if (wip.ReleaseNoStock(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId, operatorId))
                {
                    Master.MsgText = "Re-release of no-stock's successful"; Master.ErrorText = "";
                }
                else
                {
                    Master.MsgText = ""; Master.ErrorText = "Re-release of no-stock's not complete";
                }
            }

            GridViewJobs.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonReleaseNoStock_Click"

    #region StatusChange
    protected void StatusChange(string statusCode)
    {
        theErrMethod = "StatusChange";
        try
        {
            int index = GridViewInstruction.SelectedIndex;
            bool skip = false;

            CheckBox cb = new CheckBox();
            Status status = new Status();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    skip = true;

                    if (GridViewInstruction.DataKeys[row.RowIndex].Values["IssueId"].ToString() == "-1")
                        status.SetRelease(Session["ConnectionStringName"].ToString(),
                                          int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["OutboundShipmentId"].ToString()),
                                          -1,
                                          -1,
                                          statusCode);
                    else
                        status.SetRelease(Session["ConnectionStringName"].ToString(),
                                          int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["OutboundShipmentId"].ToString()),
                                          int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["IssueId"].ToString()),
                                          -1,
                                          statusCode);
                }
            }

            if (index != -1 && skip == false)
            {
                if (GridViewInstruction.DataKeys[index].Values["IssueId"].ToString() == "-1")
                    status.SetRelease(Session["ConnectionStringName"].ToString(),
                                      int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                      -1,
                                      -1,
                                      statusCode);
                else
                    status.SetRelease(Session["ConnectionStringName"].ToString(),
                                      int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                      int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString()),
                                      -1,
                                      statusCode);
            }

            Master.MsgText = "Status Change"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadRelease" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "StatusChange"

    #region ButtonPause_Click
    protected void ButtonPause_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPause_Click";
        try
        {
            StatusChange("PS");

            GridViewInstruction.DataBind();

            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPause_Click"

    #region ButtonPrintJob_Click
    protected void ButtonPrintJob_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintJob_Click";
        try
        {
            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";

            Session["ReportName"] = "Job Pick Sheet";

            ReportParameter[] RptParameters = new ReportParameter[4];

            // Create the JobId report parameter
            string jobId = Session["JobId"].ToString();
            RptParameters[0] = new ReportParameter("JobId", jobId);

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPrintJob_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            if (Tabs.TabIndex == 0)
            {
                foreach (GridViewRow row in GridViewInstruction.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }
            }
            else
            {
                foreach (GridViewRow row in GridViewJobs.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }
            }
            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    rowList.Add(GridViewJobs.DataKeys[row.RowIndex].Values["JobId"]);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Session["LabelName"] = "Despatch By Route Label.lbl";

            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }

    private void Nl_onSessionUpdate(string arg1, object arg2)
    {
        Session[arg1] = arg2;
    }
    #endregion "ButtonPrint_Click"

    #region ButtonPrintLabel_Click
    protected void ButtonPrintLabel_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    checkedList.Add((int)GridViewJobs.DataKeys[row.RowIndex].Values["JobId"]);
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                Session["LabelName"] = "Picking Label Small.lbl";
            else
                Session["LabelName"] = "Picking Label Large.lbl";

            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";

            Session["LabelCopies"] = int.Parse(TextBoxQty.Text);

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            var sessionPort = Session["Port"];
            var sessionPrinter = Session["Printer"];
            var sesionIndicatorList = Session["indicatorList"];
            var sessionCheckedList = Session["checkedList"];
            var sessionLocationList = Session["locationList"];
            var sessionConnectionString = Session["ConnectionStringName"];
            var sessionTitle = Session["Title"];
            var sessionBarcode = Session["Barcode"];
            var sessionWarehouseId = Session["WarehouseId"];
            var sessionProductList = Session["ProductList"];
            var sessionTakeOnLabel = Session["TakeOnLabel"];
            var sessionLabelCopies = Session["LabelCopies"];

            NLWAXPrint nl = new NLWAXPrint();
            nl.onSessionUpdate += Nl_onSessionUpdate;
            string printResponse = nl.CreatePrintFile("/DialaBed/",
                                                      Session["LabelName"].ToString(),
                                                      sessionPrinter,
                                                      sessionPort,
                                                      sesionIndicatorList,
                                                      sessionCheckedList,
                                                      sessionLocationList,
                                                      sessionConnectionString,
                                                      sessionTitle,
                                                      sessionBarcode,
                                                      sessionWarehouseId,
                                                      sessionProductList,
                                                      sessionTakeOnLabel,
                                                      sessionLabelCopies);
            if (printResponse == string.Empty)
            {
                Master.ErrorText = "An error has occurred! For more details please check automation manager.";
            }
            else
            {
                Master.MsgText = "Label has been printed successfuly.";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = "An error has occurred! For more details please check automation manager.";
        }
    }
    #endregion ButtonPrintLabel_Click

    #region ButtonPalletWeight
    protected void ButtonPalletWeight_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletWeight";

        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    checkedList.Add((int)GridViewJobs.DataKeys[row.RowIndex].Values["JobId"]);
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";

            Response.Redirect("~/Inbound/PalletWeight.aspx");

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPalletWeight"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}