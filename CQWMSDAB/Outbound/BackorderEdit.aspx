<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="BackorderEdit.aspx.cs" Inherits="Outbound_BackorderEdit" Title="<%$ Resources:Default, BackorderEditTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="uc4" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="uc3" %>
<%@ Register Src="../Common/CustomerSearch.ascx" TagName="ExternalCompanySearch" TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, BackorderEditTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, BackorderEditAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
                <div style="clear: left;">
                </div>
                <asp:Button ID="ButtonApprove" runat="server" Text="<%$ Resources:Default, ButtonApprove %>" OnClick="ButtonApprove_Click" />
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundDocument" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundDocument" runat="server" DataSourceID="ObjectDataSourceOutboundDocument"
                            AllowPaging="true" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewOutboundDocument_OnSelectedIndexChanged"
                            DataKeyNames="IssueId">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" ShowDeleteButton="true" ShowCancelButton="true" />
                                <asp:BoundField DataField="ExternalCompanyCode" SortExpression="ExternalCompanyCode"
                                    HeaderText="<%$ Resources:Default, CustomerCode %>"></asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" SortExpression="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="NumberOfLines" SortExpression="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" SortExpression="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="<%$ Resources:Default, Status %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="OutboundDocumentType" SortExpression="OutboundDocumentType"
                                    HeaderText="<%$ Resources:Default, OutboundDocumentType %>"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" runat="server" TypeName="Backorders"
                            SelectMethod="SearchOutboundDocument" DeleteMethod="DeleteBackorderDocument">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
                                <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="IssueId" Type="Int32"></asp:Parameter>
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Lines%>
">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <asp:Button ID="ButtonDeleteLine" runat="server" Text="<%$ Resources:Default, Delete%>" OnClick="ButtonDeleteLine_Click" />
                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDeleteLine" runat="server" ConfirmText="<%$ Resources:Default, PressOkDelete%>"  TargetControlID="ButtonDeleteLine"></ajaxToolkit:ConfirmButtonExtender>
                        <asp:Label ID="LabelErrorMsg" runat="server" Text="" EnableTheming="false" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="GridViewOutboundLine" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                            AutoGenerateSelectButton="true" AutoGenerateEditButton="true" DataKeyNames="IssueLineId"
                            DataSourceID="ObjectDataSourceOutboundLine">
                            <Columns>
                                <asp:BoundField DataField="ProductCode" ReadOnly="true" HeaderText="<%$ Resources:Default, ProductCode %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" ReadOnly="true" HeaderText="<%$ Resources:Default, Product %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" ReadOnly="true" HeaderText="<%$ Resources:Default, SKUCode %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" ReadOnly="true" HeaderText="<%$ Resources:Default, Batch %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" ReadOnly="true" HeaderText="<%$ Resources:Default, Status %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceOutboundLine" runat="server" TypeName="Backorders"
                            SelectMethod="SearchOutboundLine" UpdateMethod="UpdateOutboundLine" InsertMethod="CreateOutboundLine"
                            DeleteMethod="DeleteOutboundLine">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="IssueId" SessionField="IssueId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="IssueLineId" Type="Int32"></asp:Parameter>
                                <asp:Parameter Name="Quantity" Type="Decimal"></asp:Parameter>
                            </UpdateParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="IssueLineId" Type="Int32"></asp:Parameter>
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

