<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="CheckingStatus.aspx.cs" Inherits="Outbound_CheckingStatus"
    Title="<%$ Resources:Default, PlanningMaintenanceTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PlanningMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PlanningMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Orders%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxOrderNumber" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListLocation" runat="server" DataTextField="<%$ Resources:Default, Location %>"
                                DataValueField="LocationId" DataSourceID="ObjectDataSourceLocation" 
                                Width="150px">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                                SelectMethod="GetLocationsByAreaCode">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="String" />
                                    <asp:Parameter Name="AreaCode" DefaultValue="D" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelJobId" runat="server" Text="<%$ Resources:Default, JobId %>"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxJobId" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxReferenceNumber" runat="server"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"
                                Style="width: auto;" />
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInstruction" runat="server" 
                            DataSourceID="ObjectDataSourceInstruction" DataKeyNames="JobId"
                            AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true" 
                            PageSize="30" onrowdatabound="GridViewInstruction_RowDataBound">
                            <Columns>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="OrderStatus" HeaderText="<%$ Resources:Default, OrderStatus %>"
                                    SortExpression="OrderStatus">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="Parent" HeaderText="<%$ Resources:Default, Parent %>"
                                    SortExpression="Parent">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="ParentStatus" HeaderText="<%$ Resources:Default, ParentStatus %>"
                                    SortExpression="ParentStatus">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>"
                                    SortExpression="JobId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>"
                                    SortExpression="ReferenceNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="JobStatus" HeaderText="<%$ Resources:Default, JobStatus %>"
                                    SortExpression="JobStatus">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="Picker" HeaderText="<%$ Resources:Default, Picker %>"
                                    SortExpression="Picker">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="CheckedBy" HeaderText="<%$ Resources:Default, CheckedBy %>"
                                    SortExpression="CheckedBy">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>"
                                    SortExpression="StoreLocation">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="StartDate" HeaderText="<%$ Resources:Default, StartDate %>"
                                    SortExpression="StartDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="EndDate" HeaderText="<%$ Resources:Default, EndDate %>"
                                    SortExpression="EndDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:BoundField ReadOnly="True" DataField="DwellTime" HeaderText="<%$ Resources:Default, DwellTime %>"
                                    SortExpression="DwellTime">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewInstruction" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="ProductCheck"
                    SelectMethod="CheckingStatusSearch" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:ControlParameter Name="OrderNumber" ControlID="TextBoxOrderNumber" Type="String" />
                        <asp:ControlParameter Name="JobId" ControlID="TextBoxJobId" Type="Int32" />
                        <asp:ControlParameter Name="ReferenceNumber" ControlID="TextBoxReferenceNumber" Type="String" />
                        <asp:ControlParameter Name="StoreLocationId" ControlID="DropDownListLocation" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
