using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_OutboundPOLink : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        theErrMethod = "Page Load";

        if (!Page.IsPostBack)
        {
            try
            {
                Session["ExternalCompanyId"] = null;

                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                string pageType = Request.QueryString["pageType"];

                if (pageType == "" || pageType == null)
                    pageType = "Edit";

                if (pageType == "Insert")
                {
                    Session["OutboundDocumentId"] = null;
                    Title = "Outbound Document Insert";
                    LabelHeading.Text = "Outbound Document Insert";
                    TabPanel1.Enabled = false;

                    DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.Insert);
                }

                string fromDate = DateRange.GetFromDate().ToString();
                string toDate = DateRange.GetToDate().ToString();

                MaskedEditExtenderFromDate.CultureName = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                MaskedEditExtenderToDate.CultureName = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                if (System.Threading.Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2) == "en")
                {
                    MaskedEditExtenderFromDate.Mask = "9999/99/99";
                    MaskedEditExtenderToDate.Mask = "9999/99/99";
                }
                else
                {
                    MaskedEditExtenderFromDate.Mask = "99/99/9999";
                    MaskedEditExtenderToDate.Mask = "99/99/9999";
                };

                if (Session["FromDate"] != null)
                    fromDate = Session["FromDate"].ToString();
                else
                    Session["FromDate"] = Convert.ToDateTime(fromDate);

                if (Session["ToDate"] != null)
                    toDate = Session["ToDate"].ToString();
                else
                    Session["ToDate"] = Convert.ToDateTime(toDate);

                txtFromDate.Text = fromDate;
                txtToDate.Text = toDate;

                Master.MsgText = "Page load successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
                Master.MsgText = result; Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            GridViewOutboundDocument.DataBind();

            Master.MsgText = "GridView Succussful";
            Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonPOSearch_Click
    protected void ButtonPOSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPOSearch_Click";

        try
        {
            GridViewInboundDocument.DataBind();

            Master.MsgText = "GridView Succussful";
            Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonPOSearch_Click"

    #region ButtonInsertLine_Click
    protected void ButtonInsertLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInsertLine_Click";

        try
        {
            if (Session["OutboundDocumentId"] == null)
            {
                Master.ErrorText = "Please select a Document.";
                LabelErrorMsg.Text = "Please select a Document.";
                return;
            }

            foreach (GridViewRow row in GridViewLinkedProducts.Rows)
            {
                CheckBox CheckBoxLinkProduct = new CheckBox();
                TextBox TextBoxQuantity = new TextBox();
                CheckBoxLinkProduct = (CheckBox)row.Cells[0].FindControl("CheckBoxLinkProduct");
                TextBoxQuantity = (TextBox)row.Cells[7].FindControl("TextBoxRequiredQuantity");


                if (CheckBoxLinkProduct.Checked)
                {
                    int requiredQuantity;
                    int AvailableQuantity;

                    if (int.TryParse(TextBoxQuantity.Text, out requiredQuantity))
                    {
                        AvailableQuantity = int.Parse(row.Cells[6].Text);
                        if (AvailableQuantity < requiredQuantity)
                        {
                            Master.ErrorText = "Not enough stock.";
                            LabelErrorMsg.Text = "Not enough stock.";

                            return;
                        }

                        OutboundDocument outboundDocument = new OutboundDocument();

                        Session["StorageUnitId"] = GridViewLinkedProducts.DataKeys[row.RowIndex].Values[0];
                        Session["BatchId"] = GridViewLinkedProducts.DataKeys[row.RowIndex].Values[1];

                        if (Session["BatchId"] == null)
                            Session["BatchId"] = -1;

                        if (outboundDocument.CreateOutboundLine(Session["ConnectionStringName"].ToString(),
                                                int.Parse(Session["OperatorId"].ToString()),
                                                int.Parse(Session["OutboundDocumentId"].ToString()),
                                                int.Parse(Session["StorageUnitId"].ToString()),
                                                requiredQuantity,
                                                int.Parse(Session["BatchId"].ToString())))
                        {
                            TextBoxQuantity.Text = "";
                            CheckBoxLinkProduct.Checked = false;
                            Session["StorageUnitId"] = null;
                            Session["BatchId"] = null;
                            Master.ErrorText = "";
                            LabelErrorMsg.Text = "";

                            GridViewLinkedProducts.DataBind();
                            GridViewOutboundLine.DataBind();

                            Master.MsgText = "InsertLine";
                            Master.ErrorText = "";
                        }
                        else
                        {
                            Master.ErrorText = "May not enter duplicate Product /  Batch Combination";
                            LabelErrorMsg.Text = "May not enter duplicate Product /  Batch Combination";
                        }
                    }
                }
            }
            /*
             if (Session["StorageUnitId"] == null || (int)Session["StorageUnitId"] == -1)
             {
                 Master.ErrorText = "Please select a Product.";
                 LabelErrorMsg.Text = "Please select a Product.";
                 return;
             }

             if (TextBoxQuantity.Text == "" || TextBoxQuantity.Text == "0")
             {
                 Master.ErrorText = "Please enter a Quantity.";
                 LabelErrorMsg.Text = "Please enter a Quantity.";
                 return;
             }

             int quantity = int.Parse(TextBoxQuantity.Text);

            

            

             * */

        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonInsertLine_Click"

    #region ButtonInsertAllLines_Click
    protected void ButtonInsertAllLines_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInsertAllLines_Click";

        try
        {
            if (Session["OutboundDocumentId"] == null)
            {
                Master.ErrorText = "Please select a Document.";
                LabelErrorMsg.Text = "Please select a Document.";
                return;
            }

            OutboundDocument outboundDocument = new OutboundDocument();

            if (outboundDocument.CreateOutboundLine(Session["ConnectionStringName"].ToString(),
                                                int.Parse(Session["OperatorId"].ToString()),
                                                int.Parse(Session["OutboundDocumentId"].ToString()),
                                                int.Parse(Session["InboundDocumentId"].ToString())))
            {
                Master.ErrorText = "";
                LabelErrorMsg.Text = "";

                GridViewLinkedProducts.DataBind();
                GridViewOutboundLine.DataBind();

                Master.MsgText = "InsertLine(s)";
                Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion ButtonInsertAllLines_Click

    #region ButtonDeleteLine_Click
    protected void ButtonDeleteLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeleteLine_Click";
        try
        {
            if (GridViewOutboundLine.SelectedIndex != -1)
                GridViewOutboundLine.DeleteRow(GridViewOutboundLine.SelectedIndex);

            Master.MsgText = "Deleted"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDeleteLine_Click"

    protected void ddlInboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["InboundDocumentTypeId"] = int.Parse(ddlInboundDocumentType.SelectedValue.ToString());
    }

    #region GridViewOutboundDocument_OnSelectedIndexChanged
    protected void GridViewOutboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewOutboundDocument.SelectedIndex != -1)
            {
                Session["OutboundDocumentId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundDocumentId"].ToString());
                Session["ExternalCompanyId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["ExternalCompanyId"].ToString());
            }

            DetailsViewOutboundDocument.DataBind();

            DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.ReadOnly);

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "GridViewOutboundDocument_OnSelectedIndexChanged"

    #region GridViewInboundDocument_OnSelectedIndexChanged
    protected void GridViewInboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewInboundDocument.SelectedIndex != -1)
            {
                Session["InboundDocumentId"] = int.Parse(GridViewInboundDocument.SelectedDataKey["InboundDocumentId"].ToString());
                Session["ExternalCompanyId"] = int.Parse(GridViewInboundDocument.SelectedDataKey["ExternalCompanyId"].ToString());
            }

            GridViewLinkedProducts.DataBind();

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "GridViewInboundDocument_OnSelectedIndexChanged"

    #region DetailsViewOutboundDocument_OnModeChanging
    protected void DetailsViewOutboundDocument_OnModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        theErrMethod = "DetailsViewOutboundDocument_OnModeChanging";
        try
        {
            if (e.NewMode == DetailsViewMode.Insert)
                Session["ExternalCompanyId"] = null;

            Master.MsgText = "Document Changed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion DetailsViewOutboundDocument_OnModeChanging

    #region DetailsViewOutboundDocument_OnItemInserting
    protected void DetailsViewOutboundDocument_OnItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        theErrMethod = "DetailsViewOutboundDocument_OnItemInserting";
        try
        {
            if (Session["ExternalCompanyId"] == null || int.Parse(Session["ExternalCompanyId"].ToString()) == -1)
            {
                Master.ErrorText = "Please select a Company first.";
                LabelErrorTab2.Text = "Please select a Company first.";
                e.Cancel = true;
            }
            else
                LabelErrorTab2.Text = "";

            Master.MsgText = "Item Insert"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion #region DetailsViewOutboundDocument_OnItemInserting

    #region ObjectDataSourceOutboundDocumentUpdate_OnInserted
    protected void ObjectDataSourceOutboundDocumentUpdate_OnInserted(object source, ObjectDataSourceStatusEventArgs e)
    {
        theErrMethod = "ObjectDataSourceOutboundDocumentUpdate_OnInserted";
        try
        {
            if (e.ReturnValue.ToString() != "0")
            {
                Session["OutboundDocumentId"] = e.ReturnValue;
                DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.ReadOnly);
                DetailsViewOutboundDocument.DataBind();
            }
            else
            {
                ButtonChangeMode.Visible = true;
                LabelErrorTab2.Text = "Duplicate Order Number and Customer Combination.";
            }
            Master.MsgText = "Updated"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceOutboundDocumentUpdate_OnInserted"

    #region ButtonChangeMode_Click
    protected void ButtonChangeMode_Click(object sender, EventArgs e)
    {
        DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.Insert);
        ButtonChangeMode.Visible = false;
        LabelErrorTab2.Text = "";
    }
    #endregion ButtonChangeMode_Click

    #region ButtonQuickRelease_Click
    protected void ButtonQuickRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonQuickRelease_Click";

        try
        {
            int outboundDocumentId = -1;

            if (!int.TryParse(DetailsViewOutboundDocument.SelectedValue.ToString(), out outboundDocumentId))
            {
                outboundDocumentId = -1;
            }

            if (outboundDocumentId != -1)
            {
                Planning plan = new Planning();

                if (plan.Palletise(Session["ConnectionStringName"].ToString(), outboundDocumentId))
                {
                    Master.MsgText = "Released"; Master.ErrorText = "";
                    DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.Insert);
                }
                else
                {
                    Master.ErrorText = "Release Failed";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonQuickRelease_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "OutboundDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + exMsg.Message.ToString());
                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling


    #region DetailsViewOutboundDocument_OnDataBound
    protected void DetailsViewOutboundDocument_OnDataBound(object sender, EventArgs e)
    {
        try
        {
            string returnType = "RET";

            if (Session["ReturnType"] != null)
                returnType = Session["ReturnType"].ToString();

            switch (DetailsViewOutboundDocument.CurrentMode.ToString())
            {
                case "ReadOnly":
                    break;
                case "Insert":
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 259))
                    {
                        ((TextBox)(DetailsViewOutboundDocument.FindControl("TextBoxOrderNumberInsert"))).Enabled = false;
                        ((RequiredFieldValidator)(DetailsViewOutboundDocument.FindControl("REQOrderNumberInsert"))).Enabled = false;

                    }

                    break;
                case "Edit":
                    break;
            }

            if (DetailsViewOutboundDocument.Rows.Count == 0)
                ButtonChangeMode.Visible = true; //DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.Insert);
        }
        catch { }
    }
    #endregion DetailsViewOutboundDocument_OnDataBound

    #region CheckBoxProductSelectAll_CheckedChanged
    protected void CheckBoxProductSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            foreach (GridViewRow row in GridViewLinkedProducts.Rows)
            {
                ((CheckBox)row.Cells[0].FindControl("CheckBoxLinkProduct")).Checked = true;
            }
        }
        else
        {
            foreach (GridViewRow row in GridViewLinkedProducts.Rows)
            {
                ((CheckBox)row.Cells[0].FindControl("CheckBoxLinkProduct")).Checked = false;
            }
        }
    }
    #endregion CheckBoxProductSelectAll_CheckedChanged
}
