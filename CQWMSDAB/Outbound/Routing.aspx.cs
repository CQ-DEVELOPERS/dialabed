using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_Routing : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            if (!Page.IsPostBack)
            {
                Configuration config = new Configuration();

                int config44 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 44);
                int config45 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 45);
                int config46 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 46);
                int config47 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 47);

                LabelRed.Text = "Overdue";
                LabelOrange.Text = "Required in " + config45.ToString() + " hours";
                LabelYellow.Text = "Required in " + config46.ToString() + " hours";
                LabelStandard.Text = "Required > " + config47.ToString() + " hours";

                if (Request.QueryString["ActiveTab"] != null)
                {
                    Tabs.ActiveTabIndex = int.Parse(Request.QueryString["ActiveTab"]);
                }
                else
                {
                    if (Request.QueryString["OutboundShipmentId"] != null)
                    {
                        OutboundSearch1.Visible = false;
                        ButtonSearch.Visible = false;
                    }
                    else
                    {
                        Session["OutboundShipmentId"] = null;
                        Session["IssueId"] = null;
                    }
                }

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewAvailable.DataBind();
            GridViewPending.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region ButtonAddToRoute_Click
    protected void ButtonAddToRoute_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAddToRoute_Click";

        try
        {
            CheckBox cb = new CheckBox();
            Routing route = new Routing();

            Master.MsgText = ""; Master.ErrorText = "";

            foreach (GridViewRow row in GridViewAvailable.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    if (route.AddToRoute(Session["ConnectionStringName"].ToString(), int.Parse(GridViewAvailable.DataKeys[row.RowIndex].Values["IssueId"].ToString())))
                        Master.ErrorText = "";
                    else
                        Master.ErrorText = "Add to Route Failed";
            }

            GridViewAvailable.SelectedIndex = -1;
            GridViewPending.SelectedIndex = -1;

            GridViewAvailable.DataBind();
            GridViewAvailableLines.DataBind();
            GridViewPending.DataBind();
            GridViewPendingLines.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAddToRoute_Click

    #region ButtonRemoveFromRoute_Click
    protected void ButtonRemoveFromRoute_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRemoveFromRoute_Click";

        try
        {
            CheckBox cb = new CheckBox();
            Routing route = new Routing();

            Master.MsgText = ""; Master.ErrorText = "";

            foreach (GridViewRow row in GridViewPending.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    if (route.RemoveFromRoute(Session["ConnectionStringName"].ToString(), int.Parse(GridViewPending.DataKeys[row.RowIndex].Values["IssueId"].ToString())))
                        Master.ErrorText = "";
                    else
                        Master.ErrorText = "Add to Route Failed";
            }

            GridViewAvailable.SelectedIndex = -1;
            GridViewPending.SelectedIndex = -1;

            GridViewAvailable.DataBind();
            GridViewAvailableLines.DataBind();
            GridViewPending.DataBind();
            GridViewPendingLines.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonRemoveFromRoute_Click

    #region ButtonSend_Click
    protected void ButtonSend_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSend_Click";

        try
        {
            Routing route = new Routing();
            Master.MsgText = ""; Master.ErrorText = "";

            if (route.SendToRoutingSystem(Session["ConnectionStringName"].ToString()))
            {
                Master.MsgText = "Sent to routing system";

                GridViewAvailable.SelectedIndex = -1;
                GridViewPending.SelectedIndex = -1;

                GridViewAvailable.DataBind();
                GridViewAvailableLines.DataBind();
                GridViewPending.DataBind();
                GridViewPendingLines.DataBind();
            }
            else
                Master.ErrorText = "Error sending to routing system";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSend_Click

    #region GridViewAvailable_SelectedIndexChanged
    protected void GridViewAvailable_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewAvailable_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewAvailable.SelectedDataKey["OutboundShipmentId"].ToString();
            Session["IssueId"] = GridViewAvailable.SelectedDataKey["IssueId"];
            
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GridViewAvailable_SelectedIndexChanged

    #region GridViewPending_SelectedIndexChanged
    protected void GridViewPending_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewAvailable_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewPending.SelectedDataKey["OutboundShipmentId"].ToString();
            Session["IssueId"] = GridViewPending.SelectedDataKey["IssueId"];
            
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GridViewPending_SelectedIndexChanged

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "DropDownListBatch_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["StorageUnitId"] = int.Parse(dpList.SelectedValue);

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ButtonSelectAvailable_Click
    protected void ButtonSelectAvailable_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectAvailable_Click";

        try
        {
            GridViewCommon gvc = new GridViewCommon();

            GridViewAvailable = gvc.SelectAllCheckBox(GridViewAvailable, "CheckBoxEdit");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelectAvailable_Click

    #region ButtonSelectPending_Click
    protected void ButtonSelectPending_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectPending_Click";

        try
        {
            GridViewCommon gvc = new GridViewCommon();

            GridViewPending = gvc.SelectAllCheckBox(GridViewPending, "CheckBoxEdit");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelectPending_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundRouting", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundRouting", theErrMethod, ex);

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}