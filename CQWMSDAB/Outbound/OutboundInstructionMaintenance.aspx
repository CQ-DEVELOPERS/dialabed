<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OutboundInstructionMaintenance.aspx.cs" Inherits="Outbound_OutboundInstructionMaintenance"
    Title="<%$ Resources:Default, OutboundInstructionMaintenanceTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundInstructionMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundInstructionMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <div style="float:left;">
        <uc1:OutboundSearch id="OutboundSearch1" runat="server">
        </uc1:OutboundSearch>
    </div>
    <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonDocumentLineSearch_Click" />
    <div style="clear:left;"></div>
    <br />
    <asp:Button ID="ButtonAutoAllocation" runat="server" Text="Auto Allocate" OnClick="ButtonAutoLocations_Click" />
    <asp:Button ID="ButtonMix" runat="server" Text="Create Mixed Job" OnClick="ButtonMix_Click" />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewLineUpdate" runat="server" 
                DataSourceID="ObjectDataSourceOrderLines"               
                AutoGenerateColumns="False" 
                DataKeyNames="IssueLineId" 
                AllowPaging="True"              
                AllowSorting="True">
                <Columns>
                    <asp:CommandField></asp:CommandField>   
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>                 
                    <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                    </asp:BoundField>                    
                    <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, Batch %>">
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>">
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Availability Indicator">
                        <ItemTemplate>
                            <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" 
        runat="server" 
        TypeName="Planning"
        SelectMethod="SearchOrderLines">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
            <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
            <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
            <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
            <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
            <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
        </SelectParameters>                   
    </asp:ObjectDataSource>
</asp:Content>