<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="WavePlanning.aspx.cs" Inherits="Outbound_WavePlanning"
    Title="<%$ Resources:Default, OutboundShipmentLinkTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundShipmentLinkTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundShipmentLinkAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Thumbnail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridWave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridWave" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewLinked" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewLoadDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="GridViewLinked">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridViewLinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="GridViewUnlinked">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridViewUnlinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ButtonDocumentSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridWave" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ButtonUnlinkedSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridViewUnlinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ButtonAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridViewLinked" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewUnlinked" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewLoadDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ButtonRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridViewLinked" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewUnlinked" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewLoadDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Search %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, WavePlanning %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="RadPageView1">
                <asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
                    <table>
                        <tr>
                            <td>
                                <telerik:RadTextBox ID="TextBoxWave" runat="server" Label="<%$ Resources:Default, OutboundShipmentId %>" Width="250px"></telerik:RadTextBox>
                            </td>
                            <td>
                                <asp:Label ID="LabelStatus" runat="server" Text="<%$ Resources:Default, Status%>" Width="100px"></asp:Label>
                                <asp:DropDownList ID="DropDownListStatus" runat="server" DataTextField="Status" DataValueField="StatusId"
                                    DataSourceID="ObjectDataSourceStatus" Width="150px">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
                                    SelectMethod="GetStatus">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:Parameter Name="Type" Type="string" DefaultValue="W" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
                                <telerik:RadDatePicker ID="rdpFromDate" runat="server"></telerik:RadDatePicker>
                            </td>
                            <td>
                                <asp:Label ID="labeToDate" runat="server" Text="<%$ Resources:Default, ToDate %>" Width="100px"></asp:Label>
                                <telerik:RadDatePicker ID="rdpToDate" runat="server"></telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="right">
                                <telerik:RadButton ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search %>" OnClick="ButtonDocumentSearch_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType1" runat="server" TypeName="OutboundDocumentType"
                        SelectMethod="GetOutboundDocumentTypes">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </asp:Panel>
                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                    TargetControlID="PanelSearch" Radius="10" Color="239, 239, 236" BorderColor="64, 64, 64"
                    Enabled="True" />
                <br />
                <telerik:RadGrid ID="RadGridWave" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true"
                    AllowMultiRowSelection="True"
                    OnSelectedIndexChanged="RadGridWave_SelectedIndexChanged" PageSize="30" Height="500px" Skin="Metro"
                    OnItemCommand="RadGridWave_ItemCommand">
                    <MasterTableView DataKeyNames="WaveId" DataSourceID="ObjectDataSourceWave"
                        CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                            <telerik:GridButtonColumn Text="<%$ Resources:Default, Release %>" HeaderText="<%$ Resources:Default, Release %>" CommandName="Select"></telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="Wave" HeaderText="<%$ Resources:Default, Wave %>" SortExpression="Wave" UniqueName="Wave"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate" UniqueName="CreateDate"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" UniqueName="Status"></telerik:GridBoundColumn>
                            <telerik:GridClientDeleteColumn ButtonType="ImageButton"></telerik:GridClientDeleteColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                        ReorderColumnsOnClient="True" AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowColumnResize="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceWave" runat="server" TypeName="Wave"
                    SelectMethod="SearchWave" InsertMethod="InsertWave" UpdateMethod="UpdateWave" DeleteMethod="DeleteWave">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:ControlParameter Name="wave" ControlID="TextBoxWave" Type="String" />
                        <asp:ControlParameter Name="statusId" ControlID="DropDownListStatus" DefaultValue="-1" Type="Int32" PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="fromDate" ControlID="rdpFromDate" Type="DateTime" />
                        <asp:ControlParameter Name="toDate" ControlID="rdpToDate" Type="DateTime" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="waveId" Type="Int32" />
                        <asp:Parameter Name="wave" Type="String" />
                        <asp:Parameter Name="StatusId" Type="Int32" DefaultValue="-1"></asp:Parameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="waveId" Type="Int32" />
                        <asp:Parameter Name="wave" Type="String" />
                        <asp:Parameter Name="StatusId" Type="Int32"></asp:Parameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="waveId" Type="Int32" />
                        <asp:Parameter Name="wave" Type="String" />
                        <asp:Parameter Name="StatusId" Type="Int32"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="AreaCode" DefaultValue="D" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceRoute" runat="server" TypeName="Route"
                    SelectMethod="GetRoutes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceDespatchBay" runat="server" TypeName="Location"
                    SelectMethod="GetDespatchBayByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="AreaCode" DefaultValue="D" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceDriver" runat="server" TypeName="Contact"
                    SelectMethod="GetDriverByContactId">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <%--<asp:Parameter Name="ContactListId" Type="String" />--%>
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="RadPageView2">
                <table border="solid" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Default, LinkedOrders%>"
                                Font-Bold="true"></asp:Label>
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Default, OrdersUnlinked%>"
                                Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <telerik:RadGrid ID="GridViewLoadDetails" runat="server" AutoGenerateColumns="False" Skin="Metro"
                                DataSourceID="ObjectDataSourceLoadDetails" OnSelectedIndexChanged="RadGridWave_SelectedIndexChanged" Height="120px" Width="300px">
                                <MasterTableView DataSourceID="ObjectDataSourceLoadDetails"
                                    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
                                    InsertItemPageIndexAction="ShowItemOnFirstPage">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="LoadWeight" HeaderText="<%$ Resources:Default, LoadWeight %>" SortExpression="LoadWeight" UniqueName="LoadWeight"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LoadVolume" HeaderText="<%$ Resources:Default, LoadVolume %>" SortExpression="LoadVolume" UniqueName="LoadVolume"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NumberOfOrders" HeaderText="<%$ Resources:Default, NumberOfOrders %>" SortExpression="NumberOfOrders" UniqueName="NumberOfOrders"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NumberOfDrops" HeaderText="<%$ Resources:Default, NumberOfDrops %>" SortExpression="NumberOfDrops" UniqueName="NumberOfDrops"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DistinctStorageUnit" HeaderText="<%$ Resources:Default, Lines %>" SortExpression="DistinctStorageUnit" UniqueName="NumDistinctStorageUnitberOfDrops"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                                    AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <Resizing AllowColumnResize="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:ObjectDataSource ID="ObjectDataSourceLoadDetails" runat="server" TypeName="Wave"
                                SelectMethod="GetLoadDetails">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="waveId" SessionField="WaveId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <telerik:RadGrid ID="GridViewLinked" runat="server" AutoGenerateColumns="False" AllowSorting="true" AllowMultiRowSelection="True" Skin="Metro"
                                DataSourceID="ObjectDataSourceLinked" Width="400px">
                                <MasterTableView DataKeyNames="OutboundShipmentId,IssueId" DataSourceID="ObjectDataSourceLinked"
                                    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
                                    InsertItemPageIndexAction="ShowItemOnFirstPage">
                                    <Columns>
                                        <%--<telerik:GridButtonColumn Text="<%$ Resources:Default, Remove %>"></telerik:GridButtonColumn>--%>
                                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                                        <telerik:GridBoundColumn DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber" UniqueName="OrderNumber"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="OrderWeight" HeaderText="<%$ Resources:Default, OrderWeight %>" SortExpression="OrderWeight" UniqueName="OrderWeight"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="OrderVolume" HeaderText="<%$ Resources:Default, OrderVolume %>" SortExpression="OrderVolume" UniqueName="OrderVolume"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NumberOfDrops" HeaderText="<%$ Resources:Default, NumberOfDrops %>" SortExpression="NumberOfDrops" UniqueName="NumberOfDrops"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="false"
                                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                                    AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <Resizing AllowColumnResize="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:ObjectDataSource ID="ObjectDataSourceLinked" runat="server" TypeName="Wave"
                                SelectMethod="GetLinkedIssues">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="waveId" SessionField="waveId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                        <td>
                            <telerik:RadButton ID="ButtonAdd" runat="server" Text="<%$ Resources:Default, ButtonAdd%>" OnClick="ButtonAdd_Click" />
                            <telerik:RadButton ID="ButtonRemove" runat="server" Text="<%$ Resources:Default, ButtonRemove%>" OnClick="ButtonRemove_Click" />
                        </td>
                        <td valign="top">
                            <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                            <asp:RadioButtonList ID="rblShowOrders" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Default, Orders%>" Value="true"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Default, Shipment%>" Value="false" Selected="True"></asp:ListItem>
                            </asp:RadioButtonList>
                            <telerik:RadButton ID="ButtonUnlinkedSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonUnlinkedSearch_Click" />
                            <telerik:RadGrid ID="GridViewUnlinked" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="true" AllowMultiRowSelection="True" Skin="Metro"
                                DataSourceID="ObjectDataSourceUnlinked" OnSelectedIndexChanged="GridViewUnlinked_SelectedIndexChanged">
                                <MasterTableView DataKeyNames="OutboundShipmentId,IssueId" DataSourceID="ObjectDataSourceUnlinked"
                                    CommandItemDisplay="Top" AutoGenerateColumns="false">
                                    <Columns>
                                        <%--<telerik:GridButtonColumn Text="<%$ Resources:Default, Add %>"></telerik:GridButtonColumn>--%>
                                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                                        <%--<telerik:GridImageColumn UniqueName="AvailabilityIndicator" DataImageUrlFields='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>'></telerik:GridImageColumn>--%>
                                        <telerik:GridBoundColumn DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber" UniqueName="OrderNumber"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Route" HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route" UniqueName="Route"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, ExternalCompanyCode %>" SortExpression="ExternalCompanyCode" UniqueName="ExternalCompanyCode"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ExternalCompany" HeaderText="<%$ Resources:Default, ExternalCompany %>" SortExpression="ExternalCompany" UniqueName="ExternalCompany"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" SortExpression="NumberOfLines" UniqueName="NumberOfLines"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="OrderVolume" HeaderText="<%$ Resources:Default, OrderVolume %>" SortExpression="OrderVolume" UniqueName="OrderVolume"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="OrderWeight" HeaderText="<%$ Resources:Default, OrderWeight %>" SortExpression="OrderWeight" UniqueName="OrderWeight"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate" UniqueName="DeliveryDate"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" UniqueName="Status"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" SortExpression="OutboundDocumentType" UniqueName="OutboundDocumentType"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                                    AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <Resizing AllowColumnResize="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:ObjectDataSource ID="ObjectDataSourceUnlinked" runat="server" TypeName="Wave"
                                SelectMethod="GetUnlinkedIssues">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                    <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
                                    <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                    <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                    <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                                    <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                    <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                                    <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                                    <asp:ControlParameter ControlID="rblShowOrders" DefaultValue="0" Name="ShowOrders" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="OutboundDocumentType"
                                SelectMethod="GetOutboundDocumentTypes">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
