<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="DashboardPicking.aspx.cs" Inherits="Outbound_DashboardPicking" Title="<%$ Resources:Default, DefaultOutboundTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultOutboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultOutboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridPickingAchievedGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPickingAchievedGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridPickingAchievedKPIOperator">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPickingAchievedKPIOperator" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridPickingAchievedKPIWarehouse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPickingAchievedKPIWarehouse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <table>
        <tr valign="top">
            <td>
                <telerik:RadButton ID="RadButtonOrderStatus" runat="server" Text="Order Status" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartOrderStatus" runat="server" ChartTitle-TextBlock-Text="Order Status" Skin="DeepBlue"
                    DataGroupColumn="Legend" DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceOrderStatus" SeriesOrientation="Horizontal" Width="500px">
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceOrderStatus" runat="server" TypeName="Dashboard" SelectMethod="OutboundOrderStatusPicking">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <br />
                <telerik:RadButton ID="RadButtonOrderStatusGrid" runat="server" Text="Order Status" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridOrderStatusGrid" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceOrderStatus" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceOrderStatus">
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Orders %>" UniqueName="Units"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Units" HeaderText="Units" UniqueName="Units"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonPickingAchieved" runat="server" Text="Picking Achieved" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartPickingAchieved" runat="server" ChartTitle-TextBlock-Text="Picking Achieved per hour" Skin="DeepBlue"
                     DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourcePickingAchievedGroup" SeriesOrientation="Vertical" Width="500px" Legend-Visible="false">
                    <PlotArea>
                        <XAxis DataLabelsColumn="Legend">
                        </XAxis>
                    </PlotArea>
                    <Series>
                        <telerik:ChartSeries Name="Series 1" Type="Bar" DataYColumn="Value">
                            <Appearance LegendDisplayMode="ItemLabels" ShowLabels="true">
                            </Appearance>
                        </telerik:ChartSeries>
                    </Series>
                </telerik:RadChart>

                <asp:ObjectDataSource ID="ObjectDataSourcePickingAchievedGroup" runat="server" TypeName="Dashboard" SelectMethod="OutboundPickingAchievedGroup">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <br />
                <telerik:RadButton ID="RadButtonPickingAchievedKPIWarehouse" runat="server" Text="Warehouse KPI" Skin="Outlook" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridPickingAchievedKPIWarehouse" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePickingAchieved" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourcePickingAchieved" ShowGroupFooter="true" GroupsDefaultExpanded="false" ShowHeader="false">
                        <GroupHeaderTemplate>
                            <!-- use the AggregatesValues collection to access the totals for each group header -->
                            <telerik:RadTextBox runat="server" ID="LabelKPI" Text='<%# Eval("WarehouseKPI") %>'  Label="Target / Hour" LabelWidth="125px">
                            </telerik:RadTextBox>
                            <telerik:RadTextBox runat="server" ID="LabelValue" Text='<%# (((GridGroupHeaderItem)Container).AggregatesValues["Value"]) %>' Label="Current Performance" LabelWidth="125px"
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["Value"]) != null)%>'>
                            </telerik:RadTextBox>
                        </GroupHeaderTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="Hour" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Units %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total : "></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="WarehouseKPI" HeaderText="<%$ Resources:Default, KPI %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="KPI : "></telerik:GridBoundColumn>
                        </Columns>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="WarehouseKPI" FieldName="WarehouseKPI" />
                                    <telerik:GridGroupByField FieldAlias="Value" FieldName="Value" Aggregate="Sum" />
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldAlias="WarehouseKPI" FieldName="WarehouseKPI" />
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                    </MasterTableView>
                </telerik:RadGrid>
                <br />
                <telerik:RadButton ID="RadButtonPickingAchievedKPIOperator" runat="server" Text="Picker KPI" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridPickingAchievedKPIOperator" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePickingAchieved" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourcePickingAchieved" ShowGroupFooter="true" GroupsDefaultExpanded="false" ShowHeader="false">
                        <GroupHeaderTemplate>
                            <!-- use the AggregatesValues collection to access the totals for each group header -->
                            <telerik:RadTextBox runat="server" ID="LabelKPI" Text='<%# Eval("OperatorKPI") %>'  Label="Units / Hour" LabelWidth="125px"
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["OperatorKPI"]) != null)%>'>
                            </telerik:RadTextBox>
                            <telerik:RadTextBox runat="server" ID="LabelValue" Text='<%# (((GridGroupHeaderItem)Container).AggregatesValues["Value"]) %>' Label="Average / Hour" LabelWidth="125px"
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["Value"]) != null)%>'>
                            </telerik:RadTextBox>
                        </GroupHeaderTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="Hour" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Units %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total : "></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OperatorKPI" HeaderText="<%$ Resources:Default, KPI %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="KPI : "></telerik:GridBoundColumn>
                        </Columns>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="OperatorKPI" FieldName="OperatorKPI" />
                                    <telerik:GridGroupByField FieldAlias="Value" FieldName="Value" Aggregate="Avg" />
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldAlias="OperatorKPI" FieldName="OperatorKPI" />
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                    </MasterTableView>
                </telerik:RadGrid>
                <br />
                <%--<telerik:RadButton ID="RadButtonPickingAchievedKPI" runat="server" Text="Warehouse KPI" Skin="Outlook" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridPickingAchievedKPI" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePickingAchievedKPI" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourcePickingAchievedKPI">
                        <Columns>
                            <telerik:GridBoundColumn DataField="KPI" HeaderText="Target / Hour"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="Achieved"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                
                <asp:ObjectDataSource ID="ObjectDataSourcePickingAchievedKPI" runat="server" TypeName="Dashboard" SelectMethod="OutboundPickingAchievedKPI">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <br />--%>
                <telerik:RadButton ID="RadButtonPickingAchievedGrid" runat="server" Text="Picking Achieved" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridPickingAchievedGrid" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePickingAchieved" Skin="Outlook" EnableTheming="true" Width="500px"
                     OnPreRender="RadGridPickingAchievedGrid_PreRender">
                    <MasterTableView DataSourceID="ObjectDataSourcePickingAchieved" ShowGroupFooter="true" GroupsDefaultExpanded="false">
                        <GroupHeaderTemplate>
                            <!-- use the AggregatesValues collection to access the totals for each group header -->
                            <asp:Label runat="server" ID="LabelOperator" Text='<%# "Operator name: "+Eval("Operator") %>'
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["Operator"]) != null)%>'></asp:Label>
                            <asp:Label runat="server" ID="LabelValue" Text='<%# (((GridGroupHeaderItem)Container).AggregatesValues["Value"]) %>'
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["Value"]) != null)%>'>
                            </asp:Label>
                            <asp:Label runat="server" ID="LabelKPI" Text='<%# Eval("OperatorKPI") %>'
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["OperatorKPI"]) != null)%>'></asp:Label>
                        </GroupHeaderTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="Hour" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Units %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total : "></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OperatorKPI" HeaderText="<%$ Resources:Default, KPI %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="KPI : " Visible="false"></telerik:GridBoundColumn>
                        </Columns>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="Operator" FieldName="Operator" />
                                    <telerik:GridGroupByField FieldAlias="Value" FieldName="Value" Aggregate="Sum" />
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldAlias="Operator" FieldName="Operator" />
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourcePickingAchieved" runat="server" TypeName="Dashboard" SelectMethod="OutboundPickingAchieved">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
</asp:Content>