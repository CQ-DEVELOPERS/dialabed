<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="DespatchConfirmation.aspx.cs" Inherits="Outbound_DespatchConfirmation"
    Title="<%$ Resources:Default, DespatchConfirmationTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DespatchConfirmationTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DespatchConfirmationAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
    <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>"
        OnClick="ButtonSearch_Click" />
    <div style="clear: left;">
    </div>
    <br />
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, PrintDespatchDocuments%>"
        OnClick="ButtonPrint_Click" style="Width:auto;"/>
    <asp:Label ID="LabelDespatchedDate" runat="server" Text="<%$ Resources:Default, DespatchedDate%>:"
        Width="100px"></asp:Label>
    <asp:TextBox ID="TextBoxDespatchedDate" runat="server" Width="150px"></asp:TextBox>
    <asp:Label ID="LabelDespatchedTime" runat="server" Text="<%$ Resources:Default, DespatchedTime%>:"
        Width="100px"></asp:Label>
    <asp:TextBox ID="TextBoxDespatchedTime" runat="server" Width="150px"></asp:TextBox>
    <asp:Button ID="ButtonUpdate" runat="server" Text="<%$ Resources:Default, Update%>"
        OnClick="ButtonUpdate_Click" />
    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="TextBoxDespatchedTime"
        Mask="99:99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" MaskType="Time" AcceptAMPM="True" />
    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtender3"
        ControlToValidate="TextBoxDespatchedTime" IsValidEmpty="False" EmptyValueMessage="Despatched Time is required"
        InvalidValueMessage="Despatched Time is invalid" Display="Dynamic" TooltipMessage="Input a Despatched Time"
        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Animated="true"
        Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxDespatchedDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" AcceptNegative="None"
        DisplayMoney="None" Mask="<%$ Resources:Default,DateMask %>" MaskType="Date" CultureName="<%$ Resources:Default,CultureCode %>" MessageValidatorTip="true"
        TargetControlID="TextBoxDespatchedDate">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1"
        ControlToValidate="TextBoxDespatchedDate" Display="Dynamic" EmptyValueMessage="Despatched Date is required"
        InvalidValueMessage="Despatched Date is invalid" IsValidEmpty="False" TooltipMessage="Input a Despatched Date">
    </ajaxToolkit:MaskedEditValidator>
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundDocument">
        <ContentTemplate>
            <asp:GridView ID="GridViewOutboundDocument" runat="server" DataSourceID="ObjectDataSourceOutboundDocument"
                AllowPaging="true" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewOutboundDocument_OnSelectedIndexChanged"
                DataKeyNames="OutboundShipmentId,IssueId">
                <Columns>
                    <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, DespatchLocation %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" runat="server" TypeName="DespatchConfirmation"
                SelectMethod="SearchPickingDocuments">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                        Type="Int32" />
                    <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                    <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                    <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                        Type="String" />
                    <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                    <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonPrint" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonUpdate" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
