using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using Microsoft.Reporting.WebForms;

public partial class Outbound_PackingSlipOverride : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Init";

        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");


                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("PackingSlipOverride" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewPackingSlip.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PackingSlipOverride" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonSendOverride_Click
    protected void ButtonSendOverride_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSendOverride_Click";

        try
        {
            CheckBox cb = new CheckBox();
            DespatchConfirmation dc = new DespatchConfirmation();
            DateTime despatchedDate = DateTime.Today;

            foreach (GridViewRow row in GridViewPackingSlip.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    if (dc.PackingOverride(Session["ConnectionStringName"].ToString(),
                                         GridViewPackingSlip.DataKeys[row.RowIndex].Values["OrderNumber"].ToString()
                                        ,ddlDeliveryMethods.SelectedValue
                                        ,int.Parse(Session["OperatorId"].ToString())))
                        cb.Checked = false;
                    else
                        Master.ErrorText = "Failed to update row";
            }

            GridViewPackingSlip.DataBind();

            Master.MsgText = "Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PackingSlipOverride" + "_" + ex.Message.ToString());
            Master.MsgText = ex.Message.ToString();
        }
    }
    #endregion "ButtonSendOverride_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "DespatchConfirmation", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "DespatchConfirmation", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
