using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_OrderLineSplit : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully";
                Master.ErrorText = "";

                Configuration config = new Configuration();

                int config44 = config.GetIntValue(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 44);
                int config45 = config.GetIntValue(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 45);
                int config46 = config.GetIntValue(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 46);
                int config47 = config.GetIntValue(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 47);

                LabelRed.Text = "Overdue";
                LabelOrange.Text = "Required in " + config45.ToString() + " hours";
                LabelYellow.Text = "Required in " + config46.ToString() + " hours";
                LabelStandard.Text = "Required > " + config47.ToString() + " hours";

                if (Request.QueryString.Count > 1)
                {
                    string activeTab = Request.QueryString["ActiveTab"].ToString();

                    if (activeTab != null)
                    {
                        if (activeTab == "1")
                        {
                            Tabs.ActiveTabIndex = 1;

                            if (Session["OutboundShipmentId"] != null)
                                TextBoxOutboundShipmentId.Text = Session["OutboundShipmentId"].ToString();
                        }
                    }
                }
                else
                {
                    Session["OutboundShipmentId"] = null;
                    Session["IssueLineId"] = null;
                    Session["JobId"] = null;
                }
                string pageType = Request.QueryString["pageType"];

                if (pageType == "" || pageType == null)
                    pageType = "PLANNING";

                Session["PageType"] = pageType;
            }

            if (Session["PageType"] == null)
                Session["PageType"] = "PLANNING";

            if (Session["PageType"].ToString() == "PLANNING")
                ObjectDataSourceUnlinked.SelectMethod = "GetUnlinkedIssues";

            if (Session["PageType"].ToString() == "DESPATCH")
                ObjectDataSourceUnlinked.SelectMethod = "GetUnlinkedIssuesPicked";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";

        try
        {
            GridViewOutboundShipment.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDocumentSearch_Click"

    #region GridViewOutboundShipment_SelectedIndexChanged
    protected void GridViewOutboundShipment_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundShipment_SelectedIndexChanged";

        try
        {
            Session["OutboundShipmentId"] = GridViewOutboundShipment.DataKeys[GridViewOutboundShipment.SelectedIndex].Values["OutboundShipmentId"];
            GridViewLinked.DataBind();
            GridViewLoadDetails.DataBind();

            Master.MsgText = "Shipment Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "GridViewOutboundShipment_SelectedIndexChanged"

    #region ButtonUnlinkedSearch_Click
    protected void ButtonUnlinkedSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonUnlinkedSearch_Click";

        try
        {
            GridViewUnlinked.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString()); Master.ErrorText = result;
        }

    }
    #endregion "ButtonUnlinkedSearch_Click"

    #region GridViewLinked_SelectedIndexChanged
    protected void GridViewLinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewLinked_SelectedIndexChanged";

        try
        {
            OrderLineSplit ds = new OrderLineSplit();

            if (ds.Remove(Session["ConnectionStringName"].ToString(),
                            Convert.ToInt32(GridViewLinked.SelectedDataKey["IssueLineId"])) == true)
            {
                Master.MsgText = "Removed from Load";
                GridViewUnlinked.DataBind();

                GridViewLinked.DataBind();
                GridViewLoadDetails.DataBind();
            }
            else
                Master.ErrorText = "There was an error, please try again.";

            Master.MsgText = "Search";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "GridViewLinked_SelectedIndexChanged"

    #region GridViewUnlinked_SelectedIndexChanged
    protected void GridViewUnlinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewUnlinked_SelectedIndexChanged";

        try
        {
            int outboundShipmentId = -1;
            OutboundShipment outboundShipment = new OutboundShipment();

            if (GridViewOutboundShipment.Rows.Count == 0)
            {
                int warehouseId = int.Parse(Session["WarehouseId"].ToString());
                if (!outboundShipment.InsertOutboundShipment(Session["ConnectionStringName"].ToString(), warehouseId, DateTime.Today, "Auto insert"))
                    Master.MsgText = "Please try again";

                //Get the newly create OutboundShipmentId
                Session["OutboundShipmentId"] = outboundShipment.outboundShipmentId.ToString();
                GridViewOutboundShipment.DataBind();

                if (GridViewOutboundShipment.Rows.Count > 0)
                {
                    GridViewOutboundShipment.SelectedIndex = 0;

                    if (outboundShipment.Transfer(Session["ConnectionStringName"].ToString(), outboundShipmentId, int.Parse(GridViewUnlinked.SelectedDataKey.Value.ToString())))
                    {
                        Master.MsgText = "Linked to Load";
                        GridViewUnlinked.DataBind();
                        GridViewLinked.DataBind();
                        GridViewLoadDetails.DataBind();
                    }
                    else
                        Master.MsgText = "Please try again";
                }
            }
            else
                if (GridViewOutboundShipment.SelectedIndex == -1)
                    Master.MsgText = "Please select or insert a Shipment";
                else
                {
                    if (outboundShipment.Transfer(Session["ConnectionStringName"].ToString(), (int)Session["OutboundShipmentId"], int.Parse(GridViewUnlinked.SelectedDataKey.Value.ToString())))
                    {
                        GridViewUnlinked.DataBind();
                        GridViewLinked.DataBind();
                        GridViewLoadDetails.DataBind();
                    }
                    else
                        Master.MsgText = "Please try again";
                }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = ex.Message;
        }
    }
    #endregion "GridViewUnlinked_SelectedIndexChanged"

    #region ButtonAdd_Click
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRemove_Click";

        try
        {
            int outboundShipmentId = GetShipment(-1);

            OrderLineSplit ols = new OrderLineSplit();

            CheckBox cb = new CheckBox();

            Master.MsgText = ""; Master.ErrorText = "";

            foreach (GridViewRow row in GridViewUnlinked.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    if (ols.Transfer(Session["ConnectionStringName"].ToString(), int.Parse(GridViewUnlinked.DataKeys[row.RowIndex].Values["IssueLineId"].ToString())))
                        Master.MsgText = "Linked to Load";
                    else
                        Master.ErrorText = "Please try again";
            }

            GridViewUnlinked.DataBind();
            GridViewLinked.DataBind();
            GridViewLoadDetails.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAdd_Click

    #region ButtonRemove_Click
    protected void ButtonRemove_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRemove_Click";

        try
        {
            int outboundShipmentId = GetShipment(-1);
            OrderLineSplit ols = new OrderLineSplit();

            CheckBox cb = new CheckBox();

            Master.MsgText = ""; Master.ErrorText = "";

            foreach (GridViewRow row in GridViewLinked.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    if (ols.Remove(Session["ConnectionStringName"].ToString(), int.Parse(GridViewLinked.DataKeys[row.RowIndex].Values["IssueLineId"].ToString())))
                        Master.MsgText = "Linked to Load";
                    else
                        Master.ErrorText = "Please try again";
            }

            GridViewUnlinked.DataBind();
            GridViewLinked.DataBind();
            GridViewLoadDetails.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundRouting" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonRemove_Click

    #region GetShipment
    protected int GetShipment(int outboundShipmentId)
    {
        OutboundShipment outboundShipment = new OutboundShipment();

        if (GridViewOutboundShipment.Rows.Count == 0)
        {
            int warehouseId = int.Parse(Session["WarehouseId"].ToString());
            if (!outboundShipment.InsertOutboundShipment(Session["ConnectionStringName"].ToString(), warehouseId, DateTime.Today, "Auto insert"))
                Master.MsgText = "Please try again";

            //Get the newly created OutboundShipmentId
            outboundShipmentId = outboundShipment.outboundShipmentId;

            Session["OutboundShipmentId"] = outboundShipmentId; 
            GridViewOutboundShipment.DataBind();

            if (GridViewOutboundShipment.Rows.Count > 0)
            {
                GridViewOutboundShipment.SelectedIndex = 0;
            }
        }
        else
            if (GridViewOutboundShipment.SelectedIndex == -1)
                outboundShipmentId = -1;
            else
            {
                outboundShipmentId = (int)Session["OutboundShipmentId"];
            }

        return outboundShipmentId;
    }
    #endregion GetShipment

    #region ButtonViewDetails_Click
    protected void ButtonViewDetails_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonViewDetails_Click";
        try
        {
            if (Session["OutboundShipmentId"] != null)
            {
                Session["FromURL"] = "~/Outbound/OutboundShipmentLink.aspx?ActiveTab=1";

                //Response.Redirect("LoadOrderDetails.aspx");
                Response.Redirect("LoadPlanning.aspx?OutboundShipmentId=" + Session["OutboundShipmentId"].ToString());
            }

            Master.MsgText = "View Details"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonViewDetails_Click"

    #region ButtonDropSequence_Click
    protected void ButtonDropSequence_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDropSequence_Click";
        try
        {
            if (Session["OutboundShipmentId"] != null)
            {
                if (Session["OutboundShipmentId"].ToString() != "-1")
                {
                    Session["FromURL"] = "~/Outbound/OutboundShipmentLink.aspx?ActiveTab=1";

                    Response.Redirect("DropSequence.aspx");
                }
                else
                    Master.MsgText = "Drop Sequence"; Master.ErrorText = "A load has not been selected";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonDropSequence_Click"

    #region ButtonInsert_Click
    protected void ButtonInsert_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInsert_Click";

        try
        {
            int outboundShipmentId = -1;
            OutboundShipment outboundShipment = new OutboundShipment();

            int warehouseId = int.Parse(Session["WarehouseId"].ToString());
            if (!outboundShipment.InsertOutboundShipment(Session["ConnectionStringName"].ToString(), warehouseId, DateTime.Today, "Auto insert"))
                Master.ErrorText = "There was an error!!!";

            //Get the newly create OutboundShipmentId
            outboundShipmentId = outboundShipment.outboundShipmentId;
            TextBoxOutboundShipmentId.Text = outboundShipmentId.ToString();
            Session["OutboundShipmentId"] = outboundShipmentId;

            GridViewOutboundShipment.DataBind();

            if (GridViewOutboundShipment.Rows.Count > 0)
            {
                GridViewOutboundShipment.SelectedIndex = 0;
            }

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonInsert_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundShipmentLink", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "OutboundShipmentLink", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}


