using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

public partial class Outbound_Scheduler : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["ConnectionStringName"] == null)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return;
        }
    }

    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Init";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Default" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page Load

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridOrders.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region RadGridOrders_SelectedIndexChanged
    protected void RadGridOrders_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridOrders_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridOrders.Items)
            {
                if (item.Selected)
                {
                    Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                    Session["IssueId"] = item.GetDataKeyValue("IssueId");
                    RadGridBay.DataBind();
                    RadGridPlan.DataBind();
                    RadGridResult.DataBind();
                    ObjectDataSourceLocation.DataBind();
                    break;
                }
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrders_SelectedIndexChanged

    #region RadGridBay_SelectedIndexChanged
    protected void RadGridBay_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridBay_SelectedIndexChanged";
        try
        {
            DockSchedule ds = new DockSchedule();

            foreach (GridDataItem item in RadGridBay.Items)
            {
                if (item.Selected)
                {
                    Session["OutboundShipmentId"] = int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString());
                    Session["LocationId"] = int.Parse(item.GetDataKeyValue("LocationId").ToString());

                    if (!ds.AssignBay(Session["ConnectionStringName"].ToString(),
                                     (int)Session["OutboundShipmentId"],
                                     (int)Session["LocationId"]))
                    {
                        throw new Exception("AssignBay Failed.");
                    }
                }
            }

            ObjectDataSourceSingleBay.DataBind();
            RadSchedulerSingleBay.Rebind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Scheduler" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridBay_SelectedIndexChanged

    #region RadGridPlan_SelectedIndexChanged
    protected void RadGridPlan_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridPlan_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridPlan.Items)
            {
                if (item.Selected)
                {
                    Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                    Session["IssueId"] = item.GetDataKeyValue("IssueId");

                    RadGridResult.DataBind();
                    break;
                }
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Scheduler" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridPlan_SelectedIndexChanged

    #region RadGridResult_SelectedIndexChanged
    protected void RadGridResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridResult_SelectedIndexChanged";
        try
        {
            DockSchedule ds = new DockSchedule();

            foreach (GridDataItem item in RadGridResult.Items)
            {
                if (item.Selected)
                {
                    Session["OutboundShipmentId"] = int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString());
                    Session["InstructionTypeId"] = int.Parse(item.GetDataKeyValue("InstructionTypeId").ToString());
                    Session["OperatorGroupId"] = int.Parse(item.GetDataKeyValue("OperatorGroupId").ToString());

                    if (!ds.AssignTeam(Session["ConnectionStringName"].ToString(),
                                     (int)Session["OutboundShipmentId"],
                                     (int)Session["InstructionTypeId"],
                                     (int)Session["OperatorGroupId"]))
                    {
                        throw new Exception("AssignTeam Failed.");
                    }
                }
            }
            RadGridPlan.DataBind();
            RadGridResult.DataBind();
            //RadGridResult.SelectedIndexes.Clear();
            //foreach (GridDataItem item in RadGridResult.Items)
            //{
            //    if (item.GetDataKeyValue("Planned").ToString().ToLower() == "true")
            //    {
            //        RadGridResult.SelectedIndexes.Add(item.ItemIndex);
            //    }
            //}
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Scheduler" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridResult_SelectedIndexChanged

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "Default", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "Default", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
