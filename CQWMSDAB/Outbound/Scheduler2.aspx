<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="Scheduler2.aspx.cs" Inherits="Outbound_Scheduler" Title="<%$ Resources:Default, DefaultOutboundTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultOutboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultOutboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridOrders">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridBay" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridPlan" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridResult" />
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerAllBays" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridBay">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridBay" />
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerSingleBay" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridPlan">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPlan" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridResult" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridResult">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPlan" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridResult" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSchedulerAllBays">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerAllBays" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridPlan" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridResult" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSchedulerSingleBay">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerSingleBay" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridBay" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">  
                <UpdatedControls>  
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerAllBays" /> 
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerSingleBay" /> 
                </UpdatedControls>  
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Shipment%>"></telerik:RadTab>
                <telerik:RadTab Text="Loading Bay"></telerik:RadTab>
                <telerik:RadTab Text="Teams"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                &nbsp;<table>
                    <tr>
                        <td>
                            <uc1:OutboundSearch ID="OutboundSearch1" runat="server">
                            </uc1:OutboundSearch>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours %>"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label ID="LabelPicked" runat="server" Text="<%$ Resources:Default, Picked %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelBlue" runat="server" Text="<%$ Resources:Default, NotAllocatedRoute %>"
                                            Width="110px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageGreen" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelGreen" runat="server" Text="<%$ Resources:Default, NotAllocatedGT1Day %>"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <telerik:RadButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"></telerik:RadButton>
                <br />
                <br />
                <telerik:RadGrid ID="RadGridOrders" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="ObjectDataSourcePlanning" OnSelectedIndexChanged="RadGridOrders_SelectedIndexChanged"
                    PageSize="30" AllowMultiRowSelection="True" ShowGroupPanel="True" Height="650px"
                    AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true">
                    <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                    <MasterTableView DataKeyNames="OutboundShipmentId,IssueId" DataSourceID="ObjectDataSourcePlanning" >
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" 
                                FilterControlAltText="Filter EditCommandColumn column"></telerik:GridEditCommandColumn>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                                UniqueName="TemplateColumn">
                                <ItemTemplate>
                                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                SortExpression="OutboundShipmentId" 
                                FilterControlAltText="Filter OutboundShipmentId column" 
                                UniqueName="OutboundShipmentId">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListLocationId" 
                                ListTextField="Location" ListValueField="LocationId"
                                DataSourceID="ObjectDataSourceLocation" DataField="LocationId" 
                                HeaderText="<%$ Resources:Default, Location %>" 
                                FilterControlAltText="Filter DropDownListLocationId column">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Route" HeaderText="<%$ Resources:Default, Route %>"
                                SortExpression="Route" FilterControlAltText="Filter Route column" 
                                UniqueName="Route">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                SortExpression="OrderNumber" 
                                FilterControlAltText="Filter OrderNumber column" UniqueName="OrderNumber">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                SortExpression="CustomerCode" 
                                FilterControlAltText="Filter CustomerCode column" UniqueName="CustomerCode">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                SortExpression="Customer" FilterControlAltText="Filter Customer column" 
                                UniqueName="Customer">
                                <ItemStyle Wrap="False" Width="100px"></ItemStyle>
                            </telerik:GridBoundColumn>--%>
                            <telerik:GridDropDownColumn UniqueName="DropDownListPriorityId" 
                                ListTextField="Priority" ListValueField="PriorityId"
                                DataSourceID="ObjectDataSourcePriority" DataField="PriorityId" 
                                HeaderText="<%$ Resources:Default, Priority %>" 
                                FilterControlAltText="Filter DropDownListPriorityId column">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="PercentageComplete" HeaderText="<%$ Resources:Default, PercentagePicked %>"
                                SortExpression="PercentageComplete" 
                                FilterControlAltText="Filter PercentageComplete column" 
                                UniqueName="PercentageComplete">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                SortExpression="NumberOfLines" 
                                FilterControlAltText="Filter NumberOfLines column" UniqueName="NumberOfLines">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Units" HeaderText="<%$ Resources:Default, Units %>"
                                SortExpression="Units" FilterControlAltText="Filter Units column" 
                                UniqueName="Units">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ShortPicks" HeaderText="<%$ Resources:Default, ShortPicks %>"
                                SortExpression="ShortPicks" 
                                FilterControlAltText="Filter ShortPicks column" UniqueName="ShortPicks">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Releases" HeaderText="<%$ Resources:Default, Releases %>"
                                SortExpression="Releases" FilterControlAltText="Filter Releases column" 
                                UniqueName="Releases">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                SortExpression="CreateDate" 
                                FilterControlAltText="Filter CreateDate column" UniqueName="CreateDate">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                SortExpression="DeliveryDate" 
                                FilterControlAltText="Filter DeliveryDate column" UniqueName="DeliveryDate">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                SortExpression="Status" FilterControlAltText="Filter Status column" 
                                UniqueName="Status">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                SortExpression="OutboundDocumentType" 
                                FilterControlAltText="Filter OutboundDocumentType column" 
                                UniqueName="OutboundDocumentType">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>--%>
                            <%--<telerik:GridBoundColumn ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>"
                                SortExpression="Rating" FilterControlAltText="Filter Rating column" 
                                UniqueName="Rating">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>--%>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Weight" HeaderText="<%$ Resources:Default, Weight %>"
                                SortExpression="Weight" FilterControlAltText="Filter Weight column" 
                                UniqueName="Weight">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"
                                SortExpression="Remarks" FilterControlAltText="Filter Remarks column" 
                                UniqueName="Remarks">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn ButtonType="PushButton" 
                                FilterControlAltText="Filter EditCommandColumn1 column" 
                                UniqueName="EditCommandColumn1">
                            </EditColumn>
                        </EditFormSettings>
                        <CommandItemTemplate>
                            <asp:Button ID="DownloadPDF" runat="server" Width="100%" CommandName="ExportToPdf"
                            CssClass="pdfButton"></asp:Button>
                            <asp:Image ID="Image1" runat="server" ImageUrl="Images/Backgr.jpg" AlternateText="Sushi Bar"
                            Width="100%"></asp:Image>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                        ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                        AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowColumnResize="True" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="OutboundWIP"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel3">
                <table>
                    <tr>
                        <td valign="top">
                            <telerik:RadGrid ID="RadGridBay" runat="server" 
                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceBay"
                                OnSelectedIndexChanged="RadGridBay_SelectedIndexChanged" Width="500px" Skin="Metro" AllowAutomaticUpdates="true">
                                <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                                <MasterTableView DataKeyNames="OutboundShipmentId,LocationId" DataSourceID="ObjectDataSourceBay" >
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                                        <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                            SortExpression="OutboundShipmentId" 
                                            FilterControlAltText="Filter OutboundShipmentId column" 
                                            UniqueName="OutboundShipmentId">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDropDownColumn UniqueName="DropDownListLocationId" 
                                            ListTextField="Location" ListValueField="LocationId"
                                            DataSourceID="ObjectDataSourceDespatchBays" DataField="LocationId" 
                                            HeaderText="<%$ Resources:Default, Location %>" 
                                            FilterControlAltText="Filter DropDownListLocationId column">
                                        </telerik:GridDropDownColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="LoadingTime" HeaderText="Loading Time"
                                            SortExpression="LoadingTime" 
                                            FilterControlAltText="Filter LoadingTime column" UniqueName="LoadingTime">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="PlannedStart" HeaderText="Planned Start"
                                            SortExpression="PlannedStart" 
                                            FilterControlAltText="Filter PlannedStart column" UniqueName="PlannedStart">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                                    AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <Resizing AllowColumnResize="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:ObjectDataSource ID="ObjectDataSourceBay" runat="server" TypeName="DockSchedule"
                                SelectMethod="OutboundShipmentBay" UpdateMethod="OutboundShipmentBayUpdate">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" />
                                    <asp:Parameter Name="LocationId" Type="Int32" ></asp:Parameter>
                                    <asp:Parameter Name="PlannedStart" Type="DateTime" ></asp:Parameter>
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSourceDespatchBays" runat="server" TypeName="Location"
                                SelectMethod="GetLocationsByAreaCode">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                    <asp:Parameter Name="areaCode" Type="String" DefaultValue="D" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                        <td valign="top">
                            <telerik:RadScheduler ID="RadSchedulerSingleBay" runat="server" DataEndField="PlannedEnd" GroupBy="Location"
                                DataKeyField="DockScheduleId" DataRecurrenceField="RecurrenceRule" 
                                DataRecurrenceParentKeyField="RecurrenceParentID" DataSourceID="ObjectDataSourceDockScheduler"
                                DataDescriptionField="Description" 
                                Height="700px"
                                DataStartField="PlannedStart" DataSubjectField="Subject" Skin="Metro">
                                <AdvancedForm Modal="true" />
                                <TimelineView UserSelectable="false" />
                                <ResourceTypes>
                                    <telerik:ResourceType DataSourceID="ObjectDataSourceSingleBay" ForeignKeyField="LocationId" 
                                        KeyField="LocationId" Name="Location" TextField="Location" />
                                </ResourceTypes>
                                <ResourceStyles>
                                    <telerik:ResourceStyleMapping Type="Location" ApplyCssClass="rsCategoryBlue" Text="DSP1" />
                                    <telerik:ResourceStyleMapping Type="Location" ApplyCssClass="rsCategoryGreen" Text="DSP2" />
                                    <telerik:ResourceStyleMapping Type="Location" ApplyCssClass="rsCategoryGray" Text="DSP3" />
                                    <telerik:ResourceStyleMapping Type="Location" ApplyCssClass="rsCategoryRed" Text="DSP4" />
                                    <telerik:ResourceStyleMapping Type="Location" ApplyCssClass="rsCategoryLime" Text="DSP5" />
                                </ResourceStyles>
                            </telerik:RadScheduler>
                            <asp:ObjectDataSource ID="ObjectDataSourceDockScheduler" runat="server" TypeName="DockSchedule"
                                SelectMethod="DockScheduleSelect" InsertMethod="DockScheduleInsert" UpdateMethod="DockScheduleUpdate" DeleteMethod="DockScheduleDelete">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="LocationId" SessionField="LocationId" Type="Int32" />
                                </SelectParameters>
                                <InsertParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
                                    <asp:Parameter Name="Subject" Type="String" />
                                    <asp:Parameter Name="PlannedStart" Type="DateTime" />
                                    <asp:Parameter Name="PlannedEnd" Type="DateTime" />
                                    <asp:Parameter Name="RecurrenceRule" Type="String" />
                                    <asp:Parameter Name="RecurrenceParentID" Type="Int32" />
                                    <asp:Parameter Name="Description" Type="String" />
                                    <asp:Parameter Name="LocationId" Type="Int32" />
                                    <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
                                    <asp:Parameter Name="Subject" Type="String" />
                                    <asp:Parameter Name="PlannedStart" Type="DateTime" />
                                    <asp:Parameter Name="PlannedEnd" Type="DateTime" />
                                    <asp:Parameter Name="RecurrenceRule" Type="String" />
                                    <asp:Parameter Name="RecurrenceParentID" Type="Int32" />
                                    <asp:Parameter Name="Description" Type="String" />
                                    <asp:Parameter Name="LocationId" Type="Int32" />
                                    <asp:Parameter Name="DockScheduleId" Type="Int32" />
                                </UpdateParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="DockScheduleId" Type="Int32" />
                                </DeleteParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSourceSingleBay" runat="server" TypeName="DockSchedule"
                                SelectMethod="GetLocationsById">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="locationId" SessionField="locationId" Type="Int32" DefaultValue="-1" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel4">
                <table>
                    <tr>
                        <td>
                            <telerik:RadGrid ID="RadGridPlan" runat="server" 
                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlan"
                                OnSelectedIndexChanged="RadGridPlan_SelectedIndexChanged" Width="500px" Skin="Metro">
                                <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                                <MasterTableView DataKeyNames="OutboundShipmentId" DataSourceID="ObjectDataSourcePlan" >
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                            SortExpression="OutboundShipmentId" 
                                            FilterControlAltText="Filter OutboundShipmentId column" 
                                            UniqueName="OutboundShipmentId">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                            SortExpression="InstructionType" 
                                            FilterControlAltText="Filter InstructionType column" UniqueName="InstructionType">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>"
                                            SortExpression="RequiredQuantity" 
                                            FilterControlAltText="Filter RequiredQuantity column" UniqueName="RequiredQuantity" DataFormatString="{0:G0}">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OperatorGroup" HeaderText="<%$ Resources:Default, OperatorGroup %>"
                                            SortExpression="OperatorGroup" 
                                            FilterControlAltText="Filter OperatorGroup column" UniqueName="OperatorGroup">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="TeamStart" HeaderText="<%$ Resources:Default, TeamStart %>"
                                            SortExpression="TeamStart" 
                                            FilterControlAltText="Filter TeamStart column" UniqueName="TeamStart">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="TeamEnd" HeaderText="<%$ Resources:Default, TeamEnd %>"
                                            SortExpression="TeamEnd" 
                                            FilterControlAltText="Filter TeamEnd column" UniqueName="TeamEnd">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="DockTime" HeaderText="<%$ Resources:Default, DockTime %>"
                                            SortExpression="DockTime" 
                                            FilterControlAltText="Filter DockTime column" UniqueName="DockTime">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                                    AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <Resizing AllowColumnResize="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:ObjectDataSource ID="ObjectDataSourcePlan" runat="server" TypeName="DockSchedule"
                                SelectMethod="OutboundShipmentTeamPlan">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                        <td>
                            <telerik:RadGrid ID="RadGridResult" runat="server" 
                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceResult"
                                OnSelectedIndexChanged="RadGridResult_SelectedIndexChanged" Width="900px" Skin="Metro" AllowMultiRowSelection="true">
                                <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                                <MasterTableView DataKeyNames="OutboundShipmentId,InstructionTypeId,OperatorGroupId,Planned" DataSourceID="ObjectDataSourceResult" >
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                            SortExpression="OutboundShipmentId" 
                                            FilterControlAltText="Filter OutboundShipmentId column" 
                                            UniqueName="OutboundShipmentId">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                            SortExpression="InstructionType" 
                                            FilterControlAltText="Filter InstructionType column" UniqueName="InstructionType">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>"
                                            SortExpression="RequiredQuantity" 
                                            FilterControlAltText="Filter RequiredQuantity column" UniqueName="RequiredQuantity" DataFormatString="{0:G0}">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="OperatorGroup" HeaderText="<%$ Resources:Default, OperatorGroup %>"
                                            SortExpression="OperatorGroup" 
                                            FilterControlAltText="Filter OperatorGroup column" UniqueName="OperatorGroup">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="HourlyPickRate" HeaderText="Hourly Pick Rate"
                                            SortExpression="HourlyPickRate" 
                                            FilterControlAltText="Filter HourlyPickRate column" UniqueName="HourlyPickRate" DataFormatString="{0:G0}">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="PlannedStart" HeaderText="Planned Start"
                                            SortExpression="PlannedStart" 
                                            FilterControlAltText="Filter TeamStart column" UniqueName="PlannedStart">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn ReadOnly="True" DataField="RequiredStart" HeaderText="Required Start"
                                            SortExpression="RequiredStart" 
                                            FilterControlAltText="Filter RequiredStart column" UniqueName="RequiredStart">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                                    AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <Resizing AllowColumnResize="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:ObjectDataSource ID="ObjectDataSourceResult" runat="server" TypeName="DockSchedule"
                                SelectMethod="OutboundShipmentTeamResult">
                                <SelectParameters>
                                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="DockSchedule"
        SelectMethod="GetLocationsByShipment">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>