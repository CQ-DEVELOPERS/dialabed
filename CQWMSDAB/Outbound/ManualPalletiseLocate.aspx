<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManualPalletiseLocate.aspx.cs" Inherits="Outbound_ManualPalletiseLocate"
    Title="<%$ Resources:Default, ManualPalletiseLocateTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ManualPalletiseLocateTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ManualPalletiseLocateAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewIssueLine">
        <ContentTemplate>
            <asp:GridView ID="GridViewIssueLine" runat="server" AutoGenerateColumns="False" DataKeyNames="IssueLineId"
                AllowPaging="true">
                <Columns>
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
                    <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" />
                </Columns>
            </asp:GridView>
            <asp:GridView ID="GridViewArea" runat="server" AutoGenerateColumns="False" DataKeyNames="AreaId"
                AllowPaging="true" OnSelectedIndexChanged="GridViewArea_SelectedIndexChanged"
                AutoGenerateSelectButton="true">
                <Columns>
                    <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area %>" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="GridViewLocations" EventName="SelectedIndexChanged">
            </asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanelGridViewLocations" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewLocations" runat="server" AllowPaging="true" DataKeyNames="LocationId,AvailableQuantity"
                DataSourceID="ObjectDataSourceLocations"
                AutoGenerateSelectButton="true" OnSelectedIndexChanged="GridViewLocations_SelectedIndexChanged"
                AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default, ActualQuantity %>" />
                    <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" />
                    <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default, ReservedQuantity %>" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="GridViewArea" EventName="SelectedIndexChanged">
            </asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    
    <asp:ObjectDataSource ID="ObjectDataSourceLocations" runat="server"
        TypeName="ManualPalletiseOutbound"
        SelectMethod="GetLocations">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="IssueLineId" Type="Int32" SessionField="IssueLineId" />
            <asp:ControlParameter Name="AreaId" ControlID="GridViewArea" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
