<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="ProductionPlanning.aspx.cs" Inherits="Outbound_ProductionPlanning" Title="<%$ Resources:Default, ProductionWIPTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ManufacturingOrderSearch.ascx" TagName="ManufacturingOrderSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ProductionPlanTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ProductionPlanAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <script type="text/javascript" language="JavaScript">
        function openNewWindowPriority() {
            window.open("../Outbound/ProductionPlan.aspx", "_blank",
          "height=450px width=800px top=200 left=200 resizable=no scrollbars=no ");
        }
    </script>
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="Orders">
            <ContentTemplate>
                <uc1:ManufacturingOrderSearch ID="ManufacturingOrderSearch1" runat="server"></uc1:ManufacturingOrderSearch>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonPriority" runat="server" Text="<%$ Resources:Default, Priority %>" OnClientClick="javascript:openNewWindowPriority();" Visible="false" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonPalletise" runat="server" Text="<%$ Resources:Default, CreatePicks %>" OnClick="ButtonPalletise_Click" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonDeallocate" runat="server" Text="<%$ Resources:Default, Remove %>" OnClick="ButtonDeallocate_Click" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderDeallocate" runat="server" TargetControlID="ButtonDeallocate" ConfirmText="Press OK to confirm reverse."></ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonPrintBatchCard" runat="server" Text="<%$ Resources:Default, PrintBatchCard %>" OnClick="ButtonPrintBatchCard_Click" Style="width: auto;"/>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Image ID="ImageGreen1" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                <asp:Label ID="LabelGreen1" runat="server" Text="Raw material available in production"></asp:Label>
                <asp:Image ID="ImageYellow1" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow1" runat="server" Text="Raw material available - replenishment pending"></asp:Label>
                <asp:Image ID="ImageOrange1" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                <asp:Label ID="LabelOrange1" runat="server" Text="Raw material available - replenishment required"></asp:Label>
                <asp:Image ID="ImageRed1" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed1" runat="server" Text="Raw material not available"></asp:Label>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInstruction" runat="server" OnRowDataBound="GridViewInstruction_RowDataBound" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="False" 
                            DataSourceID="ObjectDataSourcePlanning" DataKeyNames="OutboundShipmentId,IssueId,Rank" OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged" PageSize="30"
                            OnRowCommand="GridViewInstruction_RowCommand">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/ArrowUp.png" CommandName="Up" DataTextField="Rank" Text="UP" HeaderText="" SortExpression="Rank" />
                                <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/ArrowDown.png" CommandName="Down" DataTextField="Rank" Text="DN" HeaderText="" SortExpression="Rank" />
                                <%--<asp:TemplateField HeaderText="<%$ Resources:Default, Rank %>">
                                    <ItemTemplate>
                                        <asp:TextBox ID="TextBoxRank" runat="server" Text='<%# Bind("Rank") %>' OnTextChanged="TextBoxRank_TextChanged"></asp:TextBox>
                                        <ajaxToolkit:NumericUpDownExtender ID="nudeRank" runat="server" Minimum="1" Maximum="1000" TargetControlID="TextBoxRank" Width="60"></ajaxToolkit:NumericUpDownExtender>
                                    </ItemTemplate>
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>--%>
                                <%--<asp:TemplateField HeaderText="<%$ Resources:Default, Rank %>">
                                    <ItemTemplate>
                                        <asp:TextBox ID="TextBoxRank" runat="server" Text='<%# Bind("Rank") %>' OnTextChanged="TextBoxRank_TextChanged"></asp:TextBox>
                                        <ajaxToolkit:NumericUpDownExtender ID="nudeRank" runat="server" Minimum="1" Maximum="1000" TargetControlID="TextBoxRank" Width="60"></ajaxToolkit:NumericUpDownExtender>
                                    </ItemTemplate>
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>--%>
                                <%--<asp:BoundField ReadOnly="True" DataField="Rank" HeaderText="<%$ Resources:Default, Rank %>" SortExpression="Rank">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>--%>
                                <asp:BoundField ReadOnly="true" DataField="Rank" HeaderText="<%$ Resources:Default, Rank %>" SortExpression="Rank">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>" SortExpression="OutboundShipmentId">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlBatch" runat="server" Text='<%# Bind("Batch") %>'  NavigateUrl='<% #String.Format("~/StaticInfo/BatchManagement.aspx?Batch={0}", Eval("Batch")) %>'></asp:HyperLink>
                                    </ItemTemplate>
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" SortExpression="NumberOfLines">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ManufactureDate %>" SortExpression="DeliveryDate">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxDeliveryDate" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender
                                            ID="CalendarExtenderDeliveryDate"
                                            runat="server"
                                            Animated="true"
                                            Format="<%$ Resources:Default,DateFormat %>"
                                            TargetControlID="TextBoxDeliveryDate">
                                        </ajaxToolkit:CalendarExtender>

                                        <ajaxToolkit:MaskedEditExtender
                                            ID="MaskedEditExtenderDeliveryDate"
                                            runat="server"
                                            Mask="<%$ Resources:Default,DateMask %>"
                                            MaskType="Date"
                                            CultureName="<%$ Resources:Default,CultureCode %>"
                                            MessageValidatorTip="true"
                                            TargetControlID="TextBoxDeliveryDate">
                                        </ajaxToolkit:MaskedEditExtender>

                                        <ajaxToolkit:MaskedEditValidator
                                            ID="MaskedEditValidatorDeliveryDate"
                                            runat="server"
                                            ControlExtender="MaskedEditExtenderDeliveryDate"
                                            ControlToValidate="TextBoxDeliveryDate"
                                            Display="Dynamic"
                                            EmptyValueMessage="Manufacturing Date is required"
                                            InvalidValueMessage="Manufacturing Date is invalid"
                                            IsValidEmpty="False"
                                            TooltipMessage="Input a Manufacturing Date">
                                        </ajaxToolkit:MaskedEditValidator>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="LabelDeliveryDate" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ProductionStagingArea %>" SortExpression="Location">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ProductionLocation %>" SortExpression="Pot">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPot" runat="server" DataSourceID="ObjectDataSourcePot"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("PotId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="True" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblPot" runat="server" Text='<%# Bind("Pot") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="PlannedYield" HeaderText="<%$ Resources:Default, PlannedYield %>" SortExpression="PlannedYield">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="PercentageComplete" HeaderText="<%$ Resources:Default, PercentageComplete %>" SortExpression="PercentageComplete">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPalletise" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDeallocate" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewInstruction" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="ProductionPlan"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="LocationId" SessionField="LocationId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ProductCode" SessionField="ProductCode" Type="String" />
                        <asp:SessionParameter Name="Product" SessionField="Product" Type="String" />
                        <asp:SessionParameter Name="Batch" SessionField="Batch" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="Rank" Type="Int32" />
                        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
                        <asp:Parameter Name="Batch" Type="String" DefaultValue="" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                        <asp:Parameter Name="PotId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location" SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="areaCode" DefaultValue="PRD" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePot" runat="server" TypeName="Location" SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="areaCode" DefaultValue="POT" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Lines">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelOrderLine">
                    <ContentTemplate>
                        <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="LabelGreen2" runat="server" Text="Raw material available in production"></asp:Label>
                        <asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        <asp:Label ID="LabelYellow2" runat="server" Text="Raw material available - replenishment pending"></asp:Label>
                        <asp:Image ID="ImageOrange2" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                        <asp:Label ID="LabelOrange2" runat="server" Text="Raw material available - replenishment required"></asp:Label>
                        <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="LabelRed2" runat="server" Text="Raw material not available"></asp:Label>
                        <asp:Image ID="ImageGrey2" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                        <asp:Label ID="LabelGrey2" runat="server" Text="Finished Goods Product"></asp:Label>
                        <br />
                        <asp:Button ID="ButtonCheckStock" runat="server" Text="<%$ Resources:Default, ButtonCheckStock %>" OnClick="ButtonCheckStock_Click" />
                        <asp:Button ID="ButtonReplenish" runat="server" Text="<%$ Resources:Default, ButtonReplenish %>" OnClick="ButtonReplenish_Click" />
                        <asp:GridView ID="GridViewLineUpdate" runat="server" DataSourceID="ObjectDataSourceOrderLines"
                            DataKeyNames="IssueLineId" AutoGenerateColumns="False" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                </asp:BoundField>
                                <%--<asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListBatch" runat="server" OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged"
                                            DataSourceID="ObjectDataSourceBatch" DataTextField="Batch" DataValueField="StorageUnitBatchId"
                                            SelectedValue='<%# Bind("StorageUnitBatchId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" />
                                <asp:BoundField ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>" SortExpression="RequiredQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" SortExpression="AllocatedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>" SortExpression="AvailableQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="ManufacturingQuantity" HeaderText="<%$ Resources:Default, ManufacturingQuantity %>" SortExpression="ManufacturingQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="RawQuantity" HeaderText="<%$ Resources:Default, RawQuantity %>" SortExpression="RawQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="ReceivingQuantity" HeaderText="<%$ Resources:Default, RawReceivingQuantity %>" SortExpression="ReceivingQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="ReplenishmentQuantity" HeaderText="<%$ Resources:Default, ReplenishmentQuantity %>" SortExpression="ReplenishmentQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="StockOnOrder" HeaderText="<%$ Resources:Default, StockOnOrder %>" SortExpression="StockOnOrder" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>" SortExpression="AvailablePercentage" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                            </Columns>
                        </asp:GridView>
                        <%--<asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                            SelectMethod="GetBatchesByStorageUnit" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:ControlParameter ControlID="GridViewLineUpdate" Name="StorageUnitId" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>--%>
                        <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="ProductionWIP"
                            SelectMethod="GetOrderLines" UpdateMethod="UpdateOrderLine">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" DefaultValue="-1" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" DefaultValue="-1" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Type="Int32" Name="issueLineId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitBatchId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitId"></asp:Parameter>
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="Jobs">
            <ContentTemplate>
                <asp:Button ID="ButtonSelect" runat="server" Text="Select All" OnClick="ButtonSelect_Click"></asp:Button>
                <asp:Button ID="ButtonButtonPrint" OnClick="ButtonPrint_Click" runat="server" Text="<%$ Resources:Default, ButtonLabel %>"/>
                <asp:Button ID="ButtonPrintJob" OnClick="ButtonPrintJob_Click" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"/>
                
                <asp:Label ID="LabelQty" runat="server" Text="Copies:"></asp:Label>
                <asp:TextBox ID="TextBoxQty" runat="server" Text="1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers" TargetControlID="TextBoxQty"></ajaxToolkit:FilteredTextBoxExtender>
                <asp:Button ID="ButtonPrintLabel" runat="server" OnClick="ButtonPrintLabel_Click" Text="Print Label" />
                
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                            DataKeyNames="JobId">
                            <Columns>
                                <asp:TemplateField HeaderText="Label">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField SelectText="Select" ShowSelectButton="True" />
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="ProductionWIP"
                            SelectMethod="SearchPickingJobs">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonButtonPrint" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPrintJob" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="Details">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelDetails" runat="server">
                    <ContentTemplate>
                        <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                        <asp:Label ID="LabelLocationStandard" runat="server" Text="No Location"></asp:Label>
                        <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                        <asp:Label ID="LabelLocationBlue" runat="server" Text="Allocated"></asp:Label>
                        <asp:GridView ID="GridViewDetails" runat="server" 
                            DataSourceID="ObjectDataSourceDetails"
                            AutoGenerateColumns="False" 
                            DataKeyNames="InstructionId" 
                            AllowPaging="True"              
                            AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                </asp:BoundField>                    
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="Quantity">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" SortExpression="ConfirmedQuantity">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>" SortExpression="InstructionType">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>" SortExpression="PickLocation">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" SortExpression="Operator">
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" 
                    runat="server" 
                    TypeName="ProductionWIP"
                    SelectMethod="SearchLinesByJob">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator" SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
