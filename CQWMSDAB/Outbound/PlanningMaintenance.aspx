<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PlanningMaintenance.aspx.cs" Inherits="Outbound_PlanningMaintenance"
    Title="<%$ Resources:Default, PlanningMaintenanceTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PlanningMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PlanningMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Orders%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <uc1:OutboundSearch ID="OutboundSearch1" runat="server">
                            </uc1:OutboundSearch>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours %>"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label ID="LabelPicked" runat="server" Text="<%$ Resources:Default, Picked %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelBlue" runat="server" Text="<%$ Resources:Default, NotAllocatedRoute %>"
                                            Width="110px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageGreen" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelGreen" runat="server" Text="<%$ Resources:Default, NotAllocatedGT1Day %>"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"
                                Style="width: auto;" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonDeallocate" runat="server" Text="<%$ Resources:Default, DeAllocate %>"
                                OnClick="ButtonDeallocate_Click" Style="width: auto;" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderDeallocate" runat="server"
                                TargetControlID="ButtonDeallocate" ConfirmText="<%$ Resources:Default, PressOKtoconfirmreverse %>"
                                Enabled="True">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonRelease" runat="server" Text="<%$ Resources:Default, ButtonRelease %>"
                                OnClick="ButtonRelease_Click" Style="width: auto;" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderRelease" runat="server"
                                TargetControlID="ButtonRelease" ConfirmText="<%$ Resources:Default, PressOKtoconfirmrelease %>"
                                Enabled="True">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonManual" runat="server" Text="<%$ Resources:Default, ButtonManual %>"
                                OnClick="ButtonManual_Click" style="Width:auto;"/>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderManual" runat="server"
                                TargetControlID="ButtonManual" ConfirmText="<%$ Resources:Default, PressOKconfirmmanualrelease %>"
                                Enabled="True">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonPause" runat="server" Text="<%$ Resources:Default, ButtonPause %>"
                                OnClick="ButtonPause_Click" style="Width:auto;"/>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderPause" runat="server"
                                TargetControlID="ButtonPause" ConfirmText="<%$ Resources:Default, PressOKtopauseload %>"
                                Enabled="True">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonAutoAllocation" runat="server" Text="<%$ Resources:Default, ButtonAutoAllocation %>"
                                OnClick="ButtonAutoLocations_Click" style="Width:auto;" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonReleaseNoStock" runat="server" Text="<%$ Resources:Default, ButtonReleaseNoStock %>"
                                OnClick="ButtonReleaseNoStock_Click" Style="width: auto;" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderReleaseNoStock" runat="server"
                                TargetControlID="ButtonReleaseNoStock" ConfirmText="<%$ Resources:Default, PressOKconfirmrerelease %>"
                                Enabled="True">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <!--<td>
                            <asp:LinkButton CssClass="lnkSubmit" ID="ButtonLoadPlanning" runat="server" Text="<%$ Resources:Default, ButtonLoadPlanning %>" Enabled="false" />
                        </td>
                        <td>
                            <asp:LinkButton CssClass="lnkSubmit" ID="ButtonWavePlanning" runat="server" Text="<%$ Resources:Default, ButtonWavePlanning %>" Enabled="false" />
                        </td>-->
                        <td>
                            <asp:Label ID="LabelOperator" runat="server" Text="<%$ Resources:Default,Operator %>"></asp:Label>
                            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                                DataTextField="Operator" DataValueField="OperatorId">
                            </asp:DropDownList>
                            <asp:Button ID="ButtonOperatorRelease" runat="server" Text="<%$ Resources:Default, OperatorRelease %>"
                                OnClick="ButtonOperatorRelease_Click" Style="width: auto;" />
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInstruction" runat="server" OnRowDataBound="GridViewInstruction_RowDataBound"
                            AllowPaging="true" AllowSorting="true" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlanning"
                            DataKeyNames="OutboundShipmentId,IssueId" OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged"
                            PageSize="30">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PrincipalCode" HeaderText="<%$ Resources:Default, Principal %>"
                                    SortExpression="PrincipalCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="CustomerCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="Customer">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="PercentageComplete" HeaderText="<%$ Resources:Default, PercentagePicked %>"
                                    SortExpression="PercentageComplete">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                    SortExpression="NumberOfLines">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Units" HeaderText="<%$ Resources:Default, Units %>"
                                    SortExpression="Units">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ShortPicks" HeaderText="<%$ Resources:Default, ShortPicks %>"
                                    SortExpression="ShortPicks">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Releases" HeaderText="<%$ Resources:Default, Releases %>"
                                    SortExpression="Releases">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="DeliveryDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                    SortExpression="OutboundDocumentType">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <%--<asp:BoundField ReadOnly="True" DataField="Location" HeaderText="<%$ Resources:Default, Location %>"
                                    SortExpression="Location">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>--%>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Route" HeaderText="<%$ Resources:Default, Route %>"
                                    SortExpression="Route">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>"
                                    SortExpression="Rating">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Weight" HeaderText="<%$ Resources:Default, Weight %>"
                                    SortExpression="Weight">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"
                                    SortExpression="Remarks">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDeallocate" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonRelease" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonManual" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPause" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonReleaseNoStock" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonOperatorRelease" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewInstruction" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByIssue">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="OutboundWIP"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Lines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelOrderLine">
                    <ContentTemplate>
                        <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
                        <asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        <asp:Label ID="LabelYellow2" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
                        <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
                        <asp:GridView ID="GridViewLineUpdate" runat="server" DataSourceID="ObjectDataSourceOrderLines"
                            DataKeyNames="IssueLineId,StorageUnitId" AutoGenerateColumns="False" AllowPaging="True"
                            AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListBatch" runat="server" OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged"
                                            DataSourceID="ObjectDataSourceBatch" DataTextField="Batch" DataValueField="StorageUnitBatchId"
                                            SelectedValue='<%# Bind("StorageUnitBatchId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                    SortExpression="OrderQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                    SortExpression="AvailablePercentage"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                    SortExpression="AvailableQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>"
                                    SortExpression="LocationsAllocated"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                            SelectMethod="GetBatchesByStorageUnit" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:ControlParameter ControlID="GridViewLineUpdate" Name="StorageUnitId" PropertyName="SelectedValue"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="OutboundWIP"
                            SelectMethod="GetOrderLines" UpdateMethod="UpdateOrderLine">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId"
                                    DefaultValue="-1" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" DefaultValue="-1" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Type="Int32" Name="issueLineId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitBatchId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitId"></asp:Parameter>
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Jobs%>">
            <ContentTemplate>
                <asp:Button ID="ButtonSelect" OnClick="ButtonSelect_Click" runat="server" Text="<%$ Resources:Default, SelectAll%>" style="Width:auto;" />
                     </asp:Button>
                <asp:Button ID="ButtonButtonPrint" OnClick="ButtonPrint_Click" runat="server" Text="<%$ Resources:Default, ButtonLabel %>" style="Width:auto;"/>
                <asp:Button ID="ButtonPrintJob" OnClick="ButtonPrintJob_Click" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" style="Width:auto;"/>
                <asp:Button ID="ButtonPalletWeight" runat="server" OnClick="ButtonPalletWeight_Click"
                    Text="<%$ Resources:Default, PalletWeight%>" style="Width:auto;"/>
                <asp:Label ID="LabelQty" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
                <asp:TextBox ID="TextBoxQty" runat="server" Text="1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers"
                    TargetControlID="TextBoxQty">
                </ajaxToolkit:FilteredTextBoxExtender>
                <asp:Button ID="ButtonPrintLabel" runat="server" OnClick="ButtonPrintLabel_Click"
                    Text="<%$ Resources:Default, PrintLabel%>" style="Width:auto;"/>
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                            DataKeyNames="JobId">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Label%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="OutboundWIP"
                            SelectMethod="SearchPickingJobs">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonButtonPrint" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPrintJob" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="<%$ Resources:Default, Details%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelDetails" runat="server">
                    <ContentTemplate>
                        <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                        <asp:Label ID="LabelLocationStandard" runat="server" Text="<%$ Resources:Default, NoLocation%>"></asp:Label>
                        <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                        <asp:Label ID="LabelLocationBlue" runat="server" Text="<%$ Resources:Default, Allocated%>"></asp:Label>
                        <asp:GridView ID="GridViewDetails" runat="server" DataSourceID="ObjectDataSourceDetails"
                            AutoGenerateColumns="False" DataKeyNames="InstructionId" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                    SortExpression="Quantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>"
                                    SortExpression="ConfirmedQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                    SortExpression="InstructionType"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>"
                                    SortExpression="PickLocation"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>"
                                    SortExpression="Operator"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="OutboundWIP"
                    SelectMethod="SearchLinesByJob">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
