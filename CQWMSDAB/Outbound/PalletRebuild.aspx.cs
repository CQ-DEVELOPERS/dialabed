using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_PalletRebuild : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully";
                Master.ErrorText = "";
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";

        try
        {
            GridViewOutboundShipment.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDocumentSearch_Click"

    #region GridViewOutboundShipment_SelectedIndexChanged
    protected void GridViewOutboundShipment_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundShipment_SelectedIndexChanged";

        try
        {
            Session["OutboundShipmentId"] = GridViewOutboundShipment.DataKeys[GridViewOutboundShipment.SelectedIndex].Values["OutboundShipmentId"];
            Session["IssueId"] = GridViewOutboundShipment.DataKeys[GridViewOutboundShipment.SelectedIndex].Values["IssueId"];

            ddlJobLinked.DataBind();
            GridViewLinked.DataBind();
            ddlJobsUnlinked.DataBind();
            GridViewUnlinked.DataBind();

            Session["LinkedJobId"] = int.Parse(ddlJobLinked.SelectedValue);
            Session["UnlinkedJobId"] = int.Parse(ddlJobLinked.SelectedValue);
            
            Master.MsgText = "Shipment Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "GridViewOutboundShipment_SelectedIndexChanged"

    #region ButtonUnlinkedSearch_Click
    protected void ButtonUnlinkedSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonUnlinkedSearch_Click";

        try
        {
            GridViewUnlinked.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString()); Master.ErrorText = result;
        }

    }
    #endregion "ButtonUnlinkedSearch_Click"

    #region GridViewLinked_SelectedIndexChanged
    protected void GridViewLinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewLinked_SelectedIndexChanged";

        try
        {
            GridViewUnlinked.SelectedIndex = -1;
            Session["IssueLineId"] = GridViewLinked.SelectedDataKey["IssueLineId"];
            Session["InstructionId"] = GridViewLinked.SelectedDataKey["InstructionId"];
            ButtonTransferLinked.Enabled = true;
            ButtonTransferUnlinked.Enabled = false;
            ButtonInstructionLinked.Enabled = true;
            ButtonInstructionUnlinked.Enabled = false;

            TextBoxQuantity.Text = GridViewLinked.SelectedDataKey["ConfirmedQuantity"].ToString();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewLinked_SelectedIndexChanged"

    #region GridViewUnlinked_SelectedIndexChanged
    protected void GridViewUnlinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewUnlinked_SelectedIndexChanged";

        try
        {
            GridViewLinked.SelectedIndex = -1;
            Session["IssueLineId"] = GridViewUnlinked.SelectedDataKey["IssueLineId"];
            Session["InstructionId"] = GridViewUnlinked.SelectedDataKey["InstructionId"];
            ButtonTransferLinked.Enabled = false;
            ButtonTransferUnlinked.Enabled = true;
            ButtonInstructionLinked.Enabled = false;
            ButtonInstructionUnlinked.Enabled = true;

            TextBoxQuantity.Text = GridViewUnlinked.SelectedDataKey["ConfirmedQuantity"].ToString();

        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewUnlinked_SelectedIndexChanged"

    #region ButtonTransferLinked_Click
    protected void ButtonTransferLinked_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonTransferLinked_Click";

        try
        {
            if (Session["UnlinkedJobId"] != null && Session["IssueLineId"] != null && Session["InstructionId"] != null)
            {
                PalletRebuild pr = new PalletRebuild();

                if (pr.Remove(Session["ConnectionStringName"].ToString(),
                                Convert.ToInt32(Session["UnlinkedJobId"]),
                                Convert.ToInt32(Session["IssueLineId"]),
                                Convert.ToInt32(Session["InstructionId"]),
                                Decimal.Parse(TextBoxQuantity.Text)) == true)
                {
                    Session["IssueLineId"] = null;
                    Session["InstructionId"] = null;

                    Master.MsgText = "Removed from Job";
                    Master.ErrorText = "";

                    GridViewUnlinked.DataBind();
                    GridViewLinked.DataBind();
                }
                else
                {
                    Master.ErrorText = "There was an error, please try again.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonTransferLinked_Click"

    #region ButtonTransferUnlinked_Click
    protected void ButtonTransferUnlinked_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonTransferUnlinked_Click";

        try
        {
            if (Session["LinkedJobId"] != null && Session["IssueLineId"] != null && Session["InstructionId"] != null)
            {
                PalletRebuild pr = new PalletRebuild();

                if (pr.Transfer(Session["ConnectionStringName"].ToString(),
                                Convert.ToInt32(Session["LinkedJobId"]),
                                Convert.ToInt32(Session["IssueLineId"]),
                                Convert.ToInt32(Session["InstructionId"]),
                                Decimal.Parse(TextBoxQuantity.Text)) == true)
                {
                    Session["IssueLineId"] = null;
                    Session["InstructionId"] = null;

                    Master.MsgText = "Removed from Job";
                    Master.ErrorText = "";

                    GridViewUnlinked.DataBind();
                    GridViewLinked.DataBind();
                }
                else
                {
                    Master.ErrorText = "There was an error, please try again.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonTransferUnlinked_Click"

    #region ButtonInstructionLinked_Click
    protected void ButtonInstructionLinked_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInstructionLinked_Click";

        try
        {
            if (Session["UnlinkedJobId"] != null && Session["IssueLineId"] != null && Session["InstructionId"] != null)
            {
                PalletRebuild pr = new PalletRebuild();

                if (pr.CreateInstruction(Session["ConnectionStringName"].ToString(),
                                Convert.ToInt32(Session["LinkedJobId"]),
                                Convert.ToInt32(Session["UnlinkedJobId"]),
                                Convert.ToInt32(Session["IssueLineId"]),
                                Convert.ToInt32(Session["InstructionId"]),
                                Decimal.Parse(TextBoxQuantity.Text),
                                TextBoxComments.Text,
                                (int)Session["OperatorId"]) == true)
                {
                    Session["IssueLineId"] = null;
                    Session["InstructionId"] = null;

                    Master.MsgText = "Removed from Job";
                    Master.ErrorText = "";

                    GridViewUnlinked.DataBind();
                    GridViewLinked.DataBind();
                }
                else
                {
                    Master.ErrorText = "There was an error, please try again.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonInstructionLinked_Click"

    #region ButtonInstructionUnlinked_Click
    protected void ButtonInstructionUnlinked_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInstructionUnlinked_Click";

        try
        {
            if (Session["LinkedJobId"] != null && Session["IssueLineId"] != null && Session["InstructionId"] != null)
            {
                PalletRebuild pr = new PalletRebuild();

                if (pr.Transfer(Session["ConnectionStringName"].ToString(),
                                Convert.ToInt32(Session["LinkedJobId"]),
                                Convert.ToInt32(Session["IssueLineId"]),
                                Convert.ToInt32(Session["InstructionId"]),
                                Decimal.Parse(TextBoxQuantity.Text)) == true)
                {
                    Session["IssueLineId"] = null;
                    Session["InstructionId"] = null;

                    Master.MsgText = "Removed from Job";
                    Master.ErrorText = "";

                    GridViewUnlinked.DataBind();
                    GridViewLinked.DataBind();
                }
                else
                {
                    Master.ErrorText = "There was an error, please try again.";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonInstructionUnlinked_Click"

    #region ddlJobsLinked_OnSelectedIndexChanged
    protected void ddlJobsLinked_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "ddlJobsLinked_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["LinkedJobId"] = int.Parse(dpList.SelectedValue);

            GridViewLinked.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ddlJobsLinked_OnSelectedIndexChanged"

    #region ddlJobsUnlinked_OnSelectedIndexChanged
    protected void ddlJobsUnlinked_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "ddlJobsUnlinked_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["UnlinkedJobId"] = int.Parse(dpList.SelectedValue);

            GridViewUnlinked.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletRebuild" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ddlJobsUnlinked_OnSelectedIndexChanged"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "OutboundDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + exMsg.Message.ToString());
                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}

