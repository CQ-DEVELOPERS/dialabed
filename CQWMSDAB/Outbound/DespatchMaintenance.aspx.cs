using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_DespatchMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"
    
    #region PRIVATE CONSTANTS
        //private const string DEFAULT_STATUS = "IS";
        StringBuilder strBuilder = new StringBuilder();
        private string result = "";
        private string theErrMethod = "";
    #endregion

    #region Page_Load
     protected void Page_Load(object sender, EventArgs e)
     {
         Session["countLoopsToPreventInfinLoop"] = 0;
         theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;

                Configuration config = new Configuration();

                int config44 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 44);
                int config45 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 45);
                int config46 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 46);
                int config47 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 47);

                LabelRed.Text = "Overdue";
                LabelOrange.Text = "Required in " + config45.ToString() + " hours";
                LabelYellow.Text = "Required in " + config46.ToString() + " hours";
                LabelStandard.Text = "Required > " + config47.ToString() + " hours";
            }

            if(Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
     }
    #endregion Page_Load

    #region ButtonSearch_Click
    /// <summary>
     /// 
     /// </summary>
     /// <param name="sender"></param>
     /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
     {
         theErrMethod = "ButtonSearch_Click";
         try
         {
             GridViewInstruction.DataBind();

             Master.MsgText = ""; Master.ErrorText = "";
         }
         catch (Exception ex)
         {
             result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString()); 
             Master.ErrorText = result;
         }
     }
    #endregion ButtonSearch_Click

    #region ButtonReleaseNoStock_Click
    protected void ButtonReleaseNoStock_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReleaseNoStock_Click";
        try
        {
            int index = GridViewInstruction.SelectedIndex;

            if (index != -1)
            {
                OutboundWIP wip = new OutboundWIP();

                int outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString());
                int issueId = int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString());
                int operatorId = (int)Session["OperatorId"];

                if (outboundShipmentId != -1)
                    issueId = -1;

                if (wip.ReleaseNoStock(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId, operatorId))
                {
                    Master.MsgText = "Re-release of no-stock's successful"; Master.ErrorText = "";

                    GridViewJobs.DataBind();
                }
                else
                {
                    Master.MsgText = ""; Master.ErrorText = "Re-release of no-stock's not complete";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonReleaseNoStock_Click"

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            if (Session["OutboundShipmentId"] == null)
                Session["OutboundShipmentId"] = -1;

            Session["FromURL"] = "~/Reports/DespatchReport.aspx";

            Session["ReportName"] = "Despatch Report";

            ReportParameter[] RptParameters = new ReportParameter[5];

            // Create the OutboundShipmentId report parameter
            RptParameters[0] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());
            // Create the IssueId report parameter
            RptParameters[1] = new ReportParameter("IssueId", Session["IssueId"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch (Exception ex)
        {
            result = SendErrorNow("DespatchReportPrint" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";

        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();
            GridViewDetails.DataBind();

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if(cb.Checked)
                    rowList.Add(GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndex:

    #region GridViewInstruction_RowDataBound
    protected void GridViewInstruction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                switch (_row.Row["Status"].ToString())
                {
                    case "Pause":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.OrangeRed;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Planning Complete":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.RoyalBlue;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Release":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.Green;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Manual":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.LightSteelBlue;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Checking":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.SaddleBrown;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Quality Assurance":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                }
            }
        }
    }
    #endregion GridViewInstruction_RowDataBound

    #region GridViewInstruction_RowUpdating
    protected void GridViewInstruction_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        theErrMethod = "GridViewInstruction_RowUpdating";
        try
        {
            for (int i = 0; i <= e.NewValues.Values.Count - 1; i++)
            {
                switch (i)
                {
                       
                    case 0:
                        if (e.NewValues["IssueId"] != null)
                        {
                            e.NewValues["IssueId"] = int.Parse(e.NewValues["IssueId"].ToString());//, System.Globalization.NumberStyles.Any);
                        }
                        break;                   
                   case 1:
                       if (e.NewValues["locationId"] != null)
                       {
                           e.NewValues["locationId"] = int.Parse(e.NewValues["locationId"].ToString());//, System.Globalization.NumberStyles.Any);
                       }
                        
                        break;
                    case 2:
                        if (e.NewValues["priorityId"] != null)
                        {
                            e.NewValues["priorityId"] = int.Parse(e.NewValues["priorityId"].ToString());//, System.Globalization.NumberStyles.Any);
                        }
                       
                        break;
                     case 3:
                         if (e.NewValues["deliveryDate"] != null)
                         {
                             e.NewValues["deliveryDate"] = int.Parse(e.NewValues["deliveryDate"].ToString());//, System.Globalization.NumberStyles.Any);
                         }
                       
                        break;
                    default:
                        break;
                }
            }
          
             //   e.Cancel = true;


            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion GridViewInstruction_RowUpdating

    #region GridViewInstruction_SelectedIndexChanged
    protected void GridViewInstruction_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInstruction_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewInstruction.SelectedDataKey["OutboundShipmentId"];
            Session["IssueId"] = GridViewInstruction.SelectedDataKey["IssueId"];
            ObjectDataSourceOrderLines.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion GridViewInstruction_SelectedIndexChanged

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";
        try
        {
            e.InputParameters["storageUnitId"] = int.Parse(GridViewLineUpdate.SelectedDataKey["StorageUnitId"].ToString());

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceBatch_OnSelecting"
   
    #region ObjectDataSourceOrderLines_Selecting
    protected void ObjectDataSourceOrderLines_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceOrderLines_Selecting";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceOrderLines_Selecting

    #region GridViewLineUpdate_SelectedIndexChanging
    protected void GridViewLineUpdate_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        theErrMethod = "GridViewLineUpdate_SelectedIndexChanging";

        try
        {
            Session["StorageUnitId"] = GridViewLineUpdate.DataKeys[1].Value;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewLineUpdate_SelectedIndexChanging

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "DropDownListBatch_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["StorageUnitId"] = int.Parse(dpList.SelectedValue);

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
 #endregion ErrorHandling
}