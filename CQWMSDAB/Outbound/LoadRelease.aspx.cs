using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <remarks>
/// Remove the Warehouse ID from this page before live
/// </remarks>
public partial class Outbound_LoadRelease : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Load Page";       

        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("Outbound_LoadRelease" + "_" + ex.Message.ToString()); 
                Master.ErrorText = result;
            }

        }
        
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewOutboundDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_LoadRelease" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSearch_Click"

    #region GridViewOutboundDocument_OnSelectedIndexChanged
    protected void GridViewOutboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString() == "")
                Session["OutboundShipmentId"] = -1;
            else
                Session["OutboundShipmentId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString());

            if (GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString() == "")
                Session["IssueId"] = -1;
            else
                Session["IssueId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString());

            //Response.Redirect("LineRelease.aspx");

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadRelease" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewOutboundDocument_OnSelectedIndexChanged"

    #region ButtonRelease_Click
    protected void ButtonRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRelease_Click";
        try
        {
            StatusChange("RL");

            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadRelease" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonRelease_Click"

    #region ButtonManual_Click
    protected void ButtonManual_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManual_Click";
        try
        {
            StatusChange("M");

            Master.MsgText = "Status Manual"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadRelease" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonManual_Click"

    #region StatusChange
    protected void StatusChange(string statusCode)
    {
        theErrMethod = "StatusChange";
        try
        {
            int index = GridViewOutboundDocument.SelectedIndex;
            CheckBox cb = new CheckBox();

            if (index != -1)
            {
                Status status = new Status();

                if (GridViewOutboundDocument.DataKeys[index].Values["IssueId"].ToString() == "")
                    status.SetRelease(Session["ConnectionStringName"].ToString(),
                                      int.Parse(GridViewOutboundDocument.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                      -1,
                                      -1,
                                      statusCode);
                else
                    status.SetRelease(Session["ConnectionStringName"].ToString(),
                                      -1,
                                      int.Parse(GridViewOutboundDocument.DataKeys[index].Values["IssueId"].ToString()),
                                      -1,
                                      statusCode);

                GridViewOutboundDocument.DataBind();
            }

            Master.MsgText = "Status Change"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadRelease" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "StatusChange"
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "LoadRelease", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "LoadRelease", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling



}
