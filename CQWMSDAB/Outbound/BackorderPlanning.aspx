<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BackorderPlanning.aspx.cs" Inherits="Outbound_BackorderPlanning" Title="<%$ Resources:Default, BackordersTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/BackorderSearch.ascx" TagName="BackorderSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, BackordersTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, BackordersAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Lines %>">
            <ContentTemplate>
                <uc1:BackorderSearch ID="BackorderSearch1" runat="server">
                </uc1:BackorderSearch>
                <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>">
                </asp:Button>
                <asp:Button ID="ButtonBackorderLine" runat="server" Text="<%$ Resources:Default, ButtonBackorderLine %>"
                    OnClick="ButtonBackorderLine_Click" />
                <asp:Button ID="ButtonBackorderFull" runat="server" Text="<%$ Resources:Default, ButtonBackorderFull %>"
                    OnClick="ButtonBackorderFull_Click" />
                <asp:Label ID="LabelReserved" runat="server" Text="<%$ Resources:Default, ReservedDocument %>"
                    Width="100px"></asp:Label>
                <asp:DropDownList ID="DropDownListReserved" runat="server" DataSourceID="ObjectDataSourceReserved"
                    DataTextField="OrderNumber" DataValueField="IssueId">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceReserved" runat="server" TypeName="Backorders"
                    SelectMethod="ReservedDocuments">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOrders">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOrders" runat="server" DataSourceID="ObjectDataSourceOrders"
                            AllowPaging="True" AllowSorting="True" DataKeyNames="IssueLineId" AutoGenerateColumns="False"
                            OnSelectedIndexChanged="GridViewOrders_SelectedIndexChanged" AutoGenerateSelectButton="true"
                            AutoGenerateEditButton="true" PageSize="30">
                            <Columns>
                                <asp:BoundField ReadOnly="True" DataField="LineNumber" HeaderText="<%$ Resources:Default, LineNumber %>"
                                    SortExpression="LineNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="ExternalCompanyCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="ExternalCompany"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="DeliveryDate"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="DirectDeliveryCustomer" HeaderText="<%$ Resources:Default, DirectDeliveryCustomer %>"
                                    SortExpression="DirectDeliveryCustomer"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ExternalOrderNumber" HeaderText="<%$ Resources:Default, ExternalOrderNumber %>"
                                    SortExpression="ExternalOrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AreaType %>" SortExpression="AreaType">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListAreaType" runat="server" DataSourceID="ObjectDataSourceAreaType"
                                            DataTextField="AreaType" DataValueField="AreaType" SelectedValue='<%# Bind("AreaType") %>'>
                                        </asp:DropDownList>
                                        <asp:ObjectDataSource ID="ObjectDataSourceAreaType" runat="server" TypeName="Area"
                                            SelectMethod="GetAreaTypes">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("AreaType") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OriginalQuantity" HeaderText="<%$ Resources:Default, OriginalQuantity %>"
                                    SortExpression="OriginalQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="BackOrderQuantity" HeaderText="<%$ Resources:Default, BackorderQuantity %>"
                                    SortExpression="BackOrderQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="false" DataField="AllocateQuantity" HeaderText="<%$ Resources:Default, AllocateQuantity %>"
                                    SortExpression="AllocateQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="RemainingQuantity" HeaderText="<%$ Resources:Default, RemainingQuantity %>"
                                    SortExpression="RemainingQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Pack" HeaderText="<%$ Resources:Default, Packs %>"
                                    SortExpression="Pack"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonBackorderLine" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonBackorderFull" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceOrders" runat="server" TypeName="Backorders"
                    SelectMethod="SearchPlanning" UpdateMethod="UpdatePlanning">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="ProductCode" SessionField="ProductCode" Type="String" />
                        <asp:SessionParameter Name="Product" SessionField="Product" Type="String" />
                        <asp:SessionParameter Name="SKUCode" SessionField="SKUCode" Type="String" />
                        <asp:SessionParameter Name="Lines" SessionField="Lines" Type="Boolean" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="issueLineId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="areaType" Type="String" />
                        <asp:Parameter Name="allocateQuantity" Type="Decimal" DefaultValue="-1" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
