<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="DIPlanning.aspx.cs" Inherits="Outbound_DIPlanning"
    Title="<%$ Resources:Default, PlanningMaintenanceTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentWaveSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PlanningMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PlanningMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridOrders">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonCombine" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSplit" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonRelease" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridOrderLines">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Orders%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Lines%>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                &nbsp;<table>
                    <tr>
                        <td>
                            <uc1:OutboundSearch ID="OutboundSearch1" runat="server">
                            </uc1:OutboundSearch>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours %>"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label ID="LabelPicked" runat="server" Text="<%$ Resources:Default, Picked %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelBlue" runat="server" Text="<%$ Resources:Default, NotAllocatedRoute %>"
                                            Width="110px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageGreen" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelGreen" runat="server" Text="<%$ Resources:Default, NotAllocatedGT1Day %>"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:RadioButtonList ID="rblShowOrders" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="<%$ Resources:Default, Orders%>" Value="true"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, Shipment%>" Value="false" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
                <telerik:RadButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>">
                    <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
                <telerik:RadButton ID="ButtonCombine" runat="server" Text="<%$ Resources:Default, Combine %>" OnClick="ButtonCombine_Click" />
                <telerik:RadButton ID="ButtonSplit" runat="server" Text="<%$ Resources:Default, Split %>" OnClick="ButtonSplit_Click" />
                
                <telerik:RadButton ID="ButtonWavePlanning" runat="server" Text="<%$ Resources:Default, ButtonWavePlanning %>" OnClick="ButtonWavePlanning_Click" />
                <br />
                <br />
                <telerik:RadGrid ID="RadGridOrders" runat="server" Skin="Metro" AllowAutomaticUpdates="true"
                    AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
                    AutoGenerateColumns="False" AllowMultiRowSelection="True"
                    DataSourceID="ObjectDataSourcePlanning" OnRowDataBound="RadGridOrders_RowDataBound"
                    OnItemCommand="RadGridOrders_ItemCommand"
                    OnSelectedIndexChanged="RadGridOrders_SelectedIndexChanged">
                    <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                    <MasterTableView DataKeyNames="WaveId,OutboundShipmentId,IssueId" DataSourceID="ObjectDataSourcePlanning" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                                UniqueName="TemplateColumn">
                                <ItemTemplate>
                                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Wave" HeaderText="<%$ Resources:Default, Wave %>" SortExpression="Wave"></telerik:GridBoundColumn>
                            <telerik:GridButtonColumn HeaderText="<%$ Resources:Default, Release %>" CommandName="Select" DataTextField="Release"></telerik:GridButtonColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>" SortExpression="OutboundShipmentId"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="PrincipalCode" HeaderText="<%$ Resources:Default, PrincipalCode %>" SortExpression="PrincipalCode"></telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListPriorityId" 
                                ListTextField="Priority" ListValueField="PriorityId"
                                DataSourceID="ObjectDataSourcePriority" DataField="PriorityId" 
                                HeaderText="<%$ Resources:Default, Priority %>" 
                                FilterControlAltText="Filter DropDownListPriorityId column">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" SortExpression="NumberOfLines"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Units" HeaderText="<%$ Resources:Default, Units %>" SortExpression="Units"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" SortExpression="OutboundDocumentType"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Weight" HeaderText="<%$ Resources:Default, Weight %>" SortExpression="Weight"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="NumberOfOrders" HeaderText="<%$ Resources:Default, NumberOfOrders %>" SortExpression="NumberOfOrders"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="PercentageAvailable" HeaderText="% Units available for immediate picking" SortExpression="PercentageAvailable"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="PercentageDrawDown" HeaderText="Units % in Bulk to be drawn down" SortExpression="PercentageDrawDown"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="PercentageFulfilled" HeaderText="% of Total order that can be fulfilled" SortExpression="PercentageFulfilled"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="BulkPallets" HeaderText="Estimate number of pallets to pull out of Bulk" SortExpression="BulkPallets"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="PalletSpaces" HeaderText="Available pallet locations in Wave" SortExpression="PalletSpaces"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="SimilarSKUDI" HeaderText="DI's with similar SKU's" SortExpression="SimilarSKUDI"></telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn ButtonType="PushButton" 
                                UniqueName="EditCommandColumn1">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                        <Resizing AllowColumnResize="true"></Resizing>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByIssue">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="DIPlanning"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
                        <asp:SessionParameter Name="WaveId" SessionField="WaveId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                        <asp:ControlParameter ControlID="rblShowOrders" DefaultValue="0" Name="ShowOrders" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel2">
                <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
                <asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow2" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
                <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                <asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
                
                <telerik:RadGrid ID="RadGridOrderLines" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceOrderLines"
                    AllowAutomaticUpdates="true" AllowMultiRowSelection="true" 
                    AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30" ShowFooter="true" EnableLinqExpressions="true">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="IssueLineId,StorageUnitId" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                        <Columns>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                SortExpression="OrderNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                SortExpression="ProductCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                SortExpression="Product"></telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListBatchId" ListTextField="Batch" ListValueField="StorageUnitBatchId"
                                DataSourceID="ObjectDataSourceBatch" DataField="StorageUnitBatchId" HeaderText="<%$ Resources:Default, Batch %>" DropDownControlType="RadComboBox">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                SortExpression="SKUCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                SortExpression="OrderQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                SortExpression="Status"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                SortExpression="AvailablePercentage"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                SortExpression="AvailableQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>"
                                SortExpression="LocationsAllocated"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                        <Resizing AllowColumnResize="true"></Resizing>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="DIPlanning"
                    SelectMethod="GetOrderLines" UpdateMethod="UpdateOrderLine">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId"
                            DefaultValue="-1" />
                        <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" DefaultValue="-1" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Type="Int32" Name="issueLineId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="storageUnitBatchId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="storageUnitId"></asp:Parameter>
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
        </div>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
