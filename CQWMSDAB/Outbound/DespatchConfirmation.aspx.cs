using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using Microsoft.Reporting.WebForms;

public partial class Outbound_DespatchConfirmation : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Init";

        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                string despacthedDate = DateRange.GetFromDate().ToString();
                
                TextBoxDespatchedDate.Text = despacthedDate;
                
                //TextBoxDespatchedTime.Text = despacthedDate;

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("DespatchConfirmation" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewOutboundDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("DespatchConfirmation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region GridViewOutboundDocument_OnSelectedIndexChanged
    protected void GridViewOutboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString() == "")
                Session["OutboundShipmentId"] = -1;
            else
                Session["OutboundShipmentId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString());

            if (GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString() == "")
                Session["IssueId"] = -1;
            else
                Session["IssueId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString());

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewOutboundDocument_OnSelectedIndexChanged"

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            Session["FromURL"] = "~/Outbound/DespatchConfirmation.aspx";

            Session["ReportName"] = "Despatch Document";

            ReportParameter[] RptParameters = new ReportParameter[6];

            // Create the ConnectionString report parameter
            string strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

            // Create the OutboundShipmentId report parameter
            string outboundShipmentId = "1";// Session["OutboundShipmentId"].ToString();
            RptParameters[1] = new ReportParameter("OutboundShipmentId", outboundShipmentId);

            // Create the IssueId report parameter
            string issueId = "-1";//Session["IssueId"].ToString();
            RptParameters[2] = new ReportParameter("IssueId", issueId);

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("DespatchConfirmation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPrint_Click"

    #region ButtonUpdate_Click
    protected void ButtonUpdate_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonUpdate_Click";

        try
        {
            CheckBox cb = new CheckBox();
            DespatchConfirmation dc = new DespatchConfirmation();
            DateTime despatchedDate = DateTime.Today;

            if (DateTime.TryParse(TextBoxDespatchedDate.Text + " " + TextBoxDespatchedTime.Text, out despatchedDate))
                foreach (GridViewRow row in GridViewOutboundDocument.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");

                    if (cb.Checked)
                        if (dc.UpdateLoad(Session["ConnectionStringName"].ToString(),
                                            int.Parse(GridViewOutboundDocument.DataKeys[row.RowIndex].Values["OutboundShipmentId"].ToString()),
                                            int.Parse(GridViewOutboundDocument.DataKeys[row.RowIndex].Values["IssueId"].ToString()),
                                            despatchedDate))
                            cb.Checked = false;
                        else
                            Master.ErrorText = "Failed to update row";
                }

            GridViewOutboundDocument.DataBind();

            Master.MsgText = "Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("DespatchConfirmation" + "_" + ex.Message.ToString());
            Master.MsgText = ex.Message.ToString();
        }
    }
    #endregion "ButtonUpdate_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "DespatchConfirmation", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "DespatchConfirmation", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
