using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_ManualRequest : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        theErrMethod = "Page Load";

        if (!Page.IsPostBack)
        {
            try
            {
                //Manual Request - Show Delete Button
                ButtonDeleteLine.Visible = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 343);

                //Manual Request - Show Edit Button
                this.GridViewOutboundLine.AutoGenerateEditButton = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 344);

                Session["countLoopsToPreventInfinLoop"] = 0;

                Master.MsgText = "Page load successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
                Master.MsgText = result; Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonInsertLine_Click
    protected void ButtonInsertLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInsertLine_Click";
        
        try
        {
            if (Session["StorageUnitBatchId"] == null || (int)Session["StorageUnitBatchId"] == -1)
            {
                Master.ErrorText = "Please select a Product.";
                LabelErrorMsg.Text = "Please select a Product.";
                return;
            }

            if (TextBoxQuantity.Text == "" || TextBoxQuantity.Text == "0")
            {
                Master.ErrorText = "Please enter a Quantity.";
                LabelErrorMsg.Text = "Please enter a Quantity.";
                return;
            }

            Decimal quantity = Decimal.Parse(TextBoxQuantity.Text);

            ManualRequest mr = new ManualRequest();

            if (mr.RegisterRequest(Session["ConnectionStringName"].ToString(),
                                    (int)Session["WarehouseId"],
                                    int.Parse(Session["OperatorId"].ToString()),
                                    int.Parse(Session["StorageUnitBatchId"].ToString()),
                                    quantity))
            {
                TextBoxQuantity.Text = "";
                
                Master.ErrorText = "";
                LabelErrorMsg.Text = "";

                GridViewOutboundLine.DataBind();

                Master.MsgText = "InsertLine";
                Master.ErrorText = "";
            }
            else
            {
                Master.ErrorText = "May not enter duplicate Product /  Batch Combination";
                LabelErrorMsg.Text = "May not enter duplicate Product /  Batch Combination";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonInsertLine_Click"

    #region ButtonDeleteLine_Click
    protected void ButtonDeleteLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeleteLine_Click";
        try
        {
            if (GridViewOutboundLine.SelectedIndex != -1)
                GridViewOutboundLine.DeleteRow(GridViewOutboundLine.SelectedIndex);

            Master.MsgText = "Deleted"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDeleteLine_Click"

    #region ShowAlertMessage
    public static void ShowAlertMessage(string error)
    {
        Page page = HttpContext.Current.Handler as Page;

        if (page != null)
        {
            error = error.Replace("'", "\'");

            ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "alert('" + error + "');", true);
        }
    }
    #endregion

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + exMsg.Message.ToString());
                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
