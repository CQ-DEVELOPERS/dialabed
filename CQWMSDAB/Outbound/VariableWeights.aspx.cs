using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_VariableWeights : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    private int jobId = -1;
    private int instructionId = -1;
    private int reasonId = -1;
    private string statusCode = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

                TextBoxBarcode.Focus();
            }

            GridViewInstruction.DataBind();
            
            if (GridViewInstruction.Rows.Count > 0)
            {
                jobId = int.Parse(GridViewInstruction.DataKeys[0].Values["JobId"].ToString());
                instructionId = int.Parse(GridViewInstruction.DataKeys[0].Values["InstructionId"].ToString());
                statusCode = GridViewInstruction.DataKeys[0].Values["StatusCode"].ToString();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ResetPage
    protected void ResetPage()
    {
        TextBoxBarcode.Text = "";
        GridViewInstruction.Visible = false;
    }
    #endregion ResetPage

    #region ButtonEnter_Click
    protected void ButtonEnter_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEnter_Click";

        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"] , 63))
            {
                GridViewInstruction.Visible = false;
                GridViewInstruction.DataBind();
            }
            else
            {
                GridViewInstruction.Visible = true;
                GridViewInstruction.DataBind();
            }
            if (GridViewInstruction.Rows.Count > 0)
            {
                Session["JobId"] = int.Parse(GridViewInstruction.DataKeys[0].Values["JobId"].ToString());
                Master.MsgText = Resources.Default.Successful;
            }
            else
                Master.ErrorText = Resources.Messages.ErrorRetrieving;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEnter_Click

    /// <summary>
    /// Refreshes the GridViewInstruction after updating GridViewProducts
    /// </summary>
    /// <param name="s"></param>
    /// <param name="e"></param>
    protected void GridViewUpdated(Object s, GridViewUpdatedEventArgs e)
    {
        GridViewInstruction.DataBind();
    }

    #region GridView_RowEditing
    protected void GridView_RowEditing(Object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
    {
        Session["StorageUnitId"] = int.Parse(GridViewProducts.DataKeys[e.NewEditIndex].Values["StorageUnitId"].ToString());
    }
    #endregion

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "StretchWrap", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "StretchWrap", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}
